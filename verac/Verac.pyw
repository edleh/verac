#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) 2009-2022 VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
# Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le
# modifier conformément aux dispositions de la Licence Publique Générale GNU,
# telle que publiée par la Free Software Foundation ; version 3 de la licence,
# ou encore toute version ultérieure.
# 
# Ce programme est distribué dans l'espoir qu'il sera utile,
# mais SANS AUCUNE GARANTIE ; sans même la garantie implicite de
# COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de
# détail, voir la Licence Publique Générale GNU.
#
# Vous devez avoir reçu un exemplaire de la Licence Publique Générale
# GNU en même temps que ce programme ; si ce n'est pas le cas, voir
# <http://www.gnu.org/licenses/>.
# -------------------------------------------------------------------------------


"""
DESCRIPTION :
    Fichier de lancement du logiciel.
"""


# importation des modules utiles :
import sys
import os
os.putenv('QTWEBENGINE_DISABLE_SANDBOX', '1')

# récupération du chemin :
HERE = os.path.dirname(os.path.abspath(__file__))
# ajout du chemin au path (+ libs) :
sys.path.insert(0, HERE)
sys.path.insert(0, HERE + os.sep + 'libs')
# on démarre dans le bon dossier :
os.chdir(HERE)

# importation des modules perso :
import utils
utils.changeHere(HERE)
import utils_filesdirs
import main

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui

# On récupère l'emplacement du fichier de log.
# et on en profite pour savoir si le programme est lancé pour la première fois.
# Enfin on met en place le log :
FIRST, logFile = utils.getLogFileName(mustCreate=True)
if not('NOLOG' in sys.argv):
    utils.changeLogFile(True)



if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    utils.loadStyle()

    pixmap = QtGui.QPixmap('./images/splash.png')
    splash = QtWidgets.QSplashScreen(pixmap, QtCore.Qt.Dialog)
    splash.setMask(pixmap.mask())
    splash.show()
    app.processEvents()

    #******************************************
    # Installation de l'internationalisation :
    #******************************************
    locale = QtCore.QLocale.system().name()
    if len(locale) < 1:
        locale = 'fr_FR'
    # recherche d'un i18n passé en argument (par exemple LANG=fr_FR) :
    for arg in sys.argv:
        if arg.split('=')[0] == 'LANG':
            locale = arg.split('=')[1]
    # traduction de Qt (boutons des dialogues, etc) :
    qtTranslationsPath = QtCore.QLibraryInfo.location(
        QtCore.QLibraryInfo.TranslationsPath)
    qtTranslator = QtCore.QTranslator()
    if qtTranslator.load('qtbase_' + locale, qtTranslationsPath):
        app.installTranslator(qtTranslator)
    elif qtTranslator.load('qt_' + locale, qtTranslationsPath):
        app.installTranslator(qtTranslator)
    # traduction du logiciel :
    appTranslationsPath = QtCore.QDir('./translations').canonicalPath()
    appLocalefile = '{0}_{1}'.format(utils.PROGLINK, locale)
    appTranslator = QtCore.QTranslator()
    if appTranslator.load(appLocalefile, appTranslationsPath):
        app.installTranslator(appTranslator)
    utils.changeLocale(locale)

    #******************************************
    # Lancement du logiciel :
    #******************************************
    app.setWindowIcon(QtGui.QIcon('./images/icon.png'))

    mainWindow = main.MainWindow(HERE, splash, FIRST)
    if utils.MODEBAVARD:
        mainWindow.show()
    else:
        mainWindow.showMaximized()
    mainWindow.interfaceLoaded()

    try:
        sys.exit(app.exec_())
    finally:
        utils_filesdirs.deleteLockFile()
        utils.changeLogFile(False)


