# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    blablabla
"""

# importation des modules utiles :
import utils, utils_functions


#http://www.developpez.net/forums/d933074/autres-langages/python-zope/general-python/encoder-carcteres-speciaux-html-entity/


# Source for the list of special characters
#    http://www.commentcamarche.net/contents/html/htmlcarac.php3

# THE UPPER CASE LETTERS
SPECIAL_CHARACTERS = [
# A
    (utils_functions.u('À'), "&Agrave;"),
    (utils_functions.u('Á'), "&Aacute;"),
    (utils_functions.u('Â'), "&Acirc;"),
    (utils_functions.u('Ã'), "&Atilde;"),
    (utils_functions.u('Ä'), "&Auml;"),
    (utils_functions.u('Å'), "&Aring;"),
    (utils_functions.u('Æ'), "&Aelig;"),
    (utils_functions.u('à'), "&agrave;"),
    (utils_functions.u('á'), "&aacute;"),
    (utils_functions.u('â'), "&acirc;"),
    (utils_functions.u('ã'), "&atilde;"),
    (utils_functions.u('ä'), "&auml;"),
    (utils_functions.u('å'), "&aring;"),
    (utils_functions.u('æ'), "&aelig;"),
# C
    (utils_functions.u('Ç'), "&Ccedil;"),
    (utils_functions.u('ç'), "&ccedil;"),
# E
    (utils_functions.u('È'), "&Egrave;"),
    (utils_functions.u('É'), "&Eacute;"),
    (utils_functions.u('Ê'), "&Ecirc;"),
    (utils_functions.u('Ë'), "&Euml;"),
    (utils_functions.u('è'), "&egrave;"),
    (utils_functions.u('é'), "&eacute;"),
    (utils_functions.u('ê'), "&ecirc;"),
    (utils_functions.u('ë'), "&euml;"),
# I
    (utils_functions.u('Ì'), "&Igrave;"),
    (utils_functions.u('Í'), "&Iacute;"),
    (utils_functions.u('Î'), "&Icirc;"),
    (utils_functions.u('Ï'), "&Iuml;"),
    (utils_functions.u('ì'), "&igrave;"),
    (utils_functions.u('í'), "&iacute;"),
    (utils_functions.u('î'), "&icirc;"),
    (utils_functions.u('ï'), "&iuml;"),
# N
    (utils_functions.u('Ñ'), "&Ntilde;"),
    (utils_functions.u('ñ'), "&ntilde;"),
# O
    (utils_functions.u('Ò'), "&Ograve;"),
    (utils_functions.u('Ó'), "&Oacute;"),
    (utils_functions.u('Ô'), "&Ocirc;"),
    (utils_functions.u('Õ'), "&Otilde;"),
    (utils_functions.u('Ö'), "&Ouml;"),
    (utils_functions.u('Œ'), "&Oelig;"),
    (utils_functions.u('ò'), "&ograve;"),
    (utils_functions.u('ó'), "&oacute;"),
    (utils_functions.u('ô'), "&ocirc;"),
    (utils_functions.u('õ'), "&otilde;"),
    (utils_functions.u('ö'), "&ouml;"),
    (utils_functions.u('œ'), "&oelig;"),
# U
    (utils_functions.u('Ù'), "&Ugrave;"),
    (utils_functions.u('Ú'), "&Uacute;"),
    (utils_functions.u('Û'), "&Ucirc;"),
    (utils_functions.u('Ü'), "&Uuml;"),
    (utils_functions.u('ù'), "&ugrave;"),
    (utils_functions.u('ú'), "&uacute;"),
    (utils_functions.u('û'), "&ucirc;"),
    (utils_functions.u('ü'), "&uuml;"),
# Y
    (utils_functions.u('Ý'), "&Yacute;"),
    (utils_functions.u('Ÿ'), "&Yuml;"),
    (utils_functions.u('ý'), "&yacute;"),
    (utils_functions.u('ÿ'), "&yuml;"),
                     ]




"""
"   &#34;   &quot;
&   &#38;   &amp;
€   &#128;  &euro;
Space   &#160;  &nbsp;
¢   &#162;  &cent;
£   &#163;  &pound;
¤   &#164;  &curren;
¥   &#165;  &yen
¦   &#166;  &brvbar;
§   &#167;  &sect;
¨   &#168;  &uml;
©   &#169;  &copy;
ª   &#170;  &ordf;
«   &#171;  &laquo;
¬   &#172;  &not;
®   &#174;  &reg;
¯   &#175;  &masr;
°   &#176;  &deg;
±   &#177;  &plusmn;
²   &#178;  &sup2;
³   &#179;  &sup3;
'   &#180;  &acute;
µ   &#181;  &micro;
¶   &#182;  &para;
·   &#183;  &middot;
¸   &#184;  &cedil;
¹   &#185;  &sup1;
º   &#186;  &ordm;
»   &#187;  &raquo;
¼   &#188;  &frac14;
½   &#189;  &frac12;
¾   &#190;  &frac34;
¿   &#191;  &iquest;

×   &#215;  &times;
Ø   &#216;  &Oslash;
Ð   &#208;  &eth;
Þ   &#222;  &thorn;
ß   &#223;  &szlig;

ð   &#240;  &eth;
÷   &#247;  &divide;
ø   &#248;  &oslash;
þ   &#254;  &thorn;
"""












def encodeHtml(inText):
    result = utils_functions.u(inText)

    for correspondance in SPECIAL_CHARACTERS:
        replaceWhat = correspondance[0]
        replaceWith = correspondance[1]
        result = result.replace(replaceWhat, replaceWith)

    return result







