# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    On regroupe ici les procédures d'import export
    vers ODF, vers pdf, imprimante
    depuis et vers csv
    vers Freeplane
    depuis et vers xml
    etc
"""

# importation des modules utiles:
from __future__ import division, print_function

import utils, utils_functions, utils_webengine, utils_db

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtPrintSupport
    from PyQt5 import QtSql
else:
    from PyQt4 import QtCore, QtGui as QtWidgets
    from PyQt4 import QtGui as QtPrintSupport
    from PyQt4 import QtSql



# Constantes utiles :
PDF_PRINT = ('PDF', 'PRINT')
PDF_GENERATED = False
PRINT_WEBVIEW = None


###########################################################"
#   EXPORTS DEPUIS L'INTERFACE PROF
#   (procédure générale pour regrouper les différents cas)
###########################################################"

def exportQui2Quoi(main, paramsQui={}, paramsQuoi={}, msgFin=True):
    """
    fonction très générale pour gérer pleins de cas
    QUI c'est ce qui doit être exporté (tous les tableaux, la vue courante, ...)
    QUOI c'est ce qu'on en fait (un fichier ODF, impression, pdf, ...)
    """
    utils_functions.doWaitCursor()
    try:
        # définition des objets utiles du Quoi:
        exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
        fileName = ''
        if 'fileName' in paramsQuoi:
            fileName = paramsQuoi['fileName']
        utils_functions.restoreCursor()
        if paramsQuoi['quoi'] in PDF_PRINT:
            printer = createPrinter(fileName)
            if printer == False:
                return
        utils_functions.doWaitCursor()

        # traitement du Qui (dépend un peu du quoi quand même):
        if paramsQui['qui'] == 'actualVue':
            import prof
            # on récupère les données liées à la sélection :
            dataSelection = prof.diversFromSelection(main, main.id_tableau)
            # on remplit les paramètres de la table:
            paramsTable = {
                'id_tableau': main.id_tableau,
                'dataSelection': dataSelection,
                'viewType': main.viewType,
                'viewTypeName': utils_functions.changeBadChars(main.viewButton.text()),
                'id_eleve': main.changeEleveComboBox.itemData(
                    main.changeEleveComboBox.currentIndex(), QtCore.Qt.UserRole),
                'eleveName': main.changeEleveComboBox.currentText()}
            exportDatas = view2dic(main, paramsTable, exportDatas)
            if (paramsQuoi['quoi'] in PDF_PRINT) and (main.viewType == utils.VIEW_ELEVE):
                printer.setOrientation(QtPrintSupport.QPrinter.Portrait)
        elif paramsQui['qui'] == 'actualGroup':
            import prof, prof_groupes
            # on récupère les données liées à la sélection :
            dataSelection = prof.diversFromSelection(main)
            # on récupère tous les tableaux du groupe pour la période considérée
            tableaux = prof_groupes.tableauxFromGroupe(main, dataSelection[0], dataSelection[3])
            # on crée d'abord un onglet pour chaque vue liée au groupe :
            interface = utils_db.readInConfigDict(
                main.configDict, 'SimplifiedInterface', default_int=1)[0]
            for viewType in utils.VIEWS_IF_GROUP:
                if viewType == utils.VIEW_ELEVE:
                    continue
                # on récupère le nom de la vue :
                viewTypeName = ''
                for action in main.viewMenuActions[interface]:
                    if (action != None) and (action.isVisible()):
                        if action.data()[1] == viewType:
                            viewTypeName = action.data()[2]
                if viewTypeName == '':
                    continue
                # on remplit les paramètres de la table:
                paramsTable = {
                    'id_tableau': main.id_tableau,
                    'dataSelection': dataSelection,
                    'viewType': viewType,
                    'viewTypeName': viewTypeName,
                    'id_eleve': -1,
                    'eleveName': ''}
                exportDatas = view2dic(main, paramsTable, exportDatas)
            # on crée aussi un onglet "items" par tableau :
            query_my = utils_db.query(main.db_my)
            for id_tableau in tableaux:
                commandLine_my = utils_db.q_selectAllFromWhere.format(
                    'tableaux', 'id_tableau', id_tableau)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    tableauName = query_my.value(1)
                # on récupère les données liées à la sélection :
                dataSelection = prof.diversFromSelection(main, id_tableau)
                # création d'un onglet "items" :
                viewType = utils.VIEW_ITEMS
                viewTypeName = utils_functions.u('Items - {0}').format(tableauName)
                # on remplit les paramètres de la table:
                paramsTable = {
                    'id_tableau': id_tableau,
                    'dataSelection': dataSelection,
                    'viewType': viewType,
                    'viewTypeName': viewTypeName,
                    'id_eleve': -1,
                    'eleveName': ''}
                exportDatas = view2dic(main, paramsTable, exportDatas)
                # création d'un onglet "stats" :
                viewType = utils.VIEW_STATS_TABLEAU
                viewTypeName = utils_functions.u('Stats - {0}').format(tableauName)
                # on remplit les paramètres de la table:
                paramsTable = {
                    'id_tableau': id_tableau,
                    'dataSelection': dataSelection,
                    'viewType': viewType,
                    'viewTypeName': viewTypeName,
                    'id_eleve': -1,
                    'eleveName': ''}
                exportDatas = view2dic(main, paramsTable, exportDatas)
        # finalisation du Quoi:
        if paramsQuoi['quoi'] == 'ODS':
            exportDic2ods(main, exportDatas, fileName, msgFin=False)
        elif paramsQuoi['quoi'] in PDF_PRINT:
            htmlFileName = exportDic2html(main, exportDatas)
            htmlToPrinter(main, htmlFileName, printer, fileName)
        if msgFin:
            if fileName != '':
                utils_functions.afficheMsgFinOpen(main, fileName)
            else:
                utils_functions.afficheMsgFin(main, timer=5)
    except:
        if paramsQuoi['quoi'] == 'PRINT':
            message = QtWidgets.QApplication.translate('main', 'Failed to print')
            utils_functions.afficheMsgPb(main, message)
        else:
            message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
            utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)


def view2dic(main, paramsTable, exportDatas, fullTitle=False):
    """
    Ajout d'un table de type vue dans un dictionnaire.
        paramsTable : les paramètres de la table à ajouter
        exportDatas : le dictionnaire à compléter
    return exportDatas
    """
    id_tableau = paramsTable['id_tableau']
    viewTypeName = paramsTable['viewTypeName']
    viewType = paramsTable['viewType']
    # calcul de la vue à récupérer :
    import prof
    if viewType == utils.VIEW_ITEMS:
        columns, rows = prof.calcItems(main, id_tableau=id_tableau)
    elif viewType == utils.VIEW_STATS_TABLEAU:
        columns, rows = prof.calcStats(main, id_tableau=id_tableau)
    elif viewType == utils.VIEW_BILANS:
        columns, rows = prof.calcBilans(main, id_tableau=id_tableau)
    elif viewType == utils.VIEW_BULLETIN:
        columns, rows = prof.calcBulletin(main, id_tableau=id_tableau)
    elif viewType == utils.VIEW_APPRECIATIONS:
        columns, rows = prof.calcAppreciations(main, id_tableau=id_tableau)
    elif viewType == utils.VIEW_STATS_GROUPE:
        columns, rows = prof.calcStats(main)
    elif viewType == utils.VIEW_ELEVE:
        id_eleve = paramsTable['id_eleve']
        columns, rows = prof.calcEleve(main, id_eleve, id_tableau)
        appreciation = main.editAppreciation.toPlainText()
        #print('appreciation :\n', len(appreciation), appreciation)
    elif viewType == utils.VIEW_NOTES:
        columns, rows = prof.calcNotes(main)
    elif viewType == utils.VIEW_ABSENCES:
        columns, rows = prof.calcAbsences(main)
    elif viewType == utils.VIEW_COUNTS:
        columns, rows = prof.calcCounts(main)
    elif viewType == utils.VIEW_ACCOMPAGNEMENT:
        columns, rows = prof.calcAccompagnement(main)
    elif viewType == utils.VIEW_CPT_NUM:
        columns, rows = prof.calcCptNumeriques(main)
    elif viewType == utils.VIEW_DNB:
        columns, rows = prof.calcDNB(main)
    elif viewType == utils.VIEW_SUIVIS:
        # on prend la vue courante si c'est viewSuivis
        if main.viewType == utils.VIEW_SUIVIS:
            columns, rows = main.model.columns, main.model.rows
        else:
            columns, rows = prof.calcSuivis(main)

    # création du titre :
    theTitle = ''
    if paramsTable['dataSelection'][5] == 1:
        messagePublic = QtWidgets.QApplication.translate('main', 'public')
    else:
        messagePublic = QtWidgets.QApplication.translate('main', 'private')
    if fullTitle:
        listTitles = [
            paramsTable['dataSelection'][2], 
            paramsTable['dataSelection'][1], 
            utils.PERIODES[paramsTable['dataSelection'][3]], 
            paramsTable['viewTypeName'], 
            ]
    else:
        listTitles = [paramsTable['viewTypeName'], ]
    if viewType in utils.VIEWS_IF_TABLEAU:
        #listTitles.append(paramsTable['dataSelection'][4])
        listTitles.append(messagePublic)
    for title in listTitles:
        if theTitle == '':
            theTitle = utils_functions.u('{0}').format(title)
        else:
            theTitle = utils_functions.u('{0} | {1}').format(theTitle, title)
    # en vue élève, on ajoute le nom de l'élève :
    eleveName = ''
    if viewType == utils.VIEW_ELEVE:
        eleveName = paramsTable['eleveName']
    # on lance le remplissage du dictionnaire :
    exportDatas = what2dic(
        exportDatas, viewTypeName, columns, rows, 
        viewType=viewType, theTitle=theTitle, theSubTitle=eleveName)
    return exportDatas


def what2dic(exportDatas, what, columns, rows, 
            viewType=-1, theTitle='', theSubTitle=''):
    """
    Complète le dico exportDatas avec une nouvelle table à exporter
        (en ods, pdf ou impression).
    what : le nom de la table
    columns, rows : les données (colonnes et lignes)
    columnsToIgnore : pour ne pas exporter certaines colonnes (NO_EXPORT=True)
    viewType : le type de la vue (si appelé depuis l'interface prof)
    theTitle, theSubTitle : titre et sous-titre du document (pour pdf et impression)
    """
    theList = []
    line = []
    columnsToIgnore = []
    column = 0
    for title in columns:
        if 'NO_EXPORT' in title:
            columnsToIgnore.append(column)
        else:
            lineDico = {'value': title['value'], 
                        'bold': True, 
                        'columnWidth': '2.0cm',
                        'alignHorizontal': 'center'}
            if 'toolTip' in title:
                lineDico['toolTip'] = title['toolTip']
            if 'columnWidth' in title:
                lineDico['columnWidth'] = title['columnWidth']
            if title['value'] == 'VSEPARATOR':
                lineDico['value'] = ''
                lineDico['columnWidth'] = '0.2cm'
                lineDico['cellColor'] = utils.colorGray.name()
            line.append(lineDico)
        column += 1
    theList.append(line)
    for aLine in rows:
        line = []
        column = 0
        for cell in aLine:
            if not(column in columnsToIgnore):
                lineDico = {'value': cell['value'], 
                            'alignHorizontal': 'center',
                            'bold': False}
                if viewType in utils.VIEWS_WITH_APPRECIATIONS:
                    lineDico['rowHeight'] = '1.3cm'
                if 'type' in cell:
                    lineDico['type'] = cell['type']
                if 'color' in cell:
                    lineDico['cellColor'] = cell['color'].name()
                if 'bold' in cell:
                    lineDico['bold'] = True
                if 'text-color' in cell:
                    lineDico['textColor'] = cell['text-color'].name()
                if 'align' in cell:
                    lineDico['alignHorizontal'] = cell['align']
                elif 'type' in cell:
                    if cell['type'] in utils.TYPES_ALIGN_LEFT:
                        lineDico['alignHorizontal'] = 'left'
                        if cell['type'] == utils.TEXT:
                            lineDico['wrap'] = 'wrap'
                    elif cell['type'] in utils.TYPES_ALIGN_RIGHT:
                        lineDico['alignHorizontal'] = 'right'
                if cell['value'] == 'HSEPARATOR':
                    lineDico['value'] = ''
                    lineDico['rowHeight'] = '0.2cm'
                    lineDico['cellColor'] = utils.colorGray.name()
                elif cell['value'] == 'VSEPARATOR':
                    lineDico['value'] = ''
                    lineDico['columnWidth'] = '0.2cm'
                    lineDico['cellColor'] = utils.colorGray.name()
                line.append(lineDico)
            column += 1
        theList.append(line)
    exportDatas[what] = theList
    exportDatas['TABLES_LIST'].append(what)
    if theTitle == '':
        theTitle = what
    exportDatas['TABLES_PARAMS'][what] = {
        'title': theTitle, 'subTitle': theSubTitle, 'viewType': viewType}
    return exportDatas



###########################################################"
#    EXPORTS ODS
###########################################################"

def exportStructure2ods(main, fileName):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'exportStructure2ods')
    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}

    try:
        model_my = QtSql.QSqlTableModel(main, main.db_my)
        model_commun = QtSql.QSqlTableModel(main, main.db_commun)
        query_my = utils_db.query(main.db_my)
        query_commun = utils_db.query(main.db_commun)

        def tableDB2tableODS(tableName, model, query):
            # Test de l'existence de la table:
            OK = False
            commandLine = utils_db.q_selectAllFrom.format(tableName)
            query = utils_db.queryExecute(commandLine, query=query)
            while query.next():
                OK = True
            if not(OK):
                return
            # Start the table, and describe the columns
            utils_functions.afficheMessage(main, tableName)
            model.setTable(tableName)
            model.select()
            theList = []
            # récupération des titres des colonnes :
            line = []
            for i in range(model.columnCount()):
                titleColumn = model.headerData(
                    i, 
                    QtCore.Qt.Horizontal, 
                    QtCore.Qt.DisplayRole)
                name = utils_functions.code2name(main, titleColumn)
                lineDico = {
                    'value': utils_functions.u(name), 
                    'bold': True, 
                    'alignHorizontal': 'center'}
                line.append(lineDico)
            theList.append(line)
            # récupération du contenu :
            commandLine = utils_db.q_selectAllFrom.format(tableName)
            query = utils_db.queryExecute(commandLine, query=query)
            while query.next():
                line = []
                for i in range(model.columnCount()):
                    value = query.value(i)
                    try:
                        value = float(value)
                        line.append({'value': value, 'type': utils.FLOAT})
                    except:
                        try:
                            value = int(value)
                            line.append({
                                'value': value, 'type': utils.INTEGER})
                        except:
                            line.append({
                                'value': utils_functions.u(value)})
                theList.append(line)
            # inscription dans le dictionnaire :
            exportDatas[tableName] = theList
            exportDatas['TABLES_LIST'].append(tableName)
            exportDatas['TABLES_PARAMS'][tableName] = {'title': tableName}

        def creeTableODS(feuilleName, query):
            theList = []
            utils_functions.afficheMessage(main, feuilleName)
            if feuilleName == "bilans_items":
                OK = False
                query = utils_db.queryExecute(
                    utils_db.q_selectAllFromOrder.format(
                        'bilans', 'UPPER(Name)'), 
                    query=query)
                while query.next():
                    OK = True
                if not(OK):
                    return
                # récupération des titres des colonnes :
                line = []
                for titleColumn in ('bilanName', 'bilanLabel', 'itemName', 'itemLabel', 'coeff'):
                    lineDico = {'value': utils_functions.u(titleColumn), 
                                'bold': True, 
                                'alignHorizontal': 'center'}
                    line.append(lineDico)
                theList.append(line)
                # récupération du contenu :
                commandLine = (
                    'SELECT bilans.id_bilan, bilans.Name, bilans.Label, '
                    'items.Name, items.Label, '
                    'item_bilan.coeff '
                    'FROM bilans '
                    'JOIN item_bilan ON item_bilan.id_bilan=bilans.id_bilan '
                    'JOIN items ON items.id_item=item_bilan.id_item '
                    'ORDER BY bilans.Name, items.Name')
                id_bilanEX = -1
                query = utils_db.queryExecute(commandLine, query=query)
                while query.next():
                    id_bilan = int(query.value(0))
                    bilanName = query.value(1)
                    bilanLabel = query.value(2)
                    itemName = query.value(3)
                    itemLabel = query.value(4)
                    coeff = int(query.value(5))
                    if id_bilan != id_bilanEX:
                        id_bilanEX = id_bilan
                        line = []
                        line.append({'value': utils_functions.u(bilanName)})
                        line.append({'value': utils_functions.u(bilanLabel)})
                        line.append({'value': ''})
                        line.append({'value': ''})
                        line.append({'value': ''})
                        theList.append(line)
                    line = []
                    line.append({'value': ''})
                    line.append({'value': ''})
                    line.append({'value': utils_functions.u(itemName)})
                    line.append({'value': utils_functions.u(itemLabel)})
                    line.append({'value': coeff, 'type': utils.INTEGER})
                    theList.append(line)

            elif feuilleName == "items_bilans":
                OK = False
                query = utils_db.queryExecute(
                    utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
                    query=query)
                while query.next():
                    OK = True
                if not(OK):
                    return
                # récupération des titres des colonnes :
                line = []
                for titleColumn in ('itemName', 'itemLabel', 'bilanName', 'bilanLabel', 'coeff'):
                    lineDico = {'value': utils_functions.u(titleColumn), 
                                'bold': True, 
                                'alignHorizontal': 'center'}
                    line.append(lineDico)
                theList.append(line)
                # récupération du contenu :
                commandLine = (
                    'SELECT items.id_item, items.Name, items.Label, '
                    'bilans.Name, bilans.Label, '
                    'item_bilan.coeff '
                    'FROM items '
                    'JOIN item_bilan ON item_bilan.id_item=items.id_item '
                    'JOIN bilans ON bilans.id_bilan=item_bilan.id_bilan '
                    'ORDER BY items.Name, bilans.Name')
                id_itemEX = -1
                query = utils_db.queryExecute(commandLine, query=query)
                while query.next():
                    id_item = int(query.value(0))
                    itemName = query.value(1)
                    itemLabel = query.value(2)
                    bilanName = query.value(3)
                    bilanLabel = query.value(4)
                    coeff = int(query.value(5))
                    if id_item != id_itemEX:
                        id_itemEX = id_item
                        line = []
                        line.append({'value': utils_functions.u(itemName)})
                        line.append({'value': utils_functions.u(itemLabel)})
                        line.append({'value': ''})
                        line.append({'value': ''})
                        line.append({'value': ''})
                        theList.append(line)
                    line = []
                    line.append({'value': ''})
                    line.append({'value': ''})
                    line.append({'value': utils_functions.u(bilanName)})
                    line.append({'value': utils_functions.u(bilanLabel)})
                    line.append({'value': coeff, 'type': utils.INTEGER})
                    theList.append(line)

            # inscription dans le dictionnaire :
            exportDatas[feuilleName] = theList
            exportDatas['TABLES_LIST'].append(feuilleName)
            exportDatas['TABLES_PARAMS'][feuilleName] = {'title': feuilleName}

        for what in ('items', 'bilans'):
            tableDB2tableODS(what, model_my, query_my)
        for what in ('bulletin', 'referentiel', 'confidentiel', 'suivi'):
            tableDB2tableODS(what, model_commun, query_commun)
        for what in ('bilans_items', 'items_bilans'):
            creeTableODS(what, query_my)

        exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


def studentsExportAll2ods(main, fileName):
    """
    création d'un fichier ODS contenant la liste 
    des élèves de l'établissement.
    Un premier onglet contient toute la liste.
    Il y a ensuite un onglet par classe.
    """
    utils_functions.afficheMessage(main, 'studentsExportAll2ods')
    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
    try:
        query_commun = utils_db.query(main.db_commun)
        query_users = utils_db.query(main.db_users)
        # récupération des classes :
        classes = []
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            classes.append(query_commun.value(1))
        exportDatas['TABLES_LIST'].append('ALL')
        exportDatas['TABLES_PARAMS']['ALL'] = {
            'title': QtWidgets.QApplication.translate(
                'main', 'All Students')}
        for classe in classes:
            exportDatas['TABLES_LIST'].append(classe)
            exportDatas['TABLES_PARAMS'][classe] = {'title': classe}
        # titres des colonnes :
        titles = []
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'NAME'), 
            'rowHeight': '1.0cm', 
            'bold': True, 
            'alignHorizontal': 'left', 
            'columnWidth': '5.0cm', 
            'border': False})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'FIRSTNAME'), 
            'bold': True, 
            'alignHorizontal': 'left', 
            'columnWidth': '4.0cm', 
            'border': False})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'CLASS'), 
            'bold': True, 
            'alignHorizontal': 'center', 
            'border': False})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'SEX'), 
            'bold': True, 
            'alignHorizontal': 'center', 
            'border': False})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'BIRTH'), 
            'bold': True, 
            'alignHorizontal': 'center', 
            'border': False})
        formula = '=CONCATENATE(ROWS(A4:A9999)-COUNTBLANK(A4:A9999); " {0}")'
        # premier onglet (tous) :
        theList = []
        # contenu :
        commandLine_users = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_users, 
            order=['Classe', 'NOM', 'Prenom'], 
            query=query_users)
        theList.append([
            {
                'value': utils_functions.u(
                    '{0} {1}').format(
                        len(queryList), 
                        QtWidgets.QApplication.translate(
                            'main', 'students')), 
                'stringFormula': utils_functions.u(formula).format(
                    QtWidgets.QApplication.translate('main', 'students')), 
                'rowHeight': '2.0cm', 
                'columnWidth': '5.0cm'}, 
            {'value': '', 'columnWidth': '4.0cm'}, 
            {'value': ''}, 
            {'value': ''}, 
            {'value': ''}])
        theList.append([
            {'value': '', 'rowHeight': '0.5cm', 'border': False}, 
            {'value': '', 'border': False}, 
            {'value': '', 'border': False}, 
            {'value': '', 'border': False}, 
            {'value': '', 'border': False}])
        theList.append(titles)
        sex2Initial = {1: 'M', 2: 'F', -1: ''}
        for record in queryList:
            line = []
            line.append({'value': utils_functions.u(record[1])})
            line.append({'value': utils_functions.u(record[2])})
            line.append({
                'value': utils_functions.u(record[3]), 
                'alignHorizontal': 'center'})
            try:
                sex = sex2Initial.get(int(record[7]), '')
                line.append({
                    'value': utils_functions.u(sex), 
                    'alignHorizontal': 'center'})
                dateNaissance = utils_functions.u(
                    '{0}-{1}-{2}').format(
                        record[8][4:], 
                        record[8][2:4], 
                        record[8][0:2])
                line.append({
                    'value': dateNaissance, 
                    'alignHorizontal': 'center'})
            except:
                line.append({'value': ''})
                line.append({'value': ''})
            theList.append(line)
        exportDatas['ALL'] = theList
        # les onglets des classes :
        for classe in classes:
            theList = []
            # contenu :
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (classe, )}, 
                order=['NOM', 'Prenom'], 
                query=query_users)
            theList.append([
                {
                    'rowHeight': '2.0cm', 
                    'value': classe, 
                    'fontSize': 20, 
                    'bold': True, 
                    'columnWidth': '5.0cm'}, 
                {
                    'value': utils_functions.u(
                        '{0} {1}').format(
                            len(queryList), 
                            QtWidgets.QApplication.translate(
                                'main', 'students')), 
                    'stringFormula': utils_functions.u(formula).format(
                        QtWidgets.QApplication.translate(
                            'main', 'students')), 
                    'columnWidth': '4.0cm'}, 
                {'value': ''}, 
                {'value': ''}, 
                {'value': ''}])
            theList.append([
                {'value': '', 'rowHeight': '0.5cm', 'border': False}, 
                {'value': '', 'border': False}, 
                {'value': '', 'border': False}, 
                {'value': '', 'border': False}, 
                {'value': '', 'border': False}])
            theList.append(titles)
            for record in queryList:
                line = []
                line.append({'value': utils_functions.u(record[1])})
                line.append({'value': utils_functions.u(record[2])})
                line.append({
                    'value': utils_functions.u(record[3]), 
                    'alignHorizontal': 'center'})
                try:
                    sex = sex2Initial.get(int(record[7]), '')
                    line.append({
                        'value': utils_functions.u(sex), 
                        'alignHorizontal': 'center'})
                    dateNaissance = utils_functions.u(
                        '{0}-{1}-{2}').format(
                            record[8][4:], 
                            record[8][2:4], 
                            record[8][0:2])
                    line.append({
                        'value': dateNaissance, 
                        'alignHorizontal': 'center'})
                except:
                    line.append({'value': ''})
                    line.append({'value': ''})
                theList.append(line)
            exportDatas[classe] = theList
        # on termine :
        exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


class ItemsListDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, text=''):
        super(ItemsListDlg, self).__init__(parent)
        import utils_2lists

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Create ODS spreadsheet for an assessment'))

        # Texte d'explications :
        helpText = QtWidgets.QApplication.translate(
            'main', 
            'Select below the items you want by moving them to the right list.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u('<p align="center"><b>{0}</b></p>').format(helpText))

        # sélection des items (double liste) :
        self.selectionWidget = utils_2lists.DoubleListWidget(self.main)

        # les boutons :
        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]

        # on agence tout ça :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(helpLabel)
        # sélection des items :
        layout.addWidget(self.selectionWidget)
        # les boutons :
        layout.addLayout(buttonsLayout)
        self.setLayout(layout)

        self.reloadList()
        self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('export-items-list2ods')

    def reloadList(self):
        """
        remplissage de la liste des items
        """
        query_my = utils_db.query(self.main.db_my)
        self.selectionWidget.baseList.clear()
        self.selectionWidget.selectionList.clear()

        tableaux = [self.main.id_tableau]

        # on liste tous les items :
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'UPPER(Name)'), 
            query=query_my)
        query_my2 = utils_db.query(self.main.db_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            itemText = utils_functions.u(
                '{0} \t[{1}]').format(itemName, itemLabel)
            item = QtWidgets.QListWidgetItem(itemText)
            item.setToolTip(itemLabel)
            item.setData(QtCore.Qt.UserRole, (id_item, itemName, itemLabel))
            DejaLa = False
            for id_tableau in tableaux:
                commandLine_my2 = (
                    'SELECT * FROM tableau_item '
                    'WHERE id_tableau={0} AND id_item={1}').format(id_tableau, id_item)
                query_my2 = utils_db.queryExecute(commandLine_my2, query=query_my2)
                while query_my2.next():
                    DejaLa = True
            if DejaLa == False:
                self.selectionWidget.baseList.addItem(item)
        try:
            for i in range(self.main.model.columnCount()):
                titleColumn = self.main.model.columns[i]['value']
                if i > 2:
                    commandLine_my = utils_db.qs_prof_AllWhereName.format('items')
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (titleColumn, )}, query=query_my)
                    while query_my.next():
                        id_item = int(query_my.value(0))
                        itemName = query_my.value(1)
                        itemLabel = query_my.value(2)
                        itemText = utils_functions.u(
                            '{0} \t[{1}]').format(itemName, itemLabel)
                        item = QtWidgets.QListWidgetItem(itemText)
                        item.setToolTip(itemLabel)
                        item.setData(QtCore.Qt.UserRole, (id_item, itemName, itemLabel))
                        self.selectionWidget.selectionList.addItem(item)
        except:
            pass

def exportItemsList2ods(main, fileName):
    """
    crée un fichier ODS avec une liste d'items et les niveaux d'évaluation.
    Les items peuvent être sélectionnés à la main ou d'après le tableau actuel.
    2 versions sont proposées dans le fichier final (avec ou sans titres).
    Un copier-coller permet de placer ensuite le tableau dans l'énoncé de l'évaluation.
    """
    utils_functions.afficheMessage(main, 'exportItemsList2ods')
    # faire sélectionner les items ou le tableau actuel
    items = []
    dialog = ItemsListDlg(parent=main)
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        return False
    for i in range(dialog.selectionWidget.selectionList.count()):
        itemWidget = dialog.selectionWidget.selectionList.item(i)
        (id_item, itemName, itemLabel) = itemWidget.data(QtCore.Qt.UserRole)
        items.append((itemName, itemLabel))
    main.enableInterface(dialog, lastState)

    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
    try:
        # pour avoir l'ordre 'D', 'C', 'B', 'A', 'X'
        # et non 'A', 'B', 'C', 'D', 'X' :
        order0ItemsValues = list(utils.itemsValues[:-1][::-1])
        order0ItemsValues.append(utils.itemsValues[-1])

        # les lettres à utiliser, les noms des couleurs et les légendes :
        order0 = {'letters': [], 'colorsNames': [], 'legends': []}
        order1 = {'letters': [], 'colorsNames': [], 'legends': []}
        for value in order0ItemsValues:
            order0['letters'].append(utils_functions.u(utils.affichages[value][0]))
            order0['colorsNames'].append(utils_functions.u(utils.affichages[value][1]))
            order0['legends'].append(utils_functions.u(utils.affichages[value][6]))
        for value in utils.itemsValues:
            order1['letters'].append(utils_functions.u(utils.affichages[value][0]))
            order1['colorsNames'].append(utils_functions.u(utils.affichages[value][1]))
            order1['legends'].append(utils_functions.u(utils.affichages[value][6]))
        # ligne vide :
        emptyLine = [{'value': '', 'border': False}, {'value': '', 'border': False}]
        for letter in order0['letters']:
            emptyLine.append({'value': '', 'border': False})
        # les titres :
        titles = [
            {
                'value': QtWidgets.QApplication.translate('main', 'Item'), 
                'columnWidth': '2.0cm', 'alignHorizontal': 'center', 'bold': True}, 
            {
                'value': QtWidgets.QApplication.translate('main', 'Label'), 
                'columnWidth': '7.0cm', 'alignHorizontal': 'center', 'bold': True}, 
            ]

        # onglet couleurs :
        theList = []
        for order in (order0, order1):
            line = []
            for title in titles:
                line.append(title)
            for letter in order['letters']:
                line.append({
                    'value': letter, 
                    'alignHorizontal': 'center', 
                    'columnWidth': '0.7cm', 
                    'bold': True})
            theList.append(line)
            for item in items:
                line = [
                    {'value': item[0], 'borderColor': '#000000'}, 
                    {'value': item[1], 'borderColor': '#000000'}, 
                    ]
                for letter in order['letters']:
                    line.append({
                        'value': '', 
                        'alignHorizontal': 'center', 
                        'borderColor': '#000000'})
                theList.append(line)
            theList.append(emptyLine)
            theList.append(emptyLine)
            theList.append(emptyLine)
            for item in items:
                line = [
                    {'value': item[0], 'borderColor': '#000000'}, 
                    {'value': item[1], 'borderColor': '#000000'}, 
                    ]
                for letter in order['letters']:
                    line.append({
                        'value': letter, 
                        'alignHorizontal': 'center', 
                        'bold': True, 
                        'borderColor': '#000000'})
                theList.append(line)
            theList.append(emptyLine)
            theList.append(emptyLine)
            theList.append(emptyLine)
        exportDatas['COLORS'] = theList
        exportDatas['TABLES_LIST'].append('COLORS')
        exportDatas['TABLES_PARAMS']['COLORS'] = {
            'title': QtWidgets.QApplication.translate('main', 'COLORS')}

        # onglet légendes :
        theList = []
        for order in (order0, order1):
            line = []
            for title in titles:
                line.append(title)
            for colorName in order['colorsNames']:
                line.append({
                    'value': colorName, 
                    'alignHorizontal': 'center', 
                    'columnWidth': '3.0cm', 
                    'bold': True})
            theList.append(line)
            for item in items:
                line = [
                    {'value': item[0], 'borderColor': '#000000'}, 
                    {'value': item[1], 'borderColor': '#000000'}, 
                    ]
                for letter in order['letters']:
                    line.append({
                        'value': '', 
                        'alignHorizontal': 'center', 
                        'borderColor': '#000000'})
                theList.append(line)
            theList.append(emptyLine)
            theList.append(emptyLine)
            theList.append(emptyLine)
            line = []
            for title in titles:
                line.append(title)
            for legend in order['legends']:
                line.append({
                    'value': legend, 
                    'alignHorizontal': 'center', 
                    'columnWidth': '3.0cm', 
                    'wrap': 'wrap', 
                    'rowHeight': '1.0cm', 
                    'bold': True})
            theList.append(line)
            for item in items:
                line = [
                    {'value': item[0], 'borderColor': '#000000'}, 
                    {'value': item[1], 'borderColor': '#000000'}, 
                    ]
                for letter in order['letters']:
                    line.append({
                        'value': '', 
                        'alignHorizontal': 'center', 
                        'borderColor': '#000000'})
                theList.append(line)
            theList.append(emptyLine)
            theList.append(emptyLine)
            theList.append(emptyLine)
        exportDatas['LEGENDS'] = theList
        exportDatas['TABLES_LIST'].append('LEGENDS')
        exportDatas['TABLES_PARAMS']['LEGENDS'] = {
            'title': QtWidgets.QApplication.translate('main', 'LEGENDS')}

        # export final :
        exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


def exportDic2ods(main, exportDatas, fileName, msgFin=True):
    """
    Procédure générale d'export d'un dictionnaire en fichier ods.
    Le dictionnaire exportDatas doit contenir 
    la liste des onglets et une liste par onglet.
         {
            'TABLES_LIST': [...], 
            'TABLES_PARAMS': {...}, 
            'nomOnglet1': liste1, 'nomOnglet2': liste2, etc
        }
    La liste des onglets (exportDatas['TABLES_LIST']) permet de les ordonner.
    Le dictionnaire exportDatas['TABLES_PARAMS'] permet de 
    leur donner des titres non-ascii :
        exportDatas['TABLES_PARAMS']['subjects'] = {'title': subjectsText}
    Pour chaque onglet, la liste contient elle-même une liste par ligne :
        liste1 = [[title1, title2], [cell1.1, cell1.2], [cell2.1, cell2.2], etc]
        La première ligne contient les titres et les autres contiennent les cellules.
        Ce sont des dictionnaires.
            title = {'value': 'aaa' , etc} OU cell = {'value': 'aaa' , 'type': TEXT , etc}
            'value' est obligatoire
            'type' peut être INTEGER, FLOAT, TEXT (par défaut)
    Enfin, le dictionnaire contient aussi la liste des onglets (exportDatas['TABLES_LIST'])
        afin de les ordonner.
    Utilisation : utils_export.exportDic2ods(main, exportDatas, fileName)
    """
    if fileName == '':
        return False
    utils_functions.doWaitCursor()
    try:
        import odslib

        # Create the document
        doc = odslib.ODS()
        first = True
        for tableName in exportDatas['TABLES_LIST']:
            #print('tableName', tableName)
            tableTitle = exportDatas['TABLES_PARAMS'][tableName].get('title', tableName)
            if first:
                first = False
                doc.content.getSheet(0).setSheetName(tableTitle)
            else:
                doc.content.makeSheet(tableTitle)
            theList = exportDatas[tableName]
            column, row  = 0, 0
            titles = theList[0]
            for title in titles:
                actualCell = doc.content.getCell(column, row)
                if 'stringFormula' in title:
                    actualCell.stringFormula(
                        utils_functions.u('{0}').format(title['value']), 
                        utils_functions.u('{0}').format(title['stringFormula']))
                else:
                    actualCell.stringValue(utils_functions.u('{0}').format(title['value']))
                if 'rowHeight' in title:
                    doc.content.getRow(row).setHeight(title['rowHeight'])
                if 'columnWidth' in title:
                    doc.content.getColumn(column).setWidth(title['columnWidth'])
                if 'alignHorizontal' in title:
                    actualCell.setAlignHorizontal(title['alignHorizontal'])
                else:
                    actualCell.setAlignHorizontal('center')
                actualCell.setAlignVertical('middle')
                if 'rotation' in title:
                    actualCell.setRotation(title['rotation'])
                if 'cellColor' in title:
                    actualCell.setCellColor(title['cellColor'])
                if 'fontSize' in title:
                    actualCell.setFontSize(title['fontSize'])
                if 'bold' in title:
                    actualCell.setBold(title['bold'])
                column += 1
            for aRow in theList[1:]:
                row += 1
                column = 0
                for cell in aRow:
                    actualCell = doc.content.getCell(column, row)
                    if 'borderColor' in cell:
                        actualCell.setBorderColor(cell['borderColor'])
                    else:
                        actualCell.setBorderColor('#999999')
                    if 'type' in cell:
                        if cell['type'] in utils.TYPES_NUMBER:
                            #actualCell.floatValue(cell['value']).setBorder()
                            value = cell['value']
                            if value in ('', 'X'):
                                actualCell.stringValue(value).setBorder()
                            else:
                                actualCell.floatValue(value).setBorder()
                        elif cell['type'] in utils.TYPES_NOTE:
                            value = cell['value']
                            if value in ('', 'X'):
                                actualCell.stringValue(value).setBorder()
                            else:
                                actualCell.floatValue(value).setBorder()
                        else:
                            actualCell.stringValue(cell['value']).setBorder()
                    elif 'border' in cell:
                        actualCell.stringValue(cell['value']).setBorder(cell['border'])
                    else:
                        actualCell.stringValue(cell['value']).setBorder()
                    if 'rowHeight' in cell:
                        doc.content.getRow(row).setHeight(cell['rowHeight'])
                    if 'alignHorizontal' in cell:
                        actualCell.setAlignHorizontal(cell['alignHorizontal'])
                    actualCell.setAlignVertical('middle')
                    if 'rotation' in cell:
                        actualCell.setRotation(cell['rotation'])
                    if 'cellColor' in cell:
                        actualCell.setCellColor(cell['cellColor'])
                    if 'fontSize' in cell:
                        actualCell.setFontSize(cell['fontSize'])
                    if 'bold' in cell:
                        actualCell.setBold(cell['bold'])
                    if 'wrap' in cell:
                        actualCell.setWrap(cell['wrap'])
                    if 'textColor' in cell:
                        actualCell.setFontColor(cell['textColor'])
                    column += 1
            # on ajoute le tableau des traductions des titres
            # des colonnes (pour items, bilans, etc :
            viewType = exportDatas['TABLES_PARAMS'][tableName].get('viewType', -1)
            if viewType > -1:
                hasTitles = False
                titleRow = row + 2
                row += 3
                column = 0
                for title in titles:
                    if 'toolTip' in title:
                        hasTitles = True
                        actualCell = doc.content.getCell(column, row)
                        actualCell.stringValue(utils_functions.u('{0}').format(title['value']))
                        actualCell.setBold(title['bold'])
                        actualCell.setAlignHorizontal('center')
                        actualCell.setAlignVertical('top')
                        actualCell = doc.content.getCell(column + 1, row)
                        actualCell.stringValue(title['toolTip'])
                        actualCell.setAlignHorizontal('left')
                        actualCell.setAlignVertical('top')
                        if '\n' in title['toolTip']:
                            doc.content.getRow(row).setHeight('0.9cm')
                        row += 1
                if hasTitles:
                    row = titleRow
                    actualCell = doc.content.getCell(column, row)
                    actualCell.stringValue(QtWidgets.QApplication.translate(
                        'main', 'Columns titles:'))
                    actualCell.setBold(title['bold'])
                    actualCell.setAlignHorizontal('left')
                    actualCell.setFontSize(12)

        # Move back to the first sheet
        doc.content.getSheet(0)
        # Write out the document
        doc.save(fileName)

        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()



###########################################################"
#   EXPORT PRINTER, ODT ET PDF
###########################################################"

def createPrinter(fileName=''):
    """
    Initialisation d'un printer.
    Si fileName != '', on fait un PDF
    """
    result = False
    utils_functions.doWaitCursor()
    try:
        printer = QtPrintSupport.QPrinter()
        printer.setPaperSize(QtPrintSupport.QPrinter.A4)
        printer.setOrientation(QtPrintSupport.QPrinter.Landscape)
        #printer.setPageMargins(10.0, 10.0, 10.0, 10.0, QtPrintSupport.QPrinter.Millimeter)
        printer.setColorMode(QtPrintSupport.QPrinter.Color)
        result = printer
        if fileName == '':
            # on lance le dialog d'impression :
            printer.setOutputFormat(QtPrintSupport.QPrinter.NativeFormat)
            utils_functions.restoreCursor()
            printDialog = QtPrintSupport.QPrintDialog(printer)
            if printDialog.exec_() != QtWidgets.QDialog.Accepted:
                result = False
            utils_functions.doWaitCursor()
        else:
            # on veut un PDF :
            printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
            printer.setOutputFileName(fileName)
    finally:
        utils_functions.restoreCursor()
        return result


def exportDic2html(main, exportDatas):
    """
    Procédure générale d'export d'un dictionnaire vers un fichier html.
    Utilisation : htmlFileName = utils_export.exportDic2html(main, exportDatas)
        (htmlFileName = main.tempPath + '/temp.html')
    Le dictionnaire exportDatas est détaillé plus haut 
    dans la procédure exportDic2ods.
    """

    def nl2br(text):
        """
        pour insérer des sauts de lignes (<br/>) dans les textes html
        """
        try:
            newText = text.replace('\n', '<br/>')
        except:
            newText = text
            pass
        return newText

    htmlFileName = ''
    utils_functions.doWaitCursor()
    try:
        htmlResult = ''
        htmlBegin = ('<!DOCTYPE html>'
            '\n<html>'
            '\n<head>'
            '\n    <meta content="text/html; charset=UTF-8" http-equiv="content-type">'
            '\n   <style type="text/css">'
            '\n    <link rel="stylesheet" type="text/css" href="style_export.css">'
            '\n   </style>'
            '\n</head>'
            '\n<body lang="fr-FR" text="#000000" dir="ltr">')
        htmlEnd = ('</body></html>')
        htmlTableBegin = '<table width=1000 cellpadding=2 cellspacing=0 border="2">'

        first = True
        for tableName in exportDatas['TABLES_LIST']:
            if first:
                first = False
                htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, htmlBegin)
                htmlResult = utils_functions.u('{0}\n<div>').format(htmlResult)
                # On insère la date et l'heure actuelle au début de la première page
                date = QtCore.QDate().currentDate().toString('dddd dd MMMM yyyy')
                time = QtCore.QTime().currentTime().toString('hh:mm')
                dateModif = QtWidgets.QApplication.translate('main', '{0} at {1}')
                dateModif = dateModif.format(date, time)
                htmlResult = utils_functions.u('{0}\n<p align="center">{1}</p>').format(
                    htmlResult, dateModif)
            else:
                htmlResult = utils_functions.u('{0}\n<hr>').format(htmlResult)
                htmlResult = utils_functions.u('{0}\n<div>').format(htmlResult)

            # on insère le titre de la vue
            if 'title' in exportDatas['TABLES_PARAMS'][tableName]:
                theTitle = exportDatas['TABLES_PARAMS'][tableName]['title']
                htmlResult = utils_functions.u('{0}\n<h1 align="center">{1}</h1>').format(
                    htmlResult, theTitle)
            if 'subTitle' in exportDatas['TABLES_PARAMS'][tableName]:
                subTitle = exportDatas['TABLES_PARAMS'][tableName]['subTitle']
                htmlResult = utils_functions.u('{0}\n<p>{1}</p>').format(htmlResult, subTitle)

            theList = exportDatas[tableName]
            column, row  = 0, 0
            titles = theList[0]

            htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, htmlTableBegin)

            htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, '<tr>')
            for title in titles:
                htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, '<th>')
                htmlResult = utils_functions.u('{0}\n<b>{1}</b>').format(htmlResult, title['value'])
                htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, '</th>')
                column += 1
            htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, '</tr>')

            for aRow in theList[1:]:
                htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, '<tr>')
                row += 1
                column = 0
                for data in aRow:
                    tdArgs = ''
                    value = data['value']
                    if isinstance(value, float):
                        value = '{0}'.format(value)
                        value = value.replace('.', utils.DECIMAL_SEPARATOR)
                    value = nl2br(value)
                    text = utils_functions.u('{0}').format(value)
                    if text in ('HSEPARATOR', 'VSEPARATOR'):
                        text = ''
                    if 'bold' in data:
                        if data['bold']:
                            text = utils_functions.u('<b>{0}</b>').format(text)
                    if 'cellColor' in data:
                        tdArgs = utils_functions.u('{0} bgcolor="{1}"').format(
                            tdArgs, data['cellColor'])
                    if 'textColor' in data:
                        tdArgs = utils_functions.u('{0} style="color:{1};"').format(
                            tdArgs, data['textColor'])
                    if 'alignHorizontal' in data:
                        tdArgs = utils_functions.u('{0} align="{1}"').format(
                            tdArgs, data['alignHorizontal'])
                    else:
                        tdArgs = utils_functions.u('{0} align="center"').format(tdArgs)
                    htmlResult = utils_functions.u('{0}\n<td{1}>').format(htmlResult, tdArgs)
                    htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, text)
                    htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, '</td>')
                    column += 1
                htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, '</tr>')

            htmlResult = utils_functions.u('{0}\n</table>').format(htmlResult)
            htmlResult = utils_functions.u('{0}\n</div>').format(htmlResult)

            # on ajoute le tableau des traductions des titres
            # des colonnes (pour items, bilans, etc :
            htmlTitles = ''
            htmlTitlesRows = ''
            espaces = '&nbsp;' * 10
            viewType = exportDatas['TABLES_PARAMS'][tableName].get('viewType', -1)
            if viewType > -1:
                hasTitles = False
                titleRow = row + 2
                row += 3
                column = 0
                for title in titles:
                    if 'toolTip' in title:
                        hasTitles = True
                        htmlTitlesRows = utils_functions.u(
                            '{0}\n{1}').format(htmlTitlesRows, '<tr>')
                        htmlTitlesRows = utils_functions.u(
                            '{0}\n<td>{1}</td>').format(htmlTitlesRows, espaces)
                        htmlTitlesRows = utils_functions.u(
                            '{0}\n<td valign="top" align="center"><b>{1}</b></td>').format(
                                htmlTitlesRows, title['value'])
                        htmlTitlesRows = utils_functions.u(
                            '{0}\n<td valign="top">{1}</td>').format(
                                htmlTitlesRows, nl2br(title['toolTip']))
                        htmlTitlesRows = utils_functions.u(
                            '{0}\n{1}').format(htmlTitlesRows, '</tr>')
                        row += 1
                if hasTitles:
                    htmlTitles = utils_functions.u('{0}\n<div>').format(htmlTitles)
                    htmlTitles = utils_functions.u('{0}\n<h3>{1}</h3>').format(
                        htmlTitles, 
                        QtWidgets.QApplication.translate('main', 'Columns titles:'))
                    htmlTableBegin = (
                        '<table width=1000 cellpadding=2 cellspacing=0 '
                        'style="page-break-before: auto" border="0">')
                    htmlTitles = utils_functions.u('{0}\n{1}').format(htmlTitles, htmlTableBegin)
                    htmlTitles = utils_functions.u('{0}\n{1}').format(htmlTitles, htmlTitlesRows)
                    htmlTitles = utils_functions.u('{0}\n</table>').format(htmlTitles)
                    htmlTitles = utils_functions.u('{0}\n</div>').format(htmlTitles)
            htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, htmlTitles)

        htmlResult = utils_functions.u('{0}\n{1}').format(htmlResult, htmlEnd)
        if utils.OS_NAME[0] == 'win':
            import utils_html
            htmlResult = utils_html.encodeHtml(htmlResult)

        # on enregistre htmlResult dans un fichier html temporaire :
        htmlFileName = main.tempPath + '/temp.html'
        outFile = QtCore.QFile(htmlFileName)
        if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(outFile)
            stream.setCodec('UTF-8')
            stream << htmlResult
            outFile.close()
        htmlFileName = QtCore.QFileInfo(htmlFileName).absoluteFilePath()
        cssFile = utils_functions.u('{0}/files/{1}').format(main.beginDir, 'style_export.css')
        cssFileTemp = '{0}/{1}'.format(main.tempPath, 'style_export.css')
        QtCore.QFile(cssFile).copy(cssFileTemp)

        """except:
        utils_functions.afficheMsgPb(main, message)"""
    finally:
        utils_functions.restoreCursor()
        return htmlFileName


def htmlToPrinter(main, htmlFileName, printer, fileName=''):
    """
    si fileName != '', on fait un PDF
    """
    orientation = 'Portrait'

    def convertItWebKit():
        global PDF_GENERATED
        PRINT_WEBVIEW.print_(printer)
        PDF_GENERATED = True

    def convertItWebEngine():
        if fileName != '':
            pageLayout = QtGui.QPageLayout(
                QtGui.QPageSize(QtGui.QPageSize.A4), 
                QtGui.QPageLayout.Portrait, 
                QtCore.QMarginsF(10, 10, 10, 15), 
                QtGui.QPageLayout.Millimeter, 
                QtCore.QMarginsF(10, 10, 10, 15))
            if orientation == 'Landscape':
                pageLayout.setOrientation(QtGui.QPageLayout.Landscape)
            PRINT_WEBVIEW.page().printToPdf(fileName, pageLayout)
        else:
            PRINT_WEBVIEW.page().print(printer, printingFinished)

    def printingFinished(ok=None):
        global PDF_GENERATED
        PDF_GENERATED = True

    global PDF_GENERATED, PRINT_WEBVIEW
    PDF_GENERATED = False

    PRINT_WEBVIEW = utils_webengine.MyWebEngineView(
        main, linksInBrowser=True)

    if utils_webengine.WEB_ENGINE == 'WEBENGINE':
        PRINT_WEBVIEW.page().pdfPrintingFinished.connect(printingFinished)
        PRINT_WEBVIEW.loadFinished.connect(convertItWebEngine)
    else:
        PRINT_WEBVIEW.loadFinished.connect(convertItWebKit)

    url = QtCore.QUrl().fromLocalFile(htmlFileName)
    PRINT_WEBVIEW.load(url)

    while not(PDF_GENERATED):
        QtWidgets.QApplication.processEvents()
    del(PRINT_WEBVIEW)
    PRINT_WEBVIEW = None
    return True




###########################################################"
#   EXPORTS DIVERS
###########################################################"

def exportBilan2Dirs(main, directory, id_bilan, parameters=('', '')):
    """
    blablabla
    """
    import utils_filesdirs
    utils_functions.afficheMessage(main, 'exportBilan2Dirs')
    utils_functions.doWaitCursor()

    try:
        # on récupère le nom et le label du bilan :
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'bilans', 'id_bilan', id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            bilanName = query_my.value(1)
            bilanLabel = query_my.value(2)
        # initialisation du document :
        bilanDir = parameters[0].format(bilanName, bilanLabel)
        utils_filesdirs.createDirs(directory, bilanDir)
        bilanDir = utils_functions.u('{0}/{1}').format(directory, bilanDir)
        # on récupère les items liés au bilan :
        commandLine_my = (
            'SELECT items.* '
            'FROM item_bilan '
            'JOIN items ON items.id_item=item_bilan.id_item '
            'WHERE item_bilan.id_bilan={0} '
            'ORDER BY UPPER(items.Name)').format(id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            itemDir = parameters[1].format(
                bilanName, bilanLabel, itemName, itemLabel)
            utils_filesdirs.createDirs(bilanDir, itemDir)
        utils_functions.afficheMsgFinOpenDir(main, directory, message=directory)
    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()

def exportEleves2Dirs(main, directory, eleves={'ORDER' : []}):
    """
    blablabla
    """
    import utils_filesdirs
    utils_functions.afficheMessage(main, 'exportEleves2Dirs')
    utils_functions.doWaitCursor()

    try:
        for id_eleve in eleves['ORDER']:
            eleveName = eleves[id_eleve]
            utils_filesdirs.createDirs(directory, eleveName)
        utils_functions.afficheMsgFinOpenDir(main, directory, message=directory)
    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()




###########################################################"
#   IMPORTS ET EXPORTS CSV
###########################################################"

CSV_FILES_TITLES = {
    'bilansItems': ['Nom bilan', 'Label bilan', 'Nom item', 'Label item', 'coeff', 'comment'], 
    'templates': ['TemplateName', 'TemplateLabel', 'ItemName', 'ItemLabel', 'comment', 'ordre'], 
    'templatesOLD': ['TemplateName', 'ItemName', 'ItemLabel', 'comment', 'ordre'], 
    'counts': ['CountName', 'sens', 'calcul', 'label', 'id_item'], 
    'countsOLD': ['CountName', 'sens', 'calcul', 'label'], 
    'countsOLDOLD': ['CountName', 'sens', 'calcul'], 
    'eleves': ['id', 'NOM', 'Prenom', 'Classe'], 
    }


def exportTable2Csv(main, db, table, fileName, msgFin=True):
    """
    Procédure générale d'export d'une table en fichier csv.
    utils_export.exportTable2Csv(main, db, table, fileName)
    """
    if fileName == '':
        return
    message = utils_functions.u('exportTable2Csv : {0}.{1}').format(db.connectionName(), table)
    utils_functions.afficheMessage(main, message)
    utils_functions.doWaitCursor()
    try:
        # on efface l'ancienne version du fichier :
        if QtCore.QFileInfo(fileName).exists():
            QtCore.QFile(fileName).remove()
        # on récupère la structure de la table (noms des champs et type) :
        champsTitres = []
        champsTypes = []
        query = utils_db.queryExecute(utils_db.q_pragmaTable.format(table), db=db)
        while query.next():
            champsTitres.append(utils_functions.s(query.value(1)))
            champsTypes.append(query.value(2))
        # on crée le fichier csv :
        import csv
        if utils.PYTHONVERSION >= 30:
            theFile = open(fileName, 'w', newline='', encoding='utf-8')
        else:
            theFile = open(fileName, 'wb')
        writer = csv.writer(theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        # on inscrit la ligne de titres :
        writer.writerow(champsTitres)
        # on inscrit les lignes :
        commandLine = utils_db.q_selectAllFrom.format(table)
        query = utils_db.queryExecute(commandLine, query=query)
        while query.next():
            ligne = []
            i = 0
            for champType in champsTypes:
                if champType == 'TEXT':
                    value = utils_functions.s(query.value(i))
                else:
                    try:
                        value = int(query.value(i))
                    except:
                        value = ''
                i += 1
                ligne.append(value)
            writer.writerow(ligne)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        theFile.close()
        utils_functions.restoreCursor()


def exportList2csv(main, theList, fileName, msgFin=True):
    """
    Procédure générale d'export d'une liste en fichier csv.
    La liste doit contenir une liste par ligne.
    [['TEXT', 'NUMBER'], ['Titre1', 'Titre2'], [valeur1.1, valeur1.2], [valeur2.1, valeur2.2], etc]
    La première ligne contient les types des colonnes (TEXT ou NUMBER).
    La deuxième ligne contient les titres.
    utils_export.exportList2csv(main, theList, fileName)
    """
    if fileName == '':
        return False
    ok = False
    utils_functions.afficheMessage(main, 'exportList2csv')
    utils_functions.doWaitCursor()
    try:
        # on efface l'ancienne version du fichier :
        if QtCore.QFileInfo(fileName).exists():
            QtCore.QFile(fileName).remove()
        # on crée le fichier csv :
        import csv
        if utils.PYTHONVERSION >= 30:
            theFile = open(fileName, 'w', newline='', encoding='utf-8')
        else:
            theFile = open(fileName, 'wb')
        writer = csv.writer(theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)

        champsTypes = theList[0]
        titles = theList[1]
        lineForCsv = []
        for value in titles:
            lineForCsv.append(utils_functions.s(value))
        writer.writerow(lineForCsv)
        for line in theList[2:]:
            lineForCsv = []
            i = 0
            for value in line:
                if champsTypes[i] == 'TEXT':
                    value = utils_functions.s(value)
                lineForCsv.append(value)
                i += 1
            writer.writerow(lineForCsv)

        ok = True
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        try:
            theFile.close()
        except:
            pass
        utils_functions.restoreCursor()
        return ok


def csv2List(fileName):
    """
    lit un fichier csv et retourne son contenu sous forme de 2 listes :
        headers contient les titres (première ligne du fichier)
        data contient les autres lignes.
    """
    import csv
    headers, data = [], []
    # pour gérer l'encodage du fichier :
    encodings = ('utf-8', 'iso-8859-15', 'iso-8859-1')
    for enc in encodings:
        headers, data = [], []
        firstRow = True
        if utils.PYTHONVERSION >= 30:
            theFile = open(fileName, newline='', encoding=enc)
        else:
            theFile = open(fileName, 'rb')
        try:
            reader = csv.reader(theFile, 
                delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
            for row in reader:
                newRow = []
                for item in row:
                    if item in ('', None):
                        newRow.append('')
                    elif isinstance(item, (int, float)):
                        newRow.append(item)
                    else:
                        if utils.PYTHONVERSION < 30:
                            item = item.decode(enc).encode('utf-8')
                        newRow.append(utils_functions.u(item))
                if firstRow:
                    headers = newRow
                    firstRow = False
                else:
                    data.append(newRow)
        except:
            continue
        finally:
            theFile.close()
        break
    return headers, data


def exportBilansItems2Csv(main, fileName, selectedItems=['ALL'], selectedBilans=['ALL']):
    """
    procédure d'export de la structure bilans -> items au format csv.
    on fabrique un tableau [Nom bilan, Label bilan, Nom item, Label item, coeff, comment]
    pour chaque bilan :
        on ajoute une ligne décrivant le bilan (Nom bilan, Label bilan)
            et l'éventuel commentaire(comment)
        on recherche ensuite les items liés à ce bilan, et pour chacun
        on ajoute une ligne pour l'item (Nom item, Label item, coeff, comment)
    un item lié à plusieurs bilans sera donc inséré autant de fois
        (avec éventuellement des coefficients différents)
    on exporte aussi les liens bilan -> bilan
        en mettant "BILAN" dans la colonne "comment"
    """
    utils_functions.afficheMessage(main, 'exportBilansItems2Csv')
    utils_functions.doWaitCursor()
    try:
        import csv
        if utils.PYTHONVERSION >= 30:
            theFile = open(fileName, 'w', newline='', encoding='utf-8')
        else:
            theFile = open(fileName, 'wb')
        writer = csv.writer(
            theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(CSV_FILES_TITLES['bilansItems'])
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        # on récupère les commentaires :
        comments = {}
        commandLine_my = utils_db.q_selectAllFrom.format('comments')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            isItem = int(query_my.value(0))
            id_truc = int(query_my.value(1))
            comment = query_my.value(2)
            comments[(isItem, id_truc)] = comment
        # on récupère la liste des bilans à exporter :
        bilansList, bilans = [], {}
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            if ((selectedBilans == ['ALL']) or (id_bilan in selectedBilans)):
                bilanName = query_my.value(1)
                bilanLabel = query_my.value(2)
                id_competence = int(query_my.value(3))
                comment = ''
                if (0, id_bilan) in comments:
                    comment = comments[(0, id_bilan)]
                bilansList.append(id_bilan)
                bilans[id_bilan] = (bilanName, bilanLabel, id_competence, comment)
        # on récupère les liens bilan -> bilan à exporter :
        bilan_bilan = {}
        commandLine_my = utils_db.q_selectAllFrom.format('bilan_bilan')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_bilan1 = int(query_my.value(0))
            id_bilan2 = int(query_my.value(1))
            if (id_bilan1 in bilansList) and (id_bilan2 in bilansList):
                if id_bilan1 in bilan_bilan:
                    bilan_bilan[id_bilan1].append(id_bilan2)
                else:
                    bilan_bilan[id_bilan1] = [id_bilan2]
        # on récupère la liste des items à exporter :
        itemsList, items = [], {}
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            if ((selectedItems == ['ALL']) or (id_item in selectedItems)):
                itemName = query_my.value(1)
                itemLabel = query_my.value(2)
                comment = ''
                if (1, id_item) in comments:
                    comment = comments[(1, id_item)]
                itemsList.append(id_item)
                items[id_item] = (itemName, itemLabel, comment)
        # on inscrit chaque bilan, la liste des items liés et celle des bilans liés :
        for id_bilan in bilansList:
            (bilanName, bilanLabel, id_competence, comment) = bilans[id_bilan]
            # inscription du bilan :
            writer.writerow([
                utils_functions.s(bilanName), 
                utils_functions.s(bilanLabel), 
                None, None, 
                None, 
                utils_functions.s(comment)])
            # on cherche les items liés :
            commandLine_my = (
                'SELECT items.id_item, item_bilan.coeff '
                'FROM items '
                'JOIN item_bilan ON item_bilan.id_item=items.id_item '
                'WHERE item_bilan.id_bilan={0} '
                'ORDER BY UPPER(items.Name)').format(id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                if ((selectedItems == ['ALL']) or (id_item in selectedItems)):
                    (itemName, itemLabel, comment) = items[id_item]
                    coeff = int(query_my.value(1))
                    writer.writerow([
                        None, None, 
                        utils_functions.s(itemName), 
                        utils_functions.s(itemLabel), 
                        coeff, 
                        utils_functions.s(comment)])
                if id_item in itemsList:
                    itemsList.remove(id_item)
            # on inscrit les bilans liés :
            if id_bilan in bilan_bilan:
                for id_bilan2 in bilan_bilan[id_bilan]:
                    (bilanName2, bilanLabel2, id_competence2, comment2) = bilans[id_bilan2]
                    writer.writerow([
                        None, None, 
                        utils_functions.s(bilanName2), 
                        utils_functions.s(bilanLabel2), 
                        None, 
                        utils_functions.s('BILAN')])
        # on ajoute les items non liés à un bilan (ceux qui restent dans itemsList) :
        if len(itemsList) > 0:
            writer.writerow([
                utils_functions.s('NOBILAN'), 
                utils_functions.s('NOBILAN'), 
                None, None, 
                None, 
                utils_functions.s('')])
            for id_item in itemsList:
                (itemName, itemLabel, comment) = items[id_item]
                writer.writerow([
                    None, None, 
                    utils_functions.s(itemName), 
                    utils_functions.s(itemLabel), 
                    None, 
                    utils_functions.s(comment)])
        utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        theFile.close()
        utils_functions.restoreCursor()


def importBilansItemsFromCsv(main, fileName, msgFin=True):
    """
    on récupère les bilans et items présents dans un fichier csv 
    créé par la procédure exportBilansItems2Csv
    Pour chaque bilan ou item, on vérifie qu'il n'est pas déjà là
    """
    result = False
    utils_functions.doWaitCursor()
    try:
        import prof, prof_itemsbilans, prof_db
        headers, data = csv2List(fileName)
        # on vérifie la ligne de titres :
        if headers != CSV_FILES_TITLES['bilansItems']:
            utils_functions.myPrint('BAD CSV FILE !!!!!!!!!!!!!!!!!!!!')
            message = QtWidgets.QApplication.translate(
                'main', 'The structure of the file that you selected is not correct.')
            utils_functions.restoreCursor()
            messageBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, 
                utils.PROGNAME, message, 
                QtWidgets.QMessageBox.NoButton, main)
            messageBox.exec_()
            return
        id_bilan = -1
        coeff = 1
        # pour récupérer les liens bilan -> bilan :
        bilansDic, bilan_bilan = {}, {}
        query_my = utils_db.query(main.db_my)
        query_commun = utils_db.query(main.db_commun)
        modifBilanNameNoToAll = False
        modifItemNameNoToAll = False
        # pour les messages en cas de doublette :
        qName = (
            '<p><b>{0}</b></p>'
            '<p><b>{1}</b> : {2}'
            '<br/>{3}'
            '<br/><b>{4}</b> : {5}</p>')
        qLabel = (
            '<p><b>{0}</b></p>'
            '<p>{1} : <b>{2}</b>'
            '<br/>{3}'
            '<br/>{4} : <b>{5}</b></p>')
        q2 = QtWidgets.QApplication.translate('main', 'in:')
        for row in data:
            if (row[0] == 'NOBILAN') and (row[1] == 'NOBILAN'):
                # on ne récupère pas la ligne de séparation des items orphelins :
                id_bilan = -1
                pass
            elif row[0] != '':
                # C'est donc un bilan, puisque la première colonne n'est pas vide (Nom bilan)
                bilanName = utils_functions.u(row[0])
                bilanLabel = utils_functions.u(row[1])
                comment = utils_functions.u(row[5])
                id_competence = -1
                # suite de tests pour trouver l'état du bilan :
                etat = 'NEW'
                # est-il déjà là ?
                commandLine_test = utils_db.qs_prof_AllWhereNameLabel.format('bilans')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (bilanName, bilanLabel)}, query=query_my)
                while query_my.next():
                    etat = 'DEJA_LA'
                    id_bilan = int(query_my.value(0))
                commandLine_test = utils_functions.u(
                    'SELECT * FROM {0} '
                    'WHERE code=? AND Competence=?')
                # est-ce un bilan du bulletin ?
                query_commun = utils_db.queryExecute(
                    {commandLine_test.format('bulletin'): (bilanName, bilanLabel)}, 
                    query=query_commun)
                while query_commun.next():
                    if etat == 'NEW':
                        etat = 'NEW-COMMON_CPT'
                    id_competence = utils.decalageBLT + int(query_commun.value(0))
                # est-ce un bilan du référentiel ?
                query_commun = utils_db.queryExecute(
                    {commandLine_test.format('referentiel'): (bilanName, bilanLabel)}, 
                    query=query_commun)
                while query_commun.next():
                    if etat == 'NEW':
                        etat = 'NEW-COMMON_CPT'
                    id_competence = int(query_commun.value(0))
                # est-ce un bilan confidentiel ?
                query_commun = utils_db.queryExecute(
                    {commandLine_test.format('confidentiel'): (bilanName, bilanLabel)}, 
                    query=query_commun)
                while query_commun.next():
                    if etat == 'NEW':
                        etat = 'NEW-COMMON_CPT'
                    id_competence = utils.decalageCFD + int(query_commun.value(0))
                # le label est modifié ?
                if etat == 'NEW':
                    commandLine_test = utils_db.qs_prof_AllWhereName.format('bilans')
                    query_my = utils_db.queryExecute(
                        {commandLine_test: (bilanName, )}, query=query_my)
                    bilanLabelEX = ''
                    while query_my.next():
                        etat = 'LABEL_MODIF'
                        id_bilan = int(query_my.value(0))
                        bilanLabelEX = query_my.value(2)
                    if bilanLabelEX == bilanLabel:
                        etat = 'DEJA_LA'
                # le nom est modifié ?
                if etat == 'NEW':
                    commandLine_test = utils_db.qs_prof_AllWhereLabel.format('bilans')
                    query_my = utils_db.queryExecute(
                        {commandLine_test: (bilanLabel, )}, query=query_my)
                    bilanNameEX = ''
                    while query_my.next():
                        etat = 'NAME_MODIF'
                        id_bilan = int(query_my.value(0))
                        bilanNameEX = query_my.value(1)
                    if bilanNameEX == bilanName:
                        etat = 'DEJA_LA'
                # on passe à l'action :
                if etat == 'NAME_MODIF':
                    if not(modifBilanNameNoToAll):
                        # on demande confirmation avant de modifier le bilan :
                        q1 = QtWidgets.QApplication.translate('main', 'Modify this bilan Name ?')
                        question = utils_functions.u(qName).format(
                            q1, bilanNameEX, bilanLabel, q2, bilanName, bilanLabel)
                        reply = utils_functions.messageBox(
                            main, level='question', message=question,
                            buttons=['Yes', 'NoToAll', 'No', 'Cancel'])
                        if reply == QtWidgets.QMessageBox.NoToAll:
                            modifBilanNameNoToAll = True
                            etat = 'NEW'
                        elif reply == QtWidgets.QMessageBox.Yes:
                            message = utils_functions.u('NAME_MODIF: {0} > {1} | {2}').format(bilanNameEX, 
                                bilanName, bilanLabel)
                            utils_functions.afficheMessage(main, message)
                            prof_itemsbilans.editBilan(main, id_bilan, bilanName, bilanLabel)
                        elif reply == QtWidgets.QMessageBox.No:
                            # on aura donc 2 bilans de même label :
                            etat = 'NEW'
                        else:
                            utils_functions.restoreCursor()
                            message = QtWidgets.QApplication.translate('main', 'Import Canceled')
                            utils_functions.messageBox(main, level='warning', message=message)
                            return
                    else:
                        etat = 'NEW'
                elif etat == 'LABEL_MODIF':
                    # on demande confirmation avant de modifier le bilan :
                    q1 = QtWidgets.QApplication.translate('main', 'Modify this bilan Label ?')
                    question = utils_functions.u(qLabel).format(
                        q1, bilanName, bilanLabelEX, q2, bilanName, bilanLabel)
                    reply = utils_functions.messageBox(
                        main, level='question', message=question,
                        buttons=['Yes', 'No', 'Cancel'])
                    if reply == QtWidgets.QMessageBox.Yes:
                        message = utils_functions.u('LABEL_MODIF: {0} | {1} > {2}').format(bilanName, 
                            bilanLabelEX, bilanLabel)
                        utils_functions.afficheMessage(main, message)
                        prof_itemsbilans.editBilan(main, id_bilan, bilanName, bilanLabel)
                    elif reply == QtWidgets.QMessageBox.No:
                        # pas 2 bilans du même nom, donc on passe :
                        pass
                    else:
                        utils_functions.restoreCursor()
                        message = QtWidgets.QApplication.translate('main', 'Import Canceled')
                        utils_functions.messageBox(main, level='warning', message=message)
                        return
                if etat == 'NEW-COMMON_CPT':
                    etat = 'NEW'
                if etat == 'NEW':
                    # on crée le nouveau bilan :
                    result_bilan, id_bilan = prof_itemsbilans.createBilan(main, 
                        bilanName, bilanLabel, id_competence)
                    message = utils_functions.u('createBilan: {0}, {1}').format(
                        bilanName, bilanLabel)
                    utils_functions.afficheMessage(main, message)
                # on modifie le commentaire si besoin :
                commentEX = ''
                commandLine = ('SELECT comment FROM comments '
                    'WHERE isItem=0 AND id={0}').format(id_bilan)
                query_my = utils_db.queryExecute(commandLine, query=query_my)
                while query_my.next():
                    commentEX = query_my.value(0)
                if comment != commentEX:
                    # on met à jour la base :
                    commandLine = (
                        'DELETE FROM comments '
                        'WHERE isItem=0 AND id={0}').format(id_bilan)
                    query_my = utils_db.queryExecute(commandLine, query=query_my)
                    commandLine = utils_db.insertInto('comments')
                    if comment != '':
                        query_my = utils_db.queryExecute(
                            {commandLine: (0, id_bilan, comment)}, 
                            query=query_my)
                # on remplit le dico bilansDic :
                bilansDic[(bilanName, bilanLabel)] = id_bilan
            elif row[2] != '':
                # C'est donc un item ou un bilan lié
                itemName = utils_functions.u(row[2])
                itemLabel = utils_functions.u(row[3])
                coeff = row[4]
                if coeff == '':
                    coeff = 1
                comment = utils_functions.u(row[5])
                if comment != 'BILAN':
                    # c'est donc bien un item ; suite de tests pour trouver son état :
                    etat = 'NEW'
                    # est-il déjà là ?
                    commandLine_test = utils_db.qs_prof_AllWhereNameLabel.format('items')
                    query_my = utils_db.queryExecute(
                        {commandLine_test: (itemName, itemLabel)}, query=query_my)
                    while query_my.next():
                        etat = 'DEJA_LA'
                        id_item = int(query_my.value(0))
                    # le label est modifié ?
                    if etat == 'NEW':
                        commandLine_test = utils_db.qs_prof_AllWhereName.format('items')
                        query_my = utils_db.queryExecute(
                            {commandLine_test: (itemName, )}, query=query_my)
                        itemLabelEX = ''
                        while query_my.next():
                            etat = 'LABEL_MODIF'
                            id_item = int(query_my.value(0))
                            itemLabelEX = query_my.value(2)
                        if itemLabelEX == itemLabel:
                            etat = 'DEJA_LA'
                    # le nom est modifié ?
                    if etat == 'NEW':
                        commandLine_test = utils_db.qs_prof_AllWhereLabel.format('items')
                        query_my = utils_db.queryExecute(
                            {commandLine_test: (itemLabel, )}, query=query_my)
                        itemNameEX = ''
                        while query_my.next():
                            etat = 'NAME_MODIF'
                            id_item = int(query_my.value(0))
                            itemNameEX = query_my.value(1)
                        if itemNameEX == itemName:
                            etat = 'DEJA_LA'
                    # on passe à l'action :
                    if etat == 'NAME_MODIF':
                        if not(modifItemNameNoToAll):
                            # on demande confirmation avant de modifier l'item :
                            q1 = QtWidgets.QApplication.translate('main', 'Modify this item Name ?')
                            question = utils_functions.u(qName).format(
                                q1, itemNameEX, itemLabel, q2, itemName, itemLabel)
                            reply = utils_functions.messageBox(
                                main, level='question', message=question,
                                buttons=['Yes', 'NoToAll', 'No', 'Cancel'])
                            if reply == QtWidgets.QMessageBox.NoToAll:
                                modifItemNameNoToAll = True
                                etat = 'NEW'
                            elif reply == QtWidgets.QMessageBox.Yes:
                                message = utils_functions.u('NAME_MODIF: {0} | {1} > {2}').format(
                                    itemNameEX, itemName, itemLabel)
                                utils_functions.afficheMessage(main, message)
                                prof_itemsbilans.editItem(main, id_item, itemName, itemLabel)
                            elif reply == QtWidgets.QMessageBox.No:
                                # on aura donc 2 items de même label :
                                etat = 'NEW'
                            else:
                                utils_functions.restoreCursor()
                                message = QtWidgets.QApplication.translate('main', 'Import Canceled')
                                utils_functions.messageBox(main, level='warning', message=message)
                                return
                        else:
                            etat = 'NEW'
                    elif etat == 'LABEL_MODIF':
                        # on demande confirmation avant de modifier l'item :
                        q1 = QtWidgets.QApplication.translate('main', 'Modify this item Label ?')
                        question = utils_functions.u(qLabel).format(
                            q1, itemName, itemLabelEX, q2, itemName, itemLabel)
                        reply = utils_functions.messageBox(
                            main, level='question', message=question,
                            buttons=['Yes', 'No', 'Cancel'])
                        if reply == QtWidgets.QMessageBox.Yes:
                            message = utils_functions.u('LABEL_MODIF: {0} | {1} > {2}').format(
                                itemName, itemLabelEX, itemLabel)
                            utils_functions.afficheMessage(main, message)
                            prof_itemsbilans.editItem(main, id_item, itemName, itemLabel)
                        elif reply == QtWidgets.QMessageBox.No:
                            # pas 2 items du même nom, donc on passe :
                            pass
                        else:
                            utils_functions.restoreCursor()
                            message = QtWidgets.QApplication.translate('main', 'Import Canceled')
                            utils_functions.messageBox(main, level='warning', message=message)
                            return
                    if etat == 'NEW':
                        # on crée le nouvel item :
                        result_item, id_item = prof_itemsbilans.createItem(
                            main, itemName, itemLabel)
                        message = utils_functions.u('createItem: {0}, {1}').format(
                            itemName, itemLabel)
                        utils_functions.afficheMessage(main, message, statusBar=True)
                    # on inscrit dans la table item_bilan si besoin :
                    if id_bilan != -1:
                        commandLine = utils_db.qs_prof_AllFromItemBilanWhereItemBilan.format(
                            id_item, id_bilan)
                        query_my = utils_db.queryExecute(commandLine, query=query_my)
                        mustBind = True
                        if query_my.first():
                            mustBind = False
                        if mustBind:
                            commandLine_my = utils_db.insertInto('item_bilan')
                            query_my = utils_db.queryExecute(
                                {commandLine_my: (id_item, id_bilan, coeff)}, 
                                query=query_my)
                    # on modifie le coefficient si besoin :
                    if id_bilan != -1:
                        coeffEX = coeff
                        commandLine_test = utils_db.qs_prof_AllFromItemBilanWhereItemBilan.format(
                            id_item, id_bilan)
                        query_my = utils_db.queryExecute(commandLine_test, query=query_my)
                        if query_my.first():
                            coeffEX = int(query_my.value(2))
                        if coeffEX != coeff:
                            commandLine = utils_db.qdf_prof_itemBilan3.format(id_item, id_bilan)
                            query_my = utils_db.queryExecute(commandLine, query=query_my)
                            commandLine_my = utils_db.insertInto('item_bilan')
                            query_my = utils_db.queryExecute(
                                {commandLine_my: (id_item, id_bilan, coeff)}, 
                                query=query_my)
                    # on modifie le commentaire si besoin :
                    commentEX = ''
                    commandLine = ('SELECT comment FROM comments '
                        'WHERE isItem=1 AND id={0}').format(id_item)
                    query_my = utils_db.queryExecute(commandLine, query=query_my)
                    while query_my.next():
                        commentEX = query_my.value(0)
                    if comment != commentEX:
                        # on met à jour la base :
                        commandLine = (
                            'DELETE FROM comments '
                            'WHERE isItem=1 AND id={0}').format(id_item)
                        query_my = utils_db.queryExecute(commandLine, query=query_my)
                        commandLine = utils_db.insertInto('comments')
                        if comment != '':
                            query_my = utils_db.queryExecute(
                                {commandLine: (1, id_item, comment)}, 
                                query=query_my)
                else:
                    # c'est donc un bilan lié
                    if id_bilan in bilan_bilan:
                        bilan_bilan[id_bilan].append((itemName, itemLabel))
                    else:
                        bilan_bilan[id_bilan] = [(itemName, itemLabel)]
        # tous les bilans étant créés, on s'occupe des liens bilan -> bilan :
        lines = []
        for id_bilan1 in bilan_bilan:
            for (bilanName2, bilanLabel2) in bilan_bilan[id_bilan1]:
                try:
                    # on crée le lien :
                    id_bilan2 = bilansDic[(bilanName2, bilanLabel2)]
                    lines.append((id_bilan1, id_bilan2))
                except:
                    continue
        commandLine_my = utils_db.insertInto('bilan_bilan')
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
        # on termine par une vérification des bilans communs :
        prof_db.checkCommunBilans(main, msgFin=False)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
        result = True
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to open {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()
        return result


def exportTemplates2Csv(main, fileName, selectedTemplates=[]):
    """
    procédure d'export de modèles de tableaux au format csv.
    on fabrique un tableau [TemplateName, TemplateLabel, ItemName, ItemLabel, comment]
    pour chaque modèle :
        on ajoute une ligne décrivant le modèle (TemplateName, TemplateLabel, None, None, None)
        on recherche ensuite les items liés à ce modèle, et pour chacun
        on ajoute une ligne pour l'item (None, None, ItemName, ItemLabel, comment)
    """
    utils_functions.afficheMessage(main, 'exportTemplates2Csv')
    utils_functions.doWaitCursor()
    try:
        import csv
        if utils.PYTHONVERSION >= 30:
            theFile = open(fileName, 'w', newline='', encoding='utf-8')
        else:
            theFile = open(fileName, 'wb')
        writer = csv.writer(theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(CSV_FILES_TITLES['templates'])
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        # on récupère la liste des modèles à exporter :
        templates = {}
        commandLine_my = utils_db.q_selectAllFromOrder.format(
            'templates', 'ordre, UPPER(Name)')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_template = int(query_my.value(0))
            if ((len(selectedTemplates) < 1) or (id_template in selectedTemplates)):
                templateName = query_my.value(1)
                templateLabel = query_my.value(3)
                templates[id_template] = (templateName, templateLabel)
        # on récupère les commentaires :
        comments = {}
        commandLine_my = utils_db.q_selectAllFrom.format('comments')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            isItem = int(query_my.value(0))
            id_truc = int(query_my.value(1))
            comment = query_my.value(2)
            comments[(isItem, id_truc)] = comment
        # on récupère les items :
        items = {}
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            comment = ''
            if (1, id_item) in comments:
                comment = comments[(1, id_item)]
            items[id_item] = (itemName, itemLabel, comment)
        # on inscrit chaque modèle, puis la liste des items liés :
        for id_template in selectedTemplates:
            (templateName, templateLabel) = templates[id_template]
            # inscription du modèle :
            writer.writerow([
                utils_functions.s(templateName), 
                utils_functions.s(templateLabel), 
                None, None, None, None])
            # on cherche les items liés :
            commandLine_my = (
                'SELECT items.*, tableau_item.ordre '
                'FROM items '
                'JOIN tableau_item ON tableau_item.id_item=items.id_item '
                'WHERE tableau_item.id_tableau={0} '
                'ORDER BY tableau_item.ordre').format(id_template)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                (itemName, itemLabel, comment) = items[id_item]
                ordre = int(query_my.value(3))
                writer.writerow([
                    None, None, 
                    utils_functions.s(itemName), 
                    utils_functions.s(itemLabel), 
                    utils_functions.s(comment), 
                    ordre])
        utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        theFile.close()
        utils_functions.restoreCursor()


def importTemplatesFromCsv(main, fileName, msgFin=True):
    """
    Importe des modèles.
    Pour chaque modèle, on vérifie qu'il n'est pas déjà là.
    """
    result = False
    utils_functions.doWaitCursor()
    try:
        import prof
        import prof_itemsbilans
        headers, data = csv2List(fileName)
        # on vérifie la ligne de titres :
        isOldFile = False
        if headers == CSV_FILES_TITLES['templatesOLD']:
            # VERSIONDB_PROF < 13
            isOldFile = True
        elif headers != CSV_FILES_TITLES['templates']:
            utils_functions.myPrint('BAD CSV FILE !!!!!!!!!!!!!!!!!!!!')
            message = QtWidgets.QApplication.translate(
                'main', 'The structure of the file that you selected is not correct.')
            utils_functions.restoreCursor()
            messageBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, 
                utils.PROGNAME, message, 
                QtWidgets.QMessageBox.NoButton, main)
            messageBox.exec_()
            return
        id_template = 0
        query_my = utils_db.query(main.db_my)
        query_commun = utils_db.query(main.db_commun)
        modifItemNameNoToAll = False
        for row in data:
            if row[0] != '':
                # C'est donc un modèle, puisque la première colonne n'est pas vide
                templateName = utils_functions.u(row[0])
                if isOldFile:
                    templateLabel = ''
                else:
                    templateLabel = utils_functions.u(row[1])
                # suite de tests pour trouver l'état du modèle :
                etat = 'NEW'
                # est-il déjà là ?
                commandLine_test = utils_db.qs_prof_AllWhereName.format('templates')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (templateName, )}, query=query_my)
                while query_my.next():
                    etat = 'DEJA_LA'
                    id_template = int(query_my.value(0))
                # on passe à l'action :
                if etat == 'NEW':
                    # on cherche un id_template (< 0)
                    # un ordre
                    # et on inscrit dans la table templates :
                    id_template = -1
                    commandLine_my = utils_db.q_selectAllFromOrder.format(
                        'templates', 'id_template')
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    if query_my.first():
                        id_template = int(query_my.value(0)) - 1
                    ordre = 0
                    commandLine_my = utils_db.q_selectAllFromOrder.format(
                        'templates', 'ordre')
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    if query_my.last():
                        ordre = int(query_my.value(2)) + 1
                    commandLine_my = utils_db.insertInto('templates')
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (id_template, templateName, ordre, templateLabel)}, 
                        query=query_my)
                    message = utils_functions.u('createTemplate: {0}').format(templateName)
                    utils_functions.afficheMessage(main, message)
            elif (isOldFile and row[1] != '') or (row[2] != ''):
                # C'est donc un item
                if isOldFile:
                    itemName = utils_functions.u(row[1])
                    itemLabel = utils_functions.u(row[2])
                    comment = utils_functions.u(row[3])
                    ordre = row[4]
                else:
                    itemName = utils_functions.u(row[2])
                    itemLabel = utils_functions.u(row[3])
                    comment = utils_functions.u(row[4])
                    ordre = row[5]
                if ordre == '':
                    ordre = 0
                etat = 'NEW'
                # est-il déjà là ?
                commandLine_test = utils_db.qs_prof_AllWhereNameLabel.format('items')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (itemName, itemLabel)}, query=query_my)
                while query_my.next():
                    etat = 'DEJA_LA'
                    id_item = int(query_my.value(0))
                # le label est modifié ?
                if etat == 'NEW':
                    commandLine_test = utils_db.qs_prof_AllWhereName.format('items')
                    query_my = utils_db.queryExecute(
                        {commandLine_test: (itemName, )}, query=query_my)
                    itemLabelEX = ''
                    while query_my.next():
                        etat = 'LABEL_MODIF'
                        id_item = int(query_my.value(0))
                        itemLabelEX = query_my.value(2)
                    if itemLabelEX == itemLabel:
                        etat = 'DEJA_LA'
                # le nom est modifié ?
                if etat == 'NEW':
                    commandLine_test = utils_db.qs_prof_AllWhereLabel.format('items')
                    query_my = utils_db.queryExecute(
                        {commandLine_test: (itemLabel, )}, query=query_my)
                    itemNameEX = ''
                    while query_my.next():
                        etat = 'NAME_MODIF'
                        id_item = int(query_my.value(0))
                        itemNameEX = query_my.value(1)
                    if itemNameEX == itemName:
                        etat = 'DEJA_LA'
                # pour les messages en cas de doublette :
                qName = (
                    '<p><b>{0}</b></p>'
                    '<p><b>{1}</b> : {2}'
                    '<br/>{3}'
                    '<br/><b>{4}</b> : {5}</p>')
                qLabel = (
                    '<p><b>{0}</b></p>'
                    '<p>{1} : <b>{2}</b>'
                    '<br/>{3}'
                    '<br/>{4} : <b>{5}</b></p>')
                q2 = QtWidgets.QApplication.translate('main', 'in:')
                # on passe à l'action :
                if etat == 'NAME_MODIF':
                    if not(modifItemNameNoToAll):
                        # on demande confirmation avant de modifier l'item :
                        q1 = QtWidgets.QApplication.translate('main', 'Modify this item Name ?')
                        question = utils_functions.u(qName).format(
                            q1, itemNameEX, itemLabel, q2, itemName, itemLabel)
                        reply = utils_functions.messageBox(
                            main, level='question', message=question,
                            buttons=['Yes', 'NoToAll', 'No', 'Cancel'])
                        if reply == QtWidgets.QMessageBox.NoToAll:
                            modifItemNameNoToAll = True
                            etat = 'NEW'
                        elif reply == QtWidgets.QMessageBox.Yes:
                            message = utils_functions.u('NAME_MODIF: {0} | {1} > {2}').format(itemNameEX, 
                                itemName, itemLabel)
                            utils_functions.afficheMessage(main, message)
                            prof_itemsbilans.editItem(main, id_item, itemName, itemLabel)
                        elif reply == QtWidgets.QMessageBox.No:
                            # on aura donc 2 items de même label :
                            etat = 'NEW'
                        else:
                            utils_functions.restoreCursor()
                            message = QtWidgets.QApplication.translate('main', 'Import Canceled')
                            utils_functions.messageBox(main, level='warning', message=message)
                            return
                    else:
                        etat = 'NEW'
                elif etat == 'LABEL_MODIF':
                    # on demande confirmation avant de modifier l'item :
                    q1 = QtWidgets.QApplication.translate('main', 'Modify this item Label ?')
                    question = utils_functions.u(qLabel).format(
                        q1, itemName, itemLabelEX, q2, itemName, itemLabel)
                    reply = utils_functions.messageBox(
                        main, level='question', message=question,
                        buttons=['Yes', 'No', 'Cancel'])
                    if reply == QtWidgets.QMessageBox.Yes:
                        message = utils_functions.u('LABEL_MODIF: {0} | {1} > {2}').format(itemName, 
                            itemLabelEX, itemLabel)
                        utils_functions.afficheMessage(main, message)
                        prof_itemsbilans.editItem(main, id_item, itemName, itemLabel)
                    elif reply == QtWidgets.QMessageBox.No:
                        # pas 2 items du même nom, donc on passe :
                        pass
                    else:
                        utils_functions.restoreCursor()
                        message = QtWidgets.QApplication.translate('main', 'Import Canceled')
                        utils_functions.messageBox(main, level='warning', message=message)
                        return
                if etat == 'NEW':
                    # on crée le nouvel item :
                    result_item, id_item = prof_itemsbilans.createItem(main, 
                        itemName, itemLabel)
                    message = utils_functions.u('createItem: {0}, {1}').format(itemName, itemLabel)
                    utils_functions.afficheMessage(main, message, statusBar=True)
                # on inscrit dans la table tableau_item si besoin :
                if id_template < 0:
                    commandLine = utils_db.qs_prof_tableauItem3.format(id_template, id_item)
                    query_my = utils_db.queryExecute(commandLine, query=query_my)
                    mustBind = True
                    if query_my.first():
                        mustBind = False
                    if mustBind:
                        commandLine_my = utils_db.insertInto('tableau_item')
                        query_my = utils_db.queryExecute(
                            {commandLine_my: (id_template, id_item, ordre)}, 
                            query=query_my)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
        result = True
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to open {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()
        return result


def exportCounts2Csv(main, fileName, selectedCounts=[]):
    """
    procédure d'export de comptages au format csv.
    """
    utils_functions.afficheMessage(main, 'exportCounts2Csv')
    utils_functions.doWaitCursor()
    try:
        import csv
        if utils.PYTHONVERSION >= 30:
            theFile = open(fileName, 'w', newline='', encoding='utf-8')
        else:
            theFile = open(fileName, 'wb')
        writer = csv.writer(theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(CSV_FILES_TITLES['counts'])
        for data in selectedCounts:
            writer.writerow([
                utils_functions.s(data[0]), 
                data[1], 
                data[2], 
                utils_functions.s(data[3]),
                data[4]])
        utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        theFile.close()
        utils_functions.restoreCursor()


class SelectGroupsPeriodsDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, comments=''):
        super(SelectGroupsPeriodsDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate(
                'main', 'SelectGroupsPeriods'))

        labelComments = QtWidgets.QLabel(comments)
        self.listWidgetTo = QtWidgets.QListWidget()
        self.listWidgetTo.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection)
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        self.grid = QtWidgets.QGridLayout()
        self.grid.addWidget(labelComments,      1, 0, 1, 2, QtCore.Qt.AlignHCenter)
        self.grid.addWidget(self.listWidgetTo,  3, 0, 1, 2)
        self.grid.addWidget(buttonBox,          10, 0, 1, 2)
        # la grille est le widget de base :
        self.setLayout(self.grid)
        self.setMinimumWidth(400)

        self.names2data = {}
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format(
                'groupes', 'ordre, Matiere, UPPER(Name)'), 
            db=self.main.db_my)
        while query_my.next():
            id_groupe = int(query_my.value(0))
            groupeName = query_my.value(1)
            matiereName = query_my.value(2)
            for periode in range(utils.NB_PERIODES):
                name = utils_functions.u(
                    '{0} ({1}) - {2}').format(
                        groupeName, matiereName, utils.PERIODES[periode])
                self.names2data[name] = (id_groupe, periode, matiereName)
                self.listWidgetTo.addItem(name)
                if (id_groupe, periode) == (self.main.id_groupe, utils.selectedPeriod):
                    item = self.listWidgetTo.item(self.listWidgetTo.count() - 1)
                    self.listWidgetTo.setCurrentItem(item)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('import-export-counts-csv')


def importCountsFromCsv(main, fileName, where=[], msgFin=True):
    """
    Importe des comptages pour le groupe et la période sélectionnés.
    Pour chaque comptage, on vérifie qu'il n'est pas déjà là.
    where est la liste des couples (id_groupe, periode) 
    pour lesquels ont veut inscrire les comptages.
    Si where est vide, ce sera (main.id_groupe, utils.selectedPeriod).
    """
    comments = QtWidgets.QApplication.translate(
        'main', 
        'Select couples group+period for which you want '
        '<br/>to import the counts contained '
        '<br/>in the CSV file:')
    comments = utils_functions.u('<p></p><p>{0}</p>').format(comments)
    dialog = SelectGroupsPeriodsDlg(parent=main, comments=comments)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        return False
    # récupération des couples (id_groupe, periode) :
    where = []
    selectedGroupes = dialog.listWidgetTo.selectedItems()
    for selectedGroupe in selectedGroupes :
        name = selectedGroupe.text()
        (id_groupe, periode, matiereName) = dialog.names2data[name]
        where.append((id_groupe, periode))
    # on y va :
    result = False
    utils_functions.doWaitCursor()
    try:
        import prof
        import prof_itemsbilans
        headers, data = csv2List(fileName)
        # on vérifie la ligne de titres :
        isOldFile = 0
        if headers == CSV_FILES_TITLES['countsOLDOLD']:
            # VERSIONDB_PROF < 13
            isOldFile = -2
        elif headers == CSV_FILES_TITLES['countsOLD']:
            # VERSIONDB_PROF < 16
            isOldFile = -1
        elif headers != CSV_FILES_TITLES['counts']:
            utils_functions.myPrint('BAD CSV FILE !!!!!!!!!!!!!!!!!!!!')
            message = QtWidgets.QApplication.translate(
                'main', 'The structure of the file that you selected is not correct.')
            utils_functions.restoreCursor()
            messageBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, 
                utils.PROGNAME, message, 
                QtWidgets.QMessageBox.NoButton, main)
            messageBox.exec_()
            return
        query_my = utils_db.query(main.db_my)
        if len(where) < 1:
            where = [(main.id_groupe, utils.selectedPeriod), ]
        new_id_count, new_ordre = 0, 0
        for (id_groupe, periode) in where:
            # récupération des comptages existants :
            actualCounts = {}
            commandLine_my = utils_db.qs_prof_counts.format(id_groupe, periode)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_count = int(query_my.value(2))
                countName = query_my.value(3)
                sens = int(query_my.value(4))
                calcul = int(query_my.value(5))
                ordre = int(query_my.value(6))
                countLabel = query_my.value(7)
                id_item = int(query_my.value(8))
                actualCounts[countName] = [id_count, sens, calcul, ordre, countLabel, id_item]
                new_id_count = id_count + 1
                new_ordre = ordre + 1
            # on complète avec les comptages du fichier :
            lines = []
            for row in data:
                if row[0] != '':
                    countName = utils_functions.u(row[0])
                    sens = row[1]
                    calcul = row[2]
                    if isOldFile < -1:
                        countLabel = ''
                    else:
                        countLabel = utils_functions.u(row[3])
                    if isOldFile < 0:
                        id_item = -1
                    else:
                        id_item = row[4]
                    if countName in actualCounts:
                        mustDo = False
                        if actualCounts[countName][1] != sens:
                            mustDo = True
                        if actualCounts[countName][2] != calcul:
                            mustDo = True
                        if actualCounts[countName][4] != countLabel:
                            mustDo = True
                        actualCounts[countName][1] = sens
                        actualCounts[countName][2] = calcul
                        actualCounts[countName][4] = countLabel
                        if mustDo:
                            # on efface le comptage  :
                            commandLine_my = utils_db.qdf_prof_counts.format(
                                id_groupe, periode, actualCounts[countName][0])
                            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                            # on le réécrira :
                            listArgs = [id_groupe, periode, actualCounts[countName][0], 
                                        countName, sens, calcul, 
                                        actualCounts[countName][3], countLabel, id_item]
                            lines.append(listArgs)
                    else:
                        # on l'inscrira :
                        listArgs = [id_groupe, periode, new_id_count, 
                                    countName, sens, calcul, new_ordre, countLabel, id_item]
                        lines.append(listArgs)
                        new_id_count += 1
                        new_ordre += 1
            # on écrit dans la table counts :
            commandLine_my = utils_db.insertInto('counts')
            query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
        result = True
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to open {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()
        return result


def exportEleves2Csv(main, fileName):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'exportEleves2Csv')
    utils_functions.doWaitCursor()
    try:
        import csv
        if utils.PYTHONVERSION >= 30:
            theFile = open(fileName, 'w', newline='', encoding='utf-8')
        else:
            theFile = open(fileName, 'wb')
        writer = csv.writer(theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        writer.writerow(CSV_FILES_TITLES['eleves'])
        commandLine_users = utils_db.q_selectAllFromOrder.format('eleves', 'id')
        query_users = utils_db.queryExecute(commandLine_users, db=main.db_users)
        while query_users.next():
            id_eleve = int(query_users.value(0))
            eleveName = query_users.value(1)
            elevePrenom = query_users.value(2)
            eleveClasse = query_users.value(3)
            writer.writerow([
                id_eleve, 
                utils_functions.s(eleveName), 
                utils_functions.s(elevePrenom), 
                utils_functions.s(eleveClasse)])
        utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        theFile.close()
        utils_functions.restoreCursor()

def importElevesFromCsv(main, fileName):
    """
    En version perso (sans passer par l'administration) :
    mise à jour de la liste des élèves (et des classes)
    depuis un fichier csv.
    """
    utils_functions.afficheMessage(main, 'importElevesFromCsv')
    utils_functions.doWaitCursor()
    try:
        headers, data = csv2List(fileName)
        # on vérifie la ligne de titres :
        if headers != CSV_FILES_TITLES['eleves']:
            utils_functions.myPrint('BAD CSV FILE !!!!!!!!!!!!!!!!!!!!')
            message = QtWidgets.QApplication.translate(
                'main', 'The structure of the file that you selected is not correct.')
            utils_functions.restoreCursor()
            messageBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning, 
                utils.PROGNAME, message, 
                QtWidgets.QMessageBox.NoButton, main)
            messageBox.exec_()
            return
        # besoin de 2 querys :
        query_users = utils_db.query(main.db_users)
        query_commun = utils_db.query(main.db_commun)
        # pour créer les nouvelles classes sans perdre les anciennes encore utilisées :
        listClassesEx, listClasses, idClasseMax = {}, {}, 0
        # récupération des anciennes classes :
        commandLine_commun = utils_db.q_selectAllFromOrder.format('classes', 'ordre, Classe')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            id_classe = int(query_commun.value(0))
            classe = query_commun.value(1)
            classeType = int(query_commun.value(2))
            try:
                notes = int(query_commun.value(3))
            except:
                notes = 0
            try:
                ordre = int(query_commun.value(4))
            except:
                ordre = id_classe
            if not(classe in listClassesEx):
                listClassesEx[classe] = (id_classe, classeType, notes, ordre)
            if id_classe > idClasseMax:
                idClasseMax = id_classe
        # on recrée les tables users.eleves et commun.classes :
        commandLine_users = utils_db.qdf_table.format('eleves')
        query_users = utils_db.queryExecute(
            commandLine_users, query=query_users)
        commandLine_commun = utils_db.qdf_table.format('classes')
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        # remplissage de la table users.eleves :
        commandLine_users = utils_db.insertInto('eleves', 9)
        lines = []
        id_bilan = -1
        coeff = 1
        for row in data:
            id_eleve = row[0]
            eleveName = utils_functions.u(row[1])
            elevePrenom = utils_functions.u(row[2])
            eleveClasse = utils_functions.u(row[3])
            eleveLogin = ''
            eleveMdp = ''
            eleveSexe = -1
            eleveDateNaiss = ''
            listArgs = (
                id_eleve, eleveName, elevePrenom,
                eleveClasse, eleveLogin, eleveMdp, eleveMdp, 
                eleveSexe, eleveDateNaiss)
            lines.append(listArgs)
            # on en profite pour récupérer la classe :
            if not(eleveClasse in listClasses):
                if eleveClasse in listClassesEx:
                    id_classe = listClassesEx[eleveClasse][0]
                    classeType = listClassesEx[eleveClasse][1]
                    notes = listClassesEx[eleveClasse][2]
                    ordre = listClassesEx[eleveClasse][3]
                    listClasses[eleveClasse] = (id_classe, classeType, notes, ordre)
                else:
                    idClasseMax += 1
                    # ICI faire choisir le classeType
                    classeType = 0
                    listClasses[eleveClasse] = (idClasseMax, classeType, 0, 0)
        query_users = utils_db.queryExecute(
            {commandLine_users: lines}, query=query_users)
        # remplissage de la table commun.classes d'après la liste :
        commandLine_commun = utils_db.insertInto('classes', utils_db.listTablesCommun['classes'][1])
        lines = []
        for classe in listClasses:
            (id_classe, classeType, notes, ordre) = listClasses[classe]
            lines.append((id_classe, classe, classeType, notes, ordre))
        query_commun = utils_db.queryExecute(
            {commandLine_commun: lines}, query=query_commun)
        utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


def importReferentielFromCsv(main, fileName):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'importReferentielFromCsv')
    utils_functions.doWaitCursor()
    try:
        headers, data = csv2List(fileName)
        query_commun = utils_db.queryExecute(
            utils_db.qdf_table.format('referentiel'), 
            db=main.db_commun)
        lines = []
        for row in data:
            id = row[0]
            code = utils_functions.u(row[1])
            Titre1 = utils_functions.u(row[2])
            Titre2 = utils_functions.u(row[3])
            Titre3 = utils_functions.u(row[4])
            Competence = utils_functions.u(row[5])
            T1 = utils_functions.u(row[6])
            T2 = utils_functions.u(row[7])
            T3 = utils_functions.u(row[8])
            Cpt = utils_functions.u(row[9])
            Commentaire = utils_functions.u(row[10])
            classeType = row[11]
            try:
                ordre = row[12]
            except:
                ordre = id
            listArgs = (
                id, code,
                Titre1, Titre2, Titre3, Competence,
                T1, T2, T3, Cpt,
                Commentaire, classeType, ordre)
            lines.append(listArgs)
        commandLine_commun = utils_db.insertInto(
            'referentiel', 
            utils_db.listTablesCommun['referentiel'][1])
        query_commun = utils_db.queryExecute(
            {commandLine_commun: lines}, query=query_commun)
        utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()





