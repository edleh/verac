# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Fonctions de manipulation de fichiers ou dossiers :
    copier, ...
"""

# importation des modules utiles :
from __future__ import division
import sys
import os
import shutil

# importation des modules perso :
import utils, utils_functions

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



"""
****************************************************
    CONFIGURATION
****************************************************
"""

def createTempAppDir(PROGLINK, mustReCreate=True):
    """
    (re)création du dossier temporaire
    on cree un sous-dossier /tmp/PROGLINK
    """
    tempDir = QtCore.QDir.temp()
    tempAppPath = utils_functions.u('{0}/{1}').format(
        QtCore.QDir.tempPath(), PROGLINK)
    if mustReCreate:
        emptyDir(tempAppPath)
        tempDir.rmdir(PROGLINK)
    tempAppDir = QtCore.QDir(tempAppPath)
    if tempAppDir.exists():
        return tempAppPath
    elif tempDir.mkdir(PROGLINK):
        return tempAppPath
    else:
        return QtCore.QDir.tempPath()

def createConfigAppDir(PROGLINK, beginDir='', personal=True):
    """
    Crée un sous dossier ".PROGLINK" dans le home (dossier utilisateur)
    (ou un dossier PROGLINK dans home/.config)
    Ce sera le dossier de configuration du logiciel
    """
    progLink = '.' + PROGLINK
    # récupération du dossier home:
    if utils.OS_NAME[0] == 'win':
        # home on win32 is broken
        if 'APPDATA' in os.environ:
            winAppDataDir = os.environ['APPDATA']
        else:
            winAppDataDir = QtCore.QDir.homePath()
        if utils.MODEBAVARD:
            utils_functions.myPrint('winAppDataDir: ', winAppDataDir)
        homeDir = QtCore.QDir(winAppDataDir)
        homeDirPath = QtCore.QDir(winAppDataDir).path()
    else:
        homeDir = QtCore.QDir.home()
        homeDirPath = QtCore.QDir.home().path()
        # test de la présence d'un dossier .config :
        pConfigPath = homeDirPath + '/.config/'
        if QtCore.QDir(pConfigPath).exists():
            # s'il y a un home/.PROGLINK, on le déplace dans .config
            # (en supprimant le .) :
            pProgLinkPath = homeDirPath + '/.' + PROGLINK + '/'
            if QtCore.QDir(pProgLinkPath).exists():
                if not(QtCore.QDir(pConfigPath + PROGLINK).exists()):
                    QtCore.QDir(pConfigPath).mkdir(PROGLINK)
                copyDir(pProgLinkPath, pConfigPath + PROGLINK)
                emptyDir(pProgLinkPath)
            progLink = PROGLINK
            homeDirPath = homeDirPath + '/.config'

    # test de lecture d'un fichier perso (PB Lionel N) :
    if personal:
        fileName = utils_functions.u(
            '{0}/files/personal.csv').format(beginDir)
        if QtCore.QFileInfo(fileName).exists():
            # on récupère les lignes du fichier csv :
            import utils_export
            headers, datas = utils_export.csv2List(fileName)
            for row in datas:
                if row[0] == 'homeDirPath':
                    homeDirPath = row[2]
                    progLink = '.' + PROGLINK

    # création du dossier config dans le home:
    configAppDir = QtCore.QDir(homeDirPath + '/' + progLink + '/')
    if not(configAppDir.exists()):
        QtCore.QDir(homeDirPath).mkdir(progLink)
        configAppDir = QtCore.QDir(homeDirPath + '/' + progLink + '/')
    return configAppDir

def testLockFile(main, name='verac'):
    """
    le programme est-il déjà lancé ? À améliorer ?
    """
    result = True
    LOCKFILENAME = utils_functions.u(
        '{0}/{1}_lock').format(QtCore.QDir.tempPath(), name)
    if QtCore.QFile(LOCKFILENAME).exists():
        if name == 'verac':
            appName = utils.PROGNAMEHTML
        elif name == 'verac_scheduler':
            appName = QtWidgets.QApplication.translate(
                'main', 'The scheduler')
        else:
            appName = ''
        message0 = QtWidgets.QApplication.translate(
            'main', 
            '<b>{0}</b> seems to be already in use.'
            '<br/>If you are sure this is not the case, '
            'you can continue.').format(appName)
        message1 = QtWidgets.QApplication.translate(
            'main', 'Do you want to continue?')
        message = utils_functions.u(
            '<p>{0}</p><p><b>{1}</b></p>').format(message0, message1)
        ret = utils_functions.messageBox(
            main, 
            level='warning', 
            message=message, 
            buttons=['Yes', 'No'])
        if ret != QtWidgets.QMessageBox.Yes:
            result = False
    if result:
        removeAndCopy(
            utils_functions.u('{0}/files/lock').format(main.beginDir), 
            LOCKFILENAME)
    return result

def deleteLockFile(name='verac'):
    LOCKFILENAME = utils_functions.u(
        '{0}/{1}_lock').format(QtCore.QDir.tempPath(), name)
    if QtCore.QFile(LOCKFILENAME).exists():
        removeOK = QtCore.QFile(LOCKFILENAME).remove()
        if not(removeOK):
            utils_functions.myPrint('REMOVE ERROR : ', LOCKFILENAME)



"""
****************************************************
    FICHIERS ET DOSSIERS (COPIE, ETC)
****************************************************
"""

def openFile(fileName):
    localefileFile = QtCore.QFileInfo(fileName)
    localefileFile.makeAbsolute()
    thefile = localefileFile.filePath()
    url = QtCore.QUrl().fromLocalFile(utils_functions.u(thefile))
    QtGui.QDesktopServices.openUrl(url)

def openDir(dirName):
    """
    Sous Windobs, ça ne marche qu'avec des ~1 partout
    Comme quoi Dos existe encore...
    Pas sùr que ça marche à tous les coups.
    """
    try:
        dirName = utils_functions.u(dirName)
        url = QtCore.QUrl().fromLocalFile(dirName)
        QtGui.QDesktopServices.openUrl(url)
    except:
        if utils.OS_NAME[0] == 'win':
            dirName = os.sep.join(dirName.split('/'))
            dirName = utils_functions.u(dirName)
            commandLine = utils_functions.u(
                'explorer "{0}"').format(dirName)
            os.system(commandLine)

def copyDir(src, dst, ignore=()):
    """
    copie récursive d'un dossier dans un autre
    """
    src = utils_functions.addSlash(src)
    dst = utils_functions.addSlash(dst)
    if utils.MODEBAVARD:
        utils_functions.myPrint('copyDir :', src, dst)
    has_err = False
    srcDir = QtCore.QDir(src)
    dstDir = QtCore.QDir(dst)
    if srcDir.exists():
        entries = srcDir.entryInfoList(
            QtCore.QDir.NoDotAndDotDot | \
            QtCore.QDir.Dirs | \
            QtCore.QDir.Files | \
            QtCore.QDir.Hidden)
        for entryInfo in entries:
            name = entryInfo.fileName()
            path = entryInfo.absoluteFilePath()
            if entryInfo.isDir():
                if not(name in ignore):
                    dstDir.mkdir(name)
                    # on fait suivre :
                    has_err = copyDir(
                        utils_functions.u('{0}{1}').format(src, name), 
                        utils_functions.u('{0}{1}').format(dst, name), 
                        ignore=ignore)
            elif entryInfo.isFile():
                if not(name in ignore):
                    if utils.MODEBAVARD:
                        utils_functions.myPrint('copy: ', name)
                    srcFileName = utils_functions.u(
                        '{0}{1}').format(src, name)
                    dstFileName = utils_functions.u(
                        '{0}{1}').format(dst, name)
                    if QtCore.QFile(dstFileName).exists():
                        QtCore.QFile(dstFileName).remove()
                    QtCore.QFile(srcFileName).copy(dstFileName)
    return has_err

def removeAndCopy(sourceFile, destFile, testSize=-1):
    if testSize > 0:
        sourceSize = QtCore.QFileInfo(sourceFile).size()
        if sourceSize < testSize:
            utils_functions.myPrint(
                'TEST SIZE : ', sourceFile, sourceSize)
            return False
    if QtCore.QFile(destFile).exists():
        removeOK = QtCore.QFile(destFile).remove()
        if not(removeOK):
            utils_functions.myPrint('REMOVE ERROR : ', destFile)
    #copyOK = QtCore.QFile(sourceFile).copy(destFile)
    copyOK = False
    try:
        shutil.copyfile(sourceFile, destFile)
        copyOK = True
    except:
        pass
    if not(copyOK):
        utils_functions.myPrint('COPY ERROR : ', sourceFile, destFile)
    return copyOK

def copyFile(actualPath, newPath, actualFile, srcbackup):
    src = srcbackup + actualPath + '/' + actualFile
    dst = newPath + '/' + actualFile
    if QtCore.QFileInfo(src).isDir():
        if not(QtCore.QDir(dst).exists()):
            QtCore.QDir(newPath).mkdir(actualFile)
    elif QtCore.QFileInfo(src).isFile():
        if not(QtCore.QFile(dst).exists()):
            QtCore.QFile(src).copy(dst)

def emptyDir(dirName, deleteThisDir=True, filesToKeep=[]):
    """
    Vidage récursif d'un dossier.
    Si deleteThisDir est mis à False, le dossier lui-même n'est pas supprimé.
    filesToKeep est une liste de noms de fichiers à ne pas effacer 
        (filesToKeep=['.htaccess'] par exemple).
    """
    if utils.MODEBAVARD:
        utils_functions.myPrint('emptyDir ', dirName)
    has_err = False
    aDir = QtCore.QDir(dirName)
    if aDir.exists():
        entries = aDir.entryInfoList(
            QtCore.QDir.NoDotAndDotDot | \
            QtCore.QDir.Dirs | \
            QtCore.QDir.Files | \
            QtCore.QDir.Hidden)
        for entryInfo in entries:
            path = entryInfo.absoluteFilePath()
            if entryInfo.isDir():
                # on fait suivre filesToKeep, mais deleteThisDir sera True :
                has_err = emptyDir(path, filesToKeep=filesToKeep)
            elif entryInfo.isFile():
                if not(entryInfo.fileName() in filesToKeep):
                    f = QtCore.QFile(path)
                    if f.exists():
                        if not(f.remove()):
                            utils_functions.myPrint('PB: ', path)
                            has_err = True
        if deleteThisDir:
            if not(aDir.rmdir(aDir.absolutePath())):
                utils_functions.myPrint(
                    'Erreur de suppression de : ' + aDir.absolutePath())
                has_err = True
    return has_err

def createDirs(inPath, newPaths):
    """
    création de sous-dossiers (newPaths) dans un dossier (inPath)
    newPaths peut contenir plusieurs dossiers à créer
    Par exemple, createDirs(inPath, utils_functions.u('abé/ééc'))
    """
    if newPaths.split('/')[0] != '':
        a, b = newPaths.split('/')[0], '/'.join(newPaths.split('/')[1:])
    else:
        a, b = newPaths.split('/')[1], '/'.join(newPaths.split('/')[2:])
    if a != '':
        inDir = QtCore.QDir(inPath)
        a = utils_functions.u(a)
        newDir = QtCore.QDir(inPath + '/' + a)
        if not(newDir.exists()):
            if utils.MODEBAVARD:
                utils_functions.myPrint('create ', inPath + '/' + a)
            inDir.mkdir(a)
        if b != '':
            createDirs(inPath + '/' + a, b)



"""
****************************************************
    ARCHIVAGE
****************************************************
"""

def md5Sum(fileName):
    """
    calcule la somme md5 d'un fichier
    """
    import hashlib
    md5 = hashlib.md5()
    with open(fileName, 'rb') as f:
        for chunk in iter(lambda: f.read(128 * md5.block_size), b''):
             md5.update(chunk)
    return md5.hexdigest()

def tarDirectory(tarFileName, directory, beginDir):
    result = False
    try:
        import tarfile
        tarFileName = utils_functions.u(tarFileName)
        QtCore.QDir.setCurrent(
            utils_functions.u('{0}/..').format(directory))
        tar = tarfile.open(tarFileName + '.tar.gz', 'w:gz')
        tar.add(tarFileName, tarFileName)
        tar.close()
        QtCore.QDir.setCurrent(beginDir)
        result = True
    finally:
        return result

def untarDirectory(tarFileName, directory):
    result = False
    try:
        import tarfile
        tarFileName = utils_functions.u(tarFileName)
        directory = utils_functions.u(directory)
        tar = tarfile.open(tarFileName, 'r:gz')
        tar.extractall(directory)
        tar.close()
        result = True
    finally:
        return result

def unzipFile(zipFileName, directory):
    """
    d'après 
        http://python.developpez.com/faq/?page=Compression-Archive
    """
    result = [False, '']
    try:
        import zipfile
        zipFileName = utils_functions.u(zipFileName)
        directory = utils_functions.u(directory)
        zfile = zipfile.ZipFile(zipFileName, 'r')
        for name in zfile.namelist():
            if os.path.isdir(name):
                try:
                    os.makedirs(directory + os.sep + name)
                except:
                    pass
            else:
                try:
                    os.makedirs(
                        directory + os.sep + os.path.dirname(name))
                except:
                    pass
                data = zfile.read(name)
                fp = open(directory + os.sep + name, 'wb')
                fp.write(data)
                fp.close()
                result[1] = name
        zfile.close()
        result[0] = True
    finally:
        return result



"""
****************************************************
    DIVERS
****************************************************
"""

def readTextFile(fileName):
    """
    retourne le contenu d'un fichier texte.
    fileContent = utils_filesdirs.readTextFile(fileName)
    """
    result = ''
    inFile = QtCore.QFile(fileName)
    if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(inFile)
        stream.setCodec('UTF-8')
        result = stream.readAll()
        inFile.close()
    return result

def createLinuxDesktopFile(beginDir, directory, fileName, progName):
    """
    création du fichier progName.desktop
    """
    # on ouvre le fichier livré avec l'archive :
    desktopFileName = utils_functions.u('{0}/files/{1}.desktop').format(
        beginDir, fileName)
    desktopFile = QtCore.QFile(desktopFileName)
    if not(desktopFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text)):
        return False
    stream = QtCore.QTextStream(desktopFile)
    stream.setCodec('UTF-8')
    lines = stream.readAll()
    # on remplace les variables :
    lines = lines.replace('PYTHON', utils_functions.u(sys.executable))
    lines = lines.replace('CHEMIN', utils_functions.u(beginDir))
    lines = lines.replace('PROGNAME', progName)
    desktopFile.close()
    # on crée le fichier final :
    desktopFileName = utils_functions.u('{0}/{1}.desktop').format(
        directory, progName)
    QtCore.QFile(desktopFileName).remove()
    desktopFile = QtCore.QFile(desktopFileName)
    if desktopFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(desktopFile)
        stream.setCodec('UTF-8')
        stream << lines
        desktopFile.close()
    # avec les bons attributs :
    QtCore.QFile(desktopFileName).setPermissions(\
        QtCore.QFile(desktopFileName).permissions() | \
        QtCore.QFile.ExeOwner | \
        QtCore.QFile.ExeUser | \
        QtCore.QFile.ExeGroup | \
        QtCore.QFile.ExeOther)
    # on le copie aussi dans ~/.local/share/applications :
    applicationsPath = QtCore.QDir.homePath() + '/.local/share/applications'
    if QtCore.QDir(applicationsPath).exists():
        desktopFileName2 = utils_functions.u('{0}/{1}.desktop').format(
            applicationsPath, progName)
        QtCore.QFile(desktopFileName2).remove()
        QtCore.QFile(desktopFileName).copy(desktopFileName2)
    return True


def createWinShortcutFile(
        progFileName='', 
        progName='', 
        progDescription='', 
        progIcon='icon', 
        path=''):
    """
    création du fichier progName.lnk sur le bureau windows.
    """
    import os, sys
    import pythoncom
    from win32com.shell import shell, shellcon
    shortcut = pythoncom.CoCreateInstance(
        shell.CLSID_ShellLink, 
        None, 
        pythoncom.CLSCTX_INPROC_SERVER, 
        shell.IID_IShellLink)
    program_location = sys.executable.replace('python.exe', 'pythonw.exe')
    shortcut.SetPath(program_location)
    shortcut.SetDescription(progDescription)
    shortcut.SetArguments(
        '"{1}{2}{0}{3}.pyw"'.format(os.sep, utils.HERE, path, progFileName))
    shortcut.SetIconLocation(
        '{1}{0}images{0}{2}.ico'.format(os.sep, utils.HERE, progIcon), 0)
    desktop_path = shell.SHGetFolderPath(0, shellcon.CSIDL_DESKTOP, 0, 0)
    persist_file = shortcut.QueryInterface(pythoncom.IID_IPersistFile)
    persist_file.Save(
        os.path.join(desktop_path, '{0}.lnk'.format(progName)), 0)


