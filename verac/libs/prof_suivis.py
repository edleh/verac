# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Gestion du suivi des élèves par le Prof Principal.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_web, utils_export
import prof

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



def saveGestElevesSuivi(main, rows=[]):
    """
    Sauvegarde des définitions des élèves suivis.
    """
    import utils_filesdirs
    if main.actualVersion['versionName'] == 'demo':
        utils_functions.notInDemo(main)
        return
    utils_functions.doWaitCursor()
    upOK = False
    try:
        # on récupère les liens entre élèves et compétences suivies :
        elevesCpts = []
        for row in rows:
            for eleveCpt in row[1:]:
                if eleveCpt['value'] != '':
                    id_eleve = eleveCpt['id_eleve']
                    id_suivi = eleveCpt['id_cpt']
                    if 'label2' in eleveCpt:
                        label2 = eleveCpt['label2']
                    else:
                        label2 = ''
                    elevesCpts.append((id_eleve, id_suivi, label2))

        # on efface et remplit la table profxx.suivi_eleve_cpt :
        query_my = utils_db.query(main.db_my)
        commandLine_my = utils_db.qdf_table.format('suivi_eleve_cpt')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        commandLine_my = utils_db.insertInto('suivi_eleve_cpt')
        lines = []
        for (id_eleve, id_suivi, label2) in elevesCpts:
            lines.append((id_eleve, id_suivi, label2))
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
        main.changeDBMyState()

        # on crée la base suivi_xx dans temp :
        id_pp = main.me['userId']
        dbFileTemp_suivixx = utils_functions.u('{0}/up/suivi_{1}.sqlite').format(main.tempPath, id_pp)
        # suppression d'un éventuel fichier précédent :
        if QtCore.QFile(dbFileTemp_suivixx).exists():
            removeOK = QtCore.QFile(dbFileTemp_suivixx).remove()
            if not(removeOK):
                utils_functions.myPrint('REMOVE ERROR : ', dbFileTemp_suivixx)
        (db_suivixx, dbName) = utils_db.createConnection(main, dbFileTemp_suivixx)
        query, transactionOK = utils_db.queryTransactionDB(db_suivixx)
        # on crée la table suivi_pp_eleve_cpt :
        query = utils_db.queryExecute(
            utils_db.qct_suivi_pp_eleve_cpt, query=query)
        # on la remplit :
        commandLine = utils_db.insertInto('suivi_pp_eleve_cpt', 4)
        lines = []
        for (id_eleve, id_suivi, label2) in elevesCpts:
            lines.append((id_pp, id_eleve, id_suivi, label2))
        query = utils_db.queryExecute({commandLine: lines}, query=query)
        # on crée aussi la table suivi_evals :
        query = utils_db.queryExecute(
            utils_db.qct_suivi_evals, query=query)
        # on termine la transaction :
        utils_db.endTransaction(query, db_suivixx, transactionOK)
        if query != None:
            query.clear()
            del query
        db_suivixx.close()
        del db_suivixx
        utils_db.sqlDatabase.removeDatabase(dbName)
        # on poste la base :
        # une fois la base envoyée, elle est traitée sur le site
        # pour ne pas écraser les évaluations précédentes.
        # On doit aussi mettre à jour la base suivis.sqlite
        theFileName = utils_functions.u('suivi_{0}.sqlite').format(id_pp)
        theUrl = main.siteUrlPublic + "/pages/verac_pp_putFollows.php"
        upLoader = utils_web.UpDownLoader(
            main, theFileName, main.tempPath + "/up/", theUrl, down=False)
        upLoader.launch()
        while upLoader.state == utils_web.STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if upLoader.state == utils_web.STATE_OK:
            if "OK" in utils_functions.u(upLoader.reponse):
                upOK = True
    finally:
        utils_functions.restoreCursor()
        if upOK:
            import prof_db
            prof_db.downloadSuivisDB(main, msgFin=False)
            message = QtWidgets.QApplication.translate('main', 'The file was sent successfully.')
            utils_functions.messageBox(main, message=message, timer=5)
        else:
            message = QtWidgets.QApplication.translate('main', 'A problem occurred during the transfer.')
            utils_functions.afficheMsgPb(main, message)
        return upOK


def downloadSuivisPP(main, msgFin=True):
    """
    Téléchargement de la base suivi_xx.sqlite
    """
    utils_functions.afficheMessage(main, 'downloadSuivisPP')
    id_pp = main.me['userId']
    theFileName = 'suivi_{0}.sqlite'.format(id_pp)
    if (utils.lanConfig and utils.lanConfigDir != ''):
        destDir = utils.lanConfigDir
    else:
        destDir = main.localConfigDir
    destDir += '/' + main.actualVersion['versionName']
    if utils_web.downloadFileFromSecretDir(main, '/protected/suivis/', theFileName, toDir=destDir):
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)




class GestElevesSuivisDlg(QtWidgets.QDialog):
    """
    dialogue de gestion des élèves suivis
    et de consultation des évaluations
    """
    def __init__(self, parent=None):
        super(GestElevesSuivisDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate('main', 'GestElevesSuivis')
        self.setWindowTitle(title)

        self.helpContextPage = 'gest-eleves-suivis'

        self.suivisModified = False
        self.mustCancel = False
        self.suivisPPDownloaded = False

        # le comboBox pour choisir l'affichage :
        self.whatComboBox = QtWidgets.QComboBox()
        self.whatComboBox.setMinimumWidth(200)
        self.whatComboBox.addItem(QtWidgets.QApplication.translate('main', 'DefineSuivis'))
        self.whatComboBox.addItem(QtWidgets.QApplication.translate('main', 'ConsultSuivis'))
        self.whatComboBox.activated.connect(self.whatChanged)

        # outils de la vue define :
        # le bouton d'envoi :
        self.saveDefineButton = QtWidgets.QPushButton(utils.doIcon('net-upload'), 
            QtWidgets.QApplication.translate('main', 'SaveDefine'))
        self.saveDefineButton.clicked.connect(self.saveDefine)
        self.saveDefineButton.setEnabled(False)
        # outils de la vue consult :
        # le comboBox pour choisir l'élève :
        self.whoComboBox = QtWidgets.QComboBox()
        self.whoComboBox.setMinimumWidth(200)
        self.whoComboBox.activated.connect(self.whoChanged)
        # le checkBox pour l'ordre chronologique :
        self.chronologicalCheckBox = QtWidgets.QCheckBox(QtWidgets.QApplication.translate(
            'main', 'Chronological'))
        self.chronologicalCheckBox.setToolTip(QtWidgets.QApplication.translate(
            'main', 'ChronologicalStatusTip'))
        self.chronologicalCheckBox.setChecked(False)
        self.chronologicalCheckBox.stateChanged.connect(self.chronologicalChanged)
        # le bouton de téléchargement :
        self.loadConsultButton = QtWidgets.QPushButton(utils.doIcon('net-download'), 
            QtWidgets.QApplication.translate('main', 'LoadConsult'))
        self.loadConsultButton.clicked.connect(self.loadConsult)

        # outils pour toutes les vues :
        # le bouton PRINT :
        printButton = QtWidgets.QPushButton(utils.doIcon('document-print'), 
            QtWidgets.QApplication.translate('main', 'ExportActualVue2Print'))
        printButton.setToolTip(QtWidgets.QApplication.translate('main', 
            'ExportActualVue2PrintStatusTip'))
        printButton.clicked.connect(self.suivis2print)
        # le bouton PDF :
        pdfButton = QtWidgets.QPushButton(utils.doIcon('extension-pdf'), 
            QtWidgets.QApplication.translate('main', 'ExportActualVue2pdf'))
        pdfButton.setToolTip(QtWidgets.QApplication.translate('main', 
            'ExportActualVue2pdfStatusTip'))
        pdfButton.clicked.connect(self.suivis2pdf)
        # le bouton ODS :
        odsButton = QtWidgets.QPushButton(utils.doIcon('extension-ods'), 
            QtWidgets.QApplication.translate('main', 'ExportAll2ods'))
        odsButton.setToolTip('aaaaaaa')
        odsButton.setToolTip(QtWidgets.QApplication.translate('main', 
            'ExportAll2odsStatusTip'))
        odsButton.clicked.connect(self.suivis2ods)

        # on agence tout ça :
        choiceLayout1 = QtWidgets.QVBoxLayout()
        choiceLayout1.addWidget(self.whatComboBox)
        choiceLayout1.addWidget(self.saveDefineButton)
        choiceLayout1.addWidget(self.loadConsultButton)
        choiceLayout2 = QtWidgets.QVBoxLayout()
        choiceLayout2.addWidget(self.whoComboBox)
        choiceLayout2.addWidget(self.chronologicalCheckBox)
        exportLayout1 = QtWidgets.QVBoxLayout()
        exportLayout1.addWidget(printButton)
        exportLayout1.addWidget(pdfButton)
        exportLayout2 = QtWidgets.QVBoxLayout()
        exportLayout2.addWidget(odsButton)
        toolsLayout = QtWidgets.QHBoxLayout()
        toolsLayout.addLayout(choiceLayout1)
        toolsLayout.addLayout(choiceLayout2)
        toolsLayout.addStretch(1)
        toolsLayout.addLayout(exportLayout1)
        toolsLayout.addLayout(exportLayout2)

        self.elevesSuivisView = SuivisTableView(self)
        self.model = SuivisTableModel(self.main)
        self.elevesSuivisView.setModel(self.model)
        self.whatChanged(0, mustRecreate=False)
        self.updateWhoComboBox()

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(toolsLayout)
        mainLayout.addWidget(self.elevesSuivisView)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage)

    def accept(self):
        # bouton OK ; on vérifie avant de fermer.
        if not(self.testElevesSuivisModified()):
            return
        QtWidgets.QDialog.accept(self)

    def testElevesSuivisModified(self):
        """
        Si on change de vue ou si on quitte, il faut vérifier si
        des changements de définitions sont à sauvegarder.
        """
        if not(self.suivisModified):
            return True
        message = QtWidgets.QApplication.translate(
            'main', 
            '<p>The ElevesSuivis list has been modified.</p>'
            '<p></p>'
            '<p><b>Would you save the changes?</b></p>')
        reponse = utils_functions.messageBox(
            self.main, level='warning', message=message,
            buttons=['Yes', 'No', 'Cancel'], 
            defaultButton=QtWidgets.QMessageBox.Yes)
        if reponse == QtWidgets.QMessageBox.Cancel:
            return False
        elif reponse == QtWidgets.QMessageBox.No:
            self.suivisModified = False
            self.saveDefineButton.setEnabled(False)
            return True
        # on envoie la liste des élèves suivis :
        result = saveGestElevesSuivi(self.main, self.model.rows)
        self.suivisModified = not(result)
        self.saveDefineButton.setEnabled(self.suivisModified)
        self.updateWhoComboBox()
        return result

    def updateWhoComboBox(self):
        """
        Mise à jour du comboBox de la liste des élèves suivis.
        """
        # on récupère la liste complète des élèves :
        elevesInGroup = prof.listStudentsInGroup(self.main, id_groupe=self.main.id_groupe)
        # on récupère la liste des id_eleve dans la table profxx.suivi_eleve_cpt :
        elevesIds = []
        commandLine_my = utils_db.q_selectAllFrom.format('suivi_eleve_cpt')
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_eleve = int(query_my.value(0))
            if not(id_eleve in elevesIds):
                elevesIds.append(id_eleve)
        # on remplit :
        self.whoComboBox.clear()
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            if id_eleve in elevesIds:
                self.whoComboBox.addItem(eleveName, id_eleve)
        self.whoComboBox.addItem(QtWidgets.QApplication.translate('main', 'GROUP'), -1)
        self.whoComboBox.addItem(QtWidgets.QApplication.translate('main', 'ALL'), -2)
        self.whoComboBox.setCurrentIndex(0)

    def whoChanged(self, index):
        """
        Changement de l'élève dans la vue consult.
        """
        if self.model.what != 'consult':
            return
        index = self.whoComboBox.currentIndex()
        if index < 0:
            return
        id_eleve = self.whoComboBox.itemData(index)
        self.model.setTable(what='consult', id_eleve=id_eleve)
        self.elevesSuivisView.setColumnHidden(0, True)
        self.elevesSuivisView.resizeColumnsToContents()
        self.elevesSuivisView.resizeRowsToContents()
        self.elevesSuivisView.setAlternatingRowColors(True)
        for row in range(self.model.rowCount()):
            if self.model.rows[row][1]['value'] == 'HSEPARATOR':
                self.elevesSuivisView.setRowHeight(row, 10)

    def chronologicalChanged(self):
        """
        afficher ou pas dans l'ordre chronologique
        """
        self.model.chronological = self.chronologicalCheckBox.isChecked()
        index = self.whoComboBox.currentIndex()
        if index < 0:
            return
        id_eleve = self.whoComboBox.itemData(index)
        self.model.setTable(what='consult', id_eleve=id_eleve)
        self.elevesSuivisView.setColumnHidden(0, True)
        self.elevesSuivisView.resizeColumnsToContents()
        self.elevesSuivisView.resizeRowsToContents()
        self.elevesSuivisView.setAlternatingRowColors(True)
        for row in range(self.model.rowCount()):
            if self.model.rows[row][1]['value'] == 'HSEPARATOR':
                self.elevesSuivisView.setRowHeight(row, 10)

    def saveDefine(self):
        # Sauvegarde et envoi des définitions des élèves suivis.
        result = saveGestElevesSuivi(self.main, self.model.rows)
        self.suivisModified = not(result)
        self.saveDefineButton.setEnabled(self.suivisModified)
        self.updateWhoComboBox()

    def loadConsult(self):
        # Téléchargement de la base suivi_xx.sqlite
        downloadSuivisPP(self.main)
        self.suivisPPDownloaded = True

    def whatChanged(self, index, mustRecreate=True):
        """
        Changement de vue via le comboBox whatComboBox.
        """
        if self.mustCancel:
            self.mustCancel = False
            return
        if not(self.testElevesSuivisModified()):
            self.mustCancel = True
            self.whatComboBox.setCurrentIndex(0)
            return
        index = self.whatComboBox.currentIndex()
        if index == 0:
            self.saveDefineButton.setVisible(True)
            self.whoComboBox.setVisible(False)
            self.chronologicalCheckBox.setVisible(False)
            self.loadConsultButton.setVisible(False)
            self.model.setTable('define')
        elif index == 1:
            self.saveDefineButton.setVisible(False)
            self.whoComboBox.setVisible(True)
            self.chronologicalCheckBox.setVisible(True)
            self.loadConsultButton.setVisible(True)
            if not(self.suivisPPDownloaded):
                message = utils_functions.u('<p><b>{0}</b></p>').format(
                    QtWidgets.QApplication.translate(
                        'main', 'Would you download the base?'))
                reponse = utils_functions.messageBox(
                    self.main, level='warning', message=message,
                    buttons=['Yes', 'No'], 
                    defaultButton=QtWidgets.QMessageBox.Yes)
                if reponse == QtWidgets.QMessageBox.Yes:
                    downloadSuivisPP(self.main)
                self.suivisPPDownloaded = True
            id_eleve = self.whoComboBox.itemData(self.whoComboBox.currentIndex())
            self.model.setTable(what='consult', id_eleve=id_eleve)
        self.elevesSuivisView.setColumnHidden(0, True)
        self.elevesSuivisView.resizeColumnsToContents()
        self.elevesSuivisView.resizeRowsToContents()
        self.elevesSuivisView.setAlternatingRowColors(True)
        self.whoComboBox.setCurrentIndex(0)
        if self.model.what == 'consult':
            for row in range(self.model.rowCount()):
                if self.model.rows[row][1]['value'] == 'HSEPARATOR':
                    self.elevesSuivisView.setRowHeight(row, 10)

    def suivis2ods(self):
        """
        Export des définitions et de toutes les vues en un fichier ods.
        """
        odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u('{0}/suivis.ods').format(self.main.workDir)
        odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.main.workDir = QtCore.QFileInfo(fileName).absolutePath()
        OK = False
        utils_functions.doWaitCursor()
        exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
        try:
            # d'abord la table define :
            what = 'define'
            columns, rows = calcDefine(self.main)
            exportDatas = utils_export.what2dic(exportDatas, what, columns, rows)

            # récupération des élèves :
            eleves = {}
            elevesIds = []
            elevesInGroup = prof.listStudentsInGroup(self.main, id_groupe=self.main.id_groupe)
            # on récupère la liste des id_eleve dans la table profxx.suivi_eleve_cpt :
            commandLine_my = utils_db.q_selectAllFrom.format('suivi_eleve_cpt')
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                if not(id_eleve in elevesIds):
                    elevesIds.append(id_eleve)
            # on remplit :
            for (id_eleve, ordre, eleveName) in elevesInGroup:
                if id_eleve in elevesIds:
                    eleves[id_eleve] = eleveName
            elevesIds.append(-1)
            elevesIds.append(-2)
            eleves[-1] = QtWidgets.QApplication.translate('main', 'GROUP')
            eleves[-2] = QtWidgets.QApplication.translate('main', 'ALL')

            for id_eleve in elevesIds:
                what = eleves[id_eleve]
                columns, rows = calcConsult(self.main, id_eleve, self.model.chronological)
                exportDatas = utils_export.what2dic(exportDatas, what, columns, rows)

            # on exporte en ods :
            utils_functions.restoreCursor()
            utils_export.exportDic2ods(self.main, exportDatas, fileName, msgFin=False)
            utils_functions.afficheMsgFinOpen(self.main, fileName)
            utils_functions.doWaitCursor()
            OK = True
        except:
            utils_functions.afficheMsgPb(self.main)
        finally:
            utils_functions.restoreCursor()
            return OK


    def suivis2pdf(self):
        """
        Export de la vue en un fichier pdf.
        """
        pdfTitle = QtWidgets.QApplication.translate('main', 'pdf File')
        if self.whatComboBox.currentIndex() == 0:
            what = QtWidgets.QApplication.translate('main', 'defines')
        else:
            what = self.whoComboBox.currentText()
        proposedName = utils_functions.u(
            '{0}/suivis-{1}.pdf').format(self.main.workDir, what)
        pdfExt = QtWidgets.QApplication.translate('main', 'pdf files (*.pdf)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, pdfTitle, proposedName, pdfExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.main.workDir = QtCore.QFileInfo(fileName).absolutePath()
        self.suivis2print(fileName=fileName)

    def suivis2print(self, fileName='', msgFin=True):
        OK = False
        utils_functions.doWaitCursor()
        printer = utils_export.createPrinter(fileName)
        if printer == False:
            utils_functions.restoreCursor()
            return OK
        exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
        try:
            # d'abord la table define :
            if self.whatComboBox.currentIndex() == 0:
                what = QtWidgets.QApplication.translate('main', 'defines')
            else:
                what = self.whoComboBox.currentText()
            exportDatas = utils_export.what2dic(
                exportDatas, what, self.model.columns, self.model.rows)
            # on imprime ou exporte en pdf :
            htmlFileName = utils_export.exportDic2html(self.main, exportDatas)
            utils_export.htmlToPrinter(self.main, htmlFileName, printer, fileName)
            OK = True
            if msgFin:
                utils_functions.afficheMsgFin(self.main, timer=5)
        except:
            utils_functions.afficheMsgPb(self.main)
        finally:
            utils_functions.restoreCursor()
            return OK
















def calcDefine(main):
    """
    Calcul de la table de définition des élèves suivis (define).
    """
    utils_functions.doWaitCursor()
    columns = [{'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]
    rows = []
    try:
        # on récupère la liste des compétences :
        commandLine_commun = utils_db.q_selectAllFrom.format('suivi')
        query_commun = utils_db.queryExecute(commandLine_commun, db=main.db_commun)
        while query_commun.next():
            id_suivi = int(query_commun.value(0))
            code = query_commun.value(1)
            competence = query_commun.value(5)
            column = {'id':id_suivi, 'value': code, 'toolTip':competence}
            if competence != '':
                columns.append(column)
        # et la liste des élèves :
        elevesInGroup = prof.listStudentsInGroup(main, id_groupe=main.id_groupe)
        # on récupère la table profxx.suivi_eleve_cpt :
        elevesCpts = {}
        query_my = utils_db.query(main.db_my)
        commandLine_my = utils_db.q_selectAllFrom.format('suivi_eleve_cpt')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(0))
            id_cpt = int(query_my.value(1))
            label2 = query_my.value(2)
            elevesCpts[(id_eleve, id_cpt)] = label2
        # enfin on remplit la table :
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            ligne = [{'value':eleveName, 'align': 'left'}]
            for cpt in columns[1:]:
                id_cpt = cpt['id']
                if (id_eleve, id_cpt) in elevesCpts:
                    value = 'X'
                    label2 = elevesCpts[(id_eleve, id_cpt)]
                else:
                    value = ''
                    label2 = ''
                if label2 != '':
                    ligne.append({'value': value, 
                        'toolTip':utils_functions.u('{0}\n{1} ({2})\n{3}').format(eleveName, cpt['value'], cpt['toolTip'], label2), 
                        'id_eleve':id_eleve, 'id_cpt':id_cpt, 'label2':label2, 'bold': True})
                else:
                    ligne.append({'value': value, 
                        'toolTip':utils_functions.u('{0}\n{1} ({2})').format(eleveName, cpt['value'], cpt['toolTip']), 
                        'id_eleve':id_eleve, 'id_cpt':id_cpt, 'bold': True})
            rows.append(ligne)

    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcConsult(main, id_eleveConsult=-999, chronological=False):
    """
    Calcul de la table de consultation des évaluations (consult).
    Il y a 3 cas :
        * pour un élève
        * pour le groupe
        * on affiche tout
    """
    columns = []
    rows = []
    if id_eleveConsult == -999:
        return columns, rows
    utils_functions.doWaitCursor()
    try:
        if id_eleveConsult == -2:
            affichage = 'ALL'
        elif id_eleveConsult == -1:
            affichage = 'GROUP'
        else:
            affichage = 'ELEVE'

        # les querys dont on a besoin :
        query_commun = utils_db.query(main.db_commun)
        query_my = utils_db.query(main.db_my)
        query_users = utils_db.query(main.db_users)

        # récupération des profs :
        profs = {}
        commandLine_users = utils_db.q_selectAllFrom.format('profs')

        query_users = utils_db.queryExecute(
            commandLine_users, query=query_users)
        while query_users.next():
            id_prof = int(query_users.value(0))
            nom = query_users.value(1)
            prenom = query_users.value(2)
            profs[id_prof] = utils_functions.u('{0} {1}').format(nom, prenom)

        # récupération des élèves :
        eleves = {}
        elevesIds = []
        elevesInGroup = prof.listStudentsInGroup(main, id_groupe=main.id_groupe)
        # on récupère la liste des id_eleve dans la table profxx.suivi_eleve_cpt :
        commandLine_my = utils_db.q_selectAllFrom.format('suivi_eleve_cpt')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(0))
            if not(id_eleve in elevesIds):
                elevesIds.append(id_eleve)
        # on remplit :
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            if id_eleve in elevesIds:
                eleves[id_eleve] = eleveName
        elevesIds.append(-1)
        eleves[-1] = QtWidgets.QApplication.translate('main', 'GROUP')

        # récupération des horaires (table commun.horaires) :
        horaires = {}
        commandLine_commun = utils_db.q_selectAllFromOrder.format('horaires', 'id')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            id_horaire = int(query_commun.value(0))
            name = query_commun.value(1)
            label = query_commun.value(2)
            horaires[id_horaire] = (name, label)

        # on récupère la table profxx.suivi_eleve_cpt :
        elevesSuivis = []
        elevesCpts = {}
        cptsSuivis = []
        cptsLabel2 = {}
        if affichage == 'ALL':
            commandLine_my = utils_db.q_selectAllFrom.format('suivi_eleve_cpt')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                id_cpt = int(query_my.value(1))
                label2 = query_my.value(2)
                elevesSuivis.append((id_eleve, id_cpt, label2))
                if id_eleve in elevesCpts:
                    elevesCpts[id_eleve].append(id_cpt)
                else:
                    elevesCpts[id_eleve] = [id_cpt]
                if not(id_cpt in cptsSuivis):
                    cptsSuivis.append(id_cpt)
                    cptsLabel2[(id_eleve, id_cpt)] = label2
        elif affichage == 'GROUP':
            commandLine_my = utils_db.q_selectAllFrom.format('suivi_eleve_cpt')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_cpt = int(query_my.value(1))
                label2 = query_my.value(2)
                if not(id_cpt in cptsSuivis):
                    cptsSuivis.append(id_cpt)
                    cptsLabel2[id_cpt] = ''
        else:
            commandLine_my = ('SELECT * FROM suivi_eleve_cpt '
                'WHERE id_eleve={0}').format(id_eleveConsult)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_cpt = int(query_my.value(1))
                label2 = query_my.value(2)
                cptsSuivis.append(id_cpt)
                cptsLabel2[id_cpt] = label2

        # les titres des colonnes :
        cptsSuivisColumns = []
        if affichage == 'ALL':
            columns = [{'value': '', 'columnWidth': '0.5cm', 'NO_EXPORT':True}]
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Date')})
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Horaire')})
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Prof'),
                            'columnWidth': '7.0cm'})
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Subject'),
                            'columnWidth': '4.0cm'})
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Student'), 
                            'columnWidth': '7.0cm'})
            # récupération des compétences suivies :
            commandLine_commun = utils_db.q_selectAllFrom.format('suivi')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                id_cpt = int(query_commun.value(0))
                if id_cpt in cptsSuivis:
                    code = query_commun.value(1)
                    toolTip = query_commun.value(5)
                    column = {'id':id_cpt, 'value': code, 'toolTip':toolTip}
                    columns.append(column)
                    cptsSuivisColumns.append((id_cpt, code, toolTip))
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Remark'),
                            'columnWidth': '10.0cm'})
        else:
            columns = [{'value': '', 'columnWidth': '0.5cm', 'NO_EXPORT':True}]
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Date')})
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Horaire')})
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Prof'),
                            'columnWidth': '7.0cm'})
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Subject'),
                            'columnWidth': '4.0cm'})
            # récupération des compétences suivies :
            commandLine_commun = utils_db.q_selectAllFrom.format('suivi')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                id_cpt = int(query_commun.value(0))
                if id_cpt in cptsSuivis:
                    code = query_commun.value(1)
                    toolTip = query_commun.value(5)
                    label2 = cptsLabel2[id_cpt]
                    if label2 != '':
                        toolTip = utils_functions.u('{0}\n{1}').format(toolTip, label2)
                    column = {'id':id_cpt, 'value': code, 'toolTip':toolTip}
                    columns.append(column)
                    cptsSuivisColumns.append((id_cpt, code, toolTip))
            columns.append({'value': QtWidgets.QApplication.translate('main', 'Remark'),
                            'columnWidth': '10.0cm'})

        # commande de sélection des données :
        if chronological :
            order = ''
        else:
            order = ' DESC'
        if affichage == 'ALL':
            commandLine_suivixx = (
                'SELECT * FROM suivi_evals '
                'ORDER BY date{0}, horaire, id_prof, matiere, id_eleve').format(
                    order)
        else:
            commandLine_suivixx = (
                'SELECT * FROM suivi_evals '
                'WHERE id_eleve={0} '
                'ORDER BY date{1}, horaire, id_prof, matiere').format(
                    id_eleveConsult, order)

        # on récupère les données des lignes à écrire :
        lignesBases = []
        lignesDatas = {}
        # on ouvre la base suivi_xx :
        id_pp = main.me['userId']
        theFileName = 'suivi_{0}.sqlite'.format(id_pp)
        if (utils.lanConfig and utils.lanConfigDir != ''):
            destDir = utils.lanConfigDir
        else:
            destDir = main.localConfigDir
        theFileName = utils_functions.u(
            '{0}/{1}/{2}').format(
                destDir, main.actualVersion['versionName'], theFileName)
        (db_suivixx, dbName) = utils_db.createConnection(main, theFileName)
        query_suivixx = utils_db.queryExecute(commandLine_suivixx, db=db_suivixx)
        while query_suivixx.next():
            id_prof = int(query_suivixx.value(0))
            matiereName = query_suivixx.value(1)
            #id_pp = int(query_suivixx.value(2))
            id_eleve = int(query_suivixx.value(3))
            date = query_suivixx.value(4)
            horaire = query_suivixx.value(5)
            id_cpt = int(query_suivixx.value(6))
            value = query_suivixx.value(7)
            if not((date, horaire, id_prof, matiereName, id_eleve) in lignesBases):
                lignesBases.append((date, horaire, id_prof, matiereName, id_eleve))
                lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)] = {id_cpt: value}
            else:
                lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)][id_cpt] = value
        # enfin on referme la base suivixx :
        db_suivixx.close()
        del db_suivixx
        utils_db.sqlDatabase.removeDatabase(dbName)

        # on peut remplir les lignes :
        if affichage == 'ALL':
            # c'est plus compliqué car il faut réordonner les élèves
            # et insérer des lignes vides
            evalsList, evalsDic = [], {}
            for (date, horaire, id_prof, matiereName, id_eleve) in lignesBases:
                if (date, horaire, id_prof, matiereName) in evalsList:
                    evalsDic[(date, horaire, id_prof, matiereName)].append(id_eleve)
                else:
                    evalsList.append((date, horaire, id_prof, matiereName))
                    evalsDic[(date, horaire, id_prof, matiereName)] = [id_eleve]
            lignesBases = []
            for (date, horaire, id_prof, matiereName) in evalsList:
                newElevesList = []
                for id_eleve in elevesIds:
                    if id_eleve in evalsDic[(date, horaire, id_prof, matiereName)]:
                        newElevesList.append(id_eleve)
                for id_eleve in newElevesList:
                    if not((date, horaire, id_prof, matiereName, id_eleve) in lignesBases):
                        lignesBases.append((date, horaire, id_prof, matiereName, id_eleve))
                lignesBases.append(('HSEPARATOR', -1, -1, '', -1))
            for (date, horaire, id_prof, matiereName, id_eleve) in lignesBases:
                if date == 'HSEPARATOR':
                    ligne = [{'value': ''}]
                    for i in range(5):
                        ligne.append({'value': 'HSEPARATOR'})
                    for (id_cpt, code, toolTip) in cptsSuivisColumns:
                        ligne.append({'value': 'HSEPARATOR'})
                    ligne.append({'value': 'HSEPARATOR'})
                else:
                    ligne = [{'value': ''}]
                    ligne.append({'value': date})
                    ligne.append({'value':horaires[horaire][0], 
                        'toolTip':horaires[horaire][1], 'id_horaire':horaire})
                    ligne.append({'value': profs[id_prof], 'id_prof':id_prof, 'align': 'left'})
                    ligne.append({'value': matiereName, 'align': 'left'})
                    if id_eleve == -1:
                        ligne.append({'value':eleves[id_eleve], 'id_eleve':id_eleve, 
                            'align': 'center', 'bold': True})
                    else:
                        ligne.append({'value':eleves[id_eleve], 'id_eleve':id_eleve, 
                            'align': 'left'})
                    for (id_cpt, code, toolTip) in cptsSuivisColumns:
                        value = ''
                        color = utils.colorWhite
                        label2 = ''
                        if id_cpt in lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)]:
                            value = lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)][id_cpt]
                            color = utils_functions.findColor(value, mustCalculIfMany=False)
                            value = utils_functions.valeur2affichage(value)
                        elif id_eleve in elevesCpts:
                            if not(id_cpt in elevesCpts[id_eleve]):
                                color = utils.colorLightGray
                        if (id_eleve, id_cpt) in cptsLabel2:
                            label2 = cptsLabel2[(id_eleve, id_cpt)]
                        ligne.append({'value': value, 'color':color, 'label2':label2, 'bold': True})
                    value = ''
                    if -1 in lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)]:
                        value = lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)][-1]
                    ligne.append({'value': value, 'align': 'left'})
                rows.append(ligne)
        else:
            dateEx= ''
            for (date, horaire, id_prof, matiereName, id_eleve) in lignesBases:
                if dateEx == '':
                    dateEx = date
                if date != dateEx:
                    dateEx = date
                    ligne = [{'value': ''}]
                    for i in range(4):
                        ligne.append({'value': 'HSEPARATOR'})
                    for (id_cpt, code, toolTip) in cptsSuivisColumns:
                        ligne.append({'value': 'HSEPARATOR'})
                    ligne.append({'value': 'HSEPARATOR'})
                    rows.append(ligne)
                ligne = [{'value': ''}]
                ligne.append({'value': date})
                ligne.append({'value':horaires[horaire][0], 
                    'toolTip':horaires[horaire][1], 'id_horaire':horaire})
                ligne.append({'value': profs[id_prof], 'id_prof':id_prof, 'align': 'left'})
                ligne.append({'value': matiereName, 'align': 'left'})
                for (id_cpt, code, toolTip) in cptsSuivisColumns:
                    value = ''
                    if id_cpt in lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)]:
                        value = lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)][id_cpt]
                        color = utils_functions.findColor(value, mustCalculIfMany=False)
                        value = utils_functions.valeur2affichage(value)
                    else:
                        color = utils.colorWhite
                    ligne.append({'value': value, 'color':color, 'bold': True})
                value = ''
                if -1 in lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)]:
                    value = lignesDatas[(date, horaire, id_prof, matiereName, id_eleve)][-1]
                ligne.append({'value': value, 'align': 'left'})
                rows.append(ligne)

    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()
        return columns, rows



class SuivisTableModel(QtCore.QAbstractTableModel):
    """
    le model de table
    """
    def __init__(self, main=None):
        super(SuivisTableModel, self).__init__(main)
        self.main = main
        self.rows = []
        self.columns = []
        self.what = 'define'
        self.id_eleve = -999
        self.boldFont = QtGui.QFont()
        self.boldFont.setBold(True)
        self.alignLeft = int(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.alignCenter = int(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        self.alignRight = int(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.noChangeFlag = QtCore.Qt.ItemIsSelectable
        self.chronological = False

    def setTable(self, what='define', id_eleve=-999):
        self.what = what
        self.beginResetModel()
        try:
            if what == 'define':
                self.columns, self.rows = calcDefine(self.main)
                self.previousSection = -1
            elif what == 'consult':
                self.columns, self.rows = calcConsult(self.main, id_eleve, self.chronological)
                self.id_eleve = id_eleve
                self.previousSection = -1
        finally:
            self.endResetModel()

    def rowCount(self, index=QtCore.QModelIndex()):
        return len(self.rows)

    def columnCount(self, index=QtCore.QModelIndex()):
        return len(self.columns)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        actualCell = self.rows[index.row()][index.column()]
        if role == QtCore.Qt.TextAlignmentRole:
            align = actualCell.get('align', 'center')
            if align == 'center':
                return self.alignCenter
            elif align == 'right':
                return self.alignRight
            else:
                return self.alignLeft
        elif role == QtCore.Qt.DisplayRole:
            if actualCell['value'] == 'HSEPARATOR':
                return ''
            else:
                return actualCell['value']
        elif role == QtCore.Qt.BackgroundRole:
            if self.what == 'consult':
                if 'color' in actualCell:
                    return actualCell['color']
                elif actualCell['value'] == 'HSEPARATOR':
                    return utils.colorGray
        elif role == QtCore.Qt.ToolTipRole:
            message = ''
            if (self.what == 'consult') and ('color' in actualCell):
                if 'label2' in actualCell:
                    message = actualCell['label2']
            elif 'toolTip' in actualCell:
                message = actualCell['toolTip']
            return utils_functions.u(message)
        elif role == QtCore.Qt.FontRole:
            if 'bold' in actualCell:
                return self.boldFont
        else:
            return

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.TextAlignmentRole:
            if orientation == QtCore.Qt.Horizontal:
                return self.alignCenter
            return self.alignLeft
        elif role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self.columns[section]['value']
            else:
                return self.rows[section][0]['value']
        elif role == QtCore.Qt.ToolTipRole:
            if (orientation == QtCore.Qt.Horizontal) and ('toolTip' in self.columns[section]):
                return utils_functions.u(self.columns[section]['toolTip'])
        else:
            return

    def flags(self, index):
        return self.defaultFlag



class SuivisTableView(QtWidgets.QTableView):
    """
    On dérive d'un QTableView pour adapter le double-clic 
    et le menu pop-up
    """
    def __init__(self, main=None):
        super(SuivisTableView, self).__init__(main)
        self.main = main
        # les actions du menu pop-up :
        # sélection/déselection de la case :
        textSelect = QtWidgets.QApplication.translate('main', 'Select')
        self.actionSelect = QtWidgets.QAction(
            textSelect, 
            self, 
            icon=utils.doIcon('edit-copy'))
        self.actionSelect.setData(1)
        self.actionSelect.triggered.connect(self.doPopUpMenu)
        textDeselect = QtWidgets.QApplication.translate('main', 'Deselect')
        self.actionDeselect = QtWidgets.QAction(
            textDeselect, 
            self, 
            icon=utils.doIcon('edit-copy'))
        self.actionDeselect.setData(2)
        self.actionDeselect.triggered.connect(self.doPopUpMenu)
        # édition du label2 :
        textChangeLabel2 = QtWidgets.QApplication.translate('main', 'ChangeLabel2')
        self.actionChangeLabel2 = QtWidgets.QAction(
            textChangeLabel2, 
            self, 
            icon=utils.doIcon('edit-copy'))
        self.actionChangeLabel2.setData(10)
        self.actionChangeLabel2.triggered.connect(self.doPopUpMenu)

    def mouseDoubleClickEvent(self, event):
        if self.main.model.what != 'define':
            return
        # il faut qu'il y ait une sélection :
        if len(self.selectedIndexes()) < 1:
            return
        item = self.selectedIndexes()[0]
        row, column = item.row(), item.column()
        value = self.main.model.rows[row][column]['value']
        if value == '':
            value = 'X'
        else:
            value = ''
        self.main.model.rows[row][column]['value'] = value
        self.main.suivisModified = True
        self.main.saveDefineButton.setEnabled(True)

    def contextMenuEvent(self, event):
        if self.main.model.what != 'define':
            return
        # il faut qu'il y ait une sélection :
        if len(self.selectedIndexes()) < 1:
            return
        # création et remplissage du menu :
        menu = QtWidgets.QMenu(self)
        menu.addAction(self.actionSelect)
        menu.addAction(self.actionDeselect)
        menu.addSeparator()
        menu.addAction(self.actionChangeLabel2)
        # on affiche le menu :
        menu.exec_(event.globalPos())

    def doPopUpMenu(self):
        action = self.sender()
        index = action.data()
        if index in (1, 2):
            for item in self.selectedIndexes():
                row, column = item.row(), item.column()
                if index == 1:
                    value = 'X'
                else:
                    value = ''
                self.main.model.rows[row][column]['value'] = value
            self.main.suivisModified = True
            self.main.saveDefineButton.setEnabled(True)
        elif index == 10:
            item = self.selectedIndexes()[0]
            row, column = item.row(), item.column()
            if 'label2' in self.main.model.rows[row][column]:
                label2 = self.main.model.rows[row][column]['label2']
            else:
                label2 = ''
            newLabel2, ok = QtWidgets.QInputDialog.getText(self.main, 
                                utils.PROGNAME, 
                                QtWidgets.QApplication.translate('main', 'NewLabel2:'),
                                QtWidgets.QLineEdit.Normal,
                                label2)
            if ok:
                for item in self.selectedIndexes():
                    row, column = item.row(), item.column()
                    self.main.model.rows[row][column]['label2'] = newLabel2
                    if 'toolTip' in self.main.model.rows[row][column]:
                        toolTip = self.main.model.rows[row][column]['toolTip']
                        toolTip = toolTip.split('\n')[:2]
                        toolTip = utils_functions.u('{0}\n{1}').format(toolTip[0], toolTip[1])
                        if newLabel2 != '':
                            toolTip = utils_functions.u('{0}\n{1}').format(toolTip, newLabel2)
                        self.main.model.rows[row][column]['toolTip'] = toolTip
                self.main.suivisModified = True
                self.main.saveDefineButton.setEnabled(True)





