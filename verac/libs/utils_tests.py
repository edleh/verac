# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    juste des procedures pour faire des tests.
    Ne sont utilisées que pour le développement.
"""


# importation des modules utiles :
from __future__ import division, print_function
import sys
import os

import utils, utils_functions, utils_db, utils_filesdirs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



def printMenus(main, what=None, last=''):
    if what == None:
        what = main.allMenu
    if last != '':
        last = last + ' → '

    if isinstance(what, QtWidgets.QMenu):
        last = last + what.title()
        print(last)
        for item in what.actions():
            printMenus(main, what=item, last=last)
    elif isinstance(what, QtWidgets.QMenuBar):
        print('>>> QMenuBar:')
        for item in what.actions():
            printMenus(main, what=item, last=last)
    elif isinstance(what, QtWidgets.QWidgetAction):
        print('>>> QWidgetAction:', what.defaultWidget().text() + '|' + what.defaultWidget().toolTip())
        """
        for item in what.children():
            printMenus(main, what=item, last=last)
        """
    elif isinstance(what, QtWidgets.QAction):
        if what.toolTip() != '':
            last = last + what.toolTip()
            print(last)
        elif what.text() != '':
            last = last + what.text()
            print(last)
        try:
            for item in what.menu().actions():
                printMenus(main, what=item, last=last)
        except:
            pass
    else:
        print('??:', what)



def readTextElement(xmlItem, tagName):
    element = xmlItem.elementsByTagName(tagName)
    return element.item(0).toElement().text()



def test(main, userMode):
    #printMenus(main)

    test, OK = QtWidgets.QInputDialog.getText(
        main, utils.PROGNAME, 
        QtWidgets.QApplication.translate('main', 'Test:'),
        QtWidgets.QLineEdit.Normal, '')
    if not(OK):
        return



    if userMode == 'admin':
        import admin

        if test == 'eleves5':
            # liste des élèves de 5°
            query_admin = utils_db.query(admin.db_admin)
            elevesList = []
            commandLine = 'SELECT * FROM eleves WHERE Classe LIKE "5%"'
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                elevesList.append(id_eleve)
            print(elevesList)

        elif test == 'profs':
            # état des évaluations des profs
            profs = {'ALL': {}, 'all': [], 'eleve_groupe': [], 'eleve_prof': [], 'persos': []}
            for (id_prof, file_prof, nom, prenom) in admin.PROFS:
                name = utils_functions.u('{0} {1}').format(nom, prenom)
                profs['ALL'][id_prof] = name
                profs['all'].append((id_prof, name))
            profs['all'].sort()
            admin.openDB(main, 'resultats')
            query_resultats = utils_db.query(admin.db_resultats)
            commandLine = (
                'SELECT DISTINCT id_prof FROM {0} '
                'ORDER BY id_prof')
            for what in ('eleve_groupe', 'eleve_prof', 'persos'):
                #print('***********************************************')
                #print(what)
                query_resultats = utils_db.queryExecute(
                    commandLine.format(what), query=query_resultats)
                while query_resultats.next():
                    id_prof = int(query_resultats.value(0))
                    profs[what].append((id_prof, profs['ALL'][id_prof]))
                    #print(id_prof, profs['ALL'][id_prof])
                #print(len(profs[what]))
            print('***********************************************')
            temp = [prof for prof in profs['all'] if not(prof in profs['eleve_groupe'])]
            print('RIEN :', len(temp))
            for prof in temp:
                print(prof)
            print('***********************************************')
            temp = [prof for prof in profs['eleve_groupe'] if not(prof in profs['eleve_prof'])]
            print('GROUPES MAIS PAS DE TABLEAU :', len(temp))
            for prof in temp:
                print(prof)
            print('***********************************************')
            temp = [prof for prof in profs['eleve_prof'] if not(prof in profs['persos'])]
            print('PAS DE PERSOS :', len(temp))
            for prof in temp:
                print(prof)
            print('***********************************************')
            print('PERSOS :', len(profs['persos']))
            for prof in profs['persos']:
                print(prof)
            print('***********************************************')

        elif test == 'ulis':
            profs = {}
            for (id_prof, file_prof, nom, prenom) in admin.PROFS:
                profs[id_prof] = utils_functions.u('{0} {1}').format(nom, prenom)
            admin.openDB(main, 'resultats')
            query_resultats = utils_db.query(admin.db_resultats)
            temp = []
            commandLine = (
                'SELECT '
                'eleve_groupe.id_eleve, eleves.Classe, eleves.NOM_Prenom, '
                'eleve_groupe.Matiere, eleve_groupe.groupeName, eleve_groupe.id_prof '
                'FROM eleve_groupe '
                'JOIN eleves ON eleves.id_eleve=eleve_groupe.id_eleve '
                'WHERE eleve_groupe.id_eleve IN (5840, 5832, 5804, 5808, 5818, 5406, 5846) '
                'ORDER BY eleve_groupe.Matiere, eleves.Classe, eleve_groupe.id_eleve, eleve_groupe.id_prof')
            query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
            while query_resultats.next():
                id_eleve = int(query_resultats.value(0))
                classe = query_resultats.value(1)
                eleve = query_resultats.value(2)
                matiere = query_resultats.value(3)
                groupeName = query_resultats.value(4)
                id_prof = int(query_resultats.value(5))
                temp.append((id_eleve, classe, eleve, matiere, groupeName, id_prof, profs[id_prof]))
                #print(id_eleve, classe, eleve, matiere, groupeName, id_prof, profs[id_prof])
            # on crée le fichier csv :
            import csv
            fileName = QtWidgets.QFileDialog.getSaveFileName(
                main, 
                QtWidgets.QApplication.translate('main', 'Open csv File'), 
                utils_functions.u('{0}/ulis.csv').format(main.workDir), 
                QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
            fileName = utils_functions.verifyLibs_fileName(fileName)
            if fileName == '':
                return
            if utils.PYTHONVERSION >= 30:
                theFile = open(fileName, 'w', newline='', encoding='utf-8')
            else:
                theFile = open(fileName, 'wb')
            writer = csv.writer(theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
            # on inscrit la ligne de titres :
            champsTitres = ['id_eleve', 'classe', 'eleve', 'matiere', 'groupe', 'id_prof', 'prof']
            writer.writerow(champsTitres)
            # on inscrit les lignes :
            for line in temp:
                writer.writerow(line)
            theFile.close()

        elif test == 'ulis2':
            eleves = []
            admin.openDB(main, 'recup_evals')
            query_recupEvals = utils_db.query(admin.db_recupEvals)
            commandLine = (
                'SELECT * FROM lsu '
                'WHERE lsuWhat = "accompagnement" '
                'AND lsu2 = "ULIS"')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(2))
                eleves.append(id_eleve)
            eleves = utils_functions.array2string(eleves)
            print(eleves)
            matieres = '"FRA", "MATH", "HG", "ANG", "ESP", "ALL"'
            profs = {}
            for (id_prof, file_prof, nom, prenom) in admin.PROFS:
                profs[id_prof] = utils_functions.u('{0} {1}').format(nom, prenom)
            temp = []
            admin.openDB(main, 'resultats')
            query_resultats = utils_db.query(admin.db_resultats)
            commandLine = (
                'SELECT DISTINCT '
                'eleves.id_eleve, eleves.NOM_Prenom, eleves.Classe, '
                'eleve_prof.*, eleve_groupe.* '
                'FROM eleves '
                'LEFT JOIN eleve_prof ON eleve_prof.id_eleve=eleves.id_eleve '
                'LEFT JOIN eleve_groupe '
                'ON eleve_groupe.id_eleve=eleve_prof.id_eleve '
                'AND eleve_groupe.id_prof=eleve_prof.id_prof '
                'AND eleve_groupe.Matiere=eleve_prof.Matiere '
                'WHERE eleves.id_eleve IN ({0}) '
                'AND eleve_prof.Matiere IN ({1}) '
                #'AND eleve_prof.id_prof != 10600 '
                'ORDER BY eleves.Classe, eleves.NOM_Prenom, eleve_prof.Matiere ').format(eleves, matieres)
            query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
            while query_resultats.next():
                id_eleve = int(query_resultats.value(0))
                eleve = query_resultats.value(1)
                classe = query_resultats.value(2)
                id_prof = int(query_resultats.value(3))
                matiere = query_resultats.value(5)
                temp.append((id_eleve, eleve, classe, matiere, id_prof, profs[id_prof]))
                print(id_eleve, eleve, classe, matiere, id_prof, profs[id_prof])

        elif test == 'lsu':
            badStudents = []
            admin.openDB(main, 'resultats')
            query_resultats = utils_db.query(admin.db_resultats)
            temp = {'OK': {}, 'BAD': {}}
            commandLine = (
                'SELECT lsu.*, eleves.* '
                'FROM lsu '
                'JOIN eleves ON eleves.id_eleve=lsu.id_eleve '
                'WHERE lsu.lsuWhat="socle"')
            query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
            while query_resultats.next():
                id_eleve = int(query_resultats.value(1))
                lsu1 = query_resultats.value(2)
                lsu2 = query_resultats.value(3)
                eleve = query_resultats.value(9)
                classe = query_resultats.value(8)
                if lsu2 in ('A', 'B', 'C', 'D'):
                    if not (id_eleve in temp['OK']):
                        temp['OK'][id_eleve] = [classe, eleve]
                    temp['OK'][id_eleve].append(lsu1)
            for id_eleve in temp['OK']:
                if len(temp['OK'][id_eleve]) < 10:
                    badStudent = utils_functions.u('{0} {1} : ').format(
                        temp['OK'][id_eleve][0], temp['OK'][id_eleve][1])
                    for cpt in utils.LSU['SOCLE_COMPONENTS']['ORDER_LSU']:
                        if not(cpt in temp['OK'][id_eleve]):
                            badStudent = utils_functions.u('{0}{1}, ').format(badStudent, cpt)
                    temp['BAD'][id_eleve] = badStudent
                    badStudents.append(badStudent)
            badStudents.sort()
            for badStudent in badStudents:
                print(badStudent)

        elif test == 'cp':
            admin.openDB(main, 'resultats')
            query_resultats = utils_db.query(admin.db_resultats)
            temp = {'ALL': {}, 'CP_ONLY': {}, 'SOCLE_ONLY': {}, 'OK': {}}
            commandLine = (
                'SELECT DISTINCT id_prof, matiere, name '
                'FROM bilans_details '
                'WHERE name IN ("S2D1O1", "S2D1O3", "S2D3O2", "S2D2O1", "S2D2O2", "S2D4O1", "S2D2O4", "S2D3O4") '
                'OR name LIKE "CP_%" '
                'ORDER BY id_prof, matiere, name')
            query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
            while query_resultats.next():
                id_prof = int(query_resultats.value(0))
                matiere = query_resultats.value(1)
                name = query_resultats.value(2)
                eleve = query_resultats.value(9)
                if (id_prof, matiere) in temp['ALL']:
                    temp['ALL'][(id_prof, matiere)].append(name)
                else:
                    temp['ALL'][(id_prof, matiere)] = [name, ]
            for (id_prof, matiere) in temp['ALL']:
                cp = [x for x in temp['ALL'][(id_prof, matiere)] if x[:3] == 'CP_']
                socle = [x for x in temp['ALL'][(id_prof, matiere)] if x[:2] == 'S2']
                if len(cp) < 1:
                    temp['SOCLE_ONLY'][(id_prof, matiere)] = temp['ALL'][(id_prof, matiere)]
                elif len(socle) < 1:
                    temp['CP_ONLY'][(id_prof, matiere)] = temp['ALL'][(id_prof, matiere)]
                else:
                    temp['OK'][(id_prof, matiere)] = temp['ALL'][(id_prof, matiere)]
            for what in ('OK', 'SOCLE_ONLY', 'CP_ONLY'):
                print('******************************************************')
                print(what)
                for (id_prof, matiere) in temp[what]:
                    print(id_prof, matiere, temp[what][(id_prof, matiere)])

        else:
            return









    else:
        import prof, prof_db

        if test == 'me':
            print(main.me)

        elif test == 'RANDOM_HELP_PAGES':
            for page in utils.RANDOM_HELP_PAGES:
                begin = '/home/pascal/Documents/Programmation/verac/TuxFamily/site/'
                if page in ('tech-calculs', ):
                    fileName = '{0}help-{1}.html'.format(begin, page)
                else:
                    fileName = '{0}help-prof-{1}.html'.format(begin, page)
                if QtCore.QFile(fileName).exists():
                    print('OK  :', fileName)
                else:
                    print('NON :', fileName)

        elif test == 'md5Sum':
            md5Sum = utils_filesdirs.md5Sum(main.dbFile_my)
            print('dbFile_my :', md5Sum)
            md5Sum = utils_filesdirs.md5Sum(main.dbFile_commun)
            print('dbFile_commun :', md5Sum)

        else:
            return




"""
JUSTE POUR GARDER LA COLORATION SYNTAXIQUE :
"""

