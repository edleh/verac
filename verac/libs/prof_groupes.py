# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Gestion des groupes et profils.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_2lists

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



###########################################################"
#        GESTION DES GROUPES
###########################################################"

def initGroupeMenu(main):
    """
    Pour remplir le menu de sélection du groupe
    """
    main.groupeMenuActions = []
    main.groupeMenu.clear()
    # d'abord l'entrée "Tous les groupes :
    allGroupsText = QtWidgets.QApplication.translate('main', 'All Groups')
    newAct = QtWidgets.QAction(allGroupsText, main, checkable=True)
    newAct.setData(('groupe', -1, allGroupsText, '', []))
    newAct.triggered.connect(main.selectionChanged)
    main.groupeMenu.addAction(newAct)
    main.groupeMenuActions.append(newAct)
    if main.id_groupe == -1:
        newAct.setChecked(True)
    textGroupToSelect = allGroupsText
    # puis la liste des groupes disponibles :
    query_my = utils_db.queryExecute(
        utils_db.q_selectAllFromOrder.format(
            'groupes', 'ordre, Matiere, UPPER(Name)'), 
        db=main.db_my)
    while query_my.next():
        id_groupe = int(query_my.value(0))
        name = query_my.value(1)
        matiereName = query_my.value(2)
        label = utils_functions.u('{0} ({1})').format(name, matiereName)
        newAct = QtWidgets.QAction(label, main, checkable=True)
        newAct.setData(('groupe', id_groupe, label, '', []))
        newAct.triggered.connect(main.selectionChanged)
        main.groupeMenu.addAction(newAct)
        main.groupeMenuActions.append(newAct)
        if id_groupe == main.id_groupe:
            textGroupToSelect = label
            newAct.setChecked(True)
    # enfin une entrée spéciale de création de groupe :
    main.groupeMenu.addSeparator()
    main.groupeMenu.addAction(main.actionCreateGroupe)
    # mise à jour du texte du bouton :
    main.groupeButton.setText(textGroupToSelect)



class GestGroupesDlg(QtWidgets.QDialog):
    """
    Pour éditer les groupes d'élèves
    Si doCreate=True, on ouvre directement la fenêtre de création d'un groupe
    """
    def __init__(self, parent=None, doCreate=False):
        super(GestGroupesDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate(
            'main', 'Manage groups of students')
        self.setWindowTitle(title)
        self.waiting = False

        # un dictionnaire pour récupérer plus rapidement les informations :
        self.groupes = {'ORDER': [], }
        try:
            if self.main.id_groupe > -1:
                self.id_groupe = self.main.id_groupe
                self.matiereName = matiereFromGroupe(self.main, self.id_groupe)
            else:
                self.id_groupe = 0
                self.matiereName = self.main.me['Matiere']
        except:
            self.main.id_groupe = -1
            self.id_groupe = 0
            self.matiereName = self.main.me['Matiere']
        self.oldData = (self.matiereName, self.id_groupe)

        # la liste des groupes :
        groupLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Group:')))
        self.groupesComboBox = QtWidgets.QComboBox()
        self.groupesComboBox.setMinimumWidth(300)
        self.groupesComboBox.activated.connect(self.groupesClicked)

        # les boutons pour ajouter, supprimer ou mettre à jour les groupes :
        self.addButton = QtWidgets.QPushButton(
            utils.doIcon('list-add'),
            QtWidgets.QApplication.translate('main', 'New'))
        self.addButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'To create a new group of students'))
        self.addButton.clicked.connect(self.createGroupe)
        self.editButton = QtWidgets.QPushButton(
            utils.doIcon('matiere-choose'),
            QtWidgets.QApplication.translate('main', 'Edit'))
        self.editButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Changes the selected group'))
        self.editButton.clicked.connect(self.editGroupe)
        self.removeButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'),
            QtWidgets.QApplication.translate('main', 'Delete'))
        self.removeButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Deletes the selected group and all associated tables and evaluations'))
        self.removeButton.clicked.connect(self.removeGroupe)
        self.updateButton = QtWidgets.QPushButton(
            utils.doIcon('database-update'),
            QtWidgets.QApplication.translate('main', 'Update'))
        self.updateButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 
            'Updates class groups from the download list.'
            '\nAttention you can not add feedback to students removed from the group.'))
        self.updateButton.clicked.connect(self.doUpdateAllGroups)

        # la sélection des élèves :
        self.selectionWidget = utils_2lists.ChooseEleveWidget(
            parent = self.main, withComboBox = True, withCheckBox=True)

        # les boutons de la fenêtre :
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # mise en place :
        vLayout_1 = QtWidgets.QVBoxLayout()
        vLayout_1.addWidget(groupLabel)
        vLayout_1.addWidget(self.groupesComboBox)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.addButton)
        hLayout.addWidget(self.editButton)
        hLayout.addWidget(self.removeButton)
        vLayout_2 = QtWidgets.QVBoxLayout()
        vLayout_2.addLayout(hLayout)
        vLayout_2.addWidget(self.updateButton)

        topLayout = QtWidgets.QHBoxLayout()
        topLayout.addLayout(vLayout_1)
        topLayout.addLayout(vLayout_2)
        topBox = QtWidgets.QGroupBox()
        topBox.setLayout(topLayout)
        topBox.setFlat(True)

        grid = QtWidgets.QGridLayout()
        grid.addWidget(topBox, 0, 0)
        grid.addWidget(self.selectionWidget, 1, 0)
        grid.addWidget(buttonBox, 3, 0)
        self.setLayout(grid)
        self.setWindowState(self.windowState() or QtCore.Qt.WindowMaximized)

        # on recherche les groupes existants :
        commandLine_my = utils_db.q_selectAllFromOrder.format(
            'groupes', 'ordre, Matiere, UPPER(Name)')
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_groupe = int(query_my.value(0))
            name = query_my.value(1)
            matiereName = query_my.value(2)
            isClasse = int(query_my.value(3))
            nom_classe = query_my.value(4)
            self.groupes['ORDER'].append(id_groupe)
            self.groupes[id_groupe] = {
                'matiere': matiereName, 
                'name': name, 
                'isClasse': isClasse, 
                'nom': nom_classe}
        self.updateComboBox(save=False, reloadGroups=True)

        # si demandé, on lance la création d'un nouveau groupe :
        if doCreate:
            self.createGroupe()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-groupe')

    def updateComboBox(self, save=True, reloadGroups=False):
        # si aucun groupe n'existe, on désactive les boutons et on retourne :
        if len(self.groupes['ORDER']) < 1:
            self.groupesComboBox.clear()
            self.editButton.setEnabled(False)
            self.removeButton.setEnabled(False)
            self.selectionWidget.remplirListes()
            return False
        self.editButton.setEnabled(True)
        self.removeButton.setEnabled(True)

        # si on ne veut pas sauvegarder 
        # (au lancement et à la suppression de groupe) :
        if save:
            self.testRecord()
        else:
            self.selectionWidget.mustRecord = False
        if self.waiting:
            return
        self.waiting = True
        try:
            # on gère les différents cas de mise à jour des comboBox :
            if reloadGroups:
                self.groupesComboBox.clear()
                for id_groupe in self.groupes['ORDER']:
                    matiereName = self.groupes[id_groupe]['matiere']
                    text = utils_functions.u('{0} ({1})').format(
                        self.groupes[id_groupe]['name'], matiereName)
                    self.groupesComboBox.addItem(
                        text, 
                        (matiereName, id_groupe))
            try:
                text = utils_functions.u('{0} ({1})').format(
                    self.groupes[self.id_groupe]['name'], self.matiereName)
                index = self.groupesComboBox.findText(text)
            except:
                index = -1
            if index != -1:
                self.groupesComboBox.setCurrentIndex(index)
            else:
                index = self.groupesComboBox.currentIndex()
                (self.matiereName, self.id_groupe) = self.groupesComboBox.itemData(
                    index, QtCore.Qt.UserRole)
            if self.groupes[self.id_groupe]['isClasse'] == 1:
                index = self.selectionWidget.baseComboBox.findText(
                    self.groupes[self.id_groupe]['nom'])
                self.selectionWidget.baseComboBox.setCurrentIndex(index)
                self.selectionWidget.baseList.setEnabled(False)
            else:
                self.selectionWidget.baseComboBox.setCurrentIndex(0)
                self.selectionWidget.baseList.setEnabled(True)
            self.selectionWidget.id_groupe = self.id_groupe
            self.selectionWidget.remplirListes()
        finally:
            self.waiting = False

    def groupesClicked(self, index):
        if len(self.groupes['ORDER']) < 1:
            return
        index = self.groupesComboBox.currentIndex()
        self.oldData = (self.matiereName, self.id_groupe)
        (self.matiereName, self.id_groupe) = self.groupesComboBox.itemData(
            index, QtCore.Qt.UserRole)
        self.updateComboBox()

    def createGroupe(self):
        # on passe la main à un dialog :
        self.oldData = (self.matiereName, self.id_groupe)
        dialog = NewGroupDlg(parent=self.main, selectedMatiere=self.oldData[0])
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        # on cherche un id pour le nouveau groupe :
        query_my = utils_db.queryExecute(
            'SELECT id_groupe FROM groupes', db=self.main.db_my)
        self.id_groupe = 0
        while query_my.next():
            self.id_groupe = int(query_my.value(0))
            self.id_groupe += 1
        # on récupère le nom et la matière :
        groupeName = dialog.groupNameEdit.text()
        self.matiereName = dialog.matieresComboBox.currentText()
        matiereCode = self.main.me['MATIERES']['Matiere2Code'].get(self.matiereName, '')
        if (dialog.isClassCheckBox.isChecked()):
            isClasse = 1
            classeName = dialog.classesComboBox.currentText()
            # il faut ajouter les élèves de la classe à la liste :
            commandInsert = utils_db.insertInto('groupe_eleve')
            commandLine_my2 = utils_functions.u(
                'SELECT groupes.Name FROM groupe_eleve '
                'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
                'WHERE groupes.id_groupe=? AND groupe_eleve.id_eleve=? '
                'AND groupe_eleve.ordre!=-1 AND groupes.Matiere=?')
            commandLine_my = 'SELECT * FROM eleves WHERE Classe=?'
            query_users = utils_db.query(self.main.db_users)
            queryList = utils_db.query2List(
                {commandLine_my: (classeName, )}, 
                order=['NOM', 'Prenom'], 
                query=query_users)
            ordre = 1
            for record in queryList:
                id_eleve = int(record[0])
                doExist = False
                # on avertit lorsque l'élève est déjà 
                # dans un groupe avec la même matière :
                if matiereCode in self.main.me['MATIERES']['BLT']:
                    for id_groupe in self.groupes['ORDER']:
                        if self.groupes[id_groupe]['matiere'] == self.matiereName:
                            if id_groupe != self.id_groupe:
                                query_my = utils_db.queryExecute(
                                    {commandLine_my2: (id_groupe, id_eleve, self.matiereName)}, 
                                    query=query_my)
                                while query_my.next():
                                    doExist = True
                                    nameGroupe = query_my.value(0)
                if doExist:
                    nom = utils_functions.eleveNameFromId(self.main, id_eleve)
                    message = QtWidgets.QApplication.translate(
                        'main', 
                        '<b>{0}</b> '
                        '<br/> is already recorded in this group: <b>{1}</b>. '
                        '<br/> Subject: <b>{2}</b>.')
                    message = message.format(nom, nameGroupe, self.matiereName)
                    utils_functions.messageBox(
                        self.main, level='warning', message=message)
                else:
                    query_my = utils_db.queryExecute(
                        {commandInsert: (self.id_groupe, id_eleve, ordre)}, 
                        query=query_my)
                    ordre += 1
        else:
            isClasse = -1
            classeName = ''
        # on enregistre dans la base :
        commandLine_my = utils_db.insertInto('groupes')
        query_my = utils_db.queryExecute(
            {commandLine_my: (
                self.id_groupe, groupeName, self.matiereName, isClasse, classeName, 0)}, 
            query=query_my)
        # cas des epis :
        if matiereCode in utils.LSU['EPIS']:
            # id_groupe : self.id_groupe
            # intitule : groupeName
            # theme : matiereCode
            # epiRef : id de l'epi de référence
            # matieresProfs
            # description
            try:
                epiRef = dialog.episComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = dialog.episComboBox.currentIndex()
                epiRef = dialog.episComboBox.itemData(index)
            epiRef = epiRef[0]
            matieresProfs = ''
            for i in range (dialog.epiMatieresList.count()):
                item = dialog.epiMatieresList.item(i)
                data = item.data(QtCore.Qt.UserRole)
                matieresProfs = utils_functions.u(
                    '{0}|{1}#{2}').format(matieresProfs, data[0], data[2])
            matieresProfs = matieresProfs[1:]
            description = dialog.epiDescriptionEdit.toPlainText()
            commandLine_my = (
                'DELETE FROM lsu '
                'WHERE lsuWhat="epis" AND lsu1="{0}"')
            query_my = utils_db.queryExecute(
                commandLine_my.format(self.id_groupe), query=query_my)
            listArgs = (
                'epis', self.id_groupe, groupeName, 
                matiereCode, epiRef, matieresProfs, description, '', '')
            commandLine_my = utils_db.insertInto('lsu')
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
        # cas de l'AP_LSU :
        elif matiereCode in ('AP_LSU', ):
            # id_groupe : self.id_groupe
            # intitule : groupeName
            # apRef : id de l'ap de référence
            # description
            try:
                apRef = dialog.apsComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = dialog.apsComboBox.currentIndex()
                apRef = dialog.apsComboBox.itemData(index)
            apRef = apRef[0]
            description = dialog.apDescriptionEdit.toPlainText()
            commandLine_my = (
                'DELETE FROM lsu '
                'WHERE lsuWhat="aps" AND lsu1="{0}"')
            query_my = utils_db.queryExecute(
                commandLine_my.format(self.id_groupe), query=query_my)
            listArgs = (
                'aps', self.id_groupe, groupeName, '', apRef, '', description, '', '')
            commandLine_my = utils_db.insertInto('lsu')
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
        # cas des parcours :
        elif matiereCode in ('PAR_LSU', ):
            # id_groupe : self.id_groupe
            # intitule : groupeName
            # parcoursType : type de parcours
            try:
                parcoursType = dialog.parcoursComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = dialog.parcoursComboBox.currentIndex()
                parcoursType = dialog.parcoursComboBox.itemData(index)
            parcoursType = parcoursType[0]
            commandLine_my = (
                'DELETE FROM lsu '
                'WHERE lsuWhat="parcours" AND lsu1="{0}"')
            query_my = utils_db.queryExecute(
                commandLine_my.format(self.id_groupe), query=query_my)
            listArgs = (
                'parcours', self.id_groupe, classeName, '', parcoursType, 
                '', '', '', '')
            commandLine_my = utils_db.insertInto('lsu')
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)

        # on ajoute aux matières :
        self.groupes['ORDER'].append(self.id_groupe)
        self.groupes[self.id_groupe] = {
            'matiere': self.matiereName, 
            'name': groupeName, 
            'isClasse': isClasse, 
            'nom': classeName}
        self.updateComboBox(reloadGroups=True)

    def removeGroupe(self):
        self.oldData = (self.matiereName, self.id_groupe)
        # on supprime le groupe sélectionné 
        # et tous les enregistrements du groupe :
        message1 = QtWidgets.QApplication.translate(
            'main', 
            'You will remove the group {0} '
            'and all the tables and evaluations relating to it.').format(
                utils_functions.u('<b>{0}</b><br/>').format(self.groupesComboBox.currentText()))
        message2 = QtWidgets.QApplication.translate(
            'main', 'Are you sure you want to continue?')
        message = utils_functions.u('<p>{0}</p><p><b>{1}</b></p>').format(
            message1, message2)
        if utils_functions.messageBox(
            self.main, level='question', message=message, 
            buttons=['Yes', 'No']) != QtWidgets.QMessageBox.Yes:
            return

        ########################################################################
        ###     On récupère d'abord les infos du groupe
        ########################################################################
        # on récupère les tableaux liés au groupe :
        listTableaux = tableauxFromGroupe(self.main, self.id_groupe, publicOnly=False)

        # on récupère les élèves du groupe :
        commandLine_my = (
            'SELECT id_eleve FROM groupe_eleve '
            'WHERE id_groupe={0}').format(self.id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        listEleves = []
        while query_my.next():
            listEleves.append(int(query_my.value(0)))

        ########################################################################
        ###     On peut supprimer
        ########################################################################
        # on efface des tables evaluations, tableaux et tableau_item :
        tables = ('evaluations', 'tableaux', 'tableau_item')
        for tableName in tables:
            for id_tableau in listTableaux:
                query_my = utils_db.queryExecute(
                    utils_db.qdf_where.format(tableName, 'id_tableau', id_tableau), 
                    query=query_my)
        # pour la table evaluations, il reste les id_tableau 
        # utilisant pg2selection :
        commandLine_my = (
            'DELETE FROM evaluations '
            'WHERE id_tableau>9999 AND (id_tableau % 1000)={0}').format(self.id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # on supprime les appréciations
        # en vérifiant que l'élève n'est pas dans un autre groupe 
        # avec la même matière :
        for id_eleve in listEleves:
            commandLine_my = utils_functions.u(
                'SELECT groupe_eleve.id_eleve FROM groupe_eleve '
                'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
                'WHERE groupes.Matiere="{0}" AND groupe_eleve.id_eleve={1} '
                'AND groupes.id_groupe!={2}').format(
                    self.matiereName, id_eleve, self.id_groupe)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if query_my.last():
                continue
            commandLine_my = utils_functions.u(
                'DELETE FROM appreciations '
                'WHERE Matiere="{0}" AND id_eleve={1}').format(self.matiereName, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # on supprime des tables ayant une colonne id_groupe :
        tables = (
            'groupe_eleve', 'groupes', 'groupe_bilan', 
            'notes', 'notes_values', 'absences', 'counts', 'counts_values')
        for tableName in tables:
            query_my = utils_db.queryExecute(
                utils_db.qdf_where.format(tableName, 'id_groupe', self.id_groupe), 
                query=query_my)
        # on supprime le groupe de la table profil_who :
        commandLine_my = (
            'DELETE FROM profil_who '
            'WHERE profilType="GROUPS" AND id_who={0}')
        query_my = utils_db.queryExecute(
            commandLine_my.format(self.id_groupe), query=query_my)
        # on efface aussi les profils liés aux élèves du groupe :
        lines = []
        commandLine_my = (
            'SELECT DISTINCT profil_who.* '
            'FROM profil_who '
            'LEFT JOIN groupe_eleve ON profil_who.id_who=groupe_eleve.id_eleve '
            'WHERE profil_who.profilType="STUDENTS" '
            'AND groupe_eleve.id_groupe={0}').format(self.id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_who = int(query_my.value(0))
            id_profil = int(query_my.value(1))
            if not((id_who, id_profil) in lines):
                lines.append((id_who, id_profil))
        commandLine_my = (
            'DELETE FROM profil_who '
            'WHERE profilType="STUDENTS" AND id_who={0} AND id_profil={1}')
        for (id_who, id_profil) in lines:
            query_my = utils_db.queryExecute(
                commandLine_my.format(id_who, id_profil), query=query_my)
        # on efface aussi de la table lsu si c'est un epi ou un ap :
        commandLine_my = (
            'DELETE FROM lsu '
            'WHERE lsuWhat IN ("epis", "aps") AND lsu1="{0}"')
        query_my = utils_db.queryExecute(
            commandLine_my.format(self.id_groupe), query=query_my)
        # on supprime le groupe du dictionnaire :
        self.groupes['ORDER'].remove(self.id_groupe)
        del self.groupes[self.id_groupe]
        # et on recharge la liste des groupes :
        index = self.groupesComboBox.currentIndex() - 1
        if index > -1:
            (self.matiereName, self.id_groupe) = self.groupesComboBox.itemData(
                index, QtCore.Qt.UserRole)
        self.updateComboBox(save=False, reloadGroups=True)

    def editGroupe(self):
        self.oldData = (self.matiereName, self.id_groupe)
        # on passe la main à un dialog :
        dialog = NewGroupDlg(
            parent=self.main, selectedMatiere=self.oldData[0], id_groupe=self.oldData[1])
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        # on récupère le nom et la matière :
        groupeName = dialog.groupNameEdit.text()
        self.matiereName = dialog.matieresComboBox.currentText()
        if (dialog.isClassCheckBox.isChecked()):
            isClasse = 1
            classeName = dialog.classesComboBox.currentText()
            updateGroupClass(self.main, dialog=self, group=(self.id_groupe, classeName))
        else:
            isClasse = -1
            classeName = ''
        # on update la table groupes :
        commandLine_my = utils_functions.u(
            'UPDATE groupes '
            'SET Name=?, Matiere=?, isClasse=?, nom_classe=? '
            'WHERE id_groupe=?')
        query_my = utils_db.queryExecute(
            {commandLine_my: (
                groupeName, self.matiereName, isClasse, classeName, self.id_groupe)}, 
            db=self.main.db_my)
        self.groupes[self.id_groupe] = {
            'matiere': self.matiereName, 
            'name': groupeName, 
            'isClasse': isClasse, 
            'nom': classeName}
        if self.matiereName != self.oldData[0]:
            # on efface le groupe de la table profil_who :
            commandLine_my = (
                'DELETE FROM profil_who '
                'WHERE profilType="GROUPS" AND id_who={0}').format(self.id_groupe)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            # on update la table appreciations :
            oldAppreciations = []
            commandLine_my = utils_functions.u(
                'SELECT appreciations.* FROM appreciations '
                'JOIN groupe_eleve ON groupe_eleve.id_eleve=appreciations.id_eleve '
                'WHERE groupe_eleve.id_groupe={0} AND appreciations.Matiere="{1}"'
                '').format(self.id_groupe, self.oldData[0])
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                value = query_my.value(1)
                matiereName = query_my.value(2)
                periode = int(query_my.value(3))
                oldAppreciations.append((id_eleve, value, matiereName, periode))
            for (id_eleve, value, matiereName, periode) in oldAppreciations:
                commandLine_my = utils_functions.u(
                    'UPDATE appreciations '
                    'SET Matiere=? '
                    'WHERE id_eleve=? AND value=? AND Matiere=? AND Periode=?')
                query_my = utils_db.queryExecute(
                    {commandLine_my: (
                        self.matiereName, id_eleve, value, matiereName, periode)}, 
                    query=query_my)

        # cas des epis :
        matiereCode = self.main.me['MATIERES']['Matiere2Code'].get(self.matiereName, '')
        if matiereCode in utils.LSU['EPIS']:
            # id_groupe : self.id_groupe
            # intitule : groupeName
            # theme : matiereCode
            # epiRef : id de l'epi de référence
            # matieresProfs
            # description
            try:
                epiRef = dialog.episComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = dialog.episComboBox.currentIndex()
                epiRef = dialog.episComboBox.itemData(index)
            epiRef = epiRef[0]
            matieresProfs = ''
            for i in range (dialog.epiMatieresList.count()):
                item = dialog.epiMatieresList.item(i)
                data = item.data(QtCore.Qt.UserRole)
                matieresProfs = utils_functions.u(
                    '{0}|{1}#{2}').format(matieresProfs, data[0], data[2])
            matieresProfs = matieresProfs[1:]
            description = dialog.epiDescriptionEdit.toPlainText()
            commandLine_my = (
                'DELETE FROM lsu '
                'WHERE lsuWhat="epis" AND lsu1="{0}"')
            query_my = utils_db.queryExecute(
                commandLine_my.format(self.id_groupe), query=query_my)
            listArgs = (
                'epis', self.id_groupe, groupeName, 
                matiereCode, epiRef, matieresProfs, description, '', '')
            commandLine_my = utils_db.insertInto('lsu')
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
        # cas de l'AP_LSU :
        elif matiereCode in ('AP_LSU', ):
            # id_groupe : self.id_groupe
            # intitule : groupeName
            # apRef : id de l'ap de référence
            # description
            try:
                apRef = dialog.apsComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = dialog.apsComboBox.currentIndex()
                apRef = dialog.apsComboBox.itemData(index)
            apRef = apRef[0]
            description = dialog.apDescriptionEdit.toPlainText()
            commandLine_my = (
                'DELETE FROM lsu '
                'WHERE lsuWhat="aps" AND lsu1="{0}"')
            query_my = utils_db.queryExecute(
                commandLine_my.format(self.id_groupe), query=query_my)
            listArgs = (
                'aps', self.id_groupe, groupeName, '', apRef, '', description, '', '')
            commandLine_my = utils_db.insertInto('lsu')
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
        # cas des parcours :
        elif matiereCode in ('PAR_LSU', ):
            # id_groupe : self.id_groupe
            # intitule : groupeName
            # parcoursType : type de parcours
            try:
                parcoursType = dialog.parcoursComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = dialog.parcoursComboBox.currentIndex()
                parcoursType = dialog.parcoursComboBox.itemData(index)
            parcoursType = parcoursType[0]
            commandLine_my = (
                'DELETE FROM lsu '
                'WHERE lsuWhat="parcours" AND lsu1="{0}"')
            query_my = utils_db.queryExecute(
                commandLine_my.format(self.id_groupe), query=query_my)
            listArgs = (
                'parcours', self.id_groupe, classeName, '', parcoursType, 
                '', '', '', '')
            commandLine_my = utils_db.insertInto('lsu')
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)

        self.updateComboBox(reloadGroups=True)
        self.selectionWidget.remplirListes()

    def doUpdateAllGroups(self):
        # on previent que le remplissage des élèves ne sera plus possible
        message = QtWidgets.QApplication.translate(
            'main',
            '<p>After this operation, you cannot'
            '<br/> modify any more either evaluations'
            '<br/> or appreciations for the students'
            '<br/> deleted by the groups.</p>'
            ''
            '<p></p>'
            '<p><b>It is advised to update your'
            '<br/> seizures before beginning.</b></p>'
            ''
            '<p></p>'
            '<p><b>Do you want to continue'
            '<br/> the update of lists?</b></p>')
        reponse = utils_functions.messageBox(
            self.main, level='warning', message=message, buttons=['Yes', 'No'])
        if reponse == QtWidgets.QMessageBox.No:
            return False

        # on demande à télécharger users.sqlite
        if self.main.NetOK:
            message = QtWidgets.QApplication.translate(
                'main', 
                '<p>Before updating the users group,'
                '<br/> you need to download the latest version'
                '<br/> (which is the website).</p>'
                '<p></p>'
                ''
                '<p><b>Would you download the database users'
                '<br/> before proceeding?</b></p>')
            reponse = utils_functions.messageBox(
                self.main, level='warning', message=message, buttons=['Yes', 'No'])
            if reponse == QtWidgets.QMessageBox.Yes:
                import prof_db
                prof_db.downloadUsersDB(self.main, msgFin=False, doUpdateGroups=False)
        updateGroupClass(self.main)
        self.updateComboBox(save=False)

    def accept(self):
        self.oldData = (self.matiereName, self.id_groupe)
        self.testRecord()
        QtWidgets.QDialog.accept(self)

    def testRecord(self):
        """
        on vérifie s'il faut enregistrer
        """
        if self.selectionWidget.mustRecord:
            if self.recordData():
                self.selectionWidget.mustRecord = False

    def recordData(self):
        """
        on supprime les élèves enlevés de la table groupe_eleve et de profil_who
        et on ajoute les nouveaux élèves
        """
        list_Already = {}
        list_toUpdate = {}
        list_toDelete = {}
        list_toAdd = {}

        # on récupère les élèves déjà existants dans le groupe :
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'groupe_eleve', 'id_groupe', self.oldData[1])
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            ordre = int(query_my.value(2))
            list_Already[id_eleve] = ordre

        # on récupère la liste des élèves sélectionnés :
        ordre = 1
        for i in range(self.selectionWidget.selectionList.count()):
            item = self.selectionWidget.selectionList.item(i)
            checkState = item.checkState()
            if checkState == QtCore.Qt.Unchecked:
                newOrdre = 0
            else:
                newOrdre = ordre
                ordre += 1
            id_eleve = item.data(QtCore.Qt.UserRole)
            # on vérifie si cet élève est déjà enregistré :
            if not id_eleve in list_Already:
                list_toAdd[id_eleve] = newOrdre
            else:
                if list_Already[id_eleve] != newOrdre:
                    list_toUpdate[id_eleve] = newOrdre
                list_Already.pop(id_eleve)
        # on regarde les élèves qui restent dans la 1ère liste :
        list_toDelete = list_Already

        # on lance les enregistrements.
        # les élèves déplacés :
        commandLine_my = 'UPDATE groupe_eleve SET ordre={0} WHERE id_groupe={1} AND id_eleve={2}'
        for id_eleve in list_toUpdate:
            commandLine_my2 = commandLine_my.format(
                list_toUpdate[id_eleve], self.oldData[1], id_eleve)
            query_my = utils_db.queryExecute(commandLine_my2, query=query_my)
        # les élèves ajoutés :
        commandLine_my2 = utils_functions.u(
            'SELECT groupes.Name FROM groupe_eleve '
            'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
            'WHERE groupes.id_groupe=? AND groupe_eleve.id_eleve=? '
            'AND groupe_eleve.ordre!=-1 AND groupes.Matiere=?')
        for id_eleve in list_toAdd:
            doExist = False
            # on avertit lorsque l'élève est déjà dans un groupe
            # avec la même matière du bulletin.
            # Cette vérification ne concerne que les matières du bulletin
            # (par exemple, un même élève peut être dans 2 clubs) :
            matiereCode = self.main.me['MATIERES']['Matiere2Code'].get(self.oldData[0], '')
            if matiereCode in self.main.me['MATIERES']['BLT']:
                for id_groupe in self.groupes['ORDER']:
                    if self.groupes[id_groupe]['matiere'] == self.oldData[0]:
                        if id_groupe != self.oldData[1]:
                            query_my = utils_db.queryExecute(
                                {commandLine_my2: (id_groupe, id_eleve, self.oldData[0])}, 
                                query=query_my)
                            while query_my.next():
                                doExist=True
                                nameGroupe = query_my.value(0)
            if doExist:
                nom = utils_functions.eleveNameFromId(self.main, id_eleve)
                message0 = QtWidgets.QApplication.translate('main', 
                    '<b>{0}</b> '
                    '<br/> is already recorded in this group: <b>{1}</b>. '
                    '<br/> Subject: <b>{2}</b>.')
                message0 = message0.format(nom, nameGroupe, self.oldData[0])
                message1 = QtWidgets.QApplication.translate(
                    'main', 'Do you want to add it anyway?')
                message = utils_functions.u('{0}<p>{1}</p>').format(message0, message1)
                if utils_functions.messageBox(
                    self.main, level='warning', message=message, 
                    buttons=['Yes', 'No']) == QtWidgets.QMessageBox.Yes:
                    commandLine_my = utils_db.insertInto('groupe_eleve')
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (self.oldData[1], id_eleve, list_toAdd[id_eleve])}, 
                        query=query_my)
            else:
                commandLine_my = utils_db.insertInto('groupe_eleve')
                query_my = utils_db.queryExecute(
                    {commandLine_my: (self.oldData[1], id_eleve, list_toAdd[id_eleve])}, 
                    query=query_my)

        # les élèves à supprimer. On passe leur indicateur à -1
        # pour continuer à consulter leurs données :
        commandLine_groupe_eleve = (
            'UPDATE groupe_eleve SET ordre=-1 '
            'WHERE id_groupe={0} AND id_eleve={1}')
        commandLine_test = (
            'SELECT * FROM evaluations '
            'WHERE id_eleve={0} AND id_tableau={1}')
        listTableau = tableauxFromGroupe(self.main, self.oldData[1])
        for id_eleve in list_toDelete:
            # on vérifie si l'élève a été évalué avec ce groupe :
            hasEval = False
            for id_tableau in listTableau:
                commandLine_my = commandLine_test.format(id_eleve, id_tableau)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    hasEval = True
            if hasEval:
                commandLine_my = commandLine_groupe_eleve.format(self.oldData[1], id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            else:
                commandLine_my = (
                    'DELETE FROM groupe_eleve '
                    'WHERE id_groupe={0} AND id_eleve={1}').format(self.oldData[1], id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                # on supprime également de profil_who :
                doExist = False
                for id_groupe in self.groupes['ORDER']:
                    if self.groupes[id_groupe]['matiere'] == self.oldData[0]:
                        if id_groupe != self.oldData[1]:
                            commandLine_my = utils_functions.u(
                                'SELECT groupes.Name, groupes.id_groupe '
                                'FROM groupe_eleve '
                                'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
                                'WHERE groupes.id_groupe=? AND groupe_eleve.id_eleve=? '
                                'AND groupe_eleve.ordre!=-1 '
                                'AND groupes.Matiere=?')
                            query_my = utils_db.queryExecute(
                                {commandLine_my: (id_groupe, id_eleve, self.oldData[0])}, 
                                query=query_my)
                            while query_my.next():
                                id_groupe = int(query_my.value(1))
                                doExist = True
                if not(doExist):
                    commandLine_my = (
                        'DELETE FROM profil_who '
                        'WHERE profilType="STUDENTS" AND id_who={0}').format(id_eleve)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        return True


def updateGroupClass(main, dialog=None, group=[]):
    """
    pour mettre à jour la liste des élèves lorsqu'il s'agit d'un groupe-classe.
    Si on ne l'appelle pas depuis le dialog GestGroupesDlg, 
    il faut indiquer dialog et le groupe à mettre à jour (group).
    Sinon, on met tous les groupes à jour 
    (par exemple après téléchargement de la base users).
    """
    query_my = utils_db.query(main.db_my)
    query_users = utils_db.query(main.db_users)

    allGroups = {}
    groupsToUpdate = {}
    students = {'DELETE': [], 'KEEP': [], 'NEW': []}
    if dialog != None:
        groupsToUpdate[group[0]] = group[1]
    # on recherche tous les groupes existants :
    commandLine_my = utils_db.q_selectAllFromOrder.format(
        'groupes', 'ordre, Matiere, UPPER(Name)')
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_groupe = int(query_my.value(0))
        groupName = query_my.value(1)
        groupMatiere = query_my.value(2)
        isClasse = int(query_my.value(3))
        nomClasse = query_my.value(4)
        ordre = int(query_my.value(5))
        allGroups[id_groupe] = (groupName, groupMatiere, isClasse, nomClasse, ordre)
        if (dialog == None) and (isClasse == 1):
            groupsToUpdate[id_groupe] = nomClasse

    # quelques lignes de commandes utiles :
    commandLine_my1 = utils_functions.u(
        'SELECT value FROM appreciations '
        'WHERE Matiere="{0}" AND id_eleve={1}')
    commandLine_my2 = (
        'SELECT value FROM evaluations '
        'WHERE id_eleve={0} and id_tableau={1}')
    commandLine_my3 = (
        'UPDATE groupe_eleve SET ordre=-1 '
        'WHERE id_groupe={0} AND id_eleve={1}')
    commandLine_my4 = (
        'DELETE FROM groupe_eleve '
        'WHERE id_groupe={0} AND id_eleve={1}')
    commandLine_my5 = (
        'DELETE FROM profil_who '
        'WHERE profilType="STUDENTS" AND id_who={0}')
    if dialog != None:
        commandLine_my10 = utils_functions.u(
            'SELECT groupes.Name FROM groupe_eleve '
            'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
            'WHERE groupes.id_groupe=? AND groupe_eleve.id_eleve=? '
            'AND groupe_eleve.ordre!=-1 AND groupes.Matiere=?')
    else:
        commandLine_my10 = utils_functions.u(
            'SELECT groupes.Name FROM groupe_eleve '
            'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
            'WHERE groupes.id_groupe=? AND groupe_eleve.id_eleve=? '
            'AND groupe_eleve.ordre!=-1 AND groupes.Matiere=? '
            'AND groupes.isClasse=-1')

    for id_groupe in groupsToUpdate:
        nomClasse = groupsToUpdate[id_groupe]
        matiereName = matiereFromGroupe(main, id_groupe)
        tableaux = tableauxFromGroupe(main, id_groupe)
        # on récupère la liste des élèves du groupe-classe dans la base users :
        studentsInUsers = {'ORDER': []}
        commandLine = 'SELECT * FROM eleves WHERE Classe=?'
        queryList = utils_db.query2List(
            {commandLine: (nomClasse, )}, 
            order=['NOM', 'Prenom'], 
            query=query_users)
        for record in queryList:
            id_eleve = int(record[0])
            studentName = record[1]
            studentFirstName = record[2]
            className = record[3]
            studentsInUsers[id_eleve] = (studentName, studentFirstName, className)
            studentsInUsers['ORDER'].append(id_eleve)
        # et celle qui est dans la base du prof :
        studentsInGroup = {}
        commandLine_my = (
            'SELECT id_eleve, ordre FROM groupe_eleve '
            'WHERE id_groupe={0}').format(id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(0))
            ordre = int(query_my.value(1))
            studentsInGroup[id_eleve] = ordre

        # cas des élèves ayant quitté le groupe-classe.
        # On vérifie s'il y a des données (appréciations puis évaluations)
        # pour savoir s'il faut conserver ces élèves dans la base du prof.
        for id_eleve in studentsInGroup:
            if not(id_eleve in studentsInUsers):
                mustBeKept = False
                # on cherche s'il y a une appréciation :
                commandLine = commandLine_my1.format(matiereName, id_eleve)
                query_my = utils_db.queryExecute(commandLine, query=query_my)
                if query_my.first():
                    mustBeKept = True
                if not(mustBeKept):
                    # on cherche s'il y a des évaluations :
                    for id_tableau in tableaux:
                        commandLine = commandLine_my2.format(id_eleve, id_tableau)
                        query_my = utils_db.queryExecute(commandLine, query=query_my)
                        if query_my.first():
                            mustBeKept = True
                if mustBeKept:
                    # l'élève a été évalué donc on le garde
                    # mais on passe son ordre à -1 :
                    commandLine_my = commandLine_my3.format(id_groupe, id_eleve)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    students['KEEP'].append((id_groupe, id_eleve, nomClasse))
                else:
                    # on peut effacer l'élève du groupe :
                    commandLine_my = commandLine_my4.format(id_groupe, id_eleve)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    commandLine_my = commandLine_my5.format(id_eleve)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    students['DELETE'].append((id_groupe, id_eleve, nomClasse))

        # on supprime et réécrit les élèves pour conserver l'ordre alphabétique
        ordre = 1
        for id_eleve in studentsInUsers['ORDER']:
            newOrdre = ordre
            if id_eleve in studentsInGroup:
                if studentsInGroup[id_eleve] == 0:
                    newOrdre = 0
            # on vérifie la présence dans les autres groupes:
            doExist = False
            # on avertit lorsque l'élève est déjà dans un groupe avec la même matière
            matiereCode = main.me['MATIERES']['Matiere2Code'].get(matiereName, '')
            if matiereCode in main.me['MATIERES']['BLT']:
                for idGroupe in allGroups:
                    if idGroupe == id_groupe:
                        continue
                    query_my = utils_db.queryExecute(
                        {commandLine_my10: (idGroupe, id_eleve, matiereName)}, 
                        query=query_my)
                    while query_my.next():
                        doExist = True
                        nameGroupe = query_my.value(0)
            if doExist:
                nom = utils_functions.eleveNameFromId(main, id_eleve)
                message = QtWidgets.QApplication.translate('main', 
                        '<b>{0}</b> '
                        '<br/> is already recorded in this group: <b>{1}</b>. '
                        '<br/> Subject: <b>{2}</b>.')
                message = message.format(nom, nameGroupe, matiereName)
                utils_functions.messageBox(main, level='warning', message=message)
                continue
            else:
                commandLine_my = (
                    'DELETE FROM groupe_eleve '
                    'WHERE id_groupe={0} AND id_eleve={1}').format(id_groupe, id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                query_my = utils_db.queryExecute(
                    {utils_db.insertInto('groupe_eleve'): (id_groupe, id_eleve, newOrdre)}, 
                    query=query_my)
                if not(id_eleve in studentsInGroup):
                    students['NEW'].append((id_groupe, id_eleve, studentsInUsers[id_eleve]))
                if newOrdre > 0:
                    ordre += 1

    # mise en forme et affichage du message final :
    message = ''
    if len(students['NEW']) > 0:
        m1 = QtWidgets.QApplication.translate('main', 'New students:')
        message = utils_functions.u(
            '{1}'
            '<p align="center">{0}</p>'
            '<p><b>{2}</b> {3}</p>'
            '<ul>').format(utils.SEPARATOR_LINE, message, m1, len(students['NEW']))
        for (id_groupe, id_eleve, (studentName, studentFirstName, className)) in students['NEW']:
            message = utils_functions.u(
                '{0}'
                '<li>{1} {2} ({3})</li>').format(
                    message,
                    studentName, studentFirstName, className)
        message = utils_functions.u(
            '{0}'
            '</ul>').format(message)
    if len(students['DELETE']) > 0:
        m1 = QtWidgets.QApplication.translate('main', 'Deleted students:')
        message = utils_functions.u(
            '{1}'
            '<p align="center">{0}</p>'
            '<p><b>{2}</b> {3}</p>'
            '').format(utils.SEPARATOR_LINE, message, m1, len(students['DELETE']))
    if len(students['KEEP']) > 0:
        m1 = QtWidgets.QApplication.translate('main', 'Kept students:')
        message = utils_functions.u(
            '{1}'
            '<p align="center">{0}</p>'
            '<p><b>{2}</b> {3}</p>'
            '').format(utils.SEPARATOR_LINE, message, m1, len(students['KEEP']))
    if message != '':
        utils_functions.messageBox(main, message=message)
    """
    elif dialog == None:
        utils_functions.afficheMsgFin(main, timer=5)
    """
    # fin :
    main.changeDBMyState()
    if dialog != None:
        dialog.selectionWidget.mustRecord = False


class ChooseProfDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, profs={'ORDER':[]}, actual=-1):
        super(ChooseProfDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Choose Teacher'))
        text = QtWidgets.QApplication.translate('main', 'Teachers:')
        label = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(text))
        self.comboBox = QtWidgets.QComboBox()
        self.comboBox.setMinimumWidth(200)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 2, 0)
        grid.addWidget(self.comboBox, 2, 1)
        grid.addWidget(buttonBox, 4, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # remplissage de la liste déroulante :
        for id_prof in profs['ORDER']:
            (profNomPrenom, profMatiere) = profs[id_prof]
            text = utils_functions.u(
                '{0} [{1}]').format(profNomPrenom, profMatiere)
            self.comboBox.addItem(text, id_prof)
        if actual > -1:
            index = self.comboBox.findData(actual, QtCore.Qt.UserRole)
            self.comboBox.setCurrentIndex(index)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-groupe')


class NewGroupDlg(QtWidgets.QDialog):
    """
    Pour créer ou éditer les groupes d'élèves (nom, matière, etc).
    Pour LSU, gère aussi les EPI l'AP et les parcours
    """
    def __init__(self, parent=None, selectedMatiere='', id_groupe=-1):
        super(NewGroupDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        if id_groupe == -1:
            self.setWindowTitle(
                QtWidgets.QApplication.translate('main', 'Create Group'))
        else:
            self.setWindowTitle(
                QtWidgets.QApplication.translate('main', 'Edit group'))

        self.id_groupe = id_groupe
        self.selectedMatiere = selectedMatiere

        # les champs de saisie :
        text = QtWidgets.QApplication.translate(
            'main', 'Name:')
        groupNameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.groupNameEdit = QtWidgets.QLineEdit()
        text = QtWidgets.QApplication.translate(
            'main', 'Subject:')
        matiereLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.matieresComboBox = QtWidgets.QComboBox()
        self.matieresComboBox.setMinimumWidth(200)
        text = QtWidgets.QApplication.translate(
            'main', 'Group-class')
        self.isClassCheckBox = QtWidgets.QCheckBox(text)
        self.isClassCheckBox.setCheckState(QtCore.Qt.Unchecked)
        self.classesComboBox = QtWidgets.QComboBox()
        self.classesComboBox.setMinimumWidth(200)
        self.classesComboBox.setEnabled(False)
        # on groupe les actions :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(groupNameLabel,          1, 1, 1, 2)
        grid.addWidget(self.groupNameEdit,      2, 2)
        grid.addWidget(matiereLabel,            1, 3, 1, 2)
        grid.addWidget(self.matieresComboBox,   2, 4)
        grid.addWidget(self.isClassCheckBox,    3, 2)
        grid.addWidget(self.classesComboBox,    3, 4)
        groupGroupBox = QtWidgets.QGroupBox()
        groupGroupBox.setFlat(True)
        groupGroupBox.setLayout(grid)

        # pour les epis :
        self.episData = {'THEMES': {}, 'PROFS': []}
        self.episTitle = QtWidgets.QApplication.translate(
            'main', 'EPI reference of the school:')
        self.episLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.episComboBox = QtWidgets.QComboBox()
        text1 = QtWidgets.QApplication.translate(
            'main', 'Subjects:')
        text2 = QtWidgets.QApplication.translate(
            'main', 'double-click to select a teacher')
        epiMatieresLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{1}').format(text1, text2))
        self.epiMatieresList = QtWidgets.QListWidget()
        text = QtWidgets.QApplication.translate(
            'main', 'Description:')
        epiDescriptionLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.epiDescriptionEdit = QtWidgets.QTextEdit()
        # on groupe les actions :
        episVBox = QtWidgets.QVBoxLayout()
        episVBox.addWidget(QtWidgets.QLabel('<hr width="80%">'))
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.episLabel,         1, 1, 1, 2)
        grid.addWidget(self.episComboBox,      2, 2)
        grid.addWidget(epiMatieresLabel,          3, 1, 1, 2)
        grid.addWidget(self.epiMatieresList,      4, 2)
        grid.addWidget(epiDescriptionLabel,       5, 1, 1, 2)
        grid.addWidget(self.epiDescriptionEdit,   6, 2)
        episVBox.addLayout(grid)
        self.episGroupBox = QtWidgets.QGroupBox()
        self.episGroupBox.setFlat(True)
        self.episGroupBox.setLayout(episVBox)
        self.episGroupBox.setVisible(False)

        # pour les aps :
        self.apsData = {'APS': [], }
        self.apsTitle = QtWidgets.QApplication.translate(
            'main', 'AP reference of the school:')
        self.apsLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.apsComboBox = QtWidgets.QComboBox()
        text = QtWidgets.QApplication.translate(
            'main', 'Description:')
        apDescriptionLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.apDescriptionEdit = QtWidgets.QTextEdit()
        # on groupe les actions :
        apsVBox = QtWidgets.QVBoxLayout()
        apsVBox.addWidget(QtWidgets.QLabel('<hr width="80%">'))
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.apsLabel,         1, 1, 1, 2)
        grid.addWidget(self.apsComboBox,      2, 2)
        grid.addWidget(apDescriptionLabel,       5, 1, 1, 2)
        grid.addWidget(self.apDescriptionEdit,   6, 2)
        apsVBox.addLayout(grid)
        self.apsGroupBox = QtWidgets.QGroupBox()
        self.apsGroupBox.setFlat(True)
        self.apsGroupBox.setLayout(apsVBox)
        self.apsGroupBox.setVisible(False)

        # pour les parcours :
        self.parcoursData = []
        text = QtWidgets.QApplication.translate(
            'main', 'Type of educational path:')
        self.parcoursLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.parcoursComboBox = QtWidgets.QComboBox()
        # on groupe les actions :
        parcoursVBox = QtWidgets.QVBoxLayout()
        parcoursVBox.addWidget(QtWidgets.QLabel('<hr width="80%">'))
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.parcoursLabel,         1, 1)
        grid.addWidget(self.parcoursComboBox,      1, 2)
        parcoursVBox.addLayout(grid)
        self.parcoursGroupBox = QtWidgets.QGroupBox()
        self.parcoursGroupBox.setFlat(True)
        self.parcoursGroupBox.setLayout(parcoursVBox)
        self.parcoursGroupBox.setVisible(False)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(groupGroupBox,       1, 1)
        grid.addWidget(self.episGroupBox,       2, 1)
        grid.addWidget(self.apsGroupBox,       3, 1)
        grid.addWidget(self.parcoursGroupBox,       4, 1)
        grid.addWidget(buttonBox, 9, 1)
        self.setLayout(grid)
        self.setMinimumWidth(600)

        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les tables commun.lsu et commun.matieres
        """
        query_commun = utils_db.query(self.main.db_commun)
        query_users = utils_db.query(self.main.db_users)

        # est-on PP ou en version perso :
        isPP = (self.main.me['userMode'] == 'PP')
        isPerso = (self.main.actualVersion['versionName'] == 'perso')
        # seul le PP n'a pas les matières "normales" au début :
        if not(isPP):
            for matiereCode in self.main.me['MATIERES']['BLT']:
                matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                    matiereCode, (0, '', ''))[1]
                self.matieresComboBox.addItem(matiereName, matiereCode)
            self.matieresComboBox.insertSeparator(1000)
        # le PP et la version perso ont la matière PP :
        if isPP or isPerso:
            self.matieresComboBox.addItem(
                self.main.me['MATIERES']['Code2Matiere']['PP'][1], 'PP')
            self.matieresComboBox.insertSeparator(1000)
        # tout le monde a la matière VS :
        self.matieresComboBox.addItem(
            self.main.me['MATIERES']['Code2Matiere']['VS'][1], 'VS')
        # tout le monde a les matières hors bulletin :
        for matiereCode in self.main.me['MATIERES']['OTHERS']:
            matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                matiereCode, (0, '', ''))[1]
            self.matieresComboBox.addItem(matiereName, matiereCode)
        # le PP a les matières "normales" à la fin :
        if isPP:
            self.matieresComboBox.insertSeparator(1000)
            for matiereCode in self.main.me['MATIERES']['BLT']:
                matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                    matiereCode, (0, '', ''))[1]
                self.matieresComboBox.addItem(matiereName, matiereCode)

        # on remplit la liste des classes
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            self.classesComboBox.addItem(query_commun.value(1))

        # préselection de la matière :        
        if self.id_groupe == -1:
            if not(self.selectedMatiere in ('', self.main.me['Matiere'])):
                matiereName = self.selectedMatiere
            else:
                matiereName = self.main.me['Matiere']
            index = self.matieresComboBox.findText(matiereName)
            if index > -1:
                self.matieresComboBox.setCurrentIndex(index)
        else:
            # on recherche les données du tableau
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'groupes', 'id_groupe', self.id_groupe)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            if query_my.first():
                name = query_my.value(1)
                matiereName = query_my.value(2)
                isClasse = int(query_my.value(3))
                classeName = query_my.value(4)
            if isClasse == 1:
                self.isClassCheckBox.setCheckState(QtCore.Qt.Checked)
                self.classesComboBox.setEnabled(True)
                index = self.classesComboBox.findText(classeName)
                if index > - 1:
                    self.classesComboBox.setCurrentIndex(index)
            self.groupNameEdit.setText(name)
            index = self.matieresComboBox.findText(matiereName)
            if index > - 1:
                self.matieresComboBox.setCurrentIndex(index)

        # récupération des epis existants :
        self.episData = {'THEMES': {}, 'PROFS': {'ORDER': [], }}
        for theme in utils.LSU['EPIS']:
            self.episData['THEMES'][theme] = []
        commandLine_commun = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'epis')
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=self.main.db_commun)
        while query_commun.next():
            id_epi = int(query_commun.value(1)) # id
            epiTheme = query_commun.value(2) # thematique
            epiName = query_commun.value(3) # intitule
            epiMatieres = query_commun.value(4) # discipline-refs
            epiLabel = query_commun.value(5) # description
            self.episData['THEMES'][epiTheme].append([id_epi, epiName, epiMatieres, epiLabel])
        # et des profs (pour les associer aux epis) :
        commandLine_users = utils_functions.u(
            'SELECT * FROM profs WHERE id<{0}').format(utils.decalagePP)
        queryList = utils_db.query2List(
            commandLine_users, order=['NOM', 'Prenom'], query=query_users)
        for record in queryList:
            id_prof = int(record[0])
            profNomPrenom = utils_functions.u(
                '{0} {1}').format(record[1], record[2])
            profMatiere = record[6]
            self.episData['PROFS']['ORDER'].append(id_prof)
            self.episData['PROFS'][id_prof] = (profNomPrenom, profMatiere)

        # récupération des aps existants :
        self.apsData = {'APS': [], }
        commandLine_commun = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'aps')
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=self.main.db_commun)
        while query_commun.next():
            id_ap = int(query_commun.value(1)) # id
            apName = query_commun.value(3) # intitule
            apMatieres = query_commun.value(4) # discipline-refs
            apLabel = query_commun.value(5) # description
            self.apsData['APS'].append([id_ap, apName, apMatieres, apLabel])

        # récupération des parcours existants :
        self.parcoursData = []
        commandLine_commun = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'parcours')
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=self.main.db_commun)
        while query_commun.next():
            parcoursName = query_commun.value(2) # intitule
            parcoursLabel = query_commun.value(3) # description
            self.parcoursData.append([parcoursName, parcoursLabel])

        # (pré)sélections :
        self.matieresComboBoxChanged()
        # pour les epis et l'ap c'est un peu plus compliqué :
        if self.id_groupe > -1:
            commandLine_my = (
                'SELECT * FROM lsu '
                'WHERE lsuWhat IN ("epis", "aps", "parcours") AND lsu1="{0}"')
            query_my = utils_db.queryExecute(
                commandLine_my.format(self.id_groupe), 
                query=query_my)
            if query_my.first():
                lsuWhat =  query_my.value(0)
                if lsuWhat == 'epis':
                    matiereCode = query_my.value(3)
                    epiRef = int(query_my.value(4))
                    matieresProfs = query_my.value(5)
                    description = query_my.value(6)
                    for epi in self.episData['THEMES'][matiereCode]:
                        if epi[0] == epiRef:
                            index = self.episComboBox.findText(epi[1])
                            if index > - 1:
                                self.episComboBox.setCurrentIndex(index)
                    self.episComboBoxChanged()
                    temp = matieresProfs.split('|')
                    matieresProfs = {}
                    for matiereProf in temp:
                        if len(matiereProf) > 0:
                            l = matiereProf.split('#')
                            try:
                                matieresProfs[l[0]] = int(l[1])
                            except:
                                matieresProfs[l[0]] = -1
                    for i in range(self.epiMatieresList.count()):
                        item = self.epiMatieresList.item(i)
                        data = item.data(QtCore.Qt.UserRole)
                        id_prof = matieresProfs.get(data[0], -1)
                        if id_prof > -1:
                            item.setData(
                                QtCore.Qt.UserRole, 
                                (data[0], data[1], id_prof))
                            text = utils_functions.u(
                                '{0} : {1}').format(
                                    data[1], self.episData['PROFS'][id_prof][0])
                            item.setText(text)
                    text = utils_functions.u(
                        '<b>{0}</b>').format(self.episTitle)
                    self.episLabel.setText(text)
                    self.epiDescriptionEdit.setPlainText(description)
                elif lsuWhat == 'aps':
                    apRef = int(query_my.value(4))
                    description = query_my.value(6)
                    for ap in self.apsData['APS']:
                        if ap[0] == apRef:
                            index = self.apsComboBox.findText(ap[1])
                            if index > - 1:
                                self.apsComboBox.setCurrentIndex(index)
                    self.apsComboBoxChanged()
                    text = utils_functions.u(
                        '<b>{0}</b>').format(self.apsTitle)
                    self.apsLabel.setText(text)
                    self.apDescriptionEdit.setPlainText(description)
                elif lsuWhat == 'parcours':
                    parcoursType = query_my.value(4)
                    for parcours in self.parcoursData:
                        if parcours[0] == parcoursType:
                            index = self.parcoursComboBox.findText(parcours[1])
                            if index > - 1:
                                self.parcoursComboBox.setCurrentIndex(index)
                    self.parcoursComboBoxChanged()

    def createConnexions(self):
        self.matieresComboBox.activated.connect(self.matieresComboBoxChanged)
        self.isClassCheckBox.stateChanged.connect(self.isClassCheckBoxChanged)
        self.classesComboBox.activated.connect(self.classesComboBoxChanged)
        self.episComboBox.activated.connect(self.episComboBoxChanged)
        self.epiMatieresList.itemDoubleClicked.connect(self.epiProfChanged)
        self.apsComboBox.activated.connect(self.apsComboBoxChanged)
        self.parcoursComboBox.activated.connect(self.parcoursComboBoxChanged)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-groupe')

    def isClassCheckBoxChanged(self, checkState=False):
        if checkState == QtCore.Qt.Checked:
            self.classesComboBox.setEnabled(True)
            className = self.classesComboBox.currentText()
            self.groupNameEdit.setText(className)
        else:
            self.classesComboBox.setEnabled(False)

    def matieresComboBoxChanged(self):
        try:
            matiereCode = self.matieresComboBox.currentData(QtCore.Qt.UserRole)
        except:
            index = self.matieresComboBox.currentIndex()
            matiereCode = self.matieresComboBox.itemData(index)
        self.setMaximumHeight(1)
        episVisibility = (matiereCode in utils.LSU['EPIS'])
        self.episGroupBox.setVisible(episVisibility)
        apsVisibility = (matiereCode in ('AP_LSU', ))
        self.apsGroupBox.setVisible(apsVisibility)
        parcoursVisibility = (matiereCode in ('PAR_LSU', ))
        self.parcoursGroupBox.setVisible(parcoursVisibility)
        if episVisibility:
            self.setMaximumHeight(1000)
            # partie epis :
            self.episComboBox.clear()
            if len(self.episData['THEMES'][matiereCode]) < 1:
                text = QtWidgets.QApplication.translate(
                    'main', 'no EPI for this thematic')
                self.episComboBox.setEnabled(False)
            else:
                self.episComboBox.setEnabled(True)
                text = QtWidgets.QApplication.translate(
                    'main', 'select EPI')
                self.episComboBox.addItem('', None)
                for epi in self.episData['THEMES'][matiereCode]:
                    self.episComboBox.addItem(epi[1], epi)
            text = utils_functions.u(
                '<b>{0}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{1}').format(self.episTitle, text)
            self.episLabel.setText(text)
            self.episComboBoxChanged()
        elif apsVisibility:
            self.setMaximumHeight(1000)
            # partie aps :
            self.apsComboBox.clear()
            if len(self.apsData['APS']) < 1:
                text = QtWidgets.QApplication.translate(
                    'main', 'no AP')
                self.apsComboBox.setEnabled(False)
            else:
                self.apsComboBox.setEnabled(True)
                text = QtWidgets.QApplication.translate(
                    'main', 'select AP')
                self.apsComboBox.addItem('', None)
                for ap in self.apsData['APS']:
                    self.apsComboBox.addItem(ap[1], ap)
            text = utils_functions.u(
                '<b>{0}</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{1}').format(self.apsTitle, text)
            self.apsLabel.setText(text)
            self.apsComboBoxChanged()
        elif parcoursVisibility:
            # partie parcours :
            self.parcoursComboBox.clear()
            if len(self.parcoursData) < 1:
                self.parcoursComboBox.setEnabled(False)
            else:
                self.parcoursComboBox.setEnabled(True)
                self.parcoursComboBox.addItem('', None)
                for parcours in self.parcoursData:
                    self.parcoursComboBox.addItem(parcours[1], parcours)
            self.parcoursComboBoxChanged()

    def episComboBoxChanged(self):
        try:
            epi = self.episComboBox.currentData(QtCore.Qt.UserRole)
        except:
            index = self.episComboBox.currentIndex()
            epi = self.episComboBox.itemData(index)
        self.epiMatieresList.clear()
        if epi == None:
            return
        matieres = epi[2].split('|')
        for matiereCode in matieres:
            matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                matiereCode, (0, '', ''))[1]
            item = QtWidgets.QListWidgetItem(matiereName)
            item.setData(QtCore.Qt.UserRole, (matiereCode, matiereName, -1))
            self.epiMatieresList.addItem(item)
        self.epiDescriptionEdit.setPlainText(epi[3])

    def epiProfChanged(self, item):
        current = self.epiMatieresList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        # dialog spécifique :
        dialog = ChooseProfDlg(
            parent=self.main, profs=self.episData['PROFS'], actual=data[2])
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            currentIndex = dialog.comboBox.currentIndex()
            id_prof = dialog.comboBox.itemData(currentIndex)
            current.setData(QtCore.Qt.UserRole, (data[0], data[1], id_prof))
            text = utils_functions.u('{0} : {1}').format(data[1], self.episData['PROFS'][id_prof][0])
            current.setText(text)

    def apsComboBoxChanged(self):
        try:
            ap = self.apsComboBox.currentData(QtCore.Qt.UserRole)
        except:
            index = self.apsComboBox.currentIndex()
            ap = self.apsComboBox.itemData(index)
        if ap == None:
            return
        self.apDescriptionEdit.setPlainText(ap[3])

    def parcoursComboBoxChanged(self):
        try:
            parcours = self.parcoursComboBox.currentData(QtCore.Qt.UserRole)
        except:
            index = self.parcoursComboBox.currentIndex()
            parcours = self.parcoursComboBox.itemData(index)
        if parcours == None:
            return

    def classesComboBoxChanged(self):
        className = self.classesComboBox.currentText()
        self.groupNameEdit.setText(className)

    def accept(self):
        if not(self.matieresComboBox.currentText()):
            message = QtWidgets.QApplication.translate(
                'main', 'Please choose a subject.')
            utils_functions.messageBox(self.main, level='warning', message=message)
            self.matieresComboBox.setFocus()
            return False
        elif not(self.groupNameEdit.text()):
            message = QtWidgets.QApplication.translate(
                'main', 'Please choose a name.')
            utils_functions.messageBox(self.main, level='warning', message=message)
            self.groupNameEdit.selectAll()
            self.groupNameEdit.setFocus()
            return False
        # on vérifie que le nom n'existe pas déjà pour la même matière :
        commandLine_my = (
            'SELECT * FROM groupes WHERE Matiere=? '
            'AND Name=? AND id_groupe!=?')
        args = (
            self.matieresComboBox.currentText(), 
            self.groupNameEdit.text(), 
            self.id_groupe)
        query_my = utils_db.queryExecute(
            {commandLine_my: args}, db=self.main.db_my)
        doExist = False
        while query_my.next():
            doExist = True
        if not(doExist):
            QtWidgets.QDialog.accept(self)
            return True
        else:
            message = QtWidgets.QApplication.translate(
                'main', 'This name is already in use. Please choose another.')
            utils_functions.messageBox(self.main, level='warning', message=message)
            self.groupNameEdit.selectAll()
            self.groupNameEdit.setFocus()
            return False












class CreateMultipleGroupesClassesDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None):
        super(CreateMultipleGroupesClassesDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'CreateMultipleGroupesClasses'))

        # matière :
        matiereLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Subject:')))
        self.matieresComboBox = QtWidgets.QComboBox()
        self.matieresComboBox.setMinimumWidth(200)
        # les classes :
        classesLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Classes:')))
        self.classesListWidget = QtWidgets.QListWidget()
        self.classesListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        # base des noms :
        toolTip = QtWidgets.QApplication.translate(
            'main', 
            'The names of created groups begin '
            'by what you specify here '
            'and will be followed by the class name.'
            '<br/><br/>If you leave this field blank, '
            'the group name will be that of the class.')
        nameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'BaseName:')))
        nameLabel.setToolTip(toolTip)
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.setToolTip(toolTip)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(matiereLabel, 1, 0)
        grid.addWidget(self.matieresComboBox, 1, 1)
        grid.addWidget(classesLabel, 2, 0)
        grid.addWidget(self.classesListWidget, 2, 1)
        grid.addWidget(nameLabel, 3, 0)
        grid.addWidget(self.nameEdit, 3, 1)
        grid.addWidget(buttonBox, 7, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # est-on PP ou en version perso :
        isPP = (self.main.me['userMode'] == 'PP')
        isPerso = (self.main.actualVersion['versionName'] == 'perso')
        query_commun = utils_db.query(self.main.db_commun)
        # seul le PP n'a pas les matières "normales" au début :
        if not(isPP):
            for matiereCode in self.main.me['MATIERES']['BLT']:
                matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                    matiereCode, (0, '', ''))[1]
                self.matieresComboBox.addItem(matiereName)
            self.matieresComboBox.insertSeparator(1000)
        # le PP et la version perso ont la matière PP :
        if isPP or isPerso:
            self.matieresComboBox.addItem(
                self.main.me['MATIERES']['Code2Matiere']['PP'][1])
            self.matieresComboBox.insertSeparator(1000)
        # tout le monde a la matière VS :
        self.matieresComboBox.addItem(
            self.main.me['MATIERES']['Code2Matiere']['VS'][1])
        # tout le monde a les matières hors bulletin :
        for matiereCode in self.main.me['MATIERES']['OTHERS']:
            matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                matiereCode, (0, '', ''))[1]
            self.matieresComboBox.addItem(matiereName)
        # le PP a les matières "normales" à la fin :
        if isPP:
            self.matieresComboBox.insertSeparator(1000)
            for matiereCode in self.main.me['MATIERES']['BLT']:
                matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                    matiereCode, (0, '', ''))[1]
                self.matieresComboBox.addItem(matiereName)

        # on remplit la liste des classes
        self.idFromClasse = {}
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            itemText = query_commun.value(1)
            self.idFromClasse[itemText] = int(query_commun.value(0))
            self.classesListWidget.addItem(itemText)

        # préselection de la matière du prof :
        index = self.matieresComboBox.findText(self.main.me['Matiere'])
        if index > -1:
            self.matieresComboBox.setCurrentIndex(index)


    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-multiple-groupes-classes')




class ClearDeletedElevesDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None):
        super(ClearDeletedElevesDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Clear students deleted from groups'))

        # les élèves retirés des groupes :
        elevesTitle = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate(
                'main', 'Students removed from the groups:'))
        elevesLabel = QtWidgets.QLabel(elevesTitle)
        self.elevesListWidget = QtWidgets.QListWidget()
        self.data = {}

        comments = QtWidgets.QApplication.translate(
            'main',
            '<p>Select the students you want to permanently delete.</p>'
            '<p>All the traces (assessments, opinions, etc) '
            '<br/>of this students will be <b>definitively</b> deleted.</p>')
        labelComments = QtWidgets.QLabel(comments)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(elevesLabel, 1, 0, 1, 2)
        grid.addWidget(self.elevesListWidget, 2, 1)
        grid.addWidget(labelComments, 5, 0, 1, 2, QtCore.Qt.AlignHCenter)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # on remplit la liste des élèves :
        commandLine_my = (
            'SELECT groupe_eleve.id_groupe, groupe_eleve.id_eleve, '
            'groupes.Name, groupes.Matiere '
            'FROM groupe_eleve '
            'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
            'WHERE groupe_eleve.ordre<1')
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_groupe = int(query_my.value(0))
            id_eleve = int(query_my.value(1))
            groupeName = query_my.value(2)
            Matiere = query_my.value(3)
            eleveName = utils_functions.eleveNameFromId(self.main, id_eleve)
            itemText = utils_functions.u('{0}\t({1} - {2})').format(eleveName, groupeName, Matiere)
            newItem = QtWidgets.QListWidgetItem(itemText)
            newItem.setData(QtCore.Qt.UserRole, (id_eleve, id_groupe, Matiere))
            newItem.setCheckState(QtCore.Qt.Unchecked)
            self.elevesListWidget.addItem(newItem)
            #self.elevesListWidget.addItem(itemText)
            self.data[itemText] = (id_eleve, id_groupe, Matiere)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-groupe')















def tableauxFromGroupe(main, id_groupe, periode=-1, publicOnly=True):
    result = []
    commandLine = 'SELECT id_tableau FROM tableaux WHERE id_groupe={0}'.format(id_groupe)
    if periode != -1:
        commandLine = '{0} AND (Periode={1} OR Periode=0)'.format(commandLine, periode)
    if publicOnly:
        commandLine = '{0} AND Public=1'.format(commandLine)
    query_my = utils_db.queryExecute(commandLine, db=main.db_my)
    while query_my.next():
        result.append(int(query_my.value(0)))
    return result

def matiereFromGroupe(main, id_groupe):
    matiereName = ''
    commandLine_my = utils_db.q_selectAllFromWhere.format(
        'groupes', 'id_groupe', id_groupe)
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    if query_my.first():
        matiereName = query_my.value(2)
    return matiereName

def profilsFromGroupe(main, id_groupe):
    result = []
    commandLine_my = (
        'SELECT DISTINCT profil_who.id_profil, profil_who.Periode, profil_who.profilType '
        'FROM profil_who '
        'LEFT JOIN groupe_eleve ON profil_who.id_who=groupe_eleve.id_eleve '
        'WHERE '
        '(profil_who.profilType="GROUPS" AND profil_who.id_who={0}) '
        'OR '
        '(profil_who.profilType="STUDENTS" AND groupe_eleve.id_groupe={0})')
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        id_profil = int(query_my.value(0))
        periode = int(query_my.value(1))
        profilType = query_my.value(2)
        if profilType == 'GROUPS':
            result.append((id_profil, periode, 1))
        else:
            result.append((id_profil, periode, -1))
    return result













###########################################################"
#        GESTION DES PROFILS
###########################################################"

class ManageProfilesDlg(QtWidgets.QDialog):
    """
    pour gérer les profils et les bilans associés à chacun.
    """
    def __init__(self, parent=None, matiereName=''):
        super(ManageProfilesDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Manage profiles (which will be on the bulletins)'))

        self.waiting = True
        self.boldFont = QtGui.QFont()
        self.boldFont.setBold(True)
        self.profils = {
            'mustSave': False, 
            'ALL': [], 
            'DEFAULT': -999, 
            'GROUPS': [], 
            'STUDENTS': [], 
            'BILANS': {}, 
            'DATA': {}, 
            'ASSIGNED': {'GROUPS': {}, 'STUDENTS': {}}, 
            }
        self.who = {'GROUPS': [], 'STUDENTS': [], 'profilType': ''}

        # sélection de la matière.
        # On vérifie la matière à présélectionner.
        # Pour le comboBox, on ne gardera
        # que les matières du bulletin et la Vie scolaire
        # pour lesquelles un groupe existe :
        matieresThatHasGroup = []
        commandLine_my = 'SELECT DISTINCT Matiere FROM groupes'
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            matieresThatHasGroup.append(query_my.value(0))
        self.matiereName = matiereName
        matiereTitle = '<b>' + QtWidgets.QApplication.translate('main', 'Subject:') + '</b>'
        matiereLabel = QtWidgets.QLabel(matiereTitle)
        self.matiereComboBox = QtWidgets.QComboBox()
        self.matiereComboBox.setMinimumWidth(200)
        for matiereCode in self.main.me['MATIERES']['BLT']:
            matiereName = self.main.me['MATIERES']['Code2Matiere'].get(
                matiereCode, (0, '', ''))[1]
            if matiereName in matieresThatHasGroup:
                self.matiereComboBox.addItem(matiereName)
        matiereName = self.main.me['MATIERES']['Code2Matiere']['VS'][1]
        if matiereName in matieresThatHasGroup:
            self.matiereComboBox.addItem(matiereName)

        # sélection du profil et boutons
        # pour éditer, ajouter ou supprimer :
        self.profilSelectionWidget = QtWidgets.QListWidget()
        self.profilSelectionWidget.currentItemChanged.connect(self.profilChanged)
        self.editButton = QtWidgets.QPushButton(
            utils.doIcon('document-edit'), '')
        self.editButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Rename this profile'))
        self.editButton.clicked.connect(self.editProfil)
        self.addButton = QtWidgets.QPushButton(
            utils.doIcon('list-add'), '')
        self.addButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Create a new profile'))
        self.addButton.clicked.connect(self.addProfil)
        self.removeButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'), '')
        self.removeButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Delete this profile'))
        self.removeButton.clicked.connect(self.removeProfil)

        # sélection des couples (groupe ou élève) + période
        # liés au profil sélectionné :
        self.whoSelectionWidget = QtWidgets.QTreeWidget()
        self.whoSelectionWidget.setRootIsDecorated(False)
        self.whoSelectionWidget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        self.whoSelectionWidget.setColumnCount(utils.NB_PERIODES + 1)
        self.headers = [QtWidgets.QApplication.translate('main', 'Group'), ]
        for period in range(utils.NB_PERIODES):
            self.headers.append(utils.PERIODES[period])
            self.whoSelectionWidget.header().resizeSection(period + 1, 80)
        self.whoSelectionWidget.header().resizeSection(0, 200)
        self.whoSelectionWidget.setHeaderLabels(self.headers)
        self.whoSelectionWidget.header().setDefaultAlignment(QtCore.Qt.AlignCenter)

        # sélection des bilans du profil :
        limiteBLTPersoLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate(
            'main', 
            '<p align="center">'
            'The number of assessments posted in the bulletin is '
            '<b>limited to {0}</b> for each subject.'
            '<br/>If you choose some more, only the first {0} evaluated '
            'assessments will be posted (for each student).'
            '<br/>The balance whose names are displayed in bold are '
            'your personal balance.'
            '</p>').format(self.main.limiteBLTPerso))
        self.bilansSelectionWidget = utils_2lists.ProfilBilansWidget(self.main)
        self.bilansSelectionWidget.initBilans()

        # boutons du dialog :
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # mise en place de l'interface.
        # matière :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(matiereLabel)
        hLayout.addWidget(self.matiereComboBox)
        hLayout.addStretch(1)
        matiereGroupBox = QtWidgets.QGroupBox()
        matiereGroupBox.setLayout(hLayout)
        matiereGroupBox.setFlat(True)
        # profils :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.editButton)
        hLayout.addWidget(self.addButton)
        hLayout.addWidget(self.removeButton)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.profilSelectionWidget)
        vLayout.addLayout(hLayout)
        profilsGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Profiles'))
        profilsGroupBox.setLayout(vLayout)
        profilsGroupBox.setMaximumWidth(400)
        # groupes ou élèves associés :
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.whoSelectionWidget)
        whoGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate(
                'main', 'Groups or students that this profile is assigned'))
        whoGroupBox.setLayout(vLayout)
        # on rassemble horizontalement les partie profils et "who" :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(profilsGroupBox)
        hLayout.addWidget(whoGroupBox)
        profilsAndWhoGroupBox = QtWidgets.QGroupBox()
        profilsAndWhoGroupBox.setLayout(hLayout)
        profilsAndWhoGroupBox.setFlat(True)
        # les bilans :
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(limiteBLTPersoLabel)
        vLayout.addWidget(self.bilansSelectionWidget)
        bilansSelectionGroupBox = QtWidgets.QGroupBox()
        bilansSelectionGroupBox.setLayout(vLayout)
        bilansSelectionGroupBox.setFlat(True)
        # on met tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(matiereGroupBox, 0, 0)
        grid.addWidget(profilsAndWhoGroupBox, 1, 0)
        grid.addWidget(bilansSelectionGroupBox, 2, 0)
        grid.addWidget(buttonBox, 9, 0)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.show()

        # sélection de la matière initiale :
        if self.matiereComboBox.count() > 0:
            self.matiereComboBox.activated.connect(self.matiereChanged)
            index = self.matiereComboBox.findText(self.matiereName)
            if index == -1:
                index = 0
            self.matiereComboBox.setCurrentIndex(index)
            self.matiereChanged(index)
        QtCore.QTimer.singleShot(10, self.doResize)

    def doResize(self):
        firstColumnDefaultWidth = 200
        othersColumnsWidth = 80
        lastColumnWidth = self.whoSelectionWidget.columnWidth(
            self.whoSelectionWidget.columnCount() - 1)
        firstColumnWidth = firstColumnDefaultWidth + lastColumnWidth - othersColumnsWidth
        if firstColumnWidth > firstColumnDefaultWidth:
            self.whoSelectionWidget.header().resizeSection(0, firstColumnWidth)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-profils')

    def matiereChanged(self, index):
        """
        si on change de matières, il faut :
            * réinitialiser la liste des profils
            * réinitialiser la liste des groupes+périodes
            * sélectionner le premier profil
        """
        self.saveProfilChanges(self.profilSelectionWidget.currentItem())
        self.waiting = True
        self.matiereName = self.matiereComboBox.currentText()
        self.reloadProfils()
        self.updateWhoAndPeriods(mustReload=True)
        self.profilSelectionWidget.setCurrentRow(0)
        self.waiting = False

    def reloadProfils(self):
        """
        récupération des profils et remplissage de la liste.
        On récupère aussi les bilans associés à chaque profil
        et on cherche les profils identiques.
        """
        self.profils = {
            'mustSave': False, 
            'ALL': [], 
            'DEFAULT': -999, 
            'GROUPS': [], 
            'STUDENTS': [], 
            'BILANS': {}, 
            'DATA': {}, 
            'ASSIGNED': {'GROUPS': {}, 'STUDENTS': {}}, 
            }
        # on récupère d'abord tous les id quelquesoit la matière :
        commandLine_my = utils_db.q_selectAllFrom.format('profils')
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_profil = int(query_my.value(0))
            self.profils['ALL'].append(id_profil)
        commandLine_my = utils_functions.u(
            'SELECT profils.*, profil_bilan_BLT.*, profil_who.* '
            'FROM profils '
            'LEFT JOIN profil_bilan_BLT ON profil_bilan_BLT.id_profil=profils.id_profil '
            'LEFT JOIN profil_who ON profil_who.id_profil=profils.id_profil '
            'WHERE profils.Matiere="{0}" '
            'ORDER BY profils.id_profil, profil_bilan_BLT.ordre').format(self.matiereName)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            # données du profil :
            id_profil = int(query_my.value(0))
            name = query_my.value(1)
            profilType = query_my.value(3)
            # récupération des bilans associés :
            if not(id_profil in self.profils['BILANS']):
                self.profils['BILANS'][id_profil] = []
            try:
                id_bilan = int(query_my.value(5))
                if not(id_bilan in self.profils['BILANS'][id_profil]):
                    self.profils['BILANS'][id_profil].append(id_bilan)
            except:
                pass
            # on distingue en fonction du type de profil :
            if id_profil < 0:
                # c'est le profil par défaut de la matière :
                if self.profils['DEFAULT'] == -999:
                    self.profils['DEFAULT'] = id_profil
            elif profilType == 'GROUPS':
                # c'est un profil standard :
                if not(id_profil in self.profils['GROUPS']):
                    self.profils['GROUPS'].append(id_profil)
                    self.profils['DATA'][id_profil] = {
                        'name': name, 
                        'who': []}
                # try-except est en cas de profil non attribué :
                try:
                    id_groupe = int(query_my.value(7))
                    period = int(query_my.value(9))
                    whoText = '{0}|{1}'.format(id_groupe, period)
                    if not(whoText in self.profils['DATA'][id_profil]['who']):
                        self.profils['DATA'][id_profil]['who'].append(whoText)
                except:
                    pass
            else:
                # profil attribué à des élèves :
                if not(id_profil in self.profils['STUDENTS']):
                    self.profils['STUDENTS'].append(id_profil)
                    self.profils['DATA'][id_profil] = {
                        'name': name, 
                        'who': []}
                # try-except est en cas de profil non attribué :
                try:
                    id_eleve = int(query_my.value(7))
                    period = int(query_my.value(9))
                    whoText = '{0}|{1}'.format(id_eleve, period)
                    if not(whoText in self.profils['DATA'][id_profil]['who']):
                        self.profils['DATA'][id_profil]['who'].append(whoText)
                        self.profils['ASSIGNED']['STUDENTS'][whoText] = id_profil
                except:
                    pass
        # on s'assure que le profil par défaut existera :
        if self.profils['DEFAULT'] == -999:
            i = -1
            while (i in self.profils['ALL']):
                i -= 1
            self.profils['DEFAULT'] = i
            self.profils['BILANS'][i] = []
        # mise à jour des groupes et élèves attribués :
        for id_profil in self.profils['GROUPS']:
            for whoText in self.profils['DATA'][id_profil]['who']:
                self.profils['ASSIGNED']['GROUPS'][whoText] = id_profil
        # et on recharge la liste des profils :
        self.updateProfils()

    def updateProfils(self):
        """
        remplissage de la liste des profils.
        D'après le dico self.profils.
        """
        self.profilSelectionWidget.clear()
        # profil par défaut de la matière :
        id_profil = self.profils['DEFAULT']
        itemName = QtWidgets.QApplication.translate(
            'main', 'Default profile for this subject')
        item = QtWidgets.QListWidgetItem(
            utils_functions.u('   {0}').format(itemName))
        item.setData(
            QtCore.Qt.UserRole, ['DEFAULT', id_profil])
        self.profilSelectionWidget.addItem(item)
        # profils standards des groupes :
        item = QtWidgets.QListWidgetItem()
        item.setFlags(QtCore.Qt.NoItemFlags)
        hLine = QtWidgets.QFrame()
        hLine.setFrameShape(QtWidgets.QFrame.HLine)
        self.profilSelectionWidget.addItem(item)
        self.profilSelectionWidget.setItemWidget(item, hLine)
        item = QtWidgets.QListWidgetItem(
            QtWidgets.QApplication.translate(
                'main', 'STANDARD PROFILES FOR GROUPS:'))
        item.setFont(self.boldFont)
        item.setTextAlignment(QtCore.Qt.AlignHCenter)
        item.setFlags(QtCore.Qt.NoItemFlags)
        self.profilSelectionWidget.addItem(item)
        for id_profil in self.profils['GROUPS']:
            itemName = self.profils['DATA'][id_profil]['name']
            item = QtWidgets.QListWidgetItem(
                utils_functions.u('   {0}').format(itemName))
            item.setData(
                QtCore.Qt.UserRole, ['GROUPS', id_profil])
            self.profilSelectionWidget.addItem(item)
        # profils attribués à des élèves :
        item = QtWidgets.QListWidgetItem()
        item.setFlags(QtCore.Qt.NoItemFlags)
        hLine = QtWidgets.QFrame()
        hLine.setFrameShape(QtWidgets.QFrame.HLine)
        self.profilSelectionWidget.addItem(item)
        self.profilSelectionWidget.setItemWidget(item, hLine)
        item = QtWidgets.QListWidgetItem(
            QtWidgets.QApplication.translate(
                'main', 'SPECIFIC PROFILES FOR STUDENTS:'))
        item.setFont(self.boldFont)
        item.setTextAlignment(QtCore.Qt.AlignHCenter)
        item.setFlags(QtCore.Qt.NoItemFlags)
        self.profilSelectionWidget.addItem(item)
        for id_profil in self.profils['STUDENTS']:
            itemName = self.profils['DATA'][id_profil]['name']
            item = QtWidgets.QListWidgetItem(
                utils_functions.u('   {0}').format(itemName))
            item.setData(
                QtCore.Qt.UserRole, ['STUDENTS', id_profil])
            self.profilSelectionWidget.addItem(item)

    def updateWhoAndPeriods(self, mustReload=False, profilType='GROUPS'):
        """
        remplissage de la liste des couples (groupe ou élève) + période.
        """
        if mustReload:
            self.who = {'GROUPS': [], 'STUDENTS': [], 'profilType': ''}
            commandLine_my = utils_functions.u(
                'SELECT * FROM groupes '
                'WHERE Matiere=? '
                'ORDER BY ordre, Name')
            query_my = utils_db.queryExecute(
                {commandLine_my: (self.matiereName, )}, db=self.main.db_my)
            while query_my.next():
                id_groupe = int(query_my.value(0))
                groupName = query_my.value(1)
                self.who['GROUPS'].append((id_groupe, groupName))

            students = {'LIST': [], 'groups': {'LIST': []}}
            commandLine_my = utils_functions.u(
                'SELECT groupe_eleve.*, groupes.* '
                'FROM groupe_eleve '
                'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
                'WHERE groupes.Matiere=? '
                'AND groupe_eleve.ordre!=-1 '
                'ORDER BY groupes.ordre, groupe_eleve.ordre')
            query_my = utils_db.queryExecute(
                {commandLine_my: (self.matiereName, )}, query=query_my)
            while query_my.next():
                id_groupe = int(query_my.value(0))
                id_eleve = int(query_my.value(1))
                students['LIST'].append(id_eleve)
                if not(id_groupe in students['groups']):
                    groupName = query_my.value(4)
                    students['groups']['LIST'].append(id_groupe)
                    students['groups'][id_groupe] = [groupName, id_eleve]
                else:
                    students['groups'][id_groupe].append(id_eleve)
            # on récupère les noms des élèves :
            id_students = utils_functions.array2string(students['LIST'])
            commandLine_users = utils_db.qs_users_elevesWhereId.format(id_students)
            query_users = utils_db.queryExecute(commandLine_users, db=self.main.db_users)
            while query_users.next():
                id_eleve = int(query_users.value(0))
                name = utils_functions.u('{0} {1}').format(
                    query_users.value(1), query_users.value(2))
                students[id_eleve] = name
            # on se base sur l'ordre de self.who['GROUPS'] 
            # (les groupes ne sont pas forcément ordonnés) :
            for (id_groupe, groupName) in self.who['GROUPS']:
                if id_groupe in students['groups']['LIST']:
                    groupName = students['groups'][id_groupe][0]
                    self.who['STUDENTS'].append((-1, groupName))
                    for id_eleve in students['groups'][id_groupe][1:]:
                        # au cas où il y aurait une différence avec la base users 
                        # (genre nouvel élève et autre poste pas à jour) :
                        name = students.get(id_eleve, '')
                        self.who['STUDENTS'].append((id_eleve, name))
        # mise à jour partielle de la sélection 
        # des groupes ou élèves :
        if profilType != self.who['profilType']:
            self.who['profilType'] = profilType
            self.whoSelectionWidget.clear()
            if profilType == 'GROUPS':
                self.headers[0] = QtWidgets.QApplication.translate('main', 'Group')
                self.whoSelectionWidget.setHeaderLabels(self.headers)
            elif profilType == 'STUDENTS':
                self.headers[0] = QtWidgets.QApplication.translate('main', 'Student')
                self.whoSelectionWidget.setHeaderLabels(self.headers)

    def profilChanged(self, current, previous):
        """
        changement de sélection de profil.
        On met à jour les sélections des autres listes.
        """
        self.saveProfilChanges(previous)
        try:
            currentData = current.data(QtCore.Qt.UserRole)
        except:
            return
        profilType = currentData[0]
        id_profil = currentData[1]
        # affichage des groupes ou élèves liés :
        self.updateWhoAndPeriods(profilType=profilType)
        if profilType == 'DEFAULT':
            self.whoSelectionWidget.setEnabled(False)
            self.editButton.setEnabled(False)
            self.removeButton.setEnabled(False)
        elif profilType in ('GROUPS', 'STUDENTS'):
            self.whoSelectionWidget.setEnabled(True)
            self.editButton.setEnabled(True)
            self.removeButton.setEnabled(True)
            self.whoSelectionWidget.clear()
            for (id_who, whoName) in self.who[profilType]:
                item = QtWidgets.QTreeWidgetItem()
                item.setData(0, QtCore.Qt.DisplayRole, whoName)
                item.setData(0, QtCore.Qt.UserRole, id_who)
                if id_who < 0:
                    item.setData(0, QtCore.Qt.TextAlignmentRole, QtCore.Qt.AlignHCenter)
                    item.setData(0, QtCore.Qt.FontRole, self.boldFont)
                else:
                    for period in range(utils.NB_PERIODES):
                        whoText = '{0}|{1}'.format(id_who, period)
                        item.setData(period + 1, QtCore.Qt.UserRole, whoText)
                        if whoText in self.profils['DATA'][id_profil]['who']:
                            item.setCheckState(period + 1, QtCore.Qt.Checked)
                        elif not(whoText in self.profils['ASSIGNED'][profilType]):
                            item.setCheckState(period + 1, QtCore.Qt.Unchecked)
                self.whoSelectionWidget.addTopLevelItem(item)
        # affichages des bilans liés :
        self.bilansSelectionWidget.remplirListes(
            self.profils['BILANS'][id_profil])

    def editProfil(self):
        current = self.profilSelectionWidget.currentItem()
        try:
            currentData = current.data(QtCore.Qt.UserRole)
        except:
            return
        profilType = currentData[0]
        id_profil = currentData[1]
        oldName = self.profils['DATA'][id_profil]['name']
        newName, OK = QtWidgets.QInputDialog.getText(
            self.main, utils.PROGNAME, 
            QtWidgets.QApplication.translate('main', 'Rename a profile'),
            QtWidgets.QLineEdit.Normal, oldName)
        if not(OK):
            return
        if newName != oldName:
            self.profils['DATA'][id_profil]['name'] = newName
            current.setText(
                utils_functions.u('   {0}').format(newName))
            self.profils['mustSave'] = True

    def addProfil(self):
        dialog = NewProfilDlg(self.main)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        name = dialog.nameEdit.text()
        if dialog.groupsRadio.isChecked():
            profilType = 'GROUPS'
        else:
            profilType = 'STUDENTS'
        # on cherche un id_profil :
        id_profil = 0
        while id_profil in self.profils['ALL']:
            id_profil += 1
        self.profils['ALL'].append(id_profil)
        self.profils[profilType].append(id_profil)
        self.profils['BILANS'][id_profil] = []
        self.profils['DATA'][id_profil] = {'name': name, 'who': []}
        self.profils['mustSave'] = True
        # on recharge la liste des profils :
        self.updateProfils()
        self.profilSelectionWidget.setCurrentRow(0)

    def removeProfil(self):
        current = self.profilSelectionWidget.currentItem()
        try:
            currentData = current.data(QtCore.Qt.UserRole)
        except:
            return
        profilType = currentData[0]
        if not(profilType in ('GROUPS', 'STUDENTS')):
            return
        self.waiting = True
        id_profil = currentData[1]
        self.profils['ALL'].remove(id_profil)
        if id_profil in self.profils[profilType]:
            self.profils[profilType].remove(id_profil)
        if id_profil in self.profils['BILANS']:
            del self.profils['BILANS'][id_profil]
        if id_profil in self.profils['DATA']:
            del self.profils['DATA'][id_profil]
        whoTexts = [
            whoText for whoText in self.profils['ASSIGNED'][profilType]
            if self.profils['ASSIGNED'][profilType][whoText] == id_profil
            ]
        for whoText in whoTexts:
            del self.profils['ASSIGNED'][profilType][whoText]
        self.profils['mustSave'] = True
        self.saveChanges()
        self.profilSelectionWidget.takeItem(self.profilSelectionWidget.row(current))
        self.waiting = False
        # on recharge la liste des profils :
        #self.updateProfils()
        self.profilSelectionWidget.setCurrentRow(0)

    def saveProfilChanges(self, item):
        """
        on teste si les sélections ou les bilans
        liés au profil ont été modifiés.
        """
        if self.waiting:
            return
        try:
            itemData = item.data(QtCore.Qt.UserRole)
        except:
            return
        profilType = itemData[0]
        id_profil = itemData[1]
        # test des sélections liées :
        if profilType in ('GROUPS', 'STUDENTS'):
            who = []
            parent = self.whoSelectionWidget.invisibleRootItem()
            for i in range(parent.childCount()):
                whoItem = parent.child(i)
                id_who = whoItem.data(0, QtCore.Qt.UserRole)
                if id_who > -1:
                    for period in range(utils.NB_PERIODES):
                        if whoItem.checkState(period + 1) == QtCore.Qt.Checked:
                            whoText = '{0}|{1}'.format(id_who, period)
                            who.append(whoText)
            if who != self.profils['DATA'][id_profil]['who']:
                self.profils['DATA'][id_profil]['who'] = who
                whoTexts = [
                    whoText for whoText in self.profils['ASSIGNED'][profilType]
                    if self.profils['ASSIGNED'][profilType][whoText] == id_profil
                    ]
                for whoText in whoTexts:
                    del self.profils['ASSIGNED'][profilType][whoText]
                for whoText in self.profils['DATA'][id_profil]['who']:
                    self.profils['ASSIGNED'][profilType][whoText] = id_profil
                self.profils['mustSave'] = True
        # test des bilans liés :
        bilans = []
        for i in range(self.bilansSelectionWidget.selectionList.count()):
            bilanItem = self.bilansSelectionWidget.selectionList.item(i)
            id_bilan = bilanItem.data(QtCore.Qt.UserRole)
            bilans.append(id_bilan)
        if bilans != self.profils['BILANS'][id_profil]:
            self.profils['BILANS'][id_profil] = bilans
            self.profils['mustSave'] = True
        if self.profils['mustSave']:
            self.saveChanges()

    def saveChanges(self):
        """
        réécriture des tables.
        on efface tous les profils de la matière
        avant de les réécrire.
        """
        query_my = utils_db.query(self.main.db_my)
        # on recherche les profils liés à cette matière :
        id_profils = []
        commandLine_my = utils_functions.u(
            'SELECT * FROM profils WHERE Matiere=?')
        query_my = utils_db.queryExecute(
            {commandLine_my: (self.matiereName, )}, 
            query=query_my)
        while query_my.next():
            id_profils.append(int(query_my.value(0)))
        id_profils = utils_functions.array2string(id_profils)
        # on vide les 3 tables :
        listArgs = []
        for table in ['profils', 'profil_who', 'profil_bilan_BLT']:
            listArgs.append(utils_db.qdf_where.format(table, 'id_profil', id_profils))
        if len(listArgs) > 0:
            query_my = utils_db.queryExecute(listArgs, query=query_my)
        lines = {'profils': [], 'profil_who': [], 'profil_bilan_BLT': []}
        # profil par défaut :
        id_profil = self.profils['DEFAULT']
        lines['profils'].append((id_profil, 'DEFAULT', self.matiereName, 'DEFAULT'))
        ordre = 0
        for id_bilan in self.profils['BILANS'][id_profil]:
            lines['profil_bilan_BLT'].append((id_profil, id_bilan, ordre))
            ordre += 1
        # profils standards :
        for id_profil in self.profils['GROUPS']:
            name = self.profils['DATA'][id_profil]['name']
            lines['profils'].append((id_profil, name, self.matiereName, 'GROUPS'))
            ordre = 0
            for id_bilan in self.profils['BILANS'][id_profil]:
                lines['profil_bilan_BLT'].append((id_profil, id_bilan, ordre))
                ordre += 1
            for whoText in self.profils['DATA'][id_profil]['who']:
                who = whoText.split('|')
                id_who = who[0]
                period = who[1]
                lines['profil_who'].append((id_who, id_profil, period, 'GROUPS'))
        # profils d'élèves :
        for id_profil in self.profils['STUDENTS']:
            name = self.profils['DATA'][id_profil]['name']
            lines['profils'].append((id_profil, name, self.matiereName, 'STUDENTS'))
            ordre = 0
            for id_bilan in self.profils['BILANS'][id_profil]:
                lines['profil_bilan_BLT'].append((id_profil, id_bilan, ordre))
                ordre += 1
            for whoText in self.profils['DATA'][id_profil]['who']:
                who = whoText.split('|')
                id_who = who[0]
                period = who[1]
                lines['profil_who'].append((id_who, id_profil, period, 'STUDENTS'))
        # on inscrit les enregistrements :
        for table in lines:
            commandLine_my = utils_db.insertInto(table)
            query_my = utils_db.queryExecute(
                {commandLine_my: lines[table]}, query=query_my)
        self.profils['mustSave'] = False


class NewProfilDlg(QtWidgets.QDialog):
    """
    Pour éditer les profils de bulletin
    """
    def __init__(self, parent=None):
        super(NewProfilDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Create a new profile'))

        # nom du profil :
        nameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Profile name:')))
        self.nameEdit = QtWidgets.QLineEdit()

        # type de profil :
        profilTypeLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Profile type:')))
        profilTypeGroupBox = QtWidgets.QGroupBox()
        self.groupsRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'standard profile for groups'))
        self.studentsRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'specific profile for students'))
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.groupsRadio)
        vLayout.addWidget(self.studentsRadio)
        vLayout.addStretch(1)
        profilTypeGroupBox.setLayout(vLayout)
        self.groupsRadio.setChecked(True)

        # boutons du dialog :
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(nameLabel, 1, 0)
        grid.addWidget(self.nameEdit, 1, 1, 1, 2)
        grid.addWidget(profilTypeLabel, 4, 0)
        grid.addWidget(profilTypeGroupBox, 4, 1, 1, 2)
        grid.addWidget(buttonBox, 5, 0, 1, 3)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-profils')



