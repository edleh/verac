# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Variables, classes et fonctions utilisées dans l'interface prof.
"""

# importation des modules utiles :
from __future__ import division, print_function

import utils, utils_functions, utils_db, utils_web, utils_filesdirs, utils_calculs
if utils.PYTHONVERSION >= 30:
    import utils_spell
import utils_2lists
import prof_groupes

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



###########################################################"
#        DEVENIR ADMIN
#        (CRÉER ÉTAB OU VERSION PERSO)
###########################################################"

class VeracCreateEtabDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour sélectionner le dossier verac_admin.
    On peut aussi le télécharger.
    """
    def __init__(self, parent=None,
                 helpContextPage='devenir-admin', helpBaseUrl='admin'):
        """
        mise en place de l'interface
        """
        super(VeracCreateEtabDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'CreateEtab'))

        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl

        self.veracAdminDir = ''

        # le message d'explication si on a déjà verac_admin :
        message = QtWidgets.QApplication.translate(
            'main', 
            'If you already have a <b>verac_admin</b> folder '
            '<br/>configured for your school, '
            '<br/>click the <b>Continue</b> button to select it.')
        message = utils_functions.u('<p align="center">{0}</p>').format(message)
        labelContinue = QtWidgets.QLabel(message)
        labelSep1 = QtWidgets.QLabel(
            '<p align="center">{0}<br/></p>'.format(utils.SEPARATOR_LINE))

        # le bouton Continue :
        continueButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), 
            QtWidgets.QApplication.translate('main', 'Continue'))
        continueButton.clicked.connect(self.doContinue)
        # le message d'explication si on n'a pas verac_admin :
        message = QtWidgets.QApplication.translate('main', 
            'Otherwise, fill in the 2 fields below, '
            '<br/>then click the <b>DownloadVeracAdmin</b> button.')
        message = utils_functions.u('<p align="center">{0}<br/></p>').format(message)
        labelDownload = QtWidgets.QLabel(message)
        labelSep2 = QtWidgets.QLabel(
            '<p align="center">{0}<br/></p>'.format(utils.SEPARATOR_LINE))
        # les labels et lineEdits pour les premiers réglages :
        text = QtWidgets.QApplication.translate('main', 'Full name of the school:')
        versionLabelLabel = QtWidgets.QLabel(utils_functions.u('<b>{0}</b>').format(text))
        self.versionLabelEdit = QtWidgets.QLineEdit()
        self.versionLabelEdit.setMinimumWidth(200)
        text = QtWidgets.QApplication.translate('main', 'SchoolShortName:')
        versionNameLabel = QtWidgets.QLabel(utils_functions.u('<b>{0}</b>').format(text))
        self.versionNameEdit = QtWidgets.QLineEdit()
        regExp = QtCore.QRegExp('[a-z]*')
        self.versionNameEdit.setValidator(
            QtGui.QRegExpValidator(regExp, self.versionNameEdit))
        # le message "aide" :
        message = QtWidgets.QApplication.translate('main', 
            'Click <b>Help</b> button for more details.')
        message = utils_functions.u('<p align="center">{0}<br/></p>').format(message)
        labelHelp = QtWidgets.QLabel(message)
        # le bouton DownloadVeracAdmin :
        downloadVeracAdminButton = QtWidgets.QPushButton(
            utils.doIcon('folder-remote'), 
            QtWidgets.QApplication.translate('main', 'DownloadVeracAdmin'))
        downloadVeracAdminButton.clicked.connect(self.downloadVeracAdmin)
        #le bouton annuler:
        cancelButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-cancel'), 
            QtWidgets.QApplication.translate('main', 'Cancel'))
        cancelButton.clicked.connect(self.close)
        #le bouton aide:
        helpButton = QtWidgets.QPushButton(utils.doIcon('help'), 
            QtWidgets.QApplication.translate('main', 'Help'))
        helpButton.setShortcut(
            QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        helpButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Opens contextual help in your browser'))
        helpButton.clicked.connect(self.contextHelp)
        #une boîte des boutons:
        buttonBox = QtWidgets.QDialogButtonBox()
        buttonBox.addButton(continueButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(downloadVeracAdminButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(cancelButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(helpButton, QtWidgets.QDialogButtonBox.ActionRole)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(labelContinue,               1, 0, 1, 2)
        grid.addWidget(labelSep1,                   2, 0, 1, 2)
        grid.addWidget(labelDownload,               3, 0, 1, 2)
        grid.addWidget(versionLabelLabel,           4, 0)
        grid.addWidget(self.versionLabelEdit,       4, 1)
        grid.addWidget(versionNameLabel,            5, 0)
        grid.addWidget(self.versionNameEdit,        5, 1)
        grid.addWidget(labelSep2,                   6, 0, 1, 2)
        grid.addWidget(labelHelp,                   7, 0, 1, 2)
        grid.addWidget(buttonBox,                   9, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        #self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, self.helpBaseUrl)

    def doContinue(self):
        self.veracAdminDir = ''
        # sélection du dossier verac_admin :
        title = QtWidgets.QApplication.translate(
            'main', 'Select the verac_admin directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            title, 
            self.main.workDir, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            self.veracAdminDir = directory
            baseName = QtCore.QFileInfo(directory).baseName()
            if baseName == 'verac_admin':
                self.accept()

    def downloadVeracAdmin(self):
        self.veracAdminDir = ''
        # sélection du dossier qui contiendra verac_admin :
        title = QtWidgets.QApplication.translate(
            'main', 'Select where you want to create the verac_admin directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            title, 
            self.main.workDir, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        utils_functions.doWaitCursor()
        try:
            self.veracAdminDir = directory + '/verac_admin'
            # téléchargement dans temp :
            theFileName = "verac_admin.tar.gz"
            destDir = self.main.tempPath + "/down/"
            theUrl = utils.UPDATEURL
            downLoader = utils_web.UpDownLoader(
                self.main, theFileName, destDir, theUrl, down=True, showProgress=True)
            downLoader.launch()
            while downLoader.state == utils_web.STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if downLoader.state == utils_web.STATE_OK:
                OK = 1
                # on décompresse le fichier tar.gz dans le dossier sélectionné :
                tempFile = self.main.tempPath + "/down/" + theFileName
                path = directory
                import utils_filesdirs
                if utils_filesdirs.untarDirectory(tempFile, path):
                    OK = 2
                    utils_functions.afficheMessage(self.main, 'FICHIER DECOMPRESSE')
            if OK == 2:
                # on doit encore modifier le fichier config.csv avec les réglages donnés :
                fileName = self.veracAdminDir + '/csv/commun_tables/config.csv'
                # on récupère les lignes du fichier csv :
                import utils_export
                headers, datas = utils_export.csv2List(fileName)
                for row in datas:
                    if row[0] == 'versionName':
                        row[2] = self.versionNameEdit.text()
                    elif row[0] == 'versionLabel':
                        row[2] = self.versionLabelEdit.text()
                    elif row[0] == 'prefixProfFiles':
                        row[2] = self.versionNameEdit.text()
                import csv
                if utils.PYTHONVERSION >= 30:
                    theFile = open(fileName, 'w', newline='', encoding='utf-8')
                else:
                    theFile = open(fileName, 'wb')
                writer = csv.writer(
                    theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
                writer.writerow(["key_name", "value_int", "value_text"])
                for row in datas:
                    ligne = [utils_functions.s(row[0]), row[1], utils_functions.s(row[2])]
                    writer.writerow(ligne)
                theFile.close()
                OK = 3

        finally:
            utils_functions.restoreCursor()
        if OK == 3:
            self.accept()


def createEtab(main):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'createEtab')

    dialog = VeracCreateEtabDlg(parent=main)
    reponse = dialog.exec_()
    if reponse != QtWidgets.QDialog.Accepted:
        return

    directory = dialog.veracAdminDir

    # création de la table verac_admin/ftp/secret/verac/public/commun.sqlite:config
    # d'après le fichier verac_admin/csv/commun_tables/config.csv :
    # (on travaille dans temp pour fonctionner en réseau)
    # pour ne pas interférer avec la base commun actuelle,
    # on nomme celle-ci communEtab:
    dbFile_commun = directory + '/ftp/secret/verac/public/commun.sqlite'
    dbFileTemp_commun = main.tempPath + '/communEtab.sqlite'
    utils_filesdirs.removeAndCopy(dbFile_commun, dbFileTemp_commun)
    fileName = directory + "/csv/commun_tables/config.csv"
    utils_functions.doWaitCursor()
    try:
        # ouverture de la base commun :
        query, db, dbName, transactionOK = utils_db.queryTransactionFile(
            main, dbFileTemp_commun)
        # lecture du fichier csv:
        import utils_export
        headers, datas = utils_export.csv2List(fileName)
        # on vide l'ancienne table:
        commandLine = utils_db.qdf_table.format('config')
        utils_functions.afficheMessage(main, commandLine, statusBar=True)
        query = utils_db.queryExecute(commandLine, query=query)
        # on la recrée:
        commandLine = utils_db.qct_config
        query = utils_db.queryExecute(commandLine, query=query)
        # et on la remplit:
        commandLine = utils_db.insertInto('config', 3)
        rowId = 0
        lines = []
        for row in datas:
            key_name = utils_functions.u(row[0])
            try:
                value_int = int(row[1])
            except:
                value_int = ''
            value_text = utils_functions.u(row[2])
            lines.append((key_name, value_int, value_text))
            rowId = rowId + 1
            if row[0] == 'siteUrlPublic':
                versionUrl = row[2]
            elif row[0] == 'prefixProfFiles':
                prefixProfFiles = row[2]
            elif row[0] == 'versionName':
                versionName = row[2]
            elif row[0] == 'versionLabel':
                versionLabel = row[2]
        query = utils_db.queryExecute({commandLine: lines}, query=query)

    finally:
        # on termine avec la transaction:
        utils_db.endTransaction(query, db, transactionOK)
        db.close()
        del db
        utils_db.sqlDatabase.removeDatabase(dbName)
        utils_filesdirs.removeAndCopy(dbFileTemp_commun, dbFile_commun)
        utils_functions.restoreCursor()

    # On crée le dossier versionName dans .Verac, et on le remplit:
    utils_filesdirs.createDirs(main.localConfigDir, versionName)
    originFile = directory + '/ftp/secret/verac/public/users.sqlite'
    destFile = main.localConfigDir + '/' + versionName + '/users.sqlite'
    utils_filesdirs.removeAndCopy(originFile, destFile)
    originFile = directory + '/ftp/secret/verac/public/commun.sqlite'
    destFile = main.localConfigDir + '/' + versionName + '/commun.sqlite'
    utils_filesdirs.removeAndCopy(originFile, destFile)

    # inscription de l'établissement
    # dans la base de configuration
    # .Verac/config.sqlite:
    # on supprime si déjà là:
    commandLine = utils_db.qdf_whereText.format(
        'versions', 'name', versionName)
    query = utils_db.queryExecute(commandLine, db=main.db_localConfig)

    # on récupère une id_version valable:
    commandLine = utils_db.q_selectAllFrom.format('versions')
    query = utils_db.queryExecute(commandLine, query=query)
    maxId = 2
    while query.next():
        id_version = int(query.value(0))
        if id_version > maxId:
            maxId = id_version
    id_version = maxId + 1
    # on inscrit l'établissement dans la base 
    # .Verac/config.sqlite:versions:
    commandLine = utils_db.insertInto('versions', 4)
    query = utils_db.queryExecute(
        {commandLine: (id_version, versionName, versionUrl, versionLabel)}, 
        query=query)

    # et dans .Verac/config.sqlite:config
    # pour activer le bouton admin
    # on supprime les traces d'une ancienne version:
    commandLine = utils_db.qdf_whereText.format(
        'config', 'key_name', versionName)
    query = utils_db.queryExecute(commandLine, query=query)
    commandLine = utils_db.insertInto('config', 3)
    query = utils_db.queryExecute(
        {commandLine: (versionName, '', directory)}, 
        query=query)

    message = QtWidgets.QApplication.translate(
        'main', 'You should start again the software.')
    utils_functions.afficheMsgFin(main, message=message)
    return True


class VeracAdminDirDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour sélectionner le dossier verac_admin.
    On peut aussi le télécharger.
    """
    def __init__(self, parent=None, 
                 helpContextPage='perso-administrer', helpBaseUrl='prof'):
        """
        mise en place de l'interface
        """
        super(VeracAdminDirDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'VeracAdminDir'))

        self.helpContextPage = helpContextPage

        self.veracAdminDir = ''

        # l'icone :
        taille = 64
        iconLabel = QtWidgets.QLabel()
        iconLabel.setMaximumSize(taille, taille)
        iconLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        iconLabel.setPixmap(
            utils.doIcon('dialog-warning', what='PIXMAP').scaled(
                taille, taille, 
                QtCore.Qt.KeepAspectRatio, 
                QtCore.Qt.SmoothTransformation))

        # le message d'explication :
        m1 = QtWidgets.QApplication.translate('main', 'Before continue:')
        m2 = QtWidgets.QApplication.translate(
            'main', 
            'Must sure you ave a <b>verac_admin</b> directory.'
            '<br/>You can also download it.')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>'
            '<p></p>'
            '<p align="center">{2}</p>').format(utils.SEPARATOR_LINE, m1, m2)
        label = QtWidgets.QLabel(message)

        # le bouton Continue :
        continueButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), 
            QtWidgets.QApplication.translate('main', 'Continue'))
        continueButton.clicked.connect(self.doContinue)
        # le bouton DownloadVeracAdmin :
        downloadVeracAdminButton = QtWidgets.QPushButton(
            utils.doIcon('folder-remote'), 
            QtWidgets.QApplication.translate('main', 'DownloadVeracAdmin'))
        downloadVeracAdminButton.clicked.connect(self.downloadVeracAdmin)
        #le bouton annuler:
        cancelButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-cancel'), 
            QtWidgets.QApplication.translate('main', 'Cancel'))
        cancelButton.clicked.connect(self.close)
        #le bouton aide:
        helpButton = QtWidgets.QPushButton(utils.doIcon('help'), 
            QtWidgets.QApplication.translate('main', 'Help'))
        helpButton.setShortcut(
            QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        helpButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Opens contextual help in your browser'))
        helpButton.clicked.connect(self.contextHelp)
        #une boîte des boutons:
        buttonBox = QtWidgets.QDialogButtonBox()
        buttonBox.addButton(continueButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(downloadVeracAdminButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(cancelButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(helpButton, QtWidgets.QDialogButtonBox.ActionRole)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(iconLabel, 1, 0)
        grid.addWidget(label, 1, 1)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        #self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage)

    def doContinue(self):
        self.veracAdminDir = ''
        # sélection du dossier verac_admin :
        title = QtWidgets.QApplication.translate('main', 'Select the verac_admin directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            title, 
            self.main.workDir, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            self.veracAdminDir = directory
            baseName = QtCore.QFileInfo(directory).baseName()
            if baseName == 'verac_admin':
                self.accept()

    def downloadVeracAdmin(self):
        self.veracAdminDir = ''
        # sélection du dossier qui contiendra verac_admin :
        title = QtWidgets.QApplication.translate(
            'main', 'Select where you want to create the verac_admin directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            title, 
            self.main.workDir, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        utils_functions.doWaitCursor()
        try:
            self.veracAdminDir = directory + '/verac_admin'
            # téléchargement dans temp :
            theFileName = 'verac_admin.tar.gz'
            destDir = self.main.tempPath + '/down/'
            theUrl = utils.UPDATEURL
            downLoader = utils_web.UpDownLoader(
                self.main, theFileName, destDir, theUrl, down=True, showProgress=True)
            downLoader.launch()
            while downLoader.state == utils_web.STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if downLoader.state == utils_web.STATE_OK:
                OK = 1
                # on décompresse le fichier tar.gz dans le dossier sélectionné :
                tempFile = self.main.tempPath + '/down/' + theFileName
                path = directory
                import utils_filesdirs
                if utils_filesdirs.untarDirectory(tempFile, path):
                    OK = 2
                    utils_functions.afficheMessage(self.main, 'FICHIER DECOMPRESSE')
        finally:
            utils_functions.restoreCursor()
        if OK == 2:
            self.accept()


def adminVersionPerso(main):
    """
    pour rendre l'administration possible en version perso
    """
    utils_functions.afficheMessage(main, 'adminVersionPerso')

    # on télécharge le dossier verac_admin :
    dialog = VeracAdminDirDlg(parent=main)
    reponse = dialog.exec_()
    if reponse != QtWidgets.QDialog.Accepted:
        return
    adminDir = dialog.veracAdminDir

    # on recopie les bases users et commun 
    # depuis /localConfigDir/perso 
    # vers /verac_admin/ftp/secret/verac/public :
    for database in ('users', 'commun'):
        originFile = utils_functions.u(
            '{0}/perso/{1}.sqlite').format(
                main.localConfigDir, database)
        destFile = utils_functions.u(
            '{0}/ftp/secret/verac/public/{1}.sqlite').format(
                adminDir, database)
        utils_filesdirs.removeAndCopy(originFile, destFile)

    import utils_export
    # on met à jour les fichiers classes.csv et referentiel.csv 
    # d'après la base commun :
    for table in ('classes', 'referentiel'):
        fileNameCsv = utils_functions.u(
            '{0}/csv/commun_tables/{1}.csv').format(adminDir, table)
        utils_export.exportTable2Csv(
            main, main.db_commun, table, fileNameCsv, msgFin=False)

    # on met à jour la table admin.eleves 
    # et le fichier eleves.csv d'après la base users.
    # Il faudra donc se connecter à la base admin.
    # On commence par récupérer les élèves depuis la base users :
    query_users = utils_db.query(main.db_users)
    eleves_users = utils_db.table2List('eleves', query=query_users)
    eleves_admin = []
    for eleve in eleves_users:
        eleve_admin = (eleve[0], '', eleve[1], eleve[2], eleve[3], '', '', '', '', -1, -1, -1, -1)
        eleves_admin.append(eleve_admin)
    dbFile_admin = utils_functions.u('{0}/admin.sqlite').format(adminDir)
    dbFileTemp_admin = utils_functions.u('{0}/admin.sqlite').format(main.tempPath)
    utils_filesdirs.removeAndCopy(dbFile_admin, dbFileTemp_admin)
    (db_admin, dbName) = utils_db.createConnection(main, dbFileTemp_admin)
    query, transactionOK = utils_db.queryTransactionDB(db_admin)
    # on recrée la table eleves :
    query = utils_db.queryExecute(utils_db.qdf_table.format('eleves'), query=query)
    commandLine = utils_db.insertInto('eleves', 13)
    query = utils_db.queryExecute({commandLine: eleves_admin}, query=query)
    fileNameCsv = utils_functions.u(
        '{0}/csv/admin_tables/eleves.csv').format(adminDir)
    utils_export.exportTable2Csv(
        main, db_admin, 'eleves', fileNameCsv, msgFin=False)
    # on termine la transaction :
    utils_db.endTransaction(query, db_admin, transactionOK)
    if query != None:
        query.clear()
        del query
    db_admin.close()
    del db_admin
    utils_db.sqlDatabase.removeDatabase(dbName)
    utils_filesdirs.removeAndCopy(dbFileTemp_admin, dbFile_admin)

    # inscription de l'établissement dans la base 
    # /localConfigDir/config.sqlite 
    # (tables versions et config) :
    commandLine = utils_db.qdf_whereText.format(
        'versions', 'name', main.actualVersion['versionName'])
    query = utils_db.queryExecute(commandLine, db=main.db_localConfig)
    commandLine = utils_db.insertInto('versions', 4)
    query = utils_db.queryExecute(
        {commandLine: (
            main.actualVersion['id_version'], 
            main.actualVersion['versionName'], 
            main.actualVersion['versionUrl'], 
            main.actualVersion['versionLabel'])}, 
        query=query)
    commandLine = utils_db.qdf_whereText.format(
        'config', 'key_name', main.actualVersion['versionName'])
    query = utils_db.queryExecute(commandLine, query=query)
    commandLine = utils_db.insertInto('config', 3)
    query = utils_db.queryExecute(
        {commandLine: (main.actualVersion['versionName'], '', adminDir)}, 
        query=query)

    # on demande si on change le dossier de travail de place
    # en on déplace persoprof1.sqlite si besoin :
    message = QtWidgets.QApplication.translate(
        'main',
        'Do you want to change workdir into admin_verac directory ?'
        '<br/> This will avoid to copy your personnal database into it '
        'before any action as admin.'
        '<p><b>WARNING : </b></p> workdir is updated for <b>ALL</b>'
        ' versions.')
    if utils_functions.messageBox(
        main, 
        level='question', 
        message=message, 
        buttons=['Yes', 'No']) == QtWidgets.QMessageBox.Yes:
        # on supprime la version actuelle
        QtCore.QFile.remove(main.dbFile_my)
        main.filesDir = utils_functions.u(
            '{0}/ftp/secret/verac/up/files').format(adminDir)
        main.dbName_my = 'persoprof1'
        main.dbFile_my = utils_functions.u(
            '{0}/{1}.sqlite').format(main.filesDir, main.dbName_my)
        utils_db.changeInConfigTable(main, main.db_localConfig, {'FilesDir': (0, main.filesDir)})
        import prof_db
        prof_db.saveBase_my(main)

    utils_functions.afficheMsgFin(main, timer=5)
    return True

def returnVersionPerso(main):
    """
    pour revenir à la gestion simple de la version perso
    """
    utils_functions.afficheMessage(main, 'returnVersionPerso')

    # on récupère adminDir et on l'efface de la base config.sqlite :
    adminDir = utils_db.readInConfigTable(main.db_localConfig, 'perso')[1]
    utils_db.deleteFromConfigTable(main, main.db_localConfig, ['perso', ])

    # on recopie le fichier persoprof1.sqlite dans le home :
    main.filesDir = QtCore.QDir.homePath()
    main.dbName_my = 'persoprof1'
    main.dbFile_my = utils_functions.u(
        '{0}/{1}.sqlite').format(main.filesDir, main.dbName_my)
    utils_db.changeInConfigTable(main, main.db_localConfig, {'FilesDir': (0, main.filesDir)})
    import prof_db
    prof_db.saveBase_my(main)

    # on efface le dossier adminDir :
    utils_filesdirs.emptyDir(adminDir)

    utils_functions.afficheMsgFin(main, timer=5)
    return True






















































###########################################################"
#        GESTION DES TABLEAUX
###########################################################"

class NewTableauDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, forEditOnly=False, canSelectItems=True):
        super(NewTableauDlg, self).__init__(parent)
        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Create a table'))

        # des variables :
        self.list_matieres = []
        self.list_groupes = {}
        self.dict_groupes = {}
        if canSelectItems:
            self.list_items = []
        self.waiting = False

        if not(forEditOnly):
            # un bouton pour rendre possible la gestion des groupes :
            manageGroupesButton = QtWidgets.QPushButton(
                utils.doIcon('database-eleve'), 
                QtWidgets.QApplication.translate(
                    'main', 'Manage groups of students'))
            manageGroupesButton.setToolTip(
                QtWidgets.QApplication.translate(
                    'main', 'Create or modify the groups of students'))
            manageGroupesButton.clicked.connect(self.manageGroupes)
            # la matière :
            labelText = utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Subject:'))
            matiereLabel = QtWidgets.QLabel(labelText)
            self.matieresComboBox = QtWidgets.QComboBox()
            self.matieresComboBox.setMinimumWidth(200)
            self.matieresComboBox.currentIndexChanged.connect(self.remplirGroupesComboBox)
            # le groupe :
            labelText = utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Group:'))
            groupeLabel = QtWidgets.QLabel(labelText)
            self.groupesComboBox = QtWidgets.QComboBox()
            self.groupesComboBox.currentIndexChanged.connect(self.updateWidget)

        # le nom et le label :
        labelText = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Name:'))
        nameLabel = QtWidgets.QLabel(labelText)
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.textChanged.connect(self.afficheOkButton)
        labelLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Label:')))
        self.labelEdit = QtWidgets.QLineEdit()
        # la période :
        labelText = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Period:'))
        periodeLabel = QtWidgets.QLabel(labelText)
        self.periodesComboBox = QtWidgets.QComboBox()
        self.periodesComboBox.setMinimumWidth(200)
        # public/privé :
        self.publicCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'Public table'))
        self.publicCheckBox.setCheckState(QtCore.Qt.Checked)

        if canSelectItems:
            # sélection des items :
            self.chooseItemsButton = QtWidgets.QPushButton(
                utils.doIcon('items'), 
                QtWidgets.QApplication.translate('main', 'ChooseItems'))
            self.chooseItemsButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsStatusTip1'))
            self.chooseItemsButton.clicked.connect(self.doChooseItems)
            # sélection du modèle :
            self.templateComboBox = QtWidgets.QComboBox()
            self.templateComboBox.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseTemplateStatusTip1'))
            self.templateComboBox.activated.connect(self.templateComboBoxChanged)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        if not(forEditOnly):
            grid.addWidget(manageGroupesButton,     0, 1, 1, 2)
            grid.addWidget(matiereLabel,            1, 0)
            grid.addWidget(self.matieresComboBox,   1, 1, 1, 2)
            grid.addWidget(groupeLabel,             2, 0)
            grid.addWidget(self.groupesComboBox,    2, 1, 1, 2)
        grid.addWidget(nameLabel,                   3, 0)
        grid.addWidget(self.nameEdit,               3, 1, 1, 2)
        grid.addWidget(labelLabel,                  4, 0)
        grid.addWidget(self.labelEdit,              4, 1, 1, 2)
        grid.addWidget(periodeLabel,                5, 0)
        grid.addWidget(self.periodesComboBox,       5, 1, 1, 2)
        grid.addWidget(self.publicCheckBox,         6, 1, 1, 2)
        if canSelectItems:
            grid.addWidget(self.chooseItemsButton,  10, 1)
            grid.addWidget(self.templateComboBox,   10, 2)
        grid.addWidget(buttonBox,                   20, 1, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # pour présélectionner matière, groupe et période
        # en fonction de la vue actuelle :
        aucunTableau = (self.main.id_tableau == -1)
        if aucunTableau:
            self.initialGroupe = self.main.groupeButton.text()
            if len(self.initialGroupe.split('(')) > 1:
                # on récupère la matière liée au groupe :
                self.initialMatiere = self.initialGroupe.split('(')[-1][:-1]
            else:
                # on prend la matière liée au prof :
                self.initialMatiere = self.main.me['Matiere']
            initialPeriode = utils.selectedPeriod
        else:
            # on récupère ce qui correspond au tableau sélectionné :
            data = diversFromSelection(self.main, self.main.id_tableau)
            self.initialGroupe, self.initialMatiere, initialPeriode = data[1], data[2], data[3]
            self.initialGroupe = utils_functions.u(
                '{0} ({1})').format(self.initialGroupe, self.initialMatiere)

        # remplissage et préselection de la combo période :
        for periode in range(utils.NB_PERIODES):
            self.periodesComboBox.addItem(utils.PERIODES[periode])
        if initialPeriode < 0:
            self.periodesComboBox.setCurrentIndex(0)
        else:
            self.periodesComboBox.setCurrentIndex(initialPeriode)

        if not(forEditOnly):
            self.remplirMatieresComboBox()

        if canSelectItems:
            # remplissage de la combo modèles :
            templates = []
            commandLine_my = utils_db.q_selectAllFrom.format('templates')
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                id_template = int(query_my.value(0))
                templateName = query_my.value(1)
                templateLabel = query_my.value(3)
                templates.append((id_template, templateName, templateLabel))
            if len(templates) > 0:
                templateName = QtWidgets.QApplication.translate('main', 'no template')
                self.templateComboBox.addItem(templateName, (0, ''))
                for (id_template, templateName, templateLabel) in templates:
                    self.templateComboBox.addItem(templateName, (id_template, templateLabel))
            self.id_template = 0

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-tableau')

    def remplirMatieresComboBox(self):
        if self.waiting:
            return
        self.waiting = True
        try:
            self.matieresComboBox.clear()
            query_my = utils_db.queryExecute(
                utils_db.q_selectAllFromOrder.format(
                    'groupes', 'ordre, Matiere, UPPER(Name)'), 
                db=self.main.db_my)
            self.list_groupes = {}
            self.list_matieres = []
            while query_my.next():
                id_groupe = int(query_my.value(0))
                nom_groupe = query_my.value(1)
                matiere_groupe = query_my.value(2)
                if not(matiere_groupe) in self.list_groupes:
                    self.list_matieres.append(matiere_groupe)
                    self.list_groupes[matiere_groupe] = {}
                self.list_groupes[matiere_groupe][nom_groupe] = id_groupe
                self.dict_groupes[id_groupe] = matiere_groupe
            self.list_matieres.sort()
            self.matieresComboBox.addItem(QtWidgets.QApplication.translate('main', 'All subjects'))
            for matiereName in self.list_matieres:
                self.matieresComboBox.addItem(matiereName)
            # préselection :
            index = self.matieresComboBox.findText(self.initialMatiere)
            if index == -1:
                index = 0
                self.remplirGroupesComboBox(index)
            else:
                self.matieresComboBox.setCurrentIndex(index)
        finally:
            self.waiting = False

    def remplirGroupesComboBox(self, index):
        index = self.matieresComboBox.currentIndex()
        self.groupesComboBox.clear()
        if index == 0:
            liste = self.list_matieres
        else:
            liste = [self.matieresComboBox.currentText()]
        for matiereName in liste:
            if not(self.list_groupes[matiereName]):
                continue
            liste_2 = []
            for nom_groupe in self.list_groupes[matiereName]:
                liste_2.append((nom_groupe, self.list_groupes[matiereName][nom_groupe]))
            liste_2.sort()
            for groupe in liste_2:
                self.groupesComboBox.addItem(
                    utils_functions.u('{0} ({1})').format(groupe[0], matiereName), 
                    groupe[1])
            if matiereName != liste[-1]:
                self.groupesComboBox.addItem('-------', -1)
        # préselection :
        index = self.groupesComboBox.findText(self.initialGroupe)
        if index > -1:
            self.groupesComboBox.setCurrentIndex(index)
        # on termine :
        self.updateWidget(0)
        self.afficheOkButton()

    def updateWidget(self, index):
        affiche = True
        index = self.groupesComboBox.currentIndex()
        if self.groupesComboBox.itemData(index, QtCore.Qt.UserRole) == -1:
            affiche = False
        elif self.groupesComboBox.count() == 0:
            affiche = False
        elif not(self.list_matieres) or not(self.dict_groupes):
            affiche = False
        self.okButton.setEnabled(affiche)
        self.nameEdit.setEnabled(affiche)
        self.periodesComboBox.setEnabled(affiche)
        self.publicCheckBox.setEnabled(affiche)

    def afficheOkButton(self):
        if not(self.nameEdit.text()):
            self.okButton.setEnabled(False)
        else:
            self.okButton.setEnabled(True)

    def manageGroupes(self):
        dialog = prof_groupes.GestGroupesDlg(parent=self.main)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.main.changeDBMyState()
        self.remplirMatieresComboBox()

    def doChooseItems(self):
        dialog = ListItemsDlg(parent=self.main, useMainTableau=False)
        dialog.setWindowState(QtCore.Qt.WindowMaximized)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            for i in range(dialog.selectionList.count()):
                item = dialog.selectionList.item(i)
                id_item = item.data(QtCore.Qt.UserRole)[0]
                self.list_items.append((id_item, i))
            self.templateComboBox.setCurrentIndex(0)
            self.templateComboBox.setEnabled(False)

    def templateComboBoxChanged(self):
        currentIndex = self.templateComboBox.currentIndex()
        self.id_template = self.templateComboBox.itemData(currentIndex)[0]
        self.chooseItemsButton.setEnabled(self.id_template == 0)
        if self.id_template != 0:
            self.nameEdit.setText(self.templateComboBox.currentText())
            templateLabel = self.templateComboBox.itemData(currentIndex)[1]
            self.labelEdit.setText(templateLabel)


class CreateMultipleTableauxDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None):
        super(CreateMultipleTableauxDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'CreateMultipleTableaux'))

        # des variables
        self.list_matieres = []
        self.list_groupes = {}
        self.dict_groupes = {}
        self.list_items = []
        self.waiting = False

        # sélection de la matière :
        matiereLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Subject:')))
        self.matieresComboBox = QtWidgets.QComboBox()
        self.matieresComboBox.setMinimumWidth(200)

        # sélection des groupes pour lesquels on va créer des tableaux :
        groupeLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Groups:')))
        self.groupesListWidget = QtWidgets.QListWidget()
        self.groupesListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        # sélection de la base des noms à donner aux tableaux :
        nameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'BaseName:')))
        nameLabel.setToolTip(QtWidgets.QApplication.translate(
            'main', 'CreateMultipleTableauxBaseNameToolTip'))
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.setToolTip(QtWidgets.QApplication.translate(
            'main', 'CreateMultipleTableauxBaseNameToolTip'))
        # le label :
        labelLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Label:')))
        self.labelEdit = QtWidgets.QLineEdit()
        # sélection de la période :
        periodeLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Period:')))
        self.periodesComboBox = QtWidgets.QComboBox()
        self.periodesComboBox.setMinimumWidth(200)
        self.publicCheckBox = QtWidgets.QCheckBox(QtWidgets.QApplication.translate('main', 'Public table'))
        # sélection du type public ou privé :
        self.publicCheckBox.setCheckState(QtCore.Qt.Checked)

        # sélection des items :
        self.chooseItemsButton = QtWidgets.QPushButton(utils.doIcon('items'), 
            QtWidgets.QApplication.translate('main', 'ChooseItems'))
        self.chooseItemsButton.setToolTip(QtWidgets.QApplication.translate('main', 'ChooseItemsStatusTip'))
        self.chooseItemsButton.clicked.connect(self.doChooseItems)

        # sélection du modèle :
        self.templateComboBox = QtWidgets.QComboBox()
        self.templateComboBox.setToolTip(QtWidgets.QApplication.translate('main', 'ChooseTemplateStatusTip'))
        #self.templateComboBox.setMinimumWidth(200)
        self.templateComboBox.activated.connect(self.templateComboBoxChanged)

        # on connecte:
        self.matieresComboBox.currentIndexChanged.connect(self.remplirGroupesListWidget)
        self.nameEdit.textChanged.connect(self.afficheOkButton)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(matiereLabel,            1, 0)
        grid.addWidget(self.matieresComboBox,   1, 1, 1, 2)
        grid.addWidget(groupeLabel,             2, 0)
        grid.addWidget(self.groupesListWidget,  2, 1, 1, 2)
        grid.addWidget(nameLabel,               5, 0)
        grid.addWidget(self.nameEdit,           5, 1, 1, 2)
        grid.addWidget(labelLabel,              6, 0)
        grid.addWidget(self.labelEdit,          6, 1, 1, 2)
        grid.addWidget(periodeLabel,            7, 0)
        grid.addWidget(self.periodesComboBox,   7, 1, 1, 2)
        grid.addWidget(self.publicCheckBox,     8, 1, 1, 2)
        grid.addWidget(self.chooseItemsButton,  10, 1)
        grid.addWidget(self.templateComboBox,   10, 2)
        grid.addWidget(buttonBox,               20, 1, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # pour présélectionner matière et période
        # en fonction de la vue actuelle :
        aucunTableau = (self.main.id_tableau == -1)
        if aucunTableau:
            initialGroupe = self.main.groupeButton.text()
            if len(initialGroupe.split('(')) > 1:
                # on récupère la matière liée au groupe :
                self.initialMatiere = initialGroupe.split('(')[-1][:-1]
            else:
                # on prend la matière liée au prof :
                self.initialMatiere = self.main.me['Matiere']
            initialPeriode = utils.selectedPeriod
        else:
            # on récupère ce qui correspond au tableau sélectionné :
            data = diversFromSelection(self.main, self.main.id_tableau)
            self.initialMatiere, initialPeriode = data[2], data[3]

        # remplissage et préselection de la combo période :
        for periode in range(utils.NB_PERIODES):
            self.periodesComboBox.addItem(utils.PERIODES[periode])
        if initialPeriode < 0:
            self.periodesComboBox.setCurrentIndex(0)
        else:
            self.periodesComboBox.setCurrentIndex(initialPeriode)

        self.remplirMatieresComboBox()

        # remplissage de la combo modèles :
        templates = []
        commandLine_my = utils_db.q_selectAllFrom.format('templates')
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_template = int(query_my.value(0))
            templateName = query_my.value(1)
            templateLabel = query_my.value(3)
            templates.append((id_template, templateName, templateLabel))
        if len(templates) > 0:
            templateName = QtWidgets.QApplication.translate('main', 'no template')
            self.templateComboBox.addItem(templateName, (0, ''))
            for (id_template, templateName, templateLabel) in templates:
                self.templateComboBox.addItem(templateName, (id_template, templateLabel))
        self.id_template = 0

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-multiple-tableaux')

    def remplirMatieresComboBox(self):
        if self.waiting:
            return
        self.waiting = True
        try:
            self.matieresComboBox.clear()
            query_my = utils_db.queryExecute(
                utils_db.q_selectAllFromOrder.format(
                    'groupes', 'ordre, Matiere, UPPER(Name)'), 
                db=self.main.db_my)
            self.list_groupes = {}
            self.list_matieres = []
            while query_my.next():
                id_groupe = int(query_my.value(0))
                nom_groupe = query_my.value(1)
                matiere_groupe = query_my.value(2)
                if not(matiere_groupe) in self.list_groupes:
                    self.list_matieres.append(matiere_groupe)
                    self.list_groupes[matiere_groupe] = {}
                self.list_groupes[matiere_groupe][nom_groupe] = id_groupe
                self.dict_groupes[id_groupe] = matiere_groupe
            self.list_matieres.sort()
            self.matieresComboBox.addItem(QtWidgets.QApplication.translate('main', 'All subjects'))
            for matiereName in self.list_matieres:
                self.matieresComboBox.addItem(matiereName)
            self.matieresComboBox.currentIndexChanged.connect(self.remplirGroupesListWidget)
            # préselection :
            index = self.matieresComboBox.findText(self.initialMatiere)
            if index == -1:
                index=0
                self.remplirGroupesListWidget(index)
            else:
                self.matieresComboBox.setCurrentIndex(index)
        finally:
            self.waiting = False

    def remplirGroupesListWidget(self, index):
        index = self.matieresComboBox.currentIndex()
        self.groupesListWidget.clear()
        self.idFromGroupe = {}
        if index == 0:
            liste = self.list_matieres
        else:
            liste = [self.matieresComboBox.currentText()]
        for matiereName in liste:
            if not(self.list_groupes[matiereName]):
                continue
            liste_2 = []
            for nom_groupe in self.list_groupes[matiereName]:
                liste_2.append((nom_groupe, self.list_groupes[matiereName][nom_groupe]))
            liste_2.sort()
            for groupe in liste_2:
                itemText = utils_functions.u('{0} ({1})').format(groupe[0], matiereName)
                self.groupesListWidget.addItem(itemText)
                self.idFromGroupe[itemText] = groupe[1]
            if matiereName != liste[-1]:
                itemText = "-------"
                self.groupesListWidget.addItem(itemText)
                self.idFromGroupe[itemText] = -1
        # on termine :
        self.afficheOkButton()

    def afficheOkButton(self):
        if not(self.nameEdit.text()):
            self.okButton.setEnabled(False)
        else:
            self.okButton.setEnabled(True)

    def doChooseItems(self):
        dialog = ListItemsDlg(parent=self.main, useMainTableau=False)
        dialog.setWindowState(QtCore.Qt.WindowMaximized)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            for i in range(dialog.selectionList.count()):
                item = dialog.selectionList.item(i)
                id_item = item.data(QtCore.Qt.UserRole)[0]
                self.list_items.append((id_item, i))
            self.templateComboBox.setCurrentIndex(0)
            self.templateComboBox.setEnabled(False)

    def templateComboBoxChanged(self):
        currentIndex = self.templateComboBox.currentIndex()
        self.id_template = self.templateComboBox.itemData(currentIndex)[0]
        self.chooseItemsButton.setEnabled(self.id_template == 0)
        if self.id_template != 0:
            self.nameEdit.setText(self.templateComboBox.currentText())
            templateLabel = self.templateComboBox.itemData(currentIndex)[1]
            self.labelEdit.setText(templateLabel)


def tableauNameExists(main, tableauName, id_groupe):
    query_my = utils_db.queryExecute(
        {utils_db.qs_prof_tableauNameGroupe: (tableauName, id_groupe)}, 
        db=main.db_my)
    if query_my.first():
        return True
    return False


def createNewTableau(main, id_groupe, tableauName, tableauLabel, periode=0, public=1):
    """
    blablabla
    """
    if tableauNameExists(main, tableauName, id_groupe):
        m1 = QtWidgets.QApplication.translate(
            'main', 'The name <b>{0}</b><br/> is already used.').format(tableauName)
        m2 = QtWidgets.QApplication.translate('main', 'Start again.')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>'
            '<p></p>'
            '<p align="center">{2}</p>').format(utils.SEPARATOR_LINE, m1, m2)
        utils_functions.messageBox(main, level='warning', message=message)
        return -1
    else:
        utils_functions.doWaitCursor()
        result = -1
        try:
            id_tableau = 0
            if periode < 0:
                periode = 0
            commandLine_my = utils_db.q_selectAllFromOrder.format(
                'tableaux', 'id_tableau')
            query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
            ordre = 0
            while query_my.next():
                id_tableau = int(query_my.value(0)) + 1
                newOrdre = int(query_my.value(3))
                if newOrdre > ordre:
                    ordre = newOrdre + 1
            commandLine_my = utils_db.insertInto('tableaux')
            query_my = utils_db.queryExecute(
                {commandLine_my: (
                    id_tableau, tableauName, public, ordre, periode, id_groupe, tableauLabel)}, 
                query=query_my)
            result = id_tableau
        finally:
            tableauxInit(main, periode, id_groupe, id_tableau)
            utils_functions.restoreCursor()
            return result


def editActualTableau(main):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'editActualTableau')
    dialog = NewTableauDlg(
        parent=main, forEditOnly=True, canSelectItems=False)
    dialog.setWindowTitle(
        QtWidgets.QApplication.translate('main', 'EditActualTableau'))

    data = diversFromSelection(main, main.id_tableau)
    periode, public = data[3], data[5]
    dialog.periodesComboBox.setCurrentIndex(periode)
    if public == 0:
        dialog.publicCheckBox.setChecked(False)
    dialog.nameEdit.setText(data[4])
    dialog.labelEdit.setText(data[10])
    # on affiche le dialogue :
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        return False

    utils_functions.doWaitCursor()
    try:
        tableauName = dialog.nameEdit.text()
        tableauLabel = dialog.labelEdit.text()
        periode = dialog.periodesComboBox.currentIndex()
        if dialog.publicCheckBox.isChecked():
            public = 1
        else:
            public = 0
        commandLine_my = utils_functions.u(
            'UPDATE tableaux '
            'SET Name=?, Public=?, Periode=?, Label=? '
            'WHERE id_tableau=?')
        query_my = utils_db.queryExecute(
            {commandLine_my: (
                tableauName, public, periode, tableauLabel, main.id_tableau)}, 
            db=main.db_my)
    finally:
        utils_functions.restoreCursor()
        tableauxInit(main, idTableauToSelect=main.id_tableau)
        return True


def copyTableau(main):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'copyTableau')
    dialog = NewTableauDlg(parent=main, canSelectItems=False)
    dialog.setWindowTitle(QtWidgets.QApplication.translate('main', 'Clone Table'))

    # on récupère les données matières, et périodes pour faciliter la copie. 
    # On récupère aussi les données public et id_groupe
    data = diversFromSelection(main, main.id_tableau)
    id_groupe, matiereName, periode, public = data[0], data[2], data[3], data[5]
    i = dialog.matieresComboBox.findText(matiereName)
    dialog.matieresComboBox.setCurrentIndex(i)
    dialog.periodesComboBox.setCurrentIndex(periode)
    if public == 0:
        dialog.publicCheckBox.setChecked(False)
    dialog.nameEdit.setText(data[4])
    dialog.labelEdit.setText(data[10])
    # on affiche le dialogue :
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
        index = dialog.groupesComboBox.currentIndex()
        id_groupe = dialog.groupesComboBox.itemData(index, QtCore.Qt.UserRole)
        tableauName = dialog.nameEdit.text()
        tableauLabel = dialog.labelEdit.text()
        periode = dialog.periodesComboBox.currentIndex()
        if dialog.publicCheckBox.isChecked():
            public = 1
        else:
            public = 0
        if tableauName == '':
            return
        if tableauNameExists(main, tableauName, id_groupe):
            m1 = QtWidgets.QApplication.translate(
                'main', 'The name <b>{0}</b><br/> is already used.').format(tableauName)
            m2 = QtWidgets.QApplication.translate('main', 'Start again.')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center">{1}</p>'
                '<p></p>'
                '<p align="center">{2}</p>').format(utils.SEPARATOR_LINE, m1, m2)
            utils_functions.messageBox(main, level='warning', message=message)
            return False
        else:
            utils_functions.doWaitCursor()
            result = False
            try:
                query_my = utils_db.query(main.db_my)
                # on calcule id_tableau et ordre :
                id_tableau = 0
                ordre = 0
                commandLine_my = utils_db.q_selectAllFrom.format('tableaux')
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    id_tableau = int(query_my.value(0)) + 1
                    newOrdre = int(query_my.value(3))
                    if newOrdre > ordre:
                        ordre = newOrdre + 1
                # on inscrit le nouveau tableau :
                commandLine_my = utils_db.insertInto('tableaux')
                query_my = utils_db.queryExecute(
                    {commandLine_my: (
                        id_tableau, tableauName, public, ordre, periode, id_groupe, tableauLabel)}, 
                    query=query_my)
                # on recopie les items :
                lines = []
                commandLine_my = utils_db.q_selectAllFromWhere.format(
                    'tableau_item', 'id_tableau', main.id_tableau)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    id_item = int(query_my.value(1))
                    ordre = int(query_my.value(2))
                    lines.append((id_tableau, id_item, ordre))
                commandLine_my = utils_db.insertInto('tableau_item')
                query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
                # on recopie les évaluations :
                lines = []
                commandLine_my = utils_db.q_selectAllFromWhere.format(
                    'evaluations', 'id_tableau', main.id_tableau)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    id_eleve = int(query_my.value(1))
                    id_item = int(query_my.value(2))
                    id_bilan = int(query_my.value(3))
                    value = query_my.value(4)
                    lines.append((id_tableau, id_eleve, id_item, id_bilan, value))
                commandLine_my = utils_db.insertInto('evaluations')
                query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
                result = True
            finally:
                tableauxInit(main, periode, id_groupe, id_tableau)
                utils_functions.restoreCursor()
                return result


def supprTableau(main):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'supprTableau')

    commandLine_my = utils_db.q_selectAllFromWhere.format(
        'tableaux', 'id_tableau', main.id_tableau)
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        tableauName = query_my.value(1)
    message = QtWidgets.QApplication.translate('main', 'Delete this table?')
    message = utils_functions.u('<p><b>{0}</b></p> {1}').format(message, tableauName)
    if utils_functions.messageBox(
        main, level='question', message=message, 
        buttons=['Yes', 'No']) == QtWidgets.QMessageBox.No:
        return

    utils_functions.doWaitCursor()
    try:
        commandLine_my1 = utils_db.qdf_where.format('tableaux', 'id_tableau', main.id_tableau)
        commandLine_my2 = utils_db.qdf_where.format('tableau_item', 'id_tableau', main.id_tableau)
        commandLine_my3 = utils_db.qdf_where.format('evaluations', 'id_tableau', main.id_tableau)
        query_my = utils_db.queryExecute(
            [commandLine_my1, commandLine_my2, commandLine_my3], query=query_my)
        tableauxInit(main)
    finally:
        utils_functions.restoreCursor()


class CopyItemsFromToDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, comments="", comboBox=False, sameGroup=False):
        super(CopyItemsFromToDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Copy the items in a table to others'))

        self.comboBox = comboBox
        self.sameGroup = sameGroup

        self.labelFrom = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'From:')))
        self.comboBoxFrom = QtWidgets.QComboBox()
        self.comboBoxFrom.setMinimumWidth(200)
        if sameGroup:
            self.comboBoxFrom.currentIndexChanged.connect(self.fromChanged)

        self.labelTo = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'To:')))
        if comboBox:
            self.comboBoxTo = QtWidgets.QComboBox()
            self.comboBoxTo.setMinimumWidth(200)
        else:
            self.listWidgetTo = QtWidgets.QListWidget()
            self.listWidgetTo.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        labelComments = QtWidgets.QLabel(comments)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        self.grid = QtWidgets.QGridLayout()
        self.grid.addWidget(self.labelFrom, 2, 0)
        self.grid.addWidget(self.comboBoxFrom, 2, 1)
        self.grid.addWidget(self.labelTo, 3, 0)
        if comboBox:
            self.grid.addWidget(self.comboBoxTo, 3, 1)
        else:
            self.grid.addWidget(self.listWidgetTo, 3, 1)
        self.grid.addWidget(labelComments, 5, 0, 1, 2, QtCore.Qt.AlignHCenter)
        self.grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(self.grid)
        self.setMinimumWidth(400)

        commandLine = utils_db.qs_prof_tableaux
        query_my = utils_db.queryExecute(commandLine, db=self.main.db_my)
        while query_my.next():
            id_tableau = int(query_my.value(0))
            tableauName = query_my.value(1)
            id_groupe = int(query_my.value(5))
            self.comboBoxFrom.addItem(tableauName, (id_tableau, id_groupe))
            if not(sameGroup):
                if comboBox:
                    self.comboBoxTo.addItem(tableauName)
                else:
                    self.listWidgetTo.addItem(tableauName)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('copy-items-from-to')

    def fromChanged(self, index):
        index = self.comboBoxFrom.currentIndex()
        (id_tableau, id_groupe) = self.comboBoxFrom.itemData(index, QtCore.Qt.UserRole)
        self.updateComboBoxTo(id_tableau, id_groupe)

    def updateComboBoxTo(self, id_tableauFrom, id_groupe):
        if self.comboBox:
            self.comboBoxTo.clear()
        else:
            self.listWidgetTo.clear()
        commandLine = utils_db.qs_prof_tableauxGroupe.format(id_groupe)
        query_my = utils_db.queryExecute(commandLine, db=self.main.db_my)
        while query_my.next():
            id_tableau = int(query_my.value(0))
            tableauName = query_my.value(1)
            if id_tableau != id_tableauFrom:
                if self.comboBox:
                    self.comboBoxTo.addItem(tableauName)
                else:
                    self.listWidgetTo.addItem(tableauName)


def copyItemsFromTo(main):
    """
    Recrée le tableau To en lui ajoutant à la fin 
    les items du tableau From qu'il n'a pas.
    Permet de synchroniser les items de 2 tableaux
    (par exemple 2 classes de même niveau)
    """
    comments = QtWidgets.QApplication.translate(
        'main', 
        '<p></p>'
        '<p>The <b>items</b> from the <b>fist</b> table</p>'
        '<p>will be recopied into the <b>seconds</b> tables.</p>'
        '<p></p>'
        '<p>But no item will be deleted in the <b>seconds</b>.</p>')
    dialog = CopyItemsFromToDlg(parent=main, comments=comments)
    if dialog.exec_() == QtWidgets.QDialog.Accepted:

        # on récupère le nom et l'id du tableau From :
        tableauNameFrom = dialog.comboBoxFrom.currentText()
        id_tableauFrom = -1
        query_my = utils_db.queryExecute(
            {utils_db.qs_prof_tableauName: (tableauNameFrom, )}, 
            db=main.db_my)
        while query_my.next():
            id_tableauFrom = int(query_my.value(0))
        # si l'id est -1, on quitte :
        if id_tableauFrom == -1:
            return False

        # on récupère la liste des items présents dans le tableau From, et les ordres :
        id_templateFrom = diversFromSelection(main, id_tableau=id_tableauFrom)[8]
        listItemsInFrom, listIdsInFrom = [], []
        if id_templateFrom < 0:
            commandLine_my = utils_db.qs_prof_tableauItem.format(id_templateFrom)
        else:
            commandLine_my = utils_db.qs_prof_tableauItem.format(id_tableauFrom)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(1))
            ordre = int(query_my.value(2))
            listItemsInFrom.append((id_item, ordre))
            listIdsInFrom.append(id_item)

        # on copie dans tous les tableaux sélectionnés dans la liste To :
        selectedTableaux = dialog.listWidgetTo.selectedItems()
        for selectedTableau in selectedTableaux :
            # on récupère le nom et l'id du tableau To :
            tableauNameTo = selectedTableau.text()
            id_tableauTo = -1
            query_my = utils_db.queryExecute(
                {utils_db.qs_prof_tableauName: (tableauNameTo, )}, 
                query=query_my)
            while query_my.next():
                id_tableauTo = int(query_my.value(0))
            # si l'un des id est -1, on passe :
            if id_tableauTo == -1:
                continue

            # on récupère la liste des items présents dans le tableau To, et les ordres :
            id_templateTo = diversFromSelection(main, id_tableau=id_tableauTo)[8]
            if id_templateTo < 0:
                id4To = id_templateTo
            else:
                id4To = id_tableauTo
            listItemsInTo, listIdsInTo = [], []
            commandLine_my = utils_db.qs_prof_tableauItem.format(id4To)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(1))
                ordre = int(query_my.value(2))
                listItemsInTo.append((id_item, ordre))
                listIdsInTo.append(id_item)

            # on fabrique la liste des items à ajouter au tableau To :
            listIdsNewInTo = []
            for itemFrom in listItemsInFrom:
                id_itemFrom = itemFrom[0]
                if not(id_itemFrom in listIdsInTo):
                    listIdsNewInTo.append(id_itemFrom)

            # on fabrique la liste des items qui ne sont pas dans From :
            listIdsNotInFrom = []
            for itemTo in listItemsInTo:
                id_itemTo = itemTo[0]
                if not(id_itemTo in listIdsInFrom):
                    listIdsNotInFrom.append(id_itemTo)

            # on efface les items du tableau To de la table tableau_item :
            commandLine_my = utils_db.qdf_where.format('tableau_item', 'id_tableau', id4To)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)

            # on inscrit les items provenants de From dans la table tableau_item :
            commandLine_my = utils_db.insertInto('tableau_item')
            lines = []
            newOrdre = -1
            for itemFrom in listItemsInFrom:
                id_item = itemFrom[0]
                ordre = itemFrom[1]
                lines.append((id4To, id_item, ordre))
                if newOrdre < ordre:
                    newOrdre = ordre
            # on ajoute les items qui n'étaient pas dans From :
            ordre = newOrdre
            for id_item in listIdsNotInFrom:
                ordre += 1
                lines.append((id4To, id_item, ordre))
            query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

            # on recalcule le tableau To :
            calcAll(main, id_tableau=id_tableauTo, forcer=True)
            tableauxViewChanged(main)

            # on fabrique le message final :
            message = ''
            totalNewItems = len(listIdsNewInTo)
            for id_item in listIdsNewInTo:
                commandLine_my = utils_db.q_selectAllFromWhere.format(
                    'items', 'id_item', id_item)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                if query_my.first():
                    itemName = query_my.value(1)
                    message = utils_functions.u('{0}{1}\n').format(message, itemName)

            # enfin on affiche le message final :
            if message != '':
                headMessage = QtWidgets.QApplication.translate(
                    'main', '<p><b>Added {0} items</b></p>')
                headMessage = headMessage + QtWidgets.QApplication.translate(
                    'main', '<p>in table: <b>{1}</b></p>')
                headMessage = headMessage.format(totalNewItems, tableauNameTo)
                utils_functions.messageBox(
                    main, message=headMessage, detailedText=message)
            else:
                message = utils_functions.u('<p><b>{0}</b></p>').format(
                    QtWidgets.QApplication.translate('main', 'Nothing to add'))
                utils_functions.messageBox(main, message=message)

        return True
    else:
        return False


def mergeTableaux(main):
    """
    Toutes les évaluations du premier tableau
    seront ajoutées dans le second.
    Un item qui n'est pas dans le second y sera ajouté.
    Dans ce cas, si le deuxième tableau est lié à un modèle, 
        on ajoute les items au modèle.
    Les tableaux doivent correspondre au même groupe d'élèves.
    Si un item est évalué dans les 2 tableaux, l'évaluation
        du premier sera ajoutée au bout dans le second.
    """
    comments = QtWidgets.QApplication.translate(
        'main', 
        '<p>All the <b>evaluations</b> of the <b>fist</b> table'
        '<br/>will be recopied in the <b>second</b>.</p>'
        '<p>If an item is evaluated in the 2 tables, these evaluations will be added.'
        '<br/>The <b>first</b> table will be declared <b>private</b>.</p>'
        '<p>The two tables must match the <b>same group</b> of students.</p>')
    dialog = CopyItemsFromToDlg(parent=main, comments=comments, comboBox=True, sameGroup=True)
    dialog.setWindowTitle(QtWidgets.QApplication.translate('main', 'Merge 2 tables'))
    dialog.labelFrom.setText(
        utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Merge:')))
    dialog.labelTo.setText(
        utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'In:')))

    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        return False

    utils_functions.doWaitCursor()
    try:
        result = False
        # PREMIÈRE PARTIE : récupération des choix et vérifications
        # on récupère le nom et l'id du tableau From :
        tableauNameFrom = dialog.comboBoxFrom.currentText()
        id_tableauFrom = -1
        query_my = utils_db.queryExecute(
            {utils_db.qs_prof_tableauName: (tableauNameFrom, )}, 
            db=main.db_my)
        while query_my.next():
            id_tableauFrom = int(query_my.value(0))
            id_groupeFrom = int(query_my.value(5))
        # on récupère le nom et l'id du tableau To :
        tableauNameTo = dialog.comboBoxTo.currentText()
        id_tableauTo = -1
        query_my = utils_db.queryExecute(
            {utils_db.qs_prof_tableauName: (tableauNameTo, )}, 
            query=query_my)
        while query_my.next():
            id_tableauTo = int(query_my.value(0))
            id_groupeTo = int(query_my.value(5))
        # si l'un des id est -1, on quitte :
        if (id_tableauFrom == -1) or (id_tableauTo == -1):
            return False
        # si les 2 tableaux ne sont pas liés au même groupe d'élèves,
        # on propose d'annuler ou de modifier le groupe du tableau TO
        if id_groupeFrom != id_groupeTo:
            return False

        # DEUXIÈME PARTIE : ajout des items
        # on récupère la liste des items présents dans le tableau From :
        id_templateFrom = diversFromSelection(main, id_tableau=id_tableauFrom)[8]
        listItemsInFrom = []
        if id_templateFrom < 0:
            commandLine_my = utils_db.qs_prof_tableauItem.format(id_templateFrom)
        else:
            commandLine_my = utils_db.qs_prof_tableauItem.format(id_tableauFrom)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(1))
            listItemsInFrom.append(id_item)
        # puis la liste de ceux présents dans le tableau To :
        id_templateTo = diversFromSelection(main, id_tableau=id_tableauTo)[8]
        if id_templateTo < 0:
            id4To = id_templateTo
        else:
            id4To = id_tableauTo
        listItemsInTo = []
        ordre = -1
        commandLine_my = utils_db.qs_prof_tableauItem.format(id4To)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(1))
            listItemsInTo.append(id_item)
            newOrdre = int(query_my.value(2))
            if newOrdre > ordre:
                ordre = newOrdre
        # on fabrique la liste des items à ajouter au tableau To :
        listItemsNewInTo = []
        for id_item in listItemsInFrom:
            if not(id_item in listItemsInTo):
                listItemsNewInTo.append(id_item)
        # on inscrit les nouveaux items dans la table tableau_item :
        commandLine_my = utils_db.insertInto('tableau_item')
        lines = []
        for id_item in listItemsNewInTo:
            ordre += 1
            lines.append((id4To, id_item, ordre))
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

        # TROISIÈME PARTIE : les évaluations
        # on récupère les évaluations du tableau TO :
        listEvalsInTo = {}
        commandLine_my = 'SELECT * FROM evaluations WHERE id_tableau={0} AND id_bilan=-1'.format(id_tableauTo)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            id_item = int(query_my.value(2))
            id_bilan = int(query_my.value(3))
            value = query_my.value(4)
            listEvalsInTo[(id_eleve, id_item)] = value
        # on traite les évaluations du tableau FROM :
        mustRemove = []         # liste des items présents dans les 2 tableaux
        listEvalsToAdd = []     # liste des évaluations à ajouter à TO
        commandLine_my = 'SELECT * FROM evaluations WHERE id_tableau={0} AND id_bilan=-1'.format(id_tableauFrom)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            id_item = int(query_my.value(2))
            value = query_my.value(4)
            # si aussi dans TO, on ajoute la valeur et il faudra effacer l'ancien enregistrement :
            if (id_eleve, id_item) in listEvalsInTo:
                mustRemove.append((id_eleve, id_item))
                value = listEvalsInTo[(id_eleve, id_item)] + value
            listEvalsToAdd.append((id_eleve, id_item, value))
        # on efface les enregistrements modifiés :
        for ligne in mustRemove:
            id_eleve = ligne[0]
            id_item = ligne[1]
            commandLine_my = utils_db.qdf_prof_evaluations5.format(id_tableauTo, id_eleve, id_item)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # et on inscrit les nouveaux enregistrements :
        commandLine_my = utils_db.insertInto('evaluations')
        lines = []
        for ligne in listEvalsToAdd:
            id_eleve = ligne[0]
            id_item = ligne[1]
            value = ligne[2]
            lines.append((id_tableauTo, id_eleve, id_item, -1, value))
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

        # QUATRIÈME PARTIE : le tableau FROM passe en privé
        commandLine_my = 'UPDATE tableaux SET Public=0 WHERE id_tableau={0}'.format(id_tableauFrom)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)

        # DERNIÈRE PARTIE : on termine
        # on recalcule le tableau To :
        calcAll(main, id_tableau=id_tableauTo, forcer=True)
        tableauxViewChanged(main)
        # enfin on affiche le message final :
        utils_functions.afficheMsgFin(main, timer=5)
        result = True
    finally:
        utils_functions.restoreCursor()
        return result


class ListGroupesTableauxDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None):
        super(ListGroupesTableauxDlg, self).__init__(parent)

        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Order of groups and tables'))
        self.main = parent

        # liste des groupes :
        groupesLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Groups:')))
        self.groupesListWidget = utils_2lists.DragDropListWidget(num=1)
        self.groupesListWidget.itemSelectionChanged.connect(self.groupeChanged)
        self.id_groupe = -1

        # liste des tableaux liés :
        translatedMessage = QtWidgets.QApplication.translate('main', 'Tableaux:')
        tableauxLabel = QtWidgets.QLabel(utils_functions.u('<p><b>{0}</b></p>').format(translatedMessage))
        self.tableauxListWidget = utils_2lists.DragDropListWidget(num=1)
        self.id_tableau = -1

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(groupesLabel, 2, 0)
        grid.addWidget(self.groupesListWidget, 3, 0, 1, 3)
        grid.addWidget(tableauxLabel, 2, 3)
        grid.addWidget(self.tableauxListWidget, 3, 3, 2, 3)
        grid.addWidget(buttonBox, 10, 3, 1, 3)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(600)

        self.loadGroupes()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('ordre-groupes-tableaux')

    def loadGroupes(self):
        self.groupesListWidget.clear()
        query_my = utils_db.query(self.main.db_my)
        # récupération de la liste des groupes :
        commandLine_my = utils_db.q_selectAllFromOrder.format(
            'groupes', 'ordre, Matiere, UPPER(Name)')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_groupe = int(query_my.value(0))
            groupeName = query_my.value(1)
            groupeWidget = QtWidgets.QListWidgetItem(groupeName)
            groupeWidget.setData(QtCore.Qt.UserRole, id_groupe)
            self.groupesListWidget.addItem(groupeWidget)
            if self.id_groupe == -1:
                self.id_groupe = id_groupe
                self.groupesListWidget.setCurrentItem(groupeWidget)

    def groupeChanged(self):
        self.saveTableaux()
        groupe = self.groupesListWidget.currentItem()
        self.id_groupe = groupe.data(QtCore.Qt.UserRole)
        query_my = utils_db.query(self.main.db_my)
        # récupération de la liste des tableaux liés :
        self.tableauxListWidget.clear()
        commandLine_my = utils_db.qs_prof_tableauxGroupe.format(self.id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_tableau = int(query_my.value(0))
            tableauName = query_my.value(1)
            tableauPeriode = int(query_my.value(4))
            tableauPeriode = utils.PERIODES[tableauPeriode]
            tableauAffichage = utils_functions.u('[{1}]     {0}').format(tableauName, tableauPeriode)
            tableauWidget = QtWidgets.QListWidgetItem(tableauAffichage)
            tableauWidget.setData(QtCore.Qt.UserRole, id_tableau)
            self.tableauxListWidget.addItem(tableauWidget)

    def saveGroupes(self):
        # enregistre l'ordre des groupes
        query_my = utils_db.query(self.main.db_my)
        for i in range(self.groupesListWidget.count()):
            groupeWidget = self.groupesListWidget.item(i)
            id_groupe = groupeWidget.data(QtCore.Qt.UserRole)
            commandLine_my = ('UPDATE groupes SET ordre={0} '
                                'WHERE id_groupe={1}').format(i, id_groupe)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)

    def saveTableaux(self):
        """
        enregistrement des modifications de la liste des tableaux 
        liés au groupe sélectionné.
        """
        query_my = utils_db.query(self.main.db_my)
        for i in range(self.tableauxListWidget.count()):
            tableauWidget = self.tableauxListWidget.item(i)
            id_tableau = tableauWidget.data(QtCore.Qt.UserRole)
            commandLine_my = (
                'UPDATE tableaux SET ordre={0} WHERE id_tableau={1}').format(i, id_tableau)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)


def tableauxInit(main, id_periode=-2, id_groupe=-2, idTableauToSelect=-1, mustReloadView=True):
    """
    En fonction de la sélection et des tableaux existants, 
    on met à jour l'affichage (menus et vues)
    """
    # une liste des ids des tableaux publics affichés (pour la compilation) :
    tableauxInCompil = []
    if id_periode == -2:
        id_periode = utils.selectedPeriod
    if id_groupe == -2:
        id_groupe = main.id_groupe
    utils_functions.afficheMessage(main, 'tableauxInit {0} {1} {2}'.format(
        id_periode, id_groupe, idTableauToSelect))
    if id_periode != utils.selectedPeriod:
        main.updatePeriodeMenu(id_periode)
    if id_groupe != main.id_groupe:
        main.updateGroupeMenu(id_groupe)
    if id_periode == -1:
        # on veut voir tous les tableaux, quelle que soit la période
        if id_groupe == -1:
            commandLine_my = utils_db.qs_prof_tableaux
        else:
            commandLine_my = utils_db.qs_prof_tableauxGroupe.format(id_groupe)
    else:
        if id_groupe == -1:
            commandLine_my = (
                'SELECT tableaux.* FROM tableaux '
                'JOIN groupes ON groupes.id_groupe=tableaux.id_groupe '
                'WHERE tableaux.Periode={0} '
                'ORDER BY groupes.Matiere, groupes.ordre, UPPER(groupes.Name), '
                'tableaux.ordre, UPPER(tableaux.Name)').format(id_periode)
        else:
            commandLine_my = (
                'SELECT * FROM tableaux '
                'WHERE Periode={0} AND id_groupe={1} '
                'ORDER BY ordre, UPPER(Name)').format(id_periode, id_groupe)
    # le tableau qui sera finalement sélectionné :
    newIdTableau = -1
    if idTableauToSelect == -1:
        idTableauToSelect = main.lastTableauForSelection.get(
            (id_periode, id_groupe), -1)
    # le premier tableau de la liste au cas où :
    firstIdTableau = -1
    main.tableauMenuActions = []
    main.tableauMenu.clear()
    # remplissage du menu :
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        id_tableau = int(query_my.value(0))
        if int(query_my.value(2)) == 1:
            tableauxInCompil.append(id_tableau)
        tableauName = query_my.value(1)
        tableauLabel = query_my.value(6)
        newAct = QtWidgets.QAction(tableauName, main, checkable=True)
        newAct.setData(('tableau', id_tableau, tableauName, tableauLabel, []))
        newAct.setStatusTip(tableauLabel)
        newAct.triggered.connect(main.selectionChanged)
        main.tableauMenu.addAction(newAct)
        main.tableauMenuActions.append(newAct)
        # si on n'a pas encore trouvé le tableau à sélectionner :
        if newIdTableau == -1:
            if firstIdTableau == -1:
                firstIdTableau = id_tableau
                if idTableauToSelect == -1:
                    newIdTableau = id_tableau
            if id_tableau == idTableauToSelect:
                newIdTableau = id_tableau
    if newIdTableau == -1:
        main.id_tableau = firstIdTableau
    else:
        main.id_tableau = newIdTableau
    main.lastTableauForSelection[id_periode, id_groupe] = main.id_tableau
    # tableau de tous les tableaux publics :
    try:
        if (id_groupe > -1) and (id_periode > -1) and (firstIdTableau > -1)\
            and (len(tableauxInCompil) > 1):
            id_tableau = -2
            tableauName = QtWidgets.QApplication.translate('main', 'tablesCompilation')
            newAct = QtWidgets.QAction(tableauName, main, checkable=True)
            newAct.setData(('tableau', id_tableau, tableauName, '', tableauxInCompil))
            newAct.setStatusTip(QtWidgets.QApplication.translate('main', 'tablesCompilationStatusTip'))
            newAct.triggered.connect(main.selectionChanged)
            main.tableauMenu.addSeparator()
            main.tableauMenu.addAction(newAct)
            main.tableauMenuActions.append(newAct)
    except:
        main.id_groupe = -1

    # enfin une entrée spéciale de création de tableau :
    main.tableauMenu.addSeparator()
    main.tableauMenu.addAction(main.actionCreateTableau)
    # mise à jour du menu :
    main.updateTableauMenu(main.id_tableau)
    if mustReloadView:
        tableauxViewChanged(main)
    else:
        tableauxChanged(main)


def tableauxViewChanged(main):
    tableauxChanged(main)
    viewChanged(main)


def tableauxChanged(main):
    """
    blablabla
    """
    #utils_functions.afficheMessage(main, 'tableauxChanged {0}'.format(main.id_tableau))
    utils_functions.doWaitCursor()
    try:
        aucunTableau = (main.id_tableau == -1)
        if aucunTableau:
            main.view.restore()
            main.statusBarLabel.setText('')
            main.tableauButton.setText('')
        else:
            try:
                data = diversFromSelection(main, main.id_tableau)
                groupeName = data[1]
                matiereName = data[2]
                periode = data[3]
                tableauName = data[4]
                main.public = data[5]
                periodeName = utils.PERIODES[periode]
                tableauLabel = data[10]

                # recherche d'un modèle lié au tableau :
                hasTemplate = (data[8] < 0)
                templateName = data[9]

                main.view.restore()
                if main.public == 1:
                    messagePublic = QtWidgets.QApplication.translate('main', 'public')
                else:
                    messagePublic = QtWidgets.QApplication.translate('main', 'private')
                messageMatiere = QtWidgets.QApplication.translate('main', 'Subject:')
                message = (
                    '<p align="center"><b>{0}</b> | <b>{1}</b>'
                    ' | <b>{2}</b> | <b>{3}</b> | <b>{4} : {5}</b></p>')
                message = utils_functions.u(message).format(
                    groupeName, matiereName, periodeName, messagePublic, templateName, tableauLabel)
                main.statusBarLabel.setText(message)
                main.tableauButton.setText(tableauName)
                if main.id_tableau == -2:
                    # cas d'un tableau de compilation :
                    actionsToDisable = (
                        main.actionEditActualTableau,
                        main.actionListItems,
                        main.actionCreateTemplateFromTableau,
                        main.actionLinkTableauTemplate,
                        main.actionUnlinkTableauTemplate,
                        main.actionCopyTableau,
                        main.actionSupprTableau,
                        )
                    for action in actionsToDisable:
                        action.setEnabled(False)
                else:
                    main.actionEditActualTableau.setEnabled(True)
                    for action in (
                        main.actionListItems,
                        main.actionCreateTemplateFromTableau,
                        main.actionLinkTableauTemplate):
                        action.setEnabled(not(hasTemplate))
                    main.actionUnlinkTableauTemplate.setEnabled(hasTemplate)
                    main.actionCopyTableau.setEnabled(True)
                    main.actionSupprTableau.setEnabled(True)
            except:
                main.id_tableau = -1
                main.view.restore()
                main.statusBarLabel.setText('')
                main.tableauButton.setText('')
    finally:
        utils_functions.restoreCursor()


class MyHeaderView(QtWidgets.QHeaderView):
    def __init__(self, parent):
        super(MyHeaderView, self).__init__(QtCore.Qt.Horizontal, parent)
        self.main = parent

        if utils.PYQT == 'PYQT5':
            self.setSectionsClickable(True)
        else:
            self.setClickable(True)
        self.sectionDoubleClicked.connect(self.sortTable)

    def paintSection(self, painter, rect, column):
        text = self.main.model.columns[column]['value']
        vertical = self.main.model.columns[column].get('vertical', False)
        if text == 'VSEPARATOR':
            w = rect.width() - 1
            h = rect.height()
            painter.save()
            painter.translate(rect.bottomLeft())
            painter.setPen(utils.colorGray)
            painter.setBrush(utils.colorGray)
            painter.drawRect(0, 0, w, -h)
            painter.restore()
        elif vertical:
            bilanType = self.main.model.columns[column].get('bilanType', False)
            fontSize = self.main.fontSizeSlider.value()
            w = rect.width() // 2
            h = rect.height()
            painter.save()
            painter.translate(rect.bottomLeft())
            painter.translate(w - 1, 0)
            painter.rotate(-90)
            boldFont = QtGui.QFont()
            boldFont.setBold(True)
            boldFont.setPointSize(fontSize)
            painter.setFont(boldFont)
            if bilanType:
                painter.setPen(utils.colorsBilans[bilanType])
            if len(text) > 10:
                text = utils_functions.u('{0}...').format(text[:10])
            painter.drawText(int(0.4 * fontSize), int(0.4 * fontSize), text)
            painter.setPen(QtGui.QPen(QtCore.Qt.lightGray, 0))
            painter.drawLine(0, w, h, w)
            painter.restore()
        else:
            w = rect.width() - 1
            h = rect.height()
            painter.save()
            painter.translate(rect.bottomLeft())
            painter.drawText(
                QtCore.QRect(0, 0, w, -h), 
                QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter, 
                text)
            painter.setPen(QtGui.QPen(QtCore.Qt.lightGray, 0))
            painter.drawLine(w, 0, w, -h)
            painter.restore()

    def sortTable(self, column):
        self.main.model.sortTable(column)


def viewChanged(main, id_eleve=-1, noRecalc=False):
    """
    blablabla
    """
    #utils_functions.afficheMessage(main, "viewChanged")
    utils_functions.doWaitCursor()
    try:
        main.view.setVisible(False)
        if main.model == None:
            main.model = ProfTableModel(main)
        # si on quitte la vue par élève, on réaffiche toutes les lignes :
        if (main.model.lastViewType == utils.VIEW_ELEVE):
            for row in range(main.model.rowCount()):
                main.view.setRowHidden(row, False)
        if not(noRecalc):
            if main.viewType in utils.VIEWS_IF_TABLEAU:
                main.model.setTable(id_tableau=main.id_tableau)
            elif main.viewType == utils.VIEW_ELEVE:
                main.model.setTable(id_eleve=id_eleve)
            else:
                main.model.setTable()
        main.view.setModel(main.model)

        for i in range(main.model.columnCount()):
            if i < 3:
                main.view.setColumnHidden(i, True)
            else:
                main.view.setColumnHidden(i, False)
        main.view.setAlternatingRowColors(True)
        main.view.setWordWrap(False)
        main.view.setVisible(True)
        main.view.resizeRowsToContents()

        fontSize = main.fontSizeSlider.value()
        main.view.horizontalHeader().setMinimumSectionSize(5)
        main.view.verticalHeader().setMinimumSectionSize(5)

        if main.viewType in utils.VIEWS_WITH_VERTICALS:
            h = 10 * fontSize
            main.view.horizontalHeader().setMinimumHeight(h)
            main.view.horizontalHeader().setMaximumHeight(h)
        else:
            h = 3 * fontSize
            main.view.horizontalHeader().setMinimumHeight(h)
            main.view.horizontalHeader().setMaximumHeight(h)

        if main.viewType in utils.VIEWS_WITH_EDITABLES:
            main.view.setItemDelegate(VeracDelegate(main, main.model))
        main.view.resizeColumnsToContents()

        if main.viewType == utils.VIEW_ITEMS:
            maxWidth = 20 * fontSize
            for i in range(3, main.model.columnCount()):
                if main.view.columnWidth(i) > maxWidth:
                    main.view.setColumnWidth(i, maxWidth)
        elif main.viewType == utils.VIEW_BULLETIN:
            for column in range(main.model.columnCount()):
                if main.model.columns[column]['value'] == 'VSEPARATOR':
                    main.view.setColumnWidth(column, int(0.5 * fontSize))
                elif main.model.columns[column].get('vertical', False):
                    main.view.setColumnWidth(column, 3 * fontSize)
            main.view.setWordWrap(True)
            main.view.resizeRowsToContents()
            minHeight = 6 * fontSize
            for i in range(main.model.rowCount() - 1):
                if main.view.rowHeight(i) < minHeight:
                    main.view.setRowHeight(i, minHeight)
            i = main.model.rowCount() - 1
            if main.view.rowHeight(i) < 2 * minHeight:
                main.view.setRowHeight(i, 2 * minHeight)
            # on règle la largeur de la colonne d'appréciations :
            appreciationColumn = main.model.columnCount() - 1
            main.view.setColumnWidth(appreciationColumn, 100)
            main.view.horizontalHeader().setStretchLastSection(True)
            appreciationWidth = main.view.columnWidth(appreciationColumn)
            main.view.horizontalHeader().setStretchLastSection(False)
            minWidth = 40 * fontSize
            if appreciationWidth < minWidth:
                appreciationWidth = minWidth
            main.view.setColumnWidth(appreciationColumn, appreciationWidth)
        elif main.viewType == utils.VIEW_BILANS:
            for column in range(main.model.columnCount()):
                if main.model.columns[column]['value'] == 'VSEPARATOR':
                    main.view.setColumnWidth(column, int(0.5 * fontSize))
                elif main.model.columns[column].get('vertical', False):
                    main.view.setColumnWidth(column, 3 * fontSize)
        elif main.viewType in utils.VIEWS_STATS:
            for column in range(3, main.model.columnCount()):
                if main.model.columns[column]['value'] == 'VSEPARATOR':
                    main.view.setColumnWidth(column, int(0.5 * fontSize))
                else:
                    main.view.setColumnWidth(column, 5 * fontSize)
        elif main.viewType == utils.VIEW_APPRECIATIONS:
            if utils.selectedPeriod < 999:
                appreciationColumn = 3 + utils.selectedPeriod
            else:
                appreciationColumn = 3 + utils.NB_PERIODES
            main.view.setWordWrap(True)
            for i in range(3, main.model.columnCount()):
                if i == appreciationColumn:
                    main.view.setColumnWidth(i, 50 * fontSize)
                else:
                    main.view.setColumnWidth(i, 30 * fontSize)
            main.view.resizeRowsToContents()
            minHeight = 6 * fontSize
            for i in range(main.model.rowCount() - 1):
                if main.view.rowHeight(i) < minHeight:
                    main.view.setRowHeight(i, minHeight)
            i = main.model.rowCount() - 1
            if main.view.rowHeight(i) < 2 * minHeight:
                main.view.setRowHeight(i, 2 * minHeight)
        elif main.viewType == utils.VIEW_SUIVIS:
            for column in range(main.model.columnCount()):
                if main.model.columns[column].get('vertical', False):
                    main.view.setColumnWidth(column, 3 * fontSize)
            appreciationColumn = main.model.columnCount() - 1
            main.view.setColumnWidth(appreciationColumn, 40 * fontSize)
            main.view.setColumnWidth(appreciationColumn - 2, 10 * fontSize)
            main.view.setColumnWidth(appreciationColumn - 1, 10 * fontSize)
            minHeight = 4 * fontSize
            for i in range(main.model.rowCount()):
                if main.view.rowHeight(i) < minHeight:
                    main.view.setRowHeight(i, minHeight)
        elif main.viewType == utils.VIEW_COUNTS:
            for column in range(main.model.columnCount()):
                if main.model.columns[column]['value'] == 'VSEPARATOR':
                    main.view.setColumnWidth(column, int(0.5 * fontSize))
        elif main.viewType == utils.VIEW_ACCOMPAGNEMENT:
            for column in range(main.model.columnCount()):
                if main.model.columns[column]['value'] == 'VSEPARATOR':
                    main.view.setColumnWidth(column, int(0.5 * fontSize))
                elif main.model.columns[column].get('vertical', False):
                    main.view.setColumnWidth(column, 3 * fontSize)
                elif main.model.columns[column].get('appreciationColumn', False):
                    main.view.setColumnWidth(column, 40 * fontSize)
        elif main.viewType == utils.VIEW_CPT_NUM:
            # ICI cptNum
            for column in range(main.model.columnCount()):
                if main.model.columns[column]['value'] == 'VSEPARATOR':
                    main.view.setColumnWidth(column, int(0.5 * fontSize))
                elif main.model.columns[column].get('vertical', False):
                    main.view.setColumnWidth(column, 3 * fontSize)
            main.view.setWordWrap(True)
            appreciationColumn = main.model.columnCount() - 1
            main.view.setColumnWidth(appreciationColumn, 50 * fontSize)
            main.view.resizeRowsToContents()
            """
            minHeight = 6 * fontSize
            for i in range(main.model.rowCount() - 1):
                if main.view.rowHeight(i) < minHeight:
                    main.view.setRowHeight(i, minHeight)
            i = main.model.rowCount() - 1
            if main.view.rowHeight(i) < 2 * minHeight:
                main.view.setRowHeight(i, 2 * minHeight)
            """
        elif main.viewType == utils.VIEW_DNB:
            appreciationColumn = main.model.columnCount() - 1
            width = 40
            for column in range(main.model.columnCount() - 1):
                if main.view.columnWidth(column) > width:
                    width = main.view.columnWidth(column)
            for column in range(main.model.columnCount()):
                if column == appreciationColumn:
                    main.view.setColumnWidth(column, 40 * fontSize)
                elif column > 2:
                    main.view.setColumnWidth(column, width)
        elif main.viewType == utils.VIEW_ABSENCES:
            width = 40
            for column in range(main.model.columnCount()):
                if main.view.columnWidth(column) > width:
                    width = main.view.columnWidth(column)
            for column in range(main.model.columnCount()):
                if column > 2:
                    main.view.setColumnWidth(column, width)
        elif main.viewType == utils.VIEW_ELEVE:
            main.view.setColumnWidth(3, 25)
            main.view.setColumnWidth(6, 15 * fontSize)
            pris = int(2.5 * utils.STYLE['PM_ScrollBarExtent'])
            for i in (3, 4, 6):
                pris += main.view.columnWidth(i)
            main.view.setColumnWidth(5, main.view.width() - pris)
            for row in range(main.model.rowCount()):
                if main.model.rows[row][5]['value'] == 'HSEPARATOR':
                    main.view.setRowHeight(row, int(0.5 * fontSize))
                    main.view.setRowHidden(row, False)
                elif main.model.rows[row][3].get('child', False):
                    if main.model.rows[row][3].get('expand', False):
                        main.view.setRowHidden(row, False)
                    else:
                        main.view.setRowHidden(row, True)
                else:
                    main.view.setRowHidden(row, False)
        main.view.columnIndexTitle = []
        if main.viewType == utils.VIEW_ITEMS:
            if utils.PYQT == 'PYQT5':
                main.view.horizontalHeader().setSectionsMovable(True)
            else:
                main.view.horizontalHeader().setMovable(True)
            for column in range(main.model.columnCount()):
                titleColumn = main.model.columns[column]['value']
                main.view.columnIndexTitle.append([column, column, titleColumn])
        else:
            if utils.PYQT == 'PYQT5':
                main.view.horizontalHeader().setSectionsMovable(False)
            else:
                main.view.horizontalHeader().setMovable(False)
        updateSelectedActions(main, hasSelection=False)
    finally:
        main.view.setVisible(True)
        utils_functions.restoreCursor()


def updateSelectedActions(main, hasSelection=None):
    selectionState = {
        'hasSelection': False, 
        'hasEditables': False, 
        'evals': [], 
        'texts': [], 
        'notes': [], 
        'dates': [], 
        'horaires': [], 
        'numbers': [], 
        }
    if hasSelection == None:
        hasSelection = len(main.view.selectedIndexes()) > 0
    selectionState['hasSelection'] = hasSelection
    if hasSelection:
        for item in main.view.selectedIndexes():
            row, column = item.row(), item.column()
            itemDatas = main.model.rows[row][column]
            if itemDatas.get('flag', '') == 'EDITABLE':
                if itemDatas.get('type', '') == utils.EVAL:
                    selectionState['evals'].append(item)
                    selectionState['hasEditables'] = True
                elif itemDatas.get('type', '') == utils.TEXT:
                    selectionState['texts'].append(item)
                    selectionState['hasEditables'] = True
                elif itemDatas.get('type', '') == utils.NOTE_PERSO:
                    selectionState['notes'].append(item)
                    selectionState['numbers'].append(item)
                    selectionState['hasEditables'] = True
                elif itemDatas.get('type', '') == utils.DATE:
                    selectionState['dates'].append(item)
                    selectionState['hasEditables'] = True
                elif itemDatas.get('type', '') == utils.HORAIRE:
                    selectionState['horaires'].append(item)
                    selectionState['hasEditables'] = True
            elif itemDatas.get('type', '') == utils.NUMBER:
                # permet de copier un nombre décimal non éditable 
                # (par exemple une moyenne) 
                # en récupérant le séparateur décimal :
                selectionState['numbers'].append(item)
    main.view.selectionState = selectionState

    hasEditables = selectionState['hasEditables']
    periodeOK = not(utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False))
    # actions générales :
    main.actionCut.setEnabled(hasEditables and periodeOK)
    main.actionCopy.setEnabled(hasSelection)
    main.actionPaste.setEnabled(hasEditables and periodeOK)
    main.changeValueComboBox.setEnabled((len(selectionState['evals']) > 0) and periodeOK)
    main.actionSimplify.setEnabled((len(selectionState['evals']) > 0) and periodeOK)
    clearOK = (main.id_tableau > -2) and not(utils.selectedPeriod in (-1, 999))
    main.actionClearIfUnfavorable.setEnabled((len(selectionState['evals']) > 0) and periodeOK and clearOK)
    # action de viewNotes :
    main.actionEditNote.setEnabled(hasSelection and periodeOK)
    main.actionDeleteNote.setEnabled(hasSelection and periodeOK)
    # action de VIEW_COUNTS :
    main.actionEditCount.setEnabled(hasSelection and periodeOK)
    main.actionDeleteCount.setEnabled(hasSelection and periodeOK)
    main.actionValuesDown.setEnabled((len(selectionState['notes']) > 0) and periodeOK)
    main.actionValuesUp.setEnabled((len(selectionState['notes']) > 0) and periodeOK)


class ChoosePeriodeDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, withFirst=True, initialIndex=0):
        super(ChoosePeriodeDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Choose period'))
        label = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Period:')))
        self.comboBox = QtWidgets.QComboBox()
        self.comboBox.setMinimumWidth(200)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 2, 0)
        grid.addWidget(self.comboBox, 2, 1)
        grid.addWidget(buttonBox, 4, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        if withFirst:
            for periode in range(utils.NB_PERIODES):
                self.comboBox.addItem(utils.PERIODES[periode])
        else:
            for periode in range(1, utils.NB_PERIODES):
                self.comboBox.addItem(utils.PERIODES[periode])
        self.comboBox.setCurrentIndex(initialIndex)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('periodes')





















































###########################################################"
#        TABLEAU ACTUEL
###########################################################"

def whoIsSelected(main):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'whoIsSelected')
    selection = []
    for item in main.view.selectedIndexes():
        id_eleve = main.model.rows[item.row()][0]['value']
        name_eleve = main.model.rows[item.row()][2]['value']
        selection.append((id_eleve, name_eleve))
    # on efface les doublons :
    selection = list(set(selection))
    return selection


def whatIsSelected(main):
    """
    blablabla
    """
    selection = []
    message = 'whatIsSelected : '
    for item in main.view.selectedIndexes():
        titleColumn = main.model.columns[item.column()]['value']
        if titleColumn != "Eleve":
            if 'id' in main.model.columns[item.column()]:
                id_item = main.model.columns[item.column()]['id']
            else:
                id_item = -1
            message = utils_functions.u('{0}{1}, ').format(message, titleColumn)
            selection.append((id_item, titleColumn))
    # on efface les doublons :
    selection = list(set(selection))
    utils_functions.afficheMessage(main, message)
    return selection


class ListItemsDlg(utils_2lists.DoubleListDlg):
    """
    explications
    """
    def __init__(self, parent=None, useMainTableau=True):
        self.useMainTableau = useMainTableau
        title = QtWidgets.QApplication.translate(
            'main', 'Modify the list of items (current table)')
        super(ListItemsDlg, self).__init__(
            parent, 
            helpMessage=2, 
            helpContextPage='create-tableau', 
            title=title)

    def remplirListes(self):
        """
        blablabla
        """
        self.Avant = []
        self.Apres = []
        # un tableau de compilation ne peut pas arriver ici :
        if self.useMainTableau:
            tableaux = [self.main.id_tableau]
        # on liste tous les items :
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'UPPER(Name)'), 
            db=self.main.db_my)
        query_my2 = utils_db.query(self.main.db_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            itemText = utils_functions.u(
                '{0} \t[{1}]').format(itemName, itemLabel)
            item = QtWidgets.QListWidgetItem(itemText)
            item.setToolTip(itemLabel)
            item.setData(QtCore.Qt.UserRole, (id_item, itemName))
            DejaLa = False
            if self.useMainTableau:
                for id_tableau in tableaux:
                    commandLine_my2 = (
                        'SELECT * FROM tableau_item '
                        'WHERE id_tableau={0} AND id_item={1}').format(id_tableau, id_item)
                    query_my2 = utils_db.queryExecute(commandLine_my2, query=query_my2)
                    while query_my2.next():
                        DejaLa = True
            if DejaLa == False:
                self.baseList.addItem(item)
        if self.useMainTableau:
            for i in range(self.main.model.columnCount()):
                titleColumn = self.main.model.columns[i]['value']
                if i > 2:
                    commandLine_my = utils_db.qs_prof_AllWhereName.format('items')
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (titleColumn, )}, query=query_my)
                    while query_my.next():
                        id_item = int(query_my.value(0))
                        itemName = query_my.value(1)
                        itemLabel = query_my.value(2)
                        itemText = utils_functions.u(
                            '{0} \t[{1}]').format(itemName, itemLabel)
                        item = QtWidgets.QListWidgetItem(itemText)
                        item.setToolTip(itemLabel)
                        item.setData(QtCore.Qt.UserRole, (id_item, itemName))
                        self.selectionList.addItem(item)
                        self.Avant.append((id_item, itemName))


def refreshListItems(main, dialog):
    """
    déclenché lorsqu'on modifie la liste des items d'un tableau
    """
    result = False
    for i in range(dialog.selectionList.count()):
        item = dialog.selectionList.item(i)
        (id_item, itemName) = item.data(QtCore.Qt.UserRole)
        dialog.Apres.append((id_item, itemName))
    # on récupère la liste des items à supprimer :
    suppressions = []
    for i in dialog.Avant:
        if not(i in dialog.Apres):
            suppressions.append(i)
    # message de confirmation avant la suppression :
    if len(suppressions) > 0:
        message = ''
        total = 0
        for s in suppressions:
            message = utils_functions.u('{0}{1}\n').format(message, s[1])
            total = total + 1
        headMessage1 = QtWidgets.QApplication.translate(
            'main', 'Remove {0} items ?').format(total)
        headMessage2 = QtWidgets.QApplication.translate(
            'main', 'Their scores will be deleted.')
        headMessage = utils_functions.u('<p><b>{0}</b></p><p><b>{1}</b></p>').format(
            headMessage1, headMessage2)
        if utils_functions.messageBox(
            main, level='question', message=headMessage, detailedText=message,
            buttons=['Yes', 'No']) == QtWidgets.QMessageBox.No:
            return result

    utils_functions.doWaitCursor()
    try:
        # besoin d'une transaction :
        query_my, transactionOK = utils_db.queryTransactionDB(main.db_my)
        # on supprime les évaluations :
        for s in suppressions:
            id_item = s[0]
            commandLine_my = utils_db.qdf_prof_evaluations3.format(main.id_tableau, id_item)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)

        # on efface le tableau de la table tableau_item :
        commandLine_my = utils_db.qdf_where.format('tableau_item', 'id_tableau', main.id_tableau)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # et on le réécrit (pour redéfinir les ordres) :
        commandLine_my = utils_db.insertInto('tableau_item')
        lines = []
        ordre = 0
        for item in dialog.Apres:
            id_item = item[0]
            try:
                # on n'inscrit que si cet item était déjà dans ce tableau :
                if main.id_tableau in main.tableauxForItem[id_item]:
                    ordre += 1
                    lines.append((main.id_tableau, id_item, ordre))
            except:
                # erreur déclenchée si l'item n'existait pas avant.
                # on le met alors dans le premier tableau :
                ordre += 1
                lines.append((main.id_tableau, id_item, ordre))
                main.tableauxForItem[id_item] = [main.id_tableau]
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
        result = True
    finally:
        utils_db.endTransaction(query_my, main.db_my, transactionOK)
        utils_functions.restoreCursor()
        tableauxViewChanged(main)
        if len(suppressions) > 0:
            calcAll(main, forcer=True)
        return result


class ListVisibleItemsDlg(utils_2lists.DoubleListDlg):
    """
    explications
    """
    def __init__(self, parent=None):
        title = QtWidgets.QApplication.translate('main', 'Choose the displayed items')
        super(ListVisibleItemsDlg, self).__init__(
            parent, 
            helpMessage=2, 
            helpContextPage='list-visible-items', 
            title=title)

        self.baseGroupBox.setTitle(
            QtWidgets.QApplication.translate('main', 'Hidden Items'))
        self.selectionGroupBox.setTitle(
            QtWidgets.QApplication.translate('main', 'Visibles Items'))

    def remplirListes(self):
        """
        blablabla
        """
        self.Avant = []
        self.Apres = []

        import operator
        columnIndexTitle = []
        for columnDetails in self.main.view.columnIndexTitle:
            columnIndexTitle.append(columnDetails)
        columnIndexTitle = sorted(columnIndexTitle, key=operator.itemgetter(1))
        for columnDetails in columnIndexTitle:
            columnIndex = columnDetails[0]
            if columnIndex > 2:
                commandLine_my = utils_db.qs_prof_AllWhereName.format('items')
                query_my = utils_db.queryExecute(
                    {commandLine_my: (columnDetails[2], )}, db=self.main.db_my)
                while query_my.next():
                    id_item = int(query_my.value(0))
                    itemName = query_my.value(1)
                    itemLabel = query_my.value(2)
                    itemText = utils_functions.u(
                        '{0} \t[{1}]').format(itemName, itemLabel)
                    item = QtWidgets.QListWidgetItem(itemText)
                    item.setToolTip(itemLabel)
                    item.setData(QtCore.Qt.UserRole, columnIndex)
                    if self.main.view.isColumnHidden(columnDetails[0]):
                        self.baseList.addItem(item)
                    else:
                        self.selectionList.addItem(item)
                        self.Avant.append((id_item, itemName))


def refreshListVisibleItems(main, dialog):
    """
    blablabla
    """
    for i in range(dialog.baseList.count()):
        item = dialog.baseList.item(i)
        columnIndex = item.data(QtCore.Qt.UserRole)
        main.view.setColumnHidden(columnIndex, True)
    for i in range(dialog.selectionList.count()):
        item = dialog.selectionList.item(i)
        columnIndex = item.data(QtCore.Qt.UserRole)
        main.view.setColumnHidden(columnIndex, False)
        j = main.view.horizontalHeader().visualIndex(columnIndex)
        if j != i:
            main.view.horizontalHeader().moveSection(j, i)


def hideEmptyColumns(main):
    """
    affiche ou masque les colonnes de la vue items
    selon qu'elles comportent ou non des évaluations.
    """
    columnIndexTitle = []
    for columnDetails in main.view.columnIndexTitle:
        columnIndexTitle.append(columnDetails)
    nbCols = len(columnIndexTitle)
    columnsToShow = {}
    for row in main.model.rows:
        for col in range(3, nbCols):
            if row[col]['value'] != '':
                columnsToShow[col] = True
    for col in range(3, nbCols):
        mustHide = not(col in columnsToShow)
        main.view.setColumnHidden(col, mustHide)









































###########################################################"
#        GESTION DES MODÈLES DE TABLEAUX
###########################################################"

class EditTemplateDlg(QtWidgets.QDialog):
    """
    Pour éditer un modèle (créer ou modifier).
    Pour une création, laisser data vide.
    Si c'est une édition, data contient les données du modèle à éditer :
        data : [id_template, Name, Label]
    """
    def __init__(self, parent=None, data=[]):
        super(EditTemplateDlg, self).__init__(parent)

        self.main = parent
        self.data = data
        # le titre de la fenêtre :
        if len(self.data) > 0:
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'EditTemplate'))
        else:
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'CreateTemplate'))
            # un self.data par défaut en cas de création :
            self.data = [0, '', '']
        # définitions du modèle (nom, label) :
        nameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Name:')))
        self.nameEdit = QtWidgets.QLineEdit()
        labelLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Label:')))
        self.labelEdit = QtWidgets.QLineEdit()
        # on connecte:
        self.nameEdit.textChanged.connect(self.afficheOkButton)
        self.checkLabel = QtWidgets.QLabel()
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']
        # on place tout dans une grille:
        grid = QtWidgets.QGridLayout()
        grid.addWidget(nameLabel,       1, 0)
        grid.addWidget(self.nameEdit,   1, 1, 1, 2)
        grid.addWidget(labelLabel,      2, 0)
        grid.addWidget(self.labelEdit,  2, 1, 1, 2)
        grid.addWidget(self.checkLabel, 3, 1, 1, 2)
        grid.addWidget(buttonBox,       9, 1, 1, 2)
        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)
        # mise à jour de l'affichage :
        self.nameEdit.setText(self.data[1])
        self.labelEdit.setText(self.data[2])
        self.afficheOkButton()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('templates')

    def afficheOkButton(self):
        newName = self.nameEdit.text()
        yes = newName not in ('', )
        if yes:
            # on vérifie que le nom n'est pas déjà attribué :
            query_my = utils_db.query(self.main.db_my)
            commandLine_my = utils_db.qs_prof_AllWhereName.format('templates')
            query_my = utils_db.queryExecute(
                {commandLine_my: (newName, )}, query=query_my)
            if query_my.first():
                if int(query_my.value(0)) == self.data[0]:
                    self.checkLabel.setText('')
                else:
                    yes = False
                    message = QtWidgets.QApplication.translate(
                        'main', 'The name <b>{0}</b> is already used.').format(newName)
                    self.checkLabel.setText(message)
            else:
                self.checkLabel.setText('')
        self.okButton.setEnabled(yes)

    def accept(self):
        id_template = self.data[0]
        self.data[1] = self.nameEdit.text()
        self.data[2] = self.labelEdit.text()
        query_my = utils_db.query(self.main.db_my)
        if id_template == 0:
            # nouveau modèle ; on cherche un id_template (< 0), un ordre
            # et on inscrit dans la table templates :
            id_template = -1
            commandLine_my = utils_db.q_selectAllFromOrder.format(
                'templates', 'id_template')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if query_my.first():
                id_template = int(query_my.value(0)) - 1
            ordre = 0
            commandLine_my = utils_db.q_selectAllFromOrder.format(
                'templates', 'ordre')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if query_my.last():
                ordre = int(query_my.value(2)) + 1
            commandLine_my = utils_db.insertInto('templates')
            query_my = utils_db.queryExecute(
                {commandLine_my: (id_template, self.data[1], ordre, self.data[2])}, 
                query=query_my)
            self.data[0] = id_template
        else:
            # changement de nom ou de label ; on met à jour la table templates :
            commandLine_my = utils_functions.u(
                'UPDATE templates SET Name=?, Label=? WHERE id_template=?')
            query_my = utils_db.queryExecute(
                {commandLine_my: (self.data[1], self.data[2], id_template)}, 
                query=query_my)
        QtWidgets.QDialog.accept(self)


def createTemplateFromTableau(main, templateName='', id_tableau=-1):
    """
    crée un nouveau modèle à partir d'un tableau.
    On met à jour la table tableau_item.
    """
    id_template = 0
    if id_tableau == -1:
        id_tableau = main.id_tableau
    if id_tableau < 0:
        return id_template
    if templateName == '':
        dialog = EditTemplateDlg(parent=main)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            id_template = dialog.data[0]
        else:
            return id_template
    query_my = utils_db.query(main.db_my)
    # les items sont maintenant liés au modèle :
    commandLine_my = ('UPDATE tableau_item set id_tableau={0} '
                    'WHERE id_tableau={1}').format(id_template, id_tableau)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # on lie le tableau au modèle :
    commandLine_my = utils_db.insertInto('tableau_item')
    query_my = utils_db.queryExecute(
        {commandLine_my: (id_tableau, id_template, 0)}, 
        query=query_my)
    main.changeDBMyState()
    return id_template


class ChooseTemplateDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, templates=[]):
        super(ChooseTemplateDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'ChooseTemplate'))
        label = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Template:')))
        self.comboBox = QtWidgets.QComboBox()
        self.comboBox.setMinimumWidth(200)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 2, 0)
        grid.addWidget(self.comboBox, 2, 1)
        grid.addWidget(buttonBox, 4, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # remplissage de la liste déroulante :
        for (id_template, templateName) in templates:
            self.comboBox.addItem(templateName, id_template)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('templates')


class SelectTemplatesDlg(QtWidgets.QDialog):
    """
    Un dialog pour sélectionner un ou plusieurs templates
    """
    def __init__(self, parent=None, templates=[]):
        super(SelectTemplatesDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'SelectTemplates'))

        commentText = QtWidgets.QApplication.translate(
            'main', 'Select the templates you want to export.')
        commentLabel = QtWidgets.QLabel(commentText)

        # liste des modèles :
        templatesText = QtWidgets.QApplication.translate('main', '<p><b>Templates:</b></p>')
        templatesLabel = QtWidgets.QLabel(templatesText)
        self.templatesListWidget = QtWidgets.QListWidget()
        self.templatesListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(commentLabel,                1, 0, 1, 2)
        grid.addWidget(templatesLabel,              2, 0)
        grid.addWidget(self.templatesListWidget,    3, 0, 1, 2)
        grid.addWidget(buttonBox,                   5, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        self.updateTemplates()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('templates')

    def updateTemplates(self):
        self.templatesListWidget.clear()
        query_my = utils_db.query(self.main.db_my)
        # récupération de la liste des modèles :
        commandLine_my = utils_db.q_selectAllFromOrder.format(
            'templates', 'ordre, UPPER(Name)')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_template = int(query_my.value(0))
            templateName = query_my.value(1)
            templateLabel = query_my.value(3)
            label = utils_functions.u('{0}  [{1}]').format(templateName, templateLabel)
            templateWidget = QtWidgets.QListWidgetItem(label)
            templateWidget.setData(QtCore.Qt.UserRole, id_template)
            self.templatesListWidget.addItem(templateWidget)


def linkTableauTemplate(main, id_tableau=-1, id_template=0):
    """
    lie un tableau à un modèle.
    """
    if id_tableau == -1:
        id_tableau = main.id_tableau
    if id_tableau < 0:
        return
    query_my = utils_db.query(main.db_my)
    # sélection ou création du modèle s'il n'est pas passé en paramètre :
    if id_template == 0:
        # récupération de la liste des modèles :
        templates = []
        commandLine_my = utils_db.q_selectAllFrom.format('templates')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_template = int(query_my.value(0))
            templateName = query_my.value(1)
            templates.append((id_template, templateName))
        if len(templates) > 0:
            # si la liste existe, on fait choisir :
            dialog = ChooseTemplateDlg(parent=main, templates=templates)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                currentIndex = dialog.comboBox.currentIndex()
                id_template = dialog.comboBox.itemData(currentIndex)
        else:
            # création d'un nouveau modèle d'après le tableau :
            id_template = createTemplateFromTableau(main, id_tableau=id_tableau)
    if id_template == 0:
        return
    # on récupère la liste des items liés au tableau :
    itemsInTableau = []
    commandLine_my = utils_db.qs_prof_ItemsInTableau.format(id_tableau)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_item = int(query_my.value(0))
        itemsInTableau.append(id_item)
    # on récupère la liste des items liés au modèle :
    itemsInTemplate = []
    commandLine_my = utils_db.qs_prof_ItemsInTableau.format(id_template)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_item = int(query_my.value(0))
        itemsInTemplate.append(id_item)
    # on ajoute au modèle les items qui ne seraient que dans le tableau :
    itemsToAdd = []
    for id_item in itemsInTableau:
        if not(id_item in itemsInTemplate):
            if id_item > -1:
                itemsToAdd.append(id_item)
    commandLine_my = utils_db.insertInto('tableau_item')
    ordre = len(itemsInTemplate)
    lines = []
    for id_item in itemsToAdd:
        lines.append((id_template, id_item, ordre))
        ordre += 1
    query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
    # on efface les liens entre le tableau et les items :
    commandLine_my = utils_db.qdf_where.format('tableau_item', 'id_tableau', id_tableau)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # on lie le tableau au modèle :
    commandLine_my = utils_db.insertInto('tableau_item')
    query_my = utils_db.queryExecute(
        {commandLine_my: (id_tableau, id_template, 0)}, 
        query=query_my)
    main.changeDBMyState()


def unlinkTableauTemplate(main, id_tableau=-1):
    """
    délie le tableau de son modèle.
    """
    if id_tableau == -1:
        id_tableau = main.id_tableau
    if id_tableau < 0:
        return
    query_my = utils_db.query(main.db_my)
    # on récupère l'id_template :
    commandLine_my = utils_db.qs_prof_ItemsInTableau.format(id_tableau)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    if query_my.first():
        id_template = int(query_my.value(0))
    # on efface le lien entre le tableau et son modèle :
    commandLine_my = utils_db.qdf_prof_tableauItem2.format(id_tableau, id_template)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # on récupère la liste des items liés :
    items = []
    commandLine_my = utils_db.qs_prof_ItemsInTableau.format(id_template)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_item = int(query_my.value(0))
        items.append(id_item)
    # on inscrit les liens entre les items et le tableau :
    commandLine_my = utils_db.insertInto('tableau_item')
    ordre = 0
    lines = []
    for id_item in items:
        lines.append((id_tableau, id_item, ordre))
        ordre += 1
    query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
    main.changeDBMyState()


class ManageTemplatesDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, templates=[]):
        super(ManageTemplatesDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate('main', 'ManageTemplates')
        self.setWindowTitle(title)

        # liste des modèles :
        templatesLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate(
            'main', '<p><b>Templates:</b></p>'))
        self.templatesListWidget = utils_2lists.DragDropListWidget(num=1)
        self.templatesListWidget.itemSelectionChanged.connect(self.templateChanged)
        self.id_template = 0
        # le bouton new :
        newButton = QtWidgets.QPushButton(
            utils.doIcon('template-new'), '')
        newButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'NewTemplate'))
        newButton.clicked.connect(self.newTemplate)
        # le bouton newFromBilan :
        newFromBilanButton = QtWidgets.QPushButton(
            utils.doIcon('template-bilan'), '')
        newFromBilanButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'NewTemplateFromBilan'))
        newFromBilanButton.clicked.connect(self.newTemplateFromBilan)
        # le bouton delete :
        deleteButton = QtWidgets.QPushButton(
            utils.doIcon('template-delete'), '')
        deleteButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'DeleteTemplate'))
        deleteButton.clicked.connect(self.deleteTemplate)
        # le bouton edit :
        editButton = QtWidgets.QPushButton(
            utils.doIcon('template-edit'), '')
        editButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'EditTemplate'))
        editButton.clicked.connect(self.editTemplate)
        # un layout pour les boutons :
        templatesButtons = QtWidgets.QHBoxLayout()
        templatesButtons.addStretch(1)
        templatesButtons.addWidget(newButton)
        templatesButtons.addWidget(newFromBilanButton)
        templatesButtons.addWidget(deleteButton)
        templatesButtons.addWidget(editButton)

        # liste des tableaux liés :
        tableauxLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate(
            'main', '<p><b>LinkedTableaux:</b></p>'))
        self.tableauxListWidget = QtWidgets.QListWidget()
        self.tableauxListWidget.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)

        # double liste des items :
        itemsLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate('main', '<p><b>Items:</b></p>'))
        self.selectionWidget = utils_2lists.SelectItems(self.main)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(templatesLabel, 2, 0)
        grid.addWidget(self.templatesListWidget, 3, 0, 1, 3)
        grid.addLayout(templatesButtons, 4, 0)
        grid.addWidget(tableauxLabel, 2, 3)
        grid.addWidget(self.tableauxListWidget, 3, 3, 2, 3)
        grid.addWidget(itemsLabel, 8, 0)
        grid.addWidget(self.selectionWidget, 9, 0, 1, 6)
        grid.addWidget(buttonBox, 10, 3, 1, 3)
        # la grille est le widget de base :
        self.setLayout(grid)

        self.updateTemplates()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('templates')

    def updateTemplates(self):
        self.templatesListWidget.clear()
        query_my = utils_db.query(self.main.db_my)
        # récupération de la liste des modèles :
        commandLine_my = utils_db.q_selectAllFromOrder.format(
            'templates', 'ordre, UPPER(Name)')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_template = int(query_my.value(0))
            templateName = query_my.value(1)
            templateLabel = query_my.value(3)
            label = utils_functions.u('{0}  [{1}]').format(templateName, templateLabel)
            templateWidget = QtWidgets.QListWidgetItem(label)
            data = [id_template, templateName, templateLabel]
            templateWidget.setData(QtCore.Qt.UserRole, data)
            self.templatesListWidget.addItem(templateWidget)
            if self.id_template == 0:
                self.id_template = id_template
                self.templatesListWidget.setCurrentItem(templateWidget)

    def templateChanged(self):
        self.testRecord()
        template = self.templatesListWidget.currentItem()
        self.id_template = template.data(QtCore.Qt.UserRole)[0]
        query_my = utils_db.query(self.main.db_my)
        # récupération de la liste des tableaux liés :
        self.tableauxListWidget.clear()
        commandLine_my = utils_db.qs_prof_tableauxTemplate.format(self.id_template)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_tableau = int(query_my.value(0))
            tableauName = query_my.value(1)
            tableauWidget = QtWidgets.QListWidgetItem(tableauName)
            tableauWidget.setData(QtCore.Qt.UserRole, id_tableau)
            self.tableauxListWidget.addItem(tableauWidget)
        # récupération de la liste des items liés :
        self.selectionWidget.id_template = self.id_template
        self.selectionWidget.remplirListes()

    def testRecord(self):
        """
        vérification des modifications de la liste des items liés au modèle.
        Avant d'inscrire les changements, on vérifie si les items supprimés
        sont évalués dans des tableaux liés au modèle.
        On demande alors quoi faire.
        """
        if not(self.selectionWidget.mustRecord):
            return
        query_my = utils_db.query(self.main.db_my)
        # on récupère les items à inscrire :
        items = []
        for i in range(self.selectionWidget.selectionList.count()):
            item = self.selectionWidget.selectionList.item(i)
            (id_item, itemName) = item.data(QtCore.Qt.UserRole)
            items.append((id_item, itemName))
        # on récupère la liste des items à supprimer :
        deletedItems = []
        for (id_item, itemName) in self.selectionWidget.initialList:
            if not((id_item, itemName) in items):
                deletedItems.append((id_item, itemName))
        # on récupère la liste des tableaux liés au modèle :
        tableaux = []
        for i in range(self.tableauxListWidget.count()):
            tableauWidget = self.tableauxListWidget.item(i)
            id_tableau = tableauWidget.data(QtCore.Qt.UserRole)
            tableauName = tableauWidget.text()
            tableaux.append((id_tableau, tableauName))
        # on recherche les items à supprimer et evalués :
        evaluatedItems = []
        commandLine_myBase = 'SELECT * FROM evaluations WHERE id_tableau={0} AND id_item={1}'
        for (id_tableau, tableauName) in tableaux:
            for (id_item, itemName) in deletedItems:
                commandLine_my = commandLine_myBase.format(id_tableau, id_item)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                if query_my.first():
                    evaluatedItems.append((id_tableau, tableauName, id_item, itemName))
        # si evaluatedItems n'est pas vide, on demande confirmation :
        if len(evaluatedItems) > 0:
            message = ''
            listItems = []
            evaluatedItemMessage = QtWidgets.QApplication.translate(
                'main', '{0}item {1} in tableau {2}\n')
            for (id_tableau, tableauName, id_item, itemName) in evaluatedItems:
                message = utils_functions.u(evaluatedItemMessage).format(
                    message, itemName, tableauName)
                if not(id_item in listItems):
                    listItems.append(id_item)
            total = len(listItems)
            if total == 1:
                m1 = QtWidgets.QApplication.translate(
                    'main',
                    '<b>One item to be removed is evaluated</b> '
                    'in tables related to this model.')
            else:
                m1 = QtWidgets.QApplication.translate(
                    'main',
                    '<b>{0} items to be removed are evaluated</b> '
                    'in tables related to this model.').format(total)
            m2 = QtWidgets.QApplication.translate(
                'main', 'If you continue, <b>these evaluations will be removed</b>.')
            m3 = QtWidgets.QApplication.translate(
                'main', 'Are you certain to want to continue?')
            headMessage = utils_functions.u(
                '<p>{1}</p>'
                '<p>{2}</p>'
                '<p align="center">{3}</p>'
                '<p align="center">{0}</p>').format(utils.SEPARATOR_LINE, m1, m2, m3)
            if utils_functions.messageBox(
                self.main, level='warning', message=headMessage, detailedText=message,
                buttons=['Yes', 'No']) == QtWidgets.QMessageBox.No:
                return
        # on réécrit les liens entre le modèle et les items :
        commandLine_my = utils_db.qdf_where.format('tableau_item', 'id_tableau', self.id_template)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        commandLine_my = utils_db.insertInto('tableau_item')
        ordre = 0
        lines = []
        for (id_item, itemName) in items:
            lines.append((self.id_template, id_item, ordre))
            ordre += 1
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
        # on efface les évaluations obsolètes :
        commandLine_items = ''
        separator = ''
        for (id_item, itemName) in items:
            commandLine_items = '{0}{1}id_item={2}'.format(
                commandLine_items, separator, id_item)
            separator = ' OR '
        commandLine_base = 'DELETE FROM evaluations WHERE id_tableau={0} AND NOT({1})'
        for (id_tableau, tableauName) in tableaux:
            commandLine_my = commandLine_base.format(id_tableau, commandLine_items)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # terminé :
        self.main.changeDBMyState()
        self.selectionWidget.mustRecord = False

    def accept(self):
        self.testRecord()
        if self.templatesListWidget.drop:
            # on a changé l'ordre des modèles
            # on récrit la base template :
            query_my = utils_db.query(self.main.db_my)
            commandLine_my = utils_db.qdf_table.format('templates')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            commandLine_my = utils_db.insertInto('templates')
            lines = []
            for i in range(self.templatesListWidget.count()):
                item = self.templatesListWidget.item(i)
                [id_template, templateName, templateLabel] = item.data(QtCore.Qt.UserRole)
                ordre = i
                lines.append((id_template, templateName, ordre, templateLabel))
            query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
            self.main.changeDBMyState()
        # terminé :
        QtWidgets.QDialog.accept(self)

    def newTemplate(self):
        # on crée un nouveau modèle :
        dialog = EditTemplateDlg(parent=self.main)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.updateTemplates()
            self.main.changeDBMyState()

    def newTemplateFromBilan(self):
        # on crée un nouveau modèle :
        import prof_itemsbilans
        comment1 = QtWidgets.QApplication.translate(
            'main', 'Select an assessment.')
        comment2 = QtWidgets.QApplication.translate(
            'main', 'All items related to this assessment will be added to the template.')
        comments = utils_functions.u(
            '<p align="center">{0}<br/>{1}</p>'
            '<p align="center">---------------------------<br/></p>').format(comment1, comment2)
        dialog = prof_itemsbilans.ChooseBilanDlg(
            parent=self.main, 
            comments=comments, 
            helpContextPage='templates')
        dialog.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'NewTemplateFromBilan'))
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        id_bilan = dialog.comboBox.itemData(dialog.comboBox.currentIndex())
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'bilans', 'id_bilan', id_bilan)
        query_my = utils_db.query(self.main.db_my)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            bilanName = query_my.value(1)
            bilanLabel = query_my.value(2)
        # on vérifie que le nom n'est pas déjà attribué :
        commandLine_my = utils_db.qs_prof_AllWhereName.format('templates')
        query_my = utils_db.queryExecute(
            {commandLine_my: (bilanName, )}, query=query_my)
        if query_my.first():
            message = QtWidgets.QApplication.translate(
                'main', 'The name <b>{0}</b> is already used.').format(bilanName)
            utils_functions.messageBox(self.main, level='warning', message=message)
            return
        # on cherche un id_template (< 0), un ordre
        # et on inscrit le modèle dans la table templates :
        id_template = -1
        commandLine_my = utils_db.q_selectAllFromOrder.format(
            'templates', 'id_template')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        if query_my.first():
            id_template = int(query_my.value(0)) - 1
        ordre = 0
        commandLine_my = utils_db.q_selectAllFromOrder.format(
            'templates', 'ordre')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        if query_my.last():
            ordre = int(query_my.value(2)) + 1
        commandLine_my = utils_db.insertInto('templates')
        query_my = utils_db.queryExecute(
            {commandLine_my: (id_template, bilanName, ordre, bilanLabel)}, 
            query=query_my)
        # on récupère les items liés au bilan :
        commandLine_my = (
            'SELECT items.* '
            'FROM item_bilan '
            'JOIN items ON items.id_item=item_bilan.id_item '
            'WHERE item_bilan.id_bilan={0} '
            'ORDER BY UPPER(items.Name)').format(id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        lines = []
        ordre = 0
        while query_my.next():
            id_item = int(query_my.value(0))
            lines.append((id_template, id_item, ordre))
            ordre += 1
        commandLine_my = utils_db.insertInto('tableau_item')
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
        # terminé :
        self.updateTemplates()
        self.main.changeDBMyState()

    def deleteTemplate(self):
        # on supprime le modèle :
        item = self.templatesListWidget.currentItem()
        [id_template, templateName, templateLabel] = item.data(QtCore.Qt.UserRole)
        if self.tableauxListWidget.count() > 0:
            tableaux = []
            for i in range(self.tableauxListWidget.count()):
                tableau_item = self.tableauxListWidget.item(i)
                id_tableau = tableau_item.data(QtCore.Qt.UserRole)
                tableauName = tableau_item.text()
                tableaux.append((id_tableau, tableauName))
            message = ''
            total = 0
            for (id_tableau, tableauName) in tableaux:
                message = utils_functions.u('{0}{1}\n').format(message, tableauName)
                total += 1
            headMessage = QtWidgets.QApplication.translate(
                'main', 'Unlink {0} tableaux ?').format(total)
            headMessage = utils_functions.u('<p><b>{0}</b></p>').format(headMessage)

            if utils_functions.messageBox(
                self.main, level='question', message=headMessage, detailedText=message,
                buttons=['Yes', 'No']) == QtWidgets.QMessageBox.No:
                return
            for (id_tableau, tableauName) in tableaux:
                unlinkTableauTemplate(self.main, id_tableau)
        # enfin on efface le modèle des tables templates et tableau_item :
        commandLine_my = utils_db.qdf_where.format('templates', 'id_template', id_template)
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        commandLine_my = utils_db.qdf_where.format('tableau_item', 'id_tableau', id_template)
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        # terminé :
        self.updateTemplates()
        self.main.changeDBMyState()

    def editTemplate(self):
        # on édite le modèle (changement de nom) :
        item = self.templatesListWidget.currentItem()
        dialog = EditTemplateDlg(parent=self.main, data=item.data(QtCore.Qt.UserRole))
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.updateTemplates()
            self.main.changeDBMyState()














































###########################################################"
#        GESTION DES NOTES
###########################################################"

class EditNoteDlg(QtWidgets.QDialog):
    """
    Pour éditer une note (créer ou modifier).
    Pour une création, laisser data vide.
    Si c'est une édition, data contient les données de la note à éditer :
        data = [id_groupe, Periode, id_note, Name, base, coeff, calcMode]
    """
    def __init__(self, parent=None, data=[]):
        super(EditNoteDlg, self).__init__(parent)

        self.main = parent
        self.data = data
        # le titre de la fenêtre :
        if len(self.data) > 0:
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'EditNote'))
        else:
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'CreateNote'))
            # un self.data par défaut en cas de création :
            self.data = [self.main.id_groupe, utils.selectedPeriod, -1, '', 20, 1.0, -1]

        # définitions de la note (nom, base et coefficient) :
        nameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Name:')))
        self.nameEdit = QtWidgets.QLineEdit()
        baseLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Base:')))
        self.baseSpinBox = QtWidgets.QSpinBox()
        self.baseSpinBox.setRange(1, 100)
        self.baseSpinBox.setSingleStep(1)
        coeffLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Coeff:')))
        self.coeffSpinBox = QtWidgets.QDoubleSpinBox()
        self.coeffSpinBox.setMinimum(0.0)
        self.coeffSpinBox.setMaximum(10.0)
        self.coeffSpinBox.setSingleStep(0.5)

        # pour empêcher d'utiliser le nom "Moyenne" :
        self.titleColumnMoyenne = self.main.model.columns[self.main.model.columnCount() - 1]['value']

        # pour une note calculée :
        self.calcCheckBox = QtWidgets.QCheckBox(QtWidgets.QApplication.translate(
            'main', 'CalculatedNote'))
        self.calcComboBox = QtWidgets.QComboBox()
        self.calcComboBox.setToolTip(QtWidgets.QApplication.translate(
            'main', 'ChooseCalculatedNoteModeStatusTip'))

        # on connecte:
        self.nameEdit.textChanged.connect(self.afficheOkButton)
        self.calcCheckBox.stateChanged.connect(self.calcCheckBoxChanged)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # on place tout dans une grille:
        grid = QtWidgets.QGridLayout()
        grid.addWidget(nameLabel, 1, 0)
        grid.addWidget(self.nameEdit, 1, 1, 1, 2)
        grid.addWidget(baseLabel, 2, 0)
        grid.addWidget(self.baseSpinBox, 2, 1, 1, 2)
        grid.addWidget(coeffLabel, 3, 0)
        grid.addWidget(self.coeffSpinBox, 3, 1, 1, 2)
        grid.addWidget(self.calcCheckBox, 5, 1, 1, 2)
        grid.addWidget(self.calcComboBox, 6, 1, 1, 2)
        grid.addWidget(buttonBox, 9, 1, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # remplissage de la combo calcMode :
        calculatedNoteModeItems = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModeItems')
        calculatedNoteModeBilans = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModeBilans')
        calculatedNoteModePersos = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModePersos')
        calculatedNoteModeBLT = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModeBLT')
        for calcMode in [
            calculatedNoteModeItems, 
            calculatedNoteModeBilans, 
            calculatedNoteModePersos, 
            calculatedNoteModeBLT]:
            self.calcComboBox.addItem(calcMode)

        # mise à jour de l'affichage :
        self.nameEdit.setText(self.data[3])
        self.baseSpinBox.setValue(self.data[4])
        self.coeffSpinBox.setValue(self.data[5])
        if self.data[6] < 0:
            self.calcCheckBox.setChecked(False)
            self.calcComboBox.setCurrentIndex(0)
        else:
            self.calcCheckBox.setChecked(True)
            self.calcComboBox.setCurrentIndex(self.data[6])
        self.calcCheckBoxChanged()
        self.afficheOkButton()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-note')

    def afficheOkButton(self):
        yes = self.nameEdit.text() not in ('', self.titleColumnMoyenne)
        self.okButton.setEnabled(yes)

    def calcCheckBoxChanged(self):
        self.calcComboBox.setEnabled(self.calcCheckBox.isChecked())

    def accept(self):
        self.data[3] = self.nameEdit.text()
        self.data[4] = self.baseSpinBox.value()
        self.data[5] = self.coeffSpinBox.value()
        if not(self.calcCheckBox.isChecked()):
            self.data[6] = -1
        else:
            self.data[6] = self.calcComboBox.currentIndex()
        QtWidgets.QDialog.accept(self)


def addNote(main, data):
    """
    data :          id_groupe, Periode, -1,         Name, base, coeff, calcMode
    dans la base :  id_groupe, Periode, id_note,    Name, base, coeff, calcMode, ordre
    return : id_note, ordre
    """
    id_note, ordre = 0, 0
    commandLine_my = utils_db.qs_prof_notes.format(data[0], data[1])
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        id_note = int(query_my.value(2)) + 1
        ordre = int(query_my.value(7)) + 1

    commandLine_my = utils_db.insertInto('notes')
    listArgs = (data[0], data[1], id_note, data[3], 
                data[4], data[5], data[6], ordre)
    query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
    return id_note


def editNote(main, oldData, newData):
    """
    data :          id_groupe, Periode, id_note,    Name, base, coeff, calcMode
    dans la base :  id_groupe, Periode, id_note,    Name, base, coeff, calcMode, ordre
    """
    # récupération de l'ordre :
    ordre = 0
    commandLine_my = utils_db.qs_prof_notes2.format(newData[0], newData[1], newData[2])
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        ordre = int(query_my.value(7))
    # suppression de l'ancien enregistrement :
    commandLine_my = utils_db.qdf_prof_notes.format(newData[0], newData[1], newData[2])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # inscription du nouvel enregistrement :
    commandLine_my = utils_db.insertInto('notes')
    listArgs = (newData[0], newData[1], newData[2], newData[3], 
                newData[4], newData[5], newData[6], ordre)
    query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)


def deleteNote(main, data):
    """
    data :          id_groupe, Periode, id_note,    Name, base, coeff, calcMode
    """
    # besoin d'un query :
    query_my = utils_db.query(main.db_my)
    # on efface les valeurs (table notes_values) :
    commandLine_my = utils_db.qdf_prof_noteValue2.format(data[0], data[1], data[2])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # on efface la note (table notes) :
    commandLine_my = utils_db.qdf_prof_notes.format(data[0], data[1], data[2])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # on recalcule les moyennes :
    recalculeMoyennes(main, data, column=0)


def recalculeNote(main, data, allGroup=True, column=0):
    """
    data :          id_groupe, Periode, id_note,    Name, base, coeff, calcMode
    dans la base :  id_groupe, Periode, id_note,    id_eleve, value

    si allGroup, tous les élèves sont recalculés, 
    sinon on utilise les id_eleve contenus dans la liste whatIsModified['id_eleves'].

    Si on passe column=-1, on ne met pas à jour l'affichage
    (par exemple si on enregistre la base sans être dans la vue notes)
    sinon on cherche la dernière colonne (pour afficher la moyenne).
    """
    if column > -1:
        c = 0
        for col in main.model.columns:
            if 'id' in col:
                id_col = col['id']
            else:
                id_col = -1
            if id_col == data[2]:
                column = c
            c += 1
    if data[6] < 0:
        recalculeMoyennes(main, data, column=column)
        return
    utils_functions.doWaitCursor()
    try:
        # récupération de la liste des id_eleve :
        students = []
        if allGroup:
            elevesInGroup = listStudentsInGroup(main, id_groupe=data[0])
            for eleve in elevesInGroup:
                students.append(eleve[0])
        else:
            for id_eleve in main.whatIsModified['id_eleves']:
                students.append(id_eleve)
        id_students = utils_functions.array2string(students)
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        # on efface les anciennes valeurs :
        commandLine_my = utils_db.qdf_prof_noteValue2.format(data[0], data[1], data[2])
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # une liste pour les nouvelles lignes à inscrire dans la table notes_values :
        listNoteValues = []
        # calcul des notes :
        if data[6] == 0:
            # calcul d'après les items :
            id_groupe = data[0]
            tableauxInCompil = []
            # ICI COUNTS
            commandLine_my = (
                'SELECT id_tableau FROM tableaux'
                ' WHERE id_groupe={0} AND Public=1'
                ' AND (Periode={1} OR Periode=0)')
            commandLine_my = commandLine_my.format(id_groupe, data[1])
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                tableauxInCompil.append(int(query_my.value(0)))
            base = data[4]
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            valuesDic = {}
            id_tableaux = utils_functions.array2string(tableauxInCompil)
            id_students = utils_functions.array2string(students)
            commandLine_my = ('SELECT * FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_bilan=-1').format(id_tableaux, id_students)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(1))
                value = query_my.value(4)
                if id_eleve in valuesDic:
                    valuesDic[id_eleve] += value
                else:
                    valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in students:
                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        elif data[6] == 1:
            # calcul d'après les bilans :
            id_groupe = data[0]
            periodeToUse = diversFromSelection(main, id_groupe=id_groupe, periode=data[1])[7][0]
            base = data[4]
            id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            valuesDic = {}
            commandLine_my = (
                'SELECT id_eleve, value FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_item=-1').format(id_selection, id_students)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                value = query_my.value(1)
                if id_eleve in valuesDic:
                    valuesDic[id_eleve] += value
                else:
                    valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in students:
                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        elif data[6] == 2:
            # calcul d'après les bilans persos :
            id_groupe = data[0]
            periodeToUse = diversFromSelection(main, id_groupe=id_groupe, periode=data[1])[7][0]
            base = data[4]
            id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            valuesDic = {}
            commandLine_my = (
                'SELECT evaluations.id_eleve, evaluations.value '
                'FROM evaluations '
                'JOIN bilans ON bilans.id_bilan=evaluations.id_bilan '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND bilans.id_competence=-1').format(id_selection, id_students)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                value = query_my.value(1)
                if id_eleve in valuesDic:
                    valuesDic[id_eleve] += value
                else:
                    valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in students:
                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        elif data[6] == 3:
            # calcul d'après les bilans persos sélectionnés pour le bulletin :
            # on doit aussi gérer les profils.
            id_groupe = data[0]
            dataSelection = diversFromSelection(main, id_groupe=id_groupe, periode=data[1])
            periodeToUse = dataSelection[7][0]
            matiereName = dataSelection[2]
            base = data[4]
            id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)
            # récupération des profils et des bilans sélectionnés pour chacun :
            studentsAndGroupProfils = getStudentsAndGroupProfils(
                main, id_students, periodeToUse, id_groupe, matiereName=matiereName)
            # récupération des bilans liés aux différents profils :
            bilansProfils = getBilansProfils(main)
            # récupération des bilans à utiliser pour chaque élève :
            elevesBilans = {}
            for id_eleve in students:
                elevesBilans[id_eleve] = []
                # il faut récupérer la liste des profils valables de l'élève :
                eleveProfilsDic = studentsAndGroupProfils['STUDENTS'][id_eleve]
                for profilPeriode in eleveProfilsDic:
                    id_profil = eleveProfilsDic[profilPeriode]
                    for id_bilan in bilansProfils:
                        if id_profil in bilansProfils[id_bilan]:
                            if not(id_bilan in elevesBilans[id_eleve]):
                                elevesBilans[id_eleve].append(id_bilan)
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            valuesDic = {}
            commandLine_my = (
                'SELECT evaluations.id_eleve, evaluations.id_bilan, evaluations.value '
                'FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_item=-1').format(id_selection, id_students)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                id_bilan = int(query_my.value(1))
                # on ne prend en compte que si le bilan est sélectionné :
                if id_bilan in elevesBilans[id_eleve]:
                    value = query_my.value(2)
                    if id_eleve in valuesDic:
                        valuesDic[id_eleve] += value
                    else:
                        valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in students:
                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        # inscription des nouveaux enregistrements :
        commandLine_my = utils_db.insertInto('notes_values')
        query_my = utils_db.queryExecute(
            {commandLine_my: listNoteValues}, query=query_my)
        # calcul et inscription de la moyenne :
        if allGroup:
            recalculeMoyennes(main, data, column=column)
        else:
            recalculeMoyennes(main, data, id_eleve=-2, column=column)
    except:
        print('error in recalculeNote')
    finally:
        utils_functions.restoreCursor()


def recalculeMoyennes(main, data, id_eleve=-1, row=-1, column=-1):
    """
    Calcule la moyenne de la note (données passées via data),
    ainsi que la moyenne de l'élève (via id_eleve),
    la moyenne des moyennes (pour la période sélectionnée)
    et enfin les moyennes du bilan annuel.

    Si id_eleve = -1, on doit recalculer pour tous les élèves du groupe.
    Si id_eleve = -2, on recalcule pour les id_eleve contenus dans la liste 
        whatIsModified['id_eleves'].
    Sinon, on ne calcule que pour l'élève correspondant.

    data :          id_groupe, Periode, id_note,    Name, base, coeff, calcMode
    dans la base :  id_groupe, Periode, id_note,    id_eleve, value
                    (avec id_eleve = - id_groupe - 1)

    row, column : indiquent la case sélectionnée
        cela permet de mettre à jour l'affichage
        (row = -1 si aucune case sélectionnée, c'est à dire si id_eleve = -1)
        (column = -1 si on ne veut pas mettre à jour l'affichage)
    """

    # cas de mise à jour de l'affichage :
    mustUpdateView = (column >= 0) and (id_eleve > -2)
    # récupération de la liste des id_eleve :
    ids_eleves = []
    if id_eleve == -1:
        elevesInGroup = listStudentsInGroup(main, id_groupe=data[0])
        for eleve in elevesInGroup:
            ids_eleves.append(eleve[0])
        row = 0
    elif id_eleve == -2:
        for idEleve in main.whatIsModified['id_eleves']:
            ids_eleves.append(idEleve)
    else:
        ids_eleves.append(id_eleve)

    # Calcul de la moyenne de la note indiquée :
    somme, effectif, moyenne = 0.0, 0, ''
    commandLine_my = utils_db.qs_prof_noteValue2.format(data[0], data[1], data[2])
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        value = query_my.value(0)
        if value != 'X':
            somme += float(value)
            effectif += 1
    if effectif > 0:
        moyenne = round(somme / effectif, utils.precisionNotes)
    # inscription de la moyenne dans la table notes_values :
    idForGroupe = - data[0] - 1
    commandLine_my = utils_db.qdf_prof_noteValue.format(data[0], data[1], data[2], idForGroupe)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    if moyenne != '':
        commandLine_my = utils_db.insertInto('notes_values')
        listArgs = (data[0], data[1], data[2], idForGroupe, moyenne)
        query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
    # et on modifie l'affichage :
    if mustUpdateView:
        rowGroupe = main.model.rowCount() - 1
        item = main.model.index(rowGroupe, column)
        main.model.rows[rowGroupe][column]['value'] = moyenne
        main.view.update(item)

    # Pour chaque élève :
    lines = []
    for id_eleve in ids_eleves:
        # calcul de la moyenne de l'élève pour la période sélectionnée :
        somme, effectif, moyenne = 0.0, 0, ''
        commandLine_my = utils_db.qs_prof_noteValues.format(data[0], data[1], id_eleve)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            base = float(query_my.value(1))
            coeff = float(query_my.value(2))
            value = query_my.value(0)
            if value != 'X':
                value = float(query_my.value(0))
                value = 20 *  value / base
                somme += coeff * value
                effectif += coeff
        if effectif > 0:
            moyenne = round(somme / effectif, utils.precisionNotes)
        # inscription de la moyenne dans la table notes_values :
        commandLine_my = utils_db.qdf_prof_noteValue.format(data[0], data[1], -1, id_eleve)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        if moyenne != '':
            listArgs = [data[0], data[1], -1, id_eleve, moyenne]
            lines.append(listArgs)
        # et on modifie l'affichage :
        if mustUpdateView:
            columnMoyenne = main.model.columnCount() - 1
            item = main.model.index(row, columnMoyenne)
            main.model.rows[row][columnMoyenne]['value'] = moyenne
            main.view.update(item)
            row += 1
    # inscription de la moyenne dans la table notes_values :
    commandLine_my = utils_db.insertInto('notes_values')
    query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

    # Calcul de la moyenne des moyennes :
    somme, effectif, moyenne = 0.0, 0, ''
    commandLine_my = utils_db.qs_prof_noteValue2.format(data[0], data[1], -1)
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        value = query_my.value(0)
        if value != 'X':
            somme += float(value)
            effectif += 1
    if effectif > 0:
        moyenne = round(somme / effectif, utils.precisionNotes)
    # inscription de la moyenne dans la table notes_values :
    idForGroupe = - data[0] - 1
    commandLine_my = utils_db.qdf_prof_noteValue.format(data[0], data[1], -1, idForGroupe)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    if moyenne != '':
        commandLine_my = utils_db.insertInto('notes_values')
        listArgs = (data[0], data[1], -1, idForGroupe, moyenne)
        query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
    # et on modifie l'affichage :
    if mustUpdateView:
        rowGroupe = main.model.rowCount() - 1
        columnMoyenne = main.model.columnCount() - 1
        item = main.model.index(rowGroupe, columnMoyenne)
        main.model.rows[rowGroupe][columnMoyenne]['value'] = moyenne
        main.view.update(item)

    # Calcul des bilans annuels :
    elevesNotes = {}
    commandLine_my = (
        'SELECT * FROM notes_values '
        'WHERE id_groupe={0} '
        'AND Periode<999 '
        'AND id_note=-1 '
        'AND id_eleve>-1').format(data[0])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_eleve = int(query_my.value(3))
        moyenne = query_my.value(4)
        if moyenne != 'X':
            moyenne = float(query_my.value(4))
        if id_eleve in elevesNotes:
            elevesNotes[id_eleve].append(moyenne)
        else:
            elevesNotes[id_eleve] = [moyenne]
    # on efface les anciennes valeurs :
    commandLine_my = utils_db.qdf_prof_noteValue2.format(data[0], 999, -1)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # une liste pour calculer la moyenne du groupe :
    moyennesNotes = []
    lines = []
    for id_eleve in elevesNotes:
        # Calcul de la moyenne annuelle de l'élève :
        somme, moyenne = 0.0, ''
        effectif = len(elevesNotes[id_eleve])
        for value in  elevesNotes[id_eleve]:
            somme += value
        if effectif > 0:
            moyenne = round(somme / effectif, utils.precisionNotes)
        # inscription de la moyenne dans la table notes_values :
        if moyenne != '':
            listArgs = [data[0], 999, -1, id_eleve, moyenne]
            lines.append(listArgs)
        # et ajout à la liste moyennesNotes du groupe :
        moyennesNotes.append(moyenne)
    # inscription de la moyenne dans la table notes_values :
    commandLine_my = utils_db.insertInto('notes_values')
    query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
    # Calcul de la moyenne du groupe :
    somme, moyenne = 0.0, ''
    effectif = len(moyennesNotes)
    for value in  moyennesNotes:
        somme += value
    if effectif > 0:
        moyenne = round(somme / effectif, utils.precisionNotes)
    # inscription de la moyenne dans la table notes_values :
    if moyenne != '':
        commandLine_my = utils_db.insertInto('notes_values')
        listArgs = (data[0], 999, -1, idForGroupe, moyenne)
        query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)















































###########################################################"
#        GESTION DES COMPTAGES (COUNTS)
###########################################################"

class EditCountDlg(QtWidgets.QDialog):
    """
    Pour éditer un comptage (créer ou modifier).
    Pour une création, laisser data vide.
    Si c'est une édition, data contient les données du comptage à éditer :
        data : (id_groupe, Periode, id_count, Name, sens, calcul, what, Label, id_item)
    """
    def __init__(self, parent=None, data=[]):
        super(EditCountDlg, self).__init__(parent)

        self.main = parent
        self.data = data
        # le titre de la fenêtre :
        if len(self.data) > 0:
            self.newCount = False
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'EditCount'))
        else:
            self.newCount = True
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'CreateCount'))
            # un self.data par défaut en cas de création :
            self.data = [self.main.id_groupe, utils.selectedPeriod, -1, '', 1, 1, 0, '', -1]

        # des variables utiles :
        self.oldName = self.data[3]
        self.countNames = ['', ]
        self.items = {-1: 0}

        # définitions du comptage (nom, label, item lié, sens et calcul) :
        nameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Name:')))
        if self.newCount:
            self.nameEdit = QtWidgets.QComboBox()
            self.nameEdit.setEditable(True)
        else:
            self.nameEdit = QtWidgets.QLineEdit()
        labelLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Label:')))
        self.labelEdit = QtWidgets.QLineEdit()
        # item lié :
        itemLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Linked Item:')))
        self.itemComboBox = QtWidgets.QComboBox()
        # sens :
        sensLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Sense:')))
        sensGroupBox = QtWidgets.QGroupBox()
        self.growingRadio = QtWidgets.QRadioButton(QtWidgets.QApplication.translate('main', 'Growing'))
        self.decreasingRadio = QtWidgets.QRadioButton(QtWidgets.QApplication.translate('main', 'Decreasing'))
        sensVbox = QtWidgets.QVBoxLayout()
        sensVbox.addWidget(self.growingRadio)
        sensVbox.addWidget(self.decreasingRadio)
        sensVbox.addStretch(1)
        sensGroupBox.setLayout(sensVbox)
        # calcul :
        calculLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Calculation:')))
        calculGroupBox = QtWidgets.QGroupBox()
        self.averageRadio = QtWidgets.QRadioButton(QtWidgets.QApplication.translate(
            'main', 'Average and Standard deviation'))
        self.medianRadio = QtWidgets.QRadioButton(QtWidgets.QApplication.translate(
            'main', 'Median and Quartiles'))
        self.noRadio = QtWidgets.QRadioButton(QtWidgets.QApplication.translate(
            'main', 'No calculation'))
        calculVbox = QtWidgets.QVBoxLayout()
        calculVbox.addWidget(self.medianRadio)
        calculVbox.addWidget(self.averageRadio)
        calculVbox.addWidget(self.noRadio)
        calculVbox.addStretch(1)
        calculGroupBox.setLayout(calculVbox)

        # on connecte:
        if self.newCount:
            self.nameEdit.editTextChanged.connect(self.afficheOkButton)
            self.nameEdit.currentIndexChanged.connect(self.nameEditChanged)
        else:
            self.nameEdit.textChanged.connect(self.afficheOkButton)
        self.itemComboBox.currentIndexChanged.connect(self.itemComboBoxChanged)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # on place tout dans une grille:
        grid = QtWidgets.QGridLayout()
        grid.addWidget(nameLabel,           1, 0)
        grid.addWidget(self.nameEdit,       1, 1, 1, 2)
        grid.addWidget(labelLabel,          2, 0)
        grid.addWidget(self.labelEdit,      2, 1, 1, 2)
        grid.addWidget(itemLabel,           3, 0)
        grid.addWidget(self.itemComboBox,   3, 1, 1, 2)
        grid.addWidget(sensLabel,           4, 0)
        grid.addWidget(sensGroupBox,        4, 1, 1, 2)
        grid.addWidget(calculLabel,         5, 0)
        grid.addWidget(calculGroupBox,      5, 1, 1, 2)
        grid.addWidget(buttonBox,           9, 1, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # mise à jour de l'affichage :
        initialCountData = (
            self.data[3], self.data[4], self.data[5], self.data[7], self.data[8])
        query_my = utils_db.query(self.main.db_my)
        # remplissage de la liste des items :
        self.itemComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'NO LINKED ITEM'), (-1, '', ''))
        itemIndex = 0
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            itemIndex += 1
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            itemText = utils_functions.u(
                '{0} \t[{1}]').format(itemName, itemLabel)
            itemData = (id_item, itemName, itemLabel)
            self.items[id_item] = itemIndex
            self.itemComboBox.addItem(itemText, itemData)
        if self.newCount:
            # on récupère les comptages existants déjà dans la sélection
            # pour ne pas les afficher dans la liste :
            commandLine_my = utils_db.qs_prof_counts.format(self.data[0], self.data[1])
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                countName = query_my.value(3)
                self.countNames.append(countName)
            # remplissage de la liste des comptages :
            self.nameEdit.addItem(self.data[3], initialCountData)
            query_my = utils_db.queryExecute(
                utils_db.qs_prof_counts3, query=query_my)
            while query_my.next():
                countName = query_my.value(0)
                sens = int(query_my.value(1))
                calcul = int(query_my.value(2))
                countLabel = query_my.value(3)
                id_item = int(query_my.value(4))
                countData = (countName, sens, calcul, countLabel, id_item)
                if not(countName in self.countNames):
                    self.nameEdit.addItem(countName, countData)
            self.nameEdit.setCurrentIndex(0)
        else:
            self.nameEdit.setText(self.data[3])
            # on récupère les autres noms existants
            # pour les interdire en cas de renommage :
            query_my = utils_db.queryExecute(
                utils_db.qs_prof_counts3, query=query_my)
            while query_my.next():
                countName = query_my.value(0)
                sens = int(query_my.value(1))
                calcul = int(query_my.value(2))
                countLabel = query_my.value(3)
                id_item = int(query_my.value(4))
                countData = (countName, sens, calcul, countLabel, id_item)
                if countData != initialCountData:
                    self.countNames.append(countName)
            # on met à jour label, item, sens et calcul :
            self.labelEdit.setText(self.data[7])
            itemIndex = self.items.get(self.data[8], 0)
            self.itemComboBox.setCurrentIndex(itemIndex)
            if self.data[4] > 0:
                self.growingRadio.setChecked(True)
            else:
                self.decreasingRadio.setChecked(True)
            if self.data[5] > 0:
                self.medianRadio.setChecked(True)
            elif self.data[5] == 0:
                self.averageRadio.setChecked(True)
            else:
                self.noRadio.setChecked(True)
        # mise à jour du bouton OK :
        self.afficheOkButton()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-count')

    def afficheOkButton(self):
        if self.newCount:
            yes = self.nameEdit.currentText() not in self.countNames
        else:
            yes = self.nameEdit.text() not in self.countNames
        self.okButton.setEnabled(yes)

    def nameEditChanged(self, index):
        currentIndex = self.nameEdit.currentIndex()
        countData = self.nameEdit.itemData(currentIndex)
        # on met à jour label, item, sens et calcul :
        self.labelEdit.setText(countData[3])
        itemIndex = self.items.get(countData[4], 0)
        self.itemComboBox.setCurrentIndex(itemIndex)
        if countData[1] > 0:
            self.growingRadio.setChecked(True)
        else:
            self.decreasingRadio.setChecked(True)
        if countData[2] > 0:
            self.medianRadio.setChecked(True)
        elif countData[2] == 0:
            self.averageRadio.setChecked(True)
        else:
            self.noRadio.setChecked(True)

    def itemComboBoxChanged(self, index):
        currentIndex = self.itemComboBox.currentIndex()
        itemData = self.itemComboBox.itemData(currentIndex)

    def accept(self):
        if self.newCount:
            self.data[3] = self.nameEdit.currentText()
        else:
            self.data[3] = self.nameEdit.text()
        self.data[7] = self.labelEdit.text()
        currentIndex = self.itemComboBox.currentIndex()
        itemData = self.itemComboBox.itemData(currentIndex)
        self.data[8] = itemData[0]
        if self.growingRadio.isChecked():
            self.data[4] = 1
        else:
            self.data[4] = 0
        if self.averageRadio.isChecked():
            self.data[5] = 0
        elif self.medianRadio.isChecked():
            self.data[5] = 1
        else:
            self.data[5] = -1
        QtWidgets.QDialog.accept(self)


class CreateMultipleCountsDlg(QtWidgets.QDialog):
    """
    Pour créer plusieurs comptages en une fois.
    On peut sélectionner plusieurs couples (id_groupe, periode)
    et sélectionner les comptages à créer.
    Une liste de comptages à préselectionner peut être donnée en argument
    (pour un import par exemple).
    """
    def __init__(self, parent=None, counts=[]):
        super(CreateMultipleCountsDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Create multiple counts'))

        # sélection des couples (id_groupe, periode) :
        comments = QtWidgets.QApplication.translate(
            'main', 
            'Select couples group+period for which you want to create the counts:')
        commentsLabelTo = QtWidgets.QLabel(comments)
        self.listWidgetTo = QtWidgets.QListWidget()
        self.listWidgetTo.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection)

        # sélection des comptages (double liste) :
        self.counts = counts
        comments = QtWidgets.QApplication.translate(
            'main', 
            'Select the counts you want to create by moving them to the list on the right:')
        commentsLabelCounts = QtWidgets.QLabel(comments)
        self.selectionWidget = utils_2lists.DoubleListWidget(self.main)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # on agence tout ça :
        layout = QtWidgets.QVBoxLayout()
        # groupes+périodes :
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(commentsLabelTo)
        vLayout.addWidget(self.listWidgetTo)
        layout.addLayout(vLayout)
        # comptages :
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(commentsLabelCounts)
        vLayout.addWidget(self.selectionWidget)
        layout.addLayout(vLayout)
        # les boutons :
        layout.addWidget(buttonBox)
        self.setLayout(layout)

        # remplissage de la liste des couples (id_groupe, periode) :
        query_my = utils_db.query(self.main.db_my)
        self.names2datas = {}
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format(
                'groupes', 'ordre, Matiere, UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            id_groupe = int(query_my.value(0))
            groupeName = query_my.value(1)
            matiereName = query_my.value(2)
            for i in range(utils.NB_PERIODES):
                name = utils_functions.u('{0} : \t{1} \t({2})').format(
                    matiereName, groupeName, utils.PERIODES[i])
                self.names2datas[name] = (id_groupe, i, matiereName)
                self.listWidgetTo.addItem(name)
                if (id_groupe, i) == (self.main.id_groupe, utils.selectedPeriod):
                    item = self.listWidgetTo.item(self.listWidgetTo.count() - 1)
                    self.listWidgetTo.setCurrentItem(item)

        # remplissage de la liste des comptages :
        query_my = utils_db.queryExecute(
            utils_db.qs_prof_counts3, query=query_my)
        while query_my.next():
            countName = query_my.value(0)
            sens = int(query_my.value(1))
            calcul = int(query_my.value(2))
            countLabel = query_my.value(3)
            id_item = int(query_my.value(4))
            countData = (countName, sens, calcul, countLabel, id_item)
            countText = utils_functions.u('{0} \t[{1}]').format(
                countName, countLabel)
            countWidget = QtWidgets.QListWidgetItem(countText)
            countWidget.setData(QtCore.Qt.UserRole, countData)
            if countData in self.counts:
                self.selectionWidget.selectionList.addItem(countWidget)
            else:
                self.selectionWidget.baseList.addItem(countWidget)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-count')

    def accept(self):
        # récupération des couples (id_groupe, periode) :
        where = []
        selectedGroupes = self.listWidgetTo.selectedItems()
        for selectedGroupe in selectedGroupes :
            name = selectedGroupe.text()
            (id_groupe, periode, matiereName) = self.names2datas[name]
            where.append((id_groupe, periode))
        # récupération des comptages :
        counts = []
        for i in range(self.selectionWidget.selectionList.count()):
            countWidget = self.selectionWidget.selectionList.item(i)
            countData = countWidget.data(QtCore.Qt.UserRole)
            counts.append(tuple(countData))
        # on y va :
        query_my = utils_db.query(self.main.db_my)
        new_id_count, new_ordre = 0, 0
        for (id_groupe, periode) in where:
            # récupération des comptages existants déjà :
            actualCounts = {}
            commandLine_my = utils_db.qs_prof_counts.format(id_groupe, periode)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_count = int(query_my.value(2))
                countName = query_my.value(3)
                sens = int(query_my.value(4))
                calcul = int(query_my.value(5))
                ordre = int(query_my.value(6))
                countLabel = query_my.value(7)
                id_item = int(query_my.value(8))
                actualCounts[countName] = [id_count, sens, calcul, ordre, countLabel, id_item]
                new_id_count = id_count + 1
                new_ordre = ordre + 1
            # on complète avec les comptages sélectionnés :
            lines = []
            for countData in counts:
                countName = countData[0]
                sens = countData[1]
                calcul = countData[2]
                countLabel = countData[3]
                id_item = countData[4]
                if countName in actualCounts:
                    # si le comptage est déjà là, on le met à jour si besoin :
                    id_count = actualCounts[countName][0]
                    ordre = actualCounts[countName][3]
                    mustDo = False
                    if actualCounts[countName][1] != sens:
                        actualCounts[countName][1] = sens
                        mustDo = True
                    if actualCounts[countName][2] != calcul:
                        actualCounts[countName][2] = calcul
                        mustDo = True
                    if actualCounts[countName][4] != countLabel:
                        actualCounts[countName][4] = countLabel
                        mustDo = True
                    if actualCounts[countName][5] != id_item:
                        actualCounts[countName][5] = id_item
                        mustDo = True
                    if mustDo:
                        # on efface le comptage  :
                        commandLine_my = utils_db.qdf_prof_counts.format(
                            id_groupe, periode, id_count)
                        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                        # et on le réécrira :
                        listArgs = [
                            id_groupe, periode, 
                            id_count, countName, sens, calcul, ordre, countLabel, id_item]
                        lines.append(listArgs)
                else:
                    # on l'inscrira :
                    listArgs = [
                        id_groupe, periode, 
                        new_id_count, countName, sens, calcul, new_ordre, countLabel, id_item]
                    lines.append(listArgs)
                    new_id_count += 1
                    new_ordre += 1
            # on écrit dans la table counts :
            commandLine_my = utils_db.insertInto('counts')
            query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

        QtWidgets.QDialog.accept(self)


def createCount(main, data):
    """
    data : (id_groupe, Periode, id_count, Name, sens, calcul, what, Label, id_item)
    dans la base :  id_groupe, Periode, id_count, Name, sens, calcul, ordre, Label, id_item
    """
    # besoin d'un query :
    query_my = utils_db.query(main.db_my)
    # on met à jour la table counts 
    # (en cas de sélection dans la liste et de modification) :
    query_my = utils_db.queryExecute(
        {utils_db.qup_prof_counts: (
            data[3], data[4], data[5], data[7], data[8], data[3])}, 
        query=query_my)
    # on cherche un id et un ordre :
    new_id_count, new_ordre = 0, 0
    commandLine_my = utils_db.qs_prof_counts.format(data[0], data[1])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        new_id_count = int(query_my.value(2)) + 1
        new_ordre = int(query_my.value(6)) + 1
    # on inscrit le nouveau comptage :
    commandLine_my = utils_db.insertInto('counts')
    listArgs = (
        data[0], data[1], new_id_count, 
        data[3], data[4], data[5], new_ordre, data[7], data[8])
    query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)


def editCount(main, oldName, data):
    """
    oldName : le nom initial du comptage (au cas on on voudrait le changer)
    data : (id_groupe, Periode, id_count, Name, sens, calcul, what, Label, id_item)
    dans la base :  id_groupe, Periode, id_count, Name, sens, calcul, ordre, Label, id_item
    On met à jour la table counts (y compris les autres comptages de même nom).
    """
    query_my = utils_db.query(main.db_my)
    query_my = utils_db.queryExecute(
        {utils_db.qup_prof_counts: (
            data[3], data[4], data[5], data[7], data[8], oldName)}, 
        query=query_my)


def deleteCount(main, data):
    """
    data : (id_groupe, Periode, id_count, Name, sens, calcul, what, Label, id_item)
    ICI COUNTS prévoir de proposer d'effacer le modèle de comptage s'il reste seul ?
    """
    # besoin d'un query :
    query_my = utils_db.query(main.db_my)
    # on efface les valeurs (table counts_values) :
    commandLine_my = utils_db.qdf_prof_countValue2.format(data[0], data[1], data[2])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    # on efface le comptage (table counts) :
    commandLine_my = utils_db.qdf_prof_counts.format(data[0], data[1], data[2])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)


class OrderListDlg(QtWidgets.QDialog):
    """
    Dialog pour modifier l'ordre d'affichage des comptages.
    Marche aussi avec les notes.
    """
    def __init__(self, parent=None, what='counts'):
        super(OrderListDlg, self).__init__(parent)

        # le titre de la fenêtre :
        if what == 'counts':
            title = QtWidgets.QApplication.translate('main', 'OrderCounts')
        elif what == 'notes':
            title = QtWidgets.QApplication.translate('main', 'OrderNotes')
        self.setWindowTitle(title)
        self.main = parent
        self.what = what

        self.listWidget = utils_2lists.DragDropListWidget(num=1)
        self.id_tableau = -1

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.listWidget, 0, 0)
        grid.addWidget(buttonBox, 10, 0, 1, 2)
        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        self.loadList()

    def contextHelp(self):
        # ouvre la page d'aide associée
        if self.what == 'counts':
            utils_functions.openContextHelp('create-count')
        elif self.what == 'notes':
            utils_functions.openContextHelp('create-note')

    def loadList(self):
        query_my = utils_db.query(self.main.db_my)
        # récupération de la sélection :
        data = diversFromSelection(self.main)
        id_groupe, periode = data[0], data[3]
        # remplissage de la liste :
        self.listWidget.clear()
        if self.what == 'counts':
            commandLine_my = utils_db.qs_prof_counts.format(id_groupe, periode)
        elif self.what == 'notes':
            commandLine_my = utils_db.qs_prof_notes.format(id_groupe, periode)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_what = int(query_my.value(2))
            if self.what == 'counts':
                whatName = utils_functions.u('{0}  [{1}]').format(
                    query_my.value(3), query_my.value(7))
            elif self.what == 'notes':
                whatName = query_my.value(3)
            whatWidget = QtWidgets.QListWidgetItem(whatName)
            whatWidget.setData(QtCore.Qt.UserRole, id_what)
            self.listWidget.addItem(whatWidget)

    def saveList(self):
        """
        enregistrement des modifications de la liste.
        """
        query_my = utils_db.query(self.main.db_my)
        # récupération de la sélection :
        data = diversFromSelection(self.main)
        id_groupe, periode = data[0], data[3]
        # on met à jour les ordres
        for i in range(self.listWidget.count()):
            whatWidget = self.listWidget.item(i)
            id_what = whatWidget.data(QtCore.Qt.UserRole)
            if self.what == 'counts':
                commandLine_my = ('UPDATE counts SET ordre={0} '
                                'WHERE id_groupe={1} '
                                'AND Periode={2} '
                                'AND id_count={3}').format(i, id_groupe, periode, id_what)
            elif self.what == 'notes':
                commandLine_my = ('UPDATE notes SET ordre={0} '
                                'WHERE id_groupe={1} '
                                'AND Periode={2} '
                                'AND id_note={3}').format(i, id_groupe, periode, id_what)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)


def recalculeCountsResults(main, countsToUpdate={}, rows=[]):
    """
    Calcule la valeur "couleur" de comptages,
    Le dictionnaire countsToUpdate={id_count: (column, sens, calcul), ...}
        indique les comptages à recalculer.
    La liste rows sert à finaliser la fonction calcCounts.
    """
    # récupération de la sélection :
    data = diversFromSelection(main)
    id_groupe, periode = data[0], data[3]
    elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
    # utilisation de IN pour accélérer les calculs :
    id_students = utils_functions.array2string(elevesInGroup, key=0)
    query_my = utils_db.query(main.db_my)

    if len(countsToUpdate) < 1:
        commandLine_my = utils_db.qs_prof_counts.format(id_groupe, periode)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        column = 3
        while query_my.next():
            id_count = int(query_my.value(2))
            sens = int(query_my.value(4))
            try:
                calcul = int(query_my.value(5))
            except:
                calcul = 0
            countsToUpdate[id_count] = (column, sens, calcul)
            if calcul < 0:
                column += 2
            else:
                column += 4
    for id_count in countsToUpdate:
        (column, sens, calcul) = countsToUpdate[id_count]
        if calcul > -1:
            valuesInDB, numbers, centreReduit = {}, [], {}
            commandLine_my = utils_db.qs_prof_countValue.format(
                id_groupe, periode, id_count, id_students)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(3))
                value = query_my.value(4)
                if value == '':
                    value = 0
                valuesInDB[id_eleve] = value
            for (id_eleve, ordre, eleveName) in elevesInGroup:
                if id_eleve in valuesInDB:
                    value = valuesInDB[id_eleve]
                else:
                    value = 0
                centreReduit[id_eleve] = [value, value, value]
                if value != 'X':
                    numbers.append(int(value))
            if calcul == 0:
                limites = [-1, 0, 1]
                average = utils_calculs.average(numbers)
                standardDeviation = utils_calculs.standardDeviation(numbers, average)
            else:
                limites = utils_calculs.medianAndQuartiles(numbers)
                if utils.ICI:
                    print(numbers)
                    print(limites)
                if limites[2] == False:
                    limites[2] = 1000000
                if limites[0] == False:
                    limites[0] = -1000000
            if sens == 1:
                limitesValues = ('D', 'C', 'B', 'A')
            else:
                limitesValues = ('A', 'B', 'C', 'D')
            row = -1
            for (id_eleve, ordre, eleveName) in elevesInGroup:
                row += 1
                value = centreReduit[id_eleve][0]
                if value != 'X':
                    if calcul == 0:
                        if standardDeviation != 0:
                            result1 = (value - average) / standardDeviation
                        else:
                            result1 = 0
                    else:
                        result1 = value
                    centreReduit[id_eleve][1] = result1
                    if result1 < limites[0]:
                        centreReduit[id_eleve][2] = limitesValues[0]
                    elif result1 > limites[2]:
                        centreReduit[id_eleve][2] = limitesValues[3]
                    elif result1 < limites[1]:
                        centreReduit[id_eleve][2] = limitesValues[1]
                    elif result1 > limites[1]:
                        centreReduit[id_eleve][2] = limitesValues[2]
                    else:
                        if sens == 1:
                            centreReduit[id_eleve][2] = limitesValues[2]
                        else:
                            centreReduit[id_eleve][2] = limitesValues[1]
                result = centreReduit[id_eleve][2]
                color = utils_functions.findColor(result, mustCalculIfMany=False)
                if len(rows) > 0:
                    rows[row][column + 1] = {
                        'value': utils_functions.valeur2affichage(result), 
                        'color': color, 'type': utils.EVAL, 'bold': True}
                else:
                    main.model.rows[row][column + 1] = {
                        'value': utils_functions.valeur2affichage(result), 
                        'color': color, 'type': utils.EVAL, 'bold': True}
                    item = main.model.index(row, column + 1)
                    main.view.update(item)
    if len(rows) > 0:
        return rows


def valuesUpDown(main, step=1):
    """
    Permet d'incrémenter (ou décrémenter) des valeurs entières contenues dans des cellules.
    Utilisé dans les vues VIEW_COUNTS, VIEW_CPT_NUM et VIEW_ABSENCES.
    """
    utils_functions.doWaitCursor()
    main.me['numberEditing'] = False
    main.me['numberFromDelegate'] = False
    try:
        oldState = []
        for item in main.view.selectedIndexes():
            row, column = item.row(), item.column()
            mustDo = False
            if main.viewType == utils.VIEW_COUNTS:
                if (column > 2) and (main.model.columns[column]['other'][6] == 0):
                    mustDo = True
            elif main.viewType == utils.VIEW_ABSENCES:
                if column > 2:
                    mustDo = True
            elif main.viewType == utils.VIEW_CPT_NUM:
                # ICI cptNum
                if (column > 2) and (main.model.columns[column]['datasType'] == utils.NUMBER):
                    mustDo = True
            if mustDo:
                value = main.model.rows[row][column]['value']
                if value in ('X', ''):
                    newValue = step
                else:
                    newValue = int(value) + step
                if newValue < 1:
                    newValue = ''
                doChangeValue(main, row, column, newValue, oldState, valuesUpDown=True)
    finally:
        utils_functions.restoreCursor()
        main.changeDBMyState()
        if len(oldState) > 0:
            main.undoList.append(oldState)
            main.actionUndo.setEnabled(True)


class SelectCountsDlg(QtWidgets.QDialog):
    """
    Un dialog pour sélectionner un ou plusieurs comptages
    """
    def __init__(self, parent=None, counts=[]):
        super(SelectCountsDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'SelectCounts'))

        commentText = QtWidgets.QApplication.translate(
            'main', 'Select the counts you want to export.')
        commentLabel = QtWidgets.QLabel(commentText)

        # liste des comptages :
        countsText = QtWidgets.QApplication.translate('main', '<p><b>Counts:</b></p>')
        countsLabel = QtWidgets.QLabel(countsText)
        self.countsListWidget = QtWidgets.QListWidget()
        self.countsListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(commentLabel,                1, 0, 1, 2)
        grid.addWidget(countsLabel,                 2, 0)
        grid.addWidget(self.countsListWidget,       3, 0, 1, 2)
        grid.addWidget(buttonBox,                   5, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        self.updateCounts()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('create-count')

    def updateCounts(self):
        self.countsListWidget.clear()
        query_my = utils_db.query(self.main.db_my)
        # récupération de la liste des comptages existants :
        commandLine_my = utils_db.qs_prof_counts3
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            countName = query_my.value(0)
            sens = int(query_my.value(1))
            calcul = int(query_my.value(2))
            countLabel = query_my.value(3)
            id_item = int(query_my.value(4))
            data = (countName, sens, calcul, countLabel, id_item)
            label = utils_functions.u('{0} : {1}').format(
                countName, countLabel)
            countsWidget = QtWidgets.QListWidgetItem(label)
            countsWidget.setData(QtCore.Qt.UserRole, data)
            self.countsListWidget.addItem(countsWidget)













































###########################################################"
#        DIVERS
###########################################################"

class ListElevesDlg(utils_2lists.DoubleListDlg):
    """
    explications
    """
    def __init__(self, parent=None, eleves2Freeplane=False, helpMessage=2, helpContextPage=''):
        title = QtWidgets.QApplication.translate('main', 'Students list')
        super(ListElevesDlg, self).__init__(
            parent, 
            eleves2Freeplane=eleves2Freeplane, 
            helpMessage=helpMessage, 
            helpContextPage=helpContextPage, 
            title=title)

        self.main = parent
        self.baseGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'List:'))
        self.selectionGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'Todo:'))

    def remplirListes(self):
        """
        blablabla
        """
        self.baseList.clear()
        self.Avant = []
        self.Apres = []
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())

        # récupération de la sélection :
        data = diversFromSelection(self.main)
        id_groupe, periode = data[0], data[3]
        elevesInGroup = listStudentsInGroup(self.main, id_groupe=id_groupe)
        for eleve in elevesInGroup:
            item = QtWidgets.QListWidgetItem(eleve[2])
            item.setToolTip(utils_functions.u(eleve[0]))
            item.setData(QtCore.Qt.UserRole, int(eleve[0]))
            self.baseList.addItem(item)


def listStudentsInGroup(main, id_tableau=-1, id_groupe=-1, withOld=False, old=0):
    """
    retourne la liste des élèves d'un groupe.
    On récupère d'abord les id_eleve dans la base DB_PROF,
    puis les noms des élèves dans la base DB_USERS.
    old = 0 : on récupère les élèves du groupe (par défaut)
    old = -1 : on récupère aussi les élèves suprimés manuellement du groupe
    old = -2 : on récupère aussi les élèves suprimés de la base users
    """
    eleves = []
    if id_tableau != -1:
        id_groupe = diversFromSelection(main, id_tableau)[0]
    commandLine_my = utils_functions.u(
        'SELECT * FROM groupe_eleve '
        'WHERE id_groupe={0} AND ordre>0 '
        'ORDER BY ordre').format(id_groupe)
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        id_eleve = int(query_my.value(1))
        ordre = int(query_my.value(2))
        eleveName = utils_functions.eleveNameFromId(main, id_eleve)
        eleves.append((id_eleve, ordre, eleveName))
    # on force l'affichage des élèves masqués dans certaines vues
    # pour éviter les oublis des profs
    # ou si demandé (actionShowDeletedEleves).
    # Le try except est pour admin_recup :
    try:
        if main.actionShowDeletedEleves.isChecked():
            old = -2
    except:
        pass
    if old < 0:
        commandLine_my = utils_functions.u(
            'SELECT * FROM groupe_eleve '
            'WHERE id_groupe={0} '
            'AND ordre<=0 AND ordre>{1} '
            'ORDER BY ordre DESC').format(id_groupe, old)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            ordre = int(query_my.value(2))
            eleveName = utils_functions.u('({0})').format(utils_functions.eleveNameFromId(main, id_eleve))
            eleves.append((id_eleve, ordre, eleveName))
    return eleves


def diversFromSelection(main, id_tableau=-1, id_groupe=-1, periode=-1):
    """
    Retourne divers renseignements utiles sur un tableau ou une sélection (groupe + période).
    data = (id_groupe, groupeName, matiereName, periode, 
            tableauName, public, periodesGroupe, periodesToUse, 
            id_template, templateName, 
            tableauLabel, templateLabel)
    periodesGroupe est la liste des périodes pour lesquelles le groupe possède un tableau.
    periodesToUse donne la période à lire et la liste des périodes à calculer :
        periodesToUse = (période à lire, [périodes à calculer])
    """
    public = -1
    groupeName, matiereName, tableauName, tableauLabel = '', '', '', ''
    periodesGroupe, periodesToUse = [], ()
    # si tout est à -1:
    if (id_tableau == -1) and (id_groupe == -1) and (periode == -1):
        id_tableau = main.id_tableau
    # Si c'est le tableau de compilation qui est sélectionné,
    # on utilise le premier tableau de la liste :
    # ICI COUNTS
    if id_tableau == -2:
        id_tableauToUse = main.tableauxInCompil[0]
    else:
        id_tableauToUse = id_tableau

    query_my = utils_db.query(main.db_my)
    if id_tableau != -1:
        commandLine_my = (
            'SELECT DISTINCT tableaux.id_groupe, groupes.Name, groupes.Matiere, '
            'tableaux.Periode, tableaux.Name, tableaux.Public, tableaux.Label, '
            'tableaux2.Periode '
            'FROM tableaux '
            'JOIN groupes ON groupes.id_groupe=tableaux.id_groupe '
            'JOIN tableaux AS tableaux2 ON tableaux2.id_groupe=tableaux.id_groupe '
            'WHERE tableaux.id_tableau={0}').format(id_tableauToUse)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # on passe par query_my.next() car on doit récupérer toutes les périodes :
        first = True
        while query_my.next():
            if first:
                first = False
                id_groupe = int(query_my.value(0))
                groupeName = query_my.value(1)
                matiereName = query_my.value(2)
                periode = int(query_my.value(3))
                tableauName = query_my.value(4)
                public = int(query_my.value(5))
                tableauLabel = query_my.value(6)
            periodesGroupe.append(int(query_my.value(7)))
        # Pour un tableau de compilation, on corrige le nom :
        if id_tableau == -2:
            tableauName = QtWidgets.QApplication.translate('main', 'tablesCompilation')
    else:
        if id_groupe == -1:
            id_groupe = main.id_groupe
        matiereName = ''
        if periode == -1:
            periode = utils.selectedPeriod
        tableaux = prof_groupes.tableauxFromGroupe(main, id_groupe, periode)
        if len(tableaux) > 0:
            commandLine_my = (
                'SELECT DISTINCT groupes.Name, groupes.Matiere, tableaux.Periode '
                'FROM groupes '
                'JOIN tableaux ON tableaux.id_groupe=groupes.id_groupe '
                'WHERE groupes.id_groupe={0}').format(id_groupe)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            # on passe par query_my.next() car on doit récupérer toutes les périodes :
            first = True
            while query_my.next():
                if first:
                    first = False
                    groupeName = query_my.value(0)
                    matiereName = query_my.value(1)
                periodesGroupe.append(int(query_my.value(2)))
        else:
            commandLine_my = (
                'SELECT Name, Matiere FROM groupes '
                'WHERE id_groupe={0}').format(id_groupe)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                groupeName = query_my.value(0)
                matiereName = query_my.value(1)
    # ICI COUNTS
    # en cas de comptages sans tableaux (PP par exemple) :
    counts = countsFromGroup(main, id_groupe)
    if periode in counts:
        if len(counts[periode]['ITEMS']) > 0:
            periodesGroupe.append(periode)
    # on efface les doublons :
    periodesGroupe = list(set(periodesGroupe))
    # recherche des périodes à lire et à calculer :
    if periode > 0:
        if periode in periodesGroupe:
            periodesToUse = (periode, [periode])
        else:
            periodesToUse = (0, [0])
    elif periode == 0:
        periodesToUse = (0, periodesGroupe)
    # recherche d'un modèle lié au tableau :
    id_template = 0
    templateName = QtWidgets.QApplication.translate('main', 'no template')
    templateLabel = ''
    if id_tableau > -1:
        commandLine_my = 'SELECT * FROM tableau_item WHERE id_tableau={0} AND id_item<0'.format(
            id_tableau)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_template = int(query_my.value(1))
    if id_template < 0:
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'templates', 'id_template', id_template)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            templateName = query_my.value(1)
            templateLabel = query_my.value(3)
    # on a tout récupéré :
    data = (
        id_groupe, groupeName, matiereName, periode, 
        tableauName, public, periodesGroupe, periodesToUse, 
        id_template, templateName, 
        tableauLabel, templateLabel)
    return data


def countsFromGroup(main, id_groupe, id_eleve=-1, period=-1):
    """
    récupère les comptages liés à des items des élèves du groupe.
    Si id_eleve et period sont donnés en arguments, 
    on ne retourne les résultats que d'un élève pour une période (plus année)
    pour la vue "Élève".
    """
    query_my = utils_db.query(main.db_my)
    counts = {}
    if id_eleve < 0:
        # on cherche tous les élèves et pour toutes les périodes :
        for periode in range(utils.NB_PERIODES):
            counts[periode] = {'ITEMS': [], 'VALUES': {}}
        commandLine_my = (
            'SELECT DISTINCT counts_values.Periode, counts_values.id_eleve, '
            'counts.id_item, counts_values.perso '
            'FROM counts_values '
            'JOIN counts '
            'ON counts.id_count=counts_values.id_count '
            'AND counts.id_groupe=counts_values.id_groupe '
            'AND counts.Periode=counts_values.Periode '
            'WHERE counts.id_item>-1 '
            'AND counts_values.perso!="" '
            'AND counts_values.id_groupe={0}').format(id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            count_period = int(query_my.value(0))
            count_eleve = int(query_my.value(1))
            count_item = int(query_my.value(2))
            count_value = query_my.value(3)
            utils_functions.appendUnique(counts[count_period]['ITEMS'], count_item)
            if not((count_eleve, count_item) in counts[count_period]['VALUES']):
                counts[count_period]['VALUES'][(count_eleve, count_item)] = count_value
            else:
                counts[count_period]['VALUES'][(count_eleve, count_item)] += count_value
    else:
        # un élève et une période sont sélectionnés :
        counts = {'ITEMS': [], 'VALUES': {}}
        commandLine_my = (
            'SELECT DISTINCT counts_values.Periode, counts_values.id_eleve, '
            'counts.id_item, counts_values.perso '
            'FROM counts_values '
            'JOIN counts '
            'ON counts.id_count=counts_values.id_count '
            'AND counts.id_groupe=counts_values.id_groupe '
            'AND counts.Periode=counts_values.Periode '
            'WHERE counts.id_item>-1 '
            'AND counts_values.perso!="" '
            'AND counts_values.id_groupe={0} '
            'AND counts_values.id_eleve={1} '
            'AND counts_values.Periode IN (0, {2})').format(id_groupe, id_eleve, period)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            count_item = int(query_my.value(2))
            count_value = query_my.value(3)
            utils_functions.appendUnique(counts['ITEMS'], count_item)
            if not(count_item in counts['VALUES']):
                counts['VALUES'][count_item] = count_value
            else:
                counts['VALUES'][count_item] += count_value
    return counts



































































###########################################################"
#        ÉVALUATIONS
###########################################################"

def changeAppreciation(main, id_eleve, value, onlyIfMust=True):
    if onlyIfMust:
        if not(main.me.get('mustSaveActualAppreciation', False)):
            return
        main.me['mustSaveActualAppreciation'] = False
    query_my = utils_db.query(main.db_my)
    oldState = []
    if main.viewType == utils.VIEW_ACCOMPAGNEMENT:
        # on récupère la valeur précédente :
        oldValue = ''
        commandLine_my = (
            'SELECT * FROM lsu '
            'WHERE lsuWhat="accompagnement" '
            'AND lsu1 IN ({0}) '
            'AND lsu2 = "PPRE"').format(id_eleve)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            oldValue = query_my.value(3)
        # on l'efface :
        commandLine_my = (
            'DELETE FROM lsu '
            'WHERE lsuWhat="accompagnement" '
            'AND lsu1 IN ({0}) '
            'AND lsu2 = "PPRE"').format(id_eleve)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # et on inscrit la nouvelle :
        commandLine_my = utils_db.insertInto('lsu')
        if value != '':
            query_my = utils_db.queryExecute(
                {commandLine_my: (
                    'accompagnement', id_eleve, 'PPRE', value, '', '', '', '', '')}, 
                query=query_my)
        elif oldValue != '':
            query_my = utils_db.queryExecute(
                {commandLine_my: (
                    'accompagnement', id_eleve, 'PPRE', '', '', '', '', '', '')}, 
                query=query_my)
    elif main.viewType == utils.VIEW_CPT_NUM:
        # ICI cptNum
        # on efface la valeur précédente :
        commandLine_my = (
            'DELETE FROM lsu '
            'WHERE lsuWhat="cpt-num" '
            'AND lsu1 IN ({0}) '
            'AND lsu2 = "appreciation"').format(id_eleve)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # et on inscrit la nouvelle :
        commandLine_my = utils_db.insertInto('lsu')
        if value != '':
            query_my = utils_db.queryExecute(
                {commandLine_my: (
                    'cpt-num', id_eleve, 'appreciation', value, '', '', '', '', '')}, 
                query=query_my)
    elif main.viewType == utils.VIEW_DNB:
        # on récupère l'appréciation précédente :
        oldValue = ''
        commandLine_my = utils_db.qs_prof_dnbValues2.format(id_eleve, 3)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            oldValue = query_my.value(2)
        # on l'efface :
        commandLine_my = utils_db.qdf_prof_dnbValues.format(id_eleve, 3)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # et on inscrit la nouvelle :
        commandLine_my = utils_db.insertInto('dnb_values')
        if value != '':
            query_my = utils_db.queryExecute(
                {commandLine_my: (id_eleve, 3, value)}, 
                query=query_my)
        for item in main.view.selectedIndexes():
            row, column = item.row(), item.column()
            oldState.append((item, id_eleve, 3, oldValue))
    else:
        matiereName = prof_groupes.matiereFromGroupe(main, main.id_groupe)
        periode = utils.selectedPeriod
        # on récupère l'appréciation précédente :
        oldValue = ''
        commandLine_my = utils_db.qs_prof_appreciations.format(
            id_eleve, matiereName, periode)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            oldValue = query_my.value(1)
        # on l'efface :
        commandLine_my = utils_db.qdf_prof_appreciations2.format(id_eleve, matiereName, periode)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # et on inscrit la nouvelle :
        commandLine_my = utils_db.insertInto('appreciations')
        if value != '':
            query_my = utils_db.queryExecute(
                {commandLine_my: (id_eleve, value, matiereName, periode)}, 
                query=query_my)
        for item in main.view.selectedIndexes():
            row, column = item.row(), item.column()
            oldState.append((item, id_eleve, matiereName, periode, oldValue))
    main.changeDBMyState()


def testLastNumberValidated(main):
    """
    Avant de qutter la vue notes,
    on vérifie si la dernière saisie est validée
    """
    if (main.viewType in utils.VIEWS_WITH_EVALUATIONS) \
        and (main.me.get('numberEditing', False)):
            changeValue(main, 'doValidate')


def changeValue(main, newValue, doAdd=False, doValidate=True):
    """
    blablabla
    """
    if main.actionLock.isChecked():
        m1 = QtWidgets.QApplication.translate('main', 'ENTERING LOCKED!')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center"><b>{1}</b></p>'
            '<p></p>').format(utils.SEPARATOR_LINE, m1)
        utils_functions.messageBox(main, level='warning', message=message)
        return
    if not(main.view.selectionState['hasEditables']):
        return

    utils_functions.doWaitCursor()
    # si on efface, doAdd n'a pas de sens :
    if newValue == '':
        doAdd = False
    try:
        theValue = newValue
        oldState = []
        goNext = (main.autoselect > 0) \
            and (len(main.view.selectedIndexes()) == 1) and (doValidate)

        if len(main.view.selectionState['evals']) > 0:
            if (newValue in utils.itemsValues) or (newValue == ''):
                for item in main.view.selectionState['evals']:
                    row, column = item.row(), item.column()
                    if doAdd:
                        theValue = utils_functions.affichage2valeur(item.data()) + newValue
                    doChangeValue(main, row, column, theValue, oldState, doAdd=doAdd)

        if len(main.view.selectionState['texts']) > 0:
            if newValue == '':
                # effacer les appréciations sélectionnées :
                deleteSelectedAppreciations(main)

        if len(main.view.selectionState['notes']) > 0:
            if newValue in ('x', 'X'):
                newValue = 'X'
                theValue = newValue
            mustAdd = False
            if doValidate:
                main.me['numberEditing'] = False
            else:
                if main.me.get('numberEditing', False):
                    mustAdd = True
                else:
                    main.me['numberEditing'] = True
            for item in main.view.selectionState['notes']:
                row, column = item.row(), item.column()
                noteValide = True
                if mustAdd:
                    theValue = '{0}{1}'.format(item.data(), newValue)
                test = doChangeValue(
                    main, row, column, theValue, oldState, doValidate=doValidate)
                noteValide = noteValide and test
            # mise à jour de la variable goNext :
            if main.me.get('numberFromDelegate', False):
                if newValue == 'doValidate':
                    goNext = False
                else:
                    main.me['numberFromDelegate'] = False
            else:
                if not(newValue in ('doValidate', 'X')):
                    goNext = False
            if not(noteValide):
                goNext = False

        if goNext:
            goNext = False
            row = main.view.selectedIndexes()[0].row()
            column = main.view.selectedIndexes()[0].column()
            isNote = (main.model.rows[row][column]['type'] == utils.NOTE_PERSO)
            while not(goNext):
                if main.autoselect == 1:
                    if row < main.model.rowCount() - 1:
                        row += 1
                    elif column < main.model.columnCount() - 1:
                        row = 0
                        column += 1
                    else:
                        row = 0
                        column = 3
                else:
                    if column < main.model.columnCount() - 1:
                        column += 1
                    elif row < main.model.rowCount() - 1:
                        row += 1
                        column = 3
                    else:
                        row = 0
                        column = 3
                index = main.model.index(row, column)
                main.view.setCurrentIndex(index)
                if isNote:
                    if main.model.rows[row][column]['type'] == utils.NOTE_PERSO:
                        goNext = True
                else:
                    if main.model.rows[row][column]['type'] == utils.EVAL:
                        goNext = True
    finally:
        utils_functions.restoreCursor()
        main.changeDBMyState()
        if len(oldState) > 0:
            main.undoList.append(oldState)
            main.actionUndo.setEnabled(True)


def copy(main):
    """
    Gestion du presse-papier : copier.
    Gère la différence entre des valeurs, des notes et des appréciations.
    """
    utils_functions.doWaitCursor()
    isAppreciations = False
    if main.viewType in utils.VIEWS_WITH_APPRECIATIONS:
        isAppreciations = True
    # valeurs bidons pour récupérer les limites de la sélection :
    rows = [999999, 0]
    cols = [999999, 0]
    # les données et le texte à copier :
    copyData, copyText = {}, ''
    try:
        # récupération des données et des limites de la sélection :
        for item in main.view.selectedIndexes():
            row, column = item.row(), item.column()
            data = item.data()
            # mise en forme selon le type de donnée :
            if item in main.view.selectionState['numbers']:
                # cas des décimaux :
                if isinstance(data, float):
                    data = '{0}'.format(data)
                    data = data.replace('.', utils.DECIMAL_SEPARATOR)
            elif item in main.view.selectionState['texts']:
                # pour les appréciations, si elles sont sur plusieurs lignes,
                # on les entoure de " (compatibilité avec LibreOffice) :
                if '\n' in data:
                    data = '"' + data + '"'
            elif main.model.rows[row][column].get('type', '') == utils.TEXT:
                # cas d'une appréciation non éditable (ancien trimestre) :
                if '\n' in data:
                    data = '"' + data + '"'
            copyData[(row, column)] = data
            # on met à jour les limites de la sélection si besoin :
            if row < rows[0]:
                rows[0] = row
            if row > rows[1]:
                rows[1] = row
            if column < cols[0]:
                cols[0] = column
            if column > cols[1]:
                cols[1] = column
        # mise en forme du texte à copier :
        for row in range(rows[1] - rows[0] + 1):
            if row != 0:
                copyText += '\n'
            for col in range(cols[1] - cols[0] + 1):
                if col != 0:
                    copyText += '\t'
                try:
                    toAdd = copyData[(rows[0] + row, cols[0] + col)]
                    copyText = utils_functions.u('{0}{1}').format(copyText, toAdd)
                except:
                    pass
        copyText += '\n'
        # envoi au presse-papier :
        clipboard = QtWidgets.QApplication.clipboard()
        clipboard.setText(copyText)
    finally:
        utils_functions.restoreCursor()


def paste(main):
    """
    Gestion du presse-papier : coller.
    Gère la différence entre des valeurs et des appréciations.
    """
    if main.actionLock.isChecked():
        m1 = QtWidgets.QApplication.translate('main', 'ENTERING LOCKED!')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center"><b>{1}</b></p>'
            '<p></p>').format(utils.SEPARATOR_LINE, m1)
        utils_functions.messageBox(main, level='warning', message=message)
        return
    utils_functions.doWaitCursor()
    try:
        OK = False
        # où doit-on coller :
        rowInitial = main.view.currentIndex().row()
        colInitial = main.view.currentIndex().column()
        for item in main.view.selectedIndexes():
            row, column = item.row(), item.column()
            if row < rowInitial:
                rowInitial = row
            if column < colInitial:
                colInitial = column
        # récupération du presse-papier :
        clipboard = QtWidgets.QApplication.clipboard()
        # s'il y a des textes, ils peuvent contenir des sauts de lignes.
        # on les remplace provisoirement par des @@@@@ :
        clipboardText = ''
        clipboardData = clipboard.text().split('"')
        if len(clipboardData) > 1:
            impair = False
            for part in clipboardData:
                if impair:
                    part = part.replace('\n', '@@@@@')
                impair = not(impair)
                clipboardText += part
        else:
            clipboardText = clipboard.text()

        clipboardData = []
        for rowDatas in clipboardText.split('\n'):
            # on peut remettre les sauts de lignes dans les textes :
            rowDatas2 = rowDatas.replace('@@@@@', '\n')
            clipboardData.append(rowDatas2.split('\t'))
        # il peut y avoir une ligne inutile (un dernier \n) :
        if len(clipboardData[-1]) < len(clipboardData[0]):
            clipboardData.pop()
        elif clipboardData[-1] == ['']:
            clipboardData.pop()
        # état de la touche shift (ajout ou remplacement) :
        doAdd = main.doAdd
        if main.actionShift.isChecked():
            doAdd = not(doAdd)
        # pour ajouter au undo :
        oldState = []
        # on récupère la liste des noms d'horaires valides :
        horaires = []
        if (main.viewType == utils.VIEW_SUIVIS):
            query_commun = utils_db.query(main.db_commun)
            # récupération des horaires (table commun.horaires) :
            commandLine_commun = utils_db.q_selectAllFromOrder.format('horaires', 'id')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                name = query_commun.value(1)
                horaires.append(name)
        # et celle des lettres autorisées pour les évaluations :
        evals = ['', ]
        for affichage in utils.affichages:
            evals.append(utils.affichages[affichage][0])
        # on y va :
        for r in range(len(clipboardData)):
            for c in range(len(clipboardData[r])):
                # le try-except permet de coller 
                # même si la sélection sort des limites
                try:
                    row, column = rowInitial + r, colInitial + c
                    theValue = clipboardData[r][c]
                    itemInModel = main.model.rows[row][column]
                    if itemInModel['flag'] != 'EDITABLE':
                        continue
                    valueType = itemInModel['type']
                    if valueType == utils.EVAL:
                        # on commence par vérifier les données :
                        valide = True
                        for char in theValue:
                            if not(char in evals):
                                valide = False
                                break
                        if not(valide):
                            continue
                        # on colle :
                        theValue = utils_functions.affichage2valeur(theValue)
                        if doAdd:
                            valueEx = utils_functions.affichage2valeur(
                                itemInModel['value'])
                            theValue = valueEx + theValue
                        doChangeValue(
                            main, row, column, theValue, oldState, 
                            doAdd=doAdd, fromClipboard=True)
                    elif valueType == utils.NOTE_PERSO:
                        # on commence par vérifier les données :
                        valide = True
                        theValue = theValue.replace(utils.DECIMAL_SEPARATOR, '.')
                        if not(theValue in ('', 'X')):
                            if ('.' in theValue):
                                try:
                                    value = float(theValue)
                                except:
                                    valide = False
                            else:
                                try:
                                    value = int(theValue)
                                except:
                                    valide = False
                        if not(valide):
                            continue
                        # on colle :
                        doChangeValue(
                            main, row, column, theValue, oldState, 
                            doAdd=doAdd, fromClipboard=True)
                    elif valueType == utils.TEXT:
                        if (main.viewType == utils.VIEW_SUIVIS):
                            # on colle :
                            main.model.rows[row][column]['value'] = theValue
                            item = main.model.index(row, column)
                            main.view.update(item)
                        elif not('appreciationColumn' in main.model.columns[column]):
                            # si on n'a pas sélectionné la bonne colonne, on sort :
                            continue
                        else:
                            # on colle :
                            id_eleve = main.model.rows[row][0]['value']
                            changeAppreciation(main, id_eleve, theValue, onlyIfMust=False)
                            main.model.rows[row][column]['value'] = theValue
                            item = main.model.index(row, column)
                            main.view.update(item)
                            """
                            main.view.resizeRowsToContents()
                            if main.viewType == utils.VIEW_BULLETIN:
                                minHeight = 4 * main.fontSizeSlider.value()
                            elif main.viewType == utils.VIEW_APPRECIATIONS:
                                minHeight = 5 * main.fontSizeSlider.value()
                            for i in range(main.model.rowCount()):
                                if main.view.rowHeight(i) < minHeight:
                                    main.view.setRowHeight(i, minHeight)
                            """
                    elif valueType == utils.DATE:
                        # on commence par vérifier les données :
                        date = QtCore.QDate().fromString(theValue, 'dd-MM-yyyy')
                        if not(date.isValid()):
                            continue
                        # on colle :
                        main.model.rows[row][column]['value'] = theValue
                        item = main.model.index(row, column)
                        main.view.update(item)
                    elif valueType == utils.HORAIRE:
                        # on commence par vérifier les données :
                        if not(theValue in horaires):
                            continue
                        # on colle :
                        main.model.rows[row][column]['value'] = theValue
                        item = main.model.index(row, column)
                        main.view.update(item)
                except:
                    continue
        OK = True
    finally:
        utils_functions.restoreCursor()
        if OK:
            main.changeDBMyState()
            if len(oldState) > 0:
                main.undoList.append(oldState)
                main.actionUndo.setEnabled(True)


def deleteSelectedAppreciations(main):
    """
    Efface les appréciations contenues dans la sélection (touche suppr).
    """
    if main.actionLock.isChecked():
        m1 = QtWidgets.QApplication.translate('main', 'ENTERING LOCKED!')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center"><b>{1}</b></p>'
            '<p></p>').format(utils.SEPARATOR_LINE, m1)
        utils_functions.messageBox(main, level='warning', message=message)
        return
    utils_functions.doWaitCursor()
    OK = False
    try:
        if main.viewType != utils.VIEW_SUIVIS:
            # besoin d'un query :
            query_my = utils_db.query(main.db_my)
        for item in main.view.selectionState['texts']:
            row, column = item.row(), item.column()
            if main.viewType == utils.VIEW_ACCOMPAGNEMENT:
                id_eleve = main.model.rows[row][0]['value']
                # on récupère la valeur précédente :
                oldValue = ''
                commandLine_my = (
                    'SELECT * FROM lsu '
                    'WHERE lsuWhat="accompagnement" '
                    'AND lsu1 IN ({0}) '
                    'AND lsu2 = "PPRE"').format(id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    oldValue = query_my.value(2)
                # on l'efface :
                commandLine_my = (
                    'DELETE FROM lsu '
                    'WHERE lsuWhat="accompagnement" '
                    'AND lsu1 IN ({0}) '
                    'AND lsu2 = "PPRE"').format(id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                # et on inscrit si besoin :
                commandLine_my = utils_db.insertInto('lsu')
                if oldValue != '':
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (
                            'accompagnement', id_eleve, 'PPRE', '', '', '', '', '', '')}, 
                        query=query_my)
            elif main.viewType == utils.VIEW_CPT_NUM:
                # ICI cptNum
                id_eleve = main.model.rows[row][0]['value']
                commandLine_my = (
                    'DELETE FROM lsu '
                    'WHERE lsuWhat="cpt-num" '
                    'AND lsu1 IN ({0}) '
                    'AND lsu2 = "appreciation"').format(id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            elif main.viewType == utils.VIEW_DNB:
                id_eleve = main.model.rows[row][0]['value']
                commandLine_my = utils_db.qdf_prof_dnbValues.format(id_eleve, 3)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            elif main.viewType != utils.VIEW_SUIVIS:
                if not('appreciationColumn' in main.model.columns[column]):
                    continue
                id_eleve = main.model.rows[row][0]['value']
                matiereName = prof_groupes.matiereFromGroupe(main, main.id_groupe)
                periode = utils.selectedPeriod
                commandLine_my = utils_db.qdf_prof_appreciations2.format(
                    id_eleve, matiereName, periode)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            main.model.rows[row][column]['value'] = ''
            main.view.update(item)
        OK = True
    finally:
        utils_functions.restoreCursor()
        if OK:
            main.changeDBMyState()


def doChangeValue(main, row, column, newValue, oldState,
                  doAdd=False, doValidate=True, fromClipboard=False, valuesUpDown=False):
    """
    Change la valeur d'un item, d'une note ou d'une compétence suivie
    et récupère l'état précédent dans la liste oldState (pour undo ou redo).
    On ne se sert pas de selectionState car lors d'un coller l'item à modifier 
    n'est pas forcément sélectionné.
    """
    if main.model.rows[row][column].get('flag', '') != 'EDITABLE':
        return False
    item = main.model.index(row, column)
    oldValue = main.model.rows[row][column].get('value', '')
    if main.model.rows[row][column].get('type', '') == utils.EVAL:
        oldValue = utils_functions.affichage2valeur(oldValue)
    id_eleve = main.model.rows[row][0].get('value', -1)
    id_truc = main.model.columns[column].get('id', -1)

    if main.viewType == utils.VIEW_ITEMS:
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        id_item = id_truc
        # pour que les bilans soient recalculés :
        utils_functions.appendUnique(main.whatIsModified['id_eleves'], id_eleve)
        utils_functions.appendUnique(main.whatIsModified['id_items'], id_item)
        # on modifie la base :
        if main.id_tableau != -2:
            # si ce n'est pas un tableau de compilation,
            # on a juste à modifier la valeur :
            commandLine_my = utils_db.qdf_prof_evaluations5.format(main.id_tableau, 
                id_eleve, id_item)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            commandLine_my = utils_db.insertInto('evaluations')
            if newValue != '':
                query_my = utils_db.queryExecute(
                    {commandLine_my: (
                        main.id_tableau, id_eleve, id_item, -1, newValue)}, 
                    query=query_my)
            oldState.append((item, id_eleve, id_item, oldValue))
        else:
            # ICI COUNTS
            # pour un tableau de compilation, ça se complique.
            # une liste des anciennes valeurs en cas d'annulation :
            oldValues = []
            if doAdd:
                # on ne modifie que le premier tableau de la liste :
                id_tableau = main.tableauxForItem[id_item][0]
                # on récupère son ancienne valeur :
                oldValueInFirstTableau = ''
                commandLine_my = utils_db.qs_prof_ValueItem.format(
                    id_tableau, id_eleve, id_item)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                if query_my.first():
                    oldValueInFirstTableau = query_my.value(0)
                oldValues.append((id_tableau, oldValueInFirstTableau))
                # on efface :
                commandLine_my = utils_db.qdf_prof_evaluations5.format(
                    id_tableau, id_eleve, id_item)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                # on inscrit la nouvelle valeur :
                valueNewInLastTableau = oldValueInFirstTableau + newValue[-1]
                commandLine_my = utils_db.insertInto('evaluations')
                if newValue != '':
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (
                            id_tableau, id_eleve, id_item, -1, valueNewInLastTableau)}, 
                        query=query_my)
            else:
                # on efface les valeurs dans tous les tableaux de la liste,
                # et on inscrit dans le premier :
                for id_tableau in main.tableauxForItem[id_item]:
                    # on récupère les anciennes valeurs pour la liste oldValues :
                    oldValueInTableau = ''
                    commandLine_my = utils_db.qs_prof_ValueItem.format(
                        id_tableau, id_eleve, id_item)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    if query_my.first():
                        oldValueInTableau = query_my.value(0)
                    oldValues.append((id_tableau, oldValueInTableau))
                    # on efface :
                    commandLine_my = utils_db.qdf_prof_evaluations5.format(
                        id_tableau, id_eleve, id_item)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                # on inscrit dans le premier :
                id_tableau = main.tableauxForItem[id_item][0]
                commandLine_my = utils_db.insertInto('evaluations')
                if newValue != '':
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (
                            id_tableau, id_eleve, id_item, -1, newValue)}, 
                        query=query_my)
            oldState.append((item, id_eleve, id_item, oldValue, oldValues))
        # et on modifie l'affichage :
        color = utils_functions.findColor(newValue)
        main.model.rows[row][column] = {
            'value': utils_functions.valeur2affichage(newValue), 
            'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
        main.view.update(item)
    elif main.viewType == utils.VIEW_NOTES:
        if main.model.rows[row][column]['type'] == utils.NOTE_CALCUL:
            return True
        if doValidate:
            # besoin d'un query :
            query_my = utils_db.query(main.db_my)
            id_note = id_truc
            id_groupe = main.id_groupe
            periode = utils.selectedPeriod
            mustRecupOldValue = not(fromClipboard) and (newValue != '')
            if not(main.me.get('numberFromDelegate', False)):
                mustRecupOldValue = mustRecupOldValue and (newValue != 'X')
            if mustRecupOldValue:
                newValue = oldValue
                # on récupère oldValue dans la base :
                oldValue = ''
                commandLine_my = utils_db.qs_prof_noteValue.format(
                    id_groupe, periode, id_note, id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    oldValue = query_my.value(4)
                    if oldValue != 'X':
                        oldValue = float(oldValue)
            if not(newValue in ('', 'X')):
                try:
                    newValue = '{0}'.format(newValue)
                    newValue = newValue.replace(utils.DECIMAL_SEPARATOR, '.')
                    newValue = float(newValue)
                except:
                    message = QtWidgets.QApplication.translate(
                        'main', '<b>{0}</b><br/> is not a valid number!')
                    utils_functions.messageBox(
                        main, level='critical', 
                        message=utils_functions.u(message).format(newValue))
                    main.model.rows[row][column] = {
                        'value':oldValue, 
                        'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
                    main.view.update(item)
                    return False
                base = float(main.model.columns[column]['other'][4])
                if newValue > base:
                    message = QtWidgets.QApplication.translate(
                        'main', '<b>{0}/{1}</b><br/> is not a valid rating!')
                    utils_functions.messageBox(
                        main, 
                        level='critical', 
                        message=utils_functions.u(message).format(newValue, base))
                    main.model.rows[row][column] = {
                        'value':oldValue, 
                        'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
                    main.view.update(item)
                    return False
            if newValue != oldValue:
                # on modifie la base :
                commandLine_my = utils_db.qdf_prof_noteValue.format(id_groupe, 
                    periode, id_note, id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                commandLine_my = utils_db.insertInto('notes_values')
                if newValue != '':
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (
                            id_groupe, periode, id_note, id_eleve, newValue)}, 
                        query=query_my)
                oldState.append((item, id_eleve, id_note, oldValue))
                # on recalcule la moyenne :
                recalculeMoyennes(
                    main, main.model.columns[column]['other'], id_eleve, row, column)
        # et on modifie l'affichage :
        if newValue == 'X':
            main.model.rows[row][column] = {
                'value': newValue, 'color': utils.colorLightGray, 
                'type': utils.NOTE_PERSO, 'bold': True, 'flag': 'EDITABLE'}
        else:
            main.model.rows[row][column] = {
                'value': newValue, 
                'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
        main.view.update(item)
    elif main.viewType == utils.VIEW_SUIVIS:
        # vue suivis :
        id_suivi = id_truc
        oldState.append((item, id_eleve, id_suivi, oldValue))
        # et on modifie l'affichage :
        color = utils_functions.findColor(newValue)
        main.model.rows[row][column] = {
            'value': utils_functions.valeur2affichage(newValue), 
            'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
        main.view.update(item)
        main.suivisIsModified = True
    elif main.viewType == utils.VIEW_ABSENCES:
        if doValidate:
            # besoin d'un query :
            query_my = utils_db.query(main.db_my)
            id_absence = id_truc
            periode = utils.selectedPeriod
            mustRecupOldValue = not(fromClipboard) and (newValue != '') and not(valuesUpDown)
            # on récupère absences et retards dans la base :
            absences = ['', '', '', '']
            commandLine_my = utils_db.qs_prof_absencesPeriodeEleves.format(periode, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                for i in range(4):
                    absences[i] = query_my.value(3 + i)
                    if absences[i] != '':
                        absences[i] = int(absences[i])
                    if absences[i] == 0:
                        absences[i] = ''
            if mustRecupOldValue:
                newValue = oldValue
                oldValue = ''
                oldValue = absences[id_absence]
            if newValue != '':
                try:
                    newValue = int(newValue)
                except:
                    message = QtWidgets.QApplication.translate(
                        'main', '<b>{0}</b><br/> is not a valid number!')
                    utils_functions.messageBox(
                        main, level='critical', 
                        message=utils_functions.u(message).format(newValue))
                    main.model.rows[row][column] = {
                        'value':oldValue, 
                        'type': utils.NOTE_PERSO, 'bold': True, 'flag': 'EDITABLE'}
                    main.view.update(item)
                    return False
            if newValue == 0:
                newValue = ''
            if newValue != oldValue:
                absences[id_absence] = newValue
                # on modifie la base :
                commandLine_my = utils_db.qdf_prof_absencesPeriodeEleves.format(
                    periode, id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                commandLine_my = utils_db.insertInto('absences')
                test = ''
                for i in range(4):
                    test = '{0}{1}'.format(test, absences[i])
                if test != '':
                    for i in range(4):
                        if absences[i] == '':
                            absences[i] = 0
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (
                            -1, periode, id_eleve, 
                            absences[0], absences[1], absences[2], absences[3])}, 
                        query=query_my)
                oldState.append((item, id_eleve, id_absence, oldValue))
        main.model.rows[row][column] = {
            'value': newValue, 'type': utils.NOTE_PERSO, 'bold': True, 'flag': 'EDITABLE'}
        main.view.update(item)
    elif main.viewType == utils.VIEW_COUNTS:
        id_count = id_truc
        id_groupe = main.id_groupe
        periode = utils.selectedPeriod
        if main.model.rows[row][column].get('type', '') == utils.EVAL:
            # pour que les bilans soient recalculés :
            id_item = main.model.columns[column]['other'][8]
            if id_item > -1:
                utils_functions.appendUnique(main.whatIsModified['id_eleves'], id_eleve)
                utils_functions.appendUnique(main.whatIsModified['id_items'], id_item)
                main.whatIsModified['actualTableau'] = True
            # on modifie la base :
            query_my = utils_db.query(main.db_my)
            countValue = main.model.rows[row][column - 2]['value']
            commandLine_my = utils_db.qdf_prof_countValue.format(
                id_groupe, periode, id_count, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            commandLine_my = utils_db.insertInto('counts_values')
            if (newValue != '') or (countValue != ''):
                query_my = utils_db.queryExecute(
                    {commandLine_my: (
                        id_groupe, periode, id_count, id_eleve, countValue, newValue)}, 
                    query=query_my)
            oldState.append((item, id_eleve, id_count, countValue, oldValue))
            # et on modifie l'affichage :
            color = utils_functions.findColor(newValue)
            main.model.rows[row][column] = {
                'value': utils_functions.valeur2affichage(newValue), 
                'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
        else:
            if doValidate:
                # besoin d'un query :
                query_my = utils_db.query(main.db_my)
                mustRecupOldValue = not(fromClipboard) \
                    and not(newValue in ('', 'X')) and not(valuesUpDown)
                if mustRecupOldValue:
                    newValue = oldValue
                    # on récupère oldValue dans la base :
                    oldValue = ''
                    commandLine_my = utils_db.qs_prof_countValue.format(
                        id_groupe, periode, id_count, id_eleve)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    while query_my.next():
                        oldValue = query_my.value(4)
                        if oldValue == '':
                            oldValue = 0
                        elif oldValue != 'X':
                            oldValue = int(oldValue)
                if not(newValue in ('', 'X')):
                    try:
                        newValue = '{0}'.format(newValue)
                        newValue = int(newValue)
                    except:
                        message = QtWidgets.QApplication.translate(
                            'main', '<b>{0}</b><br/> is not a valid number!')
                        utils_functions.messageBox(
                            main, level='critical', 
                            message=utils_functions.u(message).format(newValue))
                        main.model.rows[row][column] = {
                            'value':oldValue, 'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
                        main.view.update(item)
                        return False
                if newValue != oldValue:
                    # on modifie la base :
                    calcul = main.model.columns[column]['other'][5]
                    if calcul < 0:
                        persoValue = ''
                    else:
                        persoValue = main.model.rows[row][column + 2]['value']
                        persoValue = utils_functions.affichage2valeur(persoValue)
                    commandLine_my = utils_db.qdf_prof_countValue.format(
                        id_groupe, periode, id_count, id_eleve)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    commandLine_my = utils_db.insertInto('counts_values')
                    if (newValue != '') or (persoValue != ''):
                        query_my = utils_db.queryExecute(
                            {commandLine_my: (
                                id_groupe, periode, id_count, id_eleve, newValue, persoValue)}, 
                            query=query_my)
                    oldState.append((item, id_eleve, id_count, oldValue, persoValue))
                    # on recalcule la moyenne :
                    sens = main.model.columns[column]['other'][4]
                    recalculeCountsResults(
                        main, countsToUpdate={id_count: (column, sens, calcul)})
            # et on modifie l'affichage :
            if newValue == 'X':
                main.model.rows[row][column] = {
                    'value': newValue, 'color': utils.colorLightGray, 
                    'type': utils.NOTE_PERSO, 'bold': True, 'flag': 'EDITABLE'}
            else:
                main.model.rows[row][column] = {
                    'value': newValue, 
                    'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
        main.view.update(item)
    elif main.viewType == utils.VIEW_ACCOMPAGNEMENT:
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        if id_truc == 'PPRE':
            what = id_truc
            # on récupère l'appréciation :
            remark = ''
            commandLine_my = (
                'SELECT * FROM lsu '
                'WHERE lsuWhat="accompagnement" '
                'AND lsu1 IN ({0}) '
                'AND lsu2 = "PPRE"').format(id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                remark = query_my.value(3)
        else:
            what = id_truc
            remark = ''
        # on modifie la base :
        commandLine_my = (
            'DELETE FROM lsu '
            'WHERE lsuWhat="accompagnement" '
            'AND lsu1 IN ({0}) '
            'AND lsu2="{1}"').format(id_eleve, what)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        commandLine_my = utils_db.insertInto('lsu')
        if newValue != '':
            newValue = 'X'
            query_my = utils_db.queryExecute(
                {commandLine_my: (
                    'accompagnement', id_eleve, what, remark, '', '', '', '', '')}, 
                query=query_my)
        #oldState.append((item, id_eleve, id_truc, oldValue))
        # et on modifie l'affichage :
        if id_truc == 'remark':
            main.model.rows[row][column]['value'] = remark
        else:
            main.model.rows[row][column][
                'value'] = utils_functions.valeur2affichage(newValue)
            main.model.rows[row][column][
                'color'] = utils_functions.findColor(newValue)
        main.view.update(item)
    elif main.viewType == utils.VIEW_CPT_NUM:
        # ICI cptNum
        what = id_truc
        if doValidate:
            # besoin d'un query :
            query_my = utils_db.query(main.db_my)
            mustRecupOldValue = not(fromClipboard) \
                and not(newValue in ('', 'X')) and not(valuesUpDown)
            if mustRecupOldValue:
                newValue = oldValue
                # on récupère oldValue dans la base :
                oldValue = ''
                commandLine_my = (
                    'SELECT * FROM lsu '
                    'WHERE lsuWhat="cpt-num" '
                    'AND lsu1 IN ({0}) '
                    'AND lsu2 = "{1}"').format(id_eleve, what)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    oldValue = query_my.value(3)
            newValue = '{0}'.format(newValue)
            if newValue == 'X':
                newValue = ''
            if newValue != oldValue:
                # on modifie la base :
                commandLine_my = (
                    'DELETE FROM lsu '
                    'WHERE lsuWhat="cpt-num" '
                    'AND lsu1 IN ({0}) '
                    'AND lsu2="{1}"').format(id_eleve, what)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                commandLine_my = utils_db.insertInto('lsu')
                if not(newValue in ('', 'X', '0', 0)):
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (
                            'cpt-num', id_eleve, what, newValue, '', '', '', '', '')}, 
                        query=query_my)
        #oldState.append((item, id_eleve, id_truc, oldValue))
        # et on modifie l'affichage :
        main.model.rows[row][column] = {
            'value': newValue, 
            'type': utils.NOTE_PERSO, 
            'flag': 'EDITABLE'}
        main.view.update(item)
    elif main.viewType == utils.VIEW_DNB:
        # vue DNB :
        id_dnb = id_truc
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        # on modifie la base :
        commandLine_my = utils_db.qdf_prof_dnbValues.format(id_eleve, id_dnb)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        commandLine_my = utils_db.insertInto('dnb_values')
        if newValue != '':
            query_my = utils_db.queryExecute(
                {commandLine_my: (id_eleve, id_dnb, newValue)}, 
                query=query_my)
        oldState.append((item, id_eleve, id_dnb, oldValue))
        # et on modifie l'affichage :
        color = utils_functions.findColor(newValue)
        main.model.rows[row][column] = {
            'value': utils_functions.valeur2affichage(newValue), 
            'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
        main.view.update(item)
    elif main.viewType == utils.VIEW_BULLETIN:
        id_truc = id_truc.split('|')
        id_groupe = int(id_truc[0])
        matiereCode = id_truc[1]
        calculatedValue = main.model.rows[row][column]['calculatedValue']
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        # on modifie la base :
        commandLine_my = (
            'DELETE FROM lsu '
            'WHERE lsuWhat="positionnement" '
            'AND lsu1={0} '
            'AND lsu2="{1}" '
            'AND lsu3 IN ({2})').format(id_groupe, matiereCode, id_eleve)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        commandLine_my = utils_db.insertInto('lsu')
        if newValue != '':
            newValue = newValue[-1]
            query_my = utils_db.queryExecute(
                {commandLine_my: (
                    'positionnement', id_groupe, matiereCode, id_eleve, newValue, '', '', '', '')}, 
                query=query_my)
        #oldState.append((item, id_eleve, id_truc, oldValue))
        # et on modifie l'affichage :
        positioningText = QtWidgets.QApplication.translate('main', 'Positioning')
        if newValue != '':
            toolTip = QtWidgets.QApplication.translate(
                'main', 'validated.')
            bold = True
        else:
            newValue = calculatedValue
            toolTip = QtWidgets.QApplication.translate(
                'main', 'calculated by VERAC.')
            bold = False
        color = utils_functions.findColor(newValue)
        eleveName = main.model.rows[row][column]['toolTip'].split('\n')[0]
        toolTip = utils_functions.u('{0}\n{1} : {2}').format(
            eleveName, positioningText, toolTip)
        main.model.rows[row][column] = {
            'value': utils_functions.valeur2affichage(newValue), 
            'calculatedValue': calculatedValue, 
            'color': color, 
            'type': utils.EVAL, 
            'toolTip': toolTip, 
            'flag': 'EDITABLE'}
        if bold:
            main.model.rows[row][column]['bold'] = True
        main.view.update(item)

    return True


def undo(main):
    """
    blablabla
    """
    if len(main.undoList) < 1:
        return
    listForUndo = main.undoList.pop()
    listForRedo = []
    if len(main.undoList) == 0:
        main.actionUndo.setEnabled(False)
    utils_functions.doWaitCursor()
    try:
        doChangeState(main, listForUndo, listForRedo)
    finally:
        main.redoList.append(listForRedo)
        main.actionRedo.setEnabled(True)
        utils_functions.restoreCursor()
        main.changeDBMyState()


def redo(main):
    """
    blablabla
    """
    if len(main.redoList) < 1:
        return
    listForRedo = main.redoList.pop()
    listForUndo = []
    if len(main.redoList) == 0:
        main.actionRedo.setEnabled(False)
    utils_functions.doWaitCursor()
    try:
        doChangeState(main, listForRedo, listForUndo)
    finally:
        if len(listForUndo) > 0:
            main.undoList.append(listForUndo)
            main.actionUndo.setEnabled(True)
        utils_functions.restoreCursor()
        main.changeDBMyState()


def doChangeState(main, newState, oldState):
    query_my = utils_db.query(main.db_my)
    # pour les insertInto :
    lines = []
    # pour modifier la sélection après le changement :
    selectionModel = main.view.selectionModel()
    selectionModel.clear()
    if main.viewType == utils.VIEW_ITEMS:
        commandLineBind = utils_db.insertInto('evaluations')
        if main.id_tableau != -2:
            # si ce n'est pas un tableau de compilation,
            # on a juste à modifier la valeur :
            for (item, id_eleve, id_item, newValue) in newState:
                # on modifie la base :
                commandLine_my = utils_db.qdf_prof_evaluations5.format(main.id_tableau, 
                    id_eleve, id_item)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                if newValue != '':
                    lines.append((main.id_tableau, id_eleve, id_item, -1, newValue))
                # on modifie l'affichage :
                color = utils_functions.findColor(newValue)
                row, column = item.row(), item.column()
                oldValue = utils_functions.affichage2valeur(
                    main.model.rows[row][column]['value'])
                main.model.rows[row][column] = {
                    'value': utils_functions.valeur2affichage(newValue), 
                    'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
                main.view.update(item)
                oldState.append((item, id_eleve, id_item, oldValue))
                # et on modifie la sélection :
            if utils.PYQT == 'PYQT5':
                selectionModel.select(item, QtCore.QItemSelectionModel.Select)
            else:
                selectionModel.select(item, QtGui.QItemSelectionModel.Select)
        else:
            # ICI COUNTS
            oldValues = []
            for (item, id_eleve, id_item, newValue, newValues) in newState:
                # on modifie la base :
                for (id_tableau, newValueInTableau) in newValues:
                    # on récupère les anciennes valeurs pour la liste oldValues :
                    oldValueInTableau = ''
                    commandLine_my = utils_db.qs_prof_ValueItem.format(id_tableau, 
                        id_eleve, id_item)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    if query_my.first():
                        oldValueInTableau = query_my.value(0)
                    oldValues.append((id_tableau, oldValueInTableau))
                    commandLine_my = utils_db.qdf_prof_evaluations5.format(id_tableau, 
                        id_eleve, id_item)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    if newValueInTableau != '':
                        lines.append((id_tableau, id_eleve, id_item, -1, newValueInTableau))
                # on modifie l'affichage :
                color = utils_functions.findColor(newValue)
                row, column = item.row(), item.column()
                oldValue = utils_functions.affichage2valeur(
                    main.model.rows[row][column]['value'])
                main.model.rows[row][column] = {
                    'value': utils_functions.valeur2affichage(newValue), 
                    'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
                main.view.update(item)
                oldState.append((item, id_eleve, id_item, oldValue, oldValues))
                # et on modifie la sélection :
            if utils.PYQT == 'PYQT5':
                selectionModel.select(item, QtCore.QItemSelectionModel.Select)
            else:
                selectionModel.select(item, QtGui.QItemSelectionModel.Select)
        query_my = utils_db.queryExecute(
            {commandLineBind: lines}, query=query_my)
    elif main.viewType == utils.VIEW_NOTES:
        commandLineBind = utils_db.insertInto('notes_values')
        for (item, id_eleve, id_note, newValue) in newState:
            # on modifie la base :
            commandLine_my = utils_db.qdf_prof_noteValue.format(main.id_groupe, 
                utils.selectedPeriod, id_note, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if newValue != '':
                lines.append(
                    (main.id_groupe, utils.selectedPeriod, id_note, id_eleve, newValue))
            # on modifie l'affichage :
            row, column = item.row(), item.column()
            oldValue = main.model.rows[row][column]['value']
            if newValue == 'X':
                main.model.rows[row][column] = {
                    'value': newValue, 'color': utils.colorLightGray, 
                    'type': utils.NOTE_PERSO, 'bold': True, 'flag': 'EDITABLE'}
            else:
                main.model.rows[row][column] = {
                    'value': newValue, 
                    'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
            main.view.update(item)
            oldState.append((item, id_eleve, id_note, oldValue))
            # et on modifie la sélection :
            if utils.PYQT == 'PYQT5':
                selectionModel.select(item, QtCore.QItemSelectionModel.Select)
            else:
                selectionModel.select(item, QtGui.QItemSelectionModel.Select)
        query_my = utils_db.queryExecute(
            {commandLineBind: lines}, query=query_my)
        for (item, id_eleve, id_note, newValue) in newState:
            row, column = item.row(), item.column()
            recalculeMoyennes(
                main, main.model.columns[column]['other'], id_eleve, row, column)
    elif main.viewType == utils.VIEW_SUIVIS:
        for (item, id_eleve, id_suivi, newValue) in newState:
            # on modifie l'affichage :
            color = utils_functions.findColor(newValue)
            row, column = item.row(), item.column()
            oldValue = utils_functions.affichage2valeur(
                main.model.rows[row][column]['value'])
            main.model.rows[row][column] = {
                'value': utils_functions.valeur2affichage(newValue), 
                'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
            main.view.update(item)
            oldState.append((item, id_eleve, id_suivi, oldValue))
            # et on modifie la sélection :
            if utils.PYQT == 'PYQT5':
                selectionModel.select(item, QtCore.QItemSelectionModel.Select)
            else:
                selectionModel.select(item, QtGui.QItemSelectionModel.Select)
    elif main.viewType == utils.VIEW_ABSENCES:
        periode = utils.selectedPeriod
        commandLineBind = utils_db.insertInto('absences')
        for (item, id_eleve, id_absence, newValue) in newState:
            # on récupère absences et retards dans la base :
            absences = ['', '', '', '']
            commandLine_my = utils_db.qs_prof_absencesPeriodeEleves.format(
                periode, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                for i in range(4):
                    absences[i] = query_my.value(3 + i)
                    if absences[i] != '':
                        absences[i] = int(absences[i])
            absences[id_absence] = newValue
            if absences[id_absence] == 0:
                absences[id_absence] = ''
            # on modifie la base :
            commandLine_my = utils_db.qdf_prof_absencesPeriodeEleves.format(
                periode, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            test = ''
            for i in range(4):
                test = '{0}{1}'.format(test, absences[i])
            if test != '':
                for i in range(4):
                    if absences[i] == '':
                        absences[i] = 0
                lines.append((-1, periode, id_eleve, absences[0], absences[1], absences[2], absences[3]))
            # on modifie l'affichage :
            row, column = item.row(), item.column()
            oldValue = main.model.rows[row][column]['value']
            if newValue == 'X':
                main.model.rows[row][column] = {
                    'value': newValue, 'color': utils.colorLightGray, 
                    'type': utils.NOTE_PERSO, 'bold': True, 'flag': 'EDITABLE'}
            else:
                main.model.rows[row][column] = {
                    'value': newValue, 
                    'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
            main.view.update(item)
            oldState.append((item, id_eleve, id_absence, oldValue))
            # et on modifie la sélection :
            if utils.PYQT == 'PYQT5':
                selectionModel.select(item, QtCore.QItemSelectionModel.Select)
            else:
                selectionModel.select(item, QtGui.QItemSelectionModel.Select)
        query_my = utils_db.queryExecute(
            {commandLineBind: lines}, query=query_my)
    elif main.viewType == utils.VIEW_COUNTS:
        countsToUpdate = {}
        commandLineBind = utils_db.insertInto('counts_values')
        for (item, id_eleve, id_count, newCountValue, newPersoValue) in newState:
            # on modifie la base :
            commandLine_my = utils_db.qdf_prof_countValue.format(
                main.id_groupe, utils.selectedPeriod, id_count, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if (newCountValue != '') or (newPersoValue != ''):
                lines.append((main.id_groupe, utils.selectedPeriod, 
                              id_count, id_eleve, newCountValue, newPersoValue))
            # on modifie l'affichage :
            row, column = item.row(), item.column()
            if main.model.rows[row][column].get('type', '') == utils.EVAL:
                oldCountValue = newCountValue
                oldPersoValue = utils_functions.affichage2valeur(
                    main.model.rows[row][column]['value'])
                color = utils_functions.findColor(newPersoValue)
                main.model.rows[row][column] = {
                    'value': utils_functions.valeur2affichage(newPersoValue), 
                    'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
            else:
                oldCountValue = main.model.rows[row][column]['value']
                oldPersoValue = newPersoValue
                if newCountValue == 'X':
                    main.model.rows[row][column] = {
                        'value': newCountValue, 'color': utils.colorLightGray, 
                        'type': utils.NOTE_PERSO, 'bold': True, 'flag': 'EDITABLE'}
                else:
                    main.model.rows[row][column] = {
                        'value': newCountValue, 
                        'type': utils.NOTE_PERSO, 'flag': 'EDITABLE'}
                sens = main.model.columns[column]['other'][4]
                calcul = main.model.columns[column]['other'][5]
                countsToUpdate[id_count] = (column, sens, calcul)
            oldState.append((item, id_eleve, id_count, oldCountValue, oldPersoValue))
            main.view.update(item)
            # et on modifie la sélection :
            if utils.PYQT == 'PYQT5':
                selectionModel.select(item, QtCore.QItemSelectionModel.Select)
            else:
                selectionModel.select(item, QtGui.QItemSelectionModel.Select)
        query_my = utils_db.queryExecute(
            {commandLineBind: lines}, query=query_my)
        recalculeCountsResults(main, countsToUpdate=countsToUpdate)
    elif main.viewType == utils.VIEW_DNB:
        commandLineBind = utils_db.insertInto('dnb_values')
        for (item, id_eleve, id_dnb, newValue) in newState:
            # on modifie la base :
            commandLine_my = utils_db.qdf_prof_dnbValues.format(id_eleve, id_dnb)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if newValue != '':
                lines.append((id_eleve, id_dnb, newValue))
            # on modifie l'affichage :
            color = utils_functions.findColor(newValue)
            row, column = item.row(), item.column()
            oldValue = utils_functions.affichage2valeur(
                main.model.rows[row][column]['value'])
            main.model.rows[row][column] = {
                'value': utils_functions.valeur2affichage(newValue), 
                'color': color, 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'}
            main.view.update(item)
            oldState.append((item, id_eleve, id_dnb, oldValue))
            # et on modifie la sélection :
            if utils.PYQT == 'PYQT5':
                selectionModel.select(item, QtCore.QItemSelectionModel.Select)
            else:
                selectionModel.select(item, QtGui.QItemSelectionModel.Select)
        query_my = utils_db.queryExecute(
            {commandLineBind: lines}, query=query_my)
    # mise à jour de la sélection :
    for item in selectionModel.selectedIndexes():
        main.view.update(item)


def simplify(main):
    """
    Simplifie les évaluations des items sélectionnés.
    """
    utils_functions.doWaitCursor()
    try:
        oldState = []
        for item in main.view.selectionState['evals']:
            row, column = item.row(), item.column()
            value = utils_functions.affichage2valeur(item.data())
            moyenne = utils_calculs.moyenneItem(value)
            if column > 2:
                doChangeValue(main, row, column, moyenne, oldState)
    finally:
        utils_functions.restoreCursor()
        main.changeDBMyState()
        if len(oldState) > 0:
            main.undoList.append(oldState)
            main.actionUndo.setEnabled(True)


def clearIfUnfavorable(main):
    """
    Efface les évaluations sélectionnées si elles sont défavorables à l'élève.
    """
    utils_functions.doWaitCursor()
    try:
        oldState = []
        for item in main.view.selectionState['evals']:
            if item.data() == '':
                continue
            row, column = item.row(), item.column()
            id_eleve = main.model.rows[row][0].get('value', -1)
            id_item = main.model.columns[column].get('id', -1)
            period = utils.selectedPeriod
            withOutThis = ''
            withThis = ''
            query_my = utils_db.query(main.db_my)
            commandLine_my = (
                'SELECT evaluations.* FROM evaluations '
                'JOIN tableaux ON tableaux.id_tableau=evaluations.id_tableau '
                'WHERE evaluations.id_eleve={0} '
                'AND evaluations.id_item={1} '
                'AND tableaux.Public=1 '
                'AND tableaux.Periode IN (0, {2})').format(id_eleve, id_item, period)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_tableau = int(query_my.value(0))
                value = query_my.value(4)
                withThis += value
                if id_tableau != main.id_tableau:
                    withOutThis += value
            mustDelete = False
            withOutThis = utils_calculs.calculNote(main, value=withOutThis)
            withThis = utils_calculs.calculNote(main, value=withThis)
            #print('WITHOUT:', withOutThis, 'WITH:', withThis)
            if withThis != '':
                if withOutThis != '':
                    if withThis <= withOutThis:
                        mustDelete = True
            if mustDelete:
                doChangeValue(main, row, column, '', oldState)
    finally:
        utils_functions.restoreCursor()
        main.changeDBMyState()
        if len(oldState) > 0:
            main.undoList.append(oldState)
            main.actionUndo.setEnabled(True)


def testSuivisMustSelectHoraires(main):
    """
    Pour vérifier si on a bien sélectionné les horaires avant de poster.
    On vérifie d'abord s'il y a des évaluations (pour chaque ligne).
    """
    columnCount = main.model.columnCount()
    for row in range(main.model.rowCount()):
        hasEvaluations = False
        for column in range(3, columnCount - 3):
            if main.model.rows[row][column]['type'] == utils.EVAL:
                # c'est une évaluation
                value = main.model.rows[row][column]['value']
                if value != '':
                    hasEvaluations = True
        if hasEvaluations:
            horaireName = main.model.rows[row][columnCount - 2]['value']
            if horaireName == '':
                message = QtWidgets.QApplication.translate('main', 
                    '<p>You forgot to select the <b>timing</b>'
                    '<br/>of your assessments.</p>')
                utils_functions.messageBox(
                    main, level='warning', message=message)
                return True
    return False


def testSuivisModified(main):
    if main.viewType != utils.VIEW_SUIVIS:
        main.suivisIsModified = False
    if not(main.suivisIsModified):
        return True
    message = QtWidgets.QApplication.translate(
        'main', 
        '<p>The viewSuivis has been modified.</p>'
        '<p></p>'
        '<p><b>Would you upload the changes?</b></p>')
    reponse = utils_functions.messageBox(
        main, level='warning', message=message, buttons=['Yes', 'No', 'Cancel'])
    if reponse == QtWidgets.QMessageBox.Cancel:
        return False
    elif reponse == QtWidgets.QMessageBox.No:
        main.suivisIsModified = False
        return True
    # on envoie les évaluations des élèves suivis :
    if main.actualVersion['versionName'] == 'demo':
        utils_functions.notInDemo(main)
        return True
    else:
        return postSuivis(main)


def postSuivis(main):
    if testSuivisMustSelectHoraires(main):
        return False
    utils_functions.doWaitCursor()
    upOK = False
    try:
        # récupération de la liste des élèves :
        eleves = []
        for row in range(main.model.rowCount()):
            id_eleve = main.model.rows[row][0]['value']
            if id_eleve > -1:
                eleves.append(id_eleve)

        # récupération de la liste des compétences suivies :
        ids_suivis = []
        query_commun = utils_db.query(main.db_commun)
        columnCount = main.model.columnCount()
        for column in range(3, columnCount - 3):
            # on récupère l'id_suivi :
            if 'id' in main.model.columns[column]:
                id_suivi = main.model.columns[column]['id']
            else:
                id_suivi = -1
            ids_suivis.append(id_suivi)

        # récupération de qu'est-ce qui est suivi par quel PP
        # dico un peu complexe pour permettre la possibilité que 2 PP
        # suivent le même élève
        # la valeur -1 correspond au groupe (si un groupe est sélectionné)
        eleveSuiviPP = {}
        listIdPP = []
        for (id_pp, id_eleve, id_suivi, label2) in main.elevesSuivis:
            if (id_eleve in eleves) and not(id_pp in listIdPP):
                listIdPP.append(id_pp)
            if (id_eleve in eleves) and (id_suivi in ids_suivis):
                if id_eleve in eleveSuiviPP:
                    if not(id_pp in eleveSuiviPP[id_eleve][0]):
                        eleveSuiviPP[id_eleve][0].append(id_pp)
                    if not(id_suivi in eleveSuiviPP[id_eleve][1]):
                        eleveSuiviPP[id_eleve][1][id_suivi] = [id_pp]
                    else:
                        eleveSuiviPP[id_eleve][1][id_suivi].append(id_pp)
                else:
                    eleveSuiviPP[id_eleve] = ([id_pp], {id_suivi: [id_pp]})
        if main.id_groupe > -1:
            eleveSuiviPP[-1] = ([], {})
            for id_pp in listIdPP:
                eleveSuiviPP[-1][0].append(id_pp)
            for id_suivi in ids_suivis:
                eleveSuiviPP[-1][1][id_suivi] = []
                for id_pp in listIdPP:
                    eleveSuiviPP[-1][1][id_suivi].append(id_pp)

        # récupération des horaires :
        horaires = {}
        for (id_horaire, name, label) in main.horaires:
            horaires[name] = id_horaire

        # création de la table evaluations :
        evaluations = []
        id_prof = main.me['userId']
        if main.id_groupe > -1:
            matiereName = prof_groupes.matiereFromGroupe(main, main.id_groupe)
        else:
            matiereName = main.me['Matiere']
        for row in range(main.model.rowCount()):
            id_eleve = main.model.rows[row][0]['value']
            if id_eleve < 0:
                id_eleve = -1
            date = main.model.rows[row][columnCount - 3]['value']
            horaire = horaires[main.model.rows[row][columnCount - 2]['value']]
            appreciation = main.model.rows[row][columnCount - 1]['value']
            for column in range(3, columnCount - 3):
                if main.model.rows[row][column]['type'] == utils.EVAL:
                    # c'est une évaluation
                    value = main.model.rows[row][column]['value']
                    if value != '':
                        value = utils_functions.affichage2valeur(value)
                        # on récupère l'id_suivi :
                        if 'id' in main.model.columns[column]:
                            id_suivi = main.model.columns[column]['id']
                        else:
                            id_suivi = -1
                        for id_pp in eleveSuiviPP[id_eleve][1][id_suivi]:
                            evaluations.append((id_prof, matiereName, id_pp, id_eleve, 
                                                date, horaire, id_suivi, value))
            if appreciation != '':
                for id_pp in eleveSuiviPP[id_eleve][0]:
                    evaluations.append((id_prof, matiereName, id_pp, id_eleve, 
                                        date, horaire, -1, appreciation))

        # on crée la base suivi_xx dans temp :
        dbFileTemp_suivixx = utils_functions.u('{0}/up/suivi_{1}.sqlite').format(main.tempPath, id_prof)
        # suppression d'un éventuel fichier précédent :
        if QtCore.QFile(dbFileTemp_suivixx).exists():
            removeOK = QtCore.QFile(dbFileTemp_suivixx).remove()
            if not(removeOK):
                utils_functions.myPrint('REMOVE ERROR : ', dbFileTemp_suivixx)
        (db_suivixx, dbName) = utils_db.createConnection(main, dbFileTemp_suivixx)
        query, transactionOK = utils_db.queryTransactionDB(db_suivixx)
        # on crée la table suivi_evals :
        query = utils_db.queryExecute(utils_db.qct_suivi_evals, query=query)
        # on la remplit :
        commandLine = utils_db.insertInto('suivi_evals', 8)
        query = utils_db.queryExecute({commandLine: evaluations}, query=query)
        # on termine la transaction :
        utils_db.endTransaction(query, db_suivixx, transactionOK)
        if query != None:
            query.clear()
            del query
        db_suivixx.close()
        del db_suivixx
        utils_db.sqlDatabase.removeDatabase(dbName)

        # on poste la base :
        # une fois la base envoyée, elle doit être traitée sur le site
        theFileName = utils_functions.u('suivi_{0}.sqlite').format(id_prof)
        theUrl = main.siteUrlPublic + '/pages/verac_prof_putFollows.php'
        upLoader = utils_web.UpDownLoader(
            main, theFileName, main.tempPath + '/up/', theUrl, down=False)
        upLoader.launch()
        while upLoader.state == utils_web.STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if upLoader.state == utils_web.STATE_OK:
            if 'OK' in utils_functions.u(upLoader.reponse):
                upOK = True
    finally:
        utils_functions.restoreCursor()
        if upOK:
            main.suivisIsModified = False
            message = QtWidgets.QApplication.translate('main', 'The file was sent successfully.')
            utils_functions.messageBox(main, message=message, timer=5)
        else:
            message = QtWidgets.QApplication.translate('main', 'A problem occurred during the transfer.')
            utils_functions.afficheMsgPb(main, message)
        return upOK




























































































###########################################################"
#        CALCULS DES VUES À AFFICHER
#        (ITEMS, BILANS, ETC...)
###########################################################"

def doSeparatorColumn(column=True):
    if column:
        result = {
            'value': 'VSEPARATOR', 
            'columnWidth': '0.2cm', 
            'datasType': utils.INDETERMINATE}
    else:
        result = {
            'value': 'VSEPARATOR', 
            'color': utils.colorGray, 
            'type': utils.LINE, 
            'flag': 'NOCHANGE'}
    return result


def calcAll(main, id_tableau=-1, forcer=False):
    """
    après modifications d'un tableau (évaluations modifiées, ajout d'items, etc)
    il faut recalculer les bilans.
    Le dictionnaire whatIsModified indique les élèves et les items qui ont changé
    (-1 signifie qu'on recalcule tout).
    """
    # on vérifie si la dernière saisie est validée :
    testLastNumberValidated(main)
    testAppreciationMustBeSaved(main)
    try:
        whatIsModified = {'id_eleves': [], 'id_items': [], 'actualTableau': False}
        if id_tableau == -1:
            id_tableau = main.id_tableau
            whatIsModified = main.whatIsModified
            if forcer:
                main.whatIsModified['actualTableau'] = True
            if main.whatIsModified['actualTableau'] == False:
                return
        if forcer:
            whatIsModified = {'id_eleves': [-1], 'id_items': [-1], 'actualTableau':True}
        if whatIsModified == {'id_eleves': [], 'id_items': [], 'actualTableau': False}:
            return
        # on récupère les données liées au tableau
        # (groupe et périodes à recalculer) :
        data = diversFromSelection(main, id_tableau=id_tableau)
        # on lance les calculs :
        reCalcBilans(main, data=data, whatIsModified=whatIsModified)
        if id_tableau == main.id_tableau:
            main.whatIsModified = {'id_eleves': [], 'id_items': [], 'actualTableau': False}
    except:
        utils_functions.myPrint('calcAll : EXCEPT')
        pass


def reCalcBilans(main, id_groupe=-1, periode=-1, data=(),
                 whatIsModified={'id_eleves': [-1], 'id_items': [-1], 'actualTableau':False}):
    """
    On recalcule les valeurs et on les inscrit dans la table evaluations.
    On tient compte des comptages liés à des bilans.
    S'il y a des notes calculées (calcMode = 1 ou 2), on relance le calcul.
    """
    utils_functions.doWaitCursor()
    debut = QtCore.QTime.currentTime()
    transactionOK = False
    try:
        # besoin d'une transaction :
        query_my, transactionOK = utils_db.queryTransactionDB(main.db_my)
        # récupération de la sélection :
        if data == ():
            data = diversFromSelection(main, -1, id_groupe, periode)
        id_groupe, periode, periodesToUse = data[0], data[3], data[7]
        # on récupère la liste des élèves :
        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        # on récupère les comptages liés à des items des élèves du groupe :
        counts = countsFromGroup(main, id_groupe)

        if len(periodesToUse[1]) < 1:
            # bug B² (prise en compte d'un tableau 
            # "année" déplacé dans "tr1")
            periodesToUse = (periodesToUse[0], [0])
        for periodeToCalc in periodesToUse[1]:
            if utils.PROTECTED_PERIODS[periodeToCalc]:
                continue
            # on récupère tous les tableaux du groupe pour la période considérée
            tableaux = prof_groupes.tableauxFromGroupe(main, id_groupe, periodeToCalc)
            # et leurs ids (tableaux ou templates) :
            tableauxOrTemplates = []
            for id_tableau in tableaux:
                id_template = diversFromSelection(main, id_tableau=id_tableau)[8]
                if id_template < 0:
                    tableauxOrTemplates.append(id_template)
                else:
                    tableauxOrTemplates.append(id_tableau)
            id_tableauxOrTemplates = utils_functions.array2string(tableauxOrTemplates)
            id_tableaux = utils_functions.array2string(tableaux)

            # dicoBilans pour récupérer les bilans à calculer
            # pour chaque bilan, on récupère bilanName, bilanLabel, id_competence
            # mais aussi la liste des items liés (dicoBilans[id_bilan][3])
            # et un boolean mustRecalc pour savoir s'il faut recalculer ce bilan
            # (on ne le sait qu'après avoir listé tous les items liés)
            dicoBilans = {}

            # on cherche tous les bilans :
            commandLine_my = utils_functions.u(
                'SELECT DISTINCT items.*, bilans.*, item_bilan.coeff '
                'FROM items '
                'JOIN item_bilan ON item_bilan.id_item=items.id_item '
                'JOIN tableau_item ON tableau_item.id_item=items.id_item '
                'JOIN bilans ON bilans.id_bilan=item_bilan.id_bilan '
                'WHERE item_bilan.id_item>-1 '
                'AND bilans.id_competence>-2 '
                'AND tableau_item.id_tableau IN ({0})').format(id_tableauxOrTemplates)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                itemName = query_my.value(1)
                id_bilan = int(query_my.value(3))
                bilanName = query_my.value(4)
                bilanLabel = query_my.value(5)
                id_competence = int(query_my.value(6))
                coeff = int(query_my.value(7))
                mustRecalc = False
                if ((-1 in whatIsModified['id_items']) or (id_item in whatIsModified['id_items'])):
                    mustRecalc = True
                # on place les données dans le dictionnaire :
                if id_bilan in dicoBilans:
                    utils_functions.appendUnique(
                        dicoBilans[id_bilan][3], (id_item, itemName, coeff))
                    if mustRecalc:
                        dicoBilans[id_bilan][4] = True
                else:
                    dicoBilans[id_bilan] = [
                        bilanName, bilanLabel, id_competence, 
                        [(id_item, itemName, coeff)], 
                        mustRecalc]
            # on ajoute les bilans des items évalués par comptage :
            count_items = utils_functions.array2string(counts[periodeToCalc]['ITEMS'])
            commandLine_my = (
                'SELECT DISTINCT items.*, bilans.*, item_bilan.coeff '
                'FROM items '
                'JOIN item_bilan ON item_bilan.id_item=items.id_item '
                'JOIN bilans ON bilans.id_bilan=item_bilan.id_bilan '
                'WHERE items.id_item IN ({0}) '
                'AND bilans.id_competence>-2').format(count_items)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                itemName = query_my.value(1)
                id_bilan = int(query_my.value(3))
                bilanName = query_my.value(4)
                bilanLabel = query_my.value(5)
                id_competence = int(query_my.value(6))
                coeff = int(query_my.value(7))
                # ICI COUNTS
                mustRecalc = True#False
                """
                if ((-1 in whatIsModified['id_items']) or (id_item in whatIsModified['id_items'])):
                    mustRecalc = True
                """
                # on place les données dans le dictionnaire :
                if id_bilan in dicoBilans:
                    utils_functions.appendUnique(
                        dicoBilans[id_bilan][3], (id_item, itemName, coeff))
                    if mustRecalc:
                        dicoBilans[id_bilan][4] = True
                else:
                    dicoBilans[id_bilan] = [
                        bilanName, bilanLabel, id_competence, 
                        [(id_item, itemName, coeff)], 
                        mustRecalc]

            # récupération des valeurs dans un dictionnaire :
            dicoValues = {}
            commandLine_my = utils_db.qs_prof_ValuesItems.format(id_tableaux)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(1))
                id_item = int(query_my.value(2))
                value = query_my.value(4)
                if (id_eleve, id_item) in dicoValues:
                    dicoValues[(id_eleve, id_item)] += value
                else:
                    dicoValues[(id_eleve, id_item)] = value
            # on ajoute les évaluations faites par comptage :
            for (id_eleve, id_item) in counts[periodeToCalc]['VALUES']:
                count_value = counts[periodeToCalc]['VALUES'][(id_eleve, id_item)]
                if (id_eleve, id_item) in dicoValues:
                    dicoValues[(id_eleve, id_item)] += count_value
                else:
                    dicoValues[(id_eleve, id_item)] = count_value

            # on calcule le faux id_tableau (id_selection) pour lire les données :
            id_selection = utils_functions.pg2selection(periodeToCalc, id_groupe)

            lines = []
            # on vide la table evaluations des bilans de cette sélection :
            if (-1 in whatIsModified['id_eleves']):
                commandLine_my = utils_db.qdf_prof_evaluations11.format(id_selection)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            else:
                for id_eleve in whatIsModified['id_eleves']:
                    for id_bilan in dicoBilans:
                        if dicoBilans[id_bilan][4]:
                            commandLine_my = utils_db.qdf_prof_evaluations6.format(
                                id_selection, id_eleve, id_bilan)
                            lines.append(commandLine_my)
                            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            # pour chaque élève :
            lines = []
            for (id_eleve, ordre, eleveName) in elevesInGroup:
                if ((-1 in whatIsModified['id_eleves']) or (id_eleve in whatIsModified['id_eleves'])):
                    for id_bilan in dicoBilans:
                        if dicoBilans[id_bilan][4]:
                            # C'EST LÀ QU'IL FAUT CALCULER LE BILAN :
                            value = ''
                            values = utils_calculs.value2List('')
                            for item in dicoBilans[id_bilan][3]:
                                id_item = item[0]
                                coeff = item[2]
                                value2 = ''
                                if (id_eleve, id_item) in dicoValues:
                                    value2 = dicoValues[(id_eleve, id_item)]
                                    value2 = utils_calculs.moyenneItem(value2)
                                listvalueItem = utils_calculs.calculItemValue(
                                    value2, coeff)
                                for i in range(utils.NB_COLORS + 1):
                                    values[i] += listvalueItem[i]
                            value = utils_calculs.calculBilan(values)
                            #print(values, value)
                            # on remplit la table evaluations :
                            if value != '':
                                lines.append((id_selection, id_eleve, -1, id_bilan, value))
            commandLine_my = utils_db.insertInto('evaluations')
            query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

            # un deuxième dictionnaire pour les bilans du groupe
            # on passe tous les bilans en revue, mais on ne gardera
            # que ceux qui sont à recalculer (dicoBilans[id_bilan][4] à True) :
            bilansGroupeDic = {}
            bilansNotInDico = []
            id_students = utils_functions.array2string(elevesInGroup, key=0)
            commandLine_my = (
                'SELECT id_bilan, value FROM evaluations '
                'WHERE id_eleve IN ({0}) '
                'AND id_tableau={1} AND id_bilan!=-1').format(id_students, id_selection)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_bilan = int(query_my.value(0))
                value = query_my.value(1)
                try:
                    if dicoBilans[id_bilan][4]:
                        try:
                            bilansGroupeDic[id_bilan] += value
                        except:
                            bilansGroupeDic[id_bilan] = value
                except:
                    if not(id_bilan in bilansNotInDico):
                        bilansNotInDico.append(id_bilan)
            if len(bilansNotInDico) > 0:
                utils_functions.afficheMessage(main, ['bilansNotInDico :', bilansNotInDico])
            commandLine_myBase = (
                'DELETE FROM groupe_bilan '
                'WHERE id_bilan={0} AND id_groupe={1} AND Periode={2}')
            for id_bilan in bilansGroupeDic:
                commandLine_my = commandLine_myBase.format(id_bilan, id_groupe, periodeToCalc)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            commandLine_my = utils_db.insertInto('groupe_bilan')
            lines = []
            for id_bilan in bilansGroupeDic:
                value = utils_calculs.moyenneBilan(bilansGroupeDic[id_bilan])
                if value != '':
                    lines.append((id_bilan, id_groupe, periodeToCalc, value))
            if len(lines) > 0:
                query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

        # mise à jour des notes calculées d'après les items ou les bilans :
        commandLine_my = 'SELECT * FROM notes WHERE id_groupe={0} AND calcMode>=0'.format(id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            notePeriode = int(query_my.value(1))
            if utils.PROTECTED_PERIODS[notePeriode]:
                continue
            id_note = int(query_my.value(2))
            name = query_my.value(3)
            base = int(query_my.value(4))
            coeff = int(query_my.value(5))
            calcMode = int(query_my.value(6))
            data = (id_groupe, notePeriode, id_note, name, base, coeff, calcMode)
            if main.viewType != utils.VIEW_NOTES:
                recalculeNote(main, data, column=-1)
            elif notePeriode != periode:
                recalculeNote(main, data, column=-1)
            else:
                recalculeNote(main, data)
    except:
        print('except in reCalcBilans')
    finally:
        if transactionOK:
            utils_db.endTransaction(query_my, main.db_my, transactionOK)
        main.changeDBMyState()
        utils_functions.restoreCursor()


def calcAllGroupes(main, msgFin=True, withProgress=False):
    """
    recalcule les bilans de tous les groupes
    utilisé après modification des modes de calculs dans la config.
    On commence par remettre les calculs à zéros :
        on vide les dictionnaires d'accélération de l'affichage
        et on recrée la table moyennesItems.
    """
    utils_calculs.reinitCalculBilanDic()
    utils_functions.reinitFindColorDic()
    import prof_db
    prof_db.recalcMoyennesItems(main)
    debut = QtCore.QTime.currentTime()
    utils_functions.doWaitCursor()
    try:
        # liste des couples (id_groupe, periode) à traiter :
        mustDo = []
        # on récupère la liste des groupes :
        groupes = {}
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format(
                'groupes', 'ordre, Matiere, UPPER(Name)'), 
            db=main.db_my)
        while query_my.next():
            id_groupe = int(query_my.value(0))
            groupeName = query_my.value(1)
            groupes[id_groupe] = groupeName
        # pour chaque groupe, on cherche les périodes à traiter :
        commandLine_test = 'SELECT id_tableau FROM tableaux WHERE id_groupe={0} AND Periode={1}'
        for id_groupe in groupes:
            for periode in range(utils.NB_PERIODES):
                commandLine_my = commandLine_test.format(id_groupe, periode)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                if query_my.first():
                    mustDo.append((id_groupe, periode))
        total = len(mustDo)
        if withProgress:
            # création du progressDialog :
            title = utils.PROGNAME + ' : ' + QtWidgets.QApplication.translate('main', 'RecalcAllGroupes')
            progressDialog = utils_functions.initProgressDialog(main, title, total)
        num_selection = 0
        for (id_groupe, periode) in mustDo:
            num_selection += 1
            groupeName = groupes[id_groupe]
            periodeName = utils.PERIODES[periode]
            groupePeriodeMessage = utils_functions.u('{0} - {1}').format(groupeName, periodeName)
            message = QtWidgets.QApplication.translate('main', 'Recalc Groupe : {0}')
            message = utils_functions.u(message).format(groupePeriodeMessage)
            utils_functions.afficheMessage(main, message, statusBar=True)
            if withProgress:
                utils_functions.incrementeProgressDialog(main, progressDialog, message)
            reCalcBilans(main, id_groupe=id_groupe, periode=periode)
        try:
            main.whatIsModified['actualTableau'] = False
        except:
            pass
    finally:
        utils_functions.restoreCursor()
        fin = QtCore.QTime.currentTime()
        duree = debut.msecsTo(fin)
        utils_functions.myPrint('DUREE: ', duree)
        if withProgress:
            utils_functions.endProgressDialog(main, progressDialog)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)


def calcItems(main, id_tableau):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    main.tableauxForItem = {}
    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        editableFlag = ''
    else:
        editableFlag = 'EDITABLE'
    try:
        columns = [{'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]
        itemsList = []
        query_my = utils_db.query(main.db_my)

        # la liste tableaux pour gérer le cas d'une compilation :
        # ICI COUNTS ajouter les items et évaluations faites par comptages
        if id_tableau == -2:
            tableaux = main.tableauxInCompil
        else:
            tableaux = [id_tableau]

        for id_tableau in tableaux:
            id_template = diversFromSelection(main, id_tableau=id_tableau)[8]
            if id_template < 0:
                commandLine_my = utils_db.qs_prof_ItemsInTableau.format(id_template)
            else:
                commandLine_my = utils_db.qs_prof_ItemsInTableau.format(id_tableau)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                utils_functions.appendUnique(itemsList, id_item)
                if (id_item in main.tableauxForItem):
                    main.tableauxForItem[id_item].append(id_tableau)
                else:
                    main.tableauxForItem[id_item] = [id_tableau]

        for id_item in itemsList:
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'items', 'id_item', id_item)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if query_my.first():
                itemName = query_my.value(1)
                itemLabel = query_my.value(2)
                columns.append({
                    'value': itemName, 
                    'id': id_item, 
                    'toolTip':itemLabel, 
                    'datasType': utils.EVAL})

        elevesInTableau = listStudentsInGroup(main, id_tableau=tableaux[0])

        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_tableaux = utils_functions.array2string(tableaux)
        id_students = utils_functions.array2string(elevesInTableau, key=0)
        id_items = utils_functions.array2string(itemsList)
        commandLine_my = utils_db.qs_prof_evaluations.format(
            id_tableaux, id_students, id_items)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            id_item = int(query_my.value(2))
            value = query_my.value(4)
            if (id_eleve, id_item) in valuesDic:
                valuesDic[(id_eleve, id_item)] += value
            else:
                valuesDic[(id_eleve, id_item)] = value
        for (id_eleve, ordre, eleveName) in elevesInTableau:
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            column = 3
            for id_item in itemsList:
                value = ''
                if (id_eleve, id_item) in valuesDic:
                    value = valuesDic[(id_eleve, id_item)]
                color = utils_functions.findColor(value)
                toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                    eleveName, columns[column]['value'], columns[column]['toolTip'])
                row.append({
                    'value': utils_functions.valeur2affichage(value), 
                    'color': color, 
                    'type': utils.EVAL, 
                    'bold': True, 
                    'flag': editableFlag, 
                    'toolTip': toolTip})
                column += 1
            rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcStats(main, id_tableau=-1):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    try:
        query_my = utils_db.query(main.db_my)
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]
        noteText = QtWidgets.QApplication.translate('main', 'Notes')
        if id_tableau == -1:
            numberToolTip = QtWidgets.QApplication.translate('main', 'Number of balances')
            percentageToolTip = QtWidgets.QApplication.translate('main', 'Percentage of balances')
            noteToolTip = QtWidgets.QApplication.translate('main', 'Note calculated from the balances')
        else:
            numberToolTip = QtWidgets.QApplication.translate('main', 'Number of items')
            percentageToolTip = QtWidgets.QApplication.translate('main', 'Percentage of items')
            noteToolTip = QtWidgets.QApplication.translate('main', 'Note calculated from the items')

        columnsList = [e for e in utils.itemsValues]
        columnsList.reverse()
        for i in columnsList:
            value = utils.affichages[i][0]
            toolTip = utils_functions.u('{0} ({1})').format(
                numberToolTip, 
                utils.affichages[i][1])
            columns.append({
                'value': value, 
                'toolTip': toolTip, 
                'datasType': utils.NUMBER, 
                'columnWidth': '1.5cm'})
        columns.append(doSeparatorColumn())
        for i in columnsList:
            value = utils_functions.u('{0} %').format(
                utils.affichages[i][0])
            toolTip = utils_functions.u('{0} ({1})').format(
                percentageToolTip, 
                utils.affichages[i][1])
            columns.append({
                'value': value, 
                'toolTip': toolTip, 
                'datasType': utils.NUMBER, 
                'columnWidth': '1.5cm'})
        columns.append(doSeparatorColumn())
        columns.append({
            'value': noteText, 
            'toolTip': noteToolTip, 
            'datasType': utils.NUMBER, 
            'columnWidth': '1.5cm'})

        tableaux = []
        if id_tableau != -1:
            # la liste tableaux pour gérer le cas d'une compilation :
            # ICI COUNTS tenir compte des comptages
            if id_tableau == -2:
                tableaux = main.tableauxInCompil
            else:
                tableaux = [id_tableau]

        # récupération de la sélection :
        data = diversFromSelection(main, id_tableau=id_tableau)
        id_groupe, matiereName = data[0], data[2]
        periode, periodeToUse = data[3], data[7][0]

        # on calcule le faux id_tableau (id_selection) pour lire les données :
        id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)

        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)

        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        if id_tableau == -1:
            id_tableaux = '{0}'.format(id_selection)
        else:
            id_tableaux = utils_functions.array2string(tableaux)
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        if id_tableau == -1:
            commandLine_my = ('SELECT * FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_item=-1').format(id_tableaux, id_students)
        else:
            commandLine_my = ('SELECT * FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_bilan=-1').format(id_tableaux, id_students)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            value = query_my.value(4)
            if id_eleve in valuesDic:
                valuesDic[id_eleve] += value
            else:
                valuesDic[id_eleve] = value
        for id_eleve in valuesDic:
            value = valuesDic[id_eleve]
            stats = utils_calculs.calculStats(main, value)
            valuesDic[id_eleve] = stats
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            if id_eleve in valuesDic:
                temp = valuesDic[id_eleve]
            else:
                temp = ['', '', '', '', '', '', '', '', '', '', '']
            stats = temp[:utils.NB_COLORS + 1]
            stats.append('VSEPARATOR')
            stats.extend(temp[utils.NB_COLORS + 1:2 * (utils.NB_COLORS + 1)])
            stats.append('VSEPARATOR')
            stats.append(temp[2 * (utils.NB_COLORS + 1)])

            column = 3
            for value in stats:
                if value == 'VSEPARATOR':
                    row.append(doSeparatorColumn(column=False))
                elif value != '':
                    if column in (3, 9):
                        color = utils.affichages['X'][3]
                    elif column in (4, 10):
                        color = utils.affichages['D'][3]
                    elif column in (5, 11):
                        color = utils.affichages['C'][3]
                    elif column in (6, 12):
                        color = utils.affichages['B'][3]
                    elif column in (7, 13):
                        color = utils.affichages['A'][3]
                    else:
                        color = utils.colorWhite
                    toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                        eleveName, columns[column]['value'], columns[column]['toolTip'])
                    row.append({
                        'value': value, 
                        'color': color, 
                        'type': utils.NUMBER, 
                        'bold': True, 
                        'toolTip': toolTip})
                else:
                    row.append({
                        'value': value, 
                        'color': utils.colorWhite, 
                        'type': utils.NUMBER, 
                        'bold': True})
                column += 1
            rows.append(row)
    finally:
        utils_functions.restoreCursor()
        return columns, rows


def getStudentsAndGroupProfils(main, students, periods, id_groupe, matiereName=''):
    """
    retourne la liste des profils valables pour les élèves, 
    les périodes et le groupe indiqués.
    """
    result = {'STUDENTS': {}, 'GROUP': {}, 'DEFAULT': -999, 'ALL': []}
    query_my = utils_db.query(main.db_my)
    # on récupère la matière liée au groupe :
    if matiereName == '':
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'groupes', 'id_groupe', id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        if query_my.first():
            matiereName = query_my.value(2)
    # on cherche dans les tables profils et profil_who :
    commandLine_my = utils_functions.u(
        'SELECT DISTINCT profils.*, profil_who.* '
        'FROM profils '
        'LEFT JOIN profil_who ON profil_who.id_profil=profils.id_profil '
        'WHERE '
        '   profils.Matiere="{0}" '
        'AND ( '
        '       (profils.profilType="STUDENTS" '
        '       AND profil_who.id_who IN ({2}) '
        '       AND profil_who.Periode IN (0, {1})) '
        '   OR '
        '       (profils.profilType="GROUPS" '
        '       AND profil_who.id_who={3} '
        '       AND profil_who.Periode IN (0, {1})) '
        '   OR '
        '       (profils.id_profil<0) '
        '   )').format(matiereName, periods, students, id_groupe)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_profil = int(query_my.value(0))
        profilType = query_my.value(3)
        if id_profil < 0:
            # profil par défaut de la matière :
            result['DEFAULT'] = id_profil
        elif profilType == 'STUDENTS':
            # profil attribué à un élève :
            id_eleve = int(query_my.value(4))
            period = int(query_my.value(6))
            if not(id_eleve in result['STUDENTS']):
                result['STUDENTS'][id_eleve] = {}
            if not(period in result['STUDENTS'][id_eleve]):
                result['STUDENTS'][id_eleve][period] = id_profil
            # on l'ajoute à la liste de tous les profils :
            if not(id_profil in result['ALL']):
                result['ALL'].append(id_profil)
        else:
            # profil standard du groupe pour cette période :
            period = int(query_my.value(6))
            result['GROUP'][period] = id_profil
            # on l'ajoute à la liste de tous les profils :
            if not(id_profil in result['ALL']):
                result['ALL'].append(id_profil)
    # on transforme students et periods en tableaux :
    students = utils_functions.string2array(students, integer=True)
    periods = utils_functions.string2array(periods, integer=True)
    if not(0 in periods):
        periods.append(0)
    # on complète les profils des élèves.
    # Chaque élève aura un profil pour chaque période.
    # S'il n'en avait pas un d'attribué,
    # on lui met le profil standard du groupe
    # ou le profil par défaut (s'il n'y a pas de profil standard) :
    useDefaultProfil = False
    for id_eleve in students:
        if not(id_eleve in result['STUDENTS']):
            result['STUDENTS'][id_eleve] = {}
        for period in periods:
            if not(period in result['STUDENTS'][id_eleve]):
                if period in result['GROUP']:
                    id_profil = result['GROUP'][period]
                    result['STUDENTS'][id_eleve][period] = id_profil
        if len(result['STUDENTS'][id_eleve]) < 1:
            useDefaultProfil = True
            for period in periods:
                result['STUDENTS'][id_eleve][period] = result['DEFAULT']
        # cas du bilan annuel :
        if len(periods) > 2:
            for period in periods:
                if not(period in result['STUDENTS'][id_eleve]):
                    useDefaultProfil = True
                    result['STUDENTS'][id_eleve][period] = result['DEFAULT']
    # si le profil par défaut est un vrai profil,
    # on l'ajoute à la liste de tous les profils :
    if useDefaultProfil:
        id_profil = result['DEFAULT']
        if id_profil != -999:
            if not(id_profil in result['ALL']):
                result['ALL'].append(id_profil)
    return result


def getStudentsClasseTypesAndNotes(main, id_students):
    """
    retourne la liste des classeType correspondants à une liste d'élèves.
    """
    result = {'GROUP': [[], False]}
    query_commun = utils_db.query(main.db_commun)
    query_users = utils_db.query(main.db_users)
    # on récupère les classeType des classes dans la base commun :
    classes = {}
    commandLine_commun = utils_db.q_selectAllFrom.format('classes')
    query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
    while query_commun.next():
        className = query_commun.value(1)
        classType = int(query_commun.value(2))
        notes = (int(query_commun.value(3)) == 1)
        classes[className] = (classType, notes)
    # puis les classes des élèves dans la base users :
    commandLine_users = (
        'SELECT * FROM eleves '
        'WHERE id IN ({0})').format(id_students)
    query_users = utils_db.queryExecute(commandLine_users, query=query_users)
    while query_users.next():
        id_eleve = int(query_users.value(0))
        className = query_users.value(3)
        result[id_eleve] = classes[className]
        if not(classes[className][0] in result['GROUP'][0]):
            result['GROUP'][0].append(classes[className][0])
        if classes[className][1]:
            result['GROUP'][1] = True
    return result


def getItemsPeriods(main, id_groupe):
    """
    pour savoir le type d'organisation du prof (trimestre, année, mélange),
    on scrute un peu ses tableaux.
    retourne la proportion d'item en % pour chaque période.
    """
    result = {}
    for p in range(utils.NB_PERIODES):
        result[p] = 0
    total = 0
    query_my = utils_db.query(main.db_my)
    # d'abord les tableaux qui utilisent des modèles :
    commandLine_my = (
        'SELECT tableaux.id_tableau, tableaux.Periode, '
        'tableau_item.id_item '
        'FROM tableaux '
        'JOIN tableau_item AS modele ON modele.id_tableau=tableaux.id_tableau '
        'JOIN tableau_item ON tableau_item.id_tableau=modele.id_item '
        'JOIN items ON items.id_item=tableau_item.id_item '
        'WHERE tableaux.Public=1 AND modele.id_item<0 '
        'AND tableaux.id_groupe={0}').format(id_groupe)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        p = int(query_my.value(1))
        result[p] += 1
        total += 1
    # puis ceux qui n'utilisent pas de modèle :
    commandLine_my = (
        'SELECT tableaux.id_tableau, tableaux.Periode, '
        'tableau_item.id_item '
        'FROM tableaux '
        'JOIN tableau_item ON tableau_item.id_tableau=tableaux.id_tableau '
        'WHERE tableaux.Public=1 AND tableau_item.id_item>-1 '
        'AND tableaux.id_groupe={0}').format(id_groupe)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        p = int(query_my.value(1))
        result[p] += 1
        total += 1
    # arrivé là, result contient le nombre d'items pour chaque période
    # on le transforme en pourcentages :
    if total > 0:
        for p in range(utils.NB_PERIODES):
            result[p] = int(round(100 * result[p] / total))
    return result


def getBilansProfils(main):
    """
    retourne la liste des profils valables pour chaque bilan.
    """
    result = {}
    query_my = utils_db.query(main.db_my)
    commandLine_my = utils_db.q_selectAllFrom.format('profil_bilan_BLT')
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_profil = int(query_my.value(0))
        id_bilan = int(query_my.value(1))
        if not(id_bilan in result):
            result[id_bilan] = [id_profil]
        elif not(id_profil in result[id_bilan]):
            result[id_bilan].append(id_profil)
    return result


def getGroupBilans(main, id_groupe, periodes, 
                   groupClassTypes=[], profils=[], 
                   bilansProfils=[], onlyBulletin=False):
    """
    retourne la liste des bilans pour un groupe et une période donnée.
    On récupère d'abord les tableaux, puis les bilans évalués.
    Ensuite on trie les bilans (par type puis noms).
    """
    result = {'ORDER': []}
    query_my = utils_db.query(main.db_my)
    query_commun = utils_db.query(main.db_commun)

    # on récupère les classeType des compétences partagées :
    what = {'confidentiel': 'CONFIDENTIEL', 'referentiel': 'REFERENTIEL', 'bulletin': 'BULLETIN'}
    competences = {
        'CONFIDENTIEL': {'ORDER': []}, 
        'REFERENTIEL': {'ORDER': []}, 
        'BULLETIN': {'ORDER': []}}
    for table in what:
        commandLine_commun = 'SELECT code, classeType FROM {0} ORDER BY code'.format(table)
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            code = query_commun.value(0)
            classeType = int(query_commun.value(1))
            competences[what[table]]['ORDER'].append(code)
            if not(code in competences[what[table]]):
                competences[what[table]][code] = [classeType]
            else:
                competences[what[table]][code].append(classeType)

    # on regroupe les bilans par type :
    sortedBilans = {
        'ORDER': ['CONFIDENTIEL', 'REFERENTIEL', 'BULLETIN', 'PERSO_BLT', 'PERSO'], 
        'CONFIDENTIEL': [], 'REFERENTIEL': [], 'BULLETIN': [], 
        'PERSO_BLT': [], 'PERSO': []}
    dicoBilans = {}

    # on récupère uniquement les bilans pour lesquels des évaluations sont disponibles :
    # LEFT JOIN pour récupérer les bilans même sans synthèse de groupe
    commandLine_my = (
        'SELECT DISTINCT bilans.*, groupe_bilan.value, groupe_bilan.Periode '
        'FROM bilans '
        'JOIN evaluations ON evaluations.id_bilan=bilans.id_bilan '
        'LEFT JOIN groupe_bilan ON '
        '(groupe_bilan.id_bilan=bilans.id_bilan '
        'AND groupe_bilan.id_groupe={0} '
        'AND groupe_bilan.Periode IN (0, {1})) '
        'WHERE evaluations.id_tableau IN ({2}) '
        'AND bilans.id_competence>-2 '
        'ORDER BY groupe_bilan.Periode, bilans.Name').format(id_groupe, periodes[0], periodes[1])
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_bilan = int(query_my.value(0))
        bilanName = query_my.value(1)
        bilanLabel = query_my.value(2)
        id_competence = int(query_my.value(3))
        value = query_my.value(4)
        #period = int(query_my.value(5))
        if id_competence >= utils.decalageCFD:
            bilanType = 'CONFIDENTIEL'
        elif id_competence >= utils.decalageBLT:
            bilanType = 'BULLETIN'
        elif id_competence > -1:
            bilanType = 'REFERENTIEL'
            # un bilan du référentiel peut être mis sur la partie perso du bulletin :
            if id_bilan in bilansProfils:
                for profil in bilansProfils[id_bilan]:
                    if (profil in profils):
                        id_competence = -1
                        bilanType = 'PERSO_BLT'
                        classeTypes = [-1]
        else:
            bilanType = 'PERSO'
            classeTypes = [-1]
            if id_bilan in bilansProfils:
                for profil in bilansProfils[id_bilan]:
                    if (profil in profils):
                        bilanType = 'PERSO_BLT'
        # si le classeType d'une compétence partagée ne correspond pas,
        # on passe le bilan dans PERSO :
        if id_competence > -1:
            mustDo = False
            if bilanName in competences[bilanType]:
                classeTypes = competences[bilanType][bilanName]
                for classeType in classeTypes:
                    if (classeType == -1) or (classeType in groupClassTypes):
                        mustDo = True
            if not(mustDo):
                bilanType = 'PERSO'
                classeTypes = [-1]
        mustDo = True
        if (onlyBulletin) and not(bilanType in ('BULLETIN', 'PERSO_BLT')):
            mustDo = False
        if mustDo:
            if not(bilanName in dicoBilans):
                bilan2 = {
                    'Name': bilanName, 
                    'id_bilan': id_bilan, 
                    'Label': bilanLabel, 
                    'id_competence': id_competence, 
                    'type': bilanType, 
                    'classeTypes': classeTypes, 
                    'value': value}
                sortedBilans[bilanType].append(bilanName)
                dicoBilans[bilanName] = bilan2
            elif value != '':
                dicoBilans[bilanName]['value'] = value
    # on réordonne les bilans communs :
    for bilansList in sortedBilans['ORDER'][:3]:
        for bilanName in competences[bilansList]['ORDER']:
            if bilanName in sortedBilans[bilansList]:
                result[bilanName] = dicoBilans[bilanName]
                utils_functions.appendUnique(result['ORDER'], bilanName)
    # on réordonne les bilans persos du bulletin :
    persosText = ', '.join(
        utils_functions.u('"{0}"').format(bilanName) for bilanName in sortedBilans['PERSO_BLT'])
    commandLine_my = utils_functions.u(
        'SELECT DISTINCT bilans.Name '
        'FROM bilans '
        'JOIN profil_bilan_BLT ON profil_bilan_BLT.id_bilan=bilans.id_bilan '
        'JOIN profils ON profils.id_profil=profil_bilan_BLT.id_profil '
        'WHERE bilans.Name IN ({0}) '
        'ORDER BY profils.id_profil, profil_bilan_BLT.ordre').format(persosText)
    sortedBilans['PERSO_BLT'] = []
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        bilanName = query_my.value(0)
        sortedBilans['PERSO_BLT'].append(bilanName)
    # on inscrit les bilans persos dans les résultats :
    for bilansList in sortedBilans['ORDER'][3:]:
        for bilanName in sortedBilans[bilansList]:
            result[bilanName] = dicoBilans[bilanName]
            utils_functions.appendUnique(result['ORDER'], bilanName)
    return result


def getBilansValues(main, id_groupe, id_selection, id_students, id_bilans, 
                    annual=False, 
                    studentsProfils=[], bilansProfils=[], bulletin=[]):
    """
    calcul des valeurs à afficher.
    En cas de bilan annuel, on doit tenir compte des profils (car un bilan peut être évalué
    mais pas mis sur le bulletin chaque période)
    et on applique l'algorithme de calcul selon le type de prof.
    itemsPeriods donne la répartition des items selon les périodes
    et permet de choisir l'algorithme de calcul.
    """
    result = {}
    query_my = utils_db.query(main.db_my)
    if annual:
        itemsPeriods = getItemsPeriods(main, id_groupe)
    commandLine_my = (
        'SELECT * FROM evaluations '
        'WHERE id_tableau IN ({0}) '
        'AND id_eleve IN ({1}) '
        'AND id_bilan IN ({2}) '
        'ORDER BY id_tableau').format(id_selection, id_students, id_bilans)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_eleve = int(query_my.value(1))
        id_bilan = int(query_my.value(3))
        value = query_my.value(4)
        if not(id_eleve in result):
            result[id_eleve] = {}
        if annual:
            # on récupère la période :
            id_tableau = int(query_my.value(0))
            p = utils_functions.selection2pg(id_tableau)[0]
            if not(id_bilan in result[id_eleve]):
                result[id_eleve][id_bilan] = {}
                for i in range(utils.NB_PERIODES):
                    result[id_eleve][id_bilan][i] = ''
            # on vérifie les profils pour les bilans persos :
            mustDo = False
            studentProfil = studentsProfils[id_eleve][p]
            if id_bilan in bulletin:
                mustDo = True
            elif id_bilan in bilansProfils:
                if studentProfil in bilansProfils[id_bilan]:
                    mustDo = True
            if mustDo:
                result[id_eleve][id_bilan][p] = value
        else:
            result[id_eleve][id_bilan] = value
    if annual:
        for id_eleve in result:
            for id_bilan in result[id_eleve]:
                value = ''
                if itemsPeriods[0] > 0:
                    # on coefficiente en fonction de itemsPeriods :
                    for p in range(utils.NB_PERIODES):
                        v = result[id_eleve][id_bilan].get(p, '')
                        if v != '':
                            value += itemsPeriods[p] * v
                else:
                    for p in range(1, utils.NB_PERIODES):
                        v = result[id_eleve][id_bilan].get(p, '')
                        if v != '':
                            value += v
                value = utils_calculs.moyenneItem(value)
                result[id_eleve][id_bilan] = value
    return result


def getMoyennesAppreciations(main, id_groupe, matiereName, period, id_students):
    """
    """
    result = {}
    query_my = utils_db.query(main.db_my)
    # on ajoute l'id du groupe transformé (comme dans les tables) à la liste des élèves :
    id_groupeInTables = - id_groupe - 1
    if id_students == '':
        id_students = id_groupeInTables
    else:
        id_students = '{0}, {1}'.format(id_students, id_groupeInTables)
    # récupération des notes moyennes :
    commandLine_my = (
        'SELECT * FROM notes_values '
        'WHERE id_groupe={0} '
        'AND Periode={1} '
        'AND id_note=-1').format(id_groupe, period)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_eleve = int(query_my.value(3))
        value = query_my.value(4)
        result[id_eleve] = {'moyenne': value, 'appreciation': ''}
    # récupération des appréciations :
    commandLine_my = utils_db.qs_prof_appreciations.format(
        id_students, matiereName, period)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_eleve = int(query_my.value(0))
        value = query_my.value(1)
        if id_eleve in result:
            result[id_eleve]['appreciation'] = value
        else:
            result[id_eleve] = {'moyenne': '', 'appreciation': value}
    return result


def getPositionnements(main, id_groupe, matiereCode, id_students):
    """
    """
    result = {}
    query_my = utils_db.query(main.db_my)
    commandLine_my = (
        'SELECT * FROM lsu '
        'WHERE lsuWhat="positionnement" '
        'AND lsu1={0} '
        'AND lsu2="{1}" '
        'AND lsu3 IN ({2})').format(id_groupe, matiereCode, id_students)
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_eleve = int(query_my.value(3))
        value = query_my.value(4)
        result[id_eleve] = value
    return result


def calcBilans(main, id_groupe=-1, periode=-1, id_tableau=-1):
    """
    Calcule l'affichage de la vue bilans.
    La vue dépend du groupe et de la période sélectionnés.
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    try:
        # récupération de la sélection :
        data = diversFromSelection(main, id_tableau, id_groupe, periode)
        id_groupe, matiereName = data[0], data[2]
        periode, periodeToUse = data[3], data[7][0]

        # période et pg2selection :
        s = utils_functions.pg2selection(periodeToUse, id_groupe)
        periodes = ['{0}'.format(periode), '{0}'.format(s)]
        # liste des élèves :
        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        id_students = utils_functions.array2string(elevesInGroup, key=0)

        # récupération des profils des différents élèves :
        studentsAndGroupProfils = getStudentsAndGroupProfils(
            main, id_students, periodes[0], id_groupe)
        # et de leurs classeTypes :
        studentsClasseTypesNotes = getStudentsClasseTypesAndNotes(main, id_students)
        # récupération des bilans liés aux différents profils :
        bilansProfils = getBilansProfils(main)
        # récupération des bilans à afficher :
        bilans = getGroupBilans(
            main, id_groupe, periodes, 
            groupClassTypes=studentsClasseTypesNotes['GROUP'][0], 
            profils=studentsAndGroupProfils['ALL'], 
            bilansProfils=bilansProfils)
        id_bilans = ', '.join(
            '{0}'.format(bilans[bilan]['id_bilan']) for bilan in bilans['ORDER'])
        # on récupère les évaluations :
        data = getBilansValues(
            main, id_groupe, periodes[1], id_students, id_bilans)

        # on a maintenant les titres des colonnes :
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]
        lastBilanType = ''
        for bilanName in bilans['ORDER']:
            if bilans[bilanName]['type'] != lastBilanType:
                if lastBilanType != '':
                    columns.append(doSeparatorColumn())
                lastBilanType = bilans[bilanName]['type']
            id_bilan = bilans[bilanName]['id_bilan']
            bilanLabel = bilans[bilanName]['Label']
            classTypeName = utils_functions.bilanCode2ClassTypeName(main, bilanName)
            if classTypeName != '':
                text = QtWidgets.QApplication.translate('main', 'ClassesTypes')
                bilanLabel = utils_functions.u('{0}\n{1} : {2}').format(
                    bilanLabel, text, classTypeName)
            columns.append({
                'value': bilanName, 
                'id': id_bilan, 
                'toolTip': bilanLabel, 
                'datasType': utils.EVAL, 
                'vertical': True, 
                'bilanType': lastBilanType})

        for (id_eleve, ordre, eleveName) in elevesInGroup:
            # les listes des arguments pour chaque ligne (une liste par table) :
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]

            lastBilanType = ''
            column = 3
            for bilanName in bilans['ORDER']:
                if bilans[bilanName]['type'] != lastBilanType:
                    if lastBilanType != '':
                        row.append(doSeparatorColumn(column=False))
                        column += 1
                    lastBilanType = bilans[bilanName]['type']
                id_bilan = bilans[bilanName]['id_bilan']
                value = ''
                if id_eleve in data:
                    if (id_bilan in data[id_eleve]):
                        value = data[id_eleve][id_bilan]
                toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                    eleveName, columns[column]['value'], columns[column]['toolTip'])
                row.append({
                    'value': utils_functions.valeur2affichage(value), 
                    'color': utils_functions.findColor(value, mustCalculIfMany=False), 
                    'type': utils.EVAL, 
                    'bold': True, 
                    'toolTip': toolTip})
                column += 1
            # on ajoute la ligne de cet élève :
            rows.append(row)

        # on ajoute la ligne du groupe :
        groupeName = QtWidgets.QApplication.translate('main', 'GROUP')
        ordre = 999
        row = [
            {'value': - id_groupe - 1, 'type': utils.INTEGER, 'groupRow': True}, 
            {'value': ordre, 'type': utils.INTEGER}, 
            {'value': groupeName, 'type': utils.LINE, 'bold': True, 'align': 'center'}]
        lastBilanType = ''
        column = 3
        for bilanName in bilans['ORDER']:
            if bilans[bilanName]['type'] != lastBilanType:
                if lastBilanType != '':
                    row.append(doSeparatorColumn(column=False))
                    column += 1
                lastBilanType = bilans[bilanName]['type']
            value = ''
            if bilanName in bilans:
                value = bilans[bilanName]['value']
            toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                groupeName, columns[column]['value'], columns[column]['toolTip'])
            row.append({
                'value': utils_functions.valeur2affichage(value), 
                'color': utils_functions.findColor(value, mustCalculIfMany=False), 
                'type': utils.EVAL, 
                'bold': True, 
                'toolTip': toolTip})
            column += 1
        rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcBulletin(main, id_tableau=-1):
    """
    calcul de ce qui doit être affiché dans le bulletin.
    Pour les bilans du bulletin, on doit tenir compte des types de classes.
    Pour les bilans persos, on doit aussi gérer les profils.
    Enfin on distingue la période "bilan annuel" (999) qui est plus difficile à gérer.
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        editableFlag = ''
    else:
        editableFlag = 'EDITABLE'
    try:
        # récupération de la sélection :
        data = diversFromSelection(main, id_tableau=id_tableau)
        id_groupe, matiereName = data[0], data[2]
        periode, periodeToUse = data[3], data[7][0]
        matiereCode = main.me['MATIERES']['Matiere2Code'].get(matiereName, matiereName)

        # on distingue la période "bilan annuel" des autres :
        annual = (periode == 999)
        if annual:
            periodesDic = {}
            for p in range(utils.NB_PERIODES):
                s = utils_functions.pg2selection(p, id_groupe)
                periodesDic[p] = s
            periodes = [
                ', '.join('{0}'.format(e) for e in periodesDic),
                ', '.join('{0}'.format(periodesDic[e]) for e in periodesDic)]
        else:
            s = utils_functions.pg2selection(periodeToUse, id_groupe)
            periodes = ['{0}'.format(periode), '{0}'.format(s)]
        # liste des élèves :
        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe, old=-2)
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        # récupération des profils des différents élèves :
        studentsAndGroupProfils = getStudentsAndGroupProfils(
            main, id_students, periodes[0], id_groupe)
        # et de leurs classeTypes :
        studentsClasseTypesNotes = getStudentsClasseTypesAndNotes(main, id_students)
        # récupération des bilans liés aux différents profils :
        bilansProfils = getBilansProfils(main)
        # récupération des bilans à afficher :
        bilans = getGroupBilans(
            main, id_groupe, periodes, 
            groupClassTypes=studentsClasseTypesNotes['GROUP'][0], 
            profils=studentsAndGroupProfils['ALL'], 
            bilansProfils=bilansProfils, 
            onlyBulletin=True)
        id_bilans = ', '.join(
            '{0}'.format(bilans[bilan]['id_bilan']) for bilan in bilans['ORDER'])
        # liste des bilans de la partie partagée du bulletin :
        bulletin = []
        if annual:
            for bilan in bilans['ORDER']:
                if bilans[bilan]['type'] == 'BULLETIN':
                    bulletin.append(bilans[bilan]['id_bilan'])
            positionnements = getPositionnements(main, id_groupe, matiereCode, id_students)
        # on récupère les évaluations :
        data = getBilansValues(
            main, id_groupe, periodes[1], id_students, id_bilans, 
            annual=annual, 
            studentsProfils=studentsAndGroupProfils['STUDENTS'], 
            bilansProfils=bilansProfils, 
            bulletin=bulletin)
        data2 = getMoyennesAppreciations(
            main, id_groupe, matiereName, periode, id_students)
        notes = False
        for who in studentsClasseTypesNotes:
            if studentsClasseTypesNotes[who][1]:
                notes = True

        # on a maintenant les titres des colonnes :
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]
        lastBilanType = ''
        for bilanName in bilans['ORDER']:
            if bilans[bilanName]['type'] != lastBilanType:
                if lastBilanType != '':
                    columns.append(doSeparatorColumn())
                lastBilanType = bilans[bilanName]['type']
            id_bilan = bilans[bilanName]['id_bilan']
            bilanLabel = bilans[bilanName]['Label']
            classTypeName = utils_functions.bilanCode2ClassTypeName(main, bilanName)
            if classTypeName != '':
                text = QtWidgets.QApplication.translate('main', 'ClassesTypes')
                bilanLabel = utils_functions.u('{0}\n{1} : {2}').format(
                    bilanLabel, text, classTypeName)
            columns.append({
                'value': bilanName, 
                'id': id_bilan, 
                'toolTip': bilanLabel, 
                'datasType': utils.EVAL, 
                'vertical': True, 
                'bilanType': lastBilanType})
        if notes:
            noteColumn = QtWidgets.QApplication.translate('main', 'Average')
            columns.append({
                'value': noteColumn, 
                'id': -2, 
                'datasType': utils.NUMBER})
        # pour un bilan annuel, on ajoute le positionnement :
        if annual:
            columns.append(doSeparatorColumn())
            positioningText = QtWidgets.QApplication.translate('main', 'Positioning')
            columns.append({
                'value': positioningText, 
                'id': '{0}|{1}'.format(id_groupe, matiereCode), 
                'toolTip': QtWidgets.QApplication.translate(
                    'main', 
                    'can be validated'), 
                'datasType': utils.EVAL, 
                'vertical': True, 
                'bilanType': 'POSITIONNEMENT'})
        # on termine avec la colonne des appréciations :
        appreciationText = QtWidgets.QApplication.translate('main', 'Opinion')
        columns.append({
            'value': appreciationText, 
            'appreciationColumn': True, 
            'columnWidth': '10.0cm'})

        for (id_eleve, ordre, eleveName) in elevesInGroup:
            # les listes des arguments pour chaque ligne (une liste par table) :
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]

            annualValues = ''
            lastBilanType = ''
            column = 3
            for bilanName in bilans['ORDER']:
                bilan = bilans[bilanName]
                if bilan['type'] != lastBilanType:
                    if lastBilanType != '':
                        row.append(doSeparatorColumn(column=False))
                        column += 1
                    lastBilanType = bilan['type']
                id_bilan = bilan['id_bilan']
                value = ''
                if id_eleve in data:
                    if (id_bilan in data[id_eleve]):
                        value = data[id_eleve][id_bilan]
                # on vérifie les profils :
                mustDo = False
                try:
                    if bilan['type'] == 'PERSO_BLT':
                        for studentProfil in studentsAndGroupProfils['STUDENTS'][id_eleve].values():
                            if studentProfil in bilansProfils[id_bilan]:
                                mustDo = True
                    elif -1 in bilan['classeTypes']:
                        mustDo = True
                    elif studentsClasseTypesNotes[id_eleve][0] in bilan['classeTypes']:
                        mustDo = True
                except:
                    pass
                if not(mustDo):
                    value = ''
                toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                    eleveName, columns[column]['value'], columns[column]['toolTip'])
                row.append({
                    'value': utils_functions.valeur2affichage(value), 
                    'color': utils_functions.findColor(value, mustCalculIfMany=False), 
                    'type': utils.EVAL, 
                    'bold': True, 
                    'toolTip': toolTip})
                annualValues += value
                column += 1
            # on met la moyenne s'il y a des notes :
            if notes:
                if id_eleve in data2:
                    row.append({
                        'value': data2[id_eleve]['moyenne'], 
                        'color': utils.colorLightGray, 
                        'type': utils.NUMBER, 
                        'bold': True})
                else:
                    row.append({
                        'value': '', 
                        'color': utils.colorLightGray, 
                        'type': utils.INDETERMINATE})
            # pour un bilan annuel, on ajoute le positionnement :
            if annual:
                row.append(doSeparatorColumn(column=False))
                column += 1
                value = positionnements.get(id_eleve, False)
                calculatedValue = utils_calculs.moyenneBilan(annualValues)
                if value == False:
                    value = calculatedValue
                    toolTip = QtWidgets.QApplication.translate(
                        'main', 'calculated by VERAC.')
                    bold = False
                else:
                    toolTip = QtWidgets.QApplication.translate(
                        'main', 'validated.')
                    bold = True
                toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                    eleveName, positioningText, toolTip)
                cell = {
                    'value': utils_functions.valeur2affichage(value), 
                    'calculatedValue': calculatedValue, 
                    'color': utils_functions.findColor(value, mustCalculIfMany=False), 
                    'type': utils.EVAL, 
                    'toolTip': toolTip, 
                    'flag': editableFlag}
                if bold:
                    cell['bold'] = True
                row.append(cell)
            # et l'appréciation pour finir :
            appreciation = ''
            if id_eleve in data2:
                appreciation = data2[id_eleve]['appreciation']
            row.append({
                'value': appreciation, 
                'type': utils.TEXT, 
                'flag': editableFlag})
            # on ajoute la ligne de cet élève :
            rows.append(row)

        # on ajoute la ligne du groupe :
        groupeName = QtWidgets.QApplication.translate('main', 'GROUP')
        ordre = 999
        row = [
            {'value': - id_groupe - 1, 'type': utils.INTEGER, 'groupRow': True}, 
            {'value': ordre, 'type': utils.INTEGER}, 
            {'value': groupeName, 'type': utils.LINE, 'bold': True, 'align': 'center'}]
        annualValues = ''
        lastBilanType = ''
        column = 3
        for bilanName in bilans['ORDER']:
            if bilans[bilanName]['type'] != lastBilanType:
                if lastBilanType != '':
                    row.append(doSeparatorColumn(column=False))
                    column += 1
                lastBilanType = bilans[bilanName]['type']
            value = ''
            if bilanName in bilans:
                value = bilans[bilanName]['value']
            toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                groupeName, columns[column]['value'], columns[column]['toolTip'])
            row.append({
                'value': utils_functions.valeur2affichage(value), 
                'color': utils_functions.findColor(value, mustCalculIfMany=False), 
                'type': utils.EVAL, 
                'bold': True, 
                'toolTip': toolTip})
            annualValues += value
            column += 1
        id_groupeInTables = -id_groupe - 1
        if notes:
            if (id_groupeInTables in data2):
                value = data2[id_groupeInTables]['moyenne']
                row.append({
                    'value': value, 
                    'color': utils.colorLightGray, 
                    'type': utils.NUMBER, 
                    'bold': True})
            else:
                row.append({
                    'value': '', 
                    'color': utils.colorLightGray, 
                    'type': utils.INDETERMINATE})
        # pour un bilan annuel, on ajoute le positionnement :
        if annual:
            row.append(doSeparatorColumn(column=False))
            column += 1
            value = utils_calculs.moyenneBilan(annualValues)
            row.append({
                'value': utils_functions.valeur2affichage(value), 
                'color': utils_functions.findColor(value, mustCalculIfMany=False), 
                'type': utils.EVAL, 
                'bold': True, 
                'toolTip': ''})
        appreciation = ''
        if (id_groupeInTables in data2):
            appreciation = data2[id_groupeInTables]['appreciation']
        row.append({
            'value': appreciation, 
            'type': utils.TEXT, 
            'bold': True, 
            'flag': editableFlag})
        rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcAppreciations(main, id_tableau=-1):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        editableFlag = ''
    else:
        editableFlag = 'EDITABLE'
    try:
        query_my = utils_db.query(main.db_my)

        # récupération de la sélection :
        data = diversFromSelection(main, id_tableau=id_tableau)
        id_groupe, matiereName, periode = data[0], data[2], data[3]

        # on récupère la liste des élèves :
        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe, old=-2)

        # mise en place des colonnes :
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]
        for periode in range(utils.NB_PERIODES):
            columns.append({
                'value': utils.PERIODES[periode], 
                'columnWidth': '7.0cm'})
        columns.append({
            'value': utils.PERIODES[999], 
            'columnWidth': '7.0cm'})
        # on identifie la période courante :
        if utils.selectedPeriod < 999:
            appreciationColumn = 3 + utils.selectedPeriod
        else:
            appreciationColumn = 3 + utils.NB_PERIODES
        columns[appreciationColumn]['columnWidth'] = '10.0cm'
        columns[appreciationColumn]['appreciationColumn'] = True

        # remplissage du tableau :
        listPeriodes = []
        for periode in range(utils.NB_PERIODES):
            listPeriodes.append(periode)
        listPeriodes.append(999)

        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        id_periods = utils_functions.array2string(listPeriodes)
        commandLine_appreciation = utils_db.qs_prof_appreciations.format(
            id_students, matiereName, id_periods)
        query_my = utils_db.queryExecute(commandLine_appreciation, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(0))
            appreciation = query_my.value(1)
            periode = int(query_my.value(3))
            valuesDic[(id_eleve, periode)] = appreciation
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            for periode in listPeriodes:
                appreciation = ''
                if (id_eleve, periode) in valuesDic:
                    appreciation = valuesDic[(id_eleve, periode)]
                if periode == utils.selectedPeriod:
                    row.append({
                        'value': appreciation, 
                        'type': utils.TEXT, 
                        'flag': editableFlag})
                else:
                    row.append({
                        'value': appreciation, 
                        'color': utils.colorLightGray, 
                        'type': utils.TEXT})
            # on peut ajouter la ligne :
            rows.append(row)
        # on ajoute la ligne "groupe" (appréciations sur le groupe) :
        groupeName = QtWidgets.QApplication.translate('main', 'GROUP')
        ordre = 999
        row = [
            {'value': - id_groupe - 1, 'type': utils.INTEGER, 'groupRow': True}, 
            {'value': ordre, 'type': utils.INTEGER}, 
            {'value': groupeName, 'type': utils.LINE, 'bold': True, 'align': 'center'}]
        for periode in listPeriodes:
            appreciation = ''
            commandLine_appreciation = utils_db.qs_prof_appreciations.format(
                - id_groupe - 1, matiereName, periode)
            query_my = utils_db.queryExecute(commandLine_appreciation, query=query_my)
            while query_my.next():
                appreciation = query_my.value(1)
            if periode == utils.selectedPeriod:
                row.append({
                    'value': appreciation, 
                    'type': utils.TEXT, 
                    'bold': True, 
                    'flag': editableFlag})
            else:
                row.append({
                    'value': appreciation, 
                    'color': utils.colorLightGray, 
                    'type': utils.TEXT, 
                    'bold': True})
        rows.append(row)
    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcEleve(main, id_eleve, id_tableau=-1, id_sens=-1, id_groupe=-1):
    """
    """
    idsToExpand = []
    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        editableFlag = ''
    else:
        editableFlag = 'EDITABLE'
    if main.viewType == utils.VIEW_ELEVE:
        for row in main.model.rows:
            cellValue = row[3].get('value', '')
            if cellValue == 'COLLAPSE':
                idToExpand = row[0].get('value', '')
                if idToExpand != '':
                    idsToExpand.append(idToExpand)

    # les titres des colonnes de la vue élève :
    titleColumnEleve = (
        (QtWidgets.QApplication.translate('main', 'Name'),
            utils_functions.u('{0}').format(
                QtWidgets.QApplication.translate('main', 'Name of Item or Balance'))),
        (QtWidgets.QApplication.translate('main', 'Label'),
            utils_functions.u('{0}').format(
                QtWidgets.QApplication.translate('main', 'Label of Item or Balance'))),
        (QtWidgets.QApplication.translate('main', 'Value'),
            utils_functions.u('{0}').format(
                QtWidgets.QApplication.translate('main', 'Value of Item or Balance'))),
        )
    utils_functions.doWaitCursor()
    columns = [
        {'value': 'id_truc', 'NO_EXPORT': True}, 
        {'value': 'ordre', 'NO_EXPORT': True}, 
        {'value': '', 'columnWidth': '1.0cm', 'NO_EXPORT': True},
        {'value': '', 'NO_EXPORT': True}]
    for column in titleColumnEleve:
        columns.append({'value': column[0], 'toolTip': column[1]})
    columns[5]['columnWidth'] = '10.0cm'
    rows = []
    if id_sens < 0:
        id_sens = main.id_sens
    try:
        query_my = utils_db.query(main.db_my)

        # récupération de la sélection :
        data = diversFromSelection(main, id_tableau=id_tableau, id_groupe=id_groupe)
        id_groupe, matiereName = data[0], data[2]
        periode, periodeToUse = data[3], data[7][0]

        # on calcule le faux id_tableau (id_selection) pour lire les données :
        id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)

        # Si id_eleve n'est pas donné, on récupère le premier élève du groupe :
        if id_eleve == -1:
            id_eleve = listStudentsInGroup(main, id_groupe=id_groupe)[0][0]

        # on récupère tous les tableaux du groupe pour la période considérée
        tableaux = prof_groupes.tableauxFromGroupe(main, id_groupe, periode)

        # on récupère les comptages liés à des items des élèves du groupe :
        counts = countsFromGroup(main, id_groupe, id_eleve=id_eleve, period=periode)
        count_items = utils_functions.array2string(counts['ITEMS'])

        # récupération des items :
        itemDatas = {'ITEMS_NAMES': []}
        idsItems = []
        for id_tableau in tableaux:
            id_template = diversFromSelection(main, id_tableau=id_tableau)[8]
            if id_template == 0:
                id_template = id_tableau
            commandLine_my = utils_db.qs_prof_EleveItems.format(id_tableau, id_eleve, id_template)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                itemName = query_my.value(1)
                itemLabel = query_my.value(2)
                itemValue = query_my.value(3)
                id_bilan = int(query_my.value(4))
                bilanName = query_my.value(5)
                utils_functions.appendUnique(itemDatas['ITEMS_NAMES'], itemName)
                if itemName in itemDatas:
                    itemData = itemDatas[itemName]
                    if not(id_tableau in itemData[4]):
                        itemData[3] += itemValue
                    utils_functions.appendUnique(itemData[4], id_tableau)
                    utils_functions.appendUnique(itemData[5], bilanName)
                else:
                    itemDatas[itemName] = [
                        id_item, itemName, itemLabel, itemValue, [id_tableau], [bilanName]]
                utils_functions.appendUnique(idsItems, id_item)
        # on ajoute les items évalués par comptage
        # (la liste countItemsAdded sert à ne récupérer la valeur qu'une fois) :
        countItemsAdded = []
        commandLine_my = (
            'SELECT DISTINCT items.*, bilans.* '
            'FROM items '
            'JOIN item_bilan ON item_bilan.id_item=items.id_item '
            'JOIN bilans ON bilans.id_bilan=item_bilan.id_bilan '
            'WHERE items.id_item IN ({0}) '
            'AND bilans.id_competence>-2').format(count_items)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            itemValue = counts['VALUES'].get(id_item, '')
            id_bilan = int(query_my.value(3))
            bilanName = query_my.value(4)
            utils_functions.appendUnique(itemDatas['ITEMS_NAMES'], itemName)
            if itemName in itemDatas:
                itemData = itemDatas[itemName]
                if not(id_item in countItemsAdded):
                    itemData[3] += itemValue
                utils_functions.appendUnique(itemData[5], bilanName)
            else:
                itemDatas[itemName] = [
                    id_item, itemName, itemLabel, itemValue, [], [bilanName]]
            utils_functions.appendUnique(countItemsAdded, id_item)
            utils_functions.appendUnique(idsItems, id_item)
        id_items = utils_functions.array2string(idsItems)
        itemDatas['ITEMS_NAMES'].sort()
        # on ajoute les bilans non-calculés :
        bilansNoCalcDatas = {'BILANS_NAMES': []}
        commandLine_my = utils_db.qs_prof_BilansNoCalcFromItems.format(id_items)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            bilanName = query_my.value(1)
            bilanLabel = query_my.value(2)
            itemName = query_my.value(4)
            utils_functions.appendUnique(itemDatas[itemName][5], bilanName)
            utils_functions.appendUnique(bilansNoCalcDatas['BILANS_NAMES'], bilanName)
            if bilanName in bilansNoCalcDatas:
                utils_functions.appendUnique(bilansNoCalcDatas[bilanName][2], (itemName, 1))
            else:
                bilansNoCalcDatas[bilanName] = (id_bilan, bilanLabel, [(itemName, 1), ])
        bilansNoCalcDatas['BILANS_NAMES'].sort()

        # on récupère la liste des bilans persos du bulletin (pour les séparer) :
        bilansBLT = []
        studentsAndGroupProfils = getStudentsAndGroupProfils(
            main, id_eleve, periode, id_groupe, matiereName=matiereName)
        eleveProfilsDic = studentsAndGroupProfils['STUDENTS'][id_eleve]
        id_profils = ', '.join(
            '{0}'.format(eleveProfilsDic[profilPeriode]) for profilPeriode in eleveProfilsDic)
        commandLine_my = utils_db.qs_prof_profilBilans.format(id_profils)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            utils_functions.appendUnique(bilansBLT, id_bilan)

        # récupération des bilans :
        bilanDatas = {'BILANS_NAMES': [[], [], [], [], []]}
        # pour trier les bilans selon 5 types
        # (communs du bulletin, persos du bulletin, persos autres, référentiels, confidentiels)
        listBilansTri = [[], [], [], [], []]
        commandLine_my = utils_db.qs_prof_EleveBilans.format(
            id_selection, id_eleve)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            bilanName = query_my.value(1)
            bilanLabel = query_my.value(2)
            id_competence = int(query_my.value(3))
            bilanValue = query_my.value(4)
            id_item = int(query_my.value(5))
            coeff = int(query_my.value(6))
            itemName = query_my.value(7)
            # on place le nom du bilan dans la bonne liste :
            if id_competence > utils.decalageCFD:
                # confidentiels :
                utils_functions.appendUnique(bilanDatas['BILANS_NAMES'][4], bilanName)
            elif id_competence > utils.decalageBLT:
                # communs du bulletin :
                utils_functions.appendUnique(bilanDatas['BILANS_NAMES'][0], bilanName)
            elif id_competence > 0:
                # référentiels :
                utils_functions.appendUnique(bilanDatas['BILANS_NAMES'][3], bilanName)
            else:
                if id_bilan in bilansBLT:
                    # persos du bulletin :
                    utils_functions.appendUnique(bilanDatas['BILANS_NAMES'][1], bilanName)
                else:
                    # persos autres :
                    utils_functions.appendUnique(bilanDatas['BILANS_NAMES'][2], bilanName)
            try:
                bilanData = bilanDatas[bilanName]
                if bilanData[3] == '':
                    bilanData[3] = bilanValue
                utils_functions.appendUnique(bilanData[4], (itemName, coeff))
            except:
                bilanDatas[bilanName] = [
                    id_bilan, bilanName, bilanLabel, bilanValue, [(itemName, coeff), ]]
        # on peut remplir la liste bilanDatas['BILANS_NAMES'] :
        for i in range(5):
            bilanDatas['BILANS_NAMES'][i].sort()
        # on ajoute les bilans non-calculés :
        for bilanName in bilansNoCalcDatas['BILANS_NAMES']:
            utils_functions.appendUnique(bilanDatas['BILANS_NAMES'][2], bilanName)
            (id_bilan, bilanLabel, items) = bilansNoCalcDatas[bilanName]
            bilanDatas[bilanName] = [
                id_bilan, bilanName, bilanLabel, '', items]

        separatorRow = [
            {'value': -1, 'color': utils.colorGray, 'type': utils.NUMBER, 'flag': 'NOCHANGE'},
            {'value': -1, 'color': utils.colorGray, 'type': utils.NUMBER, 'flag': 'NOCHANGE'},
            {'value': '', 'color': utils.colorGray, 'type': utils.LINE, 'flag': 'NOCHANGE'},
            {'value': '', 'color': utils.colorGray, 'type': utils.INDETERMINATE, 'flag': 'NOCHANGE'},
            {'value': '', 'color': utils.colorGray, 'type': utils.LINE, 'flag': 'NOCHANGE'},
            {'value': 'HSEPARATOR', 'color': utils.colorGray, 'type': utils.LINE, 'flag': 'NOCHANGE'},
            {'value': '', 'color': utils.colorGray, 'type': utils.LINE, 'flag': 'NOCHANGE'}]

        if id_sens in (0, 2):
            # affichage Bilans → Items :
            ordre = -1
            colors = {
                0: 'BULLETIN', 
                1: 'PERSO_BLT', 
                2: 'PERSO', 
                3: 'REFERENTIEL', 
                4: 'CONFIDENTIEL'}
            for bilanGenre in range(5):
                for bilanName in bilanDatas['BILANS_NAMES'][bilanGenre]:
                    ordre += 1
                    bilanRow = []
                    id_bilan = bilanDatas[bilanName][0]
                    bilanLabel = bilanDatas[bilanName][2]
                    bilanValue = bilanDatas[bilanName][3]
                    bilanRow.append({'value': id_bilan, 'type': utils.NUMBER})
                    bilanRow.append({'value': ordre, 'type': utils.NUMBER})
                    bilanRow.append({'value': '', 'type': utils.LINE})
                    if id_bilan in idsToExpand:
                        expandMode = 'COLLAPSE'
                    else:
                        expandMode = 'EXPAND'
                    bilanRow.append({
                        'value': expandMode, 
                        'type': utils.LINE, 
                        'align': 'center', 
                        'bold': True})
                    bilanRow.append({
                        'value': bilanName, 
                        'type': utils.LINE, 
                        'bold': True, 
                        'genre':bilanGenre, 
                        'text-color': utils.colorsBilans[colors[bilanGenre]]})
                    bilanRow.append({
                        'value': bilanLabel, 
                        'type': utils.LINE, 
                        'bold': True})
                    bilanRow.append({
                        'value': utils_functions.valeur2affichage(bilanValue), 
                        'color': utils_functions.findColor(bilanValue, mustCalculIfMany=False), 
                        'type': utils.EVAL,
                        'bold': True})
                    rows.append(separatorRow)
                    rows.append(bilanRow)
                    # on affiche les items liés (et évalués) :
                    items4Bilan = bilanDatas[bilanName][4]
                    items4Bilan.sort()
                    for (itemName, coeff) in items4Bilan:
                        ordre += 1
                        id_item = -1
                        itemLabel = ''
                        itemValue = ''
                        if itemName in itemDatas:
                            id_item = itemDatas[itemName][0]
                            itemLabel = itemDatas[itemName][2]
                            itemValue = itemDatas[itemName][3]
                        if itemValue != '':
                            itemRow = []
                            itemRow.append({'value': id_item, 'type': utils.NUMBER})
                            itemRow.append({'value': ordre, 'type': utils.NUMBER})
                            itemRow.append({'value': '', 'type': utils.LINE})
                            itemRow.append({
                                'value': '', 
                                'type': utils.INDETERMINATE, 
                                'child': True, 
                                'expand': (expandMode == 'COLLAPSE')})
                            itemRow.append({'value': itemName, 'type': utils.LINE})
                            itemRow.append({'value': itemLabel, 'type': utils.LINE})
                            affichage = utils_functions.valeur2affichage(itemValue) * coeff
                            itemRow.append({
                                'value': affichage, 
                                'color': utils_functions.findColor(itemValue), 
                                'type': utils.EVAL})
                            rows.append(itemRow)
        else:
            # affichage Items → Bilans :
            ordre = -1
            for itemName in itemDatas['ITEMS_NAMES']:
                ordre += 1
                itemRow = []
                id_item = itemDatas[itemName][0]
                itemLabel = itemDatas[itemName][2]
                itemValue = itemDatas[itemName][3]
                itemRow.append({'value': id_item, 'type': utils.NUMBER})
                itemRow.append({'value': ordre, 'type': utils.NUMBER})
                itemRow.append({'value': '', 'type': utils.LINE})
                if id_item in idsToExpand:
                    expandMode = 'COLLAPSE'
                else:
                    expandMode = 'EXPAND'
                itemRow.append({'value': expandMode, 'type': utils.LINE, 'align': 'center', 'bold': True})
                itemRow.append({'value': itemName, 'type': utils.LINE, 'bold': True})
                itemRow.append({'value': itemLabel, 'type': utils.LINE, 'bold': True})
                itemRow.append({
                    'value': utils_functions.valeur2affichage(itemValue), 
                    'color': utils_functions.findColor(itemValue), 
                    'type': utils.EVAL,
                    'bold': True})
                rows.append(separatorRow)
                rows.append(itemRow)
                # on affiche les bilans liés :
                bilanNames4Item = itemDatas[itemName][5]
                bilanNames4Item.sort()
                for bilanName in bilanNames4Item:
                    ordre += 1
                    id_bilan = -1
                    bilanLabel = ''
                    bilanValue = ''
                    if bilanName in bilanDatas:
                        id_bilan = bilanDatas[bilanName][0]
                        bilanLabel = bilanDatas[bilanName][2]
                        bilanValue = bilanDatas[bilanName][3]
                    bilanRow = []
                    bilanRow.append({'value': id_bilan, 'type': utils.NUMBER})
                    bilanRow.append({'value': ordre, 'type': utils.NUMBER})
                    bilanRow.append({'value': '', 'type': utils.LINE})
                    bilanRow.append({
                        'value': '', 
                        'type': utils.INDETERMINATE, 
                        'child': True, 
                        'expand': (expandMode == 'COLLAPSE')})
                    bilanRow.append({'value': bilanName, 'type': utils.LINE})
                    bilanRow.append({'value': bilanLabel, 'type': utils.LINE})
                    bilanRow.append({
                        'value': utils_functions.valeur2affichage(bilanValue), 
                        'color': utils_functions.findColor(bilanValue, mustCalculIfMany=False), 
                        'type': utils.EVAL})
                    rows.append(bilanRow)

        if id_sens > 1:
            # récupération des appréciations précédentes :
            for p in range(1, utils.NB_PERIODES):
                main.accordion[p][2].setText('')
                if p < utils.selectedPeriod:
                    main.accordion[p][1].setEnabled(True)
                else:
                    main.accordion[p][1].setEnabled(False)
                    main.accordion[p][1].setCheckState(QtCore.Qt.Unchecked)
                    main.accordion[p][2].setVisible(False)
            if utils.selectedPeriod > 0:
                for p in range(1, utils.selectedPeriod):
                    appreciation = ''
                    commandLine_my = utils_db.qs_prof_appreciations.format(
                        id_eleve, matiereName, p)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    while query_my.next():
                        appreciation = query_my.value(1)
                    main.accordion[p][2].setText(appreciation)
                    main.accordion[p][2].setMaximumHeight(80)
                    main.accordion[p][2].updateGeometry()
                main.accordionSplitterMoved()

            # on affiche l'appréciation en cours :
            appreciation = ''
            commandLine_my = utils_db.qs_prof_appreciations.format(
                id_eleve, matiereName, periode)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                appreciation = query_my.value(1)
            main.me['actualAppreciation'] = appreciation
            main.editAppreciation.setText(appreciation)
            main.editAppreciationGroupBox.setVisible(True)
            main.editAppreciation.setReadOnly(
                utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False))
        else:
            main.editAppreciationGroupBox.setVisible(False)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def doAppreciationChanged(main):
    """
    appellée chaque fois qu'on modifie l'appréciation
    mais on n'enregistre qu'à la fin
    """
    appreciation = main.editAppreciation.toPlainText()
    if appreciation != main.me.get('actualAppreciation', ''):
        main.me['actualAppreciation'] = appreciation
        main.me['mustSaveActualAppreciation'] = True


def testAppreciationMustBeSaved(main):
    """
    pour tester s'il faut enregistrer l'appréciation
    """
    if not(main.me.get('mustSaveActualAppreciation', False)):
        return False
    if main.viewType in utils.VIEWS_WITH_APPRECIATIONS:
        if main.viewType == utils.VIEW_SUIVIS:
            return False
        elif len(main.view.selectedIndexes()) < 1:
            return False
        # récupération des données :
        row = main.view.selectedIndexes()[-1].row()
        id_eleve = main.model.rows[row][0]['value']
    elif main.viewType == utils.VIEW_ELEVE:
        # lastEleve permet d'enregistrer les modifs de l'appréciation
        # surtout en changeant d'élève par la combobox
        if main.lastEleve > -1:
            id_eleve = main.lastEleve
        else:
            id_eleve = main.changeEleveComboBox.itemData(
                main.changeEleveComboBox.currentIndex(), QtCore.Qt.UserRole)
    changeAppreciation(
        main, id_eleve, main.me['actualAppreciation'])
    main.whatIsModified['actualTableau'] = True
    main.me['mustSaveActualAppreciation'] = False
    return True


def expandCollapseRows(main, beginRow):
    theCell = main.model.rows[beginRow][3]
    cellValue = theCell.get('value', '')
    childrens = []
    row = beginRow
    child = True
    while child:
        row += 1
        try:
            child = main.model.rows[row][3].get('child', False)
            if child:
                childrens.append(row)
        except:
            child = False
    if cellValue == 'EXPAND':
        theCell['value'] = 'COLLAPSE'
        for row in childrens:
            main.view.setRowHidden(row, False)
    elif cellValue == 'COLLAPSE':
        theCell['value'] = 'EXPAND'
        for row in childrens:
            main.view.setRowHidden(row, True)


def calcNotes(main):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        editableFlag = ''
    else:
        editableFlag = 'EDITABLE'
    try:
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]
        calculatedNoteModeItems = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModeItems')
        calculatedNoteModeBilans = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModeBilans')
        calculatedNoteModePersos = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModePersos')
        calculatedNoteModeBLT = QtWidgets.QApplication.translate(
            'main', 'CalculatedNoteModeBLT')

        # récupération de la sélection :
        data = diversFromSelection(main)
        id_groupe, matiereName = data[0], data[2]
        periode, periodeToUse = data[3], data[7][0]

        commandLine_my = utils_db.qs_prof_notes.format(id_groupe, periode)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            id_note = int(query_my.value(2))
            name = query_my.value(3)
            base = int(query_my.value(4))
            coeff = float(query_my.value(5))
            calcMode = int(query_my.value(6))
            noteLabel = utils_functions.u('/{0} coeff {1}').format(base, coeff)
            if calcMode == 0:
                noteLabel = utils_functions.u('{0}\n{1}').format(noteLabel, calculatedNoteModeItems)
            elif calcMode == 1:
                noteLabel = utils_functions.u('{0}\n{1}').format(noteLabel, calculatedNoteModeBilans)
            elif calcMode == 2:
                noteLabel = utils_functions.u('{0}\n{1}').format(noteLabel, calculatedNoteModePersos)
            elif calcMode == 3:
                noteLabel = utils_functions.u('{0}\n{1}').format(noteLabel, calculatedNoteModeBLT)
            columns.append({
                'value': name, 
                'id': id_note, 
                'toolTip': noteLabel, 
                'datasType': utils.NUMBER, 
                'other': (id_groupe, periode, id_note, name, base, coeff, calcMode)})
        name = QtWidgets.QApplication.translate('main', 'Average')
        columns.append({
            'value': name, 
            'datasType': utils.NUMBER, 
            'bold': True, 
            'other': (id_groupe, periode, -1, name, 20, 1, 999)})

        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        id_notes = ', '.join('{0}'.format(e['other'][2]) for e in columns[3:])
        commandLine_my = utils_db.qs_prof_noteValue.format(
            id_groupe, periode, id_notes, id_students)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_note = int(query_my.value(2))
            id_eleve = int(query_my.value(3))
            value = query_my.value(4)
            valuesDic[(id_note, id_eleve)] = value
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            # Calculs des notes:
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            for column in columns[3:]:
                (id_groupe, periode, id_note, name, base, coeff, calcMode) = column['other']
                value = ''
                if (id_note, id_eleve) in valuesDic:
                    value = valuesDic[(id_note, id_eleve)]
                    if value != 'X':
                        value = float(value)
                if calcMode < 0:
                    if value == 'X':
                        row.append({
                            'value': value, 
                            'color': utils.colorLightGray, 
                            'type': utils.NOTE_PERSO, 
                            'bold': True, 
                            'flag': editableFlag})
                    else:
                        row.append({
                            'value': value, 
                            'type': utils.NOTE_PERSO, 
                            'flag': editableFlag})
                elif calcMode == 999:
                    row.append({
                        'value': value, 
                        'color': utils.colorLightGray, 
                        'type': utils.NOTE_CALCUL, 
                        'bold': True})
                else:
                    if value == 'X':
                        row.append({
                            'value': value, 
                            'color': utils.colorLightGray, 
                            'type': utils.NOTE_CALCUL, 
                            'bold': True})
                    else:
                        row.append({
                            'value': value, 
                            'type': utils.NOTE_CALCUL})
            rows.append(row)

        # on ajoute la ligne "groupe" (moyennes) :
        valuesDic = {}
        commandLine_my = utils_db.qs_prof_noteValue.format(
            id_groupe, periode, id_notes, - id_groupe - 1)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            id_note = int(query_my.value(2))
            value = query_my.value(4)
            if id_note in valuesDic:
                valuesDic[id_note] += value
            else:
                valuesDic[id_note] = value
        groupeName = QtWidgets.QApplication.translate('main', 'GROUP')
        ordre = 999
        row = [
            {'value': - id_groupe - 1, 'type': utils.INTEGER, 'groupRow': True}, 
            {'value': ordre, 'type': utils.INTEGER}, 
            {'value': groupeName, 'type': utils.LINE, 'bold': True, 'align': 'center'}]
        for column in columns[3:]:
            (id_groupe, periode, id_note, name, base, coeff, calcMode) = column['other']
            value = ''
            if id_note in valuesDic:
                value = valuesDic[id_note]
                if value != 'X':
                    value = float(value)
            row.append({
                'value': value, 
                'color': utils.colorLightGray, 
                'type': utils.NOTE_CALCUL, 
                'bold': True})
        rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcSuivis(main):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    try:
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]

        # récupération de la liste des élèves suivis :
        eleves_suivis = {}
        cptSuivis = {}
        try:
            for (id_pp, id_eleve, id_suivi, label2) in main.elevesSuivis:
                if id_eleve in eleves_suivis:
                    eleves_suivis[id_eleve].append(id_suivi)
                else:
                    eleves_suivis[id_eleve] = [id_suivi]
                if not(id_suivi in cptSuivis):
                    cptSuivis[id_suivi] = ''
        except:
            # si la base suivis.sqlite n'existe pas, on la télécharge :
            if (utils.lanConfig and utils.lanConfigDir != ''):
                destDir = utils.lanConfigDir
            else:
                destDir = main.localConfigDir
            dbFile_suivis = utils_functions.u('{0}/{1}/suivis.sqlite').format(
                destDir, main.actualVersion['versionName'])
            if not(QtCore.QFile(dbFile_suivis).exists()):
                import prof_db
                prof_db.downloadSuivisDB(main, msgFin=False)
            # on se connecte à suivis.sqlite pour remplir main.elevesSuivis :
            main.elevesSuivis = []
            (db_suivis, dbName) = utils_db.createConnection(main, dbFile_suivis)
            query_suivis = utils_db.query(db_suivis)
            try:
                commandLine_suivis = utils_db.q_selectAllFrom.format('suivi_pp_eleve_cpt')
                query_suivis = utils_db.queryExecute(commandLine_suivis, query=query_suivis)
                while query_suivis.next():
                    id_pp = int(query_suivis.value(0))
                    id_eleve = int(query_suivis.value(1))
                    id_suivi = int(query_suivis.value(2))
                    label2 = query_suivis.value(3)
                    main.elevesSuivis.append((id_pp, id_eleve, id_suivi, label2))
                for (id_pp, id_eleve, id_suivi, label2) in main.elevesSuivis:
                    if id_eleve in eleves_suivis:
                        eleves_suivis[id_eleve].append(id_suivi)
                    else:
                        eleves_suivis[id_eleve] = [id_suivi]
                    if not(id_suivi in cptSuivis):
                        cptSuivis[id_suivi] = ''
            finally:
                # on ferme la base :
                if query_suivis != None:
                    query_suivis.clear()
                    del query_suivis
                db_suivis.close()
                del db_suivis
                utils_db.sqlDatabase.removeDatabase(dbName)

        query_commun = utils_db.query(main.db_commun)
        # récupération des horaires (table commun.horaires) :
        try:
            horaires = main.horaires
        except:
            # si la base suivis.sqlite n'existe pas, on la télécharge :
            main.horaires = []
            commandLine_commun = utils_db.q_selectAllFromOrder.format('horaires', 'id')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                id_horaire = int(query_commun.value(0))
                name = query_commun.value(1)
                label = query_commun.value(2)
                main.horaires.append((id_horaire, name, label))

        # mise en place de la liste des élèves à afficher :
        elevesInGroup = []
        elevesId = []
        if main.id_groupe > -1:
            commandLine_my = utils_functions.u(
                'SELECT * FROM groupe_eleve '
                'WHERE id_groupe={0} AND ordre>0 '
                'ORDER BY ordre').format(main.id_groupe)
        else:
            commandLine_my = (
                'SELECT * FROM groupe_eleve '
                'WHERE ordre>0 '
                'ORDER BY id_groupe, ordre')
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            if id_eleve in eleves_suivis:
                if not(id_eleve in elevesId):
                    ordre = int(query_my.value(2))
                    eleveName = utils_functions.eleveNameFromId(main, id_eleve)
                    elevesInGroup.append((id_eleve, ordre, eleveName))
                    elevesId.append(id_eleve)

        # récupération des colonnes (compétences suivies) :
        cptSuivis = []
        for (id_pp, id_eleve, id_suivi, label2) in main.elevesSuivis:
            if id_eleve in elevesId:
                if not(id_suivi in cptSuivis):
                    cptSuivis.append(id_suivi)
        cptSuivis.sort()
        commandLine_commun = utils_db.q_selectAllFrom.format('suivi')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            id_suivi = int(query_commun.value(0))
            if id_suivi in cptSuivis:
                code = query_commun.value(1)
                competence = query_commun.value(5)
                columns.append({
                    'value': code, 
                    'id': id_suivi, 
                    'toolTip': competence, 
                    'datasType': utils.EVAL, 
                    'vertical': True, 
                    'bold': True})
        # ajout des colonnes spéciales :
        columns.append({'value': QtWidgets.QApplication.translate('main', 'Date'), 'id': -1})
        columns.append({'value': QtWidgets.QApplication.translate('main', 'Horaire'), 'id': -1})
        columns.append({
            'value': QtWidgets.QApplication.translate('main', 'Remark'), 
            'id': -1, 
            'columnWidth': '10.0cm', 
            'appreciationColumn': True})

        # remplissage de la table :
        date = QtCore.QDate().currentDate().toString('yyyy-MM-dd')
        horaire = ''
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            for id_suivi in cptSuivis:
                if id_suivi in eleves_suivis[id_eleve]:
                    row.append({'value': '', 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'})
                else:
                    row.append({'value': '', 'color': utils.colorGray, 'type': utils.INDETERMINATE})
            row.append({'value': date, 'type': utils.DATE, 'flag': 'EDITABLE'})
            row.append({'value': horaire, 'type': utils.HORAIRE, 'flag': 'EDITABLE'})
            row.append({'value': '', 'type': utils.TEXT, 'flag': 'EDITABLE'})
            rows.append(row)
        # on ajoute la ligne "groupe" :
        if main.id_groupe > -1:
            groupeName = QtWidgets.QApplication.translate('main', 'GROUP')
            ordre = 999
            row = [
                {'value': - main.id_groupe - 1, 'type': utils.INTEGER, 'groupRow': True}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': groupeName, 'type': utils.LINE, 'bold': True, 'align': 'center'}]
            for id_suivi in cptSuivis:
                row.append({'value': '', 'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'})
            row.append({'value': date, 'type': utils.DATE, 'bold': True, 'flag': 'EDITABLE'})
            row.append({'value': horaire, 'type': utils.HORAIRE, 'bold': True, 'flag': 'EDITABLE'})
            row.append({'value': '', 'type': utils.TEXT, 'bold': True, 'flag': 'EDITABLE'})
            rows.append(row)

        # s'il n'y a pas de suivi, on vide les tables :
        if len(columns) < 7:
            columns, rows = [], []
    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcAbsences(main):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        editableFlag = ''
    else:
        editableFlag = 'EDITABLE'
    try:
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]

        shortText = utils_db.readInConfigTable(main.db_commun, 'absences-00')[1]
        if shortText == '':
            shortText = QtWidgets.QApplication.translate(
                'main', 'Justified absences')
        longText = utils_db.readInConfigTable(main.db_commun, 'absences-01')[1]
        if longText == '':
            longText = QtWidgets.QApplication.translate(
                'main', 'Number of half-days of justified absence')
        columns.append({'value': shortText, 'id':0, 'toolTip':longText, 
            'datasType': utils.NUMBER, 'bold': True})

        shortText = utils_db.readInConfigTable(main.db_commun, 'absences-10')[1]
        if shortText == '':
            shortText = QtWidgets.QApplication.translate(
                'main', 'Unjustified absences')
        longText = utils_db.readInConfigTable(main.db_commun, 'absences-11')[1]
        if longText == '':
            longText = QtWidgets.QApplication.translate(
                'main', 'Number of half-days of unjustified absence')
        columns.append({'value': shortText, 'id':1, 'toolTip':longText, 
            'datasType': utils.NUMBER, 'bold': True})

        shortText = utils_db.readInConfigTable(main.db_commun, 'absences-20')[1]
        if shortText == '':
            shortText = QtWidgets.QApplication.translate(
                'main', 'Delays')
        longText = utils_db.readInConfigTable(main.db_commun, 'absences-21')[1]
        if longText == '':
            longText = QtWidgets.QApplication.translate(
                'main', 'Number of delays')
        columns.append({'value': shortText, 'id':2, 'toolTip':longText, 
            'datasType': utils.NUMBER, 'bold': True})

        shortText = utils_db.readInConfigTable(main.db_commun, 'absences-30')[1]
        if shortText == '':
            shortText = QtWidgets.QApplication.translate(
                'main', 'Hours missed')
        longText = utils_db.readInConfigTable(main.db_commun, 'absences-31')[1]
        if longText == '':
            longText = QtWidgets.QApplication.translate(
                'main', 'Number of classroom hours missed')
        columns.append({'value': shortText, 'id':3, 'toolTip':longText, 
            'datasType': utils.NUMBER, 'bold': True})

        # récupération de la sélection :
        data = diversFromSelection(main)
        id_groupe, matiereName = data[0], data[2]
        periode, periodeToUse = data[3], data[7][0]

        query_my = utils_db.query(main.db_my)

        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        commandLine_my = utils_db.qs_prof_absencesPeriodeEleves.format(periode, id_students)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(2))
            valuesDic[id_eleve] = (
                int(query_my.value(3)), 
                int(query_my.value(4)), 
                int(query_my.value(5)), 
                int(query_my.value(6)))
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            # Calculs des absences :
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            for column in columns[3:]:
                if 'id' in column:
                    what = column['id']
                else:
                    what = -1
                value = ''
                if id_eleve in valuesDic:
                    value = valuesDic[id_eleve][what]
                    value = int(value)
                    if value == 0:
                        value = ''
                row.append({
                    'value': value, 
                    'type': utils.NOTE_PERSO, 
                    'bold': True, 
                    'flag': editableFlag})
            rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcCounts(main):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        editableFlag = ''
    else:
        editableFlag = 'EDITABLE'
    try:
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]

        # récupération de la sélection :
        data = diversFromSelection(main)
        id_groupe, matiereName = data[0], data[2]
        periode, periodeToUse = data[3], data[7][0]

        commandLine_my = utils_db.qs_prof_counts_items.format(id_groupe, periode)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        counts = []
        while query_my.next():
            id_count = int(query_my.value(2))
            counts.append(id_count)
            name = query_my.value(3)
            sens = int(query_my.value(4))
            try:
                calcul = int(query_my.value(5))
            except:
                calcul = 0
            label = query_my.value(7)
            countLabel = utils_functions.u(label)
            id_item = query_my.value(8)
            if isinstance(id_item, int):
                id_item = int(id_item)
            else:
                id_item = -1
            if calcul > -1:
                if sens == 1:
                    countLabel = utils_functions.u('{0}\n{1}').format(
                        countLabel, 
                        QtWidgets.QApplication.translate('main', 'Growing Sense'))
                else:
                    countLabel = utils_functions.u('{0}\n{1}').format(
                        countLabel, 
                        QtWidgets.QApplication.translate('main', 'Decreasing Sense'))
            if calcul == 0:
                countLabel = utils_functions.u('{0}\n{1}').format(
                    countLabel, 
                    QtWidgets.QApplication.translate('main', 'Average and Standard deviation'))
            elif calcul > 0:
                countLabel = utils_functions.u('{0}\n{1}').format(
                    countLabel, 
                    QtWidgets.QApplication.translate('main', 'Median and Quartiles'))
            else:
                countLabel = utils_functions.u('{0}\n{1}').format(
                    countLabel, 
                    QtWidgets.QApplication.translate('main', 'No calculation'))
            columns.append({
                'value': name, 
                'id': id_count, 
                'toolTip': countLabel, 
                'datasType': utils.NUMBER, 
                'bold': True, 
                'other': (id_groupe, periode, id_count, name, sens, calcul, 0, label, id_item)})
            if calcul > -1:
                columns.append({
                    'value': name, 
                    'id': id_count, 
                    'toolTip': countLabel, 
                    'datasType': utils.INDETERMINATE, 
                    'other': (id_groupe, periode, id_count, name, sens, calcul, 1, label, id_item)})
                toolTipPerso = QtWidgets.QApplication.translate('main', 'personal evaluation')
                if id_item > -1:
                    toolTipPerso = utils_functions.u('{0}\n{1} {2} ({3})').format(
                        toolTipPerso, 
                        QtWidgets.QApplication.translate('main', 'Linked Item:'),
                        query_my.value(10),
                        query_my.value(11))
                columns.append({
                    'value': name, 
                    'id': id_count, 
                    'toolTip': toolTipPerso, 
                    'datasType': utils.EVAL, 
                    'other': (id_groupe, periode, id_count, name, sens, calcul, 2, label, id_item)})
            sep = doSeparatorColumn()
            sep['other'] = (id_groupe, periode, id_count, name, sens, calcul, 3, label, id_item)
            columns.append(sep)

        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        id_counts = utils_functions.array2string(counts)
        commandLine_my = utils_db.qs_prof_countValue.format(
            id_groupe, periode, id_counts, id_students)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_count = int(query_my.value(2))
            id_eleve = int(query_my.value(3))
            value = query_my.value(4)
            perso = query_my.value(5)
            valuesDic[(id_count, id_eleve)] = (value, perso)
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            # Calculs des comptages :
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            for column in columns[3:]:
                id_count = column['other'][2]
                calcul = column['other'][5]
                what = column['other'][6]
                if what == 0:
                    value, result, perso = '', '', ''
                    if (id_count, id_eleve) in valuesDic:
                        (value, perso) = valuesDic[(id_count, id_eleve)]
                        if not(value in ('', 'X')):
                            value = int(value)
                    toolTip = utils_functions.u('{0}\n{1} : {2}').format(
                        eleveName, column['value'], column['other'][7])
                    if value == 'X':
                        row.append({
                            'value': value, 
                            'color': utils.colorLightGray, 
                            'type': utils.NOTE_PERSO, 
                            'bold': True, 
                            'flag': editableFlag, 
                            'toolTip': toolTip})
                        result = 'X'
                    else:
                        row.append({
                            'value': value, 
                            'type': utils.NOTE_PERSO, 
                            'flag': editableFlag, 
                            'toolTip': toolTip})
                    if calcul > -1:
                        color = utils_functions.findColor(result, mustCalculIfMany=False)
                        row.append({
                            'value': utils_functions.valeur2affichage(result), 
                            'color': color, 
                            'type': utils.EVAL, 
                            'bold': True, 
                            'toolTip': toolTip})
                        # on ajoute la colonne perso :
                        color = utils_functions.findColor(perso)
                        row.append({
                            'value': utils_functions.valeur2affichage(perso), 
                            'color': color, 
                            'type': utils.EVAL, 
                            'bold': True, 
                            'flag': editableFlag, 
                            'toolTip': toolTip})
                    row.append(doSeparatorColumn(column=False))
            rows.append(row)
        rows = recalculeCountsResults(main, countsToUpdate={}, rows=rows)
    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcAccompagnement(main):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    try:
         # ICI CTR et devoirs-faits peuvent avoir des appréciations (pas géré)
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]

        # récupération de la sélection :
        data = diversFromSelection(main)
        id_groupe = data[0]

        # ajout des colonnes :
        for what in utils.LSU['MOD_ACCOMPAGNEMENT']:
            if what in ('PPRE', 'DF'):
                columns.append(doSeparatorColumn(column=False))
            columns.append({
                'value': what, 
                'id': what,
                'toolTip': utils.LSU['TRANSLATIONS'][what], 
                'vertical': True, 
                'datasType': utils.EVAL, 
                'bold': True})
            if what in ('PPRE', ):# 'CTR', 'DF'):
                toolTip = QtWidgets.QApplication.translate(
                    'main', 
                    'You can give a detailed description of the guiding.')
                columns.append({
                    'value': QtWidgets.QApplication.translate('main', 'Description'), 
                    'id': 'remark', 
                    'toolTip': toolTip, 
                    'columnWidth': '10.0cm', 
                    'appreciationColumn': True})
                columns.append(doSeparatorColumn(column=False))

        query_my = utils_db.query(main.db_my)
        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        commandLine_my = (
            'SELECT * FROM lsu '
            'WHERE lsuWhat="accompagnement" '
            'AND lsu1 IN ({0})').format(id_students)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            what = query_my.value(2)
            remark = query_my.value(3)
            if not(id_eleve in valuesDic):
                valuesDic[id_eleve] = {}
            valuesDic[id_eleve][what] = ['X', remark]
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            if not(id_eleve in valuesDic):
                valuesDic[id_eleve] = {}
            # Calculs des lignes :
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            for what in utils.LSU['MOD_ACCOMPAGNEMENT']:
                if what in ('PPRE', 'DF'):
                    row.append(doSeparatorColumn(column=False))
                [value, remark] = valuesDic[id_eleve].get(what, ['', ''])
                color = utils_functions.findColor(value, mustCalculIfMany=False)
                toolTip = utils_functions.u('{0}\n{1}').format(
                    eleveName, what)
                row.append({
                    'value': utils_functions.valeur2affichage(value), 
                    'color': color, 
                    'type': utils.EVAL, 
                    'bold': True, 
                    'toolTip': toolTip, 
                    'flag': 'EDITABLE'})
                if what in ('PPRE', ):# 'CTR', 'DF'):
                    row.append({
                        'value': remark, 
                        'type': utils.TEXT, 
                        'toolTip': toolTip, 
                        'flag': 'EDITABLE'})
                    row.append(doSeparatorColumn(column=False))
            rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcCptNumeriques(main):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    try:
        # ICI cptNum
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]

        # récupération de la sélection :
        data = diversFromSelection(main)
        id_groupe = data[0]

        # ajout des colonnes :
        toolTipTitle = ''
        for what in utils.LSU['CPT_NUM']:
            if len(what) < 7:
                columns.append(doSeparatorColumn())
                toolTipTitle = utils.LSU['TRANSLATIONS'][what]
            else:
                columns.append({
                    'value': what, 
                    'id': what,
                    'toolTip': utils_functions.u(
                        '{0}\n{1}').format(
                            toolTipTitle, 
                            utils.LSU['TRANSLATIONS'][what]), 
                    'vertical': True, 
                    'datasType': utils.NUMBER, 
                    'bold': True})
        columns.append(doSeparatorColumn())
        # on termine avec la colonne des appréciations :
        appreciationText = QtWidgets.QApplication.translate('main', 'Opinion')
        columns.append({
            'value': appreciationText, 
            'appreciationColumn': True, 
            'datasType': utils.INDETERMINATE, 
            'columnWidth': '10.0cm'})

        query_my = utils_db.query(main.db_my)
        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        commandLine_my = (
            'SELECT * FROM lsu '
            'WHERE lsuWhat="cpt-num" '
            'AND lsu1 IN ({0})').format(id_students)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(1))
            what = query_my.value(2)
            value = query_my.value(3)
            if not(id_eleve in valuesDic):
                valuesDic[id_eleve] = {}
            valuesDic[id_eleve][what] = value

        for (id_eleve, ordre, eleveName) in elevesInGroup:
            if not(id_eleve in valuesDic):
                valuesDic[id_eleve] = {}
            # Calculs des lignes :
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            for what in utils.LSU['CPT_NUM']:
                if len(what) < 7:
                    row.append(doSeparatorColumn(column=False))
                    toolTipTitle = utils.LSU['TRANSLATIONS'][what]
                else:
                    value = valuesDic[id_eleve].get(what, '')
                    if value != '':
                        value = int(value)
                    toolTip = utils_functions.u(
                        '{0}\n{1}\n{2}\n{3}').format(
                            eleveName, 
                            what, 
                            toolTipTitle, 
                            utils.LSU['TRANSLATIONS'][what])
                    row.append({
                        'value': value, 
                        'type': utils.NOTE_PERSO, 
                        'toolTip': toolTip, 
                        'flag': 'EDITABLE'})
            row.append(doSeparatorColumn(column=False))
            # et l'appréciation pour finir :
            appreciation = valuesDic[id_eleve].get('appreciation', '')
            row.append({
                'value': appreciation, 
                'type': utils.TEXT, 
                'toolTip': eleveName, 
                'flag': 'EDITABLE'})
            # on ajoute la ligne de cet élève :
            rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def calcDNB(main):
    """
    blablabla
    """
    import utils_dnb
    utils_functions.doWaitCursor()
    columns = []
    rows = []
    try:
        columns = [
            {'value': 'id_eleve', 'NO_EXPORT': True}, 
            {'value': 'ordre', 'NO_EXPORT': True}, 
            {'value': QtWidgets.QApplication.translate('main', 'Student'), 'columnWidth': '7.0cm'}]

        # récupération de la sélection :
        data = diversFromSelection(main)
        id_groupe = data[0]

        # ajout des colonnes :
        # validation du socle :
        columns.append({'value': QtWidgets.QApplication.translate('main', 'Socle3'), 'id':0,
            'toolTip':utils_dnb.TOOLTIP_SOCLE, 'datasType': utils.EVAL, 'bold': True})
        # validation du niveau A2 :
        columns.append({'value': QtWidgets.QApplication.translate('main', 'NiveauA2'), 'id':1,
            'toolTip':utils_dnb.TOOLTIP_A2, 'datasType': utils.EVAL, 'bold': True})
        # avis automatique :
        columns.append({'value': QtWidgets.QApplication.translate('main', 'Advise'), 'id':2,
            'toolTip':utils_dnb.TOOLTIP_OPINION, 'datasType': utils.EVAL, 'bold': True})
        # appréciation (si non vide, remplace l'avis automatique) :
        toolTip = QtWidgets.QApplication.translate('main', 
            'If a remark is written, it will replace the opinion.')
        columns.append({'value': QtWidgets.QApplication.translate('main', 'Remark (optional)'), 'id':3, 
            'toolTip': toolTip, 'columnWidth': '10.0cm', 'appreciationColumn': True, 'bold': True})

        query_my = utils_db.query(main.db_my)

        elevesInGroup = listStudentsInGroup(main, id_groupe=id_groupe)
        # utilisation d'un dico et de IN pour accélérer l'affichage :
        valuesDic = {}
        id_students = utils_functions.array2string(elevesInGroup, key=0)
        commandLine_my = utils_db.qs_prof_dnbValues.format(id_students)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(0))
            id_dnb = int(query_my.value(1))
            value = query_my.value(2)
            if id_eleve in valuesDic:
                valuesDic[id_eleve][id_dnb] = value
            else:
                valuesDic[id_eleve] = {id_dnb:value}
        for (id_eleve, ordre, eleveName) in elevesInGroup:
            # Calculs des lignes :
            row = [
                {'value': id_eleve, 'type': utils.INTEGER}, 
                {'value': ordre, 'type': utils.INTEGER}, 
                {'value': eleveName, 'type': utils.LINE}]
            if id_eleve in valuesDic:
                socle = valuesDic[id_eleve].get(0, '')
                a2 = valuesDic[id_eleve].get(1, '')
                avis = valuesDic[id_eleve].get(2, '')
                remark = valuesDic[id_eleve].get(3, '')
            else:
                socle, a2, avis, remark = '', '', '', ''
            color = utils_functions.findColor(socle, mustCalculIfMany=False)
            row.append({'value': utils_functions.valeur2affichage(socle), 'color': color, 
                'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'})
            color = utils_functions.findColor(a2, mustCalculIfMany=False)
            row.append({'value': utils_functions.valeur2affichage(a2), 'color': color, 
                'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'})
            color = utils_functions.findColor(avis, mustCalculIfMany=False)
            row.append({'value': utils_functions.valeur2affichage(avis), 'color': color, 
                'type': utils.EVAL, 'bold': True, 'flag': 'EDITABLE'})
            row.append({'value':remark, 'type': utils.TEXT, 'flag': 'EDITABLE'})
            rows.append(row)

    finally:
        utils_functions.restoreCursor()
        return columns, rows


def doDBStateFile(main):
    """
    Calcul de divers renseignements sur l'état de la base prof.
    On crée le fichier profDBState.html qui sera ensuite affiché.
    """
    utils_functions.doWaitCursor()
    try:
        # fichier final :
        tempFileName = utils_functions.u(
            '{0}/md/profDBState.html').format(main.tempPath)
        # si on ne l'a pas trouvé, c'est qu'on lance pour la première fois.
        # on recopie alors le dossier md dans temp :
        if not(QtCore.QFileInfo(tempFileName).exists()):
            utils_filesdirs.createDirs(main.tempPath, 'md')
            scrDir = main.beginDir + '/files/md'
            destDir = main.tempPath + '/md'
            utils_filesdirs.copyDir(scrDir, destDir)
        else:
            utils_filesdirs.removeAndCopy(
                'files/md/profDBState.html', 
                tempFileName)
        # on récupère son contenu (dans la variable htmlCurrent) :
        inFile = QtCore.QFile(tempFileName)
        if not inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            message = QtWidgets.QApplication.translate(
                'main', 'Cannot read file {0}:\n{1}.').format(tempFileName, inFile.errorString())
            utils_functions.messageBox(main, level='warning', message=message)
            return
        indentSize = 4
        textStream = QtCore.QTextStream(inFile)
        textStream.setCodec('UTF-8')
        htmlCurrent = textStream.readAll()
        htmlCurrent = utils_functions.u(htmlCurrent)
        inFile.close()

        # on remplace d'abord 2 3 trucs :
        htmlCurrent = htmlCurrent.replace(
            '${TITLE}', 
            QtWidgets.QApplication.translate('main', 'DBState'))

        # la partie la plus importante sera la variable messageProf :
        messageProf = ''
        query_my = utils_db.query(main.db_my)

        # quelques généralités sur le fichier (existe, date, etc) :
        translatedGENERAL = QtWidgets.QApplication.translate('main', 'General:')
        messageGeneral = utils_functions.u(
            '\n'
            '\n<h2>{0}</h2>'
            '\n<div class="row">'
            '\n    <div class="col-sm-2"></div>'
            '\n    <div class="col-sm-8">'
            '\n        <table class="table table-striped table-bordered">').format(
                translatedGENERAL)
        # on vérifie si le fichier existe :
        translatedMessage = QtWidgets.QApplication.translate('main', 'File exists:')
        if QtCore.QFileInfo(main.dbFile_my).exists():
            translatedResult = QtWidgets.QApplication.translate('main', 'OK')
        else:
            translatedResult = QtWidgets.QApplication.translate('main', 'No file')
        template = (
            '{0}'
            '\n            <tr>'
            '\n                <td>{1}</td>'
            '\n                <td><b>{2}</b></td>'
            '\n            </tr>')
        messageGeneral = utils_functions.u(template).format(
            messageGeneral, translatedMessage, translatedResult)
        # on vérifie si le mdp a été changé :
        translatedMessage = QtWidgets.QApplication.translate('main', 'Password state:')
        if main.mustChangeMdp:
            translatedResult = QtWidgets.QApplication.translate('main', 'MUST BE CHANGED')
        else:
            translatedResult = QtWidgets.QApplication.translate('main', 'OK')
        messageGeneral = utils_functions.u(template).format(
            messageGeneral, translatedMessage, translatedResult)
        # date du fichier :
        fileDateTimeNew = main.me['localDateTime']
        if fileDateTimeNew > 0:
            forAffichage = utils_functions.u(fileDateTimeNew)
            forAffichage = QtCore.QDateTime().fromString(forAffichage, 'yyyyMMddhhmm')
            forAffichage = forAffichage.toString('dd-MM-yyyy  hh:mm')
            translatedMessage = QtWidgets.QApplication.translate('main', 'File date:')
        else:
            forAffichage = ''
            translatedMessage = QtWidgets.QApplication.translate('main', 'Nothing in file')
        messageGeneral = utils_functions.u(template).format(
            messageGeneral, translatedMessage, forAffichage)
        # version de VÉRAC :
        progVersion = utils_db.readInConfigTable(main.db_my, "progversion")[1]
        translatedMessage = QtWidgets.QApplication.translate('main', 'Software version:')
        messageGeneral = utils_functions.u(template).format(
            messageGeneral, translatedMessage, progVersion)
        # version de la base :
        versionDB = utils_db.readInConfigDict(
            main.configDict, 'versionDB')[0]
        translatedMessage = QtWidgets.QApplication.translate('main', 'Version of the teacher DataBase:')
        messageGeneral = utils_functions.u(template).format(
            messageGeneral, translatedMessage, versionDB)
        if versionDB < utils.VERSIONDB_PROF:
            translatedMessage = QtWidgets.QApplication.translate('main', 'MUST REPAIR DB PROF')
            messageGeneral = utils_functions.u(template).format(
                messageGeneral, '', translatedMessage)
        messageGeneral = utils_functions.u(
            '{0}'
            '\n        </table>'
            '\n    </div>'
            '\n</div>').format(messageGeneral)
        messageProf = utils_functions.u(
            '{0}'
            '\n{1}\n\n').format(messageProf, messageGeneral)

        # structure et état :
        translatedSTRUCTURE = QtWidgets.QApplication.translate('main', 'Structure an state:')
        messageStructure = utils_functions.u(
            '<h2>{0}</h2>'
            '\n<div class="row">'
            '\n    <div class="col-sm-1"></div>'
            '\n    <div class="col-sm-10">').format(translatedSTRUCTURE)
        # récupération des données dans un dico :
        datas = {'ORDER': []}
        commandLine_my = utils_db.qs_prof_groupesTableaux
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            matiereName = query_my.value(2)
            if not (matiereName in datas['ORDER']):
                datas['ORDER'].append(matiereName)
                datas[matiereName] = {'ORDER': []}
            groupe = query_my.value(1)
            if not (groupe in datas[matiereName]['ORDER']):
                datas[matiereName]['ORDER'].append(groupe)
                datas[matiereName][groupe] = {'ORDER': []}
                datas[matiereName][groupe]['idGroupe'] = int(query_my.value(0))
                datas[matiereName][groupe]['isClasse'] = int(query_my.value(3))
                datas[matiereName][groupe]['classeName'] = query_my.value(4)
            periode = int(query_my.value(10))
            if not (periode in datas[matiereName][groupe]['ORDER']):
                datas[matiereName][groupe]['ORDER'].append(periode)
                datas[matiereName][groupe][periode] = {
                    'ORDER': [], 'BLT': False, 'Perso': False, 'Referentiel': False}
                datas[matiereName][groupe][periode]['name'] = utils.PERIODES[periode]
            tableau = query_my.value(7)
            if not (tableau in datas[matiereName][groupe][periode]['ORDER']):
                datas[matiereName][groupe][periode]['ORDER'].append(tableau)
                datas[matiereName][groupe][periode][tableau] = {}
                datas[matiereName][groupe][periode][tableau]['public']= int(query_my.value(8))
        # récupération de l'état des évaluations :
        commandLine_my = utils_db.qs_prof_groupesBilans
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            matiereName = query_my.value(2)
            groupe = query_my.value(3)
            periode = int(query_my.value(1))
            id_competence = int(query_my.value(4))
            try:
                if id_competence == -1:
                    datas[matiereName][groupe][periode]['Perso'] = True
                elif id_competence < utils.decalageBLT:
                    datas[matiereName][groupe][periode]['Referentiel'] = True
                elif id_competence < utils.decalageCFD:
                    datas[matiereName][groupe][periode]['BLT'] = True
            except:
                pass
        # des traductions :
        translatedSUBJECT = QtWidgets.QApplication.translate('main', 'SUBJECT:')
        translatedGROUP = QtWidgets.QApplication.translate('main', 'GROUP:')
        translatedCLASSGROUP = QtWidgets.QApplication.translate('main', 'class-group:')
        translatedPERIOD = QtWidgets.QApplication.translate('main', 'PERIOD:')
        translatedTABLESLIST = QtWidgets.QApplication.translate('main', 'Tables List:')
        translatedPRIVATE = QtWidgets.QApplication.translate('main', 'private table')
        translatedSTATE = QtWidgets.QApplication.translate('main', 'State assessments:')
        translations = {
            'ORDER' : ('BLT', 'Perso', 'Referentiel'),
            'BLT': QtWidgets.QApplication.translate('main', 'Shared section of the bulletin:'),
            'Perso': QtWidgets.QApplication.translate('main', 'Disciplinary section of the bulletin:'),
            'Referentiel': QtWidgets.QApplication.translate('main', 'Referentiel:')}
        translatedOK = QtWidgets.QApplication.translate('main', 'OK')
        translatedNOTHING = QtWidgets.QApplication.translate('main', 'NOTHING')
        # calcul de l'affichage :
        for matiereName in datas['ORDER']:
            messageStructure = utils_functions.u(
                '{0}\n        <h4>{1} <b>{2}</b></h4>'
                '\n        <ul>').format(
                    messageStructure, translatedSUBJECT, matiereName)
            for groupe in datas[matiereName]['ORDER']:
                comment = ''
                if datas[matiereName][groupe]['isClasse'] == 1:
                    comment = utils_functions.u(
                        '&nbsp;&nbsp;&nbsp;({0} {1})').format(
                            translatedCLASSGROUP, datas[matiereName][groupe]['classeName'])
                messageStructure = utils_functions.u(
                    '{0}'
                    '\n            <li>{1} <b>{2}</b>{3}'
                    '\n                <ul>').format(messageStructure, translatedGROUP, groupe, comment)
                for periode in datas[matiereName][groupe]['ORDER']:
                    messageStructure = utils_functions.u(
                        '{0}'
                        '\n                    <li>{1} <b>{2}</b>').format(
                            messageStructure, translatedPERIOD, utils.PERIODES[periode])
                    # liste des tableaux :
                    messageStructure = utils_functions.u(
                        '{0}'
                        '\n                        <br/>{1}'
                        '\n                        <ul>').format(
                            messageStructure, translatedTABLESLIST)
                    for tableau in datas[matiereName][groupe][periode]['ORDER']:
                        comment = ''
                        if datas[matiereName][groupe][periode][tableau]['public'] == 0:
                            comment = utils_functions.u(
                                '&nbsp;&nbsp;&nbsp;({0})').format(
                                    translatedPRIVATE)
                        messageStructure = utils_functions.u(
                            '{0}'
                            '\n                            <li><b>{1}</b>{2}'
                            '\n                            </li>').format(
                                messageStructure, tableau, comment)
                    messageStructure = utils_functions.u(
                        '{0}'
                        '\n                        </ul>').format(messageStructure)
                    # état de la structure (évaluations) :
                    messageStructure = utils_functions.u(
                        '{0}'
                        '\n                        {1}'
                        '\n                        <ul>').format(
                            messageStructure, translatedSTATE)
                    for section in translations['ORDER']:
                        if datas[matiereName][groupe][periode][section]:
                            comment = utils_functions.u(
                                '&nbsp;&nbsp;&nbsp;{0}').format(translatedOK)
                        else:
                            comment = utils_functions.u(
                                '&nbsp;&nbsp;&nbsp;{0}').format(translatedNOTHING)
                        messageStructure = utils_functions.u(
                            '{0}'
                            '\n                            <li>{1} <b>{2}</b>'
                            '\n                            </li>').format(
                                messageStructure, translations[section], comment)
                    messageStructure = utils_functions.u(
                        '{0}'
                        '\n                        </ul>').format(messageStructure)
                    messageStructure = utils_functions.u(
                        '{0}'
                        '\n                    </li>').format(messageStructure)
                messageStructure = utils_functions.u(
                    '{0}'
                    '\n                </ul>'
                    '\n            </li>').format(messageStructure)
            messageStructure = utils_functions.u(
                '{0}'
                '\n        </ul>').format(
                    messageStructure)
        messageStructure = utils_functions.u(
            '{0}'
            '\n    </div>'
            '\n</div>').format(messageStructure)
        messageProf = utils_functions.u(
            '{0}\n{1}\n\n').format(messageProf, messageStructure)

        # liste des tableaux :
        translatedLIST = QtWidgets.QApplication.translate('main', 'List of Tables:')
        translatedMATIERE = QtWidgets.QApplication.translate('main', 'MATIERE')
        translatedGROUPE = QtWidgets.QApplication.translate('main', 'GROUPE')
        translatedPERIODE = QtWidgets.QApplication.translate('main', 'PERIODE')
        translatedNAME = QtWidgets.QApplication.translate('main', 'NAME')
        translatedETAT = QtWidgets.QApplication.translate('main', 'ETAT')
        translatedCLASS = QtWidgets.QApplication.translate('main', 'CLASS')
        commandLine_my = utils_db.qs_prof_tableauxEtMatieres
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        nbTableaux, nbPublics = 0, 0
        tableauxList = []
        while query_my.next():
            nbTableaux += 1
            name = query_my.value(1)
            public = int(query_my.value(2))
            if public == 1:
                public = 'public'
                nbPublics += 1
            else:
                public = 'prive'
            periode = int(query_my.value(4))
            periode = utils.PERIODES[periode]
            matiereName = query_my.value(9)
            groupeName = query_my.value(8)
            classeName = query_my.value(11)
            tableauxList.append((matiereName, groupeName, periode, name, public, classeName))
        messageNbTableaux = '{0} tableaux'.format(nbTableaux)
        messageNbPublics = '{0} tableaux publics'.format(nbPublics)
        messageTableaux = utils_functions.u(
            '<h2>{0}</h2>'
            '\n<p>{1} ; {2}<p/>'
            '\n<table class="table table-striped table-bordered">'
            '\n    <thead>'
            '\n        <tr class="table-success">'
            '\n            <th>{3}</th>'
            '\n            <th>{4}</th>'
            '\n            <th>{5}</th>'
            '\n            <th>{6}</th>'
            '\n            <th>{7}</th>'
            '\n            <th>{8}</th>'
            '\n        </tr>'
             '\n   </thead>').format(
                translatedLIST,
                messageNbTableaux, messageNbPublics,
                translatedMATIERE, translatedGROUPE,
                translatedPERIODE, translatedNAME,
                translatedETAT, translatedCLASS)
        for tableau in tableauxList:
            messageTableau = utils_functions.u(
                '\n    <tr>'
                '\n        <td>{0}</td>'
                '\n        <td>{1}</td>'
                '\n        <td>{2}</td>'
                '\n        <td>{3}</td>'
                '\n        <td>{4}</td>'
                '\n        <td>{5}</td>'
                '\n    </tr>').format(
                    tableau[0], tableau[1], tableau[2], 
                    tableau[3], tableau[4], tableau[5])
            messageTableaux = utils_functions.u('{0}{1}').format(messageTableaux, messageTableau)
        messageTableaux = utils_functions.u(
            '{0}\n</table>').format(messageTableaux)
        messageProf = utils_functions.u(
            '{0}\n{1}\n\n').format(messageProf, messageTableaux)

        # on insère le message :
        htmlCurrent = htmlCurrent.replace(
            '${DB_STATE}', utils_functions.u(messageProf))
        # enfin on enregistre le nouveau fichier html :
        if utils.OS_NAME[0] == 'win':
            import utils_html
            htmlCurrent = utils_html.encodeHtml(htmlCurrent)
        outFile = QtCore.QFile(tempFileName)
        if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(outFile)
            stream.setCodec('UTF-8')
            stream << htmlCurrent
            outFile.close()
    finally:
        utils_functions.restoreCursor()






























































###########################################################"
#        MODEL VIEW DELEGATE
###########################################################"

class ProfTableModel(QtCore.QAbstractTableModel):
    """
    Structure :
        self.columns = [column0, column1, etc]
            avec column = {}
                'value': le texte affiché
                'id': l'id du truc (id_item par exemple)
                'toolTip': le toolTip à afficher
                'datasType': le type de donnée
                'bold': True si en gras
                'other': () : autre chose (utilisé pour les notes par exemple)
                'columnWidth': '7.0cm'
        self.rows = [row0, row1, etc]
            avec row = [data0, data1, etc]
            et data = {}
                'value' : valeur à afficher
                'color' : couleur de fond
                'type': le type de donnée
                'bold' : True si en gras
                'align': 'center', ...
    """

    def __init__(self, main=None):
        super(ProfTableModel, self).__init__(main)
        self.main = main
        self.rows = []
        self.columns = []
        self.normalFont = QtGui.QFont()
        self.boldFont = QtGui.QFont()
        self.boldFont.setBold(True)
        self.alignLeft = int(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.alignCenter = int(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        self.alignRight = int(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.editableFlag = self.defaultFlag | QtCore.Qt.ItemIsEditable
        self.noChangeFlag = QtCore.Qt.ItemIsSelectable
        self.nothingFlag = QtCore.Qt.NoItemFlags
        self.suivisFirstDay = True
        self.suivisFirstHoraire = True
        self.lastViewType = self.main.viewType

    def setTable(self, id_tableau=-1, id_eleve=-1):
        self.beginResetModel()
        try:
            self.columns, self.rows = [], []
            if self.main.viewType == utils.VIEW_ITEMS:
                self.columns, self.rows = calcItems(self.main, id_tableau)
            elif self.main.viewType == utils.VIEW_STATS_TABLEAU:
                self.columns, self.rows = calcStats(self.main, id_tableau)
            elif self.main.viewType == utils.VIEW_BILANS:
                self.columns, self.rows = calcBilans(self.main)
            elif self.main.viewType == utils.VIEW_BULLETIN:
                self.columns, self.rows = calcBulletin(self.main)
            elif self.main.viewType == utils.VIEW_APPRECIATIONS:
                self.columns, self.rows = calcAppreciations(self.main)
            elif self.main.viewType == utils.VIEW_STATS_GROUPE:
                self.columns, self.rows = calcStats(self.main, -1)
            elif self.main.viewType == utils.VIEW_ELEVE:
                self.columns, self.rows = calcEleve(self.main, id_eleve)
            elif self.main.viewType == utils.VIEW_NOTES:
                self.columns, self.rows = calcNotes(self.main)
            elif self.main.viewType == utils.VIEW_SUIVIS:
                self.columns, self.rows = calcSuivis(self.main)
                self.suivisFirstDay = True
                self.suivisFirstHoraire = True
            elif self.main.viewType == utils.VIEW_ABSENCES:
                self.columns, self.rows = calcAbsences(self.main)
            elif self.main.viewType == utils.VIEW_COUNTS:
                self.columns, self.rows = calcCounts(self.main)
            elif self.main.viewType == utils.VIEW_ACCOMPAGNEMENT:
                self.columns, self.rows = calcAccompagnement(self.main)
            elif self.main.viewType == utils.VIEW_CPT_NUM:
                self.columns, self.rows = calcCptNumeriques(self.main)
            elif self.main.viewType == utils.VIEW_DNB:
                self.columns, self.rows = calcDNB(self.main)
            self.previousSection = -1
            self.lastViewType = self.main.viewType
        finally:
            self.endResetModel()

    def rowCount(self, index=QtCore.QModelIndex()):
        return len(self.rows)

    def columnCount(self, index=QtCore.QModelIndex()):
        return len(self.columns)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column, row = index.column(), index.row()
        actualCell = self.rows[row][column]
        if role == QtCore.Qt.FontRole:
            if 'bold' in actualCell:
                return self.boldFont
            else:
                return self.normalFont
        elif role == QtCore.Qt.DecorationRole:
            cellValue = actualCell['value']
            if cellValue == 'EXPAND':
                return utils.doIcon('list-expand')
            elif cellValue == 'COLLAPSE':
                return utils.doIcon('list-collapse')
        elif role == QtCore.Qt.TextAlignmentRole:
            # on donne priorité à un align passé en paramètre :
            align = actualCell.get('align', '')
            if align == 'center':
                return self.alignCenter
            elif align == 'right':
                return self.alignRight
            elif align == 'left':
                return self.alignLeft
            # sinon on se base sur le type :
            cellType = actualCell['type']
            if cellType in utils.TYPES_ALIGN_LEFT:
                return self.alignLeft
            elif cellType in utils.TYPES_ALIGN_RIGHT:
                return self.alignRight
            else:
                return self.alignCenter
        elif role == QtCore.Qt.DisplayRole:
            cellValue = actualCell['value']
            if cellValue in ('HSEPARATOR', 'VSEPARATOR', 'EXPAND', 'COLLAPSE'):
                return ''
            else:
                return cellValue
        elif role == QtCore.Qt.BackgroundRole:
            if self.rows[row][1]['value'] == -1:
                return utils.colorGray
            else:
                return actualCell.get('color', utils.colorWhite)
        elif role == QtCore.Qt.ForegroundRole:
            return actualCell.get('text-color', utils.colorBlack)
        elif role == QtCore.Qt.ToolTipRole:
            if 'toolTip' in actualCell:
                return actualCell['toolTip']
            elif self.main.viewType == utils.VIEW_SUIVIS:
                if column in range(3, len(self.columns) - 3):
                    id_eleveSelect = self.rows[row][0]['value']
                    id_suiviSelect = self.columns[column].get('id', -1)
                    label2Select = ''
                    for (id_pp, id_eleve, id_suivi, label2) in self.main.elevesSuivis:
                        if (id_eleve == id_eleveSelect) and (id_suivi == id_suiviSelect):
                            label2Select = label2
                    if label2Select != '':
                        toolTip = utils_functions.u(label2Select)
                        actualCell['toolTip'] = toolTip
                        return toolTip
                    else:
                        actualCell['toolTip'] = ''
                        return ''
                elif self.columns[column]['value'] == QtWidgets.QApplication.translate('main', 'Horaire'):
                    horaireName = actualCell['value']
                    if horaireName == '':
                        actualCell['toolTip'] = ''
                        return ''
                    horaireLabel = ''
                    commandLine_commun = utils_functions.u(
                        'SELECT * FROM horaires WHERE Name=?')
                    query_commun = utils_db.queryExecute(
                        {commandLine_commun: (horaireName, )}, 
                        db=self.main.db_commun)
                    while query_commun.next():
                        horaireLabel = query_commun.value(2)
                    toolTip = utils_functions.u('{0} :\n{1}').format(horaireName, horaireLabel)
                    actualCell['toolTip'] = toolTip
                    return toolTip
                else:
                    actualCell['toolTip'] = ''
                    return ''
        else:
            return

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.FontRole:
            if orientation == QtCore.Qt.Horizontal:
                if 'bold' in self.columns[section]:
                    return self.boldFont
                else:
                    return self.normalFont
            elif 'bold' in self.rows[section][2]:
                return self.boldFont
            else:
                return self.normalFont
        elif role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self.columns[section]['value']
            else:
                return self.rows[section][2]['value']
        elif role == QtCore.Qt.ToolTipRole:
            # affichage du toolTip seulement pour les titres de colonnes :
            if orientation == QtCore.Qt.Horizontal:
                message = utils_functions.u(self.columns[section]['value'])
                labelColumn = self.columns[section].get('toolTip', '')
                if len(labelColumn) > 0:
                    message = utils_functions.u('{0} :\n{1}').format(message, labelColumn)
                return message
            else:
                return ''
        else:
            return

    def sortTable(self, section):
        if self.main.viewType == utils.VIEW_ELEVE:
            return

        def toNum(item):
            # pour gérer les cases vides dans les colonnes de nombres
            if item == '':
                return 0
            else:
                return item

        def itemgetter(columType, *items):
            """
            adapté de operator.itemgetter
            Pour trier selon les valeurs (A, B, ...) et non l'affichage
            et pour gérer les nombres et cases vides
            """
            if len(items) == 1:
                item = items[0]
                def g(obj):
                    if columType == utils.NUMBER:
                        return toNum(obj[item]['value'])
                    elif columType == utils.EVAL:
                        return utils_functions.affichage2valeur(obj[item]['value'])
                    else:
                        return obj[item]['value']
            else:
                def g(obj):
                    result = []
                    item = items[0]
                    if columType == utils.NUMBER:
                        result.append(toNum(obj[item]['value']))
                    elif columType == utils.EVAL:
                        result.append(utils_functions.affichage2valeur(obj[item]['value']))
                    else:
                        result.append(obj[item]['value'])
                    for item in items[1:]:
                        result.append(obj[item]['value'])
                    return tuple(result)
            return g

        self.beginResetModel()
        try:
            # on ne trie pas la ligne Groupe :
            withGroupLine = ('groupRow' in self.rows[-1][0])
            # on récupère le type de données de la colonne :
            if 'datasType' in self.columns[section]:
                columType = self.columns[section]['datasType']
            else:
                columType = utils.INDETERMINATE

            if section == self.previousSection:
                self.sortMode = (self.sortMode + 1) % 3
            else:
                self.sortMode = 0
            self.previousSection = section

            if withGroupLine:
                tempTable = self.rows[:-1]
                groupTable = self.rows[-1]
            else:
                tempTable = self.rows
            if self.sortMode == 0:
                tempTable = sorted(tempTable, key=itemgetter(columType, section, 1))
            elif self.sortMode == 1:
                tempTable = sorted(tempTable, key=itemgetter(utils.NUMBER, 1))
                tempTable = sorted(tempTable, key=itemgetter(columType, section), reverse=True)
            else:
                tempTable = sorted(tempTable, key=itemgetter(utils.NUMBER, 1))
            if withGroupLine:
                self.rows = tempTable
                self.rows.append(groupTable)
            else:
                self.rows = tempTable
        finally:
            self.endResetModel()

    def flags(self, index):
        """
        self.defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.editableFlag = self.defaultFlag | QtCore.Qt.ItemIsEditable
        self.noChangeFlag = QtCore.Qt.ItemIsSelectable
        self.nothingFlag = QtCore.Qt.NoItemFlags
        """
        cellFlag = self.rows[index.row()][index.column()].get('flag', '')
        cellType = self.rows[index.row()][index.column()].get('type', utils.INDETERMINATE)
        # les élèves supprimés ne peuvent plus être modifiés :
        if self.rows[index.row()][1]['value'] == -1:
            cellFlag = 'NOCHANGE'
        if cellFlag == 'EDITABLE':
            if cellType == utils.EVAL:
                return self.defaultFlag
            else:
                return self.editableFlag
        elif cellFlag == 'NOCHANGE':
            return self.noChangeFlag
            """
            elif self.rows[index.row()][1]['value'] == -1:
            return self.noChangeFlag
            """
        elif cellFlag == 'NOTHING':
            return self.nothingFlag
        elif cellType == utils.INDETERMINATE:
            return self.nothingFlag
        else:
            return self.defaultFlag


class ProfTableView(QtWidgets.QTableView):

    def __init__(self, main=None):
        super(ProfTableView, self).__init__(main)
        self.main = main
        self.columnIndexTitle = []
        self.horizontalHeader().sectionDoubleClicked.connect(self.sortTable)
        if utils.PYQT == 'PYQT5':
            self.horizontalHeader().setSectionsMovable(True)
        else:
            self.horizontalHeader().setMovable(True)
        self.horizontalHeader().sectionMoved.connect(self.columnMoved)
        # pour savoir si des cases sont sélectionnées,
        # mais aussi leurs types :
        self.selectionState = {
            'hasSelection': False, 
            'hasEditables': False, 
            'evals': [], 
            'texts': [], 
            'notes': [], 
            'dates': [], 
            'horaires': []}

    def columnMoved(self, column, oldIndex, newIndex):
        #utils_functions.myPrint(column, oldIndex, newIndex)
        if newIndex < oldIndex:
            for col in self.columnIndexTitle:
                if (newIndex <= col[1]) and (col[1] < oldIndex):
                    col[1] = col[1] + 1
        else:
            for col in self.columnIndexTitle:
                if (oldIndex < col[1]) and (col[1] <= newIndex):
                    col[1] = col[1] - 1
        self.columnIndexTitle[column][1] = newIndex

    def selectionChanged(self, selected, deselected):
        updateSelectedActions(self.main)
        QtWidgets.QTableView.selectionChanged(self, selected, deselected)

    def currentChanged(self, current, previous):
        if self.main.model == None:
            return
        mustDeselect = False
        if (self.main.viewType in utils.VIEWS_WITH_NOTES) \
            and (self.main.me.get('numberEditing', False)):
                changeValue(self.main, 'doValidate')
        try:
            titleRow = self.main.model.rows[current.row()][2]['value']
            titleColumn = self.main.model.columns[current.column()]['value']
            if 'toolTip' in self.main.model.columns[current.column()]:
                labelColumn = self.main.model.columns[current.column()]['toolTip']
            else:
                labelColumn = ''
            if self.main.viewType == utils.VIEW_APPRECIATIONS:
                self.main.me['actualAppreciation'] = current.data()
                message = utils_functions.u('{0} | {1} : {2}').format(titleRow, 
                    titleColumn, current.data())
            elif self.main.viewType == utils.VIEW_ELEVE:
                message = utils_functions.u('{0} | {1}').format(titleRow, titleColumn)
                cellValue = self.main.model.rows[current.row()][current.column()]['value']
                if cellValue in ('EXPAND', 'COLLAPSE'):
                    expandCollapseRows(self.main, current.row())
                    mustDeselect = True
            elif self.main.viewType == utils.VIEW_BULLETIN:
                if titleColumn == QtWidgets.QApplication.translate('main', 'Opinion'):
                    self.main.me['actualAppreciation'] = current.data()
                    message = utils_functions.u('{0} | {1} : {2}').format(
                        titleRow, titleColumn, current.data())
                else:
                    message = utils_functions.u('{0} | {1} : {2}').format(
                        titleRow, titleColumn, labelColumn)
            elif self.main.viewType in utils.VIEWS_NO:
                message = ''
            else:
                message = utils_functions.u('{0} | {1} : {2}').format(titleRow, titleColumn, labelColumn)
                if self.main.viewType == utils.VIEW_NOTES:
                    titleColumnMoyenne = self.main.model.columns[
                        self.main.model.columnCount() - 1]['value']
                    aNoteIsSelected = (titleColumn != titleColumnMoyenne)
                    self.main.actionEditNote.setEnabled(aNoteIsSelected)
                    self.main.actionDeleteNote.setEnabled(aNoteIsSelected)
            utils_functions.afficheStatusBar(self.main, message)
        except:
            utils_functions.afficheStatusBar(self.main)
        finally:
            if mustDeselect:
                self.main.view.setCurrentIndex(QtCore.QModelIndex())
            else:
                QtWidgets.QTableView.currentChanged(self, current, previous)

    def keyPressEvent(self, event):
        #print('prof.keyPressEvent', event.key())
        # cas particuliers :
        if event.key() == utils.KEY_ESCAPE:
            self.main.close()
            return
        elif self.main.viewType in utils.VIEWS_NO:
            return
        elif event.key() in utils.KEYS_ARROWS:
            # si on change de case en pleine édition d'appréciation :
            if self.main.me.get('numberEditing', False):
                changeValue(self.main, 'doValidate')
            QtWidgets.QTableView.keyPressEvent(self, event)
        elif self.main.noKeyPressEvent:
            return
        elif not(self.selectionState['hasSelection']):
            return

        # gestion des raccourcis du presse-papier :
        if event.key() == utils.KEY_CONTROL:
            self.main.doClipBoard = True
            return

        mustDo = False
        if len(self.selectionState['evals']) > 0:
            if event.key() in utils.KEYS_EVALS:
                mustDo = True
        if len(self.selectionState['notes']) > 0:
            if event.text() in utils.CHARS_NUMERIC:
                mustDo = True
            elif event.key() in utils.KEYS_VALIDATE:
                mustDo = True
        if event.key() in utils.KEYS_OTHER:
            mustDo = True
        if not(mustDo):
            return

        if len(self.selectionState['texts']) > 0:
            if event.key() in (utils.KEY_DELETE, utils.KEY_BACKSPACE):
                # effacer les appréciations sélectionnées :
                deleteSelectedAppreciations(self.main)
            elif self.main.doClipBoard and event.key() == utils.KEY_CUT:
                deleteSelectedAppreciations(self.main)
            if not(self.main.viewType in utils.VIEWS_WITH_EVALUATIONS):
                return

        value = '@@@@@'
        doValidate = True
        if event.key() == utils.KEY_SHIFT:
            self.main.doAdd = True
        elif event.key() in (utils.KEY_DELETE, utils.KEY_BACKSPACE):
            value = ''
        elif len(self.selectionState['evals']) > 0:
            if event.key() in utils.KEYS_EVALS:
                value = utils.KEYS_EVALS[event.key()]
        elif len(self.selectionState['notes']) > 0:
            if event.text() in utils.CHARS_NUMERIC[:-2]:
                value = event.text()
                doValidate = False
            elif event.text() in utils.CHARS_NUMERIC[-2:]:
                if self.main.viewType == utils.VIEW_NOTES:
                    value = event.text()
                    doValidate = False
            elif event.key() in utils.KEYS_EVALS:
                if utils.KEYS_EVALS[event.key()] == 'X':
                    value = 'X'
            elif event.key() in utils.KEYS_VALIDATE:
                value = 'doValidate'
        if value != '@@@@@':
            doAdd = self.main.doAdd
            if self.main.actionShift.isChecked():
                doAdd = not(doAdd)
            changeValue(self.main, value, doAdd=doAdd, doValidate=doValidate)
            if self.main.viewType == utils.VIEW_ITEMS:
                self.main.whatIsModified['actualTableau'] = True

    def keyReleaseEvent(self, event):
        if event.key() == utils.KEY_SHIFT:
            self.main.doAdd = False
        elif event.key() == utils.KEY_CONTROL:
            self.main.doClipBoard = False
        else:
            QtWidgets.QTableView.keyReleaseEvent(self, event)

    def sortTable(self, section):
        self.main.model.sortTable(section)

    def restore(self):
        n = self.horizontalHeader().count()
        for i in range(n):
            j = self.horizontalHeader().visualIndex(i)
            if j != i:
                self.horizontalHeader().moveSection(j, i)

    def contextMenuEvent(self, event):
        self.main.contextMenuEvent(event)

    def mouseDoubleClickEvent(self, event):
        doNotEvalEdit = False
        if not(self.selectionState['hasSelection']):
            doNotEvalEdit = True
        elif not(self.selectionState['hasEditables']):
            doNotEvalEdit = True
        elif len(self.selectionState['evals']) != 1:
            doNotEvalEdit = True
        if doNotEvalEdit:
            QtWidgets.QTableView.mouseDoubleClickEvent(self, event)
            return

        item = self.main.view.selectionState['evals'][0]
        value = utils_functions.affichage2valeur(item.data())
        dialog = EditEvaluationDlg(parent=self.main, text=item.data())
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        newValue = dialog.evalEdit.text()

        utils_functions.doWaitCursor()
        try:
            oldState = []
            row, column = item.row(), item.column()
            newValue = utils_functions.affichage2valeur(newValue)
            if column > 2:
                doChangeValue(self.main, row, column, newValue, oldState)
        finally:
            utils_functions.restoreCursor()
            self.main.changeDBMyState()
            if len(oldState) > 0:
                self.main.undoList.append(oldState)
                self.main.actionUndo.setEnabled(True)


class EditEvaluationDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, text=''):
        super(EditEvaluationDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Edit this evaluation'))

        letters = []
        for value in utils.itemsValues:
            letters.append(utils_functions.u(utils.affichages[value][0]))
        message = utils_functions.u(', ').join(
            utils_functions.u('{0}').format(e) for e in letters)
        message = QtWidgets.QApplication.translate(
            'main', 
            '<p>You can use the following characters, '
            '<br/>but respect the case (upper or lower case):</p>'
            '<p align="center"><b>{0}</b>.</p>').format(message)
        label = QtWidgets.QLabel(message)
        self.evalEdit = QtWidgets.QLineEdit()
        self.evalEdit.setText(text)
        regExp = ''
        for letter in letters:
            regExp = utils_functions.u('{0}{1}').format(regExp, letter)
        regExp = utils_functions.u('[{0}]*').format(regExp)
        regExp = QtCore.QRegExp(regExp)
        self.evalEdit.setValidator(
            QtGui.QRegExpValidator(regExp, self.evalEdit))

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 1, 0, 1, 2)
        grid.addWidget(self.evalEdit, 2, 0, 1, 2)
        grid.addWidget(buttonBox, 9, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('saisir-evaluations')


class VeracDelegate(QtWidgets.QItemDelegate):
    """
    une classe pour gérer l'affichage des cases de saisies
    des appréciations dans les tableaux.
    """

    def __init__(self, parent=None, model=None):
        super(VeracDelegate, self).__init__(parent)
        self.main = parent
        # on a besoin de récupérer le model :
        self.model = model
        # what est le type de donnée :
        self.what = utils.INDETERMINATE
        self.oldRowHeight = 0
        self.editor = None

    def paint(self, painter, option, index):
        myOption = QtWidgets.QStyleOptionViewItem(option)
        QtWidgets.QItemDelegate.paint(self, painter, myOption, index)

    def createEditor(self, parent, option, index):
        """
        Selon le type de donnée (what), on utilise un objet différent.
        En vue notes, on utilise des nombres décimaux.
        Dans les autres cas de nombres (comptages, etc) ce sont des entiers.
        """
        column = index.column()
        row = index.row()
        self.what = self.model.rows[row][column]['type']
        if self.what == utils.NOTE_PERSO:
            if self.main.viewType == utils.VIEW_NOTES:
                self.what = utils.FLOAT
            else:
                self.what = utils.INTEGER
        if self.what == utils.DATE:
            import utils_calendar
            editor = utils_calendar.PyDateEdit(parent)
        elif self.what == utils.HORAIRE:
            editor = QtWidgets.QComboBox(parent)
            for horaire in self.main.horaires:
                editor.addItem(utils_functions.u(horaire[1]), horaire[0])
            editor.setEditable(True)
        elif self.what == utils.FLOAT:
            editor = QtWidgets.QLineEdit(parent)
            # on accepte les chiffres, le point, le séparateur décimal et X:
            regExp = QtCore.QRegExp('[xX0-9\{0}\.]*'.format(utils.DECIMAL_SEPARATOR))
            editor.setValidator(QtGui.QRegExpValidator(regExp, editor))
        elif self.what == utils.INTEGER:
            editor = QtWidgets.QSpinBox(parent)
            editor.setMinimum(0)
            editor.setMaximum(1000)
        else:
            if 'appreciationColumn' in self.model.columns[column]:
                self.oldRowHeight = self.main.view.rowHeight(index.row())
                #self.main.view.setRowHeight(index.row(), 2 * self.oldRowHeight)
                self.main.view.setRowHeight(
                    index.row(), self.oldRowHeight + self.main.fontSizeSlider.value())
                if utils.PYTHONVERSION >= 30:
                    editor = utils_spell.SpellTextEdit(parent)
                else:
                    editor = QtWidgets.QTextEdit(parent)
                editor.textChanged.connect(self.doAppreciationChanged)
            else:
                editor = QtWidgets.QTextEdit(parent)
        font = editor.font()
        font.setPointSize(self.main.fontSizeSlider.value())
        editor.setFont(font)
        self.editor = editor
        return editor

    def setEditorData(self, editor, index):
        row = index.row()
        column = index.column()
        if self.what == utils.DATE:
            value = self.model.rows[row][column]['value']
            if value == '':
                date = QtCore.QDate().currentDate()
            else:
                date = QtCore.QDate().fromString(value, 'yyyy-MM-dd')
            editor.setDate(date)
        elif self.what == utils.HORAIRE:
            value = self.model.rows[row][column]['value']
            i = editor.findText(value)
            if i == -1:
                i = 0
            editor.setCurrentIndex(i)
        elif self.what == utils.FLOAT:
            self.main.me['numberFromDelegate'] = True
            value = self.model.rows[row][column]['value']
            editor.setText('{0}'.format(value))
        elif self.what == utils.INTEGER:
            self.main.me['numberFromDelegate'] = True
            value = self.model.rows[row][column]['value']
            if value in ('', 'X'):
                value = 0
            editor.setValue(value)
        else:
            value = self.model.rows[row][column]['value']
            editor.setText(value)

    def setModelData(self, editor, model, index):
        mustResizeRowsToContents = False
        if self.main.viewType == utils.VIEW_SUIVIS:
            self.main.suivisIsModified = True
        if self.what == utils.DATE:
            value = editor.date().toString('yyyy-MM-dd')
        elif self.what == utils.HORAIRE:
            currentIndex = editor.currentIndex()
            value = editor.itemText(currentIndex)
        elif self.what == utils.FLOAT:
            value = editor.text()
            value = value.replace(utils.DECIMAL_SEPARATOR, '.')
            if value == 'x':
                value = 'X'
            elif not(value in ('', 'X')):
                try:
                    value = float(value)
                except:
                    value = ''
        elif self.what == utils.INTEGER:
            editor.interpretText()
            value = editor.value()
        else:
            value = editor.toPlainText()
            if testAppreciationMustBeSaved(self.main):
                mustResizeRowsToContents = True
            else:
                self.main.view.setRowHeight(index.row(), self.oldRowHeight)
        # mise à jour de l'affichage du model et des données :
        if (self.what == utils.DATE) and (model.suivisFirstDay):
            model.suivisFirstDay = False
            message = QtWidgets.QApplication.translate(
                'main', 'Do you want to change all dates?')
            message = utils_functions.u('<p><b>{0}</b></p>').format(message)
            reply = utils_functions.messageBox(
                self.main, level='question', message=message, buttons=['Yes', 'No'])
            if reply == QtWidgets.QMessageBox.Yes:
                for row in range(model.rowCount()):
                    other = model.index(row, index.column())
                    model.setData(other, value, QtCore.Qt.EditRole)
                    model.rows[row][index.column()]['value'] = value
            else:
                model.setData(index, value, QtCore.Qt.EditRole)
                model.rows[index.row()][index.column()]['value'] = value
        elif (self.what == utils.HORAIRE) and (model.suivisFirstHoraire):
            model.suivisFirstHoraire = False
            for row in range(model.rowCount()):
                other = model.index(row, index.column())
                model.setData(other, value, QtCore.Qt.EditRole)
                model.rows[row][index.column()]['value'] = value
        elif self.what == utils.FLOAT:
            model.setData(index, value, QtCore.Qt.EditRole)
            model.rows[index.row()][index.column()]['value'] = value
            changeValue(self.main, value)
        elif self.what == utils.INTEGER:
            model.setData(index, value, QtCore.Qt.EditRole)
            model.rows[index.row()][index.column()]['value'] = value
            changeValue(self.main, value)
        else:
            model.setData(index, value, QtCore.Qt.EditRole)
            model.rows[index.row()][index.column()]['value'] = value
        """
        if mustResizeRowsToContents:
            self.main.view.resizeRowsToContents()
            if self.main.viewType == utils.VIEW_BULLETIN:
                minHeight = 4 * self.main.fontSizeSlider.value()
            elif self.main.viewType == utils.VIEW_APPRECIATIONS:
                minHeight = 5 * self.main.fontSizeSlider.value()
            for i in range(self.main.model.rowCount()):
                if self.main.view.rowHeight(i) < minHeight:
                    self.main.view.setRowHeight(i, minHeight)
        """

    def doAppreciationChanged(self):
        """
        appellée chaque fois qu'on modifie l'appréciation
        mais on n'enregistre qu'à la fin
        """
        appreciation = self.editor.toPlainText()
        if appreciation != self.main.me.get('actualAppreciation', ''):
            self.main.me['actualAppreciation'] = appreciation
            if self.main.viewType != utils.VIEW_SUIVIS:
                self.main.me['mustSaveActualAppreciation'] = True





