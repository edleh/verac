# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    blablabla
"""

# importation des modules utiles :
from __future__ import division, print_function
import os

import utils, utils_functions, utils_webengine

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtPrintSupport
else:
    from PyQt4 import QtCore, QtGui as QtWidgets
    from PyQt4 import QtGui as QtPrintSupport



# variables globales pour savoir si ça marche :
PDF_OK = False
WKHTMLTOPDF_BIN = ''
def changeGlobals(newValue0, newValue1):
    global PDF_OK, WKHTMLTOPDF_BIN
    PDF_OK = newValue0
    WKHTMLTOPDF_BIN = newValue1

PDF_GENERATED = False
PRINT_WEBVIEW = None

def searchWkhtmltopdf():
    """
    D'une part on teste si wkhtmltopdf est installé, on récupère les chemins
    et on les ajoute au path.
    Si c'est Windows, il faut vérifier aussi la base de registre.
    """
    global PDF_OK, WKHTMLTOPDF_BIN
    PDF_OK = False
    WKHTMLTOPDF_BIN = ''

    if utils.OS_NAME[0] == 'win':
        # on cherche où est wkhtmltopdf via la base de registre :
        if utils.PYTHONVERSION >= 30:
            import winreg
        else:
            import _winreg as winreg

        fileName = ''
        if fileName == '':
            try:
                # version récente :
                key = winreg.OpenKey(
                    winreg.HKEY_LOCAL_MACHINE, 'SOFTWARE\\wkhtmltopdf')
                fileName = winreg.QueryValueEx(key, 'PdfPath')[0]
                winreg.CloseKey(key)
            except:
                pass
        if fileName == '':
            try:
                # vieille version :
                key = winreg.OpenKey(
                    winreg.HKEY_CURRENT_USER, 'Software\\wkhtmltopdf')
                fileName = winreg.QueryValueEx(key, '')[0]
                winreg.CloseKey(key)
                if fileName != '':
                    fileName = os.path.join(fileName, 'wkhtmltopdf.exe')
            except:
                pass
        if fileName != '':
            if os.path.exists(fileName):
                PDF_OK = True
                WKHTMLTOPDF_BIN = fileName
    else:
        WKHTMLTOPDF_BIN = 'wkhtmltopdf'
        possiblePaths = ('/usr/local/bin', '/opt', '/usr/bin', )
        for possiblePath in possiblePaths:
            fileName = os.path.join(possiblePath, 'wkhtmltopdf')
            if os.path.exists(fileName):
                WKHTMLTOPDF_BIN = fileName
                PDF_OK = True
                break




def htmlToPdf_wkhtmltopdf(main, htmlFileName, pdfFileName, footer=('', '', False), orientation='Portrait'):
    """
    Création d'un fichier pdf en utilisant wkhtmltopdf
    """
    if not(PDF_OK):
        return False

    appPath = WKHTMLTOPDF_BIN
    args = [
        '--load-error-handling', 'ignore', 
        '--enable-local-file-access', 
        '--orientation', orientation]
    if footer != ('', '', False):
        args.extend(['--footer-font-size', '8'])
        if footer[0] != '':
            args.extend(['--footer-left', footer[0]])
        if footer[1] != '':
            args.extend(['--footer-center', footer[1]])
        if footer[2] != False:
            args.extend(['--footer-right', 'Page [page] / [toPage]'])
    args.extend([htmlFileName, pdfFileName])
    if utils.ICI:
        print('**************************************************************')
        print(appPath, ' '.join(args))
        print('**************************************************************')
    process = QtCore.QProcess(main)
    process.start(appPath, args)
    if not process.waitForStarted(3000):
        QtCore.qDebug('BUG IN wkhtmltopdf process')
        return False

    if not process.waitForFinished():
        return False

    utils_functions.myPrint('OK')
    return True



def htmlToPdf(main, htmlFileName, pdfFileName, footer=('', '', False), orientation='Portrait'):
    """
    Création d'un fichier pdf.
    Si wkhtmltopdf est installé, on l'utilise ; sinon, on utilise un WebEngineView.
    Le WebEngineView permet tout de même la fabrication des bulletins, 
    mais il n'auront pas de pieds de pages (et des sauts de pages moins bien gérés).
    """
    if orientation != 'Portrait':
        orientation = 'Landscape'
    if PDF_OK:
        if htmlToPdf_wkhtmltopdf(main, htmlFileName, pdfFileName, footer, orientation):
            return True

    def convertItWebKit():
        global PDF_GENERATED
        PRINT_WEBVIEW.print_(printer)
        PDF_GENERATED = True

    def convertItWebEngine():
        pageLayout = QtGui.QPageLayout(
            QtGui.QPageSize(QtGui.QPageSize.A4), 
            QtGui.QPageLayout.Portrait, 
            QtCore.QMarginsF(10, 10, 10, 15), 
            QtGui.QPageLayout.Millimeter, 
            QtCore.QMarginsF(10, 10, 10, 15))
        if orientation == 'Landscape':
            pageLayout.setOrientation(QtGui.QPageLayout.Landscape)
        PRINT_WEBVIEW.page().printToPdf(pdfFileName, pageLayout)

    def printingFinished(ok=None):
        global PDF_GENERATED
        PDF_GENERATED = True

    global PDF_GENERATED, PRINT_WEBVIEW
    PDF_GENERATED = False

    PRINT_WEBVIEW = utils_webengine.MyWebEngineView(
        main, linksInBrowser=True)

    if utils_webengine.WEB_ENGINE == 'WEBENGINE':
        PRINT_WEBVIEW.page().pdfPrintingFinished.connect(printingFinished)
        PRINT_WEBVIEW.loadFinished.connect(convertItWebEngine)
    else:
        printer = QtPrintSupport.QPrinter()
        printer.setPageSize(QtPrintSupport.QPrinter.A4)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        if orientation == 'Portrait':
            printer.setOrientation(QtPrintSupport.QPrinter.Portrait)
        else:
            printer.setOrientation(QtPrintSupport.QPrinter.Landscape)
        printer.setOutputFileName(pdfFileName)
        PRINT_WEBVIEW.loadFinished.connect(convertItWebKit)

    url = QtCore.QUrl().fromLocalFile(htmlFileName)
    PRINT_WEBVIEW.load(url)

    while not(PDF_GENERATED):
        QtWidgets.QApplication.processEvents()
    del(PRINT_WEBVIEW)
    PRINT_WEBVIEW = None
    return True




def webPageToPdf(main, url, pdfFileName, orientation='Portrait'):
    """
    Création d'un fichier pdf à partir d'une url.
    On utilise un WebEngineView.
    """

    def convertItWebKit():
        global PDF_GENERATED
        PRINT_WEBVIEW.print_(printer)
        PDF_GENERATED = True

    def convertItWebEngine():
        pageLayout = QtGui.QPageLayout(
            QtGui.QPageSize(QtGui.QPageSize.A4), 
            QtGui.QPageLayout.Portrait, 
            QtCore.QMarginsF(10, 10, 10, 15), 
            QtGui.QPageLayout.Millimeter, 
            QtCore.QMarginsF(10, 10, 10, 15))
        if orientation == 'Landscape':
            pageLayout.setOrientation(QtGui.QPageLayout.Landscape)
        PRINT_WEBVIEW.page().printToPdf(pdfFileName, pageLayout)

    def printingFinished(ok=None):
        global PDF_GENERATED
        PDF_GENERATED = True

    global PDF_GENERATED, PRINT_WEBVIEW
    PDF_GENERATED = False
    if orientation != 'Portrait':
        orientation = 'Landscape'

    PRINT_WEBVIEW = utils_webengine.MyWebEngineView(
        main, linksInBrowser=True)

    if utils_webengine.WEB_ENGINE == 'WEBENGINE':
        PRINT_WEBVIEW.page().pdfPrintingFinished.connect(printingFinished)
        PRINT_WEBVIEW.loadFinished.connect(convertItWebEngine)
    else:
        printer = QtPrintSupport.QPrinter()
        printer.setPageSize(QtPrintSupport.QPrinter.A4)
        printer.setOutputFormat(QtPrintSupport.QPrinter.PdfFormat)
        if orientation == 'Portrait':
            printer.setOrientation(QtPrintSupport.QPrinter.Portrait)
        else:
            printer.setOrientation(QtPrintSupport.QPrinter.Landscape)
        printer.setOutputFileName(pdfFileName)
        PRINT_WEBVIEW.loadFinished.connect(convertItWebKit)

    PRINT_WEBVIEW.load(QtCore.QUrl(url))

    while not(PDF_GENERATED):
        QtWidgets.QApplication.processEvents()
    del(PRINT_WEBVIEW)
    PRINT_WEBVIEW = None
    return True



