# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Une interface pour éditer des fichiers csv
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions
import utils_markdown

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



"""
###########################################################
###########################################################

                CsvEditor

###########################################################
###########################################################
"""

# une valeur pour les limites des spinBox :
MAXINTEGER = 1000000



class CsvItemDelegate(QtWidgets.QItemDelegate):
    """
    Un délégate pour gérer les éditions des cellules.
    Selon le type de valeur, on utilise un QSpinBox,
        un QLineEdit ou un QTextEdit.
    """
    def __init__(self, parent=None, model=None, 
                 minInt=-MAXINTEGER, maxInt=MAXINTEGER):
        super(CsvItemDelegate, self).__init__(parent)
        # on a besoin de récupérer le model :
        self.model = model
        # what est le type de donnée :
        self.what = utils.INDETERMINATE
        # on peut avoir indiqué les min et max pour les entiers :
        self.minInt = minInt
        self.maxInt = maxInt

    def createEditor(self, parent, option, index):
        # selon le type de donnée (what)
        # on utilise un objet différent :
        row = index.row()
        column = index.column()
        self.what = self.model.datas[row][column][1]
        if self.what == utils.INTEGER:
            editor = QtWidgets.QSpinBox(parent)
            editor.setMinimum(self.minInt)
            editor.setMaximum(self.maxInt)
        elif self.what == utils.FLOAT:
            editor = QtWidgets.QDoubleSpinBox(parent)
            editor.setMinimum(float(self.minInt))
            editor.setMaximum(float(self.maxInt))
        elif self.what == utils.LINE:
            editor = QtWidgets.QLineEdit(parent)
        else:
            editor = QtWidgets.QTextEdit(parent)
        return editor

    def setEditorData(self, editor, index):
        row = index.row()
        column = index.column()
        value = self.model.datas[row][column][0]
        if self.what == utils.INTEGER:
            editor.setValue(value)
        elif self.what == utils.FLOAT:
            editor.setValue(value)
        elif self.what == utils.LINE:
            editor.setText(value)
        else:
            editor.setText(value)

    def setModelData(self, editor, model, index):
        if self.what == utils.INTEGER:
            editor.interpretText()
            value = editor.value()
        elif self.what == utils.FLOAT:
            editor.interpretText()
            value = editor.value()
        elif self.what == utils.LINE:
            value = editor.text()
        else:
            value = editor.toPlainText()
        # mise à jour de l'affichage du model :
        model.setData(index, value, QtCore.Qt.EditRole)
        # mise à jour des données :
        row = index.row()
        column = index.column()
        model.datas[row][column][0] = value

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)



class CsvTableModel(QtGui.QStandardItemModel):
    """
    CsvTableModel est un model pour lire et afficher un fichier csv.
    variable utiles :
        * headers : les titres des colonnes
        * datas : les données [valeur, type]
        * emptyRow : une ligne vide pour les ajouts de lignes
    """
    def __init__(self, parent=None, csvFileName=''):
        super(CsvTableModel, self).__init__(parent)

        self.csvFileName = csvFileName
        self.headers, self.datas, self.emptyRow = [], [], []
        self.setupModelData()

    def setupModelData(self):
        # lecture et récupération du fichier csv :
        import utils_export
        self.headers, self.datas = utils_export.csv2List(self.csvFileName)
        nbCols = len(self.headers)
        nbRows = len(self.datas)
        self.setColumnCount(nbCols)
        self.setRowCount(nbRows)

        # on récupère les titres des colonnes :
        for col in range(nbCols):
            self.setHeaderData(
                col, QtCore.Qt.Horizontal, utils_functions.u(self.headers[col]))
            # et on initialise emptyRow :
            self.emptyRow.append([None, utils.INDETERMINATE])

        # on récupère les données :
        # intCols contiendra les colonnes qui sont du type INTEGER 
        # et textCols celles qui sont de type TEXT :
        intCols, textCols = [], []
        for row in range(nbRows):
            for col in range(nbCols):
                data = self.datas[row][col]
                if utils.PYTHONVERSION >= 30:
                    if (data == '') or (data == None):
                        self.datas[row][col] = [data, utils.LINE]
                    elif isinstance(data, (int, float)):
                        if int(data) == data:
                            intCols.append(col)
                        self.datas[row][col] = [data, utils.FLOAT]
                        if self.emptyRow[col][1] == utils.INDETERMINATE:
                            # on met 0 dans la ligne vide :
                            self.emptyRow[col] = [0.0, utils.FLOAT]
                    else:
                        # on repère s'il y a des multi-lignes dans la colonne :
                        if data.find('\n') > -1:
                            textCols.append(col)
                        # pour l'instant on met en LINE :
                        data = utils_functions.u(data)
                        self.datas[row][col] = [data, utils.LINE]
                        if self.emptyRow[col][1] == utils.INDETERMINATE:
                            # on met '' dans la ligne vide :
                            self.emptyRow[col] = ['', utils.LINE]
                else:
                    if (data == '') or (data == None):
                        self.datas[row][col] = [data, utils.LINE]
                    elif isinstance(data, (int, long, float)):
                        if int(data) == data:
                            intCols.append(col)
                        self.datas[row][col] = [data, utils.FLOAT]
                        if self.emptyRow[col][1] == utils.INDETERMINATE:
                            # on met 0 dans la ligne vide :
                            self.emptyRow[col] = [0.0, utils.FLOAT]
                    else:
                        # on repère s'il y a des multi-lignes dans la colonne :
                        if data.find('\n') > -1:
                            textCols.append(col)
                        # pour l'instant on met en LINE :
                        data = utils_functions.u(data)
                        self.datas[row][col] = [data, utils.LINE]
                        if self.emptyRow[col][1] == utils.INDETERMINATE:
                            # on met '' dans la ligne vide :
                            self.emptyRow[col] = ['', utils.LINE]
                self.setData(self.index(row, col, QtCore.QModelIndex()), data)

        # on ne s'occupe que maintenant des colonnes de type INTEGER et TEXT
        # car il n'y a pas forcément de saut de ligne dans chaque cellule
        for col in  intCols:
            for row in range(nbRows):
                try:
                    self.datas[row][col][0] = int(self.datas[row][col][0])
                except:
                    self.datas[row][col][0] = None
                self.datas[row][col][1] = utils.INTEGER
                self.emptyRow[col] = [0, utils.INTEGER]
        for col in  textCols:
            for row in range(nbRows):
                self.datas[row][col][1] = utils.TEXT
                self.emptyRow[col] = ['', utils.TEXT]

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column, row = index.column(), index.row()
        if role == QtCore.Qt.TextAlignmentRole:
            # alignement :
            what = self.datas[row][column][1]
            if what in utils.TYPES_ALIGN_LEFT:
                return QtCore.Qt.AlignLeft
            else:
                return QtCore.Qt.AlignRight
        elif role == QtCore.Qt.DisplayRole:
            # affichage de la case :
            return self.datas[row][column][0]
        else:
            return

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.datas)





class CsvEditorDlg(QtWidgets.QDialog):
    """
    Un dialog pour embarquer tout ça
    """
    def __init__(self, parent=None, csvFileName='', 
                 minInt=-MAXINTEGER, maxInt=MAXINTEGER):
        super(CsvEditorDlg, self).__init__(parent)
        utils_functions.doWaitCursor()
        try:
            self.main = parent
            self.minInt = minInt
            self.maxInt = maxInt

            # le titre de la fenêtre :
            title = QtWidgets.QApplication.translate('main', 'CSV file editor')
            self.setWindowTitle(title)

            # on passe le nom du fichier au model :
            self.csvFileName = csvFileName
            self.model = CsvTableModel(self, csvFileName)

            # un QTableView pour l'affichage :
            self.csvView = QtWidgets.QTableView()
            self.csvView.setModel(self.model)
            self.csvView.resizeColumnsToContents()
            self.csvView.resizeRowsToContents()
            self.csvView.setAlternatingRowColors(True)
            for i in range(self.model.columnCount()):
                if self.csvView.columnWidth(i) > 300:
                    self.csvView.setColumnWidth(i, 300)

            # un delegate pour l'édition :
            self.csvView.setItemDelegate(
                CsvItemDelegate(self, self.model, self.minInt, self.maxInt))

            if utils.PYQT == 'PYQT5':
                selectionModel = QtCore.QItemSelectionModel(self.model)
            else:
                selectionModel = QtGui.QItemSelectionModel(self.model)
            self.csvView.setSelectionModel(selectionModel)
            selectionModel.selectionChanged.connect(self.updateActions)

            # action insertRow :
            insertText = QtWidgets.QApplication.translate('main', 'Insert a row')
            insertIcon = utils.doIcon('list-add')
            self.insertRowAction = QtWidgets.QAction(insertText, self, icon=insertIcon)
            self.insertRowAction.triggered.connect(self.insertRow)
            # action removeRow :
            removeText = QtWidgets.QApplication.translate('main', 'Remove a row')
            removeIcon = utils.doIcon('list-remove')
            self.removeRowAction = QtWidgets.QAction(removeText, self, icon=removeIcon)
            self.removeRowAction.triggered.connect(self.removeRow)
            # on les met dans un toolBar :
            toolBar = QtWidgets.QToolBar()
            toolBar.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
            toolBar.addAction(self.insertRowAction)
            toolBar.addAction(self.removeRowAction)

            # un texte d'aide :
            mdHelpView = utils_markdown.MarkdownHelpViewer(
                self.main, mdFile='translations/helpCsvEditor')

            dialogButtons = utils_functions.createDialogButtons(self)
            buttonBox = dialogButtons[0]

            self.statusBar = QtWidgets.QStatusBar()

            # on place tout dans une grille :
            grid = QtWidgets.QGridLayout()
            grid.addWidget(mdHelpView,     0, 0)
            grid.addWidget(toolBar,         1, 0)
            grid.addWidget(self.csvView, 2, 0)
            grid.addWidget(buttonBox,       5, 0, 1, 2)
            grid.addWidget(self.statusBar,  6, 0)
            # la grille est le widget de base :
            self.setLayout(grid)

            self.updateActions()
        finally:
            utils_functions.restoreCursor()


    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('csv-editor', 'admin')

    def insertRow(self):
        index = self.csvView.currentIndex()
        if index.isValid():
            row = index.row()
            column = index.column()
        else:
            row, column = 0, 0
        if self.model.insertRow(row):
            row += 1
            self.model.datas.insert(row, self.model.emptyRow)
        index = self.model.index(row, column)
        self.csvView.setFocus()
        self.csvView.setCurrentIndex(index)
        self.updateActions()

    def removeRow(self):
        index = self.csvView.currentIndex()
        if not index.isValid():
            return
        row = index.row()
        column = index.column()
        if (self.model.removeRow(row)):
            self.model.datas.pop(row)
            if row > self.model.rowCount() - 1:
                row -= 1

        index = self.model.index(row, column)
        self.csvView.setFocus()
        self.csvView.setCurrentIndex(index)
        self.updateActions()

    def updateActions(self):
        hasSelection = not self.csvView.selectionModel().selection().isEmpty()
        self.removeRowAction.setEnabled(hasSelection)
        hasCurrent = self.csvView.selectionModel().currentIndex().isValid()
        if hasCurrent:
            self.csvView.closePersistentEditor(
                self.csvView.selectionModel().currentIndex())
            row = self.csvView.selectionModel().currentIndex().row()
            column = self.csvView.selectionModel().currentIndex().column()
            data = self.model.datas[row][column][0]
            if self.model.datas[row][column][1] == utils.INTEGER:
                data = int(data)
            elif self.model.datas[row][column][1] == utils.FLOAT:
                data = float(data)
            message = QtWidgets.QApplication.translate(
                'main', 'Position : ({0},{1})   Value : {2}')
            self.statusBar.showMessage(utils_functions.u(message).format(row, column, data))


    def accept(self):
        utils_functions.doWaitCursor()
        try:
            import csv
            if utils.PYTHONVERSION >= 30:
                theFile = open(
                    self.csvFileName, 'w', newline='', encoding='utf-8')
            else:
                theFile = open(self.csvFileName, 'wb')
            writer = csv.writer(
                theFile, delimiter=';', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)

            # on écrit les titres :
            ligne = []
            for hearder in self.model.headers:
                ligne.append(utils_functions.s(hearder))
            writer.writerow(ligne)

            # puis les lignes :
            for row in self.model.datas:
                ligne = []
                for data in row:
                    if data[0] == None:
                        ligne.append(data[0])
                    elif data[1] == utils.INTEGER:
                        ligne.append(int(data[0]))
                    elif data[1] == utils.FLOAT:
                        ligne.append(float(data[0]))
                    else:
                        ligne.append(utils_functions.s(data[0]))
                writer.writerow(ligne)
        except:
            message = QtWidgets.QApplication.translate('main', 'Failed to save {0}').format(
                self.csvFileName)
            utils_functions.afficheMsgPb(self.main, message)
        finally:
            theFile.close()
            utils_functions.restoreCursor()
            QtWidgets.QDialog.accept(self)


