# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
Module contenant tout ce qui permet de manipuler le web :
tester la connexion internet, downloader, ...
"""

# importation des modules utiles :
from __future__ import division, print_function

import utils, utils_functions, utils_webengine

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtNetwork
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4 import QtNetwork



###########################################################"
#   VARIABLES GLOBALES
###########################################################"

STATE_WAITING = 0
STATE_LAUNCHED = 1
STATE_FINISHED = 2
STATE_OK = 3
STATE_ERROR = -1
STATE_NONET = -2
STATE_CANCELED = -3







###########################################################"
#   ARGUMENT HTTPS ET DOURL
#   (pb windows et proxy du collège avec https)
###########################################################"

HTTPS = QtNetwork.QSslSocket.supportsSsl()
print('HTTPS : ', HTTPS)

def doUrl(url):
    """
    pas de https sous Windows !!!
    """
    if utils.PYTHONVERSION >= 30:
        if not(isinstance(url, str)):
            url = 'https://verac.tuxfamily.org/site/main-todo.html'
    else:
        if not(isinstance(url, (unicode, str))):
            url = 'https://verac.tuxfamily.org/site/main-todo.html'
    if not(HTTPS):
        url = url.replace('https', 'http')
    #print('doUrl:', url)
    return QtCore.QUrl(url)






###########################################################"
#   CONNEXION AUTOMATIQUE AU SITE
###########################################################"

SITE_ETAB_CONNECTED = False

def connectToSiteEtab(main):
    """
    Se connecte au site de l'établissement dans l'interface de VÉRAC
    """
    global SITE_ETAB_CONNECTED
    if SITE_ETAB_CONNECTED:
        return

    def loadFinished():
        global SITE_ETAB_CONNECTED
        if not(SITE_ETAB_CONNECTED):
            js = utils_functions.u(
                "var login = document.getElementById('login');"
                "var password = document.getElementById('password');"
                "var go = document.getElementById('go');"
                "login.value = '{0}';"
                "password.value = '{1}';"
                "go.click();").format(main.me['user'], main.me['Mdp'])
            main.siteWebView.page().runJavaScript(js)
            SITE_ETAB_CONNECTED = True

    url = utils_functions.addSlash(main.siteUrlPublic)
    #url = 'http://127.0.0.1:8000/'
    if utils_webengine.WEB_ENGINE == 'WEBENGINE':
        main.siteWebView.load(doUrl(url))
        main.siteWebView.loadFinished.connect(loadFinished)
    else:
        postData = utils_functions.u(
            'action=verac&login={0}&password={1}').format(
                main.me['user'], main.me['Mdp'])
        loadPostUrl(main, main.siteWebView, url, postData)
        SITE_ETAB_CONNECTED = True

def loadPostUrl(main, webView, url, postData):
    """
    affiche une page web dans un WebView
    avec envoi de données (postData).
    Utilisé par connectToSiteEtab
    """
    request = QtNetwork.QNetworkRequest(doUrl(url))
    request.setHeader(
        QtNetwork.QNetworkRequest.ContentTypeHeader, 
        'application/x-www-form-urlencoded')
    webView.load(
        request, 
        QtNetwork.QNetworkAccessManager.PostOperation, 
        utils_functions.s(postData, encoding='utf8'))







###########################################################"
#   HTTPCLIENT ET UPDOWNLOADER
###########################################################"

class HttpClient(QtCore.QObject):
    """
    Une classe pour dialoguer avec http
    (fichiers html ou php)
    """
    def __init__(self, parent=None):
        super(HttpClient, self).__init__(parent)
        self.main = parent

        self.manager = QtNetwork.QNetworkAccessManager(self.main)
        self.reply = None
        self.reponse = ''
        self.theUrl = ''

        self.state = STATE_WAITING
        self.timeDepart = QtCore.QTime.currentTime()

    def launch(self, theUrl='', postData=''):
        utils_functions.doWaitCursor()

        self.reply = None
        self.reponse = ''
        self.theUrl = theUrl

        request = QtNetwork.QNetworkRequest(doUrl(theUrl))

        try:
            host = theUrl.split('://')[1]
        except:
            host = ''
        host = host.split('/')[0]
        request.setRawHeader(
            QtCore.QByteArray().append('Host'),
            QtCore.QByteArray().append(host))
        request.setRawHeader(
            QtCore.QByteArray().append('User-Agent'), 
            QtCore.QByteArray().append('MyOwnBrowser 1.0'))
        contentType = 'application/x-www-form-urlencoded'
        request.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader, contentType)

        if len(postData) > 0:
            self.reply = self.manager.post(
                request, utils_functions.s(postData, encoding='utf8'))
        else:
            self.reply = self.manager.get(request)
        self.reply.finished.connect(self.finished)
        self.reply.error.connect(self.error)

        self.timeDepart = QtCore.QTime.currentTime()
        self.state = STATE_LAUNCHED

    def finished(self):
        utils_functions.restoreCursor()
        self.reponse = self.reply.readAll()
        if utils.PYTHONVERSION >= 30:
            self.reponse = utils_functions.u(self.reponse)
        if utils.ICI:
            print('HttpClient.reponse :', self.theUrl)
            print(self.reponse)
        if self.state == STATE_LAUNCHED:
            self.state = STATE_OK
        if self.state != STATE_OK:
            self.state = STATE_NONET
            self.main.NetOK = False
            message = QtWidgets.QApplication.translate('main', 'INTERNET CONNECTION PROBLEM!')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p align="center">{2}</p>').format(utils.SEPARATOR_LINE, message, self.theUrl)
            utils_functions.restoreCursor()
            utils_functions.messageBox(self.main, message=message)

    def error(self, code):
        utils_functions.afficheMessage(
            self.main, 'error in HttpClient : {0}'.format(code))
        utils_functions.restoreCursor()
        self.state = STATE_NONET
        self.main.NetOK = False
        utils_functions.restoreCursor()

    def cancel(self):
        if self.state != STATE_LAUNCHED:
            return
        self.reply.abort()
        self.main.NetOK = False
        self.state = STATE_CANCELED



class UpDownLoader(QtCore.QObject):
    """
    Une classe pour gérer les envois et téléchargements en http
    La variable ext sert à modifier l'extension du fichier pendant le transfert.
        ext='.png' par exemple ajoutera .png à la fin du nom du fichier.
        (PB DELAUNAY)
    La variable self.state indique l'état du UpDownLoader.
    """
    def __init__(self, parent=None, fileName='', localDir='', netDir='', 
                 down=True, ext='', showProgress=True):
        super(UpDownLoader, self).__init__(parent)

        self.main = parent
        self.theFile = None
        self.down = down
        fileNameWithExt = utils_functions.u('{0}{1}').format(fileName, ext)
        fileName = utils_functions.u(fileName)
        localDir = utils_functions.u(localDir)
        netDir = utils_functions.u(netDir)
        self.theFileName = localDir + fileName
        if down:
            self.theUrlName = netDir + fileNameWithExt
            self.theUrl = doUrl(netDir + fileNameWithExt)
        else:
            self.theUrlName = netDir
            self.theUrl = doUrl(netDir)
        self.showProgress = showProgress

        self.manager = QtNetwork.QNetworkAccessManager(self)
        self.reply = None
        self.reponse = ''

        if self.showProgress:
            self.progressDialog = QtWidgets.QProgressDialog(self.main)
            self.progressDialog.setWindowTitle(utils.PROGNAME)
            if down:
                m1 = QtWidgets.QApplication.translate('main', 'Downloading {0}.')
                text = utils_functions.u(m1).format(fileName)
            else:
                m1 = QtWidgets.QApplication.translate('main', 'Uploading {0}.')
                text = utils_functions.u(m1).format(fileName)
            self.progressDialog.setLabelText(text)
            self.progressDialog.canceled.connect(self.cancel)
        self.state = STATE_WAITING

    def launch(self):
        utils_functions.afficheStatusBar(self.main)
        if self.down:
            m1 = QtWidgets.QApplication.translate('main', 'download:')
            m1 = utils_functions.u('{0} {1}').format(m1, self.theFileName)
            utils_functions.afficheMessage(self.main, m1, statusBar=True)
            QtCore.QFile(self.theFileName).remove()
            self.theFile = QtCore.QFile(self.theFileName, self)
            self.theFile.open(QtCore.QIODevice.WriteOnly)
            request = QtNetwork.QNetworkRequest(self.theUrl)
            request.setRawHeader(
                QtCore.QByteArray().append('User-Agent'), 
                QtCore.QByteArray().append('MyOwnBrowser 1.0'))
            self.reply = self.manager.get(request)
            self.reply.finished.connect(self.finished)
            self.reply.downloadProgress.connect(self.downloadProgress)
        else:
            import random
            m1 = QtWidgets.QApplication.translate('main', 'upload:')
            m1 = utils_functions.u('{0} {1}').format(m1, self.theFileName)
            utils_functions.afficheMessage(self.main, m1, statusBar=True)
            request = QtNetwork.QNetworkRequest(self.theUrl)

            theFileMime = 'application/x-sqlite3'
            theFileFieldName = 'fichier'

            host = self.theUrlName.split('://')[1].split('/')[0]
            crlf = '\r\n'
            maxVal = QtCore.QDateTime.currentDateTime().toTime_t()
            randomNumber = random.randint(0, maxVal)\
                + random.randint(0, maxVal) + random.randint(0, maxVal)
            boundary = '---------------------------{0}'.format(randomNumber)
            contentType = 'multipart/form-data; boundary={0}'.format(boundary)
            beginBoundary = '--{0}{1}'.format(boundary, crlf)
            endBoundary = '{1}--{0}--{1}'.format(boundary, crlf)

            f = QtCore.QFile(self.theFileName)
            f.open(QtCore.QIODevice.ReadOnly)
            self.theFile = f.readAll()
            f.close()

            send = QtCore.QByteArray()
            send.append(beginBoundary)
            text = 'Content-Disposition: form-data; name="{0}"; filename="{1}"{2}'.format(
                theFileFieldName, self.theFileName, crlf)
            send.append(utils_functions.s(text))
            text = 'Content-Type: {0}{1}{1}'.format(theFileMime, crlf)
            send.append(utils_functions.s(text))
            send.append(self.theFile)
            send.append(endBoundary)

            request.setRawHeader(
                QtCore.QByteArray().append('Host'), 
                QtCore.QByteArray().append(host))
            request.setRawHeader(
                QtCore.QByteArray().append('User-Agent'), 
                QtCore.QByteArray().append('MyOwnBrowser 1.0'))
            request.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader, contentType)
            request.setHeader(QtNetwork.QNetworkRequest.ContentLengthHeader, send.size())

            self.reply = self.manager.post(request, send)
            self.reply.finished.connect(self.finished)
            self.reply.uploadProgress.connect(self.uploadProgress)
        self.state = STATE_LAUNCHED

    def finished(self):
        self.state = STATE_FINISHED
        if self.showProgress:
            self.progressDialog.close()
        if self.down:
            self.reponse = self.reply.readAll()
            if not(QtCore.QByteArray().append('Error 404') in self.reponse):
                self.theFile.write(self.reponse)
                self.theFile.close()
            else:
                self.theFile.close()
                QtCore.QFile(self.theFileName).remove()
            m1 = QtWidgets.QApplication.translate('main', 'finished DOWN')
            utils_functions.afficheMessage(self.main, m1, statusBar=True)
        else:
            self.reponse = self.reply.readAll()
            m1 = QtWidgets.QApplication.translate('main', 'finished UP')
            utils_functions.afficheMessage(self.main, m1, statusBar=True)
        if self.reply.error() == QtNetwork.QNetworkReply.NoError:
            self.state = STATE_OK
        else:
            utils_functions.myPrint('NETWORK ERROR : ', self.reply.error())
            self.state = STATE_ERROR

    def downloadProgress(self, bytesReceived, bytesTotal):
        if self.showProgress:
            self.progressDialog.setMaximum(bytesTotal)
            self.progressDialog.setValue(bytesReceived)

    def uploadProgress(self, bytesSent, bytesTotal):
        if self.showProgress:
            self.progressDialog.setMaximum(bytesTotal)
            self.progressDialog.setValue(bytesSent)

    def cancel(self):
        if self.state != STATE_LAUNCHED:
            return
        self.reply.abort()
        if self.down:
            m1 = QtWidgets.QApplication.translate('main', 'cancel DOWN')
            utils_functions.afficheMessage(self.main, m1, statusBar=True)
            self.theFile.close()
            QtCore.QFile(self.theFileName).remove()
        else:
            m1 = QtWidgets.QApplication.translate('main', 'cancel UP')
            utils_functions.afficheMessage(self.main, m1, statusBar=True)
        self.state = STATE_CANCELED



def downloadFileFromSecretDir(main, fromDir, fileName, toDir='', ext='', showProgress=True):
    """
    Fonction pour télécharger un fichier situé dans le dossier secret.
    Interroge en php.

    D'abord on envoie le nom du fichier + login + mdp à verac_getFile.php
    verac_getFile.php vérifie login + mdp
    si OK, verac_getFile.php déplace le fichier dans 
        $fichierTmp = $_SESSION['siteUrlPublic']."/tmp/".$fileName;
        Donc c'est un fichier temporaire
        et répond OK
    téléchargement du fichier main.siteUrlPublic + "/tmp/" + fileName

    Une fois terminé, on reposte un message à verac_clearTemp.php avec fileName
        verac_clearTemp.php supprime le fichier $fichierTmp

    Le fichier est d'abord téléchargé dans le dossier temporaine (main.tempPath + '/down/')
        si toDir a été précisé, on le copie ensuite dans toDir.

    La variable ext sert à modifier l'extension du fichier pendant le transfert.
    ext='.png' par exemple ajoutera .png à la fin du nom du fichier.
    (PB DELAUNAY)
    """
    import utils_filesdirs
    utils_functions.doWaitCursor()
    try:
        downloadOK = False
        # d'abord on envoie le nom du fichier + login+mdp à verac_getFile.php:
        theUrl = '{0}/pages/verac_getFile.php'.format(main.siteUrlPublic)
        postData = utils_functions.u(
            'login={0}&password={1}&fromDir={2}&fileName={3}&ext={4}').format(
                main.me['user'], main.me['Mdp'], fromDir, fileName, ext)
        httpClient = HttpClient(main)
        httpClient.launch(theUrl, postData)
        while httpClient.state == STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if httpClient.state == STATE_OK:
            # téléchargement du fichier temporaire:
            theUrl = '{0}/tmp/'.format(main.siteUrlPublic)
            theDir = '{0}/down/'.format(main.tempPath)
            # récupération de la variable ext retournée par verac_getFile.php :
            try:
                ext = httpClient.reponse.split('|')[3]
            except:
                ext = ''
            downLoader = UpDownLoader(
                main, fileName, theDir, theUrl, down=True, ext=ext, showProgress=showProgress)
            downLoader.launch()
            while downLoader.state == STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if downLoader.state == STATE_OK:
                # on envoie le nom du fichier à verac_clearTemp.php:
                theUrl = '{0}/pages/verac_clearTemp.php'.format(main.siteUrlPublic)
                postData = utils_functions.u('fileName={0}{1}').format(fileName, ext)
                httpClient.launch(theUrl, postData)
                while httpClient.state == STATE_LAUNCHED:
                    QtWidgets.QApplication.processEvents()
                if httpClient.state == STATE_OK:
                    downloadOK = True
                # on copie le fichier si besoin (toDir indiqué) :
                if toDir != '':
                    toDir = utils_functions.addSlash(toDir)
                    utils_filesdirs.removeAndCopy(
                        utils_functions.u('{0}/down/{1}').format(main.tempPath, fileName), 
                        utils_functions.u('{0}{1}').format(toDir, fileName))
                downloadOK = True
            else:
                downLoader.progressDialog.close()
    finally:
        utils_functions.restoreCursor()
        return downloadOK



###########################################################"
#   DIVERS
###########################################################"

def testNet(main, theUrl):
    """
    Pour vérifier si l'url est joignable.
    On passe par un objet HttpClient pour passer l'éventuel proxy.
    """
    utils_functions.afficheMessage(main, 'testNet: {0}'.format(theUrl))
    if theUrl == '':
        return False
    result = False
    testNetDelay = utils.testNetDelay * 1000
    httpClient = HttpClient(main)
    httpClient.launch(theUrl)
    while httpClient.state == STATE_LAUNCHED:
        dureeTotale = httpClient.timeDepart.msecsTo(
            QtCore.QTime.currentTime())
        if dureeTotale > testNetDelay:
            httpClient.cancel()
        QtWidgets.QApplication.processEvents()
    if httpClient.state == STATE_OK:
        try:
            utils_functions.myPrint('Net OK')
            result = True
        except:
            pass
    if not(result):
        utils_functions.afficheMsgPb(main, 'No Net', withMessageBox=False)
    return result


def gotoSiteEtab(main):
    """
    Affiche le site de l'établissement dans le navigateur
    """
    utils_functions.openInBrowser(main.siteUrlPublic)





###########################################################"
#   POUR LES MISES À JOUR
###########################################################"

def readDatesVersionsFile(main, web=False):
    """
    lecture du fichier dates_versions.html (local ou distant)
        * local : lecture depuis le dossier main.beginDir/files
        * distant (web=True) : téléchargement et lecture dans le dossier temporaire.
    Les 3 dates contenues dans le fichier dates_versions.html sont :
        * VÉRAC
        * dossier verac_admin
        * interface web.
    """
    dates_versions = ['0', '0', '0']
    theFileName = 'dates_versions.html'
    if web:
        destDir = main.tempPath + '/down/'
        theUrl = utils.UPDATEURL
        downLoader = UpDownLoader(
            main, theFileName, destDir, theUrl, down=True, showProgress=False)
        downLoader.launch()
        while downLoader.state == STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if downLoader.state == STATE_OK:
            # on lit la date de la dernière version:
            datesVersionsFile = QtCore.QFile(main.tempPath + '/down/dates_versions.html')
            if datesVersionsFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
                try:
                    stream = QtCore.QTextStream(datesVersionsFile)
                    stream.setCodec('UTF-8')
                    dates_versions = stream.readAll().split('|')
                except:
                    pass
                finally:
                    datesVersionsFile.close()
    else:
        datesVersionsFile = QtCore.QFile(main.beginDir + '/files/dates_versions.html')
        if datesVersionsFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            try:
                stream = QtCore.QTextStream(datesVersionsFile)
                stream.setCodec('UTF-8')
                dates_versions = stream.readAll().split('|')
            except:
                pass
            finally:
                datesVersionsFile.close()
    return dates_versions


def testUpdateVerac(main, doVerac=True, doAdmin=True, doWeb=True, messageIfOK=False):
    """
    blablabla
    """
    def date2Str(date):
        # mise en forme des dates pour affichage :
        return QtCore.QDateTime().fromString(date, 'yyyyMMddhhmm').toString('dd-MM-yyyy')

    isAdmin = (main.me['userMode'] == 'admin')
    # téléchargement du fichier dates_versions.html dans le dossier temporaire:
    datesWeb = readDatesVersionsFile(main, web=True)
    # on lit la date de la version installée:
    datesHere = readDatesVersionsFile(main)
    if isAdmin:
        netOK = testNet(main, main.actualVersion['versionUrl'])
        if netOK:
            dateVeracWebHere2 = getVersionSiteEtab(main)
            if dateVeracWebHere2 != '':
                datesHere[2] = dateVeracWebHere2
        else:
            doWeb = False
    # on compare :
    mustUpdateVerac, mustUpdateVeracAdmin, mustUpdateVeracWeb = False, False, False
    if utils_functions.toLong(datesHere[0]) < utils_functions.toLong(datesWeb[0]):
        mustUpdateVerac = True
    if isAdmin:
        if utils_functions.toLong(datesHere[1]) < utils_functions.toLong(datesWeb[1]):
            mustUpdateVeracAdmin = True
        if utils_functions.toLong(datesHere[2]) < utils_functions.toLong(datesWeb[2]):
            mustUpdateVeracWeb = True

    messageCurrentVersion = QtWidgets.QApplication.translate(
        'main', 'Date of current version: ')
    messageYourVersion = QtWidgets.QApplication.translate(
        'main', 'Date of your version: ')
    messageVeracOK = QtWidgets.QApplication.translate(
        'main', 'Your VERAC version is the last')
    if not(isAdmin):
        # Si ce n'est pas l'admin, on ne s'occupe que de VÉRAC :
        if doVerac:
            if mustUpdateVerac:
                link = utils.HELPPAGE_BEGIN + 'main-todo.html'
                m1 = QtWidgets.QApplication.translate('main', 'A new version of VERAC is available')
                m2 = QtWidgets.QApplication.translate('main', 'Must update')
                m3 = QtWidgets.QApplication.translate('main', '("Utils > UpdateVerac" menu)')
                m4 = QtWidgets.QApplication.translate('main', "What's new")
                message = utils_functions.u(
                    '<p></p>'
                    '<p align="center"><b>{0}</b>'
                    '<br/>{5}{6}'
                    '<br/>{7}{8}</p>'
                    '<p align="center"><b>{1}</b><br/>{2}</p>'
                    '<p align="center"><a href="{3}">{4}</a></p>'
                    '').format(
                        m1, m2, m3, link, m4, 
                        messageCurrentVersion, date2Str(datesWeb[0]), 
                        messageYourVersion, date2Str(datesHere[0]))
                dialog = MustUpdateDlg(
                    parent=main, message=message, 
                    buttonDoUpdateNow=True, dateVeracNew=datesWeb[0], 
                    helpContextPage='update-verac')
                dialog.exec_()
            elif messageIfOK:
                message = utils_functions.u(
                    '<p align="center">{0}</p>'
                    '<p><b>{1}</b></p>'
                    '<p>{2}<b>{3}</b>'
                    '<br/>{4}<b>{5}</b></p>'
                    '<p align="center">{0}</p>'
                    '').format(
                        utils.SEPARATOR_LINE, 
                        messageVeracOK,
                        messageCurrentVersion, date2Str(datesWeb[0]), 
                        messageYourVersion, date2Str(datesHere[0]))
                utils_functions.messageBox(main, message=message)
    else:
        """
        D'abord on affiche ce qui doit être mis à jour si besoin (jusqu'à 3 messages donc).
        Enfin on affiche ce qui est à jour (messageOK).
        """
        messageVeracAdminOK = QtWidgets.QApplication.translate(
            'main', 'Your verac_admin dir version is the last')
        messageVeracWebOK = QtWidgets.QApplication.translate(
            'main', 'Your Web interface version is the last')
        messageOK = ''
        # VÉRAC :
        if doVerac:
            if mustUpdateVerac:
                link = utils.HELPPAGE_BEGIN + 'main-todo.html'
                m1 = QtWidgets.QApplication.translate('main', 'A new version of VERAC is available')
                m2 = QtWidgets.QApplication.translate('main', 'Must update')
                m3 = QtWidgets.QApplication.translate('main', '("Utils > UpdateVerac" menu)')
                m4 = QtWidgets.QApplication.translate('main', "What's new")
                message = utils_functions.u(
                    '<p></p>'
                    '<p align="center"><b>{0}</b>'
                    '<br/>{5}{6}'
                    '<br/>{7}{8}</p>'
                    '<p align="center"><b>{1}</b><br/>{2}</p>'
                    '<p align="center"><a href="{3}">{4}</a></p>'
                    '').format(
                        m1, m2, m3, link, m4, 
                        messageCurrentVersion, date2Str(datesWeb[0]), 
                        messageYourVersion, date2Str(datesHere[0]))
                dialog = MustUpdateDlg(
                    parent=main, message=message, 
                    buttonDoUpdateNow=True, dateVeracNew=datesWeb[0], 
                    helpContextPage='update-verac')
                dialog.exec_()
            elif messageIfOK:
                messageOK = utils_functions.u(
                    '{1}'
                    '<p align="center">{0}</p>'
                    '<p><b>{2}</b></p>'
                    '<p>{3}<b>{4}</b>'
                    '<br/>{5}<b>{6}</b></p>'
                    '').format(
                        utils.SEPARATOR_LINE, 
                        messageOK, messageVeracOK,
                        messageCurrentVersion, date2Str(datesWeb[0]), 
                        messageYourVersion, date2Str(datesHere[0]))
        # le dossier verac_admin :
        if doAdmin:
            if mustUpdateVeracAdmin:
                link3 = utils.HELPPAGE_BEGIN + 'main-download.html'
                link4 = utils.HELPPAGE_BEGIN + 'help-admin-admin-update.html'
                m1 = QtWidgets.QApplication.translate('main', 'A new version of the verac_admin folder is available')
                m2 = QtWidgets.QApplication.translate('main', 'Must update')
                m3 = QtWidgets.QApplication.translate('main', 'Go to the download page')
                m4 = QtWidgets.QApplication.translate('main', 'HelpPage')
                message = utils_functions.u(
                    '<p></p>'
                    '<p align="center"><b>{0}</b>'
                    '<br/>{6}{7}'
                    '<br/>{8}{9}</p>'
                    '<p align="center"><b>{1}</b></p>'
                    '<p align="center"><a href="{2}">{3}</a></p>'
                    '<p align="center"><a href="{4}">{5}</a></p>'
                    '').format(
                        m1, m2, link3, m3, link4, m4,
                        messageCurrentVersion, date2Str(datesWeb[1]), 
                        messageYourVersion, date2Str(datesHere[1]))
                dialog = MustUpdateDlg(
                    parent=main, message=message, 
                    helpContextPage='admin-update', helpBaseUrl='admin')
                dialog.exec_()
            elif messageIfOK:
                messageOK = utils_functions.u(
                    '{1}'
                    '<p align="center">{0}</p>'
                    '<p><b>{2}</b></p>'
                    '<p>{3}<b>{4}</b>'
                    '<br/>{5}<b>{6}</b></p>'
                    '').format(
                        utils.SEPARATOR_LINE, 
                        messageOK, messageVeracAdminOK,
                        messageCurrentVersion, date2Str(datesWeb[1]), 
                        messageYourVersion, date2Str(datesHere[1]))
        # l'interface web :
        if doWeb:
            if mustUpdateVeracWeb:
                link = utils.HELPPAGE_BEGIN + 'main-todo.html'
                m1 = QtWidgets.QApplication.translate('main', 'A new version of the web interface is available')
                m2 = QtWidgets.QApplication.translate('main', 'Must update')
                m3 = QtWidgets.QApplication.translate('main', '("WebSite > Update website" menu)')
                m4 = QtWidgets.QApplication.translate('main', "What's new")
                message = utils_functions.u(
                    '<p></p>'
                    '<p align="center"><b>{0}</b>'
                    '<br/>{5}{6}'
                    '<br/>{7}{8}</p>'
                    '<p align="center"><b>{1}</b><br/>{2}</p>'
                    '<p align="center"><a href="{3}">{4}</a></p>'
                    '').format(
                        m1, m2, m3, link, m4, 
                        messageCurrentVersion, date2Str(datesWeb[2]), 
                        messageYourVersion, date2Str(datesHere[2]))
                dialog = MustUpdateDlg(
                    parent=main, message=message, 
                    buttonDoUpdateNow=True, what='web',
                    helpContextPage='website-update', helpBaseUrl='admin')
                dialog.exec_()
            elif messageIfOK:
                messageOK = utils_functions.u(
                    '{1}'
                    '<p align="center">{0}</p>'
                    '<p><b>{2}</b></p>'
                    '<p>{3}<b>{4}</b>'
                    '<br/>{5}<b>{6}</b></p>'
                    '<p align="center">{0}</p>'
                    '<p></p>'
                    '').format(
                        utils.SEPARATOR_LINE, 
                        messageOK, messageVeracWebOK,
                        messageCurrentVersion, date2Str(datesWeb[2]), 
                        messageYourVersion, date2Str(datesHere[2]))
        if messageIfOK and (messageOK != ''):
            utils_functions.messageBox(main, message=messageOK)


def getVersionSiteEtab(main):
    result = ''
    theUrl = main.actualVersion['versionUrl'] + '/pages/verac_getSiteVersion.php'
    postData = ''
    httpClient = HttpClient(main)
    httpClient.launch(theUrl, postData)
    while httpClient.state == STATE_LAUNCHED:
        QtWidgets.QApplication.processEvents()
    if httpClient.state == STATE_OK:
        try:
            reponse = utils_functions.u(httpClient.reponse)
            if 'OK|' in reponse:
                if utils.PYTHONVERSION >= 30:
                    # résolution d'un problème avec les QByteArray (b'aaa' au lieu de 'aaa') :
                    if reponse[:2] in ("b'", 'b"'):
                        reponse = reponse[2:-1]
                result = reponse.split('|')[1]
        except:
            pass
    return result


class MustUpdateDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(
            self, parent=None, message='', 
            buttonDoUpdateNow=False, dateVeracNew='', what='verac',
            helpContextPage='', helpBaseUrl='prof'):
        super(MustUpdateDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(utils.PROGNAME)

        self.dateVeracNew = dateVeracNew
        self.what = what

        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl

        # le WebView pour afficher:
        self.webView = utils_webengine.MyWebEngineView(
            self, linksInBrowser=True)
        if utils_webengine.WEB_ENGINE == 'WEBENGINE':
            # on copie si besoin le fichier css dans temp :
            sourceFile = utils_functions.u(
                '{0}/libs/dialogs.css').format(self.main.beginDir)
            destFile = self.main.tempPath + '/dialogs.css'
            copyOK = QtCore.QFile(sourceFile).copy(destFile)
            # on crée le fichier html :
            htmlResult = utils_functions.u(
                '<!DOCTYPE html> '
                '<html> '
                '<head><link href="dialogs.css" rel="stylesheet"></head> '
                '<body>{1}</body> '
                '</html> ').format(self.main.beginDir, message)
            if utils.OS_NAME[0] == 'win':
                import utils_html
                htmlResult = utils_html.encodeHtml(htmlResult)
            # on enregistre htmlResult dans un fichier html temporaire :
            htmlFileName = self.main.tempPath + '/temp.html'
            outFile = QtCore.QFile(htmlFileName)
            if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(outFile)
                stream.setCodec('UTF-8')
                stream << htmlResult
                outFile.close()
            htmlFileName = QtCore.QFileInfo(htmlFileName).absoluteFilePath()
            # on charge le fichier :
            url = QtCore.QUrl().fromLocalFile(htmlFileName)
            self.webView.load(url)
        else:
            cssFile = utils_functions.u(
                '{0}/libs/dialogs.css').format(self.main.beginDir)
            url = QtCore.QUrl().fromLocalFile(cssFile)
            self.webView.settings().setUserStyleSheetUrl(url)
            self.webView.setHtml(message)

        #le bouton OK:
        okButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), 
            QtWidgets.QApplication.translate('main', 'Ok'))
        okButton.clicked.connect(self.accept)
        #le bouton Update:
        doUpdateNowButton = QtWidgets.QPushButton(
            utils.doIcon('net-download'), 
            QtWidgets.QApplication.translate('main', 'Update Now'))
        doUpdateNowButton.clicked.connect(self.doUpdateNow)
        #le bouton aide:
        helpButton = QtWidgets.QPushButton(
            utils.doIcon('help'), 
            QtWidgets.QApplication.translate('main', 'Help'))
        helpButton.setShortcut(
            QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        helpButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Opens contextual help in your browser'))
        helpButton.clicked.connect(self.contextHelp)
        #une boîte des boutons:
        buttonBox = QtWidgets.QDialogButtonBox()
        buttonBox.addButton(okButton, QtWidgets.QDialogButtonBox.ActionRole)
        if buttonDoUpdateNow:
            buttonBox.addButton(doUpdateNowButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(helpButton, QtWidgets.QDialogButtonBox.ActionRole)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.webView, 2, 0)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMaximumWidth(500)
        self.setMaximumHeight(350)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, self.helpBaseUrl)

    def doUpdateNow(self):
        self.close()
        if self.what == 'verac':
            updateVerac(self.main, self.dateVeracNew)
        elif self.what == 'web':
            import admin_web
            admin_web.updateVeracWebSite(self.main)


def updateVerac(main, dateNew=''):
    utils_functions.afficheMessage(main, 'updateVerac')
    import utils_filesdirs
    try:
        # on enregistre avant de continuer :
        if main.me['userMode'] != 'admin':
            if main.db_myIsModified:
                main.doSaveDBMy()
        OK = 0
        # on récupère la date de la version installée pour la vérification :
        dates_versions = readDatesVersionsFile(main)
        dateVeracBefore = dates_versions[0]
        # téléchargement du fichier verac.tar.gz dans le dossier temporaire :
        if dateNew == '':
            # téléchargement du fichier dates_versions.html dans le dossier temporaire:
            dates_versions = readDatesVersionsFile(main, web=True)
            dateNew = dates_versions[0]
        if utils.OS_NAME[0] == 'win':
            theFileName = 'setup_verac.exe'
            destDir = utils_functions.addSlash(QtCore.QDir.tempPath())
            localefileFile = QtCore.QFileInfo(destDir + theFileName)
            localefileFile.makeAbsolute()
            thefile = localefileFile.filePath()
            if QtCore.QFile(thefile).exists():
                removeOK = QtCore.QFile(thefile).remove()
                if not(removeOK):
                    utils_functions.myPrint('REMOVE ERROR : ', thefile)
        else:
            theFileName = 'verac-{0}.tar.gz'.format(dateNew)
            destDir = main.tempPath + '/down/'
        theUrl = utils.UPDATEURL
        downLoader = UpDownLoader(main, 
            theFileName, destDir, theUrl, down=True, showProgress=True)
        downLoader.launch()
        while downLoader.state == STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if downLoader.state == STATE_OK:
            if utils.OS_NAME[0] == 'win':
                if main.me['userMode'] != 'admin':
                    # on fera afficher les news au prochain lancement :
                    import utils_db
                    utils_db.changeInConfigTable(
                        main, main.db_localConfig, {'mustShowNews': (1, '')})
                OK = 2
                url = QtCore.QUrl().fromLocalFile(utils_functions.u(thefile))
                QtGui.QDesktopServices.openUrl(url)
                main.close()
            else:
                OK = 1
                # on décompresse le fichier tar.gz dans beginDir:
                fileVerac = main.tempPath + '/down/' + theFileName
                path = main.beginDir + '/..'
                utils_functions.doWaitCursor()
                if utils_filesdirs.untarDirectory(fileVerac, path):
                    OK = 2
                    utils_functions.afficheMessage(main, 'FICHIER DECOMPRESSE')
                    # on récupère la date de la nouvelle version :
                    dates_versions = readDatesVersionsFile(main)
                    dateVeracAfter = dates_versions[0]
                    # on compare les dates avant et après update :
                    if (dateVeracAfter == dateNew):
                        OK = 3
                if main.me['userMode'] != 'admin':
                    # on fera afficher les news au prochain lancement :
                    import utils_db
                    utils_db.changeInConfigTable(
                        main, main.db_localConfig, {'mustShowNews': (1, '')})
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if OK == 3:
            message = QtWidgets.QApplication.translate(
                'main', 'You should start again the software.')
            utils_functions.afficheMsgFin(main, message=message)
        elif OK == 2:
            print('updateVerac : OK=2')
        elif OK == 1:
            if utils.OS_NAME[0] == 'win':
                fileName = 'setup_verac.exe'
            else:
                fileName = 'verac.tar.gz'
            m1 = QtWidgets.QApplication.translate(
                'main', 'UNABLE TO UNTAR THE NEW VERSION')
            m2 = QtWidgets.QApplication.translate(
                'main', 
                'Do you have the privileges to write into the installation directory?')
            m3 = QtWidgets.QApplication.translate(
                'main', 'You can also download the new version from the <b>{0}</b> site:')
            m3 = utils_functions.u(m3).format(utils.PROGNAMEHTML)
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p align="center">{2}</p>'
                '<p align="center">{0}</p>'
                '<p align="center">{3}</p>'
                '<p align="center"><a href="{4}{5}">{5}</a></p>'
                '<p></p>').format(utils.SEPARATOR_LINE, m1, m2, m3, utils.UPDATEURL, fileName)
            utils_functions.messageBox(main, level='warning', message=message)
        elif OK == 0:
            m1 = QtWidgets.QApplication.translate(
                'main', 'UNABLE TO DOWNLOAD THE NEW VERSION')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p></p>').format(utils.SEPARATOR_LINE, m1)
            utils_functions.messageBox(main, level='warning', message=message)


def gotoUpdatePage(main):
    theUrl = utils.HELPPAGE_BEGIN + 'main-download.html'
    utils_functions.openInBrowser(theUrl)



