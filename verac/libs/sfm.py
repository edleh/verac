#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
#       sfm.py - Simple FTP Mirror
#       
#       Copyright (c) 2009 joonis new media
#       Author: Thimo Kraemer <thimo.kraemer@joonis.de>
#       
#       Modifs: Pascal Peter (2013)
#               works with Python2.x and Python3.x
#               doAction function for external use
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

"""
Usage: sfm [-v] [-q] [-u username [-p password [-a account]]]
        store|retrieve|remove|info hostname[:port] [remotedir [localdir]]
-v: verbose (-vvv debug)
-q: quiet
-u username: ftp username (default anonymous)
-p password: ftp password
-a account: ftp account
store: mirror the content of localdir to remotedir
retrieve: mirror the content of remotedir to localdir
remove: remove remotedir recursively
info: prints some information about remote mirror
hostname[:port]: remote host
remotedir: remote directory (default initial)
localdir: local directory (default current)

Example: sfm -u myuser store ftp.mydomain.com /myfolder /my/folder
         Will mirror the content of local directory /my/folder
         to remote directory /myfolder
"""


# importation des modules utiles :
from __future__ import division, print_function
import sys
import os

import getopt
import getpass
import ftplib
import time
import datetime

PYTHONVERSION = sys.version_info[0] * 10 + sys.version_info[1]

if PYTHONVERSION >= 30:
    from io import StringIO, BytesIO
else:
    from cStringIO import StringIO


globals = {
    'verbose': 1,
    'status': {
        'dirs_total': 0,
        'dirs_created': 0,
        'dirs_removed': 0,
        'files_total': 0,
        'files_created': 0,
        'files_updated': 0,
        'files_removed': 0,
        'bytes_transfered': 0,
        'bytes_total': 0,
        'time_started': datetime.datetime.now(),
        'time_finished': 0,
        },
    }

def log(msg, level=1, abort=False):
    if level <= globals['verbose'] or abort:
        if abort:
            sys.stdout = sys.stderr
            print('')
        print(msg)
    if abort:
        sys.exit(1)

def strfbytes(value):
    units = ['Bytes', 'KB', 'MB', 'GB', 'TB']
    value = float(value)
    for unit in units:
        if value < 1024: break
        value = value // 1024
    if unit == units[0]: fmt = '{0:.0f} {1}'
    else: fmt = '{0:.2f} {1}'
    return fmt.format(value, unit)

def u(text):
    """
    retourne une version unicode de text
    returns a unicode version of text
    """
    if PYTHONVERSION >= 30:
        try:
            if isinstance(text, str):
                return text
            else:
                return str(text)
        except:
            return text
    else:
        try:
            return unicode(text)
        except:
            if isinstance(text, str):
                return text.decode('utf-8')
            else:
                return text

def formatFtpList(ftpList):
    """
    remise en forme des retours FTP de la commande LIST.
    Cette commande n'est pas normalisée et dépend du serveur.
    Donc cette procédure ne fonctionnera peut-être pas toujours.
    """
    month2Int = {
        'Jan': 1, 'Feb': 2, 'Mar': 3, 'Apr': 4,
        'May': 5, 'Jun': 6, 'Jul': 7, 'Aug': 8, 
        'Sep': 9, 'Oct': 10, 'Nov': 11, 'Dec': 12}
    newList = []
    if len(ftpList) < 1:
        return newList
    # on récupère la longueur des lignes (différents types de serveurs FTP) :
    ftpType = len(ftpList[0].split())
    for line in ftpList:
        parts = line.split()
        if ftpType == 9:
            # FTP standard
            permissions = parts[0]
            size = int(parts[4])
            month = month2Int[parts[5]]
            day = int(parts[6])
            yearOrTime = parts[7]
            name = parts[8]
            if yearOrTime.find(':') == -1:
                year = int(yearOrTime)
                hour = minute = 0
            else:
                year = datetime.date.today().year
                hour, minute = [int(s) for s in yearOrTime.split(':')]
            isDir = False
            if permissions.startswith('d'):
                isDir = True
            newList.append([name, isDir, size, year, month, day, hour, minute])
        elif ftpType == 4:
            # Microsoft FTP Service
            theDate = parts[0]
            theTime = parts[1]
            dirOrSize = parts[2]
            name = parts[3]
            isDir = False
            size = 0
            if dirOrSize == '<DIR>':
                isDir = True
            else:
                size = int(dirOrSize)
            month, day, year = [int(s) for s in theDate.split('-')]
            # si la date est sur 2 chiffres :
            if year < 100:
                year = 2000 + year
            hour = minute = 0
            newList.append([name, isDir, size, year, month, day, hour, minute])
    return newList


class localHandler:
    """ Local file and directory functions"""
    def __init__(self, ftp, root):
        self.what = 'local'
        self.ftp = ftp
        self.root = root
        self.host = ''
    
    def storefile(self, src, dst):
        fh = open(dst, 'wb')
        self.ftp.retrbinary('RETR {0}'.format(src), fh.write)
        fh.close()
    
    def storetext(self, text, dst):
        if PYTHONVERSION >= 30:
            fh = open(dst, 'w', newline='', encoding='utf-8')
        else:
            fh = open(dst, 'w')
        fh.write(text)
        fh.close()
    
    def readlines(self, path):
        if PYTHONVERSION >= 30:
            fh = open(path, newline='', encoding='utf-8')
        else:
            fh = open(path, 'r')
        buffer = [line.strip() for line in fh.readlines()]
        fh.close()
        return buffer
    
    def list(self, dir, skip_mtime=False):
        dirs = []
        files = {}
        for name in os.listdir(dir):
            path = os.path.join(dir, name)
            if os.path.isdir(path):
                dirs.append(name)
            else:
                if skip_mtime: mtime = 0
                else: mtime = int(os.path.getmtime(path))
                files[name] = {
                    'size': os.path.getsize(path),
                    'mtime': mtime,
                    }
        return (dirs, files)
    
    def makedir(self, path):
        log(u('--> Create directory {0}').format(path), 2)
        os.mkdir(path)
        globals['status']['dirs_created'] += 1
    
    def removefile(self, path):
        log(u('--> Remove file {0}').format(path), 2)
        os.remove(path)
        globals['status']['files_removed'] += 1
    
    def removedir(self, dir):
        for name in os.listdir(dir):
            path = os.path.join(dir, name)
            if os.path.isdir(path):
                self.removedir(path)
            else:
                self.removefile(path)
        log(u('--> Remove directory {0}').format(dir), 2)
        os.rmdir(dir)
        globals['status']['dirs_removed'] += 1


class remoteHandler:
    """Remote file and directory functions"""
    def __init__(self, ftp, root):
        self.what = 'remote'
        self.ftp = ftp
        self.root = root
        self.host = ftp.host
    
    def storefile(self, src, dst):
        fh = open(src, 'rb')
        self.ftp.storbinary('STOR {0}'.format(dst), fh)
        fh.close()
    
    def storetext(self, text, dst):
        if PYTHONVERSION >= 30:
            text = bytes(text, 'utf8')
            fh = BytesIO(text)
        else:
            text = text.encode('utf8')
            fh = StringIO(text)
        self.ftp.storlines('STOR {0}'.format(dst), fh)
        fh.close()
    
    def readlines(self, path):
        buffer = []
        if PYTHONVERSION >= 30:
            import codecs
            tempBuffer = []
            self.ftp.retrlines('RETR {0}'.format(path), tempBuffer.append)
            for line in tempBuffer:
                buffer.append(codecs.latin_1_encode(line)[0].decode())
        else:
            self.ftp.retrlines('RETR {0}'.format(path), buffer.append)
        return buffer

    def list(self, dir, skip_mtime=False):
        try:
            buffer = []
            self.ftp.dir('-a ', dir, buffer.append)
        except ftplib.error_temp:
            buffer = []
            self.ftp.dir(dir, buffer.append)
        dirs = []
        files = {}
        buffer = formatFtpList(buffer)
        for [name, isDir, size, year, month, day, hour, minute] in buffer:
            if name in ('.', '..'):
                continue
            if isDir:
                dirs.append(name)
            else:
                if skip_mtime:
                    mtime = 0
                else:
                    mtime = datetime.datetime(year, month, day, hour, minute)
                    mtime = int(time.mktime(mtime.timetuple()))
                files[name] = {
                    'size': size,
                    'mtime': mtime,
                    }
        return (dirs, files)
    
    def makedir(self, path):
        log('--> Create directory {0}'.format(path), 2)
        self.ftp.mkd(path)
        globals['status']['dirs_created'] += 1
    
    def removefile(self, path):
        log('--> Remove file {0}'.format(path), 2)
        try:
            self.ftp.delete(path)
        except:
            pass
        globals['status']['files_removed'] += 1
    
    def removedir(self, path):
        dirs, files = self.list(path)
        for dir in dirs:
            self.removedir('/'.join([path, dir]))
        for file in files:
            self.removefile('/'.join([path, file]))
        if path == '/':
            return
        log('--> Remove directory {0}'.format(path), 2)
        self.ftp.rmd(path)
        globals['status']['dirs_removed'] += 1


def mirror(src, dst, subdir='', createStat=True):
    if src.what == 'remote':
        src_path = '{0}{1}'.format(src.root, subdir)
    else:
        src_path = os.path.normpath(u('{0}/{1}').format(src.root, os.sep.join(subdir.split('/'))))
    if dst.what == 'remote':
        dst_path = '{0}{1}'.format(dst.root, subdir)
    else:
        dst_path = os.path.normpath(u('{0}/{1}').format(dst.root, os.sep.join(subdir.split('/'))))
    log(u('Working on {0}{1}').format(src.host, src_path))
    log(u('To {0}{1}').format(dst.host, dst_path))

    src_dirs, src_files = src.list(src_path)
    if '.sfmstat' in src_files:
        del src_files['.sfmstat']
    
    globals['status']['dirs_total'] += len(src_dirs)
    globals['status']['files_total'] += len(src_files)
    
    dst_dirs, dst_files = dst.list(dst_path, True)
    if '.sfmstat' in dst_files:
        if dst.what == 'remote':
            sfmstat = dst.readlines('/'.join([dst_path, '.sfmstat']))
        else:
            sfmstat = dst.readlines(os.path.join(dst_path, '.sfmstat'))
        del dst_files['.sfmstat']
    else:
        if dst_path == dst.root and (dst_dirs or dst_files):
            if globals['verbose']: abort = False
            else: abort = True
            log('New mirror, but target directory not empty!', abort=abort)
            result = 'y'#raw_input('Do you really want to replace this directory? [y|n]: ')
            if result.lower() not in ('y', 'yes'):
                log('Aborted', abort=True)
        sfmstat = [u('0 {0}{1}').format(src.host, src_path)]
    
    last_updated, mirror_path = sfmstat[0].split(None, 1)
    if u(mirror_path) != u(src.host + src_path):
        if globals['verbose']: abort = False
        else: abort = True
        msg = 'Mirror mismatch!\n{0} already contains another mirror of {1}'
        error = u(msg).format(dst_path, u(mirror_path))
        log(error, abort=abort)
        result = 'y'#raw_input('Do you really want to replace this mirror? [y|n]: ')
        if result.lower() not in ('y', 'yes'):
            log('Aborted', abort=True)
        sfmstat = [u('0 {0}{1}').format(src.host, src_path)]
    
    for line in sfmstat[1:]:
        mtime, file = line.split(None, 1)
        if file in dst_files:
            dst_files[file]['mtime'] = int(mtime)
    
    for dir in dst_dirs:
        if dir not in src_dirs:
            if dst.what == 'remote':
                path = '/'.join([dst_path, dir])
            else:
                path = os.path.join(dst_path, dir)
            log(u('-> Remove directory {0}').format(path))
            dst.removedir(path)
    
    for file in dst_files:
        if file not in src_files:
            if dst.what == 'remote':
                dst_file = '/'.join([dst_path, file])
            else:
                dst_file = os.path.join(dst_path, file)
            log(u('-> Remove file {0}: {1}').format(dst_file, strfbytes(dst_files[file]['size'])))
            dst.removefile(dst_file)
    
    newstat = [u('{0} {1}{2}').format(int(time.time()), src.host, src_path)]
    for file in src_files:
        if file not in dst_files or src_files[file]['mtime'] > dst_files[file]['mtime'] or src_files[file]['size'] != dst_files[file]['size']:
            if src.what == 'remote':
                src_file = '/'.join([src_path, file])
            else:
                src_file = os.path.join(src_path, file)
            if dst.what == 'remote':
                dst_file = '/'.join([dst_path, file])
            else:
                dst_file = os.path.join(dst_path, file)
            if file in dst_files:
                log(u('-> Update file {0}: {1}').format(dst_file, strfbytes(src_files[file]['size'])))
                globals['status']['files_updated'] += 1
            else:
                log(u('-> Create file {0}: {1}').format(dst_file, strfbytes(src_files[file]['size'])))
                globals['status']['files_created'] += 1
            dst.storefile(src_file, dst_file)
            globals['status']['bytes_transfered'] += src_files[file]['size']
        globals['status']['bytes_total'] += src_files[file]['size']
        newstat.append(u('{0} {1}').format(src_files[file]['mtime'], file))
    if createStat:
        if dst.what == 'remote':
            dst.storetext('\n'.join(newstat), '/'.join([dst_path, '.sfmstat']))
        else:
            dst.storetext('\n'.join(newstat), os.path.join(dst_path, '.sfmstat'))
    
    for dir in src_dirs:
        if dir not in dst_dirs:
            if dst.what == 'remote':
                dst_dir = '/'.join([dst_path, dir])
            else:
                dst_dir = os.path.join(dst_path, dir)
            log(u('-> Create directory {0}').format(dst_dir))
            try:
                dst.makedir(dst_dir)
            except:
                log(u('-> ERROR in create directory {0}').format(dst_dir))
                pass
        mirror(src, dst, '/'.join([subdir, dir]), createStat=createStat)


def info(remote):
    try:
        if remote.what == 'remote':
            sfmstat = remote.readlines(os.path.join(remote.root, '.sfmstat'))
        else:
            sfmstat = remote.readlines(os.path.join(remote.root, '.sfmstat'))
    except ftplib.error_perm as err:
        if not err.args[0].startswith('550'):
            log(err, abort=True)
        sfmstat = None
    print('')
    if sfmstat:
        last_updated, mirror_path = sfmstat[0].split(None, 1)
        last_updated = datetime.datetime.fromtimestamp(float(last_updated))
        print(u('Mirror of {0}').format(mirror_path))
        print (last_updated.strftime('Last updated on %A, %d. %B %Y at %H:%M:%S'))
    else:
        print('No mirror recognized')
    print('')
    print(u('Content of {0}{1}:').format(remote.host, remote.root))
    remote.ftp.dir(remote.root)
    print('')


def remove(remote):
    if globals['verbose']:
        info(remote)
        result = 'y'#raw_input('Do you really want to remove this directory? [y|n]: ')
        if result.lower() not in ('y', 'yes'):
            log('Aborted', abort=True)
    remote.removedir(remote.root)


def main(username='', password='', account=''):

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'vqu:p:a:')
    except getopt.GetoptError as msg:
        log('{0}\n{1}'.format(msg.args, __doc__), abort=True)
    for opt, val in opts:
        if opt == '-v': globals['verbose'] += 1
        if opt == '-q': globals['verbose'] = 0
        if opt == '-u': username = val
        if opt == '-p': password = val
        if opt == '-a': account = val

    if not args:
        log('No action given\n' + __doc__, abort=True)

    action = args[0]
    if action not in ('store', 'retrieve', 'remove', 'info'):
        log('Unknown action: {0}\n{1}'.format(action, __doc__), abort=True)

    if len(args) == 1:
        log('Missing hostname\n' + __doc__, abort=True)

    args[1] = args[1].split(':')
    host = args[1][0]
    if len(args[1]) == 1:
        port = 21
    else:
        port = int(args[1][1])

    remotedir = '/'
    if len(args) > 2:
        remotedir = args[2]
        if not remotedir.startswith('/'):
            log('Invalid remotedir, must start with /', abort=True)

    localdir = os.getcwd()
    if len(args) > 3:
        if action not in ('store', 'retrieve'):
            log('Too many arguments\n{0}'.format(__doc__), abort=True)
        localdir = os.path.abspath(args[3])
        if not os.path.isdir(localdir):
            log('localdir does not exist: {0}'.format(localdir), abort=True)

    if len(args) > 4:
        log('Too many arguments\n{0}'.format(__doc__), abort=True)

    if not username:
        username = 'anonymous'
    elif not password and globals['verbose']:
        password = getpass.getpass('FTP Password: ')

    result = doAction(username, password, account, action, host, port, remotedir, localdir)
    print(result)
    
    
def doAction(username='', password='', account='', action='store',
             host='', port=21, remotedir='', localdir='', createStat=True):

    ftp = ftplib.FTP()
    if globals['verbose'] > 2:
        ftp.set_debuglevel(globals['verbose'] - 2)
    ftp.connect(host, port)
    ftp.login(username, password, account)
    try:
        ftp.cwd(remotedir)
    except ftplib.error_perm as err:
        if err.args[0].startswith('550'):
            log('remotedir does not exist: {0}'.format(remotedir), abort=True)
        else:
            raise
    ftp.cwd('/')

    local = localHandler(ftp, localdir)
    remote = remoteHandler(ftp, remotedir)

    if action == 'store':
        mirror(local, remote, createStat=createStat)
    elif action == 'retrieve':
        mirror(remote, local, createStat=createStat)
    elif action == 'remove':
        remove(remote)
    elif action == 'info':
        info(remote)
        ftp.quit()
        return

    ftp.quit()
    log('Done')
    status = globals['status']
    status['time_finished'] = datetime.datetime.now()
    
    result = '\n'
    result += '=' * 60
    result += '\nProcessing Summary'
    result += '\n'
    result += '=' * 60
    result += '\n%-30s%30s' % ('Directories created:', status['dirs_created'])
    result += '\n%-30s%30s' % ('Directories removed:', status['dirs_removed'])
    result += '\n%-30s%30s' % ('Directories total:', status['dirs_total'])
    result += '\n'
    result += '\n%-30s%30s' % ('Files created:', status['files_created'])
    result += '\n%-30s%30s' % ('Files updated:', status['files_updated'])
    result += '\n%-30s%30s' % ('Files removed:', status['files_removed'])
    result += '\n%-30s%30s' % ('Files total:', status['files_total'])
    result += '\n'
    result += '\n%-30s%30s' % ('Bytes transfered:', strfbytes(status['bytes_transfered']))
    result += '\n%-30s%30s' % ('Bytes total:', strfbytes(status['bytes_total']))
    result += '\n'
    result += '\n%-30s%30s' % ('Time started:', status['time_started'])
    result += '\n%-30s%30s' % ('Time finished:', status['time_finished'])
    result += '\n%-30s%30s' % ('Duration:', status['time_finished']-status['time_started'])
    result += '\n'
    result += '=' * 60
    return result
    


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        log('Aborted', abort=True)
    except SystemExit:
        raise
    except Exception as err:
        log(err.args, abort=True)
