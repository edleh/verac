# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Tout ce qui concerne la gestion des photos des élèves.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_filesdirs
import admin, admin_ftp

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



"""
****************************************************
    FONCTIONS DIVERSES
****************************************************
"""

PHOTOS_WIDTH = 120
PHOTOS_HEIGHT = 154
GRID_WIDTH = 200
GRID_HEIGHT = 250

def uploadPhotos(main, msgFin=True):
    """
    envoi des photos sur le site web
    """
    debut = QtCore.QTime.currentTime()
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Sending photos'), 
        tags=('h2'))
    sousDir = '/protected/photos'
    dirSite = admin.dirSitePrive + sousDir
    dirLocal = admin.dirLocalPrive + sousDir
    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            admin_ftp.ftpMakeDir(
                admin_ftp.FTP_HOST, 
                admin_ftp.FTP_USER, 
                admin_ftp.FTP_PASS, 
                dirSite)
            reponse = admin_ftp.ftpPutDir(main, dirLocal, dirSite)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                if msgFin:
                    fin = QtCore.QTime.currentTime()
                    utils_functions.afficheDuree(main, debut, fin)
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                utils_functions.afficheMsgPb(main)



"""
****************************************************
    FENÊTRE DE GESTION DES PHOTOS
****************************************************
"""

def managePhotos(main):
    """
    lance le dialog de configuration des photos (ManagePhotosDlg).
    """
    dialog = ManagePhotosDlg(parent=main)
    lastState = main.disableInterface(dialog)
    dialog.exec_()
    main.enableInterface(dialog, lastState)
    main.editLog2 = None
    if dialog.mustUpload:
        # on demande s'il faut poster maintenant :
        message = QtWidgets.QApplication.translate(
            'main', 
            'Would you upload photos to your website now?')
        message = utils_functions.u(
            '<p><b>{0}</b></p>').format(message)
        reponse = utils_functions.messageBox(
            main, 
            level='warning', 
            message=message, 
            buttons=['Yes', 'No'])
        if reponse == QtWidgets.QMessageBox.Yes:
            uploadPhotos(main)


class ManagePhotosDlg(QtWidgets.QDialog):
    """
    """
    def __init__(self, parent=None):
        utils_functions.doWaitCursor()
        super(ManagePhotosDlg, self).__init__(parent)
        self.main = parent
        self.helpContextPage = ''
        # pour savoir si on a utilisé le bouton "appliquer"
        # et s'il faudra poster sur le site :
        self.mustUpload = False
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate(
            'main', 'Management of photos')
        self.setWindowTitle(title)
        # le tabWidget et sa liste d'onglets :
        self.tabWidget = QtWidgets.QTabWidget(self)
        self.tabWidget.currentChanged.connect(self.doCurrentTabChanged)
        self.tabs = []

        # création des onglets :
        self.tabTrombinoscope = TrombinoscopeDlg(parent=self)
        self.tabWidget.addTab(
            self.tabTrombinoscope, 
            QtWidgets.QApplication.translate(
                'main', 'Trombinoscope'))
        self.tabs.append(self.tabTrombinoscope)
        # onglet outils :
        self.tabUtils = PhotosUtilsDlg(parent=self)
        self.tabWidget.addTab(
            self.tabUtils, 
            QtWidgets.QApplication.translate(
                'main', 'Tools'))
        self.tabs.append(self.tabUtils)

        # on indique l'onglet de départ :
        indexTab = 0
        self.tabWidget.setCurrentIndex(indexTab)
        # fin de la mise en place :
        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('ok', 'help'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.tabWidget)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        utils_functions.restoreCursor()

    def doCurrentTabChanged(self, index):
        self.helpContextPage = self.tabWidget.currentWidget().helpContextPage
        if type(self.tabWidget.currentWidget()) == PhotosUtilsDlg:
            self.tabWidget.currentWidget().uploadPhotos_Button.clearFocus()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, 'admin')


class TrombinoscopeListView(QtWidgets.QListWidget):
    def __init__(self, parent=None):
        super(TrombinoscopeListView, self).__init__(parent)
        self.parent = parent

        self.setViewMode(QtWidgets.QListView.IconMode)
        self.setIconSize(QtCore.QSize(PHOTOS_WIDTH, PHOTOS_HEIGHT))
        self.setGridSize(QtCore.QSize(GRID_WIDTH, GRID_HEIGHT))
        self.setSpacing(10)
        self.setResizeMode(QtWidgets.QListView.Adjust)
        self.setMovement(QtWidgets.QListView.Snap)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        self.selectedItem = None
        self.actionDetete = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Delete'), 
            self, 
            icon=utils.doIcon('edit-clear'))
        self.actionDetete.triggered.connect(self.doDetete)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            item = self.itemAt(event.pos())
            self.setCurrentItem(item)
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            try:
                fileName = utils_functions.u(
                    event.mimeData().urls()[0].toLocalFile())
                item = self.itemAt(event.pos())
                icon = QtGui.QIcon(fileName)
                pixmap = icon.pixmap(PHOTOS_WIDTH, PHOTOS_HEIGHT)
                if (pixmap.width() < PHOTOS_WIDTH) or (pixmap.height() < PHOTOS_HEIGHT):
                    x, y = 0, 0
                    if pixmap.width() < PHOTOS_WIDTH:
                        x = (PHOTOS_WIDTH - pixmap.width()) // 2
                    if pixmap.height() < PHOTOS_HEIGHT:
                        y = (PHOTOS_HEIGHT - pixmap.height()) // 2
                    pixmap2 = QtGui.QPixmap(PHOTOS_WIDTH, PHOTOS_HEIGHT)
                    pixmap2.fill()
                    painter = QtGui.QPainter(pixmap2)
                    painter.drawPixmap(
                        x, y, pixmap, 0, 0, PHOTOS_WIDTH, PHOTOS_HEIGHT)
                    painter.end()
                    pixmap = pixmap2
                icon = QtGui.QIcon(pixmap)
                item.setIcon(icon)
                itemData = item.data(QtCore.Qt.UserRole)
                fileName = utils_functions.u(
                    '{0}/protected/photos/{1}.jpeg').format(
                        admin.dirLocalPrive, itemData[1])
                pixmap.save(fileName, 'JPG')
                self.parent.parent.mustUpload = True
            except:
                pass
            self.setCurrentItem(None)
        else:
            event.ignore()

    def contextMenuEvent(self, event):
        item = self.itemAt(event.pos())
        if item == None:
            return
        self.selectedItem = item
        # création et remplissage du menu :
        menu = QtWidgets.QMenu(self)
        menu.addAction(self.actionDetete)
        menu.exec_(event.globalPos())

    def doDetete(self):
        item = self.selectedItem
        itemData = item.data(QtCore.Qt.UserRole)
        fileName = utils_functions.u(
            '{0}/protected/photos/{1}.jpeg').format(
                admin.dirLocalPrive, itemData[1])
        if QtCore.QFile(fileName).exists():
            removeOK = QtCore.QFile(fileName).remove()
        self.selectedItem = None
        self.parent.filterComboBoxChanged(
            self.parent.filterComboBox.currentIndex())
        self.parent.parent.mustUpload = True


class TrombinoscopeDlg(QtWidgets.QDialog):
    """
    Dialogue pour afficher les documents par élève.
    """
    def __init__(self, parent=None):
        super(TrombinoscopeDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'manage-photos'

        # un dico pour remplacer temporairement la table :
        self.eleves = {}

        # la zone de sélection :
        self.filterComboBox = QtWidgets.QComboBox()
        self.filterComboBox.setMaximumWidth(300)
        self.filterComboBox.activated.connect(self.filterComboBoxChanged)
        helpText = QtWidgets.QApplication.translate(
            'main', 
            'You can view below photos '
            "<br/>and modify them one by one by drag and drop from your computer's file browser."
            '<br/>A mass import procedure is available in the Tools tab.')
        helpLabel = QtWidgets.QLabel(helpText)
        self.pdfButton = QtWidgets.QToolButton()
        self.pdfButton.setIcon(utils.doIcon('extension-pdf'))
        self.pdfButton.setText(
            QtWidgets.QApplication.translate('main', 'PDF'))
        self.pdfButton.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        toolTip = QtWidgets.QApplication.translate(
            'main', 
            'Create a pdf file of this trombinoscope')
        self.pdfButton.setToolTip(toolTip)
        self.pdfButton.setStatusTip(toolTip)
        self.pdfButton.clicked.connect(self.createPdf)
        baseLayout = QtWidgets.QHBoxLayout()
        baseLayout.addWidget(self.filterComboBox)
        baseLayout.addWidget(helpLabel)
        baseLayout.addWidget(self.pdfButton)

        # la zone d'édition :
        self.trombinoscopeList = TrombinoscopeListView(self)

        # on place tout dans une grille :
        dialogLayout = QtWidgets.QVBoxLayout()
        dialogLayout.addLayout(baseLayout)
        dialogLayout.addWidget(self.trombinoscopeList)
        self.setLayout(dialogLayout)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        remplissage du comboBox des classes (filterComboBox)
        et du dico self.eleves.
        """
        # filtre "toutes les classes" :
        self.filterComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'All the classes'))
        self.eleves['ALL'] = []
        self.eleves['CLASSES'] = []
        # on récupère la liste des classes :
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            classe = query_commun.value(1)
            self.filterComboBox.addItem(classe)
            self.eleves[classe] = []
            self.eleves['CLASSES'].append(classe)
        # lecture de admin.eleves et remplissage de self.eleves :
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            id_eleve = int(record[0])
            eleveName = record[2]
            eleveFirstName = record[3]
            eleveClasse = record[4]
            itemText = utils_functions.u('{0} {1} [{2}]').format(
                eleveName, eleveFirstName, eleveClasse)
            itemData = [
                itemText, id_eleve, eleveName, eleveFirstName, eleveClasse]
            self.eleves[eleveClasse].append(itemData)
            self.eleves['ALL'].append(itemData)
        try:
            self.filterComboBoxChanged(1)
        except:
            self.filterComboBoxChanged(0)

    def filterComboBoxChanged(self, index=-1):
        """
        mise à jour selon la classe sélectionnée
        """
        utils_functions.doWaitCursor()
        try:
            self.trombinoscopeList.clear()
            if index < 0:
                index = self.filterComboBox.currentIndex()
            else:
                self.filterComboBox.setCurrentIndex(index)
            selected = self.filterComboBox.currentText()
            if index == 0:
                selected = 'ALL'
            if not(selected in self.eleves):
                return
            for itemData in self.eleves[selected]:
                fileName = utils_functions.u(
                    '{0}/protected/photos/{1}.jpeg').format(
                        admin.dirLocalPrive, itemData[1])
                if not(QtCore.QFile(fileName).exists()):
                    fileName = utils_functions.u(
                        '{0}/files/unknown-user.png').format(self.main.beginDir)
                icon = QtGui.QIcon(fileName)
                pixmap = icon.pixmap(PHOTOS_WIDTH, PHOTOS_HEIGHT)
                icon = QtGui.QIcon(pixmap)
                if index == 0:
                    itemText = utils_functions.u(
                        '{0}\n{1}\n[{2}]').format(
                            itemData[2], itemData[3], itemData[4])
                else:
                    itemText = utils_functions.u(
                        '{0}\n{1}').format(itemData[2], itemData[3])
                item = QtWidgets.QListWidgetItem(itemText)
                item.setData(QtCore.Qt.UserRole, itemData)
                item.setIcon(icon)
                toolTip = utils_functions.u(
                    '{0} {1}\n{2}').format(itemData[2], itemData[3], itemData[1])
                item.setToolTip(toolTip)
                self.trombinoscopeList.addItem(item)
        finally:
            utils_functions.restoreCursor()

    def createPdf(self):
        """
        """
        if self.filterComboBox.currentIndex() > 0:
            htmlTitle = QtWidgets.QApplication.translate(
                'main', 'Class Trombinoscope:')
            name = self.filterComboBox.currentText()
            htmlTitle = utils_functions.u('{0} {1}').format(htmlTitle, name)
            fileTitle = utils_functions.doAscii(name, case='lower')
            replacements = [' ', '.']
            for replaceWhat in replacements:
                fileTitle = fileTitle.replace(
                    utils_functions.u(replaceWhat), '_')
            fileTitle = utils_functions.u('trombinoscope-{0}').format(fileTitle)
        else:
            htmlTitle = QtWidgets.QApplication.translate(
                'main', 'Trombinoscope of all students')
            fileTitle = htmlTitle
        espaces = '&nbsp;' * 20
        htmlTitle = utils_functions.u(
            '{0}<small class="text-muted">{1}{2} {3}</small></h2>').format(
                htmlTitle, 
                espaces, 
                self.trombinoscopeList.count(), 
                QtWidgets.QApplication.translate('main', 'students'))

        pdfTitle = QtWidgets.QApplication.translate('main', 'pdf File')
        proposedName = utils_functions.u(
            '{0}/fichiers/pdf/{1}.pdf').format(admin.adminDir, fileTitle)
        pdfExt = QtWidgets.QApplication.translate('main', 'pdf files (*.pdf)')
        fileNamePdf = QtWidgets.QFileDialog.getSaveFileName(
            self, pdfTitle, proposedName, pdfExt)
        fileNamePdf = utils_functions.verifyLibs_fileName(fileNamePdf)
        if fileNamePdf == '':
            return

        utils_functions.doWaitCursor()
        try:
            tempDir = utils_functions.u(
                '{0}/photos').format(self.main.tempPath)
            modelesDir = utils_functions.u(
                '{0}/files').format(self.main.beginDir)
            utils_filesdirs.createDirs(self.main.tempPath, 'photos')
            files = (
                ('', 'trombinoscope.html'), 
                ('', 'trombinoscope.css'), 
                ('md/', 'bootstrap.min.css'), 
                ('', 'unknown-user.png'))
            for (subDir, fileName) in files:
                utils_filesdirs.removeAndCopy(
                    utils_functions.u('{0}/{1}{2}').format(modelesDir, subDir, fileName), 
                    utils_functions.u('{0}/{1}').format(tempDir, fileName))
            fileName = utils_functions.u('{0}/trombinoscope.html').format(tempDir)
            inFile = QtCore.QFile(fileName)
            if not inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
                message = QtWidgets.QApplication.translate(
                    'main', 'Cannot read file {0}:\n{1}.').format(fileName, inFile.errorString())
                utils_functions.messageBox(self.main, level='warning', message=message)
                return
            indentSize = 4
            textStream = QtCore.QTextStream(inFile)
            textStream.setCodec('UTF-8')
            htmlModele = textStream.readAll()
            inFile.close()

            if self.filterComboBox.currentIndex() > 0:
                self.doPdf(htmlModele, tempDir, htmlTitle, fileNamePdf)
            else:
                self.doPdf(htmlModele, tempDir, htmlTitle, fileNamePdf, withClassName=True)
        finally:
            utils_functions.restoreCursor()


    def doPdf(self, htmlModele, tempDir, htmlTitle, fileNamePdf, withClassName=False):
        """
        """
        import utils_html, utils_pdf

        htmlCurrent = utils_functions.u(htmlModele)

        htmlStudentNoClass =  '\n        <td style="border:1px solid #e5e5e5">'
        htmlStudentNoClass += '\n            <div class="text-center">'
        htmlStudentNoClass += '\n                <img src="{0}">'
        htmlStudentNoClass += '\n                <p>{1}<br/>{2}</p>'
        htmlStudentNoClass += '\n            </div>'
        htmlStudentNoClass += '\n        </td>'

        htmlStudentWithClass =  '\n        <td style="border:1px solid #e5e5e5">'
        htmlStudentWithClass += '\n            <div class="text-center">'
        htmlStudentWithClass += '\n                <img src="{0}">'
        htmlStudentWithClass += '\n                <p>{1}<br/>{2}<br/>{3}</p>'
        htmlStudentWithClass += '\n            </div>'
        htmlStudentWithClass += '\n        </td>'

        htmlNewPage =  '\n    <div style="page-break-inside:always"><tr>'
        htmlNewPage += '\n        <td></td>'

        htmlNewRow =  '\n    <div style="page-break-inside:avoid"><tr>'
        htmlNewRow += '\n        <td></td>'

        htmlRowEnd =  '\n        <td></td>'
        htmlRowEnd += '\n    </tr></div>'

        htmlNothing =  '\n        <td></td>'

        htmlStudents = utils_functions.u('')

        NB_COLS = 6
        NB_ROWS = 5
        col = row = -1
        for i in range(self.trombinoscopeList.count()):
            item = self.trombinoscopeList.item(i)
            itemData = item.data(QtCore.Qt.UserRole)

            fileName = utils_functions.u(
                '{0}/protected/photos/{1}.jpeg').format(
                    admin.dirLocalPrive, itemData[1])
            if QtCore.QFile(fileName).exists():
                destFile = utils_functions.u(
                    '{0}/{1}.jpeg').format(
                        tempDir, itemData[1])
                utils_filesdirs.removeAndCopy(fileName, destFile)
                fileName = '{0}.jpeg'.format(itemData[1])
            else:
                fileName = 'unknown-user.png'

            if withClassName:
                htmlStudent = utils_functions.u(
                    htmlStudentWithClass).format(
                        fileName, itemData[2], itemData[3], itemData[4])
            else:
                htmlStudent = utils_functions.u(
                    htmlStudentNoClass).format(
                        fileName, itemData[2], itemData[3])

            col += 1
            if col == 0:
                if (row > 2) and (row % NB_ROWS == 0):
                    htmlStudents = utils_functions.u('{0}{1}').format(
                        htmlStudents, htmlNewPage)
                else:
                    htmlStudents = utils_functions.u('{0}{1}').format(
                        htmlStudents, htmlNewRow)
            htmlStudents = utils_functions.u('{0}{1}').format(
                htmlStudents, htmlStudent)
            if col == NB_COLS - 1:
                htmlStudents = utils_functions.u('{0}{1}').format(
                    htmlStudents, htmlRowEnd)
                col = -1
                row += 1
        if col < NB_COLS - 1:
            for i in range(NB_COLS - col):
                htmlStudents = utils_functions.u('{0}{1}').format(
                    htmlStudents, htmlNothing)

        htmlCurrent = htmlCurrent.replace(
            '${TITLE}', 
            htmlTitle
            )
        htmlCurrent = htmlCurrent.replace(
            '${STUDENTS}', htmlStudents)
        if utils.OS_NAME[0] == 'win':
            htmlCurrent = utils_html.encodeHtml(htmlCurrent)

        fileName = 'temp'
        tempFileName = utils_functions.u(
            '{0}/{1}.html').format(tempDir, fileName)
        if QtCore.QFile(tempFileName).exists():
            QtCore.QFile(tempFileName).remove()
        outFile = QtCore.QFile(tempFileName)
        if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(outFile)
            stream.setCodec('UTF-8')
            stream << htmlCurrent
            outFile.close()

        # création du fichier pdf :
        tempFileName = QtCore.QFileInfo(tempFileName).absoluteFilePath()
        tempFileNamePdf = utils_functions.u(
            '{0}/{1}.pdf').format(tempDir, fileName)
        # d'abord dans temp car wkhtmltopdf n'aime pas 
        # les chemins accentués ou avec espaces
        footer = ('', '', True)
        utils_pdf.htmlToPdf(self.main, tempFileName, tempFileNamePdf, footer)

        utils_filesdirs.removeAndCopy(tempFileNamePdf, fileNamePdf)




class PhotosUtilsDlg(QtWidgets.QDialog):
    """
    les outils.
    """
    def __init__(self, parent=None):
        super(PhotosUtilsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'manage-photos'
        # des modifications ont été faites :
        self.modified = False

        # les messages :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)

        # mise en forme des boutons :
        self.fromDir_Button = QtWidgets.QToolButton()
        self.clearPhotos_Button = QtWidgets.QToolButton()
        self.uploadPhotos_Button = QtWidgets.QToolButton()
        self.doPdfFiles_Button = QtWidgets.QToolButton()
        buttons = {
            self.fromDir_Button: (
                'import-photos', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Import photos'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Import photos from a directory'), 
                self.photosFromDir), 
            self.clearPhotos_Button: (
                'photos-clear', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Clear photos'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Clear photos of students who left the school'), 
                self.doClearPhotos), 
            self.uploadPhotos_Button: (
                'net-upload', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Send photos'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'To send the photos on the website'), 
                self.doUploadPhotos), 
            self.doPdfFiles_Button: (
                'extension-pdf', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Classes PDF'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'To create pdf files of trombinoscopes of all classes'), 
                self.doPdfFiles), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize'] * 2
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])

        # formatage des noms de fichiers :
        msg0 = QtWidgets.QApplication.translate(
            'main', 
            'Indicated below the file name format to import.')
        msg1 = ''
        msg2 = QtWidgets.QApplication.translate(
            'main', 
            'Use the following benchmarks (see help for more explanation):')
        msg10 = QtWidgets.QApplication.translate(
            'main', ": the student's name")
        msg11 = QtWidgets.QApplication.translate(
            'main', ': her first name')
        msg12 = QtWidgets.QApplication.translate(
            'main', ': her class')
        msg13 = QtWidgets.QApplication.translate(
            'main', ': her identifier')
        self.formatTranslations = {
            '[NAME]' : QtWidgets.QApplication.translate('main', '[NAME]'), 
            '[FirstName]' : QtWidgets.QApplication.translate('main', '[FirstName]'), 
            '[Class]' : QtWidgets.QApplication.translate('main', '[Class]'), 
            '[ID]' : QtWidgets.QApplication.translate('main', '[ID]'), 
            }
        formatMessage = utils_functions.u(
            '<br/><b>{0}</b><br/>{1}<br/>{2}'
            '<ul>'
            '<li>{3}{4}</li>'
            '<li>{5}{6}</li>'
            '<li>{7}{8}</li>'
            '<li>{9}{10}</li>'
            '</ul>').format(
                msg0, msg1, msg2, 
                self.formatTranslations['[NAME]'], msg10, 
                self.formatTranslations['[FirstName]'], msg11, 
                self.formatTranslations['[Class]'], msg12, 
                self.formatTranslations['[ID]'], msg13)
        formatLabel = QtWidgets.QLabel(formatMessage)
        formatFilesNames = utils_functions.u(
            '{0} {1}').format(
                self.formatTranslations['[NAME]'], 
                self.formatTranslations['[FirstName]'])
        self.formatEdit = QtWidgets.QLineEdit(formatFilesNames)
        self.formatEdit.setMinimumWidth(300)
        formatLayout = QtWidgets.QVBoxLayout()
        formatLayout.addWidget(formatLabel)
        formatLayout.addWidget(self.formatEdit)

        # première série d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.fromDir_Button)
        hLayout0.addLayout(formatLayout)
        hLayout0.addStretch(1)
        hLayout1 = QtWidgets.QHBoxLayout()
        hLayout1.addWidget(self.clearPhotos_Button)
        hLayout1.addWidget(self.uploadPhotos_Button)
        hLayout1.addWidget(self.doPdfFiles_Button)
        hLayout1.addStretch(1)

        # mise en place :
        self.statusBar = QtWidgets.QStatusBar()
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(hLayout0)
        labelSeparator = QtWidgets.QLabel('<hr width="80%">')
        vLayout.addWidget(labelSeparator)
        vLayout.addLayout(hLayout1)
        vLayout.addStretch(1)
        grid = QtWidgets.QHBoxLayout()
        grid.addLayout(vLayout)
        grid.addLayout(messagesLayout)
        self.setLayout(grid)

    def photosFromDir(self):

        reversedTranslations = {}
        for text in self.formatTranslations:
            reversedTranslations[self.formatTranslations[text]] = text
        fields = []
        separators = []
        formatText = self.formatEdit.text()
        for p in formatText.split('['):
            pp = p.split(']')
            try:
                if pp[0] != '':
                    field = utils_functions.u('[{0}]').format(pp[0])
                    field = reversedTranslations.get(field, 'ERROR')
                    fields.append(field)
                if not(pp[1] in ('', ' ')):
                    separators.append(pp[1])
            except:
                continue
        if 'ERROR' in fields:
            message = QtWidgets.QApplication.translate(
                'main', 
                'The proposed format contains errors.')
            message = utils_functions.u(
                '<p><b>{0}</b></p>').format(message)
            reponse = utils_functions.messageBox(
                self.main, 
                level='critical', 
                message=message)
            return

        title = QtWidgets.QApplication.translate(
            'main', 
            'Select the folder containing the photos')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            title, 
            QtCore.QDir.homePath(), 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        utils_functions.doWaitCursor()
        try:
            result = {'OK': {}, 'NO': {}}
            replacements = ['-', '—', '  ', '  ', '  ']
            for what in separators:
                replacements.append(what)
            extensions = ('jpg', 'jpeg', 'png', )
            # récupération de la liste des élèves :
            students = {}
            query_admin = utils_db.query(admin.db_admin)
            commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                eleveName = query_admin.value(2)
                eleveFirstName = query_admin.value(3)
                eleveClass = query_admin.value(4)
                eleveAll = utils_functions.u('{0} {1} [{2}]').format(
                    eleveName, eleveFirstName, eleveClass)
                baseName = utils_functions.u('')
                for field in fields:
                    if field == '[NAME]':
                        baseName = utils_functions.u('{0} {1}').format(baseName, eleveName)
                    elif field == '[FirstName]':
                        baseName = utils_functions.u('{0} {1}').format(baseName, eleveFirstName)
                    elif field == '[Class]':
                        baseName = utils_functions.u('{0} {1}').format(baseName, eleveClass)
                    elif field == '[ID]':
                        baseName = utils_functions.u('{0} {1}').format(baseName, id_eleve)
                baseName = utils_functions.doAscii(baseName[1:])
                for replaceWhat in replacements:
                    baseName = baseName.replace(
                        utils_functions.u(replaceWhat), ' ')
                students[baseName] = (id_eleve, eleveAll)

            # récupération de la liste des fichiers :
            filesNames = {}
            dirIterator = QtCore.QDirIterator(
                directory, QtCore.QDir.Files, QtCore.QDirIterator.Subdirectories)
            while dirIterator.hasNext():
                fileName = dirIterator.next()
                if QtCore.QFileInfo(fileName).suffix().lower() in extensions:
                    baseName = utils_functions.doAscii(
                        QtCore.QFileInfo(fileName).baseName())
                    for replaceWhat in replacements:
                        baseName = baseName.replace(
                            utils_functions.u(replaceWhat), ' ')
                    filesNames[baseName] = fileName
                    if baseName in students:
                        result['OK'][baseName] = students[baseName][0]
                    else:
                        result['NO'][baseName] = filesNames[baseName]
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate(
                    'main', 'Import photos from a directory'), 
                tags=('h2'))
            OK_Text = QtWidgets.QApplication.translate('main', 'Allocated photos')
            NO_Text = QtWidgets.QApplication.translate('main', 'Unassigned photos')
            RESULT_Text = QtWidgets.QApplication.translate('main', 'Balance sheet')
            utils_functions.afficheMessage(
                self.main, 
                ['****************************', OK_Text], 
                tags=('h3'))
            for baseName in result['OK']:
                utils_functions.afficheMessage(
                    self.main, 
                    students[baseName][1], 
                    tags=('h4'))
            utils_functions.afficheMessage(
                self.main, 
                ['****************************', NO_Text], 
                tags=('h3'))
            for baseName in result['NO']:
                utils_functions.afficheMessage(
                    self.main, 
                    utils_functions.u('{0} : {1}').format(
                        baseName, result['NO'][baseName]), 
                    tags=('h4'))
            utils_functions.afficheMessage(
                self.main, 
                ['****************************', RESULT_Text], 
                tags=('h3'))
            utils_functions.afficheMessage(
                self.main, 
                utils_functions.u('{0}: {1} --- {2}: {3}').format(
                    OK_Text, len(result['OK']), NO_Text, len(result['NO'])), 
                tags=('h4'), p=True)
            utils_functions.afficheMessage(
                self.main, '****************************', tags=('h3'))

            for baseName in result['OK']:
                fileName = utils_functions.u(filesNames[baseName])
                id_eleve = students[baseName][0]
                try:
                    icon = QtGui.QIcon(fileName)
                    pixmap = icon.pixmap(PHOTOS_WIDTH, PHOTOS_HEIGHT)
                    if (pixmap.width() < PHOTOS_WIDTH) or (pixmap.height() < PHOTOS_HEIGHT):
                        x, y = 0, 0
                        if pixmap.width() < PHOTOS_WIDTH:
                            x = (PHOTOS_WIDTH - pixmap.width()) // 2
                        if pixmap.height() < PHOTOS_HEIGHT:
                            y = (PHOTOS_HEIGHT - pixmap.height()) // 2
                        pixmap2 = QtGui.QPixmap(PHOTOS_WIDTH, PHOTOS_HEIGHT)
                        pixmap2.fill()
                        painter = QtGui.QPainter(pixmap2)
                        painter.drawPixmap(x, y, pixmap, 0, 0, PHOTOS_WIDTH, PHOTOS_HEIGHT)
                        painter.end()
                        pixmap = pixmap2
                    fileName = utils_functions.u(
                        '{0}/protected/photos/{1}.jpeg').format(
                            admin.dirLocalPrive, id_eleve)
                    pixmap.save(fileName, 'JPG')
                except:
                    utils_functions.afficheMessage(
                        self.main, 
                        'EXCEPT IN photosFromDir : {0}'.format(id_eleve), 
                        tags=('h4'))
                    continue
        finally:
            utils_functions.restoreCursor()
        self.parent.tabTrombinoscope.filterComboBoxChanged(
            self.parent.tabTrombinoscope.filterComboBox.currentIndex())
        self.parent.mustUpload = True

    def doClearPhotos(self):
        utils_functions.doWaitCursor()
        try:
            # récupération de la liste des id des élèves :
            students = []
            query_admin = utils_db.query(admin.db_admin)
            commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                students.append(id_eleve)
            # récupération de la liste des fichiers à supprimer :
            extensions = ('jpeg', )
            directory = utils_functions.u(
                '{0}/protected/photos').format(admin.dirLocalPrive)
            oldFilesNames = []
            dirIterator = QtCore.QDirIterator(
                directory, QtCore.QDir.Files, QtCore.QDirIterator.Subdirectories)
            while dirIterator.hasNext():
                fileName = dirIterator.next()
                if QtCore.QFileInfo(fileName).suffix().lower() in extensions:
                    baseName = QtCore.QFileInfo(fileName).baseName()
                    try:
                        baseName = int(baseName)
                        if not(baseName in students):
                            oldFilesNames.append(fileName)
                    except:
                        oldFilesNames.append(fileName)
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate(
                    'main', 'Clear photos of students who left the school'), 
                tags=('h2'))
            RESULT_Text = QtWidgets.QApplication.translate(
                'main', 'Number of photos deleted:')
            RESULT_Text = utils_functions.u('{0} {1}').format(
                RESULT_Text, len(oldFilesNames))
            utils_functions.afficheMessage(
                self.main, 
                ['****************************', RESULT_Text], 
                tags=('h3'))
            # suppression des fichiers :
            for fileName in oldFilesNames:
                QtCore.QFile(fileName).remove()
                utils_functions.afficheMessage(
                    self.main, fileName, tags=('h4'))
        finally:
            utils_functions.restoreCursor()
        self.parent.mustUpload = True

    def doUploadPhotos(self):
        uploadPhotos(self.main)
        self.parent.mustUpload = False

    def doPdfFiles(self):
        """
        """
        htmlTitleBase = QtWidgets.QApplication.translate(
            'main', 'Class Trombinoscope:')
        tempDir = utils_functions.u(
            '{0}/photos').format(self.main.tempPath)
        modelesDir = utils_functions.u(
            '{0}/files').format(self.main.beginDir)
        utils_filesdirs.createDirs(self.main.tempPath, 'photos')
        files = (
            ('', 'trombinoscope.html'), 
            ('', 'trombinoscope.css'), 
            ('md/', 'bootstrap.min.css'), 
            ('', 'unknown-user.png'))
        for (subDir, fileName) in files:
            utils_filesdirs.removeAndCopy(
                utils_functions.u('{0}/{1}{2}').format(modelesDir, subDir, fileName), 
                utils_functions.u('{0}/{1}').format(tempDir, fileName))
        fileName = utils_functions.u('{0}/trombinoscope.html').format(tempDir)
        inFile = QtCore.QFile(fileName)
        if not inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            message = QtWidgets.QApplication.translate(
                'main', 'Cannot read file {0}:\n{1}.').format(fileName, inFile.errorString())
            utils_functions.messageBox(self.main, level='warning', message=message)
            return
        indentSize = 4
        textStream = QtCore.QTextStream(inFile)
        textStream.setCodec('UTF-8')
        htmlModele = textStream.readAll()
        inFile.close()

        utils_functions.doWaitCursor()
        try:
            index = 0
            for classe in self.parent.tabs[0].eleves['CLASSES']:
                index += 1
                self.parent.tabs[0].filterComboBoxChanged(index)

                name = self.parent.tabs[0].filterComboBox.currentText()
                htmlTitle = utils_functions.u('{0} {1}').format(htmlTitleBase, name)
                utils_functions.afficheMessage(self.main, htmlTitle, editLog=True)
                fileTitle = utils_functions.doAscii(name, case='lower')
                replacements = [' ', '.']
                for replaceWhat in replacements:
                    fileTitle = fileTitle.replace(
                        utils_functions.u(replaceWhat), '_')
                fileTitle = utils_functions.u('trombinoscope-{0}').format(fileTitle)

                fileNamePdf = utils_functions.u(
                    '{0}/fichiers/pdf/{1}.pdf').format(admin.adminDir, fileTitle)

                self.parent.tabs[0].doPdf(htmlModele, tempDir, htmlTitle, fileNamePdf)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheMsgFin(self.main, timer=5)

