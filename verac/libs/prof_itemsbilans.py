# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Gestion des items et des bilans.
    Mais aussi des liens et des commentaires.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_webengine, utils_db, utils_graphwidget, utils_2lists

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtSql
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4 import QtSql



###########################################################"
#   FONCTIONS DE CRÉATION ET D'ÉDITION
###########################################################"

def createItem(main, itemName, itemLabel):
    """
    création d'un nouvel item.
    On vérifie qu'il n'existe pas déjà, et on cherche un id libre.
    """
    utils_functions.doWaitCursor()
    try:
        result = True
        id_item = -1
        commandLine_my = utils_db.qs_prof_AllWhereName.format('items')
        query_my = utils_db.queryExecute(
            {commandLine_my: (itemName, )}, db=main.db_my)
        if query_my.first():
            id_item = int(query_my.value(0))
            result = False
        if result:
            # on récupère la liste des id_item :
            ids_items = []
            query_my = utils_db.queryExecute(
                utils_db.q_selectAllFromOrder.format('items', 'id_item'), 
                query=query_my)
            while query_my.next():
                ids_items.append(int(query_my.value(0)))
            # on cherche le premier id_item disponible :
            id_item = 0
            while id_item in ids_items:
                id_item += 1
            commandLine_my = utils_db.insertInto('items')
            query_my = utils_db.queryExecute(
                {commandLine_my: (id_item, itemName, itemLabel)}, 
                query=query_my)
    finally:
        utils_functions.restoreCursor()
        return result, id_item

def editItem(main, id_item, itemName, itemLabel):
    """
    modification d'un item
    """
    utils_functions.doWaitCursor()
    try:
        result = False
        # on supprime l'ancien item :
        commandLine_my = utils_db.qdf_where.format('items', 'id_item', id_item)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        # et on le recrée :
        commandLine_my = utils_db.insertInto('items')
        query_my = utils_db.queryExecute(
            {commandLine_my: (id_item, itemName, itemLabel)}, 
            query=query_my)
        result = True
    finally:
        utils_functions.restoreCursor()
        return result

def createBilan(main, bilanName, bilanLabel, id_competence):
    """
    création d'un bilan.
    On vérifie qu'il n'existe pas déjà, et on cherche un id libre.
    """
    utils_functions.doWaitCursor()
    try:
        result = True
        id_bilan = -1
        commandLine_my = utils_db.qs_prof_AllWhereName.format('bilans')
        query_my = utils_db.queryExecute(
            {commandLine_my: (bilanName, )}, db=main.db_my)
        if query_my.first():
            id_bilan = int(query_my.value(0))
            result = False
        if result:
            # on récupère la liste des id_bilan :
            ids_bilans = []
            query_my = utils_db.queryExecute(
                utils_db.q_selectAllFromOrder.format('bilans', 'id_bilan'), 
                query=query_my)
            while query_my.next():
                ids_bilans.append(int(query_my.value(0)))
            # on cherche le premier id_bilan disponible :
            id_bilan = 0
            while id_bilan in ids_bilans:
                id_bilan += 1
            commandLine_my = utils_db.insertInto('bilans')
            query_my = utils_db.queryExecute(
                {commandLine_my: (id_bilan, bilanName, bilanLabel, id_competence)}, 
                query=query_my)
    finally:
        utils_functions.restoreCursor()
        return result, id_bilan

def editBilan(main, id_bilan, bilanName, bilanLabel):
    """
    modification d'un bilan (Nom ou Label)
    """
    utils_functions.doWaitCursor()
    try:
        result = False
        # on récupère id_competence :
        id_competence = -1
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'bilans', 'id_bilan', id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        if query_my.first():
            id_competence = int(query_my.value(3))
        # on supprime l'ancien bilan :
        commandLine_my = utils_db.qdf_where.format('bilans', 'id_bilan', id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # et on le recrée :
        commandLine_my = utils_db.insertInto('bilans')
        query_my = utils_db.queryExecute(
            {commandLine_my: (id_bilan, bilanName, bilanLabel, id_competence)}, 
            query=query_my)
        result = True
    finally:
        utils_functions.restoreCursor()
        return result

def editLink(main, id_item, id_bilan, coeff):
    """
    modification d'un lien item -> bilan
    """
    utils_functions.doWaitCursor()
    try:
        result = False
        # on efface l'éventuelle précédente liaison :
        commandLine_my = utils_db.qdf_prof_itemBilan3.format(id_item, id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        # on n'écrit que s'il y a un coeff :
        if coeff != '':
            commandLine_my = utils_db.insertInto('item_bilan')
            query_my = utils_db.queryExecute(
                {commandLine_my: (id_item, id_bilan, coeff)}, 
                query=query_my)
        result = True
    finally:
        utils_functions.restoreCursor()
        return result

def createCptFromList(main, tableName, dialog, what='CreateItems'):
    """
    après avoir sélectionné des compétences du bulletin etc,
    cette fonction crée soit :
        les items, bilans et liaisons correspondants (si what='CreateItems')
        les bilans demandés (si what='CreateBilans')
    """
    # listCPT contiendra chaque compétence à créer [id_competence, name, label]
    listCPT = []
    for selectedIndex in dialog.cptView.selectedIndexes():
        if selectedIndex.column() == 0:
            item = selectedIndex.data(QtCore.Qt.UserRole)
            for cpt in item.lastChilds():
                name = cpt.itemData[0]
                label = cpt.itemData[1]
                id_cpt = cpt.itemData[3]
                # on calcule id_competence :
                if tableName == 'bulletin':
                    id_competence = id_cpt + utils.decalageBLT
                elif tableName == 'referentiel':
                    id_competence = id_cpt
                elif tableName == 'confidentiel':
                    id_competence = id_cpt + utils.decalageCFD
                # on ajoute à listCPT si besoin :
                utils_functions.appendUnique(listCPT, [id_competence, name, label])
    # on crée maintenant les items et/ou bilans :
    for [id_competence, name, label] in listCPT:
        if what == 'CreateItems':
            result, id_item = createItem(main, name, label)
            result, id_bilan = createBilan(main, name, label, id_competence)
            editLink(main, id_item, id_bilan, 1)
        elif what == 'CreateBilans':
            result, id_bilan = createBilan(main, name, label, id_competence)



###########################################################"
#   DIALOGUES DE SÉLECTION, CRÉATION OU ÉDITION
###########################################################"

class ChooseBilanDlg(QtWidgets.QDialog):
    """
    dialogue permettant de choisir un bilan
    dans une liste déroulante
    """
    def __init__(self, parent=None, comments='', helpContextPage='items-bilans'):
        super(ChooseBilanDlg, self).__init__(parent)

        self.main = parent
        self.helpContextPage = helpContextPage
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'ChooseBilan'))
        # des explications :
        labelComments = QtWidgets.QLabel(comments)

        # la liste déroulante :
        labelText = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Balance:'))
        label = QtWidgets.QLabel(labelText)
        self.comboBox = QtWidgets.QComboBox()
        # les boutons :
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(labelComments, 1, 0, 1, 2, QtCore.Qt.AlignHCenter)
        grid.addWidget(label, 2, 0)
        grid.addWidget(self.comboBox, 2, 1)
        grid.addWidget(buttonBox, 4, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(300)
        self.setMaximumWidth(500)

        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
            db=self.main.db_my)
        while query_my.next():
            t = utils_functions.u('{0} : {1}').format(query_my.value(1), query_my.value(2))
            self.comboBox.addItem(t, int(query_my.value(0)))

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage)

class ChooseBilanForDirsDlg(QtWidgets.QDialog):
    """
    dialogue permettant de choisir un bilan
    dans une liste déroulante pour la création d'une arborescence.
    Par rapport à ChooseBilanDlg, on ajoute des choix sur les noms
    à donner aux dossiers.
    """
    def __init__(self, parent=None, 
                 comments='', dirsNamesComments='', 
                 helpContextPage='items-bilans'):
        super(ChooseBilanForDirsDlg, self).__init__(parent)

        self.main = parent
        self.helpContextPage = helpContextPage
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'ChooseBilan'))
        # des explications :
        labelComments = QtWidgets.QLabel(comments)

        # la liste déroulante :
        labelText = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Balance:'))
        label = QtWidgets.QLabel(labelText)
        self.comboBox = QtWidgets.QComboBox()

        # formatage des noms des sous-dossier à créer
        # (avec un label d'explications) :
        bilanDirNameFormat = utils_db.readInConfigDict(
            self.main.configDict, 'bilanDirNameFormat', default_text='{0}-{1}')[1]
        itemsDirNameFormat = utils_db.readInConfigDict(
            self.main.configDict, 'itemsDirNameFormat', default_text='{2}-{3}')[1]
        labelDirsNamesComments = QtWidgets.QLabel(dirsNamesComments)
        translatedText = utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Bilan directory name:'))
        bilanDirNameLabel = QtWidgets.QLabel(translatedText)
        self.bilanDirNameEdit = QtWidgets.QLineEdit(bilanDirNameFormat)
        translatedText = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Items directorys names:'))
        itemsDirNameLabel = QtWidgets.QLabel(translatedText)
        self.itemsDirNameEdit = QtWidgets.QLineEdit(itemsDirNameFormat)

        # les boutons :
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(labelComments, 1, 0, 1, 2, QtCore.Qt.AlignHCenter)
        grid.addWidget(label, 2, 0)
        grid.addWidget(self.comboBox, 2, 1)
        grid.addWidget(labelDirsNamesComments, 5, 0, 1, 2, QtCore.Qt.AlignHCenter)
        grid.addWidget(bilanDirNameLabel,     6, 0)
        grid.addWidget(self.bilanDirNameEdit, 6, 1)
        grid.addWidget(itemsDirNameLabel,     7, 0)
        grid.addWidget(self.itemsDirNameEdit, 7, 1)
        grid.addWidget(buttonBox, 9, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(300)
        self.setMaximumWidth(500)

        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
            db=self.main.db_my)
        while query_my.next():
            t = utils_functions.u('{0} : {1}').format(query_my.value(1), query_my.value(2))
            self.comboBox.addItem(t, int(query_my.value(0)))

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage)

class ChooseCptFromListDlg(QtWidgets.QDialog):
    """
    dialogue permettant de choisir une ou des compétences
    dans une liste spéciale (bulletin, référentiel, confidentiel)
    """
    def __init__(self, parent=None, 
                 tableName='bulletin', extendedSelection=False, onlyCpt=True):
        super(ChooseCptFromListDlg, self).__init__(parent)

        self.main = parent
        self.tableName = tableName
        self.extendedSelection = extendedSelection
        self.onlyCpt = onlyCpt

        # le titre de la fenêtre :
        if extendedSelection:
            if self.tableName == 'bulletin':
                title = QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLT')
            elif self.tableName == 'referentiel':
                title = QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPT')
            elif self.tableName == 'confidentiel':
                title = QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFD')
        else:
            if self.tableName == 'bulletin':
                title = QtWidgets.QApplication.translate('main', 'Choose competence from bulletin')
            elif self.tableName == 'referentiel':
                title = QtWidgets.QApplication.translate('main', 'Choose competence from referential')
            elif self.tableName == 'confidentiel':
                title = QtWidgets.QApplication.translate('main', 'Choose a confidential competence')
        self.setWindowTitle(title)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]
        self.okButton = dialogButtons[1]['ok']

        # model et view:
        titleView = (
            QtWidgets.QApplication.translate('main', 'Hierarchy'), 
            QtWidgets.QApplication.translate('main', 'Competence'),
            QtWidgets.QApplication.translate('main', 'ClassType'),
            'Id')
        cptModel = CptTreeModel(self.main, self.tableName, titleView)
        self.cptView = QtWidgets.QTreeView()
        self.cptView.setModel(cptModel)
        if self.extendedSelection:
            self.cptView.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        else:
            if utils.PYQT == 'PYQT5':
                selectionModel = QtCore.QItemSelectionModel(cptModel)
            else:
                selectionModel = QtGui.QItemSelectionModel(cptModel)
            self.cptView.setSelectionModel(selectionModel)
            selectionModel.currentRowChanged.connect(self.cptChanged)
        self.cptView.expandAll()
        self.cptView.setColumnWidth(0, 300)
        self.cptView.setColumnWidth(1, 400)
        self.cptView.setColumnHidden(3, True)
        self.id_cpt = -1

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.cptView)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)
        self.setWindowState(self.windowState() or QtCore.Qt.WindowMaximized)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('choose-cpt')

    def cptChanged(self, index):
        """
        Pour que le bouton OK ne soit disponible
        que si la sélection est bien une compétence et pas un titre.
        Ne concerne que la version avec sélection simple (extendedSelection=False).
        """
        text = index.model().data(index, QtCore.Qt.DisplayRole)
        self.id_cpt = index.model().itemId
        if self.onlyCpt:
            self.okButton.setEnabled(self.id_cpt > -1)

class CreateMachinDlg(QtWidgets.QDialog):
    """
    dialogue permettant de créer ou éditer un item ou un bilan.
    paramètres :
        what : CreateItem, EditItem, CreateBilan, EditBilan
        withChoose : avec les 3 boutons pour sélectionner une compétence partagée
        id_item, id_bilan : pour éditer
    Pour un item ou pour un bilan perso, on peut aussi éditer le conseil (pour les élèves).
    """
    def __init__(self, parent=None, what='CreateItem', withChoose=True, id_item=-1, id_bilan=-1):
        super(CreateMachinDlg, self).__init__(parent)

        self.main = parent
        self.what = what
        # le titre de la fenêtre :
        if self.what == 'CreateItem':
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Create an item'))
        elif self.what == 'EditItem':
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Edit an item'))
            withChoose = False
        elif self.what == 'CreateBilan':
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Create a balance'))
        elif self.what == 'EditBilan':
            self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Edit a balance'))
            withChoose = False
        self.withChoose = withChoose
        # les id utiles :
        self.id_item = id_item
        self.id_bilan = id_bilan
        self.id_competence = -1
        # nom :
        nameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Name:')))
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.setMinimumWidth(200)
        # description :
        labelLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Label:')))
        self.labelEdit = QtWidgets.QLineEdit()
        self.labelEdit.setMinimumWidth(200)
        # conseil donné aux élèves :
        text = QtWidgets.QApplication.translate('main', 'Advice:')
        adviceLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.adviceEdit = QtWidgets.QLineEdit()
        self.adviceEdit.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Advices to progress that students can see in the web interface.'))
        self.adviceEditButton = QtWidgets.QPushButton(
            utils.doIcon('comments'),
            QtWidgets.QApplication.translate('main', 'Edit'))
        self.adviceEditButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'AdviceEditStatusTip'))
        self.adviceEditButton.clicked.connect(self.adviceEditWysiwyg)
        # un CheckBox pour les bilans persos sans calculs :
        if self.what in ('CreateBilan', 'EditBilan'):
            self.uncalculatedLabel = QtWidgets.QLabel(utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Uncalculated:')))
            self.uncalculatedCheckBox = QtWidgets.QCheckBox()
            self.uncalculatedCheckBox.setToolTip(QtWidgets.QApplication.translate(
                'main', 'If this box is checked, this report will not be calculated.'))
        # pour éditer les liens item-bilan :
        if (id_bilan > -1) and (id_item > -1):
            label3 = QtWidgets.QLabel(utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'coefficient:')))
            self.spinBox3 = QtWidgets.QSpinBox()
            self.spinBox3.setRange(1, 10)
            self.spinBox3.setSingleStep(1)
            self.spinBox3.setValue(1)
        # les 3 boutons pour sélectionner une compétence partagée :
        if self.withChoose:
            cptChooseIcon = utils.doIcon('cpt-choose')
            self.chooseBLTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'Choose competence from bulletin'))
            self.chooseBLTButton.clicked.connect(self.doChooseFromList)
            self.chooseCPTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'Choose competence from referential'))
            self.chooseCPTButton.clicked.connect(self.doChooseFromList)
            self.chooseCFDButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'Choose a confidential competence'))
            self.chooseCFDButton.clicked.connect(self.doChooseFromList)
        # le dialogButtons final :
        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(nameLabel, 1, 0)
        grid.addWidget(self.nameEdit, 1, 1, 1, 2)
        grid.addWidget(labelLabel, 2, 0)
        grid.addWidget(self.labelEdit, 2, 1, 1, 2)
        grid.addWidget(adviceLabel, 3, 0)
        grid.addWidget(self.adviceEdit, 3, 1)
        grid.addWidget(self.adviceEditButton, 3, 2)
        if self.what in ('CreateBilan', 'EditBilan'):
            grid.addWidget(self.uncalculatedLabel, 4, 0)
            grid.addWidget(self.uncalculatedCheckBox, 4, 1)
            if (id_bilan > -1) and (id_item > -1):
                grid.addWidget(label3, 5, 0)
                grid.addWidget(self.spinBox3, 5, 1, 1, 2)
        if self.withChoose:
            grid.addWidget(self.chooseBLTButton, 15, 0, 1, 3)
            grid.addWidget(self.chooseCPTButton, 16, 0, 1, 3)
            grid.addWidget(self.chooseCFDButton, 17, 0, 1, 3)
        grid.addWidget(buttonBox, 99, 0, 1, 3)
        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # en cas de sélection d'une compétence d'une liste comme item,
        # on récupère nom et label pour le bilan lié :
        self.cptName = ''
        self.cptLabel = ''
        # si on édite, on récupère l'ancien nom et les autres données utiles :
        self.oldAdvice = ''
        if self.what == 'EditItem':
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'items', 'id_item', id_item)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            if query_my.first():
                self.oldName = query_my.value(1)
                self.nameEdit.setText(self.oldName)
                self.labelEdit.setText(query_my.value(2))
            commandLine_my = utils_db.qs_prof_FromComments.format(1, id_item)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                self.oldAdvice = query_my.value(2)
            self.adviceEdit.setText(self.oldAdvice)
        elif self.what == 'EditBilan':
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'bilans', 'id_bilan', id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                self.oldName = query_my.value(1)
                self.nameEdit.setText(self.oldName)
                self.labelEdit.setText(query_my.value(2))
                self.id_competence = int(query_my.value(3))
            commandLine_my = utils_db.qs_prof_FromComments.format(0, id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                self.oldAdvice = query_my.value(2)
            self.adviceEdit.setText(self.oldAdvice)
            if self.id_competence > -1:
                self.uncalculatedLabel.setVisible(False)
                self.uncalculatedCheckBox.setVisible(False)
                self.adviceEdit.setEnabled(False)
                self.adviceEditButton.setEnabled(False)
            else:
                self.uncalculatedCheckBox.setChecked(self.id_competence == -2)
            if id_item > -1:
                commandLine_my = utils_db.qs_prof_AllFromItemBilanWhereItemBilan.format(
                    id_item, id_bilan)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    self.spinBox3.setValue(int(query_my.value(2)))

    def contextHelp(self):
        # ouvre la page d'aide associée
        if self.what in ('CreateItem', 'EditItem'):
            utils_functions.openContextHelp('create-item')
        elif self.what in ('CreateBilan', 'EditBilan'):
            utils_functions.openContextHelp('create-bilan')

    def doChooseFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(parent=self.main, tableName=tableName)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            if tableName == 'bulletin':
                self.id_competence = dialog.id_cpt + utils.decalageBLT
            elif tableName == 'referentiel':
                self.id_competence = dialog.id_cpt
            elif tableName == 'confidentiel':
                self.id_competence = dialog.id_cpt + utils.decalageCFD
            commandLine_commun = utils_db.q_selectAllFromWhere.format(
                tableName, 'id', dialog.id_cpt)
            query_commun = utils_db.queryExecute(commandLine_commun, db=self.main.db_commun)
            if query_commun.first():
                name = query_commun.value(1)
                label = query_commun.value(5)
                advice = query_commun.value(10)
                self.cptName = name
                self.cptLabel = label
                self.nameEdit.setText(name)
                self.labelEdit.setText(label)
                self.adviceEdit.setText(advice)
                if self.what in ('CreateItem', 'EditItem'):
                    self.nameEdit.setEnabled(True)
                    self.labelEdit.setEnabled(True)
                    self.adviceEdit.setEnabled(True)
                    self.adviceEditButton.setEnabled(True)
                else:
                    self.nameEdit.setEnabled(False)
                    self.labelEdit.setEnabled(False)
                    self.adviceEdit.setEnabled(False)
                    self.adviceEditButton.setEnabled(False)
                    self.uncalculatedLabel.setVisible(False)
                    self.uncalculatedCheckBox.setChecked(False)
                    self.uncalculatedCheckBox.setVisible(False)

    def adviceEditWysiwyg(self):
        """
        """
        import utils_htmleditor
        dialog = utils_htmleditor.HtmlEditorDlg(
            parent=self.main, content=self.adviceEdit.text())
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        advice = dialog.webViewWidget.webView.toHtml()
        advice = utils_htmleditor.formatAdvice(advice)
        self.adviceEdit.setText(advice)

    def accept(self):
        """
        blablabla
        """
        name = self.nameEdit.text()
        label = self.labelEdit.text()
        advice = self.adviceEdit.text()
        if name == '':
            message = QtWidgets.QApplication.translate('main', 'Please choose a name.')
            utils_functions.messageBox(self.main, level='warning', message=message)
            return
        elif label == '':
            message = QtWidgets.QApplication.translate('main', 'Please choose a label.')
            utils_functions.messageBox(self.main, level='warning', message=message)
            return
        if self.what == 'CreateItem':
            result, self.id_item = createItem(self.main, name, label)
        elif self.what == 'EditItem':
            result = True
            if name != self.oldName:
                id_item = self.id_item
                commandLine_my = utils_db.qs_prof_AllWhereName.format('items')
                query_my = utils_db.queryExecute(
                    {commandLine_my: (name, )}, db=self.main.db_my)
                while query_my.next():
                    id_item = int(query_my.value(0))
                if id_item != self.id_item:
                    result = False
        elif self.what == 'CreateBilan':
            if self.uncalculatedCheckBox.isChecked():
                self.id_competence = -2
            result, self.id_bilan = createBilan(self.main, name, label, self.id_competence)
        elif self.what == 'EditBilan':
            result = True
            if self.id_item > -1:
                coeff = self.spinBox3.value()
            if name != self.oldName:
                id_bilan = self.id_bilan
                commandLine_my = utils_db.qs_prof_AllWhereName.format('bilans')
                query_my = utils_db.queryExecute(
                    {commandLine_my: (name, )}, db=self.main.db_my)
                while query_my.next():
                    id_bilan = int(query_my.value(0))
                if id_bilan != self.id_bilan:
                    result = False
        if result:
            if self.what == 'CreateItem':
                if self.id_competence > -1:
                    r, id_bilan = createBilan(
                        self.main, self.cptName, self.cptLabel, self.id_competence)
                    editLink(self.main, self.id_item, id_bilan, 1)
            elif self.what == 'EditItem':
                commandLine_my = 'UPDATE items SET Name=?, Label=? WHERE id_item=?'
                query_my = utils_db.queryExecute(
                    {commandLine_my: (name, label, self.id_item)}, 
                    db=self.main.db_my)
            elif self.what == 'EditBilan':
                if self.id_competence < 0:
                    if self.uncalculatedCheckBox.isChecked():
                        commandLine_my1 = utils_functions.u(
                            'UPDATE bilans SET Name=?, Label=?, id_competence=-2 '
                            'WHERE id_bilan=?')
                    else:
                        commandLine_my1 = utils_functions.u(
                            'UPDATE bilans SET Name=?, Label=?, id_competence=-1 '
                            'WHERE id_bilan=?')
                query_my = utils_db.queryExecute(
                    {commandLine_my1: (name, label, self.id_bilan)}, 
                    db=self.main.db_my)
                if self.id_item != -1:
                    commandLine_my2 = (
                        'UPDATE item_bilan SET coeff={0} '
                        'WHERE id_item={1} AND id_bilan={2}')
                    commandLine_my2 = commandLine_my2.format(coeff, self.id_item, self.id_bilan)
                    query_my = utils_db.queryExecute(
                        commandLine_my2, query=query_my)
            if advice != self.oldAdvice:
                # on met à jour la base :
                if self.what in ('CreateItem', 'EditItem'):
                    isItem = 1
                    id_what = self.id_item
                else:
                    isItem = 0
                    id_what = self.id_bilan
                commandLine = 'DELETE FROM comments WHERE isItem={0} AND id={1}'.format(
                    isItem, id_what)
                query_my = utils_db.queryExecute(commandLine, db=self.main.db_my)
                commandLine = utils_db.insertInto('comments')
                if advice != '':
                    query_my = utils_db.queryExecute(
                        {commandLine: (isItem, id_what, advice)}, 
                        query=query_my)
            QtWidgets.QDialog.accept(self)
        else:
            errorMessage1 = QtWidgets.QApplication.translate(
                'main', 'The {0} name is already there.').format(
                    utils_functions.u('<b>{0}</b>').format(name))
            errorMessage2 = QtWidgets.QApplication.translate('main', 'Again.')
            errorMessage = utils_functions.u('<p>{0}</p><p><b>{1}</b></p>').format(
                errorMessage1, errorMessage2)
            utils_functions.messageBox(self, level='warning', message=errorMessage)

class AddBilanToItemDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None):
        super(AddBilanToItemDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Add a balance'))

        label1 = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Balance:')))
        label1.setMinimumWidth(100)
        label1.setMaximumWidth(100)
        self.comboBox1 = QtWidgets.QComboBox()
        self.comboBox1.setMinimumWidth(400)
        label2 = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'coefficient:')))
        self.spinBox2 = QtWidgets.QSpinBox()
        self.spinBox2.setRange(1, 10)
        self.spinBox2.setSingleStep(1)
        self.spinBox2.setValue(1)
        cptChooseIcon = utils.doIcon('cpt-choose')
        self.chooseBLTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'Choose competence from bulletin'))
        self.chooseBLTButton.clicked.connect(self.doChooseFromList)
        self.chooseCPTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'Choose competence from referential'))
        self.chooseCPTButton.clicked.connect(self.doChooseFromList)
        self.chooseCFDButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'Choose a confidential competence'))
        self.chooseCFDButton.clicked.connect(self.doChooseFromList)
        createBilanButton = QtWidgets.QPushButton(
            utils.doIcon('bilan-add'), 
            QtWidgets.QApplication.translate('main', 'Create a balance'))
        createBilanButton.clicked.connect(self.doCreateBilan)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label1, 1, 0)
        grid.addWidget(self.comboBox1, 1, 1)
        grid.addWidget(label2, 2, 0)
        grid.addWidget(self.spinBox2, 2, 1)
        grid.addWidget(self.chooseBLTButton, 3, 0, 1, 2)
        grid.addWidget(self.chooseCPTButton, 4, 0, 1, 2)
        grid.addWidget(self.chooseCFDButton, 5, 0, 1, 2)
        grid.addWidget(createBilanButton, 6, 0, 1, 2)
        grid.addWidget(buttonBox, 7, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)

        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
            db=self.main.db_my)
        while query_my.next():
            t = query_my.value(1) + ": " + query_my.value(2)
            self.comboBox1.addItem(t, int(query_my.value(0)))

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('gest-items')

    def doChooseFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(parent=self.main, tableName=tableName)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            if tableName == 'bulletin':
                id_competence = dialog.id_cpt + utils.decalageBLT
            elif tableName == 'referentiel':
                id_competence = dialog.id_cpt
            elif tableName == 'confidentiel':
                id_competence = dialog.id_cpt + utils.decalageCFD
            commandLine_commun = utils_db.q_selectAllFromWhere.format(
                tableName, 'id', dialog.id_cpt)
            query_commun = utils_db.queryExecute(commandLine_commun, db=self.main.db_commun)
            while query_commun.next():
                name = query_commun.value(1)
                label = query_commun.value(5)
                result, id_bilan = createBilan(self.main, name, label, id_competence)
                if result:
                    t = name + ": " + label
                    i = self.comboBox1.findText(t, QtCore.Qt.MatchContains)
                    if i == -1:
                        self.comboBox1.addItem(t, id_bilan)
                        i = self.comboBox1.findText(t, QtCore.Qt.MatchContains)
                    self.comboBox1.setCurrentIndex(i)
                else:
                    errorMessage1 = QtWidgets.QApplication.translate(
                        'main', 'The {0} name is already there.').format(
                            utils_functions.u('<b>{0}</b>').format(name))
                    errorMessage2 = QtWidgets.QApplication.translate('main', 'Again.')
                    errorMessage = utils_functions.u('<p>{0}</p><p><b>{1}</b></p>').format(
                        errorMessage1, errorMessage2)
                    utils_functions.messageBox(self, level='warning', message=errorMessage)

    def doCreateBilan(self):
        """
        appel d'un dialog spécifique
        """
        dialog = CreateMachinDlg(parent=self.main, what='CreateBilan', withChoose=False)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            bilanName = dialog.nameEdit.text()
            bilanLabel = dialog.labelEdit.text()
            id_bilan = dialog.id_bilan
            t = bilanName + ": " + bilanLabel
            self.comboBox1.addItem(t, id_bilan)
            self.comboBox1.setCurrentIndex(self.comboBox1.findText(t, QtCore.Qt.MatchContains))



###########################################################"
#   DIALOGUES GÉNÉRAUX DE GESTION DES ITEMS, BILANS ETC
###########################################################"

class GestItemsDlg(QtWidgets.QDialog):
    """
    gestion des items (et des bilans liés)
    """
    def __init__(self, parent=None):
        super(GestItemsDlg, self).__init__(parent)
        utils_functions.doWaitCursor()
        try:
            self.main = parent
            # le titre de la fenêtre :
            title = QtWidgets.QApplication.translate('main', 'Manage items')
            self.setWindowTitle(title)
            # des modifications ont été faites :
            self.modified = False

            # à gauche la liste des items et quelques boutons :
            self.id_item = -1
            self.itemsModel = GestItemsModel(self.main, 'items')
            # un QTableView pour l'affichage :
            self.itemsView = QtWidgets.QTableView()
            self.itemsView.setModel(self.itemsModel)
            # un delegate pour l'édition :
            self.itemsView.setItemDelegate(MyItemDelegate(self, 
                model=self.itemsModel, table='items', minInt=1))
            if utils.PYQT == 'PYQT5':
                selectionModel = QtCore.QItemSelectionModel(self.itemsModel)
            else:
                selectionModel = QtGui.QItemSelectionModel(self.itemsModel)
            self.itemsView.setSelectionModel(selectionModel)
            selectionModel.currentRowChanged.connect(self.itemsChanged)
            # les boutons :
            createItemButton = QtWidgets.QPushButton(
                utils.doIcon('item-add'), 
                QtWidgets.QApplication.translate('main', 'Create an item'))
            createItemButton.clicked.connect(self.doCreateItem)
            self.editItemButton = QtWidgets.QPushButton(
                utils.doIcon('item-edit'), 
                QtWidgets.QApplication.translate('main', 'EditThisItem'))
            self.editItemButton.clicked.connect(self.doEditItem)
            self.deleteItemButton = QtWidgets.QPushButton(
                utils.doIcon('item-delete'), 
                QtWidgets.QApplication.translate('main', 'DeleteThisItem'))
            self.deleteItemButton.clicked.connect(self.doDeleteItem)
            cptChooseIcon = utils.doIcon('cpt-choose')
            self.chooseBLTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLT'))
            self.chooseBLTButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLTStatusTip'))
            self.chooseBLTButton.clicked.connect(self.doChooseFromList)
            self.chooseCPTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPT'))
            self.chooseCPTButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPTStatusTip'))
            self.chooseCPTButton.clicked.connect(self.doChooseFromList)
            self.chooseCFDButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFD'))
            self.chooseCFDButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFDStatusTip'))
            self.chooseCFDButton.clicked.connect(self.doChooseFromList)
            # on termine la zone de gauche :
            chooseLayout = QtWidgets.QHBoxLayout()
            chooseLayout.addWidget(self.chooseBLTButton)
            chooseLayout.addWidget(self.chooseCPTButton)
            chooseLayout.addWidget(self.chooseCFDButton)
            itemsLayout = QtWidgets.QVBoxLayout()
            itemsLayout.addWidget(self.itemsView)
            itemsLayout.addWidget(createItemButton)
            itemsLayout.addWidget(self.editItemButton)
            itemsLayout.addWidget(self.deleteItemButton)
            itemsLayout.addLayout(chooseLayout)
            itemsGroupBox = QtWidgets.QGroupBox(self.main.TR_ITEMS)
            itemsGroupBox.setLayout(itemsLayout)

            # à droite la liste des bilans et quelques boutons :
            self.id_bilan = -1
            self.bilansModel = GestItemsModel(self.main, 'bilans')
            # un QTableView pour l'affichage :
            self.bilansView = QtWidgets.QTableView()
            self.bilansView.setModel(self.bilansModel)
            # un delegate pour l'édition :
            self.bilansView.setItemDelegate(MyItemDelegate(self, 
                model=self.bilansModel, table='bilans', minInt=1))
            if utils.PYQT == 'PYQT5':
                selectionModel = QtCore.QItemSelectionModel(self.bilansModel)
            else:
                selectionModel = QtGui.QItemSelectionModel(self.bilansModel)
            self.bilansView.setSelectionModel(selectionModel)
            selectionModel.currentRowChanged.connect(self.bilansChanged)
            # les boutons :
            self.addBilanButton = QtWidgets.QPushButton(
                utils.doIcon('bilan-add'), 
                QtWidgets.QApplication.translate('main', 'Add a balance'))
            self.addBilanButton.setEnabled(False)
            self.addBilanButton.clicked.connect(self.doAddBilan)
            self.editBilanButton = QtWidgets.QPushButton(
                utils.doIcon('bilan-edit'), 
                QtWidgets.QApplication.translate('main', 'EditThisBilan'))
            self.editBilanButton.setEnabled(False)
            self.editBilanButton.clicked.connect(self.doEditBilan)
            self.removeBilanButton = QtWidgets.QPushButton(
                utils.doIcon('bilan-remove'), 
                QtWidgets.QApplication.translate('main', 'RemoveThisBilan'))
            self.removeBilanButton.setEnabled(False)
            self.removeBilanButton.clicked.connect(self.doRemoveBilan)
            # on termine la zone de droite :
            bilansLayout = QtWidgets.QVBoxLayout()
            bilansLayout.addWidget(self.bilansView)
            bilansLayout.addWidget(self.addBilanButton)
            bilansLayout.addWidget(self.editBilanButton)
            bilansLayout.addWidget(self.removeBilanButton)
            bilansGroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Balances'))
            bilansGroupBox.setLayout(bilansLayout)

            # on regroupe les 2 zones :
            listesLayout = QtWidgets.QHBoxLayout()
            listesLayout.addWidget(itemsGroupBox)
            listesLayout.addWidget(bilansGroupBox)
            listesGroupBox = QtWidgets.QGroupBox()
            listesGroupBox.setLayout(listesLayout)
            listesGroupBox.setFlat(True)

            dialogButtons = utils_functions.createDialogButtons(
                self, layout=True, buttons=('ok', 'apply', 'cancel', 'help'))
            buttonsLayout = dialogButtons[0]
            self.buttonsList = dialogButtons[1]
            self.buttonsList['apply'].clicked.connect(self.doApply)
            self.buttonsList['apply'].setEnabled(False)

            # Mise en place :
            mainLayout = QtWidgets.QVBoxLayout()
            mainLayout.addWidget(listesGroupBox)
            mainLayout.addLayout(buttonsLayout)
            self.setLayout(mainLayout)

            self.initAll()
            try:
                self.itemsView.selectRow(0)
            except:
                pass
            self.itemsChanged(self.itemsView.currentIndex())
        finally:
            utils_functions.restoreCursor()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('gest-items')

    def initAll(self, onlyBilans=False):
        utils_functions.doWaitCursor()
        try:
            if not(onlyBilans):
                # on met à jour l'affichage des items :
                self.itemsModel.setupModelData()
                self.itemsView.setColumnHidden(0, True)
                self.itemsView.setAlternatingRowColors(True)
                self.itemsView.setSelectionMode(QtWidgets.QTableView.SingleSelection)
                self.itemsView.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
                self.itemsView.horizontalHeader().setStretchLastSection(True)
                #self.itemsView.setSortingEnabled(True)
                #self.itemsView.sortByColumn(1, QtCore.Qt.AscendingOrder)
                self.itemsView.resizeRowsToContents()
                self.itemsView.resizeColumnsToContents()
            # on met à jour l'affichage des bilans :
            self.bilansModel.setupModelData(id_itemSelected=self.id_item)
            self.bilansView.setColumnHidden(0, True)
            self.bilansView.setAlternatingRowColors(True)
            self.bilansView.setSelectionMode(QtWidgets.QTableView.SingleSelection)
            self.bilansView.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
            self.bilansView.horizontalHeader().setStretchLastSection(True)
            #self.bilansView.setSortingEnabled(True)
            #self.bilansView.sortByColumn(1, QtCore.Qt.AscendingOrder)
            self.bilansView.resizeRowsToContents()
            self.bilansView.resizeColumnsToContents()
        finally:
            utils_functions.restoreCursor()

    def itemsChanged(self, index):
        """
        on change l'item sélectionné
        on doit mettre à jour l'affichage des bilans
        """
        if index.isValid():
            self.id_item = self.itemsModel.datas[index.row()][0][2]
            self.addBilanButton.setEnabled(True)
            self.editItemButton.setEnabled(True)
            self.deleteItemButton.setEnabled(True)
        else:
            self.id_item = -1
            self.addBilanButton.setEnabled(False)
            self.editItemButton.setEnabled(False)
            self.deleteItemButton.setEnabled(False)
        self.initAll(onlyBilans=True)
        try:
            self.bilansView.selectRow(0)
        except:
            pass
        self.bilansChanged(self.bilansView.currentIndex())

    def doCreateItem(self):
        """
        appel d'un dialog spécifique
        """
        dialog = CreateMachinDlg(parent=self.main, what='CreateItem')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.initAll()
            # on sélectionne le nouvel item :
            selection = 0
            for line in self.itemsModel.datas:
                if line[0][2] == dialog.id_item:
                    selection = line[0][1] - 1
            self.itemsView.selectRow(selection)
            self.doModified()

    def doEditItem(self):
        """
        appel d'un dialog spécifique
        """
        selection = self.itemsView.currentIndex().row()
        dialog = CreateMachinDlg(parent=self.main, what='EditItem', id_item=self.id_item)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.initAll()
            self.itemsView.selectRow(selection)
            self.doModified()

    def doDeleteItem(self):
        """
        blablabla
        """
        utils_functions.afficheMessage(self.main, 'doDeleteItem')
        message = utils_functions.u('')

        commandLine_myBase = (
            'SELECT tableau_item.id_item, {1}.{2}, {1}.Name '
            'FROM tableau_item, {1} '
            'WHERE tableau_item.id_item={0} '
            'AND {1}.{2}=tableau_item.id_tableau')
        commandLine_my = commandLine_myBase.format(
            self.id_item, 'tableaux', 'id_tableau')
        query_my = utils_db.queryExecute(
            commandLine_my, db=self.main.db_my)
        while query_my.next():
            tableauName = query_my.value(2)
            message = message + "<p>" + tableauName + "</p>"
        commandLine_my = commandLine_myBase.format(
            self.id_item, 'templates', 'id_template')
        query_my = utils_db.queryExecute(
            commandLine_my, query=query_my)
        list_templates = []
        while query_my.next():
            id_template = int(query_my.value(1))
            list_templates.append(id_template)
            tableauName = query_my.value(2)
            message = message + "<p>" + tableauName + "</p>"
        message = QtWidgets.QApplication.translate(
            'main', 
            '<p><b>This Item is used in Tables or Templates:</b></p>'
            ' {0} <p><b>Continue ?</b></p>').format(message)
        if utils_functions.messageBox(
            self.main, 
            level='question', 
            message=message, 
            buttons=['Yes', 'No']) == QtWidgets.QMessageBox.No:
            return
        ListIdTableau = []
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'tableau_item', 'id_item', self.id_item)
        query_my = utils_db.queryExecute(
            commandLine_my, query=query_my)
        while query_my.next():
            id_tableau = int(query_my.value(0))
            ListIdTableau.append(id_tableau)
        for id_tableau in ListIdTableau:
            commandLine_my = utils_db.qdf_prof_tableauItem2.format(
                id_tableau, self.id_item)
            query_my = utils_db.queryExecute(
                commandLine_my, query=query_my)
        commandLine_my1 = utils_db.qdf_where.format('items', 'id_item', self.id_item)
        commandLine_my2 = utils_db.qdf_where.format('item_bilan', 'id_item', self.id_item)
        commandLine_my3 = utils_db.qdf_where.format('evaluations', 'id_item', self.id_item)
        commandLine_my4 = (
            'UPDATE counts SET id_item=-1 '
            'WHERE id_item IN ({0})').format(self.id_item)
        commandLine_my5 = (
            'DELETE FROM comments '
            'WHERE isItem=1 AND id IN ({0})').format(self.id_item)
        commandLines = [
            commandLine_my1, 
            commandLine_my2, 
            commandLine_my3, 
            commandLine_my4, 
            commandLine_my5]
        query_my = utils_db.queryExecute(commandLines, query=query_my)
        # on vérifie s'il faut rafraichir l'affichage
        commandLine_my = (
            'SELECT * FROM tableau_item '
            'WHERE id_tableau={0}').format(self.main.id_tableau)
        query_my = utils_db.queryExecute(
            commandLine_my, query=query_my)
        id_modele = 0
        if query_my.last():
            id_modele = int(query_my.value(1))
        # on sélectionne l'item suivant :
        exIndex = self.itemsView.currentIndex().row()
        if exIndex >= self.itemsModel.rowCount() - 1:
            exIndex -= 1
        self.initAll()
        try:
            self.itemsView.selectRow(exIndex)
        except:
            pass
        self.itemsChanged(self.itemsView.currentIndex())
        self.doModified()

    def bilansChanged(self, index):
        """
        on change le bilan sélectionné
        """
        if index.isValid():
            self.id_bilan = self.bilansModel.datas[index.row()][0][2]
            id_competence = self.bilansModel.datas[index.row()][0][3]
            self.editBilanButton.setEnabled(id_competence <= 0)
            self.removeBilanButton.setEnabled(True)
        else:
            self.id_bilan = -1
            self.editBilanButton.setEnabled(False)
            self.removeBilanButton.setEnabled(False)

    def doAddBilan(self):
        """
        appel d'un dialog spécifique
        """
        dialog = AddBilanToItemDlg(parent=self.main)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # on récupère les données du bilan :
            currentIndex = dialog.comboBox1.currentIndex()
            id_bilan = dialog.comboBox1.itemData(currentIndex)
            coeff = dialog.spinBox2.value()
            # si déjà liés, on quitte :
            commandLine_my = utils_db.qs_prof_AllFromItemBilanWhereItemBilan.format(
                self.id_item, id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                return
            # on crée les liens :
            affectOther(self.main, 'linkItemBilan', self.id_item, id_bilan, coeff)
            self.initAll(onlyBilans=True)
            # on sélectionne le nouveau bilan :
            selection = 0
            for line in self.bilansModel.datas:
                if line[0][2] == id_bilan:
                    selection = line[0][1] - 1
            self.bilansView.selectRow(selection)
            self.doModified()

    def doEditBilan(self):
        """
        appel d'un dialog spécifique
        """
        selection = self.bilansView.currentIndex().row()
        dialog = CreateMachinDlg(
            parent=self.main, what='EditBilan', id_bilan=self.id_bilan, id_item=self.id_item)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.initAll(onlyBilans=True)
            self.bilansView.selectRow(selection)
            self.doModified()

    def doRemoveBilan(self):
        """
        on retire le lien de l'item avec ce bilan.
        """
        # on traite les liens :
        affectOther(self.main, 'unlinkItemBilan', self.id_item, self.id_bilan)
        self.initAll(onlyBilans=True)
        try:
            self.bilansView.selectRow(0)
        except:
            pass
        self.doModified()

    def doChooseFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(
            parent=self.main, tableName=tableName, extendedSelection=True)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # création des items, bilans et liaisons :
            createCptFromList(self.main, tableName, dialog, what='CreateItems')
            # enfin on met à jour l'affichage :
            self.initAll()
            self.itemsView.selectRow(self.itemsModel.rowCount() - 1)
            self.doModified()

    def doModified(self):
        self.modified = True
        self.buttonsList['apply'].setEnabled(True)

    def doApply(self):
        """
        bouton "appliquer" les changements sans fermer la fenêtre
        """
        self.main.initialTables = saveTables(
            self.main, tables=TABLES_FOR_ITEMS_BILANS)
        self.main.changeDBMyState()
        self.buttonsList['apply'].setEnabled(False)
        self.buttonsList['cancel'].clearFocus()


class GestItemsBilansDlg(QtWidgets.QDialog):
    """
    gestion des items et bilans
    vue sous forme d'un graphWidget avec des noeuds et des liens
    """
    def __init__(self, parent=None):
        super(GestItemsBilansDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate('main', 'Manage items and balances')
        self.setWindowTitle(title)
        # des modifications ont été faites :
        self.modified = False

        # les icones :
        self.icons = {}
        for color in ('red', 'yellow', 'blue', 'blue2', 'magenta', 'green', 'cyan'):
            self.icons[color] = utils.doIcon('color-{0}'.format(color))

        # la liste des items :
        self.itemsListWidget = QtWidgets.QListWidget()
        self.itemsListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.itemsListWidget.itemSelectionChanged.connect(self.updateGraphWidget)
        self.itemsListWidget.itemDoubleClicked.connect(self.itemDoubleClicked)
        createItemButton = QtWidgets.QPushButton(
            utils.doIcon('item-add'), 
            QtWidgets.QApplication.translate('main', 'Create an item'))
        createItemButton.clicked.connect(self.doCreateItem)
        cptChooseIcon = utils.doIcon('cpt-choose')
        self.chooseBLTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLT'))
        self.chooseBLTButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLTStatusTip'))
        self.chooseBLTButton.clicked.connect(self.doChooseFromList)
        self.chooseCPTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPT'))
        self.chooseCPTButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPTStatusTip'))
        self.chooseCPTButton.clicked.connect(self.doChooseFromList)
        self.chooseCFDButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFD'))
        self.chooseCFDButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFDStatusTip'))
        self.chooseCFDButton.clicked.connect(self.doChooseFromList)
        itemsLayout = QtWidgets.QVBoxLayout()
        itemsLayout.addWidget(self.itemsListWidget)
        itemsLayout.addWidget(createItemButton)
        itemsLayout.addWidget(self.chooseBLTButton)
        itemsLayout.addWidget(self.chooseCPTButton)
        itemsLayout.addWidget(self.chooseCFDButton)
        itemsGroupBox = QtWidgets.QGroupBox(self.main.TR_ITEMS)
        itemsGroupBox.setLayout(itemsLayout)
        itemsGroupBox.setMaximumWidth(200)

        # le GraphWidget :
        linkAllButton = QtWidgets.QPushButton(
            utils.doIcon('link-insert'), 
            QtWidgets.QApplication.translate('main', 'LinkAll'))
        linkAllButton.setToolTip(QtWidgets.QApplication.translate('main', 'LinkAllStatusTip'))
        linkAllButton.clicked.connect(self.doLinkAll)
        unlinkAllButton = QtWidgets.QPushButton(
            utils.doIcon('link-cancel'), 
            QtWidgets.QApplication.translate('main', 'UnlinkAll'))
        unlinkAllButton.setToolTip(QtWidgets.QApplication.translate('main', 'UnlinkAllStatusTip'))
        unlinkAllButton.clicked.connect(self.doUnlinkAll)
        self.showIntermediateLinksCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'ShowIntermediateLinks'))
        self.showIntermediateLinksCheckBox.setToolTip(
            QtWidgets.QApplication.translate('main', 'ShowIntermediateLinksStatusTip'))
        self.showIntermediateLinksCheckBox.setChecked(True)
        self.showIntermediateLinksCheckBox.stateChanged.connect(self.showIntermediateLinksChanged)
        self.hideIntermediate = False

        deleteAllButton = QtWidgets.QPushButton(
            utils.doIcon('edit-clear'), 
            QtWidgets.QApplication.translate('main', 'DeleteAll'))
        deleteAllButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'DeleteAllStatusTip'))
        deleteAllButton.clicked.connect(self.doDeleteAll)
        export2CsvButton = QtWidgets.QPushButton(
            utils.doIcon('extension-csv'), 
            QtWidgets.QApplication.translate('main', 'Export2Csv'))
        export2CsvButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'Export2CsvStatusTip'))
        export2CsvButton.clicked.connect(self.doExport2Csv)
        graphWidget = utils_graphwidget.GraphWidget(self)
        self.scene = graphWidget.scene()
        linksButtonsLayout = QtWidgets.QHBoxLayout()
        linksButtonsLayout.addWidget(linkAllButton)
        linksButtonsLayout.addWidget(unlinkAllButton)
        linksButtonsLayout.addWidget(self.showIntermediateLinksCheckBox)
        linksButtons2Layout = QtWidgets.QHBoxLayout()
        linksButtons2Layout.addWidget(deleteAllButton)
        linksButtons2Layout.addWidget(export2CsvButton)
        linksLayout = QtWidgets.QVBoxLayout()
        linksLayout.addWidget(graphWidget)
        linksLayout.addLayout(linksButtonsLayout)
        linksLayout.addLayout(linksButtons2Layout)
        linksGroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Links'))
        linksGroupBox.setLayout(linksLayout)

        # la liste des bilans :
        self.bilansListWidget = QtWidgets.QListWidget()
        self.bilansListWidget.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.bilansListWidget.itemSelectionChanged.connect(self.updateGraphWidget)
        self.bilansListWidget.itemDoubleClicked.connect(self.bilanDoubleClicked)
        createBilanButton = QtWidgets.QPushButton(
            utils.doIcon('bilan-add'), 
            QtWidgets.QApplication.translate('main', 'Create a balance'))
        createBilanButton.clicked.connect(self.doCreateBilan)
        cptChooseIcon = utils.doIcon('cpt-choose')
        self.chooseBilansBLTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLT'))
        self.chooseBilansBLTButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLTStatusTip'))
        self.chooseBilansBLTButton.clicked.connect(self.doChooseBilansFromList)
        self.chooseBilansCPTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPT'))
        self.chooseBilansCPTButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPTStatusTip'))
        self.chooseBilansCPTButton.clicked.connect(self.doChooseBilansFromList)
        self.chooseBilansCFDButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFD'))
        self.chooseBilansCFDButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFDStatusTip'))
        self.chooseBilansCFDButton.clicked.connect(self.doChooseBilansFromList)
        bilansLayout = QtWidgets.QVBoxLayout()
        bilansLayout.addWidget(self.bilansListWidget)
        bilansLayout.addWidget(createBilanButton)
        bilansLayout.addWidget(self.chooseBilansBLTButton)
        bilansLayout.addWidget(self.chooseBilansCPTButton)
        bilansLayout.addWidget(self.chooseBilansCFDButton)
        bilansGroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Balances'))
        bilansGroupBox.setLayout(bilansLayout)
        bilansGroupBox.setMaximumWidth(200)

        # on groupe tout ça :
        listesLayout = QtWidgets.QHBoxLayout()
        listesLayout.addWidget(itemsGroupBox)
        listesLayout.addWidget(linksGroupBox)
        listesLayout.addWidget(bilansGroupBox)
        listesGroupBox = QtWidgets.QGroupBox()
        listesGroupBox.setLayout(listesLayout)
        listesGroupBox.setFlat(True)

        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('ok', 'apply', 'cancel', 'help'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]
        self.buttonsList['apply'].clicked.connect(self.doApply)
        self.buttonsList['apply'].setEnabled(False)

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listesGroupBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        self.createActions()
        self.itemsList = {}
        self.bilansList = {}
        self.initAll()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('gest-items-bilans')

    def createActions(self):
        """
        création des actions du menu popup
        """
        # débuter un lien :
        self.actionLinkOrUnlinkBegin = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Start to create or delete a link'), 
            self)
        self.actionLinkOrUnlinkBegin.setIcon(utils.doIcon('link-begin'))
        self.actionLinkOrUnlinkBegin.setData(0)
        self.actionLinkOrUnlinkBegin.triggered.connect(self.doPopUpMenu)
        # terminer un lien :
        self.actionLinkOrUnlinkEnd = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Ending create or delete a link'), 
            self)
        self.actionLinkOrUnlinkEnd.setIcon(utils.doIcon('link-end'))
        self.actionLinkOrUnlinkEnd.setData(1)
        self.actionLinkOrUnlinkEnd.triggered.connect(self.doPopUpMenu)
        # annuler un début de lien :
        self.actionLinkOrUnlinkCancel = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Abandon the creation or delete link'), 
            self)
        self.actionLinkOrUnlinkCancel.setIcon(utils.doIcon('link-cancel'))
        self.actionLinkOrUnlinkCancel.setData(2)
        self.actionLinkOrUnlinkCancel.triggered.connect(self.doPopUpMenu)
        # éditer un item :
        self.actionEditItem = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'EditThisItem'), 
            self)
        self.actionEditItem.setIcon(utils.doIcon('item-edit'))
        self.actionEditItem.setData(10)
        self.actionEditItem.triggered.connect(self.doPopUpMenu)
        # supprimer un item :
        self.actionDeleteItem = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'DeleteThisItem'), 
            self)
        self.actionDeleteItem.setIcon(utils.doIcon('item-delete'))
        self.actionDeleteItem.setData(11)
        self.actionDeleteItem.triggered.connect(self.doPopUpMenu)
        # éditer un bilan :
        self.actionEditBilan = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'EditThisBilan'), 
            self)
        self.actionEditBilan.setIcon(utils.doIcon('bilan-edit'))
        self.actionEditBilan.setData(20)
        self.actionEditBilan.triggered.connect(self.doPopUpMenu)
        # supprimer un bilan :
        self.actionDeleteBilan = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'DeleteThisBilan'), 
            self)
        self.actionDeleteBilan.setIcon(utils.doIcon('bilan-delete'))
        self.actionDeleteBilan.setData(21)
        self.actionDeleteBilan.triggered.connect(self.doPopUpMenu)
        # afficher les bilans liés au bilan sélectionné :
        self.actionShowLinkedBilansToThis = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'ShowLinkedBilansToThis'), 
            self)
        self.actionShowLinkedBilansToThis.setIcon(utils.doIcon('bilans-link'))
        self.actionShowLinkedBilansToThis.setData(22)
        self.actionShowLinkedBilansToThis.triggered.connect(self.doPopUpMenu)

        # supprimer un lien :
        self.actionDeleteLink = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'DeleteThisLink'), 
            self)
        self.actionDeleteLink.setIcon(utils.doIcon('link-delete'))
        self.actionDeleteLink.setData(100)
        self.actionDeleteLink.triggered.connect(self.doPopUpMenu)
        # éditer le coefficient d'un lien :
        self.actionEditLinkCoeff = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'EditLinkCoeff'), 
            self)
        self.actionEditLinkCoeff.setIcon(utils.doIcon('link-edit'))
        self.actionEditLinkCoeff.setData(101)
        self.actionEditLinkCoeff.triggered.connect(self.doPopUpMenu)

        # tout relier :
        self.actionLinkAll = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'LinkAll'), 
            self)
        self.actionLinkAll.setIcon(utils.doIcon('link-insert'))
        self.actionLinkAll.setData(150)
        self.actionLinkAll.triggered.connect(self.doPopUpMenu)
        # tout délier :
        self.actionUnlinkAll = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'UnlinkAll'), 
            self)
        self.actionUnlinkAll.setIcon(utils.doIcon('link-cancel'))
        self.actionUnlinkAll.setData(151)
        self.actionUnlinkAll.triggered.connect(self.doPopUpMenu)
        # tout supprimer :
        self.actionDeleteAll = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'DeleteAll'), 
            self)
        self.actionDeleteAll.setIcon(utils.doIcon('edit-clear'))
        self.actionDeleteAll.setData(153)
        self.actionDeleteAll.triggered.connect(self.doPopUpMenu)
        # sélectionner tous les bilans liés aux items affichés :
        self.actionSelectBilans = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'SelectBilans'), 
            self)
        self.actionSelectBilans.setIcon(utils.doIcon('bilans'))
        self.actionSelectBilans.setData(154)
        self.actionSelectBilans.triggered.connect(self.doPopUpMenu)
        # sélectionner tous les items liés aux bilans affichés :
        self.actionSelectItems = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'SelectItems'), 
            self)
        self.actionSelectItems.setIcon(utils.doIcon('items'))
        self.actionSelectItems.setData(155)
        self.actionSelectItems.triggered.connect(self.doPopUpMenu)
        # export csv :
        self.actionExport2Csv = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Export2Csv'), 
            self)
        self.actionExport2Csv.setIcon(utils.doIcon('extension-csv'))
        self.actionExport2Csv.setData(156)
        self.actionExport2Csv.triggered.connect(self.doPopUpMenu)

        # afficher tous les bilans liés entre eux :
        self.actionShowAllLinkedBilans = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'ShowAllLinkedBilans'), 
            self)
        self.actionShowAllLinkedBilans.setIcon(utils.doIcon('bilans-link'))
        self.actionShowAllLinkedBilans.setData(160)
        self.actionShowAllLinkedBilans.triggered.connect(self.doPopUpMenu)
        # afficher/masquer les liens intermédiaires :
        self.actionShowIntermediateLinks = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'ShowIntermediateLinksStatusTip'), 
            self, 
            checkable=True)
        self.actionShowIntermediateLinks.setIcon(utils.doIcon('link'))
        self.actionShowIntermediateLinks.setChecked(True)
        self.actionShowIntermediateLinks.setData(161)
        self.actionShowIntermediateLinks.triggered.connect(self.doPopUpMenu)

    def initAll(self):
        """
        blablabla
        """
        utils_functions.doWaitCursor()
        self.scene.mustWait = True
        try:
            # pour récupère les positions :
            self.itemsListOld = {}
            for id_item in self.itemsList:
                pos = self.itemsList[id_item][1].pos()
                self.itemsListOld[id_item] = (pos.x(), pos.y())
            self.bilansListOld = {}
            for id_bilan in self.bilansList:
                pos = self.bilansList[id_bilan][1].pos()
                self.bilansListOld[id_bilan] = (pos.x(), pos.y())
            # on efface les 2 ListWidgets et la scène :
            self.itemsListWidget.clear()
            self.bilansListWidget.clear()
            self.scene.clear()
            # il n'y a plus de sélection :
            self.selectedNode = None
            self.beginNode = None
            # on vide les listes et dicos :
            self.itemsNodesList = []
            self.bilansNodesList = []
            self.itemsList = {}
            self.bilansList = {}
            # on reconstruit l'affichage (ListWidgets etc)
            self.initLists()
        finally :
            self.scene.mustWait = False
            self.updateGraphWidget()
            utils_functions.restoreCursor()

    def initLists(self):
        """
        Remplissages des listes des items et des bilans
        """
        import random
        bold = QtGui.QFont()
        bold.setBold(True)
        italic = QtGui.QFont()
        italic.setItalic(True)
        query_my = utils_db.query(self.main.db_my)
        # récupération de la liste des items liés (à un bilan au moins) :
        linkedItems = []
        commandLine_my = 'SELECT DISTINCT id_item FROM item_bilan'
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            linkedItems.append(id_item)
        # on récupère la liste des items :
        items, nbItems = [], 0
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            items.append((id_item, itemName, itemLabel))
        nbItems, i = len(items), 0
        for (id_item, itemName, itemLabel) in items:
            # on place l'item dans itemsListWidget :
            itemText = utils_functions.u(
                '{0} \t[{1}]').format(itemName, itemLabel)
            itemWidget = QtWidgets.QListWidgetItem(itemName)
            itemWidget.setToolTip(itemLabel)
            itemWidget.setData(QtCore.Qt.UserRole, id_item)
            color = 'yellow'
            if not(id_item in linkedItems):
                color = 'red'
            itemWidget.setIcon(
                self.icons[color])
            self.itemsListWidget.addItem(itemWidget)
            # et on crée le node pour la scène :
            i += 1
            if id_item in self.itemsListOld:
                pos = self.itemsListOld[id_item]
            else:
                pos = (random.randint(-200, -50), -200 + 400 * i / nbItems)
            itemData = (id_item, itemName, itemLabel, None)
            itemNode = utils_graphwidget.Node(self.scene, 
                pos, color=color, data=itemData, genre='item')
            itemNode.setToolTip(utils_functions.u(
                '{0} :\n{1}').format(itemName, itemLabel))
            # on met à jour la liste et le dico :
            self.itemsNodesList.append(itemNode)
            self.itemsList[id_item] = [itemWidget, itemNode, False]
        # récupération de la liste des bilans liés (à un item au moins) :
        linkedBilans = []
        commandLine_my = 'SELECT DISTINCT id_bilan FROM item_bilan'
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            linkedBilans.append(id_bilan)
        # récupération de la liste des bilans liés à un autre bilan 
        # (les bilan2 donc fin de lien) :
        bilans2 = []
        commandLine_my = 'SELECT DISTINCT id_bilan2 FROM bilan_bilan'
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_bilan2 = int(query_my.value(0))
            bilans2.append(id_bilan2)
        # récupération de la liste des bilans racines :
        racines = createPathsList(self.main)[1]
        # on récupère la liste des bilans triés par type :
        bilansTri = {
            'ORDER': (
                'perso', 
                'persoNonCalc', 
                'bulletin', 
                'referentiel', 
                'confidentiel'), 
            'perso': [], 
            'persoNonCalc': [], 
            'confidentiel': [], 
            'bulletin': [], 
            'referentiel': []}
        bilans, nbBilans = [], 0
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            bilanName = query_my.value(1)
            bilanLabel = query_my.value(2)
            id_competence = int(query_my.value(3))
            if id_competence == -1:
                bilansTri['perso'].append(
                    (id_bilan, bilanName, bilanLabel, id_competence))
            elif id_competence == -2:
                bilansTri['persoNonCalc'].append(
                    (id_bilan, bilanName, bilanLabel, id_competence))
            elif id_competence > utils.decalageCFD:
                bilansTri['confidentiel'].append(
                    (id_bilan, bilanName, bilanLabel, id_competence))
            elif id_competence > utils.decalageBLT:
                bilansTri['bulletin'].append(
                    (id_bilan, bilanName, bilanLabel, id_competence))
            else:
                bilansTri['referentiel'].append(
                    (id_bilan, bilanName, bilanLabel, id_competence))
        for what in bilansTri['ORDER']:
            bilans.extend(bilansTri[what])
        nbBilans, i = len(bilans), 0
        for (id_bilan, bilanName, bilanLabel, id_competence) in bilans:
            # couleur et classType du bilan :
            if id_competence == -1:
                # bilan perso :
                color = 'blue'
                classType = ''
            elif id_competence == -2:
                # bilan perso non calculé :
                color = 'blue2'
                classType = ''
            elif id_competence > utils.decalageCFD:
                # confidentiel :
                color = 'cyan'
                classType = ''
                classTypeName = utils_functions.bilanCode2ClassTypeName(
                    self.main, bilanName)
                if classTypeName != '':
                    text = QtWidgets.QApplication.translate(
                        'main', 'ClassesTypes')
                    classType = utils_functions.u(
                        '{0} : {1}').format(text, classTypeName)
            elif id_competence > utils.decalageBLT:
                # bulletin :
                color = 'green'
                classType = ''
                classTypeName = utils_functions.bilanCode2ClassTypeName(
                    self.main, bilanName)
                if classTypeName != '':
                    text = QtWidgets.QApplication.translate(
                        'main', 'ClassesTypes')
                    classType = utils_functions.u(
                        '{0} : {1}').format(text, classTypeName)
            else:
                # referentiel :
                color = 'magenta'
                classType = ''
                classTypeName = utils_functions.bilanCode2ClassTypeName(
                    self.main, bilanName)
                if classTypeName != '':
                    text = QtWidgets.QApplication.translate(
                        'main', 'ClassesTypes')
                    classType = utils_functions.u(
                        '{0} : {1}').format(text, classTypeName)
            if not(id_bilan in linkedBilans):
                color = 'red'
            # on place le bilan dans bilansListWidget :
            bilanWidget = QtWidgets.QListWidgetItem(bilanName)
            bilanWidget.setToolTip(bilanLabel)
            bilanWidget.setData(QtCore.Qt.UserRole, id_bilan)
            if id_bilan in racines:
                bilanWidget.setFont(bold)
            elif id_bilan in bilans2:
                bilanWidget.setFont(italic)
            bilanWidget.setIcon(
                self.icons[color])
            self.bilansListWidget.addItem(bilanWidget)
            # et on crée le node pour la scène :
            i += 1
            if id_bilan in self.bilansListOld:
                pos = self.bilansListOld[id_bilan]
            else:
                pos = (random.randint(50, 200), -200 + 400 * i / nbBilans)
            bilanData = (id_bilan, bilanName, bilanLabel, id_competence)
            bilanNode = utils_graphwidget.Node(self.scene, 
                pos, color=color, data=bilanData, genre='bilan')
            if classType == '':
                bilanNode.setToolTip(utils_functions.u(
                    '{0} :\n{1}').format(bilanName, bilanLabel))
            else:
                bilanNode.setToolTip(utils_functions.u(
                    '{0} :\n{1}\n{2}').format(
                        bilanName, bilanLabel, classType))
            # on met à jour la liste et le dico :
            self.bilansNodesList.append(bilanNode)
            self.bilansList[id_bilan] = [bilanWidget, bilanNode, False]

    def updateGraphWidget(self):
        """
        blablabla
        """
        if self.scene.mustWait:
            return
        utils_functions.doWaitCursor()
        self.scene.mustWait = True
        try:
            # on efface tous les edges :
            edges = [
                item for item in self.scene.items()
                if isinstance(item, utils_graphwidget.Edge)]
            for item in edges:
                self.scene.removeItem(item)
            # on affiche les items sélectionnés :
            for id_item in self.itemsList:
                [itemWidget, itemNode, visible] = self.itemsList[id_item]
                isInGraphWidget = itemNode in self.scene.items()
                dataList = [
                    item2.data(QtCore.Qt.UserRole)
                    for item2 in self.itemsListWidget.selectedItems()]
                dataItem = itemWidget.data(QtCore.Qt.UserRole)
                if dataItem in dataList:
                    if not(isInGraphWidget):
                        self.scene.addItem(itemNode)
                    self.itemsList[id_item][2] = True
                else:
                    if isInGraphWidget:
                        self.scene.removeItem(itemNode)
                    self.itemsList[id_item][2] = False
            # on affiche les bilans sélectionnés :
            for id_bilan in self.bilansList:
                [bilanWidget, bilanNode, visible] = self.bilansList[id_bilan]
                isInGraphWidget = bilanNode in self.scene.items()
                dataList = [
                    bilan2.data(QtCore.Qt.UserRole)
                    for bilan2 in self.bilansListWidget.selectedItems()]
                dataBilan = bilanWidget.data(QtCore.Qt.UserRole)
                if dataBilan in dataList:
                    if not(isInGraphWidget):
                        self.scene.addItem(bilanNode)
                    self.bilansList[id_bilan][2] = True
                else:
                    if isInGraphWidget:
                        self.scene.removeItem(bilanNode)
                    self.bilansList[id_bilan][2] = False
            # on recrée les edges :
            query_my = utils_db.query(self.main.db_my)
            # les edges entre bilans liés :
            for id_bilan1 in self.bilansList:
                if self.bilansList[id_bilan1][2]:
                    commandLine_my2 = utils_db.qs_prof_fromBilanBilan2.format(id_bilan1)
                    query_my = utils_db.queryExecute(commandLine_my2, query=query_my)
                    while query_my.next():
                        id_bilan2 = int(query_my.value(0))
                        if self.bilansList[id_bilan2][2]:
                            newEdge = utils_graphwidget.Edge(self.scene, 
                                                            self.bilansList[id_bilan1][1], 
                                                            self.bilansList[id_bilan2][1], 
                                                            defaultColor=QtCore.Qt.blue, 
                                                            selectedColor=QtCore.Qt.darkBlue, 
                                                            double=False)
                            self.scene.addItem(newEdge)
            # les edges entre items et bilans
            # si actionShowIntermediateLinks est décochée, on n'affichera que les premiers liens
            bilansToIgnore = []
            if self.hideIntermediate:
                # on met dans la liste bilansToIgnore les bilans
                # qui sont à l'arrivée d'un lien
                bilansEdges = [
                    item for item in self.scene.items()
                    if isinstance(item, utils_graphwidget.Edge)]
                for bilansEdge in bilansEdges:
                    id_bilan = bilansEdge.dest.data[0]
                    if not(id_bilan in bilansToIgnore):
                        bilansToIgnore.append(id_bilan)
            for id_item in self.itemsList:
                if self.itemsList[id_item][2]:
                    commandLine_my2 = utils_db.q_selectAllFromWhere.format(
                        'item_bilan', 'id_item', id_item)
                    query_my = utils_db.queryExecute(commandLine_my2, query=query_my)
                    while query_my.next():
                        id_bilan = int(query_my.value(1))
                        coeff = int(query_my.value(2))
                        if self.bilansList[id_bilan][2] and not(id_bilan in bilansToIgnore):
                            # si le bilan est affiché et n'est pas dans la liste bilansToIgnore,
                            # on peut afficher le lien :
                            newEdge = utils_graphwidget.Edge(self.scene, 
                                                            self.itemsList[id_item][1], 
                                                            self.bilansList[id_bilan][1], 
                                                            width=coeff)
                            self.scene.addItem(newEdge)
        finally :
            utils_functions.restoreCursor()
            self.scene.mustWait = False

    def bilanDoubleClicked(self, bilan):
        """
        EditBilan
        """
        id_bilan = bilan.data(QtCore.Qt.UserRole)
        if self.bilansList[id_bilan][1].data[3] >= 0:
            return
        dialog = CreateMachinDlg(
            parent=self.main, what='EditBilan', id_bilan=id_bilan, id_item=-1)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            bilanName = dialog.nameEdit.text()
            bilanLabel = dialog.labelEdit.text()
            uncalculated = dialog.uncalculatedCheckBox.isChecked()
            if id_bilan in self.bilansList:
                [bilanWidget, bilanNode, visible] = self.bilansList[id_bilan]
                bilanWidget.setText(bilanName)
                bilanWidget.setToolTip(bilanLabel)
                color = 'blue'
                if uncalculated:
                    color = 'blue2'
                bilanWidget.setIcon(
                    self.icons[color])
                bilanNode.setText(bilanName)
                bilanNode.setToolTip(
                    utils_functions.u('{0} :\n{1}').format(bilanName, bilanLabel))
            self.updateGraphWidget()
            self.doModified()

    def itemDoubleClicked(self, item):
        """
        EditItem
        """
        id_item = item.data(QtCore.Qt.UserRole)
        dialog = CreateMachinDlg(parent=self.main, what='EditItem', id_item=id_item)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            itemName = dialog.nameEdit.text()
            itemLabel = dialog.labelEdit.text()
            if id_item in self.itemsList:
                [itemWidget, itemNode, visible] = self.itemsList[id_item]
                itemWidget.setText(itemName)
                itemWidget.setToolTip(itemLabel)
                itemNode.setText(itemName)
                itemNode.setToolTip(utils_functions.u('{0} :\n{1}').format(itemName, itemLabel))
            self.updateGraphWidget()
            self.doModified()

    def nodeMouseDoubleClickEvent(self, event, theNode=None):
        utils_functions.doWaitCursor()
        self.scene.mustWait = True
        try:
            if isinstance(theNode, utils_graphwidget.Node):
                if theNode.genre == 'item':
                    id_item = theNode.data[0]
                    commandLine_my = utils_db.q_selectAllFromWhere.format(
                        'item_bilan', 'id_item', id_item)
                    query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
                    while query_my.next():
                        id_bilan = int(query_my.value(1))
                        coeff = int(query_my.value(2))
                        if id_bilan in self.bilansList:
                            self.bilansList[id_bilan][0].setSelected(True)
                else:
                    id_bilan = theNode.data[0]
                    # on affiche les items liés :
                    commandLine_my = utils_db.q_selectAllFromWhere.format(
                        'item_bilan', 'id_bilan', id_bilan)
                    query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
                    while query_my.next():
                        id_item = int(query_my.value(0))
                        coeff = int(query_my.value(2))
                        if id_item in self.itemsList:
                            self.itemsList[id_item][0].setSelected(True)
                    # on affiche aussi les bilans liés :
                    commandLine_my = utils_db.qs_prof_fromBilanBilan2.format(id_bilan)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    while query_my.next():
                        id_bilan2 = int(query_my.value(0))
                        self.bilansList[id_bilan2][0].setSelected(True)
                    # et aussi les bilans auxquel il est lié :
                    commandLine_my = utils_db.qs_prof_fromBilanBilan1.format(id_bilan)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    while query_my.next():
                        id_bilan1 = int(query_my.value(0))
                        self.bilansList[id_bilan1][0].setSelected(True)
        finally :
            utils_functions.restoreCursor()
            self.scene.mustWait = False
            self.updateGraphWidget()

    def itemContextMenuEvent(self, event, theGraphicsItem=None):
        menu = QtWidgets.QMenu(self)
        context = 0
        if isinstance(theGraphicsItem, utils_graphwidget.Node):
            context = 1
        elif isinstance(theGraphicsItem, utils_graphwidget.Edge):
            context = 2
        if context == 1:
            if self.beginNode == None:
                menu.addAction(self.actionLinkOrUnlinkBegin)
            elif self.beginNode.genre != theGraphicsItem.genre:
                menu.addAction(self.actionLinkOrUnlinkEnd)
                menu.addAction(self.actionLinkOrUnlinkCancel)
            elif theGraphicsItem.genre == 'bilan':
                menu.addAction(self.actionLinkOrUnlinkEnd)
                menu.addAction(self.actionLinkOrUnlinkCancel)
            else:
                menu.addAction(self.actionLinkOrUnlinkCancel)
            menu.addSeparator()
            if theGraphicsItem.genre == 'item':
                menu.addAction(self.actionEditItem)
                menu.addAction(self.actionDeleteItem)
            else:
                id_competence = theGraphicsItem.data[3]
                if id_competence < 0:
                    menu.addAction(self.actionEditBilan)
                menu.addAction(self.actionDeleteBilan)
                menu.addAction(self.actionShowLinkedBilansToThis)
            self.selectedNode = theGraphicsItem
            menu.addSeparator()
        elif context == 2:
            if self.beginNode != None:
                menu.addAction(self.actionLinkOrUnlinkCancel)
                menu.addSeparator()
            menu.addAction(self.actionDeleteLink)
            # si c'est un lien item->bilan :
            itemBilanLink = (theGraphicsItem.source.genre == 'item')
            if itemBilanLink:
                menu.addAction(self.actionEditLinkCoeff)
            self.selectedNode = theGraphicsItem
            menu.addSeparator()
        else:
            if self.beginNode != None:
                menu.addAction(self.actionLinkOrUnlinkCancel)
                menu.addSeparator()
        menu.addAction(self.actionLinkAll)
        menu.addAction(self.actionUnlinkAll)
        menu.addAction(self.actionDeleteAll)
        menu.addAction(self.actionSelectBilans)
        menu.addAction(self.actionSelectItems)
        menu.addAction(self.actionExport2Csv)
        menu.addSeparator()
        menu.addAction(self.actionShowAllLinkedBilans)
        menu.addAction(self.actionShowIntermediateLinks)
        menu.exec_(event.screenPos())
        if context > 0:
            theGraphicsItem.select(False)
            self.selectedNode = None

    def doPopUpMenu(self):
        theGraphicsItem = self.scene.lastGraphicsItem
        action = self.sender()
        i = action.data()
        if i == 0:
            #LinkOrUnlinkBegin
            self.beginNode = theGraphicsItem
        elif i == 1:
            #LinkOrUnlinkEnd
            self.doLinkOrUnlinkEnd(theGraphicsItem)
        elif i == 2:
            #LinkOrUnlinkCancel
            self.beginNode = None
        elif i == 10:
            #EditItem
            id_item = theGraphicsItem.data[0]
            dialog = CreateMachinDlg(parent=self.main, what='EditItem', id_item=id_item)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                itemName = dialog.nameEdit.text()
                itemLabel = dialog.labelEdit.text()
                if id_item in self.itemsList:
                    [itemWidget, itemNode, visible] = self.itemsList[id_item]
                    itemWidget.setText(itemName)
                    itemWidget.setToolTip(itemLabel)
                    itemNode.setText(itemName)
                    itemNode.setToolTip(utils_functions.u('{0} :\n{1}').format(itemName, itemLabel))
                self.updateGraphWidget()
                self.doModified()
        elif i == 11:
            #DeleteItem
            id_item = theGraphicsItem.data[0]
            message = utils_functions.u('')
            commandLine_myBase = (
                'SELECT tableau_item.id_item, {1}.{2}, {1}.Name '
                'FROM tableau_item, {1} '
                'WHERE tableau_item.id_item={0} '
                'AND {1}.{2}=tableau_item.id_tableau')
            commandLine_my = commandLine_myBase.format(
                id_item, 'tableaux', 'id_tableau')
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                tableauName = query_my.value(2)
                message = message + "<p>" + tableauName + "</p>"
            commandLine_my = commandLine_myBase.format(
                id_item, 'templates', 'id_template')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            list_templates = []
            while query_my.next():
                id_template = int(query_my.value(1))
                list_templates.append(id_template)
                tableauName = query_my.value(2)
                message = message + "<p>" + tableauName + "</p>"
            message = QtWidgets.QApplication.translate(
                'main', 
                '<p><b>This Item is used in Tables or Templates:</b></p>'
                ' {0} <p><b>Continue ?</b></p>').format(message)
            if utils_functions.messageBox(
                self.main, 
                level='question', 
                message=message, 
                buttons=['Yes', 'No']) == QtWidgets.QMessageBox.No:
                return
            ListIdTableau = []
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'tableau_item', 'id_item', id_item)
            query_my = utils_db.queryExecute(
                commandLine_my, query=query_my)
            while query_my.next():
                id_tableau = int(query_my.value(0))
                ListIdTableau.append(id_tableau)
            for id_tableau in ListIdTableau:
                commandLine_my = utils_db.qdf_prof_tableauItem2.format(
                    id_tableau, id_item)
                query_my = utils_db.queryExecute(
                    commandLine_my, query=query_my)
            commandLine_my1 = utils_db.qdf_where.format('items', 'id_item', id_item)
            commandLine_my2 = utils_db.qdf_where.format('item_bilan', 'id_item', id_item)
            commandLine_my3 = utils_db.qdf_where.format('evaluations', 'id_item', id_item)
            commandLine_my4 = (
                'UPDATE counts SET id_item=-1 '
                'WHERE id_item IN ({0})').format(id_item)
            commandLine_my5 = (
                'DELETE FROM comments '
                'WHERE isItem=1 AND id IN ({0})').format(id_item)
            commandLines = [
                commandLine_my1, 
                commandLine_my2, 
                commandLine_my3, 
                commandLine_my4, 
                commandLine_my5]
            query_my = utils_db.queryExecute(commandLines, query=query_my)
            # on vérifie s'il faut rafraichir l'affichage
            commandLine_my = (
                'SELECT * FROM tableau_item '
                'WHERE id_tableau={0}').format(self.main.id_tableau)
            query_my = utils_db.queryExecute(
                commandLine_my, query=query_my)
            id_modele = 0
            if query_my.last():
                id_modele = int(query_my.value(1))
            if id_item in self.itemsList:
                [itemWidget, itemNode, visible] = self.itemsList[id_item]
                self.itemsListWidget.takeItem(
                    self.itemsListWidget.row(itemWidget))
                self.itemsNodesList.remove(itemNode)
                self.scene.removeItem(itemNode)
                del(self.itemsList[id_item])
            self.updateGraphWidget()
            self.doModified()
        elif i == 20:
            #EditBilan
            id_bilan = theGraphicsItem.data[0]
            dialog = CreateMachinDlg(
                parent=self.main, what='EditBilan', id_bilan=id_bilan, id_item=-1)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                bilanName = dialog.nameEdit.text()
                bilanLabel = dialog.labelEdit.text()
                uncalculated = dialog.uncalculatedCheckBox.isChecked()
                if id_bilan in self.bilansList:
                    [bilanWidget, bilanNode, visible] = self.bilansList[id_bilan]
                    bilanWidget.setText(bilanName)
                    bilanWidget.setToolTip(bilanLabel)
                    bilanNode.setText(bilanName)
                    bilanNode.setToolTip(utils_functions.u('{0} :\n{1}').format(bilanName, bilanLabel))
                self.updateGraphWidget()
                self.doModified()
        elif i == 21:
            #DeleteBilan
            id_bilan = theGraphicsItem.data[0]
            # on efface ce bilan des tables :
            commandLine_my1 = utils_db.qdf_where.format('bilans', 'id_bilan', id_bilan)
            commandLine_my2 = utils_db.qdf_where.format('item_bilan', 'id_bilan', id_bilan)
            commandLine_my3 = utils_db.qdf_where.format('profil_bilan_BLT', 'id_bilan', id_bilan)
            commandLine_my4 = (
                'DELETE FROM bilan_bilan '
                'WHERE id_bilan1={0} OR id_bilan2={0}').format(id_bilan)
            commandLine_my5 = utils_db.qdf_where.format('evaluations', 'id_bilan', id_bilan)
            commandLine_my6 = utils_db.qdf_where.format('groupe_bilan', 'id_bilan', id_bilan)
            commandLine_my7 = (
                'DELETE FROM comments '
                'WHERE isItem=0 AND id IN ({0})').format(id_bilan)
            commandLines = [
                commandLine_my1, 
                commandLine_my2, 
                commandLine_my3, 
                commandLine_my4, 
                commandLine_my5, 
                commandLine_my6, 
                commandLine_my7]
            query_my = utils_db.queryExecute(commandLines, db=self.main.db_my)
            if id_bilan in self.bilansList:
                [bilanWidget, bilanNode, visible] = self.bilansList[id_bilan]
                self.bilansListWidget.takeItem(
                    self.bilansListWidget.row(bilanWidget))
                self.bilansNodesList.remove(bilanNode)
                self.scene.removeItem(bilanNode)
                del(self.bilansList[id_bilan])
            self.updateGraphWidget()
            self.doModified()
        elif i == 22:
            #ShowLinkedBilansToThis
            id_bilan = theGraphicsItem.data[0]
            commandLine_my = ('SELECT * FROM bilan_bilan '
                            'WHERE id_bilan1={0} OR id_bilan2={0}').format(id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                id_bilan1 = int(query_my.value(0))
                id_bilan2 = int(query_my.value(1))
                if id_bilan1 != id_bilan:
                    self.bilansList[id_bilan1][0].setSelected(True)
                else:
                    self.bilansList[id_bilan2][0].setSelected(True)
        elif i == 100:
            #DeleteLink
            source = theGraphicsItem.source
            dest = theGraphicsItem.dest
            # on vérifie qui est l'item et qui le bilan :
            itemBilanLink = (theGraphicsItem.source.genre == 'item')
            if itemBilanLink:
                # c'est un lien item-bilan
                id_item = theGraphicsItem.source.data[0]
                id_bilan = theGraphicsItem.dest.data[0]
                # on traite les liens :
                affectOther(self.main, 'unlinkItemBilan', id_item, id_bilan)
            else:
                # c'est un lien bilan1->bilan2
                id_bilan1 = theGraphicsItem.source.data[0]
                id_bilan2 = theGraphicsItem.dest.data[0]
                # on supprime le lien :
                query_my = utils_db.query(self.main.db_my)
                commandLine_my = utils_db.qdf_prof_bilanBilan.format(id_bilan1, id_bilan2)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                # et on traite les liens items-bilans :
                affectOther(self.main, 'unlinkBilans', id_bilan1, id_bilan2)
            self.beginNode = None
            self.updateGraphWidget()
            self.doModified()
        elif i == 101:
            #EditLinkCoeff
            # on demande la nouvelle valeur :
            message = utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'coefficient:'))
            coeff, ok = QtWidgets.QInputDialog.getInt(
                self.main, 
                utils.PROGNAME, 
                message, 
                theGraphicsItem.width, 1, 10, 1)
            if not(ok):
                return
            # on modifie le lien :
            id_item = theGraphicsItem.source.data[0]
            id_bilan = theGraphicsItem.dest.data[0]
            query_my = utils_db.query(self.main.db_my)
            commandLine_my = ('UPDATE item_bilan SET coeff={0} '
                'WHERE id_item={1} AND id_bilan={2}').format(coeff, id_item, id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            self.beginNode = None
            self.updateGraphWidget()
            self.doModified()
        elif i == 150:
            #LinkAll
            self.doLinkAll()
        elif i == 151:
            #UnlinkAll
            self.doUnlinkAll()
        elif i == 153:
            #DeleteAll
            self.doDeleteAll()
        elif i == 154:
            #SelectBilans
            self.doSelectBilans()
        elif i == 155:
            #SelectItems
            self.doSelectItems()
        elif i == 156:
            #Export2Csv
            self.doExport2Csv()
        elif i == 160:
            #ShowAllLinkedBilans
            self.doShowAllLinkedBilans()
        elif i == 161:
            # afficher/masquer les liens intermédiaires
            self.showIntermediateLinksChanged()

    def doLinkOrUnlinkEnd(self, theGraphicsItem):
        itemBilanLink = True
        query_my = utils_db.query(self.main.db_my)
        # on vérifie qui est l'item et qui le bilan :
        if theGraphicsItem.genre == 'item':
            id_item = theGraphicsItem.data[0]
            id_bilan = self.beginNode.data[0]
        elif self.beginNode.genre == 'item':
            id_bilan = theGraphicsItem.data[0]
            id_item = self.beginNode.data[0]
        else:
            itemBilanLink = False
            id_bilan1 = self.beginNode.data[0]
            id_bilan2 = theGraphicsItem.data[0]
        if itemBilanLink:
            # c'est un lien item-bilan
            # le lien existe-t'il déjà ?
            commandLine = utils_db.qs_prof_AllFromItemBilanWhereItemBilan.format(id_item, 
                id_bilan)
            query_my = utils_db.queryExecute(commandLine, query=query_my)
            mustCreate = True
            while query_my.next():
                mustCreate = False
            if mustCreate:
                message = utils_functions.u('<p><b>{0}</b></p>').format(
                    QtWidgets.QApplication.translate('main', 'coefficient:'))
                coeff, ok = QtWidgets.QInputDialog.getInt(
                    self.main, 
                    utils.PROGNAME, 
                    message, 
                    1, 1, 10, 1)
                if not(ok):
                    self.beginNode = None
                    return
                # on crée les liens :
                affectOther(self.main, 'linkItemBilan', id_item, id_bilan, coeff)
            else:
                # on traite les liens :
                affectOther(self.main, 'unlinkItemBilan', id_item, id_bilan)
        else:
            # c'est un lien bilan1->bilan2
            # le lien existe-t'il déjà ?
            commandLine = utils_db.qs_prof_fromBilanBilan.format(id_bilan1, id_bilan2)
            query_my = utils_db.queryExecute(commandLine, query=query_my)
            mustCreate = True
            while query_my.next():
                mustCreate = False
            if mustCreate:
                # liste des id_bilan2 à ne pas accepter :
                idToIgnore = [id_bilan1]
                paths = createPathsList(self.main)[0]
                for path in paths :
                    if id_bilan1 in path:
                        for id_bilan in path:
                            if not(id_bilan in idToIgnore):
                                idToIgnore.append(id_bilan)
                # si le lien n'est pas acceptable, on le dit et on quitte :
                if id_bilan2 in idToIgnore:
                    message = QtWidgets.QApplication.translate(
                        'main', 
                        '<p><b>These balances can not be connected!</b></p>'
                        '<p>There are already connected by a path.</p>')
                    utils_functions.messageBox(self.main, level='warning', message=message)
                else:
                    # on crée le lien :
                    commandLine_my = utils_db.insertInto('bilan_bilan')
                    query_my = utils_db.queryExecute(
                        {commandLine_my: (id_bilan1, id_bilan2)}, 
                        query=query_my)
                    affectOther(self.main, 'linkBilans', id_bilan1, id_bilan2)
            else:
                # on supprime le lien :
                commandLine_my = utils_db.qdf_prof_bilanBilan.format(id_bilan1, id_bilan2)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                # et on traite les liens items-bilans :
                affectOther(self.main, 'unlinkBilans', id_bilan1, id_bilan2)
        self.beginNode = None
        if self.scene.tempLine != None:
            self.scene.removeItem(self.scene.tempLine)
            self.scene.tempLine = None
        self.updateGraphWidget()
        self.doModified()

    def doLinkAll(self):
        # on récupère les items affichés :
        selectedItems = [
            id_item for id_item in self.itemsList
            if self.itemsList[id_item][2]]
        # on récupère les bilans affichés :
        selectedBilans = [
            id_bilan for id_bilan in self.bilansList
            if self.bilansList[id_bilan][2]]
        # on fabrique les liens :
        coeff = 1
        for id_item in selectedItems:
            for id_bilan in selectedBilans:
                # on crée les liens :
                affectOther(self.main, 'linkItemBilan', id_item, id_bilan, coeff)
        self.beginNode = None
        self.updateGraphWidget()
        self.doModified()

    def doUnlinkAll(self):
        # on récupère les items affichés :
        selectedItems = [
            id_item for id_item in self.itemsList
            if self.itemsList[id_item][2]]
        # on récupère les bilans affichés :
        selectedBilans = [
            id_bilan for id_bilan in self.bilansList
            if self.bilansList[id_bilan][2]]
        # on supprime tous les liens :
        for id_item in selectedItems:
            for id_bilan in selectedBilans:
                # on traite les liens (sans confirmations) :
                affectOther(self.main, 'unlinkItemBilan', id_item, id_bilan, withMessage=False)
        self.beginNode = None
        self.updateGraphWidget()
        self.doModified()

    def doShowAllLinkedBilans(self):
        self.initAll()
        utils_functions.doWaitCursor()
        self.scene.mustWait = True
        try:
            commandLine_my = utils_db.q_selectAllFrom.format('bilan_bilan')
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                id_bilan1 = int(query_my.value(0))
                id_bilan2 = int(query_my.value(1))
                self.bilansList[id_bilan1][0].setSelected(True)
                self.bilansList[id_bilan2][0].setSelected(True)
        finally :
            utils_functions.restoreCursor()
            self.scene.mustWait = False
            self.updateGraphWidget()

    def showIntermediateLinksChanged(self):
        """
        afficher/masquer les liens intermédiaires
        """
        sender = self.sender()
        self.hideIntermediate = not(sender.isChecked())
        if sender == self.showIntermediateLinksCheckBox:
            self.actionShowIntermediateLinks.setChecked(not(self.hideIntermediate))
        else:
            self.showIntermediateLinksCheckBox.setChecked(not(self.hideIntermediate))
        self.updateGraphWidget()

    def doDeleteAll(self):
        # un message d'avertissement :
        beginMsg = QtWidgets.QApplication.translate('main', 
            'All items displayed and balances will be deleted.')
        continueMsg = QtWidgets.QApplication.translate('main', 
            'Are you sure you want to continue?')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p><b>{1}<br/>{2}</b></p>').format(utils.SEPARATOR_LINE, beginMsg, continueMsg)
        if utils_functions.messageBox(
            self.main, level='warning', 
            message=message, buttons=['Yes', 'Cancel']) != QtWidgets.QMessageBox.Yes:
            return

        utils_functions.doWaitCursor()
        self.scene.mustWait = True
        try:
            # on récupère les items affichés :
            selectedItems = [
                id_item for id_item in self.itemsList if self.itemsList[id_item][2]]
            id_items = utils_functions.array2string(selectedItems)
            # on récupère les bilans affichés :
            selectedBilans = [
                id_bilan for id_bilan in self.bilansList if self.bilansList[id_bilan][2]]
            id_bilans = utils_functions.array2string(selectedBilans)
            query_my = utils_db.query(self.main.db_my)
            # on supprime les items :
            commandLine_my1 = utils_db.qdf_where.format('items', 'id_item', id_items)
            commandLine_my2 = utils_db.qdf_where.format('item_bilan', 'id_item', id_items)
            commandLine_my3 = utils_db.qdf_where.format('evaluations', 'id_item', id_items)
            commandLine_my4 = (
                'UPDATE counts SET id_item=-1 '
                'WHERE id_item IN ({0})').format(id_items)
            commandLine_my5 = (
                'DELETE FROM comments '
                'WHERE isItem=1 AND id IN ({0})').format(id_items)
            commandLine_my6 = utils_db.qdf_where.format('tableau_item', 'id_item', id_items)
            # on supprime les bilans :
            commandLine_my11 = utils_db.qdf_where.format('bilans', 'id_bilan', id_bilans)
            commandLine_my12 = utils_db.qdf_where.format('item_bilan', 'id_bilan', id_bilans)
            commandLine_my13 = utils_db.qdf_where.format('profil_bilan_BLT', 'id_bilan', id_bilans)
            commandLine_my14 = (
                'DELETE FROM bilan_bilan '
                'WHERE id_bilan1 IN ({0}) OR id_bilan2 IN ({0})').format(id_bilans)
            commandLine_my15 = utils_db.qdf_where.format('evaluations', 'id_bilan', id_bilans)
            commandLine_my16 = utils_db.qdf_where.format('groupe_bilan', 'id_bilan', id_bilans)
            commandLine_my17 = (
                'DELETE FROM comments '
                'WHERE isItem=0 AND id IN ({0})').format(id_bilans)
            commandLines = [
                commandLine_my1, 
                commandLine_my2, 
                commandLine_my3, 
                commandLine_my4, 
                commandLine_my5, 
                commandLine_my6, 
                commandLine_my11, 
                commandLine_my12, 
                commandLine_my13, 
                commandLine_my14, 
                commandLine_my15, 
                commandLine_my16, 
                commandLine_my17]
            query_my = utils_db.queryExecute(commandLines, db=self.main.db_my)
           # on met à jour l'affichage :
            self.beginNode = None
            for id_item in selectedItems:
                if id_item in self.itemsList:
                    [itemWidget, itemNode, visible] = self.itemsList[id_item]
                    self.itemsListWidget.takeItem(self.itemsListWidget.row(itemWidget))
                    self.itemsNodesList.remove(itemNode)
                    self.scene.removeItem(itemNode)
                    del(self.itemsList[id_item])
            for id_bilan in selectedBilans:
                if id_bilan in self.bilansList:
                    [bilanWidget, bilanNode, visible] = self.bilansList[id_bilan]
                    self.bilansListWidget.takeItem(self.bilansListWidget.row(bilanWidget))
                    self.bilansNodesList.remove(bilanNode)
                    self.scene.removeItem(bilanNode)
                    del(self.bilansList[id_bilan])
            self.updateGraphWidget()
        finally :
            utils_functions.restoreCursor()
            self.scene.mustWait = False
            # on met à jour l'affichage :
            self.beginNode = None
            self.updateGraphWidget()
            self.doModified()

    def doSelectBilans(self):
        # on récupère les items affichés :
        selectedItems = [
            id_item for id_item in self.itemsList
            if self.itemsList[id_item][2]]
        for id_item in selectedItems:
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'item_bilan', 'id_item', id_item)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                id_bilan = int(query_my.value(1))
                coeff = int(query_my.value(2))
                if id_bilan in self.bilansList:
                    self.bilansList[id_bilan][0].setSelected(True)

    def doSelectItems(self):
        # on récupère les bilans affichés :
        selectedBilans = [
            id_bilan for id_bilan in self.bilansList
            if self.bilansList[id_bilan][2]]
        for id_bilan in selectedBilans:
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'item_bilan', 'id_bilan', id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                coeff = int(query_my.value(2))
                if id_item in self.itemsList:
                    self.itemsList[id_item][0].setSelected(True)

    def doExport2Csv(self):
        import prof
        # on récupère les items affichés :
        selectedItems = [
            id_item for id_item in self.itemsList
            if self.itemsList[id_item][2]]
        # on récupère les bilans affichés :
        selectedBilans = [
            id_bilan for id_bilan in self.bilansList
            if self.bilansList[id_bilan][2]]
        csvTitle = QtWidgets.QApplication.translate('main', 'Save csv File')
        matiereName = prof.diversFromSelection(self.main)[2]
        matiereCode = self.main.me['MATIERES']['Matiere2Code'].get(matiereName, '')
        proposedName = utils_functions.u(
            '{0}/{1}-Liste_CPT.csv').format(self.main.workDir, matiereCode)
        csvExt = QtWidgets.QApplication.translate('main', 'csv files (*.csv)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, csvTitle, proposedName, csvExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.main.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportBilansItems2Csv(
            self.main, fileName, selectedItems, selectedBilans)

    def doCreateItem(self):
        utils_functions.myPrint('doCreateItem')
        dialog = CreateMachinDlg(parent=self.main, what='CreateItem')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.initAll()
            self.doModified()

    def doChooseFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(
            parent=self.main, tableName=tableName, extendedSelection=True)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # création des items, bilans et liaisons :
            createCptFromList(self.main, tableName, dialog, what='CreateItems')
            # enfin on met à jour l'affichage :
            self.initAll()
            self.doModified()

    def doCreateBilan(self):
        dialog = CreateMachinDlg(parent=self.main, what='CreateBilan')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.initAll()
            self.doModified()

    def doChooseBilansFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBilansBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseBilansCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseBilansCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(
            parent=self.main, tableName=tableName, extendedSelection=True)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # création des bilans :
            createCptFromList(self.main, tableName, dialog, what='CreateBilans')
            # enfin on met à jour l'affichage :
            self.initAll()
            self.doModified()

    def doModified(self):
        self.modified = True
        self.buttonsList['apply'].setEnabled(True)

    def doApply(self):
        """
        bouton "appliquer" les changements sans fermer la fenêtre
        """
        self.main.initialTables = saveTables(
            self.main, tables=TABLES_FOR_ITEMS_BILANS)
        self.main.changeDBMyState()
        self.buttonsList['apply'].setEnabled(False)
        self.buttonsList['cancel'].clearFocus()


class ItemsBilansTableDlg(QtWidgets.QDialog):
    """
    gestion des items et bilans dans un tableau
    """
    def __init__(self, parent=None):
        super(ItemsBilansTableDlg, self).__init__(parent)
        utils_functions.doWaitCursor()
        try:
            self.main = parent
            # le titre de la fenêtre :
            title = QtWidgets.QApplication.translate('main', 'ItemsBilansTable')
            self.setWindowTitle(title)
            # des modifications ont été faites :
            self.modified = False

            # création du model :
            self.model = ItemsBilansTableModel(self.main)
            # un QTableView pour l'affichage :
            self.tableView = QtWidgets.QTableView()
            self.tableView.setModel(self.model)
            # un delegate pour l'édition :
            self.tableView.setItemDelegate(MyItemDelegate(self, model=self.model))

            cptChooseIcon = utils.doIcon('cpt-choose')
            # les boutons pour les items :
            createItemButton = QtWidgets.QPushButton(
                utils.doIcon('item-add'), 
                QtWidgets.QApplication.translate('main', 'Create an item'))
            createItemButton.clicked.connect(self.doCreateItem)
            self.chooseBLTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLT'))
            self.chooseBLTButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLTStatusTip'))
            self.chooseBLTButton.clicked.connect(self.doChooseFromList)
            self.chooseCPTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPT'))
            self.chooseCPTButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPTStatusTip'))
            self.chooseCPTButton.clicked.connect(self.doChooseFromList)
            self.chooseCFDButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFD'))
            self.chooseCFDButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFDStatusTip'))
            self.chooseCFDButton.clicked.connect(self.doChooseFromList)
            #les boutons pour les bilans :
            createBilanButton = QtWidgets.QPushButton(
                utils.doIcon('bilan-add'), 
                QtWidgets.QApplication.translate('main', 'Create a balance'))
            createBilanButton.clicked.connect(self.doCreateBilan)
            self.chooseBilansBLTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLT'))
            self.chooseBilansBLTButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromBLTStatusTip'))
            self.chooseBilansBLTButton.clicked.connect(self.doChooseBilansFromList)
            self.chooseBilansCPTButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPT'))
            self.chooseBilansCPTButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCPTStatusTip'))
            self.chooseBilansCPTButton.clicked.connect(self.doChooseBilansFromList)
            self.chooseBilansCFDButton = QtWidgets.QPushButton(
                cptChooseIcon, 
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFD'))
            self.chooseBilansCFDButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChooseItemsFromCFDStatusTip'))
            self.chooseBilansCFDButton.clicked.connect(self.doChooseBilansFromList)
            # on agence tout cas boutons :
            itemsBilansGrid = QtWidgets.QGridLayout()
            itemsBilansGrid.addWidget(createItemButton,         0, 0, 1, 3)
            itemsBilansGrid.addWidget(self.chooseBLTButton,          1, 0)
            itemsBilansGrid.addWidget(self.chooseCPTButton,          1, 1)
            itemsBilansGrid.addWidget(self.chooseCFDButton,          1, 2)
            itemsBilansGrid.addWidget(createBilanButton,        0, 3, 1, 3)
            itemsBilansGrid.addWidget(self.chooseBilansBLTButton,    1, 3)
            itemsBilansGrid.addWidget(self.chooseBilansCPTButton,    1, 4)
            itemsBilansGrid.addWidget(self.chooseBilansCFDButton,    1, 5)

            dialogButtons = utils_functions.createDialogButtons(
                self, buttons=('ok', 'apply', 'cancel', 'help'))
            buttonBox = dialogButtons[0]
            self.buttonsList = dialogButtons[1]
            self.buttonsList['apply'].clicked.connect(self.doApply)
            self.buttonsList['apply'].setEnabled(False)

            self.statusBar = QtWidgets.QStatusBar()

            # on place tout dans une grille :
            grid = QtWidgets.QGridLayout()
            grid.addWidget(self.tableView,      2, 0)
            grid.addLayout(itemsBilansGrid,     3, 0)
            grid.addWidget(buttonBox,           5, 0, 1, 2)
            grid.addWidget(self.statusBar,      6, 0)
            self.setLayout(grid)

            self.initAll()
        finally:
            utils_functions.restoreCursor()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('items-bilans-table')

    def initAll(self):
        utils_functions.doWaitCursor()
        try:
            self.model.setupModelData()
            self.tableView.setColumnHidden(0, True)
            self.tableView.setAlternatingRowColors(True)
            self.tableView.resizeColumnsToContents()
            self.tableView.resizeRowsToContents()
        finally:
            utils_functions.restoreCursor()

    def doCreateItem(self):
        utils_functions.myPrint('doCreateItem')
        dialog = CreateMachinDlg(parent=self.main, what='CreateItem')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.initAll()
            self.doModified()

    def doChooseFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(
            parent=self.main, tableName=tableName, extendedSelection=True)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # création des items, bilans et liaisons :
            createCptFromList(self.main, tableName, dialog, what='CreateItems')
            # enfin on met à jour l'affichage :
            self.initAll()
            self.doModified()

    def doCreateBilan(self):
        dialog = CreateMachinDlg(parent=self.main, what='CreateBilan')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.initAll()
            self.doModified()

    def doChooseBilansFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBilansBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseBilansCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseBilansCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(
            parent=self.main, tableName=tableName, extendedSelection=True)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # création des bilans :
            createCptFromList(self.main, tableName, dialog, what='CreateBilans')
            # enfin on met à jour l'affichage :
            self.initAll()
            self.doModified()

    def doModified(self):
        self.modified = True
        self.buttonsList['apply'].setEnabled(True)

    def doApply(self):
        """
        bouton "appliquer" les changements sans fermer la fenêtre
        """
        self.main.initialTables = saveTables(
            self.main, tables=TABLES_FOR_ITEMS_BILANS)
        self.main.changeDBMyState()
        self.buttonsList['apply'].setEnabled(False)
        self.buttonsList['cancel'].clearFocus()


class LinkItemsToBilanDlg(QtWidgets.QDialog):
    """
    Pour relier plusieurs items à un même bilan en une seule fois
    """
    def __init__(self, parent=None):
        super(LinkItemsToBilanDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate('main', 'Link several items to a balance')
        self.setWindowTitle(title)

        self.baseList = utils_2lists.DragDropListWidget(parent=self, num=0)
        #self.baseList.setSortingEnabled(True)
        baseLayout = QtWidgets.QVBoxLayout()
        baseLayout.addWidget(self.baseList)
        self.baseGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Base'))
        self.baseGroupBox.setLayout(baseLayout)

        self.selectionList = utils_2lists.DragDropListWidget(parent=self, num=1)
        selectionLayout = QtWidgets.QVBoxLayout()
        selectionLayout.addWidget(self.selectionList)
        self.selectionGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Selection'))
        self.selectionGroupBox.setLayout(selectionLayout)

        listesLayout = QtWidgets.QHBoxLayout()
        listesLayout.addWidget(self.baseGroupBox)
        listesLayout.addWidget(self.selectionGroupBox)
        listesGroupBox = QtWidgets.QGroupBox()
        listesGroupBox.setLayout(listesLayout)
        listesGroupBox.setFlat(True)

        label1 = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Balance:')))
        label1.setMinimumWidth(100)
        label1.setMaximumWidth(100)
        self.comboBox1 = QtWidgets.QComboBox()
        self.comboBox1.setMinimumWidth(400)
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
            db=self.main.db_my)
        while query_my.next():
            t = query_my.value(1) + ': ' + query_my.value(2)
            self.comboBox1.addItem(t, int(query_my.value(0)))
        label2 = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'coefficient:')))
        self.spinBox2 = QtWidgets.QSpinBox()
        self.spinBox2.setRange(1, 10)
        self.spinBox2.setSingleStep(1)
        self.spinBox2.setValue(1)
        cptChooseIcon = utils.doIcon('cpt-choose')
        self.chooseBLTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'Choose competence from bulletin'))
        self.chooseBLTButton.clicked.connect(self.doChooseFromList)
        self.chooseCPTButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'Choose competence from referential'))
        self.chooseCPTButton.clicked.connect(self.doChooseFromList)
        self.chooseCFDButton = QtWidgets.QPushButton(
            cptChooseIcon, 
            QtWidgets.QApplication.translate('main', 'Choose a confidential competence'))
        self.chooseCFDButton.clicked.connect(self.doChooseFromList)
        createBilanButton = QtWidgets.QPushButton(
            utils.doIcon('bilan-add'), 
            QtWidgets.QApplication.translate('main', 'Create a balance'))
        createBilanButton.clicked.connect(self.doCreateBilan)

        grid = QtWidgets.QGridLayout()
        grid.addWidget(label1, 1, 0)
        grid.addWidget(self.comboBox1, 1, 1)
        grid.addWidget(label2, 2, 0)
        grid.addWidget(self.spinBox2, 2, 1)
        grid.addWidget(self.chooseBLTButton, 3, 0, 1, 2)
        grid.addWidget(self.chooseCPTButton, 4, 0, 1, 2)
        grid.addWidget(self.chooseCFDButton, 5, 0, 1, 2)
        grid.addWidget(createBilanButton, 10, 0, 1, 2)
        bilanGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Balance:'))
        bilanGroupBox.setLayout(grid)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listesGroupBox)
        mainLayout.addWidget(bilanGroupBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        self.remplirListes()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('link-items-to-bilan')

    def remplirListes(self):
        """
        blablabla
        """
        self.Avant = []
        self.Apres = []
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format(
                'items', 'UPPER(Name)'), 
            db=self.main.db_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            itemText = utils_functions.u(
                '{0} \t[{1}]').format(itemName, itemLabel)
            item = QtWidgets.QListWidgetItem(itemText)
            item.setToolTip(itemLabel)
            item.setData(QtCore.Qt.UserRole, id_item)
            self.baseList.addItem(item)

    def doAfterDrop(self):
        self.baseList.removeAfterDrop()
        self.selectionList.removeAfterDrop()

    def doChooseFromList(self):
        """
        appel d'un dialog spécifique
        """
        sender = self.sender()
        if sender == self.chooseBLTButton:
            tableName = 'bulletin'
        elif sender == self.chooseCPTButton:
            tableName = 'referentiel'
        elif sender == self.chooseCFDButton:
            tableName = 'confidentiel'
        else:
            return
        dialog = ChooseCptFromListDlg(parent=self.main, tableName=tableName)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            if tableName == 'bulletin':
                id_competence = dialog.id_cpt + utils.decalageBLT
            elif tableName == 'referentiel':
                id_competence = dialog.id_cpt
            elif tableName == 'confidentiel':
                id_competence = dialog.id_cpt + utils.decalageCFD
            commandLine_commun = utils_db.q_selectAllFromWhere.format(
                tableName, 'id', dialog.id_cpt)
            query_commun = utils_db.queryExecute(commandLine_commun, db=self.main.db_commun)
            while query_commun.next():
                name = query_commun.value(1)
                label = query_commun.value(5)
                r, id_bilan = createBilan(self.main, name, label, id_competence)
                t = name + ': ' + label
                i = self.comboBox1.findText(t, QtCore.Qt.MatchContains)
                if i == -1:
                    self.comboBox1.addItem(t, id_bilan)
                    i = self.comboBox1.findText(t, QtCore.Qt.MatchContains)
                self.comboBox1.setCurrentIndex(i)

    def doCreateBilan(self):
        """
        appel d'un dialog spécifique
        """
        dialog = CreateMachinDlg(
            parent=self.main, what='CreateBilan', withChoose=False)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            bilanName = dialog.nameEdit.text()
            bilanLabel = dialog.labelEdit.text()
            id_bilan = dialog.id_bilan
            t = bilanName + ": " + bilanLabel
            self.comboBox1.addItem(t, id_bilan)
            self.comboBox1.setCurrentIndex(self.comboBox1.findText(t, QtCore.Qt.MatchContains))

def linkItemsToBilan(main, dialog):
    """
    Applique les changements demandé dans LinkItemsToBilanDlg
    """
    # on récupère les données du bilan :
    currentIndex = dialog.comboBox1.currentIndex()
    id_bilan = dialog.comboBox1.itemData(currentIndex)
    coeff = dialog.spinBox2.value()
    # pour chaque item :
    for i in range(dialog.selectionList.count()):
        # on récupère l'id_item :
        item = dialog.selectionList.item(i)
        id_item = item.data(QtCore.Qt.UserRole)
        # on crée les liens :
        affectOther(main, 'linkItemBilan', id_item, id_bilan, coeff)

class LinkBilansDlg(QtWidgets.QDialog):
    """
    Pour relier des bilans entre eux.
    Liste de tous les bilans à gauche.
    Liste des bilans liés à droite, avec ajout et suppression.
    """
    def __init__(self, parent=None):
        super(LinkBilansDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate('main', 'LinkBilans')
        self.setWindowTitle(title)
        self.waiting = False

        # à gauche, la liste de tous les bilans dans un QTableView :
        self.bilans1Model = QtSql.QSqlTableModel(self.main, self.main.db_my)
        self.bilans1Model.setTable('bilans')
        self.bilans1Model.select()
        self.bilans1Model.setHeaderData(1, QtCore.Qt.Horizontal, 
            QtWidgets.QApplication.translate('main', 'Name'))
        self.bilans1Model.setHeaderData(2, QtCore.Qt.Horizontal, 
            QtWidgets.QApplication.translate('main', 'Label'))
        self.bilans1View = QtWidgets.QTableView()
        self.bilans1View.setModel(self.bilans1Model)
        self.bilans1View.setSelectionMode(QtWidgets.QTableView.SingleSelection)
        self.bilans1View.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
        self.bilans1View.setColumnHidden(0, True)
        self.bilans1View.setColumnHidden(3, True)
        self.bilans1View.resizeColumnsToContents()
        self.bilans1View.horizontalHeader().setStretchLastSection(True)
        self.bilans1View.setSortingEnabled(True)
        self.bilans1View.sortByColumn(1, QtCore.Qt.AscendingOrder)
        if utils.PYQT == 'PYQT5':
            selectionModel = QtCore.QItemSelectionModel(self.bilans1Model)
        else:
            selectionModel = QtGui.QItemSelectionModel(self.bilans1Model)
        self.bilans1View.setSelectionModel(selectionModel)
        selectionModel.currentRowChanged.connect(self.bilans1Changed)

        bilans1Layout = QtWidgets.QVBoxLayout()
        bilans1Layout.addWidget(self.bilans1View)
        bilans1GroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Balances'))
        bilans1GroupBox.setLayout(bilans1Layout)
        bilans1GroupBox.setMinimumWidth(300)

        # à droite, les bilans liés à celui sélectionné plus des outils :
        self.bilans2Model = QtSql.QSqlQueryModel(self.main)
        self.bilans2View = QtWidgets.QTableView()
        # un comboBox pour ajouter un bilan lié :
        self.addBilanComboBox = QtWidgets.QComboBox()
        #self.addBilanComboBox.setMinimumWidth(200)
        self.addBilanComboBox.activated.connect(self.addBilanComboBoxChanged)
        # un bouton pour retirer un bilan lié :
        self.removeBilanButton = QtWidgets.QPushButton(
            utils.doIcon('bilan-remove'), 
            QtWidgets.QApplication.translate('main', 'Remove a balance'))
        self.removeBilanButton.setEnabled(False)
        self.removeBilanButton.clicked.connect(self.doRemoveBilan)
        # mise en forme :
        bilans2Layout = QtWidgets.QVBoxLayout()
        bilans2Layout.addWidget(self.bilans2View)
        bilans2Layout.addWidget(self.addBilanComboBox)
        bilans2Layout.addWidget(self.removeBilanButton)
        bilans2GroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Linked Balances'))
        bilans2GroupBox.setLayout(bilans2Layout)
        bilans2GroupBox.setMinimumWidth(300)

        # mise en forme des 2 partie dans un QGroupBox :
        listesLayout = QtWidgets.QHBoxLayout()
        listesLayout.addWidget(bilans1GroupBox)
        listesLayout.addWidget(bilans2GroupBox)
        listesGroupBox = QtWidgets.QGroupBox()
        listesGroupBox.setLayout(listesLayout)
        listesGroupBox.setFlat(True)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place de tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listesGroupBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        self.id_bilan1 = -1
        self.id_bilan2 = -1
        try:
            self.bilans1View.selectRow(0)
        except:
            pass
        self.bilans1Changed(self.bilans1View.currentIndex())

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('link-bilans')

    def bilans1Changed(self, index):
        """
        on change le bilan1 sélectionné
        on doit mettre à jour l'affichage des bilans2 liés
        et le comboBox addBilanComboBox
        """
        if self.waiting:
            return
        self.waiting = True
        try:
            self.addBilanComboBox.clear()
            if index.isValid():
                record = self.bilans1Model.record(index.row())
                self.id_bilan1 = record.value('id_bilan')
                commandLine_my = (
                    'SELECT bilan_bilan.id_bilan2, bilans.* '
                    'FROM bilan_bilan '
                    'JOIN bilans ON bilans.id_bilan=bilan_bilan.id_bilan2 '
                    'WHERE bilan_bilan.id_bilan1={0} '
                    'ORDER BY bilans.Name')
                commandLine_my = commandLine_my.format(self.id_bilan1)
                self.bilans2Model.setQuery(commandLine_my, self.main.db_my)

                self.bilans2Model.setHeaderData(2, QtCore.Qt.Horizontal, 
                    QtWidgets.QApplication.translate('main', 'Name'))
                self.bilans2Model.setHeaderData(3, QtCore.Qt.Horizontal, 
                    QtWidgets.QApplication.translate('main', 'Label'))
                self.bilans2View.setModel(self.bilans2Model)
                self.bilans2View.setSelectionMode(QtWidgets.QTableView.SingleSelection)
                self.bilans2View.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
                self.bilans2View.setColumnHidden(0, True)
                self.bilans2View.setColumnHidden(1, True)
                self.bilans2View.setColumnHidden(4, True)
                self.bilans2View.resizeColumnsToContents()
                self.bilans2View.horizontalHeader().setStretchLastSection(True)
                self.bilans2View.setSortingEnabled(True)
                self.bilans2View.sortByColumn(2, QtCore.Qt.AscendingOrder)
                if utils.PYQT == 'PYQT5':
                    selectionModel = QtCore.QItemSelectionModel(self.bilans2Model)
                else:
                    selectionModel = QtGui.QItemSelectionModel(self.bilans2Model)
                self.bilans2View.setSelectionModel(selectionModel)
                selectionModel.currentRowChanged.connect(self.bilans2Changed)

                # liste des bilans à ne pas mettre dans la liste :
                idToIgnore = [self.id_bilan1]
                paths = createPathsList(self.main)[0]
                for path in paths :
                    if self.id_bilan1 in path:
                        for id_bilan in path:
                            if not(id_bilan in idToIgnore):
                                idToIgnore.append(id_bilan)
                # on met d'abord le titre dans le comboBox :
                addBilanName = QtWidgets.QApplication.translate('main', 'Add a balance')
                self.addBilanComboBox.addItem(
                    utils.doIcon('bilan-add'), addBilanName, -1)
                # on remplit la liste :
                query_my = utils_db.queryExecute(
                    utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
                    db=self.main.db_my)
                while query_my.next():
                    id_bilan2 = int(query_my.value(0))
                    if not(id_bilan2 in idToIgnore):
                        t = query_my.value(1) + ": " + query_my.value(2)
                        self.addBilanComboBox.addItem(t, id_bilan2)
                self.addBilanComboBox.setEnabled(True)
                self.id_bilan2 = -1
                self.removeBilanButton.setEnabled(False)
            else:
                self.bilans2Model.clear()
                self.bilans2View.setModel(None)
                self.addBilanComboBox.setEnabled(False)
                self.id_bilan2 = -1
                self.removeBilanButton.setEnabled(False)
        finally:
            self.waiting = False

    def bilans2Changed(self, index):
        """
        on change le bilan sélectionné
        """
        if index.isValid():
            record = self.bilans2Model.record(index.row())
            self.id_bilan2 = record.value('id_bilan')
            self.removeBilanButton.setEnabled(True)
        else:
            self.id_bilan2 = -1
            self.removeBilanButton.setEnabled(False)

    def addBilanComboBoxChanged(self):
        currentIndex = self.addBilanComboBox.currentIndex()
        id_bilan2 = self.addBilanComboBox.itemData(currentIndex)
        # on ajoute le lien à la table bilan_bilan :
        commandLine_my = utils_db.insertInto('bilan_bilan')
        query_my = utils_db.queryExecute(
            {commandLine_my: (self.id_bilan1, id_bilan2)}, 
            db=self.main.db_my)
        affectOther(self.main, 'linkBilans', self.id_bilan1, id_bilan2)
        # on met à jour l'affichage :
        self.bilans1Changed(self.bilans1View.currentIndex())

    def doRemoveBilan(self):
        """
        blablabla
        """
        if self.id_bilan1 < 0:
            return
        record = self.bilans2Model.record(self.bilans2View.currentIndex().row())
        id_bilan2 = record.value('id_bilan')
        # on supprime le lien de la table bilan_bilan :
        commandLine_my = utils_db.qdf_prof_bilanBilan.format(self.id_bilan1, id_bilan2)
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        # et on traite les liens items-bilans :
        affectOther(self.main, 'unlinkBilans', self.id_bilan1, id_bilan2)
        # on met à jour l'affichage :
        self.bilans1Changed(self.bilans1View.currentIndex())
        try:
            self.bilans2View.selectRow(0)
        except:
            pass
        self.bilans2Changed(self.bilans2View.currentIndex())

class GestAdvicesDlg(QtWidgets.QDialog):
    """
    Gestion des commentaires
    """
    def __init__(self, parent=None):
        super(GestAdvicesDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate('main', 'Manage advices')
        self.setWindowTitle(title)

        self.model = QtSql.QSqlQueryModel(self.main)
        self.view = QtWidgets.QTableView()

        self.showItemsButton = QtWidgets.QPushButton(
            utils.doIcon('items'), 
            QtWidgets.QApplication.translate('main', 'Show Items'))
        self.showItemsButton.clicked.connect(self.doShowItems)
        self.showItemsButton.setCheckable(True)
        self.showBilansButton = QtWidgets.QPushButton(
            utils.doIcon('bilans'), 
            QtWidgets.QApplication.translate('main', 'Show Balances'))
        self.showBilansButton.clicked.connect(self.doShowBilans)
        self.showBilansButton.setCheckable(True)

        whatLayout = QtWidgets.QVBoxLayout()
        whatLayout.addWidget(self.view)
        whatLayout.addWidget(self.showItemsButton)
        whatLayout.addWidget(self.showBilansButton)
        self.whatGroupBox = QtWidgets.QGroupBox(self.main.TR_ITEMS)
        self.whatGroupBox.setLayout(whatLayout)
        self.whatGroupBox.setMinimumWidth(300)

        import utils_htmleditor
        self.webViewWidget = utils_htmleditor.WebViewWidget(parent=self.main)

        self.saveChangesButton = QtWidgets.QPushButton(
            utils.doIcon('document-save'), 
            QtWidgets.QApplication.translate('main', 'Save changes'))
        self.saveChangesButton.clicked.connect(self.doSaveChanges)

        self.eraseAdviceButton = QtWidgets.QPushButton(
            utils.doIcon('edit-clear'), 
            QtWidgets.QApplication.translate('main', 'Erase advice'))
        self.eraseAdviceButton.clicked.connect(self.doEraseAdvice)

        adviceLayout = QtWidgets.QVBoxLayout()
        adviceLayout.addWidget(self.webViewWidget)
        adviceLayout.addWidget(self.eraseAdviceButton)
        adviceLayout.addWidget(self.saveChangesButton)
        adviceGroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Advice'))
        adviceGroupBox.setLayout(adviceLayout)

        listesLayout = QtWidgets.QHBoxLayout()
        listesLayout.addWidget(self.whatGroupBox)
        listesLayout.addWidget(adviceGroupBox)
        listesGroupBox = QtWidgets.QGroupBox()
        listesGroupBox.setLayout(listesLayout)
        listesGroupBox.setFlat(True)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listesGroupBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        self.showItemsBilans('items', mustRecreate=False)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('gest-comments')

    def doShowItems(self):
        self.showItemsBilans('items')

    def doShowBilans(self):
        self.showItemsBilans('bilans')

    def showItemsBilans(self, what, mustRecreate=True):
        """
        on change l'item sélectionné
        on doit mettre à jour l'affichage des bilans
        """
        if what=="items":
            self.whatGroupBox.setTitle(self.main.TR_ITEMS)
            self.showItemsButton.setChecked(True)
            self.showBilansButton.setChecked(False)
            self.isItem = 1

            self.model.setQuery(
                utils_db.q_selectAllFromOrder.format(
                    'items', 'UPPER(Name)'), 
                self.main.db_my)
        else:
            self.whatGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'Balances'))
            self.showItemsButton.setChecked(False)
            self.showBilansButton.setChecked(True)
            self.isItem = 0

            commandLine = ('SELECT id_bilan, Name, Label FROM bilans '
                            'WHERE id_competence=-1 ORDER BY UPPER(Name)')
            self.model.setQuery(commandLine, self.main.db_my)

        self.model.setHeaderData(1, QtCore.Qt.Horizontal, 
            QtWidgets.QApplication.translate('main', 'Name'))
        self.model.setHeaderData(2, QtCore.Qt.Horizontal, 
            QtWidgets.QApplication.translate('main', 'Label'))

        self.view.setModel(self.model)
        self.view.setSelectionMode(QtWidgets.QTableView.SingleSelection)
        self.view.setSelectionBehavior(QtWidgets.QTableView.SelectRows)
        self.view.setColumnHidden(0, True)
        self.view.resizeColumnsToContents()
        self.view.setColumnWidth(2, 200)
        self.view.setSortingEnabled(True)
        self.view.sortByColumn(1, QtCore.Qt.AscendingOrder)

        if utils.PYQT == 'PYQT5':
            selectionModel = QtCore.QItemSelectionModel(self.model)
        else:
            selectionModel = QtGui.QItemSelectionModel(self.model)
        self.view.setSelectionModel(selectionModel)
        selectionModel.currentRowChanged.connect(self.whatChanged)

        self.id_what = -1
        self.actualIndex = -1
        if utils_webengine.WEB_ENGINE == 'WEBENGINE':
            self.initialAdvice = '<html contenteditable="true"><head></head><body></body></html>'
        else:
            self.initialAdvice = '<html><head></head><body></body></html>'
        try:
            self.view.selectRow(0)
        except:
            pass

    def whatChanged(self, index):
        """
        on change l'item sélectionné
        on doit mettre à jour l'affichage du conseil
        On doit aussi enregistrer les changements si besoin
        """
        if index.isValid():
            if index.row() == self.actualIndex:
                return
            self.doSaveChanges()
            self.actualIndex = index.row()
            record = self.model.record(index.row())
            if self.isItem==1:
                self.id_what = record.value('id_item')
            else:
                self.id_what = record.value('id_bilan')
        else:
            self.id_what = -1
            self.actualIndex = -1
        try:
            self.webViewWidget.waiting = True
            content = utils_functions.u('')
            commandLine = ('SELECT comment FROM comments '
                            'WHERE isItem={0} AND id={1}').format(self.isItem, self.id_what)
            query_my = utils_db.queryExecute(commandLine, db=self.main.db_my)
            while query_my.next():
                content = query_my.value(0)
            self.webViewWidget.webView.setHtml(content)
            self.webViewWidget.waiting = False
            self.initialAdvice = self.webViewWidget.webView.toHtml()
        except:
            utils_functions.afficheMsgPb(self.main)

    def doSaveChanges(self):
        """
        On enregistre les changements si besoin
        """
        # si rien n'est sélectionné, on quitte :
        if (self.id_what == -1) or (self.actualIndex == -1):
            return
        # si rien n'a changé, on quitte :
        advice = self.webViewWidget.webView.toHtml()
        if advice == self.initialAdvice:
            return
        import utils_htmleditor
        advice = utils_htmleditor.formatAdvice(advice)
        # on met à jour la base :
        commandLine = 'DELETE FROM comments WHERE isItem={0} AND id={1}'.format(
            self.isItem, self.id_what)
        query_my = utils_db.queryExecute(commandLine, db=self.main.db_my)
        commandLine = utils_db.insertInto('comments')
        if advice != '':
            query_my = utils_db.queryExecute(
                {commandLine: (self.isItem, self.id_what, advice)}, 
                query=query_my)

    def doEraseAdvice(self):
        try:
            # on met à jour la base :
            commandLine = (
                'DELETE FROM comments '
                'WHERE isItem={0} AND id={1}').format(self.isItem, self.id_what)
            query_my = utils_db.queryExecute(commandLine, db=self.main.db_my)
            # on vide l'affichage :
            self.webViewWidget.waiting = True
            content = ''
            self.webViewWidget.webView.setHtml(content)
            self.webViewWidget.waiting = False
            self.initialAdvice = self.webViewWidget.webView.toHtml()
        except:
            utils_functions.afficheMsgPb(self.main)



###########################################################"
#   MODELS ET DELEGATES
###########################################################"

class GestItemsModel(QtCore.QAbstractTableModel):
    """
    Model pour afficher les items ou les bilans dans la fenêtre "GestItems".
    variable utiles :
        * columns : les titres des colonnes (id, Affichage, ToolTip, ...)
        * datas : les données [TYPE, valeur, toolTip, ...]
    """
    def __init__(self, parent=None, table='items'):
        super(GestItemsModel, self).__init__(parent)

        self.main = parent
        self.table = table
        self.columns = [(-1, '', '')]
        self.datas = []

        self.textGray = QtGui.QBrush(QtGui.QColor('#555555'))
        self.alignLeft = int(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.alignCenter = int(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        self.alignRight = int(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.editableFlag = self.defaultFlag | QtCore.Qt.ItemIsEditable
        self.nothingFlag = QtCore.Qt.NoItemFlags

    def setupModelData(self, id_itemSelected=-1):
        self.beginResetModel()
        try:
            coeffTitle = QtWidgets.QApplication.translate('main', 'Coeff')
            nameTitle = QtWidgets.QApplication.translate('main', 'Name')
            labelTitle = QtWidgets.QApplication.translate('main', 'Label')
            self.datas = []
            query_my = utils_db.query(self.main.db_my)
            if self.table == 'items':
                self.columns = [(-1, '', ''), (-1, nameTitle, ''), (-1, labelTitle, '')]
                # on récupère les items :
                commandLine_my = utils_db.q_selectAllFromOrder.format(
                    'items', 'UPPER(Name)')
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                numLine = 0
                while query_my.next():
                    id_item = int(query_my.value(0))
                    itemName = query_my.value(1)
                    itemLabel = query_my.value(2)
                    numLine += 1
                    toolTip = utils_functions.u('{0} :\n  {1}').format(itemName, itemLabel)
                    newLine = [
                        [utils.INDETERMINATE, numLine, id_item], 
                        [utils.LINE, itemName, toolTip], 
                        [utils.LINE, itemLabel, toolTip]]
                    self.datas.append(newLine)
            elif self.table == 'bilans':
                self.columns = [(-1, '', ''), (-1, coeffTitle, ''), 
                                (-1, nameTitle, ''), (-1, labelTitle, '')]
                # on récupère les bilans :
                commandLine_my = (
                    'SELECT item_bilan.coeff, bilans.* '
                    'FROM item_bilan '
                    'JOIN bilans ON bilans.id_bilan=item_bilan.id_bilan '
                    'WHERE item_bilan.id_item={0} '
                    'ORDER BY bilans.Name')
                commandLine_my = commandLine_my.format(id_itemSelected)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                numLine = 0
                while query_my.next():
                    coeff = int(query_my.value(0))
                    id_bilan = int(query_my.value(1))
                    bilanName = query_my.value(2)
                    bilanLabel = query_my.value(3)
                    id_competence = int(query_my.value(4))
                    numLine += 1
                    toolTip = utils_functions.u('{0} :\n  {1}').format(bilanName, bilanLabel)
                    newLine = [
                        [utils.INDETERMINATE, numLine, id_bilan, id_competence], 
                        [utils.INTEGER, coeff, toolTip, id_itemSelected], 
                        [utils.LINE, bilanName, toolTip], 
                        [utils.LINE, bilanLabel, toolTip]]
                    self.datas.append(newLine)
        finally:
            self.endResetModel()

    def rowCount(self, index=QtCore.QModelIndex()):
        return len(self.datas)

    def columnCount(self, index=QtCore.QModelIndex()):
        return len(self.columns)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column, row = index.column(), index.row()
        what = self.datas[row][column][0]
        if role == QtCore.Qt.TextAlignmentRole:
            # alignement :
            if (what in utils.TYPES_ALIGN_LEFT) and (column > 1):
                return self.alignLeft
            elif what in utils.TYPES_ALIGN_RIGHT:
                return self.alignRight
            else:
                return self.alignCenter
        elif role == QtCore.Qt.DisplayRole:
            # affichage de la case :
            value = self.datas[row][column][1]
            if what in utils.TYPES_NUMBER:
                if value == 0:
                    value = ''
            return value
        elif role == QtCore.Qt.ForegroundRole:
            # couleur du fond de la case :
            if self.table == 'bilans':
                id_competence = self.datas[row][0][3]
                if id_competence > 0:
                    if column > 1:
                        return self.textGray
        elif role == QtCore.Qt.ToolTipRole:
            return utils_functions.u(self.datas[row][column][2])
        else:
            return

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.TextAlignmentRole:
            # alignement :
            if orientation == QtCore.Qt.Horizontal:
                return self.alignCenter
            return self.alignRight
        elif role == QtCore.Qt.DisplayRole:
            # affichage :
            if orientation == QtCore.Qt.Horizontal:
                titleColumn = self.columns[section][1]
                return titleColumn
            else:
                try:
                    titleRow = self.datas[section][0][1]
                except:
                    titleRow = ''
                return titleRow
        elif role == QtCore.Qt.ToolTipRole:
            # affichage du toolTip seulement pour les titres de colonnes :
            if orientation == QtCore.Qt.Horizontal:
                try:
                    return utils_functions.u(self.columns[section][2])
                except:
                    return ''
            return ''
        else:
            return

    def flags(self, index):
        if index.column() > 0:
            if index.column() > 1:
                if self.table == 'bilans':
                    id_competence = self.datas[index.row()][0][3]
                    if id_competence > 0:
                        return self.defaultFlag
            return self.editableFlag
        else:
            return self.nothingFlag

class ItemsBilansTableModel(QtCore.QAbstractTableModel):
    """
    Model pour afficher les items, bilans et liens dans un tableau.
    variable utiles :
        * columns : les titres des colonnes (id, Affichage, ToolTip, ...)
        * datas : les données [TYPE, valeur, toolTip, ...]
        * links : les liens entre items et bilans
    """
    def __init__(self, parent=None):
        super(ItemsBilansTableModel, self).__init__(parent)

        self.main = parent
        self.columns = [('', '')]
        self.datas = []
        self.links = {}

        self.alignLeft = int(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.alignCenter = int(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        self.defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.editableFlag = self.defaultFlag | QtCore.Qt.ItemIsEditable
        self.nothingFlag = QtCore.Qt.NoItemFlags

    def setupModelData(self):
        self.beginResetModel()
        try:
            self.columns = [('', '')]
            self.datas = []
            self.links = {}
            query_my = utils_db.query(self.main.db_my)
            # on récupère les bilans :
            commandLine_my = utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_bilan = int(query_my.value(0))
                bilanName = query_my.value(1)
                bilanLabel = query_my.value(2)
                id_competence = int(query_my.value(3))
                self.columns.append((id_bilan, bilanName, bilanLabel, id_competence))
            # on récupère les items :
            items = []
            commandLine_my = utils_db.q_selectAllFromOrder.format(
                'items', 'UPPER(Name)')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                itemName = query_my.value(1)
                itemLabel = query_my.value(2)
                items.append((id_item, itemName, itemLabel))
            # on récupère les liens :
            commandLine_my = utils_db.q_selectAllFrom.format('item_bilan')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                id_bilan = int(query_my.value(1))
                coeff = int(query_my.value(2))
                self.links[(id_item, id_bilan)] = coeff
            # on remplit les lignes (self.datas) :
            for item in items:
                newLine = [item]
                for bilan in self.columns[1:]:
                    id_item = item[0]
                    id_bilan = bilan[0]
                    coeff = 0
                    if (id_item, id_bilan) in self.links:
                        coeff = self.links[(id_item, id_bilan)]
                    newLine.append([
                        utils.INTEGER, 
                        coeff, 
                        utils_functions.u(
                            '{0} [{1}]\n{2} [{3}]').format(item[1], item[2], bilan[1], bilan[2]), 
                        id_item, 
                        id_bilan])
                self.datas.append(newLine)
        finally:
            self.endResetModel()

    def rowCount(self, index=QtCore.QModelIndex()):
        return len(self.datas)

    def columnCount(self, index=QtCore.QModelIndex()):
        return len(self.columns)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column, row = index.column(), index.row()
        if role == QtCore.Qt.TextAlignmentRole:
            # alignement :
            return self.alignCenter
        elif role == QtCore.Qt.DisplayRole:
            # affichage de la case :
            coeff = self.datas[row][column][1]
            if coeff == 0:
                coeff = ''
            return coeff
        elif role == QtCore.Qt.ToolTipRole:
            return utils_functions.u(self.datas[row][column][2])
        else:
            return

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.TextAlignmentRole:
            # alignement :
            if orientation == QtCore.Qt.Horizontal:
                return self.alignCenter
            return self.alignLeft
        elif role == QtCore.Qt.DisplayRole:
            # affichage :
            if orientation == QtCore.Qt.Horizontal:
                titleColumn = self.columns[section][1]
                return titleColumn
            else:
                titleRow = self.datas[section][0][1]
                return titleRow
        elif role == QtCore.Qt.ToolTipRole:
            # affichage du toolTip seulement pour les titres de colonnes :
            if orientation == QtCore.Qt.Horizontal:
                try:
                    return utils_functions.u(self.columns[section][2])
                except:
                    return ''
            else:
                try:
                    return utils_functions.u(self.datas[section][0][2])
                except:
                    return ''
        else:
            return

    def flags(self, index):
        if index.column() > 0:
            return self.editableFlag
        else:
            return self.nothingFlag

class MyItemDelegate(QtWidgets.QItemDelegate):
    """
    Un délégate pour gérer les éditions
        (coefficients des liens, noms et labels des items ou bilans).
    Les données de self.model.datas doivent être de la forme 
        [TYPE, valeur, toolTip, ...]
    """
    def __init__(self, parent=None, model=None, table='items', minInt=0):
        super(MyItemDelegate, self).__init__(parent)
        # on récupère le parent, la fenêtre principale et le model :
        self.parent = parent
        self.main = parent.parent()
        self.model = model
        # what est le type de donnée :
        self.what = utils.INDETERMINATE
        # le nom de la table à traiter (pour GestItems) :
        self.table = table
        # on peut avoir indiqué la valeur min pour les entiers :
        self.minInt = minInt

    def createEditor(self, parent, option, index):
        # on utilise un QSpinBox :
        row = index.row()
        column = index.column()
        self.what = self.model.datas[row][column][0]
        if self.what == utils.INTEGER:
            editor = QtWidgets.QSpinBox(parent)
            editor.setRange(self.minInt, 10)
            editor.setSingleStep(1)
        elif self.what == utils.LINE:
            editor = QtWidgets.QLineEdit(parent)
        return editor

    def setEditorData(self, editor, index):
        row = index.row()
        column = index.column()
        value = self.model.datas[row][column][1]
        if self.what == utils.INTEGER:
            editor.setValue(value)
        elif self.what == utils.LINE:
            editor.setText(value)

    def setModelData(self, editor, model, index):
        row = index.row()
        column = index.column()
        # on récupère la valeur précédente :
        valueEX = model.datas[row][column][1]
        # et la nouvelle valeur :
        if self.what == utils.INTEGER:
            editor.interpretText()
            value = editor.value()
            if value == 0:
                value = ''
        elif self.what == utils.LINE:
            value = editor.text()
        # mise à jour des données et de l'affichage du model :
        model.setData(index, value, QtCore.Qt.EditRole)
        model.datas[row][column][1] = value
        # mise à jour la base de données :
        if self.what == utils.INTEGER:
            if self.table == 'bilans':
                # on a donc modifié le coefficient d'un lien :
                id_item = model.datas[row][column][3]
                id_bilan = model.datas[row][0][2]
            else:
                # on a donc modifié le coefficient d'un lien :
                id_item = model.datas[row][column][3]
                id_bilan = model.datas[row][column][4]
            # on répercute en cas de bilans liés :
            if value != '':
                editLink(self.main, id_item, id_bilan, value)
                affectOther(self.main, 'linkItemBilan', id_item, id_bilan, value)
            elif valueEX != '':
                affectOther(self.main, 'unlinkItemBilan', id_item, id_bilan)
            self.parent.initAll()
        elif self.table == 'items':
            # on a modifié itemName ou itemLabel
            id_item = model.datas[row][0][2]
            itemName = model.datas[row][1][1]
            itemLabel = model.datas[row][2][1]
            editItem(self.main, id_item, itemName, itemLabel)
        elif self.table == 'bilans':
            # on a modifié bilanName ou bilanLabel
            id_bilan = model.datas[row][0][2]
            bilanName = model.datas[row][2][1]
            bilanLabel = model.datas[row][3][1]
            editBilan(self.main, id_bilan, bilanName, bilanLabel)
        self.parent.doModified()

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)



###########################################################"
#   FONCTIONS DIVERSES
###########################################################"

TABLES_FOR_ITEMS_BILANS = (
    'items', 
    'bilans', 
    'item_bilan', 
    'bilan_bilan', 
    'tableau_item', 
    'evaluations', 
    'counts', 
    'comments', 
    'profil_bilan_BLT', 
    )

def saveTables(main, tables=()):
    """
    Sauvegarde des tables passées dans la liste
    pour restauration si on annule.
    (généralement items, bilans, bilan_bilan et item_bilan)
    Retourne un dico du contenu des tables.
    """
    query_my = utils_db.query(main.db_my)
    initialTables = {}
    for tableName in utils_db.listTablesProf:
        if tableName in tables:
            initialTables[tableName] = utils_db.table2List(tableName, query=query_my)
    return initialTables

def restoreTables(main, initialTables):
    """
    Restauration des tables contenues dans le dico initialTables
    (généralement items, bilans, bilan_bilan et item_bilan)
    """
    if main.closing:
        return
    query_my = utils_db.query(main.db_my)
    for tableName in utils_db.listTablesProf:
        if tableName in initialTables:
            # recréation de la table :
            commandLines = [
                utils_db.qdf_table.format(tableName), 
                utils_db.listTablesProf[tableName][0]]
            query_my = utils_db.queryExecute(
                commandLines, query=query_my)
            commandLine_my = utils_db.insertInto(tableName)
            query_my = utils_db.queryExecute(
                {commandLine_my: initialTables[tableName]}, query=query_my)

def createPathsList(main):
    """
    retourne la liste de tous les chemins maximaux possibles
    à partir de la table bilan_bilan (graphe ordonné).
    Utilise la sous-procédure récursive findPaths.
    """
    def findPaths(actualNode, arcs, nodes):
        """
        retourne la liste des chemins commençant par actualNode
        """
        # liste des fils de actualNode :
        fils = [node2 for (node1, node2) in arcs if node1 == actualNode]
        # si fils est vide, on est tombé sur une feuille :
        if len(fils) < 1:
            return [[actualNode]]
        # sinon, on lance la récursion :
        paths = []
        for node in fils:
            newPaths = findPaths(node, arcs, nodes)
            for path in newPaths:
                actualPath = [actualNode]
                actualPath.extend(path)
                paths.append(actualPath)
        return paths

    utils_functions.doWaitCursor()
    paths = []
    try:
        # liste des arcs d'après la table bilan_bilan :
        arcs = []
        # un dico pour les noeuds du graphe (les id_bilan)
        # pour chacun, on donne 2 variables booléennes
        # la première indique si c'est une racine et la deuxième si c'est une feuille
        nodes = {}
        # récupération de la table et remplissage de arcs et nodes :
        commandLine_my = utils_db.q_selectAllFrom.format('bilan_bilan')
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            id_bilan1 = int(query_my.value(0))
            id_bilan2 = int(query_my.value(1))
            # on ajoute l'arc à la liste :
            arcs.append((id_bilan1, id_bilan2))
            # ajouts ou mises à jour dans nodes :
            if id_bilan1 in nodes:
                # si id_bilan1 est déjà là, on indique que ce n'est pas une feuille :
                nodes[id_bilan1][1] = False
            else:
                # on ajoute id_bilan1, sachant déjà que ce n'est pas une feuille :
                nodes[id_bilan1] = [True, False]
            if id_bilan2 in nodes:
                # si id_bilan2 est déjà là, on indique que ce n'est pas une racine :
                nodes[id_bilan2][0] = False
            else:
                # on ajoute id_bilan2, sachant déjà que ce n'est pas une racine :
                nodes[id_bilan2] = [False, True]
        # liste des noeuds racines :
        racines = [node for node in nodes if nodes[node][0]]
        # trouver tous les chemins maximaux :
        for racine in racines:
            newPaths = findPaths(racine, arcs, nodes)
            paths.extend(newPaths)
    finally:
        utils_functions.restoreCursor()
        return (paths, racines)

def affectOther(main, what, id1, id2, coeff=1, withMessage=True):
    """
    Lorsqu'on crée ou supprime un lien (item-bilan ou bilan->bilan),
    il faut le répercuter aux éventuels bilans liés.
    """
    query_my = utils_db.query(main.db_my)
    if what == 'linkBilans':
        # on récupère les items liés au bilan id1 :
        items = []
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'item_bilan', 'id_bilan', id1)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            coeff = int(query_my.value(2))
            items.append((id_item, coeff))
        # on cherche la liste des bilans liés au bilan id2 :
        idToAffect = [id2]
        paths = createPathsList(main)[0]
        for path in paths :
            if id2 in path:
                for id_bilan in path[path.index(id2) + 1:]:
                    if not(id_bilan in idToAffect):
                        idToAffect.append(id_bilan)
        # et on relie les items aux bilans de la liste :
        lines = []
        for (id_item, coeff) in items:
            for id_bilan in idToAffect:
                # on efface l'éventuelle précédente liaison :
                commandLine_my = utils_db.qdf_prof_itemBilan3.format(id_item, id_bilan)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                # on relie l'item au bilan2 :
                lines.append((id_item, id_bilan, coeff))
        query_my = utils_db.queryExecute(
            {utils_db.insertInto('item_bilan'): lines}, query=query_my)
    elif what == 'unlinkBilans':
        # on récupère la liste et les détails des items liés au bilan id1 :
        items = []
        itemsMessage = ''
        commandLine_my = (
            'SELECT items.* '
            'FROM item_bilan '
            'JOIN items ON items.id_item=item_bilan.id_item '
            'WHERE id_bilan={0}')
        commandLine_my = commandLine_my.format(id1)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            items.append(id_item)
            itemsMessage = utils_functions.u(
                '{0}{1} [{2}]\n').format(itemsMessage, itemName, itemLabel)
        # on récupère la liste et les détails des bilans liés au bilan id2 :
        bilans = [id2]
        paths = createPathsList(main)[0]
        for path in paths :
            if id2 in path:
                for id_bilan in path[path.index(id2) + 1:]:
                    if not(id_bilan in bilans):
                        bilans.append(id_bilan)
        bilansMessage = ''
        for id_bilan in bilans:
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'bilans', 'id_bilan', id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                bilanName = query_my.value(1)
                bilanLabel = query_my.value(2)
            bilansMessage = utils_functions.u(
                '{0}{1} [{2}]\n').format(bilansMessage, bilanName, bilanLabel)
        # on demande s'il faut supprimer les liens items-bilan2 :
        detailsMessage = utils_functions.u('ITEMS :\n{0}\n\nBILANS :\n{1}').format(itemsMessage, bilansMessage)
        if len(items) + len(bilans) > 0:
            headMessage = QtWidgets.QApplication.translate(
                'main', 
                '<p><b>Do you want to remove the links between '
                '<br/>{0} items and {1} balances?</b></p>').format(len(items), len(bilans))
            messageBox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, 
                                            utils.PROGNAME, headMessage, 
                                            QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, 
                                            main)
            messageBox.setDetailedText(detailsMessage)
            messageBox.setMinimumWidth(400)
            # si la réponse est oui, on supprime les liens items-bilan :
            if messageBox.exec_() == QtWidgets.QMessageBox.Yes:
                for id_item in items:
                    for id_bilan in bilans:
                        commandLine_my = utils_db.qdf_prof_itemBilan3.format(id_item, id_bilan)
                        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    elif what == 'linkItemBilan':
        # on cherche la liste des bilans liés au bilan id2 :
        idToAffect = [id2]
        paths = createPathsList(main)[0]
        for path in paths :
            if id2 in path:
                for id_bilan in path[path.index(id2) + 1:]:
                    if not(id_bilan in idToAffect):
                        idToAffect.append(id_bilan)
        # et on relie l'item id1 aux bilans de la liste :
        lines = []
        for id_bilan in idToAffect:
            # on efface l'éventuelle précédente liaison :
            commandLine_my = utils_db.qdf_prof_itemBilan3.format(id1, id_bilan)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            # on relie l'item au bilan2 :
            lines.append((id1, id_bilan, coeff))
        query_my = utils_db.queryExecute(
            {utils_db.insertInto('item_bilan'): lines}, query=query_my)
    elif what == 'unlinkItemBilan':
        # on récupère la liste et les détails des bilans liés au bilan id2 :
        bilans = []
        paths = createPathsList(main)[0]
        for path in paths :
            if id2 in path:
                for id_bilan in path[path.index(id2) + 1:]:
                    if not(id_bilan in bilans):
                        bilans.append(id_bilan)
        mustDo = True
        if withMessage:
            bilansMessage = ''
            for id_bilan in bilans:
                commandLine_my = utils_db.q_selectAllFromWhere.format(
                    'bilans', 'id_bilan', id_bilan)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    bilanName = query_my.value(1)
                    bilanLabel = query_my.value(2)
                bilansMessage = utils_functions.u(
                    '{0}{1} [{2}]\n').format(bilansMessage, bilanName, bilanLabel)
            mustDo = False
            # on demande s'il faut supprimer les liens avec les bilans de la liste :
            detailsMessage = utils_functions.u('BILANS :\n{0}').format(bilansMessage)
            if len(bilans) > 0:
                headMessage = QtWidgets.QApplication.translate(
                    'main', 
                    '<p><b>Do you want to remove the links with '
                    '<br/>{0} other balances?</b></p>').format(len(bilans))
                messageBox = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Question, 
                                                utils.PROGNAME, headMessage, 
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, 
                                                main)
                messageBox.setDetailedText(detailsMessage)
                messageBox.setMinimumWidth(400)
                # si la réponse est oui, on supprime les liens items-bilan :
                if messageBox.exec_() == QtWidgets.QMessageBox.Yes:
                    mustDo = True
        if mustDo:
            for id_bilan in bilans:
                commandLine_my = utils_db.qdf_prof_itemBilan3.format(id1, id_bilan)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # dans tous les cas, on efface le lien id1 -> id2 :
        commandLine_my = utils_db.qdf_prof_itemBilan3.format(id1, id2)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)

def verificationTable(main, table='item_bilan'):
    """
    Vérifie et répare une table.
    Créé pour réparer item_bilan en cas de doublons.
    """
    query_my = utils_db.query(main.db_my)
    if table == 'item_bilan':
        commandLine_my = ('SELECT COUNT(*) AS NBR_DOUBLES, id_item, id_bilan '
                            'FROM   item_bilan '
                            'GROUP  BY id_item, id_bilan '
                            'HAVING COUNT(*) > 1 '
                            'ORDER BY id_bilan, id_item')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        if query_my.first():
            dico, liste = {}, []
            # on récupère :
            commandLine_my = utils_db.q_selectAllFrom.format('item_bilan')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                try:
                    id_item = int(query_my.value(0))
                    id_bilan = int(query_my.value(1))
                    coeff = int(query_my.value(2))
                    if (id_item, id_bilan) in dico:
                        liste.append((id_item, id_bilan, coeff))
                    dico[(id_item, id_bilan)] = coeff
                except:
                    pass
            # on vide :
            commandLine_my = utils_db.qdf_table.format('item_bilan')
            query_my = utils_db.queryExecute(
                commandLine_my, query=query_my)
            # on remplit :
            commandLine_my = utils_db.insertInto('item_bilan')
            lines = []
            for (id_item, id_bilan) in dico:
                lines.append((id_item, id_bilan, dico[(id_item, id_bilan)]))
            query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
            main.changeDBMyState()



###########################################################"
#   CLASSES POUR L'AFFICHAGE DES CPT DU BULLETIN ET AUTRES
###########################################################"

class CptTreeItem(object):
    def __init__(self, id, data, parent=None):
        self.parentItem = parent
        self.itemData = data
        self.id = id
        self.childItems = []

    def appendChild(self, item):
        self.childItems.append(item)

    def child(self, row):
        return self.childItems[row]

    def childCount(self):
        return len(self.childItems)

    def columnCount(self):
        return len(self.itemData)

    def data(self, column):
        try:
            return self.itemData[column]
        except IndexError:
            return None

    def parent(self):
        return self.parentItem

    def row(self):
        if self.parentItem:
            return self.parentItem.childItems.index(self)
        return 0

    def lastChilds(self):
        """
        retourne la liste des enfants extrémités
        """
        result = []
        if len(self.childItems) < 1:
            result.append(self)
        else:
            for aChild in self.childItems:
                result.extend(aChild.lastChilds())
        return result

class CptTreeModel(QtCore.QAbstractItemModel):
    def __init__(self, parent=None, table='bulletin', title=('Name', 'Label', 'classeType', 'id')):
        super(CptTreeModel, self).__init__(parent)
        self.main = parent
        self.rootItem = CptTreeItem(-1, title)
        self.setupModel(table, self.rootItem)
        self.itemId = -1

    def columnCount(self, parent):
        return 4

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid():
            return None
        if role == QtCore.Qt.DisplayRole:
            item = index.internalPointer()
            self.itemId = item.id
            return item.data(index.column())
        elif role == QtCore.Qt.UserRole:
            item = index.internalPointer()
            return item
        else:
            return None

    def flags(self, index):
        if not index.isValid():
            return QtCore.Qt.NoItemFlags
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def headerData(self, section, orientation, role):
        if orientation == QtCore.Qt.Horizontal and role == QtCore.Qt.DisplayRole:
            return self.rootItem.data(section)
        return None

    def index(self, row, column, parent):
        if not self.hasIndex(row, column, parent):
            return QtCore.QModelIndex()
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        childItem = parentItem.child(row)
        if childItem:
            return self.createIndex(row, column, childItem)
        else:
            return QtCore.QModelIndex()

    def parent(self, index):
        if not index.isValid():
            return QtCore.QModelIndex()
        childItem = index.internalPointer()
        parentItem = childItem.parent()
        if parentItem == self.rootItem:
            return QtCore.QModelIndex()
        return self.createIndex(parentItem.row(), 0, parentItem)

    def rowCount(self, parent):
        if parent.column() > 0:
            return 0
        if not parent.isValid():
            parentItem = self.rootItem
        else:
            parentItem = parent.internalPointer()
        return parentItem.childCount()

    def setupModel(self, table, parent):
        parents = [parent]
        indentations = [0]

        commandLine = utils_db.q_selectAllFromOrder.format(table, 'ordre')
        query = utils_db.queryExecute(commandLine, db=self.main.db_commun)
        while query.next():
            id = -1
            id_cpt = int(query.value(0))
            code = query.value(1)
            titre1 = query.value(2)
            titre2 = query.value(3)
            titre3 = query.value(4)
            cpt = query.value(5)
            classeType = utils_functions.classType2Name(self.main, int(query.value(11)))
            if titre1 != '':
                position = 0
                lineData = [code + ' : ' + titre1, '', classeType, id_cpt]
            elif titre2 != '':
                position = 1
                lineData = [code + ' : ' + titre2, '', classeType, id_cpt]
            elif titre3 != '':
                position = 2
                lineData = [code + ' : ' + titre3, '', classeType, id_cpt]
            else:
                position = 3
                lineData = [code, cpt, classeType, id_cpt]
                id = id_cpt

            if position > indentations[-1]:
                # The last child of the current parent is now the new
                # parent unless the current parent has no children.
                if parents[-1].childCount() > 0:
                    parents.append(parents[-1].child(parents[-1].childCount() - 1))
                    indentations.append(position)
            else:
                while position < indentations[-1] and len(parents) > 0:
                    parents.pop()
                    indentations.pop()
            # Append a new item to the current parent's list of children.
            parents[-1].appendChild(CptTreeItem(id, lineData, parents[-1]))


