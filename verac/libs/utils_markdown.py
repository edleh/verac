# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    gestion de l'aide contextuelle affichée dans l'interface admin.
    Il y a :
        * un menu (placé à gauche) sous forme de treeview
        * une zone d'affichage des pages d'aide (webview)
        * un bouton pour lancer des actions
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_webengine, utils_filesdirs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



PIXEL_SIZE = 20



class MarkdownHelpViewer(QtWidgets.QAbstractScrollArea):
    """
    Il contient :
        un WebView dont les liens sont désactivés
    """
    def __init__(self, parent=None, mdFile='', nbLines=-1, linksInBrowser=True):
        """
        initialisation
        """
        super(MarkdownHelpViewer, self).__init__(parent)
        self.main = parent

        self.mdWebView = utils_webengine.MyWebEngineView(
            self, linksInBrowser=linksInBrowser)

        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.mdWebView)
        self.setLayout(vBoxLayout)
        if mdFile != '':
            self.openMdFile(mdFile, nbLines=nbLines)

    def openMdFile(self, mdFile, nbLines=-1, args=()):
        """
        ouverture d'un fichier mardown
        * mdFile : nom du fichier
        * nbLines : permet de calculer la hauteur en fonction 
          du nombre de lignes du fichier :
            -2 : pas de max (pour que la hauteur soit maximale)
            -1 : min et max dépendent de nbLines
            valeur imposée : dans ce cas la heuteur sera fixe (min = max)
        * args : une liste d'arguments peut être passée, 
          si le fichier contient des {i}, pour utiliser
          la fonction format (le remplacement se fera dans md2html)
        """
        mdFile = utils_functions.doLocale(
            utils.LOCALE, mdFile, '.md')
        if nbLines == -3:
            nbLines = self.countLinesInMdFile(mdFile)
            maxHeight = (2 + nbLines) * PIXEL_SIZE
            self.setMinimumHeight(maxHeight)
            self.setMaximumHeight(maxHeight)
        elif nbLines == -2:
            # on met la valeur par défaut au max
            nbLines = self.countLinesInMdFile(mdFile)
            maxHeight = (2 + nbLines) * PIXEL_SIZE
            self.setMinimumHeight(maxHeight // 2)
            self.setMaximumHeight(16777215)
        elif nbLines == -1:
            nbLines = self.countLinesInMdFile(mdFile)
            maxHeight = (2 + nbLines) * PIXEL_SIZE
            self.setMinimumHeight(maxHeight // 2)
            self.setMaximumHeight(maxHeight)
        else:
            maxHeight = (2 + nbLines) * PIXEL_SIZE
            self.setMinimumHeight(maxHeight)
            self.setMaximumHeight(maxHeight)
        #print('openMdFile:', nbLines, maxHeight, self.height())
        outFileName = md2html(
            self.main, mdFile, args=args, template='makdownwebview')
        url = QtCore.QUrl().fromLocalFile(outFileName)
        self.mdWebView.load(url)

    def countLinesInMdFile(self, fileName):
        """
        calcule le nombre de lignes à prévoir dans un fichier Markdown.
        """
        result = 1
        mdFile = QtCore.QFile(fileName)
        if mdFile.open(QtCore.QFile.ReadOnly):
            stream = QtCore.QTextStream(mdFile)
            stream.setCodec('UTF-8')
            content = stream.readAll()
            mdFile.close()
            result += content.count('\n')
        return result




def md2html(main, mdFile, args=(), template='default', replace=False):
    """
    retourne un fichier html d'après un fichier md (markdown).
    Les dossiers main.beginDir et main.tempPath
    doivent être définis.
    """
    # fichier final :
    outFileName = utils_functions.u(
        '{0}/md/{1}.html').format(
            main.tempPath, 
            QtCore.QFileInfo(mdFile).baseName())
    # s'il existe déjà, rien à faire de plus :
    if len(args) < 1:
        if QtCore.QFileInfo(outFileName).exists():
            if not(replace):
                return outFileName
    # fichier du modèle html à utiliser :
    templateFile = utils_functions.u(
        '{0}/md/{1}.html').format(main.tempPath, template)
    # si on ne l'a pas trouvé, c'est qu'on lance pour la première fois.
    # on recopie alors le dossier md dans temp :
    if not(QtCore.QFileInfo(templateFile).exists()):
        utils_filesdirs.createDirs(main.tempPath, 'md')
        scrDir = main.beginDir + '/files/md'
        destDir = main.tempPath + '/md'
        utils_filesdirs.copyDir(scrDir, destDir)
    # récupération du contenu du modèle html :
    htmlLines = ''
    inFile = QtCore.QFile(templateFile)
    if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(inFile)
        stream.setCodec('UTF-8')
        htmlLines = stream.readAll()
        inFile.close()
    # récupération du fichier Markdown :
    mdLines = ''
    inFile = QtCore.QFile(mdFile)
    if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(inFile)
        stream.setCodec('UTF-8')
        mdLines = stream.readAll()
        inFile.close()
    if len(args) > 0:
        mdLines = utils_functions.u(mdLines).format(*args)
    # on met en forme et on remplace le repère du modèle :
    mdLines = mdLines.replace('\n', '\\n').replace("'", "\\'")
    htmlLines = htmlLines.replace('# USER TEXT', mdLines)
    # on enregistre le nouveau fichier html :
    outFile = QtCore.QFile(outFileName)
    if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(outFile)
        stream.setCodec('UTF-8')
        stream << htmlLines
        outFile.close()
    return outFileName




