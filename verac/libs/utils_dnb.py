# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
   CONFIGURATION DU DNB ET DE L'EXPORT NOTANET.
   CE FICHIER RISQUE D'ÊTRE À ADAPTER CHAQUE ANNÉE...

   https://foad.orion.education.fr/
    Travail collaboratif
    editeurs
    NOTANET
    Documentation
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtWidgets
else:
    from PyQt4 import QtGui as QtWidgets



"""
UNE LISTE DE TRADUCTIONS
"""

traductionsDNB = {
    # pour les matières (notes particulières autorisées dans le jargon NOTANET) :
    'AB': QtWidgets.QApplication.translate('main', 'DNB_CODE-AB'),
    'DI': QtWidgets.QApplication.translate('main', 'DNB_CODE-DI'),
    'NN': QtWidgets.QApplication.translate('main', 'DNB_CODE-NN'), # obsolète ?
    # pour la validation du socle :
    'MS': QtWidgets.QApplication.translate('main', 'DNB_CODE-MS'),
    'ME': QtWidgets.QApplication.translate('main', 'DNB_CODE-ME'),
    'MN': QtWidgets.QApplication.translate('main', 'DNB_CODE-MN'),
    # pour le niveau A2 :
    'VA': QtWidgets.QApplication.translate('main', 'DNB_CODE-VA'),
    'NV': QtWidgets.QApplication.translate('main', 'DNB_CODE-NV'),

    # les noms affichés des matières dans les fiches DNB :
    'FRANCAIS': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-FRANCAIS'),
    'MATHS': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-MATHS'),
    'LV1': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-LV1'),
    'SVT': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-SVT'),
    'PHYSIQUE': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-PHYSIQUE'),
    'EPS': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-EPS'),
    'ARTS_P': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-ARTS_P'),
    'MUSIQ': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-MUSIQ'),
    'TECHNO': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-TECHNO'),
    'LV2': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-LV2'),
    'VIE_SCOL': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-VIE_SCOL'),
    'OPTION': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-OPTION'),
    'SOCLE_B2I': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-SOCLE_B2I'), # obsolète ?
    'HG': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-HG'),
    'EDUC_CIVIQUE': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-EDUC_CIVIQUE'),
    'SOCLE_A2': QtWidgets.QApplication.translate('main', 'DNB_MATIERE-SOCLE_A2'),
    }


"""
LISTE DES MATIÈRES DU DNB ET NOTANET.
À vérifier chaque année !!!

id_matiere : code matière interne à NOTANET
Dans la liste :
    libellé de la matière
    mode de calcul
    nombre de points
    tuple des notes particulières autorisées
    liste des id équivalents à cette matière dans la table commun.matieres
"""
listeMatieresDNB = {
    101: [traductionsDNB['FRANCAIS'], 'POINTS', 20, ('AB', ), ['FRA']], 
    102: [traductionsDNB['MATHS'], 'POINTS', 20, ('AB', ), ['MATH']], 
    103: [traductionsDNB['LV1'], 'POINTS', 20, ('AB', 'DI', ), ['ANG']], 
    104: [traductionsDNB['SVT'], 'POINTS', 20, ('AB', 'DI', ), ['SVT']], 
    105: [traductionsDNB['PHYSIQUE'], 'POINTS', 20, ('AB', 'DI', ), ['PHY']], 
    106: [traductionsDNB['EPS'], 'POINTS', 20, ('AB', 'DI', ), ['EPS']], 
    107: [traductionsDNB['ARTS_P'], 'POINTS', 20, ('AB', 'DI', ), ['AP']], 
    108: [traductionsDNB['MUSIQ'], 'POINTS', 20, ('AB', 'DI', ), ['MUSIQ']], 
    109: [traductionsDNB['TECHNO'], 'POINTS', 20, ('AB', 'DI', ), ['TECH']], 
    110: [traductionsDNB['LV2'], 'POINTS', 20, ('AB', 'DI', ), ['ALL', 'ESP']], 
    #112: [traductionsDNB['VIE_SCOL'], 'POINTS', 20, ('AB', ), ['VS']], 

    113: [traductionsDNB['OPTION'], 'PTSUP', 10, ('AB', 'DI', ), ['LAT', 'ODP']], 

    #114: [traductionsDNB['SOCLE_B2I'], '' 0, ('AB', 'MS', 'ME', 'MN', ), []], 
    #115: [traductionsDNB['SOCLE_A2'], '', 0, ('AB', 'MS', 'ME', ), []], 
    121: [traductionsDNB['HG'], 'NOTNONCA', 20, ('AB', ), ['HG']], 
    122: [traductionsDNB['EDUC_CIVIQUE'], 'NOTNONCA', 20, ('AB', ), ['EC']], 
    130: [traductionsDNB['SOCLE_A2'], '', 0, ('AB', 'VA', 'NV', ), []], 
    }

# les matières pour lesquelles il faudra préciser le nom (LV, option) :
mustAddMatiereName = (103, 110, 113)




"""
POUR L'INTERFACE DE VALIDATIONS PAR LE CHEF D'ÉTABLISSEMENT.
(procédure prof.calcDNB())
"""
TOOLTIP_SOCLE = utils_functions.u(
    '  {0}->AB ({1})\n  {2}->MS ({3})\n  {4}->ME ({5})\n  {6}->MN ({7})').format(
        QtWidgets.QApplication.translate('main', 'EMPTY'), traductionsDNB['AB'], 
        utils_functions.valeur2affichage('AB'), traductionsDNB['MS'], 
        utils_functions.valeur2affichage('CD'), traductionsDNB['ME'], 
        utils_functions.valeur2affichage('X'), traductionsDNB['MN'])

TOOLTIP_A2 = utils_functions.u(
    '  {0}->AB ({1})\n  {2}->VA ({3})\n  {4}->NV ({5})').format(
        QtWidgets.QApplication.translate('main', 'EMPTY'), traductionsDNB['AB'], 
        utils_functions.valeur2affichage('AB'), traductionsDNB['VA'], 
        utils_functions.valeur2affichage('CD'), traductionsDNB['NV'])

OPINION_A = QtWidgets.QApplication.translate('main', 'VeryFavorable')
OPINION_B = QtWidgets.QApplication.translate('main', 'Favorable')
OPINION_C = QtWidgets.QApplication.translate('main', 'MustProve')
OPINION_D = QtWidgets.QApplication.translate('main', 'Unfavorable')
TOOLTIP_OPINION = utils_functions.u(
    '  {0}->{1}\n  {2}->{3}\n  {4}->{5}\n  {6}->{7}').format(
        utils.affichages['A'][0], OPINION_A, 
        utils.affichages['B'][0], OPINION_B, 
        utils.affichages['C'][0], OPINION_C, 
        utils.affichages['D'][0], OPINION_D)




def arrondirAuDemiPointSuperieur(nombre):
    """
    J'ai du mal à appeler cela arrondir mais c'est comme ça pour les examens.
    """
    partie_entiere = int(nombre)
    partie_decimale = nombre - partie_entiere
    if partie_decimale > 0.5:
        return partie_entiere + 1
    elif partie_decimale > 0:
        return partie_entiere + 0.5
    else:
        return partie_entiere







"""
POUR LA FABRICATION DES FICHES BREVET
(mise en forme des remplacement dans le modèle)
"""

# matières de type "POINTS" (id < 113) sauf les LV :
""" VERSION AVEC POINTS DNB (SUPPRIMÉ EN 2013)
htmlMatierePoints = (
    '    <tr>\n'
    '        <td class="matiere"><b>{0}</b></td>\n'
    '        <td class="valeur">{1}</td>\n'
    '        <td class="valeur">{2}</td>\n'
    '        <td class="appreciation">\n'
    '            {3}\n'
    '        </td>\n'
    '        <td class="valeur">{4}</td>\n'
    '    </tr>\n')
"""
htmlMatierePoints = (
    '    <tr>\n'
    '        <td class="matiere"><b>{0}</b></td>\n'
    '        <td class="valeur">{1}</td>\n'
    '        <td class="valeur">{2}</td>\n'
    '        <td class="appreciation">\n'
    '            {3}\n'
    '        </td>\n'
    '    </tr>\n')

# LV (on doit préciser) :
htmlMatiereWithName = (
    '    <tr>\n'
    '        <td class="matiere"><b>{0}</b> ({1})</td>\n'
    '        <td class="valeur">{2}</td>\n'
    '        <td class="valeur">{3}</td>\n'
    '        <td class="appreciation">\n'
    '            {4}\n'
    '        </td>\n'
    '    </tr>\n')


# option (Latin, ODP) "PTSUP" (id = 113) :
POINTS_SUP_TEXT = QtWidgets.QApplication.translate('main', 'extra points')
""" VERSION AVEC POINTS DNB (SUPPRIMÉ EN 2013)
htmlMatierePtSup = (
    '    <tr>\n'
    '        <td class="matiere"><b>{0}</b><br/>({1})</td>\n'
    '        <td class="valeur">{2}</td>\n'
    '        <td class="valeur">{3}</td>\n'
    '        <td class="appreciation">\n'
    '            {4}\n'
    '        </td>\n'
    '        <td class="valeur">{5}<br/>{6}</td>\n'
    '    </tr>\n')
"""
htmlMatierePtSup = (
    '    <tr>\n'
    '        <td class="matiere"><b>{0}</b><br/>({1})</td>\n'
    '        <td class="valeur">{2}</td>\n'
    '        <td class="valeur">{3}</td>\n'
    '        <td class="appreciation">\n'
    '            {4}\n'
    '        </td>\n'
    '    </tr>\n')

# matières qui ne comptent pas "NOTNONCA" :
htmlMatiereNotNonCa = (
    '    <tr>\n'
    '        <td class="matiere"><b>{0}</b><br/>(pour information)</td>\n'
    '        <td class="valeur">{1}</td>\n'
    '        <td class="valeur">{2}</td>\n'
    '        <td class="appreciation">\n'
    '            {3}\n'
    '        </td>\n'
    '    </tr>\n')

# Niveau A2
NIVEAU_A2_TEXT = QtWidgets.QApplication.translate('main', 'NIVEAU_A2_TEXT')
htmlNiveauA2 = (
    '    <tr>\n'
    '        <td colspan="3" class="matiere"><b>{0}</b></td>\n'
    '        <td class="valeur">{1}</td>\n'
    '    </tr>\n')

# taille limite des appréciations (pour n'avoir qu'une page) :
LIMITE_APPRECIATIONS = 140





