# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
Variables, classes et fonctions utilisées dans l'interface admin.
"""

# importation des modules utiles :
from __future__ import division, print_function

import utils, utils_functions, utils_db, utils_filesdirs, utils_2lists
import admin_edit, admin_docs, admin_ftp

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
else:
    from PyQt4 import QtCore, QtGui as QtWidgets



###########################################################"
#   VARIABLES GLOBALES
###########################################################"

# des listes globales manipulées lors des calculs des résultats :

# PROFS pour récupérer la liste des profs existants
# remplie depuis la table admin.profs (dans initConfig et createTableFromCsv)
# chaque élément est de la forme (id, file, nom, prenom)
PROFS = []

# MATIERES contiendra les listes des matières (du bulletin et spéciales)
# ainsi que des dico permettant de passer du nom au code et réciproquement.
# Obtenu en lisant la table commun.matieres (dans initFromCommun).
MATIERES = {
    'BLT': [], 
    'BLTSpeciales': [], 
    'OTHERS': [], 
    'Matiere2Code': {}, 
    'Code2Matiere': {}, 
    }

# Classes verrouillées :
LOCKED_CLASSES = {'LOADED': False, 'ORDER': [], 'LOCKED': [], }



###########################################################"
#   VARIABLES GLOBALES CHARGÉES DEPUIS LA BASE ADMIN
#   (voir initConfig plus loin)
###########################################################"


# chemins des sous-dossiers :
adminDir, csvDir, modelesDir = '', '', ''
# variables de chemin vers les répertoires public, secret et privé :
dirSitePublic, dirSiteSecret, dirSitePrive = '', '', ''
# miroir local du ftp
dirLocalFtp, dirLocalPublic, dirLocalPrive = '', '', ''

# miroir local et préfixe des fichiers des profs:
dirLocalProfxx, prefixProfFiles = '', ''

# en cas de réseau, dossier files (fichiers des profs) :
dirLanProfxx = ''

# pour savoir quelles sont les périodes qui sont archivées :
archivedPeriode = []

# listeSousRubriques
# pour avoir la liste des sous-rubriques de la partie partagée du bulletin.
# Pour les tableaux de synthèses et le radar dans l'interface web.
# Chargé depuis la base commun.
# utilisé dans les procédures createResultats
listeSousRubriques = {'bulletin': [], 'referentiel': [], 'confidentiel': []}
elevesNotes, classesNotes = {}, {}

# les bases gérées par l'admin:
dbFile_admin, dbFileTemp_admin, db_admin = '', '', None
dbFile_recupEvals, dbFileTemp_recupEvals, db_recupEvals = '', '', None
dbFile_referentialPropositions, dbFileTemp_referentialPropositions, db_referentialPropositions = '', '', None
dbFile_referentialValidations, dbFileTemp_referentialValidations, db_referentialValidations = '', '', None
dbFile_documents, dbFileTemp_documents, db_documents = '', '', None
dbFile_structures, dbFileTemp_structures, db_structures = '', '', None
dbFile_compteur, dbFileTemp_compteur, db_compteur = '', '', None
dbFile_configweb, dbFileTemp_configweb, db_configweb = '', '', None
dbFileFtp_commun = ''
dbFileFtp_users = ''
# dépendant de la période sélectionnée :
dbFile_resultats, dbFileTemp_resultats, db_resultats = '', '', None
dbFile_commun = ''

def initConfig(main):
    global adminDir, csvDir, modelesDir
    global dirSitePublic, dirSiteSecret, dirSitePrive
    global dirLocalFtp, dirLocalPublic, dirLocalPrive
    global PROFS, dirLocalProfxx, prefixProfFiles, dirLanProfxx
    global dbFile_admin, dbFileTemp_admin, db_admin
    global dbFile_recupEvals, dbFileTemp_recupEvals
    global dbFile_documents, dbFileTemp_documents
    global dbFile_structures, dbFileTemp_structures
    global dbFile_compteur, dbFileTemp_compteur
    global dbFile_configweb, dbFileTemp_configweb
    global dbFile_referentialPropositions, dbFileTemp_referentialPropositions
    global dbFile_referentialValidations, dbFileTemp_referentialValidations
    global dbFileFtp_commun
    global dbFileFtp_users

    utils.changeShowLog(True)
    maintenant = QtCore.QDateTime.currentDateTime().toString('yyyy-MM-dd hh:mm')
    utils_functions.afficheMessage(
        main, 
        [QtWidgets.QApplication.translate('main', 'MESSAGES WINDOW'), maintenant], 
        tags=('h1'))

    # d'abord on récupère le dossier admin :
    adminDir = utils_db.readInConfigTable(main.db_localConfig, main.actualVersion['versionName'])[1]
    # Si on lance une archive, le dossier verac_admin est à côté du logiciel :
    theDir = main.beginDir + '/../verac_admin'
    if (utils.installType == utils.INSTALLTYPE_ARCHIVE) and (QtCore.QDir(theDir).exists()):
        adminDir = theDir

    csvDir = adminDir + '/csv'
    modelesDir = adminDir + '/modeles'
    utils_filesdirs.createDirs(adminDir, 'fichiers/pdf')
    utils_filesdirs.createDirs(adminDir, 'fichiers/xml')

    # puis on définit et ouvre la base admin :
    dbFile_admin = adminDir + '/admin.sqlite'
    dbFileTemp_admin = main.tempPath + '/admin.sqlite'
    utils_filesdirs.removeAndCopy(dbFile_admin, dbFileTemp_admin)
    db_admin = utils_db.createConnection(main, dbFileTemp_admin)[0]

    # variables de connexion ftp :
    ftpHost = utils_db.readInConfigTable(db_admin, 'baseSiteFtp', 'config_admin')[1]
    ftpUser = utils_db.readInConfigTable(db_admin, 'userFtp', 'config_admin')[1]
    ftpPass = utils_db.readInConfigTable(db_admin, 'mdpFtp', 'config_admin')[1]
    ftpPort = utils_db.readInConfigTable(db_admin, 'ftpPort', 'config_admin', default_int=21)[0]
    admin_ftp.ftpUpdateConfig(newFtpHost=ftpHost, newFtpUser=ftpUser, newFtpPass=ftpPass, newFtpPort=ftpPort)
    # variables de chemin vers les répertoires 'public', 'secret' et 'privé' :
    dirSitePublic = utils_db.readInConfigTable(db_admin, 'dirSitePublic', 'config_admin')[1]
    dirSiteSecret = utils_db.readInConfigTable(db_admin, 'dirSiteSecret', 'config_admin')[1]
    dirSitePrive = dirSiteSecret + '/verac'
    # miroir local du ftp :
    dirLocalFtp = adminDir + '/ftp'
    dirLocalPublic = dirLocalFtp + '/verac'
    dirLocalPrive = dirLocalFtp + '/secret/verac'

    # miroir local et préfixe des fichiers des profs :
    dirLocalProfxx = dirLocalPrive + '/up/files/'
    prefixProfFiles = utils_db.readInConfigTable(
        main.db_commun, 'prefixProfFiles', 'config')[1]

    # en cas de réseau, dossier files (fichiers des profs) :
    dirLanProfxx = utils.lanFilesDir
    if dirLanProfxx != '':
        dirLanProfxx = utils_functions.addSlash(dirLanProfxx)

    # on remplit la liste des profs :
    if main.actualVersion['versionName'] != 'perso':
        query_admin = utils_db.query(db_admin)
        commandLine_admin = utils_db.q_selectAllFrom.format('profs')
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            prof_id = int(record[0])
            prof_file = utils_functions.u('{0}{1}').format(prefixProfFiles, prof_id)
            prof_nom = record[2]
            prof_prenom = record[3]
            utils_functions.appendUnique(
                PROFS, 
                (prof_id, prof_file, prof_nom, prof_prenom))
    else:
        utils_functions.appendUnique(PROFS, (1, utils_functions.u('persoprof1'), '', ''))

    # les autres bases gérées par l'admin :
    # (recup_evals, referential_propositions, documents, etc)
    dbFile_recupEvals = adminDir + '/recup_evals.sqlite'
    dbFileTemp_recupEvals = main.tempPath + '/recup_evals.sqlite'
    dbFile_referentialPropositions = dirLocalPrive + '/protected/referential_propositions.sqlite'
    dbFileTemp_referentialPropositions = main.tempPath + '/referential_propositions.sqlite'
    dbFile_referentialValidations = dirLocalPrive + '/protected/referential_validations.sqlite'
    dbFileTemp_referentialValidations = main.tempPath + '/referential_validations.sqlite'
    dbFile_documents = dirLocalPrive + '/protected/documents.sqlite'
    dbFileTemp_documents = main.tempPath + '/documents.sqlite'
    dbFile_structures = dirLocalPrive + '/protected/structures/structures.sqlite'
    dbFileTemp_structures = main.tempPath + '/structures.sqlite'
    dbFile_compteur = dirLocalPrive + '/public/compteur.sqlite'
    dbFileTemp_compteur = main.tempPath + '/compteur.sqlite'
    dbFile_configweb = dirLocalPublic + '/_private/configweb.sqlite'
    dbFileTemp_configweb = main.tempPath + '/configweb.sqlite'
    # on vérifie leur existence (sinon on les crée) :
    createIfNotExistsDBs(main)
    # recup_evals et referential_propositions sont automatiquement recopiées dans temp
    # (les autres sont recopiées dans temp et ouvertes seulement si besoin)
    utils_filesdirs.removeAndCopy(dbFile_recupEvals, dbFileTemp_recupEvals)
    utils_filesdirs.removeAndCopy(
        dbFile_referentialPropositions, dbFileTemp_referentialPropositions)
    # mirroirs de commun et users dans le FTP :
    dbFileFtp_commun = dirLocalPrive + '/public/commun.sqlite'
    dbFileFtp_users = dirLocalPrive + '/public/users.sqlite'

    # on initialise les périodes :
    initPeriodesState(main)

    # test de l'état du ftp :
    admin_ftp.ftpTest(main, askPassword=False)

def createIfNotExistsDBs(main):
    """
    On vérifie si les bases de données existent bien.
    Sinon, on les crée.
    """
    # base recup_evals :
    if not(QtCore.QFile(dbFile_recupEvals).exists()):
        global db_recupEvals
        db_recupEvals = utils_db.createConnection(main, dbFileTemp_recupEvals)[0]
        try:
            query, transactionOK = utils_db.queryTransactionDB(db_recupEvals)
            listCommands = [
                utils_db.qct_recupEvals_absences,
                utils_db.qct_recupEvals_appreciations,
                utils_db.qct_recupEvals_bilans,
                utils_db.qct_config, 
                utils_db.qct_recupEvals_dnbValues, 
                utils_db.qct_recupEvals_eleveGroupe,
                utils_db.qct_recupEvals_eleveProf,
                utils_db.qct_recupEvals_filesprofconfig,
                utils_db.qct_recupEvals_lsu,
                utils_db.qct_recupEvals_notes,
                utils_db.qct_recupEvals_persos]
            query = utils_db.queryExecute(listCommands, query=query)
            commandLine = utils_db.insertInto('config', 3)
            listArgs = ('versionDB', utils.VERSIONDB_RECUPEVALS, '')
            query = utils_db.queryExecute(
                {commandLine: listArgs}, query=query)
        finally:
            utils_db.endTransaction(query, db_recupEvals, transactionOK)
            utils_filesdirs.removeAndCopy(dbFileTemp_recupEvals, dbFile_recupEvals)
    # base referential_propositions
    # on gère aussi le renommage de validations en referential_propositions :
    oldFile = dirLocalPrive + '/protected/validations.sqlite'
    if QtCore.QFile(oldFile).exists():
        if utils_filesdirs.removeAndCopy(oldFile, dbFile_referentialPropositions):
            QtCore.QFile(oldFile).remove()
    if not(QtCore.QFile(dbFile_referentialPropositions).exists()):
        global db_referentialPropositions
        db_referentialPropositions = utils_db.createConnection(
            main, dbFileTemp_referentialPropositions)[0]
        try:
            query, transactionOK = utils_db.queryTransactionDB(
                db_referentialPropositions)
            listCommands = [
                utils_db.qct_config, 
                utils_db.qct_referentialPropositions_bilansDetails,
                utils_db.qct_referentialPropositions_bilansPropositions,
                utils_db.qct_referentialPropositions_eleves]
            query = utils_db.queryExecute(listCommands, query=query)
            commandLine = utils_db.insertInto('config', 3)
            listArgs = ('versionDB', utils.VERSIONDB_PROPOSITIONS, '')
            query = utils_db.queryExecute(
                {commandLine: listArgs}, query=query)
        finally:
            utils_db.endTransaction(
                query, db_referentialPropositions, transactionOK)
            utils_filesdirs.removeAndCopy(
                dbFileTemp_referentialPropositions, dbFile_referentialPropositions)
    # base referential_validations
    if not(QtCore.QFile(dbFile_referentialValidations).exists()):
        mustDo = True
        # on essaye d'abord de la télécharger :
        try:
            downloadDB(main, db='referential_validations', msgFin=False)
            if QtCore.QFileInfo(dbFile_referentialValidations).size() < 10:
                QtCore.QFile(dbFile_referentialValidations).remove()
            else:
                mustDo = False
        except:
            pass
        if mustDo:
            global db_referentialValidations
            db_referentialValidations = utils_db.createConnection(
                main, dbFileTemp_referentialValidations)[0]
            try:
                query, transactionOK = utils_db.queryTransactionDB(
                    db_referentialValidations)
                listCommands = [
                    utils_db.qct_config, 
                    utils_db.qct_referentialValidations_bilansValidations]
                query = utils_db.queryExecute(listCommands, query=query)
                commandLine = utils_db.insertInto('config', 3)
                listArgs = ('versionDB', utils.VERSIONDB_VALIDATIONS, '')
                query = utils_db.queryExecute(
                    {commandLine: listArgs}, query=query)
            finally:
                utils_db.endTransaction(
                    query, db_referentialValidations, transactionOK)
                utils_filesdirs.removeAndCopy(
                    dbFileTemp_referentialValidations, dbFile_referentialValidations)
            uploadDB(main, db='referential_validations', msgFin=False)
    # base documents :
    if not(QtCore.QFile(dbFile_documents).exists()):
        global db_documents
        db_documents = utils_db.createConnection(main, dbFileTemp_documents)[0]
        try:
            query, transactionOK = utils_db.queryTransactionDB(db_documents)
            listCommands = [
                utils_db.qct_config, 
                utils_db.qct_documents_eleves,
                utils_db.qct_documents_profs]
            query = utils_db.queryExecute(listCommands, query=query)
            commandLine = utils_db.insertInto('config', 3)
            listArgs = ('versionDB', utils.VERSIONDB_DOCUMENTS, '')
            query = utils_db.queryExecute(
                {commandLine: listArgs}, query=query)
        finally:
            utils_db.endTransaction(query, db_documents, transactionOK)
            utils_filesdirs.removeAndCopy(dbFileTemp_documents, dbFile_documents)
    # base structures :
    if not(QtCore.QFile(dbFile_structures).exists()):
        global db_structures
        if not(QtCore.QDir(dirLocalPrive + '/protected/structures').exists()):
            QtCore.QDir(dirLocalPrive + '/protected').mkdir('structures')
        db_structures = utils_db.createConnection(main, dbFileTemp_structures)[0]
        try:
            query, transactionOK = utils_db.queryTransactionDB(db_structures)
            listCommands = [
                utils_db.qct_config, utils_db.qct_structures_structures]
            query = utils_db.queryExecute(listCommands, query=query)
            commandLine = utils_db.insertInto('config', 3)
            listArgs = ('versionDB', utils.VERSIONDB_STRUCTURES, '')
            query = utils_db.queryExecute(
                {commandLine: listArgs}, query=query)
        finally:
            utils_db.endTransaction(query, db_structures, transactionOK)
            utils_filesdirs.removeAndCopy(dbFileTemp_structures, dbFile_structures)

def openDB(main, dbName):
    """
    Permet de ne copier une base dans temp que si on en a besoin.
    """
    if dbName == 'documents':
        global db_documents
        if db_documents == None:
            utils_filesdirs.removeAndCopy(dbFile_documents, dbFileTemp_documents)
            db_documents = utils_db.createConnection(main, dbFileTemp_documents)[0]
    elif dbName == 'compteur':
        global db_compteur
        if db_compteur == None:
            utils_filesdirs.removeAndCopy(dbFile_compteur, dbFileTemp_compteur)
            db_compteur = utils_db.createConnection(main, dbFileTemp_compteur)[0]
    elif dbName == 'configweb':
        global db_configweb
        if db_configweb == None:
            utils_filesdirs.removeAndCopy(
                dbFile_configweb, dbFileTemp_configweb, testSize=10)
            db_configweb = utils_db.createConnection(main, dbFileTemp_configweb)[0]
    elif dbName == 'structures':
        global db_structures
        if db_structures == None:
            utils_filesdirs.removeAndCopy(dbFile_structures, dbFileTemp_structures)
            db_structures = utils_db.createConnection(main, dbFileTemp_structures)[0]
    elif dbName == 'recup_evals':
        global db_recupEvals
        if db_recupEvals == None:
            db_recupEvals = utils_db.createConnection(main, dbFileTemp_recupEvals)[0]
    elif dbName == 'referential_propositions':
        global db_referentialPropositions
        if db_referentialPropositions == None:
            db_referentialPropositions = utils_db.createConnection(
                main, dbFileTemp_referentialPropositions)[0]
    elif dbName == 'referential_validations':
        global db_referentialValidations
        if db_referentialValidations == None:
            db_referentialValidations = utils_db.createConnection(
                main, dbFileTemp_referentialValidations)[0]
    elif dbName == 'resultats':
        global db_resultats
        if db_resultats == None:
            db_resultats = utils_db.createConnection(main, dbFileTemp_resultats)[0]


###########################################################"
#   FIN DES VARIABLES GLOBALES
###########################################################"





###########################################################"
#   CRÉATION DES TABLES
#   (dans les bases admin et commun)
###########################################################"

TABLE2CSVFILE = {
    'commun': (
        'config', 'matieres', 'classes', 'classestypes',
        'periodes', 'horaires', 
        'bulletin', 'referentiel', 'confidentiel',
        'suivi', 'sous_rubriques', 'lsu'),
    'admin': {
        'admin_only': ('config_admin', 'adresses', 'lsu'),
        'admin_and_users': ('eleves', 'profs'),
        },
    }


def editCsvFile(main, fileName):
    """
    Avant de lancer l'éditeur (dans utils_csveditor),
    on repère si le fichier csv à éditer fait partie de la base commun ou admin.
    Pour une table de la base commun, on mettra la base à jour 
        et on demandera s'il faut envoyer le fichier.
    Pour une table de la base admin, on mettra la base à jour.
    On distingue les tables uniquement dans admin
        de celles qui sont aussi dans users.
    Si c'est la table profs ou eleves, 
        on demandera AVANT s'il faut télécharger users, 
        on mettre ensuite la base users à jour
        et on demandera s'il faut l'envoyer.
    """
    # 3 dictionnaires pour gérer les tables des bases commun et admin :
    table2csvFile = {
        'commun': {},
        'admin': {
            'admin_only': {},
            'admin_and_users': {},
            },
        }
    for table in TABLE2CSVFILE['commun']:
        table2csvFile['commun'][table] = utils_functions.u(
            '{0}/commun_tables/{1}.csv').format(csvDir, table)
    for table in TABLE2CSVFILE['admin']['admin_only']:
        table2csvFile['admin']['admin_only'][table] = utils_functions.u(
            '{0}/admin_tables/{1}.csv').format(csvDir, table)
    for table in TABLE2CSVFILE['admin']['admin_and_users']:
        table2csvFile['admin']['admin_and_users'][table] = utils_functions.u(
            '{0}/admin_tables/{1}.csv').format(csvDir, table)
    # si c'est une table de la base admin (admin_and_users),
    # on propose d'abord de télécharger users :
    if fileName in table2csvFile['admin']['admin_and_users'].values():
        mustDownloadDBs(main)
    # on lance l'éditeur de fichier csv :
    import utils_csveditor
    dialog = utils_csveditor.CsvEditorDlg(parent=main, csvFileName=fileName)
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        return
    main.enableInterface(dialog, lastState)
    # on recrée la table :
    tableName = QtCore.QFileInfo(fileName).baseName()
    if fileName in table2csvFile['commun'].values():
        # si c'est une table de la base commun :
        createTableFromCsv(
            main, tableName, fileDbName='commun', mustDownUp=False, msgFin=False)
        mustUploadDBs(main, databases=['commun'])
    elif fileName in table2csvFile['admin']['admin_only'].values():
        # si c'est une table de la base admin (admin_only) :
        createTableFromCsv(main, tableName, mustDownUp=False)
    elif fileName in table2csvFile['admin']['admin_and_users'].values():
        # si c'est une table de la base admin (admin_and_users) :
        createTableFromCsv(main, tableName, mustDownUp=False)
        # mise à jour de la base users :
        updateUsersBase(main, mode=tableName, msgFin=False)
        mustUploadDBs(main, databases=['users'])

def createTableFromCsv(main, tableName='', fileDbName='admin', mustDownUp=True, msgFin=True):
    """
    Création d'une table à partir d'un fichier csv (pour les bases admin et commun).
    Si tableName est vide, on crée toutes les tables.
    fileDbName indique la base concernée ('admin' ou 'commun').
    On traite aussi la récupération et l'envoi des bases users et commun,
    sauf si mustDownUp=False (permet un appel extérieur, depuis editCsvFile par exemple).
    """
    # prefixProfFiles peut être modifié :
    global prefixProfFiles
    # si on a besoin de recréer le fichier csv à la fin 
    # (bug de changement de versionDB) :
    mustRepairCsvFile = False
    # 3 dictionnaires pour gérer les tables des bases commun et admin :
    table2csvFile = {
        'commun': {},
        'admin': {
            'admin_only': {},
            'admin_and_users': {},
            },
        }
    for table in TABLE2CSVFILE['commun']:
        table2csvFile['commun'][table] = utils_functions.u(
            '{0}/commun_tables/{1}.csv').format(csvDir, table)
    for table in TABLE2CSVFILE['admin']['admin_only']:
        table2csvFile['admin']['admin_only'][table] = utils_functions.u(
            '{0}/admin_tables/{1}.csv').format(csvDir, table)
    for table in TABLE2CSVFILE['admin']['admin_and_users']:
        table2csvFile['admin']['admin_and_users'][table] = utils_functions.u(
            '{0}/admin_tables/{1}.csv').format(csvDir, table)
    # si c'est une table de la base admin (admin_and_users),
    # on propose d'abord de télécharger users :
    if mustDownUp and (fileDbName == 'admin'):
        if (tableName in table2csvFile['admin']['admin_and_users']) or (tableName == ''):
            mustDownloadDBs(main)

    utils_functions.doWaitCursor()
    try:
        if fileDbName == 'commun':
            db = main.db_commun
            query, transactionOK = utils_db.queryTransactionDB(db)
        elif fileDbName == 'admin':
            db = db_admin
            query, transactionOK = utils_db.queryTransactionDB(db)

        # un dico des tables et fichiers csv à traiter :
        csvFiles = {}
        if tableName == '':
            if fileDbName == 'commun':
                for tableName in table2csvFile['commun']:
                    csvFiles[tableName] = table2csvFile['commun'][tableName]
            elif fileDbName == 'admin':
                for tableName in table2csvFile['admin']['admin_only']:
                    csvFiles[tableName] = table2csvFile['admin']['admin_only'][tableName]
                for tableName in table2csvFile['admin']['admin_and_users']:
                    csvFiles[tableName] = table2csvFile['admin']['admin_and_users'][tableName]
        else:
            if fileDbName == 'commun':
                csvFiles[tableName] = utils_functions.u(
                    '{0}/commun_tables/{1}.csv').format(csvDir, tableName)
            elif fileDbName == 'admin':
                csvFiles[tableName] = utils_functions.u(
                    '{0}/admin_tables/{1}.csv').format(csvDir, tableName)
        import utils_export
        for tableName in csvFiles:
            utils_functions.afficheMessage(
                main, 'tableName: ' + tableName, editLog=True)
            fileName = csvFiles[tableName]

            # on récupère le fichier csv :
            headers, datas = utils_export.csv2List(fileName)

            # suppression de l'ancienne table :
            commandLine = utils_db.qdf_table.format(tableName)
            utils_functions.afficheMessage(main, commandLine, editLog=True)
            query = utils_db.queryExecute(commandLine, query=query)

            # création de la table :
            commandLine = ''
            if fileDbName == 'commun':
                commandLine = utils_db.listTablesCommun[tableName][0]
                nbCol = utils_db.listTablesCommun[tableName][1]
            elif fileDbName == 'admin':
                commandLine = utils_db.listTablesAdmin[tableName][0]
                nbCol = utils_db.listTablesAdmin[tableName][1]
            # au cas où, on crée une commande avec id et TEXT pour le reste :
            if commandLine == '':
                commandLine = (
                    'CREATE TABLE IF NOT EXISTS {0} '
                    '(id INTEGER PRIMARY KEY, ').format(tableName)
                for i in range(nbCol - 1):
                    commandLine = commandLine + '{0} TEXT, '.format(headers[i+1])
                commandLine = commandLine[:-2] + ')'
            utils_functions.afficheMessage(main, commandLine, editLog=True)
            query = utils_db.queryExecute(commandLine, query=query)

            # et remplissage :
            commandLine = utils_db.insertInto(tableName, nbCol)
            utils_functions.afficheMessage(main, commandLine)
            rowId = 0
            lines = []
            for row in datas:
                message = utils_functions.u('INSERT INTO {0} VALUES : ').format(tableName)
                if tableName == 'config_admin':
                    key_name = utils_functions.u(row[0])
                    try:
                        value_int = int(row[1])
                    except:
                        value_int = ''
                    value_text = utils_functions.u(row[2])
                    message = message + utils_functions.u('{0} {1} {2}').format(
                        key_name, value_int, value_text)
                    lines.append((key_name, value_int, value_text))
                elif tableName == 'matieres':
                    id_matiere = int(row[0])
                    Matiere = utils_functions.u(row[1])
                    MatiereCode = utils_functions.u(row[2])
                    MatiereLabel = utils_functions.u(row[3])
                    try:
                        OrdreBLT = int(row[4])
                    except:
                        OrdreBLT = ''
                    try:
                        id_prof = int(row[5])
                    except:
                        id_prof = ''
                    message = message + utils_functions.u('{0} {1} {2} {3} {4} {5}').format(
                        id_matiere, Matiere, MatiereCode, MatiereLabel, OrdreBLT, id_prof)
                    lines.append((id_matiere, Matiere, MatiereCode, MatiereLabel, OrdreBLT, id_prof))
                else:
                    line = []
                    for value in row:
                        value = utils_functions.u(value)
                        message = message + ' ' + utils_functions.u(value)
                        line.append(value)
                    if len(row) < nbCol:
                        mustRepairCsvFile = True
                        # vérification de la colonne ordre (versionDB = 6) :
                        if tableName in ('bulletin', 'referentiel', 'confidentiel', 'suivi'):
                            message = message + ' ' + utils_functions.u(rowId)
                            line.append(rowId)
                    lines.append(line)
                utils_functions.afficheMessage(main, message, editLog=True)
                rowId += 1
            query = utils_db.queryExecute({commandLine: lines}, query=query)

            # si on recrée la table profs, il faut recréer PROFS :
            if fileDbName == 'admin':
                if tableName == 'profs':
                    global PROFS
                    PROFS = []
                    commandLine_admin = utils_db.q_selectAllFrom.format('profs')
                    queryList = utils_db.query2List(
                        commandLine_admin, order=['NOM', 'Prenom'], query=query)
                    for record in queryList:
                        prof_id = int(record[0])
                        prof_file = utils_functions.u('{0}{1}').format(prefixProfFiles, prof_id)
                        prof_nom = record[2]
                        prof_prenom = record[3]
                        utils_functions.appendUnique(
                            PROFS, 
                            (prof_id, prof_file, prof_nom, prof_prenom))

        # on inscrit la version de la base :
        if fileDbName == 'commun':
            utils_db.changeInConfigTable(main, main.db_commun, 
                {'versionDB': (utils.VERSIONDB_COMMUN, '')})
    finally:
        if fileDbName == 'commun':
            utils_db.endTransaction(query, db, transactionOK)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, dbFileFtp_commun)
            # si on a recréé la table config, 
            # on relit les variables globales et main :
            if 'config' in csvFiles:
                prefixProfFiles = utils_db.readInConfigTable(
                    main.db_commun, 'prefixProfFiles')[1]
                main.siteUrlBase = utils_db.readInConfigTable(
                    main.db_commun, 'siteUrlBase')[1]
                main.siteUrlPublic = utils_db.readInConfigTable(
                    main.db_commun, 'siteUrlPublic')[1]
                main.siteUrlPublic = utils_functions.removeSlash(main.siteUrlPublic)
            # pour certaines tables, si la base commun-x n'existe pas,
            # il faut recréer les listes globales issues de la base commun :
            elif ('matieres' or 'sous_rubriques' or 'classes') in csvFiles:
                dbFileX = dirLocalPrive + '/public/commun-{0}.sqlite'.format(
                    utils.selectedPeriod)
                if not(QtCore.QFileInfo(dbFileX).exists()):
                    initFromCommun(main, main.db_commun)
            # si on a recréé une table de compétences de la base commun,
            # il faut refaire la table bilans_liens :
            elif ('bulletin' or 'referentiel' or 'confidentiel') in csvFiles:
                createTableBilansLiens(main)
            if mustRepairCsvFile:
                import utils_export
                utils_export.exportTable2Csv(
                    main, main.db_commun, tableName, csvFiles[tableName], msgFin=False)
        elif fileDbName == 'admin':
            utils_db.endTransaction(query, db, transactionOK)
            if 'config_admin' in csvFiles:
                # on relit les variables globales qui peuvent avoir été changées :
                global dirSitePublic, dirSiteSecret, dirSitePrive
                # variables de connexion ftp:
                ftpHost = utils_db.readInConfigTable(
                    db_admin, 'baseSiteFtp', 'config_admin')[1]
                ftpUser = utils_db.readInConfigTable(
                    db_admin, 'userFtp', 'config_admin')[1]
                ftpPass = utils_db.readInConfigTable(
                    db_admin, 'mdpFtp', 'config_admin')[1]
                admin_ftp.ftpUpdateConfig(
                    newFtpHost=ftpHost, newFtpUser=ftpUser, newFtpPass=ftpPass)
                # variables de chemin vers les répertoires public, secret et privé :
                dirSitePublic = utils_db.readInConfigTable(
                    db_admin, 'dirSitePublic', 'config_admin')[1]
                dirSiteSecret = utils_db.readInConfigTable(
                    db_admin, 'dirSiteSecret', 'config_admin')[1]
                dirSitePrive = dirSiteSecret + '/verac'
            if mustRepairCsvFile:
                import utils_export
                utils_export.exportTable2Csv(
                    main, main.db_admin, tableName, csvFiles[tableName], msgFin=False)

        utils_functions.restoreCursor()
        if mustDownUp:
            if fileDbName == 'commun':
                mustUploadDBs(main, databases=['commun'])
            elif fileDbName == 'admin':
                # si c'est une table de la base admin (admin_and_users),
                # on doit mettre à jour la base users
                # et la poster :
                mustUpload = False
                if 'eleves' in csvFiles:
                    updateUsersBase(main, mode='eleves', msgFin=False)
                    mustUpload = True
                if 'profs' in csvFiles:
                    updateUsersBase(main, mode='profs', msgFin=False)
                    mustUpload = True
                if mustUpload:
                    mustUploadDBs(main, databases=['users'])
        utils_functions.afficheStatusBar(main)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)

def updateTableLettersInCommun(main, deletes=[], changes={}):
    """
    Les lettres affichées dans l'interface de VÉRAC (V, J, ... par défaut)
    peuvent être modifiées par l'admin dans le menu Outils -> Paramètres.
    Pour que ces choix soient récupérés par les profs
    et dans l'interface Web, on les inscrit dans une table (config_affichage) 
    de la base commun.sqlite.
    Cette table n'est donc pas créée via un fichier csv.
    """
    utils_functions.doWaitCursor()
    try:
        query_commun, transactionOK = utils_db.queryTransactionDB(main.db_commun)
        utils_db.deleteFromConfigTable(
            main, main.db_commun, deletes, table='config_affichage')
        utils_db.changeInConfigTable(
            main, main.db_commun, changes, table='config_affichage', delete=False)
        changes = {}
        for item in utils.itemsValues:
            changes[item] = ('', utils.affichages[item][0])
        utils_db.changeInConfigTable(main, main.db_commun, changes, table='config_affichage')
    finally:
        utils_db.endTransaction(query_commun, main.db_commun, transactionOK)
        utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
        utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, dbFileFtp_commun)
        utils_functions.restoreCursor()

def calculeLogin(prenom, nom):
    """
    calcul du login de l'élève d'après la procédure
    utilisée dans Scribe (histoire d'avoir le même login).
    Modifications : tant pis pour le collage à Scribe (leur procédure a trop de défauts).
    """
    def simplifie(inText):
        """
        pour virer 2 3 trucs qui n'ont rien à faire dans les logins
        """
        newText = inText
        newText = newText.replace('\t', '')
        newText = newText.replace('\n', '')
        newText = newText.replace('\v', '')
        newText = newText.replace('\f', '')
        newText = newText.replace('\r', '')
        newText = newText.replace('-', ' ')
        newText = newText.replace("'", ' ')
        return newText

    def createName(names=[]):
        """
        on essaye de ne pas couper un morceau
        """
        name = ''
        encore = True
        if len(names) < 1:
            # nom ou prénom vide (ça existe ?)
            encore = False
        while encore:
            nextName = names.pop(0)
            if len(nextName) > 15:
                # on coupe le morceau :
                nextName = nextName[:15]
            if len(name) < 8:
                name = '{0}{1}'.format(name, nextName)
            if len(names) < 1:
                encore = False
            if len(name) > 10:
                # 10 caractères suffisent
                encore = False
        if len(name) > 15:
            # on coupe :
            name = name[:15]
        return name

    login = ''
    # on récupère la liste des prénoms et des noms :
    prenoms = simplifie(prenom).split()
    noms = simplifie(nom).split()
    # en minuscules et sans accents :
    for p in prenoms:
        prenoms[prenoms.index(p)] = utils_functions.doAscii(p, case='lower')
    for n in noms:
        noms[noms.index(n)] = utils_functions.doAscii(n, case='lower')
    # un seul mot pour chacun et limité en longueur :
    prenom = createName(prenoms)
    nom = createName(noms)
    # on peut créer le login (avec une dernière limite) :
    login = prenom + '.' + nom
    login = login[:30]
    return login

def compareLogins(newLogin, oldLogin):
    """
    vérifie si newLogin correspond à l'ancien (oldLogin).
    On tient compte du fait que oldLogin peut se terminer par un numéro.
    """
    idem = False
    if newLogin == oldLogin:
        idem = True
    elif oldLogin[:len(newLogin)] == newLogin:
        # on vérifie que la suite est un numéro :
        try:
            n = int(oldLogin[len(newLogin):])
            idem = True
        except:
            idem = False
    return idem

def createLogin(prenom, nom, users_login, oldLogin=''):
    """
    création du login de l'élève d'après la procédure
    utilisée dans Scribe (histoire d'avoir le même login).
    Modifications : tant pis pour le collage à Scribe (leur procédure a trop de défauts).
    On ajoute un numéro en cas d'homonymes (on commence à 2 comme le cahier de textes).
    """
    # on calcule le nouveau login (sans numéro) :
    newLogin = calculeLogin(prenom, nom)
    # si on est retombé sur le même login, on renvoie l'ancien
    # (en gardant l'éventuel numéro) :
    if compareLogins(newLogin, oldLogin):
        return oldLogin, users_login
    # il faut vérifier les homonymes :
    i = 2
    newLoginWithNum = newLogin
    while (newLoginWithNum in users_login):
        newLoginWithNum = '{0}{1}'.format(newLogin, i)
        i += 1
    users_login.append(newLoginWithNum)
    return newLoginWithNum, users_login

class createReplacementDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour recréer la liste des correspondances de nom de classe,
    lorsque ceux contenus dans le fichier ElevesSansAdresses.xml
    sont différents de ceux définis dans VÉRAC
    """
    def __init__(self, parent=None, replaceWhere=''):
        """
        mise en place de l'interface
        """
        super(createReplacementDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'createReplacement'))
        self.helpBaseUrl = 'admin'

        if replaceWhere == 'xmlClasses':
            self.helpContextPage = 'profs-stsweb'
            self.pageId = 'chooseClasseName'
            messageTitle = QtWidgets.QApplication.translate(
                'main', '<p><b>Classes Names Replacement:</b></p>')
        elif replaceWhere == 'xmlMatieres':
            self.helpContextPage = 'profs-stsweb'
            self.pageId = 'chooseMatiereName'
            messageTitle = QtWidgets.QApplication.translate(
                'main', '<p><b>Subjects Names Replacement:</b></p>')
        elif replaceWhere == 'xmlProfs':
            self.helpContextPage = 'profs-stsweb'
            self.pageId = 'chooseProfName'
            messageTitle = QtWidgets.QApplication.translate(
                'main', '<p><b>Teachers Names Replacement:</b></p>')
        message = QtWidgets.QApplication.translate(
            'main', 
            '<p>Names (in file) that does not exist in your '
            '<br/>configuration are already joined to name that exists.</p>'
            '<p>Do you wish to reset this ?</p>')
        label = QtWidgets.QLabel(utils_functions.u('{0}{1}').format(messageTitle, message))

        dialogButtons = utils_functions.createDialogButtons(self, 
            layout=True, buttons=('yes', 'no', 'help'))
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(label)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(
            self.helpContextPage, self.helpBaseUrl, self.pageId)

class ChooseClassNameDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour sélectionner un nom de classe valable,
    si ceux contenus dans le fichier ElevesSansAdresses.xml
    sont différents de ceux définis dans VÉRAC.
    Marche aussi avec l'import profs par xml et l'import base eleve.
    """
    def __init__(
            self, parent=None, 
            badName='', classesNames=[], eleve='', 
            helpContextPage='profs-stsweb', 
            helpBaseUrl='admin', 
            pageId='chooseClasseName'):
        """
        mise en place de l'interface
        """
        super(ChooseClassNameDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Select the class name'))

        self.eleve = eleve
        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl
        self.pageId = pageId

        message = QtWidgets.QApplication.translate(
            'main', 
            '<p align="center">The name of class <b>{0}</b> is in the file,'
            '<br/>but does not exist in your configuration.</p>'
            '<p>Select the right name in the list below '
            '<br/> (you can also add this class) :</p>')
        message = message.format(badName)
        if self.eleve != '':
            message = utils_functions.u(
                '<b>{0} :</b> {1}').format(self.eleve, message)
        else:
            message = utils_functions.u('{0}').format(message)
        label = QtWidgets.QLabel(message)

        self.classComboBox = QtWidgets.QComboBox()
        self.classComboBox.setMinimumWidth(200)
        self.classComboBox.addItem(QtWidgets.QApplication.translate(
            'main', 'Add this class'))
        if self.eleve != '':
            self.classComboBox.addItem(QtWidgets.QApplication.translate(
                'main', 'Do not import this student'))
        for nomClasse in classesNames:
            self.classComboBox.addItem(nomClasse)
        self.classComboBox.activated.connect(self.classComboBoxChanged)

        # si on sélectionne "Add this class", choix du nom de la classe :
        text = QtWidgets.QApplication.translate(
            'main', 'Name of the new class:')
        self.classNameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.classNameEdit = QtWidgets.QLineEdit(badName)
        self.classNameLabel.setMinimumWidth(200)
        self.classNameEdit.setMinimumWidth(200)

        # si on sélectionne "Add this class", choix du type de classe :
        text = QtWidgets.QApplication.translate(
            'main', 'Class type:')
        self.classTypeLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.classTypeComboBox = QtWidgets.QComboBox()
        # on remplit le comboBox :
        query_commun = utils_db.query(self.main.db_commun)
        commandLine = utils_db.q_selectAllFromOrder.format(
            'classestypes', 'id')
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        while query_commun.next():
            id = int(query_commun.value(0))
            name = query_commun.value(1)
            self.classTypeComboBox.addItem(
                utils_functions.u(name), id)

        # un CheckBox pour les classes à notes :
        text = QtWidgets.QApplication.translate(
            'main', 'Class with notes:')
        self.notesLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.notesCheckBox = QtWidgets.QCheckBox()
        text = QtWidgets.QApplication.translate(
            'main', 'Check this box if the class must have notes')
        self.notesCheckBox.setToolTip(text)
        self.notesCheckBox.setChecked(False)

        # comboBox pour mémoriser le choix
        self.choiceComboBox = QtWidgets.QComboBox()
        text = QtWidgets.QApplication.translate(
            'main', 'Apply to all students from this class')
        self.choiceComboBox.addItem(text)
        text = QtWidgets.QApplication.translate(
            'main', 'Apply only to this student')
        self.choiceComboBox.addItem(text)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 1, 0, 1, 2)
        grid.addWidget(self.classComboBox, 2, 0, 1, 2)
        grid.addWidget(self.classNameLabel, 3, 0)
        grid.addWidget(self.classNameEdit, 3, 1)
        grid.addWidget(self.classTypeLabel, 4, 0)
        grid.addWidget(self.classTypeComboBox, 4, 1)
        grid.addWidget(self.notesLabel, 5, 0)
        grid.addWidget(self.notesCheckBox, 5, 1)
        if self.eleve != '':
            grid.addWidget(self.choiceComboBox, 8, 0, 1, 2)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        #self.setMinimumWidth(400)
        self.classComboBoxChanged()

    def classComboBoxChanged(self):
        # pour que les objets liés à un ajout de classe ne soient actifs
        # que si on a sélectionné "Add this class"
        ObjectsList = (
            self.classNameLabel, self.classNameEdit, 
            self.classTypeLabel, self.classTypeComboBox,
            self.notesLabel, self.notesCheckBox)
        enable = (self.classComboBox.currentIndex() == 0)
        for aObject in ObjectsList:
            aObject.setEnabled(enable)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(
            self.helpContextPage, self.helpBaseUrl, self.pageId)

class ChooseMatiereNameDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour sélectionner un nom de matière valable,
    lors de l'import profs par xml
    si ceux contenus dans le fichier xml
    sont différents de ceux définis dans VÉRAC.
    """
    def __init__(
            self, parent=None, 
            badName='', matieresNames=[], 
            helpContextPage='profs-stsweb', 
            helpBaseUrl='admin', 
            pageId='chooseMatiereName'):
        """
        mise en place de l'interface
        """
        super(ChooseMatiereNameDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Select the subject name'))

        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl
        self.pageId = pageId

        message = QtWidgets.QApplication.translate(
            'main', 
            '<p align="center">The subject name <b>{0}</b> is in the file,'
            '<br/>but does not exist in your configuration.</p>'
            '<p>Select the right name in the list below '
            '<br/> (you can also add this subject) :</p>')
        message = message.format(badName)
        message = utils_functions.u('{0}').format(message)
        label = QtWidgets.QLabel(message)

        self.matiereComboBox = QtWidgets.QComboBox()
        self.matiereComboBox.setMinimumWidth(200)
        self.matiereComboBox.addItem(QtWidgets.QApplication.translate(
            'main', 'Add this subject'))
        for nomMatiere in matieresNames:
            self.matiereComboBox.addItem(nomMatiere)
        self.matiereComboBox.activated.connect(self.matiereComboBoxChanged)

        # si on sélectionne "Add this subject", choix du nom de la matière :
        text = QtWidgets.QApplication.translate(
            'main', 'Name of the new subject:')
        self.matiereNameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.matiereNameEdit = QtWidgets.QLineEdit(badName)
        self.matiereNameLabel.setMinimumWidth(200)
        self.matiereNameEdit.setMinimumWidth(200)

        # si on sélectionne "Add this subject", choix du code de la matière :
        text = QtWidgets.QApplication.translate('main', 'Code:')
        self.matiereCodeLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.matiereCodeEdit = QtWidgets.QLineEdit()

        # si on sélectionne "Add this subject", choix du label de la matière :
        text = QtWidgets.QApplication.translate('main', 'Label:')
        self.matiereLabelLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.matiereLabelEdit = QtWidgets.QLineEdit()

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 1, 0, 1, 2)
        grid.addWidget(self.matiereComboBox, 2, 0, 1, 2)
        grid.addWidget(self.matiereNameLabel, 3, 0)
        grid.addWidget(self.matiereNameEdit, 3, 1)
        grid.addWidget(self.matiereCodeLabel, 4, 0)
        grid.addWidget(self.matiereCodeEdit, 4, 1)
        grid.addWidget(self.matiereLabelLabel, 5, 0)
        grid.addWidget(self.matiereLabelEdit, 5, 1)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        #self.setMinimumWidth(400)
        self.matiereComboBoxChanged()

    def matiereComboBoxChanged(self):
        # pour que les objets liés à un ajout de classe ne soient actifs
        # que si on a sélectionné "Add this subject"
        ObjectsList = (
            self.matiereNameLabel, self.matiereNameEdit,
            self.matiereCodeLabel, self.matiereCodeEdit,
            self.matiereLabelLabel, self.matiereLabelEdit)
        enable = (self.matiereComboBox.currentIndex() == 0)
        for aObject in ObjectsList:
            aObject.setEnabled(enable)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(
            self.helpContextPage, self.helpBaseUrl, self.pageId)

class ChooseProfNameDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour sélectionner un nom de prof valable,
    lors de l'import profs par xml
    si ceux contenus dans le fichier xml
    sont différents de ceux définis dans VÉRAC.
    chooseMatiere=(profFonction == 'ENS')
    """
    def __init__(
            self, parent=None, 
            badName='', badData=('', '', '', '', ''),
            profFonction='ENS', #chooseMatiere=True, 
            listNomsProfs=[], matieresNames=[], 
            helpContextPage='profs-stsweb', 
            helpBaseUrl='admin', 
            pageId='chooseProfName'):
        """
        mise en place de l'interface
        """
        super(ChooseProfNameDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        fonctionsNames = {
            'ENS': QtWidgets.QApplication.translate('main', 'teacher'), 
            'EDU': QtWidgets.QApplication.translate('main', 'teacher'), 
            'DIR': QtWidgets.QApplication.translate('main', 'director')}
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Select the {0} name').format(fonctionsNames[profFonction]))

        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl
        self.pageId = pageId

        message = QtWidgets.QApplication.translate(
            'main', 
            '<p align="center">The name of {0}:'
            '<br/><b>{1}</b>'
            '<br/>is in the file,'
            '<br/>but does not exist in your configuration.</p>'
            '<p>Select the right name in the list below '
            '<br/> (you can also add this) :</p>')
        message = message.format(fonctionsNames[profFonction], badName)
        message = utils_functions.u('{0}').format(message)
        label = QtWidgets.QLabel(message)

        self.profComboBox = QtWidgets.QComboBox()
        self.profComboBox.setMinimumWidth(200)
        self.profComboBox.addItem(QtWidgets.QApplication.translate(
            'main', 'Add this {0}').format(fonctionsNames[profFonction]))
        self.profComboBox.addItem(QtWidgets.QApplication.translate(
            'main', 'Do not import this'))
        for nomProf in listNomsProfs:
            self.profComboBox.addItem(nomProf)
        self.profComboBox.activated.connect(self.profComboBoxChanged)

        # si on sélectionne "Add this teacher", choix du nom :
        text = QtWidgets.QApplication.translate(
            'main', 'Name of the new {0}:').format(fonctionsNames[profFonction])
        self.profNameLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.profNameEdit = QtWidgets.QLineEdit(badData[0])
        self.profNameLabel.setMinimumWidth(200)
        self.profNameEdit.setMinimumWidth(300)
        # choix du prénom :
        text = QtWidgets.QApplication.translate('main', 'FirstName:')
        self.profPrenomLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.profPrenomEdit = QtWidgets.QLineEdit(badData[1])
        # choix de la matière (on présélectionne si elle est dans matieresNames) :
        text = QtWidgets.QApplication.translate('main', 'Subject:')
        self.profMatiereLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.profMatiereComboBox = QtWidgets.QComboBox()
        for matiereName in matieresNames:
            self.profMatiereComboBox.addItem(utils_functions.u(matiereName))
        index = self.profMatiereComboBox.findText(badData[2])
        if index != -1:
            self.profMatiereComboBox.setCurrentIndex(index)
        # choix du login :
        text = QtWidgets.QApplication.translate('main', 'Login:')
        self.profLoginLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.profLoginEdit = QtWidgets.QLineEdit(badData[3])
        # choix du mot de passe :
        text = QtWidgets.QApplication.translate(
            'main', 'Initial password:')
        self.profMdpLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(text))
        self.profMdpEdit = QtWidgets.QLineEdit(badData[4])

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 1, 0, 1, 2)
        grid.addWidget(self.profComboBox, 2, 0, 1, 2)
        grid.addWidget(self.profNameLabel, 3, 0)
        grid.addWidget(self.profNameEdit, 3, 1)
        grid.addWidget(self.profPrenomLabel, 4, 0)
        grid.addWidget(self.profPrenomEdit, 4, 1)
        if profFonction == 'ENS':
            grid.addWidget(self.profMatiereLabel, 5, 0)
            grid.addWidget(self.profMatiereComboBox, 5, 1)
        grid.addWidget(self.profLoginLabel, 6, 0)
        grid.addWidget(self.profLoginEdit, 6, 1)
        grid.addWidget(self.profMdpLabel, 7, 0)
        grid.addWidget(self.profMdpEdit, 7, 1)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.profComboBoxChanged()

    def profComboBoxChanged(self):
        # pour que les objets liés à un ajout de classe ne soient actifs
        # que si on a sélectionné "Add this teacher"
        ObjectsList = (
            self.profNameLabel, self.profNameEdit,
            self.profPrenomLabel, self.profPrenomEdit,
            self.profMatiereLabel, self.profMatiereComboBox,
            self.profLoginLabel, self.profLoginEdit,
            self.profMdpLabel, self.profMdpEdit)
        enable = (self.profComboBox.currentIndex() == 0)
        for aObject in ObjectsList:
            aObject.setEnabled(enable)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(
            self.helpContextPage, self.helpBaseUrl, self.pageId)

def teachersFromSTSWeb(main, fileName, msgFin=True):
    """
    L'import des profs se fait avec le fichier sts_emp_RNE_aaaa.xml
    où RNE est le n° de RNE de l'établissement et aaaa l'année (septembre).
    Par exemple, à VÉRAC pour 2011-2012, le fichier s'appelle sts_emp_0332706M_2011.xml.
    Mais le fichier contient aussi :
        * les classes
        * les matières
    """
    utils_functions.doWaitCursor()
    OK = False
    data = {
        'profs': [], 
        'replacements': [], 
        'classes': [], 
        'matieres': [], 
        'lsu': [], 
        }
    mustRecreateTable = {
        'replacements': False, 
        }
    query_admin = utils_db.query(db_admin)
    query_commun = utils_db.query(main.db_commun)

    try:
        import utils_export_xml
        (ok, dataInXML) = utils_export_xml.teachersFromSTSWeb(main, fileName)
        if not(ok):
            return

        # *******************************************************
        # RÉCUPÉRATION DES CLASSES
        # *******************************************************
        # récupération des noms des classes depuis la base commun :
        classesNames = []
        maxId = 0
        maxOrdre = 0
        commandLine_commun = utils_db.q_selectAllFromOrder.format(
            'classes', 'ordre, Classe')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            id_classe = int(query_commun.value(0))
            if id_classe > maxId:
                maxId = id_classe
            ordre = int(query_commun.value(4))
            if ordre > maxOrdre:
                maxOrdre = ordre
            classe = query_commun.value(1)
            classesNames.append(classe)
        # récupération des remplacements depuis la base admin :
        namesReplacements = {}
        replaceWhere = 'xmlClasses'
        mustRecreateTable['replacements'] = False
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'replacements', 'replaceWhere', replaceWhere)
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        mustAsk = False
        while query_admin.next():
            replaceWhat = query_admin.value(1)
            replaceWith = query_admin.value(2)
            if replaceWith in classesNames:
                namesReplacements[replaceWhat] = replaceWith
            mustAsk = True
        if mustAsk:
            utils_functions.restoreCursor()
            dialog = createReplacementDlg(
                parent=main, replaceWhere=replaceWhere)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                mustRecreateTable['replacements'] = True
                namesReplacements = {}
            utils_functions.doWaitCursor()
        # on traite les nouvelles données :
        for classe in dataInXML['classes']:
            if classe in namesReplacements:
                continue
            elif classe in classesNames:
                continue
            else:
                utils_functions.restoreCursor()
                dialog = ChooseClassNameDlg(
                    parent=main, 
                    badName=classe, 
                    classesNames=classesNames)
                if dialog.exec_() != QtWidgets.QDialog.Accepted:
                    return
                utils_functions.doWaitCursor()
                if dialog.classComboBox.currentIndex() == 0:
                    # on ajoute la classe :
                    newName = dialog.classNameEdit.text()
                    classType = dialog.classTypeComboBox.currentIndex()
                    notes = 0
                    if dialog.notesCheckBox.isChecked():
                        notes = 1
                    maxId += 1
                    id_classe = maxId
                    maxOrdre += 1
                    ordre = maxOrdre
                    data['classes'].append(
                        (id_classe, newName, classType, notes, ordre))
                else:
                    newName = dialog.classComboBox.currentText()
                if newName != classe:
                    namesReplacements[classe] = newName
                    mustRecreateTable['replacements'] = True
                if not(newName in classesNames):
                    classesNames.append(newName)
        # on réécrit les remplacements :
        if mustRecreateTable['replacements']:
            for replaceWhat in namesReplacements:
                replaceWith = namesReplacements[replaceWhat]
                data['replacements'].append(
                    (replaceWhere, replaceWhat, replaceWith))

        # *******************************************************
        # RÉCUPÉRATION DES MATIÈRES
        # *******************************************************
        # récupération des noms des matières depuis la base commun :
        matieresNames = []
        for matiereCode in MATIERES['BLT']:
            matiereName = MATIERES['Code2Matiere'][matiereCode][1]
            matieresNames.append(matiereName)
        for (matiereCode, temp_id_prof) in MATIERES['BLTSpeciales']:
            matiereName = MATIERES['Code2Matiere'][matiereCode][1]
            matieresNames.append(matiereName)
        maxId = 0
        maxOrdre = 0
        commandLine_commun = utils_db.q_selectAllFrom.format('matieres')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            id_matiere = int(query_commun.value(0))
            if id_matiere > maxId:
                maxId = id_matiere
            ordre = int(query_commun.value(4))
            if ordre > maxOrdre:
                maxOrdre = ordre
        # récupération des remplacements depuis la base admin :
        namesReplacementsSubjects = {}
        replaceWhere = 'xmlMatieres'
        mustRecreateTable['replacements'] = False
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'replacements', 'replaceWhere', replaceWhere)
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        mustAsk = False
        while query_admin.next():
            replaceWhat = query_admin.value(1)
            replaceWithCode = query_admin.value(2)
            replaceWithName = MATIERES['Code2Matiere'][replaceWithCode][1]
            if replaceWithName in matieresNames:
                namesReplacementsSubjects[replaceWhat] = replaceWithName
            mustAsk = True
        if mustAsk:
            utils_functions.restoreCursor()
            dialog = createReplacementDlg(parent=main, replaceWhere=replaceWhere)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                mustRecreateTable['replacements'] = True
                namesReplacementsSubjects = {}
            utils_functions.doWaitCursor()
        # on traite les nouvelles données :
        for matiere in dataInXML['matieres']:
            if matiere in namesReplacementsSubjects:
                continue
            elif matiere in matieresNames:
                continue
            else:
                utils_functions.restoreCursor()
                dialog = ChooseMatiereNameDlg(
                    parent=main, 
                    badName=matiere, 
                    matieresNames=matieresNames)
                if dialog.exec_() != QtWidgets.QDialog.Accepted:
                    return
                utils_functions.doWaitCursor()
                if dialog.matiereComboBox.currentIndex() == 0:
                    # on ajoute la matière :
                    newName = dialog.matiereNameEdit.text()
                    newCode = dialog.matiereCodeEdit.text()
                    if newCode == '':
                        newCode = newName
                    newLabel = dialog.matiereLabelEdit.text()
                    if newLabel == '':
                        newLabel = newName
                    maxId += 1
                    id_matiere = maxId
                    maxOrdre += 1
                    newOrdreBLT = maxOrdre
                    data['matieres'].append(
                        (id_matiere, newName, newCode, newLabel, newOrdreBLT, ''))
                else:
                    newName = dialog.matiereComboBox.currentText()
                if newName != matiere:
                    namesReplacementsSubjects[matiere] = newName
                    mustRecreateTable['replacements'] = True
                if not(newName in matieresNames):
                    matieresNames.append(newName)
        # on réécrit les remplacements :
        if mustRecreateTable['replacements']:
            for replaceWhat in namesReplacementsSubjects:
                replaceWith = namesReplacementsSubjects[replaceWhat]
                replaceWith = MATIERES['Matiere2Code'].get(replaceWith, replaceWith)
                data['replacements'].append(
                    (replaceWhere, replaceWhat, replaceWith))

        # *******************************************************
        # RÉCUPÉRATION DES PROFS
        # *******************************************************
        # récupération des profs depuis la base admin :
        profs = {
            'IDS': [[], []], 
            'LOGINS': [], 
            'ENS': [], 'EDU': [], 'DIR': [], 'ALL': [], 
            'ENS_ID': {}, 'LSU': {}}
        # on récupère le nom de la matière Vie Scolaire (à traiter à part) :
        VSName = MATIERES['Code2Matiere'].get(
            'VS', (-2, 'Vie scolaire', 'Vie scolaire'))[1]
        # on récupère le contenu de la table admin.lsu :
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'prof')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            profId = int(query_admin.value(1))
            lsu2 = int(query_admin.value(2))
            lsu3 = query_admin.value(3)
            profs['LSU'][profId] = (lsu2, lsu3)
        commandLine_admin = utils_db.q_selectAllFrom.format('profs')
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            profId = int(record[0])
            profNom = record[2]
            profPrenom = record[3]
            profMatiereCode = record[4]
            profMatiere = MATIERES['Code2Matiere'].get(profMatiereCode, (0, '', ''))[1]
            profLogin = record[5]
            if profId < utils.decalagePP:
                # pour les ids utilisés, on mélange profs et cpe :
                profs['IDS'][0].append(profId)
                # pour les noms, on sépare :
                prof = utils_functions.u('{0} {1} ({2})').format(
                    profNom, profPrenom, profMatiere)
                if profMatiere != VSName:
                    profs['ENS'].append(prof)
                    profs['ENS_ID'][prof] = profId
                else:
                    profs['EDU'].append(prof)
                profs['ALL'].append(prof)
            elif profId >= utils.decalageChefEtab:
                # ids et noms des chefs d'établissement :
                profs['IDS'][1].append(profId)
                prof = utils_functions.u('{0} {1}').format(profNom, profPrenom)
                profs['DIR'].append(prof)
                profs['ALL'].append(prof)
            # pour les logins, on mélange tout :
            profs['LOGINS'].append(profLogin)
        # récupération des remplacements depuis la base admin :
        namesReplacementsTeachers = {}
        replaceWhere = 'xmlProfs'
        mustRecreateTable['replacements'] = False
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'replacements', 'replaceWhere', replaceWhere)
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        mustAsk = False
        while query_admin.next():
            replaceWhat = query_admin.value(1)
            replaceWith = query_admin.value(2)
            if replaceWith in profs['ALL']:
                namesReplacementsTeachers[replaceWhat] = replaceWith
            mustAsk = True
        if mustAsk:
            utils_functions.restoreCursor()
            dialog = createReplacementDlg(parent=main, replaceWhere=replaceWhere)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                mustRecreateTable['replacements'] = True
                namesReplacementsTeachers = {}
            utils_functions.doWaitCursor()
        # on traite les nouvelles données.
        # on commence par les profs, puis les CPE, et enfin les chefs d'établissement :
        fonctions = ('ENS', 'EDU', 'DIR')
        for profFonction in fonctions:
            for profIdInXML in dataInXML['profs'][profFonction]:
                profId = -1
                profNom = dataInXML['profs'][profFonction][profIdInXML]['NOM']
                profPrenom = dataInXML['profs'][profFonction][profIdInXML]['Prenom']
                profMatiere = dataInXML['profs'][profFonction][profIdInXML]['Matiere']
                profType = dataInXML['profs'][profFonction][profIdInXML]['Type']
                profDateNaiss = dataInXML['profs'][profFonction][profIdInXML]['DateNaiss']
                profCivilite = dataInXML['profs'][profFonction][profIdInXML]['Civilite']
                # on remplace la matière :
                if profFonction == 'ENS':
                    profMatiere = namesReplacementsSubjects.get(profMatiere, profMatiere)
                elif profFonction == 'EDU':
                    profMatiere = VSName
                # on remet en forme le prénom trouvé dans le xml :
                prenoms = ' '.join(profPrenom.split('-')).split()
                profPrenom = ''
                for p in prenoms:
                    profPrenom = utils_functions.u('{0}{1}{2} ').format(
                        profPrenom, p[0].upper(), p[1:].lower())
                profPrenom = profPrenom[:-1]
                # mise en forme du nom complet du prof :
                if profFonction == 'DIR':
                    prof = utils_functions.u('{0} {1}').format(
                        profNom, profPrenom)
                else:
                    prof = utils_functions.u('{0} {1} ({2})').format(
                        profNom, profPrenom, profMatiere)
                if prof in namesReplacementsTeachers:
                    if profFonction == 'ENS':
                        replaceWith = namesReplacementsTeachers[prof]
                        profId = profs['ENS_ID'][replaceWith]
                        profs['LSU'][profId] = (profIdInXML, profType)
                    continue
                elif prof in profs[profFonction]:
                    if profFonction == 'ENS':
                        profId = profs['ENS_ID'][prof]
                        profs['LSU'][profId] = (profIdInXML, profType)
                    continue
                else:
                    # c'est donc un nouveau prof :
                    utils_functions.restoreCursor()
                    profLogin, profs['LOGINS'] = createLogin(
                        profPrenom, profNom, profs['LOGINS'])
                    profMdp = profLogin
                    dialog = ChooseProfNameDlg(
                        parent=main, 
                        badName=prof, 
                        badData=(profNom, profPrenom, profMatiere, profLogin, profMdp), 
                        profFonction=profFonction, #chooseMatiere=(profFonction == 'ENS'), 
                        listNomsProfs=profs[profFonction], 
                        matieresNames=matieresNames)
                    if dialog.exec_() != QtWidgets.QDialog.Accepted:
                        return
                    utils_functions.doWaitCursor()
                    if dialog.profComboBox.currentIndex() == 0:
                        # on ajoute le prof
                        newName = dialog.profNameEdit.text()
                        newPrenom = dialog.profPrenomEdit.text()
                        if profFonction == 'ENS':
                            newMatiere = dialog.profMatiereComboBox.currentText()
                        elif profFonction == 'EDU':
                            newMatiere = VSName
                        else:
                            newMatiere = ''
                        newLogin = dialog.profLoginEdit.text()
                        newMdp = dialog.profMdpEdit.text()
                        # on cherche aussi un id libre :
                        if profFonction == 'DIR':
                            newProf = utils_functions.u('{0} {1}').format(
                                newName, newPrenom)
                            profId = utils.decalageChefEtab
                            while profId in profs['IDS'][1]:
                                profId += 1
                            profs['IDS'][1].append(profId)
                        else:
                            newProf = utils_functions.u('{0} {1} ({2})').format(
                                newName, newPrenom, newMatiere)
                            profId = 0
                            while profId in profs['IDS'][0]:
                                profId += 1
                            profs['IDS'][0].append(profId)
                        # on l'ajoute à la base admin :
                        newMatiereCode = MATIERES['Matiere2Code'].get(newMatiere, newMatiere)
                        data['profs'].append(
                            (profId, profIdInXML, newName, newPrenom, newMatiereCode, newLogin, newMdp))
                    elif dialog.profComboBox.currentIndex() == 1:
                        continue
                    else:
                        newProf = dialog.profComboBox.currentText()
                        profId = profs['ENS_ID'][newProf]
                    if newProf != prof:
                        namesReplacementsTeachers[prof] = newProf
                        mustRecreateTable['replacements'] = True
                    if not(newProf in profs[profFonction]):
                        profs[profFonction].append(newProf)
                        profs['ALL'].append(newProf)
                    if profFonction == 'ENS':
                        profs['ENS_ID'][newProf] = profId
                        profs['LSU'][profId] = (profIdInXML, profType)

        # on réécrit les remplacements :
        if mustRecreateTable['replacements']:
            for replaceWhat in namesReplacementsTeachers:
                replaceWith = namesReplacementsTeachers[replaceWhat]
                data['replacements'].append(
                    (replaceWhere, replaceWhat, replaceWith))

        # on mettra la table admin.lsu à jour :
        for profId in profs['LSU']:
            (lsu2, lsu3) = profs['LSU'][profId]
            data['lsu'].append(
                ('prof', '{0}'.format(profId), '{0}'.format(lsu2), lsu3, '', ''))

        OK = True
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if not(OK):
            message = QtWidgets.QApplication.translate(
                'main', 'Importing the file failed or you have canceled.')
            utils_functions.messageBox(main, level='warning', message=message)
        elif msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
        return OK, data

def studentsFromSIECLE(main, fileName, msgFin=True):
    """
    mise à jour des élèves à partir du fichier SIECLE
    """
    # doit-on supprimer les élèves sortis du fichier :
    message = QtWidgets.QApplication.translate(
        'main', 
        '<b>Should we remove students who are not in the file?</b>'
        '<br/><br/>(in general, this is to do that earlier this year)')
    reponse = utils_functions.messageBox(
        main, level='warning', message=message, buttons=['Yes', 'No'])
    delete = (reponse == QtWidgets.QMessageBox.Yes)

    utils_functions.doWaitCursor()
    OK = False
    datas = {
        'eleves': [], 
        'replacements': [], 
        'classes': [], 
        'adresses': [], 
        }
    mustRecreateTable = {
        'replacements': False, 
        'adresses': False, 
        }
    query_admin = utils_db.query(db_admin)
    query_commun = utils_db.query(main.db_commun)

    try:
        import utils_export_xml
        (ok, elevesInXML) =  \
            utils_export_xml.studentsFromSIECLE(main, fileName)
        if not(ok):
            return

        # pour gérer le fait que les noms des classes dans le fichier xml
        # ne correspondent pas forcément à ceux de la base :
        namesReplacements = {}
        replaceWhere = 'xmlClasses'
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'replacements', 'replaceWhere', replaceWhere)
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        mustAsk = False
        while query_admin.next():
            replaceWhat = query_admin.value(1)
            replaceWith = query_admin.value(2)
            namesReplacements[replaceWhat] = replaceWith
            mustAsk = True
        if mustAsk:
            utils_functions.restoreCursor()
            dialog = createReplacementDlg(parent=main, replaceWhere=replaceWhere)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                mustRecreateTable['replacements'] = True
                namesReplacements = {}
            utils_functions.doWaitCursor()

        # récupération des noms des classes dans la base commun :maxIdClasse
        classesNames = []
        maxId = 0
        maxOrdre = 0
        commandLine_commun = utils_db.q_selectAllFromOrder.format(
            'classes', 'ordre, Classe')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            id_classe = int(query_commun.value(0))
            if id_classe > maxId:
                maxId = id_classe
            ordre = int(query_commun.value(4))
            if ordre > maxOrdre:
                maxOrdre = ordre
            classe = query_commun.value(1)
            classesNames.append(classe)

        # on crée la liste des login profs pour éviter les login d'homonymes
        users_login = []
        commandLine_admin = utils_db.q_selectAllFrom.format('profs')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            users_login.append(query_admin.value(5))
        # on récupère les élèves déjà présents dans admin.eleves
        # pour ne pas remettre des élèves précédemment supprimés
        # (si on lance avec delete=False après avoir lancé avec delete=True)
        usersDict = {}
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            try:
                id_user = int(query_admin.value(0))
                num_user = query_admin.value(1)
                nom_user = query_admin.value(2)
                prenom_user = query_admin.value(3)
                classe_user = query_admin.value(4)
                login_user = query_admin.value(5)
                date_naiss_user = int(query_admin.value(6))
                mdp_user = query_admin.value(7)
                anDernier_user = query_admin.value(8)
                if anDernier_user == None:
                    anDernier_user = ''
                sexe_user = query_admin.value(12)
                usersDict[id_user] = {
                    'num':num_user, 'Login':login_user, 'Mdp':mdp_user,
                    'NOM':nom_user, 'Prenom':prenom_user, 'Classe':classe_user, 
                    'Date_naiss':date_naiss_user, 'AnDernier':anDernier_user, 
                    'sexe': sexe_user}
                users_login.append(login_user)
            except:
                continue

        # on y va :
        elevesAdresses = {}
        id2id = {}
        for id_eleve in elevesInXML:
            mustInsertEleve = True
            EleveDateSortie = elevesInXML[id_eleve]['DATE_SORTIE']
            if EleveDateSortie != '':
                # l'élève est donc sorti.
                # si delete=True, on le supprime de la base.
                # sinon, si on l'avait déjà supprimé, on ne le remet pas.
                if delete or not(id_eleve in usersDict):
                    mustInsertEleve = False
            if mustInsertEleve == False:
                continue
            eleve_id = elevesInXML[id_eleve]['eleve_id']
            EleveNom = elevesInXML[id_eleve]['NOM']
            ElevePrenom = elevesInXML[id_eleve]['Prenom']
            EleveDateNaiss = elevesInXML[id_eleve]['Date_naiss']
            EleveSexe = elevesInXML[id_eleve]['sexe']
            adresse = elevesInXML[id_eleve]['adresse']
            if adresse != '':
                elevesAdresses[id_eleve] = (
                    eleve_id, 
                    elevesInXML[id_eleve]['nomComplet'], 
                    adresse)
            try:
                EleveDateSortie = int(EleveDateSortie[6:] + EleveDateSortie[3:5] + EleveDateSortie[:2])
            except:
                EleveDateSortie = -1
            try:
                EleveDateEntree = elevesInXML[id_eleve]['DATE_ENTREE']
                EleveDateEntree = int(EleveDateEntree[6:] + EleveDateEntree[3:5] + EleveDateEntree[:2])
            except:
                EleveDateEntree = -1

            # on cherche la classe de l'élève :
            EleveClasse = ''
            codeStructure = elevesInXML[id_eleve]['Classe']
            if codeStructure == '???':
                # PB Vincent L
                if id_eleve in usersDict:
                    codeStructure = usersDict[id_eleve]['Classe']
            if codeStructure == '???':
                # PB Vincent L
                EleveClasse = codeStructure
            elif codeStructure in classesNames:
                EleveClasse = codeStructure
            elif codeStructure in namesReplacements:
                EleveClasse = namesReplacements[codeStructure]
            elif len(codeStructure) > 0:
                utils_functions.restoreCursor()
                dialog = ChooseClassNameDlg(
                    parent=main, 
                    badName=codeStructure, 
                    classesNames=classesNames, 
                    eleve=elevesInXML[id_eleve]['nomComplet'])
                if dialog.exec_() != QtWidgets.QDialog.Accepted:
                    return
                utils_functions.doWaitCursor()
                if dialog.classComboBox.currentIndex() == 1:
                    # on passe cet élève
                    item = ''
                elif dialog.classComboBox.currentIndex() == 0:
                    # on ajoute la classe
                    newName = dialog.classNameEdit.text()
                    classType = dialog.classTypeComboBox.currentIndex()
                    notes = 0
                    if dialog.notesCheckBox.isChecked():
                        notes = 1
                    maxId += 1
                    id_classe = maxId
                    maxOrdre += 1
                    ordre = maxOrdre
                    # on ajoute la nouvelle classe :
                    datas['classes'].append(
                        (id_classe, newName, classType, notes, ordre))
                    # si on l'a renommée, on gère les remplacements :
                    if newName != codeStructure:
                        namesReplacements[codeStructure] = newName
                        mustRecreateTable['replacements'] = True
                    item = newName
                else:
                    item = dialog.classComboBox.currentText()
                if dialog.choiceComboBox.currentIndex() == 0:
                    namesReplacements[codeStructure] = item
                    mustRecreateTable['replacements'] = True
                EleveClasse = item
                utils_functions.restoreCursor()
                utils_functions.doWaitCursor()

            if EleveClasse == '???':
                # PB Vincent L
                EleveClasse = ''
                mustInsertEleve = True
                id2id[id_eleve] = eleve_id
            elif EleveClasse == '':
                mustInsertEleve = False
            if mustInsertEleve:
                if id_eleve in usersDict:
                    # cet élève est déjà là ; si possible, on garde login et mdp.
                    data_users = usersDict[id_eleve]
                    # si l'ancien login avait été fait à la main, on le garde :
                    oldLogin = data_users['Login']
                    oldLoginVerac = calculeLogin(data_users['Prenom'], data_users['NOM'])
                    if not(compareLogins(oldLoginVerac, oldLogin)):
                        # c'est donc le cas ; on garde l'ancien login :
                        EleveLogin = oldLogin
                    elif (EleveNom == data_users['NOM']) and (ElevePrenom == data_users['Prenom']):
                        # nom et prénom n'ont pas changé ; on garde l'ancien login :
                        EleveLogin = oldLogin
                    else:
                        # on crée le nouveau login :
                        EleveLogin, users_login = createLogin(
                            ElevePrenom, EleveNom, users_login, oldLogin=oldLogin)
                    # si l'ancien mdp avait été fait à la main, on le garde :
                    oldMdp = data_users['Mdp']
                    oldDateNaiss = data_users['Date_naiss']
                    try:
                        # car un mot de passe fait à la main n'est pas forcément un nombre
                        oldMdpInt = int(oldMdp)
                    except:
                        oldMdpInt = 0
                    if oldMdpInt != oldDateNaiss:
                        # c'est donc le cas ; on garde l'ancien mot de passe :
                        EleveMdp = oldMdp
                    elif int(EleveDateNaiss) == oldDateNaiss:
                        # date de naissance n'a pas été changée ; on garde l'ancien mdp :
                        EleveMdp = oldMdp
                    else:
                        # on modifie le mdp :
                        EleveMdp = EleveDateNaiss
                else:
                    # c'est donc un nouvel élève :
                    EleveLogin, users_login = createLogin(
                        ElevePrenom, EleveNom, users_login)
                    EleveMdp = EleveDateNaiss

                datas['eleves'].append((
                    id_eleve, 
                    elevesInXML[id_eleve]['num'], 
                    EleveNom, 
                    ElevePrenom, 
                    EleveClasse, 
                    EleveLogin, 
                    EleveDateNaiss, 
                    EleveMdp, 
                    elevesInXML[id_eleve]['AnDernier'], 
                    eleve_id, 
                    EleveDateEntree, 
                    EleveDateSortie, 
                    EleveSexe))
                textEleve = utils_functions.u('{0} {1} ({2})').format(
                    EleveNom, ElevePrenom, EleveClasse)
                utils_functions.afficheMessage(main, textEleve, editLog=True)
                id2id[id_eleve] = eleve_id

        # vérification de la table adresses :
        dicoAdresses = {}
        # on recrée la table adresses si besoin :
        commandLine_admin = utils_db.listTablesAdmin['adresses'][0]
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        # on ne garde que les élèves présents dans le fichier :
        commandLine_admin = utils_db.q_selectAllFromOrder.format(
            'adresses', 'id_eleve')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            eleve_id = id2id.get(id_eleve, -1)
            if eleve_id > -1:
                if int(query_admin.value(1)) != eleve_id:
                    mustRecreateTable['adresses'] = True
                state = int(query_admin.value(2))
                nom1 = query_admin.value(3)
                nom2 = query_admin.value(4)
                adresse = query_admin.value(5)
                use = int(query_admin.value(6))
                if not(id_eleve in dicoAdresses):
                    dicoAdresses[id_eleve] = {}
                dicoAdresses[id_eleve][state] = (
                    nom1, nom2, adresse, use)
            else:
                mustRecreateTable['adresses'] = True

        # si on a utilisé le fichier ElevesAvecAdresses, 
        # on vérifie les adresses des élèves :
        if len(elevesAdresses) > 0:
            mustDo = {'DELETE': [], 'UPDATE': [], 'INSERT': [], 'IN_DB': {}}
            commandLine_admin = utils_db.q_selectAllFromWhere.format(
                'adresses', 'state', 1)
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                nomEx = query_admin.value(3)
                adresseEx = query_admin.value(5)
                # l'élève est donc déjà présent dans la base :
                mustDo['IN_DB'][id_eleve] = (nomEx, adresseEx)
                if id_eleve in elevesAdresses:
                    (eleve_id, nom, adresse) = elevesAdresses[id_eleve]
                    if (nom != nomEx) or (adresse != adresseEx):
                        # il a été modifié :
                        mustDo['UPDATE'].append(id_eleve)
                else:
                    # son adresse a été supprimée :
                    mustDo['DELETE'].append(id_eleve)
            for id_eleve in elevesAdresses:
                if not(id_eleve in mustDo['IN_DB']):
                    # son adresse n'était pas encore dans la base :
                    mustDo['INSERT'].append(id_eleve)
            # on y va :
            for id_eleve in mustDo['DELETE']:
                try:
                    del dicoAdresses[id_eleve][1]
                    mustRecreateTable['adresses'] = True
                except:
                    pass
                try:
                    del dicoAdresses[id_eleve][2]
                    mustRecreateTable['adresses'] = True
                except:
                    pass
            for id_eleve in mustDo['UPDATE']:
                try:
                    del dicoAdresses[id_eleve][1]
                except:
                    pass
                mustDo['INSERT'].extend(mustDo['UPDATE'])
            for id_eleve in mustDo['INSERT']:
                (eleve_id, nom, adresse) = elevesAdresses[id_eleve]
                if not(id_eleve in dicoAdresses):
                    dicoAdresses[id_eleve] = {}
                dicoAdresses[id_eleve][1] = (nom, '', adresse, 1)
                mustRecreateTable['adresses'] = True
        # si l'élève n'a pas déjà d'adresse de responsable, 
        # on ajoutera une ligne vide (state = -1) :
        inAdresses = []
        commandLine_admin = (
            'SELECT DISTINCT id_eleve, eleve_id FROM adresses '
            'WHERE NOT(state IN (1, 2))')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            eleve_id = int(query_admin.value(1))
            inAdresses.append((id_eleve, eleve_id))
        for id_eleve in id2id:
            eleve_id = id2id[id_eleve]
            if not((id_eleve, eleve_id) in inAdresses):
                if not(id_eleve in dicoAdresses):
                    dicoAdresses[id_eleve] = {}
                dicoAdresses[id_eleve][-1] = ('', '', '', 0)
                mustRecreateTable['adresses'] = True
        # on écrit les nouvelles lignes de la table adresses :
        if mustRecreateTable['adresses']:
            for id_eleve in dicoAdresses:
                eleve_id = id2id[id_eleve]
                for state in dicoAdresses[id_eleve]:
                    (nom1, nom2, adresse, use) = dicoAdresses[id_eleve][state]
                    datas['adresses'].append(
                        (id_eleve, eleve_id, state, nom1, nom2, adresse, use))
        if mustRecreateTable['replacements']:
            for replaceWhat in namesReplacements:
                replaceWith = namesReplacements[replaceWhat]
                datas['replacements'].append(
                    (replaceWhere, replaceWhat, replaceWith))

        OK = True
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if not(OK):
            message = QtWidgets.QApplication.translate(
                'main', 'Importing the file failed or you have canceled.')
            utils_functions.messageBox(main, level='warning', message=message)
        elif msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
        return OK, datas

def addressesFromSIECLE(main, fileName, msgFin=True):
    """
    """
    utils_functions.doWaitCursor()
    OK = False
    datas = []
    query_admin = utils_db.query(db_admin)

    try:
        import utils_export_xml
        (ok, responsables, personnes, adresses) = \
            utils_export_xml.addressesFromSIECLE(main, fileName)
        if not(ok):
            return

        # liste des élèves d'après la table admin.eleves :
        students = []
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            students.append(int(query_admin.value(0)))

        # on recrée la table adresses si besoin :
        commandLine_admin = utils_db.listTablesAdmin['adresses'][0]
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        # liste de ceux qui sont dans la table admin.adresses :
        id2id = {}
        commandLine_admin = (
            'SELECT DISTINCT id_eleve, eleve_id '
            'FROM adresses')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            eleve_id = int(query_admin.value(1))
            if id_eleve in students:
                id2id[id_eleve] = eleve_id

        mustDo = {'IN_FILE': [], 'DELETE': [], 'UPDATE': [], 'INSERT': [], 'IN_DB': {}}
        dicoAdresses = {}
        for id_eleve in id2id:
            eleve_id = id2id[id_eleve]
            dicoAdresses[id_eleve] = {'eleve_id': eleve_id, 10: [], 20: [], 50: [], 100: []}
            if not(eleve_id in responsables):
                responsables[eleve_id] = {0: -1, 1: -1, 2: -1}
            legal0 = responsables[eleve_id][0]
            adresse0 = '0'
            if legal0 > 0:
                try:
                    (nom0, adresse_id) = personnes[legal0]
                    adresse0 = adresses[adresse_id]
                    if len(adresse0) < 10:
                        adresse0 = '0'
                except:
                    adresse0 = '0'
            legal1 = responsables[eleve_id][1]
            adresse1 = '1'
            if legal1 > 0:
                try:
                    (nom1, adresse_id) = personnes[legal1]
                    adresse1 = adresses[adresse_id]
                    if len(adresse1) < 10:
                        adresse1 = '1'
                except:
                    adresse1 = '1'
            legal2 = responsables[eleve_id][2]
            adresse2 = '2'
            if legal2 > 0:
                try:
                    (nom2, adresse_id) = personnes[legal2]
                    adresse2 = adresses[adresse_id]
                    if len(adresse2) < 10:
                        adresse2 = '2'
                except:
                    adresse2 = '2'
            if (len(adresse0) > 9) or (len(adresse1) > 9) or (len(adresse2) > 9):
                mustDo['IN_FILE'].append(id_eleve)
            if adresse1.upper() == adresse0.upper():
                dicoAdresses[id_eleve][50] = [nom0, nom1, adresse0]
                if len(adresse2) > 9:
                    dicoAdresses[id_eleve][100] = [nom2, '', adresse2]
            elif adresse1.upper() == adresse2.upper():
                dicoAdresses[id_eleve][50] = [nom1, nom2, adresse1]
                if len(adresse0) > 9:
                    dicoAdresses[id_eleve][100] = [nom0, '', adresse0]
            elif adresse2.upper() == adresse0.upper():
                dicoAdresses[id_eleve][50] = [nom0, nom2, adresse0]
                if len(adresse1) > 9:
                    dicoAdresses[id_eleve][100] = [nom1, '', adresse1]
            else:
                if len(adresse0) > 9:
                    dicoAdresses[id_eleve][10] = [nom0, '', adresse0]
                if len(adresse1) > 9:
                    dicoAdresses[id_eleve][20] = [nom1, '', adresse1]
                if len(adresse2) > 9:
                    dicoAdresses[id_eleve][100] = [nom2, '', adresse2]
        commandLine_admin = (
            'SELECT * FROM adresses '
            'WHERE state IN (10, 20, 50, 100)')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            state = int(query_admin.value(2))
            nom1Ex = query_admin.value(3)
            nom2Ex = query_admin.value(4)
            adresseEx = query_admin.value(5)
            # l'élève est donc déjà présent dans la base :
            if not(id_eleve in mustDo['IN_DB']):
                mustDo['IN_DB'][id_eleve] = {}
            mustDo['IN_DB'][id_eleve][state] = (nom1Ex, nom2Ex, adresseEx)
            if id_eleve in dicoAdresses:
                if len(dicoAdresses[id_eleve][state]) > 0:
                    [nom1, nom2, adresse] = dicoAdresses[id_eleve][state]
                    if (nom1 != nom1Ex) or (nom2 != nom2Ex) or (adresse != adresseEx):
                        # l'adresse a été modifiée :
                        mustDo['UPDATE'].append(id_eleve)
            else:
                # son adresse a été supprimée :
                mustDo['DELETE'].append(id_eleve)
        for id_eleve in mustDo['IN_FILE']:
            if not(id_eleve in mustDo['IN_DB']):
                # son adresse n'était pas encore dans la base :
                mustDo['INSERT'].append(id_eleve)

        for id_eleve in id2id:
            eleve_id = dicoAdresses[id_eleve]['eleve_id']
            legalOk = False
            if len(dicoAdresses[id_eleve][50]) > 0:
                legalOk = True
                line = [id_eleve, eleve_id, 50]
                line.extend(dicoAdresses[id_eleve][50])
                line.append(1)
                datas.append(line)
            else:
                if len(dicoAdresses[id_eleve][10]) > 0:
                    legalOk = True
                    line = [id_eleve, eleve_id, 10]
                    line.extend(dicoAdresses[id_eleve][10])
                    line.append(1)
                    datas.append(line)
                if len(dicoAdresses[id_eleve][20]) > 0:
                    legalOk = True
                    line = [id_eleve, eleve_id, 20]
                    line.extend(dicoAdresses[id_eleve][20])
                    line.append(1)
                    datas.append(line)
            if len(dicoAdresses[id_eleve][100]) > 0:
                legalOk = True
                line = [id_eleve, eleve_id, 100]
                line.extend(dicoAdresses[id_eleve][100])
                line.append(1)
                datas.append(line)
            if not(legalOk):
                # si l'élève n'a pas d'adresse de responsable, 
                # on ajoutera une ligne vide (state = -1) :
                line = (id_eleve, eleve_id, -1, '', '', '', 0)
                datas.append(line)
        # on garde les adresses perso :
        commandLine_admin = (
            'SELECT * FROM adresses '
            'WHERE NOT(state IN (10, 20, 50, 100))')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            eleve_id = int(query_admin.value(1))
            state = int(query_admin.value(2))
            nom1 = query_admin.value(3)
            nom2 = query_admin.value(4)
            adresse = query_admin.value(5)
            use = int(query_admin.value(6))
            line = (id_eleve, eleve_id, state, nom1, nom2, adresse, use)
            datas.append(line)

        OK = True
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if not(OK):
            message = QtWidgets.QApplication.translate(
                'main', 'Importing the file failed or you have canceled.')
            utils_functions.messageBox(main, level='warning', message=message)
        elif msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
        return OK, datas

def phonesAndMailsFromSIECLE(main, fileName, msgFin=True):
    """
    """
    utils_functions.doWaitCursor()
    OK = False
    data = []
    query_admin = utils_db.query(db_admin)
    try:
        import utils_export_xml
        (ok, responsables, personnes) =  \
            utils_export_xml.phonesAndMailsFromSIECLE(main, fileName)
        if not(ok):
            return

        # liste des élèves d'après la table admin.eleves :
        students = {}
        orderStudents = []
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_admin, order=['Classe', 'NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            orderStudents.append(record[0])
            students[int(record[0])] = (record[2], record[3], record[4])

        # on recrée la table adresses si besoin :
        commandLine_admin = utils_db.listTablesAdmin['adresses'][0]
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        # liste de ceux qui sont dans la table admin.adresses :
        id2id = {}
        commandLine_admin = (
            'SELECT DISTINCT id_eleve, eleve_id '
            'FROM adresses')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            eleve_id = int(query_admin.value(1))
            if id_eleve in orderStudents:
                id2id[id_eleve] = eleve_id

        dicoPhonesAndMails = {}
        for id_eleve in orderStudents:
            if not(id_eleve in id2id):
                continue
            eleve_id = id2id[id_eleve]
            dicoPhonesAndMails[id_eleve] = {'eleve_id': eleve_id}
            line = [id_eleve, ]
            for d in students[id_eleve]:
                line.append(d)
            if not(eleve_id in responsables):
                responsables[eleve_id] = {0: -1, 1: -1, 2: -1}
            for legalLevel in (1, 2, 0):
                dicoPhonesAndMails[id_eleve][legalLevel] = ('', '', '', '', '')
                legal_id = responsables[eleve_id][legalLevel]
                if legal_id > 0:
                    try:
                        dicoPhonesAndMails[id_eleve][legalLevel] = personnes[legal_id]
                    except:
                        pass
                for d in dicoPhonesAndMails[id_eleve][legalLevel]:
                    line.append(d)
            data.append(line)
        OK = True
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if not(OK):
            message = QtWidgets.QApplication.translate(
                'main', 'Importing the file failed or you have canceled.')
            utils_functions.messageBox(main, level='warning', message=message)
        elif msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
        return OK, data


def createTableBilansLiens(main):
    # ICI bilans_liens
    return
    """
    Dans le cas de bulletins (ou autres) multiples (utilisation de plusieurs classestypes),
    certaines lignes de la table bulletin peuvent être répétées.
    On met à jour dans cette procédure la table bilans_liens,
    qui sert à repérer les lignes ainsi répétées.
    """
    utils_functions.doWaitCursor()
    try:
        query_commun, transactionOK = utils_db.queryTransactionDB(main.db_commun)

        # récupération des équivalences dans une liste :
        equivalencesList = []
        for tableName in ['bulletin', 'referentiel', 'confidentiel']:
            commandLine = utils_db.q_selectAllFromOrder.format(tableName, 'code')
            utils_functions.afficheMessage(main, commandLine, editLog=True)
            equivalence = ('', '', [])
            # structure d'une equivalence : tableName, code, liste des (id, classeType)
            query_commun = utils_db.queryExecute(commandLine, query=query_commun)
            while query_commun.next():
                id = int(query_commun.value(0))
                code = query_commun.value(1)
                classeType = int(query_commun.value(11))
                if code != equivalence[1]:
                    # nouvelle equivalence
                    if len(equivalence[2]) > 1:
                        equivalencesList.append(equivalence)
                    equivalence = (tableName, code, [(id, classeType)])
                else:
                    # on ajoute (id, classeType) à la liste
                    equivalence[2].append((id, classeType))

        # recréation de la table :
        commandLine = utils_db.qdf_table.format('bilans_liens')
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        commandLine = utils_db.qct_commun_bilans_liens
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)

        # et remplissage :
        lines = []
        commandLine = utils_db.insertInto('bilans_liens', 7)
        for equivalence in equivalencesList:
            tableName = equivalence[0]
            code = equivalence[1]
            # on récupère les id :
            idList = []
            for a in equivalence[2]:
                idList.append(a[0])
            idList.sort()
            id1 = idList[0]
            for id2 in idList[1:]:
                lines.append((tableName, code, id1, id2))
        query_commun = utils_db.queryExecute(
            {commandLine: lines}, query=query_commun)
    finally:
        utils_db.endTransaction(query_commun, main.db_commun, transactionOK)
        utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
        utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, dbFileFtp_commun)
        utils_functions.restoreCursor()


###########################################################"
#   FIN DE : CRÉATION DES TABLES
###########################################################"





###########################################################"
#   GESTION DES UTILISATEURS
#   (création, mise à jour des tables profs et eleves)
#   (base users)
###########################################################"


def updateUsersBase(main, mode='all', msgFin=True):
    """
    """
    utils_functions.doWaitCursor()
    try:
        query_users, transactionOK = utils_db.queryTransactionDB(main.db_users)
        query_admin = utils_db.query(db_admin)

        # des message pour la traduction :
        changesText = QtWidgets.QApplication.translate('main', 'CHANGES:')
        noChangeText = QtWidgets.QApplication.translate('main', 'NO CHANGE')

        if mode != 'profs':
            # une liste pour la nouvelle table users.eleves :
            newTable = []
            # 3 autres pour indiquer les nouveaux, les sortants, les modifiés et les autres :
            newUsers, deletedUsers, updatedUsers = [], [], []
            # et des message pour la traduction :
            newUserText = QtWidgets.QApplication.translate('main', 'NEW STUDENT')
            deleteUserText = QtWidgets.QApplication.translate('main', 'DELETED STUDENT')
            updatedUserText = QtWidgets.QApplication.translate('main', 'UPDATED STUDENT')

            # on récupère l'ancienne version de la table users.eleves dans un dico :
            usersDic = {}
            commandLine_users = utils_db.q_selectAllFrom.format('eleves')
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            while query_users.next():
                id_users = int(query_users.value(0))
                nom_users = query_users.value(1)
                prenom_users = query_users.value(2)
                classe_users = query_users.value(3)
                login_users = query_users.value(4)
                mdp_users = query_users.value(5)
                initialMdp_users = query_users.value(6)
                sexe_users = query_users.value(7)
                dateNaissance_users = query_users.value(8)
                data = (
                    nom_users, prenom_users, classe_users, 
                    login_users, mdp_users, initialMdp_users, 
                    sexe_users, dateNaissance_users)
                usersDic[id_users] = data

            # on liste la nouvelle version de la table admin.eleves :
            commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_admin = int(query_admin.value(0))
                #num_admin = query_admin.value(1)
                nom_admin = query_admin.value(2)
                prenom_admin = query_admin.value(3)
                classe_admin = query_admin.value(4)
                login_admin = utils_functions.doEncode(query_admin.value(5), algo='sha256')
                #dateNaissance_admin = query_admin.value(6)
                mdp_admin = utils_functions.doEncode(query_admin.value(7), algo='sha256')
                initialMdp_admin = mdp_admin
                sexe_admin = query_admin.value(12)
                dateNaissance_admin = query_admin.value(6)
                try:
                    # si l'élève était déjà là, on récupère son mdp
                    # qu'il a peut-être changé :
                    data_users = usersDic[id_admin]
                    nom_users = data_users[0]
                    prenom_users = data_users[1]
                    # un test pour être certain que c'est le même élève
                    # on ne l'écrabouillera que si le nom et le prénom ont changé :
                    if (nom_users == nom_admin) or (prenom_users == prenom_admin):
                        mdp_admin = data_users[4]
                        # on ne le met dans la liste updatedUsers que si quelquechose
                        # a changé (nom, prénom, classe ou login) :
                        classe_users = data_users[2]
                        login_users = data_users[3]
                        infos_users = (nom_users, prenom_users, classe_users, login_users)
                        infos_admin = (nom_admin, prenom_admin, classe_admin, login_admin)
                        if infos_users != infos_admin:
                            textUser = '{0} : {1} {2} ({3}) -> {4} {5} ({6})'
                            textUser = utils_functions.u(textUser).format(
                                updatedUserText, 
                                nom_users, prenom_users, classe_users, 
                                nom_admin, prenom_admin, classe_admin)
                            utils_functions.appendUnique(updatedUsers, textUser)
                    else:
                        # sinon on le considère comme nouvel élève :
                        textUser = utils_functions.u('{0} : {1} {2} ({3})').format(
                            newUserText, nom_admin, prenom_admin, classe_admin)
                        utils_functions.appendUnique(newUsers, textUser)
                    # on supprime l'élève du dico
                    # (permettra de savoir qui a été supprimé des listes) :
                    del usersDic[id_admin]
                except:
                    # c'est donc un nouvel élève
                    textUser = utils_functions.u('{0} : {1} {2} ({3})').format(
                        newUserText, nom_admin, prenom_admin, classe_admin)
                    utils_functions.appendUnique(newUsers, textUser)
                    pass
                # on a les données à inscrire dans users.eleves :
                data = (
                    id_admin, nom_admin, prenom_admin, classe_admin, 
                    login_admin, mdp_admin, initialMdp_admin, 
                    sexe_admin, dateNaissance_admin)
                utils_functions.appendUnique(newTable, data)

            # on remplit la liste des élèves supprimés :
            for id_users in usersDic:
                data_users = usersDic[id_users]
                nom_users = data_users[0]
                prenom_users = data_users[1]
                classe_users = data_users[2]
                textUser = utils_functions.u('{0} : {1} {2} ({3})').format(
                    deleteUserText, nom_users, prenom_users, classe_users)
                utils_functions.appendUnique(deletedUsers, textUser)

            # on recrée la table users.eleves :
            commandLine_users = utils_db.qdf_table.format('eleves')
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            commandLine_users = utils_db.qct_users_eleves
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            commandLine_users = utils_db.insertInto('eleves', 9)
            query_users = utils_db.queryExecute(
                {commandLine_users: newTable}, query=query_users)

            # on affiche les 3 listes d'élèves :
            utils_functions.afficheMessage(main, changesText, tags=('h2'))
            for textUser in updatedUsers:
                utils_functions.afficheMessage(main, textUser, editLog=True)
            for textUser in deletedUsers:
                utils_functions.afficheMessage(main, textUser, editLog=True)
            for textUser in newUsers:
                utils_functions.afficheMessage(main, textUser, editLog=True)
            if (len(updatedUsers) + len(deletedUsers) + len(newUsers) < 1):
                utils_functions.afficheMessage(main, noChangeText, editLog=True)
            utils_functions.afficheMessage(main, '', editLog=True, p=True)

        if mode != 'eleves':
            # une liste pour la nouvelle table users.profs :
            newTable = []
            # 3 autres pour indiquer les nouveaux, les sortants et les autres :
            newUsers, deletedUsers, updatedUsers = [], [], []
            # et des message pour la traduction :
            newUserText = QtWidgets.QApplication.translate('main', 'NEW TEACHER')
            deleteUserText = QtWidgets.QApplication.translate('main', 'DELETED TEACHER')
            updatedUserText = QtWidgets.QApplication.translate('main', 'UPDATED TEACHER')

            # on récupère l'ancienne version de la table users.profs dans un dico :
            usersDic = {}
            commandLine_users = utils_db.q_selectAllFrom.format('profs')
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            while query_users.next():
                id_users = int(query_users.value(0))
                nom_users = query_users.value(1)
                prenom_users = query_users.value(2)
                login_users = query_users.value(3)
                mdp_users = query_users.value(4)
                initialMdp_users = query_users.value(5)
                matiere_users = query_users.value(6)
                data = (nom_users, prenom_users, login_users, 
                        mdp_users, initialMdp_users, matiere_users)
                usersDic[id_users] = data

            # on liste la nouvelle version de la table admin.profs :
            commandLine_admin = utils_db.q_selectAllFrom.format('profs')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_admin = int(query_admin.value(0))
                #num_admin = query_admin.value(1)
                nom_admin = query_admin.value(2)
                prenom_admin = query_admin.value(3)
                matiere_adminCode = query_admin.value(4)
                matiere_admin = MATIERES['Code2Matiere'].get(matiere_adminCode, (0, '', ''))[1]
                login_admin = utils_functions.doEncode(query_admin.value(5), algo='sha256')
                mdp_admin = utils_functions.doEncode(query_admin.value(6), algo='sha256')
                initialMdp_admin = mdp_admin
                try:
                    # si le prof était déjà là, on récupère son mdp
                    # qu'il a peut-être changé :
                    data_users = usersDic[id_admin]
                    nom_users = data_users[0]
                    prenom_users = data_users[1]
                    mdp_users = data_users[3]
                    # un test pour être certain que c'est le même prof
                    # on ne l'écrabouillera que si le nom et le prénom ont changé :
                    if (nom_users == nom_admin) or (prenom_users == prenom_admin):
                        mdp_admin = mdp_users
                        # on ne le met dans la liste updatedUsers que si quelquechose
                        # a changé (nom, prénom, login ou matière) :
                        login_users = data_users[2]
                        matiere_users = data_users[5]
                        infos_users = (nom_users, prenom_users, login_users, matiere_users)
                        infos_admin = (nom_admin, prenom_admin, login_admin, matiere_admin)
                        if infos_users != infos_admin:
                            textUser = '{0} : {1} {2} ({3}) -> {4} {5} ({6})'
                            textUser = utils_functions.u(textUser).format(
                                updatedUserText, 
                                nom_users, prenom_users, matiere_users, 
                                nom_admin, prenom_admin, matiere_admin)
                            utils_functions.appendUnique(updatedUsers, textUser)
                    else:
                        # sinon on le considère comme nouveau prof :
                        textUser = utils_functions.u('{0} : {1} {2} ({3})').format(
                            newUserText, nom_admin, prenom_admin, matiere_admin)
                        utils_functions.appendUnique(newUsers, textUser)
                    # on supprime le prof du dico
                    # (permettra de savoir qui a été supprimé des listes) :
                    del usersDic[id_admin]
                except:
                    # c'est donc un nouveau prof
                    textUser = utils_functions.u('{0} : {1} {2} ({3})').format(
                        newUserText, nom_admin, prenom_admin, matiere_admin)
                    utils_functions.appendUnique(newUsers, textUser)
                    pass
                # on a les données à inscrire dans users.profs :
                data = (id_admin, nom_admin, prenom_admin, 
                        login_admin, mdp_admin, initialMdp_admin, matiere_admin)
                utils_functions.appendUnique(newTable, data)

            # on remplit la liste des profs supprimés :
            for id_users in usersDic:
                data_users = usersDic[id_users]
                nom_users = data_users[0]
                prenom_users = data_users[1]
                matiere_users = data_users[5]
                textUser = utils_functions.u('{0} : {1} {2} ({3})').format(
                    deleteUserText, nom_users, prenom_users, matiere_users)
                utils_functions.appendUnique(deletedUsers, textUser)

            # on recrée la table users.profs :
            commandLine_users = utils_db.qdf_table.format('profs')
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            commandLine_users = utils_db.qct_users_profs
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            commandLine_users = utils_db.insertInto('profs', 7)
            query_users = utils_db.queryExecute(
                {commandLine_users: newTable}, query=query_users)

            # on affiche les 3 listes de profs :
            utils_functions.afficheMessage(main, changesText, tags=('h2'))
            for textUser in updatedUsers:
                utils_functions.afficheMessage(main, textUser, editLog=True)
            for textUser in deletedUsers:
                utils_functions.afficheMessage(main, textUser, editLog=True)
            for textUser in newUsers:
                utils_functions.afficheMessage(main, textUser, editLog=True)
            if (len(updatedUsers) + len(deletedUsers) + len(newUsers) < 1):
                utils_functions.afficheMessage(main, noChangeText, editLog=True)
            utils_functions.afficheMessage(main, '', editLog=True, p=True)

    finally:
        utils_db.endTransaction(query_users, main.db_users, transactionOK)
        utils_filesdirs.removeAndCopy(main.dbFileTemp_users, main.dbFile_users)
        utils_filesdirs.removeAndCopy(main.dbFileTemp_users, dbFileFtp_users)

        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)

def manageDB(main, what='', indexTab=0, modified=False):
    """
    lance le dialog de configuration des bases (admin_edit.ManageDatabasesDlg).
        * what indique le type de configuration :
            * ConfigurationSchool
            * Classes
            * SharedCompetences
            * Users
            * Addresses
            * WebSite
        * indexTab permet de sélectionner un onglet
        * modified si la configuration est modifiée dès le départ
          (utilisé avec WebSite si on télécharge l'état du site)
    """
    # des trucs à faire avant dans certains cas :
    if what == 'WebSite':
        # on conserve les variables de connexion en cas d'annulation :
        global dirSitePublic, dirSiteSecret, dirSitePrive
        oldFtpHost = admin_ftp.FTP_HOST
        oldFtpUser = admin_ftp.FTP_USER
        oldFtpPass = admin_ftp.FTP_PASS
        dirSitePublic_EX = dirSitePublic
        dirSiteSecret_EX = dirSiteSecret
        dirSitePrive_EX = dirSitePrive
    elif what == 'Users':
        # on propose de télécharger la base users avant de continuer :
        mustDownloadDBs(main)

    # on lance le dialog :
    dialog = admin_edit.ManageDatabasesDlg(
        parent=main, what=what, indexTab=indexTab, modified=modified)
    lastState = main.disableInterface(dialog)
    dialogResponse = dialog.exec_()
    if dialogResponse != QtWidgets.QDialog.Accepted:
        if not(dialog.applyed):
            if what == 'WebSite':
                # on restaure les variables de connexion :
                admin_ftp.ftpUpdateConfig(
                    newFtpHost=oldFtpHost, 
                    newFtpUser=oldFtpUser, 
                    newFtpPass=oldFtpPass)
                dirSitePublic = dirSitePublic_EX
                dirSiteSecret = dirSiteSecret_EX
                dirSitePrive = dirSitePrive_EX
            main.enableInterface(dialog, lastState)
            return
    main.enableInterface(dialog, lastState)
    main.editLog2 = None

    # on enregistre les modifications dans les bases :
    if dialogResponse == QtWidgets.QDialog.Accepted:
        for tab in dialog.tabs:
            tab.checkModifications()
    # on met à jour les versions locales des bases (depuis le dossier temp) :
    if dialog.modifiedTables['DATABASES']['admin']:
        utils_filesdirs.removeAndCopy(dbFileTemp_admin, dbFile_admin)
    if dialog.modifiedTables['DATABASES']['commun']:
        utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
        utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, dbFileFtp_commun)
    # on recrée les fichier csv :
    exportModifIntoCSV(main, dialog.modifiedTables)

    # DES TRUCS À FAIRE SELON CE QUI A ÉTÉ MODIFIÉ :

    if dialog.modifiedTables['commun']['periodes']:
        # on met à jour la période actuelle :
        utils.changePeriodes(main)
        main.initPeriodeMenu()
        if utils.selectedPeriod < utils.NB_PERIODES:
            utils.changeSelectedPeriod(0)
    if dialog.modifiedTables['commun']['config']:
        # on met à jour des variables :
        global prefixProfFiles
        config = {}
        query_commun = utils_db.query(main.db_commun)
        commandLine_commun = utils_db.q_selectAllFrom.format('config')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            config[query_commun.value(0)] = (
                query_commun.value(1), query_commun.value(2))
        prefixProfFiles = config.get('prefixProfFiles', (0, ''))[1]
        main.siteUrlBase = config.get('siteUrlBase', (0, ''))[1]
        main.siteUrlPublic = config.get('siteUrlPublic', (0, ''))[1]
        main.siteUrlPublic = utils_functions.removeSlash(
            main.siteUrlPublic)
        main.limiteBLTPerso = config.get('limiteBLTPerso', (4, ''))[0]
        main.annee_scolaire = config.get('annee_scolaire', (0, ''))
        if main.annee_scolaire[0] < 1:
            main.annee_scolaire = utils_functions.calcAnneeScolaire()
        # on doit aussi mettre à jour l'adresse du site web
        # dans la base db_localConfig :
        commandLine = 'UPDATE versions SET url="{0}" WHERE id_version={1}'
        commandLine = utils_functions.u(commandLine).format(
            main.siteUrlPublic, main.actualVersion['id_version'])
        query = utils_db.query(main.db_localConfig)
        query = utils_db.queryExecute(commandLine, query=query)
        # si on est en réseau, on fait de même dans db_lanConfig :
        if main.db_lanConfig != None:
            query = utils_db.query(main.db_lanConfig)
            query = utils_db.queryExecute(commandLine, query=query)
            dbFile_lanConfig = utils.lanConfigDir + '/config.sqlite'
            fileTemp = utils_functions.u('{0}/lanConfig.sqlite').format(main.tempPath)
            utils_filesdirs.removeAndCopy(fileTemp, dbFile_lanConfig)
    if dialog.modifiedTables['admin']['config_admin']:
        # on met à jour des variables de connexion :
        ftpHost = utils_db.readInConfigTable(
            db_admin, 'baseSiteFtp', 'config_admin')[1]
        ftpUser = utils_db.readInConfigTable(
            db_admin, 'userFtp', 'config_admin')[1]
        ftpPass = utils_db.readInConfigTable(
            db_admin, 'mdpFtp', 'config_admin')[1]
        admin_ftp.ftpUpdateConfig(
            newFtpHost=ftpHost, newFtpUser=ftpUser, newFtpPass=ftpPass)
        dirSitePublic = utils_db.readInConfigTable(
            db_admin, 'dirSitePublic', 'config_admin')[1]
        dirSiteSecret = utils_db.readInConfigTable(
            db_admin, 'dirSiteSecret', 'config_admin')[1]
        dirSitePrive = dirSiteSecret + '/verac'
    # on met à jour configweb après avoir mis à jour config_admin :
    if dialog.modifiedTables['DATABASES']['configweb']:
        import admin_web
        utils_filesdirs.removeAndCopy(
            dbFileTemp_configweb, dbFile_configweb, testSize=10)
        admin_web.createConfigWebDB(
            main, doDBConfig=True, doConfig=True, postDB=False)

    # si on a recréé une table de compétences de la base commun,
    # il faut refaire la table bilans_liens :
    mustDo = False
    if dialog.modifiedTables['commun']['bulletin']:
        mustDo = True
    if dialog.modifiedTables['commun']['referentiel']:
        mustDo = True
    if dialog.modifiedTables['commun']['confidentiel']:
        mustDo = True
    if mustDo:
        createTableBilansLiens(main)

    # pour certaines tables, si la base commun-x n'existe pas,
    # il faut recréer les listes globales issues de la base commun :
    mustDo = False
    if dialog.modifiedTables['commun']['matieres']:
        mustDo = True
    elif dialog.modifiedTables['commun']['sous_rubriques']:
        mustDo = True
    elif dialog.modifiedTables['commun']['classes']:
        mustDo = True
    elif dialog.modifiedTables['admin']['eleves']:
        mustDo = True
    if mustDo:
        dbFileX = dirLocalPrive + '/public/commun-{0}.sqlite'.format(utils.selectedPeriod)
        if not(QtCore.QFileInfo(dbFileX).exists()):
            initFromCommun(main, main.db_commun)

    if dialog.modifiedTables['admin']['profs']:
        # on recrée la liste PROFS et on met à jour la base users :
        global PROFS
        PROFS = []
        query_admin = utils_db.query(db_admin)
        commandLine_admin = utils_db.q_selectAllFrom.format('profs')
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            prof_id = int(record[0])
            prof_file = utils_functions.u('{0}{1}').format(prefixProfFiles, prof_id)
            prof_nom = record[2]
            prof_prenom = record[3]
            utils_functions.appendUnique(
                PROFS, 
                (prof_id, prof_file, prof_nom, prof_prenom))
        updateUsersBase(main, mode='profs', msgFin=False)
    if dialog.modifiedTables['admin']['eleves']:
        # on met à jour la base users :
        updateUsersBase(main, mode='eleves', msgFin=False)

    # enfin on envoie les bases :
    databases = []
    for database in ('commun', 'users', 'configweb'):
        if dialog.modifiedTables['DATABASES'][database]:
            databases.append(database)
    if len(databases) > 0:
        mustUploadDBs(main, databases=databases)

def exportModifIntoCSV(main, modifiedTables={}):
    """
    recréation des fichiers csv après modifications des bases (pour compatibilité).
    """
    OK = False
    try:
        if modifiedTables == {}:
            # on refait tout :
            default = True
        else:
            # on ne refait que ce qui est demandé :
            default = False
        csvFiles = {
            'admin': ['config_admin', 'eleves', 'profs', 'adresses', 'lsu'], 
            'commun': ['config', 'matieres', 'classes', 'classestypes',
                       'periodes', 'sous_rubriques', 'horaires',
                       'referentiel', 'bulletin', 'confidentiel', 'suivi', 'lsu'], 
            }
        import utils_export
        if not('admin' in modifiedTables):
            modifiedTables['admin'] = {}
        for table in csvFiles['admin']:
            mustDo = modifiedTables['admin'].get(table, default)
            if mustDo:
                fileName = csvDir + '/admin_tables/{0}.csv'.format(table)
                utils_export.exportTable2Csv(
                    main, db_admin, table, fileName, msgFin=False)
        if not('commun' in modifiedTables):
            modifiedTables['commun'] = {}
        for table in csvFiles['commun']:
            mustDo = modifiedTables['commun'].get(table, default)
            if mustDo:
                fileName = csvDir + '/commun_tables/{0}.csv'.format(table)
                utils_export.exportTable2Csv(
                    main, main.db_commun, table, fileName, msgFin=False)
        OK = True
    except:
        utils_functions.afficheMsgPb(main)
    finally:
        return OK

def showMdpProfsNoChange(main):
    """
    affiche la liste des profs qui n'ont pas changé le mot de passe initial
    """
    # pour permettre de télécharger la base users avant de continuer :
    mustDownloadDBs(main)
    utils_functions.afficheMessage(main, 
        QtWidgets.QApplication.translate('main', 'ShowMdpProfsNoChange'), tags=('h2'))
    utils_functions.doWaitCursor()
    query_users = utils_db.query(main.db_users)
    try:
        commandLine_users = 'SELECT * FROM profs WHERE Mdp=initialMdp'
        query_users = utils_db.queryExecute(commandLine_users, query=query_users)
        while query_users.next():
            nom = query_users.value(1)
            prenom = query_users.value(2)
            matiereName = query_users.value(6)
            affichage = utils_functions.u('{0} {1}  ({2})').format(nom, prenom, matiereName)
            utils_functions.afficheMessage(main, affichage, editLog=True)
    finally:
        utils_functions.restoreCursor()

def studentsExportAll2ods(main, fileName):
    """
    création d'un fichier ODS contenant la liste des élèves de l'établissement.
    Un premier onglet contient toute la liste.
    Il y a ensuite un onglet par classe.
    """
    import utils_export
    utils_functions.afficheMessage(main, 'studentsExportAll2ods')
    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
    try:
        query_commun = utils_db.query(main.db_commun)
        query_admin = utils_db.query(db_admin)
        # récupération des classes :
        classes = []
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            classes.append(query_commun.value(1))
        exportDatas['TABLES_LIST'].append('ALL')
        exportDatas['TABLES_PARAMS']['ALL'] = {
            'title':QtWidgets.QApplication.translate('main', 'All Students')}
        for classe in classes:
            exportDatas['TABLES_LIST'].append(classe)
            exportDatas['TABLES_PARAMS'][classe] = {'title':classe}
        # titres des colonnes :
        titles = []
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'NAME'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '5.0cm'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'FIRSTNAME'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '4.0cm'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'CLASS'), 
            'bold': True, 'alignHorizontal': 'center'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'SEX'), 
            'bold': True, 'alignHorizontal': 'center'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'LOGIN'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '4.0cm'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'INITIAL PASSWORD'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '4.0cm'})
        # premier onglet (tous) :
        theList = []
        theList.append(titles)
        # contenu :
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_admin, order=['Classe', 'NOM', 'Prenom'], query=query_admin)
        sex2Initial = {1: 'M', 2: 'F', -1: ''}
        for record in queryList:
            line = []
            line.append({'value': utils_functions.u(record[2])})
            line.append({'value': utils_functions.u(record[3])})
            line.append(
                {'value': utils_functions.u(record[4]), 'alignHorizontal': 'center'})
            sex = sex2Initial.get(int(record[12]), '')
            line.append(
                {'value': utils_functions.u(sex), 'alignHorizontal': 'center'})
            line.append({'value': utils_functions.u(record[5])})
            line.append({'value': utils_functions.u(record[7])})
            theList.append(line)
        exportDatas['ALL'] = theList
        # les onglets des classes :
        for classe in classes:
            theList = []
            theList.append(titles)
            # contenu :
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (classe, )}, 
                order=['NOM', 'Prenom'], 
                query=query_admin)
            for record in queryList:
                line = []
                line.append({'value': utils_functions.u(record[2])})
                line.append({'value': utils_functions.u(record[3])})
                line.append(
                    {'value': utils_functions.u(record[4]), 'alignHorizontal': 'center'})
                sex = sex2Initial.get(int(record[12]), '')
                line.append(
                    {'value': utils_functions.u(sex), 'alignHorizontal': 'center'})
                line.append({'value': utils_functions.u(record[5])})
                line.append({'value': utils_functions.u(record[7])})
                theList.append(line)
            exportDatas[classe] = theList
        # on termine :
        utils_export.exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()

def teachersExportAll2ods(main, fileName):
    """
    création d'un fichier ODS contenant la liste des profs de l'établissement.
    """
    import utils_export
    utils_functions.afficheMessage(main, 'teachersExportAll2ods')
    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
    try:
        query_admin = utils_db.query(db_admin)
        exportDatas['TABLES_LIST'].append('ALL')
        exportDatas['TABLES_PARAMS']['ALL'] = {
            'title':QtWidgets.QApplication.translate('main', 'All Teachers')}
        # titres des colonnes :
        titles = []
        titles.append({
            'value': 'ID', 
            'bold': True, 'alignHorizontal': 'left'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'NAME'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '5.0cm'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'FIRSTNAME'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '4.0cm'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'SUBJECT'), 
            'bold': True, 'alignHorizontal': 'center'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'LOGIN'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '5.0cm'})
        titles.append({
            'value': QtWidgets.QApplication.translate('main', 'INITIAL PASSWORD'), 
            'bold': True, 'alignHorizontal': 'left', 'columnWidth': '6.0cm'})
        # premier onglet (tous) :
        theList = []
        theList.append(titles)
        # contenu :
        commandLine_admin = utils_db.q_selectAllFrom.format('profs')
        queryList = utils_db.query2List(
            commandLine_admin, order=['Matiere', 'NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            line = []
            value = int(utils_functions.u(record[0]))
            line.append({'value': value, 'type': utils.INTEGER})
            line.append({'value': utils_functions.u(record[2])})
            line.append({'value': utils_functions.u(record[3])})
            line.append(
                {'value': utils_functions.u(record[4]), 'alignHorizontal': 'center'})
            line.append({'value': utils_functions.u(record[5])})
            line.append({'value': utils_functions.u(record[6])})
            theList.append(line)
        exportDatas['ALL'] = theList
        # on termine :
        utils_export.exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


###########################################################"
#   FIN DE : GESTION DES UTILISATEURS
###########################################################"




###########################################################"
#   AUTRES PROCÉDURES
#   (divers trucs dans le menu administration)
###########################################################"

def mustDownloadDBs(main, databases=[]):
    """
    Avant de modifier la base users ou configweb,
    on demande s'il faut la télécharger.
    """
    result = False
    if not(admin_ftp.ftpTest(main)):
        return result
    if len(databases) < 1:
        databases = ['users']
    espaces = '&nbsp;' * 20
    if len(databases) == 1:
        m0 = QtWidgets.QApplication.translate(
            'main', 'Before changing the following database:')
        m1 = '{0}<b>{1}</b>'.format(espaces, databases[0])
        m2 = QtWidgets.QApplication.translate(
            'main', 'You must download the latest version (which is the website).')
        m3 = QtWidgets.QApplication.translate(
            'main', 'Would you download it now?')
    else:
        m0 = QtWidgets.QApplication.translate(
            'main', 'Before changing the following databases:')
        m1 = '{0}<b>{1}</b>'.format(espaces, databases[0])
        for database in databases[1:]:
            m1 = '{0}, <b>{1}</b>'.format(m1, database)
        m2 = QtWidgets.QApplication.translate(
            'main', 'You must download the latest version (which is the website).')
        m3 = QtWidgets.QApplication.translate(
            'main', 'Would you download them now?')
    message = utils_functions.u(
        '<p>{0}'
        '<br/>{1}.</p>'
        '<p>{2}</p>'
        '<p><br/><b>{3}</b></p>').format(m0, m1, m2, m3)
    reponse = utils_functions.messageBox(
        main, level='warning', message=message, buttons=['Yes', 'No'])
    if reponse == QtWidgets.QMessageBox.Yes:
        if 'users' in databases:
            downloadDB(main, msgFin=False)
        if 'configweb' in databases:
            downloadDB(main, db='configweb', msgFin=False)
        result = True
    return result

def downloadDB(main, db='users', msgFin=True):
    """
    téléchargement d'une base avant modification.
    Par défaut c'est la base users.
    Mais ce peut être aussi configweb, referential_validations etc.
    """
    if db == 'users':
        dirSite = dirSitePrive + '/public'
        dirLocal = dirLocalPrive + '/public'
        theFileName = 'users.sqlite'
        testSize = -1
    elif db == 'configweb':
        dirSite = dirSitePublic + '/_private'
        dirLocal = dirLocalPublic + '/_private'
        theFileName = 'configweb.sqlite'
        testSize = 10
    elif db == 'referential_validations':
        dirSite = dirSitePrive + '/protected'
        dirLocal = dirLocalPrive + '/protected'
        theFileName = 'referential_validations.sqlite'
        testSize = -1

    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            reponse = False
            # on télécharge le fichier sqlite :
            reponse = admin_ftp.ftpGetFile(
                main, dirSite, dirLocal, theFileName, testSize=testSize)
            if reponse:
                # on se reconnecte :
                if db == 'users':
                    utils_db.closeConnection(main, dbName='users')
                    utils_filesdirs.removeAndCopy(dbFileFtp_users, main.dbFile_users)
                    utils_filesdirs.removeAndCopy(dbFileFtp_users, main.dbFileTemp_users)
                    main.db_users = utils_db.createConnection(main, main.dbFileTemp_users)[0]
                elif db == 'configweb':
                    global db_configweb
                    try:
                        db_configweb.close()
                        db_configweb = None
                    except:
                        pass
                    utils_db.closeConnection(main, dbName='configweb')
                    utils_filesdirs.removeAndCopy(
                        dbFile_configweb, dbFileTemp_configweb, testSize=testSize)
                    db_configweb = utils_db.createConnection(main, dbFileTemp_configweb)[0]
                elif db == 'referential_validations':
                    global db_referentialValidations
                    try:
                        db_referentialValidations.close()
                        db_referentialValidations = None
                    except:
                        pass
                    utils_db.closeConnection(main, dbName='referential_validations')
                    utils_filesdirs.removeAndCopy(
                        dbFile_referentialValidations, dbFileTemp_referentialValidations)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                if msgFin:
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                utils_functions.afficheMsgPb(main)

def mustUploadDBs(main, databases=[], msgFin=True):
    """
    Après avoir modifié les bases users, commun ou configweb,
    on demande s'il faut les poster.
    """
    if not(admin_ftp.ftpTest(main)):
        return
    if len(databases) < 1:
        databases = ['users', 'commun', 'configweb']
    espaces = '&nbsp;' * 20
    if len(databases) == 1:
        m0 = QtWidgets.QApplication.translate(
            'main', 'The following database has been changed:')
        m1 = '{0}<b>{1}</b>'.format(espaces, databases[0])
        m2 = QtWidgets.QApplication.translate(
            'main', 'It must be sent to your website.')
        m3 = QtWidgets.QApplication.translate(
            'main', 'Would you upload it now?')
    else:
        m0 = QtWidgets.QApplication.translate(
            'main', 'The following databases have been modified:')
        m1 = '{0}<b>{1}</b>'.format(espaces, databases[0])
        for database in databases[1:]:
            m1 = '{0}, <b>{1}</b>'.format(m1, database)
        m2 = QtWidgets.QApplication.translate(
            'main', 'They must be sent to your website.')
        m3 = QtWidgets.QApplication.translate(
            'main', 'Would you upload them now?')
    message = utils_functions.u(
        '<p>{0}'
        '<br/>{1}.</p>'
        '<p>{2}</p>'
        '<p><br/><b>{3}</b></p>').format(m0, m1, m2, m3)
    reponse = utils_functions.messageBox(
        main, level='warning', message=message, buttons=['Yes', 'No'])
    if reponse == QtWidgets.QMessageBox.Yes:
        if 'users' in databases:
            uploadDB(main, db='users', msgFin=False)
        if 'commun' in databases:
            uploadDB(main, db='commun', msgFin=False)
        if 'configweb' in databases:
            uploadDB(main, db='configweb', msgFin=False)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)

def uploadDB(main, db='commun', msgFin=True):
    """
    envoi FTP d'une base (commun, users etc).
    On met aussi à jour le fichier dates_db.html
    """

    def writeDatesDBFile(datesDBFile, htmlText):
        """
        écriture du fichier dates_db.html 
        (contient les dates des bases commun et users)
        """
        outFile = QtCore.QFile(datesDBFile)
        if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(outFile)
            stream.setCodec('UTF-8')
            stream << htmlText
            outFile.close()

    withDates = ('commun', 'users')
    if db in withDates:
        dirLocal = dirLocalPrive + '/public/'
        dirSite = dirSitePrive + '/public'
        configDir = utils_functions.u(
            '{0}/{1}/').format(main.localConfigDir, main.actualVersion['versionName'])
        datesDBFileName = 'dates_db.html'
        # récupération des dates précédentes :
        dates_db = utils_functions.readDatesDBFile(main, dirLocal)
        maintenant = QtCore.QDateTime.currentDateTime().toString('yyyyMMddhhmm')
    if db == 'commun':
        theFileName = 'commun.sqlite'
        dates_db[0] = maintenant
    elif db == 'users':
        theFileName = 'users.sqlite'
        dates_db[1] = maintenant
    elif db == 'configweb':
        dirLocal = dirLocalPublic + '/_private'
        dirSite = dirSitePublic + '/_private'
        theFileName = 'configweb.sqlite'
    elif db == 'referential_validations':
        dirLocal = dirLocalPrive + '/protected'
        dirSite = dirSitePrive + '/protected'
        theFileName = 'referential_validations.sqlite'
    if db in withDates:
        # création du nouveau fichier dates_db.html :
        htmlText = utils_functions.u('{0}|{1}').format(dates_db[0], dates_db[1])
        writeDatesDBFile(dirLocal + datesDBFileName, htmlText)
    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            # on envoie le fichier sur le site :
            reponse = admin_ftp.ftpPutFile(main, dirSite, dirLocal, theFileName)
            if db in withDates:
                # on copie le fichier dates_db.html dans .Verac :
                utils_filesdirs.removeAndCopy(
                    dirLocal + datesDBFileName, 
                    configDir + datesDBFileName)
                # et on l'envoie sur le site :
                reponse = admin_ftp.ftpPutFile(main, dirSite, dirLocal, datesDBFileName)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                if msgFin:
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                utils_functions.afficheMsgPb(main)

def createConnectionFile(main, msgFin=True):
    """
    blablabla
    """
    utils_functions.doWaitCursor()
    result = False
    try:
        # création d'un dossier dans temp :
        utils_filesdirs.createDirs(
            main.tempPath, 'connection/{0}'.format(main.actualVersion['versionName']))
        # copie du dossier .Verac (configuration) :
        utils_filesdirs.copyDir(
            main.localConfigDir + '/{0}'.format(main.actualVersion['versionName']), 
            main.tempPath + '/connection/{0}'.format(main.actualVersion['versionName']),
            ignore=('referential_propositions.sqlite', ))
        # compression et enregistrement :
        directory = main.tempPath + '/connection/{0}'.format(main.actualVersion['versionName'])
        tarName = QtCore.QFileInfo(directory).baseName()
        if utils_filesdirs.tarDirectory(tarName, directory, main.beginDir):
            originFile = directory + '.tar.gz'
            destFile = adminDir + '/{0}.tar.gz'.format(main.actualVersion['versionName'])
            utils_filesdirs.removeAndCopy(originFile, destFile)
            QtCore.QFile(originFile).remove()
            result = True
        # suppression du dossier temp/archive :
        utils_filesdirs.emptyDir(main.tempPath + '/connection')
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if result:
            if msgFin:
                fileName = '{0}.tar.gz'.format(main.actualVersion['versionName'])
                message = QtWidgets.QApplication.translate(
                    'main', 'The {0} file was created in the verac_admin directory.')
                utils_functions.afficheMsgFin(main, message=message.format(fileName))
        else:
            utils_functions.afficheMsgPb(main)

def createTeachersFilesArchive(main, msgFin=True):
    """
    Crée une archive tar.gz de sauvegarde des fichiers profs 
    (dossier /verac_admin/ftp/secret/verac/up/files).
    Le nom de l'archive est "files-aaaa-mm-jj.tar.gz".
    On garde les archives des 30 derniers jours.
    """
    archivesDir = utils_functions.u(
        '{0}/../archives').format(adminDir)
    archivesDir = QtCore.QDir().cleanPath(archivesDir)
    QtCore.QDir().mkpath(archivesDir)
    today = QtCore.QDate.currentDate().toString('yyyy-MM-dd')
    baseName = utils_functions.u('files-{0}').format(today)
    fileName = utils_functions.u(
        '{0}/{1}.tar.gz').format(archivesDir, baseName)
    # il y a 30 jours :
    last = QtCore.QDate.currentDate().addDays(-30).toString('yyyy-MM-dd')
    lastName = utils_functions.u('files-{0}').format(last)

    utils_functions.doWaitCursor()
    result = False
    try:
        # création d'un dossier d'archive dans temp :
        utils_filesdirs.createDirs(
            main.tempPath, 'archive/{0}'.format(baseName))
        directory = utils_functions.u(
            '{0}/archive/{1}').format(main.tempPath, baseName)
        # copie du dossier files :
        utils_filesdirs.copyDir(
            utils_functions.u('{0}/up/files').format(dirLocalPrive), 
            directory)
        # compression et enregistrement :
        if utils_filesdirs.tarDirectory(baseName, directory, main.beginDir):
            originFile = directory + '.tar.gz'
            utils_filesdirs.removeAndCopy(originFile, fileName)
            QtCore.QFile(originFile).remove()
        # suppression du dossier temp/archive :
        utils_filesdirs.emptyDir(main.tempPath + '/archive')
        # on supprime les vieilles archives :
        dirIterator = QtCore.QDirIterator(
            archivesDir, QtCore.QDir.Files)
        while dirIterator.hasNext():
            fileName2 = dirIterator.next()
            baseName2 = QtCore.QFileInfo(fileName2).baseName()
            if baseName2 < lastName:
                QtCore.QFile(fileName2).remove()

        result = True
    finally:
        utils_functions.restoreCursor()
        if not(result):
            utils_functions.afficheMsgPb(main)

def createYearArchive(main, msgFin=True):
    """
    Crée une archive tar.gz de sauvegarde de l'année scolaire.
    L'archive contient :
        * le dossier verac_admin
        * le dossier .Verac
        * le dossier verac (logiciel)
    Comme elle contient la version du logiciel correspondant au moment de l'archive,
    elle sera toujours lisible. 
    """
    # choix du nom de l'archive à créer (et chemin)
    # rappeler qu'il ne faut pas enregistrer dans verac_admin :
    targzTitle = QtWidgets.QApplication.translate('main', 'Save tar.gz File')
    proposedName = utils_functions.u(
        '{0}/archive_verac_admin_{1}.tar.gz').format(
            main.workDir, main.annee_scolaire[1])
    targzExt = QtWidgets.QApplication.translate(
        'main', 'tar.gz files (*.tar.gz)')
    fileName = QtWidgets.QFileDialog.getSaveFileName(
        main, targzTitle, proposedName, targzExt)
    fileName = utils_functions.verifyLibs_fileName(fileName)
    if fileName == '':
        return

    utils_functions.doWaitCursor()
    result = False
    try:
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Create an archive of the current year'), 
            tags=('h2'))
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'copy of the data in the temporary folder'), 
            editLog=True)
        # création d'un dossier d'archive dans temp :
        utils_filesdirs.createDirs(main.tempPath, 'archive/verac_admin')
        utils_filesdirs.createDirs(main.tempPath, 'archive/.Verac')
        utils_filesdirs.createDirs(main.tempPath, 'archive/verac')
        # copie du dossier verac_admin (données) :
        utils_filesdirs.copyDir(adminDir, main.tempPath + '/archive/verac_admin')
        # copie du dossier .Verac (configuration) :
        utils_filesdirs.copyDir(main.localConfigDir, main.tempPath + '/archive/.Verac')
        # copie du dossier verac (installation du logiciel) :
        utils_filesdirs.copyDir(main.beginDir, main.tempPath + '/archive/verac')

        # compression et enregistrement :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'creation of the archive file'), 
            editLog=True)
        directory = main.tempPath + '/archive'
        tarName = QtCore.QFileInfo(directory).baseName()
        if utils_filesdirs.tarDirectory(tarName, directory, main.beginDir):
            originFile = directory + '.tar.gz'
            destFile = fileName
            utils_filesdirs.removeAndCopy(originFile, destFile)
            QtCore.QFile(originFile).remove()

        # suppression du dossier temp/archive :
        utils_filesdirs.emptyDir(main.tempPath + '/archive')

        result = True
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if result:
            if msgFin:
                utils_functions.afficheMsgFin(main, timer=5)
        else:
            utils_functions.afficheMsgPb(main)

def clearForNewYear(main, msgFin=True):
    """
    Grand nettoyage de début d'année scolaire.
    """
    global db_resultats, db_documents, db_recupEvals
    global dbFile_resultats, dbFileTemp_resultats, dbFile_commun

    # commencer par demander une confirmation :
    m1 = QtWidgets.QApplication.translate(
        'main', 
        "Your data will be re-initialized to begin one New Year's Day school.")
    m2 = QtWidgets.QApplication.translate(
        'main', 'Are you certain to want to continue?')
    message = utils_functions.u(
        '<p align="center">{0}</p>'
        '<p align="center">{1}</p>'
        '<p></p>'
        '<p align="center">{2}</p>').format(utils.SEPARATOR_LINE, m1, m2)
    if utils_functions.messageBox(
        main, level='warning', 
        message=message, buttons=['Yes', 'Cancel']) != QtWidgets.QMessageBox.Yes:
        return False

    utils_functions.doWaitCursor()
    result = False
    try:
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Cleaning for a new year'), 
            tags=('h2'))

        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'reset periods'), 
            editLog=True)
        # on récupère la période actuelle, 
        # la première et la dernière (ou année si pas d'autre) :
        actualPeriode = utils.selectedPeriod
        if utils.NB_PERIODES > 1:
            firstPeriode = 1
        else:
            firstPeriode = 0

        # on libère les bases (resultats, commun, documents, recup_evals) :
        if db_resultats != None:
            db_resultats.close()
            del db_resultats
            db_resultats = None
            utils_db.sqlDatabase.removeDatabase('resultats')
            if actualPeriode > -1:
                utils_db.sqlDatabase.removeDatabase('resultats-{0}'.format(actualPeriode))
        main.db_commun.close()
        del main.db_commun
        main.db_commun = None
        if db_documents != None:
            db_documents.close()
            del db_documents
            db_documents = None
            utils_db.sqlDatabase.removeDatabase('documents')
        if db_recupEvals != None:
            db_recupEvals.close()
            del db_recupEvals
            db_recupEvals = None
            utils_db.sqlDatabase.removeDatabase('recup_evals')
        QtCore.QFile(dbFileTemp_resultats).remove()
        QtCore.QFile(dbFileTemp_documents).remove()
        QtCore.QFile(dbFileTemp_recupEvals).remove()
        dbFile_resultats = dirLocalPrive + '/protected/resultats.sqlite'
        dbFileTemp_resultats = main.tempPath + '/resultats.sqlite'
        # on reconnecte la base commun :
        dbFile_commun = dbFileFtp_commun
        utils_filesdirs.removeAndCopy(dbFile_commun, main.dbFileTemp_commun)
        main.db_commun = utils_db.createConnection(main, main.dbFileTemp_commun)[0]

        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'removing obsolete databases'), 
            editLog=True)
        # on efface les bases obsolètes (et les traces de vieux fichiers) :
        filesToRemove = []
        for periode in range(utils.NB_PERIODES):
            filesToRemove.append(
                utils_functions.u('{0}/resultats-{1}.sqlite').format(
                    main.tempPath, periode))
            filesToRemove.append(
                utils_functions.u('{0}/protected/resultats-{1}.sqlite').format(
                    dirLocalPrive, periode))
            filesToRemove.append(
                utils_functions.u('{0}/public/commun-{1}.sqlite').format(
                    dirLocalPrive, periode))
            filesToRemove.append(
                utils_functions.u('{0}/{1}/commun-{2}.sqlite').format(
                    main.localConfigDir, main.actualVersion['versionName'], periode))
        filesToRemove.append(dbFile_resultats)
        filesToRemove.append(dbFile_documents)
        filesToRemove.append(dbFile_recupEvals)
        for aFile in filesToRemove:
            if QtCore.QFile(aFile).exists():
                QtCore.QFile(aFile).remove()

        # on recrée les nouvelles bases toutes neuves :
        createIfNotExistsDBs(main)
        # on recrée aussi la base resultats :
        if not(QtCore.QFile(dbFile_resultats).exists()):
            db_resultats = utils_db.createConnection(main, dbFileTemp_resultats)[0]
            try:
                query, transactionOK = utils_db.queryTransactionDB(db_resultats)
                listCommands = [
                    utils_db.qct_resultats_absences, 
                    utils_db.qct_resultats_appreciations, 
                    utils_db.qct_resultats_bilans, 
                    utils_db.qct_resultats_bilansDetails, 
                    utils_db.qct_config, 
                    utils_db.qct_resultats_eleveGroupe, 
                    utils_db.qct_resultats_eleveProf, 
                    utils_db.qct_resultats_eleves, 
                    utils_db.qct_resultats_lsu, 
                    utils_db.qct_resultats_notes, 
                    utils_db.qct_resultats_notes_classes, 
                    utils_db.qct_resultats_persos, 
                    utils_db.qct_resultats_syntheses]
                query = utils_db.queryExecute(listCommands, query=query)
                commandLine = utils_db.insertInto('config', 3)
                listArgs = ('versionDB', utils.VERSIONDB_RESULTATS, '')
                query = utils_db.queryExecute(
                    {commandLine: listArgs}, query=query)
            finally:
                utils_db.endTransaction(query, db_resultats, transactionOK)
                utils_filesdirs.removeAndCopy(dbFileTemp_resultats, dbFile_resultats)

        # on déprotège toutes les périodes :
        utils_db.changeInConfigTable(
            main, 
            db_admin, 
            {'ProtectedPeriods': (0, '')}, 
            table='config_calculs')
        for i in range(utils.NB_PERIODES):
            utils.changeProtectedPeriod(i, False)
        # on efface les classes verrouillées :
        clearLockedClasses(main)
        # on réinitialise la liste des périodes archivées :
        initArchivedPeriode(main)
        # on sélectionne la première période :
        utils.changeSelectedPeriod(firstPeriode)
        main.updatePeriodeMenu(firstPeriode)
        utils_db.changeInConfigTable(
            main, 
            db_admin, 
            {'SelectedPeriod': (firstPeriode, '')}, 
            table='config_calculs')
        initPeriode(main)

        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'removing obsolete local files'), 
            editLog=True)
        # suppression des fichiers locaux obsolètes (sauf les .htaccess) :
        dirsToEmpty = [
            adminDir + '/fichiers/odt',
            adminDir + '/fichiers/pdf',
            adminDir + '/fichiers/xml',
            dirLocalPrive + '/protected/docsprofs',
            dirLocalPrive + '/protected/documents',
            dirLocalPrive + '/protected/suivis',
            dirLocalPrive + '/up/files',
            ]
        for aDir in dirsToEmpty:
            utils_filesdirs.emptyDir(
                aDir, deleteThisDir=False, filesToKeep=['.htaccess'])
        # suppression des fichiers obsolètes dans les dossiers de config :
        dirsToEmpty = [
            main.localConfigDir + '/' + main.actualVersion['versionName'], 
            utils.lanConfigDir + '/' + main.actualVersion['versionName'], 
            ]
        for aDir in dirsToEmpty:
            utils_filesdirs.emptyDir(
                aDir, 
                deleteThisDir=False, 
                filesToKeep=['commun.sqlite', 'users.sqlite', 'dates_db.html'])
        # on recrée la base suivis.sqlite :
        dbFileTemp_suivis = main.tempPath + '/suivis.sqlite'
        dbFile_suivis = dirLocalPrive + '/protected/suivis/suivis.sqlite'
        db_suivis = utils_db.createConnection(main, dbFileTemp_suivis)[0]
        try:
            query, transactionOK = utils_db.queryTransactionDB(db_suivis)
            query = utils_db.queryExecute(utils_db.qct_suivi_pp_eleve_cpt, query=query)
        finally:
            utils_db.endTransaction(query, db_suivis, transactionOK)
            utils_filesdirs.removeAndCopy(dbFileTemp_suivis, dbFile_suivis)

        # on incrémente l'année scolaire (possibilité de corriger) :
        year = main.annee_scolaire[0] + 1
        title = utils_functions.u('{0} ({1})').format(
            utils.PROGNAME, QtWidgets.QApplication.translate('main', 'question message'))
        m0 = QtWidgets.QApplication.translate(
            'main', 'School year:')
        m1 = QtWidgets.QApplication.translate(
            'main', 
            'The number of the school year will be incremented '
            'by the value indicated below.<br/>'
            'It must be the end of the new school year (eg 2017 for 2016-2017)<br/> '
            'and should not be changed during the school year.<br/>'
            'You can correct the value proposed below '
            'if it is not correct.<br/><br/>'
            'By cons, you can change the title of the school year as you see fit<br/> '
            'in the general configuration of the facility.')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>'
            '<p></p>'
            '<p><b>{2}</b></p>').format(utils.SEPARATOR_LINE, m1, m0)
        utils_functions.restoreCursor()
        year, ok = QtWidgets.QInputDialog.getInt(
            main, title, message, year, 2000, 3000, 1)
        annee_scolaire = '{0}-{1}'.format(year - 1, year)
        main.annee_scolaire = (year, annee_scolaire)
        # on inscrit dans la base commun :
        commandLineInsertAll = utils_functions.u(
            'INSERT INTO config VALUES ("{0}", {1}, "{2}")')
        commandLines_commun = [
            utils_db.qdf_whereText.format('config', 'key_name', 'annee_scolaire'),
            commandLineInsertAll.format('annee_scolaire', year, annee_scolaire)]
        query_commun, transactionOK_commun = utils_db.queryTransactionDB(
            main.db_commun)
        try:
            query_commun = utils_db.queryExecute(
                commandLines_commun, query=query_commun)
        finally:
            utils_db.endTransaction(
                query_commun, main.db_commun, transactionOK_commun)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, dbFileFtp_commun)
            modifiedTables = {'commun': {'config': True}}
            exportModifIntoCSV(main, modifiedTables)

        # on propose de modifier les ids de profs principaux
        # et de réinitialiser leurs mots de passe :
        lines = []
        matierePP = 'PP'
        query_admin = utils_db.query(db_admin)
        commandLine_admin = utils_functions.u(
            'SELECT * FROM profs WHERE Matiere="{0}" ORDER BY id').format(matierePP)
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        idMin, idMax = utils.decalageChefEtab, utils.decalagePP
        while query_admin.next():
            id_PP = int(query_admin.value(0))
            if id_PP < idMin:
                idMin = id_PP
            if id_PP > idMax:
                idMax = id_PP
            lines.append([
                id_PP, query_admin.value(1), query_admin.value(2), 
                query_admin.value(3), query_admin.value(4), 
                query_admin.value(5), query_admin.value(6)])
        decalageMin = idMin - utils.decalagePP - 1
        decalageMin = decalageMin - decalageMin % 100
        decalageMax = utils.decalageChefEtab - idMax - 1
        decalageMax = decalageMax - decalageMax % 100
        title = utils_functions.u('{0} ({1})').format(
            utils.PROGNAME, QtWidgets.QApplication.translate('main', 'question message'))
        m1 = QtWidgets.QApplication.translate(
            'main', 
            'Do you want to change the ids of leading teachers <br/> and reset their passwords?')
        m2 = QtWidgets.QApplication.translate(
            'main', 'ids offset:')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>'
            '<p></p>'
            '<p><b>{2}</b></p>').format(utils.SEPARATOR_LINE, m1, m2)
        utils_functions.restoreCursor()
        decalage, ok = QtWidgets.QInputDialog.getInt(
            main, title, message, 100, -decalageMin, decalageMax, 100)
        utils_functions.doWaitCursor()
        if ok:
            for line in lines:
                line[0] += decalage
            query_admin, transactionOK_admin = utils_db.queryTransactionDB(
                db_admin)
            try:
                # on efface les anciens enregistrements :
                commandLine = utils_functions.u('DELETE FROM profs WHERE Matiere=?')
                query_admin = utils_db.queryExecute(
                    {commandLine: (matierePP, )}, query=query_admin)
                # on écrit les nouvelles lignes :
                commandLine = utils_db.insertInto(
                    'profs', utils_db.listTablesAdmin['profs'][1])
                query_admin = utils_db.queryExecute(
                    {commandLine: lines}, query=query_admin)
            finally:
                utils_db.endTransaction(
                    query_admin, db_admin, transactionOK_admin)
            # on met à jour les versions locales des bases (depuis le dossier temp) :
            utils_filesdirs.removeAndCopy(dbFileTemp_admin, dbFile_admin)
            # on recrée le fichier csv :
            import utils_export
            fileName = csvDir + '/admin_tables/profs.csv'
            utils_export.exportTable2Csv(
                main, db_admin, 'profs', fileName, msgFin=False)
            # on recrée la liste PROFS :
            global PROFS
            PROFS = []
            query_admin = utils_db.query(db_admin)
            commandLine_admin = utils_db.q_selectAllFrom.format('profs')
            queryList = utils_db.query2List(
                commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
            for record in queryList:
                prof_id = int(record[0])
                prof_file = utils_functions.u('{0}{1}').format(prefixProfFiles, prof_id)
                prof_nom = record[2]
                prof_prenom = record[3]
                utils_functions.appendUnique(
                    PROFS, 
                    (prof_id, prof_file, prof_nom, prof_prenom))
            # on met à jour la base users :
            lines_users = []
            for line in lines:
                login = utils_functions.doEncode(line[5], algo='sha256')
                mdp = utils_functions.doEncode(line[6], algo='sha256')
                lines_users.append((line[0], line[2], line[3], login, mdp, mdp, line[4]))
            query_users, transactionOK = utils_db.queryTransactionDB(main.db_users)
            try:
                # on efface les anciens enregistrements :
                matierePPName = MATIERES['Code2Matiere'].get(matierePP, (0, 'PP', ''))[1]
                commandLine = utils_functions.u('DELETE FROM profs WHERE Matiere=?')
                query_users = utils_db.queryExecute(
                    {commandLine: (matierePPName, )}, query=query_users)
                # on écrit les nouvelles lignes :
                commandLine = utils_db.insertInto('profs', 7)
                query_users = utils_db.queryExecute(
                    {commandLine: lines_users}, query=query_users)
            finally:
                utils_db.endTransaction(query_users, main.db_users, transactionOK)
                utils_filesdirs.removeAndCopy(main.dbFileTemp_users, main.dbFile_users)
                utils_filesdirs.removeAndCopy(main.dbFileTemp_users, dbFileFtp_users)

        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'removing obsolete remote files'), 
            editLog=True)
        # on télécharge d'abord la base referential_validations 
        # pour l'avoir dans /protected 
        # et on la nettoie des élèves trop anciens :
        downloadDB(main, db='referential_validations', msgFin=False)
        openDB(main, 'referential_validations')
        query_validations, transactionOK_validations = utils_db.queryTransactionDB(
            db_referentialValidations)
        openDB(main, 'referential_propositions')
        query_propositions = utils_db.query(db_referentialPropositions)
        try:
            # on récupère la liste des élèves à garder :
            commandLine_propositions = 'SELECT id_eleve FROM eleves'
            query_propositions = utils_db.queryExecute(
                commandLine_propositions, query=query_propositions)
            students = []
            while query_propositions.next():
                id_eleve = int(query_propositions.value(0))
                students.append(id_eleve)
            # et on efface les autres :
            forIn = utils_functions.array2string(students)
            commandLine_validations = (
                'DELETE FROM bilans_validations '
                'WHERE NOT (id_eleve IN ({0}))').format(forIn)
            query_validations = utils_db.queryExecute(commandLine, query=query_validations)
        finally:
            utils_db.endTransaction(
                query_validations, 
                db_referentialValidations, 
                transactionOK_validations)
            utils_filesdirs.removeAndCopy(
                dbFileTemp_referentialValidations, dbFile_referentialValidations)

        # suppression des fichiers distants obsolètes (sauf les .htaccess) :
        if admin_ftp.ftpTest(main):
            dirsToEmpty = [
                ('/protected/docsprofs', 'docProf'), 
                ('/protected/documents', 'docEleve'), 
                ('/protected', ''), 
                ('/protected/suivis', ''), 
                #('/up/files', ''), 
                ('/public', ''), 
                ]
            for (sousDir, fileType) in dirsToEmpty:
                dirSite = dirSitePrive + sousDir
                dirLocal = dirLocalPrive + sousDir
                reponse = admin_ftp.ftpPutDir(main, dirLocal, dirSite, fileType)

        # nettoyage des tables admin.ftpGetDir et admin.ftpPutDir :
        try:
            query_admin, transactionOK = utils_db.queryTransactionDB(db_admin)
            commandLine = utils_db.qct_admin_ftpPut
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            commandLine = utils_db.qdf_table.format('ftpPutDir')
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            commandLine = utils_db.qct_admin_ftpGet
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            commandLine = utils_db.qdf_table.format('ftpGetDir')
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
        finally:
            utils_db.endTransaction(query_admin, db_admin, transactionOK)
            utils_filesdirs.removeAndCopy(dbFileTemp_admin, dbFile_admin)

        # on lance une récup complète :
        import admin_calc_results
        admin_calc_results.updateBilans(
            main, recupLevel=utils.RECUP_LEVEL_REFERENTIAL, msgFin=False)

        # on poste la base documents :
        if not(utils.ICI):
            admin_docs.uploadDBDocuments(main, msgFin=False)

        result = True
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if result:
            if msgFin:
                utils_functions.afficheMsgFin(main, timer=5)
        else:
            utils_functions.afficheMsgPb(main)


###########################################################"
#   FIN DE : AUTRES PROCÉDURES
###########################################################"





###########################################################"
#   GESTION DES PÉRIODES ET ARCHIVES
###########################################################"


def initPeriodesState(main):
    """
    fonction appelée au lancement
    """
    # on récupère la liste des périodes archivées :
    initArchivedPeriode(main)
    # on récupère la période sélectionnée :
    selectInitialPeriode(main)
    # on vérifie quelles sont les périodes protégées :
    initProtectedPeriods(main)

def selectInitialPeriode(main):
    """
    réaffiche la période sélectionnée lors de la dernière utilisation
    """
    if utils.NB_PERIODES > 1:
        actualPeriode = 1
        for i in range(1, utils.NB_PERIODES - 1):
            if archivedPeriode[i]:
                actualPeriode = i + 1
    else:
        actualPeriode = 0
    if not(utils.NOGUI):
        main.updatePeriodeMenu(actualPeriode)
    utils.changeSelectedPeriod(actualPeriode)

def initPeriode(main, periodeEx=-1):
    """
    au lancement (periodeEx=-1) ou si on change de période, 
    il faut réinitialiser plusieurs choses :
        * les bases resultats et commun à utiliser
        * les listes des matières et autres trucs qui dépendent de la base commun
        * les actions disponibles si la période est protégée
    """
    global dbFile_resultats, dbFileTemp_resultats, dbFile_commun

    # on libère les bases précédentes :
    if periodeEx > -1:
        QtCore.QFile(main.dbFileTemp_commun).remove()
        QtCore.QFile(dbFileTemp_resultats).remove()

    periode = utils.selectedPeriod

    # si la base resultats-x de la période sélectionnée existe, on l'utilise
    # sinon, on utilise la base resultats. Idem avec la base commun :
    if archivedPeriode[periode]:
        dbFile_resultats = dirLocalPrive + '/protected/resultats-{0}.sqlite'.format(periode)
        dbFileTemp_resultats = main.tempPath + '/resultats-{0}.sqlite'.format(periode)
        dbFile_commun = dirLocalPrive + '/public/commun-{0}.sqlite'.format(periode)
    else:
        dbFile_resultats = dirLocalPrive + '/protected/resultats.sqlite'
        dbFileTemp_resultats = main.tempPath + '/resultats.sqlite'
        dbFile_commun = dbFileFtp_commun
    utils_filesdirs.removeAndCopy(dbFile_resultats, dbFileTemp_resultats)
    utils_db.closeConnection(main, dbName='commun')
    utils_filesdirs.removeAndCopy(dbFile_commun, main.dbFileTemp_commun)
    main.db_commun = utils_db.createConnection(main, main.dbFileTemp_commun)[0]

    # les listes suivantes sont remplies à partir de la base commun :
    initFromCommun(main, main.db_commun)

    # mise à jour de l'affichage :
    protected = utils.PROTECTED_PERIODS[periode]
    if protected:
        message = QtWidgets.QApplication.translate(
            'main', 'Protected period')
    else:
        message = QtWidgets.QApplication.translate(
            'main', 'Unprotected period')
    message = utils_functions.u('<p align="center"><b>{0}</b></p>').format(message)
    if periode == 0:
        message = ''

    if not(utils.NOGUI):
        main.periodeLabel.setText(message)
        # mise à jour des actions disponibles :
        for action in (
            main.actionCreateCommunTableFromCsv, 
            main.actionUploadDBCommun_admin, 
            main.actionCreateConnectionFile_admin, 
            main.actionUpdateBilans_admin, 
            main.actionUpdateBilansComplete_admin, 
            main.actionUpdateBilansSelect_admin, 
            main.actionCalculateReferential_admin, 
            main.actionClearReferential_admin, 
            main.actionUploadDBResultats_admin):
            action.setEnabled(not(protected))
        if main.actualVersion['versionName'] == 'perso':
            for action in (
                main.actionUploadDBCommun_admin,
                main.actionUploadDBResultats_admin):
                action.setEnabled(False)
        for action in (main.actionSwitchToNextPeriod_admin, ):
            action.setEnabled(periode != 0)

    utils_functions.myPrint(
        'initPeriode', periodeEx, periode, utils_db.sqlDatabase.connectionNames())

def initFromCommun(main, db):
    """
    au lancement ou lorsqu'on change de période ou si on modifie la base commun,
    il faut réinitialiser certaines listes globales issues de la base commun.
    On met aussi à jour les listes des classes et élèves "à notes".
    """
    global MATIERES
    global listeSousRubriques
    global elevesNotes, classesNotes

    query_commun = utils_db.query(main.db_commun)

    # on recharge la liste des matières :
    MATIERES = utils_functions.loadMatieres(main)

    # récupération des codes des sous-rubriques 
    # du bulletin (partie partagée) :
    commandLine = utils_db.q_selectAllFrom.format('sous_rubriques')
    query_commun = utils_db.queryExecute(commandLine, query=query_commun)
    toWhat = {2: 'bulletin', 3: 'referentiel', 4: 'confidentiel'}
    listeSousRubriques = {
        'bulletin': [], 'referentiel': [], 'confidentiel': []}
    while query_commun.next():
        rubriqueCode = query_commun.value(1)
        for i in toWhat:
            value = query_commun.value(i)
            if (value != None) and (value != ''):
                if not(rubriqueCode in listeSousRubriques[toWhat[i]]):
                    listeSousRubriques[toWhat[i]].append(rubriqueCode)

    # récupération des classes à notes :
    elevesNotes, classesNotes = {}, {}
    commandLine = utils_db.q_selectAllFromWhere.format(
        'classes', 'notes', 1)
    query_commun = utils_db.queryExecute(commandLine, query=query_commun)
    while query_commun.next():
        classeName = query_commun.value(1)
        id_classe = int(query_commun.value(0))
        classesNotes[classeName] = id_classe
    classes = utils_functions.array2string(
        classesNotes, text=True)
    commandLine = utils_functions.u(
        'SELECT id, Classe FROM eleves '
        'WHERE Classe IN ({0})').format(classes)
    query_admin = utils_db.queryExecute(commandLine, db=db_admin)
    while query_admin.next():
        id_eleve = int(query_admin.value(0))
        classeName = query_admin.value(1)
        elevesNotes[id_eleve] = (classeName, classesNotes[classeName])

    # lecture des paramètres de l'établissement 
    # depuis commun.config.
    # mise à jour de limiteBLTPerso et annee_scolaire 
    # (en cas de changement de période) :
    config = {}
    commandLine_commun = utils_db.q_selectAllFrom.format('config')
    query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
    while query_commun.next():
        config[query_commun.value(0)] = (
            query_commun.value(1), query_commun.value(2))
    main.limiteBLTPerso = config.get('limiteBLTPerso', (4, ''))[0]
    main.annee_scolaire = config.get('annee_scolaire', (0, ''))
    if main.annee_scolaire[0] < 1:
        main.annee_scolaire = utils_functions.calcAnneeScolaire()

def calcActualPeriode(main):
    """
    """
    lastFile = 0
    testPeriode = utils.NB_PERIODES - 1
    while testPeriode > 0:
        if archivedPeriode[testPeriode]:
            lastFile = testPeriode
            break
        testPeriode -= 1
    if lastFile < utils.NB_PERIODES - 1:
        lastFile += 1
    return lastFile

def initArchivedPeriode(main):
    """
    """
    global archivedPeriode
    archivedPeriode = []
    for i in range(utils.NB_PERIODES):
        theFile1 = dirLocalPrive + '/protected/resultats-{0}.sqlite'.format(i)
        theFile2 = dirLocalPrive + '/public/commun-{0}.sqlite'.format(i)
        periodeState = (QtCore.QFileInfo(theFile1).exists() and QtCore.QFileInfo(theFile2).exists())
        archivedPeriode.append(periodeState)

def switchToNextPeriod(main, msgFin=True):
    """
    bascule à la période suivante
    """
    # création et envoi de l'archive :
    createArchive(main, msgFin=False)
    # sélection de la nouvelle période :
    oldPeriode = utils.selectedPeriod
    if oldPeriode < utils.NB_PERIODES - 1:
        newPeriode = oldPeriode + 1
        utils.changeSelectedPeriod(newPeriode)
        main.updatePeriodeMenu(newPeriode)
        utils_db.changeInConfigTable(
            main, 
            db_admin, 
            {'SelectedPeriod': (newPeriode, '')}, 
            table='config_calculs')
        initPeriode(main, periodeEx=oldPeriode)
    # on efface les classes verrouillées :
    clearLockedClasses(main)
    # on lance une récup complète pour finir :
    import admin_calc_results
    admin_calc_results.updateBilans(
        main, recupLevel=utils.RECUP_LEVEL_REFERENTIAL, msgFin=False)
    if msgFin:
        utils_functions.afficheMsgFin(main, timer=5)

def createArchive(main, msgFin=True):
    """
    """
    periode = utils.selectedPeriod
    if periode < 1:
        return
    global archivedPeriode
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Create an archive of results'), 
        tags=('h2'))

    # première partie : confirmation (à faire) return si non

    # deuxième partie : (re)création des fichiers archives resultats-x et commun-x
    originFile = dbFile_resultats
    destFile = dirLocalPrive + '/protected/resultats-{0}.sqlite'.format(periode)
    utils_filesdirs.removeAndCopy(originFile, destFile)
    originFile = dirLocalPrive + '/public/commun.sqlite'
    destFile = dirLocalPrive + '/public/commun-{0}.sqlite'.format(periode)
    utils_filesdirs.removeAndCopy(originFile, destFile)

    # troisième partie : envoi des fichiers en ftp
    debut = QtCore.QTime.currentTime()
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Sending archive'), 
        editLog=True)
    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            # on envoie le fichier resultats-x :
            dirSite = dirSitePrive + '/protected'
            dirLocal = dirLocalPrive + '/protected'
            theFileName = 'resultats-{0}.sqlite'.format(periode)
            reponse1 = admin_ftp.ftpPutFile(main, dirSite, dirLocal, theFileName)
            # on envoie le fichier commun-x :
            dirSite = dirSitePrive + '/public'
            dirLocal = dirLocalPrive + '/public'
            theFileName = 'commun-{0}.sqlite'.format(periode)
            reponse2 = admin_ftp.ftpPutFile(main, dirSite, dirLocal, theFileName)
            reponse = reponse1 and reponse2
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                # on peut considérer l'archivage réussi :
                archivedPeriode[periode] = True
                protectedPeriods = utils_db.readInConfigTable(
                    db_admin, 'ProtectedPeriods', table='config_calculs')[1]
                protectedPeriods = protectedPeriods.split('|')
                periodText = '{0}'.format(periode)
                if not(periodText in protectedPeriods):
                    protectedPeriods.append(periodText)
                    protectedPeriodsKey = '|'.join(protectedPeriods)
                    utils_db.changeInConfigTable(
                        main, 
                        db_admin, 
                        {'ProtectedPeriods': (0, protectedPeriodsKey)}, 
                        table='config_calculs')
                initProtectedPeriods(main)
                if msgFin:
                    fin = QtCore.QTime.currentTime()
                    utils_functions.afficheDuree(main, debut, fin)
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                message = QtWidgets.QApplication.translate(
                    'main', 'Sending archive failed')
                utils_functions.afficheMsgPb(main, message)

def initProtectedPeriods(main):
    """
    """
    utils.clearProtectedPeriods()

    protectedPeriods = utils_db.readInConfigTable(
        db_admin, 'ProtectedPeriods', table='config_calculs')[1]
    protectedPeriods = protectedPeriods.split('|')
    print('protectedPeriods:', protectedPeriods)

    for i in range(utils.NB_PERIODES):
        period = '{0}'.format(i)
        if period in protectedPeriods:
            utils.changeProtectedPeriod(i, True)
        else:
            utils.changeProtectedPeriod(i, False)
    if not(utils.NOGUI):
        main.initPeriodeMenu()
    initPeriode(main)

class ProtectedPeriodsDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue toute prête avec une liste de QCheckBox.
    """
    def __init__(self, parent=None, helpContextPage='periods', helpBaseUrl='admin'):
        """
        mise en place de l'interface
        """
        super(ProtectedPeriodsDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Protected periods'))

        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl

        self.checkBoxList = []
        for i in range(1, utils.NB_PERIODES):
            checkBox = QtWidgets.QCheckBox(utils.PERIODES[i])
            checkBox.setChecked(utils.PROTECTED_PERIODS[i])
            self.checkBoxList.append(checkBox)

        listeLayout = QtWidgets.QVBoxLayout()
        for checkBox in self.checkBoxList:
            listeLayout.addWidget(checkBox)
        listeCheckBox = QtWidgets.QGroupBox()
        listeCheckBox.setLayout(listeLayout)
        listeCheckBox.setFlat(True)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listeCheckBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)
        #self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, self.helpBaseUrl)

def listProtectedPeriods(main):
    """
    """
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate(
            'main', 'List of Protected periods'), 
        tags=('h2'))

    dialog = ProtectedPeriodsDlg(parent=main)
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
        mustSave = False
        protectedPeriods = []
        for i in range(len(dialog.checkBoxList)):
            new = dialog.checkBoxList[i].isChecked()
            if new:
                protectedPeriods.append('{0}'.format(i + 1))
            old = utils.PROTECTED_PERIODS[i + 1]
            if new != old:
                mustSave = True
        if mustSave:
            protectedPeriods = '|'.join(protectedPeriods)
            utils_db.changeInConfigTable(
                main, 
                db_admin, 
                {'ProtectedPeriods': (0, protectedPeriods)}, 
                table='config_calculs')
            initProtectedPeriods(main)

def unprotectPeriode(main, period=0):
    """
    """
    initialIndex = utils.selectedPeriod - 1
    if initialIndex < 0:
        return
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'unprotect the period'), 
        tags=('h2'))
    msgFin = utils_functions.u(utils.PERIODES[period])

    # Lire la base, pour voir si la période n'est pas déjà déprotégée
    protectedPeriods = utils_db.readInConfigTable(
        db_admin, 'ProtectedPeriods', table='config_calculs')[1]
    protectedPeriods = protectedPeriods.split('|')
    periodText = '{0}'.format(period)
    if periodText in protectedPeriods:
        protectedPeriods = [x for x in protectedPeriods if x != periodText]
        protectedPeriodsKey = '|'.join(protectedPeriods)
        utils_db.changeInConfigTable(
            main, 
            db_admin, 
            {'ProtectedPeriods': (0, protectedPeriodsKey)}, 
            table='config_calculs')
        msgFin = utils_functions.u('{0}{1}').format(
            msgFin, 
            QtWidgets.QApplication.translate(
                'main', '<p>Periode unlocked</p>'))
    else:
        msgFin = utils_functions.u('{0}{1}').format(
            msgFin, 
            QtWidgets.QApplication.translate(
                'main', '<p>Periode is already unlocked</p>'))

    initProtectedPeriods(main)
    utils_functions.messageBox(main, level='warning', message=msgFin)
    utils_functions.afficheMessage(main, msgFin, editLog=True)

def verifProtectedPeriod(main, levelVerif=0):
    """
    on vérifie les périodes protégées
    """
    result = True
    message = QtWidgets.QApplication.translate(
        'main',
        '<p><b>{0} is locked</b></p>'
        '<p>Do you want to unlock this period'
        '<br/>before making an update of the results ?</p>')

    if utils.PROTECTED_PERIODS[0]:
        result = False
        reply = utils_functions.messageBox(
            main, 
            level='question', 
            message=message.format(utils.PERIODES[0]), 
            buttons=['Yes', 'No'])
        if reply == QtWidgets.QMessageBox.Yes:
            unprotectPeriode(main, 0)
            result = True
        else:
            return result

    if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
        result = False
        reply = utils_functions.messageBox(
            main, 
            level='question', 
            message=message.format(utils.PERIODES[utils.selectedPeriod]), 
            buttons=['Yes', 'No'])
        if reply == QtWidgets.QMessageBox.Yes:
            unprotectPeriode(main, utils.selectedPeriod)
            result = True
        else:
            return result

    if (levelVerif > 0) or (utils.selectedPeriod == 0):
        result = False
        if utils.selectedPeriod == 0:
            data = utils.NB_PERIODES
        else:
            data = utils.selectedPeriod
        protection = False
        msg = ''
        for i in range(1, data):
            if utils.PROTECTED_PERIODS[i]:
                protection = True
                msg += utils_functions.u('<li><b>{0}</b></li>').format(utils.PERIODES[i])
        if protection:
            message = QtWidgets.QApplication.translate(
                'main',
                '<p>Periods are locked:</p>'
                '<ul>{0}</ul>'
                '<p>Only periods that are <b>NOT locked</b> will be updated.</p>')
            message = message.format(msg)
            utils_functions.messageBox(main, level='warning', message=message, timer=10)
        result = True
    return result


def loadLockedClasses(main, force=False):
    """
    """
    global LOCKED_CLASSES
    if LOCKED_CLASSES['LOADED'] and not(force):
        return
    LOCKED_CLASSES = {'LOADED': True, 'ORDER': [], 'LOCKED': [], }
    # récupération de la liste des classes (base commun) :
    commandLine_commun = utils_db.qs_commun_classes
    query_commun = utils_db.queryExecute(
        commandLine_commun, db=main.db_commun)
    while query_commun.next():
        className = query_commun.value(1)
        LOCKED_CLASSES['ORDER'].append(className)
    # état des verrous (table admin.config_calculs :
    lockedClasses = utils_db.readInConfigTable(
        db_admin, 'LockedClasses', table='config_calculs')[1]
    lockedClasses = lockedClasses.split('|')
    for className in lockedClasses:
        LOCKED_CLASSES['LOCKED'].append(className)

def clearLockedClasses(main):
    """
    """
    global LOCKED_CLASSES
    LOCKED_CLASSES['LOADED'] = True
    LOCKED_CLASSES['LOCKED'] = []
    utils_db.changeInConfigTable(
        main, 
        db_admin, 
        {'LockedClasses': (0, '')}, 
        table='config_calculs')

class LockedClassesDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour sélectionner les classes verrouillées.
    """
    def __init__(
        self, parent=None, helpContextPage='locked-classes', helpBaseUrl='admin'):
        """
        mise en place de l'interface
        """
        super(LockedClassesDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(
            QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Locked classes'))

        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl
        helpMessage = QtWidgets.QApplication.translate(
            'main', 
            'Place the classes to be locked in the right list.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpMessage))

        # sélection des classes (double liste) :
        self.selectionWidget = utils_2lists.DoubleListWidget(self.main)
        # remplissage des listes :
        for className in LOCKED_CLASSES['ORDER']:
            locked = (className in LOCKED_CLASSES['LOCKED'])
            item = QtWidgets.QListWidgetItem(className)
            if locked:
                self.selectionWidget.selectionList.addItem(item)
            else:
                self.selectionWidget.baseList.addItem(item)

        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(helpLabel)
        mainLayout.addWidget(self.selectionWidget)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)
        #self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(
            self.helpContextPage, self.helpBaseUrl)

def listLockedClasses(main):
    """
    verrouillage des classes.
    On appelle le dialog dédié 
    puis on met à jour la table admin.config_calculs
    """
    loadLockedClasses(main)
    dialog = LockedClassesDlg(parent=main)
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
        lockedClasses = []
        if dialog.selectionWidget.selectionList.count() > 0:
            for i in range(dialog.selectionWidget.selectionList.count()):
                itemWidget = dialog.selectionWidget.selectionList.item(i)
                className = itemWidget.text()
                lockedClasses.append(className)
        lockedClasses = '|'.join(lockedClasses)
        utils_db.changeInConfigTable(
            main, 
            db_admin, 
            {'LockedClasses': (0, lockedClasses)}, 
            table='config_calculs')
        loadLockedClasses(main, force=True)


###########################################################"
#   FIN DE : GESTION DES PÉRIODES ET ARCHIVES
###########################################################"





###########################################################"
#   GESTION DES FICHIERS PROFS
###########################################################"


class VerifProfsFilesDlg(QtWidgets.QDialog):
    """
    Fenêtre de dialogue pour sélectionner ce qui sera vérifié dans les fichiers profs
    """
    def __init__(self, parent=None, what=(), helpContextPage='verif-profs-files', helpBaseUrl='admin'):
        """
        mise en place de l'interface
        """
        super(VerifProfsFilesDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate('main', 'Checking teachers files'))

        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl

        self.checkBoxList = []
        for key in what['orderList']:
            checkBox = QtWidgets.QCheckBox(what[key][1])
            checkBox.setStatusTip(key)
            checkBox.setChecked(what[key][0])
            self.checkBoxList.append(checkBox)

        separator = QtWidgets.QLabel(
            '<p align="center">{0}</p>'.format(utils.SEPARATOR_LINE))
        self.onlyIfProblem = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Display only if there is a problem'))

        listeLayout = QtWidgets.QVBoxLayout()
        for checkBox in self.checkBoxList:
            listeLayout.addWidget(checkBox)
        listeLayout.addWidget(separator)
        listeLayout.addWidget(self.onlyIfProblem)
        listeCheckBox = QtWidgets.QGroupBox()
        listeCheckBox.setLayout(listeLayout)
        listeCheckBox.setFlat(True)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listeCheckBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)
        self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, self.helpBaseUrl)

def verifProfsFiles(main, msgFin=True):
    """
    permet de lancer une vérification de l'état des fichiers
    envoyés par les profs.
    Utile pour l'admin pour détecter un problème éventuel.
    """

    idsProfs = []
    dialog = ListProfsDlg(parent=main, helpContextPage='verif-profs-files')
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        return
    for i in range(dialog.selectionList.count()):
        item = dialog.selectionList.item(i)
        id_prof = item.data(QtCore.Qt.UserRole)
        idsProfs.append(id_prof)
    if len(idsProfs) < 1:
        for i in range(dialog.baseList.count()):
            item = dialog.baseList.item(i)
            id_prof = item.data(QtCore.Qt.UserRole)
            idsProfs.append(id_prof)
    main.enableInterface(dialog, lastState)

    # la liste de ce qui sera vérifié :
    what = {
        'orderList': ('mdpChanged', 'dateFile', 'veracVersion', 'dbProfVersion', 'anneeScolaire', 'tableauxList'),
        'mdpChanged': [
            True, 
            QtWidgets.QApplication.translate(
                'main', 'password status (requires download of the database users)')], 
        'dateFile': [
            True, 
            QtWidgets.QApplication.translate(
                'main', 'date of prof file')], 
        'veracVersion': [
            True, 
            QtWidgets.QApplication.translate(
                'main', 'VERAC version')], 
        'dbProfVersion': [
            True, 
            QtWidgets.QApplication.translate(
                'main', 'prof version of the base (requires download the file)')], 
        'anneeScolaire': [
            True, 
            QtWidgets.QApplication.translate(
                'main', 'school year')], 
        'tableauxList': [
            True, 
            QtWidgets.QApplication.translate(
                'main', 'display the list of tables')]
        }
    # on lance le dialog :
    dialog = VerifProfsFilesDlg(parent=main, what=what)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        return
    for checkBox in dialog.checkBoxList:
        what[checkBox.statusTip()][0] = checkBox.isChecked()
    onlyIfProblem = dialog.onlyIfProblem.isChecked()
    # si on demande à vérifier les mdp, on télécharge la base users :
    if what['mdpChanged'][0]:
        downloadDB(main, msgFin=False)
    # si on demande à vérifier les versions des bases, on télécharge les fichiers profxx :
    if what['dbProfVersion'][0]:
        import admin_calc_results
        admin_calc_results.downloadProfxxFiles(
            main, recupLevel=utils.RECUP_LEVEL_SELECT, idsProfs=idsProfs, msgFin=False)

    # début de l'affichage :
    utils_functions.afficheMessage(main, 
        QtWidgets.QApplication.translate('main', 'Checking teachers files'), tags=('h2'))

    utils_functions.doWaitCursor()
    debut = QtCore.QTime.currentTime()
    try:
        query_admin = utils_db.query(db_admin)
        if what['mdpChanged'][0]:
            # on ouvre la base users :
            query_users = utils_db.query(main.db_users)

        # pour chaque prof :
        for aProf in PROFS:
            if not(aProf[0] in idsProfs):
                continue
            if onlyIfProblem:
                mustDo = False
            else:
                mustDo = True
            messageProf = '<br/>'
            messageProf = utils_functions.u(
                '{0}<h3>{1} {2} ({3}.sqlite)</h3>').format(
                    messageProf, aProf[2], aProf[3], aProf[1])

            profxx = aProf[1] + '.sqlite'
            if what['dbProfVersion'][0]:
                dirTemp = main.tempPath + "/down/"
                fileProfxx = dirTemp + profxx
            else:
                fileProfxx = dirLocalProfxx + profxx

            if what['mdpChanged'][0]:
                # on vérifie si le mdp a été changé :
                mdpChanged = False
                commandLine_users = 'SELECT Mdp, initialMdp FROM profs WHERE id={0}'.format(aProf[0])
                query_users = utils_db.queryExecute(commandLine_users, query=query_users)
                if query_users.first():
                    mdp = query_users.value(0)
                    initialMdp = query_users.value(1)
                    if mdp != initialMdp:
                        mdpChanged = True
                translatedMessage = QtWidgets.QApplication.translate('main', 'Password state:')
                if mdpChanged:
                    translatedResult = QtWidgets.QApplication.translate('main', 'OK')
                    mustDoThis = False
                else:
                    translatedResult = QtWidgets.QApplication.translate('main', 'MUST BE CHANGED')
                    mustDoThis = True
                    mustDo = True
                if not(onlyIfProblem) or mustDoThis:
                    messageProf = utils_functions.u(
                        '{0}{1} {2}<br/>').format(messageProf, translatedMessage, translatedResult)

            # on vérifie si le fichier existe :
            translatedMessage = QtWidgets.QApplication.translate('main', 'File exists:')
            if QtCore.QFileInfo(fileProfxx).exists():
                translatedResult = QtWidgets.QApplication.translate('main', 'OK')
            else:
                translatedResult = QtWidgets.QApplication.translate('main', 'No file')
                messageProf = utils_functions.u(
                    '{0}{1} {2}<br/>').format(messageProf, translatedMessage, translatedResult)
                utils_functions.afficheMessage(main, messageProf, editLog=True, html=True)
                continue

            # on ouvre le fichier :
            try:
                (db_profxx, dbName) = utils_db.createConnection(main, fileProfxx)
                query_profxx = utils_db.query(db_profxx)

                # date du fichier :
                if what['dateFile'][0]:
                    fileDateTimeNew = utils_db.readInConfigTable(db_profxx, 'datetime', default_int=-1)[0]
                    if fileDateTimeNew > 0:
                        forAffichage = utils_functions.u(fileDateTimeNew)
                        forAffichage = QtCore.QDateTime().fromString(forAffichage, 'yyyyMMddhhmm')
                        forAffichage = forAffichage.toString('dd-MM-yyyy  hh:mm')
                        translatedMessage = QtWidgets.QApplication.translate('main', 'File date:')
                        messageProf = utils_functions.u('{0}{1} {2}<br/>').format(messageProf, 
                            translatedMessage, forAffichage)
                    else:
                        translatedMessage = QtWidgets.QApplication.translate('main', 'Nothing in file')
                        messageProf = utils_functions.u('{0}{1}<br/>').format(messageProf, translatedMessage)
                        utils_functions.afficheMessage(main, messageProf, editLog=True, html=True)
                        continue

                # version de VÉRAC :
                if what['veracVersion'][0]:
                    progVersion = utils_db.readInConfigTable(db_profxx, "progversion")[1]
                    translatedMessage = QtWidgets.QApplication.translate('main', 'Software version:')
                    messageProf = utils_functions.u('{0}{1} {2}<br/>').format(messageProf, 
                        translatedMessage, progVersion)

                # version de la base :
                if what['dbProfVersion'][0]:
                    versionDB_profxx = utils_db.readVersionDB(db_profxx)
                    translatedMessage0 = QtWidgets.QApplication.translate('main', 'Version of the teacher DataBase:')
                    if versionDB_profxx < utils.VERSIONDB_PROF:
                        translatedMessage1 = QtWidgets.QApplication.translate(
                            'main', 'MUST REPAIR DB PROF')
                        messageProf = utils_functions.u('{0}{1} {2}<br/>').format(messageProf, 
                            translatedMessage0, versionDB_profxx)
                        messageProf = utils_functions.u('{0}{1}<br/>').format(messageProf, translatedMessage1)
                        mustDo = True
                    elif not(onlyIfProblem):
                        messageProf = utils_functions.u('{0}{1} {2}<br/>').format(messageProf, 
                            translatedMessage0, versionDB_profxx)

                # année scolaire :anneeScolaire
                if what['anneeScolaire'][0]:
                    anneeScolaire = utils_db.readInConfigTable(db_profxx, "annee_scolaire")[0]
                    mustDoThis = (anneeScolaire != main.annee_scolaire[0])
                    if not(onlyIfProblem) or mustDoThis:
                        translatedMessage = QtWidgets.QApplication.translate('main', 'School year:')
                        messageProf = utils_functions.u('{0}{1} {2}<br/>').format(messageProf, 
                            translatedMessage, anneeScolaire)
                        mustDo = True

                # liste des tableaux :
                if what['tableauxList'][0]:
                    translatedLIST = QtWidgets.QApplication.translate('main', 'List of Tables:')
                    translatedMATIERE = QtWidgets.QApplication.translate('main', 'MATIERE')
                    translatedGROUPE = QtWidgets.QApplication.translate('main', 'GROUPE')
                    translatedPERIODE = QtWidgets.QApplication.translate('main', 'PERIODE')
                    translatedNAME = QtWidgets.QApplication.translate('main', 'NAME')
                    translatedETAT = QtWidgets.QApplication.translate('main', 'ETAT')
                    translatedCLASS = QtWidgets.QApplication.translate('main', 'CLASS')

                    commandLine_profxx = utils_db.qs_prof_tableauxEtMatieres
                    query_profxx = utils_db.queryExecute(commandLine_profxx, query=query_profxx)
                    nbTableaux, nbPublics = 0, 0
                    tableauxList = []
                    while query_profxx.next():
                        nbTableaux += 1
                        name = query_profxx.value(1)
                        public = int(query_profxx.value(2))
                        if public == 1:
                            public = 'public'
                            nbPublics += 1
                        else:
                            public = 'prive'
                        periode = int(query_profxx.value(4))
                        periode = utils.PERIODES[periode]
                        matiereName = query_profxx.value(9)
                        groupeName = query_profxx.value(8)
                        classeName = query_profxx.value(11)
                        tableauxList.append((matiereName, groupeName, periode, name, public, classeName))
                    messageNbTableaux = '{0} tableaux'.format(nbTableaux)
                    messageNbPublics = '{0} tableaux publics'.format(nbPublics)
                    messageProf = utils_functions.u('{0}{1} ({2})').format(messageProf, 
                        messageNbTableaux, messageNbPublics)

                    messageTableaux = utils_functions.u('<h4>{0}</h4>'
                        '<table border="1"><tr>'
                        '<td><b>{1}</b></td>'
                        '<td><b>{2}</b></td>'
                        '<td><b>{3}</b></td>'
                        '<td><b>{4}</b></td>'
                        '<td><b>{5}</b></td>'
                        '<td><b>{6}</b></td>'
                        '</tr>').format(translatedLIST,
                                        translatedMATIERE, translatedGROUPE,
                                        translatedPERIODE, translatedNAME,
                                        translatedETAT, translatedCLASS)
                    for tableau in tableauxList:
                        messageTableau = utils_functions.u('<tr>'
                            '<td>{0}</td>'
                            '<td>{1}</td>'
                            '<td>{2}</td>'
                            '<td>{3}</td>'
                            '<td>{4}</td>'
                            '<td>{5}</td>'
                            '</tr>').format(tableau[0], tableau[1], tableau[2], 
                                            tableau[3], tableau[4], tableau[5])
                        messageTableaux = utils_functions.u('{0}{1}').format(messageTableaux, messageTableau)
                    messageTableaux = utils_functions.u('{0}</table>').format(messageTableaux)
                    messageProf = utils_functions.u('{0}{1}<br/>').format(messageProf, messageTableaux)

                # on referme la base prof :
                query_profxx.clear()
                del query_profxx
                db_profxx.close()
                del db_profxx
                utils_db.sqlDatabase.removeDatabase(dbName)
                # et on affiche le message :
                messageProf = utils_functions.u('{0}<br/>').format(messageProf)
                if mustDo:
                    utils_functions.afficheMessage(main, messageProf, editLog=True, html=True)
            except:
                utils_functions.afficheMessage(main, utils_functions.u('except: {0}').format(profxx))
                pass

    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if msgFin:
            fin = QtCore.QTime.currentTime()
            utils_functions.afficheMessage(main, '<br/>', editLog=True, html=True)
            utils_functions.afficheDuree(main, debut, fin)
            utils_functions.afficheMsgFin(main, timer=5)

def getDateFile(main, fileProfxx):
    """
    Ouvre un fichier profxx et lit la date
    dateFile, dateFileLL = getDateFile(main, fileProfxx)
    ATTENTION : on doit passer le fichier avec son chemin
    """
    # date bidon au cas où :
    dateFile = QtCore.QDateTime().fromString('111111111111', 'yyyyMMddhhmm')
    try:
        # on ouvre le fichier :
        (db_profxx, dbName) = utils_db.createConnection(main, fileProfxx)
        query_profxx = utils_db.query(db_profxx)
        # date du fichier :
        fileDateTime = utils_db.readInConfigTable(db_profxx, 'datetime', default_int=-1)[0]
        if fileDateTime > 0:
            dateFile = QtCore.QDateTime().fromString(utils_functions.u(fileDateTime), 'yyyyMMddhhmm')
        # on referme la base prof :
        query_profxx.clear()
        del query_profxx
        db_profxx.close()
        del db_profxx
        utils_db.sqlDatabase.removeDatabase(dbName)
    except:
        utils_functions.afficheMessage(main, utils_functions.u('except: {0}').format(profxx))
    finally:
        dateFileLL = utils_functions.toLong(dateFile.toString('yyyyMMddhhmm'))
        return dateFile, dateFileLL

def getDatesDB(main, profxx):
    """
    Lit les dates enregistrées dans la table admin.ftpGetDir
    pour le fichierprofxx.
    Gère les 2 fileType : fileProf et fileProfLocal
    dateDB, dateDBLL, dateDBLocal, dateDBLocalLL = getDatesDB(main, profxx)
    ATTENTION : on doit passer le nom du fichier
    """
    # dates bidons au cas où :
    dateDB = QtCore.QDateTime().fromString('100010101010', 'yyyyMMddhhmm')
    dateDBLocal = QtCore.QDateTime().fromString('100010101010', 'yyyyMMddhhmm')
    try:
        query_admin = utils_db.query(db_admin)
        commandLine = utils_functions.u(
            'SELECT datetime FROM ftpGetDir '
            'WHERE fileType="fileProf" AND fileName="{0}"').format(profxx)
        query_admin = utils_db.queryExecute(commandLine, query=query_admin)
        if query_admin.first():
            dateDB = QtCore.QDateTime().fromString(
                utils_functions.u(query_admin.value(0)), 'yyyyMMddhhmm')
        commandLine = utils_functions.u(
            'SELECT datetime FROM ftpGetDir '
            'WHERE fileType="fileProfLocal" AND fileName="{0}"').format(profxx)
        query_admin = utils_db.queryExecute(commandLine, query=query_admin)
        if query_admin.first():
            dateDBLocal = QtCore.QDateTime().fromString(
                utils_functions.u(query_admin.value(0)), 'yyyyMMddhhmm')
    except:
        utils_functions.afficheMessage(
            main, utils_functions.u('except: {0}').format(profxx))
    finally:
        dateDBLL = utils_functions.toLong(dateDB.toString('yyyyMMddhhmm'))
        dateDBLocalLL = utils_functions.toLong(dateDBLocal.toString('yyyyMMddhhmm'))
        return dateDB, dateDBLL, dateDBLocal, dateDBLocalLL

def lanGetProfxxDir(main, recupLevel=0, idsProfs=[-1]):
    """
    Met à jour le dossier réseau contenant les fichiers des profs.
    On teste les dates des fichiers et on les compare à celles qui sont
    inscrites dans la table admin.ftpGetDir.
    Si recupLevel > utils.RECUP_LEVEL_SIMPLE, on récupère tous les fichiers.
    Retours :
        * reponse : si ça a marché
        * newFiles : dico des fichiers à récupérer (avec leur date)
    """
    reponse, newFiles = False, {}
    if not(QtCore.QDir(dirLanProfxx).exists()):
        return reponse, newFiles
    try:
        lines = []
        profs = ''
        for prof in PROFS:
            id_prof = prof[0]
            profxx = prof[1] + '.sqlite'
            fileProfxx = dirLanProfxx + profxx
            if not(QtCore.QFileInfo(fileProfxx).exists()):
                continue
            if (idsProfs == [-1]) or (id_prof in idsProfs):
                messages = [utils_functions.u('<b>{0}</b>').format(profxx)]
                mustDown = False
                # récupération de dateFile dans le fichier du prof :
                dateFile, dateFileLL = getDateFile(main, fileProfxx)
                # récupération de dateDBLan dans la base (si existe):
                dateDB, dateDBLL, dateDBLan, dateDBLanLL = getDatesDB(main, profxx)
                if recupLevel > utils.RECUP_LEVEL_SIMPLE:
                    mustDown = True
                else:
                    if dateDBLanLL < dateFileLL:
                        mustDown = True
                        messages.append(dateFile.toString('dd-MM-yyyy  hh:mm') + '   (dateFile)')
                        messages.append(dateDBLan.toString('dd-MM-yyyy  hh:mm') + '   (dateDBLan)')
                if mustDown:
                    messages.append('MUST RECUP ' + profxx)
                    if profs == '':
                        profs = '"{0}"'.format(profxx)
                    else:
                        profs = '{0}, "{1}"'.format(profs, profxx)
                    lines.append(('fileProfLan', profxx, dateFile.toString('yyyyMMddhhmm')))
                    newFiles[profxx] = (id_prof, dateDBLL, dateDBLanLL, dateFileLL)
                else:
                    messages.append('INUTILE')
                utils_functions.afficheMessage(main, messages, editLog=True)

        # mise à jour de la base admin :
        if profs != '':
            query_admin, transactionOK = utils_db.queryTransactionDB(db_admin)
            try:
                commandLine = utils_db.qct_admin_ftpGet
                query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                commandLine = utils_db.qdf_admin_ftpGet.format('fileProfLan', profs)
                query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                commandLine = utils_db.insertInto(
                    'ftpGetDir', utils_db.listTablesAdmin['ftpGetDir'][1])
                query_admin = utils_db.queryExecute({commandLine: lines}, query=query_admin)
            finally:
                utils_db.endTransaction(query_admin, db_admin, transactionOK)
                utils_filesdirs.removeAndCopy(dbFileTemp_admin, dbFile_admin)

        reponse = True
    except:
        reponse = False
    finally:
        return reponse, newFiles

def localGetProfxxDir(main, recupLevel=0, idsProfs=[-1]):
    """
    Met à jour le dossier local contenant les fichiers des profs.
    On teste les dates des fichiers et on les compare à celles qui sont
    inscrites dans la table admin.ftpGetDir.
    Si recupLevel > utils.RECUP_LEVEL_SIMPLE, on récupère tous les fichiers.
    Retours :
        * reponse : si ça a marché
        * newFiles : dico des fichiers à récupérer (avec leur date)
    """
    try:
        reponse, newFiles = False, {}
        lines = []
        profs = ''
        for prof in PROFS:
            id_prof = prof[0]
            profxx = prof[1] + '.sqlite'
            fileProfxx = dirLocalProfxx + profxx
            if not(QtCore.QFileInfo(fileProfxx).exists()):
                continue
            if (idsProfs == [-1]) or (id_prof in idsProfs):
                messages = [utils_functions.u('<b>{0}</b>').format(profxx)]
                mustDown = False
                # récupération de dateFile dans le fichier du prof :
                dateFile, dateFileLL = getDateFile(main, fileProfxx)
                # récupération de dateDBLocal dans la base (si existe):
                dateDB, dateDBLL, dateDBLocal, dateDBLocalLL = getDatesDB(main, profxx)
                if recupLevel > utils.RECUP_LEVEL_SIMPLE:
                    mustDown = True
                else:
                    if dateDBLocalLL < dateFileLL:
                        mustDown = True
                        messages.append(dateFile.toString('dd-MM-yyyy  hh:mm') + '   (dateFile)')
                        messages.append(dateDBLocal.toString('dd-MM-yyyy  hh:mm') + '   (dateDBLocal)')
                if mustDown:
                    messages.append('MUST RECUP ' + profxx)
                    if profs == '':
                        profs = '"{0}"'.format(profxx)
                    else:
                        profs = '{0}, "{1}"'.format(profs, profxx)
                    lines.append(('fileProfLocal', profxx, dateFile.toString('yyyyMMddhhmm')))
                    newFiles[profxx] = (id_prof, dateDBLL, dateDBLocalLL, dateFileLL)
                else:
                    messages.append('INUTILE')
                utils_functions.afficheMessage(main, messages, editLog=True)

        # mise à jour de la base admin :
        if profs != '':
            query_admin, transactionOK = utils_db.queryTransactionDB(db_admin)
            try:
                commandLine = utils_db.qct_admin_ftpGet
                query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                commandLine = utils_db.qdf_admin_ftpGet.format('fileProfLocal', profs)
                query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                commandLine = utils_db.insertInto(
                    'ftpGetDir', utils_db.listTablesAdmin['ftpGetDir'][1])
                query_admin = utils_db.queryExecute({commandLine: lines}, query=query_admin)
            finally:
                utils_db.endTransaction(query_admin, db_admin, transactionOK)
                utils_filesdirs.removeAndCopy(dbFileTemp_admin, dbFile_admin)

        reponse = True
    except:
        reponse = False
    finally:
        return reponse, newFiles


###########################################################"
#   FIN DE : GESTION DES FICHIERS PROFS
###########################################################"





###########################################################"
#   UTILISÉS À PLUSIEURS ENDROITS
#   (diverses procédures)
###########################################################"


class ListElevesDlg(utils_2lists.DoubleListDlg):
    """
    explications
    """
    def __init__(
        self, parent=None, 
        firstSelection=[], 
        helpMessage=10, 
        helpContextPage='', helpBaseUrl='admin'):
        title = QtWidgets.QApplication.translate('main', 'Students list')
        self.firstTime = True
        super(ListElevesDlg, self).__init__(
            parent, 
            withComboBox=True, 
            firstSelection=firstSelection,
            helpMessage=helpMessage, 
            helpContextPage=helpContextPage, helpBaseUrl=helpBaseUrl, 
            title=title)

        self.main = parent
        self.baseGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'List:'))
        self.selectionGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'Todo:'))

    def remplirBaseComboBox(self):
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        self.baseComboBox.addItem(QtWidgets.QApplication.translate('main', 'All the classes'))
        while query_commun.next():
            classe = query_commun.value(1)
            self.baseComboBox.addItem(classe)

    def remplirListes(self):
        """
        blablabla
        """
        self.baseList.clear()
        self.Apres = []
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())

        commandLine_users = utils_db.q_selectAllFrom.format('eleves')
        query_users = utils_db.query(self.main.db_users)
        if self.baseComboBox.currentIndex() == 0:
            commandLine = commandLine_users
            queryList = utils_db.query2List(
                commandLine, order=['Classe', 'NOM', 'Prenom'], query=query_users)
        else:
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (self.baseComboBox.currentText(), )}, 
                order=['NOM', 'Prenom'], 
                query=query_users)
        for record in queryList:
            t = utils_functions.u('{0} {1} {2}').format(record[1], record[2], record[3])
            if not (t in self.Apres):
                item = QtWidgets.QListWidgetItem(t)
                item.setToolTip(utils_functions.u(record[0]))
                item.setData(QtCore.Qt.UserRole, int(record[0]))
                if self.firstTime:
                    if record[0] in self.firstSelection:
                        self.selectionList.addItem(item)
                    else:
                        self.baseList.addItem(item)
                else:
                    self.baseList.addItem(item)
        if self.firstTime:
            self.firstTime = False

    def updateListes(self):
        """
        blablabla
        """
        self.remplirListes()

class ListProfsDlg(utils_2lists.DoubleListDlg):
    """
    explications
    """
    def __init__(
        self, parent=None, helpMessage=10, helpContextPage='', helpBaseUrl='admin'):
        title = QtWidgets.QApplication.translate('main', 'List of teachers')
        super(ListProfsDlg, self).__init__(
            parent, 
            withComboBox=True, 
            helpMessage=helpMessage, 
            helpContextPage=helpContextPage, helpBaseUrl=helpBaseUrl, 
            title=title)

        self.main = parent
        self.baseGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'List:'))
        self.selectionGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'Todo:'))

    def remplirBaseComboBox(self):
        self.baseComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'All subjects'))
        for matiereCode in MATIERES['ALL']:
            matiereName = MATIERES['Code2Matiere'][matiereCode][1]
            self.baseComboBox.addItem(matiereName)

    def remplirListes(self):
        """
        blablabla
        """
        self.baseList.clear()
        self.Apres = []
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())
        query_admin = utils_db.query(db_admin)
        commandLine_all = utils_db.q_selectAllFrom.format('profs')
        commandLine_matiere = utils_functions.u('SELECT * FROM profs WHERE Matiere="{0}"')
        if self.baseComboBox.currentIndex() == 0:
            commandLine_admin = commandLine_all
        else:
            matiereCode = MATIERES['Matiere2Code'].get(
                self.baseComboBox.currentText(), '')
            commandLine_admin = commandLine_matiere.format(matiereCode)
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            matiereName = MATIERES['Code2Matiere'].get(record[4], (0, '', ''))[1]
            t = utils_functions.u('{0} {1} [{2}]').format(
                record[2], record[3], matiereName)
            if not (t in self.Apres):
                item = QtWidgets.QListWidgetItem(t)
                item.setToolTip(utils_functions.u(record[0]))
                item.setData(QtCore.Qt.UserRole, int(record[0]))
                self.baseList.addItem(item)

    def updateListes(self):
        """
        blablabla
        """
        self.remplirListes()


class ListCPTDlg(utils_2lists.DoubleListDlg):
    """
    explications
    """
    def __init__(
        self, parent=None, helpContextPage='', helpBaseUrl='admin'):
        title = QtWidgets.QApplication.translate('main', 'Balances selection')
        self.bilans = {}
        super(ListCPTDlg, self).__init__(
            parent,
            helpMessage=11, 
            helpContextPage=helpContextPage, helpBaseUrl=helpBaseUrl, 
            title=title)

        self.main = parent
        self.baseGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'List:'))
        self.selectionGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'Todo:'))

    def remplirListes(self):
        """
        blablabla
        """
        self.baseList.clear()
        self.Apres = []
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = 'SELECT DISTINCT * FROM referentiel WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            name = query_commun.value(1)
            title = query_commun.value(5)
            self.bilans[name] = title
            t = utils_functions.u('{0} : {1}').format(name, title)
            if not (t in self.Apres):
                item = QtWidgets.QListWidgetItem(t)
                item.setData(QtCore.Qt.UserRole, (name, title))
                self.baseList.addItem(item)

class ListMatieresDlg(utils_2lists.DoubleListDlg):
    """
    explications
    """
    def __init__(
        self, parent=None, helpContextPage='', helpBaseUrl='admin'):
        title = QtWidgets.QApplication.translate('main', 'Subjects selection')
        self.matieres = {}
        super(ListMatieresDlg, self).__init__(
            parent,
            helpMessage=11, 
            helpContextPage=helpContextPage, helpBaseUrl=helpBaseUrl, 
            title=title)

        self.main = parent
        self.baseGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'List:'))
        self.selectionGroupBox.setTitle(QtWidgets.QApplication.translate('main', 'Todo:'))

    def remplirListes(self):
        """
        blablabla
        """
        self.baseList.clear()
        self.Apres = []
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())
        for matiereCode in MATIERES['ALL']:
            matiereName = MATIERES['Code2Matiere'][matiereCode][1]
            self.matieres[matiereName] = matiereCode
            t = utils_functions.u('{0} : {1}').format(matiereName, matiereCode)
            if not (t in self.Apres):
                item = QtWidgets.QListWidgetItem(t)
                item.setData(QtCore.Qt.UserRole, (matiereName, matiereCode))
                self.baseList.addItem(item)




###########################################################"
#   FIN DE : UTILISÉS À PLUSIEURS ENDROITS
###########################################################"



