# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
Gestion de l'interface web par l'admin.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_web, utils_db, utils_filesdirs
import admin, admin_ftp


# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
else:
    from PyQt4 import QtCore, QtGui as QtWidgets



correspondances = {0:False, 1:True, False:0, True:1}

states = {
    'LIST': [
        'SEPARATOR:General Restrictions',
        'siteEnabled',
        'studentsSiteEnabled',
        'studentsShowCptDetails',
        'studentsShowClassColumn',
        'teachersCanValidate',

        'SEPARATOR:Pages authorized for students (all periods)',
        'studentsShowBilans',
        'studentsShowReferentiel',
        'studentsShowSocleComponents',
        'studentsShowDetails',
        'studentsShowSuivi',
        'studentsShowAppreciationsGroup',
        'studentsShowDocuments',
        'studentsShowCalculatrice',

        'SEPARATOR:Restrictions relating only to the current period',
        'studentsShowActualPeriod',
        'studentsShowAppreciations',
        'studentsShowNotes',
        ], 

    'siteEnabled': [
        True, 
        QtWidgets.QApplication.translate('main', 'the site is enabled'), 
        'SITE_ENABLED'], 
    'studentsSiteEnabled': [
        True, 
        QtWidgets.QApplication.translate(
            'main', 'students have access to the site'), 
        'SITE_ENABLED_ELEVES'], 
    'studentsShowCptDetails': [
        True, 
        QtWidgets.QApplication.translate(
            'main', 'students can view the details of shared competences'), 
        'SHOW_CPT_DETAILS'], 
    'studentsShowClassColumn': [
        True, 
        QtWidgets.QApplication.translate(
            'main', 'students  see the "Class" column'), 
        'SHOW_CLASS_COLUMN'], 
    'teachersCanValidate': [
        True, 
        QtWidgets.QApplication.translate(
            'main', 'teachers can validate the referential'), 
        'CAN_VALIDATE'], 

    'studentsShowBilans': [
        True, 
        QtWidgets.QApplication.translate('main', 'Balances'), 
        'SHOW_BILANS'], 
    'studentsShowReferentiel': [
        True, 
        QtWidgets.QApplication.translate('main', 'Referential'), 
        'SHOW_REFERENTIEL'], 
    'studentsShowSocleComponents': [
        True, 
        QtWidgets.QApplication.translate('main', 'Components of the socle'), 
        'SHOW_SOCLE_COMPONENTS'], 
    'studentsShowDetails': [
        True, 
        QtWidgets.QApplication.translate('main', 'Details'), 
        'SHOW_DETAILS'], 
    'studentsShowSuivi': [
        True, 
        QtWidgets.QApplication.translate('main', 'Follows'), 
        'SHOW_SUIVI'], 
    'studentsShowAppreciationsGroup': [
        True, 
        QtWidgets.QApplication.translate('main', 'Opinions (on groups)'), 
        'SHOW_APPRECIATION_GROUPE'], 
    'studentsShowDocuments': [
        True, 
        QtWidgets.QApplication.translate('main', 'Papers'), 
        'SHOW_DOCUMENTS'], 
    'studentsShowCalculatrice': [
        True, 
        QtWidgets.QApplication.translate('main', 'Calculator'), 
        'SHOW_CALCULATRICE'], 

    'studentsShowActualPeriod': [
        True, 
        QtWidgets.QApplication.translate(
            'main', 'students have access to the actual period'), 
        'SHOW_ACTUAL_PERIODE'], 
    'studentsShowAppreciations': [
        True, 
        QtWidgets.QApplication.translate(
            'main', 'students see the opinions'), 
        'SHOW_APPRECIATION'], 
    'studentsShowNotes': [
        True, 
        QtWidgets.QApplication.translate(
            'main', 'students see the notes (for classes with notes)'), 
        'SHOW_NOTES'], 
    }





def updateVeracWebSite(main, msgFin=True):
    """
    Procédure de mise à jour de l'interface web par l'admin.
        * téléchargement et décompression de verac_web
        * mise à jour du dossier local /verac_admin/ftp/verac
        * mise à jour du site par FTP
    """
    utils_functions.doWaitCursor()
    try:
        # PREMIÈRE PARTIE :
        # téléchargement et décompression de verac_web dans temp
        m1 = QtWidgets.QApplication.translate('main', 'download verac_web')
        utils_functions.afficheMessage(main, m1, tags=('h2'))
        updateVeracWebDir(main)

        # DEUXIÈME PARTIE :
        # mise à jour du dossier local /verac_admin/ftp/verac
        m1 = QtWidgets.QApplication.translate('main', 'update /verac_admin/ftp/verac')
        utils_functions.afficheMessage(main, m1, tags=('h2'))
        try:
            admin.db_configweb.close()
            admin.db_configweb = None
        except:
            pass
        utils_db.closeConnection(main, dbName='configweb')
        utils_filesdirs.removeAndCopy(
            admin.dbFile_configweb, admin.dbFileTemp_configweb, testSize=10)
        utils_filesdirs.emptyDir(
            admin.dirLocalPublic, 
            deleteThisDir=False)
        scrDir = main.tempPath + '/down/verac'
        destDir = admin.dirLocalPublic
        utils_filesdirs.copyDir(scrDir, destDir)
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_configweb, admin.dbFile_configweb, testSize=10)
        # on ajoute le fichier dates_versions.html dans le dossier _private :
        dates_versions = utils_web.readDatesVersionsFile(main, web=True)
        tempFile = utils_functions.u('{0}/down/dates_versions.html').format(main.tempPath)
        destFile = utils_functions.u('{0}/_private/dates_versions.html').format(admin.dirLocalPublic)
        utils_filesdirs.removeAndCopy(tempFile, destFile)

        # TROISIÈME PARTIE :
        # mise à jour du site par FTP
        m1 = QtWidgets.QApplication.translate('main', 'update web site via FTP')
        utils_functions.afficheMessage(main, m1, tags=('h2'))
        if admin_ftp.ftpTest(main):
            import sfm
            result = sfm.doAction(
                username=admin_ftp.FTP_USER, 
                password=admin_ftp.FTP_PASS, 
                host=admin_ftp.FTP_HOST, 
                port=admin_ftp.FTP_PORT, 
                remotedir=admin.dirSitePublic, 
                localdir=admin.dirLocalPublic)
            utils_functions.myPrint(result)
            traductions = {
                'Processing Summary': QtWidgets.QApplication.translate('main', 'Processing Summary'),
                'Directories created:': QtWidgets.QApplication.translate('main', 'Directories created:'),
                'Directories removed:': QtWidgets.QApplication.translate('main', 'Directories removed:'),
                'Directories total:': QtWidgets.QApplication.translate('main', 'Directories total:'),
                'Files created:': QtWidgets.QApplication.translate('main', 'Files created:'),
                'Files updated:': QtWidgets.QApplication.translate('main', 'Files updated:'),
                'Files removed:': QtWidgets.QApplication.translate('main', 'Files removed:'),
                'Files total:': QtWidgets.QApplication.translate('main', 'Files total:'),
                'Bytes transfered:': QtWidgets.QApplication.translate('main', 'Bytes transfered:'),
                'Bytes total:': QtWidgets.QApplication.translate('main', 'Bytes total:'),
                'Time started:': QtWidgets.QApplication.translate('main', 'Time started:'),
                'Time finished:': QtWidgets.QApplication.translate('main', 'Time finished:'),
                'Duration:': QtWidgets.QApplication.translate('main', 'Duration:'), 
                }
            for text in traductions:
                result = result.replace(text, traductions[text])
            result = result.split('\n')
            utils_functions.afficheMessage(main, result, editLog=True)

    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)


def updateVeracWebDir(main, dateNew=''):
    utils_functions.afficheMessage(main, 'updateVeracWebDir')
    import utils_filesdirs
    try:
        OK = 0
        # on récupère la date de la version installée pour la vérification :
        dates_versions = utils_web.readDatesVersionsFile(main)
        dateVeracBefore = dates_versions[2]
        # téléchargement du fichier verac_web.tar.gz dans le dossier temporaire :
        if dateNew == '':
            # téléchargement du fichier dates_versions.html dans le dossier temporaire:
            dates_versions = utils_web.readDatesVersionsFile(main, web=True)
            dateNew = dates_versions[2]
        theFileName = 'verac_web.tar.gz'
        destDir = main.tempPath + '/down/'
        theUrl = utils.UPDATEURL
        downLoader = utils_web.UpDownLoader(main, 
            theFileName, destDir, theUrl, down=True, showProgress=True)
        downLoader.launch()
        while downLoader.state == utils_web.STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if downLoader.state == utils_web.STATE_OK:
            OK = 1
            # on décompresse le fichier tar.gz dans temp:
            fileVerac = main.tempPath + '/down/' + theFileName
            path = main.tempPath + '/down'
            utils_functions.doWaitCursor()
            if utils_filesdirs.untarDirectory(fileVerac, path):
                OK = 2
                utils_functions.afficheMessage(main, 'FICHIER DECOMPRESSE')
        if OK == 2:
            # on récupère la date de la nouvelle version :
            dates_versions = utils_web.readDatesVersionsFile(main)
            dateVeracAfter = dates_versions[2]
            # on compare les dates avant et après update :
            if (dateVeracAfter == dateNew):
                OK = 3

    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if OK == 1:
            fileName = 'verac_web.tar.gz'
            m1 = QtWidgets.QApplication.translate(
                'main', 'UNABLE TO UNTAR THE NEW VERSION')
            m2 = QtWidgets.QApplication.translate(
                'main', 
                'Do you have the privileges to write into the installation directory?')
            m3 = QtWidgets.QApplication.translate(
                'main', 'You can also download the new version from the <b>{0}</b> site:')
            m3 = utils_functions.u(m3).format(utils.PROGNAMEHTML)
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p align="center">{2}</p>'
                '<p align="center">{0}</p>'
                '<p align="center">{3}</p>'
                '<p align="center"><a href="{4}{5}">{5}</a></p>'
                '<p></p>').format(utils.SEPARATOR_LINE, m1, m2, m3, utils.UPDATEURL, fileName)
            utils_functions.messageBox(main, level='warning', message=message)
        elif OK == 0:
            m1 = QtWidgets.QApplication.translate(
                'main', 'UNABLE TO DOWNLOAD THE NEW VERSION')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p></p>').format(utils.SEPARATOR_LINE, m1)
            utils_functions.messageBox(main, level='warning', message=message)



















def createConfigWebDB(main, 
                      doDBConfig=False, doConfig=False, doStates=False, 
                      first=False, postDB=True):
    """
    Création ou mise à jour de la base configweb.
        * si elle n'existe pas (upgrade ; paramètre first=True)
        * après avoir modifié un paramètre
        * doDBConfig, doConfig et doStates pour indiquer les tables à reconstruire
        * si postDB=False, on n'envoie pas le fichier (pour appel depuis admin_edit)
    """
    if first:
        doDBConfig = True
        doConfig = True
        doStates = True
    query_configweb = None
    webSiteOk = False
    try:
        utils_functions.doWaitCursor()
        admin.openDB(main, 'configweb')
        query_configweb, transactionOK = utils_db.queryTransactionDB(admin.db_configweb)

        # on commence par vérifier si le site web est en place :
        webSiteOk = admin_ftp.ftpTest(main)

        if doDBConfig:
            # on recrée la table :
            listCommands = [utils_db.qdf_table.format('dbconfig'), utils_db.qct_configweb_dbconfig]
            query_configweb = utils_db.queryExecute(listCommands, query=query_configweb)
            # on crée une liste des lignes à insérer :
            configs = [('versionDB', utils.VERSIONDB_CONFIGWEB, ''), ]
            configs.append(('webSiteOk', correspondances[webSiteOk], 'BOOLEAN'))
            # on écrit les lignes :
            commandLine = utils_db.insertInto('dbconfig', 3)
            query_configweb = utils_db.queryExecute(
                {commandLine: configs}, query=query_configweb)

        if doConfig:
            # on récupère la valeur de LANG :
            actualLocale = 'fr_FR'
            query_configweb = utils_db.query(admin.db_configweb)
            commandLine_configweb = 'SELECT * FROM config WHERE key_name="LANG"'
            query_configweb = utils_db.queryExecute(
                commandLine_configweb, query=query_configweb)
            if query_configweb.first():
                actualLocale = query_configweb.value(2)
                if actualLocale == 'fr':
                    actualLocale = 'fr_FR'
            # on recrée la table :
            listCommands = [utils_db.qdf_table.format('config'), utils_db.qct_config]
            query_configweb = utils_db.queryExecute(listCommands, query=query_configweb)
            # on crée une liste des lignes à insérer :
            configs = []
            configs.append(('VERSION_NAME', '', main.actualVersion['versionName']))
            configs.append(('VERSION_LABEL', '', main.actualVersion['versionLabel']))
            configs.append(('URL_SITE_PUBLIC', '', main.actualVersion['versionUrl']))
            configs.append(('PREFIXE_DB_PROF', '', admin.prefixProfFiles))
            relativeSecretPath = QtCore.QDir(
                admin.dirSitePublic).relativeFilePath(admin.dirSiteSecret)
            configs.append(('CHEMIN_SECRET_RELATIF', '', relativeSecretPath))
            configs.append(('LANG', '', actualLocale))
            configs.append(('EVAL_A', '', utils.affichages['A'][0]))
            configs.append(('EVAL_B', '', utils.affichages['B'][0]))
            configs.append(('EVAL_C', '', utils.affichages['C'][0]))
            configs.append(('EVAL_D', '', utils.affichages['D'][0]))
            configs.append(('EVAL_X', '', utils.affichages['X'][0]))
            configs.append(('COLOR_A', '', utils.affichages['A'][2]))
            configs.append(('COLOR_B', '', utils.affichages['B'][2]))
            configs.append(('COLOR_C', '', utils.affichages['C'][2]))
            configs.append(('COLOR_D', '', utils.affichages['D'][2]))
            configs.append(('COLOR_X', '', utils.affichages['X'][2]))

            # ICI à rendre modifiable ? ; FINIR
            configs.append(('TPS_MAX_INACTIVITE', 1800, ''))
            configs.append(('MODECALCUL', 0, ''))
            configs.append(('MODECALCULNOTES', 0, ''))
            configs.append(('NBMAXEVAL', 3, ''))
            configs.append(('COEFLAST', 0, 'BOOLEAN'))
            configs.append(('CALCULATRICE_A', 20, ''))
            configs.append(('CALCULATRICE_B', 14, ''))
            configs.append(('CALCULATRICE_C', 7, ''))
            configs.append(('CALCULATRICE_D', 0, ''))
            # on écrit les lignes :
            commandLine = utils_db.insertInto('config', 3)
            query_configweb = utils_db.queryExecute(
                {commandLine: configs}, query=query_configweb)

        if doStates:
            global states
            # on recrée la table :
            listCommands = [utils_db.qdf_table.format('state'), utils_db.qct_configweb_state]
            query_configweb = utils_db.queryExecute(listCommands, query=query_configweb)
            # on écrit les lignes :
            lines = []
            commandLine = utils_db.insertInto('state', 3)
            for state in states['LIST']:
                if state[:10] != 'SEPARATOR:':
                    lines.append((states[state][2], correspondances[states[state][0]], 'BOOLEAN'))
            query_configweb = utils_db.queryExecute(
                {commandLine: lines}, query=query_configweb)

        if first:
            # téléchargement du fichier constantes.php :
            theFileName = 'constantes.php'
            destDir = main.tempPath + '/down/'
            theUrl = utils.UPDATEURL
            downLoader = utils_web.UpDownLoader(
                main, theFileName, destDir, theUrl, down=True)
            downLoader.launch()
            while downLoader.state == utils_web.STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if downLoader.state == utils_web.STATE_OK:
                tempFile = utils_functions.u('{0}/down/{1}').format(
                    main.tempPath, theFileName)
                localFile = utils_functions.u('{0}/_private/{1}').format(
                    admin.dirLocalPublic, theFileName)
                utils_filesdirs.removeAndCopy(tempFile, localFile)

    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(admin.dbFile_configweb)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_db.endTransaction(query_configweb, admin.db_configweb, transactionOK)
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_configweb, admin.dbFile_configweb, testSize=10)
        utils_functions.restoreCursor()
        if postDB and webSiteOk:
            # On envoie le fichier sur le site :
            dirLocal = admin.dirLocalPublic + '/_private'
            dirSite = admin.dirSitePublic + '/_private'
            theFileName = 'configweb.sqlite'
            admin_ftp.ftpPutFile(main, dirSite, dirLocal, theFileName)
            if first:
                admin_ftp.ftpPutFile(main, dirSite, dirLocal, 'constantes.php')



