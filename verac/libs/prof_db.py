# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Gestion de la base perso du prof (enregistrement, etc.).
    Aussi un peu des bases commun, users et suivis.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_web, utils_filesdirs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
else:
    from PyQt4 import QtCore, QtGui as QtWidgets




def createBase_my(main):
    """
    À la connexion, si la base prof n'existe pas, elle est créée
    """
    utils_functions.doWaitCursor()
    try:
        listArgs = []
        for table in utils_db.listTablesProf:
            arg = utils_db.listTablesProf[table][0]
            listArgs.append(arg)
        utils_db.queryExecute(listArgs, db=main.db_my)
        # on inscrit la version de la base :
        utils_db.changeInConfigTable(
            main, main.db_my, {'versionDB': (utils.VERSIONDB_PROF, '')})
    finally:
        utils_functions.restoreCursor()


def updateConfigBase_my(main, date=-1):
    """
    Mise à jour de la table config
    avant d'enregistrer ou de poster
    """
    changes = {}
    if date < 10:
        date = int(
            QtCore.QDateTime.currentDateTime().toString('yyyyMMddhhmm'))
    changes['datetime'] = (date, utils_functions.getComputerInfos())
    changes['progversion'] = ('', utils.PROGVERSION)
    if main.actionShift.isChecked():
        changes['shift'] = (1, '')
    else:
        changes['shift'] = (0, '')
    if main.actionAutoSaveDBMy.isChecked():
        changes['autosave'] = (1, '')
    else:
        changes['autosave'] = (0, '')
    changes['autoselect'] = (main.autoselect, '')
    if main.actionShowDeletedEleves.isChecked():
        changes['showDeletedEleves'] = (1, '')
    else:
        changes['showDeletedEleves'] = (0, '')
    utils_db.changeInConfigTable(main, main.db_my, changes)
    query_my = utils_db.queryExecute(utils_db.qcmd_Vacuum, db=main.db_my)
    return date


def recalcMoyennesItems(main):
    import utils_calculs
    moyennesItems = {}
    commandLine_my = 'SELECT DISTINCT value FROM evaluations WHERE id_bilan=-1'
    query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
    while query_my.next():
        value = query_my.value(0)
        moyenne = utils_calculs.moyenneItem(value)
        moyennesItems[value] = moyenne
    commandLine_my = utils_db.qdf_table.format('moyennesItems')
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    lines = []
    for value in moyennesItems:
        lines.append((value, moyennesItems[value]))
    commandLine_my = utils_db.insertInto('moyennesItems')
    query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)


def saveBase_my(main):
    """
    Enregistrement de la base perso.
    On inscrit la date et on copie le fichier (actuellement dans temp)
    dans le dossier files
    """
    utils_functions.doWaitCursor()
    date = -1
    try:
        utils_functions.afficheMessage(main, 'saveBase_my')
        # mise à jour de la table config (date etc) :
        date = updateConfigBase_my(main)
        main.me['localDateTime'] = date
        # recalcul de la table moyennesItems :
        recalcMoyennesItems(main)
        # on enregistre d'abord dans temp (problèmes réseaux) :
        dbFile_myTemp = utils_functions.u('{0}/{1}.sqlite').format(
            main.tempPath, main.dbName_my)
        # suppression d'un éventuel fichier précédent :
        if QtCore.QFile(dbFile_myTemp).exists():
            removeOK = QtCore.QFile(dbFile_myTemp).remove()
            if not(removeOK):
                utils_functions.myPrint('REMOVE ERROR : ', dbFile_myTemp)
        # copie de la base :
        db_myTemp = utils_db.createConnection(main, dbFile_myTemp)[0]
        db_myTemp = utils_db.copyDBMy(main, 'db_memory', dbFile_myTemp)
        db_myTemp.close()
        del db_myTemp
        utils_db.sqlDatabase.removeDatabase(main.dbName_my)
        # copie du fichier :
        utils_filesdirs.removeAndCopy(dbFile_myTemp, main.dbFile_my)
    finally:
        utils_functions.restoreCursor()
        return date


def compareDatesBases_my(main, forcer=False):
    """
    Retourne les dates des fichiers local et distant
    (integer + str)
    et le résultat de leur comparaison.
    La date du fichier local est lue dans le dico main.me ou dans la base.
    Pour le fichier distant :
        * on tente d'abord par le fichier verac_prof_fileDateTime.php
        * si netDateTime vaut toujours -1, on tente en téléchargeant la base

    """
    result = {}
    noFileText = QtWidgets.QApplication.translate('main', 'NO FILE')

    # on lit la date du fichier local (localDateTime) :
    localDateTime = main.me.get('localDateTime', -1)
    if localDateTime < 0:
        localDateTime = utils_db.readInConfigTable(
            main.db_my, 'datetime', default_int=-1)[0]
        main.me['localDateTime'] = localDateTime
    # version texte :
    if localDateTime < 0:
        localDateTimeStr = noFileText
    else:
        localDateTimeStr = utils_functions.u(localDateTime)
        localDateTimeStr = QtCore.QDateTime().fromString(
            localDateTimeStr, 'yyyyMMddhhmm')
        localDateTimeStr = localDateTimeStr.toString('dd-MM-yyyy  hh:mm')
    result['localDateTimeStr'] = localDateTimeStr

    # on cherche la date du fichier distant (netDateTime) :
    if forcer:
        netDateTime = -1
    else:
        netDateTime = main.me.get('netDateTime', -1)
    if netDateTime < 0:
        theUrl = main.actualVersion['versionUrl'] + '/pages/verac_prof_fileDateTime.php'
        postData = utils_functions.u('id_prof={0}').format(main.me['userId'])
        httpClient = utils_web.HttpClient(main)
        httpClient.launch(theUrl, postData)
        while httpClient.state == utils_web.STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if httpClient.state == utils_web.STATE_OK:
            try:
                netDateTime = utils_functions.verifyLibs_toInt(
                    httpClient.reponse.split('|')[1])
            except:
                pass
    if netDateTime < 0:
        theFileName = main.dbName_my + '.sqlite'
        if utils_web.downloadFileFromSecretDir(main, '/up/files/', theFileName):
            db_my_net = utils_db.createConnection(
                main, main.tempPath + '/down/' + theFileName)[0]
            netDateTime = utils_db.readInConfigTable(
                db_my_net, 'datetime', default_int=-1)[0]
            db_my_net.close()
            del db_my_net
            utils_db.sqlDatabase.removeDatabase(main.dbName_my)
    if netDateTime < 10:
        netDateTime = -1
    main.me['netDateTime'] = netDateTime
    # version texte :
    if netDateTime < 0:
        netDateTimeStr = noFileText
    else:
        netDateTimeStr = utils_functions.u(netDateTime)
        netDateTimeStr = QtCore.QDateTime().fromString(
            netDateTimeStr, 'yyyyMMddhhmm')
        netDateTimeStr = netDateTimeStr.toString('dd-MM-yyyy  hh:mm')
    result['netDateTimeStr'] = netDateTimeStr

    # on compare :
    whoIsMostRecent = 'LOCAL'
    if netDateTime > -1:
        if netDateTime > localDateTime:
            whoIsMostRecent = 'REMOTE'
        elif netDateTime == localDateTime:
            whoIsMostRecent = 'SAME'
    result['whoIsMostRecent'] = whoIsMostRecent
    return result


def postDBMy(main, date=-1):
    """
    Envoi du fichier prof vers l'interface web.
    On enregistre d'abord dans temp/up (pour envoyer la version actuelle).
    On envoie via le fichier verac_prof_putFile.php qui place le fichier au bon endroit,
    et répond si tout c'est bien passé.
    """
    # on compare les dates avant pour prévenir les erreurs :
    comparaison = compareDatesBases_my(main, forcer=True)
    # affichage du message final :
    if comparaison['whoIsMostRecent'] == 'REMOTE':
        m1 = QtWidgets.QApplication.translate('main', 'localDate')
        m2 = QtWidgets.QApplication.translate('main', 'netDate')
        whoIsMostRecentMsg = QtWidgets.QApplication.translate(
            'main', 'The distant file seems most recent.')
        m3 = QtWidgets.QApplication.translate(
            'main', 'Are you sure you want to continue?')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p><b>{5}<br/>{6}</b></p>'
            '<p></p>'
            '<table border="0">'
            '<tr><td>{1} : </td><td><b>{2}</b></td></tr>'
            '<tr><td>{3} : </td><td><b>{4}</b></td></tr>'
            '</table>').format(
                utils.SEPARATOR_LINE, 
                m1, comparaison['localDateTimeStr'], 
                m2, comparaison['netDateTimeStr'], 
                whoIsMostRecentMsg, 
                m3)
        if utils_functions.messageBox(
            main, level='warning', 
            message=message, buttons=['Yes', 'Cancel']) != QtWidgets.QMessageBox.Yes:
            return False
    # donc on y va :
    utils_functions.doWaitCursor()
    upOK = False
    try:
        # mise à jour de la table config (date etc) :
        date = updateConfigBase_my(main, date=date)
        # recalcul de la table moyennesItems :
        recalcMoyennesItems(main)
        # on enregistre dans temp/up :
        theFileUp = utils_functions.u('{0}/up/{1}.sqlite').format(
            main.tempPath, main.dbName_my)
        # suppression d'un éventuel fichier précédent :
        if QtCore.QFile(theFileUp).exists():
            removeOK = QtCore.QFile(theFileUp).remove()
            if not(removeOK):
                utils_functions.myPrint('REMOVE ERROR : ', theFileUp)
        # copie de la base :
        db_myTemp = utils_db.createConnection(main, theFileUp)[0]
        db_myTemp = utils_db.copyDBMy(main, 'db_memory', theFileUp)
        db_myTemp.close()
        del db_myTemp
        utils_db.sqlDatabase.removeDatabase(main.dbName_my)
        # envoi du fichier :
        theFileName = main.dbName_my + '.sqlite'
        theUrl = main.siteUrlPublic + "/pages/verac_prof_putFile.php"
        upLoader = utils_web.UpDownLoader(
            main, theFileName, main.tempPath + "/up/", theUrl, down=False)
        upLoader.launch()
        while upLoader.state == utils_web.STATE_LAUNCHED:
            QtWidgets.QApplication.processEvents()
        if upLoader.state == utils_web.STATE_OK:
            if "OK" in utils_functions.u(upLoader.reponse):
                upOK = True
    finally:
        utils_functions.restoreCursor()
        if upOK:
            main.me['netDateTime'] = date
            message = QtWidgets.QApplication.translate(
                'main', 'The file was sent successfully.')
            utils_functions.messageBox(main, message=message, timer=5)
        else:
            message = QtWidgets.QApplication.translate(
                'main', 'A problem occurred during the transfer.')
            utils_functions.afficheMsgPb(main, message)


def compareBase_my(main):
    utils_functions.afficheMessage(main, 'compareBase_my')
    # récupération des données :
    comparaison = compareDatesBases_my(main, forcer=True)
    whoIsMostRecent = comparaison['whoIsMostRecent']
    # affichage du message final :
    m1 = QtWidgets.QApplication.translate('main', 'localDate')
    m2 = QtWidgets.QApplication.translate('main', 'netDate')
    if whoIsMostRecent == 'SAME':
        whoIsMostRecentMsg = QtWidgets.QApplication.translate(
            'main', 'The 2 files have the same date.')
        m3 = ''
        m4 = ''
    elif whoIsMostRecent == 'LOCAL':
        whoIsMostRecentMsg = QtWidgets.QApplication.translate(
            'main', 'The local file seems most recent.')
        m3 = QtWidgets.QApplication.translate(
            'main', 'Remember to send it.')
        m4 = QtWidgets.QApplication.translate(
            'main', '("File > SaveAndPostDBMy" menu)')
    else:
        whoIsMostRecentMsg = QtWidgets.QApplication.translate(
            'main', 'The distant file seems most recent.')
        m3 = QtWidgets.QApplication.translate(
            'main', 'You should download it.')
        m4 = QtWidgets.QApplication.translate(
            'main', '("File > DownloadDBMy" menu)')
        downloadNowText = QtWidgets.QApplication.translate('main', 'Download Now')
    message = utils_functions.u(
        '<p align="center">{0}</p>'
        '<p><b>{5}<br/>{6}</b><br/>{7}</p>'
        '<p></p>'
        '<table border="0">'
        '<tr><td>{1} : </td><td><b>{2}</b></td></tr>'
        '<tr><td>{3} : </td><td><b>{4}</b></td></tr>'
        '</table>').format(
            utils.SEPARATOR_LINE, 
            m1, comparaison['localDateTimeStr'], 
            m2, comparaison['netDateTimeStr'], 
            whoIsMostRecentMsg, 
            m3, m4)
    if whoIsMostRecent == 'REMOTE':
        reply = utils_functions.messageBox(
            main, 
            level='warning', 
            message=message, 
            buttons=[
                'Close', 
                ('net-download', downloadNowText, QtWidgets.QMessageBox.YesRole)])
        if reply == QtWidgets.QMessageBox.AcceptRole:
            downloadBase_my(main, verif=False, msgFin=False)
    else:
        utils_functions.messageBox(
            main, level='warning', message=message, buttons=['Close'])


def downloadBase_my(main, verif=True, msgFin=True):
    utils_functions.afficheMessage(main, 'downloadBase_my')
    if verif:
        # on compare les dates avant pour prévenir les erreurs :
        comparaison = compareDatesBases_my(main, forcer=True)
        # affichage du message final :
        if comparaison['whoIsMostRecent'] == 'LOCAL':
            whoIsMostRecentMsg = QtWidgets.QApplication.translate(
                'main', 'The local file seems most recent.')
            continueMsg = QtWidgets.QApplication.translate(
                'main', 'Are you sure you want to continue?')
            m1 = QtWidgets.QApplication.translate(
                'main', 'localDate')
            m2 = QtWidgets.QApplication.translate(
                'main', 'netDate')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p><b>{1}</b> : {2}</p>'
                '<p><b>{3}</b> : {4}</p>'
                '<p></p>'
                '<p><b>{5}<br/>{6}</b></p>'
                '<p></p>').format(
                    utils.SEPARATOR_LINE, 
                    comparaison['localDateTimeStr'], m1, 
                    comparaison['netDateTimeStr'], m2, 
                    whoIsMostRecentMsg, 
                    continueMsg)
            if utils_functions.messageBox(
                main, level='warning', 
                message=message, buttons=['Yes', 'Cancel']) != QtWidgets.QMessageBox.Yes:
                return False

    import utils_upgrades
    import prof, prof_groupes
    # on télécharge dans temp :
    dbFile_myTemp = utils_functions.u('{0}/{1}.sqlite').format(
        main.tempPath, main.dbName_my)
    # suppression d'un éventuel fichier précédent :
    if QtCore.QFile(dbFile_myTemp).exists():
        removeOK = QtCore.QFile(dbFile_myTemp).remove()
        if not(removeOK):
            utils_functions.myPrint('REMOVE ERROR : ', dbFile_myTemp)
    # téléchargement :
    theFileName = main.dbName_my + '.sqlite'
    if utils_web.downloadFileFromSecretDir(
        main, '/up/files/', theFileName, toDir=main.tempPath):
        # on copie le fichier en mémoire vive :
        db_myTemp = utils_db.createConnection(main, dbFile_myTemp)[0]
        main.db_my = utils_db.copyDBMy(main, dbFile_myTemp, 'db_memory')
        db_myTemp.close()
        del db_myTemp
        utils_db.sqlDatabase.removeDatabase(main.dbName_my)
        # test de upgrade de la base :
        main.versionDBMyIsOk = utils_upgrades.upgradeProf(main)
        # on met à jour l'affichage :
        main.reloadConfigDict()
        localDateTime = utils_db.readInConfigTable(
            main.db_my, 'datetime', default_int=-1)[0]
        main.me['localDateTime'] = localDateTime
        # si la base montre que VÉRAC est obsolète,
        # on désactive les envois enregistrements etc :
        if not(main.versionDBMyIsOk):
            main.actionAutoSaveDBMy.setChecked(False)
            actionsToDisable = (
                main.actionSaveDBMy,
                main.actionPostDBMy,
                main.actionSaveAsDBMy,
                main.actionAutoSaveDBMy,
                main.actionSaveAndPostDBMy,
                main.actionCompareDBMy,
                main.actionDownloadDBMy,
                main.actionTestUpdateDBUsersCommun,
                main.actionDownloadSuivisDB,
                main.actionPostSuivis)
            for action in actionsToDisable:
                action.setEnabled(False)
        # on termine :
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)


def saveAsBase_my(main):
    utils_functions.afficheMessage(main, 'saveAsBase_my')
    title = QtWidgets.QApplication.translate('main', 'Choose a Directory')
    directory = QtWidgets.QFileDialog.getExistingDirectory(
        main, 
        title, 
        QtCore.QDir.homePath(), 
        QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
    if directory == '':
        return
    utils_functions.doWaitCursor()
    try:
        # mise à jour de la table config (date etc) :
        updateConfigBase_my(main)
        # recalcul de la table moyennesItems :
        recalcMoyennesItems(main)
        # on enregistre d'abord dans temp (problèmes réseaux) :
        dbFile_myTemp = utils_functions.u('{0}/{1}.sqlite').format(
            main.tempPath, main.dbName_my)
        # suppression d'un éventuel fichier précédent :
        if QtCore.QFile(dbFile_myTemp).exists():
            removeOK = QtCore.QFile(dbFile_myTemp).remove()
            if not(removeOK):
                utils_functions.myPrint('REMOVE ERROR : ', dbFile_myTemp)
        # copie de la base :
        db_myTemp = utils_db.createConnection(main, dbFile_myTemp)[0]
        db_myTemp = utils_db.copyDBMy(main, 'db_memory', dbFile_myTemp)
        db_myTemp.close()
        del db_myTemp
        utils_db.sqlDatabase.removeDatabase(main.dbName_my)
        # copie du fichier :
        dbFile_my_save = utils_functions.u('{0}/{1}.sqlite').format(
            directory, main.dbName_my)
        utils_filesdirs.removeAndCopy(dbFile_myTemp, dbFile_my_save)
    finally:
        utils_functions.restoreCursor()


def testUpdateDBUsersCommunSuivis(main, testOnly=False):
    """
    teste s'il faut mettre à jour les bases commun, users et suivis.
    Si besoin, les bases sont téléchargées.
    """
    def date2Str(date):
        # mise en forme des dates pour affichage :
        return QtCore.QDateTime().fromString(date, 'yyyyMMddhhmm').toString('dd-MM-yyyy')

    # récupération des dates du site :
    destDir = main.tempPath + '/down/'
    theFileName = 'dates_db.html'
    if not(utils_web.downloadFileFromSecretDir(main, '/public/', theFileName)):
        return
    datesWeb = utils_functions.readDatesDBFile(main, destDir)
    if testOnly:
        messageCommunOK = QtWidgets.QApplication.translate(
            'main', 'Your Commun DB version is the last')
        messageUsersOK = QtWidgets.QApplication.translate(
            'main', 'Your Users DB version is the last')
        messageCommunMustBeUpdated = QtWidgets.QApplication.translate(
            'main', 'Your Commun DB must be updated')
        messageUsersMustBeUpdated = QtWidgets.QApplication.translate(
            'main', 'Your Users DB must be updated')
        messageCurrentVersion = QtWidgets.QApplication.translate(
            'main', 'Date of current version: ')
        messageYourVersion = QtWidgets.QApplication.translate(
            'main', 'Date of your version: ')
        messageDoNow = QtWidgets.QApplication.translate(
            'main', 'Would you like to upgrade now?')
        messageDownloadAnyway = QtWidgets.QApplication.translate(
            'main', 'Download anyway')
        message = ''
        mustUpdate = [False, False]
    # récupération des dates locales :
    hereDir = main.localConfigDir + '/' + main.actualVersion['versionName'] + '/'
    datesHere = utils_functions.readDatesDBFile(main, hereDir)
    utils_functions.myPrint('DatesDBUsersCommun :', datesWeb, datesHere)
    if not(testOnly):
        # on copie le fichier dates_db.html dans configDir :
        theFile = destDir + theFileName
        theFileHere = hereDir + theFileName
        utils_filesdirs.removeAndCopy(theFile, theFileHere)
    # on compare et on télécharge la base commun si besoin :
    dateWeb = datesWeb[0]
    dateHere = datesHere[0]
    if testOnly:
        if dateHere < dateWeb:
            m1 = messageCommunMustBeUpdated
            mustUpdate[0] = True
        else:
            m1 = messageCommunOK
        message = utils_functions.u(
            '{1}'
            '<p align="center">{0}</p>'
            '<p><b>{2}</b></p>'
            '<p>{3}<b>{4}</b>'
            '<br/>{5}<b>{6}</b></p>'
            '').format(
                utils.SEPARATOR_LINE, 
                message, m1,
                messageCurrentVersion, date2Str(dateWeb), 
                messageYourVersion, date2Str(dateHere))
    else:
        if dateHere < dateWeb:
            downloadCommunDB(main, msgFin=False)
    # on compare et on télécharge la base users si besoin :
    dateWeb = datesWeb[1]
    dateHere = datesHere[1]
    if testOnly:
        if dateHere < dateWeb:
            m1 = messageUsersMustBeUpdated
            mustUpdate[1] = True
        else:
            m1 = messageUsersOK
        message = utils_functions.u(
            '{1}'
            '<p align="center">{0}</p>'
            '<p><b>{2}</b></p>'
            '<p>{3}<b>{4}</b>'
            '<br/>{5}<b>{6}</b></p>'
            '').format(
                utils.SEPARATOR_LINE, 
                message, m1,
                messageCurrentVersion, date2Str(dateWeb), 
                messageYourVersion, date2Str(dateHere))
    else:
        if dateHere < dateWeb:
            downloadUsersDB(main, msgFin=False)
    # test de la base suivis :
    if not(testOnly):
        downloadSuivisDB(main, msgFin=False)
    if testOnly:
        if (mustUpdate == [False, False]):
            message = utils_functions.u(
                '{0}<p></p>').format(message)
            reply = utils_functions.messageBox(
                main, 
                message=message, 
                buttons=[
                    'Close', 
                    ('database-update', messageDownloadAnyway, QtWidgets.QMessageBox.YesRole)
                    ])
            if reply == QtWidgets.QMessageBox.AcceptRole:
                # on copie le fichier dates_db.html dans configDir :
                theFile = destDir + theFileName
                theFileHere = hereDir + theFileName
                utils_filesdirs.removeAndCopy(theFile, theFileHere)
                downloadCommunDB(main, msgFin=False)
                downloadUsersDB(main, msgFin=False)
        else:
            message = utils_functions.u(
                '{1}'
                '<p align="center">{0}</p>'
                '<p><b>{2}</b></p>'
                '<p></p>').format(utils.SEPARATOR_LINE, message, messageDoNow)
            reply = utils_functions.messageBox(
                main, level='warning', message=message, buttons=['Yes', 'No'])
            if reply == QtWidgets.QMessageBox.Yes:
                if mustUpdate[0]:
                    downloadCommunDB(main, msgFin=False)
                if mustUpdate[1]:
                    downloadUsersDB(main, msgFin=False)
                # on copie le fichier dates_db.html dans configDir :
                theFile = destDir + theFileName
                theFileHere = hereDir + theFileName
                utils_filesdirs.removeAndCopy(theFile, theFileHere)


def downloadUsersDB(main, msgFin=True, doUpdateGroups=True):
    utils_functions.afficheMessage(main, 'downloadUsersDB')
    theFileName = 'users.sqlite'
    if (utils.lanConfig and utils.lanConfigDir != ''):
        destDir = utils.lanConfigDir
    else:
        destDir = main.localConfigDir
    destDir += '/' + main.actualVersion['versionName']
    utils_db.closeConnection(main, dbName='users')
    if utils_web.downloadFileFromSecretDir(main, '/public/', theFileName, toDir=destDir):
        utils_filesdirs.removeAndCopy(main.dbFile_users, main.dbFileTemp_users)
        main.db_users = utils_db.createConnection(main, main.dbFileTemp_users)[0]
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
    else:
        utils_filesdirs.removeAndCopy(main.dbFile_users, main.dbFileTemp_users)
        main.db_users = utils_db.createConnection(main, main.dbFileTemp_users)[0]
    if doUpdateGroups:
        import prof_groupes
        prof_groupes.updateGroupClass(main)


def downloadCommunDB(main, msgFin=True):
    utils_functions.afficheMessage(main, 'downloadCommunDB')
    theFileName = 'commun.sqlite'
    if (utils.lanConfig and utils.lanConfigDir != ''):
        destDir = utils.lanConfigDir
    else:
        destDir = main.localConfigDir
    destDir += '/' + main.actualVersion['versionName']
    utils_db.closeConnection(main, dbName='commun')
    if utils_web.downloadFileFromSecretDir(main, '/public/', theFileName, toDir=destDir):
        utils_filesdirs.removeAndCopy(main.dbFile_commun, main.dbFileTemp_commun)
        main.db_commun = utils_db.createConnection(main, main.dbFileTemp_commun)[0]
        checkCommunBilans(main, msgFin=False)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
    else:
        utils_filesdirs.removeAndCopy(main.dbFile_commun, main.dbFileTemp_commun)
        main.db_commun = utils_db.createConnection(main, main.dbFileTemp_commun)[0]


def downloadSuivisDB(main, msgFin=True):
    utils_functions.afficheMessage(main, 'downloadSuivisDB')
    theFileName = 'suivis.sqlite'
    if (utils.lanConfig and utils.lanConfigDir != ''):
        destDir = utils.lanConfigDir
    else:
        destDir = main.localConfigDir
    destDir += '/' + main.actualVersion['versionName']
    if utils_web.downloadFileFromSecretDir(main, '/protected/suivis/', theFileName, toDir=destDir):
        try:
            del main.elevesSuivis
        except:
            pass
        if main.viewType == utils.VIEW_SUIVIS:
            main.doShow(doReload=True)
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)


def createYearArchive(main, msgFin=True):
    """
    Crée une archive tar.gz de sauvegarde de l'année scolaire.
    L'archive contient :
        * le dossier .Verac
        * le dossier verac (logiciel)
        le fichier profxx.sqlite
    Comme elle contient la version du logiciel correspondant au moment de l'archive,
    elle sera toujours lisible. 
    """
    # calcul de l'année scolaire (pour 2010-2011, on prendra 1011) :
    annee_scolaire = ''
    currentDate = QtCore.QDateTime.currentDateTime().date()
    year = currentDate.year()
    # à partir du mois d'octobre, c'est l'année suivante :
    if currentDate.month() > 9:
        year += 1
    year = year % 1000
    annee_scolaire = utils_functions.u('{0}{1}').format(year - 1, year)
    # choix du nom de l'archive à créer (et chemin)
    targzTitle = QtWidgets.QApplication.translate('main', 'Save tar.gz File')
    proposedName = utils_functions.u('{0}/archive_verac_{1}.tar.gz').format(
        main.workDir, annee_scolaire)
    targzExt = QtWidgets.QApplication.translate('main', 'tar.gz files (*.tar.gz)')
    fileName = QtWidgets.QFileDialog.getSaveFileName(
        main, targzTitle, proposedName, targzExt)
    fileName = utils_functions.verifyLibs_fileName(fileName)
    if fileName == '':
        return
    main.workDir = QtCore.QFileInfo(fileName).absolutePath()
    utils_functions.doWaitCursor()
    result = False
    try:
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'copy of the data in the temporary folder'), 
            console=True)
        # création d'un dossier d'archive dans temp :
        archiveName = 'archive_{0}'.format(annee_scolaire)
        utils_filesdirs.createDirs(main.tempPath, '{0}/.Verac'.format(archiveName))
        utils_filesdirs.createDirs(main.tempPath, '{0}/verac'.format(archiveName))
        destFile = main.tempPath + '/{0}/'.format(archiveName) + QtCore.QFileInfo(main.dbFile_my).fileName()
        QtCore.QFile(main.dbFile_my).copy(destFile)
        # copie du dossier .Verac (configuration) :
        utils_filesdirs.copyDir(main.localConfigDir, main.tempPath + '/{0}/.Verac'.format(archiveName))
        # copie du dossier verac (installation du logiciel) :
        utils_filesdirs.copyDir(main.beginDir, main.tempPath + '/{0}/verac'.format(archiveName))
        # compression et enregistrement :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'creation of the archive file'), 
            console=True)
        directory = main.tempPath + '/{0}'.format(archiveName)
        tarName = QtCore.QFileInfo(directory).baseName()
        if utils_filesdirs.tarDirectory(tarName, directory, main.beginDir):
            originFile = directory + '.tar.gz'
            destFile = fileName
            utils_filesdirs.removeAndCopy(originFile, destFile)
            QtCore.QFile(originFile).remove()
        # suppression du dossier temp/archiveName :
        utils_filesdirs.emptyDir(main.tempPath + '/{0}'.format(archiveName))
        # on propose de nettoyer la base :
        m1 = QtWidgets.QApplication.translate(
            'main', 'You also want to start cleaning up your file?')
        m2 = QtWidgets.QApplication.translate(
            'main', 'This is done only to change the school year.')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>'
            '<p></p>'
            '<p align="center"><b>{2}</b></p>').format(utils.SEPARATOR_LINE, m1, m2)
        if utils_functions.messageBox(
            main, level='warning', 
            message=message, buttons=['Yes', 'No']) == QtWidgets.QMessageBox.Yes:
            clearForNewYear(main, msgBegin=False, msgFin=False)
        result = True
    finally:
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if result:
            if msgFin:
                utils_functions.afficheMsgFin(main, timer=5)
        else:
            utils_functions.afficheMsgPb(main)


def clearForNewYear(main, msgBegin=True, msgFin=True):
    """
    grand nettoyage de début d'année scolaire.
    """
    # commencer par demander une confirmation :
    if msgBegin:
        m1 = QtWidgets.QApplication.translate(
            'main', 
            "Your data (except substructure) will be re-initialized "
            "to begin one New Year's Day school.")
        m2 = QtWidgets.QApplication.translate(
            'main', 'Are you certain to want to continue?')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>'
            '<p></p>'
            '<p align="center">{2}</p>').format(utils.SEPARATOR_LINE, m1, m2)
        if utils_functions.messageBox(
            main, level='warning', 
            message=message, buttons=['Yes', 'Cancel']) != QtWidgets.QMessageBox.Yes:
            return False

    import prof, prof_groupes
    utils_functions.doWaitCursor()
    result = False
    try:
        query_my = utils_db.query(main.db_my)
        # liste des tables à ne pas vider 
        # (tableau_item et counts seront nettoyées ensuite) :
        tablesToIgnore = (
            'config', 
            'items', 'bilans', 'item_bilan', 'bilan_bilan', 'comments', 
            'templates', 'tableau_item', 'counts', 
            'profils', 'profil_bilan_BLT')
        # on recrée toutes les autres :
        for table in utils_db.listTablesProf:
            if not(table in tablesToIgnore):
                query_my = utils_db.queryExecute(
                    utils_db.qdf_table.format(table), query=query_my)
                query_my = utils_db.queryExecute(
                    utils_db.listTablesProf[table][0], query=query_my)
        # pour la table counts, on garde les modèles :
        commandLine_my = 'DELETE FROM counts WHERE id_groupe>-1'
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # pour la table tableau_item, on doit conserver les templates :
        tableau_item_list = []
        commandLine_my = utils_db.q_selectAllFrom.format('tableau_item')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_tableau = int(query_my.value(0))
            id_item = int(query_my.value(1))
            ordre = int(query_my.value(2))
            if id_tableau < 0:
                # on ne garde que les templates (id_tableau < 0)
                utils_functions.appendUnique(
                    tableau_item_list, (id_tableau, id_item, ordre))
        # recréation de la table :
        commandLines = [
            utils_db.qdf_table.format('tableau_item'), 
            utils_db.qct_prof_tableauItem]
        query_my = utils_db.queryExecute(commandLines, query=query_my)
        # on la remplit à partir de la liste tableau_item_list :
        commandLine_my = utils_db.insertInto('tableau_item')
        query_my = utils_db.queryExecute(
            {commandLine_my: tableau_item_list}, query=query_my)
        # on déprotège toutes les périodes :
        utils_db.changeInConfigTable(
            main, 
            main.db_my, 
            {'ProtectedPeriods': (0, '')})
        for i in range(utils.NB_PERIODES):
            utils.changeProtectedPeriod(i, False)
        utils.changeProtectedPeriod(-1, False)
        utils.changeProtectedPeriod(999, False)
        # on incrémente l'année scolaire :
        year = utils_db.readInConfigDict(
            main.configDict, 'annee_scolaire', default_int=0)[0]
        year += 1
        utils_db.changeInConfigTable(
            main, main.db_my, {'annee_scolaire': (year, '')})
        # enfin on recharge la vue :
        prof_groupes.initGroupeMenu(main)
        prof.tableauxInit(main)
        main.changeDBMyState()
        result = True
    finally:
        utils_functions.restoreCursor()
        if result:
            if msgFin:
                utils_functions.afficheMsgFin(main, timer=5)
        else:
            utils_functions.afficheMsgPb(main)


def initProtectedPeriods(main):
    """
    mise à jour de la liste des périodes protégées.
    Au lancement ou après modification.
    """
    utils.clearProtectedPeriods()
    protectedPeriods = utils_db.readInConfigDict(
        main.configDict, 'ProtectedPeriods')[1]
    protectedPeriods = protectedPeriods.split('|')
    for i in range(utils.NB_PERIODES):
        period = '{0}'.format(i)
        if period in protectedPeriods:
            utils.changeProtectedPeriod(i, True)
        else:
            utils.changeProtectedPeriod(i, False)
    utils.changeProtectedPeriod(-1, False)
    utils.changeProtectedPeriod(999, False)


class ProtectedPeriodsDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue toute prête avec une liste de QCheckBox.
    """
    def __init__(self, parent=None, helpContextPage='periodes'):
        """
        mise en place de l'interface
        """
        super(ProtectedPeriodsDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Protected periods'))

        self.helpContextPage = helpContextPage

        self.checkBoxList = []
        for i in range(1, utils.NB_PERIODES):
            checkBox = QtWidgets.QCheckBox(utils.PERIODES[i])
            checkBox.setChecked(utils.PROTECTED_PERIODS[i])
            self.checkBoxList.append(checkBox)

        listeLayout = QtWidgets.QVBoxLayout()
        for checkBox in self.checkBoxList:
            listeLayout.addWidget(checkBox)
        listeCheckBox = QtWidgets.QGroupBox()
        listeCheckBox.setLayout(listeLayout)
        listeCheckBox.setFlat(True)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listeCheckBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage)

def changeProtectedPeriods(main):
    """
    Gestion des périodes protégées.
    """
    dialog = ProtectedPeriodsDlg(parent=main)
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
        mustSave = False
        protectedPeriods = []
        for i in range(len(dialog.checkBoxList)):
            new = dialog.checkBoxList[i].isChecked()
            if new:
                protectedPeriods.append('{0}'.format(i + 1))
            old = utils.PROTECTED_PERIODS[i + 1]
            if new != old:
                mustSave = True
        if mustSave:
            protectedPeriods = '|'.join(protectedPeriods)
            utils_db.changeInConfigTable(
                main, 
                main.db_my, 
                {'ProtectedPeriods': (0, protectedPeriods)})
            main.configDict = utils_db.configTable2Dict(main.db_my)
            initProtectedPeriods(main)
            main.changeDBMyState()


def checkCommunBilans(main, msgFin=True, fileProfxx=None):
    """
    Vérifie et corrige les bilans issus de la table bulletin, referentiel ou confidentiel.
    Il peut y avoir des incohérences si on a importé une liste de compétences 
        utilisant d'autres tables, ou si l'admin a modifié ces tables.
    Cette procédure peut être lancée via l'interface prof, ou automatiquement
        après une mise à jour de la base commun ou un import des compétences.
    La base peut être passée en paramètre, 
        ce qui permet à l'admin de mettre toutes les bases à jour lors d'une récup.
    """
    if fileProfxx != None:
        fileProfxxTemp = utils_functions.u('{0}/{1}.sqlite').format(main.tempPath, 'profxx')
        utils_filesdirs.removeAndCopy(fileProfxx, fileProfxxTemp)
        (main.db_my, dbName) = utils_db.createConnection(main, fileProfxxTemp)
    modified = False
    # besoin d'une transaction :
    query_my, transactionOK = utils_db.queryTransactionDB(main.db_my)
    query_commun = utils_db.query(main.db_commun)

    # PREMIÈRE PARTIE : RÉPARATION DES BILANS QUI SERAIENT EN DOUBLE (PB NADIA) :
    doubles = {}
    commandLine_my = (
        'SELECT b1.id_bilan, b2.id_bilan '
        'FROM bilans AS b1 '
        'JOIN bilans AS b2 '
        'ON (b1.Name=b2.Name AND b1.id_bilan<b2.id_bilan) '
        'ORDER BY b1.Name')
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_bilan1 = int(query_my.value(0))
        id_bilan2 = int(query_my.value(1))
        doubles[id_bilan2] = id_bilan1
    if len(doubles) > 0:
        modified = True
        commandLine_my2 = 'UPDATE {0} SET id_bilan={1} WHERE id_bilan={2}'
        for id_bilan2 in doubles:
            id_bilan1 = doubles[id_bilan2]
            # on efface id_bilan2 de la table bilans
            # et on remplace id_bilan2 par id_bilan1
            # dans les tables utilisant id_bilan :
            commandLines = [
                utils_db.qdf_where.format('bilans', 'id_bilan', id_bilan2), ]
            for table in ('item_bilan', 'groupe_bilan', 'profil_bilan_BLT', 'evaluations'):
                commandLines.append(commandLine_my2.format(table, id_bilan1, id_bilan2))
            query_my = utils_db.queryExecute(commandLines, query=query_my)
        # on traite à part la table bilan_bilan (plus complexe) :
        lines = []
        commandLine_my = utils_db.q_selectAllFrom.format('bilan_bilan')
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_bilan1 = int(query_my.value(1))
            if id_bilan1 in doubles:
                id_bilan1 = doubles[id_bilan1]
            id_bilan2 = int(query_my.value(2))
            if id_bilan2 in doubles:
                id_bilan2 = doubles[id_bilan2]
            if id_bilan1 != id_bilan2:
                # appendUnique pour éviter les doublettes :
                utils_functions.appendUnique(lines, (id_bilan1, id_bilan2))
        # on recrée la table bilan_bilan :
        query_my = utils_db.queryExecute(
            utils_db.qdf_table.format('bilan_bilan'), query=query_my)
        query_my = utils_db.queryExecute(
            utils_db.listTablesProf['bilan_bilan'][0], query=query_my)
        commandLine_my = utils_db.insertInto('bilan_bilan')
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

    # DEUXIÈME PARTIE : RÉPARATION DES BILANS COMMUNS DÉJÀ LÀ :
    # récupération dans la base prof des bilans provenant du bulletin, etc :
    lists = (('confidentiel', [], utils.decalageCFD), 
            ('bulletin', [], utils.decalageBLT), 
            ('referentiel', [], 0))
    commandLine_my = 'SELECT * FROM bilans WHERE id_competence>-1'
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_bilan = int(query_my.value(0))
        Name = query_my.value(1)
        Label = query_my.value(2)
        id_competence = int(query_my.value(3))
        if id_competence > utils.decalageCFD:
            lists[0][1].append((id_bilan, Name, Label, id_competence))
        elif id_competence > utils.decalageBLT:
            lists[1][1].append((id_bilan, Name, Label, id_competence))
        else:
            lists[2][1].append((id_bilan, Name, Label, id_competence))
    # des commandes utiles :
    commandLine_commun1 = utils_functions.u(
        'SELECT * FROM {0} WHERE code=? AND Competence=?')
    commandLine_commun2 = utils_functions.u('SELECT * FROM {0} WHERE code=?')
    commandLine_commun3 = utils_functions.u('SELECT * FROM {0} WHERE Competence=?')
    commandLine_my1 = 'UPDATE bilans SET id_competence={0} WHERE id_bilan={1}'
    commandLine_my2 = utils_functions.u(
        'UPDATE bilans SET id_competence=?, Label=? WHERE id_bilan=?')
    commandLine_my3 = utils_functions.u(
        'UPDATE bilans SET id_competence=?, Name=? WHERE id_bilan=?')
    commandLine_my4 = 'UPDATE bilans SET id_competence=-1 WHERE id_bilan={0}'
    # on traite chacune des 3 tables (confidentiel, bulletin et referentiel) :
    for listTableCpt in lists:
        (table, listCpt, decalage) = listTableCpt
        for (id_bilan, Name, Label, id_competence) in listCpt:
            ok = False
            id_prof = id_competence - decalage
            # test n°1 : Name et Label sont valables
            commandLine_commun = commandLine_commun1.format(table)
            query_commun = utils_db.queryExecute(
                {commandLine_commun: (Name, Label)}, query=query_commun)
            if query_commun.first():
                id_commun = int(query_commun.value(0))
                ok = True
            if ok:
                # on corrige id_competence si besoin :
                if id_commun != id_prof:
                    new_id_prof = id_commun + decalage
                    commandLine_my = commandLine_my1.format(new_id_prof, id_bilan)
                    #utils_functions.myPrint(table, ':', commandLine_my)
                    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                    modified = True
                continue
            # test n°2 : Name valable, mais pas Label
            commandLine_commun = commandLine_commun2.format(table)
            query_commun = utils_db.queryExecute(
                {commandLine_commun: (Name, )}, query=query_commun)
            if query_commun.first():
                id_commun = int(query_commun.value(0))
                Competence = query_commun.value(5)
                ok = True
            if ok:
                # on corrige Label et id_competence si besoin :
                new_id_prof = id_commun + decalage
                query_my = utils_db.queryExecute(
                    {commandLine_my2: (new_id_prof, Competence, id_bilan)}, 
                    query=query_my)
                modified = True
                continue
            # test n°3 : Label valable, mais pas Name
            commandLine_commun = commandLine_commun3.format(table)
            query_commun = utils_db.queryExecute(
                {commandLine_commun: (Label, )}, query=query_commun)
            if query_commun.first():
                id_commun = int(query_commun.value(0))
                code = query_commun.value(1)
                ok = True
            if ok:
                # on corrige Name et id_competence si besoin :
                new_id_prof = id_commun + decalage
                query_my = utils_db.queryExecute(
                    {commandLine_my3: (new_id_prof, code, id_bilan)}, 
                    query=query_my)
                modified = True
                continue
            # test n°4 : pas trouvé ; on passe en bilan perso
            commandLine_my = commandLine_my4.format(id_bilan)
            #utils_functions.myPrint(table, ':', commandLine_my)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            modified = True
            continue

    # TROISIÈME PARTIE : RÉPARATION DES BILANS PERSOS QUI DEVRAIENT ÊTRE COMMUNS :
    persos = []
    commandLine_my = 'SELECT * FROM bilans WHERE id_competence<0'
    query_my = utils_db.queryExecute(commandLine_my, query=query_my)
    while query_my.next():
        id_bilan = int(query_my.value(0))
        Name = query_my.value(1)
        Label = query_my.value(2)
        id_competence = int(query_my.value(3))
        persos.append((id_bilan, Name, Label, id_competence))
    # on traite chacune des 3 tables (confidentiel, bulletin et referentiel) :
    commandLine_commun1 = utils_functions.u(
        'SELECT * FROM {0} WHERE code=? AND Competence!=""')
    commandLine_my2 = utils_functions.u(
        'UPDATE bilans SET id_competence=?, Label=? WHERE id_bilan=?')
    lists = (('confidentiel', utils.decalageCFD), 
            ('bulletin', utils.decalageBLT), 
            ('referentiel', 0))
    for (id_bilan, Name, Label, id_competence) in persos:
        found = False
        for (table, decalage) in lists:
            if found:
                continue
            # test n°1 : Name est valable
            commandLine_commun = commandLine_commun1.format(table)
            query_commun = utils_db.queryExecute(
                {commandLine_commun: (Name, )}, query=query_commun)
            if query_commun.first():
                id_commun = int(query_commun.value(0))
                Competence = query_commun.value(5)
                found = True
            if found:
                # on corrige Label et id_competence si besoin :
                new_id_prof = id_commun + decalage
                query_my = utils_db.queryExecute(
                    {commandLine_my2: (new_id_prof, Competence, id_bilan)}, 
                    query=query_my)
                modified = True
                continue

    if modified and (fileProfxx == None):
        main.changeDBMyState()
    utils_db.endTransaction(query_my, main.db_my, transactionOK)
    if fileProfxx != None:
        query_my.clear()
        del query_my
        main.db_my.close()
        del main.db_my
        utils_db.sqlDatabase.removeDatabase(dbName)
        utils_filesdirs.removeAndCopy(fileProfxxTemp, fileProfxx)
    if msgFin:
        utils_functions.afficheMsgFin(main, timer=5)
    return modified


