# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
Pour consulter et éditer une base sqlite.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_filesdirs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtSql
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4 import QtSql



###########################################################"
#   VARIABLES GLOBALES
###########################################################"

DB_FILE_DIR = ''
DB_FILE_TEMP = ''
ADMIN_DB = None
NO_EDITABLES = ()
MUST_SAVE = False
SAVE_BUTTON = None

def initAdminDB(main):
    """
    Première initialisation de certaines variables globales dépendant du module admin
    """
    global DB_FILE_DIR, DB_FILE_TEMP, NO_EDITABLES
    if DB_FILE_DIR != '':
        return
    import admin
    DB_FILE_DIR = admin.adminDir
    DB_FILE_TEMP = main.tempPath + '/temp_db.sqlite'
    NO_EDITABLES = (
        admin.dbFile_admin, 
        admin.dbFile_recupEvals, 
        admin.dbFile_referentialPropositions, 
        admin.dbFile_referentialValidations, 
        admin.dbFile_documents, 
        admin.dbFile_compteur, 
        admin.dbFile_resultats, 
        main.dbFile_users, 
        admin.dbFileFtp_users, 
        admin.dbFileFtp_commun, 
        main.dbFile_commun, 
        admin.dbFile_commun)



class AdminDBItemDelegate(QtWidgets.QItemDelegate):
    """
    Un délégate pour gérer les éditions des cellules.
    Selon le type de valeur, on utilise un QSpinBox,
        un QLineEdit ou un QTextEdit.
    """
    def __init__(self, parent=None, model=None):
        super(AdminDBItemDelegate, self).__init__(parent)
        # on a besoin de récupérer le model :
        self.model = model
        # what est le type de donnée :
        self.what = utils.INDETERMINATE

    def createEditor(self, parent, option, index):
        # selon le type de donnée (what)
        # on utilise un objet différent :
        row = index.row()
        column = index.column()
        self.what = self.model.datas[row][column][1]
        value = self.model.datas[row][column][0]
        if self.what in utils.TYPES_NUMBER:
            editor = QtWidgets.QLineEdit(parent)
            # on accepte les chiffres, le point et le séparateur décimal:
            regExp = QtCore.QRegExp('[0-9\{0}\.]*'.format(utils.DECIMAL_SEPARATOR))
            editor.setValidator(QtGui.QRegExpValidator(regExp, editor))
        elif self.what == utils.LINE:
            editor = QtWidgets.QLineEdit(parent)
        else:
            editor = QtWidgets.QTextEdit(parent)
        return editor

    def setEditorData(self, editor, index):
        row = index.row()
        column = index.column()
        value = self.model.datas[row][column][0]
        if self.what in utils.TYPES_NUMBER:
            editor.setText(utils_functions.u('{0}').format(value))
        elif self.what == utils.LINE:
            editor.setText(value)
        else:
            editor.setText(value)

    def setModelData(self, editor, model, index):
        row = index.row()
        column = index.column()
        if self.what in utils.TYPES_NUMBER:
            value = editor.text()
            value = value.replace(utils.DECIMAL_SEPARATOR, '.')
            if value != '':
                if (utils.DECIMAL_SEPARATOR in value) or ('.' in value):
                    try:
                        value = float(value)
                    except:
                        value = model.datas[row][column][0]
                else:
                    try:
                        value = int(value)
                    except:
                        value = model.datas[row][column][0]
        elif self.what == utils.LINE:
            value = editor.text()
        else:
            value = editor.toPlainText()
        # mise à jour des données :
        if value != model.datas[row][column][0]:
            model.datas[row][column][0] = value
            model.setData(index, value, QtCore.Qt.EditRole)
            model.saveChanges()

    def updateEditorGeometry(self, editor, option, index):
        editor.setGeometry(option.rect)


class AdminDBTableModel(QtGui.QStandardItemModel):
    """
    AdminDBTableModel est un model pour lire et afficher une table de base sqlite.
    variable utiles :
        * headers : les titres des colonnes
        * datas : les données [valeur, type]
        * emptyRow : une ligne vide pour les ajouts de lignes
    """
    def __init__(self, parent=None, editable=True):
        super(AdminDBTableModel, self).__init__(parent)
        self.main = parent
        self.editable = editable
        self.tableName = ''
        self.headers = []
        self.datas = []
        self.emptyRow = []
        self.setTable()

    def saveChanges(self):
        global MUST_SAVE
        utils_functions.doWaitCursor()
        try:
            query, transactionOK = utils_db.queryTransactionDB(ADMIN_DB)
            # on vide la table :
            commandLine = utils_db.qdf_table.format(self.tableName)
            query = utils_db.queryExecute(commandLine, query=query)

            forInsert = ''
            for header in self.headers:
                if forInsert == '':
                    forInsert = '?'
                else:
                    forInsert = '{0}, ?'.format(forInsert)
            commandLine = 'INSERT INTO {0} VALUES ({1})'.format(self.tableName, forInsert)
            lines = []
            for row in self.datas:
                newRow = []
                for data in row:
                    newRow.append(data[0])
                lines.append(newRow)
            query = utils_db.queryExecute({commandLine: lines}, query=query)
        finally:
            utils_db.endTransaction(query, ADMIN_DB, transactionOK)
            MUST_SAVE = True
            SAVE_BUTTON.setEnabled(True)
            utils_functions.restoreCursor()

    def setTable(self, tableName=''):
        if not(tableName in ADMIN_DB.tables()):
            return
        utils_functions.doWaitCursor()
        try:
            self.tableName = tableName
            query = utils_db.query(ADMIN_DB)
            columns = utils_db.getColumns(self.tableName, query=query)
            nbCols = len(columns)
            rows = utils_db.table2List(self.tableName, query=query, columns=columns)
            nbRows = len(rows)

            self.headers = []
            self.datas = []
            self.emptyRow = []
            correspondances = {
                'NULL': utils.NUMBER, 'INTEGER': utils.NUMBER, 
                'REAL': utils.NUMBER, 'NUMERIC': utils.NUMBER,
                'TEXT': utils.TEXT, 'BLOB': utils.TEXT}
            for column in columns:
                self.headers.append(column[1])
                dataType = correspondances.get(column[2], utils.INDETERMINATE)
                self.emptyRow.append(['', dataType])
            for row in rows:
                newRow = []
                index = 0
                for data in row:
                    columType = columns[index][2]
                    if columType == 'NUMBER':
                        dataType = utils.NUMBER
                    elif columType == 'TEXT':
                        dataType = utils.TEXT
                    else:
                        dataType = utils.INDETERMINATE
                    if utils.PYTHONVERSION >= 30:
                        if (data == '') or (data == None):
                            dataType = utils.LINE
                        elif isinstance(data, (int, float)):
                            dataType = utils.NUMBER
                        else:
                            # on repère s'il y a des multi-lignes dans la colonne :
                            if data.find('\n') > -1:
                                dataType = utils.TEXT
                            else:
                                dataType = utils.LINE
                    else:
                        if (data == '') or (data == None):
                            dataType = utils.LINE
                        elif isinstance(data, (int, long, float)):
                            dataType = utils.NUMBER
                        else:
                            # on repère s'il y a des multi-lignes dans la colonne :
                            if data.find('\n') > -1:
                                dataType = utils.TEXT
                            else:
                                dataType = utils.LINE
                    newRow.append([data, dataType])
                    index += 1
                self.datas.append(newRow)
            self.setColumnCount(nbCols)
            self.setRowCount(nbRows)
            # on récupère les titres des colonnes :
            for col in range(nbCols):
                self.setHeaderData(
                    col, QtCore.Qt.Horizontal, utils_functions.u(self.headers[col]))
        finally:
            utils_functions.restoreCursor()

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column, row = index.column(), index.row()
        what = self.datas[row][column][1]
        if role == QtCore.Qt.TextAlignmentRole:
            # alignement :
            if what in utils.TYPES_ALIGN_LEFT:
                return QtCore.Qt.AlignLeft
            else:
                return QtCore.Qt.AlignRight
        elif role == QtCore.Qt.DisplayRole:
            # affichage de la case :
            data = self.datas[row][column][0]
            if (utils.PYTHONVERSION >= 30) and (utils.PYQT == 'PYSIDE'):
                if what == utils.NUMBER:
                    return utils_functions.u(data)
            return data
        else:
            return

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.datas)

    def flags(self, index):
        defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        editableFlag = defaultFlag | QtCore.Qt.ItemIsEditable
        if self.editable:
            return editableFlag
        else:
            return defaultFlag


class AdminDBSqlModel(QtGui.QStandardItemModel):
    """
    AdminDBSqlModel est un model pour faire des requêtes sur une base sqlite.
    variable utiles :
        * headers : les titres des colonnes
        * datas : les données [valeur, type]
    """
    def __init__(self, parent=None):
        super(AdminDBSqlModel, self).__init__(parent)
        self.main = parent
        self.setupModelData()

    def setupModelData(self, headers=[], datas=[]):
        self.headers = headers
        self.datas = datas
        nbCols = len(self.headers)
        nbRows = len(self.datas)
        self.setColumnCount(nbCols)
        self.setRowCount(nbRows)
        # on récupère les titres des colonnes :
        for col in range(nbCols):
            self.setHeaderData(
                col, QtCore.Qt.Horizontal, utils_functions.u(self.headers[col]))

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column, row = index.column(), index.row()
        what = self.datas[row][column][1]
        if role == QtCore.Qt.TextAlignmentRole:
            return QtCore.Qt.AlignLeft
        elif role == QtCore.Qt.DisplayRole:
            data = self.datas[row][column][0]
            return data
        else:
            return

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.headers)

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.datas)

    def flags(self, index):
        defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        return defaultFlag


class AdminDBTableView(QtWidgets.QTableView):
    """
    Pour afficher une table ou le résultat d'une requête.
    C'est surtout selectionChanged dont on a besoin.
    """
    def __init__(self, parent=None):
        super(AdminDBTableView, self).__init__(parent)
        self.parent = parent

    def selectionChanged(self, selected, deselected):
        self.parent.updateActions(selected)
        QtWidgets.QTableView.selectionChanged(self, selected, deselected)


class DBFileEditorDlg(QtWidgets.QDialog):
    """
    Un dialog pour embarquer tout ça
    """
    def __init__(self, parent=None):
        super(DBFileEditorDlg, self).__init__(parent)
        global SAVE_BUTTON
        utils_functions.doWaitCursor()
        try:
            self.main = parent
            # le titre de la fenêtre :
            title = QtWidgets.QApplication.translate('main', 'DBFileEditor')
            self.setWindowTitle(title)

            # on passe le nom du fichier au model :
            self.dbFileName = ''
            self.model = None

            # un QTableView et son model pour l'affichage :
            self.dbView = AdminDBTableView(self)
            self.dbView.setModel(self.model)

            # action openDB :
            text = QtWidgets.QApplication.translate('main', 'Open DB')
            statusTip = QtWidgets.QApplication.translate('main', 'OpenDBStatusTip')
            icon = utils.doIcon('database-open')
            self.openDBAction = QtWidgets.QAction(text, self, icon=icon, statusTip=statusTip)
            # action showTables :
            text = QtWidgets.QApplication.translate('main', 'ShowTables')
            statusTip = QtWidgets.QApplication.translate('main', 'ShowTablesStatusTip')
            icon = utils.doIcon('tableaux')
            self.showTablesAction = QtWidgets.QAction(
                text, self, icon=icon, statusTip=statusTip, checkable=True, checked=True)
            # action ShowSql :
            text = QtWidgets.QApplication.translate('main', 'ShowSql')
            statusTip = QtWidgets.QApplication.translate('main', 'ShowSqlStatusTip')
            icon = utils.doIcon('sql')
            self.showSqlAction = QtWidgets.QAction(
                text, self, icon=icon, statusTip=statusTip, checkable=True, checked=False)
            # label et combobox :
            self.dbLabel = QtWidgets.QLabel()
            self.tablesComboBox = QtWidgets.QComboBox()
            self.tablesComboBox.setMinimumWidth(200)
            grid = QtWidgets.QGridLayout()
            grid.addWidget(self.dbLabel, 0, 0)
            grid.addWidget(self.tablesComboBox, 1, 0)
            self.dbGroupBox = QtWidgets.QGroupBox()
            self.dbGroupBox.setLayout(grid)
            self.dbGroupBox.setFlat(True)
            # action insertRow :
            text = QtWidgets.QApplication.translate('main', 'Insert a row')
            statusTip = QtWidgets.QApplication.translate('main', 'Insert a new row into the table')
            icon = utils.doIcon('list-add')
            self.insertRowAction = QtWidgets.QAction(text, self, icon=icon, statusTip=statusTip)
            # action removeRow :
            text = QtWidgets.QApplication.translate('main', 'Remove a row')
            statusTip = QtWidgets.QApplication.translate('main', 'Delete the selected row')
            icon = utils.doIcon('list-remove')
            self.removeRowAction = QtWidgets.QAction(text, self, icon=icon, statusTip=statusTip)
            # on les met dans un toolBar :
            toolBar = QtWidgets.QToolBar()
            toolBar.setIconSize(
                QtCore.QSize(
                    utils.STYLE['PM_LargeIconSize'], 
                    utils.STYLE['PM_LargeIconSize']))
            toolBar.addAction(self.openDBAction)
            toolBar.addSeparator()
            toolBar.addAction(self.showTablesAction)
            toolBar.addAction(self.showSqlAction)
            toolBar.addSeparator()
            toolBar.addWidget(self.dbGroupBox)
            toolBar.addAction(self.insertRowAction)
            toolBar.addAction(self.removeRowAction)

            # pour saisir des requêtes :
            self.sqlEdit = QtWidgets.QTextEdit()
            self.sqlEdit.setMaximumHeight(100)
            self.sqlButton = QtWidgets.QPushButton(
                QtWidgets.QApplication.translate('main', 'Execute query'))
            self.sqlButton.setMaximumWidth(150)
            self.sqlLabel = QtWidgets.QLabel()
            self.sqlModel = None
            # on agence tout ça :
            hLayout = QtWidgets.QHBoxLayout()
            hLayout.addWidget(self.sqlButton)
            hLayout.addWidget(self.sqlLabel)
            vLayout = QtWidgets.QVBoxLayout()
            vLayout.addWidget(self.sqlEdit)
            vLayout.addLayout(hLayout)
            vLayout.addWidget(self.dbView)

            # boîte de bouton et statusBar :
            dialogButtons = utils_functions.createDialogButtons(
                self, buttons=('close', 'apply', 'help'))
            buttonBox = dialogButtons[0]
            self.applyButton = dialogButtons[1]['apply']
            applyText = QtWidgets.QApplication.translate('main', 'Save')
            self.applyButton.setText(applyText)
            self.applyButton.setEnabled(False)
            SAVE_BUTTON = self.applyButton
            self.statusBar = QtWidgets.QStatusBar()

            # on place tout dans une grille :
            grid = QtWidgets.QGridLayout()
            grid.addWidget(toolBar,         0, 0)
            grid.addLayout(vLayout,     1, 0)
            grid.addWidget(buttonBox,       5, 0, 1, 2)
            grid.addWidget(self.statusBar,  6, 0)
            self.setLayout(grid)

            self.showTablesOrSql()
            self.insertRowAction.setEnabled(False)
            self.removeRowAction.setEnabled(False)
            self.openDBAction.triggered.connect(self.openDBFile)
            self.showTablesAction.triggered.connect(self.showTablesOrSql)
            self.showSqlAction.triggered.connect(self.showTablesOrSql)
            self.tablesComboBox.activated.connect(self.tablesChanged)
            self.insertRowAction.triggered.connect(self.insertRow)
            self.removeRowAction.triggered.connect(self.removeRow)
            self.sqlButton.clicked.connect(self.doSqlRequest)
            self.applyButton.clicked.connect(self.saveDBFile)
        finally:
            utils_functions.restoreCursor()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('db-file-editor', 'admin')

    def openDBFile(self):
        """
        blablabla
        """
        global DB_FILE_DIR, ADMIN_DB
        initAdminDB(self.main)
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, QtWidgets.QApplication.translate('main', 'Open Database File'), 
            DB_FILE_DIR, 
            QtWidgets.QApplication.translate('main', 'Database files (*.sqlite)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        try:
            if self.closeDBFile() == False:
                return
            self.dbFileName = fileName
            utils_filesdirs.removeAndCopy(fileName, DB_FILE_TEMP)
            ADMIN_DB = utils_db.createConnection(self.main, DB_FILE_TEMP)[0]
            # affichage du nom de la base :
            dbFileName = QtCore.QFileInfo(fileName).fileName()
            message = utils_functions.u('<p align="center"><b>{0}</b></p>').format(dbFileName)
            self.dbLabel.setText(message)
            # affichage des tables:
            self.tablesInit()
            DB_FILE_DIR = QtCore.QFileInfo(fileName).absolutePath()
        except:
            message = QtWidgets.QApplication.translate(
                'main', 'Cannot read file {0}.').format(fileName)
            utils_functions.messageBox(self.main, level='warning', message=message)

    def closeDBFile(self):
        global ADMIN_DB, MUST_SAVE
        if ADMIN_DB == None:
            return True
        if MUST_SAVE:
            reponse = self.messageMustSave()
            if reponse == QtWidgets.QMessageBox.Cancel:
                return False
            elif reponse == QtWidgets.QMessageBox.Save:
                self.saveDBFile()
            else:
                MUST_SAVE = False
        self.dbView.setModel(None)
        del self.model
        self.tablesComboBox.clear()
        ADMIN_DB.close()
        del ADMIN_DB
        ADMIN_DB = None
        utils_db.sqlDatabase.removeDatabase('temp_db')
        self.model = QtSql.QSqlTableModel()
        self.dbLabel.setText('')
        return True

    def messageMustSave(self):
        m1 = QtWidgets.QApplication.translate('main', 'The DB {0} has been modified.')
        fileName = QtCore.QFileInfo(self.dbFileName).fileName()
        m3 = '<b>{0}</b>'.format(fileName)
        m1 = m1.format(m3)
        m2 = QtWidgets.QApplication.translate('main', 'Do you want to save your changes?')
        message = utils_functions.u('<p>{0}</p><p>{1}</p>').format(m1, m2)
        reponse = utils_functions.messageBox(
            self.main, level='warning', message=message,
            buttons=['Save', 'Discard', 'Cancel'])
        return reponse

    def saveDBFile(self):
        global MUST_SAVE
        utils_filesdirs.removeAndCopy(DB_FILE_TEMP, self.dbFileName)
        self.applyButton.setEnabled(False)
        MUST_SAVE = False

    def tablesInit(self):
        dbTables = ADMIN_DB.tables()[:]
        dbTables.sort()
        self.tablesComboBox.clear()
        for t in dbTables:
            self.tablesComboBox.addItem(t)
        if self.dbFileName in NO_EDITABLES:
            message = QtWidgets.QApplication.translate(
                'main', 'This dataBase is not editable.')
            utils_functions.messageBox(self.main, message=message)
            self.model = AdminDBTableModel(parent=self.main, editable=False)
            self.insertRowAction.setEnabled(False)
            self.removeRowAction.setEnabled(False)
        else:
            self.model = AdminDBTableModel(parent=self.main)
            # un delegate pour l'édition :
            self.dbView.setItemDelegate(AdminDBItemDelegate(self.main, self.model))
            self.insertRowAction.setEnabled(True)
            self.removeRowAction.setEnabled(False)
        self.applyButton.setEnabled(False)
        self.sqlModel = AdminDBSqlModel(parent=self.main)
        self.dbView.setModel(self.model)
        self.tablesChanged()

    def tablesChanged(self):
        tableName = self.tablesComboBox.currentText()
        self.model.setTable(tableName)
        self.dbView.setAlternatingRowColors(True)
        self.dbView.resizeColumnsToContents()
        self.dbView.resizeRowsToContents()
        for i in range(self.model.columnCount()):
            if self.dbView.columnWidth(i) > 300:
                self.dbView.setColumnWidth(i, 300)

    def afterRequest(self):
        global MUST_SAVE
        MUST_SAVE = True
        self.applyButton.setEnabled(True)
        oldTableName = self.tablesComboBox.currentText()
        dbTables = ADMIN_DB.tables()[:]
        dbTables.sort()
        self.tablesComboBox.clear()
        for t in dbTables:
            self.tablesComboBox.addItem(t)
        if oldTableName in dbTables:
            index = self.tablesComboBox.findText(oldTableName)
            self.tablesComboBox.setCurrentIndex(index)
        tableName = self.tablesComboBox.currentText()
        self.model.setTable(tableName)

    def showTablesOrSql(self, showTables=True):
        self.statusBar.showMessage('')
        if self.sender() == self.showTablesAction:
            showTables = True
            self.dbView.setModel(self.model)
        elif self.sender() == self.showSqlAction:
            showTables = False
            self.dbView.setModel(self.sqlModel)
        else:
            showTables = showTables
        self.showTablesAction.setChecked(showTables)
        self.showSqlAction.setChecked(not(showTables))
        for widget in (self.insertRowAction, self.removeRowAction):
            widget.setVisible(showTables)
        for widget in (self.sqlEdit, self.sqlLabel, self.sqlButton):
            widget.setVisible(not(showTables))

    def doSqlRequest(self):
        utils_functions.doWaitCursor()
        try:
            self.sqlLabel.setText('')
            self.sqlModel.setupModelData()
            query = utils_db.query(ADMIN_DB)
            commandLine = self.sqlEdit.toPlainText()
            # on vire les trucs qui pourraient polluer le début :
            commandLine = commandLine.lstrip(' \t\r\n\0')
            if commandLine[:7].upper() == 'SELECT ':
                query = utils_db.queryExecute(commandLine, query=query)
                if query.lastError().isValid():
                    m1 = QtWidgets.QApplication.translate('main', 'ERROR:')
                    m2 = query.lastError().driverText()
                    m3 = query.lastError().databaseText()
                    message = utils_functions.u('<b>{0}</b> {1}  [{2}]').format(m1, m2, m3)
                    self.sqlLabel.setText(message)
                else:
                    record = query.record()
                    nbCols = record.count()
                    headers = []
                    for i in range(nbCols):
                        headers.append(record.fieldName(i))
                    datas = []
                    while query.next():
                        row = []
                        for i in range(nbCols):
                            value = query.value(i)
                            row.append([value, 'aaa'])
                        datas.append(row)
                    nbRows = len(datas)
                    m1 = QtWidgets.QApplication.translate('main', 'OK:')
                    m2 = QtWidgets.QApplication.translate('main', 'results')
                    message = utils_functions.u('<b>{0}</b> {1} {2}').format(m1, nbRows, m2)
                    self.sqlLabel.setText(message)
                    self.sqlModel.setupModelData(headers, datas)
            else:
                try:
                    if self.model.editable:
                        query = utils_db.queryExecute(commandLine, query=query)
                        if query.lastError().isValid():
                            m1 = QtWidgets.QApplication.translate('main', 'ERROR:')
                            m2 = query.lastError().driverText()
                            m3 = query.lastError().databaseText()
                            message = utils_functions.u('<b>{0}</b> {1}  [{2}]').format(m1, m2, m3)
                            self.sqlLabel.setText(message)
                        else:
                            m1 = QtWidgets.QApplication.translate('main', 'OK')
                            message = utils_functions.u('<b>{0}</b>').format(m1)
                            self.sqlLabel.setText(message)
                        self.afterRequest()
                    else:
                        m1 = QtWidgets.QApplication.translate('main', 'ERROR:')
                        m2 = QtWidgets.QApplication.translate('main', 'this DataBase is not editable.')
                        message = utils_functions.u('<b>{0}</b> {1}').format(m1, m2)
                        self.sqlLabel.setText(message)
                except:
                    m1 = QtWidgets.QApplication.translate('main', 'ERROR')
                    message = utils_functions.u('<b>{0}</b>').format(m1)
                    self.sqlLabel.setText(message)
        finally:
            utils_functions.restoreCursor()

    def updateActions(self, selected):
        try:
            if self.showSqlAction.isChecked():
                self.statusBar.showMessage('')
                return
            hasSelection = len(selected.indexes()) > 0
            row, column = -1, -1
            for item in selected.indexes():
                row = item.row()
                column = item.column()
            if (row < 0) and (column < 0):
                self.statusBar.showMessage('')
                return
            if self.model.editable:
                self.insertRowAction.setEnabled(True)
                self.removeRowAction.setEnabled(hasSelection)
            data = self.model.datas[row][column][0]
            message = QtWidgets.QApplication.translate('main', 'Position : ({0},{1})   Value : {2}')
            self.statusBar.showMessage(utils_functions.u(message).format(row, column, data))
        except:
            pass

    def insertRow(self):
        index = self.dbView.currentIndex()
        if index.isValid():
            row = index.row()
            column = index.column()
        else:
            row, column = 0, 0
        if self.model.insertRow(row):
            row += 1
            self.model.datas.insert(row, self.model.emptyRow)
        self.model.saveChanges()
        index = self.model.index(row, column)
        self.dbView.setFocus()
        self.dbView.setCurrentIndex(index)

    def removeRow(self):
        index = self.dbView.currentIndex()
        if not index.isValid():
            return
        row = index.row()
        column = index.column()
        if (self.model.removeRow(row)):
            self.model.datas.pop(row)
            if row > self.model.rowCount() - 1:
                row -= 1
        self.model.saveChanges()
        index = self.model.index(row, column)
        self.dbView.setFocus()
        self.dbView.setCurrentIndex(index)


    def accept(self):
        if self.closeDBFile() == False:
            return
        QtWidgets.QDialog.accept(self)








