#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
   Un planificateur de tâches pour l'admin.
   Pour faire des récups régulières 
   (toutes les nuits par exemple) sans lancer VÉRAC.
   Il peut gérer plusieurs version de VÉRAC.
   Afin d'éviter les fuites mémoires dans le QSqlDatabase 
   (ouverture et fermeture de tables),
   on lance les récup via un programme auxiliaire (admin_recup.pyw).
"""

# importation des modules utiles :
from __future__ import division
import sys
import os
 
# récupération du chemin :
HERE = os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), '..'))
# ajout du chemin au path (+ libs) :
sys.path.insert(0, HERE)
sys.path.insert(0, HERE + os.sep + 'libs')
# on démarre dans le bon dossier :
os.chdir(HERE)

# importation des modules perso :
import utils, utils_functions, utils_db, utils_filesdirs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



# ******************************************************************
# VARIABLES GLOBALES
# ******************************************************************
# liste de tous les établissements dont on est admin
# chaque élément est une liste 
# [versionName, versionUrl, versionLabel, adminDir, typeSchedule]
VERSIONS = []
# l'heure de la première récup si on a sélectionné :
#       * toutes les 6 h
#       * toutes les 12 h
#       * tous les jours
INITIALTIME = 0
# liste de tous les établissements qu'on récupère
# chaque élément est une liste 
# [versionName, versionUrl, versionLabel, adminDir, typeSchedule, horaires]
# où horaires est aussi une liste ; par exemple :
# [22, 4, 10, 16] si on a choisi toutes les 6 heures, et 22 h comme INITIALTIME
RECUPS = []
# niveau de récupération
RECUP_LEVEL = 'RECUP_LEVEL_REFERENTIAL'



# ******************************************************************
# DIALOG DE CONFIGURATION
# ******************************************************************
class ConfigDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue pour configurer les planifications
    et l'horaire initial
    """
    def __init__(self, parent=None):
        """
        mise en place de l'interface
        """
        super(ConfigDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(
            QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'ConfigDlg'))

        # ************************************
        # La liste des établissements gérés, 
        # et les types de schedules associés
        # ************************************
        # les types de schedules possibles :
        self.typesSchedules = [
            QtWidgets.QApplication.translate('main', 'Never'),
            QtWidgets.QApplication.translate('main', 'All times'),
            QtWidgets.QApplication.translate('main', 'Every 6 hours'),
            QtWidgets.QApplication.translate('main', 'Every 12 hours'),
            QtWidgets.QApplication.translate('main', 'Daily')]
        # pour indiquer les détails de la version :
        labelToolTip = utils_functions.u(
            'versionName : {0}\nversionUrl : {1}'
            '\nversionLabel : {2}\nadminDir : {3}')
        # des listes pour avoir accès aux choix (menus, boutons, ...) :
        self.typeScheduleMenusList = []
        self.pushButtonsList = []
        self.schedulesList = []
        # on utilise la liste newVersions pour ne pas modifier 
        # VERSIONS avant confirmation :
        self.newVersions = []
        # on va numéroter les versions :
        numVersion = 0
        for version in VERSIONS:
            # on ajoute la version à la liste newVersions :
            self.newVersions.append(version[:])
            # un label pour afficher le nom de l'établissement :
            versionLabel = version[2]
            label = QtWidgets.QLabel('   ' + versionLabel)
            label.setToolTip(labelToolTip.format(
                version[0], version[1], versionLabel, version[3]))
            # un menu avec les schedules possibles :
            typeScheduleMenu = QtWidgets.QMenu()
            numTypeSchedule = 0
            for typeSchedule in self.typesSchedules:
                newAct = QtWidgets.QAction(typeSchedule, self)
                # on inscrit numVersion et numTypeSchedule 
                # en data pour les retrouver :
                newAct.setData(numVersion * 1000 + numTypeSchedule)
                newAct.triggered.connect(self.typeScheduleChanged)
                typeScheduleMenu.addAction(newAct)
                numTypeSchedule += 1
            self.typeScheduleMenusList.append(typeScheduleMenu)
            # un pushButton pour changer de type :
            versionTypeSchedule = version[4]
            pushButton = QtWidgets.QPushButton(
                self.typesSchedules[versionTypeSchedule])
            pushButton.setToolTip(
                QtWidgets.QApplication.translate(
                    'main', 
                    'ChooseTypeSchedule'))
            pushButton.setMenu(typeScheduleMenu)
            self.pushButtonsList.append(pushButton)
            # on place le label et le bouton dans un Layout horizontal :
            versionLayout = QtWidgets.QHBoxLayout()
            versionLayout.addWidget(label)
            versionLayout.addWidget(pushButton)
            self.schedulesList.append(versionLayout)
            numVersion += 1
        # et on met tout ça dans un GroupBox :
        listeLayout = QtWidgets.QVBoxLayout()
        for versionLayout in self.schedulesList:
            listeLayout.addLayout(versionLayout)
        listeGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Schools'))
        listeGroupBox.setLayout(listeLayout)

        # ************************************
        # Un comboBox pour choisir 
        # l'heure initiale de récup :
        # ************************************
        self.initialTimeComboBox = QtWidgets.QComboBox()
        for h in range(24):
            self.initialTimeComboBox.addItem('{0} h'.format(h))
        self.initialTimeComboBox.setCurrentIndex(INITIALTIME)
        initialTimeLayout = QtWidgets.QHBoxLayout()
        initialTimeLayout.addWidget(self.initialTimeComboBox)
        initialTimeGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Initial time'))
        initialTimeGroupBox.setLayout(initialTimeLayout)

        # ************************************
        # Un checkBox pour choisir 
        # le niveau de récup :
        # ************************************
        message = QtWidgets.QApplication.translate(
            'main', 'Complete calculations every time')
        self.levelCheckBox = QtWidgets.QCheckBox(message)
        self.levelCheckBox.setChecked(
            RECUP_LEVEL == 'RECUP_LEVEL_REFERENTIAL')
        levelLayout = QtWidgets.QHBoxLayout()
        levelLayout.addWidget(self.levelCheckBox)
        levelGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Recovery level'))
        levelGroupBox.setLayout(levelLayout)

        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(initialTimeGroupBox)
        hLayout.addWidget(levelGroupBox)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listeGroupBox)
        mainLayout.addLayout(hLayout)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('scheduler', 'admin')

    def typeScheduleChanged(self):
        data = self.sender().data()
        numVersion = data // 1000
        newTypeSchedule = data % 1000
        self.pushButtonsList[numVersion].setText(
            self.typesSchedules[newTypeSchedule])
        self.newVersions[numVersion][4] = newTypeSchedule



# ******************************************************************
# DIALOG D'AFFICHAGE DES RAPPORTS
# ******************************************************************

class ReportDlg(QtWidgets.QDialog):
    """
    Affiche les fichiers rapport.txt dans des onglets.
    Évite aussi les problèmes d'encodage avec Windows (!)
    """
    def __init__(
            self, parent=None,
            helpContextPage='report', helpBaseUrl='admin'):
        super(ReportDlg, self).__init__(parent)

        self.main = parent
        self.setSizeGripEnabled(True)
        self.setWindowFlags(
            QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'ReportDlg'))
        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl

        # Zone d'affichage :
        tabWidget = QtWidgets.QTabWidget()
        # un onglet par version :
        for version in VERSIONS:
            versionName = version[0]
            adminDir = version[3]
            reportFileName = utils_functions.u(
                '{0}/rapport.txt').format(adminDir)
            tabWidget.addTab(
                BaseEditorTab(fileName=reportFileName), 
                versionName)
        tabWidget.setMinimumSize(600, 300)

        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('close', 'help'))
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(tabWidget)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(
            self.helpContextPage, self.helpBaseUrl)


class BaseEditorTab(QtWidgets.QWidget):
    def __init__(self, parent=None, fileName=''):
        super(BaseEditorTab, self).__init__(parent)
        self.bigTruc = QtWidgets.QTextEdit()
        self.bigTruc.setReadOnly(True)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addWidget(self.bigTruc)
        self.setLayout(mainLayout)
        inFile = QtCore.QFile(fileName)
        if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(inFile)
            stream.setCodec('UTF-8')
            self.bigTruc.setPlainText(stream.readAll())
            inFile.close()



# ******************************************************************
# INTERFACE
# ******************************************************************

class Window(QtWidgets.QDialog):
    def __init__(self):
        super(Window, self).__init__()

        global VERSIONS, INITIALTIME, RECUP_LEVEL

        # on est admin ; il faut le définir dans un self.me minimal :
        self.me = {
            'userMode': 'admin', 'userName': 'Administrateur', 
            'userId': -1, 'user': '', 'Mdp': '', 'Matiere': '', 
            'other': ''}

        # le dossier des fichiers etc :
        self.beginDir = QtCore.QDir.currentPath()

        # le programme est-il déjà lancé ?
        if utils_filesdirs.testLockFile(self, name='verac_scheduler') == False:
            sys.exit(1)

        # verac_scheduler ne peut être lancé que depuis un poste admin.
        # Donc on utilise db_localConfig qui est dans le home :
        localConfigDir = utils_filesdirs.createConfigAppDir(
            utils.PROGLINK, beginDir=self.beginDir).canonicalPath()
        self.dbFile_localConfig = utils_functions.u(
            '{0}/config.sqlite').format(localConfigDir)

        # récupération du dossier temporaire :
        self.tempPath = utils_filesdirs.createTempAppDir(
            utils.PROGLINK + '_sheduler')
        # on y copie la base config :
        self.dbFile_localConfigTemp = utils_functions.u(
            '{0}/config.sqlite').format(self.tempPath)
        utils_filesdirs.removeAndCopy(
            self.dbFile_localConfig, self.dbFile_localConfigTemp)

        # on ouvre une transaction avec la base config :
        query_config, db_localConfig, dbName, transactionOK = \
            utils_db.queryTransactionFile(
                self, self.dbFile_localConfigTemp)

        # 2 dictionnaires pour récupérer les versions 
        # (dans la base ou existantes) :
        versionsInDB, versionsExists = {}, {}
        # on crée la table config.schedules si besoin :
        commandLine = utils_db.qct_config_schedules
        query_config = utils_db.queryExecute(
            commandLine, query=query_config)
        # on récupère les versions qui sont inscrites 
        # dans la table config.schedules :
        commandLine = utils_db.q_selectAllFrom.format('schedules')
        query_config = utils_db.queryExecute(
            commandLine, query=query_config)
        while query_config.next():
            versionName = query_config.value(0)
            versionUrl = query_config.value(1)
            versionLabel = query_config.value(2)
            adminDir = query_config.value(3)
            typeSchedule = int(query_config.value(4))
            if versionName == 'InitialTime':
                INITIALTIME = typeSchedule
            elif versionName == 'RecupLevel':
                RECUP_LEVEL = versionUrl
            else:
                versionsInDB[versionName] = [
                    [versionUrl, versionLabel, adminDir], 
                    typeSchedule]
        # on récupère les versions qui existent :
        commandLine = (
            'SELECT versions.*, config.value_text '
            'FROM versions '
            'JOIN config ON config.key_name=versions.name '
            'WHERE config.key_name!="" '
            'ORDER BY versions.id_version')
        query_config = utils_db.queryExecute(
            commandLine, query=query_config)
        while query_config.next():
            versionName = query_config.value(1)
            versionUrl = query_config.value(2)
            versionLabel = query_config.value(3)
            adminDir = query_config.value(4)
            versionsExists[versionName] = [
                [versionUrl, versionLabel, adminDir], 
                0]

        # si versionsInDB est vide, c'est qu'on lance pour la première fois
        # il faudra alors lancer la fenêtre de config :
        mustConfig = (versionsInDB == {})
        # si quelque chose a changé, on devra copier la base :
        mustCopy = False
        commandLineUpdate = utils_functions.u(
            'UPDATE schedules SET versionUrl=?, '
            'versionLabel=?, '
            'adminDir=? '
            'WHERE versionName=?')
        for versionName in versionsExists:
            versionUrl = versionsExists[versionName][0][0]
            versionLabel = versionsExists[versionName][0][1]
            adminDir = versionsExists[versionName][0][2]
            if not(versionName in versionsInDB):
                # nouvelle version :
                mustConfig = True
                typeSchedule = 0
            elif versionsInDB[versionName][0] != versionsExists[versionName][0]:
                # des modifications :
                mustCopy = True
                typeSchedule = versionsInDB[versionName][1]
                versionsInDB[versionName][0] = versionsExists[versionName][0]
                query_config = utils_db.queryExecute(
                    {commandLineUpdate: (
                        versionUrl, 
                        versionLabel, 
                        adminDir, 
                        versionName)}, 
                    query=query_config)
            else:
                # pas de modif :
                typeSchedule = versionsInDB[versionName][1]
            VERSIONS.append([
                versionName, 
                versionUrl, 
                versionLabel, 
                adminDir, 
                typeSchedule])

        # on supprime les shedules obsolètes :
        for versionName in versionsInDB:
            if not(versionName in versionsExists):
                mustCopy = True
                commandLine = utils_db.qdf_whereText.format(
                    'schedules', 'versionName', versionName)
                query_config = utils_db.queryExecute(
                    commandLine, query=query_config)

        # on ferme la base :
        utils_db.endTransaction(query_config, db_localConfig, transactionOK)
        db_localConfig.close()
        del db_localConfig
        utils_db.sqlDatabase.removeDatabase(dbName)
        if mustCopy:
            utils_filesdirs.removeAndCopy(
                self.dbFile_localConfigTemp, self.dbFile_localConfig)

        # création de l'interface :
        self.createMessages()
        self.createIcons()
        self.createActions()
        self.createTrayIcon()

        # self.app est l'executable de Python :
        self.app = sys.executable
        if utils.OS_NAME[0] == 'win':
            self.app = self.app.replace('ython.exe', 'ythonw.exe')

        # on a besoin d'un process :
        self.process = QtCore.QProcess(self)
        # et d'un timer :
        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.timerIsOut)
        # le timer se déclenchera toutes les demi-heures :
        self.delay = 30 * 60 * 1000
        self.heureActuelle = -1
        self.lastTest = ''
        self.lastRecup = ''
        self.timer.start(self.delay)
        # actualVersion si un établissement est en cours de récup
        # pour attendre la fin avant de récupérer le suivant
        self.actualVersion = ''

        self.setWindowTitle(self.PROGNAME)
        self.trayIcon.show()
        self.changeEtat()
        self.doubleClic = False

        self.createRecupsList()
        if mustConfig:
            self.doConfig()

    def createMessages(self):
        """
        Traduction des différents messages affichés :
        """
        self.PROGNAME = QtWidgets.QApplication.translate('main', 'VERAC')
        self.lastTestToolTip = QtWidgets.QApplication.translate(
            'main', 'Last test:')
        self.lastRecupToolTip = QtWidgets.QApplication.translate(
            'main', 'Last recup:')
        self.workingToolTip = QtWidgets.QApplication.translate(
            'main', 'Retrieving ...')
        self.veracToolTip = QtWidgets.QApplication.translate(
            'main', 'VERAC is launched')
        self.configToolTip = QtWidgets.QApplication.translate(
            'main', 'Settings')
        self.endMessageRecup = QtWidgets.QApplication.translate(
            'main', 'RecoveryIsComplete')
        self.endMessageVerac = QtWidgets.QApplication.translate(
            'main', 'VeracIsClosed')
        if utils.OS_NAME[0] == 'linux':
            self.baseToolTip = '{0} <b>{1}</b><br/>{2} <b>{3}</b>'
        else:
            self.baseToolTip = '{0} {1}\n{2} {3}'

    def createIcons(self):
        self.appIcon = utils.doIcon('icon', ext='png')
        self.disableIcon = utils.doIcon('icon-nb', ext='png')
        self.recupIcon = utils.doIcon('update-results')
        self.reportIcon = utils.doIcon('view-pim-journal')
        self.gotoIcon = utils.doIcon('net-go')
        self.veracIcon = utils.doIcon('verac-launch', ext='png')
        self.configIcon = utils.doIcon('configure')
        self.aboutIcon = utils.doIcon('help-about')
        self.helpIcon = utils.doIcon('help')
        self.quitIcon = utils.doIcon('application-exit')

    def createActions(self):
        # Lancer une récup
        self.actionRecup = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Recup'), 
            self, 
            icon=self.recupIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'RecupStatusTip'), 
            triggered=self.doRecup)
        # Voir le rapport de la dernière récup
        self.actionLastReport = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'LastReport'), 
            self, 
            icon=self.reportIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'LastReportStatusTip'), 
            triggered=self.doLastReport)
        # Aller sur les sites des établissements :
        self.actionGotoSiteEtab = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'GotoSiteEtab'), 
            self, 
            icon=self.gotoIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'GotoSiteEtabStatusTip'), 
            triggered=self.doGotoSiteEtab)
        # Lancer VÉRAC
        self.actionVerac = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Verac'), 
            self, 
            icon=self.veracIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'VeracStatusTip'), 
            triggered=self.doVerac)
        # Réglages
        self.actionConfig = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Settings'), 
            self, 
            icon=self.configIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'ConfigStatusTip'), 
            triggered=self.doConfig)
        # Aide etc
        self.actionAbout = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&About'), 
            self, 
            icon=self.aboutIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'AboutStatusTip'), 
            triggered=self.about)
        self.actionHelpPage = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'HelpPage'), 
            self, 
            icon=self.helpIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'HelpPageStatusTip'), 
            triggered=self.helpPage)
        self.actionQuit = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'E&xit'), 
            self, 
            icon=self.quitIcon, 
            statusTip=QtWidgets.QApplication.translate(
                'main', 'QuitSchedulerStatusTip'), 
            triggered=self.close)

    def closeEvent(self, event):
        utils_filesdirs.deleteLockFile(name='verac_scheduler')
        # suppression du dossier temporaire :
        utils_filesdirs.emptyDir(self.tempPath)
        tempDir = QtCore.QDir.temp()
        tempDir.rmdir(utils.PROGLINK + '_sheduler')
        # on termine :
        sys.exit(1)

    def createTrayIcon(self):
        self.trayIconMenu = QtWidgets.QMenu(self)

        # Les actions importantes :
        self.trayIconMenu.addAction(self.actionRecup)
        self.trayIconMenu.addAction(self.actionLastReport)
        self.trayIconMenu.addAction(self.actionGotoSiteEtab)
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.actionVerac)
        self.trayIconMenu.addAction(self.actionConfig)
        # Aide etc
        self.trayIconMenu.addSeparator()
        self.trayIconMenu.addAction(self.actionAbout)
        self.trayIconMenu.addAction(self.actionHelpPage)
        self.trayIconMenu.addAction(self.actionQuit)
        # création du TrayIcon :
        self.trayIcon = QtWidgets.QSystemTrayIcon(self)
        self.trayIcon.setContextMenu(self.trayIconMenu)
        self.trayIcon.setIcon(self.appIcon)
        self.trayIcon.activated.connect(self.iconActivated)
        self.trayIconMenu.hovered.connect(self._actionHovered)

    def _actionHovered(self, action):
        """
        Pour afficher les "tooltip" lors du survol des menus
        """
        tip = action.statusTip()
        QtWidgets.QToolTip.showText(QtGui.QCursor.pos(), tip)

    def setEnabledAll(self, value=False):
        """
        activer/désactiver les actions importantes
        """
        self.actionRecup.setEnabled(value)
        self.actionLastReport.setEnabled(value)
        self.actionVerac.setEnabled(value)
        self.actionConfig.setEnabled(value)
        self.actionQuit.setEnabled(value)

    def changeEtat(self, newEtat='nothing'):
        """
        mise à jour de l'affichage lors d'un changement d'état
        """
        try:
            if newEtat == self.etat:
                return
        except:
            pass
        if newEtat == 'recup':
            self.setEnabledAll()
            self.trayIcon.setIcon(self.recupIcon)
            if utils.OS_NAME[0] == 'linux':
                toolTip = '{0}<br/>{1} <b>{2}</b><br/>{3} <b>{4}</b>'
            else:
                toolTip = '{0}\n{1} {2}\n{3} {4}'
            toolTip = utils_functions.u(toolTip).format(
                self.workingToolTip, 
                self.lastTestToolTip, self.lastTest, 
                self.lastRecupToolTip, self.lastRecup)
            self.trayIcon.setToolTip(toolTip)
        elif newEtat == 'verac':
            # désactivé dans doProcess :
            self.setEnabledAll()
            self.timer.stop()
            self.trayIcon.setIcon(self.disableIcon)
            self.trayIcon.setToolTip(self.veracToolTip)
        elif newEtat == 'config':
            self.setEnabledAll()
            self.timer.stop()
            self.trayIcon.setIcon(self.configIcon)
            self.trayIcon.setToolTip(self.configToolTip)
        elif newEtat == 'nothing':
            self.setEnabledAll(True)
            self.timer.start()
            self.trayIcon.setIcon(self.appIcon)
            toolTip = utils_functions.u(self.baseToolTip).format(
                self.lastTestToolTip, self.lastTest, 
                self.lastRecupToolTip, self.lastRecup)
            self.trayIcon.setToolTip(toolTip)
        self.etat = newEtat

    def createRecupsList(self):
        """
        (Re)création de la liste globale RECUPS.
        Elle contient les établissements à gérer
        et pour chacun, la liste des horaires de récupération.
        """
        global RECUPS
        RECUPS = []
        for version in VERSIONS:
            typeSchedule = version[4]
            if typeSchedule > 0:
                recup = version[:]
                horaires = []
                if typeSchedule == 1:
                    for h in range(24):
                        horaires.append(h)
                elif typeSchedule == 2:
                    for h in range(4):
                        horaires.append((INITIALTIME + 6 * h) % 24)
                elif typeSchedule == 3:
                    horaires.append(INITIALTIME)
                    horaires.append((INITIALTIME + 12) % 24)
                elif typeSchedule == 4:
                    horaires.append(INITIALTIME)
                recup.append(horaires)
                RECUPS.append(recup)

    def iconActivated(self, reason):
        """
        pour détecter les clics et double-clics
        """
        if reason == QtWidgets.QSystemTrayIcon.DoubleClick:
            self.doubleClic = True
            if self.etat == 'nothing':
                self.doRecup()
        elif reason == QtWidgets.QSystemTrayIcon.Trigger:
            QtCore.QTimer.singleShot(200, self.clicTimerIsOut)

    def clicTimerIsOut(self):
        if not(self.doubleClic):
            # donc c'est bien un simple clic
            self.trayIcon.contextMenu().popup(QtGui.QCursor.pos())
        self.doubleClic = False

    def timerIsOut(self):
        """
        le timer se déclenche toutes les demi-heures, 
        et lance les récupérations nécéssaires
        """
        if self.etat != 'nothing':
            return
        maintenant = QtCore.QDateTime.currentDateTime()
        self.lastTest = maintenant.toString('yyyy-MM-dd hh:mm')
        heureActuelle = int(maintenant.toString('hh'))
        if heureActuelle != self.heureActuelle:
            self.heureActuelle = heureActuelle
            for version in RECUPS:
                if heureActuelle in version[5]:
                    self.doRecup(version[0])
                    self.lastRecup = maintenant.toString('yyyy-MM-dd hh:mm')
        toolTip = utils_functions.u(self.baseToolTip).format(
            self.lastTestToolTip, self.lastTest, 
            self.lastRecupToolTip, self.lastRecup)
        self.trayIcon.setToolTip(toolTip)

    def doRecup(self, versionName=''):
        """
        Lance une récupération de l'établissement passé en paramètre.
        Si versionName est vide, on récupère tous les établissements gérés.
        """
        maintenant = QtCore.QDateTime.currentDateTime()
        theApp = utils_functions.u(
            '{0}/libs/admin_recup.pyw').format(self.beginDir)
        if versionName in ('', False):
            for version in RECUPS:
                self.actualVersion = version[0]
                self.doProcess([theApp, RECUP_LEVEL, version[0]], 'recup')
                while self.actualVersion != '':
                    QtWidgets.QApplication.processEvents()
        else:
            self.actualVersion = versionName
            self.doProcess([theApp, RECUP_LEVEL, versionName], 'recup')
            while self.actualVersion != '':
                QtWidgets.QApplication.processEvents()
        self.lastRecup = maintenant.toString('yyyy-MM-dd hh:mm')
        toolTip = utils_functions.u(self.baseToolTip).format(
            self.lastTestToolTip, self.lastTest, 
            self.lastRecupToolTip, self.lastRecup)
        self.trayIcon.setToolTip(toolTip)

    def doLastReport(self):
        """
        Affiche les rapports de la dernière récupération
        de chaque établissement géré
        """
        dialog = ReportDlg(parent=self)
        dialog.exec_()

    def doGotoSiteEtab(self):
        """
        Ouvre le site web de chaque établissement géré
        """
        for version in RECUPS:
            utils_functions.openInBrowser(version[1])

    def doVerac(self):
        """
        lance VÉRAC en process (pour suspendre le timer).
        """
        theApp = utils_functions.u('{0}/Verac.pyw').format(self.beginDir)
        #self.doProcess([theApp, 'FROM_SHEDULER'], 'verac')
        self.doProcess([theApp, ], 'verac')

    def doConfig(self):
        """
        Configuration du scheduler :
            liste des établissements
            heure initiale et fréquence des récupérations
        """
        global VERSIONS, INITIALTIME, RECUP_LEVEL
        self.changeEtat('config')

        dialog = ConfigDlg(parent=self)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # on recrée la liste VERSIONS :
            VERSIONS = []
            for version in dialog.newVersions:
                VERSIONS.append(version[:])
            # on récupère INITIALTIME et RECUP_LEVEL :
            INITIALTIME = dialog.initialTimeComboBox.currentIndex()
            if dialog.levelCheckBox.isChecked():
                RECUP_LEVEL = 'RECUP_LEVEL_REFERENTIAL'
            else:
                RECUP_LEVEL = ''
            # on recrée la table config.schedules 
            # après avoir récupéré la base config :
            utils_filesdirs.removeAndCopy(
                self.dbFile_localConfig, self.dbFile_localConfigTemp)
            query_config, db_localConfig, dbName, transactionOK = utils_db.queryTransactionFile(
                self, self.dbFile_localConfigTemp)
            commandLine = utils_db.qdf_table.format('schedules')
            query_config = utils_db.queryExecute(
                commandLine, query=query_config)
            commandLine = utils_db.qct_config_schedules
            query_config = utils_db.queryExecute(
                commandLine, query=query_config)
            commandLine = utils_db.insertInto('schedules', 5)
            # on inscrit d'abord INITIALTIME et RECUP_LEVEL, 
            # puis les établissements :
            listArgs = []
            listArgs.append(('InitialTime', '', '', '', INITIALTIME))
            listArgs.append(('RecupLevel', RECUP_LEVEL, '', '', 0))
            for version in VERSIONS:
                listArgs.append(tuple(version))
            query_config = utils_db.queryExecute(
                {commandLine: listArgs}, query=query_config)
            # on ferme la base :
            utils_db.endTransaction(
                query_config, db_localConfig, transactionOK)
            db_localConfig.close()
            del db_localConfig
            utils_db.sqlDatabase.removeDatabase(dbName)
            # et on copie config.sqlite :
            localConfigDir = utils_filesdirs.createConfigAppDir(
                utils.PROGLINK, beginDir=self.beginDir).canonicalPath()
            utils_filesdirs.removeAndCopy(
                self.dbFile_localConfigTemp, self.dbFile_localConfig)

            # on doit recréer la liste RECUPS :
            self.createRecupsList()

        self.changeEtat()

    def doProcess(self, args, etat):
        """
        lance un process (programme Python)
        """
        self.process.start(self.app, args)
        if not self.process.waitForStarted(3000):
            QtCore.qDebug('BUG IN verac_scheduler process')
            return False
        if etat == 'verac':
            self.changeEtat()
        else:
            self.changeEtat(etat)
            self.process.finished.connect(self.processEnded)
        return True

    def processEnded(self, exitCode):
        """
        fin du process
        """
        if self.etat == 'recup':
            message = utils_functions.u('{0} :\n {1}').format(
                self.endMessageRecup, self.actualVersion)
            icon = QtWidgets.QSystemTrayIcon.MessageIcon(
                QtWidgets.QSystemTrayIcon.Information)
            self.trayIcon.showMessage(self.PROGNAME, message, icon, 2000)
            self.actionLastReport.setEnabled(True)
            self.actualVersion = ''
        self.changeEtat()

    def about(self):
        """
        Affiche la fenetre AboutDlg
        """
        from libs import utils_about
        aboutdialog = utils_about.AboutDlg(self, utils.LOCALE)
        aboutdialog.exec_()

    def helpPage(self):
        """
        ouvre la page d'aide du scheduler
        """
        utils_functions.openContextHelp('scheduler', 'admin')



# ******************************************************************
# LANCEMENT DU PROGRAMME
# ******************************************************************

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    utils.loadStyle()

    if not QtWidgets.QSystemTrayIcon.isSystemTrayAvailable():
        QtWidgets.QMessageBox.critical(
            None, 
            'Systray', 
            "I couldn't detect any system tray on this system.")
        sys.exit(1)

    ###########################################
    # Installation de l'internationalisation:
    ###########################################
    locale = QtCore.QLocale.system().name()
    qtTranslationsPath = QtCore.QLibraryInfo.location(
        QtCore.QLibraryInfo.TranslationsPath)
    qtTranslator = QtCore.QTranslator()
    if qtTranslator.load('qtbase_' + locale, qtTranslationsPath):
        app.installTranslator(qtTranslator)
    elif qtTranslator.load('qt_' + locale, qtTranslationsPath):
        app.installTranslator(qtTranslator)
    trans_dir = QtCore.QDir('./translations')
    localefile = trans_dir.filePath(utils.PROGLINK + '_' + locale)
    appTranslator = QtCore.QTranslator()
    if appTranslator.load(localefile, ""):
        app.installTranslator(appTranslator)
    utils.changeLocale(locale)

    QtWidgets.QApplication.setQuitOnLastWindowClosed(False)

    window = Window()
    try:
        sys.exit(app.exec_())
    finally:
        utils_filesdirs.deleteLockFile(name='verac_scheduler')


