# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
Ce module contient les variables et constantes utiles au programme.
    * les constantes en majuscules sont utilisées par d'autres modules
    * les constantes en minuscules sont internes au fichier utils
    * les fonctions "change..." permettent de modifier certaines variables globales
    * le suffixe _initial sert à réinitialiser certaines variables via l'interface
"""

# importation des modules utiles :
from __future__ import division, print_function
import sys
import os



"""
****************************************************
    VERSIONS DE PYTHON, QT, ETC
****************************************************
"""

# version de Python :
PYTHONVERSION = sys.version_info[0] * 10 + sys.version_info[1]

# PyQt5 ou PyQt4 :
PYQT = ''
if 'PYQT4' in sys.argv:
    try:
        import sip
        sip.setapi('QString', 2)
        sip.setapi('QVariant', 2)
        from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
        PYQT = 'PYQT4'
    except:
        pass
else:
    try:
        # on teste d'abord PyQt5 :
        from PyQt5 import QtCore, QtWidgets, QtGui
        PYQT = 'PYQT5'
    except:
        # puis PyQt4 :
        try:
            import sip
            sip.setapi('QString', 2)
            sip.setapi('QVariant', 2)
            from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
            PYQT = 'PYQT4'
        except:
            pass
if PYQT == '':
    print('YOU MUST INSTALL PYQT !')

# version de Qt :
qtVersion = QtCore.qVersion()

# détection du système (nom et 32 ou 64) :
OS_NAME = ['', '']
def detectPlatform():
    global OS_NAME
    # 32 ou 64 bits :
    if sys.maxsize > 2**32:
        bits = 64
    else:
        bits = 32
    # platform et osName :
    platform = sys.platform
    osName = ''
    if platform.startswith('linux'):
        osName = 'linux'
    elif platform.startswith('win'):
        osName = 'win'
    elif platform.startswith('freebsd'):
        osName = 'freebsd'
    elif platform.startswith('darwin'):
        import platform
        if 'powerpc' in platform.uname():
            osName = 'powerpc'
        else:
            osName = 'mac'
    OS_NAME = [osName, bits]
detectPlatform()


HERE = ''
def changeHere(newValue):
    global HERE
    HERE = newValue



"""
****************************************************
    LES ARGUMENTS PRIS EN COMPTE :
        * MODEBAVARD, ICI, NOGUI
        * PYQT, NOLOG sont gérés à part
****************************************************
"""

# l'argument MODEBAVARD affichera plus de messages :
MODEBAVARD = False
if 'MODEBAVARD' in sys.argv:
    MODEBAVARD = True
print('MODEBAVARD: ', MODEBAVARD)

def changeModeBavard(newValue):
    global MODEBAVARD
    MODEBAVARD = newValue

# l'argument ICI rien que pour du déboggage :
ICI = False
if 'ICI' in sys.argv:
    ICI = True
    print('ICI')

# l'argument DOC pour créer la documentation :
DOC = False
if 'DOC' in sys.argv:
    DOC = True
    print('DOC')

# l'argument NOGUI pour lancer sur un serveur, sans interface graphique
NOGUI = False
if 'NOGUI' in sys.argv:
    NOGUI = True

def changeNoGui(newValue):
    global NOGUI
    NOGUI = newValue



"""
****************************************************
    GESTION DU FICHIER DE LOG
****************************************************
"""

# les variables liées au fichier de log :
SHOWLOG = False
LOGFILENAME = ''
LOGFILE = None
USELOGFILE = False
saveout = None
saveerr = None

def changeShowLog(newValue):
    global SHOWLOG
    SHOWLOG = newValue

def getLogFileName(mustCreate=True):
    global LOGFILENAME
    # on récupère le chemin de config :
    if OS_NAME[0] == 'win':
        appDataDir = os.environ['APPDATA']
        configDirName = "." + PROGLINK
        configDir = os.path.join(appDataDir, configDirName)
    else:
        homedir = os.path.expanduser('~')
        homeConfigDir = os.path.join(homedir, '.config')
        if os.path.exists(homeConfigDir):
            configDirName = PROGLINK
            configDir = os.path.join(homeConfigDir, configDirName)
            configDirEX = os.path.join(homedir, "." + PROGLINK)
            if os.path.exists(configDirEX):
                os.makedirs(configDir)
        else:
            configDirName = "." + PROGLINK
            configDir = os.path.join(homedir, configDirName)
    # newConfigDir indiquera si on a dû créer le dossier de config
    # (donc si le programme est lancé pour la première fois).
    if os.path.exists(configDir):
        newConfigDir = False
    else:
        newConfigDir = True
        os.makedirs(configDir)
    # on crèe le nom du fichier log :
    LOGFILENAME = os.path.join(configDir, "verac.log")
    if mustCreate:
        if os.path.exists(LOGFILENAME):
            log = open(LOGFILENAME, 'w')
            log.close()
    return newConfigDir, LOGFILENAME

def changeLogFile(useLogFile=True):
    print('changeLogFile :', useLogFile)
    global saveout, saveerr, LOGFILE, LOGFILENAME, USELOGFILE
    if useLogFile == USELOGFILE:
        return
    if useLogFile:
        saveout = sys.stdout
        saveerr = sys.stderr
        label = "LOGFILENAME : "
        try:
            LOGFILE = open(LOGFILENAME, 'a')
        except:
            label = "BEURK LOGFILENAME : "
            if OS_NAME[0] == 'win':
                LOGFILENAME = os.environ['HOMEDRIVE'] + os.sep + "verac.log"
            else:
                LOGFILENAME = "verac.log"
            LOGFILE = open(LOGFILENAME, 'a')
        sys.stdout = LOGFILE
        sys.stderr = LOGFILE
        print('changeLogFile : ', useLogFile)
    else :
        print('changeLogFile : ', useLogFile)
        sys.stdout = saveout
        sys.stderr = saveerr
        LOGFILE.close()
    USELOGFILE = useLogFile
    print('USELOGFILE : ', USELOGFILE)

def saveLogFile():
    if 'NOLOG' in sys.argv:
        return
    print('saveLogFile : USELOGFILE :', USELOGFILE)
    if USELOGFILE:
        #changeLogFile(False)
        LOGFILE.flush()



"""
****************************************************
    VARIABLES LIÉES AU LOGICIEL
****************************************************
"""

PROGNAME = 'VERAC' # redéfinit plus loin dans initTranslations
PROGNAMEHTML = 'V&Eacute;RAC'
PROGLINK = 'Verac'
PROGLABEL = 'Vers une Évaluation Réussie Avec les Compétences'
PROGLABELHTML = '<b>V</b>ers une <b>&Eacute;</b>valuation <b>R</b>&eacute;ussie <b>A</b>vec les <b>C</b>omp&eacute;tences'
PROGVERSION = '1.0'
PROGYEAR = '2009-2022'
PROGMAIL = 'verac at tuxfamily.org'
PROGMAIL2 = 'verac@tuxfamily.org'
LICENCETITLE = 'GNU General Public License version 3'

PROGWEB = 'https://verac.tuxfamily.org'
HELPPAGE_BEGIN = 'https://verac.tuxfamily.org/site/'
UPDATEURL = 'https://download.tuxfamily.org/verac/downloads/'

# redéfinit plus loin dans initTranslations :
DATABASE_CONNECTION_FAIL_MESSAGE = (
    'Unable to establish a database connection.'
    '\n({0})'
    '\n\nClick Cancel to exit.')

def changeProgVersion(main):
    """
    ajoute la date de la version au bout de la variable PROGVERSION
    Pour affichage dans la fenêtre About
    """
    global PROGVERSION
    dateVersion = '0'
    datesVersionsFile = QtCore.QFile(main.beginDir + '/files/dates_versions.html')
    if datesVersionsFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        try:
            stream = QtCore.QTextStream(datesVersionsFile)
            stream.setCodec('UTF-8')
            dates_versions = stream.readAll().split('|')
            dateVersion = dates_versions[0]
        except:
            pass
        finally:
            datesVersionsFile.close()
    jour = QtCore.QDateTime().fromString(dateVersion, 'yyyyMMddhhmm').toString('___dd-MM-yyyy')
    PROGVERSION = PROGVERSION + jour



"""
****************************************************
    VERSIONS DES BASES DE DONNÉES
    (UTILES POUR LES UPGRADES)
****************************************************
"""

#VERSIONDB_CONFIG = 1           # 2011-??-??
VERSIONDB_CONFIG = 2            # 2015-07-04
#VERSIONDB_PROF = 7             # 2011-12-10    ajout de la table templates
#VERSIONDB_PROF = 8             # 2012-06-09    ajout des tables notes et notes_values
#VERSIONDB_PROF = 9             # 2012-10-06    ajout de la colonne groupes.ordre
#VERSIONDB_PROF = 10            # 2012-10-26    ajout de la table absences
#VERSIONDB_PROF = 11            # 2012-11-09    ajout de la table counts et counts_values
#VERSIONDB_PROF = 12            # 2012-11-09    ajout de la colonne counts.calcul
#VERSIONDB_PROF = 13            # 2013-10-02    ajout de la colonne Label (templates, tableaux, counts)
#VERSIONDB_PROF = 14            # 2014-01-22    ajout de la colonne counts_values.perso
#VERSIONDB_PROF = 15            # 2014-12-13    table moyennes renommée en moyennesItems
#VERSIONDB_PROF = 16            # 2015-03-29    ajout de la colonne counts.id_item
#VERSIONDB_PROF = 17            # 2015-05-18    réorganisation des profils
#VERSIONDB_PROF = 18            # 2016-07-19    table absences modifiée (LSU)
VERSIONDB_PROF = 19             # 2016-10-28    table lsu
#VERSIONDB_COMMUN = 4           # 2012-04-10    suivi et horaires
#VERSIONDB_COMMUN = 5           # 2012-06-06    colonne classes.notes
#VERSIONDB_COMMUN = 6           # 2013-07-31    colonnes bulletin.ordre, etc
#VERSIONDB_COMMUN = 7           # 2014-12-13    colonnes classes.ordre
#VERSIONDB_COMMUN = 8           # 2016-10-25    table lsu
#VERSIONDB_COMMUN = 9           # 2017-03-26    matière parcours (LSU)
VERSIONDB_COMMUN = 10           # 2018-06-30    nouvelle table bilans_liens (remplace equivalences)
#VERSIONDB_USERS = 4            # 2011-04-17
VERSIONDB_USERS = 5             # 2017-09-21    table eleves : ajout des colonnes sexe et date_naiss
#VERSIONDB_ADMIN = 4            # 2011-07-03
#VERSIONDB_ADMIN = 5            # 2013-02-15    ajout eleves.AnDernier
#VERSIONDB_ADMIN = 6            # 2015-10-27    MatiereCode
#VERSIONDB_ADMIN = 7            # 2016-08-25    table lsu
#VERSIONDB_ADMIN = 8            # 2017-03-19    table eleves : ajout de 3 colonnes (pour lsu)
#VERSIONDB_ADMIN = 9            # 2017-09-21    table eleves : ajout d'une colonne (sexe)
VERSIONDB_ADMIN = 10            # 2018-11-28    table config_calculs : ProtectedPeriod_ & LockedClasses
#VERSIONDB_RECUPEVALS = 3       # 2012-03-20    table eleve_groupe
#VERSIONDB_RECUPEVALS = 4       # 2012-06-20    table notes
#VERSIONDB_RECUPEVALS = 5       # 2012-10-27    table absences
#VERSIONDB_RECUPEVALS = 6       # 2013-04-17    table dnb_values
#VERSIONDB_RECUPEVALS = 7       # 2015-06-29    suppression de la table validations
#VERSIONDB_RECUPEVALS = 8       # 2015-06-29    tables absences et dnb_values modifiées
#VERSIONDB_RECUPEVALS = 9       # 2016-07-19    table absences modifiée (LSU)
VERSIONDB_RECUPEVALS = 10       # 2016-10-29    table lsu
#VERSIONDB_RESULTATS = 3        # 2011-08-14
#VERSIONDB_RESULTATS = 4        # 2013-04-06    table eleve_login
#VERSIONDB_RESULTATS = 5        # 2013-11-14    colonne eleve_login.initialMdp
#VERSIONDB_RESULTATS = 6        # 2013-12-18    table bilans_details
#VERSIONDB_RESULTATS = 7        # 2014-01-03    eleve_login fusionné avec eleves
#VERSIONDB_RESULTATS = 8        # 2016-07-19    table absences modifiée (LSU)
VERSIONDB_RESULTATS = 9         # 2017-02-18    table lsu
#VERSIONDB_PROPOSITIONS = 1     # 2011-06-10
VERSIONDB_PROPOSITIONS = 2      # 2015-07-07    tables inutiles (validations)
VERSIONDB_VALIDATIONS = 1       # 2015-07-07
VERSIONDB_DOCUMENTS = 1         # 2011-08-24
VERSIONDB_CONFIGWEB = 1         # 2013-07-09
VERSIONDB_STRUCTURE = 1         # 2015-02-23
VERSIONDB_STRUCTURES = 1        # 2015-03-04



"""
****************************************************
    DÉTECTION DU TYPE D'INSTALLATION DE VÉRAC
        * ARCHIVE (-1), LOCALE (0), RÉSEAU (1)
****************************************************
"""
INSTALLTYPE_ARCHIVE = -1
INSTALLTYPE_LOCAL = 0
INSTALLTYPE_LAN = 1

installType_initial = INSTALLTYPE_LOCAL
installType = installType_initial

def changeInstallType(newValue):
    global installType
    installType = newValue

# le dossier de config .Verac est-il en réseau :
lanConfig_initial = False
lanConfig = lanConfig_initial
lanConfigDir_initial = ''
lanConfigDir = lanConfigDir_initial
# le dossier des fichiers profxx :
lanFilesDir_initial = ''
lanFilesDir = lanFilesDir_initial
# le dossier de travail (enregistrement des fichiers d'export) est-il en réseau :
lanWorkDir_initial = ''
lanWorkDir = lanWorkDir_initial
# si oui, désactive-t'on le bouton dans la fenêtre de connexion :
lanWorkDirFixed_initial = False
lanWorkDirFixed = lanWorkDirFixed_initial

def changeLanConfig(newValue, newDir, changeInitial=False):
    """
    Le paramètre changeInitial sert lors de la détection automatique
    du réseau lors du lancement de VÉRAC.
    """
    global lanConfig, lanConfigDir, lanConfig_initial, lanConfigDir_initial
    lanConfig = newValue
    lanConfigDir = newDir
    if changeInitial:
        lanConfig_initial = newValue
        lanConfigDir_initial = newDir

def changeLanFilesDir(newDir, changeInitial=False):
    """
    Le paramètre changeInitial sert lors de la détection automatique
    du réseau lors du lancement de VÉRAC.
    """
    global lanFilesDir, lanFilesDir_initial
    lanFilesDir = newDir
    if changeInitial:
        lanFilesDir_initial = newDir

def changeLanWorkDir(newDir, changeInitial=False):
    """
    Le paramètre changeInitial sert lors de la détection automatique
    du réseau lors du lancement de VÉRAC.
    """
    global lanWorkDir, lanWorkDir_initial
    lanWorkDir = newDir
    if changeInitial:
        lanWorkDir_initial = newDir

def changeLanWorkDirFixed(newValue):
    global lanWorkDirFixed
    lanWorkDirFixed = newValue



"""
****************************************************
    DES PARAMÈTRES DE CONFIGURATION MODIFIABLES DANS L'INTERFACE
        * 
        * 
        * 
        * 
****************************************************
"""

# les périodes :
PERIODES = {
    0: 'Year', 1: 'Quarter 1', 2: 'Quarter 2', 3: 'Quarter 3', 
    -1: 'All Periodes', 999: 'Annual Report'}
NB_PERIODES = 4

def changePeriodes(main):
    global PERIODES, NB_PERIODES
    import utils_db
    newPeriodeTableau = []
    commandLine_commun = utils_db.q_selectAllFrom.format('periodes')
    query_commun = utils_db.queryExecute(commandLine_commun, db=main.db_commun)
    while query_commun.next():
        periode_name = query_commun.value(1)
        newPeriodeTableau.append(periode_name)
    i = 0
    if len(newPeriodeTableau) > 0:
        PERIODES = {}
        for periode in newPeriodeTableau:
            PERIODES[i] = periode
            i += 1
        PERIODES[-1] = QtWidgets.QApplication.translate('main', 'All Periodes')
        PERIODES[999] = QtWidgets.QApplication.translate('main', 'Annual Report')
        NB_PERIODES = i

# la période sélectionnée :
selectedPeriod = 0

def changeSelectedPeriod(newValue):
    global selectedPeriod
    selectedPeriod = newValue

# les périodes protégées :
PROTECTED_PERIODS = {}

def clearProtectedPeriods():
    global PROTECTED_PERIODS
    PROTECTED_PERIODS = {}

def changeProtectedPeriod(periodeIndex, newValue):
    global PROTECTED_PERIODS
    PROTECTED_PERIODS[periodeIndex] = newValue



# la précision des notes calculées
# (1 pour 1 chiffre après la virgule, 2 pour 2 chiffres, etc) :
precisionNotes_initial = 1
precisionNotes = precisionNotes_initial

def changePrecisionNotes(newValue):
    global precisionNotes
    precisionNotes = newValue

# variable pour savoir si les détails sont récupérés (concerne l'admin seulement)
# cas très particulier, donc 0 par défaut
ADMIN_WITH_DETAILS = 0

def changeAdminMustRecupDetails(newValue):
    global ADMIN_WITH_DETAILS
    ADMIN_WITH_DETAILS = newValue

# variable pour savoir si la colonne classe est affichée 
# dans les bulletins imprimés (concerne l'admin seulement)
ADMIN_NO_CLASS_COLUMN = 0

def changeAdminNoClassColumn(newValue):
    global ADMIN_NO_CLASS_COLUMN
    ADMIN_NO_CLASS_COLUMN = newValue

# le délai des tests de connexion au site web
# (5 secondes par défaut) :
testNetDelay_initial = 5
testNetDelay = testNetDelay_initial

def changeTestNetDelay(newValue):
    global testNetDelay
    testNetDelay = newValue



"""
****************************************************
    DES PARAMÈTRES DE CONFIGURATION NON MODIFIABLES DANS L'INTERFACE
        * 
        * 
        * 
        * 
****************************************************
"""

# pour les différentier des autres compétences,
# les id des compétences du bulletin commencent à 10000 :
decalageBLT = 10000
decalageCFD = 2 * decalageBLT
# idem pour les id des profs principaux et chefs d'établissement :
decalagePP = 10000
decalageChefEtab = 20000

# quelques couleurs souvent utilisées :
colorWhite = QtGui.QColor(QtCore.Qt.white)
colorBlack = QtGui.QColor(QtCore.Qt.black)
colorGray = QtGui.QColor(QtCore.Qt.gray)
colorLightGray = QtGui.QColor(QtCore.Qt.lightGray)
# couleurs d'affichages des noms des bilans selon leur genre :
colorsBilans = {
    'BULLETIN': QtGui.QColor('#00006a'), 
    'REFERENTIEL': QtGui.QColor('#008a17'), 
    'CONFIDENTIEL': QtGui.QColor('#ed56af'), 
    'PERSO_BLT': QtGui.QColor('#5a0aed'), 
    'PERSO': QtGui.QColor('#000000'), 
    'POSITIONNEMENT': QtGui.QColor('#1533ea'), 
    }

# des caractères spéciaux non-ascii
if OS_NAME[0] == 'linux':
    evalColorChar = ('&#11044;', '⬤')
else:
    evalColorChar = ('&bull;', '•')


# différents niveaux de récupération des résultats :
RECUP_LEVEL_SIMPLE = 0
RECUP_LEVEL_SELECT = 2
RECUP_LEVEL_ALL = 4
RECUP_LEVEL_REFERENTIAL = 9



# constantes pour l'export LSU (réforme 2016) :
LSU = {
    'EPIS': (
        'EPI_SAN', 'EPI_ART', 'EPI_EDD', 'EPI_ICC', 
        'EPI_LGA', 'EPI_LGE', 'EPI_PRO', 'EPI_STS'), 

    'SOCLE_COMPONENTS': {
        'ORDER_LSU': (
            'CPD_FRA', 
            'CPD_SCI', 
            'REP_MND', 
            'CPD_ETR', 
            'SYS_NAT', 
            'CPD_ART', 
            'FRM_CIT', 
            'MET_APP'), 
        'ORDER_REFERENTIAL': (
            'CPD_FRA', 
            'CPD_ETR', 
            'CPD_SCI', 
            'CPD_ART', 
            'MET_APP', 
            'FRM_CIT', 
            'SYS_NAT', 
            'REP_MND'), 
        'REFERENTIAL': {
            'S2D1O1': ('CPD_FRA', 5), 
            'S2D1O2': ('CPD_ETR', 5), 
            'S2D1O3': ('CPD_SCI', 5), 
            'S2D1O4': ('CPD_ART', 5), 
            'S2D2': ('MET_APP', 3), 
            'S2D3': ('FRM_CIT', 3), 
            'S2D4': ('SYS_NAT', 3), 
            'S2D5': ('REP_MND', 3), 
            }, 
        'CPD_FRA': ('S2D1O1', 'CPD_FRA (Domaine 1 - Objectif 1)', 5), 
        'CPD_ETR': ('S2D1O2', 'CPD_ETR (Domaine 1 - Objectif 2)', 5), 
        'CPD_SCI': ('S2D1O3', 'CPD_SCI (Domaine 1 - Objectif 3)', 5), 
        'CPD_ART': ('S2D1O4', 'CPD_ART (Domaine 1 - Objectif 4)', 5), 
        'MET_APP': ('S2D2', 'MET_APP (Domaine 2)', 3), 
        'FRM_CIT': ('S2D3', 'FRM_CIT (Domaine 3)', 3), 
        'SYS_NAT': ('S2D4', 'SYS_NAT (Domaine 4)', 3), 
        'REP_MND': ('S2D5', 'REP_MND (Domaine 5)', 3), 
        }, 

    'ENS_COMPLEMENT': (
        'AUC', 'LCA', 'LCR', 
        'PRO', 'LSF', 'LVE', 'LCE', 'CHK'), 

    'PARCOURS': (
        'PAR_AVN', 'PAR_CIT', 'PAR_ART', 'PAR_SAN'), 

    'MOD_ACCOMPAGNEMENT': (
        'PAP', 'PAI', 'PPRE', 'PPS', 
        'ULIS', 'UPE2A', 'SEGPA', 'CTR', 'DF'), 

    'CPT_NUM': (
        'CN_INF', 'CN_INF_MEN', 'CN_INF_GER', 'CN_INF_TRA', 
        'CN_COM', 'CN_COM_INT', 'CN_COM_PAR', 'CN_COM_COL', 'CN_COM_SIN', 
        'CN_CRE', 'CN_CRE_TEX', 'CN_CRE_MUL', 'CN_CRE_ADA', 'CN_CRE_PRO', 
        'CN_PRO', 'CN_PRO_SEC', 'CN_PRO_DON', 'CN_PRO_SAN', 
        'CN_ENV', 'CN_ENV_RES', 'CN_ENV_EVO'), 

    'LSU_X': 'B', 

    'TRANSLATIONS': {
        # traductions (chargées plus loin dans initTranslations)
        'PAR_LSU': 'LSU_PAR_LSU', 
        'PAR_AVN': 'LSU_PAR_AVN', 
        'PAR_CIT': 'LSU_PAR_CIT', 
        'PAR_ART': 'LSU_PAR_ART', 
        'PAR_SAN': 'LSU_PAR_SAN', 

        'PAP': 'LSU_ACC_PAP', 
        'PAI': 'LSU_ACC_PAI', 
        'PPRE': 'LSU_ACC_PPRE', 
        'PPS': 'LSU_ACC_PPS', 
        'ULIS': 'LSU_ACC_ULIS', 
        'UPE2A': 'LSU_ACC_UPE2A', 
        'SEGPA': 'LSU_ACC_SEGPA', 
        'CTR': 'LSU_ACC_CTR', 
        'DF': 'LSU_ACC_DF', 

        'AUC': 'LSU_COMP_AUC', 
        'LCA': 'LSU_COMP_LCA', 
        'LCR': 'LSU_COMP_LCR', 
        'PRO': 'LSU_COMP_PRO', 
        'LSF': 'LSU_COMP_LSF', 
        'LVE': 'LSU_COMP_LVE', 
        'LCE': 'LSU_COMP_LCE', 
        'CHK': 'LSU_COMP_CHK', 

        'CN_INF': 'LSU_CN_INF', 
        'CN_INF_MEN': 'LSU_CN_INF_MEN', 
        'CN_INF_GER': 'LSU_CN_INF_GER', 
        'CN_INF_TRA': 'LSU_CN_INF_TRA', 
        'CN_COM': 'LSU_CN_COM', 
        'CN_COM_INT': 'LSU_CN_COM_INT', 
        'CN_COM_PAR': 'LSU_CN_COM_PAR', 
        'CN_COM_COL': 'LSU_CN_COM_COL', 
        'CN_COM_SIN': 'LSU_CN_COM_SIN', 
        'CN_CRE': 'LSU_CN_CRE', 
        'CN_CRE_TEX': 'LSU_CN_CRE_TEX', 
        'CN_CRE_MUL': 'LSU_CN_CRE_MUL', 
        'CN_CRE_ADA': 'LSU_CN_CRE_ADA', 
        'CN_CRE_PRO': 'LSU_CN_CRE_PRO', 
        'CN_PRO': 'LSU_CN_PRO', 
        'CN_PRO_SEC': 'LSU_CN_PRO_SEC', 
        'CN_PRO_DON': 'LSU_CN_PRO_DON', 
        'CN_PRO_SAN': 'LSU_CN_PRO_SAN', 
        'CN_ENV': 'LSU_CN_ENV', 
        'CN_ENV_RES': 'LSU_CN_ENV_RES', 
        'CN_ENV_EVO': 'LSU_CN_ENV_EVO', 
        }

    }



"""
****************************************************
    LETTRES, TOUCHES DU CLAVIER ET COULEURS 
    UTILISÉES POUR ÉVALUER.
    CERTAINES VARIABLES PEUVENT ÊTRE MODIFIÉES 
    DANS L'INTERFACE (ADMIN OU PROF).
****************************************************
"""

# en interne et dans les bases de données, VÉRAC manipule A, B, C, D et X (non-modifiable) :
itemsValues = ('A', 'B', 'C', 'D', 'X')

# et il y a 4 couleurs (non-modifiable) :
NB_COLORS = 4

# les icônes des couleurs :
pixmapsItems_initial = {}
pixmapsItems = {}

def changePixmapsItems(newPixmaps, initial=False):
    global pixmapsItems, pixmapsItems_initial
    if initial:
        pixmapsItems_initial = {}
        i = 0
        for pixmap in newPixmaps:
            pixmapsItems_initial[itemsValues[i]] = pixmap
            i += 1
    else:
        pixmapsItems = {}
        i = 0
        for pixmap in newPixmaps:
            pixmapsItems[itemsValues[i]] = pixmap
            i += 1

# la configuration de l'affichage (terminée dans initTranslations) :
affichages_initial = {
    'A': [
        'V', 'Green', '#00ff00', QtGui.QColor('#00ff00'), 
        (QtCore.Qt.Key_V, 'V'), (QtCore.Qt.Key_4, '4'), 
        'Very good control'], 
    'B': [
        'J', 'Yellow', '#ffff00', QtGui.QColor('#ffff00'), 
        (QtCore.Qt.Key_J, 'J'), (QtCore.Qt.Key_3, '3'), 
        'Satisfactory control'], 
    'C': [
        'O', 'Orange', '#ff8000', QtGui.QColor('#ff8000'), 
        (QtCore.Qt.Key_O, 'O'), (QtCore.Qt.Key_2, '2'), 
        'Delicate control'], 
    'D': [
        'R', 'Red', '#ff0000', QtGui.QColor('#ff0000'), 
        (QtCore.Qt.Key_R, 'R'), (QtCore.Qt.Key_1, '1'), 
        'Insufficient control'], 
    'X': [
        'X', 'Not Rated', '#c0c0c0', QtGui.QColor('#c0c0c0'), 
        (QtCore.Qt.Key_X, 'X'), (QtCore.Qt.Key_9, '9'), 
        'The student was marked missing'], 
    }
affichages = {}

def changeAffichages(newAffichages):
    global affichages
    for item in newAffichages:
        for i in (0, 1):
            if newAffichages[item][i] != '':
                affichages[item][i] = newAffichages[item][i]
        if newAffichages[item][2] != '':
            affichages[item][2] = newAffichages[item][2]
            affichages[item][3] = QtGui.QColor(affichages[item][2])
        for i in (3, 4):
            if newAffichages[item][i][0] != 0:
                affichages[item][i + 1] = (
                    newAffichages[item][i][0], newAffichages[item][i][1])
        if newAffichages[item][5] != '':
            affichages[item][5] = newAffichages[item][5]
    # on recharge aussi le dico des touches (voir plus loin) :
    changeKeysEvals()

def changeAffichagesInitial(newAffichages):
    global affichages_initial
    for item in newAffichages:
        for i in (0, 1):
            if newAffichages[item][i] != '':
                affichages_initial[item][i] = newAffichages[item][i]
        if newAffichages[item][2] != '':
            affichages_initial[item][2] = newAffichages[item][2]
            affichages_initial[item][3] = QtGui.QColor(affichages_initial[item][2])
        for i in (3, 4):
            if newAffichages[item][i][0] != 0:
                affichages_initial[item][i + 1] = (
                    newAffichages[item][i][0], newAffichages[item][i][1])
        if newAffichages[item][5] != '':
            affichages_initial[item][5] = newAffichages[item][5]

def initTranslations(main):
    global PROGNAME, DATABASE_CONNECTION_FAIL_MESSAGE, LSU
    global affichages_initial, affichages
    PROGNAME = QtWidgets.QApplication.translate('main', 'VERAC')
    DATABASE_CONNECTION_FAIL_MESSAGE = QtWidgets.QApplication.translate(
        'main', 
        'Unable to establish a database connection.'
        '\n({0})'
        '\n\nClick Cancel to exit.')
    # les noms des couleurs :
    labelsItems = {
        'A': QtWidgets.QApplication.translate('main', 'Green'), 
        'B': QtWidgets.QApplication.translate('main', 'Yellow'), 
        'C': QtWidgets.QApplication.translate('main', 'Orange'), 
        'D': QtWidgets.QApplication.translate('main', 'Red'), 
        'X': QtWidgets.QApplication.translate('main', 'Not Rated')}
    # les légendes des niveaux :
    legendsItems = {
        'A': QtWidgets.QApplication.translate('main', 'Very good control'), 
        'B': QtWidgets.QApplication.translate('main', 'Satisfactory control'), 
        'C': QtWidgets.QApplication.translate('main', 'Delicate control'), 
        'D': QtWidgets.QApplication.translate('main', 'Insufficient control'), 
        'X': QtWidgets.QApplication.translate('main', 'The student was marked missing')}
    # mise à jour de affichages_initial et de affichages :
    for item in affichages_initial:
        affichages_initial[item][1] = labelsItems[item]
        affichages_initial[item][6] = legendsItems[item]
        affichages[item] = []
        for what in affichages_initial[item]:
            affichages[item].append(what)

    # les traductions liées au LSU :
    LSU['TRANSLATIONS']['PAR_LSU'] = QtWidgets.QApplication.translate('main', 'LSU_PAR_LSU')
    LSU['TRANSLATIONS']['PAR_AVN'] = QtWidgets.QApplication.translate('main', 'LSU_PAR_AVN')
    LSU['TRANSLATIONS']['PAR_CIT'] = QtWidgets.QApplication.translate('main', 'LSU_PAR_CIT')
    LSU['TRANSLATIONS']['PAR_ART'] = QtWidgets.QApplication.translate('main', 'LSU_PAR_ART')
    LSU['TRANSLATIONS']['PAR_SAN'] = QtWidgets.QApplication.translate('main', 'LSU_PAR_SAN')

    LSU['TRANSLATIONS']['PAP'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_PAP')
    LSU['TRANSLATIONS']['PAI'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_PAI')
    LSU['TRANSLATIONS']['PPRE'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_PPRE')
    LSU['TRANSLATIONS']['PPS'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_PPS')
    LSU['TRANSLATIONS']['ULIS'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_ULIS')
    LSU['TRANSLATIONS']['UPE2A'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_UPE2A')
    LSU['TRANSLATIONS']['SEGPA'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_SEGPA')
    LSU['TRANSLATIONS']['CTR'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_CTR')
    LSU['TRANSLATIONS']['DF'] = QtWidgets.QApplication.translate('main', 'LSU_ACC_DF')

    LSU['TRANSLATIONS']['AUC'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_AUC')
    LSU['TRANSLATIONS']['LCA'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_LCA')
    LSU['TRANSLATIONS']['LCR'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_LCR')
    LSU['TRANSLATIONS']['PRO'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_PRO')
    LSU['TRANSLATIONS']['LSF'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_LSF')
    LSU['TRANSLATIONS']['LVE'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_LVE')
    LSU['TRANSLATIONS']['LCE'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_LCE')
    LSU['TRANSLATIONS']['CHK'] = QtWidgets.QApplication.translate('main', 'LSU_COMP_CHK')

    LSU['TRANSLATIONS']['CN_INF'] = QtWidgets.QApplication.translate('main', 'LSU_CN_INF')
    LSU['TRANSLATIONS']['CN_INF_MEN'] = QtWidgets.QApplication.translate('main', 'LSU_CN_INF_MEN')
    LSU['TRANSLATIONS']['CN_INF_GER'] = QtWidgets.QApplication.translate('main', 'LSU_CN_INF_GER')
    LSU['TRANSLATIONS']['CN_INF_TRA'] = QtWidgets.QApplication.translate('main', 'LSU_CN_INF_TRA')
    LSU['TRANSLATIONS']['CN_COM'] = QtWidgets.QApplication.translate('main', 'LSU_CN_COM')
    LSU['TRANSLATIONS']['CN_COM_INT'] = QtWidgets.QApplication.translate('main', 'LSU_CN_COM_INT')
    LSU['TRANSLATIONS']['CN_COM_PAR'] = QtWidgets.QApplication.translate('main', 'LSU_CN_COM_PAR')
    LSU['TRANSLATIONS']['CN_COM_COL'] = QtWidgets.QApplication.translate('main', 'LSU_CN_COM_COL')
    LSU['TRANSLATIONS']['CN_COM_SIN'] = QtWidgets.QApplication.translate('main', 'LSU_CN_COM_SIN')
    LSU['TRANSLATIONS']['CN_CRE'] = QtWidgets.QApplication.translate('main', 'LSU_CN_CRE')
    LSU['TRANSLATIONS']['CN_CRE_TEX'] = QtWidgets.QApplication.translate('main', 'LSU_CN_CRE_TEX')
    LSU['TRANSLATIONS']['CN_CRE_MUL'] = QtWidgets.QApplication.translate('main', 'LSU_CN_CRE_MUL')
    LSU['TRANSLATIONS']['CN_CRE_ADA'] = QtWidgets.QApplication.translate('main', 'LSU_CN_CRE_ADA')
    LSU['TRANSLATIONS']['CN_CRE_PRO'] = QtWidgets.QApplication.translate('main', 'LSU_CN_CRE_PRO')
    LSU['TRANSLATIONS']['CN_PRO'] = QtWidgets.QApplication.translate('main', 'LSU_CN_PRO')
    LSU['TRANSLATIONS']['CN_PRO_SEC'] = QtWidgets.QApplication.translate('main', 'LSU_CN_PRO_SEC')
    LSU['TRANSLATIONS']['CN_PRO_DON'] = QtWidgets.QApplication.translate('main', 'LSU_CN_PRO_DON')
    LSU['TRANSLATIONS']['CN_PRO_SAN'] = QtWidgets.QApplication.translate('main', 'LSU_CN_PRO_SAN')
    LSU['TRANSLATIONS']['CN_ENV'] = QtWidgets.QApplication.translate('main', 'LSU_CN_ENV')
    LSU['TRANSLATIONS']['CN_ENV_RES'] = QtWidgets.QApplication.translate('main', 'LSU_CN_ENV_RES')
    LSU['TRANSLATIONS']['CN_ENV_EVO'] = QtWidgets.QApplication.translate('main', 'LSU_CN_ENV_EVO')



# les touches utilisées pour évaluer :
KEYS_EVALS = {}

def changeKeysEvals():
    global KEYS_EVALS
    KEYS_EVALS = {}
    for item in affichages:
        KEYS_EVALS[affichages[item][4][0]] = item
        KEYS_EVALS[affichages[item][5][0]] = item

# touches de clavier pour valider une saisie :
KEYS_VALIDATE = (QtCore.Qt.Key_Tab, QtCore.Qt.Key_Return, QtCore.Qt.Key_Enter)
# touches de déplacement (flèches) :
KEYS_ARROWS = (QtCore.Qt.Key_Up, QtCore.Qt.Key_Down, QtCore.Qt.Key_Left, QtCore.Qt.Key_Right)
# autres touches utilisées :
KEY_SHIFT = QtCore.Qt.Key_Shift
KEY_CONTROL = QtCore.Qt.Key_Control
KEY_COPY = QtCore.Qt.Key_C
KEY_PASTE = QtCore.Qt.Key_V
KEY_CUT = QtCore.Qt.Key_X
KEY_UNDO = QtCore.Qt.Key_Z
KEY_REDO = QtCore.Qt.Key_Y
KEY_DELETE = QtCore.Qt.Key_Delete
KEY_BACKSPACE = QtCore.Qt.Key_Backspace
KEY_ESCAPE = QtCore.Qt.Key_Escape
KEYS_OTHER = (
    KEY_SHIFT, 
    KEY_CONTROL, KEY_COPY, KEY_PASTE, KEY_CUT, KEY_UNDO, KEY_REDO, 
    KEY_DELETE, KEY_BACKSPACE, KEY_ESCAPE)


"""
****************************************************
    LES VUES POSSIBLES DANS L'INTERFACE PROF 
    ET DES LISTES DE VUES.
****************************************************
"""

# si un tableau est sélectionné :
VIEW_ITEMS = 0
VIEW_STATS_TABLEAU = 1
VIEWS_IF_TABLEAU = (VIEW_ITEMS, VIEW_STATS_TABLEAU)

# si un groupe est sélectionné :
VIEW_BILANS = 10
VIEW_BULLETIN = 11
VIEW_APPRECIATIONS = 12
VIEW_STATS_GROUPE = 13
VIEW_ELEVE = 14
VIEW_NOTES = 15
VIEW_SUIVIS = 20
VIEW_ABSENCES = 21
VIEW_COUNTS = 22
VIEW_ACCOMPAGNEMENT = 29
VIEW_CPT_NUM = 30
VIEW_DNB = 31
VIEWS_IF_GROUP = (
    VIEW_BILANS, VIEW_BULLETIN, VIEW_APPRECIATIONS, 
    VIEW_STATS_GROUPE, VIEW_ELEVE, VIEW_NOTES, 
    VIEW_ABSENCES, VIEW_ACCOMPAGNEMENT, VIEW_CPT_NUM, VIEW_DNB, 
    VIEW_COUNTS)

# vues sans réel affichage :
VIEW_NEWS = -2
VIEW_RANDOM_HELP = -3
VIEW_DBSTATE = -10
VIEW_WEBSITE = -20
VIEW_NOTHING = -99
VIEWS_NO = (VIEW_NEWS, VIEW_RANDOM_HELP, VIEW_DBSTATE, VIEW_WEBSITE, VIEW_NOTHING)

# juste pour placer des séparateurs dans le menu :
VIEW_SEPARATOR = -999

# liste des vues dans le menu :
VIEWS_MENU = (
    VIEW_ITEMS, VIEW_STATS_TABLEAU, 
    VIEW_SEPARATOR, 
    VIEW_BILANS, VIEW_BULLETIN, VIEW_APPRECIATIONS, 
    VIEW_STATS_GROUPE, VIEW_ELEVE, VIEW_NOTES, VIEW_COUNTS, 
    VIEW_SEPARATOR, 
    VIEW_SUIVIS, VIEW_ABSENCES, VIEW_ACCOMPAGNEMENT, VIEW_CPT_NUM, VIEW_DNB, 
    VIEW_SEPARATOR, 
    VIEW_NEWS, 
    VIEW_RANDOM_HELP, 
    VIEW_DBSTATE, 
    VIEW_WEBSITE)
VIEWS_MENU_SIMPLIFIED = (
    VIEW_ITEMS, 
    VIEW_BILANS, VIEW_BULLETIN, 
    VIEW_ELEVE, VIEW_COUNTS, 
    VIEW_SEPARATOR, 
    VIEW_ABSENCES, VIEW_ACCOMPAGNEMENT, VIEW_CPT_NUM, VIEW_DNB, 
    VIEW_SEPARATOR, 
    VIEW_NEWS, 
    VIEW_RANDOM_HELP)

# autres listes utiles :
VIEWS_WITH_COLORS = (
    VIEW_ITEMS, VIEW_BILANS, VIEW_BULLETIN, 
    VIEW_ELEVE, VIEW_SUIVIS, VIEW_COUNTS, 
    VIEW_ACCOMPAGNEMENT, VIEW_DNB)
VIEWS_STATS = (VIEW_STATS_TABLEAU, VIEW_STATS_GROUPE)
VIEWS_WITH_APPRECIATIONS = (
    VIEW_BULLETIN, VIEW_APPRECIATIONS, 
    VIEW_SUIVIS, VIEW_ACCOMPAGNEMENT, VIEW_CPT_NUM, VIEW_DNB)
VIEWS_WITH_EVALUATIONS = (
    VIEW_ITEMS, VIEW_NOTES, VIEW_SUIVIS, 
    VIEW_ABSENCES, VIEW_COUNTS, 
    VIEW_ACCOMPAGNEMENT, VIEW_CPT_NUM, VIEW_DNB)
VIEWS_WITH_NOTES = (VIEW_NOTES, VIEW_ABSENCES, VIEW_COUNTS, VIEW_CPT_NUM)
VIEWS_WITH_GROUPLINE = (
    VIEW_BILANS, VIEW_BULLETIN, VIEW_APPRECIATIONS, 
    VIEW_NOTES, VIEW_SUIVIS)
VIEWS_WITH_EDITABLES = (
    VIEW_BULLETIN, VIEW_APPRECIATIONS, VIEW_NOTES, 
    VIEW_SUIVIS, VIEW_ABSENCES, VIEW_COUNTS, 
    VIEW_ACCOMPAGNEMENT, VIEW_CPT_NUM, VIEW_DNB)
VIEWS_WITH_VERTICALS = (
    VIEW_BILANS, VIEW_BULLETIN, VIEW_SUIVIS, VIEW_ACCOMPAGNEMENT, VIEW_CPT_NUM)

# les pages d'aide contextuelle associées :
VIEWS_HELPPAGE = {
    VIEW_ITEMS: 'vue-items', 
    VIEW_STATS_TABLEAU: 'vue-stats-tableau', 
    VIEW_BILANS: 'vue-bilans', 
    VIEW_BULLETIN: 'vue-bulletin', 
    VIEW_APPRECIATIONS: 'vue-appreciations', 
    VIEW_STATS_GROUPE: 'vue-stats-groupe', 
    VIEW_ELEVE: 'vue-eleve', 
    VIEW_NOTES: 'notes', 
    VIEW_SUIVIS: 'gest-eleves-suivis', 
    VIEW_ABSENCES: 'vs', 
    VIEW_COUNTS: 'create-count', 
    VIEW_ACCOMPAGNEMENT: 'accompagnement', 
    VIEW_CPT_NUM: 'cpt-numeriques', 
    VIEW_DNB: 'dnb-chef-etab', 
    VIEW_NOTHING: 'prise-en-main-rapide', 
    }

# les pages d'aide au hasard :
RANDOM_HELP_PAGES = [
    'news', 
    'tech-calculs', 
    'lsu', 
    'vue-items', 'vue-stats-tableau', 
    'vue-bilans', 'vue-bulletin', 'vue-appreciations', 'vue-stats-groupe', 'vue-eleve', 
    'create-multiple-groupes-classes', 
    'templates', 'ordre-groupes-tableaux', 'compilation', 'create-multiple-tableaux', 
    'saisir-evaluations', 'copier-coller', 'simplifier', 'verif-bulletin', 'appreciations', 'list-visible-items', 
    'import-export', 
    'prof-principal', 'epi', 'ap', 
    'interface', 'items-bilans-tableaux-periodes']

# pour savoir si wkhtmltopdf est installé :
WKHTMLTOPDF = None

def changeWkhtmltopdf(newValue):
    global WKHTMLTOPDF
    WKHTMLTOPDF = newValue


# pour savoir où on a travaillé (peut être utile en cas de débogage) :
COMPUTER_INFO = ''

def changeComputerInfos(newValue):
    global COMPUTER_INFO
    COMPUTER_INFO = newValue

# les langues disponibles dans l'interface web :
WEB_LOCALES = (
    ('fr_FR', QtWidgets.QApplication.translate('main', 'French')), 
    ('en_US', QtWidgets.QApplication.translate('main', 'English')), 
    )

# la langue de l'interface :
LOCALE = ''

def changeLocale(newValue):
    global LOCALE
    LOCALE = newValue


"""
****************************************************
    GESTION DES DIFFÉRENTS TYPES DE DONNÉES 
    (NOMBRES, TEXTES, ...)
****************************************************
"""

INDETERMINATE = -1

INTEGER = 0
FLOAT = 1
NUMBER = 4
TYPES_NUMBER = (NUMBER, INTEGER, FLOAT)

LINE = 2
TEXT = 3
TYPES_TEXT = (LINE, TEXT)

EVAL = 5
DATE = 10
HORAIRE = 11
NOTE_PERSO = 20
NOTE_CALCUL = 21
TYPES_NOTE = (NOTE_PERSO, NOTE_CALCUL)

# alignement par type :
TYPES_ALIGN_LEFT = (LINE, TEXT)
TYPES_ALIGN_RIGHT = (INTEGER, FLOAT)
TYPES_ALIGN_CENTER = (INDETERMINATE, EVAL, NUMBER, DATE, HORAIRE, NOTE_PERSO, NOTE_CALCUL)

# les caractères numériques :
DECIMAL_SEPARATOR = QtCore.QLocale().decimalPoint()
CHARS_NUMERIC = ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', DECIMAL_SEPARATOR)



"""
****************************************************
    VARIABLES POUR LES CALCULS
****************************************************
"""

# Les variables pour les calculs d'item / Bilans
# les valeurs initiales servent au bouton "Réinitialiser" de la fenêtre de réglage
# Ainsi, les valeurs initiales ne sont plus "en dur" qu'ici.
# 1 pour BEST, 2 pour LAST, 0 pour ALL
MODECALCUL_initial = 0
MODECALCUL = MODECALCUL_initial

def changeModeCalcul(newValue):
    global MODECALCUL
    MODECALCUL = newValue

# nombre maxi d'éval prises en compte. Si =0, alors tout est pris saus si ALL
NBMAXEVAL_initial = 3
NBMAXEVAL = NBMAXEVAL_initial

def changeNbMaxEval(newValue):
    global NBMAXEVAL
    NBMAXEVAL = newValue

# 1 pour coefficienter + fort les dernières évaluations
COEFLAST_initial = 0
COEFLAST = COEFLAST_initial

def changeCoefLast(newValue):
    global COEFLAST
    COEFLAST = newValue

# Pourcentage maximal d'évaluations non réalisées
# pour prise en compte de l'item dans les calculs de bilan
levelValidItemPC_initial = 50
levelValidItemPC = levelValidItemPC_initial

def changeLevelValidItemPC(newValue):
    global levelValidItemPC
    levelValidItemPC = newValue

# ou Nbre minimal d'évaluations réalisées
# pour prise en compte de l'item dans les calculs de bilan
levelValidItemNB_initial = 3
levelValidItemNB = levelValidItemNB_initial

def changeLevelValidItemNB(newValue):
    global levelValidItemNB
    levelValidItemNB = newValue

# opérateur logique utilisé (choix entre ET et OU pour les 2 levelValidItem précédents)
# 0 pour OU, 1 pour ET
levelValidItemOP_initial = 0
levelValidItemOP = levelValidItemOP_initial

def changeLevelValidItemOP(newValue):
    global levelValidItemOP
    levelValidItemOP = newValue


# Les variables pour les calculs de notes.
# Servent aussi dans moyenneBilan.
# 0 : par défaut, 1 : comme pour le DNB, 2 : personnalisé
MODECALCULNOTES_initial = 0
MODECALCULNOTES = MODECALCULNOTES_initial

#Valeur théorique de chaque évaluation (pour les moyennes)
valeursEval_initial = [100, 67, 33, 0, 0]
valeursEval = []
for valeur in valeursEval_initial:
    valeursEval.append(valeur)

def changeValeursEval(index, newValue):
    global valeursEval
    valeursEval[index] = newValue

def changeModeCalculNotes(newValue):
    global MODECALCULNOTES, valeursEval
    MODECALCULNOTES = newValue
    if MODECALCULNOTES == 0:
        valeursEval = [100, 67, 33, 0, 0]
    elif MODECALCULNOTES == 1:
        valeursEval = [100, 80, 50, 20, 0]






#Niveau attendu pour la "moyenne"
#levelItem_initial = [80, 51, 25, 1, 0]
levelItem_initial = [83, 50, 17, 0, 0]
levelItem = []
for valeur in levelItem_initial:
    levelItem.append(valeur)

def changeLevelItem(newValue):
    global levelItem
    levelItem = newValue


# Conditions de Bilan pour le prof et l'admin (en %)
# les valeurs initiales servent au bouton "Réinitialiser" de la fenêtre de réglage
# Ainsi, les valeurs initiales ne sont plus "en dur" qu'ici.

# levelA=[minA, minAB, maxC, maxD]
levelA_initial = [50, 90, 10, 0]
levelA = []
for valeur in levelA_initial:
    levelA.append(valeur)

def changeLevelA(index, newValue):
    global levelA
    levelA[index] = newValue

# levelB=[minAB, maxC, maxD]
levelB_initial = [50, 50, 30]
levelB = []
for valeur in levelB_initial:
    levelB.append(valeur)

def changeLevelB(index, newValue):
    global levelB
    levelB[index] = newValue

# levelX et levelX_NB
levelX_initial = 50
levelX = levelX_initial
levelX_NB_initial = 1
levelX_NB = levelX_NB_initial

def changeLevelX(newValue):
    global levelX
    levelX = newValue

def changeLevelX_NB(newValue):
    global levelX_NB
    levelX_NB = newValue


STYLE = {}
def loadStyle():
    global STYLE
    style = QtWidgets.QApplication.style()
    STYLE = {
        'PM_ToolBarIconSize': style.pixelMetric(QtWidgets.QStyle.PM_ToolBarIconSize), 
        'PM_LargeIconSize': style.pixelMetric(QtWidgets.QStyle.PM_LargeIconSize), 
        'PM_SmallIconSize': style.pixelMetric(QtWidgets.QStyle.PM_SmallIconSize), 
        'PM_MessageBoxIconSize': style.pixelMetric(QtWidgets.QStyle.PM_MessageBoxIconSize), 
        'PM_ScrollBarExtent': style.pixelMetric(QtWidgets.QStyle.PM_ScrollBarExtent), 
        }

SUPPORTED_IMAGE_FORMATS = ('png', )

def loadSupportedImageFormats():
    global SUPPORTED_IMAGE_FORMATS
    SUPPORTED_IMAGE_FORMATS = QtGui.QImageReader.supportedImageFormats()

def doIcon(fileName='', ext='svgz', what='ICON'):
    if ext in SUPPORTED_IMAGE_FORMATS:
        allFileName = 'images/{0}.{1}'.format(fileName, ext)
    else:
        allFileName = 'images/png/{0}.png'.format(fileName)
    if not(QtCore.QFile(allFileName).exists()):
        print('doIcon:', allFileName)
        allFileName = 'images/png/{0}.png'.format(fileName)
    if what == 'ICON':
        return QtGui.QIcon(allFileName)
    else:
        return QtGui.QPixmap(allFileName)







"""
****************************************************
    AUTRES TRUCS
****************************************************
"""

SEPARATOR_LINE = '_' * 40


