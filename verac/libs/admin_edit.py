# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Une fenêtre d'edition des bases admin et users
    Possibilités :
    - Editer id, Nom, Prenom, Date de naissance (élève), Login et Mdp
    - Réinitialiser un Mdp dans la base users
    - Supprimer des utilisateurs
    - Ajouter des utilisateurs
    TODO :
    - formater les entrées date de naissance (type JJ/MM/AAAA)

    Sécurités mises en place :
    - les id ne sont pas modifiables immédiatement
    - les id ne peuvent être que numérique
    - les modifications ne sont enregistrées dans les bases qu'après validation
    de la boite de dialogue ;
    - 2 profs ou 2 élèves ne peuvent avoir le même id ;
    - 2 utilisateurs ne peuvent avoir le même Login (à améliorer)
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_web, utils_filesdirs
import admin, admin_web, admin_ftp

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



# des RegExp tout prêts utilisés plusieurs fois :
# que des lettres minuscules :
simpleRegExp = QtCore.QRegExp('[a-z]*')
# pour les chemins ftp :
dirRegExp = QtCore.QRegExp('[a-zA-Z0-9\/\:\_\-\.\$\#\!\?\*\+]*')
testRegExp = QtCore.QRegExp('[a-zA-Z0-9\/\_\.\$\#\!\?\*\+]*')

def setOk(validator, size, ok=True, toolTip='aaa'):
    if ok:
        iconFileName = 'dialog-ok-apply'
    else:
        iconFileName = 'dialog-warning'
    validator.setPixmap(
        utils.doIcon(iconFileName, what='PIXMAP').scaled(
            size, size,
            QtCore.Qt.KeepAspectRatio,
            QtCore.Qt.SmoothTransformation))
    if toolTip != 'aaa':
        validator.setToolTip(toolTip)





###########################################################"
#   GESTION DES BASES DE DONNÉES
#   MANAGING DATABASES
###########################################################"

class ManageDatabasesDlg(QtWidgets.QDialog):
    """
    """
    def __init__(self, parent=None, what='', indexTab=0, modified=False):
        utils_functions.doWaitCursor()
        super(ManageDatabasesDlg, self).__init__(parent)
        self.main = parent
        self.what = what
        self.helpContextPage = ''
        # pour savoir ce qui a été modifié :
        self.modifiedTables = {
            'DATABASES': {
                'admin': False, 
                'commun': False, 
                'users': False, 
                'configweb': False},
            'admin': {
                'config': False, 
                'config_admin': False, 
                'config_calculs': False, 
                'eleves': False, 
                'profs': False, 
                'replacements': False, 
                'lsu': False, 
                'adresses': False},
            'commun': {
                'config': False, 
                'matieres': False, 
                'periodes': False, 
                'horaires': False, 
                'classes': False, 
                'classestypes': False, 
                'bulletin': False, 
                'referentiel': False, 
                'confidentiel': False, 
                'suivi': False, 
                'sous_rubriques': False},
            'users': {
                'config': False, 
                'eleves': False, 
                'profs': False},
            'configweb': {
                'dbconfig': False, 
                'config': False, 
                'state': False},
            }
        # et si on a utilisé le bouton "appliquer" :
        self.applyed = False
        # certaines tables sont modifiées en cours de traitement
        # le dico suivant sert à les remplacer :
        self.tables = {}
        modePerso = (self.main.actualVersion['versionName'] == 'perso')
        # le titre de la fenêtre :
        titles = {
            'ConfigurationSchool': 'Configuring School', 
            'Classes': 'Classes', 
            'SharedCompetences': 'Shared competences', 
            'Users': 'Users management', 
            'Addresses': 'Management of addresses', 
            'WebSite': 'WebSite', 
            }
        title = QtWidgets.QApplication.translate('main', titles[what])
        self.setWindowTitle(title)

        # les boutons du ManageDatabasesDlg :
        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('ok', 'apply', 'cancel', 'help'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]
        self.buttonsList['apply'].clicked.connect(self.doApply)
        self.buttonsList['apply'].setEnabled(False)

        # le tabWidget et sa liste d'onglets :
        self.tabWidget = QtWidgets.QTabWidget(self)
        self.tabWidget.currentChanged.connect(self.doCurrentTabChanged)
        self.tabs = []
        # création des onglets en fonction de what :
        if what == 'ConfigurationSchool':
            # onglet School :
            if not(modePerso):
                self.tabSchool = SchoolDlg(parent=self)
                self.tabWidget.addTab(
                    self.tabSchool, 
                    QtWidgets.QApplication.translate('main', 'School'))
                self.tabs.append(self.tabSchool)
            # onglet Periods :
            self.tabPeriods = PeriodsDlg(parent=self)
            self.tabWidget.addTab(
                self.tabPeriods, 
                QtWidgets.QApplication.translate('main', 'Periods'))
            self.tabs.append(self.tabPeriods)
            # onglet Hours :
            self.tabHours = HoursDlg(parent=self)
            self.tabWidget.addTab(
                self.tabHours, 
                QtWidgets.QApplication.translate('main', 'Hours'))
            self.tabs.append(self.tabHours)
            # onglet Subjects :
            self.tabSubjects = SubjectsDlg(parent=self)
            self.tabWidget.addTab(
                self.tabSubjects, 
                QtWidgets.QApplication.translate('main', 'Subjects'))
            self.tabs.append(self.tabSubjects)
            # onglet Others :
            self.tabOthers = OthersDlg(parent=self)
            self.tabWidget.addTab(
                self.tabOthers, 
                QtWidgets.QApplication.translate('main', 'Others'))
            self.tabs.append(self.tabOthers)
        elif what == 'Classes':
            # récupération de commun.classestypes :
            self.tables['commun.classestypes'] = []
            query_commun = utils_db.query(self.main.db_commun)
            commandLine_commun = utils_db.q_selectAllFrom.format('classestypes')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                self.tables['commun.classestypes'].append((
                    int(query_commun.value(0)), query_commun.value(1)))
            # onglet Classes :
            self.tabClasses = ClassesDlg(parent=self)
            self.tabWidget.addTab(
                self.tabClasses, 
                QtWidgets.QApplication.translate('main', 'Classes'))
            self.tabs.append(self.tabClasses)
            # onglet ClassesTypes :
            self.tabClassesTypes = ClassesTypesDlg(parent=self)
            self.tabWidget.addTab(
                self.tabClassesTypes, 
                QtWidgets.QApplication.translate('main', 'ClassesTypes'))
            self.tabs.append(self.tabClassesTypes)
        elif what == 'SharedCompetences':
            # onglet Bulletin :
            self.tabBulletin = CptDlg(
                parent=self, tableName='bulletin')
            self.tabWidget.addTab(
                self.tabBulletin, 
                QtWidgets.QApplication.translate(
                    'main', 'Competences of the school report'))
            self.tabs.append(self.tabBulletin)
            # onglet Referentiel :
            self.tabReferentiel = CptDlg(
                parent=self, tableName='referentiel')
            self.tabWidget.addTab(
                self.tabReferentiel, 
                QtWidgets.QApplication.translate(
                    'main', 'Competences of referential'))
            self.tabs.append(self.tabReferentiel)
            # onglet Confidentiel :
            self.tabConfidentiel = CptDlg(
                parent=self, tableName='confidentiel')
            self.tabWidget.addTab(
                self.tabConfidentiel, 
                QtWidgets.QApplication.translate(
                    'main', 'Confidential competences'))
            self.tabs.append(self.tabConfidentiel)
            # onglet Suivi :
            self.tabSuivi = CptDlg(
                parent=self, tableName='suivi')
            self.tabWidget.addTab(
                self.tabSuivi, 
                QtWidgets.QApplication.translate(
                    'main', 'Follow competences'))
            self.tabs.append(self.tabSuivi)
            # onglet outils :
            self.tabCptUtils = CptUtilsDlg(parent=self)
            self.tabWidget.addTab(
                self.tabCptUtils, 
                QtWidgets.QApplication.translate('main', 'Tools'))
            self.tabs.append(self.tabCptUtils)
        elif what == 'Users':
            # onglet Teachers :
            if not(modePerso):
                self.tabTeachers = TeachersDlg(parent=self)
                self.tabWidget.addTab(
                    self.tabTeachers, 
                    QtWidgets.QApplication.translate('main', 'Teachers'))
                self.tabs.append(self.tabTeachers)
            # onglet Students :
            self.tabStudents = StudentsDlg(parent=self)
            self.tabWidget.addTab(
                self.tabStudents, 
                QtWidgets.QApplication.translate('main', 'Students'))
            self.tabs.append(self.tabStudents)
            # onglet outils :
            self.tabUsersUtils = UsersUtilsDlg(parent=self)
            self.tabWidget.addTab(
                self.tabUsersUtils, 
                QtWidgets.QApplication.translate('main', 'Tools'))
            self.tabs.append(self.tabUsersUtils)
        elif what == 'Addresses':
            # onglet Addresses :
            self.tabAddresses = AddressesDlg(parent=self)
            self.tabWidget.addTab(
                self.tabAddresses, 
                QtWidgets.QApplication.translate('main', 'Addresses'))
            self.tabs.append(self.tabAddresses)
            # onglet outils :
            self.tabAddressesUtils = AddressesUtilsDlg(parent=self)
            self.tabWidget.addTab(
                self.tabAddressesUtils, 
                QtWidgets.QApplication.translate('main', 'Tools'))
            self.tabs.append(self.tabAddressesUtils)
        elif what == 'WebSite':
            # onglet WebSiteSetup :
            self.tabWebSiteSetup = WebSiteSetupDlg(parent=self)
            self.tabWidget.addTab(
                self.tabWebSiteSetup, 
                QtWidgets.QApplication.translate(
                    'main', 'Setting up the website'))
            self.tabs.append(self.tabWebSiteSetup)
            # onglet WebSiteState :
            self.tabWebSiteState = WebSiteStateDlg(parent=self)
            self.tabWidget.addTab(
                self.tabWebSiteState, 
                QtWidgets.QApplication.translate(
                    'main', 'Website state'))
            self.tabs.append(self.tabWebSiteState)

        # on indique l'onglet de départ :
        self.tabWidget.setCurrentIndex(indexTab)

        if modified:
            for tab in self.tabs:
                tab.modified = True
            self.buttonsList['apply'].setEnabled(True)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.tabWidget)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        utils_functions.restoreCursor()

    def doCurrentTabChanged(self, index):
        self.helpContextPage = self.tabWidget.currentWidget().helpContextPage
        if type(self.tabWidget.currentWidget()) == ClassesDlg:
            self.tabWidget.currentWidget().initializeComboBox()
        elif type(self.tabWidget.currentWidget()) == UsersUtilsDlg:
            self.tabWidget.currentWidget().studentsFromSIECLE_Button.clearFocus()
        elif type(self.tabWidget.currentWidget()) == CptUtilsDlg:
            self.tabWidget.currentWidget().schoolReportTemplate_Button.clearFocus()
        elif type(self.tabWidget.currentWidget()) == AddressesUtilsDlg:
            self.tabWidget.currentWidget().fromSIECLE_Button.clearFocus()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, 'admin')

    def doApply(self):
        """
        bouton "appliquer" les changements sans fermer la fenêtre
        """
        for tab in self.tabs:
            tab.checkModifications()
        self.applyed = True
        self.updateButtons(False)
        self.buttonsList['cancel'].clearFocus()

    def updateButtons(self, modified=True):
        self.buttonsList['apply'].setEnabled(modified)




###########################################################"
#   CONFIGURATION GÉNÉRALE DE L'ÉTABLISSEMENT
#   GENERAL CONFIGURATION OF SCHOOL
###########################################################"

class SchoolDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer l'établissement (nom, adresse, ...)
    """
    def __init__(self, parent=None):
        super(SchoolDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'config-etab'
        # des modifications ont été faites :
        self.modified = False

        # Nom complet de l'établissement :
        text = QtWidgets.QApplication.translate('main', 'Full name of the school:')
        schoolLabelLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.schoolLabelEdit = QtWidgets.QLineEdit()
        # Nom court de l'établissement :
        text = QtWidgets.QApplication.translate('main', 'SchoolShortName:')
        schoolShortNameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.schoolShortNameEdit = QtWidgets.QLineEdit()
        self.schoolShortNameEdit.setValidator(
            QtGui.QRegExpValidator(simpleRegExp, self.schoolShortNameEdit))
        # Préfixe des fichiers profs :
        text = QtWidgets.QApplication.translate('main', 'prefixProfFiles:')
        prefixProfFilesLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.prefixProfFilesEdit = QtWidgets.QLineEdit()
        # Adresse de l'établissement (sur 3 lignes) :
        text = QtWidgets.QApplication.translate('main', 'Adress:')
        adressLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.adress1Edit = QtWidgets.QLineEdit()
        self.adress2Edit = QtWidgets.QLineEdit()
        self.adress3Edit = QtWidgets.QLineEdit()
        # Numéro de téléphone de l'établissement :
        text = QtWidgets.QApplication.translate('main', 'Phone:')
        telephoneLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.telephoneEdit = QtWidgets.QLineEdit()
        # Numéro UAI (ex RNE) de l'établissement :
        text = QtWidgets.QApplication.translate('main', 'School Code:')
        codeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.codeEdit = QtWidgets.QLineEdit()
        # Académie :
        text = QtWidgets.QApplication.translate('main', 'Academie:')
        academieLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.academieEdit = QtWidgets.QLineEdit()
        # Département :
        text = QtWidgets.QApplication.translate('main', 'Departement:')
        departementLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.departementEdit = QtWidgets.QLineEdit()

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(schoolLabelLabel,            1, 0)
        grid.addWidget(self.schoolLabelEdit,        1, 1, 1, 3)
        grid.addWidget(schoolShortNameLabel,        2, 0)
        grid.addWidget(self.schoolShortNameEdit,    2, 1)
        grid.addWidget(prefixProfFilesLabel,        2, 2)
        grid.addWidget(self.prefixProfFilesEdit,    2, 3)
        grid.addWidget(adressLabel,                 10, 0)
        grid.addWidget(self.adress1Edit,            10, 1, 1, 3)
        grid.addWidget(self.adress2Edit,            11, 1, 1, 3)
        grid.addWidget(self.adress3Edit,            12, 1, 1, 3)
        grid.addWidget(telephoneLabel,              15, 0)
        grid.addWidget(self.telephoneEdit,          15, 1)
        grid.addWidget(codeLabel,               15, 2)
        grid.addWidget(self.codeEdit,           15, 3)
        grid.addWidget(academieLabel,            20, 0)
        grid.addWidget(self.academieEdit,        20, 1)
        grid.addWidget(departementLabel,            20, 2)
        grid.addWidget(self.departementEdit,        20, 3)
        self.setLayout(grid)
        self.setMinimumWidth(600)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les bases admin.config et commun.config
        pour remplir les QLineEdit.
        """
        texts = {}
        # lecture de admin.config :
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFrom.format('config')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            texts[query_admin.value(0)] = query_admin.value(2)
        # lecture de commun.config :
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.q_selectAllFrom.format('config')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            texts[query_commun.value(0)] = query_commun.value(2)
        # mise à jour des QLineEdit :
        self.schoolLabelEdit.setText(texts.get('versionLabel', ''))
        self.schoolShortNameEdit.setText(texts.get('versionName', ''))
        self.prefixProfFilesEdit.setText(texts.get('prefixProfFiles', ''))
        self.prefixProfFilesEdit.setValidator(
            QtGui.QRegExpValidator(simpleRegExp, self.prefixProfFilesEdit))
        self.adress1Edit.setText(texts.get('ETAB_ADRESSE_1', ''))
        self.adress2Edit.setText(texts.get('ETAB_ADRESSE_2', ''))
        self.adress3Edit.setText(texts.get('ETAB_ADRESSE_3', ''))
        self.telephoneEdit.setText(texts.get('ETAB_TELEPHONE', ''))
        self.codeEdit.setText(texts.get('ETAB_CODE', ''))
        self.academieEdit.setText(texts.get('ACADEMIE', ''))
        self.departementEdit.setText(texts.get('DEPARTEMENT', ''))

    def createConnexions(self):
        lineEdits = (#QLineEdit
            self.schoolLabelEdit, 
            self.schoolShortNameEdit, 
            self.prefixProfFilesEdit, 
            self.adress1Edit, self.adress2Edit, self.adress3Edit, 
            self.telephoneEdit, 
            self.codeEdit, 
            self.academieEdit, 
            self.departementEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.editingFinished.connect(self.doModified)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        commandLines_commun = []
        commandLines_admin = []
        commandLines_configweb = []
        commandLineInsertText = utils_functions.u('INSERT INTO config VALUES ("{0}", "", "{1}")')
        commandLineInsertInteger = 'INSERT INTO config VALUES ("{0}", {1}, "")'

        if self.schoolLabelEdit.isModified():
            self.parent.modifiedTables['commun']['config'] = True
            newText = self.schoolLabelEdit.text()
            commandLines_commun.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'versionLabel'),
                commandLineInsertText.format('versionLabel', newText)])
            commandLines_configweb.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'VERSION_LABEL'),
                commandLineInsertText.format('VERSION_LABEL', newText)])
        if self.schoolShortNameEdit.isModified():
            self.parent.modifiedTables['commun']['config'] = True
            newText = self.schoolShortNameEdit.text()
            commandLines_commun.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'versionName'),
                commandLineInsertText.format('versionName', newText)])
            commandLines_configweb.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'VERSION_NAME'),
                commandLineInsertText.format('VERSION_NAME', newText)])
        if self.prefixProfFilesEdit.isModified():
            self.parent.modifiedTables['commun']['config'] = True
            newText = self.prefixProfFilesEdit.text()
            commandLines_commun.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'prefixProfFiles'),
                commandLineInsertText.format('prefixProfFiles', newText)])
            commandLines_configweb.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'PREFIXE_DB_PROF'),
                commandLineInsertText.format('PREFIXE_DB_PROF', newText)])
        if self.adress1Edit.isModified():
            commandLines_admin.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'ETAB_ADRESSE_1'),
                commandLineInsertText.format('ETAB_ADRESSE_1', self.adress1Edit.text())])
        if self.adress2Edit.isModified():
            commandLines_admin.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'ETAB_ADRESSE_2'),
                commandLineInsertText.format('ETAB_ADRESSE_2', self.adress2Edit.text())])
        if self.adress3Edit.isModified():
            commandLines_admin.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'ETAB_ADRESSE_3'),
                commandLineInsertText.format('ETAB_ADRESSE_3', self.adress3Edit.text())])
        if self.telephoneEdit.isModified():
            commandLines_admin.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'ETAB_TELEPHONE'),
                commandLineInsertText.format('ETAB_TELEPHONE', self.telephoneEdit.text())])
        if self.codeEdit.isModified():
            commandLines_admin.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'ETAB_CODE'),
                commandLineInsertText.format('ETAB_CODE', self.codeEdit.text())])
        if self.academieEdit.isModified():
            commandLines_admin.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'ACADEMIE'),
                commandLineInsertText.format('ACADEMIE', self.academieEdit.text())])
        if self.departementEdit.isModified():
            commandLines_admin.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'DEPARTEMENT'),
                commandLineInsertText.format('DEPARTEMENT', self.departementEdit.text())])

        if len(commandLines_commun) > 0:
            self.parent.modifiedTables['DATABASES']['commun'] = True
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
            try:
                query_commun = utils_db.queryExecute(
                    commandLines_commun, query=query_commun)
            finally:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
        if len(commandLines_admin) > 0:
            self.parent.modifiedTables['DATABASES']['admin'] = True
            query_admin, transactionOK_admin = utils_db.queryTransactionDB(
                admin.db_admin)
            try:
                query_admin = utils_db.queryExecute(
                    commandLines_admin, query=query_admin)
            finally:
                utils_db.endTransaction(
                    query_admin, admin.db_admin, transactionOK_admin)
        if len(commandLines_configweb) > 0:
            self.parent.modifiedTables['DATABASES']['configweb'] = True
            admin.openDB(self.main, 'configweb')
            query_configweb, transactionOK_configweb = utils_db.queryTransactionDB(
                admin.db_configweb)
            try:
                query_configweb = utils_db.queryExecute(
                    commandLines_configweb, query=query_configweb)
            finally:
                utils_db.endTransaction(
                    query_configweb, admin.db_configweb, transactionOK_configweb)
        self.modified = False


class PeriodsDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer la liste des périodes.
    """
    def __init__(self, parent=None):
        super(PeriodsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'config-etab'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []

        # la zone de sélection :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        upButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-up'), 
            QtWidgets.QApplication.translate('main', 'Up'))
        upButton.clicked.connect(self.doUp)
        downButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-down'), 
            QtWidgets.QApplication.translate('main', 'Down'))
        downButton.clicked.connect(self.doDown)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList,   1, 0)
        baseGrid.addWidget(upButton,        10, 0)
        baseGrid.addWidget(downButton,      11, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(self, layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # name :
        text = QtWidgets.QApplication.translate('main', 'Name:')
        nameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.editingFinished.connect(self.doEdited)
        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,    1, 1)
        editGrid.addWidget(nameLabel,       2, 0)
        editGrid.addWidget(self.nameEdit,   2, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de commun.periodes :
        """
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.q_selectAllFrom.format('periodes')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            item = QtWidgets.QListWidgetItem(utils_functions.u(query_commun.value(1)))
            item.setData(
                QtCore.Qt.UserRole, 
                [int(query_commun.value(0)), 
                 query_commun.value(1)])
            self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        self.lastData = current.data(QtCore.Qt.UserRole)
        actions = {
            self.nameEdit: ('TEXT', 1),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 1)

    def doUp(self):
        current = self.baseList.currentItem()
        index = self.baseList.indexFromItem(current).row()
        current = self.baseList.takeItem(index)
        self.baseList.insertItem(index - 1, current)
        self.baseList.setCurrentItem(current)
        self.doModified()

    def doDown(self):
        current = self.baseList.currentItem()
        index = self.baseList.indexFromItem(current).row()
        current = self.baseList.takeItem(index)
        self.baseList.insertItem(index + 1, current)
        self.baseList.setCurrentItem(current)
        self.doModified()

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        if self.sender() == self.nameEdit:
            current = self.baseList.currentItem()
            current.setText(newText)
            data = current.data(QtCore.Qt.UserRole)
            data[1] = newText
            current.setData(QtCore.Qt.UserRole, data)
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            index = self.baseList.count()
            text = QtWidgets.QApplication.translate('main', 'New Record')
            item = QtWidgets.QListWidgetItem(text)
            item.setData(QtCore.Qt.UserRole, [index, text])
            self.baseList.addItem(item)
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'delete':
            if self.baseList.count() < 2:
                return
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.sender().setEnabled(self.baseList.count() > 1)
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setText(self.lastData[1])
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['commun'] = True
        self.parent.modifiedTables['commun']['periodes'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            lines.append((index, data[1]))
        query_commun, transactionOK_commun = utils_db.queryTransactionDB(
            self.main.db_commun)
        try:
            listCommands = [utils_db.qdf_table.format('periodes'), 
                            utils_db.listTablesCommun['periodes'][0]]
            query_commun = utils_db.queryExecute(listCommands, query=query_commun)
            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                'periodes', utils_db.listTablesCommun['periodes'][1])
            query_commun = utils_db.queryExecute(
                {commandLine: lines}, query=query_commun)
        finally:
            utils_db.endTransaction(
                query_commun, self.main.db_commun, transactionOK_commun)
        self.modified = False


class HoursDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer la liste des horaires.
    """
    def __init__(self, parent=None):
        super(HoursDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'config-etab'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []

        # la zone de sélection :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList,   1, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(self, layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # id :
        text = QtWidgets.QApplication.translate('main', 'Id:')
        idLabel = QtWidgets.QLabel(utils_functions.u('<b>{0}</b>').format(text))
        self.idEdit = QtWidgets.QLineEdit()
        self.idEdit.setValidator(QtGui.QIntValidator(self.idEdit))
        self.idEdit.editingFinished.connect(self.doEdited)
        # name :
        text = QtWidgets.QApplication.translate('main', 'Name:')
        nameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.editingFinished.connect(self.doEdited)
        # label :
        text = QtWidgets.QApplication.translate('main', 'Label:')
        labelLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.labelEdit = QtWidgets.QLineEdit()
        self.labelEdit.editingFinished.connect(self.doEdited)
        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,    1, 1)
        editGrid.addWidget(idLabel,         2, 0)
        editGrid.addWidget(self.idEdit,     2, 1)
        editGrid.addWidget(nameLabel,       3, 0)
        editGrid.addWidget(self.nameEdit,   3, 1)
        editGrid.addWidget(labelLabel,      4, 0)
        editGrid.addWidget(self.labelEdit,  4, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de commun.horaires :
        """
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.q_selectAllFromOrder.format('horaires', 'id')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            item = QtWidgets.QListWidgetItem(utils_functions.u(query_commun.value(1)))
            item.setData(
                QtCore.Qt.UserRole, 
                [int(query_commun.value(0)), query_commun.value(1), query_commun.value(2)])
            self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        self.lastData = current.data(QtCore.Qt.UserRole)
        actions = {
            self.idEdit: ('TEXT', 0),
            self.nameEdit: ('TEXT', 1),
            self.labelEdit: ('TEXT', 2),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 1)

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        if self.sender() == self.nameEdit:
            current.setText(newText)
            data[1] = newText
            current.setData(QtCore.Qt.UserRole, data)
        elif self.sender() == self.labelEdit:
            data[2] = newText
            current.setData(QtCore.Qt.UserRole, data)
        elif self.sender() == self.idEdit:
            # on vérifie que l'id est disponible :
            newId = int(newText)
            lastId = data[0]
            dejaLa = False
            currentIndex = self.baseList.indexFromItem(current).row()
            for index in range(self.baseList.count()):
                otherItem = self.baseList.item(index)
                otherData = otherItem.data(QtCore.Qt.UserRole)
                if (otherData[0] == newId) and (index != currentIndex):
                    dejaLa = True
            if dejaLa:
                msg = QtWidgets.QApplication.translate(
                    'main', 'This Id is already in use, choose another.')
                utils_functions.messageBox(self.main, level='warning', message=msg)
                self.idEdit.setText('{0}'.format(lastId))
                return
            data[0] = newId
            current.setData(QtCore.Qt.UserRole, data)
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            newIndex = self.baseList.count()
            # on cherche un id disponible :
            ids = []
            for index in range(self.baseList.count()):
                item = self.baseList.item(index)
                itemId = int(item.data(QtCore.Qt.UserRole)[0])
                ids.append(itemId)
            newId = 1
            while newId in ids:
                newId += 1
            text = QtWidgets.QApplication.translate('main', 'New Record')
            item = QtWidgets.QListWidgetItem(text)
            item.setData(QtCore.Qt.UserRole, [newId, text, text])
            self.baseList.addItem(item)
            self.baseList.setCurrentRow(newIndex)
            self.doModified()
        elif what == 'delete':
            if self.baseList.count() < 2:
                return
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.sender().setEnabled(self.baseList.count() > 1)
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setText(self.lastData[1])
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['commun'] = True
        self.parent.modifiedTables['commun']['horaires'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            lines.append((data[0], data[1], data[2]))
        query_commun, transactionOK_commun = utils_db.queryTransactionDB(
            self.main.db_commun)
        try:
            listCommands = [utils_db.qdf_table.format('horaires'), 
                            utils_db.listTablesCommun['horaires'][0]]
            query_commun = utils_db.queryExecute(listCommands, query=query_commun)
            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                'horaires', utils_db.listTablesCommun['horaires'][1])
            query_commun = utils_db.queryExecute(
                {commandLine: lines}, query=query_commun)
        finally:
            utils_db.endTransaction(
                query_commun, self.main.db_commun, transactionOK_commun)
        self.modified = False


class SubjectsDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer la liste des matières.
    """
    def __init__(self, parent=None):
        super(SubjectsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'config-etab'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []

        # la zone de sélection :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList, 1, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(self, layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # name :
        text = QtWidgets.QApplication.translate('main', 'Name:')
        nameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.editingFinished.connect(self.doEdited)
        # code :
        text = QtWidgets.QApplication.translate('main', 'Code:')
        codeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.codeEdit = QtWidgets.QLineEdit()
        self.codeEdit.editingFinished.connect(self.doEdited)
        # label :
        text = QtWidgets.QApplication.translate('main', 'Label:')
        labelLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.labelEdit = QtWidgets.QLineEdit()
        self.labelEdit.editingFinished.connect(self.doEdited)
        # ordreBLT :
        text = QtWidgets.QApplication.translate('main', 'OrdreBLT:')
        ordreBLTLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.ordreBLTSpinBox = QtWidgets.QSpinBox()
        self.ordreBLTSpinBox.setMinimum(0)
        self.ordreBLTSpinBox.setMaximum(100)
        self.ordreBLTSpinBox.valueChanged.connect(self.doChanged)

        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,            1, 1)
        editGrid.addWidget(nameLabel,               2, 0)
        editGrid.addWidget(self.nameEdit,           2, 1)
        editGrid.addWidget(codeLabel,               3, 0)
        editGrid.addWidget(self.codeEdit,           3, 1)
        editGrid.addWidget(labelLabel,              4, 0)
        editGrid.addWidget(self.labelEdit,          4, 1)
        editGrid.addWidget(ordreBLTLabel,           5, 0)
        editGrid.addWidget(self.ordreBLTSpinBox,    5, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de commun.matieres :
        """
        self.fromSoft = True
        query_commun = utils_db.query(self.main.db_commun)
        commandLines = (
            'SELECT * FROM matieres WHERE OrdreBLT>0 ORDER BY OrdreBLT, Matiere', 
            'SELECT * FROM matieres WHERE OrdreBLT=0 ORDER BY Matiere', 
            'SELECT * FROM matieres WHERE OrdreBLT<0 ORDER BY Matiere')
        i = 1
        for commandLine_commun in commandLines:
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                item = QtWidgets.QListWidgetItem(utils_functions.u(query_commun.value(1)))
                item.setData(
                    QtCore.Qt.UserRole, 
                    [int(query_commun.value(0)), 
                    query_commun.value(1), 
                    query_commun.value(2),
                    query_commun.value(3),
                    int(query_commun.value(4)),
                    query_commun.value(5)])
                self.baseList.addItem(item)
            if i < len(commandLines):
                i += 1
                item = QtWidgets.QListWidgetItem('_____________________')
                item.setData(QtCore.Qt.UserRole, 'SEPARATOR')
                item.setFlags(QtCore.Qt.NoItemFlags)
            self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        self.fromSoft = True
        self.lastData = current.data(QtCore.Qt.UserRole)
        actions = {
            self.nameEdit: ('TEXT', 1),
            self.codeEdit: ('TEXT', 2),
            self.labelEdit: ('TEXT', 3),
            self.ordreBLTSpinBox: ('INTEGER', 4),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'INTEGER':
                value = self.lastData[actions[action][1]]
                if value < 0:
                    action.setMinimum(-10)
                else:
                    action.setMinimum(0)
                action.setValue(value)
            action.setEnabled(self.lastData[4] > -1)
        self.fromSoft = False

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        if self.sender() == self.nameEdit:
            current.setText(newText)
            data[1] = newText
            current.setData(QtCore.Qt.UserRole, data)
        elif self.sender() == self.codeEdit:
            data[2] = newText
            current.setData(QtCore.Qt.UserRole, data)
        elif self.sender() == self.labelEdit:
            data[3] = newText
            current.setData(QtCore.Qt.UserRole, data)
        self.doModified()

    def doChanged(self, value):
        if self.fromSoft:
            return
        if self.sender() == self.ordreBLTSpinBox:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[4] = value
            current.setData(QtCore.Qt.UserRole, data)
            # TODO : réorganiser l'affichage
            newIndex = 0
            firstSeparator = -1
            lastSeparator = -1
            for index in range(self.baseList.count()):
                otherItem = self.baseList.item(index)
                otherData = otherItem.data(QtCore.Qt.UserRole)
                if otherData == 'SEPARATOR':
                    if firstSeparator < 0:
                        firstSeparator = index
                    else:
                        lastSeparator = index
            if value > 0:
                for index in range(firstSeparator):
                    otherItem = self.baseList.item(index)
                    otherData = otherItem.data(QtCore.Qt.UserRole)
                    if otherData[4] < data[4]:
                        newIndex = index
                    elif otherData[4] == data[4]:
                        if otherData[1] < data[1]:
                            newIndex = index
            else:
                for index in range(firstSeparator + 1, lastSeparator):
                    otherItem = self.baseList.item(index)
                    otherData = otherItem.data(QtCore.Qt.UserRole)
                    if otherData[1] < data[1]:
                        newIndex = index
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            if index > newIndex:
                newIndex += 1
            self.baseList.insertItem(newIndex, current)
            self.baseList.setCurrentItem(current)
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            index = self.baseList.count()
            text = QtWidgets.QApplication.translate('main', 'New Record')
            item = QtWidgets.QListWidgetItem(text)
            item.setData(QtCore.Qt.UserRole, [index, text, text, text, 0, ''])
            self.baseList.addItem(item)
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'delete':
            if self.baseList.count() < 2:
                return
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setText(self.lastData[1])
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            # on passe les séparateurs :
            while self.baseList.item(index).data(QtCore.Qt.UserRole) == 'SEPARATOR':
                index -= 1
                if index < 0:
                    index = self.baseList.count() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            # on passe les séparateurs :
            while self.baseList.item(index).data(QtCore.Qt.UserRole) == 'SEPARATOR':
                index += 1
                if index == self.baseList.count():
                    index = 0
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['commun'] = True
        self.parent.modifiedTables['commun']['matieres'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data != 'SEPARATOR':
                lines.append((index, data[1], data[2], data[3], data[4], data[5]))
        query_commun, transactionOK_commun = utils_db.queryTransactionDB(
            self.main.db_commun)
        try:
            listCommands = [utils_db.qdf_table.format('matieres'), 
                            utils_db.listTablesCommun['matieres'][0]]
            query_commun = utils_db.queryExecute(listCommands, query=query_commun)
            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                'matieres', utils_db.listTablesCommun['matieres'][1])
            query_commun = utils_db.queryExecute(
                {commandLine: lines}, query=query_commun)
        finally:
            utils_db.endTransaction(
                query_commun, self.main.db_commun, transactionOK_commun)
        self.modified = False


class OthersDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer d'autres trucs :
        limiteBLTPerso
        année scolaire
        Absences et retards
    """
    def __init__(self, parent=None):
        super(OthersDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'config-etab'
        # des modifications ont été faites :
        self.modified = False
        # données initiales :
        self.initialDatas = {}

        # limiteBLTPerso :Various
        text = QtWidgets.QApplication.translate(
            'main', 'Various')
        limiteBLTPersoGroup = QtWidgets.QGroupBox(text)
        text = QtWidgets.QApplication.translate(
            'main', 'limiteBLTPerso:')
        limiteBLTPersoLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.limiteBLTPersoSpinBox = QtWidgets.QSpinBox()
        self.limiteBLTPersoSpinBox.setMinimum(4)
        self.limiteBLTPersoSpinBox.setMaximum(100)
        text = QtWidgets.QApplication.translate(
            'main', 'limiteBLTPersoToolTip')
        limiteBLTPersoLabel.setToolTip(text)
        self.limiteBLTPersoSpinBox.setToolTip(text)
        grid = QtWidgets.QGridLayout()
        grid.addWidget(limiteBLTPersoLabel,            1, 0)
        grid.addWidget(self.limiteBLTPersoSpinBox,        1, 1)
        limiteBLTPersoGroup.setLayout(grid)

        # année scolaire etc :
        text = QtWidgets.QApplication.translate(
            'main', 'School year')
        anneeScolaireGroup = QtWidgets.QGroupBox(text)
        text = QtWidgets.QApplication.translate(
            'main', 'School year:')
        anneeScolaireLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.anneeScolaireSpinBox = QtWidgets.QSpinBox()
        self.anneeScolaireSpinBox.setMinimum(1800)
        self.anneeScolaireSpinBox.setMaximum(3000)
        text = QtWidgets.QApplication.translate(
            'main', 'Use the year to end of the school year.')
        anneeScolaireLabel.setToolTip(text)
        self.anneeScolaireSpinBox.setToolTip(text)
        text = QtWidgets.QApplication.translate(
            'main', 'School year title:')
        anneeScolaireTitleLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.anneeScolaireTitleEdit = QtWidgets.QLineEdit()
        grid = QtWidgets.QGridLayout()
        grid.addWidget(anneeScolaireLabel,            1, 0)
        grid.addWidget(self.anneeScolaireSpinBox,        1, 1)
        grid.addWidget(anneeScolaireTitleLabel,        2, 0)
        grid.addWidget(self.anneeScolaireTitleEdit,    2, 1)
        anneeScolaireGroup.setLayout(grid)

        # Absences et retards :
        shortNamesWidth = 200
        self.absencesWidgets = {
            0: [QtWidgets.QLabel(), QtWidgets.QLineEdit(), QtWidgets.QLineEdit()], 
            1: [QtWidgets.QLabel(), QtWidgets.QLineEdit(), QtWidgets.QLineEdit()], 
            2: [QtWidgets.QLabel(), QtWidgets.QLineEdit(), QtWidgets.QLineEdit()], 
            3: [QtWidgets.QLabel(), QtWidgets.QLineEdit(), QtWidgets.QLineEdit()], 
            }
        text = QtWidgets.QApplication.translate(
            'main', 'Absences and delays')
        absencesGroup = QtWidgets.QGroupBox(text)

        shortText = QtWidgets.QApplication.translate(
            'main', 'Justified absences')
        self.initialDatas['absences-00'] = (-1, shortText)
        self.absencesWidgets[0][0].setText(utils_functions.u(
            '<b>{0} :</b>').format(shortText))
        self.absencesWidgets[0][1].setText(shortText)
        self.absencesWidgets[0][1].setMaximumWidth(shortNamesWidth)
        longText = QtWidgets.QApplication.translate(
            'main', 'Number of half-days of justified absence')
        self.initialDatas['absences-01'] = (-1, longText)
        self.absencesWidgets[0][2].setText(longText)

        shortText = QtWidgets.QApplication.translate(
            'main', 'Unjustified absences')
        self.initialDatas['absences-10'] = (-1, shortText)
        self.absencesWidgets[1][0].setText(utils_functions.u(
            '<b>{0} :</b>').format(shortText))
        self.absencesWidgets[1][1].setText(shortText)
        self.absencesWidgets[1][1].setMaximumWidth(shortNamesWidth)
        longText = QtWidgets.QApplication.translate(
            'main', 'Number of half-days of unjustified absence')
        self.initialDatas['absences-11'] = (-1, longText)
        self.absencesWidgets[1][2].setText(longText)

        shortText = QtWidgets.QApplication.translate(
            'main', 'Delays')
        self.initialDatas['absences-20'] = (-1, shortText)
        self.absencesWidgets[2][0].setText(utils_functions.u(
            '<b>{0} :</b>').format(shortText))
        self.absencesWidgets[2][1].setText(shortText)
        self.absencesWidgets[2][1].setMaximumWidth(shortNamesWidth)
        longText = QtWidgets.QApplication.translate(
            'main', 'Number of delays')
        self.initialDatas['absences-21'] = (-1, longText)
        self.absencesWidgets[2][2].setText(longText)

        shortText = QtWidgets.QApplication.translate(
            'main', 'Hours missed')
        self.initialDatas['absences-30'] = (-1, shortText)
        self.absencesWidgets[3][0].setText(utils_functions.u(
            '<b>{0} :</b>').format(shortText))
        self.absencesWidgets[3][1].setText(shortText)
        self.absencesWidgets[3][1].setMaximumWidth(shortNamesWidth)
        longText = QtWidgets.QApplication.translate(
            'main', 'Number of classroom hours missed')
        self.initialDatas['absences-31'] = (-1, longText)
        self.absencesWidgets[3][2].setText(longText)

        grid = QtWidgets.QGridLayout()
        for i in range(4):
            for j in range(3):
                grid.addWidget(self.absencesWidgets[i][j], i, j)
        absencesGroup.setLayout(grid)

        # on agence tout ça :
        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(limiteBLTPersoGroup)
        vBoxLeft.addStretch(1)
        vBoxRight = QtWidgets.QVBoxLayout()
        vBoxRight.addWidget(anneeScolaireGroup)
        vBoxRight.addStretch(1)
        hBoxTop = QtWidgets.QHBoxLayout()
        hBoxTop.addLayout(vBoxLeft)
        hBoxTop.addLayout(vBoxRight)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(hBoxTop)
        mainLayout.addWidget(absencesGroup)
        mainLayout.addStretch(1)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit la base commun.config pour remplir les champs
        et conserver les valeurs initiales.
        """
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.q_selectAllFrom.format('config')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            self.initialDatas[query_commun.value(0)] = (
                query_commun.value(1), query_commun.value(2))
        # mise à jour de l'affichage :
        if 'limiteBLTPerso' in self.initialDatas:
            self.limiteBLTPersoSpinBox.setValue(
                self.initialDatas['limiteBLTPerso'][0])
        else:
            self.initialDatas['limiteBLTPerso'] = (-1, '')
            self.limiteBLTPersoSpinBox.setValue(4)

        if 'annee_scolaire' in self.initialDatas:
            anneeScolaire = self.initialDatas['annee_scolaire']
        else:
            self.initialDatas['annee_scolaire'] = (0, '')
            anneeScolaire = utils_functions.calcAnneeScolaire()
        self.anneeScolaireSpinBox.setValue(anneeScolaire[0])
        self.anneeScolaireTitleEdit.setText(anneeScolaire[1])

        for i in range(4):
            for j in range(2):
                self.absencesWidgets[i][j + 1].setText(
                    self.initialDatas['absences-{0}{1}'.format(i, j)][1])


    def createConnexions(self):
        lineEdits = (#QLineEdit
            self.anneeScolaireTitleEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.editingFinished.connect(self.doModified)
        for i in range(4):
            for j in range(2):
                self.absencesWidgets[i][j + 1].editingFinished.connect(self.doModified)
        spinBoxs = (#QSpinBox
            self.limiteBLTPersoSpinBox, 
            self.anneeScolaireSpinBox, 
            )
        for spinBox in spinBoxs:
            spinBox.valueChanged.connect(self.doModified)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        commandLines_commun = []
        commandLineInsertText = utils_functions.u(
            'INSERT INTO config VALUES ("{0}", "", "{1}")')
        commandLineInsertInteger = 'INSERT INTO config VALUES ("{0}", {1}, "")'
        commandLineInsertAll = utils_functions.u(
            'INSERT INTO config VALUES ("{0}", {1}, "{2}")')

        if self.limiteBLTPersoSpinBox.value() != self.initialDatas['limiteBLTPerso'][0]:
            self.parent.modifiedTables['commun']['config'] = True
            newInt = self.limiteBLTPersoSpinBox.value()
            commandLines_commun.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'limiteBLTPerso'),
                commandLineInsertInteger.format('limiteBLTPerso', newInt)])

        updateAnneeScolaire = False
        if self.anneeScolaireSpinBox.value() != self.initialDatas['annee_scolaire'][0]:
            updateAnneeScolaire = True
        elif self.anneeScolaireTitleEdit.isModified():
            updateAnneeScolaire = True
        if updateAnneeScolaire:
            self.parent.modifiedTables['commun']['config'] = True
            newInt = self.anneeScolaireSpinBox.value()
            newText = self.anneeScolaireTitleEdit.text()
            commandLines_commun.extend([
                utils_db.qdf_whereText.format('config', 'key_name', 'annee_scolaire'),
                commandLineInsertAll.format('annee_scolaire', newInt, newText)])

        for i in range(4):
            for j in range(2):
                lineEdit = self.absencesWidgets[i][j + 1]
                if lineEdit.isModified():
                    what = 'absences-{0}{1}'.format(i, j)
                    commandLines_commun.extend([
                        utils_db.qdf_whereText.format('config', 'key_name', what), 
                        commandLineInsertText.format(what, lineEdit.text())])

        if len(commandLines_commun) > 0:
            self.parent.modifiedTables['DATABASES']['commun'] = True
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
            try:
                query_commun = utils_db.queryExecute(
                    commandLines_commun, query=query_commun)
            finally:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
        self.modified = False


class ClassesDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer la liste des classes.
    """
    def __init__(self, parent=None):
        super(ClassesDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'config-classes'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []

        # la zone de sélection :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        upButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-up'), 
            QtWidgets.QApplication.translate('main', 'Up'))
        upButton.clicked.connect(self.doUp)
        downButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-down'), 
            QtWidgets.QApplication.translate('main', 'Down'))
        downButton.clicked.connect(self.doDown)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList,   1, 0)
        baseGrid.addWidget(upButton,        10, 0)
        baseGrid.addWidget(downButton,      11, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(self, layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # id :
        text = QtWidgets.QApplication.translate('main', 'Id:')
        idLabel = QtWidgets.QLabel(utils_functions.u('<b>{0}</b>').format(text))
        self.idEdit = QtWidgets.QLineEdit()
        self.idEdit.setValidator(QtGui.QIntValidator(self.idEdit))
        self.idEdit.editingFinished.connect(self.doEdited)
        # name :
        text = QtWidgets.QApplication.translate('main', 'Name:')
        nameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.editingFinished.connect(self.doEdited)
        # classType :
        text = QtWidgets.QApplication.translate(
            'main', 'Class type:')
        classTypeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.classTypeComboBox = QtWidgets.QComboBox()
        self.classTypeComboBox.currentIndexChanged.connect(self.doChanged)
        # notes :
        text = QtWidgets.QApplication.translate(
            'main', 'Class with notes:')
        notesLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.notesCheckBox = QtWidgets.QCheckBox()
        self.notesCheckBox.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Check this box if the class must have notes'))
        self.notesCheckBox.stateChanged.connect(self.doChanged)
        # désactivée :
        text = QtWidgets.QApplication.translate('main', 'Desabled:')
        desabledLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.desabledCheckBox = QtWidgets.QCheckBox()
        self.desabledCheckBox.setToolTip(QtWidgets.QApplication.translate(
            'main', 'ClassDesabledCheckBoxStatusTip'))
        self.desabledCheckBox.stateChanged.connect(self.doChanged)

        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,            1, 1)
        editGrid.addWidget(idLabel,                 2, 0)
        editGrid.addWidget(self.idEdit,             2, 1)
        editGrid.addWidget(nameLabel,               3, 0)
        editGrid.addWidget(self.nameEdit,           3, 1)
        editGrid.addWidget(classTypeLabel,          4, 0)
        editGrid.addWidget(self.classTypeComboBox,  4, 1)
        editGrid.addWidget(notesLabel,              5, 0)
        editGrid.addWidget(self.notesCheckBox,      5, 1)
        editGrid.addWidget(desabledLabel,           6, 0)
        editGrid.addWidget(self.desabledCheckBox,   6, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de commun.classes :
        """
        self.fromSoft = True
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.q_selectAllFromOrder.format(
            'classes', 'ordre, Classe')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            # si la classe est désactivée, l'ordre vaut 9999 :
            desabled = 0
            if int(query_commun.value(4)) == 9999:
                desabled = 1
            item = QtWidgets.QListWidgetItem(
                utils_functions.u(query_commun.value(1)))
            item.setData(
                QtCore.Qt.UserRole, 
                [int(query_commun.value(0)), 
                 query_commun.value(1), 
                 int(query_commun.value(2)),
                 int(query_commun.value(3)),
                 desabled])
            self.baseList.addItem(item)
        self.initializeComboBox()
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def initializeComboBox(self):
        # on remplit le comboBox via le dico tables :
        self.fromSoft = True
        self.classTypeComboBox.clear()
        for line in self.parent.tables['commun.classestypes']:
            self.classTypeComboBox.addItem(utils_functions.u(line[1]), line[0])
        try:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.classTypeComboBox.setCurrentIndex(data[2])
        except:
            pass
        self.fromSoft = False

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        self.fromSoft = True
        self.lastData = current.data(QtCore.Qt.UserRole)
        actions = {
            self.idEdit: ('TEXT', 0),
            self.nameEdit: ('TEXT', 1),
            self.classTypeComboBox: ('COMBOBOX', 2),
            self.notesCheckBox: ('CHECKBOX', 3),
            self.desabledCheckBox: ('CHECKBOX', 4),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'COMBOBOX':
                action.setCurrentIndex(self.lastData[actions[action][1]])
            elif actions[action][0] == 'CHECKBOX':
                action.setChecked(self.lastData[actions[action][1]] == 1)
            elif actions[action][0] == 'INTEGER':
                value = self.lastData[actions[action][1]]
                if value < 0:
                    action.setMinimum(-10)
                else:
                    action.setMinimum(0)
                action.setValue(value)
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 1)
        self.fromSoft = False

    def doUp(self):
        current = self.baseList.currentItem()
        index = self.baseList.indexFromItem(current).row()
        current = self.baseList.takeItem(index)
        self.baseList.insertItem(index - 1, current)
        self.baseList.setCurrentItem(current)
        self.doModified()

    def doDown(self):
        current = self.baseList.currentItem()
        index = self.baseList.indexFromItem(current).row()
        current = self.baseList.takeItem(index)
        self.baseList.insertItem(index + 1, current)
        self.baseList.setCurrentItem(current)
        self.doModified()

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        if self.sender() == self.nameEdit:
            current.setText(newText)
            data[1] = newText
            current.setData(QtCore.Qt.UserRole, data)
            self.doModified()
        elif self.sender() == self.idEdit:
            # on vérifie que l'id est disponible :
            newId = int(newText)
            lastId = data[0]
            dejaLa = False
            currentIndex = self.baseList.indexFromItem(current).row()
            for index in range(self.baseList.count()):
                otherItem = self.baseList.item(index)
                otherData = otherItem.data(QtCore.Qt.UserRole)
                if (otherData[0] == newId) and (index != currentIndex):
                    dejaLa = True
            if dejaLa:
                msg = QtWidgets.QApplication.translate(
                    'main', 'This Id is already in use, choose another.')
                utils_functions.messageBox(self.main, level='warning', message=msg)
                self.idEdit.setText('{0}'.format(lastId))
                return
            data[0] = newId
            current.setData(QtCore.Qt.UserRole, data)
            self.doModified()

    def doChanged(self, value):
        if self.fromSoft:
            return
        if self.sender() == self.classTypeComboBox:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[2] = value
            current.setData(QtCore.Qt.UserRole, data)
        elif self.sender() == self.notesCheckBox:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if value == 2:
                data[3] = 1
            else:
                data[3] = 0
            current.setData(QtCore.Qt.UserRole, data)
        elif self.sender() == self.desabledCheckBox:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if value == 2:
                data[4] = 1
            else:
                data[4] = 0
            current.setData(QtCore.Qt.UserRole, data)
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            newIndex = self.baseList.count()
            # on cherche un id disponible :
            ids = []
            for index in range(self.baseList.count()):
                item = self.baseList.item(index)
                itemId = int(item.data(QtCore.Qt.UserRole)[0])
                ids.append(itemId)
            newId = 1
            while newId in ids:
                newId += 1
            text = QtWidgets.QApplication.translate('main', 'New Record')
            item = QtWidgets.QListWidgetItem(text)
            item.setData(QtCore.Qt.UserRole, [newId, text, 0, 0, 0])
            self.baseList.addItem(item)
            self.baseList.setCurrentRow(newIndex)
            self.doModified()
        elif what == 'delete':
            if self.baseList.count() < 2:
                return
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setText(self.lastData[1])
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['commun'] = True
        self.parent.modifiedTables['commun']['classes'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            order = index
            if data[4] == 1:
                order = 9999
            lines.append((data[0], data[1], data[2], data[3], order))
        query_commun, transactionOK_commun = utils_db.queryTransactionDB(
            self.main.db_commun)
        try:
            listCommands = [
                utils_db.qdf_table.format('classes'), 
                utils_db.listTablesCommun['classes'][0]]
            query_commun = utils_db.queryExecute(listCommands, query=query_commun)
            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                'classes', utils_db.listTablesCommun['classes'][1])
            query_commun = utils_db.queryExecute(
                {commandLine: lines}, query=query_commun)
        finally:
            utils_db.endTransaction(
                query_commun, self.main.db_commun, transactionOK_commun)
        self.modified = False


class ClassesTypesDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer les types de classes.
    """
    def __init__(self, parent=None):
        super(ClassesTypesDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'config-classes'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []

        # la zone de sélection :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        upButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-up'), 
            QtWidgets.QApplication.translate('main', 'Up'))
        upButton.clicked.connect(self.doUp)
        downButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-down'), 
            QtWidgets.QApplication.translate('main', 'Down'))
        downButton.clicked.connect(self.doDown)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList,   1, 0)
        baseGrid.addWidget(upButton,        10, 0)
        baseGrid.addWidget(downButton,      11, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(self, layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # name :
        text = QtWidgets.QApplication.translate('main', 'Name:')
        nameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.nameEdit = QtWidgets.QLineEdit()
        self.nameEdit.editingFinished.connect(self.doEdited)
        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,    1, 1)
        editGrid.addWidget(nameLabel,       2, 0)
        editGrid.addWidget(self.nameEdit,   2, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de commun.classestypes dans le dico tables :
        """
        for line in self.parent.tables['commun.classestypes']:
            item = QtWidgets.QListWidgetItem(utils_functions.u(line[1]))
            item.setData(QtCore.Qt.UserRole, [line[0], line[1]])
            self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        self.lastData = current.data(QtCore.Qt.UserRole)
        actions = {
            self.nameEdit: ('TEXT', 1),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 1)

    def doUp(self):
        current = self.baseList.currentItem()
        index = self.baseList.indexFromItem(current).row()
        current = self.baseList.takeItem(index)
        self.baseList.insertItem(index - 1, current)
        self.baseList.setCurrentItem(current)
        self.doModified()
        self.updateParentTables()

    def doDown(self):
        current = self.baseList.currentItem()
        index = self.baseList.indexFromItem(current).row()
        current = self.baseList.takeItem(index)
        self.baseList.insertItem(index + 1, current)
        self.baseList.setCurrentItem(current)
        self.doModified()
        self.updateParentTables()

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        if self.sender() == self.nameEdit:
            current = self.baseList.currentItem()
            current.setText(newText)
            data = current.data(QtCore.Qt.UserRole)
            data[1] = newText
            current.setData(QtCore.Qt.UserRole, data)
        self.doModified()
        self.updateParentTables()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            index = self.baseList.count()
            text = QtWidgets.QApplication.translate('main', 'New Record')
            item = QtWidgets.QListWidgetItem(text)
            item.setData(QtCore.Qt.UserRole, [index, text])
            self.baseList.addItem(item)
            self.baseList.setCurrentRow(index)
            self.doModified()
            self.updateParentTables()
        elif what == 'delete':
            if self.baseList.count() < 2:
                return
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.sender().setEnabled(self.baseList.count() > 1)
            self.doModified()
            self.updateParentTables()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setText(self.lastData[1])
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def updateParentTables(self):
        self.parent.tables['commun.classestypes'] = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            self.parent.tables['commun.classestypes'].append((index, data[1]))

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['commun'] = True
        self.parent.modifiedTables['commun']['classestypes'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            lines.append((index, data[1]))
        query_commun, transactionOK_commun = utils_db.queryTransactionDB(
            self.main.db_commun)
        try:
            listCommands = [utils_db.qdf_table.format('classestypes'), 
                            utils_db.listTablesCommun['classestypes'][0]]
            query_commun = utils_db.queryExecute(listCommands, query=query_commun)
            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                'classestypes', utils_db.listTablesCommun['classestypes'][1])
            query_commun = utils_db.queryExecute(
                {commandLine: lines}, query=query_commun)
        finally:
            utils_db.endTransaction(
                query_commun, self.main.db_commun, transactionOK_commun)
        self.modified = False





###########################################################"
#   COMPÉTENCES PARTAGÉES
#   SHARED COMPETENCES
###########################################################"

class CptDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer une liste de compétences (bulletin, référentiel, ...).
    """
    def __init__(self, parent=None, tableName=''):
        super(CptDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'define-cpt'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []
        # la base correspondant à la liste ('bulletin', ...)
        self.tableName = tableName
        # l'indentation de l'affichage en fonction du niveau :
        espace = '&nbsp;'
        self.indentations = {0:'', 1:espace * 10, 2:espace * 20, 3:espace * 30}
        # pour empêcher le focus sur un bouton (add, et) lors de la touche entrée :
        self.noFocus = False
        # pour ne pas lancer l'édition 2 fois :
        self.inDoEdited = False

        # la zone de sélection :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList,   1, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(
            self, 
            buttons=('add', 'insert', 'delete', 'cancel', 'previous', 'next'), 
            layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # code :
        text = QtWidgets.QApplication.translate('main', 'Code:')
        codeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.t1Edit = QtWidgets.QLineEdit()
        self.t2Edit = QtWidgets.QLineEdit()
        self.t3Edit = QtWidgets.QLineEdit()
        self.cptEdit = QtWidgets.QLineEdit()
        for edit in (self.t1Edit, self.t2Edit, self.t3Edit, self.cptEdit):
            edit.setMaximumWidth(100)
            edit.editingFinished.connect(self.doEdited)
        codeLayout = QtWidgets.QHBoxLayout()
        codeLayout.addWidget(self.t1Edit)
        codeLayout.addWidget(self.t2Edit)
        codeLayout.addWidget(self.t3Edit)
        codeLayout.addWidget(self.cptEdit)
        # le checkBox pour définir les sous-rubriques :
        text = QtWidgets.QApplication.translate('main', 'SubHeading')
        self.subHeadingCheckBox = QtWidgets.QCheckBox(text)
        self.subHeadingCheckBox.setToolTip(QtWidgets.QApplication.translate(
            'main', 'SubHeadingCheckBoxStatusTip'))
        self.subHeadingCheckBox.stateChanged.connect(self.doChanged)
        if self.tableName in ('bulletin', 'referentiel', 'confidentiel'):
            codeLayout.addWidget(self.subHeadingCheckBox)
        codeLayout.addStretch(1)
        # classType :
        text = QtWidgets.QApplication.translate('main', 'Class type:')
        classTypeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.classTypeComboBox = QtWidgets.QComboBox()
        self.classTypeComboBox.currentIndexChanged.connect(self.doChanged)
        # label :
        text = QtWidgets.QApplication.translate('main', 'Label:')
        labelLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.labelEdit = QtWidgets.QLineEdit()
        self.labelEdit.editingFinished.connect(self.doEdited)
        # conseil donné aux élèves et son bouton d'édition :
        text = QtWidgets.QApplication.translate('main', 'Advice:')
        adviceLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.adviceEdit = QtWidgets.QLineEdit()
        self.adviceEdit.editingFinished.connect(self.doEdited)
        self.adviceEditButton = QtWidgets.QPushButton(
            utils.doIcon('comments'),
            QtWidgets.QApplication.translate('main', 'Edit'))
        self.adviceEditButton.setToolTip(
            QtWidgets.QApplication.translate('main', 'AdviceEditStatusTip'))
        self.adviceEditButton.clicked.connect(self.adviceEditWysiwyg)
        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,            1, 1, 1, 4)
        editGrid.addWidget(codeLabel,               2, 0)
        editGrid.addLayout(codeLayout,              2, 1)
        editGrid.addWidget(classTypeLabel,          2, 2)
        editGrid.addWidget(self.classTypeComboBox,  2, 3)
        editGrid.addWidget(labelLabel,              3, 0)
        editGrid.addWidget(self.labelEdit,          3, 1, 1, 3)
        editGrid.addWidget(adviceLabel,            4, 0)
        editGrid.addWidget(self.adviceEdit,        4, 1, 1, 2)
        editGrid.addWidget(self.adviceEditButton,  4, 3)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(editGrid, 2, 0)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self, dataList=[]):
        """
        lecture de commun.table (table étant bulletin, referentiel, ...).
        On ne récupère pas la colonne ordre dans data, car elle sera 
        reconstruite à partir de l'ordre d'affichage des items.
        On remplit aussi le comboBox des types de classes
        avec 2 entrées supplémentaires (nowhere et everywhere).
        Si la liste dataList n'est pas vide, c'est qu'on appelle
        depuis un import de modèle.
        """
        self.fromSoft = True
        if len(dataList) > 0:
            self.baseList.clear()
            for cpt in dataList:
                code = cpt[1]
                titre1 = cpt[2]
                titre2 = cpt[3]
                titre3 = cpt[4]
                competence = cpt[5]
                data = [
                    int(cpt[0]), 
                    code, 
                    titre1, 
                    titre2, 
                    titre3, 
                    competence, 
                    cpt[6], 
                    cpt[7], 
                    cpt[8], 
                    cpt[9], 
                    False, 
                    cpt[10], 
                    int(cpt[11]), 
                    0]
                if self.tableName in admin.listeSousRubriques:
                    if code in admin.listeSousRubriques[self.tableName]:
                        data[10] = True
                level = self.getLevel(data)
                data[13] = level
                item = QtWidgets.QListWidgetItem('')
                item.setData(QtCore.Qt.UserRole, data)
                self.baseList.addItem(item)
                if data[10]:
                    title = '{0}<font color="blue">{1}</font> : <b>{2}{3}{4}{5}</b>'
                else:
                    title = '{0}<font color="blue">{1}</font> : {2}{3}{4}{5}'
                title = utils_functions.u(title).format(
                    self.indentations[level], code, titre1, titre2, titre3, competence)
                self.baseList.setItemWidget(item, QtWidgets.QLabel(title))
            self.doModified()
        else:
            query_commun = utils_db.query(self.main.db_commun)
            commandLine_commun = utils_db.q_selectAllFromOrder.format(self.tableName, 'ordre')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                code = query_commun.value(1)
                titre1 = query_commun.value(2)
                titre2 = query_commun.value(3)
                titre3 = query_commun.value(4)
                competence = query_commun.value(5)
                data = [
                    int(query_commun.value(0)),
                    code,
                    titre1,
                    titre2,
                    titre3,
                    competence,
                    query_commun.value(6),
                    query_commun.value(7),
                    query_commun.value(8),
                    query_commun.value(9),
                    False, 
                    query_commun.value(10),
                    int(query_commun.value(11)),
                    0]
                if self.tableName in admin.listeSousRubriques:
                    if code in admin.listeSousRubriques[self.tableName]:
                        data[10] = True
                level = self.getLevel(data)
                data[13] = level
                item = QtWidgets.QListWidgetItem('')
                item.setData(QtCore.Qt.UserRole, data)
                self.baseList.addItem(item)
                if data[10]:
                    title = '{0}<font color="blue">{1}</font> : <b>{2}{3}{4}{5}</b>'
                else:
                    title = '{0}<font color="blue">{1}</font> : {2}{3}{4}{5}'
                title = utils_functions.u(title).format(
                    self.indentations[level], code, titre1, titre2, titre3, competence)
                self.baseList.setItemWidget(item, QtWidgets.QLabel(title))
            # on remplit le comboBox classestypes :
            self.classTypeComboBox.addItem(
                QtWidgets.QApplication.translate('main', 'nowhere'), -2)
            self.classTypeComboBox.addItem(
                QtWidgets.QApplication.translate('main', 'everywhere'), -1)
            commandLine_commun = utils_db.q_selectAllFrom.format('classestypes')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                self.classTypeComboBox.addItem(
                    utils_functions.u(query_commun.value(1)), int(query_commun.value(0)))
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def verifyAll(self):
        """
        vérifie la cohérence de la hiérarchie et répare si besoin.
        """
        level = 10
        d = ['', '', '']
        for i in range(self.baseList.count()):
            item = self.baseList.item(i)
            data = item.data(QtCore.Qt.UserRole)
            newLevel = data[13]
            mustUpdate = False
            if newLevel == 0:
                for j in range(3):
                    if data[j + 7] != '':
                        data[j + 7] = ''
                        mustUpdate = True
            elif newLevel == 1:
                if data[6] != d[0]:
                    data[6] = d[0]
                    mustUpdate = True
                for j in range(2):
                    if data[j + 8] != '':
                        data[j + 8] = ''
                        mustUpdate = True
            elif newLevel == 2:
                for j in range(2):
                    if data[j + 6] != d[j]:
                        data[j + 6] = d[j]
                        mustUpdate = True
                if data[9] != '':
                    data[9] = ''
                    mustUpdate = True
            else:
                for j in range(3):
                    if data[j + 6] != d[j]:
                        data[j + 6] = d[j]
                        mustUpdate = True
                newLevel -= 1
            d = [data[6], data[7], data[8]]
            level = newLevel
            if mustUpdate:
                item.setData(QtCore.Qt.UserRole, data)
                self.updateItem(item)
                self.doModified()

    def getLevel(self, data):
        """
        calcul du niveau d'une ligne.
        """
        if data[9] != '':
            level = 3
        elif data[8] != '':
            level = 2
        elif data[7] != '':
            level = 1
        else:
            level = 0
        return level

    def updateItem(self, item):
        """
        mise à jour du code et de l'affichage d'un item.
        """
        data = item.data(QtCore.Qt.UserRole)
        data[1] = utils_functions.u('{0}{1}{2}{3}').format(
            data[6], data[7], data[8], data[9])
        item.setData(QtCore.Qt.UserRole, data)
        if data[10]:
            title = '{0}<font color="blue">{1}</font> : <b>{2}{3}{4}{5}</b>'
        else:
            title = '{0}<font color="blue">{1}</font> : {2}{3}{4}{5}'
        title = utils_functions.u(title).format(
            self.indentations[data[13]], data[1], data[2], data[3], data[4], data[5])
        self.baseList.itemWidget(item).setText(title)

    def updateEnabled(self, level):
        for edit in (self.t1Edit, self.t2Edit, self.t3Edit, self.cptEdit):
            edit.setEnabled(True)
        if level == 3:
            self.t1Edit.setEnabled(False)
            self.t2Edit.setEnabled(False)
            self.t3Edit.setEnabled(False)
        elif level == 2:
            self.t1Edit.setEnabled(False)
            self.t2Edit.setEnabled(False)
        elif level == 1:
            self.t1Edit.setEnabled(False)
        self.labelEdit.setText(self.lastData[level + 2])

    def getChildren(self, item):
        """
        retourne la liste des enfants d'un item.
        """
        children = []
        data = item.data(QtCore.Qt.UserRole)
        level = data[13]
        for i in range(self.baseList.count()):
            other = self.baseList.item(i)
            otherData = other.data(QtCore.Qt.UserRole)
            if otherData == data:
                index = i
                break
        childLevel = level + 1
        while childLevel > level:
            index += 1
            if index == self.baseList.count():
                return children
            child = self.baseList.item(index)
            childLevel = child.data(QtCore.Qt.UserRole)[13]
            if childLevel > level:
                children.append(child)
        return children

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        On calcule aussi le niveau pour savoir quels Edit sont actifs.
        """
        try:
            self.lastData = current.data(QtCore.Qt.UserRole)
        except:
            return
        self.fromSoft = True
        actions = {
            self.t1Edit: ('TEXT', 6),
            self.t2Edit: ('TEXT', 7),
            self.t3Edit: ('TEXT', 8),
            self.cptEdit: ('TEXT', 9),
            self.subHeadingCheckBox: ('CHECKBOX', 10),
            self.adviceEdit: ('TEXT', 11),
            self.classTypeComboBox: ('COMBOBOX', 12),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'CHECKBOX':
                action.setChecked(self.lastData[actions[action][1]] == 1)
            elif actions[action][0] == 'COMBOBOX':
                action.setCurrentIndex(self.lastData[actions[action][1]] + 2)
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 1)
        # gestion du niveau (titre ou cpt) :
        level = self.lastData[13]
        self.updateEnabled(level)
        self.fromSoft = False

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        if self.inDoEdited:
            return
        self.inDoEdited = True
        mustVerifyAll = False
        try:
            sender = self.sender()
            # pour relier l'Edit au data correspondant :
            edit2data = {
                self.t1Edit: 6,
                self.t2Edit: 7,
                self.t3Edit: 8,
                self.cptEdit: 9,
                self.labelEdit: -1,
                self.adviceEdit: 11}
            if not sender in edit2data:
                return
            # on récupère les données :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            children = self.getChildren(current)
            index = self.baseList.indexFromItem(current).row()
            # on calcule edit2data[self.labelEdit] :
            oldLevel = data[13]
            if sender == self.labelEdit:
                edit2data[sender] = oldLevel + 2
            oldText = data[edit2data[sender]]
            newText = sender.text()
            if newText == oldText:
                # inutile d'aller plus loin si rien n'a été modifié
                return
            # quelques cas particuliers :
            if sender == self.t1Edit:
                # T1 ne peut pas être vide :
                if newText == '':
                    msg = QtWidgets.QApplication.translate(
                        'main', 'The code of the first level must not be empty.')
                    utils_functions.messageBox(self.main, level='warning', message=msg)
                    self.t1Edit.setText(data[6])
                    return
            elif index == 0:
                # Le premier item de la liste doit rester de premier niveau :
                if (newText != '') and (sender in(self.t2Edit, self.t3Edit, self.cptEdit)):
                    msg = QtWidgets.QApplication.translate(
                        'main', 'The first item in the list must remain the first level.')
                    utils_functions.messageBox(self.main, level='warning', message=msg)
                    sender.setText('')
                    return
            # on met data à jour :
            dataIndex = edit2data[sender]
            data[dataIndex] = newText
            if sender in (self.t1Edit, self.t2Edit, self.t3Edit):
                # on répercute le changement aux enfants :
                for child in children:
                    childData = child.data(QtCore.Qt.UserRole)
                    childData[dataIndex] = newText
                    child.setData(QtCore.Qt.UserRole, childData)
            level = self.getLevel(data)
            if level != oldLevel:
                # le niveau a changé :
                mustVerifyAll = True
                data[13] = level
                # on décale le label :
                data[level + 2] = data[oldLevel + 2]
                data[oldLevel + 2] = ''
                self.updateEnabled(level)
                self.labelEdit.setText(data[level + 2])
            # on met l'item à jour :
            current.setData(QtCore.Qt.UserRole, data)
            self.updateItem(current)
            # on met les enfants à jour :
            for child in children:
                self.updateItem(child)
            if mustVerifyAll:
                self.verifyAll()
                self.doCurrentItemChanged(current, current)
            self.doModified()
        finally:
            self.inDoEdited = False
            self.noFocus = True
            QtCore.QTimer.singleShot(10, self.doFocus)

    def doFocus(self):
        """
        permet aux boutons (add etc) de reprendre le focus.
        """
        self.noFocus = False

    def adviceEditWysiwyg(self):
        if self.noFocus:
            # pour redonner le focus (au cas où)
            self.noFocus = False
            return
        import utils_htmleditor
        dialog = utils_htmleditor.HtmlEditorDlg(
            parent=self.main, content=self.adviceEdit.text())
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        advice = dialog.webViewWidget.webView.toHtml()
        advice = utils_htmleditor.formatAdvice(advice)
        #self.adviceEdit.setText(advice)
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        data[11] = advice
        current.setData(QtCore.Qt.UserRole, data)
        self.updateItem(current)
        self.doModified()

    def doChanged(self, value):
        if self.fromSoft:
            return
        if self.sender() == self.classTypeComboBox:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[12] = value - 2
            current.setData(QtCore.Qt.UserRole, data)
        elif self.sender() == self.subHeadingCheckBox:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if value == 2:
                data[10] = True
            else:
                data[10] = False
            current.setData(QtCore.Qt.UserRole, data)
            self.updateItem(current)
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        if self.noFocus:
            # pour redonner le focus (au cas où)
            self.noFocus = False
            return
        what = self.sender().objectName()
        if what == 'add':
            # on cherche un id disponible :
            ids = []
            for index in range(self.baseList.count()):
                item = self.baseList.item(index)
                itemId = int(item.data(QtCore.Qt.UserRole)[0])
                ids.append(itemId)
            newId = 1
            while newId in ids:
                newId += 1
            # on cherche la position et le niveau à utiliser 
            # (à priori le nouvel item sera un fils de celui qui est sélectionné) :
            last = self.baseList.currentItem()
            index = self.baseList.indexFromItem(last).row() + 1
            lastData = last.data(QtCore.Qt.UserRole)
            level = self.getLevel(lastData) + 1
            # on crée les données :
            code = '??'
            text = QtWidgets.QApplication.translate('main', 'New Record')
            data = [newId, '', '', '', '', '', '', '', '', '', False, '', -1, level]
            if level == 1:
                data[3] = text
                data[6] = lastData[6]
                data[7] = code
            elif level == 2:
                data[4] = text
                data[6] = lastData[6]
                data[7] = lastData[7]
                data[8] = code
            elif level == 3:
                data[5] = text
                data[6] = lastData[6]
                data[7] = lastData[7]
                data[8] = lastData[8]
                data[9] = code
            else:
                # pas de fils pour une compétence !
                # On crée un item du même niveau :
                data[5] = text
                data[6] = lastData[6]
                data[7] = lastData[7]
                data[8] = lastData[8]
                data[9] = code
                data[13] = level - 1
            # on crée enfin l'item :
            item = QtWidgets.QListWidgetItem('')
            item.setData(QtCore.Qt.UserRole, data)
            self.baseList.insertItem(index, item)
            self.baseList.setItemWidget(item, QtWidgets.QLabel(''))
            self.updateItem(item)
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'insert':
            # on cherche un id disponible :
            ids = []
            for index in range(self.baseList.count()):
                item = self.baseList.item(index)
                itemId = int(item.data(QtCore.Qt.UserRole)[0])
                ids.append(itemId)
            newId = 1
            while newId in ids:
                newId += 1
            # on cherche la position et le niveau à utiliser 
            # (à priori le nouvel item sera un frère de celui qui est sélectionné) :
            last = self.baseList.currentItem()
            index = self.baseList.indexFromItem(last).row()
            lastData = last.data(QtCore.Qt.UserRole)
            level = self.getLevel(lastData)
            # on crée les données :
            code = '??'
            text = QtWidgets.QApplication.translate('main', 'New Record')
            data = [newId, '', '', '', '', '', '', '', '', '', False, '', -1, level]
            if level == 0:
                data[2] = text
                data[6] = code
            elif level == 1:
                data[3] = text
                data[6] = lastData[6]
                data[7] = code
            elif level == 2:
                data[4] = text
                data[6] = lastData[6]
                data[7] = lastData[7]
                data[8] = code
            elif level == 3:
                data[5] = text
                data[6] = lastData[6]
                data[7] = lastData[7]
                data[8] = lastData[8]
                data[9] = code
            else:
                data[5] = text
                data[6] = lastData[6]
                data[7] = lastData[7]
                data[8] = lastData[8]
                data[9] = code
            # on crée enfin l'item :
            item = QtWidgets.QListWidgetItem('')
            item.setData(QtCore.Qt.UserRole, data)
            self.baseList.insertItem(index, item)
            self.baseList.setItemWidget(item, QtWidgets.QLabel(''))
            self.updateItem(item)
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'delete':
            # on récupère les données :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            level = data[13]
            children = self.getChildren(current)
            index = self.baseList.indexFromItem(current).row()
            # Le premier item ne peut pas être supprimé :
            if index == 0:
                msg = QtWidgets.QApplication.translate(
                    'main', 'The first item can not be deleted.')
                utils_functions.messageBox(self.main, level='warning', message=msg)
                return
            if len(children) > 0:
                # on demande s'il faut aussi supprimer les enfants :
                msg = QtWidgets.QApplication.translate(
                    'main', 'Should also remove sub-items?')
                reply = utils_functions.messageBox(
                    self.main, level='question', message=msg, buttons=['Yes', 'No'])
                if reply == QtWidgets.QMessageBox.Yes:
                    for child in children:
                        childIndex = self.baseList.indexFromItem(child).row()
                        toDelete = self.baseList.takeItem(childIndex)
                        del(toDelete)
                else:
                    # on répercute le changement aux enfants :
                    for child in children:
                        childData = child.data(QtCore.Qt.UserRole)
                        childData[level + 6] = ''
                        child.setData(QtCore.Qt.UserRole, childData)
                        self.updateItem(child)
            # on supprime l'item :
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.verifyAll()
            self.baseList.setCurrentRow(index)
            self.sender().setEnabled(self.baseList.count() > 1)
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.updateItem(current)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['commun'] = True
        self.parent.modifiedTables['commun'][self.tableName] = True
        mustUpdateSousRubriques = (self.tableName in ('bulletin', 'referentiel', 'confidentiel'))
        if mustUpdateSousRubriques:
            self.parent.modifiedTables['commun']['sous_rubriques'] = True
            sousRubriques = []
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            lines.append((data[0], data[1], data[2], data[3], data[4], 
                          data[5], data[6], data[7], data[8], data[9], 
                          data[11], data[12], index))
            if mustUpdateSousRubriques:
                if data[10]:
                    if not(data[1] in sousRubriques):
                        sousRubriques.append(data[1])
        query_commun, transactionOK_commun = utils_db.queryTransactionDB(
            self.main.db_commun)
        try:
            listCommands = [utils_db.qdf_table.format(self.tableName), 
                            utils_db.listTablesCommun[self.tableName][0]]
            query_commun = utils_db.queryExecute(listCommands, query=query_commun)
            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                self.tableName, utils_db.listTablesCommun[self.tableName][1])
            query_commun = utils_db.queryExecute(
                {commandLine: lines}, query=query_commun)
            if mustUpdateSousRubriques:
                # récupération des codes des sous-rubriques du bulletin (partie partagée) :
                commandLine = utils_db.q_selectAllFrom.format('sous_rubriques')
                query_commun = utils_db.queryExecute(commandLine, query=query_commun)
                toWhat = {
                    2: ('bulletin', utils.decalageBLT), 
                    3: ('referentiel', 0), 
                    4: ('confidentiel', utils.decalageCFD)}
                listeSousRubriques = {'bulletin': [], 'referentiel': [], 'confidentiel': []}
                while query_commun.next():
                    code = query_commun.value(1)
                    for i in toWhat:
                        tableName = toWhat[i][0]
                        value = query_commun.value(i)
                        if (value != None) and (value != ''):
                            if not(code in listeSousRubriques[tableName]):
                                listeSousRubriques[tableName].append(code)
                listeSousRubriques[self.tableName] = []
                for code in sousRubriques:
                    listeSousRubriques[self.tableName].append(code)
                lines = []
                for i in toWhat:
                    sousRubriquesIndex = 0
                    decalage = toWhat[i][1]
                    tableName = toWhat[i][0]
                    for code in listeSousRubriques[tableName]:
                        if tableName == 'bulletin':
                            lines.append((decalage + sousRubriquesIndex, code, 'x', '', ''))
                        elif tableName == 'referentiel':
                            lines.append((decalage + sousRubriquesIndex, code, '', 'x', ''))
                        elif tableName == 'confidentiel':
                            lines.append((decalage + sousRubriquesIndex, code, '', '', 'x'))
                        sousRubriquesIndex += 1
                listCommands = [utils_db.qdf_table.format('sous_rubriques'), 
                                utils_db.listTablesCommun['sous_rubriques'][0]]
                query_commun = utils_db.queryExecute(listCommands, query=query_commun)
                # on écrit les lignes :
                commandLine = utils_db.insertInto(
                    'sous_rubriques', utils_db.listTablesCommun['sous_rubriques'][1])
                query_commun = utils_db.queryExecute(
                    {commandLine: lines}, query=query_commun)

        finally:
            utils_db.endTransaction(
                query_commun, self.main.db_commun, transactionOK_commun)
        self.modified = False

class CptUtilsDlg(QtWidgets.QDialog):
    """
    les outils de la page users.
    """
    def __init__(self, parent=None):
        super(CptUtilsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'cpt-modeles'
        # des modifications ont été faites :
        self.modified = False

        # les messages :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)

        # récupération d'un modèle de compétences :
        templatesTitle = QtWidgets.QApplication.translate(
            'main', 
            'Choose a list of shared competences '
            'among the proposed models')
        templatesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(templatesTitle)
        templatesLabel = QtWidgets.QLabel(templatesTitle)
        self.schoolReportTemplate_Button = QtWidgets.QToolButton()
        self.referentielTemplate_Button = QtWidgets.QToolButton()
        self.confidentielTemplate_Button = QtWidgets.QToolButton()
        self.suiviTemplate_Button = QtWidgets.QToolButton()

        # mise en forme des boutons :
        buttons = {
            self.schoolReportTemplate_Button: (
                'bulletin', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'School report'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Select competences of the school report list '
                    'among the proposed models'), 
                self.chooseSchoolReportTemplate), 
            self.referentielTemplate_Button: (
                'referentiel', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Referential'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Select referential among the proposed models'), 
                self.chooseReferentielTemplate), 
            self.confidentielTemplate_Button: (
                'confidentiel', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Confidential competences'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Select confidential competences '
                    'among the proposed models'), 
                self.chooseConfidentielTemplate), 
            self.suiviTemplate_Button: (
                'suivis', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Follow'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Select followed competences '
                    'among the proposed models'), 
                self.chooseSuiviTemplate), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize'] * 2
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])

        # séries d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.schoolReportTemplate_Button)
        hLayout0.addWidget(self.referentielTemplate_Button)
        hLayout0.addStretch(1)
        hLayout1 = QtWidgets.QHBoxLayout()
        hLayout1.addWidget(self.confidentielTemplate_Button)
        hLayout1.addWidget(self.suiviTemplate_Button)
        hLayout1.addStretch(1)

        # mise en place :
        self.statusBar = QtWidgets.QStatusBar()
        labelSeparator = QtWidgets.QLabel('<hr width="80%">')
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(templatesLabel)
        vLayout.addLayout(hLayout0)
        vLayout.addLayout(hLayout1)
        vLayout.addStretch(1)
        grid = QtWidgets.QHBoxLayout()
        grid.addLayout(vLayout)
        grid.addLayout(messagesLayout)
        self.setLayout(grid)

    def chooseSchoolReportTemplate(self):
        data = self.chooseTemplate('bulletin')
        if len(data) > 0:
            # on recharge les données du premier onglet :
            self.parent.tabs[0].initialize(dataList=data)

    def chooseReferentielTemplate(self):
        data = self.chooseTemplate('referentiel')
        if len(data) > 0:
            # on recharge les données du deuxième onglet :
            self.parent.tabs[1].initialize(dataList=data)

    def chooseConfidentielTemplate(self):
        data = self.chooseTemplate('confidentiel')
        if len(data) > 0:
            # on recharge les données du troisième onglet :
            self.parent.tabs[2].initialize(dataList=data)

    def chooseSuiviTemplate(self):
        data = self.chooseTemplate('suivi')
        if len(data) > 0:
            # on recharge les données du quatrième onglet :
            self.parent.tabs[3].initialize(dataList=data)

    def chooseTemplate(self, tableName):
        """
        on sélectionne et on lit un fichier csv 
        de modèle de compétences partagées.
        À FAIRE : les télécharger plutôt que les livrer dans verac_admin.
        """
        csvDir = utils_functions.u('{0}/modeles/{1}').format(
            admin.csvDir, tableName)
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open csv File'),
            csvDir, 
            QtWidgets.QApplication.translate(
                'main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return []
        import utils_export
        destFile = utils_functions.u('{0}/{1}.csv').format(
            self.main.tempPath, tableName)
        utils_filesdirs.removeAndCopy(fileName, destFile)
        # on récupère le fichier csv :
        headers, data = utils_export.csv2List(fileName)
        return data

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.modified = False





###########################################################"
#   UTILISATEURS
#   USERS
###########################################################"

class TeachersDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer la liste des profs.
    """
    def __init__(self, parent=None):
        super(TeachersDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'gest-direct-users'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []
        # un dico pour remplacer temporairement la table :
        self.profs = {}
        # liste des logins élèves (pour ne pas utiliser un login déjà pris) :
        self.elevesLogins = []
        # pour ne pas lancer l'édition 2 fois :
        self.inDoEdited = False
        # pour repérer les autres tables modifiées :
        self.otherTables = {
            'replacements': [], 
            'lsu': [], 
            'classes': [], 
            'matieres': [], 
            'users': [], 
            }

        # la zone de sélection :
        self.filterComboBox = QtWidgets.QComboBox()
        self.filterComboBox.currentIndexChanged.connect(self.filterComboBoxChanged)
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.filterComboBox, 1, 0)
        baseGrid.addWidget(self.baseList,       2, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(self, layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # id :
        text = QtWidgets.QApplication.translate('main', 'Id:')
        idLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.idEdit = QtWidgets.QLineEdit()
        self.idEdit.setValidator(QtGui.QIntValidator(self.idEdit))
        # num :
        text = QtWidgets.QApplication.translate('main', 'Num:')
        numLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.numEdit = QtWidgets.QLineEdit()
        # name :
        text = QtWidgets.QApplication.translate('main', 'Name:')
        nameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.nameEdit = QtWidgets.QLineEdit()
        # firstName :
        text = QtWidgets.QApplication.translate('main', 'FirstName:')
        firstNameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.firstNameEdit = QtWidgets.QLineEdit()
        # subject :
        text = QtWidgets.QApplication.translate('main', 'Subject:')
        subjectLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.subjectComboBox = QtWidgets.QComboBox()
        self.subjectComboBox.currentIndexChanged.connect(self.doChanged)
        # login :
        text = QtWidgets.QApplication.translate('main', 'Login:')
        loginLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.loginEdit = QtWidgets.QLineEdit()
        # initial password :
        text = QtWidgets.QApplication.translate('main', 'Initial<br/>Password:')
        initialPasswordLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.initialPasswordEdit = QtWidgets.QLineEdit()
        # password :
        text = QtWidgets.QApplication.translate('main', 'Password:')
        passwordLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.size = 18
        self.toolTipPasswordOk = QtWidgets.QApplication.translate(
            'main', 'The password has been changed.')
        self.toolTipPasswordBad = QtWidgets.QApplication.translate(
            'main', 'The password has not been changed!')
        self.passwordChanged = QtWidgets.QLabel()
        self.passwordChanged.setMaximumSize(self.size, self.size)
        self.passwordChanged.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        reinitPasswordButton = QtWidgets.QPushButton(
            utils.doIcon('view-refresh'), 
            '',
            toolTip = QtWidgets.QApplication.translate('main', 'ReinitPassword'))
        reinitPasswordButton.setObjectName('ReinitPassword')
        reinitPasswordButton.clicked.connect(self.doChangePassword)
        changePasswordButton = QtWidgets.QPushButton(
            utils.doIcon('edit-password'), 
            '',
            toolTip = QtWidgets.QApplication.translate('main', 'ChangePassword'))
        changePasswordButton.setObjectName('ChangePassword')
        changePasswordButton.clicked.connect(self.doChangePassword)
        passwordLayout = QtWidgets.QHBoxLayout()
        passwordLayout.addStretch(1)
        passwordLayout.addWidget(self.passwordChanged)
        passwordLayout.addWidget(reinitPasswordButton)
        passwordLayout.addWidget(changePasswordButton)
        # fileName :
        text = QtWidgets.QApplication.translate('main', 'FileName:')
        fileNameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.fileNameEdit = QtWidgets.QLineEdit()
        self.fileNameEdit.setEnabled(False)
        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,    1, 1)
        editGrid.addWidget(idLabel,       2, 0)
        editGrid.addWidget(self.idEdit,   2, 1)
        editGrid.addWidget(numLabel,       3, 0)
        editGrid.addWidget(self.numEdit,   3, 1)
        editGrid.addWidget(nameLabel,       4, 0)
        editGrid.addWidget(self.nameEdit,   4, 1)
        editGrid.addWidget(firstNameLabel,      5, 0)
        editGrid.addWidget(self.firstNameEdit,  5, 1)
        editGrid.addWidget(subjectLabel,      6, 0)
        editGrid.addWidget(self.subjectComboBox,  6, 1)
        editGrid.addWidget(loginLabel,      7, 0)
        editGrid.addWidget(self.loginEdit,  7, 1)
        editGrid.addWidget(initialPasswordLabel,      8, 0)
        editGrid.addWidget(self.initialPasswordEdit,  8, 1)
        editGrid.addWidget(passwordLabel,      9, 0)
        editGrid.addLayout(passwordLayout,        9, 1)
        editGrid.addWidget(fileNameLabel,      10, 0)
        editGrid.addWidget(self.fileNameEdit,  10, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()
        self.createConnexions()

    def initialize(self, data=[]):
        """
        remplissage des 2 comboBox des matières 
        (filterComboBox et subjectComboBox)
        et du dico self.profs.
        On récupère aussi les logins des élèves.
        Si la liste data n'est pas vide, c'est qu'on appelle
        depuis un import SIECLE ou autre.
        """
        self.fromSoft = True

        # on récupère l'état des mots de passe :
        badPasswords = []
        if len(data) < 1:
            query_users = utils_db.query(self.main.db_users)
            commandLine_users = 'SELECT * FROM profs WHERE Mdp=initialMdp'
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            while query_users.next():
                id_prof = int(query_users.value(0))
                badPasswords.append(id_prof)
        else:
             for itemData in self.profs['ALL']:
                 id_prof = itemData[1]
                 profPasswordChanged = itemData[8]
                 if not(profPasswordChanged):
                     badPasswords.append(id_prof)

        # on récupère la liste des matières :
        if len(data) < 1:
            self.profs = {'ALL': [], '': []}
            self.filterComboBox.addItem(
                QtWidgets.QApplication.translate('main', 'All Subjects'))
            self.filterComboBox.addItem(
                QtWidgets.QApplication.translate('main', 'No Subject'))
            self.subjectComboBox.addItem('')
            self.filterComboBox.insertSeparator(1000)
            # les matières "normales" au début :
            for matiereCode in admin.MATIERES['BLT']:
                matiereName = admin.MATIERES['Code2Matiere'][matiereCode][1]
                self.filterComboBox.addItem(matiereName)
                self.subjectComboBox.addItem(matiereName)
                self.profs[matiereName] = []
            self.filterComboBox.insertSeparator(1000)
            self.subjectComboBox.insertSeparator(1000)
            # matières spéciales (PP, ...) :
            for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
                matiereName = admin.MATIERES['Code2Matiere'][matiereCode][1]
                self.filterComboBox.addItem(matiereName)
                self.subjectComboBox.addItem(matiereName)
                self.profs[matiereName] = []
            self.filterComboBox.insertSeparator(1000)
            self.subjectComboBox.insertSeparator(1000)
            # les matières hors bulletin :
            for matiereCode in admin.MATIERES['OTHERS']:
                matiereName = admin.MATIERES['Code2Matiere'][matiereCode][1]
                self.filterComboBox.addItem(matiereName)
                self.subjectComboBox.addItem(matiereName)
                self.profs[matiereName] = []
        else:
            """
            for matiereName in self.profs:
                self.profs[matiereName] = []
            """
            self.filterComboBox.insertSeparator(1000)
            self.subjectComboBox.insertSeparator(1000)
            for line in data['matieres']:
                matiereName = line[1]
                if not(matiereName in self.profs):
                    self.filterComboBox.addItem(matiereName)
                    self.subjectComboBox.addItem(matiereName)
                    self.profs[matiereName] = []
                    self.otherTables['matieres'].append(line)
            # on en profite pour compléter otherTables si besoin :
            for line in data['replacements']:
                self.otherTables['replacements'].append(line)
            for line in data['lsu']:
                self.otherTables['lsu'].append(line)

        # on récupère la liste des nouvelles classes :
        if len(data) > 0:
            for line in data['classes']:
                self.otherTables['classes'].append(line)

        # liste des profs :
        if len(data) < 1:
            # on récupère d'après la table admin.profs :
            query_admin = utils_db.query(admin.db_admin)
            commandLine_admin = utils_db.q_selectAllFrom.format('profs')
            queryList = utils_db.query2List(
                commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
            for record in queryList:
                id_prof = int(record[0])
                profNum = record[1]
                profName = record[2]
                profFirstName = record[3]
                profSubjectCode = record[4]
                profSubject = admin.MATIERES['Code2Matiere'].get(
                    profSubjectCode, (0, '', ''))[1]
                profLogin = record[5]
                profInitialPassword = record[6]
                # si la matière n'est pas dans le dico, on prévient :
                if not(profSubject in self.profs):
                    m1 = QtWidgets.QApplication.translate(
                        'main', 
                        'The <b>{0}</b> subject<br/>'
                        ' (attributed to teacher <b>{1} {2}</b>)<br/>'
                        ' does not exist!')
                    m1 = utils_functions.u(m1).format(profSubject, profName, profFirstName)
                    m2 = QtWidgets.QApplication.translate(
                        'main', 
                        'Remember to assign a valid subject.')
                    message = utils_functions.u(
                        '<p align="center">{0}</p>'
                        '<p align="center">{1}</p>'
                        '<p align="center">{2}</p>'
                        '<p></p>').format(utils.SEPARATOR_LINE, m1, m2)
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    profSubject = ''
                itemText = utils_functions.u('{0} {1} [{2}]').format(
                    profName, profFirstName, profSubject)
                profPasswordChanged = not(id_prof in badPasswords)
                itemData = [
                    itemText, 
                    id_prof, profNum, profName, profFirstName, profSubject, 
                    profLogin, profInitialPassword, profPasswordChanged]
                self.profs[profSubject].append(itemData)
                self.profs['ALL'].append(itemData)
            # on récupère les logins des élèves :
            commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                self.elevesLogins.append(query_admin.value(5))
        else:
            # on récupère d'après la nouvelle liste :
            self.doModified()
            for line in data['profs']:
                id_prof = line[0]
                profNum = line[1]
                profName = line[2]
                profFirstName = line[3]
                profSubjectCode = line[4]
                profSubject = admin.MATIERES['Code2Matiere'].get(
                    profSubjectCode, (0, profSubjectCode, ''))[1]
                profLogin = line[5]
                profInitialPassword = line[6]
                # si la matière n'est pas dans le dico, on prévient :
                if not(profSubject in self.profs):
                    m1 = QtWidgets.QApplication.translate(
                        'main', 
                        'The <b>{0}</b> subject<br/>'
                        ' (attributed to teacher <b>{1} {2}</b>)<br/>'
                        ' does not exist!')
                    m1 = utils_functions.u(m1).format(profSubject, profName, profFirstName)
                    m2 = QtWidgets.QApplication.translate(
                        'main', 
                        'Remember to assign a valid subject.')
                    message = utils_functions.u(
                        '<p align="center">{0}</p>'
                        '<p align="center">{1}</p>'
                        '<p align="center">{2}</p>'
                        '<p></p>').format(utils.SEPARATOR_LINE, m1, m2)
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    profSubject = ''
                itemText = utils_functions.u('{0} {1} [{2}]').format(
                    profName, profFirstName, profSubject)
                profPasswordChanged = not(id_prof in badPasswords)
                itemData = [
                    itemText, 
                    id_prof, profNum, profName, profFirstName, profSubject, 
                    profLogin, profInitialPassword, profPasswordChanged]
                self.profs[profSubject].append(itemData)
                self.profs['ALL'].append(itemData)
        # on sélectionne "toutes les matières" :
        self.filterComboBoxChanged(0)
        self.fromSoft = False

    def createConnexions(self):
        lineEdits = (#QLineEdit
            self.idEdit, 
            self.numEdit, 
            self.nameEdit, 
            self.firstNameEdit, 
            self.loginEdit, 
            self.initialPasswordEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.editingFinished.connect(self.doEdited)

    def filterComboBoxChanged(self, index=-1):
        """
        mise à jour de la liste selon la matière sélectionnée
        """
        self.fromSoft = True
        if index < 0:
            index = self.filterComboBox.currentIndex()
        else:
            self.filterComboBox.setCurrentIndex(index)
        selected = self.filterComboBox.currentText()
        if index == 0:
            selected = 'ALL'
        elif index == 1:
            selected = ''
        self.baseList.clear()
        if not(selected in self.profs):
            return
        for itemData in self.profs[selected]:
            item = QtWidgets.QListWidgetItem(itemData[0])
            item.setData(QtCore.Qt.UserRole, itemData)
            self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        On a besoin d'un try except en cas de changement de sélection de matière.
        """
        self.fromSoft = True
        try:
            self.lastData = current.data(QtCore.Qt.UserRole)
        except:
            self.lastData = ['', '', '', '', '', '', '', '', False]
        actions = {
            self.idEdit: ('TEXT', 1),
            self.numEdit: ('TEXT', 2),
            self.nameEdit: ('TEXT', 3),
            self.firstNameEdit: ('TEXT', 4),
            self.subjectComboBox: ('COMBOBOX', 5),
            self.loginEdit: ('TEXT', 6),
            self.initialPasswordEdit: ('TEXT', 7)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'COMBOBOX':
                index = action.findText(self.lastData[actions[action][1]])
                action.setCurrentIndex(index)
        # affichage du nom du fichier prof :
        self.fileNameEdit.setText(utils_functions.u('{0}{1}.sqlite').format(
            admin.prefixProfFiles, self.idEdit.text()))
        # état du mot de passe :
        if self.lastData[8] == True:
            setOk(self.passwordChanged, self.size, True, toolTip=self.toolTipPasswordOk)
        else:
            setOk(self.passwordChanged, self.size, False, toolTip=self.toolTipPasswordBad)
        self.fromSoft = False

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        if self.inDoEdited:
            return
        self.inDoEdited = True
        actions = {
            self.idEdit: 1,
            self.numEdit: 2,
            self.nameEdit: 3,
            self.firstNameEdit: 4,
            self.loginEdit: 6,
            self.initialPasswordEdit: 7}
        newText = self.sender().text()
        index = actions.get(self.sender(), 0)
        if index == 1:
            # on vérifie que l'id est disponible :
            newId = int(newText)
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            lastId = data[1]
            others = []
            for otherData in self.profs['ALL']:
                if otherData[1] != lastId:
                    others.append(otherData[1])
            if newId in others:
                msg = QtWidgets.QApplication.translate(
                    'main', 'This Id is already in use, choose another.')
                utils_functions.messageBox(self.main, level='warning', message=msg)
                self.idEdit.setText('{0}'.format(lastId))
                self.inDoEdited = False
                return
            # mise à jour de l'item :
            subject = data[5]
            self.profs[subject].remove(data)
            self.profs['ALL'].remove(data)
            data[1] = newId
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.profs :
            self.profs[subject].append(data)
            self.profs[subject].sort()
            self.profs['ALL'].append(data)
            self.profs['ALL'].sort()
            self.doModified()
            # mise à jour du nom du fichier prof :
            self.fileNameEdit.setText(utils_functions.u('{0}{1}.sqlite').format(
                admin.prefixProfFiles, self.idEdit.text()))
        elif index == 6:
            # on vérifie que le login est disponible :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            lastLogin = data[6]
            others = []
            for otherData in self.profs['ALL']:
                if otherData[6] != lastLogin:
                    others.append(otherData[6])
            if (newText in others) or (newText in self.elevesLogins):
                msg = QtWidgets.QApplication.translate(
                    'main', 'This login is already in use, choose another.')
                utils_functions.messageBox(self.main, level='warning', message=msg)
                self.loginEdit.setText(lastLogin)
                self.inDoEdited = False
                return
            # mise à jour de l'item :
            subject = data[5]
            self.profs[subject].remove(data)
            self.profs['ALL'].remove(data)
            data[6] = newText
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.profs :
            self.profs[subject].append(data)
            self.profs[subject].sort()
            self.profs['ALL'].append(data)
            self.profs['ALL'].sort()
            self.doModified()
        elif index > 1:
            # mise à jour de l'item :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            subject = data[5]
            self.profs[subject].remove(data)
            self.profs['ALL'].remove(data)
            data[index] = newText
            if index in (3, 4):
                data[0] = utils_functions.u('{0} {1} [{2}]').format(
                    data[3], data[4], data[5])
                current.setText(data[0])
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.profs :
            self.profs[subject].append(data)
            self.profs[subject].sort()
            self.profs['ALL'].append(data)
            self.profs['ALL'].sort()
            self.doModified()
        self.inDoEdited = False

    def doChanged(self, value):
        if self.fromSoft:
            return
        if self.sender() == self.subjectComboBox:
            newSubject = self.subjectComboBox.currentText()
            # mise à jour de l'item :
            current = self.baseList.currentItem()
            try:
                data = current.data(QtCore.Qt.UserRole)
            except:
                return
            self.profs[data[5]].remove(data)
            self.profs['ALL'].remove(data)
            data[5] = newSubject
            data[0] = utils_functions.u('{0} {1} [{2}]').format(
                data[3], data[4], data[5])
            current.setText(data[0])
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.profs :
            self.profs[newSubject].append(data)
            self.profs[newSubject].sort()
            self.profs['ALL'].append(data)
            self.profs['ALL'].sort()
            self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            # on propose le premier id disponible :
            newId = 1
            others = []
            for otherData in self.profs['ALL']:
                others.append(otherData[1])
            while newId in others:
                newId += 1
            text = QtWidgets.QApplication.translate('main', 'New Record')
            item = QtWidgets.QListWidgetItem(text)
            data = [text, newId, '', text, '', '', '', '', False]
            item.setData(QtCore.Qt.UserRole, data)
            self.baseList.addItem(item)
            self.baseList.setCurrentRow(self.baseList.count() - 1)
            # mise à jour du dico self.profs :
            self.profs[''].append(data)
            self.profs[''].sort()
            self.profs['ALL'].append(data)
            self.profs['ALL'].sort()
            self.doModified()
        elif what == 'delete':
            # mise à jour de l'item :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.profs[data[5]].remove(data)
            self.profs[data[5]].sort()
            self.profs['ALL'].remove(data)
            self.profs['ALL'].sort()
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.profs[data[5]].remove(data)
            self.profs['ALL'].remove(data)
            data = self.lastData
            current.setText(data[0])
            current.setData(QtCore.Qt.UserRole, data)
            self.profs[data[5]].append(data)
            self.profs[data[5]].sort()
            self.profs['ALL'].append(data)
            self.profs['ALL'].sort()
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doChangePassword(self):
        what = self.sender().objectName()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        id_prof = data[1]
        profInitialPassword = data[7]
        commandLine_update = utils_functions.u(
            'UPDATE profs SET Mdp="{0}" WHERE id={1}')
        if what == 'ReinitPassword':
            encMdp = utils_functions.doEncode(profInitialPassword, algo='sha256')
            commandLine_users = commandLine_update.format(encMdp, id_prof)
            self.otherTables['users'].append(commandLine_users)
            data[8] = False
            setOk(self.passwordChanged, self.size, False, toolTip=self.toolTipPasswordBad)
        elif what == 'ChangePassword':
            newMdp, OK = QtWidgets.QInputDialog.getText(
                self.main, utils.PROGNAME, 
                QtWidgets.QApplication.translate('main', 'New password:'),
                QtWidgets.QLineEdit.Normal, '')
            if not(OK):
                return
            encMdp = utils_functions.doEncode(newMdp, algo='sha256')
            commandLine_users = commandLine_update.format(encMdp, id_prof)
            self.otherTables['users'].append(commandLine_users)
            if newMdp != profInitialPassword:
                data[8] = True
                setOk(self.passwordChanged, self.size, True, toolTip=self.toolTipPasswordOk)
            else:
                data[8] = False
                setOk(self.passwordChanged, self.size, False, toolTip=self.toolTipPasswordBad)
        current.setData(QtCore.Qt.UserRole, data)
        self.doModified()

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['admin'] = True
        self.parent.modifiedTables['admin']['profs'] = True
        self.parent.modifiedTables['DATABASES']['users'] = True
        replacementsModified = (len(self.otherTables['replacements']) > 0)
        lsuModified = (len(self.otherTables['lsu']) > 0)
        classesModified = (len(self.otherTables['classes']) > 0)
        matieresModified = (len(self.otherTables['matieres']) > 0)
        usersModified = (len(self.otherTables['users']) > 0)
        if replacementsModified:
            self.parent.modifiedTables['admin']['replacements'] = True
        if lsuModified:
            self.parent.modifiedTables['admin']['lsu'] = True
        if matieresModified:
            self.parent.modifiedTables['DATABASES']['commun'] = True
            self.parent.modifiedTables['commun']['matieres'] = True
            matiere2Code = {}
            for matiereName in admin.MATIERES['Matiere2Code']:
                matiere2Code[matiereName] = admin.MATIERES['Matiere2Code'][matiereName]
            for matiere in self.otherTables['matieres']:
                matiere2Code[matiere[1]] = matiere[2]
        if classesModified:
            self.parent.modifiedTables['DATABASES']['commun'] = True
            self.parent.modifiedTables['commun']['classes'] = True
        self.filterComboBoxChanged(0)
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if matieresModified:
                profSubjectCode = matiere2Code.get(data[5], '')
            else:
                profSubjectCode = admin.MATIERES['Matiere2Code'].get(data[5], '')
            lines.append((
                data[1], data[2], data[3], data[4], 
                profSubjectCode, data[6], data[7]))
        query_admin, transactionOK_admin = utils_db.queryTransactionDB(
            admin.db_admin)
        if matieresModified or classesModified:
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
        if usersModified:
            query_users, transactionOK_users = utils_db.queryTransactionDB(
                self.main.db_users)
        try:
            # premières commandes :
            listCommands = [
                utils_db.qdf_table.format('profs'), 
                utils_db.listTablesAdmin['profs'][0]
                ]
            if replacementsModified:
                listCommands.append(
                    utils_db.listTablesAdmin['replacements'][0])
                what = {'xmlClasses': False, 'xmlMatieres': False, 'xmlProfs': False}
                for replacement in self.otherTables['replacements']:
                    what[replacement[0]] = True
                for replaceWhere in what:
                    if what[replaceWhere]:
                        commandLine = utils_db.qdf_whereText.format(
                            'replacements', 'replaceWhere', replaceWhere)
                        listCommands.append(commandLine)
            if lsuModified:
                commandLine = utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'prof')
                listCommands.append(commandLine)
            query_admin = utils_db.queryExecute(listCommands, query=query_admin)

            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                'profs', utils_db.listTablesAdmin['profs'][1])
            query_admin = utils_db.queryExecute(
                {commandLine: lines}, query=query_admin)
            if replacementsModified:
                commandLine = utils_db.insertInto(
                    'replacements', utils_db.listTablesAdmin['replacements'][1])
                query_admin = utils_db.queryExecute(
                    {commandLine: self.otherTables['replacements']}, 
                    query=query_admin)
            if lsuModified:
                commandLine = utils_db.insertInto(
                    'lsu', utils_db.listTablesAdmin['lsu'][1])
                query_admin = utils_db.queryExecute(
                    {commandLine: self.otherTables['lsu']}, 
                    query=query_admin)
            if classesModified:
                commandLine = utils_db.insertInto(
                    'classes', utils_db.listTablesCommun['classes'][1])
                query_commun = utils_db.queryExecute(
                    {commandLine: self.otherTables['classes']}, 
                    query=query_commun)
            if matieresModified:
                commandLine = utils_db.insertInto(
                    'matieres', utils_db.listTablesCommun['matieres'][1])
                query_commun = utils_db.queryExecute(
                    {commandLine: self.otherTables['matieres']}, 
                    query=query_commun)
            if usersModified:
                for commandLine_users in self.otherTables['users']:
                    query_users = utils_db.queryExecute(commandLine_users, query=query_users)

        finally:
            utils_db.endTransaction(
                query_admin, admin.db_admin, transactionOK_admin)
            if matieresModified or classesModified:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
            if usersModified:
                utils_db.endTransaction(
                    query_users, self.main.db_users, transactionOK_users)
        self.otherTables = {
            'replacements': [], 
            'lsu': [], 
            'classes': [], 
            'matieres': [], 
            'users': [], 
            }
        self.modified = False


class StudentsDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer la liste des élèves.
    """
    def __init__(self, parent=None):
        super(StudentsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'gest-direct-users'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []
        # un dico pour remplacer temporairement la table :
        self.eleves = {}
        # liste des logins profs (pour ne pas utiliser un login déjà pris) :
        self.profsLogins = []
        # pour ne pas lancer l'édition 2 fois :
        self.inDoEdited = False
        # pour repérer les autres tables modifiées :
        self.otherTables = {
            'replacements': [], 
            'classes': [], 
            'adresses': [], 
            'users': [], 
            }

        # la zone de sélection :
        self.filterComboBox = QtWidgets.QComboBox()
        self.filterComboBox.currentIndexChanged.connect(self.filterComboBoxChanged)
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.filterComboBox, 1, 0)
        baseGrid.addWidget(self.baseList,       2, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(self, layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # id :
        text = QtWidgets.QApplication.translate('main', 'Id:')
        idLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.idEdit = QtWidgets.QLineEdit()
        self.idEdit.setValidator(QtGui.QIntValidator(self.idEdit))
        # num :
        text = QtWidgets.QApplication.translate('main', 'Num:')
        numLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.numEdit = QtWidgets.QLineEdit()
        # name :
        text = QtWidgets.QApplication.translate('main', 'Name:')
        nameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.nameEdit = QtWidgets.QLineEdit()
        # firstName :
        text = QtWidgets.QApplication.translate('main', 'FirstName:')
        firstNameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.firstNameEdit = QtWidgets.QLineEdit()
        # classe :
        text = QtWidgets.QApplication.translate('main', 'Class:')
        classeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.classeComboBox = QtWidgets.QComboBox()
        self.classeComboBox.currentIndexChanged.connect(self.doChanged)
        # login :
        text = QtWidgets.QApplication.translate('main', 'Login:')
        loginLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.loginEdit = QtWidgets.QLineEdit()
        # birthday :
        text = QtWidgets.QApplication.translate('main', 'Birthday:')
        birthdayLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.birthdayEdit = QtWidgets.QLineEdit()
        text = QtWidgets.QApplication.translate('main', 'must be of the form ddmmyyyy')
        self.birthdayEdit.setToolTip(text)
        # initial password :
        text = QtWidgets.QApplication.translate('main', 'Initial<br/>Password:')
        initialPasswordLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.initialPasswordEdit = QtWidgets.QLineEdit()
        # password :
        text = QtWidgets.QApplication.translate('main', 'Password:')
        passwordLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.size = 18
        self.toolTipPasswordOk = QtWidgets.QApplication.translate(
            'main', 'The password has been changed.')
        self.toolTipPasswordBad = QtWidgets.QApplication.translate(
            'main', 'The password has not been changed!')
        self.passwordChanged = QtWidgets.QLabel()
        self.passwordChanged.setMaximumSize(self.size, self.size)
        self.passwordChanged.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        reinitPasswordButton = QtWidgets.QPushButton(
            utils.doIcon('view-refresh'), 
            '',
            toolTip = QtWidgets.QApplication.translate('main', 'ReinitPassword'))
        reinitPasswordButton.setObjectName('ReinitPassword')
        reinitPasswordButton.clicked.connect(self.doChangePassword)
        changePasswordButton = QtWidgets.QPushButton(
            utils.doIcon('edit-password'), 
            '',
            toolTip = QtWidgets.QApplication.translate('main', 'ChangePassword'))
        changePasswordButton.setObjectName('ChangePassword')
        changePasswordButton.clicked.connect(self.doChangePassword)
        passwordLayout = QtWidgets.QHBoxLayout()
        passwordLayout.addStretch(1)
        passwordLayout.addWidget(self.passwordChanged)
        passwordLayout.addWidget(reinitPasswordButton)
        passwordLayout.addWidget(changePasswordButton)
        # lastYear :
        text = QtWidgets.QApplication.translate('main', 'LastYear:')
        lastYearLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.lastYearEdit = QtWidgets.QLineEdit()
        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,    1, 1)
        editGrid.addWidget(idLabel,       2, 0)
        editGrid.addWidget(self.idEdit,   2, 1)
        editGrid.addWidget(numLabel,       3, 0)
        editGrid.addWidget(self.numEdit,   3, 1)
        editGrid.addWidget(nameLabel,       4, 0)
        editGrid.addWidget(self.nameEdit,   4, 1)
        editGrid.addWidget(firstNameLabel,      5, 0)
        editGrid.addWidget(self.firstNameEdit,  5, 1)
        editGrid.addWidget(classeLabel,      6, 0)
        editGrid.addWidget(self.classeComboBox,  6, 1)
        editGrid.addWidget(loginLabel,      7, 0)
        editGrid.addWidget(self.loginEdit,  7, 1)
        editGrid.addWidget(birthdayLabel,      8, 0)
        editGrid.addWidget(self.birthdayEdit,  8, 1)
        editGrid.addWidget(initialPasswordLabel,      9, 0)
        editGrid.addWidget(self.initialPasswordEdit,  9, 1)
        editGrid.addWidget(passwordLabel,      10, 0)
        editGrid.addLayout(passwordLayout,        10, 1)
        editGrid.addWidget(lastYearLabel,      11, 0)
        editGrid.addWidget(self.lastYearEdit,  11, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()
        self.createConnexions()

    def initialize(self, data=[]):
        """
        remplissage des 2 comboBox des classes 
        (filterComboBox et classeComboBox)
        et du dico self.eleves.
        On récupère aussi les logins des profs.
        Si la liste data n'est pas vide, c'est qu'on appelle
        depuis un import SIECLE ou autre.
        """
        self.fromSoft = True

        # on récupère l'état des mots de passe :
        badPasswords = []
        if len(data) < 1:
            query_users = utils_db.query(self.main.db_users)
            commandLine_users = 'SELECT * FROM eleves WHERE Mdp=initialMdp'
            query_users = utils_db.queryExecute(commandLine_users, query=query_users)
            while query_users.next():
                id_eleve = int(query_users.value(0))
                badPasswords.append(id_eleve)
        else:
             for itemData in self.eleves['ALL']:
                 id_eleve = itemData[1]
                 elevePasswordChanged = itemData[10]
                 if not(elevePasswordChanged):
                     badPasswords.append(id_eleve)

        # on récupère la liste des classes :
        if len(data) < 1:
            self.eleves = {'ALL': []}
            self.filterComboBox.addItem(
                QtWidgets.QApplication.translate('main', 'All the classes'))
            query_commun = utils_db.query(self.main.db_commun)
            commandLine_commun = utils_db.qs_commun_classes
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                classe = query_commun.value(1)
                self.filterComboBox.addItem(classe)
                self.classeComboBox.addItem(classe)
                self.eleves[classe] = []
        else:
            for classe in self.eleves:
                self.eleves[classe] = []
            for line in data['classes']:
                classe = line[1]
                self.filterComboBox.addItem(classe)
                self.classeComboBox.addItem(classe)
                self.eleves[classe] = []
                self.otherTables['classes'].append(line)
            # on en profite pour compléter otherTables si besoin :
            for line in data['replacements']:
                self.otherTables['replacements'].append(line)
            for line in data['adresses']:
                self.otherTables['adresses'].append(line)

        # liste des élèves :
        if len(data) < 1:
            # on récupère d'après la table admin.eleves :
            query_admin = utils_db.query(admin.db_admin)
            commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
            queryList = utils_db.query2List(
                commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
            for record in queryList:
                id_eleve = int(record[0])
                eleveNum = record[1]
                eleveName = record[2]
                eleveFirstName = record[3]
                eleveClasse = record[4]
                eleveLogin = record[5]
                eleveBirthday = record[6]
                elevePassword = record[7]
                try:
                    eleveLastYear = record[8]
                except:
                    eleveLastYear = ''
                eleve_id = int(record[9])
                eleveDateEntree = int(record[10])
                eleveDateSortie = int(record[11])
                try:
                    eleveSexe = int(record[12])
                except:
                    eleveSexe = 1
                itemText = utils_functions.u('{0} {1} [{2}]').format(
                    eleveName, eleveFirstName, eleveClasse)
                elevePasswordChanged = not(id_eleve in badPasswords)
                itemData = [
                    itemText, 
                    id_eleve, eleveNum, eleveName, eleveFirstName, eleveClasse, 
                    eleveLogin, eleveBirthday, elevePassword, eleveLastYear,
                    elevePasswordChanged, 
                    eleve_id, eleveDateEntree, eleveDateSortie, 
                    eleveSexe]
                # si la classe n'est pas dans le dico, on prévient :
                if not(eleveClasse in self.eleves):
                    m1 = QtWidgets.QApplication.translate(
                        'main', 
                        'The <b>{0}</b> class<br/>'
                        ' (attributed to student <b>{1} {2}</b>)<br/>'
                        ' does not exist!')
                    m1 = utils_functions.u(m1).format(eleveClasse, eleveName, eleveFirstName)
                    m2 = QtWidgets.QApplication.translate(
                        'main', 
                        'Remember to assign a valid class or to create this class.')
                    message = utils_functions.u(
                        '<p align="center">{0}</p>'
                        '<p align="center">{1}</p>'
                        '<p align="center">{2}</p>'
                        '<p></p>').format(utils.SEPARATOR_LINE, m1, m2)
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    self.eleves[eleveClasse] = []
                self.eleves[eleveClasse].append(itemData)
                self.eleves['ALL'].append(itemData)
            # on récupère aussi les logins des profs :
            commandLine_admin = utils_db.q_selectAllFrom.format('profs')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                self.profsLogins.append(query_admin.value(5))
        else:
            # on récupère d'après la nouvelle liste :
            self.doModified()
            for line in data['eleves']:
                id_eleve = line[0]
                eleveNum = line[1]
                eleveName = line[2]
                eleveFirstName = line[3]
                eleveClasse = line[4]
                eleveLogin = line[5]
                eleveBirthday = line[6]
                elevePassword = line[7]
                eleveLastYear = line[8]
                eleve_id = line[9]
                eleveDateEntree = line[10]
                eleveDateSortie = line[11]
                eleveSexe = line[12]
                itemText = utils_functions.u('{0} {1} [{2}]').format(
                    eleveName, eleveFirstName, eleveClasse)
                elevePasswordChanged = not(id_eleve in badPasswords)
                itemData = [
                    itemText, 
                    id_eleve, eleveNum, eleveName, eleveFirstName, eleveClasse, 
                    eleveLogin, eleveBirthday, elevePassword, eleveLastYear,
                    elevePasswordChanged, 
                    eleve_id, eleveDateEntree, eleveDateSortie, 
                    eleveSexe]
                # si la classe n'est pas dans le dico, on prévient :
                if not(eleveClasse in self.eleves):
                    m1 = QtWidgets.QApplication.translate(
                        'main', 
                        'The <b>{0}</b> class<br/>'
                        ' (attributed to student <b>{1} {2}</b>)<br/>'
                        ' does not exist!')
                    m1 = utils_functions.u(m1).format(eleveClasse, eleveName, eleveFirstName)
                    m2 = QtWidgets.QApplication.translate(
                        'main', 
                        'Remember to assign a valid class or to create this class.')
                    message = utils_functions.u(
                        '<p align="center">{0}</p>'
                        '<p align="center">{1}</p>'
                        '<p align="center">{2}</p>'
                        '<p></p>').format(utils.SEPARATOR_LINE, m1, m2)
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    self.eleves[eleveClasse] = []
                self.eleves[eleveClasse].append(itemData)
                self.eleves['ALL'].append(itemData)
        # on sélectionne "toutes les classes" :
        self.filterComboBoxChanged(0)
        self.fromSoft = False

    def createConnexions(self):
        lineEdits = (#QLineEdit
            self.idEdit, 
            self.numEdit, 
            self.nameEdit, 
            self.firstNameEdit, 
            self.loginEdit, 
            self.birthdayEdit, 
            self.initialPasswordEdit, 
            self.lastYearEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.editingFinished.connect(self.doEdited)

    def filterComboBoxChanged(self, index=-1):
        """
        mise à jour de la liste selon la classe sélectionnée
        """
        if index < 0:
            index = self.filterComboBox.currentIndex()
        else:
            self.filterComboBox.setCurrentIndex(index)
        selected = self.filterComboBox.currentText()
        if index == 0:
            selected = 'ALL'
        self.baseList.clear()
        if not(selected in self.eleves):
            return
        self.fromSoft = True
        for itemData in self.eleves[selected]:
            item = QtWidgets.QListWidgetItem(itemData[0])
            item.setData(QtCore.Qt.UserRole, itemData)
            self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        On a besoin d'un try except en cas de changement de sélection de classe.
        """
        self.fromSoft = True
        try:
            self.lastData = current.data(QtCore.Qt.UserRole)
        except:
            self.lastData = ['', '', '', '', '', '', '', '', '', '', False, '', '', '', '']
        actions = {
            self.idEdit: ('TEXT', 1),
            self.numEdit: ('TEXT', 2),
            self.nameEdit: ('TEXT', 3),
            self.firstNameEdit: ('TEXT', 4),
            self.classeComboBox: ('COMBOBOX', 5),
            self.loginEdit: ('TEXT', 6),
            self.birthdayEdit: ('TEXT', 7),
            self.initialPasswordEdit: ('TEXT', 8),
            self.lastYearEdit: ('TEXT', 9)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'COMBOBOX':
                index = action.findText(self.lastData[actions[action][1]])
                action.setCurrentIndex(index)
        # état du mot de passe :
        if self.lastData[10] == True:
            setOk(self.passwordChanged, self.size, True, toolTip=self.toolTipPasswordOk)
        else:
            setOk(self.passwordChanged, self.size, False, toolTip=self.toolTipPasswordBad)
        self.fromSoft = False

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        if self.inDoEdited:
            return
        self.inDoEdited = True
        actions = {
            self.idEdit: 1,
            self.numEdit: 2,
            self.nameEdit: 3,
            self.firstNameEdit: 4,
            self.loginEdit: 6,
            self.birthdayEdit: 7,
            self.initialPasswordEdit: 8,
            self.lastYearEdit: 9}
        newText = self.sender().text()
        index = actions.get(self.sender(), 0)
        if index == 1:
            # on vérifie que l'id est disponible :
            newId = int(newText)
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            lastId = data[1]
            others = []
            for otherData in self.eleves['ALL']:
                if otherData[1] != lastId:
                    others.append(otherData[1])
            if newId in others:
                msg = QtWidgets.QApplication.translate(
                    'main', 'This Id is already in use, choose another.')
                utils_functions.messageBox(self.main, level='warning', message=msg)
                self.idEdit.setText('{0}'.format(lastId))
                self.inDoEdited = False
                return
            # mise à jour de l'item :
            classe = data[5]
            self.eleves[classe].remove(data)
            self.eleves['ALL'].remove(data)
            data[1] = newId
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.eleves :
            self.eleves[classe].append(data)
            self.eleves[classe].sort()
            self.eleves['ALL'].append(data)
            self.eleves['ALL'].sort()
            self.doModified()
        elif index == 6:
            # on vérifie que le login est disponible :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            lastLogin = data[6]
            others = []
            for otherData in self.eleves['ALL']:
                if otherData[6] != lastLogin:
                    others.append(otherData[6])
            if (newText in others) or (newText in self.profsLogins):
                msg = QtWidgets.QApplication.translate(
                    'main', 'This login is already in use, choose another.')
                utils_functions.messageBox(self.main, level='warning', message=msg)
                self.loginEdit.setText(lastLogin)
                self.inDoEdited = False
                return
            # mise à jour de l'item :
            classe = data[5]
            self.eleves[classe].remove(data)
            self.eleves['ALL'].remove(data)
            data[6] = newText
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.eleves :
            self.eleves[classe].append(data)
            self.eleves[classe].sort()
            self.eleves['ALL'].append(data)
            self.eleves['ALL'].sort()
            self.doModified()
        elif index == 7:
            # on vérifie que la date est valable :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            lastBirthday = data[7]
            date = QtCore.QDateTime().fromString(utils_functions.u(newText), 'ddMMyyyy')
            if date.toString('dd/MM/yyyy') == '':
                msg1 = QtWidgets.QApplication.translate(
                    'main', 'The birthday <b>{0}</b> is not valid.').format(newText)
                msg2 = QtWidgets.QApplication.translate(
                    'main', 'It must be of the form <b>ddmmyyyy</b>.')
                msg = utils_functions.u('<p>{0}</p><p>{1}</p>').format(msg1, msg2)
                utils_functions.messageBox(self.main, level='warning', message=msg)
                self.birthdayEdit.setText(lastBirthday)
                self.inDoEdited = False
                return
            # mise à jour de l'item :
            classe = data[5]
            self.eleves[classe].remove(data)
            self.eleves['ALL'].remove(data)
            data[index] = newText
            if index in (3, 4):
                data[0] = utils_functions.u('{0} {1} [{2}]').format(
                    data[3], data[4], data[5])
                current.setText(data[0])
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.eleves :
            self.eleves[classe].append(data)
            self.eleves[classe].sort()
            self.eleves['ALL'].append(data)
            self.eleves['ALL'].sort()
            self.doModified()
        elif index > 1:
            # mise à jour de l'item :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            classe = data[5]
            self.eleves[classe].remove(data)
            self.eleves['ALL'].remove(data)
            data[index] = newText
            if index in (3, 4):
                data[0] = utils_functions.u('{0} {1} [{2}]').format(
                    data[3], data[4], data[5])
                current.setText(data[0])
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.eleves :
            self.eleves[classe].append(data)
            self.eleves[classe].sort()
            self.eleves['ALL'].append(data)
            self.eleves['ALL'].sort()
            self.doModified()
        self.inDoEdited = False

    def doChanged(self, value):
        if self.fromSoft:
            return
        if self.sender() == self.classeComboBox:
            newClasse = self.classeComboBox.currentText()
            # mise à jour de l'item :
            current = self.baseList.currentItem()
            try:
                data = current.data(QtCore.Qt.UserRole)
            except:
                return
            self.eleves[data[5]].remove(data)
            self.eleves['ALL'].remove(data)
            data[5] = newClasse
            data[0] = utils_functions.u('{0} {1} [{2}]').format(
                data[3], data[4], data[5])
            current.setText(data[0])
            current.setData(QtCore.Qt.UserRole, data)
            # mise à jour du dico self.eleves :
            self.eleves[newClasse].append(data)
            self.eleves[newClasse].sort()
            self.eleves['ALL'].append(data)
            self.eleves['ALL'].sort()
            self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            # on propose un id (max + 1 pour ne pas retomber sur un ancien élève) :
            newId = 1
            others = []
            for otherData in self.eleves['ALL']:
                others.append(otherData[1])
            others.sort()
            if len(others) > 0:
                newId = others[-1] + 1
            text = QtWidgets.QApplication.translate('main', 'New Record')
            item = QtWidgets.QListWidgetItem(text)
            classe = self.filterComboBox.itemText(1)
            data = [text, newId, '', text, '', classe, '', '12071816', '', '', False, -1, -1, -1, -1]
            item.setData(QtCore.Qt.UserRole, data)
            self.baseList.addItem(item)
            self.baseList.setCurrentRow(self.baseList.count() - 1)
            # mise à jour du dico self.eleves :
            self.eleves[classe].append(data)
            self.eleves[classe].sort()
            self.eleves['ALL'].append(data)
            self.eleves['ALL'].sort()
            self.doModified()
        elif what == 'delete':
            # mise à jour de l'item :
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.eleves[data[5]].remove(data)
            self.eleves[data[5]].sort()
            self.eleves['ALL'].remove(data)
            self.eleves['ALL'].sort()
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.eleves[data[5]].remove(data)
            self.eleves['ALL'].remove(data)
            data = self.lastData
            current.setText(data[0])
            current.setData(QtCore.Qt.UserRole, data)
            self.eleves[data[5]].append(data)
            self.eleves[data[5]].sort()
            self.eleves['ALL'].append(data)
            self.eleves['ALL'].sort()
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doChangePassword(self):
        what = self.sender().objectName()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        id_eleve = data[1]
        eleveInitialPassword = data[7]
        commandLine_update = utils_functions.u(
            'UPDATE eleves SET Mdp="{0}" WHERE id={1}')
        if what == 'ReinitPassword':
            encMdp = utils_functions.doEncode(eleveInitialPassword, algo='sha256')
            commandLine_users = commandLine_update.format(encMdp, id_eleve)
            self.otherTables['users'].append(commandLine_users)
            data[10] = False
            setOk(self.passwordChanged, self.size, False, toolTip=self.toolTipPasswordBad)
        elif what == 'ChangePassword':
            newMdp, OK = QtWidgets.QInputDialog.getText(
                self.main, utils.PROGNAME, 
                QtWidgets.QApplication.translate('main', 'New password:'),
                QtWidgets.QLineEdit.Normal, '')
            if not(OK):
                return
            encMdp = utils_functions.doEncode(newMdp, algo='sha256')
            commandLine_users = commandLine_update.format(encMdp, id_eleve)
            self.otherTables['users'].append(commandLine_users)
            if newMdp != eleveInitialPassword:
                data[10] = True
                setOk(self.passwordChanged, self.size, True, toolTip=self.toolTipPasswordOk)
            else:
                data[10] = False
                setOk(self.passwordChanged, self.size, False, toolTip=self.toolTipPasswordBad)
        current.setData(QtCore.Qt.UserRole, data)
        self.doModified()

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['admin'] = True
        self.parent.modifiedTables['admin']['eleves'] = True
        self.parent.modifiedTables['DATABASES']['users'] = True
        replacementsModified = (len(self.otherTables['replacements']) > 0)
        adressesModified = (len(self.otherTables['adresses']) > 0)
        classesModified = (len(self.otherTables['classes']) > 0)
        usersModified = (len(self.otherTables['users']) > 0)
        if replacementsModified:
            self.parent.modifiedTables['admin']['replacements'] = True
        if adressesModified:
            self.parent.modifiedTables['admin']['adresses'] = True
        if classesModified:
            self.parent.modifiedTables['DATABASES']['commun'] = True
            self.parent.modifiedTables['commun']['classes'] = True
        self.filterComboBoxChanged(0)
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            lines.append((data[1], data[2], data[3], data[4], data[5], 
                          data[6], data[7], data[8], data[9], 
                          data[11], data[12], data[13], 
                          data[14]))
        query_admin, transactionOK_admin = utils_db.queryTransactionDB(
            admin.db_admin)
        if classesModified:
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
        if usersModified:
            query_users, transactionOK_users = utils_db.queryTransactionDB(
                self.main.db_users)
        try:
            # premières commandes :
            listCommands = [
                utils_db.qdf_table.format('eleves'), 
                utils_db.listTablesAdmin['eleves'][0]
                ]
            if replacementsModified:
                listCommands.append(
                    utils_db.listTablesAdmin['replacements'][0])
                commandLine = utils_db.qdf_whereText.format(
                    'replacements', 'replaceWhere', 'xmlClasses')
                listCommands.append(commandLine)
            if adressesModified:
                listCommands.append(
                    utils_db.listTablesAdmin['adresses'][0])
                listCommands.append(
                    utils_db.qdf_table.format('adresses'))
            query_admin = utils_db.queryExecute(listCommands, query=query_admin)

            # on écrit les lignes :
            commandLine = utils_db.insertInto(
                'eleves', utils_db.listTablesAdmin['eleves'][1])
            query_admin = utils_db.queryExecute(
                {commandLine: lines}, query=query_admin)
            if replacementsModified:
                commandLine = utils_db.insertInto(
                    'replacements', utils_db.listTablesAdmin['replacements'][1])
                query_admin = utils_db.queryExecute(
                    {commandLine: self.otherTables['replacements']}, 
                    query=query_admin)
            if adressesModified:
                commandLine = utils_db.insertInto(
                    'adresses', utils_db.listTablesAdmin['adresses'][1])
                query_admin = utils_db.queryExecute(
                    {commandLine: self.otherTables['adresses']}, 
                    query=query_admin)
            if classesModified:
                commandLine = utils_db.insertInto(
                    'classes', utils_db.listTablesCommun['classes'][1])
                query_commun = utils_db.queryExecute(
                    {commandLine: self.otherTables['classes']}, 
                    query=query_commun)
            if usersModified:
                for commandLine_users in self.otherTables['users']:
                    query_users = utils_db.queryExecute(commandLine_users, query=query_users)

        finally:
            utils_db.endTransaction(
                query_admin, admin.db_admin, transactionOK_admin)
            if classesModified:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
            if usersModified:
                utils_db.endTransaction(
                    query_users, self.main.db_users, transactionOK_users)
        self.otherTables = {
            'replacements': [], 
            'classes': [], 
            'adresses': [], 
            'users': [], 
            }
        self.modified = False

class UsersUtilsDlg(QtWidgets.QDialog):
    """
    les outils de la page users.
    """
    def __init__(self, parent=None):
        super(UsersUtilsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'gest-direct-users'
        # des modifications ont été faites :
        self.modified = False

        # les messages :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)

        # import des élèves depuis SIECLE :
        self.studentsFromSIECLE_Button = QtWidgets.QToolButton()
        # réinitialiser des mots de passe élèves :
        self.studentsReinitPasswords_Button = QtWidgets.QToolButton()

        # import des profs depuis STSWeb :
        self.teachersFromSTSWeb_Button = QtWidgets.QToolButton()
        # réinitialiser des mots de passe profs :
        self.teachersReinitPasswords_Button = QtWidgets.QToolButton()
        # changer des mots de passe profs :
        self.teachersChangePasswords_Button = QtWidgets.QToolButton()

        # exporter les listes d'élèves en ODS :
        self.studentsExportAll2ods_Button = QtWidgets.QToolButton()
        # exporter les listes de profs en ODS :
        self.teachersExportAll2ods_Button = QtWidgets.QToolButton()

        # mise en forme des boutons :
        buttons = {
            self.studentsFromSIECLE_Button: (
                'xml-to-database-eleve', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Update students from SIECLE'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Update students from ElevesAvecAdresses.xml '
                    'file exported from SIECLE'), 
                self.studentsFromSIECLE), 
            self.studentsReinitPasswords_Button: (
                'password-eleve', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Reset password (students)'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Reset password (students selected from a list)'), 
                self.studentsReinitPasswords), 

            self.teachersFromSTSWeb_Button: (
                'xml-to-database-prof', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Update teachers from STSWeb'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Update teachers from sts_emp_RNE_aaaa.xml '
                    'file exported from STSWeb'), 
                self.teachersFromSTSWeb), 
            self.teachersReinitPasswords_Button: (
                'password-prof', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Reset password (teachers)'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Reset password (teachers selected from a list)'), 
                self.teachersReinitPasswords), 
            self.teachersChangePasswords_Button: (
                'password-prof', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Change password (teachers)'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Change password (teachers selected from a list)'), 
                self.teachersChangePasswords), 

            self.studentsExportAll2ods_Button: (
                'extension-ods', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Export Lists of students to ods'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Export Lists of students of the school '
                    'in an ODF spreadsheet file (* .ods)'), 
                self.studentsExportAll2ods), 
            self.teachersExportAll2ods_Button: (
                'extension-ods', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Export Lists of teachers to ods'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Export Lists of teachers of the school '
                    'in an ODF spreadsheet file (* .ods)'), 
                self.teachersExportAll2ods), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize']
        if self.main.height() < ICON_SIZE * 10:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])

        # séries d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.studentsFromSIECLE_Button)
        hLayout0.addStretch(1)
        hLayout1 = QtWidgets.QHBoxLayout()
        hLayout1.addWidget(self.studentsReinitPasswords_Button)
        hLayout1.addStretch(1)
        hLayout2 = QtWidgets.QHBoxLayout()
        hLayout2.addWidget(self.teachersFromSTSWeb_Button)
        hLayout2.addStretch(1)
        hLayout3 = QtWidgets.QHBoxLayout()
        hLayout3.addWidget(self.teachersReinitPasswords_Button)
        hLayout3.addWidget(self.teachersChangePasswords_Button)
        hLayout3.addStretch(1)
        hLayout4 = QtWidgets.QHBoxLayout()
        hLayout4.addWidget(self.studentsExportAll2ods_Button)
        if self.main.actualVersion['versionName'] != 'perso':
            hLayout4.addWidget(self.teachersExportAll2ods_Button)
        hLayout4.addStretch(1)

        # mise en place :
        self.statusBar = QtWidgets.QStatusBar()
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(hLayout0)
        vLayout.addLayout(hLayout1)
        if self.main.actualVersion['versionName'] != 'perso':
            labelSeparator = QtWidgets.QLabel('<hr width="80%">')
            vLayout.addWidget(labelSeparator)
            vLayout.addLayout(hLayout2)
            vLayout.addLayout(hLayout3)
        labelSeparator = QtWidgets.QLabel('<hr width="80%">')
        vLayout.addWidget(labelSeparator)
        vLayout.addLayout(hLayout4)
        vLayout.addStretch(1)
        grid = QtWidgets.QHBoxLayout()
        grid.addLayout(vLayout)
        grid.addLayout(messagesLayout)
        self.setLayout(grid)

    def studentsFromSIECLE(self):
        """
        importation des élèves depuis le fichier
        xml de SIECLE.
        """
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open xml or zip File'),
            admin.adminDir, 
            QtWidgets.QApplication.translate(
                'main', 'xml or zip files (*.xml *.zip)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        if QtCore.QFileInfo(fileName).suffix() == 'zip':
            result = utils_filesdirs.unzipFile(
                fileName, self.main.tempPath)
            fileName = self.main.tempPath + '/' + result[1]
        if fileName == '':
            return
        OK, data = admin.studentsFromSIECLE(self.main, fileName)
        # on recharge les données du deuxième onglet :
        if OK:
            self.parent.tabs[1].initialize(data=data)

    def studentsReinitPasswords(self):
        """
        on sélectionne des élèves
        à qui on remet le mot de passe initial
        """
        idsEleves = []
        dialog = admin.ListElevesDlg(parent=self.main, helpMessage=1)
        dialog.setWindowState(QtCore.Qt.WindowMaximized)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            idsEleves.append(id_eleve)
        if len(idsEleves) < 1:
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate('main', 'LIST EMPTY'), 
                editLog=True, 
                p=True)
            return
        commandLine_update = utils_functions.u(
            'UPDATE eleves SET Mdp="{0}" WHERE id={1}')
        tab = self.parent.tabs[1]
        for i in range(tab.baseList.count()):
            item = tab.baseList.item(i)
            data = item.data(QtCore.Qt.UserRole)
            id_eleve = data[1]
            if id_eleve in idsEleves:
                eleveInitialPassword = data[7]
                encMdp = utils_functions.doEncode(eleveInitialPassword, algo='sha256')
                commandLine_users = commandLine_update.format(encMdp, id_eleve)
                tab.otherTables['users'].append(commandLine_users)
                data[10] = False
                setOk(tab.passwordChanged, tab.size, False, toolTip=tab.toolTipPasswordBad)
            item.setData(QtCore.Qt.UserRole, data)
        tab.doModified()

    def teachersFromSTSWeb(self):
        """
        importation des profs depuis le fichier
        xml de SIECLE.
        """
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open xml or zip File'),
            admin.adminDir, 
            QtWidgets.QApplication.translate(
                'main', 'xml or zip files (*.xml *.zip)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        if QtCore.QFileInfo(fileName).suffix() == 'zip':
            result = utils_filesdirs.unzipFile(
                fileName, self.main.tempPath)
            fileName = self.main.tempPath + '/' + result[1]
        if fileName == '':
            return
        OK, data = admin.teachersFromSTSWeb(self.main, fileName)
        # on recharge les données du deuxième onglet :
        if OK:
            self.parent.tabs[0].initialize(data=data)

    def teachersReinitPasswords(self):
        """
        on sélectionne des profs
        à qui on remet le mot de passe initial
        """
        idsProfs = []
        dialog = admin.ListProfsDlg(parent=self.main, helpMessage=1)
        dialog.setWindowState(QtCore.Qt.WindowMaximized)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_prof = item.data(QtCore.Qt.UserRole)
            idsProfs.append(id_prof)
        if len(idsProfs) < 1:
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate('main', 'LIST EMPTY'), 
                editLog=True, 
                p=True)
            return
        commandLine_update = utils_functions.u(
            'UPDATE profs SET Mdp="{0}" WHERE id={1}')
        tab = self.parent.tabs[0]
        for i in range(tab.baseList.count()):
            item = tab.baseList.item(i)
            data = item.data(QtCore.Qt.UserRole)
            id_prof = data[1]
            if id_prof in idsProfs:
                profInitialPassword = data[7]
                encMdp = utils_functions.doEncode(profInitialPassword, algo='sha256')
                commandLine_users = commandLine_update.format(encMdp, id_prof)
                tab.otherTables['users'].append(commandLine_users)
                data[8] = False
                setOk(tab.passwordChanged, tab.size, False, toolTip=tab.toolTipPasswordBad)
            item.setData(QtCore.Qt.UserRole, data)
        tab.doModified()

    def teachersChangePasswords(self):
        """
        on sélectionne des profs
        à qui on change le mot de passe
        """
        idsProfs = []
        dialog = admin.ListProfsDlg(parent=self.main, helpMessage=1)
        dialog.setWindowState(QtCore.Qt.WindowMaximized)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_prof = item.data(QtCore.Qt.UserRole)
            idsProfs.append(id_prof)
        if len(idsProfs) < 1:
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate('main', 'LIST EMPTY'), 
                editLog=True, 
                p=True)
            return
        newMdp, OK = QtWidgets.QInputDialog.getText(
            self.main, 
            utils.PROGNAME, 
            QtWidgets.QApplication.translate('main', 'New password:'),
            QtWidgets.QLineEdit.Normal, 
            '')
        if not(OK):
            return
        encMdp = utils_functions.doEncode(newMdp, algo='sha256')
        commandLine_update = utils_functions.u(
            'UPDATE profs SET Mdp="{0}" WHERE id={1}')
        tab = self.parent.tabs[0]
        for i in range(tab.baseList.count()):
            item = tab.baseList.item(i)
            data = item.data(QtCore.Qt.UserRole)
            id_prof = data[1]
            if id_prof in idsProfs:
                profInitialPassword = data[7]
                commandLine_users = commandLine_update.format(encMdp, id_prof)
                tab.otherTables['users'].append(commandLine_users)
                if newMdp != profInitialPassword:
                    data[8] = True
                    setOk(tab.passwordChanged, tab.size, True, toolTip=tab.toolTipPasswordOk)
                else:
                    data[8] = False
                    setOk(tab.passwordChanged, tab.size, False, toolTip=tab.toolTipPasswordBad)
            item.setData(QtCore.Qt.UserRole, data)
        tab.doModified()

    def studentsExportAll2ods(self):
        """
        export de la liste des élèves en fichier ODS.
        """
        odsTitle = QtWidgets.QApplication.translate(
            'main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u('{0}/eleves-{1}.ods').format(
            admin.adminDir + '/fichiers', self.main.actualVersion['versionName'])
        odsExt = QtWidgets.QApplication.translate(
            'main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName != '':
            admin.studentsExportAll2ods(self.main, fileName)

    def teachersExportAll2ods(self):
        """
        export de la liste des élèves en fichier ODS.
        """
        odsTitle = QtWidgets.QApplication.translate(
            'main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u('{0}/profs-{1}.ods').format(
            admin.adminDir + '/fichiers', self.main.actualVersion['versionName'])
        odsExt = QtWidgets.QApplication.translate(
            'main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName != '':
            admin.teachersExportAll2ods(self.main, fileName)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.modified = False





###########################################################"
#   ADRESSES
#   ADDRESSES
###########################################################"

class AddressesDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer la liste des adresses.
    """
    def __init__(self, parent=None):
        super(AddressesDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'gest-adresses'
        # des modifications ont été faites :
        self.modified = False
        # un dico pour remplacer temporairement la table :
        self.addresses = {
            'CLASSES': {'ALL': [], }, 
            'DATA': {}, 
            }
        # pour ne pas lancer l'édition 2 fois :
        self.fromSoft = False

        # la zone de sélection :
        self.filterComboBox = QtWidgets.QComboBox()
        self.filterComboBox.currentIndexChanged.connect(
            self.filterComboBoxChanged)
        self.whoSelectionWidget = QtWidgets.QTreeWidget()
        self.whoSelectionWidget.setRootIsDecorated(False)
        self.whoSelectionWidget.setColumnCount(5)
        self.headers = [
            QtWidgets.QApplication.translate('main', 'Student'),
            QtWidgets.QApplication.translate('main', 'perso'),
            QtWidgets.QApplication.translate('main', '1'),
            QtWidgets.QApplication.translate('main', '2'),
            QtWidgets.QApplication.translate('main', 'other'),
            ]
        self.whoSelectionWidget.header().resizeSection(0, 200)
        self.columnsUse = {}
        for i in range(4):
            self.whoSelectionWidget.header().resizeSection(i + 1, 80)
            self.columnsUse[i + 1] = False
        self.whoSelectionWidget.setHeaderLabels(self.headers)
        self.whoSelectionWidget.header().setDefaultAlignment(
            QtCore.Qt.AlignCenter)
        self.whoSelectionWidget.currentItemChanged.connect(
            self.doCurrentItemChanged)
        self.whoSelectionWidget.header().sectionDoubleClicked.connect(
            self.doSwitchUse)
        self.whoSelectionWidget.itemClicked.connect(self.doItemClicked)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.filterComboBox,     1, 0)
        baseGrid.addWidget(self.whoSelectionWidget, 2, 0)

        # la zone d'édition :
        self.saveToolTip = QtWidgets.QApplication.translate(
            'main', 'Save changes')
        # adresse élève :
        text = QtWidgets.QApplication.translate(
            'main', 'Personal address of the student:')
        studentAddressLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.studentAddressEdit = QtWidgets.QTextEdit()
        self.studentAddressEdit.textChanged.connect(self.doEdited)
        self.studentAddressSave = QtWidgets.QPushButton(
            utils.doIcon('document-save'), '')
        # adresse 1 :
        text = QtWidgets.QApplication.translate(
            'main', 'First address:')
        firstAddressLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.firstAddressEdit = QtWidgets.QTextEdit()
        self.firstAddressEdit.textChanged.connect(self.doEdited)
        self.firstAddressSave = QtWidgets.QPushButton(
            utils.doIcon('document-save'), '')
        # adresse 2 :
        text = QtWidgets.QApplication.translate(
            'main', 'Second address:')
        secondAddressLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.secondAddressEdit = QtWidgets.QTextEdit()
        self.secondAddressEdit.textChanged.connect(self.doEdited)
        self.secondAddressSave = QtWidgets.QPushButton(
            utils.doIcon('document-save'), '')
        # adresse 0 :
        text = QtWidgets.QApplication.translate(
            'main', 'Other address:')
        otherAddressLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.otherAddressEdit = QtWidgets.QTextEdit()
        self.otherAddressEdit.textChanged.connect(self.doEdited)
        self.otherAddressSave = QtWidgets.QPushButton(
            utils.doIcon('document-save'), '')
        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addWidget(studentAddressLabel,     1, 0, 1, 2)
        editGrid.addWidget(self.studentAddressEdit, 2, 1)
        editGrid.addWidget(self.studentAddressSave, 2, 2)
        editGrid.addWidget(firstAddressLabel,       3, 0, 1, 2)
        editGrid.addWidget(self.firstAddressEdit,   4, 1)
        editGrid.addWidget(self.firstAddressSave,   4, 2)
        editGrid.addWidget(secondAddressLabel,      5, 0, 1, 2)
        editGrid.addWidget(self.secondAddressEdit,  6, 1)
        editGrid.addWidget(self.secondAddressSave,  6, 2)
        editGrid.addWidget(otherAddressLabel,       7, 0, 1, 2)
        editGrid.addWidget(self.otherAddressEdit,   8, 1)
        editGrid.addWidget(self.otherAddressSave,   8, 2)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(editGrid, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.fields = {
            self.studentAddressEdit: ('TEXT', 4),
            self.firstAddressEdit: ('TEXT', 5),
            self.secondAddressEdit: ('TEXT', 6),
            self.otherAddressEdit: ('TEXT', 7)}
        self.saveButtons = (
            self.studentAddressSave,
            self.firstAddressSave,
            self.secondAddressSave,
            self.otherAddressSave, )
        for button in self.saveButtons:
            button.setToolTip('')
            button.setEnabled(False)
            button.clicked.connect(self.doSaveAddress)
        self.initialize()
        QtCore.QTimer.singleShot(10, self.doResize)

    def doResize(self):
        """
        déclenchée via un timer, permet de redimensionner
        les colonnes en fonction de la place disponible.
        """
        firstColumnDefaultWidth = 200
        othersColumnsWidth = 80
        lastColumnWidth = self.whoSelectionWidget.columnWidth(
            self.whoSelectionWidget.columnCount() - 1)
        firstColumnWidth = firstColumnDefaultWidth + lastColumnWidth - othersColumnsWidth
        if firstColumnWidth > firstColumnDefaultWidth:
            self.whoSelectionWidget.header().resizeSection(0, firstColumnWidth)

    def initialize(self, data=[]):
        """
        remplissage du comboBox des classes (filterComboBox)
        et du dico self.addresses.
        Si la liste data n'est pas vide, c'est qu'on appelle
        depuis l'import SIECLE.
        """
        if len(data) > 0:
            # on récupère l'état des cases à cocher :
            oldUses = {}
            for id_eleve in self.addresses['DATA']:
                itemData = self.addresses['DATA'][id_eleve]
                eleve_id = itemData[2]
                for i in range(4):
                    addressData = itemData[4 + i]
                    if len(addressData) > 0:
                        [nom1, nom2, adresse, use, state] = self.text2list(addressData)
                        oldUses[(id_eleve, state)] = use
        # on vide :
        self.filterComboBox.clear()
        self.addresses = {
            'CLASSES': {'ALL': [], }, 
            'DATA': {}, 
            }
        # besoin d'un dico :
        dicoAdresses = {}

        # filtre "toutes les classes" :
        self.filterComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'All the classes'))
        # on récupère la liste des classes :
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            classe = query_commun.value(1)
            self.filterComboBox.addItem(classe)
            self.addresses['CLASSES'][classe] = []
        # lecture de admin.adresses ou de la liste data :
        query_admin = utils_db.query(admin.db_admin)
        if len(data) > 0:
            # on récupère d'après la nouvelle liste :
            self.doModified()
            for line in data:
                id_eleve = line[0]
                eleve_id = line[1]
                state = line[2]
                nom1 = line[3]
                nom2 = line[4]
                adresse = line[5]
                use = line[6]
                use = oldUses.get((id_eleve, state), use)
                if not(id_eleve in dicoAdresses):
                    dicoAdresses[id_eleve] = {}
                dicoAdresses[id_eleve][state] = (
                    nom1, nom2, adresse, use, eleve_id)
        else:
            # on lit la table admin.adresses :
            commandLine_admin = utils_db.q_selectAllFromOrder.format(
                'adresses', 'id_eleve')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                eleve_id = int(query_admin.value(1))
                state = int(query_admin.value(2))
                nom1 = query_admin.value(3)
                nom2 = query_admin.value(4)
                adresse = query_admin.value(5)
                use = int(query_admin.value(6))
                if not(id_eleve in dicoAdresses):
                    dicoAdresses[id_eleve] = {}
                dicoAdresses[id_eleve][state] = (
                    nom1, nom2, adresse, use, eleve_id)
        # lecture de admin.eleves et remplissage de self.addresses :
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            id_eleve = int(record[0])
            eleveName = record[2]
            eleveFirstName = record[3]
            eleveClasse = record[4]
            adresse0 = ''
            adresse1 = ''
            adresse2 = ''
            adresse3 = ''
            eleve_id = -1
            if not(id_eleve in dicoAdresses):
                dicoAdresses[id_eleve] = {}
            else:
                for state in dicoAdresses[id_eleve]:
                    (nom1, nom2, adresse, use, eleve_id) = dicoAdresses[id_eleve][state]
                    adresse = self.list2text(
                        nom1, nom2, adresse, use, state)
                    if state in (1, 2):
                        adresse0 = adresse
                    elif state in (10, 11):
                        adresse1 = adresse
                    elif state in (20, 21):
                        adresse2 = adresse
                    elif state in (50, 51):
                        adresse1 = adresse
                    elif state in (100, 101):
                        adresse3 = adresse
            itemText = utils_functions.u('{0} {1} [{2}]').format(
                eleveName, eleveFirstName, eleveClasse)
            itemData = [
                itemText, 
                id_eleve, eleve_id, eleveClasse, 
                adresse0, adresse1, adresse2, adresse3]
            # si la classe n'est pas dans le dico, on prévient :
            if not(eleveClasse in self.addresses['CLASSES']):
                m1 = QtWidgets.QApplication.translate(
                    'main', 
                    'The <b>{0}</b> class<br/>'
                     ' (attributed to student <b>{1} {2}</b>)<br/>'
                     ' does not exist!')
                m1 = utils_functions.u(m1).format(
                    eleveClasse, eleveName, eleveFirstName)
                m2 = QtWidgets.QApplication.translate(
                    'main', 
                    'Remember to assign a valid class or to create this class.')
                message = utils_functions.u(
                    '<p align="center">{0}</p>'
                    '<p align="center">{1}</p>'
                    '<p align="center">{2}</p>'
                    '<p></p>').format(utils.SEPARATOR_LINE, m1, m2)
                utils_functions.messageBox(
                    self.main, level='warning', message=message)
                self.addresses['CLASSES'][eleveClasse] = []
            self.addresses['CLASSES'][eleveClasse].append(id_eleve)
            self.addresses['CLASSES']['ALL'].append(id_eleve)
            self.addresses['DATA'][id_eleve] = itemData
        self.filterComboBoxChanged(0)

    def text2list(self, text):
        result = text.split('<$>')
        if len(result) > 4:
            result[3] = int(result[3])
            result[4] = int(result[4])
        return result

    def list2text(self, nom1, nom2, adresse, use, state):
        result = utils_functions.u(
            '{0}<$>{1}<$>{2}<$>{3}<$>{4}').format(
                nom1, nom2, adresse, use, state)
        return result

    def createItem(self, id_eleve):
        itemData = self.addresses['DATA'][id_eleve]
        item = QtWidgets.QTreeWidgetItem()
        item.setData(0, QtCore.Qt.UserRole, id_eleve)
        item.setData(0, QtCore.Qt.DisplayRole, itemData[0])
        for i in range(4):
            addressData = itemData[4 + i]
            if len(addressData) > 0:
                [nom1, nom2, adresse, use, state] = self.text2list(addressData)
                if use > 0:
                    item.setCheckState(1 + i, QtCore.Qt.Checked)
                else:
                    item.setCheckState(1 + i, QtCore.Qt.Unchecked)
        return item

    def filterComboBoxChanged(self, index=-1):
        """
        mise à jour de la liste selon la classe sélectionnée
        """
        if index < 0:
            index = self.filterComboBox.currentIndex()
        else:
            self.filterComboBox.setCurrentIndex(index)
        selected = self.filterComboBox.currentText()
        if index == 0:
            selected = 'ALL'
        self.whoSelectionWidget.clear()
        if not(selected in self.addresses['CLASSES']):
            return
        firstItem = None
        for id_eleve in self.addresses['CLASSES'][selected]:
            itemData = self.addresses['DATA'][id_eleve]
            item = self.createItem(id_eleve)
            self.whoSelectionWidget.addTopLevelItem(item)
            if firstItem == None:
                firstItem = item
        if firstItem != None:
            self.whoSelectionWidget.setCurrentItem(firstItem)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et 
        les boutons d'édition à jour.
        On a besoin d'un try except en cas de changement 
        de sélection de classe.
        """
        try:
            id_eleve = current.data(0, QtCore.Qt.UserRole)
            itemData = self.addresses['DATA'][id_eleve]
        except:
            id_eleve = -1
            itemData = ['', -1, -1, '', '', '', '', '']
        self.fromSoft = True
        for field in self.fields:
            (what, index) = self.fields[field]
            if what == 'LINE':
                field.setText(utils_functions.u(itemData[index]))
            elif what == 'TEXT':
                text = itemData[index]
                if text != '':
                    data = self.text2list(text)
                    nom1 = data[0]
                    nom2 = data[1]
                    adresse = data[2]
                    use = data[3]
                    noms = ''
                    if nom1 != '':
                        noms = utils_functions.u(
                            '{0}{1}').format(noms, nom1)
                    if nom2 != '':
                        noms = utils_functions.u(
                            '{0}|{1}').format(noms, nom2)
                    adresse = utils_functions.u(
                        '{0}###{1}').format(noms, adresse)
                    l = [e for e in adresse.split('|') if e != '']
                    adresse = utils_functions.u('\n').join(l)
                    if len(l) < 5:
                        adresse = adresse.replace('###', '\n\n')
                    else:
                        adresse = adresse.replace('###', '\n')
                    text = utils_functions.u('{0}').format(adresse)
                field.setPlainText(text)
        for button in self.saveButtons:
            button.setToolTip('')
            button.setEnabled(False)
        self.fromSoft = False

    def doSwitchUse(self, column):
        if column < 1:
            return
        self.columnsUse[column] = not(self.columnsUse[column])
        if self.columnsUse[column]:
            newUse = 1
            newCheck = QtCore.Qt.Checked
        else:
            newUse = 0
            newCheck = QtCore.Qt.Unchecked
        parent = self.whoSelectionWidget.invisibleRootItem()
        section = column - 1
        for i in range(parent.childCount()):
            item = parent.child(i)
            id_eleve = item.data(0, QtCore.Qt.UserRole)
            addressData = self.addresses['DATA'][id_eleve][4 + section]
            if len(addressData) > 0:
                [nom1, nom2, adresse, use, state] = self.text2list(addressData)
                addressData = self.list2text(
                    nom1, nom2, adresse, newUse, state)
                self.addresses['DATA'][id_eleve][4 + section] = addressData
                item.setCheckState(1 + section, newCheck)
        self.doModified()

    def doItemClicked(self, item, column):
        if column < 1:
            return
        section = column - 1
        id_eleve = item.data(0, QtCore.Qt.UserRole)
        addressData = self.addresses['DATA'][id_eleve][4 + section]
        if len(addressData) > 0:
            [nom1, nom2, adresse, use, state] = self.text2list(addressData)
            if item.checkState(1 + section) == QtCore.Qt.Checked:
                newUse = 1
            else:
                newUse = 0
            if newUse != use:
                self.doModified()
            addressData = self.list2text(
                nom1, nom2, adresse, newUse, state)
            self.addresses['DATA'][id_eleve][4 + section] = addressData

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        if self.fromSoft:
            return
        (what, index) = self.fields.get(self.sender(), ('', 0))
        self.saveButtons[index - 4].setEnabled(True)
        self.saveButtons[index - 4].setToolTip(self.saveToolTip)

    def doSaveAddress(self):
        """
        l'utilisateur a demandé à enregistrer une 
        modification d'adresse.
        Ça peut être aussi une création ou suppression.
        """
        if not(self.sender() in self.saveButtons):
            return
        # récupération de l'adresse modifiée :
        index = self.saveButtons.index(self.sender())
        self.saveAddress(index)

    def saveAddress(self, index):
        """
        l'utilisateur a demandé à enregistrer une 
        modification d'adresse.
        Ça peut être aussi une création ou suppression.
        """
        def calcNewAddress(newAddress):
            newAddress = newAddress.replace('\n', '|')
            newAddress = newAddress.split('||')
            nom1, nom2 = '', ''
            noms = newAddress[0].split('|')
            if len(noms) > 0:
                nom1 = noms[0]
            if len(noms) > 1:
                nom2 = noms[1]
            newAddress = utils_functions.u('||').join(newAddress[1:])
            return newAddress, nom1, nom2

        textEdits = (
            self.studentAddressEdit,
            self.firstAddressEdit,
            self.secondAddressEdit,
            self.otherAddressEdit)
        newAddress, nom1, nom2 = calcNewAddress(textEdits[index].toPlainText())

        # mise à jour de l'item :
        item = self.whoSelectionWidget.currentItem()
        id_eleve = item.data(0, QtCore.Qt.UserRole)
        itemData = self.addresses['DATA'][id_eleve]
        data = self.text2list(itemData[4 + index])
        addressCreated = (len(data) < 2)
        # si on supprime une adresse, il faut récupérer 
        # les autres modifications en cours, sinon elles seront perdues 
        # avec le réaffichage (pour supprimer la case à cocher) :
        addressDeleted = (len(newAddress) < 1)
        otherEdits = {}
        if addressDeleted:
            for i in range(4):
                if i != index:
                    if self.saveButtons[i].isEnabled():
                        otherEdits[i] = textEdits[i].toPlainText()
        if addressCreated:
            use = 1
            if index == 0:
                state = 1
            elif index == 1:
                if len(self.secondAddressEdit.toPlainText()) < 1:
                    state = 50
                else:
                    state = 10
            elif index == 2:
                state = 20
            else:
                state = 100
        else:
            use = int(data[3])
            state = int(data[4])
        if addressDeleted:
            itemData[4 + index] = ''
        else:
            itemData[4 + index] = self.list2text(
                nom1, nom2, newAddress, use, state)
        self.addresses['DATA'][id_eleve] = itemData
        eleve_id = self.addresses['DATA'][id_eleve][2]
        # mise à jour du reste :
        if addressCreated:
            item.setCheckState(1 + index, QtCore.Qt.Checked)
        elif addressDeleted:
            # on recrée l'item pour supprimer la case à cocher :
            itemIndex = self.whoSelectionWidget.indexFromItem(item).row()
            item = self.whoSelectionWidget.takeTopLevelItem(itemIndex)
            del(item)
            item = self.createItem(id_eleve)
            self.whoSelectionWidget.insertTopLevelItem(itemIndex, item)
            self.whoSelectionWidget.setCurrentItem(item)
            # on restaure les autres textes modifiés :
            for i in otherEdits:
                textEdits[i].setPlainText(otherEdits[i])
        # terminé :
        self.saveButtons[index].setEnabled(False)
        self.saveButtons[index].setToolTip('')
        self.doModified()

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        # on enregistre les dernières modifications manuelles :
        for i in range(4):
            if self.saveButtons[i].isEnabled():
                self.saveAddress(i)
        if not(self.modified):
            return

        data = []
        for id_eleve in self.addresses['DATA']:
            itemData = self.addresses['DATA'][id_eleve]
            eleve_id = itemData[2]
            for i in range(4):
                addressData = itemData[4 + i]
                if len(addressData) > 0:
                    [nom1, nom2, adresse, use, state] = self.text2list(addressData)
                    data.append(
                        (id_eleve, eleve_id, state, nom1, nom2, adresse, use))
        self.parent.modifiedTables['DATABASES']['admin'] = True
        self.parent.modifiedTables['admin']['adresses'] = True
        query_admin, transactionOK_admin = utils_db.queryTransactionDB(
            admin.db_admin)
        try:
            # on écrit les lignes :
            commandLine_admin = utils_db.qdf_table.format('adresses')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            if len(data) > 0:
                commandLine_admin = utils_db.insertInto(
                    'adresses', utils_db.listTablesAdmin['adresses'][1])
                query_admin = utils_db.queryExecute(
                    {commandLine_admin: data}, query=query_admin)
        finally:
            utils_db.endTransaction(
                query_admin, admin.db_admin, transactionOK_admin)
        self.modified = False

class AddressesUtilsDlg(QtWidgets.QDialog):
    """
    les outils de la page adresses.
    """
    def __init__(self, parent=None):
        super(AddressesUtilsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'gest-adresses'
        # des modifications ont été faites :
        self.modified = False

        # les messages :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)

        # import depuis SIECLE :
        self.fromSIECLE_Button = QtWidgets.QToolButton()
        self.addressesPhonesAndMails2ods_Button = QtWidgets.QToolButton()

        # mise en forme des boutons :
        buttons = {
            self.fromSIECLE_Button: (
                'xml-to-database', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Update addresses from SIECLE'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Update addresses from ResponsablesAvecAdresses.xml '
                    'file exported from SIECLE'), 
                self.addressesFromSIECLE), 
            self.addressesPhonesAndMails2ods_Button: (
                'phones-to-ods', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Addresses, Phones and Mails SIECLE to ODS'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Export as ODF data (addresses + phone numbers + mails) '
                    'in the file ResponsablesAvecAdresses.xml'), 
                self.addressesPhonesAndMails2ods), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize'] * 2
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])

        # première série d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.fromSIECLE_Button)
        hLayout0.addStretch(1)
        hLayout1 = QtWidgets.QHBoxLayout()
        hLayout1.addWidget(self.addressesPhonesAndMails2ods_Button)
        hLayout1.addStretch(1)

        # mise en place :
        self.statusBar = QtWidgets.QStatusBar()
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(hLayout0)
        vLayout.addLayout(hLayout1)
        vLayout.addStretch(1)
        grid = QtWidgets.QHBoxLayout()
        grid.addLayout(vLayout)
        grid.addLayout(messagesLayout)
        self.setLayout(grid)

    def addressesFromSIECLE(self):
        """
        importation des adresses depuis le fichier
        xml de SIECLE.
        """
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open xml or zip File'),
            admin.adminDir, 
            QtWidgets.QApplication.translate(
                'main', 'xml or zip files (*.xml *.zip)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        if QtCore.QFileInfo(fileName).suffix() == 'zip':
            result = utils_filesdirs.unzipFile(
                fileName, self.main.tempPath)
            fileName = self.main.tempPath + '/' + result[1]
        if fileName == '':
            return
        OK, data = admin.addressesFromSIECLE(self.main, fileName)
        # on recharge les données du premier onglet :
        if OK:
            self.parent.tabs[0].initialize(data=data)

    def addressesPhonesAndMails2ods(self):
        """
        importation des numéros de tel depuis le fichier
        xml de SIECLE et export des adresses et des 
        numéros de téléphone en fichier ODS.
        """
        # sélection du fichier ResponsablesAvecAdresses :
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open xml or zip File'),
            admin.adminDir, 
            QtWidgets.QApplication.translate(
                'main', 'xml or zip files (*.xml *.zip)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        if QtCore.QFileInfo(fileName).suffix() == 'zip':
            result = utils_filesdirs.unzipFile(
                fileName, self.main.tempPath)
            fileName = self.main.tempPath + '/' + result[1]
        if fileName == '':
            return

        # récupération des n° de tel contenus dans le fichier :
        OK, phonesAndMailsData = admin.phonesAndMailsFromSIECLE(
            self.main, fileName, msgFin=False)
        if not(OK):
            return

        # récupération des adresses dans la table admin.adresses :
        adressesData = []
        dicoAdresses = {}
        for line in phonesAndMailsData:
            id_eleve = line[0]
            dicoAdresses[id_eleve] = {}
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFromOrder.format(
            'adresses', 'id_eleve')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            state = int(query_admin.value(2))
            nom1 = query_admin.value(3)
            nom2 = query_admin.value(4)
            adresse = query_admin.value(5)
            use = int(query_admin.value(6))
            if state in (10, 11):
                numAdress = 1
            elif state in (20, 21):
                numAdress = 2
            elif state in (50, 51):
                numAdress = 1
            elif state in (100, 101):
                numAdress = 3
            else:
                # on ne prend pas les adresses persos :
                use = 0
            if (use > 0) and (id_eleve in dicoAdresses):
                dicoAdresses[id_eleve][numAdress] = (
                    nom1, nom2, adresse)
        # remplissage de addressesData :
        for aLine in phonesAndMailsData:
            id_eleve = aLine[0]
            line = [id_eleve, aLine[1], aLine[2], aLine[3]]
            for i in range(3):
                (nom1, nom2, adresse) = dicoAdresses[id_eleve].get(i + 1, ('', '', ''))
                noms = ''
                if nom1 != '':
                    noms = utils_functions.u(
                        '{0}{1}').format(noms, nom1)
                if nom2 != '':
                    noms = utils_functions.u(
                        '{0}|{1}').format(noms, nom2)
                adresse = utils_functions.u(
                    '{0}###{1}').format(noms, adresse)
                if len(adresse) < 5:
                    line.append('')
                else:
                    l = [e for e in adresse.split('|') if e != '']
                    adresse = utils_functions.u('\n').join(l)
                    if len(l) < 5:
                        adresse = adresse.replace('###', '\n\n')
                    else:
                        adresse = adresse.replace('###', '\n')
                    line.append(utils_functions.u('{0}').format(adresse))
            adressesData.append(line)

        # on peut créer le fichier ODS :
        odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u(
            '{0}/fichiers/responsables.ods').format(admin.adminDir)
        odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(self.main, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        utils_functions.doWaitCursor()
        exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
        import utils_export
        try:
            addressesTableTitle = QtWidgets.QApplication.translate('main', 'addresses')
            phonesTableTitle = QtWidgets.QApplication.translate('main', 'phones')
            mailsTableTitle = QtWidgets.QApplication.translate('main', 'mails')
            nameTitle = QtWidgets.QApplication.translate('main', 'NAME')
            firstNameTitle = QtWidgets.QApplication.translate('main', 'FIRSTNAME')
            classTitle = QtWidgets.QApplication.translate('main', 'CLASS')
            addressTitle = QtWidgets.QApplication.translate('main', 'ADDRESS')
            otherAddressTitle = QtWidgets.QApplication.translate('main', 'OTHER ADDRESS')
            legalTitle = QtWidgets.QApplication.translate('main', 'LEGAL REPRESENTATIVE')
            otherTitle = QtWidgets.QApplication.translate('main', 'OTHER REPRESENTATIVE')
            persoTitle = QtWidgets.QApplication.translate('main', 'PERSONAL')
            portableTitle = QtWidgets.QApplication.translate('main', 'PORTABLE')
            proTitle = QtWidgets.QApplication.translate('main', 'PROFESSIONAL')
            mailTitle = QtWidgets.QApplication.translate('main', 'MAIL ADDRESS')

            # onglet addresses :
            columns = [
                nameTitle, firstNameTitle, classTitle, 
                utils_functions.u('{0} 1').format(addressTitle), 
                utils_functions.u('{0} 2').format(addressTitle), 
                otherAddressTitle]
            theList = []
            line = []
            for title in columns:
                lineDico = {'value': title, 'bold': True}
                if title in (columns[0], ):
                    lineDico['columnWidth'] = '5.0cm'
                elif title in (columns[1], ):
                    lineDico['columnWidth'] = '4.0cm'
                elif title in (columns[2], ):
                    lineDico['columnWidth'] = '1.8cm'
                else:
                    lineDico['columnWidth'] = '10.0cm'
                line.append(lineDico)
            theList.append(line)
            for aLine in adressesData:
                line = []
                i = 0
                for cell in aLine[1:]:
                    if i in (0, 1, ):
                        line.append({'value': cell, })
                    elif i in (2, ):
                        line.append({'value': cell, 'alignHorizontal': 'center'})
                    else:
                        line.append({'value': cell, 'wrap': 'wrap', 'rowHeight': '3.0cm'})
                    i += 1
                theList.append(line)
            exportDatas['addresses'] = theList
            exportDatas['TABLES_LIST'].append('addresses')
            exportDatas['TABLES_PARAMS']['addresses'] = {'title': addressesTableTitle}

            # onglet phones :
            columns = [
                nameTitle, firstNameTitle, classTitle, 
                utils_functions.u('{0} 1').format(legalTitle), persoTitle, portableTitle, proTitle, 
                utils_functions.u('{0} 2').format(legalTitle), persoTitle, portableTitle, proTitle, 
                otherTitle, persoTitle, portableTitle, proTitle]
            theList = []
            line = []
            for title in columns:
                lineDico = {'value': title, 'bold': True}
                if title in (columns[0], ):
                    lineDico['columnWidth'] = '5.0cm'
                elif title in (columns[1], ):
                    lineDico['columnWidth'] = '4.0cm'
                elif title in (columns[2], ):
                    lineDico['columnWidth'] = '1.8cm'
                elif title in (columns[3], columns[7], columns[11], ):
                    lineDico['columnWidth'] = '7.0cm'
                else:
                    lineDico['columnWidth'] = '3.5cm'
                line.append(lineDico)
            theList.append(line)
            for aLine in phonesAndMailsData:
                line = []
                i = 0
                for cell in aLine[1:]:
                    if i in (0, 1, 3, 8, 13):
                        line.append({'value': cell, })
                    elif i in (2, 4, 5, 6, 9, 10, 11, 14, 15, 16):
                        line.append({'value': cell, 'alignHorizontal': 'center'})
                    i += 1
                theList.append(line)
            exportDatas['phones'] = theList
            exportDatas['TABLES_LIST'].append('phones')
            exportDatas['TABLES_PARAMS']['phones'] = {'title': phonesTableTitle}

            # onglet mails :
            columns = [
                nameTitle, firstNameTitle, classTitle, 
                utils_functions.u('{0} 1').format(legalTitle), mailTitle, 
                utils_functions.u('{0} 2').format(legalTitle), mailTitle, 
                otherTitle, mailTitle]
            theList = []
            line = []
            for title in columns:
                lineDico = {'value': title, 'bold': True}
                if title in (columns[0], ):
                    lineDico['columnWidth'] = '5.0cm'
                elif title in (columns[1], ):
                    lineDico['columnWidth'] = '4.0cm'
                elif title in (columns[2], ):
                    lineDico['columnWidth'] = '1.8cm'
                elif title in (columns[3], columns[5], columns[7], ):
                    lineDico['columnWidth'] = '7.0cm'
                else:
                    lineDico['columnWidth'] = '6.0cm'
                line.append(lineDico)
            theList.append(line)
            for aLine in phonesAndMailsData:
                line = []
                i = 0
                for cell in aLine[1:]:
                    if i in (0, 1, 3, 7, 8, 12, 13, 17):
                        line.append({'value': cell, })
                    elif i in (2, 17):
                        line.append({'value': cell, 'alignHorizontal': 'center'})
                    i += 1
                theList.append(line)
            exportDatas['mails'] = theList
            exportDatas['TABLES_LIST'].append('mails')
            exportDatas['TABLES_PARAMS']['mails'] = {'title': mailsTableTitle}

            # on exporte en ods :
            utils_functions.restoreCursor()
            utils_export.exportDic2ods(self.main, exportDatas, fileName, msgFin=False)
            utils_functions.afficheMsgFinOpen(self.main, fileName)
            utils_functions.doWaitCursor()
        except:
            utils_functions.afficheMsgPb(self.main)
        finally:
            utils_functions.restoreCursor()

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.modified = False





###########################################################"
#   SITE WEB
#   WEBSITE
###########################################################"

class WebSiteSetupDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer le site web (installation).
    """
    def __init__(self, parent=None):
        super(WebSiteSetupDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'website-setup'
        # des modifications ont été faites :
        self.modified = False

        self.size = 18
        testText = QtWidgets.QApplication.translate('main', 'Test')
        postText = QtWidgets.QApplication.translate('main', 'Send or update')

        # FTP ou SFTP :
        # ICI SFTP
        text = QtWidgets.QApplication.translate('main', 'Use SFTP:')
        sftpLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.sftpCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'SFTP'))
        self.sftpIsModified = False

        # Adresse du serveur FTP :
        text = QtWidgets.QApplication.translate('main', 'BaseSiteFtp:')
        baseSiteFtpLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.baseSiteFtpEdit = QtWidgets.QLineEdit()
        self.baseSiteFtpValid = QtWidgets.QLabel()
        self.baseSiteFtpValid.setMaximumSize(self.size, self.size)
        self.baseSiteFtpValid.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.baseSiteFtpTest = QtWidgets.QPushButton(testText)
        self.baseSiteFtpTest.clicked.connect(self.doTest)

        # Utilisateur FTP :
        text = QtWidgets.QApplication.translate('main', 'FTP User:')
        userFtpLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.userFtpEdit = QtWidgets.QLineEdit()
        # Mot de passe FTP :
        text = QtWidgets.QApplication.translate('main', 'FTP Password:')
        mdpFtpLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.mdpFtpEdit = QtWidgets.QLineEdit()
        self.mdpFtpValid = QtWidgets.QLabel()
        self.mdpFtpValid.setMaximumSize(self.size, self.size)
        self.mdpFtpValid.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.mdpFtpTest = QtWidgets.QPushButton(testText)
        self.mdpFtpTest.clicked.connect(self.doTest)

        # Port FTP :
        text = QtWidgets.QApplication.translate('main', 'FTP Port:')
        ftpPortLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        text = QtWidgets.QApplication.translate(
            'main', 
            'The default FTP port is 21. '
            '<br/>Do not change unless you are sure it is necessary.')
        ftpPortAvert = QtWidgets.QLabel(utils_functions.u(
            '{0}').format(text))
        self.ftpPortSpinBox = QtWidgets.QSpinBox()
        self.ftpPortSpinBox.setRange(1, 65536)
        self.ftpPortIsModified = False

        # Chemin du répertoire public :
        text = QtWidgets.QApplication.translate('main', 'DirSitePublic:')
        dirPublicLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.dirPublicEdit = QtWidgets.QLineEdit()
        self.dirPublicValid = QtWidgets.QLabel()
        self.dirPublicValid.setMaximumSize(self.size, self.size)
        self.dirPublicValid.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.dirPublicTest = QtWidgets.QPushButton(testText)
        self.dirPublicTest.clicked.connect(self.doTest)
        self.postPublic = QtWidgets.QPushButton(postText)
        self.postPublic.clicked.connect(self.doPost)

        # Chemin du répertoire secret :
        text = QtWidgets.QApplication.translate('main', 'DirSiteSecret:')
        dirSecretLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.dirSecretEdit = QtWidgets.QLineEdit()
        self.dirSecretValid = QtWidgets.QLabel()
        self.dirSecretValid.setMaximumSize(self.size, self.size)
        self.dirSecretValid.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.dirSecretTest = QtWidgets.QPushButton(testText)
        self.dirSecretTest.clicked.connect(self.doTest)
        self.postSecret = QtWidgets.QPushButton(postText)
        self.postSecret.clicked.connect(self.doPost)

        # Adresse de base du site :
        text = QtWidgets.QApplication.translate('main', 'WebSiteUrlBase:')
        siteUrlBaseLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.siteUrlBaseEdit = QtWidgets.QLineEdit()
        self.siteUrlBaseValid = QtWidgets.QLabel()
        self.siteUrlBaseValid.setMaximumSize(self.size, self.size)
        self.siteUrlBaseValid.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.siteUrlBaseTest = QtWidgets.QPushButton(testText)
        self.siteUrlBaseTest.clicked.connect(self.doTest)

        # Adresse du site public :
        text = QtWidgets.QApplication.translate('main', 'SiteUrlPublic:')
        siteUrlPublicLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.siteUrlPublicEdit = QtWidgets.QLineEdit()
        self.siteUrlPublicValid = QtWidgets.QLabel()
        self.siteUrlPublicValid.setMaximumSize(self.size, self.size)
        self.siteUrlPublicValid.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.siteUrlPublicTest = QtWidgets.QPushButton(testText)
        self.siteUrlPublicTest.clicked.connect(self.doTest)

        # Langue par défaut de l'interface web :
        text = QtWidgets.QApplication.translate(
            'main', 'Default language of the website:')
        siteLanguageLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.siteLanguageComboBox = QtWidgets.QComboBox()
        self.siteLanguageIsModified = False

        self.state = {}
        for what in (
            'baseSiteFtp', 'userFtp',  'mdpFtp', 
            'dirSitePublic', 'dirSiteSecret', 
            'siteUrlBase', 'siteUrlPublic'):
            self.state[what] = False

        # réglages du ftp :
        ftpGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'FTP access (or SFTP)'))
        grid = QtWidgets.QGridLayout()
        
        grid.addWidget(sftpLabel,        5, 0, 1, 4)
        grid.addWidget(self.sftpCheckBox,       6, 1)

        grid.addWidget(baseSiteFtpLabel,        10, 0, 1, 4)
        grid.addWidget(self.baseSiteFtpEdit,    11, 1)
        grid.addWidget(self.baseSiteFtpValid,   11, 2)
        grid.addWidget(self.baseSiteFtpTest,    11, 3)
        grid.addWidget(userFtpLabel,            20, 0, 1, 4)
        grid.addWidget(self.userFtpEdit,        21, 1)
        grid.addWidget(mdpFtpLabel,             30, 0, 1, 4)
        grid.addWidget(self.mdpFtpEdit,         31, 1)
        grid.addWidget(self.mdpFtpValid,        31, 2)
        grid.addWidget(self.mdpFtpTest,         31, 3)
        grid.addWidget(ftpPortLabel,            40, 0, 1, 4)
        grid.addWidget(ftpPortAvert,            41, 1, 1, 3)
        grid.addWidget(self.ftpPortSpinBox,     42, 1)
        ftpGroupBox.setLayout(grid)

        # dossiers d'installation :
        foldersGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Installation folders'))
        grid = QtWidgets.QGridLayout()
        grid.addWidget(dirPublicLabel,          10, 0, 1, 4)
        grid.addWidget(self.dirPublicEdit,      11, 1)
        grid.addWidget(self.dirPublicValid,     11, 2)
        grid.addWidget(self.dirPublicTest,      11, 3)
        grid.addWidget(self.postPublic,         12, 1)
        grid.addWidget(dirSecretLabel,          20, 0, 1, 4)
        grid.addWidget(self.dirSecretEdit,      21, 1)
        grid.addWidget(self.dirSecretValid,     21, 2)
        grid.addWidget(self.dirSecretTest,      21, 3)
        grid.addWidget(self.postSecret,         22, 1)
        foldersGroupBox.setLayout(grid)

        # accès http au site :
        httpGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'HTTP access (or HTTPS)'))
        grid = QtWidgets.QGridLayout()
        grid.addWidget(siteUrlBaseLabel,            10, 0, 1, 4)
        grid.addWidget(self.siteUrlBaseEdit,        11, 1)
        grid.addWidget(self.siteUrlBaseValid,       11, 2)
        grid.addWidget(self.siteUrlBaseTest,        11, 3)
        grid.addWidget(siteUrlPublicLabel,          20, 0, 1, 4)
        grid.addWidget(self.siteUrlPublicEdit,      21, 1)
        grid.addWidget(self.siteUrlPublicValid,     21, 2)
        grid.addWidget(self.siteUrlPublicTest,      21, 3)
        grid.addWidget(siteLanguageLabel,           30, 0, 1, 4)
        grid.addWidget(self.siteLanguageComboBox,   31, 1)
        httpGroupBox.setLayout(grid)

        # on agence tout ça :
        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(ftpGroupBox)
        vBoxLeft.addStretch(1)
        vBoxRight = QtWidgets.QVBoxLayout()
        vBoxRight.addWidget(foldersGroupBox)
        vBoxRight.addWidget(httpGroupBox)
        vBoxRight.addStretch(1)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addLayout(vBoxLeft)
        hBox.addLayout(vBoxRight)

        # mise en place finale :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(hBox)
        self.setLayout(mainLayout)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les bases admin.config et commun.config
        pour remplir les QLineEdit.
        On met aussi à jour l'état de l'installation.
        """
        texts = {}
        ftpPort = 21
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFrom.format('config_admin')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            if query_admin.value(0) == 'ftpPort':
                ftpPort = int(query_admin.value(1))
            else:
                texts[query_admin.value(0)] = query_admin.value(2)
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.q_selectAllFrom.format('config')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            texts[query_commun.value(0)] = query_commun.value(2)
        # mise à jour des QLineEdit :
        # ICI SFTP
        self.sftpCheckBox.setChecked(texts.get('SFTP', 'NO') == 'YES')
        self.sftpIsModified = False
        self.baseSiteFtpEdit.setText(texts.get('baseSiteFtp', ''))
        self.userFtpEdit.setText(texts.get('userFtp', ''))
        self.mdpFtpEdit.setText(texts.get('mdpFtp', ''))
        self.ftpPortSpinBox.setValue(ftpPort)
        self.ftpPortIsModified = False
        self.dirPublicEdit.setText(texts.get('dirSitePublic', ''))
        self.dirPublicEdit.setValidator(
            QtGui.QRegExpValidator(dirRegExp, self.dirPublicEdit))
        self.dirSecretEdit.setText(texts.get('dirSiteSecret', ''))
        self.dirSecretEdit.setValidator(
            QtGui.QRegExpValidator(dirRegExp, self.dirSecretEdit))
        self.siteUrlBaseEdit.setText(texts.get('siteUrlBase', ''))
        self.siteUrlPublicEdit.setText(texts.get('siteUrlPublic', ''))
        # validations :
        test = admin_ftp.FTP_HOST_OK
        setOk(self.baseSiteFtpValid, self.size, test)
        self.state['baseSiteFtp'] = test
        test = admin_ftp.FTP_LOGIN_OK
        setOk(self.mdpFtpValid, self.size, test)
        self.state['mdpFtp'] = test
        """
        self.doTest(who=self.dirPublicTest, allTest=False)
        self.doTest(who=self.dirSecretTest, allTest=False)
        self.doTest(who=self.siteUrlBaseTest)
        self.doTest(who=self.siteUrlPublicTest)
        """
        self.doTest(first=True)
        actualLocale = utils.LOCALE
        admin.openDB(self.main, 'configweb')
        query_configweb = utils_db.query(admin.db_configweb)
        commandLine_configweb = 'SELECT * FROM config WHERE key_name="LANG"'
        query_configweb = utils_db.queryExecute(commandLine_configweb, query=query_configweb)
        if query_configweb.first():
            actualLocale = query_configweb.value(2)
            if actualLocale == 'fr':
                actualLocale = 'fr_FR'
        index = 0
        i = 0
        for locale in utils.WEB_LOCALES:
            text = utils_functions.u('{0} ({1})').format(locale[0], locale[1])
            self.siteLanguageComboBox.addItem(text, locale[0])
            if locale[0] == actualLocale:
                index = i
            i += 1
        self.siteLanguageComboBox.setCurrentIndex(index)
        self.siteLanguageIsModified = False

    def createConnexions(self):
        lineEdits = (#QLineEdit
            self.baseSiteFtpEdit, 
            self.userFtpEdit, 
            self.mdpFtpEdit, 
            self.dirPublicEdit, 
            self.dirSecretEdit, 
            self.siteUrlBaseEdit, 
            self.siteUrlPublicEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.editingFinished.connect(self.doModified)
        spinBoxs = (#QSpinBox
            self.ftpPortSpinBox, 
            )
        for spinBox in spinBoxs:
            spinBox.valueChanged.connect(self.ftpPortChanged)
        comboBoxs = (#QComboBox
            self.siteLanguageComboBox, 
            )
        for comboBox in comboBoxs:
            comboBox.activated.connect(self.siteLanguageChanged)
        # ICI SFTP
        checkBoxs = (#QCheckBox
            self.sftpCheckBox, 
            )
        for checkBox in checkBoxs:
            checkBox.stateChanged.connect(self.sftpChanged)

    def ftpPortChanged(self):
        self.ftpPortIsModified = True
        self.doModified()

    def siteLanguageChanged(self):
        self.siteLanguageIsModified = True
        self.doModified()

    # ICI SFTP
    def sftpChanged(self):
        self.sftpIsModified = True
        self.doModified()

    def doTest(self, who=None, allTest=True, first=False):
        utils_functions.doWaitCursor()
        try:
            if who in (None, False):
                who = self.sender()
            if first:
                # PB chez 1&1 (délai entre 2 ftp ?)
                host = self.baseSiteFtpEdit.text()
                user = self.userFtpEdit.text()
                password = self.mdpFtpEdit.text()
                ftpPort = self.ftpPortSpinBox.value()
                theDirPublic = self.dirPublicEdit.text()
                theDirSecret = self.dirSecretEdit.text()
                test = admin_ftp.ftpTestDirExists(
                    host, user, password, [theDirPublic, theDirSecret])

                setOk(self.dirPublicValid, self.size, test[theDirPublic])
                self.state['dirSitePublic'] = test[theDirPublic]
                setOk(self.dirSecretValid, self.size, test[theDirSecret])
                self.state['dirSiteSecret'] = test[theDirSecret]

                test = utils_web.testNet(self.main, self.siteUrlBaseEdit.text())
                setOk(self.siteUrlBaseValid, self.size, test)
                self.state['siteUrlBase'] = test
                test = utils_web.testNet(self.main, self.siteUrlPublicEdit.text())
                setOk(self.siteUrlPublicValid, self.size, test)
                self.state['siteUrlPublic'] = test

            elif who == self.baseSiteFtpTest:
                host = self.baseSiteFtpEdit.text()
                test = admin_ftp.ftpTestHost(host)
                setOk(self.baseSiteFtpValid, self.size, test)
                self.state['baseSiteFtp'] = test
            elif who == self.mdpFtpTest:
                test = admin_ftp.ftpTestLogin(
                    self.baseSiteFtpEdit.text(), 
                    self.userFtpEdit.text(), 
                    self.mdpFtpEdit.text())
                setOk(self.mdpFtpValid, self.size, test)
                self.state['userFtp'] = test
                self.state['mdpFtp'] = test
            elif who == self.dirPublicTest:
                host = self.baseSiteFtpEdit.text()
                user = self.userFtpEdit.text()
                password = self.mdpFtpEdit.text()
                ftpPort = self.ftpPortSpinBox.value()
                theDir = self.dirPublicEdit.text()
                theDir = utils_functions.removeSlash(theDir)
                test = admin_ftp.ftpTestDirExists(
                    host, user, password, theDir)[theDir]
                setOk(self.dirPublicValid, self.size, test)
                self.state['dirSitePublic'] = test
                if not(allTest):
                    return
                # le chemin doit commencer par un / :
                if theDir[0] != '/':
                    setOk(self.dirPublicValid, self.size, False)
                    self.state['dirSitePublic'] = False
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 'The path must begin with a /')
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    return
                if not(test):
                    lastDir = theDir.split('/')[-1]
                    # le chemin doit se terminer par "verac" :
                    if lastDir != 'verac':
                        utils_functions.restoreCursor()
                        message = QtWidgets.QApplication.translate(
                            'main', "The last directory's name must be <b>verac</>!")
                        utils_functions.messageBox(self.main, level='warning', message=message)
                        return
                    # on vérifie l'existence du chemin (avant "verac") :
                    parentDir = '/'.join(theDir.split('/')[:-1])
                    test = admin_ftp.ftpTestDirExists(
                        host, user, password, parentDir)[parentDir]
                    if not(test):
                        utils_functions.restoreCursor()
                        message = QtWidgets.QApplication.translate(
                            'main', 'The proposed (before /verac) path is incorrect!')
                        utils_functions.messageBox(self.main, level='warning', message=message)
                        return
                    # le dossier "verac" n'existe pas, donc on le crée :
                    test = admin_ftp.ftpMakeDir(host, user, password, theDir)
                    if not(test):
                        return
                    # on demande s'il faut poster :
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 
                        'Do you want to update the website now?')
                    reply = utils_functions.messageBox(
                        self.main, level='question', message=message, buttons=['Yes', 'No'])
                    if reply != QtWidgets.QMessageBox.Yes:
                        return
                    utils_functions.doWaitCursor()
                    import sfm
                    remotedir = theDir
                    localdir = utils_functions.u(admin.dirLocalPublic)
                    result = sfm.doAction(
                        username=user, 
                        password=password, 
                        host=host, 
                        port=ftpPort, 
                        remotedir=remotedir, 
                        localdir=localdir)
                    utils_functions.myPrint(result)
                setOk(self.dirPublicValid, self.size, True)
                self.state['dirSitePublic'] = True
            elif who == self.dirSecretTest:
                host = self.baseSiteFtpEdit.text()
                user = self.userFtpEdit.text()
                password = self.mdpFtpEdit.text()
                ftpPort = self.ftpPortSpinBox.value()
                theDir = self.dirSecretEdit.text()
                theDir = utils_functions.removeSlash(theDir)
                test = admin_ftp.ftpTestDirExists(
                    host, user, password, theDir)[theDir]
                setOk(self.dirSecretValid, self.size, test)
                self.state['dirSiteSecret'] = test
                if not(allTest):
                    return
                # le chemin doit commencer par un / :
                if theDir[0] != '/':
                    setOk(self.dirSecretValid, self.size, False)
                    self.state['dirSiteSecret'] = False
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 'The path must begin with a /')
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    return
                if test:
                    return
                # le chemin doit commencer par un / :
                if theDir[0] != '/':
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 'The path must begin with a /')
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    return
                # on vérifie l'existence du chemin (avant "secret") :
                parentDir = '/'.join(theDir.split('/')[:-1])
                test = admin_ftp.ftpTestDirExists(
                    host, user, password, parentDir)[parentDir]
                if not(test):
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 'The proposed (before /secret) path is incorrect!')
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    return
                # on vérifie que le dossier secret n'est pas dans le dossier public :
                thePublicDir = self.dirPublicEdit.text()
                thePublicDir = utils_functions.removeSlash(thePublicDir)
                if (theDir[:len(thePublicDir)] == thePublicDir):
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 'The secret directory must not be placed in the public directory!')
                    utils_functions.messageBox(self.main, level='warning', message=message)
                    return
                # le dossier "secret" n'existe pas, donc on le crée :
                test = admin_ftp.ftpMakeDir(host, user, password, theDir)
                if not(test):
                    return
                # idem pour le sous-dossier verac :
                test = admin_ftp.ftpMakeDir(
                    host, user, password, '{0}/verac'.format(theDir))
                if not(test):
                    return
                # on demande s'il faut poster :
                utils_functions.restoreCursor()
                message = QtWidgets.QApplication.translate(
                    'main', 
                    'Do you want to update the secret directory now?')
                reply = utils_functions.messageBox(
                    self.main, level='question', message=message, buttons=['Yes', 'No'])
                if reply != QtWidgets.QMessageBox.Yes:
                    return
                utils_functions.doWaitCursor()
                import sfm
                remotedir = '{0}/verac'.format(theDir)
                localdir = admin.dirLocalPrive
                result = sfm.doAction(
                    username=user, 
                    password=password, 
                    host=host, 
                    port=ftpPort, 
                    remotedir=remotedir, 
                    localdir=localdir)
                utils_functions.myPrint(result)
                setOk(self.dirSecretValid, self.size, True)
                self.state['dirSiteSecret'] = True
            elif who == self.siteUrlBaseTest:
                test = utils_web.testNet(self.main, self.siteUrlBaseEdit.text())
                setOk(self.siteUrlBaseValid, self.size, test)
                self.state['siteUrlBase'] = test
            elif who == self.siteUrlPublicTest:
                test = utils_web.testNet(self.main, self.siteUrlPublicEdit.text())
                setOk(self.siteUrlPublicValid, self.size, test)
                self.state['siteUrlPublic'] = test
        finally:
            self.doStates()
            utils_functions.restoreCursor()

    def doPost(self):
        utils_functions.doWaitCursor()
        host = self.baseSiteFtpEdit.text()
        user = self.userFtpEdit.text()
        password = self.mdpFtpEdit.text()
        ftpPort = self.ftpPortSpinBox.value()
        try:
            if self.sender() == self.postPublic:
                import sfm
                theDir = self.dirPublicEdit.text()
                theDir = utils_functions.removeSlash(theDir)
                remotedir = theDir
                localdir = utils_functions.u(admin.dirLocalPublic)
                result = sfm.doAction(
                    username=user, 
                    password=password, 
                    host=host, 
                    port=ftpPort, 
                    remotedir=remotedir, 
                    localdir=localdir)
                utils_functions.myPrint(result)
            elif self.sender() == self.postSecret:
                import sfm
                theDir = self.dirSecretEdit.text()
                theDir = utils_functions.removeSlash(theDir)
                remotedir = '{0}/verac'.format(theDir)
                localdir = admin.dirLocalPrive
                result = sfm.doAction(
                    username=user, 
                    password=password, 
                    host=host, 
                    port=ftpPort, 
                    remotedir=remotedir, 
                    localdir=localdir)
                utils_functions.myPrint(result)
        finally:
            utils_functions.restoreCursor()

    def doStates(self):
        for action in (self.userFtpEdit, self.mdpFtpEdit, self.mdpFtpTest):
            action.setEnabled(self.state['baseSiteFtp'])
        for action in (self.dirPublicEdit, self.dirPublicTest):
            action.setEnabled(self.state['mdpFtp'])
        for action in (self.dirSecretEdit, self.dirSecretTest, self.postPublic):
            action.setEnabled(self.state['dirSitePublic'])
        for action in (self.postSecret, ):
            action.setEnabled(self.state['dirSiteSecret'])
        test = self.state['dirSiteSecret'] and self.state['siteUrlBase']
        for action in (self.siteUrlPublicEdit, self.siteUrlPublicTest):
            action.setEnabled(test)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        commandLines_commun = []
        commandLines_admin = []
        commandLines_configweb = []
        commandLine = 'UPDATE {0} SET value_text="{1}" WHERE key_name="{2}"'
        mustCalcRelativeSecretPath = False

        # ICI SFTP
        if self.sftpIsModified:
            self.parent.modifiedTables['admin']['config_admin'] = True
            if self.sftpCheckBox.isChecked():
                newText = 'YES'
            else:
                newText = 'NO'
            commandLines_admin.append(
                utils_db.qdf_whereText.format(
                    'config_admin', 'key_name', 'SFTP'))
            commandLines_admin.append(
                'INSERT INTO config_admin VALUES ("SFTP", "", "{0}")'.format(newText))
            #admin_ftp.ftpUpdateConfig(newFtpHost=newText)
        if self.baseSiteFtpEdit.isModified():
            self.parent.modifiedTables['admin']['config_admin'] = True
            newText = self.baseSiteFtpEdit.text()
            commandLines_admin.append(
                commandLine.format('config_admin', newText, 'baseSiteFtp'))
            admin_ftp.ftpUpdateConfig(newFtpHost=newText)
        if self.userFtpEdit.isModified():
            self.parent.modifiedTables['admin']['config_admin'] = True
            newText = self.userFtpEdit.text()
            commandLines_admin.append(
                commandLine.format('config_admin', newText, 'userFtp'))
            admin_ftp.ftpUpdateConfig(newFtpUser=newText)
        if self.mdpFtpEdit.isModified():
            self.parent.modifiedTables['admin']['config_admin'] = True
            newText = self.mdpFtpEdit.text()
            commandLines_admin.append(
                commandLine.format('config_admin', newText, 'mdpFtp'))
            admin_ftp.ftpUpdateConfig(newFtpPass=newText)
        if self.ftpPortIsModified:
            ftpPort = self.ftpPortSpinBox.value()
            self.parent.modifiedTables['admin']['config_admin'] = True
            commandLines_admin.append(
                utils_db.qdf_whereText.format(
                    'config_admin', 'key_name', 'ftpPort'))
            if ftpPort != 21:
                commandLines_admin.append(
                    'INSERT INTO config_admin VALUES ("ftpPort", {0}, "")'.format(ftpPort))
            admin_ftp.ftpUpdateConfig(newFtpPort=ftpPort)
        if self.dirPublicEdit.isModified():
            mustCalcRelativeSecretPath = True
            self.parent.modifiedTables['admin']['config_admin'] = True
            newText = self.dirPublicEdit.text()
            commandLines_admin.append(
                commandLine.format('config_admin', newText, 'dirSitePublic'))
        if self.dirSecretEdit.isModified():
            mustCalcRelativeSecretPath = True
            self.parent.modifiedTables['admin']['config_admin'] = True
            newText = self.dirSecretEdit.text()
            commandLines_admin.append(
                commandLine.format('config_admin', newText, 'dirSiteSecret'))
        if mustCalcRelativeSecretPath:
            self.parent.modifiedTables['configweb']['config'] = True
            dirPublic = self.dirPublicEdit.text()
            dirSecret = self.dirSecretEdit.text()
            relativeSecretPath = QtCore.QDir(dirPublic).relativeFilePath(dirSecret)
            commandLines_configweb.append(
                commandLine.format('config', relativeSecretPath, 'CHEMIN_SECRET_RELATIF'))
        if self.siteUrlBaseEdit.isModified():
            self.parent.modifiedTables['commun']['config'] = True
            newText = self.siteUrlBaseEdit.text()
            commandLines_commun.append(
                commandLine.format('config', newText, 'siteUrlBase'))
        if self.siteUrlPublicEdit.isModified():
            self.parent.modifiedTables['commun']['config'] = True
            self.parent.modifiedTables['configweb']['config'] = True
            newText = self.siteUrlPublicEdit.text()
            commandLines_commun.append(
                commandLine.format('config', newText, 'siteUrlPublic'))
            commandLines_configweb.append(
                commandLine.format('config', newText, 'URL_SITE_PUBLIC'))
            self.main.actualVersion['versionUrl'] = newText
        if self.siteLanguageIsModified:
            newLocale = ''
            newText = self.siteLanguageComboBox.currentText()
            for locale in utils.WEB_LOCALES:
                text = utils_functions.u('{0} ({1})').format(locale[0], locale[1])
                if text == newText:
                    newLocale = locale[0]
            if newLocale != '':
                self.parent.modifiedTables['configweb']['config'] = True
                commandLines_configweb.append(
                    commandLine.format('config', newLocale, 'LANG'))

        if len(commandLines_commun) > 0:
            self.parent.modifiedTables['DATABASES']['commun'] = True
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
            try:
                query_commun = utils_db.queryExecute(
                    commandLines_commun, query=query_commun)
            finally:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
        if len(commandLines_admin) > 0:
            self.parent.modifiedTables['DATABASES']['admin'] = True
            query_admin, transactionOK_admin = utils_db.queryTransactionDB(
                admin.db_admin)
            try:
                query_admin = utils_db.queryExecute(
                    commandLines_admin, query=query_admin)
            finally:
                utils_db.endTransaction(
                    query_admin, admin.db_admin, transactionOK_admin)
        if len(commandLines_configweb) > 0:
            self.parent.modifiedTables['DATABASES']['configweb'] = True
            admin.openDB(self.main, 'configweb')
            query_configweb, transactionOK_configweb = utils_db.queryTransactionDB(
                admin.db_configweb)
            try:
                query_configweb = utils_db.queryExecute(
                    commandLines_configweb, query=query_configweb)
            finally:
                utils_db.endTransaction(
                    query_configweb, admin.db_configweb, transactionOK_configweb)
        self.modified = False


class WebSiteStateDlg(QtWidgets.QDialog):
    """
    Dialogue pour configurer l'état du site web (ce qui est activé).
    """
    def __init__(self, parent=None):
        super(WebSiteStateDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'website-state'
        # des modifications ont été faites :
        self.modified = False
        # pour annuler :
        self.lastData = []

        # la zone d'édition (un textedit et des cases à cocher) :
        self.checkBoxs = {}
        QtWidgets.QApplication.translate(
            'main', 'General Restrictions')
        QtWidgets.QApplication.translate(
            'main', 'Pages authorized for students (all periods)')
        QtWidgets.QApplication.translate(
            'main', 'Restrictions relating only to the current period')
        groups = (
            (QtWidgets.QGroupBox(), QtWidgets.QVBoxLayout()), 
            (QtWidgets.QGroupBox(), QtWidgets.QVBoxLayout()), 
            (QtWidgets.QGroupBox(), QtWidgets.QVBoxLayout()), 
            (QtWidgets.QGroupBox(), QtWidgets.QVBoxLayout()))
        # le TextEdit du message :
        i = 0
        title = QtWidgets.QApplication.translate(
            'main', 'Message for userss')
        self.messageEdit = QtWidgets.QTextEdit()
        self.messageEdit.textChanged.connect(self.doEdited)
        groups[i][0].setTitle(title)
        groups[i][1].addWidget(self.messageEdit)
        # les 3 zones de cases à cocher :
        for state in admin_web.states['LIST']:
            if state[:10] == 'SEPARATOR:':
                i += 1
                title = QtWidgets.QApplication.translate('main', state[10:])
                groups[i][0].setTitle(title)
            else:
                checkBox = QtWidgets.QCheckBox(admin_web.states[state][1])
                checkBox.setChecked(admin_web.states[state][0])
                checkBox.stateChanged.connect(self.doChanged)
                self.checkBoxs[state] = checkBox
                groups[i][1].addWidget(checkBox)
        for (groupBox, vBoxLayout) in groups:
            groupBox.setLayout(vBoxLayout)

        # on agence tout ça :
        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(groups[1][0])
        vBoxLeft.addWidget(groups[2][0])
        vBoxLeft.addStretch(1)
        vBoxRight = QtWidgets.QVBoxLayout()
        vBoxRight.addWidget(groups[0][0])
        vBoxRight.addWidget(groups[3][0])
        vBoxRight.addStretch(1)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addLayout(vBoxLeft)
        hBox.addLayout(vBoxRight)

        # mise en place finale :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(hBox)
        self.setLayout(mainLayout)
        #self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de configweb.state :
        """
        self.fromSoft = True
        admin.openDB(self.main, 'configweb')
        for state in admin_web.states['LIST']:
            if state[:10] != 'SEPARATOR:':
                value_int = utils_db.readInConfigTable(
                    admin.db_configweb, 
                    admin_web.states[state][2], table='state', default_int=1)[0]
                self.checkBoxs[state].setChecked(value_int == 1)
        message = utils_db.readInConfigTable(
            admin.db_configweb, 
            'MESSAGE_TO_USERS', table='state')[1]
        self.messageEdit.setPlainText(message)
        self.fromSoft = False

    def doEdited(self):
        """
        le message a été modifié.
        """
        if self.fromSoft:
            return
        self.modified = True
        self.parent.updateButtons()

    def doChanged(self, value):
        if self.fromSoft:
            return
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.parent.modifiedTables['DATABASES']['configweb'] = True
        self.parent.modifiedTables['configweb']['state'] = True
        lines = []
        for state in admin_web.states['LIST']:
            if state[:10] != 'SEPARATOR:':
                keyName = admin_web.states[state][2]
                if self.checkBoxs[state].isChecked():
                    lines.append((keyName, 1, 'BOOLEAN'))
                else:
                    lines.append((keyName, 0, 'BOOLEAN'))
        lines.append(('MESSAGE_TO_USERS', '', self.messageEdit.toPlainText()))
        admin.openDB(self.main, 'configweb')
        query_configweb, transactionOK_configweb = utils_db.queryTransactionDB(
            admin.db_configweb)
        try:
            # on recrée la table :
            listCommands = [utils_db.qdf_table.format('state'), 
                            utils_db.qct_configweb_state]
            query_configweb = utils_db.queryExecute(listCommands, query=query_configweb)
            # on écrit les lignes :
            commandLine = utils_db.insertInto('state', 3)
            query_configweb = utils_db.queryExecute(
                {commandLine: lines}, query=query_configweb)
        finally:
            utils_db.endTransaction(
                query_configweb, admin.db_configweb, transactionOK_configweb)
        self.modified = False

