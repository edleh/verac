# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    un calendrier
    d'après la doc PyQT (datetimeedit.py ; voir ci-dessous)
"""
#============================================================================#
# (Re)Implementation of QDateEdit and QDateTimeEdit. These classes force the #
# use of the calendar popup.                                                 #
#----------------------------------------------------------------------------#
# Copyright (c) 2008 by Denviso GmbH, <ulrich.berning@denviso.de>            #
#                                                                            #
# All Rights Reserved                                                        #
#                                                                            #
# Permission to use, copy, modify, and distribute this software and its      #
# documentation for any purpose and without fee is hereby granted,           #
# provided that the above copyright notice appear in all copies and that     #
# both that copyright notice and this permission notice appear in            #
# supporting documentation.                                                  #
#                                                                            #
# DENVISO DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS                       #
# SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY              #
# AND FITNESS, IN NO EVENT SHALL DENVISO BE LIABLE FOR ANY                   #
# SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES                  #
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,                    #
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER                      #
# TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE              #
# OR PERFORMANCE OF THIS SOFTWARE.                                           #
#----------------------------------------------------------------------------#

# importation des modules utiles :
from __future__ import division

import utils, utils_functions

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
    QtCore.Signal = QtCore.pyqtSignal
else:
    from PyQt4 import QtCore, QtGui as QtWidgets
    QtCore.Signal = QtCore.pyqtSignal



#============================================================================#
# PyDateEdit                                                                 #
#----------------------------------------------------------------------------#
class PyDateEdit(QtWidgets.QDateEdit):
    #
    # Initialize base class
    # Force use of the calendar popup
    # Set default values for calendar properties
    #
    def __init__(self, *args):
        QtWidgets.QDateEdit.__init__(self, *args)
        QtWidgets.QDateEdit.setCalendarPopup(self, True)
        self.__cw = None
        self.__firstDayOfWeek = QtCore.Qt.Monday
        self.__gridVisible = False
        self.__horizontalHeaderFormat = QtWidgets.QCalendarWidget.ShortDayNames
        self.__verticalHeaderFormat = QtWidgets.QCalendarWidget.ISOWeekNumbers
        self.__navigationBarVisible = True

    #
    # Call event handler of base class
    # Get the calendar widget, if not already done
    # Set the calendar properties
    #
    def mousePressEvent(self, event):
        QtWidgets.QDateEdit.mousePressEvent(self, event)
        if not self.__cw:
            self.__cw = self.findChild(QtWidgets.QCalendarWidget)
            if self.__cw:
                self.__cw.setFirstDayOfWeek(self.__firstDayOfWeek)
                self.__cw.setGridVisible(self.__gridVisible)
                self.__cw.setHorizontalHeaderFormat(self.__horizontalHeaderFormat)
                self.__cw.setVerticalHeaderFormat(self.__verticalHeaderFormat)
                self.__cw.setNavigationBarVisible(self.__navigationBarVisible)

    #
    # Make sure, the calendarPopup property is invisible in Designer
    #
    def getCalendarPopup(self):
        return True
    if utils.PYQT == 'PYSIDE':
        calendarPopup = QtCore.Property('bool', fget=getCalendarPopup)
    else:
        calendarPopup = QtCore.pyqtProperty(bool, fget=getCalendarPopup)

    #
    # Property firstDayOfWeek: Qt::DayOfWeek
    # Get: getFirstDayOfWeek()
    # Set: setFirstDayOfWeek()
    # Reset: resetFirstDayOfWeek()
    #
    def getFirstDayOfWeek(self):
        return self.__firstDayOfWeek
    def setFirstDayOfWeek(self, dayOfWeek):
        if dayOfWeek != self.__firstDayOfWeek:
            self.__firstDayOfWeek = dayOfWeek
            if self.__cw:
                self.__cw.setFirstDayOfWeek(dayOfWeek)
    def resetFirstDayOfWeek(self):
        if self.__firstDayOfWeek != QtCore.Qt.Monday:
            self.__firstDayOfWeek = QtCore.Qt.Monday
            if self.__cw:
                self.__cw.setFirstDayOfWeek(QtCore.Qt.Monday)
    if utils.PYQT == 'PYSIDE':
        firstDayOfWeek = QtCore.Property('Qt::DayOfWeek',
                                            fget=getFirstDayOfWeek,
                                            fset=setFirstDayOfWeek,
                                            freset=resetFirstDayOfWeek)
    else:
        firstDayOfWeek = QtCore.pyqtProperty(QtCore.Qt.DayOfWeek,
                                            fget=getFirstDayOfWeek,
                                            fset=setFirstDayOfWeek,
                                            freset=resetFirstDayOfWeek)

    #
    # Property gridVisible: bool
    # Get: isGridVisible()
    # Set: setGridVisible()
    # Reset: resetGridVisible()
    #
    def isGridVisible(self):
        return self.__gridVisible
    def setGridVisible(self, show):
        if show != self.__gridVisible:
            self.__gridVisible = show
            if self.__cw:
                self.__cw.setGridVisible(show)
    def resetGridVisible(self):
        if self.__gridVisible != False:
            self.__gridVisible = False
            if self.__cw:
                self.__cw.setGridVisible(False)
    if utils.PYQT == 'PYSIDE':
        gridVisible = QtCore.Property('bool',
                                      fget=isGridVisible,
                                      fset=setGridVisible,
                                      freset=resetGridVisible)
    else:
        gridVisible = QtCore.pyqtProperty(bool,
                                      fget=isGridVisible,
                                      fset=setGridVisible,
                                      freset=resetGridVisible)

    #
    # Property horizontalHeaderFormat: QCalendarWidget::HorizontalHeaderFormat
    # Get: getHorizontalHeaderFormat()
    # Set: setHorizontalHeaderFormat()
    # Reset: resetHorizontalHeaderFormat()
    #
    def getHorizontalHeaderFormat(self):
        return self.__horizontalHeaderFormat
    def setHorizontalHeaderFormat(self, format):
        if format != self.__horizontalHeaderFormat:
            self.__horizontalHeaderFormat = format
            if self.__cw:
                self.__cw.setHorizontalHeaderFormat(format)
    def resetHorizontalHeaderFormat(self):
        if self.__horizontalHeaderFormat != QtWidgets.QCalendarWidget.ShortDayNames:
            self.__horizontalHeaderFormat = QtWidgets.QCalendarWidget.ShortDayNames
            if self.__cw:
                self.__cw.setHorizontalHeaderFormat(QtWidgets.QCalendarWidget.ShortDayNames)
    if utils.PYQT == 'PYSIDE':
        horizontalHeaderFormat = QtCore.Property('QCalendarWidget::HorizontalHeaderFormat',
                                                 fget=getHorizontalHeaderFormat,
                                                 fset=setHorizontalHeaderFormat,
                                                 freset=resetHorizontalHeaderFormat)
    else:
        horizontalHeaderFormat = QtCore.pyqtProperty(QtWidgets.QCalendarWidget.HorizontalHeaderFormat,
                                                 fget=getHorizontalHeaderFormat,
                                                 fset=setHorizontalHeaderFormat,
                                                 freset=resetHorizontalHeaderFormat)

    #
    # Property verticalHeaderFormat: QCalendarWidget::VerticalHeaderFormat
    # Get: getVerticalHeaderFormat()
    # Set: setVerticalHeaderFormat()
    # Reset: resetVerticalHeaderFormat()
    #
    def getVerticalHeaderFormat(self):
        return self.__verticalHeaderFormat
    def setVerticalHeaderFormat(self, format):
        if format != self.__verticalHeaderFormat:
            self.__verticalHeaderFormat = format
            if self.__cw:
                self.__cw.setVerticalHeaderFormat(format)
    def resetVerticalHeaderFormat(self):
        if self.__verticalHeaderFormat != QtWidgets.QCalendarWidget.ISOWeekNumbers:
            self.__verticalHeaderFormat = QtWidgets.QCalendarWidget.ISOWeekNumbers
            if self.__cw:
                self.__cw.setVerticalHeaderFormat(QtWidgets.QCalendarWidget.ISOWeekNumbers)
    if utils.PYQT == 'PYSIDE':
        verticalHeaderFormat = QtCore.Property('QCalendarWidget::VerticalHeaderFormat',
                                               fget=getVerticalHeaderFormat,
                                               fset=setVerticalHeaderFormat,
                                               freset=resetVerticalHeaderFormat)
    else:
        verticalHeaderFormat = QtCore.pyqtProperty(QtWidgets.QCalendarWidget.VerticalHeaderFormat,
                                               fget=getVerticalHeaderFormat,
                                               fset=setVerticalHeaderFormat,
                                               freset=resetVerticalHeaderFormat)

    #
    # Property navigationBarVisible: bool
    # Get: isNavigationBarVisible()
    # Set: setNavigationBarVisible()
    # Reset: resetNavigationBarVisible()
    #
    def isNavigationBarVisible(self):
        return self.__navigationBarVisible
    def setNavigationBarVisible(self, visible):
        if visible != self.__navigationBarVisible:
            self.__navigationBarVisible = visible
            if self.__cw:
                self.__cw.setNavigationBarVisible(visble)
    def resetNavigationBarVisible(self):
        if self.__navigationBarVisible != True:
            self.__navigationBarVisible = True
            if self.__cw:
                self.__cw.setNavigationBarVisible(True)
    if utils.PYQT == 'PYSIDE':
        navigationBarVisible = QtCore.Property('bool',
                                               fget=isNavigationBarVisible,
                                               fset=setNavigationBarVisible,
                                               freset=resetNavigationBarVisible)
    else:
        navigationBarVisible = QtCore.pyqtProperty(bool,
                                               fget=isNavigationBarVisible,
                                               fset=setNavigationBarVisible,
                                               freset=resetNavigationBarVisible)

