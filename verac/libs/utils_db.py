# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient les fonctions et variables utiles 
    pour manipuler les bases de données.
"""

# importation des modules utiles :
from __future__ import division, print_function
import utils, utils_functions

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore
    from PyQt5 import QtSql
else:
    from PyQt4 import QtCore
    from PyQt4 import QtSql



# variable globale sqlDatabase :
sqlDatabase = QtSql.QSqlDatabase()



def createConnection(main, dbFileName, dbName=''):
    """
    pour se connecter à une base.
    """
    global sqlDatabase
    if dbFileName == 'db_memory':
        return createConnectionMemory(main)
    if dbName == '':
        dbName = QtCore.QFileInfo(dbFileName).baseName()
    if dbName in sqlDatabase.connectionNames():
        db = sqlDatabase.database(dbName)
        utils_functions.myPrint('DEJA_LA :', dbName, dbFileName)
    else:
        db = sqlDatabase.addDatabase('QSQLITE', dbName)
        utils_functions.afficheMessage(
            main, 
            utils_functions.u('createConnection: {0} [{1}]').format(dbName, dbFileName), 
            console=True)
    db.setDatabaseName(dbFileName)
    if not db.open():
        if utils.NOGUI:
            message = 'Unable to establish a database connection.\n({0})'
            message = utils_functions.u(message).format(dbFileName)
            utils_functions.afficheMessage(main, message, statusBar=True)
        else:
            message = utils_functions.u(
                utils.DATABASE_CONNECTION_FAIL_MESSAGE).format(dbFileName)
            utils_functions.messageBox(
                main, level='critical', message=message, buttons=['Cancel'])
        return (None, None)
    return (db, dbName)

def closeConnection(main, dbName='commun'):
    """
    pour déconnecter une base.
    """
    global sqlDatabase
    if dbName == 'commun':
        try:
            main.db_commun.close()
            del main.db_commun
        except:
            return False
    elif dbName == 'users':
        try:
            main.db_users.close()
            del main.db_users
        except:
            return False

    sqlDatabase.removeDatabase(dbName)
    utils_functions.afficheMessage(
        main, 
        utils_functions.u('closeConnection: {0}').format(dbName), 
        console=True)
    return True







def createConnectionMemory(main):
    """
    connexion à une base en mémoire vive
    """
    global sqlDatabase
    dbName = 'qt_sql_default_connection'
    if dbName in sqlDatabase.connectionNames():
        db = sqlDatabase.database(dbName)
    else:
        db = sqlDatabase.addDatabase('QSQLITE')
        db.setDatabaseName(':memory:')
        utils_functions.afficheMessage(main, 'createConnectionMemory', console=True)
        if not db.open():
            utils_functions.afficheMessage(main, 'NOT OPEN', console=True)
    return (db, dbName)



def query(db):
    """
    crèe et retourne un QSqlQuery sur la DB passée en paramètre
        query_my = utils_db.query(db_my)
    """
    return QtSql.QSqlQuery(db)

def queryTransactionFile(main, dbFile):
    """
    crèe et retourne un QSqlQuery sur le fichier sqlite DB passé en paramètre
        query_documents, db_documents, 
            dbName, transactionOK = utils_db.queryTransactionFile(main, dbFile_documents)
    """
    (db, dbName) = createConnection(main, dbFile)
    transactionOK = db.transaction()
    utils_functions.myPrint('TRANSACTION : ', dbName, transactionOK)
    return QtSql.QSqlQuery(db), db, dbName, transactionOK

def queryTransactionDB(db):
    """
    crèe et retourne un QSqlQuery sur une DB déjà ouverte passé en paramètre
        query_admin, transactionOK = utils_db.queryTransactionDB(db_admin)
    """
    transactionOK = db.transaction()
    utils_functions.myPrint(
        'TRANSACTION : ', db.databaseName(), db.connectionName(), transactionOK)
    return QtSql.QSqlQuery(db), transactionOK

def endTransaction(query, db, transactionOK):
    """
    termine une transaction SANS FERMER la base
        utils_db.endTransaction(query_admin, db_admin, transactionOK)
    S'il faut libérer et refermer la base, 
    ça doit être fait dans le processus qui l'a ouverte !
    """
    if transactionOK:
        db.commit()
    query.exec_('VACUUM')


def queryExecute(what, db=None, query=None):
    """
    what est soit
        * une seule commande (string)
        * une liste de commandes, chacune pouvant être aussi un dictionnaire
        * un dictionnaire (pour des commandes avec prepare-bind).
    pour un dictionnaire
        * la clé est la commande (contenant des ?)
        * la valeur dans le dico est soit
            * une liste de tuples
            * un seul tuple.
        * chaque tuple donne les arguments pour une ligne à executer.
    exemples :
        query = utils_db.queryExecute(commandLine, query=query)
        query = utils_db.queryExecute(
            [
                'DELETE FROM table', 
                {'INSERT INTO table VALUES (?, ?)': [('A', 0), ('B', 1)]}
            ], 
            query=query)

        query = utils_db.queryExecute({commandLine: [('A', 0), ('B', 1)]}, query=query)
        query = utils_db.queryExecute({commandLine: ('A', 0)}, query=query)

    """
    if db != None:
        query = QtSql.QSqlQuery(db)
    if query == None:
        print('queryExecute: NO QUERY')
        return None
    result = True
    commandLine = ''
    if isinstance(what, (list, tuple)):
        # on passe une liste de commandes :
        for what2 in what:
            if isinstance(what2, dict):
                # c'est un dictionnaire de commandes :
                for commandLine in what2:
                    what3 = what2[commandLine]
                    if isinstance(what3, (list, dict)):
                        # on a passé plusieurs tuples de valeurs :
                        if len(what3) > 0:
                            try:
                                query.clear()
                                query.prepare(utils_functions.u(commandLine))
                            except:
                                query.prepare(commandLine)
                            for line in what3:
                                for arg in line:
                                    try:
                                        query.addBindValue(utils_functions.u(arg))
                                    except:
                                        query.addBindValue(arg)
                                result = query.exec_()
                    else:
                        # on a passé un seul tuple de valeurs :
                        try:
                            query.clear()
                            query.prepare(utils_functions.u(commandLine))
                        except:
                            query.prepare(commandLine)
                        for arg in what3:
                            try:
                                query.addBindValue(utils_functions.u(arg))
                            except:
                                query.addBindValue(arg)
                        result = query.exec_()
            else:
                # c'est une commande simple :
                commandLine = what2
                try:
                    query.clear()
                    result = query.exec_(utils_functions.u(commandLine))
                except:
                    result = query.exec_(commandLine)
    elif isinstance(what, dict):
        # on passe un dictionnaire de commandes :
        for commandLine in what:
            what3 = what[commandLine]
            if isinstance(what3, (list, dict)):
                # on a passé plusieurs tuples de valeurs :
                if len(what3) > 0:
                    try:
                        query.clear()
                        query.prepare(utils_functions.u(commandLine))
                    except:
                        query.prepare(commandLine)
                    for line in what3:
                        for arg in line:
                            try:
                                query.addBindValue(utils_functions.u(arg))
                            except:
                                query.addBindValue(arg)
                        result = query.exec_()
            else:
                # on a passé un seul tuple de valeurs :
                try:
                    query.clear()
                    query.prepare(utils_functions.u(commandLine))
                except:
                    query.prepare(commandLine)
                for arg in what3:
                    try:
                        query.addBindValue(utils_functions.u(arg))
                    except:
                        query.addBindValue(arg)
                result = query.exec_()
    else:
        # on passe une seule commande :
        # query = utils_db.queryExecute(commandLine, query=query)
        commandLine = what
        try:
            query.clear()
            result = query.exec_(utils_functions.u(commandLine))
        except:
            result = query.exec_(commandLine)
    if result == False:
        utils_functions.myPrint('')
        utils_functions.myPrint('MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM')
        utils_functions.myPrint('MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM')
        utils_functions.myPrint('MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM')
        utils_functions.myPrint('queryExecute: FALSE')
        utils_functions.myPrint(commandLine)
        utils_functions.myPrint(query.lastError().text())
        utils_functions.myPrint('WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW')
        utils_functions.myPrint('WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW')
        utils_functions.myPrint('WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW')
        utils_functions.myPrint('')
    return query


def query2List(what, order=[], db=None, query=None):
    """
    pour faire une requête SELECT en contournant la limitation de SQLITE
    qui ne fait les tris (ORDER BY) que sur les caractères ascii.
    Utilisé surtout pour trier les élèves ou profs par ordre alphabétique.

    what est soit
        * une seule commande (string)
        * un dictionnaire (pour une commande avec prepare-bind)
            * la clé est la commande (contenant des ?)
            * la valeur est un tuple.
    exemples :
        queryList = utils_db.query2List(commandLine, order=['NOM', 'Prenom'], query=query)
        queryList = utils_db.query2List({commandLine: ('A', 0)}, order=['NOM', 'Prenom'], query=query)
    """
    theList = []
    listForSort = []

    query = queryExecute(what, db=db, query=query)
    orderIndex = []
    for columnName in order:
        orderIndex.append(query.record().indexOf(columnName))
    recordCount = query.record().count()
    while query.next():
        asciiName = ''
        for i in orderIndex:
            asciiName = utils_functions.u('{0} {1}').format(
                asciiName, utils_functions.doAscii(query.value(i)))
        line = [asciiName, ]
        for i in range(recordCount):
            line.append(query.value(i))
        listForSort.append(line)
    listForSort = sorted(listForSort)
    for record in listForSort:
        theList.append(record[1:])
    if db != None:
        query.clear()
        del query
    return theList










# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#                   UNE LISTE DE COMMANDES
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# commandes générales, 
# ou utilisées dans plusieurs tables :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qcmd_Vacuum = 'VACUUM'
q_dropTable = 'DROP TABLE IF EXISTS "{0}"'
q_pragmaTable = 'PRAGMA table_info("{0}")'

qct_config = (
    'CREATE TABLE IF NOT EXISTS config '
    '(key_name TEXT UNIQUE, value_int INTEGER, value_text TEXT)')

qdf_table = 'DELETE FROM {0}'
qdf_where = 'DELETE FROM {0} WHERE {1} IN ({2})'
qdf_whereText = utils_functions.u('DELETE FROM {0} WHERE {1}="{2}"')

q_selectAllFrom = 'SELECT * FROM {0}'
q_selectAllFromOrder = 'SELECT * FROM {0} ORDER BY {1}'
q_selectDistinctAllFrom = 'SELECT DISTINCT * FROM {0}'
q_selectDistinctAllFromOrder = 'SELECT DISTINCT * FROM {0} ORDER BY {1}'
q_selectAllFromWhere = 'SELECT * FROM {0} WHERE {1} IN ({2})'
q_selectAllFromWhereText = utils_functions.u('SELECT * FROM {0} WHERE {1}="{2}"')



def insertInto(table, nbCols=-1, db=None):
    """
    crèe et retourne une ligne INSERT INTO
        nom de la table et nombre de colonnes :
            commandLine_add = utils_db.insertInto('config', 3)
        nom de la table et base de donnée :
            commandLine_add = utils_db.insertInto('bulletin', db=main.db_commun)
        nom de la table seulement pour la base prof :
            commandLine_add = utils_db.insertInto('tableaux')
    """
    if db != None:
        nbCols = db.record(table).count()
    if nbCols == -1:
        if table in listTablesProf:
            nbCols = listTablesProf[table][1]
    commandLine = 'INSERT INTO {0} VALUES (?'.format(table)
    for i in range(nbCols - 1):
        commandLine = commandLine + ', ?'
    commandLine = commandLine + ')'
    return commandLine



# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base prof :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qs_prof_AllWhereNameLabel = utils_functions.u(
    'SELECT * FROM {0} WHERE Name=? AND Label=?')
qs_prof_AllWhereName = utils_functions.u('SELECT * FROM {0} WHERE Name=?')
qs_prof_AllWhereLabel = utils_functions.u('SELECT * FROM {0} WHERE Label=?')

qct_prof_items = (
    'CREATE TABLE IF NOT EXISTS items '
    '(id_item INTEGER PRIMARY KEY, Name TEXT, Label TEXT)')

qct_prof_bilans = (
    'CREATE TABLE IF NOT EXISTS bilans '
    '(id_bilan INTEGER PRIMARY KEY, Name TEXT, Label TEXT, id_competence INTEGER)')

qct_prof_itemBilan = (
    'CREATE TABLE IF NOT EXISTS item_bilan '
    '(id_item INTEGER, id_bilan INTEGER, coeff INTEGER)')
qdf_prof_itemBilan3 = (
    'DELETE FROM item_bilan '
    'WHERE id_item={0} AND id_bilan={1}')
qs_prof_AllFromItemBilanWhereItemBilan = (
    'SELECT * FROM item_bilan '
    'WHERE id_item={0} AND id_bilan={1}')

qct_prof_bilanBilan = (
    'CREATE TABLE IF NOT EXISTS bilan_bilan '
    '(id_bilan1 INTEGER, id_bilan2 INTEGER)')
qdf_prof_bilanBilan = 'DELETE FROM bilan_bilan WHERE id_bilan1={0} AND id_bilan2={1}'
qs_prof_fromBilanBilan1 = 'SELECT id_bilan1 FROM bilan_bilan WHERE id_bilan2={0}'
qs_prof_fromBilanBilan2 = 'SELECT id_bilan2 FROM bilan_bilan WHERE id_bilan1={0}'
qs_prof_fromBilanBilan = 'SELECT * FROM bilan_bilan WHERE id_bilan1={0} AND id_bilan2={1}'
qs_prof_bilanBilanNames = (
    'SELECT bilans1.id_bilan, bilans1.Name, bilans2.id_bilan, bilans2.Name '
    'FROM bilan_bilan '
    'JOIN bilans AS bilans1 ON bilans1.id_bilan=bilan_bilan.id_bilan1 '
    'JOIN bilans AS bilans2 ON bilans2.id_bilan=bilan_bilan.id_bilan2')

qct_prof_comments = (
    'CREATE TABLE IF NOT EXISTS comments '
    '(isItem INTEGER, id INTEGER, comment TEXT)')
qs_prof_FromComments = 'SELECT * FROM comments WHERE isItem={0} AND id={1}'

qct_prof_tableaux = (
    'CREATE TABLE IF NOT EXISTS tableaux '
    '(id_tableau INTEGER PRIMARY KEY, Name TEXT, Public INTEGER, '
    'ordre INTEGER, Periode INTEGER, id_groupe INTEGER, Label TEXT)')
qs_prof_tableauxEtMatieres = (
    'SELECT tableaux.*, groupes.* '
    'FROM tableaux '
    'JOIN groupes ON groupes.id_groupe=tableaux.id_groupe '
    'ORDER BY groupes.Matiere, groupes.Name')
qs_prof_tableauName = utils_functions.u('SELECT * FROM tableaux WHERE Name=?')
qs_prof_tableauNameGroupe = utils_functions.u(
    'SELECT * FROM tableaux '
    'WHERE Name=? AND id_groupe=?')
qs_prof_tableaux = (
    'SELECT tableaux.* '
    'FROM tableaux '
    'JOIN groupes ON groupes.id_groupe=tableaux.id_groupe '
    'ORDER BY groupes.Matiere, groupes.ordre, UPPER(groupes.Name), '
    'tableaux.ordre, UPPER(tableaux.Name)')
qs_prof_tableauxGroupe = (
    'SELECT * FROM tableaux '
    'WHERE id_groupe={0} '
    'ORDER BY ordre, Periode, UPPER(Name)')
qs_prof_groupesTableaux = (
    'SELECT groupes.*, tableaux.* '
    'FROM groupes '
    'JOIN tableaux ON tableaux.id_groupe=groupes.id_groupe '
    'ORDER BY groupes.ordre, groupes.Matiere, tableaux.Periode, tableaux.ordre')
qs_prof_groupesBilans = (
    'SELECT groupe_bilan.id_groupe, groupe_bilan.Periode, '
    'groupes.Matiere, groupes.Name, bilans.id_competence, bilans.Name '
    'FROM groupe_bilan '
    'JOIN bilans ON bilans.id_bilan=groupe_bilan.id_bilan '
    'JOIN groupes ON groupes.id_groupe=groupe_bilan.id_groupe '
    'WHERE bilans.id_competence>-2 '
    'ORDER BY groupe_bilan.id_groupe, groupe_bilan.Periode, bilans.id_competence')


qs_prof_tableauxTemplate = (
    'SELECT DISTINCT tableau_item.id_tableau, tableaux.Name '
    'FROM tableau_item '
    'JOIN tableaux ON tableaux.id_tableau=tableau_item.id_tableau '
    'WHERE tableau_item.id_item={0} ORDER BY tableaux.ordre')

qct_prof_templates = (
    'CREATE TABLE IF NOT EXISTS templates '
    '(id_template INTEGER, Name TEXT, ordre INTEGER, Label TEXT)')

qct_prof_tableauItem = (
    'CREATE TABLE IF NOT EXISTS tableau_item '
    '(id_tableau INTEGER, id_item INTEGER, ordre INTEGER)')
qdf_prof_tableauItem2 = 'DELETE FROM tableau_item WHERE id_tableau={0} AND id_item={1}'
qs_prof_ItemsInTableau = 'SELECT id_item FROM tableau_item WHERE id_tableau={0} ORDER BY ordre'
qs_prof_tableauItem = 'SELECT * FROM tableau_item WHERE id_tableau={0} ORDER BY ordre'
qs_prof_tableauItem2 = (
    'SELECT DISTINCT items.* '
    'FROM tableau_item '
    'JOIN items ON items.id_item=tableau_item.id_item '
    'WHERE tableau_item.id_tableau={0} '
    'ORDER BY tableau_item.ordre')
qs_prof_tableauItem3 = 'SELECT * FROM tableau_item WHERE id_tableau={0} AND id_item={1}'

qct_prof_evaluations = (
    'CREATE TABLE IF NOT EXISTS evaluations '
    '(id_tableau INTEGER, id_eleve INTEGER, '
    'id_item INTEGER, id_bilan INTEGER, value TEXT)')
qdf_prof_evaluations2 = 'DELETE FROM evaluations WHERE id_tableau={0} AND id_eleve={1}'
qdf_prof_evaluations3 = 'DELETE FROM evaluations WHERE id_tableau={0} AND id_item={1}'
qdf_prof_evaluations5 = (
    'DELETE FROM evaluations '
    'WHERE id_tableau={0} AND id_eleve={1} AND id_item={2}')
qdf_prof_evaluations6 = (
    'DELETE FROM evaluations '
    'WHERE id_tableau={0} AND id_eleve={1} AND id_bilan={2}')
qdf_prof_evaluations11 = 'DELETE FROM evaluations WHERE id_tableau={0} AND id_item=-1'
qs_prof_FromEvaluations = 'SELECT * FROM evaluations WHERE id_tableau={0} AND id_eleve={1}'
qs_prof_evaluations = (
    'SELECT * FROM evaluations '
    'WHERE id_tableau IN ({0}) AND id_eleve IN ({1}) AND id_item IN ({2})')
qs_prof_evaluations2 = (
    'SELECT * FROM evaluations '
    'WHERE id_tableau IN ({0}) AND id_eleve IN ({1}) AND id_bilan IN ({2})')

qs_prof_ValueItem = (
    'SELECT value FROM evaluations '
    'WHERE id_tableau={0} AND id_eleve={1} AND id_item={2}')
qs_prof_ValueItem2 = (
    'SELECT value FROM evaluations '
    'WHERE ({0}) AND id_eleve={1} AND id_item={2}')
qs_prof_ValueBilan = (
    'SELECT value FROM evaluations '
    'WHERE id_tableau={0} AND id_eleve={1} AND id_bilan={2}')
qs_prof_ValuesItems = 'SELECT * FROM evaluations WHERE id_tableau IN ({0}) AND id_bilan<0'

qct_prof_appreciations = (
    'CREATE TABLE IF NOT EXISTS appreciations '
    '(id_eleve INTEGER, value TEXT, Matiere TEXT, Periode INTEGER)')
qdf_prof_appreciations2 = utils_functions.u(
    'DELETE FROM appreciations '
    'WHERE id_eleve={0} AND Matiere="{1}" AND Periode={2}')
qs_prof_appreciations = utils_functions.u(
    'SELECT * FROM appreciations '
    'WHERE id_eleve IN ({0}) '
    'AND Matiere="{1}" '
    'AND Periode IN ({2})')

qct_prof_moyennesItems = 'CREATE TABLE IF NOT EXISTS moyennesItems (value TEXT, moyenne TEXT)'

qs_prof_EleveItems = (
    'SELECT tableau_item.id_item, items.Name, items.Label, '
    'evaluations.value, bilans.id_bilan, bilans.Name '
    'FROM tableau_item '
    'JOIN items ON items.id_item=tableau_item.id_item '
    'JOIN evaluations ON evaluations.id_tableau={0} '
    'AND evaluations.id_eleve={1} '
    'AND evaluations.id_item=tableau_item.id_item '
    'JOIN item_bilan ON item_bilan.id_item=tableau_item.id_item '
    'JOIN bilans ON bilans.id_bilan=item_bilan.id_bilan '
    'WHERE tableau_item.id_tableau={2} '
    'AND bilans.id_competence>-2 '
    'ORDER BY tableau_item.ordre')

qs_prof_EleveBilans = (
    'SELECT bilans.id_bilan, bilans.Name, bilans.Label, bilans.id_competence, '
    'evaluations.value, item_bilan.id_item, item_bilan.coeff, items.Name '
    'FROM evaluations '
    'JOIN bilans ON bilans.id_bilan=evaluations.id_bilan '
    'JOIN item_bilan ON item_bilan.id_bilan=bilans.id_bilan '
    'JOIN items ON items.id_item=item_bilan.id_item '
    'WHERE evaluations.id_tableau={0} '
    'AND evaluations.id_eleve={1} '
    'AND evaluations.id_item=-1 '
    'AND bilans.id_competence>-2 '
    'ORDER BY bilans.id_bilan, item_bilan.id_item')

qs_prof_BilansNoCalcFromItems = (
    'SELECT bilans.id_bilan, bilans.Name, bilans.Label, '
    'item_bilan.id_item, items.Name '
    'FROM bilans '
    'JOIN item_bilan ON item_bilan.id_bilan=bilans.id_bilan '
    'JOIN items ON items.id_item=item_bilan.id_item '
    'WHERE bilans.id_competence=-2 '
    'AND item_bilan.id_item IN ({0}) '
    'ORDER BY item_bilan.id_item, bilans.Name')

qct_prof_groupes = (
    'CREATE TABLE IF NOT EXISTS groupes '
    '(id_groupe INTEGER PRIMARY KEY, Name TEXT, Matiere TEXT, '
    'isClasse INTEGER, nom_classe TEXT, ordre INTEGER)')

qct_prof_groupeEleve = (
    'CREATE TABLE IF NOT EXISTS groupe_eleve '
    '(id_groupe INTEGER, id_eleve INTEGER, ordre INTEGER)')
qs_prof_groupeEleve = (
    'SELECT * FROM groupe_eleve '
    'WHERE id_groupe={0} AND ordre!=-1 ORDER BY ordre')


qct_prof_profils = (
    'CREATE TABLE IF NOT EXISTS profils '
    '(id_profil INTEGER PRIMARY KEY, Name TEXT, Matiere TEXT, profilType TEXT)')

qct_prof_profilWho = (
    'CREATE TABLE IF NOT EXISTS profil_who '
    '(id_who INTEGER, id_profil INTEGER, Periode INTEGER, profilType TEXT)')

qct_prof_profilBilanBLT = (
    'CREATE TABLE IF NOT EXISTS profil_bilan_BLT '
    '(id_profil INTEGER, id_bilan INTEGER, ordre INTEGER)')

qs_prof_profilBilans = (
    'SELECT profil_bilan_BLT.id_bilan, bilans.Name, bilans.Label '
    'FROM profil_bilan_BLT '
    'JOIN bilans ON bilans.id_bilan=profil_bilan_BLT.id_bilan '
    'WHERE profil_bilan_BLT.id_profil IN ({0}) '
    'ORDER BY profil_bilan_BLT.ordre')


qct_prof_groupeBilan = (
    'CREATE TABLE IF NOT EXISTS groupe_bilan '
    '(id_bilan INTEGER, id_groupe INTEGER, Periode INTEGER, value TEXT)')
qs_prof_bilans = (
    'SELECT * FROM bilans '
    'WHERE id_competence<{0} '
    'AND bilans.id_competence>-2 '
    'ORDER BY UPPER(Name)')
qs_prof_groupeValue = (
    'SELECT value FROM groupe_bilan '
    'WHERE id_bilan={0} AND id_groupe={1} AND Periode={2}')

qct_prof_suivi_eleve_cpt = (
    'CREATE TABLE IF NOT EXISTS suivi_eleve_cpt '
    '(id_eleve INTEGER, id_cpt INTEGER, label2 TEXT)')

qct_prof_notes = (
    'CREATE TABLE IF NOT EXISTS notes '
    '(id_groupe INTEGER, Periode INTEGER, id_note INTEGER, Name TEXT, '
    'base INTEGER, coeff INTEGER, calcMode INTEGER, ordre INTEGER)')
qs_prof_notes = (
    'SELECT * FROM notes '
    'WHERE id_groupe={0} AND Periode={1} '
    'ORDER BY ordre, id_note')
qs_prof_notes2 = (
    'SELECT * FROM notes '
    'WHERE id_groupe={0} AND Periode={1} AND id_note={2}')
qdf_prof_notes = (
    'DELETE FROM notes '
    'WHERE id_groupe={0} AND Periode={1} AND id_note={2}')
qct_prof_notesValues = (
    'CREATE TABLE IF NOT EXISTS notes_values '
    '(id_groupe INTEGER, Periode INTEGER, id_note INTEGER, '
    'id_eleve INTEGER, value INTEGER)')
qs_prof_noteValue = (
    'SELECT * FROM notes_values '
    'WHERE id_groupe={0} AND Periode={1} '
    'AND id_note IN ({2}) AND id_eleve IN ({3})')
qs_prof_noteValue2 = (
    'SELECT value FROM notes_values '
    'WHERE id_groupe={0} AND Periode={1} AND id_note={2} AND id_eleve>=0')
qs_prof_noteValues = (
    'SELECT notes_values.value, notes.base, notes.coeff '
    'FROM notes_values '
    'JOIN notes ON ('
    'notes.id_groupe=notes_values.id_groupe '
    'AND notes.Periode=notes_values.Periode '
    'AND notes.id_note=notes_values.id_note) '
    'WHERE notes_values.id_groupe={0} '
    'AND notes_values.Periode={1} '
    'AND notes_values.id_eleve={2} '
    'AND notes_values.id_note>=0')
qdf_prof_noteValue = (
    'DELETE FROM notes_values '
    'WHERE id_groupe={0} AND Periode={1} AND id_note={2} AND id_eleve={3}')
qdf_prof_noteValue2 = (
    'DELETE FROM notes_values '
    'WHERE id_groupe={0} AND Periode={1} AND id_note={2}')

qct_prof_absences = (
    'CREATE TABLE IF NOT EXISTS absences '
    '(id_groupe INTEGER, Periode INTEGER, id_eleve INTEGER, '
    'abs0 INTEGER, abs1 INTEGER, abs2 INTEGER, abs3 INTEGER)')
qs_prof_absencesPeriodeEleves = (
    'SELECT * FROM absences '
    'WHERE Periode={0} AND id_eleve IN ({1})')
qs_prof_absencesPeriode = (
    'SELECT * FROM absences '
    'WHERE Periode={0}')
qdf_prof_absencesPeriodeEleves = (
    'DELETE FROM absences '
    'WHERE Periode={0} AND id_eleve IN ({1})')

qct_prof_counts = (
    'CREATE TABLE IF NOT EXISTS counts '
    '(id_groupe INTEGER, Periode INTEGER, id_count INTEGER, '
    'Name TEXT, sens INTEGER, calcul INTEGER, ordre INTEGER, Label TEXT, id_item INTEGER)')
qs_prof_counts = (
    'SELECT * FROM counts '
    'WHERE id_groupe={0} AND Periode={1} AND id_count>-1 '
    'ORDER BY ordre, id_count')
qs_prof_counts2 = (
    'SELECT * FROM counts '
    'WHERE id_groupe={0} AND Periode={1} AND id_count={2}')
qs_prof_counts3 = (
    'SELECT DISTINCT Name, sens, calcul, Label, id_item '
    'FROM counts '
    'ORDER BY ordre')
qs_prof_counts_items = (
    'SELECT counts.*, items.* '
    'FROM counts '
    'LEFT JOIN items ON items.id_item=counts.id_item '
    'WHERE counts.id_groupe={0} AND counts.Periode={1} AND counts.id_count>-1 '
    'ORDER BY counts.ordre, counts.id_count')
qdf_prof_counts = (
    'DELETE FROM counts '
    'WHERE id_groupe={0} AND Periode={1} AND id_count={2}')
qup_prof_counts = utils_functions.u(
    'UPDATE counts '
    'SET Name=?, sens=?, calcul=?, Label=?, id_item=? '
    'WHERE Name=?')
qct_prof_countsValues = (
    'CREATE TABLE IF NOT EXISTS counts_values '
    '(id_groupe INTEGER, Periode INTEGER, id_count INTEGER, '
    'id_eleve INTEGER, value INTEGER, perso TEXT)')
qs_prof_countValue = (
    'SELECT * FROM counts_values '
    'WHERE id_groupe={0} AND Periode={1} '
    'AND id_count IN ({2}) AND id_eleve IN ({3})')
qdf_prof_countValue2 = (
    'DELETE FROM counts_values '
    'WHERE id_groupe={0} AND Periode={1} AND id_count={2}')
qdf_prof_countValue = (
    'DELETE FROM counts_values '
    'WHERE id_groupe={0} AND Periode={1} AND id_count={2} AND id_eleve={3}')


qct_prof_dnbValues = (
    'CREATE TABLE IF NOT EXISTS dnb_values '
    '(id_eleve INTEGER, id_dnb INTEGER, value TEXT)')
qs_prof_dnbValues = 'SELECT * FROM dnb_values WHERE id_eleve IN ({0})'
qs_prof_dnbValues2 = 'SELECT * FROM dnb_values WHERE id_eleve={0} AND id_dnb={1}'
qdf_prof_dnbValues = 'DELETE FROM dnb_values WHERE id_eleve={0} AND id_dnb={1}'

qct_prof_lsu = (
    'CREATE TABLE IF NOT EXISTS lsu '
    '(lsuWhat TEXT, '
    'lsu1 TEXT, lsu2 TEXT, lsu3 TEXT, lsu4 TEXT, '
    'lsu5 TEXT, lsu6 TEXT, lsu7 TEXT, lsu8 TEXT)')

# une liste de toutes les tables (nom, commande de création, nombre de colonnes) :
listTablesProf = {
    'config': (qct_config, 3),
    'items': (qct_prof_items, 3),
    'bilans': (qct_prof_bilans, 4),
    'item_bilan': (qct_prof_itemBilan, 3),
    'bilan_bilan': (qct_prof_bilanBilan, 2),
    'comments': (qct_prof_comments, 3),
    'tableaux': (qct_prof_tableaux, 7),
    'templates': (qct_prof_templates, 4),
    'tableau_item': (qct_prof_tableauItem, 3),
    'evaluations': (qct_prof_evaluations, 5),
    'appreciations': (qct_prof_appreciations, 4),
    'moyennesItems': (qct_prof_moyennesItems, 2),
    'suivi_eleve_cpt': (qct_prof_suivi_eleve_cpt, 3),
    'groupes': (qct_prof_groupes, 6),
    'groupe_eleve': (qct_prof_groupeEleve, 3),
    'groupe_bilan': (qct_prof_groupeBilan, 4),
    'profils': (qct_prof_profils, 4),
    'profil_who': (qct_prof_profilWho, 4),
    'profil_bilan_BLT': (qct_prof_profilBilanBLT, 3),
    'notes': (qct_prof_notes, 8),
    'notes_values': (qct_prof_notesValues, 5),
    'absences': (qct_prof_absences, 7),
    'counts': (qct_prof_counts, 9),
    'counts_values': (qct_prof_countsValues, 6),
    'dnb_values': (qct_prof_dnbValues, 3),
    'lsu': (qct_prof_lsu, 9),
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables pour le suivi des élèves :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

qct_suivi_pp_eleve_cpt = (
    'CREATE TABLE IF NOT EXISTS suivi_pp_eleve_cpt '
    '(id_pp INTEGER, id_eleve INTEGER, id_cpt INTEGER, label2 TEXT)')
qct_suivi_evals = (
    'CREATE TABLE IF NOT EXISTS suivi_evals '
    '(id_prof INTEGER, matiere TEXT, id_pp INTEGER, id_eleve INTEGER, '
    'date TEXT, horaire INTEGER, id_cpt INTEGER, value TEXT)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base users :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_users_eleves = (
    'CREATE TABLE IF NOT EXISTS eleves '
    '(id INTEGER PRIMARY KEY, NOM TEXT, Prenom TEXT, Classe TEXT, '
    'Login TEXT, Mdp TEXT, initialMdp TEXT, '
    'sexe INTEGER, date_naiss TEXT)')
qs_users_elevesWhereId = 'SELECT * FROM eleves WHERE id IN ({0})'

qct_users_profs = (
    'CREATE TABLE IF NOT EXISTS profs '
    '(id INTEGER PRIMARY KEY, NOM TEXT, Prenom TEXT, Login TEXT, '
    'Mdp TEXT, initialMdp TEXT, Matiere TEXT)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base commun :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_commun_listeCpt = (
    'CREATE TABLE IF NOT EXISTS {0} '
    '(id INTEGER PRIMARY KEY, code TEXT, '
    'Titre1 TEXT, Titre2 TEXT, Titre3 TEXT, Competence TEXT, '
    'T1 TEXT, T2 TEXT, T3 TEXT, Cpt TEXT, '
    'Commentaire TEXT, classeType INTEGER, ordre INTEGER)')
qct_commun_classes = (
    'CREATE TABLE IF NOT EXISTS classes '
    '(id INTEGER PRIMARY KEY, Classe TEXT, classeType INTEGER, notes INTEGER, ordre INTEGER)')
qs_commun_classes = 'SELECT * FROM classes WHERE ordre<9999 ORDER BY ordre, Classe'
qct_commun_classesTypes = (
    'CREATE TABLE IF NOT EXISTS classestypes '
    '(id INTEGER PRIMARY KEY, name TEXT)')
qct_commun_bilans_liens = (
    'CREATE TABLE IF NOT EXISTS bilans_liens '
    '(table1 TEXT, code1 TEXT, '
    'table2 TEXT, code2 TEXT, '
    'equivalence INTEGER)')

qct_commun_matieres = (
    'CREATE TABLE IF NOT EXISTS matieres '
    '(id INTEGER PRIMARY KEY, Matiere TEXT, MatiereCode TEXT, MatiereLabel TEXT, '
    'OrdreBLT INTEGER, id_prof INTEGER)')

qct_commun_periodes = (
    'CREATE TABLE IF NOT EXISTS periodes '
    '(id INTEGER PRIMARY KEY, Periode TEXT)')
qct_commun_sousRubriques = (
    'CREATE TABLE IF NOT EXISTS sous_rubriques '
    '(id INTEGER, code TEXT, '
    'bulletin TEXT, referentiel TEXT, confidentiel TEXT)')
qct_commun_horaires = (
    'CREATE TABLE IF NOT EXISTS horaires '
    '(id INTEGER PRIMARY KEY, Name TEXT, Label TEXT)')
qct_commun_lsu = (
    'CREATE TABLE IF NOT EXISTS lsu '
    '(lsuWhat TEXT, '
    'lsu1 TEXT, lsu2 TEXT, lsu3 TEXT, lsu4 TEXT, '
    'lsu5 TEXT, lsu6 TEXT, lsu7 TEXT, lsu8 TEXT)')


qs_commun_classeType = utils_functions.u('SELECT classeType FROM {0} WHERE code=?')

# une liste de toutes les tables (nom, commande de création, nombre de colonnes) :
listTablesCommun = {
    'config': (qct_config, 3),
    'matieres': (qct_commun_matieres, 6),
    'classes': (qct_commun_classes, 5),
    'classestypes': (qct_commun_classesTypes, 2),
    'periodes': (qct_commun_periodes, 2),
    'horaires': (qct_commun_horaires, 3),
    'bulletin': (qct_commun_listeCpt.format('bulletin'), 13),
    'referentiel': (qct_commun_listeCpt.format('referentiel'), 13),
    'confidentiel': (qct_commun_listeCpt.format('confidentiel'), 13),
    'suivi': (qct_commun_listeCpt.format('suivi'), 13),
    'sous_rubriques': (qct_commun_sousRubriques, 5),
    'bilans_liens': (qct_commun_bilans_liens, 5),
    'lsu': (qct_commun_lsu, 9),
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base admin :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_admin_eleves = (
    'CREATE TABLE IF NOT EXISTS eleves '
    '(id INTEGER PRIMARY KEY, num TEXT, NOM TEXT, Prenom TEXT, Classe TEXT, '
    'Login TEXT, Date_naiss TEXT, Mdp TEXT, AnDernier TEXT, '
    'eleve_id INTEGER, dateEntree INTEGER, dateSortie INTEGER, '
    'sexe INTEGER)')
qct_admin_profs = (
    'CREATE TABLE IF NOT EXISTS profs '
    '(id INTEGER PRIMARY KEY, num TEXT, '
    'NOM TEXT, Prenom TEXT, Matiere TEXT, Login TEXT, Mdp TEXT)')
qct_admin_ftpGet = (
    'CREATE TABLE IF NOT EXISTS ftpGetDir '
    '(fileType TEXT, fileName TEXT, datetime INTEGER)')
qdf_admin_ftpGet = utils_functions.u(
    'DELETE FROM ftpGetDir '
    'WHERE fileType="{0}" AND fileName IN ({1})')
qct_admin_ftpPut = (
    'CREATE TABLE IF NOT EXISTS ftpPutDir '
    '(fileType TEXT, fileName TEXT, datetime INTEGER)')
qs_admin_ftpPut = utils_functions.u(
    'SELECT datetime FROM ftpPutDir '
    'WHERE fileType="{0}" AND fileName="{1}"')
qdf_admin_ftpPut = utils_functions.u(
    'DELETE FROM ftpPutDir '
    'WHERE fileType="{0}" AND fileName="{1}"')
qct_admin_replacements = (
    'CREATE TABLE IF NOT EXISTS replacements '
    '(replaceWhere TEXT, replaceWhat TEXT, replaceWith TEXT)')
qct_admin_configAdmin = (
    'CREATE TABLE IF NOT EXISTS config_admin '
    '(key_name TEXT UNIQUE, value_int INTEGER, value_text TEXT)')
qct_admin_configCalculs = (
    'CREATE TABLE IF NOT EXISTS config_calculs '
    '(key_name TEXT UNIQUE, value_int INTEGER, value_text TEXT)')
qct_admin_adresses = (
    'CREATE TABLE IF NOT EXISTS adresses '
    '(id_eleve INTEGER, eleve_id INTEGER, state INTEGER, '
    'nom1 TEXT, nom2 TEXT, adresse TEXT, use INTEGER)')
qct_admin_lsu = (
    'CREATE TABLE IF NOT EXISTS lsu '
    '(lsuWhat TEXT, '
    'lsu1 TEXT, lsu2 TEXT, lsu3 TEXT, lsu4 TEXT, lsu5 TEXT)')

# une liste de toutes les tables (nom, commande de création, nombre de colonnes) :
listTablesAdmin = {
    'config': (qct_config, 3), 
    'config_admin': (qct_admin_configAdmin, 3), 
    'config_calculs': (qct_admin_configCalculs, 3), 
    'eleves': (qct_admin_eleves, 13), 
    'profs': (qct_admin_profs, 7), 
    'ftpGetDir': (qct_admin_ftpGet, 3), 
    'ftpPutDir': (qct_admin_ftpPut, 3), 
    'replacements': (qct_admin_replacements, 3), 
    'adresses': (qct_admin_adresses, 7), 
    'lsu': (qct_admin_lsu, 6), 
    }


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base recup_evals :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_recupEvals_filesprofconfig = (
    'CREATE TABLE IF NOT EXISTS files_prof_config '
    '(id_prof INTEGER, datetime INTEGER)')
qct_recupEvals_eleveProf = (
    'CREATE TABLE IF NOT EXISTS eleve_prof '
    '(id_prof INTEGER, id_eleve INTEGER, Matiere TEXT, Periode INTEGER)')
qct_recupEvals_eleveGroupe = (
    'CREATE TABLE IF NOT EXISTS eleve_groupe '
    '(id_prof INTEGER, id_eleve INTEGER, id_groupe INTEGER, '
    'groupeName TEXT, Matiere TEXT, Periode INTEGER)')
qct_recupEvals_bilans = (
    'CREATE TABLE IF NOT EXISTS bilans '
    '(id_prof INTEGER, id_eleve INTEGER, id_bilan INTEGER, id_competence INTEGER, '
    'Name TEXT, Label TEXT, Value TEXT, Matiere TEXT, Periode INTEGER)')
qct_recupEvals_persos = (
    'CREATE TABLE IF NOT EXISTS persos '
    '(id_prof INTEGER, id_eleve INTEGER, id_bilan INTEGER, Name TEXT, Label TEXT, '
    'Value TEXT, groupeValue TEXT, Matiere TEXT, ordre INTEGER, Periode INTEGER)')
qct_recupEvals_appreciations = (
    'CREATE TABLE IF NOT EXISTS appreciations '
    '(id_prof INTEGER, id_eleve INTEGER, '
    'appreciation TEXT, Matiere TEXT, Periode INTEGER)')
qct_recupEvals_detailsLinks = (
    'CREATE TABLE IF NOT EXISTS details_links '
    '(id_prof INTEGER, id_item INTEGER, id_bilan INTEGER, '
    'id_competence INTEGER, itemLabel TEXT)')
qct_recupEvals_detailsValues = (
    'CREATE TABLE IF NOT EXISTS details_values '
    '(id_prof INTEGER, id_tableau INTEGER, id_eleve INTEGER, '
    'id_item INTEGER, value TEXT, Periode INTEGER)')
qct_recupEvals_notes = (
    'CREATE TABLE IF NOT EXISTS notes '
    '(id_prof INTEGER, id_eleve INTEGER, '
    'Matiere TEXT, Periode INTEGER, value INTEGER)')
qct_recupEvals_absences = (
    'CREATE TABLE IF NOT EXISTS absences '
    '(id_prof INTEGER, id_eleve INTEGER, Periode INTEGER, '
    'abs0 INTEGER, abs1 INTEGER, abs2 INTEGER, abs3 INTEGER)')
qct_recupEvals_dnbValues = (
    'CREATE TABLE IF NOT EXISTS dnb_values '
    '(id_prof INTEGER, id_eleve INTEGER, id_dnb INTEGER, value TEXT)')
qct_recupEvals_lsu = (
    'CREATE TABLE IF NOT EXISTS lsu '
    '(id_prof INTEGER, lsuWhat TEXT, '
    'lsu1 TEXT, lsu2 TEXT, lsu3 TEXT, lsu4 TEXT, '
    'lsu5 TEXT, lsu6 TEXT, lsu7 TEXT, lsu8 TEXT)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base documents :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_documents_eleves = (
    'CREATE TABLE IF NOT EXISTS eleves '
    '(id_eleve INTEGER, nomFichier TEXT, labelDocument TEXT, type INTEGER)')
qct_documents_profs = (
    'CREATE TABLE IF NOT EXISTS profs '
    '(id INTEGER, nomFichier TEXT, labelDocument TEXT, type INTEGER)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base structures :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_structures_structures = (
    'CREATE TABLE IF NOT EXISTS structures '
    '(matiere TEXT, title TEXT, fileName TEXT, ordre INTEGER)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base resultats :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_resultats_eleveProf = (
    'CREATE TABLE IF NOT EXISTS eleve_prof '
    '(id_prof INTEGER, id_eleve INTEGER, Matiere TEXT)')
qct_resultats_eleveGroupe = (
    'CREATE TABLE IF NOT EXISTS eleve_groupe '
    '(id_prof INTEGER, id_eleve INTEGER, '
    'id_groupe INTEGER, groupeName TEXT, Matiere TEXT)')
qct_resultats_eleves = (
    'CREATE TABLE IF NOT EXISTS eleves '
    '(id_eleve INTEGER, DateFichier TEXT, Classe TEXT, '
    'NOM_Prenom TEXT, Date_Naissance TEXT, Login TEXT, initialMdp TEXT)')
qct_resultats_appreciations = (
    'CREATE TABLE IF NOT EXISTS appreciations '
    '(id_eleve INTEGER, matiere TEXT, appreciation TEXT, id_prof INTEGER)')
qct_resultats_bilans = (
    'CREATE TABLE IF NOT EXISTS bilans '
    '(id_eleve INTEGER, name TEXT, value TEXT)')
qct_resultats_bilansDetails = (
    'CREATE TABLE IF NOT EXISTS bilans_details '
    '(id_eleve INTEGER, name TEXT, id_prof INTEGER, matiere TEXT, value TEXT)')
qct_resultats_persos = (
    'CREATE TABLE IF NOT EXISTS persos '
    '(id_eleve INTEGER, name TEXT, label TEXT, '
    'value TEXT, groupeValue TEXT, id_prof INTEGER)')
qct_resultats_syntheses = (
    'CREATE TABLE IF NOT EXISTS syntheses '
    '(id_eleve INTEGER, name TEXT, value TEXT, id_prof INTEGER)')
qct_resultats_notes = (
    'CREATE TABLE IF NOT EXISTS notes '
    '(id_eleve INTEGER, matiere TEXT, '
    'value INTEGER, id_classe INTEGER, id_prof INTEGER)')
qct_resultats_notes_classes = (
    'CREATE TABLE IF NOT EXISTS notes_classes '
    '(id_classe INTEGER, matiere TEXT, '
    'moyenne INTEGER, ecart INTEGER, mini INTEGER, maxi INTEGER, id_prof INTEGER)')
qct_resultats_absences = (
    'CREATE TABLE IF NOT EXISTS absences '
    '(id_eleve INTEGER, '
    'abs0 INTEGER, abs1 INTEGER, abs2 INTEGER, abs3 INTEGER)')
qct_resultats_lsu = (
    'CREATE TABLE IF NOT EXISTS lsu '
    '(lsuWhat TEXT, id_eleve INTEGER, '
    'lsu1 TEXT, lsu2 TEXT, lsu3 TEXT, lsu4 TEXT)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base referential_propositions :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_referentialPropositions_bilansDetails = (
    'CREATE TABLE IF NOT EXISTS bilans_details '
    '(id_eleve INTEGER, annee_scolaire TEXT, date TEXT, Matiere TEXT, '
    'nom_prof TEXT, bilan_name TEXT, value TEXT)')
qct_referentialPropositions_bilansPropositions = (
    'CREATE TABLE IF NOT EXISTS bilans_propositions '
    '(id_eleve INTEGER, date TEXT, bilan_name TEXT, value TEXT, validation INTEGER)')
qct_referentialPropositions_eleves = (
    'CREATE TABLE IF NOT EXISTS eleves '
    '(id_eleve INTEGER, NOM TEXT, Prenom TEXT, annee_scolaire TEXT)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base referential_validations :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_referentialValidations_bilansValidations = (
    'CREATE TABLE IF NOT EXISTS bilans_validations '
    '(id_eleve INTEGER, date TEXT, bilan_name TEXT, '
    'id_prof INTEGER, nom_prof TEXT, value TEXT, validation INTEGER)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base config :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_config_schedules = (
    'CREATE TABLE IF NOT EXISTS schedules '
    '(versionName TEXT, versionUrl TEXT, versionLabel TEXT, '
    'adminDir TEXT, typeSchedule INTEGER)')
qct_config_conn = (
    'CREATE TABLE IF NOT EXISTS conn '
    '(id_conn INTEGER, save INTEGER, user TEXT, mdp TEXT)')


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# les tables de la base configweb :
# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
qct_configweb_dbconfig = (
    'CREATE TABLE IF NOT EXISTS dbconfig '
    '(key_name TEXT UNIQUE, value_int INTEGER, value_text TEXT)')
qct_configweb_state = (
    'CREATE TABLE IF NOT EXISTS state '
    '(key_name TEXT UNIQUE, value_int INTEGER, value_text TEXT)')

# une liste de toutes les tables (nom, commande de création, nombre de colonnes) :
listTablesConfigWeb = {
    'config': (qct_config, 3),
    'dbconfig': (qct_configweb_dbconfig, 3),
    'state': (qct_configweb_state, 3),
    }



"""
###########################################################
###########################################################

                base memory

###########################################################
###########################################################
"""

def copyDBMy(main, dbFile_source, dbFile_dest):
    """
    copie la base prof vers la base en mémoire vive ou l'inverse.
    self.db_my = utils_db.copyDBMy(self, dbFile_myTemp, 'db_memory')
    """
    # connexion aux 2 bases :
    db_source = createConnection(main, dbFile_source)[0]
    db_dest = createConnection(main, dbFile_dest)[0]
    query_source = query(db_source)
    query_dest = query(db_dest)
    # on récupère la version de la base :
    versionDB = 0
    commandLine_source = 'SELECT value_int FROM config WHERE key_name="versionDB"'
    query_source = queryExecute(commandLine_source, query=query_source)
    while query_source.next():
        versionDB = int(query_source.value(0))
    utils_functions.myPrint('copyDBMy versionDB', versionDB)
    # on récupère la liste des tables à recopier :
    listTables = {}
    for table in listTablesProf:
        listTables[table] = listTablesProf[table]
    upgradeTables = {'OLD': {}, 'NEW': {}}
    try :
        transactionOK = db_dest.transaction()
        # suppression et recréation des tables de base :
        listArgs = []
        for table in listTables:
            listArgs.append(q_dropTable.format(table))
            listArgs.append(listTables[table][0])
        query_dest = queryExecute(listArgs, query=query_dest)
        # remplissage des tables de base :
        for table in listTables:
            if not(table in upgradeTables['NEW']):
                # calcul du nombre de colonnes de la table :
                nbCols = listTables[table][1]
                # mise en forme de commandLine_source et de commandLine_dest :
                commandLine_source = q_selectDistinctAllFrom.format(table)
                query_source = queryExecute(commandLine_source, query=query_source)
                commandLine_dest = insertInto(table, nbCols)
                lines = []
                # pour chaque ligne :
                while query_source.next():
                    # on récupère les valeurs dans listArgs :
                    listArgs = []
                    for i in range(nbCols):
                        listArgs.append(query_source.value(i))
                    lines.append(listArgs)
                # on copie dans la base :
                query_dest = queryExecute({commandLine_dest: lines}, query=query_dest)
    finally :
        if transactionOK:
            db_dest.commit()
        query_dest = queryExecute(qcmd_Vacuum, query=query_dest)
        return db_dest



def readVersionDB(db):
    # versionDB = utils_db.readVersionDB(db)
    versionDB = readInConfigTable(db, 'versionDB')[0]
    return versionDB

def configTable2Dict(db, table='config'):
    """
    retourne un dictionnaire des lignes d'une table config
    self.configDict = utils_db.configTable2Dict(self.db_my)
    """
    result = {}
    commandLine = q_selectDistinctAllFrom.format(table)
    query = queryExecute(commandLine, db=db)
    while query.next():
        result[query.value(0)] = [query.value(1), query.value(2)]
    query.clear()
    del query
    return result

def readInConfigDict(dico, key_name, default_int=0, default_text=''):
    """
    lit une clé du dictionnaire config
    self.id_groupe = utils_db.readInConfigDict(
        self.configDict, 'SelectedGroupe', default_int=-1)[0]
    bilanDirNameFormat = utils_db.readInConfigDict(
            self.configDict, 'bilanDirNameFormat', default_text='{0}-{1}')[1]
    """
    value_int = default_int
    value_text = default_text
    newKey = True
    if key_name in dico:
        [value_int, value_text] = dico[key_name]
        try:
            value_int = int(value_int)
        except:
            value_int = default_int
        newKey = False
    return (value_int, value_text, newKey)

def readInConfigTable(db, key_name, table='config', default_int=0, default_text=''):
    """
    lit une clé de la table db.config
    """
    value_int = default_int
    value_text = default_text
    newKey = True
    commandLine = 'SELECT value_int, value_text FROM {0} WHERE key_name="{1}"'
    commandLine = utils_functions.u(commandLine).format(table, key_name)
    query = queryExecute(commandLine, db=db)
    while query.next():
        try:
            value_int = int(query.value(0))
        except:
            value_int = default_int
        value_text = query.value(1)
        newKey = False
    query.clear()
    del query
    return (value_int, value_text, newKey)

def changeInConfigTable(main, db, changes={}, table='config', delete=True):
    """
    Écrit une ou plusieurs clés dans la table db.config (ou autre si précisé).
    Créé la clé si besoin, voir la table.
    Chaque clé du dictionnaire changes est une clé de la base à modifier,
        et changes[key_name] = (value_int, value_text)
    utils_db.changeInConfigTable(main, main.db_my, {'date': (aujourdhui, '')})
    """
    if len(changes) < 1:
        return False
    # la table existe-t-elle ?
    commandLine = 'PRAGMA table_info({0})'.format(table)
    query = queryExecute(commandLine, db=db)
    if query.last():
        lastCol = int(query.value(0))
    else:
        # la table n'existe pas, donc on la crée :
        commandLine = (
            'CREATE TABLE IF NOT EXISTS {0} (key_name TEXT UNIQUE, '
            'value_int INTEGER, value_text TEXT)')
        commandLine = utils_functions.u(commandLine).format(table)
        query = queryExecute(commandLine, db=db)
        # on inscrit la date :
        aujourdhui = int(QtCore.QDate.currentDate().toString('yyyyMMdd'))
        commandLine = utils_functions.u('REPLACE INTO {0} VALUES (?, ?, ?)').format(table)
        query = queryExecute({commandLine: ('date', aujourdhui, '')}, query=query)
    # on y va :
    msgChange = 'changeInConfigTable: [{0}] {1} ({2}, {3})'
    commandLineDelete = 'DELETE FROM {0} WHERE key_name="{1}"'
    commandLineInsert = 'INSERT INTO {0} VALUES (?, ?, ?)'
    lines = []
    for key_name in changes:
        (value_int, value_text) = changes[key_name]
        try:
            msg = utils_functions.u(msgChange).format(
                db.connectionName(), key_name, value_int, value_text)
            utils_functions.afficheMessage(main, msg)
        except:
            pass
        # on (re)crée la clé:
        commandLine = utils_functions.u(commandLineDelete).format(table, key_name)
        if delete:
            query = queryExecute(commandLine, query=query)
        lines.append((key_name, value_int, value_text))
    commandLine = utils_functions.u(commandLineInsert).format(table)
    query = queryExecute({commandLine: lines}, query=query)
    query.clear()
    del query
    return True

def deleteFromConfigTable(main, db, keys=[], table='config'):
    """
    Supprime une ou plusieurs clés dans la table db.config (ou autre si précisé).
    utils_db.deleteFromConfigTable(main, main.db_my, ['date', ])
    """
    if len(keys) < 1:
        return False
    # la table existe-t-elle ?
    commandLine = 'PRAGMA table_info({0})'.format(table)
    query = queryExecute(commandLine, db=db)
    if query.last():
        lastCol = int(query.value(0))
    else:
        return False
    # on y va :
    msgDelete = 'deleteFromConfigTable: [{0}] {1}'
    forIn = utils_functions.array2string(keys, text=True)
    for key_name in keys:
        try:
            msg = utils_functions.u(msgDelete).format(db.connectionName(), key_name)
            utils_functions.afficheMessage(main, msg)
        except:
            pass
    # on efface les clés :
    commandLine = utils_functions.u(qdf_where).format(table, 'key_name', forIn)
    query = queryExecute(commandLine, query=query)
    query.clear()
    del query
    return True



def readInConfigDB(main, key_name, table='config', default_int=0, default_text=''):
    """
    Lit une clé d'une table de la base config.sqlite.
    Gère s'il faut lire dans db_localConfig ou db_lanConfig
    """
    if main.db_lanConfig != None:
        # on est en réseau, donc on teste d'abord dans la base db_lanConfig :
        (value_int, value_text, newKey) = readInConfigTable(
            main.db_lanConfig, key_name, table, default_int, default_text)
        # si on n'a rien trouvé, on teste enfin dans db_localConfig :
        if newKey:
            (value_int, value_text, newKey) = readInConfigTable(
                main.db_localConfig, key_name, table, default_int, default_text)
    else:
        # pas de réseau, donc on utilise la base db_localConfig :
        (value_int, value_text, newKey) = readInConfigTable(
            main.db_localConfig, key_name, table, default_int, default_text)
    return (value_int, value_text, newKey)

def changeInConfigDB(main, key_name, value_int=0, value_text='', table='config'):
    """
    Écrit une clé dans une table de la base config.sqlite (db_localConfig).
    Gère si on a le droit de modifier cette clé (sinon elle est dans db_lanConfig).
    """
    # On teste d'abord si la clé existe dans la base db_lanConfig :
    if main.db_lanConfig != None:
        newKey = readInConfigTable(main.db_lanConfig, key_name, table)[2]
        if not(newKey):
            return False
    # si on n'a rien trouvé, on modifie dans la base db_localConfig :
    changeInConfigTable(main, main.db_localConfig, {key_name: (value_int, value_text)}, table)
    return True

def getColumns(table, db=None, query=None):
    """
    retourne la liste des colonnes d'une table sous la forme :
        [(columnIndex, columnName, columnType), ...]
    """
    if db != None:
        query = query(db)
    query.exec_(q_pragmaTable.format(table))
    columns = []
    while query.next():
        columnIndex = int(query.value(0))
        columnName = query.value(1)
        columnType = query.value(2)
        columns.append((columnIndex, columnName, columnType))
    if db != None:
        query.clear()
        del query
    return columns

def table2List(table, db=None, query=None, columns=[], order=''):
    """
    retourne une liste des lignes d'une table
    """
    result = []
    if db != None:
        query = query(db)
    if len(columns) < 1:
        columns = getColumns(table, db, query)
    if order == '':
        commandLine = q_selectDistinctAllFrom.format(table)
    else:
        commandLine = q_selectDistinctAllFromOrder.format(table, order)
    query = queryExecute(commandLine, query=query)
    while query.next():
        line = []
        for column in columns:
            try:
                data = query.value(column[0])
                if column[2] == 'INTEGER':
                    if int(data) == data:
                        line.append(int(data))
                    else:
                        line.append(data)
                else:
                    line.append(data)
            except:
                line.append('')
        result.append(tuple(line))
    if db != None:
        query.clear()
        del query
    return result

def tablesInDB(db=None, query=None):
    """
    retourne la liste des tables d'une base
    """
    tables = []
    commandLine = 'SELECT name FROM sqlite_master WHERE type="table"'
    query = queryExecute(commandLine, db=db, query=query)
    while query.next():
        tables.append(query.value(0))
    return tables

