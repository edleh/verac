# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    MISE À JOUR DES RÉSULTATS
    (récupération des fichiers profs, calcul des bilans, ...)
"""

# importation des modules utiles :
from __future__ import division, print_function
import os

import utils, utils_functions, utils_db, utils_filesdirs, utils_calculs
import admin, admin_ftp
import utils_markdown, utils_2lists

from copy import deepcopy

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
else:
    from PyQt4 import QtCore, QtGui as QtWidgets




###########################################################"
#   VARIABLES GLOBALES
###########################################################"

# fichier rapport-x.log :
REPORT_FILENAME = ''
STEP_TEXT = QtWidgets.QApplication.translate('main', 'Step')
STUDENTS_TO_EXCLUDE = []




def updateBilans(main, recupLevel=0, idsProfs=[-1], idsEleves=[-1], msgFin=True):
    """
    updateBilans sert à automatiser l'enchaînement des autres procédures en un seul appel.
        passer idsProfs=[-1] pour traiter tous les profs
        passer idsProfs=[] pour faire sélectionner les profs à traiter
        idem avec idsEleves
    Les procédures appelées sont  :
        downloadProfxxFiles
        recupProfxx
        createResultats
        calculateReferentialPropositions
        createReportFile
        uploadDBResultats
        createTeachersFilesArchive
    Les niveaux de récupération (recupLevel) :
        RECUP_LEVEL_SIMPLE : récup simple
        RECUP_LEVEL_ALL : récup complète sans envoi de la base referential_propositions
        RECUP_LEVEL_SELECT : récup complète d'une sélection de profs ou d'élèves
        RECUP_LEVEL_REFERENTIAL : récup complète avec envoi de la base referential_propositions
    """
    withMsgFin = msgFin
    debut = QtCore.QTime.currentTime()
    reponse = False
    withWarning = True

    if len(idsProfs) < 1:
        dialog = admin.ListProfsDlg(
            parent=main, helpContextPage='update-bilans')
        lastState = main.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            main.enableInterface(dialog, lastState)
            return reponse
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_prof = item.data(QtCore.Qt.UserRole)
            idsProfs.append(id_prof)
        main.enableInterface(dialog, lastState)
        if len(idsProfs) < 1:
            idsProfs = [-1]

    if len(idsEleves) < 1:
        dialog = admin.ListElevesDlg(
            parent=main, helpContextPage='update-bilans')
        lastState = main.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            main.enableInterface(dialog, lastState)
            return reponse
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            idsEleves.append(id_eleve)
        main.enableInterface(dialog, lastState)
        if len(idsEleves) < 1:
            idsEleves = [-1]

    # classes verrouillées :
    admin.loadLockedClasses(main)
    if len(admin.LOCKED_CLASSES['LOCKED']) > 0:
        global STUDENTS_TO_EXCLUDE
        STUDENTS_TO_EXCLUDE = []
        classesNames = utils_functions.array2string(
            admin.LOCKED_CLASSES['LOCKED'], text=True)
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = (
            'SELECT id FROM eleves '
            'WHERE Classe IN ({0})').format(classesNames)
        query_admin = utils_db.queryExecute(
            commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            STUDENTS_TO_EXCLUDE.append(id_eleve)

    try:
        admin.openDB(main, 'resultats')
        changes = {}
        query_resultats, transactionOK_resultats = utils_db.queryTransactionDB(
            admin.db_resultats)
        # on inscrit la date de la récup :
        aujourdhui = QtCore.QDateTime.currentDateTime().toString('dd/MM/yyyy')
        changes['dateRecup'] = ('', aujourdhui)
        # les infos sur le poste (peut être utile en cas de bug) :
        changes['computerInfos'] = ('', utils_functions.getComputerInfos())
        # On vérifie s'il faut augmenter recupLevel 
        # (suite à une mise à jour de la base résultat, ou en fonction du compteur) :
        if recupLevel in (utils.RECUP_LEVEL_SIMPLE, utils.RECUP_LEVEL_ALL):
            value_int = utils_db.readInConfigTable(admin.db_resultats, 'mustForcer')[0]
            if value_int == 1:
                # si mustForcer vaut 1 (mise à jour de la base), on force la récup :
                recupLevel = utils.RECUP_LEVEL_ALL
                withWarning = False
                changes['mustForcer'] = (-4, '')
            elif value_int == 0:
                # si mustForcer vaut 0, on force la récup :
                recupLevel = utils.RECUP_LEVEL_REFERENTIAL
                withWarning = False
                changes['mustForcer'] = (-4, '')
            else:
                # on incrémente mustForcer :
                if not(utils.ICI):
                    changes['mustForcer'] = (value_int + 1, '')
        utils_db.changeInConfigTable(main, admin.db_resultats, changes)
    finally:
        utils_db.endTransaction(query_resultats, admin.db_resultats, transactionOK_resultats)
        utils_filesdirs.removeAndCopy(admin.dbFileTemp_resultats, admin.dbFile_resultats)
    # téléchargement des fichiers des profs :
    if utils.ICI and (recupLevel == utils.RECUP_LEVEL_SELECT):
        reponse = True
    else:
        reponse, idsProfsOK = downloadProfxxFiles(
            main, recupLevel=recupLevel, idsProfs=idsProfs, msgFin=False)
        if recupLevel > utils.RECUP_LEVEL_SELECT:
            idsProfs = idsProfsOK
    # récupération des fichiers profs.
    # On récupère les messages pour ne les afficher qu'à la fin :
    recupProfxxOK = False
    teachersMessages = {'OK' : [], 'PB' : [], 'OLD' : []}
    if len(idsProfs) > 0:
        recupProfxxResult, teachersMessages = recupProfxx(
            main, 
            recupLevel=recupLevel, 
            withWarning=withWarning, 
            idsProfs=idsProfs, 
            idsEleves=idsEleves, 
            msgFin=False)
        elevesModifs = recupProfxxResult['elevesModifs']
        if (len(recupProfxxResult['newProfxxFiles']) > 0) and (len(elevesModifs) > 0):
            recupProfxxOK = True

    # calcul des résultats :
    if (recupLevel > utils.RECUP_LEVEL_SIMPLE) or (recupProfxxOK):
        if recupLevel > utils.RECUP_LEVEL_SIMPLE:
            reponse2 = createResultats(
                main, recupLevel=recupLevel, idsEleves=idsEleves, msgFin=False)
        else:
            reponse2 = createResultats(
                main, recupLevel=recupLevel, idsEleves=elevesModifs, msgFin=False)
        reponse = reponse and reponse2
        # on fait une archive des fichiers profs :
        admin.createTeachersFilesArchive(main)
        # des trucs en plus faits de temps en temps (quand recupLevel=RECUP_LEVEL_REFERENTIAL) :
        if recupLevel == utils.RECUP_LEVEL_REFERENTIAL:
            # demande de nettoyage de la base compteur :
            import admin_compteur
            try:
                admin_compteur.cleanCompteurSiteEtab(main)
            except:
                pass
            # calcul des propositions pour le référentiel :
            calculateReferentialPropositions(main, idsEleves=[-1])
        # le fichier rapport-x.log :
        if not(utils.NOGUI) and not(utils.ICI):
            createReportFile(main)
        # Si le paramètre "ICI" n'est pas passé en argument, on poste :
        if not(utils.ICI):
            if recupLevel == utils.RECUP_LEVEL_REFERENTIAL:
                what = 'all'
            else:
                what = 'resultats'
            reponse2 = uploadDBResultats(main, what=what, msgFin=False)
            reponse = reponse and reponse2

    # on affiche les messages profs :
    if len(teachersMessages['OK']) > 0:
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'List of teachers whose files were recovered'), 
            tags=('h2'))
        utils_functions.afficheMessage(
            main, 
            teachersMessages['OK'], 
            editLog=True)
    if len(teachersMessages['PB']) > 0:
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'List of teachers problematic files'), 
            tags=('h2'))
        utils_functions.afficheMessage(
            main, 
            teachersMessages['PB'], 
            editLog=True)
    if len(teachersMessages['OLD']) > 0:
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'ListTooOldProfs'), 
            tags=('h2'))
        utils_functions.afficheMessage(
            main, 
            teachersMessages['OLD'], 
            editLog=True)

    if withMsgFin:
        utils_functions.afficheMsgFin(main, timer=5)
    fin = QtCore.QTime.currentTime()
    utils_functions.afficheDuree(main, debut, fin)
    return reponse

def downloadProfxxFiles(main, recupLevel=0, idsProfs=[-1], msgFin=True):
    """
    Télécharge les fichiers profs et synchronise les différents dossiers.
    passer idsProfs=[-1] pour traiter tous les profs
    passer idsProfs=[] pour faire sélectionner les profs à traiter
    retourne la liste idsProfsOK des fichiers profs réellement mis à jours.
    """
    reponse, idsProfsOK = False, []
    dirTemp = main.tempPath + "/down/"
    newFiles = []
    newFilesLan, newFilesLocal, newFilesSite = {}, {}, {}

    # ICI LFTP
    if admin_ftp.LFTP:
        admin_ftp.ftpGetProfxxDir_LFTP(main)

    utils_functions.doWaitCursor()
    debut = QtCore.QTime.currentTime()

    if len(idsProfs) < 1:
        utils_functions.restoreCursor()
        dialog = admin.ListProfsDlg(
            parent=main, helpContextPage='update-bilans')
        lastState = main.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            main.enableInterface(dialog, lastState)
            return reponse, idsProfsOK
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_prof = item.data(QtCore.Qt.UserRole)
            idsProfs.append(id_prof)
        main.enableInterface(dialog, lastState)
        utils_functions.doWaitCursor()
        if len(idsProfs) < 1:
            idsProfs = [-1]

    try:
        # on vérifie d'abord les fichiers locaux :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'DownloadProfxxFilesLocal'), 
            tags=('h2'))
        reponse, newFilesLocal = admin.localGetProfxxDir(main, recupLevel=recupLevel, idsProfs=idsProfs)

        # on vérifie ensuite les fichiers du dossier réseau :
        if admin.dirLanProfxx != '':
            utils_functions.afficheMessage(
                main, 
                QtWidgets.QApplication.translate('main', 'DownloadProfxxFilesLan'), 
                tags=('h2'))
            reponse, newFilesLan = admin.lanGetProfxxDir(main, recupLevel=recupLevel, idsProfs=idsProfs)
            # et on synchronise les 2 dossiers :
            for prof in admin.PROFS:
                id_prof = prof[0]
                profxx = prof[1] + '.sqlite'
                if (profxx in newFilesLocal) and (profxx in newFilesLan):
                    # fichier dans les 2 listes
                    # on va donc vérifier par les dates enregistrées dans le fichier :
                    dateFileLanLL = newFilesLan[profxx][3]
                    dateFileLL = newFilesLocal[profxx][3]
                    if dateFileLL < dateFileLanLL:
                        utils_filesdirs.removeAndCopy(admin.dirLanProfxx + profxx, admin.dirLocalProfxx + profxx)
                        newFilesLocal[profxx] = newFilesLan[profxx]
                    elif dateFileLanLL < dateFileLL:
                        utils_filesdirs.removeAndCopy(admin.dirLocalProfxx + profxx, admin.dirLanProfxx + profxx)
                elif profxx in newFilesLocal:
                    # fichier seulement dans newFilesLocal :
                    utils_filesdirs.removeAndCopy(admin.dirLocalProfxx + profxx, admin.dirLanProfxx + profxx)
                elif profxx in newFilesLan:
                    # fichier seulement dans newFilesLan :
                    utils_filesdirs.removeAndCopy(admin.dirLanProfxx + profxx, admin.dirLocalProfxx + profxx)
                    newFilesLocal[profxx] = newFilesLan[profxx]

        # puis sur le site web :
        # ICI LFTP
        if admin_ftp.LFTP:
            # si on utilise LFTP, c'est déjà fait
            for prof in admin.PROFS:
                id_prof = prof[0]
                profxx = prof[1] + '.sqlite'
                if profxx in newFilesLocal:
                    idsProfsOK.append(id_prof)
        elif admin_ftp.ftpTest(main):
            utils_functions.afficheMessage(
                main, 
                QtWidgets.QApplication.translate('main', 'DownloadProfxxFilesWeb'), 
                tags=('h2'))
            # on télécharge dans temp pour ne pas écraser un fichier local plus récent :
            reponse, newFilesSite = admin_ftp.ftpGetProfxxDir(
                main, dirTemp, recupLevel=recupLevel, idsProfs=idsProfs)
            # on décide quoi faire :
            for prof in admin.PROFS:
                id_prof = prof[0]
                profxx = prof[1] + '.sqlite'
                if (profxx in newFilesLocal) and (profxx in newFilesSite):
                    # fichier dans les 2 listes :
                    idsProfsOK.append(id_prof)
                    # on va donc vérifier par les dates enregistrées dans le fichier :
                    dateFileTemp, dateFileTempLL = admin.getDateFile(main, dirTemp + profxx)
                    dateFileLL = newFilesLocal[profxx][3]
                    if dateFileLL < dateFileTempLL:
                        # le fichier téléchargé est plus récent, donc c'est lui qui compte :
                        utils_filesdirs.removeAndCopy(dirTemp + profxx, admin.dirLocalProfxx + profxx)
                        if admin.dirLanProfxx != '':
                            utils_filesdirs.removeAndCopy(dirTemp + profxx, admin.dirLanProfxx + profxx)
                elif profxx in newFilesLocal:
                    # fichier seulement dans newFilesLocal :
                    idsProfsOK.append(id_prof)
                elif profxx in newFilesSite:
                    # fichier seulement dans newFilesSite :
                    idsProfsOK.append(id_prof)
                    # on copie le fichier téléchargé :
                    utils_filesdirs.removeAndCopy(dirTemp + profxx, admin.dirLocalProfxx + profxx)
                    if admin.dirLanProfxx != '':
                        utils_filesdirs.removeAndCopy(dirTemp + profxx, admin.dirLanProfxx + profxx)
        else:
            # pas de FTP ; on vérifie seulement les fichiers locaux :
            for prof in admin.PROFS:
                id_prof = prof[0]
                profxx = prof[1] + '.sqlite'
                if profxx in newFilesLocal:
                    idsProfsOK.append(id_prof)

        # on efface les doublons :
        idsProfsOK = list(set(idsProfsOK))

        # au cas où ça arriverait, 
        # suppression des fichiers trouvés qui ne correspondraient pas à un prof :
        for profxx in newFilesLocal:
            id_prof = newFilesLocal[profxx][0]
            profExists = False
            for prof in admin.PROFS:
                if prof[0] == id_prof:
                    profExists = True
            if not(profExists):
                fileProfxx = admin.dirLocalProfxx + profxx
                if QtCore.QFileInfo(fileProfxx).exists():
                    utils_functions.myPrint('REMOVE : ', profxx)
                    QtCore.QFile(fileProfxx).remove()

    finally:
        if (recupLevel > utils.RECUP_LEVEL_SIMPLE) or (len(newFiles) > 0):
            utils_functions.afficheMessage(
                main, 
                QtWidgets.QApplication.translate('main', 'List of downloaded files profs'), 
                tags=('h2'))
            for prof in admin.PROFS:
                if utils_functions.u(prof[1] + '.sqlite') in newFiles:
                    utils_functions.afficheMessage(
                        main, 
                        utils_functions.u('{0} {1} ({2}.sqlite)').format(prof[2], prof[3], prof[1]), 
                        editLog=True)
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if reponse:
            if msgFin:
                fin = QtCore.QTime.currentTime()
                utils_functions.afficheDuree(main, debut, fin)
                utils_functions.afficheMsgFin(main, timer=5)
        else:
            message = QtWidgets.QApplication.translate('main', 'Downloading Teachers files failed')
            utils_functions.afficheMsgPb(main, message)
        return reponse, idsProfsOK

def recupProfxx(main, recupLevel=0, withWarning=True, idsProfs=[-1], idsEleves=[-1], msgFin=True):
    """
    Récupère le contenu des fichiers profs.
    passer idsProfs=[-1] pour traiter tous les profs
    passer idsProfs=[] pour faire sélectionner les profs à traiter
    idem pour idsEleves
    recupProfxxResult contient les listes de ce qui est récupéré
    """
    recupProfxxResult = {
        'newProfxxFiles': [], 
        'pbProfxxFiles': [], 
        'tooOldProfxxFiles': [], 
        'elevesModifs': [], 
        }

    utils_functions.doWaitCursor()
    debut = QtCore.QTime.currentTime()
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Retrieving files Teachers'), 
        tags=('h2'))
    try:
        if len(idsProfs) < 1:
            utils_functions.restoreCursor()
            dialog = admin.ListProfsDlg(
                parent=main, helpContextPage='update-bilans')
            lastState = main.disableInterface(dialog)
            if dialog.exec_() != QtWidgets.QDialog.Accepted:
                main.enableInterface(dialog, lastState)
                return recupProfxxResult
            for i in range(dialog.selectionList.count()):
                item = dialog.selectionList.item(i)
                id_prof = item.data(QtCore.Qt.UserRole)
                idsProfs.append(id_prof)
            main.enableInterface(dialog, lastState)
            utils_functions.doWaitCursor()
            if len(idsProfs) < 1:
                idsProfs = [-1]

        # Listes remplies dans la procédure recupProfxx
        # pour remplir les tables de la base recup_evals
        # et utilisées dans la procédure createTablesInRecupDB :
        # la liste des liens entre élèves et profs à modifier dans la base
        # la liste des bilans à modifier dans la base
        # la liste des appréciations à modifier dans la base
        # les détails (si demandés par l'admin)
        # la liste des liens entre élèves et groupes dans les fichiers profs
        # la liste des moyennes (pour les classes à notes)
        # la liste des absences (Vie scolaire)
        # la liste des avis pour le DNB (chef d'établissement)
        # la liste des epis, aps et parcours du prof
        calcLists = {
            'filesProfsConfig': [], 
            'eleveProf': [], 
            'bilans': [], 
            'persos': [], 
            'appreciations': [], 
            'detailsLinks': [], 
            'detailsValues': [], 
            'eleveGroupe': [], 
            'notes': [], 
            'absences': [], 
            'dnb': [], 
            'lsu': [], 
            }

        # liste des fichiers profs existants :
        list_profxx = []
        for aProf in admin.PROFS:
            if (idsProfs == [-1]) or (aProf[0] in idsProfs):
                profxx = aProf[1] + '.sqlite'
                fileProfxx = admin.dirLocalProfxx + profxx
                if QtCore.QFileInfo(fileProfxx).exists():
                    list_profxx.append(profxx)

        query_admin = utils_db.query(admin.db_admin)
        admin.openDB(main, 'recup_evals')
        query_recupEvals = utils_db.query(admin.db_recupEvals)
        versionDB_recupEvals = utils_db.readVersionDB(admin.db_recupEvals)

        if withWarning:
            if not(admin.verifProtectedPeriod(main, levelVerif=recupLevel)):
                return recupProfxxResult

        # Les lignes de commandes de récupération.
        # Les commandes se terminant par _Ex servent à récupérer 
        # les anciennes valeurs dans la base recup_evals.
        # Les autres seront lancées dans la base prof.

        # Fin des commandes afin de gérer les périodes :
        endForPeriodes = ''
        dicoPeriodes = {}
        for i in range(utils.NB_PERIODES):
            dicoPeriodes[i] = not(utils.PROTECTED_PERIODS[i])
            if utils.selectedPeriod > 0:
                if recupLevel > utils.RECUP_LEVEL_SIMPLE:
                    if i > utils.selectedPeriod:
                        dicoPeriodes[i] = False
                else:
                    if not(i in (utils.selectedPeriod, 0)):
                        dicoPeriodes[i] = False
        dicoPeriodes[999] = True
        for i in dicoPeriodes:
            if dicoPeriodes[i]:
                if endForPeriodes == '':
                    endForPeriodes = '{0}'.format(i)
                else:
                    endForPeriodes = '{0}, {1}'.format(endForPeriodes, i)
        endForPeriodes = 'AND P IN ({0})'.format(endForPeriodes)

        # on lance les récups des fichiers profs :
        for profxx in list_profxx:
            messages = [utils_functions.u('<br/><b>{0}</b>').format(profxx)]
            profLists, modifs, messages2 = doProf(
                main, recupLevel, profxx, idsEleves, endForPeriodes)
            for what in calcLists:
                calcLists[what].extend(profLists[what])
            for what in recupProfxxResult:
                recupProfxxResult[what].extend(modifs[what])
            messages.extend(messages2)
            utils_functions.afficheMessage(main, messages, editLog=True)

        # on efface les doublons :
        for what in recupProfxxResult:
            recupProfxxResult[what] = list(set(recupProfxxResult[what]))

        # mise à jour de la base si besoin :
        if (recupLevel > utils.RECUP_LEVEL_SIMPLE) or (len(recupProfxxResult['newProfxxFiles']) > 0):
            mustClear = (recupLevel > utils.RECUP_LEVEL_SELECT) and (idsProfs == [-1])
            createTablesInRecupDB(main, calcLists, mustClear, idsEleves)

    finally:
        # les messages ne sont pas affichés ici 
        # mais à la fin de la récupération :
        teachersMessages = {'OK' : [], 'PB' : [], 'OLD' : []}
        if (recupLevel > utils.RECUP_LEVEL_SIMPLE) or (len(recupProfxxResult['newProfxxFiles']) > 0):
            for aProf in admin.PROFS:
                if utils_functions.u(aProf[1] + '.sqlite') in recupProfxxResult['newProfxxFiles']:
                    message = utils_functions.u('{0} {1} ({2}.sqlite)').format(
                        aProf[2], aProf[3], aProf[1])
                    teachersMessages['OK'].append(message)
        if len(recupProfxxResult['pbProfxxFiles']) > 0:
            for aProf in admin.PROFS:
                if utils_functions.u(aProf[1] + '.sqlite') in recupProfxxResult['pbProfxxFiles']:
                    message = utils_functions.u('{0} {1} ({2}.sqlite)').format(
                        aProf[2], aProf[3], aProf[1])
                    teachersMessages['PB'].append(message)
        if len(recupProfxxResult['tooOldProfxxFiles']) > 0:
            for aProf in admin.PROFS:
                if utils_functions.u(aProf[1] + '.sqlite') in recupProfxxResult['tooOldProfxxFiles']:
                    message = utils_functions.u('{0} {1} ({2}.sqlite)').format(
                        aProf[2], aProf[3], aProf[1])
                    teachersMessages['OLD'].append(message)
        utils_functions.restoreCursor()
        if msgFin:
            fin = QtCore.QTime.currentTime()
            utils_functions.afficheDuree(main, debut, fin)
            utils_functions.afficheMsgFin(main, timer=5)

        return recupProfxxResult, teachersMessages


# commandes utilisées dans la procédure doProf :
CMD_PROF = {
    # commandes de récupération des anciens 
    # enregistrements d'une table dans recup_evals :
    'commandLineExProf': (
        'SELECT *, Periode AS P '
        'FROM {0} '
        'WHERE id_prof={1} {2}'), 
    # commandes de récupération des liens élèves-profs :
    'eleves': (
        'SELECT DISTINCT groupe_eleve.id_eleve, '
        'groupes.Matiere, tableaux.Periode AS P '
        'FROM groupe_eleve '
        'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
        'JOIN tableaux ON tableaux.id_groupe=groupe_eleve.id_groupe '
        'WHERE tableaux.Public=1 {0}'), 
    # commandes de récupération des bilans communs 
    # (bulletin, referentiel, confidentiel) :
    'bilans': (
        'SELECT DISTINCT evaluations.id_eleve, evaluations.id_bilan, '
        'bilans.id_competence, bilans.Name, bilans.Label, '
        'evaluations.value, groupes.Matiere, '
        '((evaluations.id_tableau - evaluations.id_tableau%1000) / 1000 - 10) AS P '
        'FROM evaluations '
        'JOIN bilans ON bilans.id_bilan=evaluations.id_bilan '
        'JOIN groupes ON (groupes.id_groupe=(evaluations.id_tableau%1000)) '
        'WHERE evaluations.id_item=-1 AND bilans.id_competence>-1 {0}'), 
    # commandes de récupération des bilans persos des profs :
    # le "ORDER BY P DESC" est nécessaire en cas d'évaluation année+période
    # LEFT JOIN pour récupérer les bilans même sans synthèse de groupe
    'persos': (
        'SELECT DISTINCT evaluations.id_eleve, evaluations.id_bilan, '
        'bilans.Name, bilans.Label, evaluations.value, '
        'groupe_bilan.value, groupes.Matiere, groupes.id_groupe, '
        '((evaluations.id_tableau - evaluations.id_tableau%1000) / 1000 - 10) AS P '
        'FROM evaluations '
        'JOIN bilans ON bilans.id_bilan=evaluations.id_bilan '
        'JOIN groupes ON (groupes.id_groupe=(evaluations.id_tableau%1000)) '
        'LEFT JOIN groupe_bilan ON (groupe_bilan.id_groupe=groupes.id_groupe '
        'AND groupe_bilan.id_bilan=evaluations.id_bilan '
        'AND groupe_bilan.Periode=P) '
        'WHERE evaluations.id_item=-1 '
        'AND bilans.id_competence>-2 '
        'AND bilans.id_competence<{0} {1} '
        'ORDER BY P DESC'), 
    # commandes de récupération des appreciations :
    'appreciations': (
        'SELECT DISTINCT *, Periode AS P '
        'FROM appreciations '
        'WHERE 1=1 {0}'), 
    # commandes de récupération des détails 
    # (si utils.ADMIN_WITH_DETAILS) :
    'detailsLinks': (
        'SELECT DISTINCT item_bilan.id_item, item_bilan.id_bilan, '
        'bilans.id_competence, '
        'items.Label '
        'FROM item_bilan '
        'JOIN bilans ON bilans.id_bilan=item_bilan.id_bilan '
        'JOIN items ON items.id_item=item_bilan.id_item '
        'WHERE bilans.id_competence>=0'), 
    'detailsValues': (
        'SELECT DISTINCT evaluations.*, tableaux.Periode AS P '
        'FROM evaluations '
        'JOIN tableaux ON tableaux.id_tableau=evaluations.id_tableau '
        'WHERE tableaux.Public=1 AND evaluations.id_bilan=-1 {0}'), 
    # commandes de récupération des liens élèves-groupes :
    'groupes': (
        'SELECT DISTINCT groupe_eleve.id_eleve, groupe_eleve.id_groupe, '
        'groupes.Name, groupes.Matiere '
        'FROM groupe_eleve '
        'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
        'WHERE groupe_eleve.ordre>0'), 
    # commandes de récupération des notes :
    'notes': (
        'SELECT DISTINCT notes_values.id_eleve, groupes.Matiere, '
        'notes_values.Periode AS P , notes_values.value '
        'FROM notes_values '
        'JOIN groupes ON groupes.id_groupe=notes_values.id_groupe '
        'WHERE id_note=-1 AND id_eleve>-1 {0}'), 
    # commandes de récupération des absences :
    'absences': (
        'SELECT DISTINCT id_eleve, Periode AS P, '
        'abs0, abs1, abs2, abs3 '
        'FROM absences '
        'WHERE 1=1 {0}'), 
    # commandes de récupération de la table lsu :
    'lsu': (
        'SELECT * FROM lsu '
        'WHERE lsuWhat IN '
        '("epis", "aps", "parcours", "positionnement", "accompagnement", "cpt-num")'), 
    }


def doProf(main, recupLevel, profxx, idsEleves, endForPeriodes):
    """
    """

    def mustDoEleve(id_eleve, testLockedClasses=False):
        if testLockedClasses:
            test1 = (-1 in idsEleves)
            test2 = (id_eleve not in STUDENTS_TO_EXCLUDE)
            test3 = (id_eleve in idsEleves)
            result =  ((test1 and test2) or test3)
        else:
            test1 = (-1 in idsEleves)
            test2 = (id_eleve in idsEleves)
            result = (test1 or test2)
        return result

    # création des listes qui seront retournées :
    profLists = {
        'filesProfsConfig': [], 
        'eleveProf': [], 
        'bilans': [], 
        'persos': [], 
        'appreciations': [], 
        'detailsLinks': [], 
        'detailsValues': [], 
        'eleveGroupe': [], 
        'notes': [], 
        'absences': [], 
        'dnb': [], 
        'lsu': [], 
        }

    # récupération des listes de la procédure recupProfxx :
    modifs = {
        'newProfxxFiles': [], 
        'pbProfxxFiles': [], 
        'tooOldProfxxFiles': [], 
        'elevesModifs': [], 
        }

    # messages à afficher à la fin :
    messages = []
    db_profxx = None
    try:
        for aProf in admin.PROFS:
            if profxx == aProf[1] + '.sqlite':
                id_prof = aProf[0]
                name_prof = aProf[2] + ' ' + aProf[3]
        messages.append(name_prof)
        (db_profxx, dbName) = utils_db.createConnection(
            main, admin.dirLocalProfxx + profxx)
        query_profxx = utils_db.query(db_profxx)
        query_recupEvals = utils_db.query(admin.db_recupEvals)

        year = utils_db.readInConfigTable(db_profxx, 'annee_scolaire')[0]
        badYear = False
        if year < main.annee_scolaire[0]:
            badYear = True
            messages.append('TOO OLD YEAR')
        elif year > main.annee_scolaire[0]:
            badYear = True
            messages.append('TOO NEW YEAR')
        if badYear:
            return

        fileDateTimeNew = utils_db.readInConfigTable(db_profxx, 'datetime')[0]
        forAffichage = utils_functions.u(fileDateTimeNew)
        forAffichage = QtCore.QDateTime().fromString(forAffichage, 'yyyyMMddhhmm')
        forAffichage = forAffichage.toString('dd-MM-yyyy  hh:mm')
        messages.append(forAffichage + '   (fileDateTimeNew)')
        fileDateTime = 0
        commandLine = 'SELECT datetime FROM files_prof_config WHERE id_prof={0}'.format(id_prof)
        query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
        while query_recupEvals.next():
            fileDateTime = query_recupEvals.value(0)
        forAffichage = utils_functions.u(fileDateTime)
        forAffichage = QtCore.QDateTime().fromString(forAffichage, 'yyyyMMddhhmm')
        forAffichage = forAffichage.toString('dd-MM-yyyy  hh:mm')
        messages.append(forAffichage + '   (fileDateTime)')
        if fileDateTimeNew > fileDateTime:
            mustRecup = True
        elif fileDateTimeNew == 0:
            mustRecup = False
            modifs['pbProfxxFiles'].append(profxx)
            messages.append('PROBLEME')
        else:
            mustRecup = False
            messages.append('INUTILE')

        if mustRecup or (recupLevel > utils.RECUP_LEVEL_SIMPLE):
            profLists['filesProfsConfig'].append((id_prof, fileDateTimeNew))
            versionDB_profxx = utils_db.readVersionDB(db_profxx)
            # on force la mise à jour de la base prof :
            if versionDB_profxx < utils.VERSIONDB_PROF:
                utils_functions.myPrint(id_prof, ' versionDB : ', versionDB_profxx)
                modifs['tooOldProfxxFiles'].append(profxx)
                # on referme la base prof :
                query_profxx.clear()
                del query_profxx
                db_profxx.close()
                del db_profxx
                utils_db.sqlDatabase.removeDatabase(dbName)
                db_profxx = None
                # on lance le upgrade :
                import utils_upgrades
                profxxNoExt = profxx.split('.')[0]
                try:
                    utils_upgrades.upgradeProf(
                        main, admin.dirLocalProfxx + profxx, profxxNoExt)
                except:
                    modifs['pbProfxxFiles'].append(profxx)
                # on rouvre la base :
                (db_profxx, dbName) = utils_db.createConnection(
                    main, admin.dirLocalProfxx + profxx)
                query_profxx = utils_db.query(db_profxx)
            messages.append('RECUP : ' + profxx)
            modifs['newProfxxFiles'].append(profxx)

            # **************************************************
            # récupération des liens élèves-profs.
            # Chaque ligne contient :
            #     (id_prof, id_eleve, matiereCode, periode)
            # **************************************************
            # on récupère les anciens enregistrements :
            listEx = []
            query_recupEvals = utils_db.queryExecute(
                CMD_PROF['commandLineExProf'].format('eleve_prof', id_prof, endForPeriodes), 
                query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                if mustDoEleve(id_eleve):
                    matiereCode = query_recupEvals.value(2)
                    periode = int(query_recupEvals.value(3))
                    data = (id_prof, id_eleve, matiereCode, periode)
                    listEx.append(data)
            # on récupère les nouveaux enregistrements :
            listNew = []
            query_profxx = utils_db.queryExecute(
                CMD_PROF['eleves'].format(endForPeriodes), query=query_profxx)
            while query_profxx.next():
                id_eleve = int(query_profxx.value(0))
                if mustDoEleve(id_eleve):
                    matiereName = query_profxx.value(1)
                    matiereCode = admin.MATIERES['Matiere2Code'].get(matiereName, matiereName)
                    periode = int(query_profxx.value(2))
                    data = (id_prof, id_eleve, matiereCode, periode)
                    listNew.append(data)
            # on cherche ce qu'il faut effacer :
            for data in listEx:
                if not(data in listNew):
                    modifs['elevesModifs'].append(data[1])
            # on cherche ce qu'il faut ajouter :
            for data in listNew:
                if not(data in listEx):
                    modifs['elevesModifs'].append(data[1])
            # et on ajoute à la liste totale :
            profLists['eleveProf'].extend(listNew)

            # ****************************************************
            # récupération des bilans communs.
            # Chaque ligne contient :
            #     (id_prof, id_eleve, id_bilan, id_competence, 
            #     bilanName, bilanLabel, value, matiereCode, periode)
            # ****************************************************
            # on récupère les anciens enregistrements :
            listLocked = []
            listEx = []
            query_recupEvals = utils_db.queryExecute(
                CMD_PROF['commandLineExProf'].format('bilans', id_prof, endForPeriodes), 
                query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                id_bilan = int(query_recupEvals.value(2))
                id_competence = int(query_recupEvals.value(3))
                bilanName = query_recupEvals.value(4)
                bilanLabel = query_recupEvals.value(5)
                value = query_recupEvals.value(6)
                matiereCode = query_recupEvals.value(7)
                periode = int(query_recupEvals.value(8))
                data = (id_prof, id_eleve, id_bilan, id_competence, 
                    bilanName, bilanLabel, value, matiereCode, periode)
                # on laisse passer les évaluations des PP :
                if mustDoEleve(id_eleve, testLockedClasses=True) or (matiereCode == 'PP'):
                    listEx.append(data)
                else:
                    listLocked.append(data)
            # on récupère les nouveaux enregistrements :
            # on passe par un dictionnaire car il peut y avoir plusieurs tableaux
            dicoNew = {}
            query_profxx = utils_db.queryExecute(
                CMD_PROF['bilans'].format(endForPeriodes), query=query_profxx)
            while query_profxx.next():
                id_eleve = int(query_profxx.value(0))
                matiereName = query_profxx.value(6)
                matiereCode = admin.MATIERES['Matiere2Code'].get(matiereName, matiereName)
                # on laisse passer les évaluations des PP :
                if mustDoEleve(id_eleve, testLockedClasses=True) or (matiereCode == 'PP'):
                    id_bilan = int(query_profxx.value(1))
                    id_competence = int(query_profxx.value(2))
                    bilanName = query_profxx.value(3)
                    bilanLabel = query_profxx.value(4)
                    value = query_profxx.value(5)
                    periode = int(query_profxx.value(7))
                    dicoData = (id_prof, id_eleve, id_bilan, id_competence, 
                        bilanName, bilanLabel, matiereCode, periode)
                    try:
                        dicoNew[dicoData] = dicoNew[dicoData] + value
                    except:
                        dicoNew[dicoData] = value
            # on remplit listNew à partir de dicoNew :
            listNew = []
            for (id_prof, id_eleve, id_bilan, id_competence, bilanName, bilanLabel, \
                matiereCode, periode) in dicoNew:
                dicoData = (
                    id_prof, id_eleve, 
                    id_bilan, id_competence, bilanName, bilanLabel, 
                    matiereCode, periode)
                value = dicoNew[dicoData]
                value = utils_calculs.moyenneBilan(value)
                data = (
                    id_prof, id_eleve, 
                    id_bilan, id_competence, bilanName, bilanLabel, value, 
                    matiereCode, periode)
                listNew.append(data)
            # on cherche ce qu'il faut effacer :
            for data in listEx:
                if not(data in listNew):
                    modifs['elevesModifs'].append(data[1])
            # on cherche ce qu'il faut ajouter :
            for data in listNew:
                if not(data in listEx):
                    modifs['elevesModifs'].append(data[1])
            # et on ajoute à la liste totale :
            profLists['bilans'].extend(listLocked)
            profLists['bilans'].extend(listNew)

            # **********************************************************
            # récupération des bilans persos des profs pour le bulletin.
            # Chaque ligne contient :
            #     (id_prof, id_eleve, 
            #      id_bilan, bilanName, bilanLabel, value, 
            #      groupeValue, matiereCode, ordre, periode)
            # **********************************************************
            # on récupère les anciens enregistrements :
            listLocked = []
            listEx = []
            query_recupEvals = utils_db.queryExecute(
                CMD_PROF['commandLineExProf'].format('persos', id_prof, endForPeriodes), 
                query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                id_bilan = int(query_recupEvals.value(2))
                bilanName = query_recupEvals.value(3)
                bilanLabel = query_recupEvals.value(4)
                value = query_recupEvals.value(5)
                groupeValue = query_recupEvals.value(6)
                matiereCode = query_recupEvals.value(7)
                ordre = int(query_recupEvals.value(8))
                periode = int(query_recupEvals.value(9))
                data = (
                    id_prof, id_eleve, id_bilan, 
                    bilanName, bilanLabel, value, groupeValue, 
                    matiereCode, ordre, periode)
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    listEx.append(data)
                else:
                    listLocked.append(data)
            # on récupère les nouveaux enregistrements :
            # on passe par un dictionnaire car il peut y avoir plusieurs tableaux
            dicoNew = {}
            # une liste pour gérer les bilans déclarés dans année et période
            # (ne pas les compter 2 fois ; on met aussi la matière car des profs ayant 2 matières
            #  peuvent sélectionner les mêmes bilans persos)
            dejaLa = []
            # récupération des profils liés aux différents bilans :
            bilansProfils = {}
            commandLine = utils_db.q_selectAllFrom.format('profil_bilan_BLT')
            query_profxx = utils_db.queryExecute(commandLine, query=query_profxx)
            while query_profxx.next():
                id_profil = int(query_profxx.value(0))
                id_bilan = int(query_profxx.value(1))
                ordre = int(query_profxx.value(2))
                if not(id_bilan in bilansProfils):
                    bilansProfils[id_bilan] = {}
                bilansProfils[id_bilan][id_profil] = ordre
            # récupération des profils des élèves et des groupes.
            # Le ORDER permet de récupérer les profils par défaut 
            # et standards en premier.
            profils = {}
            commandLine = (
                'SELECT DISTINCT profils.*, profil_who.* '
                'FROM profils '
                'LEFT JOIN profil_who ON profil_who.id_profil=profils.id_profil '
                'ORDER BY profils.Matiere, profil_who.id_who, profils.id_profil')
            query_profxx = utils_db.queryExecute(commandLine, query=query_profxx)
            while query_profxx.next():
                # on récupère la matière en premier :
                matiereName = query_profxx.value(2)
                matiereCode = admin.MATIERES['Matiere2Code'].get(matiereName, matiereName)
                if not(matiereCode in profils):
                    profils[matiereCode] = {
                        'STUDENTS': {}, 'GROUPS': {}, 'DEFAULT': -999}
                # puis le reste :
                id_profil = int(query_profxx.value(0))
                profilType = query_profxx.value(3)
                # on répartit les données :
                if id_profil < 0:
                    # profil par défaut de la matière :
                    profils[matiereCode]['DEFAULT'] = id_profil
                elif profilType == 'STUDENTS':
                    # profil attribué à des élèves.
                    # try-except est en cas de profil non attribué :
                    try:
                        id_eleve = int(query_profxx.value(4))
                        period = int(query_profxx.value(6))
                        if not(id_eleve in profils[matiereCode]['STUDENTS']):
                            profils[matiereCode]['STUDENTS'][id_eleve] = {}
                        if not(period in profils[matiereCode]['STUDENTS'][id_eleve]):
                            profils[matiereCode]['STUDENTS'][id_eleve][period] = id_profil
                    except:
                        pass
                elif profilType == 'GROUPS':
                    # profil standard de groupes.
                    # try-except est en cas de profil non attribué :
                    try:
                        id_groupe = int(query_profxx.value(4))
                        period = int(query_profxx.value(6))
                        if not(id_groupe in profils[matiereCode]['GROUPS']):
                            profils[matiereCode]['GROUPS'][id_groupe] = {}
                        if not(period in profils[matiereCode]['GROUPS'][id_groupe]):
                            profils[matiereCode]['GROUPS'][id_groupe][period] = id_profil
                    except:
                        pass
            # et un autre dico pour les ordres attribués pour l'affichage :
            elevesOrdres = {}
            # on y va :
            query_profxx = utils_db.queryExecute(
                CMD_PROF['persos'].format(utils.decalageBLT, endForPeriodes), 
                query=query_profxx)
            while query_profxx.next():
                id_eleve = int(query_profxx.value(0))
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    id_bilan = int(query_profxx.value(1))
                    bilanName = query_profxx.value(2)
                    bilanLabel = query_profxx.value(3)
                    value = query_profxx.value(4)
                    groupeValue = query_profxx.value(5)
                    # on garde la matière telle que dans la base prof pour l'instant :
                    matiereName = query_profxx.value(6)
                    matiereCode = admin.MATIERES['Matiere2Code'].get(matiereName, matiereName)
                    if not(matiereCode in profils):
                        profils[matiereCode] = {
                            'STUDENTS': {}, 'GROUPS': {}, 'DEFAULT': -999}
                    id_groupe = int(query_profxx.value(7))
                    periode = int(query_profxx.value(8))
                    if not(id_eleve in elevesOrdres):
                        elevesOrdres[id_eleve] = []
                    if not(id_bilan in bilansProfils):
                        bilansProfils[id_bilan] = {}
                    # récupération des profils disponibles pour cet élève :
                    periodeToTest = periode
                    if periode == 0:
                        periodeToTest = utils.selectedPeriod
                    eleveProfils = {periodeToTest: []}
                    if periodeToTest > 0:
                        eleveProfils[0] = []
                    if id_eleve in profils[matiereCode]['STUDENTS']:
                        # l'élève a au moins un profil pour une des périodes :
                        for p in eleveProfils:
                            if p in profils[matiereCode]['STUDENTS'][id_eleve]:
                                id_profil = profils[matiereCode]['STUDENTS'][id_eleve][p]
                                utils_functions.appendUnique(eleveProfils[p], id_profil)
                    if id_groupe in profils[matiereCode]['GROUPS']:
                        # le groupe a au moins un profil standard pour une des périodes :
                        for p in eleveProfils:
                            if p in profils[matiereCode]['GROUPS'][id_groupe]:
                                id_profil = profils[matiereCode]['GROUPS'][id_groupe][p]
                                # on n'ajoute le profil du groupe
                                # que si l'élève n'en a pas déjà un :
                                if len(eleveProfils[p]) < 1:
                                    eleveProfils[p].append(id_profil)
                    # si aucun profil n'a été trouvé,
                    # on met le profil par défaut :
                    test = 0
                    for p in eleveProfils:
                        test += len(eleveProfils[p])
                    if test < 1:
                        id_profil = profils[matiereCode]['DEFAULT']
                        eleveProfils[0].append(id_profil)
                    # puis récupérer l'ordre d'affichage dans le bulletin :
                    ordre = -1
                    for p in eleveProfils:
                        for id_profil in eleveProfils[p]:
                            if id_profil in bilansProfils[id_bilan]:
                                ordre = bilansProfils[id_bilan][id_profil]
                                # la période inscrite sera celle du profil 
                                # (cas du prof ayant des tableaux "année" mais voulant
                                # des profils différents selon les périodes) :
                                if periode == 0:
                                    periode = p
                    if ordre > -1:
                        if (id_eleve, id_bilan, matiereCode) in dejaLa:
                            # on repasse l'ordre à -1 pour ne pas écrire 2 fois le bilan
                            ordre = -1
                        else:
                            dejaLa.append((id_eleve, id_bilan, matiereCode))
                    # si l'ordre est toujours -1, c'est que ce bilan n'est pas dans le bulletin :
                    if ordre > -1:
                        # pour bien avoir des ordres différents
                        # (cas de 2 groupes de la même matière) :
                        while ordre in elevesOrdres[id_eleve]:
                            ordre += 1
                        elevesOrdres[id_eleve].append(ordre)
                        dicoData = (
                            id_prof, id_eleve, id_bilan, bilanName, bilanLabel, 
                            matiereCode, ordre, periode)
                        try:
                            dicoNew[dicoData] = [
                                dicoNew[dicoData][0] + value, 
                                dicoNew[dicoData][1] + groupeValue]
                        except:
                            dicoNew[dicoData] = [value, groupeValue]
            # on remplit listNew à partir de dicoNew :
            listNew = []
            for (id_prof, id_eleve, id_bilan, bilanName, bilanLabel, \
                matiereCode, ordre, periode) in dicoNew:
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    dicoData = (
                        id_prof, id_eleve, id_bilan, bilanName, bilanLabel, 
                        matiereCode, ordre, periode)
                    [value, groupeValue] = dicoNew[dicoData]
                    value = utils_calculs.moyenneBilan(value)
                    groupeValue = utils_calculs.moyenneBilan(groupeValue)
                    data = (
                        id_prof, id_eleve, id_bilan, bilanName, bilanLabel, 
                        value, groupeValue, 
                        matiereCode, ordre, periode)
                    listNew.append(data)
            # on cherche ce qu'il faut effacer :
            for data in listEx:
                id_eleve = data[1]
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    if not(data in listNew):
                        modifs['elevesModifs'].append(id_eleve)
            # on cherche ce qu'il faut ajouter :
            for data in listNew:
                id_eleve = data[1]
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    if not(data in listEx):
                        modifs['elevesModifs'].append(id_eleve)
            # et on ajoute à la liste totale :
            profLists['persos'].extend(listLocked)
            profLists['persos'].extend(listNew)

            # ********************************************************
            # récupération des appréciations.
            # Chaque ligne contient :
            #     (id_prof, id_eleve, appreciation, matiereCode, periode)
            # ********************************************************
            # on récupère les anciens enregistrements :
            listEx = []
            query_recupEvals = utils_db.queryExecute(
                CMD_PROF['commandLineExProf'].format('appreciations', id_prof, endForPeriodes), 
                query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                if mustDoEleve(id_eleve):
                    appreciation = query_recupEvals.value(2)
                    matiereCode = query_recupEvals.value(3)
                    periode = int(query_recupEvals.value(4))
                    data = (id_prof, id_eleve, appreciation, matiereCode, periode)
                    listEx.append(data)
            # on récupère les nouveaux enregistrements :
            listNew = []
            query_profxx = utils_db.queryExecute(
                CMD_PROF['appreciations'].format(endForPeriodes), 
                query=query_profxx)
            while query_profxx.next():
                id_eleve = int(query_profxx.value(0))
                if mustDoEleve(id_eleve):
                    appreciation = query_profxx.value(1)
                    matiereName = query_profxx.value(2)
                    matiereCode = admin.MATIERES['Matiere2Code'].get(matiereName, matiereName)
                    periode = int(query_profxx.value(3))
                    data = (id_prof, id_eleve, appreciation, matiereCode, periode)
                    listNew.append(data)
            # on cherche ce qu'il faut effacer :
            for data in listEx:
                if not(data in listNew):
                    modifs['elevesModifs'].append(data[1])
            # on cherche ce qu'il faut ajouter :
            for data in listNew:
                if not(data in listEx):
                    modifs['elevesModifs'].append(data[1])
            # et on ajoute à la liste totale :
            profLists['appreciations'].extend(listNew)

            # **************************************************
            # récupération des notes.
            # Chaque ligne contient :
            #     (id_prof, id_eleve, matiereCode, periode, value)
            # **************************************************
            # on récupère les anciens enregistrements :
            listLocked = []
            listEx = []
            query_recupEvals = utils_db.queryExecute(
                CMD_PROF['commandLineExProf'].format('notes', id_prof, endForPeriodes), 
                query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                matiereCode = query_recupEvals.value(2)
                periode = int(query_recupEvals.value(3))
                # on ne transforme pas en float maintenant :
                value = query_recupEvals.value(4)
                data = (id_prof, id_eleve, matiereCode, periode, value)
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    listEx.append(data)
                else:
                    listLocked.append(data)
            # on récupère les nouveaux enregistrements :
            listNew = []
            query_profxx = utils_db.queryExecute(
                CMD_PROF['notes'].format(endForPeriodes), query=query_profxx)
            while query_profxx.next():
                id_eleve = int(query_profxx.value(0))
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    if id_eleve in admin.elevesNotes:
                        matiereName = query_profxx.value(1)
                        matiereCode = admin.MATIERES['Matiere2Code'].get(matiereName, matiereName)
                        periode = int(query_profxx.value(2))
                        # on ne transforme pas en float maintenant :
                        value = query_profxx.value(3)
                        data = (id_prof, id_eleve, matiereCode, periode, value)
                        listNew.append(data)
            # on cherche ce qu'il faut effacer :
            for data in listEx:
                if not(data in listNew):
                    modifs['elevesModifs'].append(data[1])
            # on cherche ce qu'il faut ajouter :
            for data in listNew:
                if not(data in listEx):
                    modifs['elevesModifs'].append(data[1])
            # et on ajoute à la liste totale :
            profLists['notes'].extend(listLocked)
            profLists['notes'].extend(listNew)

            # ************************************************************
            # récupération des absences.
            # Chaque ligne contient :
            #     (id_prof, id_eleve, periode, abs0, abs1, abs2, abs3)
            # ************************************************************
            # on récupère les anciens enregistrements :
            listLocked = []
            listEx = []
            query_recupEvals = utils_db.queryExecute(
                CMD_PROF['commandLineExProf'].format('absences', id_prof, endForPeriodes), 
                query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                periode = int(query_recupEvals.value(2))
                abs0 = int(query_recupEvals.value(3))
                abs1 = int(query_recupEvals.value(4))
                abs2 = int(query_recupEvals.value(5))
                abs3 = int(query_recupEvals.value(6))
                data = (id_prof, id_eleve, periode, abs0, abs1, abs2, abs3)
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    listEx.append(data)
                else:
                    listLocked.append(data)
            # on récupère les nouveaux enregistrements :
            listNew = []
            query_profxx = utils_db.queryExecute(
                CMD_PROF['absences'].format(endForPeriodes), 
                query=query_profxx)
            while query_profxx.next():
                id_eleve = int(query_profxx.value(0))
                if mustDoEleve(id_eleve, testLockedClasses=True):
                    periode = int(query_profxx.value(1))
                    abs0 = int(query_profxx.value(2))
                    abs1 = int(query_profxx.value(3))
                    abs2 = int(query_profxx.value(4))
                    abs3 = int(query_profxx.value(5))
                    data = (id_prof, id_eleve, periode, abs0, abs1, abs2, abs3)
                    listNew.append(data)
            # on cherche ce qu'il faut effacer :
            for data in listEx:
                if not(data in listNew):
                    modifs['elevesModifs'].append(data[1])
            # on cherche ce qu'il faut ajouter :
            for data in listNew:
                if not(data in listEx):
                    modifs['elevesModifs'].append(data[1])
            # et on ajoute à la liste totale :
            profLists['absences'].extend(listLocked)
            profLists['absences'].extend(listNew)

            # ***********************************************
            # récupération des avis pour le DNB 
            # (si c'est un chef d'établissement).
            # Chaque ligne contient :
            #     (id_eleve, id_dnb, value)
            # ***********************************************
            if id_prof >= utils.decalageChefEtab:
                # on récupère les anciens enregistrements :
                listEx = []
                commandLineDNB_profxx = utils_db.q_selectAllFromWhere.format(
                    'dnb_values', 'id_prof', id_prof)
                query_recupEvals = utils_db.queryExecute(
                    commandLineDNB_profxx, query=query_recupEvals)
                while query_recupEvals.next():
                    id_eleve = int(query_recupEvals.value(1))
                    if mustDoEleve(id_eleve):
                        id_dnb = int(query_recupEvals.value(2))
                        value = query_recupEvals.value(3)
                        data = (id_prof, id_eleve, id_dnb, value)
                        listEx.append(data)
                # on récupère les nouveaux enregistrements :
                listNew = []
                commandLineDNB_profxx = utils_db.q_selectAllFrom.format(
                    'dnb_values')
                query_profxx = utils_db.queryExecute(
                    commandLineDNB_profxx, query=query_profxx)
                while query_profxx.next():
                    id_eleve = int(query_profxx.value(0))
                    if mustDoEleve(id_eleve):
                        id_dnb = int(query_profxx.value(1))
                        value = query_profxx.value(2)
                        data = (id_prof, id_eleve, id_dnb, value)
                        listNew.append(data)
                # on cherche ce qu'il faut effacer :
                for data in listEx:
                    if not(data in listNew):
                        modifs['elevesModifs'].append(data[1])
                # on cherche ce qu'il faut ajouter :
                for data in listNew:
                    if not(data in listEx):
                        modifs['elevesModifs'].append(data[1])
                # et on ajoute à la liste totale :
                profLists['dnb'].extend(listNew)

            # ********************************************************
            # récupération de la table lsu.
            # Chaque ligne contient :
            #     (id_prof, lsuWhat, lsu1, lsu2, ..., lsu8)
            # ********************************************************
            # on récupère les nouveaux enregistrements :
            listNew = []
            query_profxx = utils_db.queryExecute(
                CMD_PROF['lsu'], query=query_profxx)
            while query_profxx.next():
                data = (
                    id_prof, query_profxx.value(0), 
                    query_profxx.value(1), query_profxx.value(2), 
                    query_profxx.value(3), query_profxx.value(4), 
                    query_profxx.value(5), query_profxx.value(6), 
                    query_profxx.value(7), query_profxx.value(8))
                listNew.append(data)
            # on ajoute à la liste totale :
            profLists['lsu'].extend(listNew)


            # ***********************************************
            # Mise à jour des liens élèves-profs
            # pour prendre en compte les matières où il y 
            # aurait des notes ou des appréciations, 
            # mais pas d'évaluation (!)
            # ***********************************************
            for (id_prof, id_eleve, appreciation, matiereCode, periode) in profLists['appreciations']:
                data = (id_prof, id_eleve, matiereCode, periode)
                if not(data in profLists['eleveProf']):
                    profLists['eleveProf'].append(data)
            for (id_prof, id_eleve, matiereCode, periode, value) in profLists['notes']:
                data = (id_prof, id_eleve, matiereCode, periode)
                if not(data in profLists['eleveProf']):
                    profLists['eleveProf'].append(data)

            # récupération des détails:
            if utils.ADMIN_WITH_DETAILS == 1:
                # **********************************************************
                # Chaque ligne de profLists['detailsLinks'] contient :
                #     (id_prof, id_item, id_bilan, id_competence, itemLabel)
                # **********************************************************
                # on récupère les anciens enregistrements :
                listEx = []
                query_recupEvals = utils_db.queryExecute(
                    CMD_PROF['commandLineExProf'].format('details_links', id_prof, endForPeriodes), 
                    query=query_recupEvals)
                while query_recupEvals.next():
                    id_item = int(query_recupEvals.value(1))
                    id_bilan = int(query_recupEvals.value(2))
                    id_competence = int(query_recupEvals.value(3))
                    itemLabel = query_recupEvals.value(4)
                    data = (id_prof, id_item, id_bilan, id_competence, itemLabel)
                    listEx.append(data)
                # on récupère les nouveaux enregistrements :
                listNew = []
                query_profxx = utils_db.queryExecute(
                    CMD_PROF['detailsLinks'], query=query_profxx)
                while query_profxx.next():
                    id_item = int(query_profxx.value(0))
                    id_bilan = int(query_profxx.value(1))
                    id_competence = int(query_profxx.value(2))
                    itemLabel = query_profxx.value(3)
                    data = (id_prof, id_item, id_bilan, id_competence, itemLabel)
                    listNew.append(data)
                # et on ajoute à la liste totale :
                profLists['detailsLinks'].extend(listNew)

                # *************************************************************
                # Chaque ligne de profLists['detailsValues'] contient :
                #     (id_prof, id_tableau, id_eleve, id_item, value, periode)
                # *************************************************************
                # on récupère les anciens enregistrements :
                listEx = []
                query_recupEvals = utils_db.queryExecute(
                    CMD_PROF['commandLineExProf'].format('details_values', id_prof, endForPeriodes), 
                    query=query_recupEvals)
                while query_recupEvals.next():
                    id_tableau = int(query_recupEvals.value(1))
                    id_eleve = int(query_recupEvals.value(2))
                    if mustDoEleve(id_eleve):
                        id_item = int(query_recupEvals.value(3))
                        value = query_recupEvals.value(4)
                        periode = int(query_recupEvals.value(5))
                        data = (id_prof, id_tableau, id_eleve, id_item, value, periode)
                        listEx.append(data)
                # on récupère les nouveaux enregistrements :
                listNew = []
                query_profxx = utils_db.queryExecute(
                    CMD_PROF['detailsValues'].format(endForPeriodes), 
                    query=query_profxx)
                while query_profxx.next():
                    id_tableau = int(query_profxx.value(0))
                    id_eleve = int(query_profxx.value(1))
                    if mustDoEleve(id_eleve):
                        id_item = int(query_profxx.value(2))
                        value = query_profxx.value(4)
                        periode = int(query_profxx.value(5))
                        data = (id_prof, id_tableau, id_eleve, id_item, value, periode)
                        listNew.append(data)
                # et on ajoute à la liste totale :
                profLists['detailsValues'].extend(listNew)

            # ******************************************************
            # récupération des liens élèves-groupes.
            # Chaque ligne contient :
            #     (id_prof, id_eleve, id_groupe, matiereCode, periode)
            # ******************************************************
            # on récupère les anciens enregistrements :
            listEx = []
            query_recupEvals = utils_db.queryExecute(
                CMD_PROF['commandLineExProf'].format('eleve_groupe', id_prof, endForPeriodes), 
                query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                if mustDoEleve(id_eleve):
                    id_groupe = int(query_recupEvals.value(2))
                    groupeName = query_recupEvals.value(3)
                    matiereCode = query_recupEvals.value(4)
                    periode = int(query_recupEvals.value(5))
                    data = (id_prof, id_eleve, id_groupe, groupeName, matiereCode, periode)
                    listEx.append(data)
            # on récupère les nouveaux enregistrements :
            listNew = []
            query_profxx = utils_db.queryExecute(
                CMD_PROF['groupes'], query=query_profxx)
            while query_profxx.next():
                id_eleve = int(query_profxx.value(0))
                if mustDoEleve(id_eleve):
                    id_groupe = int(query_profxx.value(1))
                    groupeName = query_profxx.value(2)
                    matiereName = query_profxx.value(3)
                    matiereCode = admin.MATIERES['Matiere2Code'].get(matiereName, matiereName)
                    periode = utils.selectedPeriod
                    # on inscrit pour la période actuelle et pour le bilan annuel (999) :
                    data = (id_prof, id_eleve, id_groupe, groupeName, matiereCode, periode)
                    listNew.append(data)
                    data = (id_prof, id_eleve, id_groupe, groupeName, matiereCode, 999)
                    listNew.append(data)
            # on cherche ce qu'il faut effacer :
            for data in listEx:
                if not(data in listNew):
                    modifs['elevesModifs'].append(data[1])
            # on cherche ce qu'il faut ajouter :
            for data in listNew:
                if not(data in listEx):
                    modifs['elevesModifs'].append(data[1])
            # et on ajoute à la liste totale :
            profLists['eleveGroupe'].extend(listNew)

            messages.append('RECUP OK')
    except:
        messages.append(utils_functions.u('except: {0}').format(profxx))
        modifs['pbProfxxFiles'].append(profxx)
        pass
    finally:
        if db_profxx != None:
            # on referme la base prof :
            query_profxx.clear()
            del query_profxx
            db_profxx.close()
            del db_profxx
            utils_db.sqlDatabase.removeDatabase(dbName)
        return profLists, modifs, messages

def createTablesInRecupDB(main, calcLists, mustClear=False, idsEleves=[-1]):
    """
    Mise à jours de la base recup_evals 
    et de la table referential_propositions.bilans_details.
    """
    message = QtWidgets.QApplication.translate(
        'main', 'Updating the recup_evals database.')
    utils_functions.afficheMessage(main, message, editLog=True, p=True)
    try:
        # ouverture des bases et transactions :
        admin.openDB(main, 'recup_evals')
        query_recupEvals, transactionOK_recupEvals = utils_db.queryTransactionDB(
            admin.db_recupEvals)
        admin.openDB(main, 'referential_propositions')
        query_propositions, transactionOK_propositions = utils_db.queryTransactionDB(
            admin.db_referentialPropositions)

        # inscription des profs dans la table recup_evals.files_prof_config :
        if mustClear:
            commandLine = utils_db.qdf_table.format('files_prof_config')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            commandLine = utils_db.qct_recupEvals_filesprofconfig
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
        # on a besoin d'un dictionnaire pour la table 
        # referential_propositions.bilans_details :
        dateProfDic = {}
        for ligne in calcLists['filesProfsConfig']:
            id_prof = ligne[0]
            datetime = ligne[1]
            commandLine = utils_db.qdf_where.format(
                'files_prof_config', 'id_prof', id_prof)
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            # pour referential_propositions.bilans_details :
            forDic = utils_functions.u(datetime)
            forDic = QtCore.QDateTime().fromString(forDic, 'yyyyMMddhhmm')
            forDic = forDic.toString('dd/MM/yyyy')
            for prof in admin.PROFS:
                if prof[0] == id_prof:
                    profNom = utils_functions.u('{0} {1}').format(prof[2], prof[3])
            dateProfDic[id_prof] = (forDic, profNom)
        commandLine = utils_db.insertInto('files_prof_config', 2)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['filesProfsConfig']}, query=query_recupEvals)

        # fin des lignes de commandes pour sélectionner les profs concernés :
        endLine_idProfs = ''
        endLine_nomProfs = ''
        if not(mustClear):
            id_profs, noms_profs = '', ''
            for ligne in calcLists['filesProfsConfig']:
                id_prof = ligne[0]
                nom_prof = dateProfDic[id_prof][1]
                if id_profs == '':
                    id_profs = '{0}'.format(id_prof)
                else:
                    id_profs = '{0}, {1}'.format(id_profs, id_prof)
                if noms_profs == '':
                    noms_profs = utils_functions.u('"{0}"').format(nom_prof)
                else:
                    noms_profs = utils_functions.u('{0}, "{1}"').format(
                        noms_profs, nom_prof)
            endLine_idProfs = 'id_prof IN ({0})'.format(id_profs)
            endLine_nomProfs = utils_functions.u(
                'nom_prof IN ({0})').format(noms_profs)

        # fin des lignes de commandes pour sélectionner les élèves concernés :
        endLine_idEleves = ''
        if not(mustClear):
            if not(-1 in idsEleves):
                id_eleves = utils_functions.array2string(idsEleves)
                endLine_idEleves = ' AND id_eleve IN ({0})'.format(id_eleves)

        # préparation des lignes de commande de suppression :
        periodsForIn = [0, 999]
        maxi = 1
        if utils.selectedPeriod == 0:
            maxi = utils.NB_PERIODES
        else:
            maxi = utils.selectedPeriod + 1
        for i in range(1, maxi):
            if not(utils.PROTECTED_PERIODS[i]):
                periodsForIn.append(i)
        forIn = utils_functions.array2string(periodsForIn)
        command_periode = ' WHERE Periode IN ({0})'.format(forIn)
        command_del = '{0}{1}'.format('DELETE FROM {0}', command_periode)
        commandLine_eleveProf = command_del.format('eleve_prof')
        commandLine_bilans = command_del.format('bilans')
        commandLine_persos = command_del.format('persos')
        commandLine_appreciations = command_del.format('appreciations')
        commandLine_notes = command_del.format('notes')
        commandLine_absences = command_del.format('absences')
        commandLine_eleveGroupe = command_del.format('eleve_groupe')
        for command in (commandLine_eleveProf, 
                        commandLine_bilans, 
                        commandLine_persos, 
                        commandLine_appreciations, 
                        commandLine_notes, 
                        commandLine_absences, 
                        commandLine_eleveGroupe):
            commandLine = '{0} AND {1}{2}'.format(
                command, endLine_idProfs, endLine_idEleves)
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
        # les avis DNB ne dépendent pas de la période :
        commandLine_dnb = 'DELETE FROM dnb_values WHERE {0}{1}'.format(
            endLine_idProfs, endLine_idEleves)
        query_recupEvals = utils_db.queryExecute(
            commandLine_dnb, query=query_recupEvals)
        # les epis, aps etc ne dépendent pas de la période :
        commandLine_lsu = (
            'DELETE FROM lsu '
            'WHERE lsuWhat IN '
            '("epis", "aps", "parcours", "positionnement", "accompagnement", "cpt-num") '
            'AND {0}').format(
                endLine_idProfs)
        query_recupEvals = utils_db.queryExecute(
            commandLine_lsu, query=query_recupEvals)

        # mise à jour de la table recup_evals.eleve_prof :
        commandLine = utils_db.insertInto('eleve_prof', 4)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['eleveProf']}, query=query_recupEvals)

        # mise à jour de la table recup_evals.bilans :
        commandLine = utils_db.insertInto('bilans', 9)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['bilans']}, query=query_recupEvals)
        # suppression des éventuels doublons dans la table bilans :
        commandLine = (
            'DELETE FROM bilans '
            'WHERE rowid NOT IN ('
            '    SELECT min(rowid) '
            '    FROM bilans '
            '    GROUP BY '
            '        id_prof, id_eleve, Periode, Matiere, id_competence '
            '    )')
        query_recupEvals = utils_db.queryExecute(
            commandLine, query=query_recupEvals)

        # mise à jour de la table recup_evals.persos :
        commandLine = utils_db.insertInto('persos', 10)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['persos']}, query=query_recupEvals)
        # suppression des éventuels doublons dans la table persos :
        commandLine = (
            'DELETE FROM persos '
            'WHERE rowid NOT IN ('
            '    SELECT min(rowid) '
            '    FROM persos '
            '    GROUP BY '
            '        id_prof, id_eleve, Periode, Matiere, id_bilan '
            '    )')
        query_recupEvals = utils_db.queryExecute(
            commandLine, query=query_recupEvals)

        # mise à jour de la table recup_evals.appreciations :
        commandLine = utils_db.insertInto('appreciations', 5)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['appreciations']}, query=query_recupEvals)

        # mise à jour de la table recup_evals.notes :
        commandLine = utils_db.insertInto('notes', 5)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['notes']}, query=query_recupEvals)

        # mise à jour de la table recup_evals.absences :
        commandLine = utils_db.insertInto('absences', 7)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['absences']}, query=query_recupEvals)

        # mise à jour de la table recup_evals.dnb_values :
        commandLine = utils_db.insertInto('dnb_values', 4)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['dnb']}, query=query_recupEvals)

        # mise à jour de la table recup_evals.lsu :
        commandLine = utils_db.insertInto('lsu', 10)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['lsu']}, query=query_recupEvals)

        # mise à jour de la table recup_evals.eleve_groupe :
        commandLine = utils_db.insertInto('eleve_groupe', 6)
        query_recupEvals = utils_db.queryExecute(
            {commandLine: calcLists['eleveGroupe']}, query=query_recupEvals)

        # mise à jour de la table referential_propositions.bilans_details
        # récupération des anciennes valeurs :
        oldDic, nom2id = {}, {}
        for id_prof in dateProfDic:
            nom2id[dateProfDic[id_prof][1]] = id_prof
        commandLine_propositions = utils_functions.u(
            '{0} WHERE annee_scolaire="{1}" AND {2}{3}').format(
                utils_db.q_selectAllFrom.format('bilans_details'), 
                main.annee_scolaire[0], 
                endLine_nomProfs, 
                endLine_idEleves)
        query_propositions = utils_db.queryExecute(
            commandLine_propositions, query=query_propositions)
        while query_propositions.next():
            id_eleve = int(query_propositions.value(0))
            matiereCode = query_propositions.value(3)
            nom_prof = query_propositions.value(4)
            if (nom_prof in nom2id):
                id_prof = nom2id[nom_prof]
            elif (utils_functions.u(nom_prof) in nom2id):
                id_prof = nom2id[utils_functions.u(nom_prof)]
            else:
                id_prof = -1
            bilanName = query_propositions.value(5)
            value = query_propositions.value(6)
            date = query_propositions.value(2)
            if id_prof != -1:
                oldDic[(id_eleve, matiereCode, id_prof, bilanName)] = (value, date)
        # suppression des enregistrements à réécrire dans 
        # referential_propositions.bilans_details :
        commandLine_propositions = (
            'DELETE FROM bilans_details '
            'WHERE annee_scolaire="{0}" AND {1}{2}')
        commandLine_propositions = utils_functions.u(
            commandLine_propositions).format(
                main.annee_scolaire[0], endLine_nomProfs, endLine_idEleves)
        query_propositions = utils_db.queryExecute(commandLine_propositions, query=query_propositions)
        # on récupère les enregistrements depuis recup_evals
        # car les évaluations de toutes les périodes 
        # doivent être prises en compte :
        referentialPropositions_bilansDetails, tempDic = [], {}
        commandLine_recupEvals = '{0} WHERE id_competence<{1} AND {2}{3}'.format(
            utils_db.q_selectAllFrom.format('bilans'),
            utils.decalageBLT,
            endLine_idProfs,
            endLine_idEleves)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_eleve = int(query_recupEvals.value(1))
            matiereCode = query_recupEvals.value(7)
            id_prof = int(query_recupEvals.value(0))
            bilanName = query_recupEvals.value(4)
            value = query_recupEvals.value(6)
            if not((id_eleve, matiereCode, id_prof, bilanName) in tempDic):
                tempDic[(id_eleve, matiereCode, id_prof, bilanName)] = ''
            tempDic[(id_eleve, matiereCode, id_prof, bilanName)] += value
        # on calcule les bilans :
        for data in tempDic:
            value = utils_calculs.calculBilan(
                tempDic[data], admin=True)
            if value != '':
                id_eleve = data[0]
                matiereCode = data[1]
                id_prof = data[2]
                bilanName = data[3]
                if id_prof in dateProfDic:
                    date = dateProfDic[id_prof][0]
                    nom_prof = dateProfDic[id_prof][1]
                    if data in oldDic:
                        if oldDic[data][0] == tempDic[data]:
                            date = oldDic[data][1]
                    ligneForReferentialPropositions = (
                        id_eleve, 
                        main.annee_scolaire[0], 
                        date, 
                        matiereCode, 
                        nom_prof, 
                        bilanName, 
                        value)
                    utils_functions.appendUnique(
                        referentialPropositions_bilansDetails, 
                        ligneForReferentialPropositions)
        # et on remplit la table bilans_details :
        commandLine_propositions = utils_db.insertInto('bilans_details', 7)
        query_propositions = utils_db.queryExecute(
            {commandLine_propositions: referentialPropositions_bilansDetails}, 
            query=query_propositions)

        if utils.ADMIN_WITH_DETAILS == 1:
            """
            Récupération des détails:
            chaque ligne de calcLists['detailsLinks'] contient :
                id_prof, id_item, id_bilan, id_competence, itemLabel
            chaque ligne de calcLists['detailsValues'] contient :
                id_prof, id_tableau, id_eleve, id_item, value, periode
            """
            if mustClear:
                commandLine = utils_db.qdf_table.format('details_links')
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                commandLine = command_del.format('details_values')
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                commandLine = utils_db.qct_recupEvals_detailsLinks
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                commandLine = utils_db.qct_recupEvals_detailsValues
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            else:
                commandLine = utils_db.qct_recupEvals_detailsLinks
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                commandLine = utils_db.qct_recupEvals_detailsValues
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                # les profs concernés :
                commandLine_detailsValues = command_del.format('details_values')
                command_prof = ' AND id_prof={0}'
                for ligne in calcLists['filesProfsConfig']:
                    id_prof = ligne[0]
                    commandLine = utils_db.qdf_where.format(
                        'details_links', 'id_prof', id_prof)
                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                    command_fin = command_prof.format(id_prof)
                    commandLine = commandLine_detailsValues + command_fin
                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            commandLine = utils_db.insertInto('details_links', 5)
            query_recupEvals = utils_db.queryExecute(
                {commandLine: calcLists['detailsLinks']}, query=query_recupEvals)
            commandLine = utils_db.insertInto('details_values', 6)
            query_recupEvals = utils_db.queryExecute(
                {commandLine: calcLists['detailsValues']}, query=query_recupEvals)
        else:
            # si on a décoché la récupération des détails, cela permet
            # de vider les 2 tables devenues inutiles
            tables = utils_db.tablesInDB(query=query_recupEvals)
            if 'details_links' in tables:
                commandLine = utils_db.qdf_table.format('details_links')
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                if 'details_values' in tables:
                    commandLine = utils_db.qdf_table.format('details_values')
                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)

    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_db.endTransaction(
            query_recupEvals, admin.db_recupEvals, transactionOK_recupEvals)
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_recupEvals, admin.dbFile_recupEvals)
        utils_db.endTransaction(
            query_propositions, 
            admin.db_referentialPropositions, 
            transactionOK_propositions)
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_referentialPropositions, admin.dbFile_referentialPropositions)

def createResultats(main, recupLevel=0, idsEleves=[-1], msgFin=True):
    """
    on met à jour la base des résultats
    passer idsEleves=[-1] pour traiter tous les élèves
    passer idsEleves=[] pour faire sélectionner les élèves à traiter
    si recupLevel > RECUP_LEVEL_SELECT, on recrée les tables 
    (donc on traite tous les élèves)
    """
    if len(idsEleves) < 1:
        dialog = admin.ListElevesDlg(
            parent=main, helpContextPage='update-bilans')
        lastState = main.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            main.enableInterface(dialog, lastState)
            return
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            idsEleves.append(id_eleve)
        if len(idsEleves) < 1:
            for i in range(dialog.baseList.count()):
                item = dialog.baseList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                idsEleves.append(id_eleve)
        main.enableInterface(dialog, lastState)

    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Calculation of results'), 
        tags=('h2'))
    utils_functions.doWaitCursor()
    debut = QtCore.QTime.currentTime()
    reponse = False
    try:
        # ouverture des bases et query :
        admin.openDB(main, 'resultats')
        query_resultats, transactionOK_resultats = utils_db.queryTransactionDB(
            admin.db_resultats)
        query_admin = utils_db.query(admin.db_admin)
        query_commun = utils_db.query(main.db_commun)
        admin.openDB(main, 'recup_evals')
        query_recupEvals = utils_db.query(admin.db_recupEvals)

        # test de la version de la base resultats :
        versionDB_resultats = utils_db.readVersionDB(admin.db_resultats)
        if versionDB_resultats < utils.VERSIONDB_RESULTATS:
            recupLevel = utils.RECUP_LEVEL_ALL
            idsEleves = [-1]
            if versionDB_resultats < 8:
                query_resultats = utils_db.queryExecute(
                    utils_db.q_dropTable.format('absences'), query=query_resultats)
            utils_db.changeInConfigTable(
                main, 
                admin.db_resultats, 
                {'versionDB': (utils.VERSIONDB_RESULTATS, '')})

        # un message pour l'affichage :
        messageCalcTable = QtWidgets.QApplication.translate(
            'main', 'Calculating the table: <b>{0}</b>')

        # morceaux de lignes de commandes pour gérer les périodes : 
        commandLine_periode1 = 'Periode={0}'.format(utils.selectedPeriod)
        commandLine_periode2 = '(Periode=0 OR Periode={0})'.format(utils.selectedPeriod)
        commandLine_periode3 = '(Periode<={0})'.format(utils.selectedPeriod)

        # on recrée les tables (au cas où) :
        listCommands = [
            utils_db.qct_resultats_eleveProf, 
            utils_db.qct_resultats_eleveGroupe, 
            utils_db.qct_resultats_eleves, 
            utils_db.qct_resultats_appreciations, 
            utils_db.qct_resultats_bilans, 
            utils_db.qct_resultats_bilansDetails, 
            utils_db.qct_resultats_persos, 
            utils_db.qct_resultats_syntheses, 
            utils_db.qct_resultats_notes, 
            utils_db.qct_resultats_notes_classes, 
            utils_db.qct_resultats_absences, 
            utils_db.qct_resultats_lsu]
        query_resultats = utils_db.queryExecute(listCommands, query=query_resultats)

        tables = (
            'eleve_prof', 'eleve_groupe', 'eleves', 
            'appreciations', 'bilans', 'bilans_details', 'persos', 'syntheses', 
            'notes', 'notes_classes', 'absences', 
            )
        if (recupLevel > utils.RECUP_LEVEL_SELECT) or (-1 in idsEleves):
            # on supprime entièrement les tables :
            for table in tables:
                query_resultats = utils_db.queryExecute(
                    utils_db.qdf_table.format(table), query=query_resultats)
        else:
            # on efface les anciens enregistrements :
            forIn_idsEleves = utils_functions.array2string(idsEleves)
            for table in tables:
                if table != 'notes_classes':
                    commandLine = utils_db.qdf_where.format(
                        table, 'id_eleve', forIn_idsEleves)
                    query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
            # on efface aussi les classes :
            for table in ('eleves', 'bilans', 'syntheses'):
                commandLine = 'DELETE FROM {0} WHERE id_eleve<0'.format(table)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
            # la table notes_classes n'a pas d'id_eleve :
            notesIdsClasses = []
            for id_eleve in idsEleves:
                if id_eleve in admin.elevesNotes:
                    notesIdsClasses.append(admin.elevesNotes[id_eleve][1])
            # on efface les doublons :
            notesIdsClasses = list(set(notesIdsClasses))
            forIn_notes = utils_functions.array2string(notesIdsClasses)
            commandLine = utils_db.qdf_where.format(
                'notes_classes', 'id_classe', forIn_notes)
            query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)

        # pour raccourcir les conditions d'inscription :
        mustDo = ((recupLevel > utils.RECUP_LEVEL_SELECT) or (-1 in idsEleves))


        # *********************************************
        # table eleve_prof
        # *********************************************
        utils_functions.afficheMessage(
            main, messageCalcTable.format('eleve_prof'), editLog=True)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        commandLine_recupEvals = '{0} WHERE {1}'.format(
            utils_db.q_selectAllFrom.format('eleve_prof'), commandLine_periode2)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_prof = int(query_recupEvals.value(0))
            id_eleve = int(query_recupEvals.value(1))
            matiereCode = query_recupEvals.value(2)
            if (mustDo) or (id_eleve in idsEleves):
                utils_functions.appendUnique(
                    lines, 
                    (id_prof, id_eleve, matiereCode))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('eleve_prof', 3)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table eleve_groupe
        # *********************************************
        utils_functions.afficheMessage(
            main, messageCalcTable.format('eleve_groupe'), editLog=True)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        commandLine_recupEvals = '{0} WHERE {1}'.format(
            utils_db.q_selectAllFrom.format('eleve_groupe'), commandLine_periode2)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_prof = int(query_recupEvals.value(0))
            id_eleve = int(query_recupEvals.value(1))
            id_groupe = int(query_recupEvals.value(2))
            groupeName = query_recupEvals.value(3)
            matiereCode = query_recupEvals.value(4)
            if (mustDo) or (id_eleve in idsEleves):
                utils_functions.appendUnique(
                    lines, 
                    (id_prof, id_eleve, id_groupe, groupeName, matiereCode))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('eleve_groupe', 5)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table eleves
        # *********************************************
        utils_functions.afficheMessage(
            main, messageCalcTable.format('eleves'), editLog=True, p=True)
        # on récupère les types des classes dans la base commun :
        classeTypeFromClasse = {}
        commandLine_commun = utils_db.q_selectAllFrom.format('classes')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            #id_classe = int(query_commun.value(0))
            classeName = query_commun.value(1)
            classeType = int(query_commun.value(2))
            classeTypeFromClasse[classeName] = classeType
        # on récupère les classes et leurs id :
        # un dictionnaire pour retrouver l'id (< 0) d'une classe
        # idFromClasse[classeName] = id_classe
        idFromClasse = {}
        minIdClasse = 0
        # un dictionnaire pour retrouver la classe d'un élève
        # classeFromEleve[id_eleve] = (id_classe, classeType)
        classeFromEleve = {}
        # on récupère idFromClasse :
        commandLine_resultats = 'SELECT * FROM eleves WHERE id_eleve<0 ORDER BY -id_eleve'
        query_resultats = utils_db.queryExecute(commandLine_resultats, query=query_resultats)
        while query_resultats.next():
            id_classe = int(query_resultats.value(0))
            classeName = query_resultats.value(2)
            idFromClasse[classeName] = id_classe
            minIdClasse = id_classe
        # on récupère les enregistrements à inscrire depuis admin,
        # et on les met dans un dictionnaire :
        lines = []
        dateFichier = QtCore.QDate.currentDate().toString('dd/MM/yyyy')
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        messages = []
        queryList = utils_db.query2List(
            commandLine_admin, order=['Classe', 'NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            id_eleve = int(record[0])
            classe = record[4]
            login = record[5]
            initialMdp = record[7]
            if (mustDo) or (id_eleve in idsEleves):
                eleveNomPrenom = utils_functions.u('{0} {1}').format(record[2], record[3])
                dn = record[6]
                eleveDateNaissance = dn[0: 2] + '/' + dn[2: 4] + '/' + dn[4:]
                utils_functions.appendUnique(
                    lines, 
                    (id_eleve, dateFichier, classe, 
                     eleveNomPrenom, eleveDateNaissance, login, initialMdp))
                messages.append(utils_functions.u('> {0} {1}').format(eleveNomPrenom, classe))
            # si la classe n'est pas déjà là, on l'ajoute :
            if not(classe in idFromClasse):
                minIdClasse -= 1
                idFromClasse[classe] = minIdClasse
            id_classe = idFromClasse[classe]
            classeType = classeTypeFromClasse[classe]
            classeFromEleve[id_eleve] = (id_classe, classeType)
        utils_functions.afficheMessage(main, messages, editLog=True)
        # on ajoute les classes à la liste :
        for classe in idFromClasse:
            utils_functions.appendUnique(
                lines, 
                (idFromClasse[classe], '', classe, '', '', '', ''))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('eleves', 7)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table appreciations
        # *********************************************
        utils_functions.afficheMessage(
            main, 
            messageCalcTable.format('appreciations'), 
            editLog=True, 
            p=True)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        commandLine_recupEvals = '{0} WHERE {1}'.format(
            utils_db.q_selectAllFrom.format('appreciations'), commandLine_periode1)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_eleve = int(query_recupEvals.value(1))
            matiereCode = query_recupEvals.value(3)
            appreciation = query_recupEvals.value(2)
            id_prof = int(query_recupEvals.value(0))
            if appreciation != '':
                if (mustDo) or (id_eleve in idsEleves):
                    utils_functions.appendUnique(
                        lines, 
                        (id_eleve, matiereCode, appreciation, id_prof))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('appreciations', 4)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # récupération des sous-rubriques du bulletin
        # *********************************************
        message = QtWidgets.QApplication.translate(
            'main', 'Recovery of sub-sections of bulletin')
        utils_functions.afficheMessage(main, message, editLog=True)
        # un dictionnaire pour récupérer les enregistrements
        # à inscrire dans la table synthese :
        syntheseDic = {}
        # un dictionnaire pour retrouver la sous-rubrique d'un bilan
        # sousRubriqueFromBilan[bilanName] = 
        #       {'what': (sousRubrique, liste, niveau), 'classesTypes': []}
        # l'entier liste servira à savoir si c'est le bulletin, le referentiel ou confidentiel
        # l'entier niveau servira à savoir si c'est le niveau T1, T2 ou T3
        # il peut y avoir plusieurs types de classes ayant ce bilan, d'où la liste
        sousRubriqueFromBilan = {}
        # On remplit sousRubriqueFromBilan :
        commandLine_commun = 'SELECT DISTINCT * FROM bulletin WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            name = query_commun.value(1)
            T1 = query_commun.value(6)
            T2 = utils_functions.u('{0}{1}').format(
                query_commun.value(6), query_commun.value(7))
            T3 = utils_functions.u('{0}{1}{2}').format(
                query_commun.value(6), query_commun.value(7), query_commun.value(8))
            classeType = int(query_commun.value(11))
            if name in sousRubriqueFromBilan:
                sousRubriqueFromBilan[name]['classesTypes'].append(classeType)
            else:
                if T1 in admin.listeSousRubriques['bulletin']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T1, 1, 1), 'classesTypes': [classeType]}
                elif T2 in admin.listeSousRubriques['bulletin']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T2, 1, 2), 'classesTypes': [classeType]}
                elif T3 in admin.listeSousRubriques['bulletin']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T3, 1, 3), 'classesTypes': [classeType]}
        commandLine = 'SELECT DISTINCT * FROM referentiel WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        while query_commun.next():
            name = query_commun.value(1)
            T1 = query_commun.value(6)
            T2 = utils_functions.u('{0}{1}').format(
                query_commun.value(6), query_commun.value(7))
            T3 = utils_functions.u('{0}{1}{2}').format(
                query_commun.value(6), query_commun.value(7), query_commun.value(8))
            classeType = int(query_commun.value(11))
            if name in sousRubriqueFromBilan:
                sousRubriqueFromBilan[name]['classesTypes'].append(classeType)
            else:
                if T1 in admin.listeSousRubriques['referentiel']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T1, 2, 1), 'classesTypes': [classeType]}
                elif T2 in admin.listeSousRubriques['referentiel']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T2, 2, 2), 'classesTypes': [classeType]}
                elif T3 in admin.listeSousRubriques['referentiel']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T3, 2, 3), 'classesTypes': [classeType]}
        commandLine = 'SELECT DISTINCT * FROM confidentiel WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        while query_commun.next():
            name = query_commun.value(1)
            T1 = query_commun.value(6)
            T2 = utils_functions.u('{0}{1}').format(
                query_commun.value(6), query_commun.value(7))
            T3 = utils_functions.u('{0}{1}{2}').format(
                query_commun.value(6), query_commun.value(7), query_commun.value(8))
            classeType = int(query_commun.value(11))
            if name in sousRubriqueFromBilan:
                sousRubriqueFromBilan[name]['classesTypes'].append(classeType)
            else:
                if T1 in admin.listeSousRubriques['confidentiel']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T1, 3, 1), 'classesTypes': [classeType]}
                elif T2 in admin.listeSousRubriques['confidentiel']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T2, 3, 2), 'classesTypes': [classeType]}
                elif T3 in admin.listeSousRubriques['confidentiel']:
                    sousRubriqueFromBilan[name] = {
                        'what': (T3, 3, 3), 'classesTypes': [classeType]}

        # ***********************************************************
        # tables bilans et bilans_details
        #
        # si un prof évalue à la fois dans année et dans la période,
        # on ne doit pas récupérer le bilan de "année".
        # D'où l'ajout d'un "ORDER BY" et les tuples "last" et "new"
        # pour écarter ce cas.
        # ***********************************************************
        utils_functions.afficheMessage(
            main, 
            messageCalcTable.format('bilans'), 
            editLog=True)
        # on recrée les tables :
        query_resultats = utils_db.queryExecute(
            [utils_db.qct_resultats_bilans, utils_db.qct_resultats_bilansDetails], 
            query=query_resultats)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire.
        # tempDic est rempli dans un deuxième temps (après tempDicDetails)
        # pour qu'un prof ayant évalué sur plusieurs périodes
        # n'ait pas plus de poids
        message = QtWidgets.QApplication.translate(
            'main', 'Calculating students assessments')
        utils_functions.afficheMessage(main, message, editLog=True)
        lines = []
        tempDic = {}
        tempDicDetails = {}
        linesDetails = []
        # on ordonne pour ne pas récupérer des valeurs inutiles (voir remarque ci-dessus) :
        order = 'ORDER BY id_eleve, id_competence, Matiere, id_prof, Periode DESC'
        # d'abord les bilans du bulletin et confidentiels (comptent année + période) :
        commandLine_recupEvals = '{0} WHERE {1} AND id_competence>={2} {3}'.format(
            utils_db.q_selectAllFrom.format('bilans'),
            commandLine_periode2,
            utils.decalageBLT,
            order)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        last = ()
        while query_recupEvals.next():
            id_prof = int(query_recupEvals.value(0))
            id_eleve = int(query_recupEvals.value(1))
            id_competence = int(query_recupEvals.value(3))
            name = query_recupEvals.value(4)
            value = query_recupEvals.value(6)
            matiereCode = query_recupEvals.value(7)
            periode = int(query_recupEvals.value(8))
            new = (id_eleve, id_competence, matiereCode, id_prof)
            if (new != last) or (periode > 0):
                if (mustDo) or (id_eleve in idsEleves):
                    if not((id_eleve, name, id_prof, matiereCode) in tempDicDetails):
                        tempDicDetails[(id_eleve, name, id_prof, matiereCode)] = ''
                    tempDicDetails[(id_eleve, name, id_prof, matiereCode)] += value
            last = new
        # puis les bilans du référentiel (comptent toutes les périodes jusqu'à celle sélectionnée) :
        commandLine_recupEvals = '{0} WHERE {1} AND id_competence<{2} {3}'.format(
            utils_db.q_selectAllFrom.format('bilans'),
            commandLine_periode3,
            utils.decalageBLT,
            order)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        last = ()
        while query_recupEvals.next():
            id_prof = int(query_recupEvals.value(0))
            id_eleve = int(query_recupEvals.value(1))
            id_competence = int(query_recupEvals.value(3))
            name = query_recupEvals.value(4)
            value = query_recupEvals.value(6)
            matiereCode = query_recupEvals.value(7)
            periode = int(query_recupEvals.value(8))
            new = (id_eleve, id_competence, matiereCode, id_prof)
            if (new != last) or (periode > 0):
                if (mustDo) or (id_eleve in idsEleves):
                    if not((id_eleve, name, id_prof, matiereCode) in tempDicDetails):
                        tempDicDetails[(id_eleve, name, id_prof, matiereCode)] = ''
                    tempDicDetails[(id_eleve, name, id_prof, matiereCode)] += value
            last = new
        # on calcule les bilans :
        for (id_eleve, name, id_prof, matiereCode) in tempDicDetails:
            value = utils_calculs.calculBilan(
                tempDicDetails[(id_eleve, name, id_prof, matiereCode)], 
                admin=True)
            if not((id_eleve, name) in tempDic):
                tempDic[(id_eleve, name)] = ''
            tempDic[(id_eleve, name)] += value
            if value != '':
                linesDetails.append((id_eleve, name, id_prof, matiereCode, value))
        for (id_eleve, name) in tempDic:
            value = utils_calculs.calculBilan(
                tempDic[(id_eleve, name)], admin=True)
            if value != '':
                utils_functions.appendUnique(
                    lines, 
                    (id_eleve, name, value))
                # on teste si c'est à récupérer pour la table synthese :
                # le classeType de l'élève doit être dans la liste des classesTypes du bilan
                if id_eleve in classeFromEleve:
                    classeType_eleve = classeFromEleve[id_eleve][1]
                else:
                    classeType_eleve = 0
                if name in sousRubriqueFromBilan:
                    (sousRubrique, liste, niveau) = sousRubriqueFromBilan[name]['what']
                    classesTypes = sousRubriqueFromBilan[name]['classesTypes']
                    if (-1 in classesTypes) or (classeType_eleve in classesTypes):
                        if sousRubrique != '':
                            if not((id_eleve, sousRubrique, liste, niveau) in syntheseDic):
                                syntheseDic[(id_eleve, sousRubrique, liste, niveau)] = ''
                            syntheseDic[(id_eleve, sousRubrique, liste, niveau)] += value
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('bilans', 3)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)
        commandLine_resultats = utils_db.insertInto('bilans_details', 5)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: linesDetails}, query=query_resultats)
        # on calcule les bilans des classes en interrogeant resultats.bilans :
        message = QtWidgets.QApplication.translate(
            'main', 'Calculating classes assessments')
        utils_functions.afficheMessage(main, message, editLog=True)
        lines = []
        tempDic = {}
        commandLine_resultats = 'SELECT * FROM bilans WHERE id_eleve>-1'
        query_resultats = utils_db.queryExecute(commandLine_resultats, query=query_resultats)
        while query_resultats.next():
            id_eleve = int(query_resultats.value(0))
            name = query_resultats.value(1)
            value = query_resultats.value(2)
            # le try-except permet de passer les élèves
            # qui n'existent pas (ou plus)
            # (cas du prof n'ayant pas nettoyé sa base !!!)
            try:
                (id_classe, classeType) = classeFromEleve[id_eleve]
                if not((id_classe, name) in tempDic):
                    tempDic[(id_classe, name)] = ''
                tempDic[(id_classe, name)] += value
            except:
                continue
        # on calcule les bilans :
        for (id_classe, name) in tempDic:
            value = utils_calculs.moyenneBilan(tempDic[(id_classe, name)])
            if value != '':
                utils_functions.appendUnique(
                    lines, 
                    (id_classe, name, value))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('bilans', 3)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table persos
        # *********************************************
        utils_functions.afficheMessage(
            main, 
            messageCalcTable.format('persos'), 
            editLog=True)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        tempDic = {}
        commandLine_recupEvals = '{0} WHERE {1} ORDER BY id_eleve, Matiere, ordre'.format(
            utils_db.q_selectAllFrom.format('persos'),
            commandLine_periode2)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_eleve = int(query_recupEvals.value(1))
            label = query_recupEvals.value(4)
            value = query_recupEvals.value(5)
            groupeValue = query_recupEvals.value(6)
            matiereCode = query_recupEvals.value(7)
            id_prof = int(query_recupEvals.value(0))
            if (mustDo) or (id_eleve in idsEleves):
                try:
                    ordre = tempDic[(id_eleve, matiereCode, id_prof)][0]
                    ordre += 1
                    tempDic[(id_eleve, matiereCode, id_prof)][0] = ordre
                    if ordre < 10:
                        name = utils_functions.u('{0}-0{1}').format(matiereCode, ordre)
                    else:
                        name = utils_functions.u('{0}-{1}').format(matiereCode, ordre)
                    tempDic[(id_eleve, matiereCode, id_prof)][1].append(
                        (id_eleve, name, label, value, groupeValue, id_prof))
                except:
                    name = utils_functions.u('{0}-01').format(matiereCode)
                    tempDic[(id_eleve, matiereCode, id_prof)] = [
                        1, [(id_eleve, name, label, value, groupeValue, id_prof)]]
        # on calcule les bilans :
        for data in tempDic:
            for (id_eleve, name, label, value, groupeValue, id_prof) in tempDic[data][1]:
                if value != '':
                    utils_functions.appendUnique(
                        lines, 
                        (id_eleve, name, label, value, groupeValue, id_prof))
                    # on récupère pour la table synthese :
                    matiereCode = data[1]
                    if not((id_eleve, matiereCode, -1, id_prof) in syntheseDic):
                        syntheseDic[(id_eleve, matiereCode, -1, id_prof)] = ''
                    syntheseDic[(id_eleve, matiereCode, -1, id_prof)] += value
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('persos', 6)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table syntheses
        # *********************************************
        utils_functions.afficheMessage(
            main, 
            messageCalcTable.format('syntheses'), 
            editLog=True)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        tempDic = {}
        tempTotal = {}
        # on commence par calculer les moyennes :
        for data in syntheseDic:
            id_eleve = data[0]
            name = data[1]
            liste = data[2]
            niveau = data[3]
            value = syntheseDic[data]
            # on ne garde pas si c'est dans referentiel ou confidentiel :
            if liste < 2:
                if not(id_eleve in tempTotal):
                    tempTotal[id_eleve] = ''
                tempTotal[id_eleve] += value
            value = utils_calculs.calculBilan(value, admin=True)
            syntheseDic[data] = value
            if value != '':
                if liste == -1:
                    id_prof = niveau
                else:
                    id_prof = -(10 * liste + niveau)
                utils_functions.appendUnique(
                    lines, 
                    (id_eleve, name, value, id_prof))
        for id_eleve in tempTotal:
            values = utils_calculs.value2List(tempTotal[id_eleve])
            for i in range(utils.NB_COLORS + 1):
                valeurItem = utils.itemsValues[i]
                value = values[i]
                tempDic[(id_eleve, "Total-" + valeurItem)] = value
        for data in tempDic:
            id_eleve = data[0]
            name = data[1]
            value = tempDic[data]
            if value != 0:
                utils_functions.appendUnique(
                    lines, 
                    (id_eleve, name, value, -1))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('syntheses', 4)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)
        # on calcule les syntheses des classes en interrogeant resultats.syntheses :
        message = QtWidgets.QApplication.translate(
            'main', 'Calculating classes synthesis')
        utils_functions.afficheMessage(main, message, editLog=True)
        lines = []
        tempDic = {}
        # id_prof!=-1 pour ne pas récupérer les totaux :
        commandLine_resultats = (
            'SELECT * FROM syntheses '
            'WHERE (id_eleve>-1 AND id_prof!=-1)')
        query_resultats = utils_db.queryExecute(commandLine_resultats, query=query_resultats)
        while query_resultats.next():
            id_eleve = int(query_resultats.value(0))
            name = query_resultats.value(1)
            value = query_resultats.value(2)
            # le try-except permet de passer les élèves
            # qui n'existent pas (ou plus)
            # (cas du prof n'ayant pas nettoyé sa base !!!)
            try:
                (id_classe, classeType) = classeFromEleve[id_eleve]
            except:
                continue
            (id_classe, classeType) = classeFromEleve[id_eleve]
            id_prof = int(query_resultats.value(3))
            if id_prof > -1:
                id_prof = -1
            if not((id_classe, name, id_prof) in tempDic):
                tempDic[(id_classe, name, id_prof)] = ''
            tempDic[(id_classe, name, id_prof)] += value
        # on calcule les bilans :
        for (id_classe, name, id_prof) in tempDic:
            value = utils_calculs.moyenneBilan(
                tempDic[(id_classe, name, id_prof)])
            if value != '':
                utils_functions.appendUnique(
                    lines, 
                    (id_classe, name, value, id_prof))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('syntheses', 4)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table notes
        # *********************************************
        utils_functions.afficheMessage(
            main, 
            messageCalcTable.format('notes'), 
            editLog=True)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        commandLine_recupEvals = '{0} WHERE {1}'.format(
            utils_db.q_selectAllFrom.format('notes'), commandLine_periode1)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_eleve = int(query_recupEvals.value(1))
            matiereCode = query_recupEvals.value(2)
            # on ne transforme pas en float maintenant :
            value = query_recupEvals.value(4)
            id_classe = admin.elevesNotes[id_eleve][1]
            id_prof = int(query_recupEvals.value(0))
            if (mustDo) or (id_eleve in idsEleves):
                utils_functions.appendUnique(
                    lines, 
                    (id_eleve, matiereCode, value, id_classe, id_prof))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('notes', 5)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table notes_classes
        # *********************************************
        utils_functions.afficheMessage(
            main, 
            messageCalcTable.format('notes_classes'), 
            editLog=True)
        notesIdsClasses = []
        if (recupLevel > utils.RECUP_LEVEL_SELECT) or (-1 in idsEleves):
            for classeName in admin.classesNotes:
                notesIdsClasses.append(admin.classesNotes[classeName])
        else:
            for id_eleve in idsEleves:
                if id_eleve in admin.elevesNotes:
                    notesIdsClasses.append(admin.elevesNotes[id_eleve][1])
        # on efface les doublons :
        notesIdsClasses = list(set(notesIdsClasses))
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        # pour chaque classe à note :
        for id_classe in notesIdsClasses:
            # on récupère la liste des notes des élèves :
            notes = {}
            commandLine_resultats = utils_db.q_selectAllFromWhere.format(
                'notes', 'id_classe', id_classe)
            query_resultats = utils_db.queryExecute(
                commandLine_resultats.format(id_classe), query=query_resultats)
            while query_resultats.next():
                matiereCode = query_resultats.value(1)
                try:
                    value = float(query_resultats.value(2))
                    id_prof = int(query_resultats.value(4))
                    if (id_prof, matiereCode) in notes:
                        notes[(id_prof, matiereCode)].append(value)
                    else:
                        notes[(id_prof, matiereCode)] = [value]
                except:
                    continue
            for (id_prof, matiereCode) in notes:
                listNotes = notes[(id_prof, matiereCode)]
                # on calcule les valeurs :
                moyenne = utils_calculs.average(listNotes)
                ecart = utils_calculs.standardDeviation(listNotes, moyenne)
                moyenne = round(moyenne, utils.precisionNotes)
                ecart = round(ecart, utils.precisionNotes)
                mini, maxi = utils_calculs.minMax(listNotes)
                utils_functions.appendUnique(
                    lines, 
                    (id_classe, matiereCode, moyenne, ecart, mini, maxi, id_prof))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('notes_classes', 7)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # table absences
        # *********************************************
        utils_functions.afficheMessage(
            main, 
            messageCalcTable.format('absences'), 
            editLog=True)
        # on récupère les enregistrements à inscrire depuis recup_evals,
        # et on les met dans un dictionnaire :
        lines = []
        commandLine_recupEvals = '{0} WHERE {1}'.format(
            utils_db.q_selectAllFrom.format('absences'), commandLine_periode1)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_eleve = int(query_recupEvals.value(1))
            abs0 = int(query_recupEvals.value(3))
            abs1 = int(query_recupEvals.value(4))
            abs2 = int(query_recupEvals.value(5))
            abs3 = int(query_recupEvals.value(6))
            if (mustDo) or (id_eleve in idsEleves):
                utils_functions.appendUnique(
                    lines, 
                    (id_eleve, abs0, abs1, abs2, abs3))
        # on écrit les enregistrements dans resultats :
        commandLine_resultats = utils_db.insertInto('absences', 5)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        # *********************************************
        # finalisation
        # *********************************************
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        reponse = True
        if msgFin:
            fin = QtCore.QTime.currentTime()
            utils_functions.afficheDuree(main, debut, fin)
            utils_functions.afficheMsgFin(main, timer=5)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'The creation of the results failed')
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_db.endTransaction(
            query_resultats, admin.db_resultats, transactionOK_resultats)
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_resultats, admin.dbFile_resultats)
        utils_functions.restoreCursor()
        return reponse

def calculateReferentialPropositions(main, idsEleves=[-1], msgFin=False):
    """
    passer idsEleves=[-1] pour traiter tous les élèves
    passer idsEleves=[] pour faire sélectionner les élèves à traiter

    on calcule la table referential_propositions.bilans_propositions
    (la table bilans_details existe déjà).
    On met aussi à jour la table eleves.
    """
    reponse = False
    debut = QtCore.QTime.currentTime()
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Calculation of the referential'), 
        tags=('h2'))
    utils_functions.doWaitCursor()
    if len(idsEleves) < 1:
        utils_functions.restoreCursor()
        dialog = admin.ListElevesDlg(
            parent=main, helpContextPage='update-bilans')
        lastState = main.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            main.enableInterface(dialog, lastState)
            return reponse
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            idsEleves.append(id_eleve)
        main.enableInterface(dialog, lastState)
        utils_functions.doWaitCursor()
        if len(idsEleves) < 1:
            idsEleves = [-1]
    today = QtCore.QDate.currentDate().toString('dd/MM/yyyy')

    admin.openDB(main, 'referential_propositions')
    query_propositions, transactionOK_propositions = utils_db.queryTransactionDB(
        admin.db_referentialPropositions)
    query_commun = utils_db.query(main.db_commun)
    try:
        # **********************************************************
        # PARTIE 1
        # on récupère les résultats des bilans
        # depuis la table referential_propositions.bilans_details
        # **********************************************************
        bilansDetails = {}
        commandLine_propositions = utils_db.q_selectAllFromOrder.format(
            'bilans_details', 'id_eleve, bilan_name, annee_scolaire, Matiere')
        query_propositions = utils_db.queryExecute(
            commandLine_propositions, query=query_propositions)
        while query_propositions.next():
            id_eleve = int(query_propositions.value(0))
            if not((-1 in idsEleves) or (id_eleve in idsEleves)):
                continue
            annee = int(query_propositions.value(1))
            date = query_propositions.value(2)
            matiereCode = query_propositions.value(3)
            nomprof = query_propositions.value(4)
            bilanName = query_propositions.value(5)
            value = query_propositions.value(6)
            if not(id_eleve in bilansDetails):
                bilansDetails[id_eleve] = {}
            if not(bilanName in bilansDetails[id_eleve]):
                bilansDetails[id_eleve][bilanName] = {}
            if not (annee in bilansDetails[id_eleve][bilanName]):
                bilansDetails[id_eleve][bilanName][annee] = {}
            if not (matiereCode in bilansDetails[id_eleve][bilanName][annee]):
                bilansDetails[id_eleve][bilanName][annee][matiereCode] = ''
            bilansDetails[id_eleve][bilanName][annee][matiereCode] += value

        # **********************************************************
        # PARTIE 2
        # on récupère la liste des compétences du référentiel
        # et on l'organise dans un dictionnaire
        # **********************************************************
        referential = {'CODES': {}, 'VALUES': {}}
        commandLine_commun = utils_db.q_selectAllFromOrder.format(
            'referentiel', 'ordre')
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        T1, T2, T3 = '', '', ''
        while query_commun.next():
            code = query_commun.value(1)
            titres = []
            for i in range(2, 6):
                titres.append(query_commun.value(i))
            if titres[0] != '':
                T1 = code
                T2, T3 = '', ''
                referential['CODES'][T1] = {}
                referential['VALUES'][T1] = ''
            elif titres[1] != '':
                T2 = code
                T3 = ''
                referential['CODES'][T1][T2] = {}
                referential['VALUES'][T2] = ''
            elif titres[2] != '':
                T3 = code
                if not(T2 in referential['CODES'][T1]):
                    referential['CODES'][T1][T2] = {}
                referential['CODES'][T1][T2][T3] = []
                referential['VALUES'][T3] = ''
            else:
                if not(T2 in referential['CODES'][T1]):
                    referential['CODES'][T1][T2] = {}
                if not(T3 in referential['CODES'][T1][T2]):
                    referential['CODES'][T1][T2][T3] = []
                referential['CODES'][T1][T2][T3].append(code)
                referential['VALUES'][code] = ''

        # **********************************************************
        # PARTIE 3
        # on calcule les propositions de VÉRAC
        # **********************************************************
        validDic = {}
        lignePropositions = []
        for id_eleve in bilansDetails:
            if not id_eleve in validDic:
                validDic[id_eleve]= deepcopy(referential['VALUES'])
            for T1 in referential['CODES']:
                for T2 in referential['CODES'][T1]:
                    for T3 in referential['CODES'][T1][T2]:
                        for CPT in referential['CODES'][T1][T2][T3]:
                            if CPT in bilansDetails[id_eleve]:
                                values = utils_calculs.value2List('')
                                value = ''
                                annees = {'ORDER': [], 'VALUES': {}}
                                for annee in bilansDetails[id_eleve][CPT]:
                                    annees['ORDER'].append(annee)
                                    anneeValue = ''
                                    for matiereCode in bilansDetails[id_eleve][CPT][annee]:
                                        anneeValue += bilansDetails[id_eleve][CPT][annee][matiereCode]
                                    annees['VALUES'][annee] = anneeValue
                                annees['ORDER'].sort()
                                coeff = 0
                                for annee in annees['ORDER']:
                                    coeff += 1
                                    for i in range(coeff):
                                        value += annees['VALUES'][annee]
                                # on calcule la moyenne brute du bilan :
                                value = utils_calculs.moyenneBilan(value)
                                # on inscrit la valeur :
                                if value != '':
                                    lignePropositions.append((id_eleve, today, CPT, value, 0))
                                validDic[id_eleve][CPT] = value
                                if T3 != '':
                                    validDic[id_eleve][T3] += value
                                elif T2 != '':
                                    validDic[id_eleve][T2] += value
                                else:
                                    validDic[id_eleve][T1] += value
                        # on calcule le résultat du titre de niveau 3.
                        if T3 != '':
                            # on enregistre si au moins 1/3 des items sont renseignés :
                            nbItems = len(referential['CODES'][T1][T2][T3])
                            nbEvals = len(validDic[id_eleve][T3])
                            ratio = round(nbItems / 3)
                            if nbEvals >= ratio:
                                value = utils_calculs.moyenneBilan(
                                    validDic[id_eleve][T3])
                                validDic[id_eleve][T2] += value
                                lignePropositions.append((id_eleve, today, T3, value, 0))
                    # on calcule le résultat du titre de niveau 2 :
                    if T2 != '':
                        nbItems = len(referential['CODES'][T1][T2])
                        nbEvals = len(validDic[id_eleve][T2])
                        ratio = round(nbItems / 3)
                        if nbEvals >= ratio:
                            value = utils_calculs.moyenneBilan(
                                validDic[id_eleve][T2])
                            validDic[id_eleve][T1] += value
                            lignePropositions.append((id_eleve, today, T2, value, 0))
                # on calcule le résultat du titre de niveau 1 :
                nbItems = len(referential['CODES'][T1])
                nbEvals = len(validDic[id_eleve][T1])
                ratio = round(nbItems / 3)
                if nbEvals >= ratio:
                    value = utils_calculs.moyenneBilan(
                        validDic[id_eleve][T1])
                    lignePropositions.append((id_eleve, today, T1, value, 0))

        # **********************************************************
        # PARTIE 4
        # on enregistre les propositions
        # **********************************************************
        # On supprime les enregistrements concernant les élèves (propositions uniquement)
        query_propositions = utils_db.queryExecute(
            utils_db.qct_referentialPropositions_bilansPropositions, 
            query=query_propositions)
        if idsEleves == [-1]:
            query_propositions = utils_db.queryExecute(
                utils_db.qdf_table.format('bilans_propositions'), 
                query=query_propositions)
        else:
            id_eleves = utils_functions.array2string(idsEleves)
            commandLine = utils_db.qdf_where.format(
                'bilans_propositions', 'id_eleve', id_eleves)
            query_propositions = utils_db.queryExecute(commandLine, query=query_propositions)
        commandInsert = utils_db.insertInto('bilans_propositions', 5)
        query_propositions = utils_db.queryExecute(
            {commandInsert: lignePropositions}, query=query_propositions)
 
        # **********************************************************
        # PARTIE 5
        # on met à jour la liste des élèves
        # **********************************************************
        query_propositions = utils_db.queryExecute(
            utils_db.qct_referentialPropositions_eleves, 
            query=query_propositions)
        # calcul de l'année scolaire (pour 2010-2011, on prendra 2011) :
        currentDate = QtCore.QDateTime.currentDateTime().date()
        year = currentDate.year()
        # à partir du mois d'août, c'est l'année suivante :
        if currentDate.month() > 7:
            year += 1
        adminStudents = {}
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = 'SELECT id, NOM, Prenom FROM eleves'
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            eleveName = query_admin.value(1)
            eleveFirstName = query_admin.value(2)
            adminStudents[id_eleve] = utils_functions.u('{0}|{1}|{2}').format(
                eleveName, eleveFirstName, year)
        propositionsStudents = {}
        query_propositions = utils_db.queryExecute(
            utils_db.q_selectAllFrom.format('eleves'), 
            query=query_propositions)
        while query_propositions.next():
            id_eleve = int(query_propositions.value(0))
            eleveName = query_propositions.value(1)
            eleveFirstName = query_propositions.value(2)
            annee = int(query_propositions.value(3))
            propositionsData = utils_functions.u('{0}|{1}|{2}').format(
                eleveName, eleveFirstName, annee)
            if id_eleve in adminStudents:
                # on vérifie s'il faut mettre à jour les données :
                adminData = adminStudents[id_eleve]
                if propositionsData != adminData:
                    propositionsStudents[id_eleve] = adminData
            elif annee < year - 2:
                # on pourra supprimer cet élève
                # (plus d'évaluations depuis 2 ans) :
                propositionsStudents[id_eleve] = 'DELETE'
        for id_eleve in adminStudents:
            if not(id_eleve in propositionsStudents):
                propositionsStudents[id_eleve] = adminStudents[id_eleve]
        lastYearStudents = {}
        commandLine_propositions = (
            'SELECT DISTINCT id_eleve, annee_scolaire '
            'FROM bilans_details '
            'ORDER BY id_eleve, annee_scolaire DESC')
        query_propositions = utils_db.queryExecute(
            commandLine_propositions, query=query_propositions)
        while query_propositions.next():
            id_eleve = int(query_propositions.value(0))
            annee = int(query_propositions.value(1))
            if not(id_eleve in lastYearStudents):
                lastYearStudents[id_eleve] = annee
                if annee < year - 2:
                    if not(id_eleve in adminStudents):
                        # on pourra supprimer cet élève
                        # (plus d'évaluations depuis 2 ans) :
                        propositionsStudents[id_eleve] = 'DELETE'
                elif not(id_eleve in propositionsStudents):
                    # élève dont on a perdu le nom 
                    # (parti avant 2015 et création de cette table) :
                    propositionsStudents[id_eleve] = '||{0}'.format(annee)
        lines = {'delete': [], 'insert': [], 'deleteAll': [], }
        commandInsert = utils_db.insertInto('eleves', 4)
        for id_eleve in propositionsStudents:
            propositionsData = propositionsStudents[id_eleve]
            if propositionsData == 'DELETE':
                lines['deleteAll'].append(id_eleve)
            else:
                lines['delete'].append(id_eleve)
                propositionsData = propositionsData.split('|')
                lines['insert'].append(
                    (id_eleve, propositionsData[0], propositionsData[1], propositionsData[2]))
        forIn = utils_functions.array2string(lines['delete'])
        commandLine = utils_db.qdf_where.format('eleves', 'id_eleve', forIn)
        query_propositions = utils_db.queryExecute(commandLine, query=query_propositions)
        query_propositions = utils_db.queryExecute(
            {commandInsert: lines['insert']}, query=query_propositions)
        forIn = utils_functions.array2string(lines['deleteAll'])
        for table in (
            'bilans_details', 
            'bilans_propositions', 
            'eleves'):
            commandLine = utils_db.qdf_where.format(table, 'id_eleve', forIn)
            query_propositions = utils_db.queryExecute(commandLine, query=query_propositions)

        reponse = True
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(admin.dbFile_referentialPropositions)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_db.endTransaction(
            query_propositions, 
            admin.db_referentialPropositions, 
            transactionOK_propositions)
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_referentialPropositions, admin.dbFile_referentialPropositions)
        utils_functions.restoreCursor()
        calculateLSUComponents(main, idsEleves=idsEleves)
        utils_functions.afficheStatusBar(main)
        if msgFin:
            fin = QtCore.QTime.currentTime()
            utils_functions.afficheDuree(main, debut, fin)
            utils_functions.afficheMsgFin(main, timer=5)
        return reponse

def calculateLSUComponents(main, idsEleves=[-1], msgFin=False):
    """
    passer idsEleves=[-1] pour traiter tous les élèves

    LSU : on inscrit dans la table resultats.lsu
    les résultats pour les 8 composantes du socle 
    ainsi que les positionnements (Latin etc).
    Cela permettra de les lire dans l'interface web, etc.
    """
    reponse = False

    query_admin = utils_db.query(admin.db_admin)
    socleData = {}
    socle2socle = {}
    commandLine_admin = utils_db.q_selectAllFromWhereText.format(
        'lsu', 'lsuWhat', 'socle')
    query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
    while query_admin.next():
        lsu1 = query_admin.value(1)
        lsu2 = query_admin.value(2)
        socle2socle[lsu2] = lsu1
    if len(socle2socle) < 1:
        return reponse

    ensCompData = {}
    temp = {
        'eleve2classe': {}, 
        'classe2cycle': {}, 
        'matiere2EnseignementComplement': {}, 
        'enseignementsComplements': {}, 
        'STUDENTS': []
        }
    value2pos = {'D': 1, 'C': 2, 'B': 3, 'A': 4}

    debut = QtCore.QTime.currentTime()
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Calculation of the components of the socle'), 
        tags=('h4'))
    utils_functions.doWaitCursor()
    try:
        # on télécharge la base referential_validations :
        admin.downloadDB(main, db='referential_validations', msgFin=False)
        # ouverture des bases et query :
        admin.openDB(main, 'resultats')
        query_resultats, transactionOK_resultats = utils_db.queryTransactionDB(
            admin.db_resultats)
        admin.openDB(main, 'referential_propositions')
        query_propositions = utils_db.query(admin.db_referentialPropositions)
        admin.openDB(main, 'referential_validations')
        query_validations = utils_db.query(admin.db_referentialValidations)
        query_recupEvals = utils_db.query(admin.db_recupEvals)

        # fins de lignes pour les commandes :
        if idsEleves == [-1]:
            commandLineEnd = ''
            commandLineLsuEnd = ''
        else:
            studentsForIn = utils_functions.array2string(idsEleves)
            commandLineEnd = ' AND id_eleve IN ({0})'.format(studentsForIn)
            commandLineLsuEnd = ' AND lsu3 IN ({0})'.format(studentsForIn)

        # on récupère les enseignements de complément
        # depuis la table admin.lsu :
        commandLine_admin = (
            'SELECT * FROM lsu '
            'WHERE lsuWhat="ensComp"')
        query_admin = utils_db.queryExecute(
            commandLine_admin, query=query_admin)
        while query_admin.next():
            replaceWith = query_admin.value(1)
            matieres = query_admin.value(2)
            if len(matieres) > 0:
                matieres = matieres.split('|')
                temp['enseignementsComplements'][replaceWith] = {}
                for replaceWhat in matieres:
                    temp['matiere2EnseignementComplement'][replaceWhat] = replaceWith
        # au cas où le latin manquerait encore :
        if not('LCA' in temp['enseignementsComplements']):
            commandLine_admin = (
                'SELECT * FROM lsu '
                'WHERE lsuWhat="matiere" AND lsu2 LIKE "LCA%"')
            query_admin = utils_db.queryExecute(
                commandLine_admin, query=query_admin)
            while query_admin.next():
                replaceWhat = query_admin.value(1)
                if not('LCA' in temp['enseignementsComplements']):
                    temp['enseignementsComplements']['LCA'] = {}
                temp['matiere2EnseignementComplement'][replaceWhat] = 'LCA'
        enseignementComplementForIn = utils_functions.array2string(
            temp['matiere2EnseignementComplement'], 
            text=True)
        # on vérifie s'il y a validation du positionnement :
        commandLine_recupEvals = (
            'SELECT * FROM persos '
            'WHERE Matiere IN ({0}){1}').format(enseignementComplementForIn, commandLineEnd)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_eleve = int(query_recupEvals.value(1))
            value = query_recupEvals.value(5)
            matiere = query_recupEvals.value(7)
            ensComp = temp['matiere2EnseignementComplement'].get(matiere, '')
            if len(ensComp) > 0:
                if not(id_eleve in temp['enseignementsComplements'][ensComp]):
                    temp['enseignementsComplements'][ensComp][id_eleve] = ''
                if value in value2pos:
                    temp['enseignementsComplements'][ensComp][id_eleve] += value
        # on vérifie aussi s'il y a validation du positionnement :
        commandLine_recupEvals = (
            'SELECT * FROM lsu '
            'WHERE lsuWhat="positionnement" '
            'AND lsu2 IN ({0}){1}').format(enseignementComplementForIn, commandLineLsuEnd)
        query_recupEvals = utils_db.queryExecute(commandLine_recupEvals, query=query_recupEvals)
        while query_recupEvals.next():
            id_eleve = int(query_recupEvals.value(4))
            value = query_recupEvals.value(5)
            matiere = query_recupEvals.value(3)
            ensComp = temp['matiere2EnseignementComplement'].get(matiere, '')
            if len(ensComp) > 0:
                if value in value2pos:
                    temp['enseignementsComplements'][ensComp][id_eleve] = value
        for ensComp in temp['enseignementsComplements']:
            for id_eleve in temp['enseignementsComplements'][ensComp]:
                utils_functions.appendUnique(temp['STUDENTS'], id_eleve)
                value = utils_calculs.moyenneBilan(
                    temp['enseignementsComplements'][ensComp][id_eleve])
                pos = value2pos.get(value, 1)
                if pos > 2:
                    temp['enseignementsComplements'][ensComp][id_eleve] = 2
                else:
                    temp['enseignementsComplements'][ensComp][id_eleve] = 1
        # on récupère les classes et les cycles des élèves :
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            if (-1 in idsEleves) or (id_eleve in idsEleves):
                classe = query_admin.value(4)
                temp['eleve2classe'][id_eleve] = classe
                # on ne prend que les classes de 3° :
                if classe[0] == '3':
                    temp['classe2cycle'][classe] = 4
        # un seul ensComp possible par élève 
        # (on garde au hasard une des meilleures valeurs) :
        for id_eleve in temp['STUDENTS']:
            # try except pour écarter les élèves partis mais évalués quand même
            try:
                classe = temp['eleve2classe'][id_eleve]
                cycle = temp['classe2cycle'].get(classe, 3)
                if cycle == 4:
                    enseignementComplement = 'AUC'
                    pos = -1
                    for ensComp in temp['enseignementsComplements']:
                        if id_eleve in temp['enseignementsComplements'][ensComp]:
                            newPos = temp['enseignementsComplements'][ensComp][id_eleve]
                            if newPos > pos:
                                enseignementComplement = ensComp
                                pos = newPos
                    ensCompData[id_eleve] = (enseignementComplement, pos)
            except:
                continue
        #print('ensCompData :', ensCompData)

        # récupération des évaluations :
        bilansNames = utils_functions.array2string(socle2socle, text=True)
        commandLineBase = (
            'SELECT * FROM {0} '
            'WHERE bilan_name IN ({1}){2}')
        commandLine = commandLineBase.format(
            'bilans_propositions', bilansNames, commandLineEnd)
        query_propositions = utils_db.queryExecute(commandLine, query=query_propositions)
        while query_propositions.next():
            id_eleve = int(query_propositions.value(0))
            bilan_name = query_propositions.value(2)
            value = query_propositions.value(3)
            if not (id_eleve in socleData):
                socleData[id_eleve] = {}
            socleData[id_eleve][socle2socle[bilan_name]] = value
        commandLine = commandLineBase.format(
            'bilans_validations', bilansNames, commandLineEnd)
        query_validations = utils_db.queryExecute(commandLine, query=query_validations)
        while query_validations.next():
            id_eleve = int(query_validations.value(0))
            bilan_name = query_validations.value(2)
            value = query_validations.value(5)
            if not (id_eleve in socleData):
                socleData[id_eleve] = {}
            socleData[id_eleve][socle2socle[bilan_name]] = value

        # on écrit les enregistrements dans resultats :
        if (-1 in idsEleves):
            # on supprime entièrement la table :
            query_resultats = utils_db.queryExecute(
                utils_db.qdf_table.format('lsu'), query=query_resultats)
        else:
            # on efface les anciens enregistrements :
            commandLine = utils_db.qdf_where.format(
                'lsu', 'id_eleve', studentsForIn)
            query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
        lines = []
        for id_eleve in socleData:
            for bilan_name in socleData[id_eleve]:
                value = socleData[id_eleve][bilan_name]
                if value != '':
                    utils_functions.appendUnique(
                        lines, 
                        ('socle', id_eleve, bilan_name, value, '', ''))
        for id_eleve in ensCompData:
            (ensComp, value) = ensCompData[id_eleve]
            utils_functions.appendUnique(
                lines, 
                ('ensComp', id_eleve, ensComp, value, '', ''))
        commandLine_resultats = utils_db.insertInto('lsu', 6)
        query_resultats = utils_db.queryExecute(
            {commandLine_resultats: lines}, query=query_resultats)

        reponse = True
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(admin.dbFile_resultats)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_db.endTransaction(
            query_resultats, admin.db_resultats, transactionOK_resultats)
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_resultats, admin.dbFile_resultats)
        utils_functions.restoreCursor()
        utils_functions.afficheStatusBar(main)
        if msgFin:
            fin = QtCore.QTime.currentTime()
            utils_functions.afficheDuree(main, debut, fin)
            utils_functions.afficheMsgFin(main, timer=5)
        return reponse

def createReportFile(main):
    global REPORT_FILENAME
    dayOfWeek = QtCore.QDate.currentDate().dayOfWeek()
    REPORT_FILENAME = 'rapport-{0}.log'.format(dayOfWeek)
    theFileName = utils_functions.u('{0}/protected/{1}').format(
        admin.dirLocalPrive, REPORT_FILENAME)
    utils.changeLogFile(True)
    utils.LOGFILE.flush()
    utils_filesdirs.removeAndCopy(utils.LOGFILENAME, theFileName)

def uploadDBResultats(main, what='all', msgFin=True):
    """
    what :
        resultats
        referential_propositions
        all : les 2
    """
    dirSite = admin.dirSitePrive + '/protected'
    dirLocal = admin.dirLocalPrive + '/protected'
    theFileName1 = 'resultats.sqlite'
    theFileName2 = 'referential_propositions.sqlite'
    debut = QtCore.QTime.currentTime()

    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Upload the database'), 
        tags=('h2'))

    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            # on envoie les fichiers :
            if what in ('all', 'resultats'):
                reponse1 = admin_ftp.ftpPutFile(main, dirSite, dirLocal, theFileName1)
            else:
                reponse1 = True
            if what in ('all', 'referential_propositions'):
                reponse2 = admin_ftp.ftpPutFile(main, dirSite, dirLocal, theFileName2)
            else:
                reponse2 = True
            if REPORT_FILENAME != '':
                admin_ftp.ftpPutFile(main, dirSite, dirLocal, REPORT_FILENAME)
            reponse = reponse1 and reponse2
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                if msgFin:
                    fin = QtCore.QTime.currentTime()
                    utils_functions.afficheDuree(main, debut, fin)
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                message = QtWidgets.QApplication.translate(
                    'main', 'Upload of the resultats DB failed')
                utils_functions.afficheMsgPb(main, message)
            return reponse

def checkSchoolReports(main):
    """
    Vérification des matières qui sont présentes sur les bulletins.
    Permet de lancer une vérification de l'état des fichiers
    envoyés par les profs.
    Utile pour l'admin pour détecter un problème éventuel.
    """
    utils_functions.doWaitCursor()
    try:
        # ouverture des bases et query :
        admin.openDB(main, 'resultats')
        query_resultats = utils_db.query(admin.db_resultats)

        # début de l'affichage :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'CheckSchoolReportsStatusTip'), 
            tags=('h2'))

        noNotes = QtWidgets.QApplication.translate(
            'main', 'NO NOTES')
        noPersos = QtWidgets.QApplication.translate(
            'main', 'NO PERSONAL BALANCE')
        classes = []
        bulletins = {}
        bulletinsNotes = {}
        # on récupère les noms des profs :
        profs = {}
        for (id_prof, prof_file, prof_nom, prof_prenom) in admin.PROFS:
            profs[id_prof] = utils_functions.u(
                '{0} {1}').format(prof_nom, prof_prenom)

        # recherche des matières qui ont des bilans persos sur le bulletin :
        commandLine_resultats = (
            'SELECT DISTINCT eleves.Classe, persos.id_prof, persos.name '
            'FROM persos '
            'JOIN eleves ON eleves.id_eleve=persos.id_eleve '
            'ORDER BY eleves.Classe, persos.name')
        query_resultats = utils_db.queryExecute(commandLine_resultats, query=query_resultats)
        while query_resultats.next():
            classe = query_resultats.value(0)
            id_prof = int(query_resultats.value(1))
            prof = profs[id_prof]
            name = query_resultats.value(2)
            if len(name.split('-')) > 1:
                matiereCode = name.split('-')[0]
            else:
                matiereCode = name[:-1]
            if not(classe in bulletins):
                classes.append(classe)
                bulletins[classe] = [(matiereCode, prof)]
            else:
                if not((matiereCode, prof) in bulletins[classe]):
                    bulletins[classe].append((matiereCode, prof))
            # pour être sûr que la classe est aussi dans bulletinsNotes :
            bulletinsNotes[classe] = []
        # recherche des notes pour les classes à notes :
        commandLine_resultats = (
            'SELECT DISTINCT eleves.Classe, notes.id_prof, notes.matiere '
            'FROM notes '
            'JOIN eleves ON eleves.id_eleve=notes.id_eleve '
            'ORDER BY eleves.Classe, notes.matiere')
        query_resultats = utils_db.queryExecute(commandLine_resultats, query=query_resultats)
        while query_resultats.next():
            classe = query_resultats.value(0)
            id_prof = int(query_resultats.value(1))
            prof = profs[id_prof]
            matiereCode = query_resultats.value(2)
            # on ne s'occupe que des classes à notes :
            if classe in admin.classesNotes:
                if not(classe in bulletinsNotes):
                    if not(classe in classes):
                        classes.append(classe)
                    bulletinsNotes[classe] = [(matiereCode, prof)]
                else:
                    if not((matiereCode, prof) in bulletinsNotes[classe]):
                        bulletinsNotes[classe].append((matiereCode, prof))
                # pour être sûr que la classe est aussi dans bulletins :
                if not(classe in bulletins):
                    bulletins[classe] = []
        # affichage des résultats :
        messages = []
        classes.sort()
        for classe in classes:
            messages.append(utils_functions.u('<br/><b>{0} :</b>').format(classe))
            if classe in bulletins:
                bulletins[classe].sort()
                if classe in admin.classesNotes:
                    # il y a plus de cas à gérer :
                    for (matiereCode, prof) in bulletins[classe]:
                        if (matiereCode, prof) in bulletinsNotes[classe]:
                            message = utils_functions.u(
                                '{0} ({1})').format(matiereCode, prof)
                        else:
                            """
                            message = utils_functions.u(
                                '{0} ({1}) <b>{2}</b>').format(matiereCode, prof, noNotes)
                            """
                            message = utils_functions.u(
                                '{0} ({1})').format(matiereCode, prof)
                        messages.append(message)
                    # on cherche enfin les profs qui n'auraient mis que des notes :
                    for (matiereCode, prof) in bulletinsNotes[classe]:
                        if not((matiereCode, prof) in bulletins[classe]):
                            message = utils_functions.u(
                                '{0} ({1}) <b>{2}</b>').format(matiereCode, prof, noPersos)
                            messages.append(message)
                else:
                    for (matiereCode, prof) in bulletins[classe]:
                        message = utils_functions.u('{0} ({1})').format(matiereCode, prof)
                        messages.append(message)
        utils_functions.afficheMessage(main, messages, editLog=True, p=True)

    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()

def doLastReport(main):
    """
    Efface le contenu du editLog,
    et charge le dernier fichier rapport.txt.
    """
    main.editLog.clear()
    reportFileName = admin.adminDir + "/rapport.txt"
    inFile = QtCore.QFile(reportFileName)
    if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(inFile)
        stream.setCodec('UTF-8')
        main.editLog.setPlainText(stream.readAll())
        inFile.close()











def doClearReferential(main):
    """
    lance le dialog de nettoyage du référentiel.
    """

    dialog = ClearReferentialWizard(parent=main)
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        result = True
    else:
        main.enableInterface(dialog, lastState)
    main.editLog2 = None



class ClearReferentialWizard(QtWidgets.QDialog):
    """
    le Wizard de nettoyage du référentiel.
    On crée tous les widgets nécessaires mais ils seront visibles selon la page.
    Pages :
        0 : explications, sélection de ce qui sera effacé et des élèves
        1 : actions
    """
    def __init__(self, parent=None):
        super(ClearReferentialWizard, self).__init__(parent)
        self.main = parent
        # page actuelle et navigation :
        self.page = {'back': -1, 'actual': 0, 'next': 1, 'final': 1}
        # titre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Clear the referential'))
        self.helpContextPage = 'clear-referential'

        # les données :
        self.data = {
            'what': 'socle', 
            'who': 'classes', 
            'classes': [], 
            'idsEleves': [], 
            }

        # affichage des pages :
        self.pagesWidget = QtWidgets.QStackedWidget()
        self.pagesWidget.addWidget(WhatAndWhoPage(self))
        self.pagesWidget.addWidget(DoStuffPage(self))

        # des boutons :
        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('back', 'next', 'close', 'help'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]

        # on agence tout ça :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.pagesWidget)
        layout.addLayout(buttonsLayout)
        self.setLayout(layout)

        # on appelle la première page :
        self.reInit()
        self.loadPage()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, 'admin')

    def reInit(self):
        """
        initialisation de quelques valeurs de data.
        Lues dans la base admin pour la pluspart.
        """

    def doFinish(self):
        self.accept()

    def doBack(self):
        self.pagesWidget.widget(self.page['actual']).doFinish()
        self.page['next'] = self.page['actual']
        self.page['actual'] = self.page['back']
        self.page['back'] -= 1
        if self.page['back'] < 0:
            self.page['back'] = 0
        self.loadPage()

    def doNext(self):
        self.pagesWidget.widget(self.page['actual']).doFinish()
        self.page['back'] = self.page['actual']
        self.page['actual'] = self.page['next']
        self.page['next'] += 1
        if self.page['next'] > self.page['final']:
            self.page['next'] = self.page['final']
        self.loadPage()

    def loadPage(self):
        """
        appel et mise en place d'une page du wizard.
        On commence par vérifier ce qui doit être affiché et quels boutons sont actifs.
        Ensuite on remplit les données liées à la page.
        """
        utils_functions.doWaitCursor()
        try:
            # widgets visibles et boutons disponibles selon la page :
            self.buttonsList['back'].setEnabled(self.page['actual'] > 0)
            self.buttonsList['next'].setEnabled(self.page['actual'] < self.page['final'])
            self.pagesWidget.widget(self.page['actual']).reInit()
            self.pagesWidget.setCurrentIndex(self.page['actual'])
        finally:
            utils_functions.restoreCursor()



class WhatAndWhoPage(QtWidgets.QWidget):
    """
    qui est concerné (who) : classes ou élèves
    qu'est-ce qu'on efface (what) :
        * 
    """
    def __init__(self, parent=None):
        super(WhatAndWhoPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = QtWidgets.QApplication.translate(
            'main', 'Selection of cleaning type and students')
        title = utils_functions.u(
            '<h3>{0} 1/2 - {1}</h3>').format(STEP_TEXT, title)
        titleLabel = QtWidgets.QLabel(title)

        # Aide :
        mdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpClearReferential')
        h = int(mdHelpView.height() * 1.5)
        mdHelpView.setMinimumHeight(h)
        mdHelpView.setMaximumHeight(h)

        # what - qu'est-ce qu'on efface :
        text = QtWidgets.QApplication.translate(
            'main', 'Type of cleaning')
        whatGroup = QtWidgets.QGroupBox(text)
        whatList = {
            'ORDER': ('socle', 'referential', 'socleAll', 'referentialAll', ), 
            'socle': QtWidgets.QApplication.translate(
                'main', 'socle only - previous years'), 
            'referential': QtWidgets.QApplication.translate(
                'main', 'all referential - previous years'), 
            'socleAll': QtWidgets.QApplication.translate(
                'main', 'socle only - all years'), 
            'referentialAll': QtWidgets.QApplication.translate(
                'main', 'all referential - all years'), 
            }
        grid = QtWidgets.QVBoxLayout()
        self.whatRadioButtons = {}
        for what in whatList['ORDER']:
            radioButton = QtWidgets.QRadioButton(whatList[what])
            self.whatRadioButtons[what] = radioButton
            grid.addWidget(radioButton)
        grid.addStretch()
        whatGroup.setLayout(grid)
        self.whatRadioButtons[self.parent.data['what']].setChecked(True)

        # who - type de sélection (classes ou élève) :
        whoGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Selection (classes or students)'))
        classesText = QtWidgets.QApplication.translate(
            'main', 'Classes')
        studentsText = QtWidgets.QApplication.translate(
            'main', 'Students')
        self.classesRadioButton = QtWidgets.QRadioButton(classesText)
        self.classesRadioButton.setChecked(True)
        self.studentsRadioButton = QtWidgets.QRadioButton(studentsText)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget(self.classesRadioButton)
        hBox.addWidget(self.studentsRadioButton)
        whoGroupBox.setLayout(hBox)

        # Classes :
        self.classesGroup = QtWidgets.QGroupBox()
        helpMessage = QtWidgets.QApplication.translate(
            'main', 
            'Check the classes to be processed')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpMessage))
        self.classesListWidget = QtWidgets.QListWidget()
        grid = QtWidgets.QGridLayout()
        grid.addWidget(helpLabel, 1, 0)
        grid.addWidget(self.classesListWidget, 2, 0)
        self.classesGroup.setLayout(grid)
        self.classesGroup.setVisible(True)

        # Élèves :
        self.studentsGroup = QtWidgets.QGroupBox()
        helpMessage = QtWidgets.QApplication.translate(
            'main', 
            'To select all, you can leave the empty right list.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpMessage))
        self.selectionWidget = utils_2lists.ChooseStudentsWidget(
            parent = self.main, withComboBox = True)
        grid = QtWidgets.QGridLayout()
        grid.addWidget(helpLabel, 1, 0)
        grid.addWidget(self.selectionWidget, 2, 0)
        self.studentsGroup.setLayout(grid)
        self.studentsGroup.setVisible(False)

        # on agence tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)
        mainLayout.addWidget(mdHelpView)

        leftLayout = QtWidgets.QVBoxLayout()
        leftLayout.addWidget(whatGroup)
        whatGroup.setMinimumWidth(self.main.width() // 3)

        rightLayout = QtWidgets.QVBoxLayout()
        rightLayout.addWidget(whoGroupBox)
        rightLayout.addWidget(self.classesGroup)
        rightLayout.addWidget(self.studentsGroup)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addLayout(leftLayout)
        hLayout.addLayout(rightLayout)
        mainLayout.addLayout(hLayout)
        self.setLayout(mainLayout)

        # on remplit la liste des classes
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            item = QtWidgets.QListWidgetItem(
                query_commun.value(1))
            item.setCheckState(QtCore.Qt.Unchecked)
            self.classesListWidget.addItem(item)
        # mise en place des connexions :
        self.classesRadioButton.toggled.connect(self.doChanged)
        for what in self.whatRadioButtons:
            self.whatRadioButtons[what].toggled.connect(self.doChanged)
        self.reInit()

    def reInit(self):
        """
        appelé chaque fois qu'on affiche la page
        """

    def doFinish(self):
        """
        mise à jour du dico data
        """
        # sélection des classes à traiter :
        self.parent.data['classes'] = []
        for index in range(self.classesListWidget.count()):
            item = self.classesListWidget.item(index)
            if item.checkState() == QtCore.Qt.Checked:
                self.parent.data['classes'].append(item.text())
        # sélection des élèves à traiter :
        self.parent.data['idsEleves'] = []
        for i in range(self.selectionWidget.selectionList.count()):
            item = self.selectionWidget.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            self.parent.data['idsEleves'].append(id_eleve)
        if len(self.parent.data['idsEleves']) < 1:
            for i in range(self.selectionWidget.baseList.count()):
                item = self.selectionWidget.baseList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                self.parent.data['idsEleves'].append(id_eleve)

    def doChanged(self, first=None, second=None):
        """
        un champ a été modifié.
        On met data à jour.
        """
        for what in self.whatRadioButtons:
            if self.sender() == self.whatRadioButtons[what]:
                # first : checked
                if first:
                    self.parent.data['what'] = what
                return
        if self.sender() == self.classesRadioButton:
            # first : checked
            if first:
                self.parent.data['who'] = 'classes'
                self.studentsGroup.setVisible(False)
                self.classesGroup.setVisible(True)
            else:
                self.parent.data['who'] = 'students'
                self.classesGroup.setVisible(False)
                self.studentsGroup.setVisible(True)



class DoStuffPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(DoStuffPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = QtWidgets.QApplication.translate(
            'main', 'Cleaning and posting')
        title = utils_functions.u(
            '<h3>{0} 2/2 - {1}</h3>').format(STEP_TEXT, title)
        titleLabel = QtWidgets.QLabel(title)

        # les boutons (création et ouverture du dossier) :
        self.launchStuff_Button = QtWidgets.QToolButton()
        self.postDB_Button = QtWidgets.QToolButton()
        # mise en forme des boutons :
        buttons = {
            self.launchStuff_Button: (
                'clear', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Launch the procedure'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Launch the referential cleanup procedure'), 
                self.launchStuff), 
            self.postDB_Button: (
                'net-upload', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Post the DB'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Post the referential DB to your website'), 
                self.postDB), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize']
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE * 2, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])
        # première série d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.launchStuff_Button)
        hLayout0.addWidget(self.postDB_Button)
        hLayout0.addStretch(1)

        # EditLog :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)

        # on agence tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)

        leftLayout = QtWidgets.QVBoxLayout()
        leftLayout.addLayout(hLayout0)
        leftLayout.addStretch(1)

        rightLayout = QtWidgets.QVBoxLayout()
        rightLayout.addLayout(messagesLayout)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addLayout(leftLayout)
        hLayout.addLayout(rightLayout)
        mainLayout.addLayout(hLayout)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):
        """
        appelé chaque fois qu'on affiche la page
        """
        #print(self.parent.data)

    def doFinish(self):
        """
        juste pour compatibilité
        """

    def launchStuff(self):
        """
        procédure de nettoyage
            * on fabrique la ligne de commande en fonction de la sélection
            * on efface
            * on recalcule le référentiel.
        on nettoie aussi la base des validations
            pour le socle, on nettoie pour tous les élèves
            permet d'effacer les validations des 3°
        """
        idsEleves = []
        if self.parent.data['who'] == 'students':
            for id_eleve in self.parent.data['idsEleves']:
                idsEleves.append(id_eleve)
        else:
            # on récupère les élèves d'après les classes :
            classesNames = utils_functions.array2string(
                self.parent.data['classes'], text=True)
            query_admin = utils_db.query(admin.db_admin)
            commandLine = utils_functions.u(
                'SELECT * FROM eleves WHERE Classe IN ({0})').format(classesNames)
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                idsEleves.append(id_eleve)
        id_eleves = utils_functions.array2string(idsEleves)

        what = self.parent.data['what']
        if what == 'socle':
            commandLine_propositions = (
                'DELETE FROM bilans_details '
                'WHERE annee_scolaire != "{0}" '
                'AND bilan_name LIKE "S2%" '
                'AND id_eleve IN ({1})')
            commandLine_validations = (
                'DELETE FROM bilans_validations '
                'WHERE date LIKE "%{0}" '
                'AND bilan_name LIKE "S2%"')
        elif what == 'referential':
            commandLine_propositions = (
                'DELETE FROM bilans_details '
                'WHERE annee_scolaire != "{0}" '
                'AND id_eleve IN ({1})')
            commandLine_validations = (
                'DELETE FROM bilans_validations '
                'WHERE date LIKE "%{0}" '
                'AND id_eleve IN ({1})')
        elif what == 'socleAll':
            commandLine_propositions = (
                'DELETE FROM bilans_details '
                'WHERE bilan_name LIKE "S2%" '
                'AND id_eleve IN ({1})')
            commandLine_validations = (
                'DELETE FROM bilans_validations '
                'WHERE bilan_name LIKE "S2%"')
        elif what == 'referentialAll':
            commandLine_propositions = (
                'DELETE FROM bilans_details '
                'WHERE id_eleve IN ({1})')
            commandLine_validations = (
                'DELETE FROM bilans_validations '
                'WHERE id_eleve IN ({1})')
        else:
            return
        commandLine_propositions = commandLine_propositions.format(
            self.main.annee_scolaire[0], id_eleves)
        commandLine_validations = commandLine_validations.format(
            self.main.annee_scolaire[0] - 1, id_eleves)
        utils_functions.afficheMessage(
            self.main, 
            QtWidgets.QApplication.translate('main', 'Clear the referential'), 
            tags=('h2'))
        utils_functions.afficheMessage(
            self.main, commandLine_propositions, editLog=True, p=True)
        utils_functions.afficheMessage(
            self.main, commandLine_validations, editLog=True, p=True)

        try:
            # on télécharge la base referential_validations :
            admin.downloadDB(self.main, db='referential_validations', msgFin=False)
            # ouverture des bases et transactions :
            admin.openDB(self.main, 'referential_propositions')
            query_propositions, transactionOK_propositions = utils_db.queryTransactionDB(
                admin.db_referentialPropositions)
            query_propositions = utils_db.queryExecute(
                commandLine_propositions, query=query_propositions)
            admin.openDB(self.main, 'referential_validations')
            query_validations, transactionOK_validations = utils_db.queryTransactionDB(
                admin.db_referentialValidations)
            query_validations = utils_db.queryExecute(
                commandLine_validations, query=query_validations)
        except:
            utils_functions.afficheMsgPb(self.main)
        finally:
            utils_db.endTransaction(
                query_propositions, 
                admin.db_referentialPropositions, 
                transactionOK_propositions)
            utils_filesdirs.removeAndCopy(
                admin.dbFileTemp_referentialPropositions, 
                admin.dbFile_referentialPropositions)
            utils_db.endTransaction(
                query_validations, 
                admin.db_referentialValidations, 
                transactionOK_validations)
            utils_filesdirs.removeAndCopy(
                admin.dbFileTemp_referentialValidations, 
                admin.dbFile_referentialValidations)

        # envoi de la base referential_validations :
        admin.uploadDB(self.main, db='referential_validations', msgFin=False)
        # recalcul des propositions :
        calculateReferentialPropositions(
            self.main, idsEleves=idsEleves, msgFin=False)

    def postDB(self):
        """
        envoi de la base referential_propositions.sqlite
        """
        uploadDBResultats(self.main, what='referential_propositions')




