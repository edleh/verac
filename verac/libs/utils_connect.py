# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    La fenêtre de connexion.
    Plus quelques trucs liés : changement de mot de passe, ...
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_web, utils_filesdirs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui




"""
###########################################################
###########################################################

                Connexion

###########################################################
###########################################################
"""


class DropButton(QtWidgets.QPushButton):
    """
    un QPushButton permettant d'ouvrir un fichier
    par drag-drop
    """
    def __init__(self, icon=None, text='', parent=None, extension='sqlite'):
        super(DropButton, self).__init__(icon, text, parent)
        self.parent = parent
        self.extension = extension
        self.fileName = ''

        self.setAcceptDrops(True)

    def dragEnterEvent(self, event):
        event.acceptProposedAction()

    def dragMoveEvent(self, event):
        event.acceptProposedAction()

    def dropEvent(self, event):
        self.fileName = ''
        mimeData = event.mimeData()
        if mimeData.hasUrls():
            #print('hasUrls:', mimeData.urls())
            if len(mimeData.urls()) == 1:
                fileName = mimeData.urls()[0].toLocalFile()
                if self.extension == '':
                    self.fileName = fileName
                else:
                    # on vérifie l'extension :
                    extension = QtCore.QFileInfo(fileName).suffix()
                    if extension.lower() == self.extension:
                        self.fileName = fileName
        event.acceptProposedAction()
        if self.fileName != '':
            self.doAfterDrop()

    def doAfterDrop(self):
        """
        si self.parent a une fonction doAfterDrop 
        pour traiter l'après drag-drop, on l'appelle.
        """
        try:
            self.parent.doAfterDrop()
        except:
            pass

    def dragLeaveEvent(self, event):
        event.accept()



class ConnectDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, user='', mdp='', adminOk=False):
        super(ConnectDlg, self).__init__(parent)

        self.main = parent
        self.adminOk = adminOk
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Connection'))
        self.setWindowFlags(QtCore.Qt.Dialog)

        # le type de version :
        versionLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Version:')))
        self.versionComboBox = QtWidgets.QComboBox()
        self.versionComboBox.setToolTip(
            QtWidgets.QApplication.translate('main', 'Select the version'))
        self.versionComboBox.activated.connect(self.versionChanged)

        # pour ajouter un établissement par adresse web :
        self.urlEtabLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Url:')))
        self.urlEtabEdit = QtWidgets.QLineEdit('http://adresse/verac')
        self.urlEtabEdit.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Enter the web address of the school'))

        # login et mdp :
        self.loginLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Login:')))
        self.loginComboBox = QtWidgets.QComboBox()
        self.loginComboBox.activated.connect(self.loginChanged)
        self.mdpLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Password:')))
        self.mdpEdit = QtWidgets.QLineEdit()
        self.mdpEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.saveCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'Remember User'))

        # le bouton de connexion :
        self.connectButton = QtWidgets.QPushButton(
            utils.doIcon('network-connect'), 
            QtWidgets.QApplication.translate('main', 'Connect'))
        self.connectButton.clicked.connect(self.doConnect)
        # le bouton annuler :
        cancelButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-cancel'), 
            QtWidgets.QApplication.translate('main', 'Abort'))
        cancelButton.clicked.connect(self.close)
        # le bouton aide :
        helpButton = QtWidgets.QPushButton(
            utils.doIcon('help'), 
            QtWidgets.QApplication.translate('main', 'Help'))
        helpButton.setShortcut(
            QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        helpButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Opens contextual help in your browser'))
        helpButton.clicked.connect(self.contextHelp)
        # le bouton des actions supplémentaires :
        moreButton = QtWidgets.QPushButton(
            utils.doIcon('list-add'), 
            QtWidgets.QApplication.translate('main', 'Other actions'))
        moreButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Show/Hide other actions available'))
        moreButton.clicked.connect(self.moreActions)
        # une boîte des boutons :
        buttonBox = QtWidgets.QDialogButtonBox()
        buttonBox.addButton(
            self.connectButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(cancelButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(helpButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(moreButton, QtWidgets.QDialogButtonBox.ActionRole)

        # le bouton de connexion admin :
        self.adminButton = QtWidgets.QPushButton(
            utils.doIcon('applications-system'), 
            QtWidgets.QApplication.translate('main', 'Administrator'))
        self.adminButton.clicked.connect(self.doAdmin)

        # cases à cocher :
        self.showMdpCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'View password'))
        self.showMdpCheckBox.clicked.connect(self.doShowMdp)
        self.noNetCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'No internet'))
        self.debugCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'Debugging'))

        # le bouton de suppression de l'utilisateur :
        self.userRemoveButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'), 
            QtWidgets.QApplication.translate('main', 'Remove the user'))
        self.userRemoveButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Remove the selected user from the comboBox'))
        self.userRemoveButton.clicked.connect(self.removeUser)

        openDirText = QtWidgets.QApplication.translate('main', 'Open')
        openDirToolTip = QtWidgets.QApplication.translate('main', 'Open folder')
        # dossier des fichiers profs :
        filesDirTitle = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Files folder:')))
        self.filesDirLabel = QtWidgets.QLabel()
        openFilesDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'), openDirText)
        openFilesDirButton.setToolTip(openDirToolTip)
        openFilesDirButton.clicked.connect(self.doOpenFilesDir)
        changeWorkDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-development'), 
            QtWidgets.QApplication.translate('main', 'Change'))
        changeWorkDirButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Select another folder'))
        changeWorkDirButton.clicked.connect(self.doChangeWorkDir)
        # dossier de config :
        configDirTitle = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate(
                    'main', 'Configuration folder:')))
        configDirLabel = QtWidgets.QLabel(self.main.localConfigDir)
        openConfigDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'), openDirText)
        openConfigDirButton.setToolTip(openDirToolTip)
        openConfigDirButton.clicked.connect(self.doOpenConfigDir)

        # le bouton de suppression de l'établissement :
        self.schoolDeleteButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'), 
            QtWidgets.QApplication.translate('main', 'Remove the selected school'))
        self.schoolDeleteButton.clicked.connect(self.deleteSchool)

        # ouvrir le fichier d'un collègue (un DropButton) :
        self.openOtherFileButton = DropButton(
            utils.doIcon('database-prof'), 
            QtWidgets.QApplication.translate('main', 'Open other teacher file'), 
            self)
        self.openOtherFileButton.clicked.connect(self.openOtherFile)

        # on groupe les actions supplémentaires :
        moreVBox = QtWidgets.QVBoxLayout()
        moreVBox.addWidget(QtWidgets.QLabel('<hr width="80%">'))
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget(self.showMdpCheckBox)
        hBox.addWidget(self.userRemoveButton)
        moreVBox.addLayout(hBox)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget(self.noNetCheckBox)
        hBox.addWidget(self.debugCheckBox)
        moreVBox.addLayout(hBox)
        moreVBox.addWidget(self.schoolDeleteButton)
        if utils.lanFilesDir == '':
            hBox = QtWidgets.QHBoxLayout()
            hBox.addWidget(filesDirTitle)
            hBox.addWidget(self.filesDirLabel)
            moreVBox.addLayout(hBox)
            hBox = QtWidgets.QHBoxLayout()
            hBox.addStretch(1)
            hBox.addWidget(openFilesDirButton)
            hBox.addWidget(changeWorkDirButton)
            moreVBox.addLayout(hBox)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget(configDirTitle)
        hBox.addWidget(configDirLabel)
        moreVBox.addLayout(hBox)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addStretch(1)
        hBox.addWidget(openConfigDirButton)
        moreVBox.addLayout(hBox)
        moreVBox.addWidget(self.openOtherFileButton)
        self.moreGroupBox = QtWidgets.QGroupBox()
        self.moreGroupBox.setFlat(True)
        self.moreGroupBox.setLayout(moreVBox)
        self.moreGroupBox.setVisible(False)

        # on place tout dans une grille:
        grid = QtWidgets.QGridLayout()
        grid.addWidget(versionLabel,            1, 0)
        grid.addWidget(self.versionComboBox,    1, 1, 1, 3)
        grid.addWidget(self.urlEtabLabel,       10, 0)
        grid.addWidget(self.urlEtabEdit,        10, 1, 1, 3)
        grid.addWidget(self.loginLabel,         11, 0)
        grid.addWidget(self.loginComboBox,      11, 1, 1, 3)
        grid.addWidget(self.mdpLabel,           12, 0)
        grid.addWidget(self.mdpEdit,            12, 1, 1, 3)
        grid.addWidget(self.saveCheckBox,       13, 1)
        grid.addWidget(buttonBox,               15, 0, 1, 4)
        grid.addWidget(self.adminButton,        16, 0, 1, 4)
        grid.addWidget(self.moreGroupBox,       18, 0, 1, 4)
        # la grille est le widget de base:
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # les comptes de la version démo :
        self.demoUsers = {
            'LIST': (
                'demo.prof1', 'demo.prof2', 'demo.prof3', 
                'demo.pp1', 'demo.pp2', 
                'demo.cpe', 
                'chef.etab'), 
            'demo.prof1': 'prof1', 'demo.prof2': 'prof2', 'demo.prof3': 'prof3', 
            'demo.pp1': 'pp1', 'demo.pp2': 'pp2', 
            'demo.cpe': 'cpe', 
            'chef.etab': 'etab'}

        self.newEtabFileName = ''

        try:
            value_text = utils_db.readInConfigDB(self.main, 'FilesDir')[1]
            if value_text == '':
                value_text = utils_db.readInConfigDB(self.main, 'WorkDir')[1]
            if value_text == '':
                value_text = QtCore.QDir.homePath()
            self.filesDirLabel.setText(value_text)
        except:
            self.filesDirLabel.setText(QtCore.QDir.homePath())

        # remplissage du comboBox Version :
        self.recreateVersionComboBox()
        self.versionChanged()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('connect-dialog')

    def moreActions(self):
        """
        affiche/masque les actions supplémentaires
        """
        visibility = not(self.moreGroupBox.isVisible())
        self.moreGroupBox.setVisible(visibility)
        if visibility:
            self.setMaximumHeight(1000)
        else:
            self.setMaximumHeight(1)

    def recreateVersionComboBox(self, newVersionName=''):
        """
        pour remplir la liste des versions
        au début, mais aussi en cas d'ajout ou de suppression
        """
        self.versionComboBox.clear()
        newVersion = True
        if newVersionName == '':
            newVersionName = self.main.actualVersion['versionName']
            newVersion = False
        if utils.lanConfigDir != '':
            theDb = self.main.db_lanConfig
        else:
            theDb = self.main.db_localConfig
        query = utils_db.queryExecute(
            utils_db.q_selectAllFrom.format('versions'), 
            db=theDb)
        while query.next():
            id_version = int(query.value(0))
            versionName = query.value(1)
            versionUrl = query.value(2)
            versionLabel = query.value(3)
            self.versionComboBox.addItem(versionLabel, id_version)
            if versionName == newVersionName:
                self.versionComboBox.setCurrentIndex(
                    self.versionComboBox.count() - 1)
                self.actualVersion = {
                    'id_version': id_version, 
                    'versionName': versionName, 
                    'versionUrl': versionUrl, 
                    'versionLabel': versionLabel}
        self.versionComboBox.addItem(
            '--------------------------------------', 1000)
        self.versionComboBox.addItem(
            QtWidgets.QApplication.translate(
                'main', 'Add a school (internet address)'), 1001)
        self.versionComboBox.addItem(
            QtWidgets.QApplication.translate(
                'main', 'Add a school (file)'), 1002)
        if newVersion:
            self.versionChanged()

    def afficheWidget(self):
        # liste des Widgets pas toujours visibles :
        widgets = {
            'id2list': {0: 'perso', 1: 'demo', 2: 'etab', 999: 'new'},
            'perso': (),
            'demo': (
                self.loginLabel, self.loginComboBox),
            'etab': (
                self.loginLabel, self.loginComboBox, 
                self.mdpLabel, self.mdpEdit, 
                self.saveCheckBox, 
                self.showMdpCheckBox, 
                self.noNetCheckBox, 
                self.userRemoveButton, 
                self.schoolDeleteButton),
            'new': (
                self.urlEtabLabel, self.urlEtabEdit, 
                self.loginLabel, self.loginComboBox, 
                self.mdpLabel, self.mdpEdit, 
                self.saveCheckBox, 
                self.showMdpCheckBox),
            }
        if utils.lanFilesDir != '':
            widgets['etab'] = (
                self.loginLabel, self.loginComboBox, 
                self.mdpLabel, self.mdpEdit, 
                self.saveCheckBox, self.showMdpCheckBox, self.noNetCheckBox)
            self.schoolDeleteButton.setVisible(False)
        id_version = self.actualVersion['id_version']
        if id_version in widgets['id2list']:
            version = widgets['id2list'][id_version]
        else:
            version = widgets['id2list'][2]
        for otherVersion in ('perso', 'demo', 'etab', 'new'):
            for widget in widgets[otherVersion]:
                widget.setVisible(widget in widgets[version])
        self.loginComboBox.setEditable(version in ('etab', 'new'))
        if version == 'demo':
            self.loginComboBox.setToolTip(
                QtWidgets.QApplication.translate('main', 'Select a teacher'))
        elif version == 'etab':
            self.loginComboBox.setToolTip(
                QtWidgets.QApplication.translate(
                    'main', 
                    'Enter your login name <br/>or select it in the comboBox'))
        self.adminButton.setVisible(self.adminOk)
        self.adjustSize()
        if self.adminOk and not(self.saveCheckBox.isChecked()):
            self.connectButton.setShortcut('')
            self.adminButton.setShortcut(
                QtGui.QKeySequence(QtCore.Qt.Key_Return))
        else:
            self.adminButton.setShortcut('')
            self.connectButton.setShortcut(
                QtGui.QKeySequence(QtCore.Qt.Key_Return))

    def versionChanged(self):
        id_version = self.versionComboBox.itemData(
            self.versionComboBox.currentIndex(), QtCore.Qt.UserRole)
        self.loginComboBox.clear()
        self.mdpEdit.setText('')
        if id_version < 1000:
            commandLine = utils_db.q_selectAllFromWhere.format(
                'versions', 'id_version', id_version)
            if utils.lanConfigDir != '':
                query = utils_db.queryExecute(
                    commandLine, db=self.main.db_lanConfig)
            else:
                query = utils_db.queryExecute(
                    commandLine, db=self.main.db_localConfig)
            ok = False
            while query.next():
                versionName = query.value(1)
                versionUrl = query.value(2)
                versionLabel = query.value(3)
                ok = True
            if not(ok):
                query = utils_db.queryExecute(
                    utils_db.q_selectAllFrom.format('versions'), 
                    query=query)
                if query.first():
                    versionName = query.value(1)
                    versionUrl = query.value(2)
                    versionLabel = query.value(3)
            self.actualVersion = {
                'id_version': id_version, 
                'versionName': versionName, 
                'versionUrl': versionUrl, 
                'versionLabel': versionLabel}
            adminDir = utils_db.readInConfigDB(self.main, versionName)[1]
            self.adminOk = (adminDir != '')
            if id_version > 1:
                users = {}
                commandLine = 'SELECT * FROM conn WHERE id_conn={0} ORDER BY save'
                query = utils_db.queryExecute(
                    commandLine.format(id_version), 
                    db=self.main.db_localConfig)
                while query.next():
                    save = int(query.value(1))
                    user = query.value(2)
                    mdp = query.value(3)
                    users[(user, mdp)] = save
                    self.loginComboBox.addItem(user, mdp)
                if len(users) > 0:
                    self.loginComboBox.setCurrentIndex(0)
                    self.saveCheckBox.setChecked(True)
                    mdp = self.loginComboBox.itemData(
                        self.loginComboBox.currentIndex(), QtCore.Qt.UserRole)
                    self.mdpEdit.setText(mdp)
                else:
                    self.saveCheckBox.setChecked(False)
            else:
                for user in self.demoUsers['LIST']:
                    self.loginComboBox.addItem(user, self.demoUsers[user])
            self.afficheWidget()
        elif id_version == 1000:
            self.versionComboBox.setCurrentIndex(0)
        elif id_version == 1001:
            self.actualVersion = {
                'id_version': 999, 
                'versionName': '', 
                'versionUrl': '', 
                'versionLabel': ''}
            self.afficheWidget()
        elif id_version == 1002:
            self.addEtabViaFile()

    def loginChanged(self):
        id_version = self.actualVersion['id_version']
        if id_version > 999:
            return
        if id_version < 2:
            return
        mdp = self.loginComboBox.itemData(
            self.loginComboBox.currentIndex(), QtCore.Qt.UserRole)
        self.mdpEdit.setText(mdp)

    def addEtabViaFile(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, 
            QtWidgets.QApplication.translate('main', 'Open New School File'),
            self.filesDirLabel.text(),
            QtWidgets.QApplication.translate('main', 'tar.gz files (*.tar.gz)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.actualVersion = {
            'id_version': 998, 
            'versionName': '', 
            'versionUrl': '', 
            'versionLabel': ''}
        self.afficheWidget()
        self.newEtabFileName = fileName

    def removeUser(self):
        utils_functions.doWaitCursor()
        try:
            # nettoyage de la base config :
            id_version = self.actualVersion['id_version']
            commandLine = utils_functions.u(
                'DELETE FROM conn '
                'WHERE id_conn={0} '
                'AND user="{1}"').format(
                    id_version, self.loginComboBox.currentText())
            query = utils_db.queryExecute(
                commandLine, db=self.main.db_localConfig)
            # copie du fichier config modifié 
            # (si on annule après avoir supprimé,
            # VÉRAC ne peut plus démarrer) :
            fileTemp = utils_functions.u(
                '{0}/localConfig.sqlite').format(self.main.tempPath)
            dbFile_localConfig = self.main.localConfigDir + '/config.sqlite'
            utils_filesdirs.removeAndCopy(fileTemp, dbFile_localConfig)
            # nouveau remplissage du comboBox Version:
            self.versionChanged()
        finally:
            utils_functions.restoreCursor()

    def deleteSchool(self):
        versionLabel = self.actualVersion['versionLabel']
        title = QtWidgets.QApplication.translate(
            'main', 'Remove a school')
        message = QtWidgets.QApplication.translate(
            'main', 
            'Are you certain to want to remove this school: <br/><b>{0}<b> ?')
        message = message.format(versionLabel)
        if utils_functions.messageBox(
            self, 
            level='question', 
            title=title, 
            message=message, 
            buttons=['Yes', 'No']) == QtWidgets.QMessageBox.No:
            return
        utils_functions.doWaitCursor()
        try:
            # nettoyage de la base config :
            id_version = self.actualVersion['id_version']
            versionName = self.actualVersion['versionName']
            commandLine = utils_db.qdf_whereText.format(
                'versions', 'name', versionName)
            query = utils_db.queryExecute(commandLine, db=self.main.db_localConfig)
            commandLine = utils_db.qdf_whereText.format(
                'config', 'key_name', versionName)
            query = utils_db.queryExecute(commandLine, query=query)
            commandLine = utils_db.qdf_whereText.format(
                'config', 'key_name', 'actualVersion')
            query = utils_db.queryExecute(commandLine, query=query)
            commandLine = utils_db.qdf_where.format(
                'conn', 'id_conn', id_version)
            query = utils_db.queryExecute(commandLine, query=query)
            commandLine = utils_db.qdf_whereText.format(
                'schedules', 'versionName', versionName)
            query = utils_db.queryExecute(commandLine, query=query)
            # suppression du dossier de l'établissement :
            dirName = self.main.localConfigDir + '/' + versionName
            utils_filesdirs.emptyDir(dirName)
            # copie du fichier config modifié 
            # (si on annule après avoir supprimé, VÉRAC ne peut plus démarrer) :
            fileTemp = utils_functions.u('{0}/localConfig.sqlite').format(self.main.tempPath)
            dbFile_localConfig = self.main.localConfigDir + '/config.sqlite'
            utils_filesdirs.removeAndCopy(fileTemp, dbFile_localConfig)
            # nouveau remplissage du comboBox Version:
            self.recreateVersionComboBox(newVersionName='demo')
        finally:
            utils_functions.restoreCursor()

    def openOtherFile(self):
        """
        permet d'ouvrir le fichier d'un autre prof
        pour l'aider à résoudre un problème par exemple.
        Envois et autres sont désactivés mais on peut enregistrer.
        DANGEREUX ? => prévenir
        """
        title = QtWidgets.QApplication.translate('main', 'Open sqlite File')
        ext = QtWidgets.QApplication.translate('main', 'sqlite files (*.sqlite)')
        proposedDir = self.main.workDir
        theFile = QtWidgets.QFileDialog.getOpenFileName(
            self.main, title, proposedDir, ext)
        theFile = utils_functions.verifyLibs_fileName(theFile)
        if theFile == '':
            return
        utils_functions.doWaitCursor()
        try:
            self.main.me['userMode'] = 'prof'
            id_version = self.actualVersion['id_version']
            prefixProfFiles = utils_db.readInConfigTable(
                self.main.db_commun, "prefixProfFiles", "config")[1]
            if prefixProfFiles == "":
                prefixProfFiles = "prof"
            self.main.filesDir = self.filesDirLabel.text()
            dbName_my = prefixProfFiles + utils_functions.u(self.main.me['userId'])
            self.main.dbName_my = dbName_my
            self.main.dbFile_my = theFile
            self.main.NetOK = False
            utils_functions.myPrint(utils_db.sqlDatabase.connectionNames())
        finally:
            utils_functions.restoreCursor()
        QtWidgets.QDialog.accept(self)

    def doAfterDrop(self):
        """
        permet d'ouvrir le fichier d'un autre prof
        par drag-drop
        """
        theFile = self.openOtherFileButton.fileName
        theFile = utils_functions.verifyLibs_fileName(theFile)
        if theFile == '':
            return
        utils_functions.doWaitCursor()
        try:
            self.main.me['userMode'] = 'prof'
            id_version = self.actualVersion['id_version']
            prefixProfFiles = utils_db.readInConfigTable(
                self.main.db_commun, "prefixProfFiles", "config")[1]
            if prefixProfFiles == "":
                prefixProfFiles = "prof"
            self.main.filesDir = self.filesDirLabel.text()
            dbName_my = prefixProfFiles + utils_functions.u(self.main.me['userId'])
            self.main.dbName_my = dbName_my
            self.main.dbFile_my = theFile
            self.main.NetOK = False
            utils_functions.myPrint(utils_db.sqlDatabase.connectionNames())
        finally:
            utils_functions.restoreCursor()
        QtWidgets.QDialog.accept(self)

    def doChangeWorkDir(self):
        title = QtWidgets.QApplication.translate('main', 'Choose a Directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self, 
            title, 
            self.filesDirLabel.text(), 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            self.filesDirLabel.setText(directory)

    def doOpenFilesDir(self):
        dirName = QtCore.QDir(self.filesDirLabel.text()).absolutePath()
        utils_filesdirs.openDir(dirName)

    def doOpenConfigDir(self):
        dirName = QtCore.QDir(self.main.localConfigDir).absolutePath()
        utils_filesdirs.openDir(dirName)

    def doShowMdp(self):
        if self.showMdpCheckBox.isChecked():
            self.mdpEdit.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.mdpEdit.setEchoMode(QtWidgets.QLineEdit.Password)

    def doAdmin(self):
        """
        Connexion de l'admin
        La présence du sous-dossier admin suffit, donc pas besoin de mot de passe.

        Si on lance une archive, le dossier verac_admin est à côté du logiciel,
        et on passe NetOK à False (pour empêcher les mises à jour).
        """
        theDir = self.main.beginDir + '/../verac_admin'
        if (utils.installType == utils.INSTALLTYPE_ARCHIVE) and (QtCore.QDir(theDir).exists()):
            self.main.NetOK = False
        id_version = self.actualVersion['id_version']
        if (id_version < 1000):
            self.main.me['userMode'] = 'admin'
            self.main.me['userName'] = 'Administrateur'
            if self.main.actualVersion['id_version'] != id_version:
                changeActualVersion(self.main, id_version)
            QtWidgets.QDialog.accept(self)
        else:
            m1 = QtWidgets.QApplication.translate('main', 'Not Admin of:')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p></p>'
                '<p align="center">{2}</p>').format(utils.SEPARATOR_LINE, m1, self.actualVersion['versionLabel'])
            utils_functions.messageBox(self, level='warning', message=message)

    def doConnect(self):
        """
        Connexion d'un prof.
        On vérifie son login et son mdp (encodés) dans la base users.
        Si le mdp n'a pas été changé, on passe la variable main.mustChangeMdp à True 
        (sauf perso et demo).
        """
        utils_functions.afficheTime(self.main, 'doConnect')
        utils_functions.doWaitCursor()
        if self.debugCheckBox.isChecked():
            utils.changeLogFile(True)
            utils.changeModeBavard(True)
        try:
            self.main.me['userMode'] = 'prof'
            id_version = self.actualVersion['id_version']
            result = False
            # le dico coherenceMdp servira à prévenir si les mdp web et local diffèrent :
            coherenceMdp = {}
            if id_version == 0:
                # version perso :
                utils_functions.myPrint('VERSION PERSO')
                user = utils_functions.u('perso')
                encUser = utils_functions.doEncode(user)
                mdp = utils_functions.u('')
                encMdp = utils_functions.doEncode(mdp)
                mustSave = False
                newEtab, newEtabViaFile = False, False
                self.main.me['userId'] = 1
                self.main.me['user'] = user
                self.main.me['Mdp'] = mdp
                self.main.me['userName'] = self.actualVersion['versionLabel']
                result = True
            elif id_version == 1:
                # version demo :
                utils_functions.myPrint('VERSION DEMO')
                user = self.loginComboBox.currentText()
                encUser = utils_functions.doEncode(user)
                mdp = self.loginComboBox.itemData(
                    self.loginComboBox.currentIndex(), QtCore.Qt.UserRole)
                encMdp = utils_functions.doEncode(mdp)
                mustSave = False
                newEtab, newEtabViaFile = False, False
            else:
                # version etab :
                utils_functions.myPrint('VERSION ETAB')
                user = self.loginComboBox.currentText()
                mdp = self.mdpEdit.text()
                encUser = utils_functions.doEncode(user)
                encMdp = utils_functions.doEncode(mdp)
                mustSave = self.saveCheckBox.isChecked()
                newEtab = (id_version == 999)
                if newEtab:
                    self.actualVersion['versionUrl'] = self.urlEtabEdit.text()
                newEtabViaFile = (id_version == 998)
                initialMdp = utils_functions.u('')

            if self.noNetCheckBox.isChecked():
                self.main.NetOK = False
            elif (utils.installType != utils.INSTALLTYPE_ARCHIVE):
                self.main.NetOK = utils_web.testNet(self.main, self.actualVersion['versionUrl'])

            if newEtab:
                # connexion à un nouvel établissement:
                if self.main.NetOK:
                    utils_functions.myPrint('VERSION ETAB: NEW')
                    # on tente la base en ligne:
                    userId = -1
                    theUrl = self.actualVersion['versionUrl'] + "/pages/verac_prof_login.php"
                    postData = utils_functions.u('login={0}&password={1}').format(user, mdp)
                    httpClient = utils_web.HttpClient(self.main)
                    httpClient.launch(theUrl, postData)
                    while httpClient.state == utils_web.STATE_LAUNCHED:
                        QtWidgets.QApplication.processEvents()
                    if httpClient.state == utils_web.STATE_OK:
                        try:
                            userId = utils_functions.verifyLibs_toInt(
                                httpClient.reponse.split('|')[1])
                        except:
                            pass
                        if userId < 0:
                            utils_functions.myPrint(httpClient.reponse)
                        else:
                            self.main.me['userId'] = userId
                            self.main.me['user'] = user
                            self.main.me['Mdp'] = mdp
                            datetime = utils_functions.verifyLibs_toInt(
                                httpClient.reponse.split('|')[4])
                            self.main.me['netDateTime'] = datetime
                            self.actualVersion['versionName'] = utils_functions.u(
                                httpClient.reponse.split('|')[2])
                            self.actualVersion['versionLabel'] = utils_functions.u(
                                httpClient.reponse.split('|')[3])
                            self.actualVersion = downloadConfigIfNew(self.main, self.actualVersion)
                            id_version = self.actualVersion['id_version']
                            commandLine = utils_db.q_selectAllFromWhere.format(
                                'profs', 'id', userId)
                            query = utils_db.queryExecute(commandLine, db=self.main.db_users)
                            while query.next():
                                self.main.me['userName'] = query.value(2) + ' ' + query.value(1)
                                self.main.me['Matiere'] = query.value(6)
                            utils_functions.afficheMessage(self.main, 
                                ['YES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', self.main.me['userName']])
                            result = True
            elif newEtabViaFile:
                # à faire...améliorer tester deboguer
                # connexion à un nouvel établissement par fichier tar.gz:
                utils_functions.myPrint('VERSION ETAB: NEW VIA FILE')
                # ça se passe dans la fonction setConfigIfNew:
                self.actualVersion = setConfigIfNew(
                    self.main, self.newEtabFileName)
                id_version = self.actualVersion['id_version']

                # on vérifie dans la base locale:
                commandLine = utils_db.q_selectAllFrom.format('profs')
                query = utils_db.queryExecute(commandLine, db=self.main.db_users)
                while query.next():
                    if query.value(3) in encUser:
                        encMdpInDB = query.value(4)
                        if encMdpInDB in encMdp:
                            utils_functions.afficheMessage(self.main, 
                                ['YES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!', query.value(1)])
                            self.main.me['userId'] = int(query.value(0))
                            self.main.me['userName'] = query.value(2) + ' ' + query.value(1)
                            self.main.me['user'] = user
                            self.main.me['Mdp'] = mdp
                            result = True

            if self.main.actualVersion['id_version'] != id_version:
                changeActualVersion(self.main, id_version)

            prefixProfFiles = utils_db.readInConfigTable(
                self.main.db_commun, "prefixProfFiles", "config")[1]
            if prefixProfFiles == "":
                prefixProfFiles = "prof"

            if (result == False) and (self.main.NetOK) and (newEtab == False):
                # d'abord on tente la base en ligne :
                utils_functions.myPrint('on tente la base en ligne')
                userId = -1
                theUrl = self.actualVersion['versionUrl'] + "/pages/verac_prof_login.php"
                postData = utils_functions.u('login={0}&password={1}').format(user, mdp)
                httpClient = utils_web.HttpClient(self.main)
                httpClient.launch(theUrl, postData)
                while httpClient.state == utils_web.STATE_LAUNCHED:
                    QtWidgets.QApplication.processEvents()
                if httpClient.state == utils_web.STATE_OK:
                    try:
                        userId = utils_functions.verifyLibs_toInt(
                            httpClient.reponse.split('|')[1])
                    except:
                        self.main.NetOK = False
                    if userId > -1:
                        self.main.me['userId'] = userId
                        self.main.me['user'] = user
                        self.main.me['Mdp'] = mdp
                        try:
                            datetime = utils_functions.verifyLibs_toInt(
                                httpClient.reponse.split('|')[4])
                            self.main.me['netDateTime'] = datetime
                        except:
                            pass
                        commandLine = utils_db.q_selectAllFromWhere.format(
                            'profs', 'id', userId)
                        query = utils_db.queryExecute(commandLine, db=self.main.db_users)
                        while query.next():
                            self.main.me['userName'] = query.value(2) + ' ' + query.value(1)
                            encMdpInDB = query.value(4)
                            initialMdp = query.value(5)
                            self.main.me['Matiere'] = query.value(6)
                        utils_functions.afficheMessage(self.main, 
                            ['YES NET', self.main.me['userName']])
                        coherenceMdp['webMdp'] = (True, encMdp)
                        # on teste si le mot de passe local est le même
                        # afin de prévenir sinon :
                        try:
                            if encMdpInDB in encMdp:
                                coherenceMdp['localMdp'] = (True, encMdp)
                            else:
                                coherenceMdp['localMdp'] = (False, encMdp, encMdpInDB)
                        except:
                            # Erreur déclenchée si la base locale n'est pas à jour
                            # (ne contient pas un nouveau prof pourtant reconnu par la base distante).
                            # On fait comme si OK pour permettre la connexion :
                            coherenceMdp['localMdp'] = (True, encMdp)
                        result = True
                    else:
                        coherenceMdp['webMdp'] = (False, encMdp)

            if (result == False) and (newEtab == False):
                # ensuite la base locale:
                utils_functions.myPrint('ensuite la base locale')
                commandLine = utils_functions.u(
                    'SELECT * FROM profs '
                    'WHERE Login IN ("{0}", "{1}") '
                    'AND mdp IN ("{2}", "{3}")').format(
                        encUser[0], encUser[1], encMdp[0], encMdp[1])
                query = utils_db.queryExecute(commandLine, db=self.main.db_users)
                if query.first():
                    utils_functions.afficheMessage(self.main, ['YES LOCAL', query.value(1)])
                    self.main.me['userId'] = int(query.value(0))
                    self.main.me['userName'] = utils_functions.u('{0} {1}').format(query.value(2), 
                        query.value(1))
                    self.main.me['Mdp'] = mdp
                    initialMdp = query.value(5)
                    coherenceMdp['localMdp'] = (True, encMdp)
                    self.main.me['Matiere'] = query.value(6)
                    self.main.me['user'] = user
                    result = True
                else:
                    coherenceMdp['localMdp'] = (False, encMdp)

        finally:
            utils_functions.restoreCursor()
            utils_functions.myPrint('coherenceMdp', coherenceMdp)
            """
            Les cas où il faut prévenir :
                * le site répond mais webMdp ne marche pas mais localMdp marche
                * le site répond et webMdp marche, mais pas localMdp
            """
            if 'webMdp' in coherenceMdp:
                if coherenceMdp['webMdp'][0]:
                    if not(coherenceMdp['localMdp'][0]):
                        # on doit télécharger la base users :
                        import prof_db
                        prof_db.downloadUsersDB(self.main, msgFin=False, doUpdateGroups=False)
                else:
                    if coherenceMdp['localMdp'][0]:
                        # on prévient qu'il faut changer le
                        # mdp depuis le site web :
                        message = QtWidgets.QApplication.translate('main', 
                            '<p>The password you used to log is saved on your computer,'
                            '<br/><b>but is not the same as that registered on the website '
                            'of the school.</b></p>'
                            '<p>Perhaps you have changed the password when your computer'
                            '<br/>was not connected to the Internet?</p>'
                            '<p>To solve this problem, you should:'
                            '<ul>'
                            '<li>You can reconnect with the password from the website</li>'
                            '<li>Or go to the web interface and change your website password.</li>'
                            '</ul></p>')
                        utils_functions.messageBox(
                            self.main, level='warning', message=message, buttons=['Ok'])
                        self.main.NetOK = False

        # si tout c'est bien passé:
        if result == True:
            # seulement pour les versions étab (ni perso ni demo) :
            if id_version > 1:
                # si on n'a pas encore changé de mot de passe :
                if initialMdp in encMdp:
                    self.main.mustChangeMdp = True
                    mustSave = False
                # on met à jour la table conn (connexion) de la base config :
                users = []
                if mustSave:
                    users.append((user, mdp))
                for index in range(self.loginComboBox.count()):
                    other = self.loginComboBox.itemText(index)
                    mdp = self.loginComboBox.itemData(index, QtCore.Qt.UserRole)
                    if not((other, mdp) in users) and (other != user):
                        users.append((other, mdp))
                query_config = utils_db.query(self.main.db_localConfig)
                versionDB = 0
                commandLine = utils_db.q_selectAllFromWhereText.format(
                    'config', 'key_name', 'versionDB')
                query_config = utils_db.queryExecute(commandLine, query=query_config)
                if query_config.first():
                    versionDB = int(query_config.value(1))
                else:
                    commandLine = 'INSERT INTO config VALUES (?, ?, ?)'
                    query_config = utils_db.queryExecute(
                        {commandLine: ('versionDB', utils.VERSIONDB_CONFIG, '')}, 
                        query=query_config)
                    commandLine = utils_db.q_dropTable.format('conn')
                    query_config = utils_db.queryExecute(commandLine, query=query_config)
                    query_config = utils_db.queryExecute(
                        utils_db.qct_config_conn, query=query_config)
                if versionDB < 2:
                    # on récupère le contenu de la table conn :
                    lines = []
                    commandLine = utils_db.q_selectAllFrom.format('conn')
                    query_config = utils_db.queryExecute(commandLine, query=query_config)
                    while query_config.next():
                        lines.append(
                            (int(query_config.value(0)), 
                             int(query_config.value(1)), 
                             query_config.value(2), 
                             query_config.value(3)))
                    commandLine = utils_db.q_dropTable.format('conn')
                    query_config = utils_db.queryExecute(commandLine, query=query_config)
                    query_config = utils_db.queryExecute(
                        utils_db.qct_config_conn, query=query_config)
                    commandLine = 'INSERT INTO conn VALUES (?, ?, ?, ?)'
                    query_config = utils_db.queryExecute(
                        {commandLine: lines}, query=query_config)
                    utils_db.changeInConfigTable(
                        self.main, 
                        self.main.db_localConfig, 
                        {'versionDB': (utils.VERSIONDB_CONFIG, '')})

                commandLine = utils_db.qdf_where.format(
                    'conn', 'id_conn', id_version)
                query_config = utils_db.queryExecute(commandLine, query=query_config)
                commandLine = 'INSERT INTO conn VALUES (?, ?, ?, ?)'
                lines = []
                save = 0
                for (user, mdp) in users:
                    lines.append((id_version, save, user, mdp))
                    save += 1
                query_config = utils_db.queryExecute(
                    {commandLine: lines}, query=query_config)

            self.main.filesDir = self.filesDirLabel.text()

            utils_functions.myPrint('')
            utils_functions.myPrint('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
            utils_functions.myPrint('prefixProfFiles: ', prefixProfFiles)
            utils_functions.myPrint('&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&')
            utils_functions.myPrint('')
            dbName_my = prefixProfFiles + utils_functions.u(self.main.me['userId'])
            self.main.dbName_my = dbName_my

            # Si on lance une archive, le fichier profxx est à côté du logiciel.
            # On passe aussi NetOK à False (pour empêcher les mises à jour).
            theFile = self.main.beginDir + '/../' + dbName_my + '.sqlite'
            if (utils.installType == utils.INSTALLTYPE_ARCHIVE) and (QtCore.QFile(theFile).exists()):
                self.main.dbFile_my = theFile
                self.main.NetOK = False
            else:
                if utils.lanFilesDir != '':
                    theDir = utils.lanFilesDir
                else:
                    theDir = self.main.filesDir
                self.main.dbFile_my = utils_functions.u('{0}').format(
                    theDir + '/' + dbName_my + '.sqlite')
            utils_functions.myPrint(utils_db.sqlDatabase.connectionNames())
            utils_db.changeInConfigDB(self.main, 'FilesDir', '', self.main.filesDir)
            QtWidgets.QDialog.accept(self)
        # sinon:
        else:
            m1 = QtWidgets.QApplication.translate('main', 'Error, try again!')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p></p>').format(utils.SEPARATOR_LINE, m1)
            utils_functions.messageBox(self, level='warning', message=message)
            self.loginComboBox.setFocus()


def downloadConfigIfNew(main, actualVersion):
    """
    modifie la version actuelle, et surtout ses paramètres
    """
    versionName = actualVersion['versionName']
    versionUrl = actualVersion['versionUrl']
    main.siteUrlPublic = utils_functions.removeSlash(versionUrl)

    # on supprime les traces d'une ancienne version:
    commandLine = utils_db.qdf_whereText.format(
        'versions', 'name', versionName)
    query = utils_db.queryExecute(commandLine, db=main.db_localConfig)

    # on crée le sous-dossier dans .Verac:
    utils_filesdirs.createDirs(main.localConfigDir, versionName)

    # on télécharge les bases users et commun:
    theFileName = 'users.sqlite'
    destDir = main.localConfigDir + '/' + versionName
    OK = False
    if utils_web.downloadFileFromSecretDir(main, '/public/', theFileName, toDir=destDir):
        OK = True
    if OK == True:
        OK = False
        theFileName = 'commun.sqlite'
        if utils_web.downloadFileFromSecretDir(main, '/public/', theFileName, toDir=destDir):
            OK = True
    # en cas de pépin, on quitte:
    utils_functions.myPrint('OK: ', OK)
    if OK == False:
        return actualVersion

    # on se connecte aux bases users et commun:
    main.dbFile_users = main.localConfigDir + '/' + versionName + '/users.sqlite'
    main.db_users = utils_db.createConnection(main, main.dbFile_users)[0]
    main.dbFile_commun = main.localConfigDir + '/' + versionName + '/commun.sqlite'
    main.db_commun = utils_db.createConnection(main, main.dbFile_commun)[0]

    # on lit versionLabel dans la base commun:
    versionLabel = utils_db.readInConfigTable(main.db_commun, "versionLabel")[1]

    # on récupère une id_version valable:
    maxId = 2
    commandLine = utils_db.q_selectAllFrom.format('versions')
    query = utils_db.queryExecute(commandLine, query=query)
    while query.next():
        id_version = int(query.value(0))
        if id_version > maxId:
            maxId = id_version
    id_version = maxId + 1

    # on inscrit dans la base .Verac/config:
    commandLine = utils_db.insertInto('versions', 4)
    query = utils_db.queryExecute(
        {commandLine: (id_version, versionName, versionUrl, versionLabel)}, 
        query=query)

    newActualVersion = {
        'id_version': id_version, 
        'versionName': versionName, 
        'versionUrl': versionUrl, 
        'versionLabel': versionLabel}
    return newActualVersion


def setConfigIfNew(main, newEtabFileName):
    """
    modifie la version actuelle, et surtout ses paramètres
    """
    # on décompresse le tar.gz dans .Verac:
    path = main.localConfigDir
    utils_functions.doWaitCursor()
    if utils_filesdirs.untarDirectory(newEtabFileName, path):
        utils_functions.afficheMessage(main, 'FICHIER DECOMPRESSE')
    utils_functions.restoreCursor()

    # on lit le nom de l'établissement:
    #c'est le nom du fichier (dossier compressé)
    versionName = QtCore.QFileInfo(newEtabFileName).baseName()

    # on supprime les traces d'une ancienne version:
    commandLine = utils_db.qdf_whereText.format(
        'versions', 'name', versionName)
    query = utils_db.queryExecute(commandLine, db=main.db_localConfig)

    # on se connecte aux bases users et commun:
    main.dbFile_users = main.localConfigDir + '/' + versionName + '/users.sqlite'
    main.db_users = utils_db.createConnection(main, main.dbFile_users)[0]
    main.dbFile_commun = main.localConfigDir + '/' + versionName + '/commun.sqlite'
    main.db_commun = utils_db.createConnection(main, main.dbFile_commun)[0]

    # on lit versionUrl et versionLabel:
    versionUrl = utils_db.readInConfigTable(main.db_commun, "siteUrlPublic")[1]
    versionLabel = utils_db.readInConfigTable(main.db_commun, "versionLabel")[1]

    # on récupère une id_version valable:
    maxId = 2
    commandLine = utils_db.q_selectAllFrom.format('versions')
    query = utils_db.queryExecute(commandLine, db=main.db_localConfig)
    while query.next():
        id_version = int(query.value(0))
        if id_version > maxId:
            maxId = id_version
    id_version = maxId + 1

    # on inscrit dans la base .Verac/config:
    commandLine = utils_db.insertInto('versions', 4)
    query = utils_db.queryExecute(
        {commandLine: (id_version, versionName, versionUrl, versionLabel)}, 
        query=query)

    newActualVersion = {
        'id_version': id_version, 
        'versionName': versionName, 
        'versionUrl': versionUrl, 
        'versionLabel': versionLabel}
    return newActualVersion


def readActualVersion(main):
    """
    lit et retourne les attributs de la version actuellement sélectionnée
    son id, nom (demo par exemple), url et label
    """
    id_version, versionName, versionUrl, versionLabel = 0, 'perso', '', ''
    id_version = utils_db.readInConfigDB(main, 'actualVersion')[0]
    commandLine = utils_db.q_selectAllFromWhere.format(
        'versions', 'id_version', id_version)
    if utils.lanConfigDir != '':
        query = utils_db.queryExecute(commandLine, db=main.db_lanConfig)
    else:
        query = utils_db.queryExecute(commandLine, db=main.db_localConfig)
    while query.next():
        versionName = query.value(1)
        versionUrl = query.value(2)
        versionLabel = query.value(3)
    result = {
        'id_version': id_version, 
        'versionName': versionName, 
        'versionUrl': versionUrl, 
        'versionLabel': versionLabel}
    return result

def changeActualVersion(main, id_version):
    """
    modifie la version actuelle, et surtout ses paramètres
    """
    versionName, versionUrl, versionLabel = 'perso', '', ''
    utils_db.changeInConfigTable(
        main, 
        main.db_localConfig, 
        {'actualVersion': (id_version, '')})
    commandLine = utils_db.q_selectAllFromWhere.format(
        'versions', 'id_version', id_version)
    query = utils_db.queryExecute(commandLine, db=main.db_localConfig)
    while query.next():
        versionName = query.value(1)
        versionUrl = query.value(2)
        versionLabel = query.value(3)
    main.actualVersion = {
        'id_version': id_version, 
        'versionName': versionName, 
        'versionUrl': versionUrl, 
        'versionLabel': versionLabel}

    # gestion des bases users et commun:
    main.dbFile_users = main.localConfigDir + '/' + versionName + '/users.sqlite'
    main.db_users = utils_db.createConnection(main, main.dbFile_users)[0]
    main.dbFile_commun = main.localConfigDir + '/' + versionName + '/commun.sqlite'
    main.db_commun = utils_db.createConnection(main, main.dbFile_commun)[0]
    # lecture des paramètres de l'établissement 
    # depuis commun.config :
    config = {}
    query_commun = utils_db.query(main.db_commun)
    commandLine_commun = utils_db.q_selectAllFrom.format('config')
    query_commun = utils_db.queryExecute(
        commandLine_commun, query=query_commun)
    while query_commun.next():
        config[query_commun.value(0)] = (
            query_commun.value(1), query_commun.value(2))
    main.siteUrlBase = config.get('siteUrlBase', (0, ''))[1]
    main.siteUrlPublic = config.get('siteUrlPublic', (0, ''))[1]
    main.siteUrlPublic = utils_functions.removeSlash(
        main.siteUrlPublic)
    main.limiteBLTPerso = config.get('limiteBLTPerso', (4, ''))[0]
    main.annee_scolaire = config.get('annee_scolaire', (0, ''))
    if main.annee_scolaire[0] < 1:
        main.annee_scolaire = utils_functions.calcAnneeScolaire()








"""
###########################################################
###########################################################

                Mdp

###########################################################
###########################################################
"""

class ChangeMdpDlg(QtWidgets.QDialog):
    """
    dialog de modification de mot de passe
    """
    def __init__(self, parent=None):
        super(ChangeMdpDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Change password'))

        # les champs pour mdp et confirmation :
        mdpLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'New password:')))
        self.mdpEdit = QtWidgets.QLineEdit()
        self.mdpEdit.setEchoMode(QtWidgets.QLineEdit.Password)
        mdpConfirmLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Confirm:')))
        self.mdpConfirmEdit = QtWidgets.QLineEdit()
        self.mdpConfirmEdit.setEchoMode(QtWidgets.QLineEdit.Password)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(mdpLabel, 1, 0)
        grid.addWidget(self.mdpEdit, 1, 1)
        grid.addWidget(mdpConfirmLabel, 2, 0)
        grid.addWidget(self.mdpConfirmEdit, 2, 1)
        grid.addWidget(buttonBox, 9, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('change-mdp')

def changeMdp(main):
    """
    changement de mot de passe.
    Étapes :
        1. affichage du dialog pour saisir le nouveau mdp
        2. si le site web est opérationnel, mise à jour en ligne.
           Pour cela, on envoie la demande à la page verac_prof_changePassword.php
           et on récupère la réponse
        3. mise à jour locale (fichier users.sqlite).
           Si le site web est opérationnel, la mise à jour locale n'est faite 
           que si la mise à jour distante a réussi
        4. message final
    """
    utils_functions.afficheMessage(main, 'changeMdp', statusBar=True)
    # dialog de saisie du mot de passe :
    dialog = ChangeMdpDlg(parent=main)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        return
    newMdp = dialog.mdpEdit.text()
    if newMdp == '':
        message = QtWidgets.QApplication.translate(
            'main', 'The password is empty')
        utils_functions.messageBox(main, level='warning', message=message)
        return
    if dialog.mdpConfirmEdit.text() != newMdp:
        message = QtWidgets.QApplication.translate(
            'main', '2 entries are different')
        utils_functions.messageBox(main, level='warning', message=message)
        return

    utils_functions.doWaitCursor()
    try:
        netPasswordChanged = False
        noNetPassword = False
        localPasswordChanged = False
        # d'abord on change la base en ligne :
        if not(main.NetOK):
            noNetPassword = True
        else:
            theUrl = main.siteUrlPublic + "/pages/verac_prof_changePassword.php"
            postData = utils_functions.u(
                'login={0}&password={1}&newPassword={2}').format(
                    main.me['user'], main.me['Mdp'], newMdp)
            httpClient = utils_web.HttpClient(main)
            httpClient.launch(theUrl, postData)
            while httpClient.state == utils_web.STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if httpClient.state == utils_web.STATE_OK:
                try:
                    userId = utils_functions.verifyLibs_toInt(
                        httpClient.reponse.split('|')[1])
                except:
                    userId = -1
                if userId > -1:
                    utils_functions.afficheMessage(
                        main, 'Base distante modifiee', statusBar=True)
                    netPasswordChanged = True
        # ensuite la base locale :
        if netPasswordChanged or noNetPassword:
            encMdp = utils_functions.doEncode(newMdp, algo='sha256')
            commandLine_users = utils_functions.u(
                'UPDATE profs SET Mdp="{0}" WHERE id={1}').format(encMdp, main.me['userId'])
            query_users = utils_db.queryExecute(
                [commandLine_users, utils_db.qcmd_Vacuum], 
                db=main.db_users)
            utils_functions.afficheMessage(main, 'Base locale modifiee', statusBar=True)
            localPasswordChanged = True
            utils_filesdirs.removeAndCopy(main.dbFileTemp_users, main.dbFile_users)
        # enfin la valeur en mémoire :
        if localPasswordChanged:
            main.me['Mdp'] = newMdp
        utils_functions.myPrint(
            'modifOK: ', netPasswordChanged, noNetPassword, localPasswordChanged)
    finally:
        utils_functions.restoreCursor()
        if netPasswordChanged and localPasswordChanged:
            message = QtWidgets.QApplication.translate(
                'main', 'The password was changed successfully.')
        elif noNetPassword and localPasswordChanged:
            message = QtWidgets.QApplication.translate(
                'main', 'The local password (on this computer) was changed successfully.')
        else:
            message = QtWidgets.QApplication.translate(
                'main', 'The password change failed.')
        utils_functions.messageBox(main, message=message)


def mustChangeMdp(main):
    """
    averti qu'il faut changer de mot de passe
    usage : utils_connect.mustChangeMdp(self)
    """
    message = QtWidgets.QApplication.translate(
        'main', 
        '<p>For better security, you must <b>change your password</b>.</p>'
        '<p>(menu "Utils > Change password")</p>')
    utils_functions.messageBox(main, message=message)






