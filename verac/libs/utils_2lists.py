# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Une fenêtre à 2 listes avec drag drop
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



"""
ITEMS_IN_DRAGDROP est une liste globale permettant de savoir quels items sont déplacés,
et depuis quelle liste.
ITEMS_IN_DRAGDROP = [num, [item1, item2, ...]]
    num est le numéro de la liste d'où proviennent les items (et -1 si pas d'item)
    [item1, item2, ...] est la liste des items
"""
ITEMS_IN_DRAGDROP = [-1]


class DragDropListWidget(QtWidgets.QListWidget):
    """
    la classe utilisée pour les 2 listes.
    Gère le drag-drop via la valeur du paramètre num :
        0 pour la première liste
        1 pour la deuxième
    Si extendedSelection est à True, on peut sélectionner plusieurs items.
    Si removeOnDrag est à True, les items seront supprimés après le drop.
    Si acceptMoves est à True, un drag-drop interne permettra de déplacer les items.
    """
    def __init__(self, parent=None, 
                num=0, extendedSelection=True, removeOnDrag=True, acceptMoves=True, withCheckBox=False):
        """
        initialisation
        """
        super(DragDropListWidget, self).__init__(parent)
        self.parent = parent

        self.num = num
        if num == 0:
            self.other = 1
        else:
            self.other = 0
        if extendedSelection:
            self.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.removeOnDrag = removeOnDrag
        self.withCheckBox = withCheckBox
        self.acceptMoves = acceptMoves
        self.setDragEnabled(True)
        self.setMovement(QtWidgets.QListView.Snap)
        self.setAcceptDrops(True)
        self.setDropIndicatorShown(True)
        self.drop = False

    def startDrag(self, supportedActions):
        """
        Début d'un drag.
        On met self.num et la liste des items dans la liste globale ITEMS_IN_DRAGDROP.
        """
        global ITEMS_IN_DRAGDROP
        ITEMS_IN_DRAGDROP = [self.num]
        listItems = []
        for item in self.selectedItems():
            listItems.append(item)
        ITEMS_IN_DRAGDROP.append(listItems)
        return super(DragDropListWidget, self).startDrag(supportedActions)

    def dragEnterEvent(self, event):
        """
        On gère les cas où le drop sera autorisé.
        """
        if ITEMS_IN_DRAGDROP[0] == self.other:
            super(DragDropListWidget, self).dragEnterEvent(event)
        elif (ITEMS_IN_DRAGDROP[0] == self.num) and (self.acceptMoves):
            super(DragDropListWidget, self).dragEnterEvent(event)
        else:
            event.ignore()

    def dropEvent(self, event):
        """
        Fin d'un drag-drop.
        Si self.acceptMoves est à False, on doit recréer les items
        avant de les ajouter.
        """
        if not(self.acceptMoves):
            for item in ITEMS_IN_DRAGDROP[1]:
                # on doit recréer un nouvel item, sinon ça ne fonctionne pas :
                newItem = QtWidgets.QListWidgetItem(item.text())
                newItem.setData(QtCore.Qt.UserRole, item.data(QtCore.Qt.UserRole))
                newItem.setToolTip(item.toolTip())
                newItem.setIcon(item.icon())
                # et on l'ajoute à la fin de la liste :
                self.addItem(newItem)
        elif self.withCheckBox and ITEMS_IN_DRAGDROP[0] == self.other:
            for item in ITEMS_IN_DRAGDROP[1]:
                # on doit recréer un nouvel item, sinon ça ne fonctionne pas :
                newItem = QtWidgets.QListWidgetItem(item.text())
                newItem.setData(QtCore.Qt.UserRole, item.data(QtCore.Qt.UserRole))
                newItem.setToolTip(item.toolTip())
                newItem.setIcon(item.icon())
                newItem.setCheckState(QtCore.Qt.Checked)
                # et on l'ajoute à la fin de la liste :
                self.addItem(newItem)
        else:
            super(DragDropListWidget, self).dropEvent(event)
        # on passe ensuite au traitement d'après drop :
        self.drop = True
        self.doAfterDrop()

    def doAfterDrop(self):
        """
        Après un drag-drop.
        Si self.parent a bien une fonction doAfterDrop pour traiter l'après drag-drop
        (gérer les 2 listes), on l'appelle.
        Sinon, on appelle self.removeAfterDrop.
        """
        try:
            self.parent.doAfterDrop()
        except:
            self.removeAfterDrop()

    def removeAfterDrop(self):
        """
        Après un drop, si les items viennent de la liste,
        et si self.removeOnDrag est à True, 
        on supprime les items.
        Enfin on vide la liste globale ITEMS_IN_DRAGDROP.
        """
        global ITEMS_IN_DRAGDROP
        if ITEMS_IN_DRAGDROP[0] == self.num:
            if self.removeOnDrag:
                for item in ITEMS_IN_DRAGDROP[1]:
                    self.takeItem(self.row(item))
            ITEMS_IN_DRAGDROP = [-1]




class DragDropEdit(QtWidgets.QLineEdit):
    """
    Variante pour utiliser un edit au lieu d'une liste.
    Typiquement, on fait un drag-drop depuis la liste vers l'édit.
    """
    def __init__(self, parent=None, num=0):
        """
        initialisation
        """
        super(DragDropEdit, self).__init__(parent)
        self.parent = parent

        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.num = num
        if num == 0:
            self.other = 1
        else:
            self.other = 0
        self.item = QtWidgets.QListWidgetItem()
        self.setReadOnly(True)

    def startDrag(self, supportedActions):
        """
        Début d'un drag.
        On met self.num et l'item dans la liste globale ITEMS_IN_DRAGDROP.
        """
        global ITEMS_IN_DRAGDROP
        ITEMS_IN_DRAGDROP = [self.num]
        listItems = [self.item]
        ITEMS_IN_DRAGDROP.append(listItems)
        return super(DragDropEdit, self).startDrag(supportedActions)

    def dragEnterEvent(self, event):
        """
        On gère les cas où le drop sera autorisé.
        """
        if ITEMS_IN_DRAGDROP[0] == self.other:
            event.accept()
            super(DragDropEdit, self).dragEnterEvent(event)
        else:
            event.ignore()

    def dropEvent(self, event):
        """
        Fin d'un drag-drop.
        """
        super(DragDropEdit, self).dropEvent(event)
        self.item = ITEMS_IN_DRAGDROP[1][0]
        self.setText(self.item.text())
        # on passe ensuite au traitement d'après drop :
        self.doAfterDrop()

    def doAfterDrop(self):
        """
        Après un drag-drop.
        Si self.parent a bien une fonction doAfterDrop pour traiter l'après drag-drop
        (gérer les 2 listes), on l'appelle.
        Sinon, on appelle self.removeAfterDrop.
        """
        try:
            self.parent.doAfterDrop()
        except:
            self.removeAfterDrop()

    def removeAfterDrop(self):
        """
        Après un drop, si les items viennent de la liste,
        on vide la liste globale ITEMS_IN_DRAGDROP.
        """
        global ITEMS_IN_DRAGDROP
        if ITEMS_IN_DRAGDROP[0] == self.num:
            ITEMS_IN_DRAGDROP = [-1]


class DoubleListDlg(QtWidgets.QDialog):
    """
    une fenêtre de dialogue toute prête avec 2 listes.
    """
    def __init__(self, parent=None, 
                 withComboBox=False, eleves2Freeplane=False, 
                 firstSelection=[], 
                 helpMessage=0, 
                 helpContextPage='', helpBaseUrl='prof', 
                 title=''):
        """
        mise en place de l'interface
        """
        super(DoubleListDlg, self).__init__(parent)

        self.main = parent
        self.setWindowTitle(title)

        self.withComboBox = withComboBox
        self.firstSelection = firstSelection
        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl

        helpMessages = [
            QtWidgets.QApplication.translate(
                'main', 
                'Place your selection in the list on the right.'), 
            QtWidgets.QApplication.translate(
                'main', 
                'Place and order your selection in the list on the right.'), 
            QtWidgets.QApplication.translate(
                'main', 
                'To select all, you can leave the empty right list.'), 
            ]
        if helpMessage == 0:
            helpMessage = ''
        elif helpMessage == 1:
            helpMessage = utils_functions.u(
                '<p align="center">{0}</p>').format(helpMessages[0])
        elif helpMessage == 2:
            helpMessage = utils_functions.u(
                '<p align="center">{0}</p>').format(helpMessages[1])
        elif helpMessage == 10:
            helpMessage = utils_functions.u(
                '<p align="center">{0}<br/>{1}</p>').format(
                    helpMessages[0], helpMessages[2])
        elif helpMessage == 11:
            helpMessage = utils_functions.u(
                '<p align="center">{0}<br/>{1}</p>').format(
                    helpMessages[1], helpMessages[2])
        helpMessageLabel = QtWidgets.QLabel(helpMessage)

        self.baseList = DragDropListWidget(parent=self, num=0)
        #self.baseList.setSortingEnabled(True)
        baseLayout = QtWidgets.QVBoxLayout()
        if withComboBox:
            self.baseComboBox = QtWidgets.QComboBox()
            self.baseComboBox.activated.connect(self.updateListes)
            baseLayout.addWidget(self.baseComboBox)
        baseLayout.addWidget(self.baseList)
        self.baseGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Base'))
        self.baseGroupBox.setLayout(baseLayout)

        self.selectionList = DragDropListWidget(parent=self, num=1)
        """
        upButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-up'), 
            QtWidgets.QApplication.translate('main', 'Up'))
        upButton.clicked.connect(self.doUp)
        downButton = QtWidgets.QPushButton(
            utils.doIcon('arrow-down'), 
            QtWidgets.QApplication.translate('main', 'Down'))
        downButton.clicked.connect(self.doDown)
        """
        selectionLayout = QtWidgets.QVBoxLayout()
        selectionLayout.addWidget(self.selectionList)
        #selectionLayout.addWidget(upButton)
        #selectionLayout.addWidget(downButton)
        self.selectionGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Selection'))
        self.selectionGroupBox.setLayout(selectionLayout)

        listesLayout = QtWidgets.QHBoxLayout()
        listesLayout.addWidget(self.baseGroupBox)
        listesLayout.addWidget(self.selectionGroupBox)
        listesGroupBox = QtWidgets.QGroupBox()
        listesGroupBox.setLayout(listesLayout)
        listesGroupBox.setFlat(True)

        if eleves2Freeplane:
            # réglages supplémentaires pour un export eleves2Freeplane :
            self.comboBoxGroup = QtWidgets.QComboBox()
            self.comboBoxGroup.addItem(
                QtWidgets.QApplication.translate('main', 'ActualGroup'))
            self.comboBoxGroup.addItem(
                QtWidgets.QApplication.translate('main', 'AllGroupsForEachSstudent'))
            self.comboBoxGroup.setCurrentIndex(0)
            self.communBLTCheckBox = QtWidgets.QCheckBox(
                QtWidgets.QApplication.translate('main', 'CompetencesBulletinShared'))
            self.communBLTCheckBox.setChecked(True)
            self.communOtherCheckBox = QtWidgets.QCheckBox(
                QtWidgets.QApplication.translate('main', 'CompetencesOtherShared'))
            self.communOtherCheckBox.setChecked(True)
            self.persoBLTCheckBox = QtWidgets.QCheckBox(
                QtWidgets.QApplication.translate('main', 'CompetencesPersoBLT'))
            self.persoBLTCheckBox.setChecked(True)
            self.persoOtherCheckBox = QtWidgets.QCheckBox(
                QtWidgets.QApplication.translate('main', 'CompetencesPersoOther'))
            vLayout = QtWidgets.QVBoxLayout()
            vLayout.addWidget(self.comboBoxGroup)
            vLayout.addWidget(self.communBLTCheckBox)
            vLayout.addWidget(self.communOtherCheckBox)
            vLayout.addWidget(self.persoBLTCheckBox)
            vLayout.addWidget(self.persoOtherCheckBox)
            eleves2FreeplaneGroupBox = QtWidgets.QGroupBox()
            eleves2FreeplaneGroupBox.setLayout(vLayout)
            eleves2FreeplaneGroupBox.setFlat(True)

        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        if helpMessage != '':
            mainLayout.addWidget(helpMessageLabel)
        mainLayout.addWidget(listesGroupBox)
        if eleves2Freeplane:
            mainLayout.addWidget(eleves2FreeplaneGroupBox)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        self.remplirBaseComboBox()
        self.remplirListes()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, self.helpBaseUrl)

    def remplirBaseComboBox(self):
        """
        à redéfinir en dérivant la classe
        """
        pass

    def remplirListes(self):
        """
        à redéfinir en dérivant la classe
        """
        pass

    def updateListes(self):
        """
        à redéfinir en dérivant la classe
        """
        pass

    def doAfterDrop(self):
        self.baseList.removeAfterDrop()
        self.selectionList.removeAfterDrop()
        if self.withComboBox:
            self.updateListes()

    def doUp(self):
        """
        on fait remonter les items sélectionnés
        """
        items = self.selectionList.selectedItems()
        for item in items:
            try:
                itemIndex = self.selectionList.indexFromItem(item).row()
                item = self.selectionList.takeItem(itemIndex)
                self.selectionList.insertItem(itemIndex - 1, item)
                self.selectionList.setCurrentItem(item)
            except:
                pass

    def doDown(self):
        """
        on fait redescendre les items sélectionnés
        """
        items = self.selectionList.selectedItems()
        itemsInvert = []
        for item in items:
            itemsInvert.insert(0, item)
        for item in itemsInvert:
            try:
                itemIndex = self.selectionList.indexFromItem(item).row()
                item = self.selectionList.takeItem(itemIndex)
                self.selectionList.insertItem(itemIndex + 1, item)
                self.selectionList.setCurrentItem(item)
            except:
                pass



class DoubleListWidget(QtWidgets.QWidget):
    """
    un widget avec 2 listes.
    """
    def __init__(self, parent=None, withComboBox=False, first=False, 
                id_groupe=-1, periode=0, id_profil=-1, id_template=0, 
                data={}, withCheckBox=False, 
                helpContextPage='', helpBaseUrl='prof'):
        """
        mise en place de l'interface
        """
        super(DoubleListWidget, self).__init__(parent)

        self.main = parent
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowMaximizeButtonHint)

        self.withComboBox = withComboBox
        self.first = first
        self.id_groupe = id_groupe
        self.id_profil = id_profil
        self.periode = periode
        self.id_template = id_template
        self.data = data
        self.mustRecord = False
        self.helpContextPage = helpContextPage
        self.helpBaseUrl = helpBaseUrl

        self.baseList = DragDropListWidget(parent=self, num=0, acceptMoves=False)
        #self.baseList.setSortingEnabled(True)
        baseLayout = QtWidgets.QVBoxLayout()
        if withComboBox:
            self.baseComboBox = QtWidgets.QComboBox()
            self.baseComboBox.activated.connect(self.updateListes)
            baseLayout.addWidget(self.baseComboBox)
        baseLayout.addWidget(self.baseList)
        self.baseGroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Base'))
        self.baseGroupBox.setLayout(baseLayout)

        self.selectionList = DragDropListWidget(parent=self, num=1, withCheckBox=withCheckBox)

        selectionLayout = QtWidgets.QVBoxLayout()
        selectionLayout.addWidget(self.selectionList)
        self.selectionGroupBox = QtWidgets.QGroupBox(QtWidgets.QApplication.translate('main', 'Selection'))
        self.selectionGroupBox.setLayout(selectionLayout)
        if withCheckBox:
            self.selectionList.itemClicked.connect(self.selectionClicked)
        listesLayout = QtWidgets.QHBoxLayout()
        listesLayout.addWidget(self.baseGroupBox)
        listesLayout.addWidget(self.selectionGroupBox)
        listesGroupBox = QtWidgets.QGroupBox()
        listesGroupBox.setLayout(listesLayout)
        listesGroupBox.setFlat(True)

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(listesGroupBox)
        self.setLayout(mainLayout)
        self.setMinimumWidth(400)

        self.remplirBaseComboBox()
        self.remplirListes()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, self.helpBaseUrl)

    def remplirBaseComboBox(self):
        """
        à redéfinir en dérivant la classe
        """
        pass

    def remplirListes(self):
        """
        à redéfinir en dérivant la classe
        """
        pass

    def updateListes(self):
        """
        à redéfinir en dérivant la classe
        """
        pass

    def doAfterDrop(self):
        self.baseList.removeAfterDrop()
        self.selectionList.removeAfterDrop()
        if self.withComboBox:
            self.updateListes()
        self.mustRecord = True

    def selectionClicked(self, item):
        self.mustRecord = True









###########################################################"
#   DES CLASSES SPÉCIALISÉES
###########################################################"




class ChooseStudentsWidget(DoubleListWidget):

    def remplirBaseComboBox(self):
        # on remplit la liste des classes
        self.baseComboBox.addItem(QtWidgets.QApplication.translate(
            'main', 'All the classes'))
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(commandLine_commun, db=self.main.db_commun)
        while query_commun.next():
            self.baseComboBox.addItem(query_commun.value(1))

    def remplirListes(self):
        self.baseList.clear()
        self.selectionList.clear()
        self.Avant = []
        self.Apres = []

        query_users = utils_db.query(self.main.db_users)
        commandLine_users = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_users, order=['Classe', 'NOM', 'Prenom'], query=query_users)
        for record in queryList:
            id_eleve = int(record[0])
            t = utils_functions.u('{0} {1} {2}').format(record[1], record[2], record[3])
            DejaLa = False
            item = QtWidgets.QListWidgetItem(t)
            item.setToolTip(utils_functions.u(record[0]))
            item.setData(QtCore.Qt.UserRole, id_eleve)
            if DejaLa == False and not(t in self.Apres):
                self.baseList.addItem(item)
            else:
                name_eleve = utils_functions.u('{0} {1}').format(record[1], record[2])
                self.Avant.append((id_eleve, name_eleve))

    def updateListes(self):
        """
        blablabla
        """
        self.baseList.clear()
        self.Apres = []
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())

        query_users = utils_db.query(self.main.db_users)
        commandLine_users = utils_db.q_selectAllFrom.format('eleves')
        if self.baseComboBox.currentIndex() == 0:
            queryList = utils_db.query2List(
                commandLine_users, order=['Classe', 'NOM', 'Prenom'], query=query_users)
        else:
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (self.baseComboBox.currentText(), )}, 
                order=['NOM', 'Prenom'], 
                query=query_users)
        for record in queryList:
            t = utils_functions.u('{0} {1} {2}').format(record[1], record[2], record[3])
            if not (t in self.Apres):
                item = QtWidgets.QListWidgetItem(t)
                item.setToolTip(utils_functions.u(record[0]))
                item.setData(QtCore.Qt.UserRole, int(record[0]))
                self.baseList.addItem(item)







class ChooseEleveWidget(DoubleListWidget):

    def remplirBaseComboBox(self):
        # on remplit la liste des classes
        self.baseComboBox.addItem(QtWidgets.QApplication.translate(
            'main', 'All the classes'))
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(commandLine_commun, db=self.main.db_commun)
        while query_commun.next():
            self.baseComboBox.addItem(query_commun.value(1))

    def remplirListes(self):
        import prof_groupes
        self.baseList.clear()
        self.selectionList.clear()
        self.Avant = []
        self.Apres = []
        matiereName = prof_groupes.matiereFromGroupe(self.main, self.id_groupe)
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())

        query_users = utils_db.query(self.main.db_users)
        commandLine_users = utils_db.q_selectAllFrom.format('eleves')
        if self.baseComboBox.currentIndex() == 0:
            queryList = utils_db.query2List(
                commandLine_users, order=['Classe', 'NOM', 'Prenom'], query=query_users)
        else:
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (self.baseComboBox.currentText(), )}, 
                order=['NOM', 'Prenom'], 
                query=query_users)
        for record in queryList:
            id_eleve = int(record[0])
            t = utils_functions.u('{0} {1} {2}').format(record[1], record[2], record[3])
            DejaLa = False
            commandLine_my = utils_functions.u(
                'SELECT groupe_eleve.* FROM groupe_eleve '
                'JOIN groupes ON groupes.id_groupe=groupe_eleve.id_groupe '
                'WHERE groupes.Matiere="{0}" AND groupe_eleve.id_eleve={1} '
                'AND groupe_eleve.ordre!=-1').format(matiereName, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
            if query_my.last():
                DejaLa = True
            item = QtWidgets.QListWidgetItem(t)
            item.setToolTip(utils_functions.u(record[0]))
            item.setData(QtCore.Qt.UserRole, id_eleve)
            if DejaLa == False and not(t in self.Apres):
                self.baseList.addItem(item)
            else:
                name_eleve = utils_functions.u('{0} {1}').format(record[1], record[2])
                self.Avant.append((id_eleve, name_eleve))

        commandLine_my = (
            'SELECT id_eleve, ordre FROM groupe_eleve '
            'WHERE id_groupe={0} AND ordre!=-1 ORDER BY ordre').format(self.id_groupe)
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_eleve = int(query_my.value(0))
            ordre = int(query_my.value(1))
            eleveName = utils_functions.eleveNameFromId(self.main, id_eleve)
            if not eleveName in self.Apres:
                item = QtWidgets.QListWidgetItem(eleveName)
                item.setToolTip(utils_functions.u(query_my.value(0)))
                item.setData(QtCore.Qt.UserRole, id_eleve)
                if ordre == 0:
                    item.setCheckState(QtCore.Qt.Unchecked)
                else:
                    item.setCheckState(QtCore.Qt.Checked)
                self.selectionList.addItem(item)

    def updateListes(self):
        """
        blablabla
        """
        self.baseList.clear()
        self.Apres = []
        for i in range(self.selectionList.count()):
            item = self.selectionList.item(i)
            self.Apres.append(item.text())

        query_users = utils_db.query(self.main.db_users)
        commandLine_users = utils_db.q_selectAllFrom.format('eleves')
        if self.baseComboBox.currentIndex() == 0:
            queryList = utils_db.query2List(
                commandLine_users, order=['Classe', 'NOM', 'Prenom'], query=query_users)
        else:
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (self.baseComboBox.currentText(), )}, 
                order=['NOM', 'Prenom'], 
                query=query_users)
        for record in queryList:
            t = utils_functions.u('{0} {1} {2}').format(record[1], record[2], record[3])
            if not (t in self.Apres):
                item = QtWidgets.QListWidgetItem(t)
                item.setToolTip(utils_functions.u(record[0]))
                item.setData(QtCore.Qt.UserRole, int(record[0]))
                self.baseList.addItem(item)





class ProfilBilansWidget(DoubleListWidget):

    def initBilans(self):
        # pour mettre en gras les bilans persos :
        boldFont = QtGui.QFont()
        boldFont.setBold(True)
        self.data['boldFont'] = boldFont
        # on cherche tous les bilans :
        self.data['BILANS'] = []
        commandLine_my = (
            'SELECT DISTINCT * '
            'FROM bilans '
            'WHERE id_competence<{0} '
            'AND id_competence>-2 '
            'ORDER BY id_competence, Name').format(utils.decalageBLT)
        query_my = utils_db.queryExecute(commandLine_my, db=self.main.db_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            bilanName = query_my.value(1)
            bilanLabel = query_my.value(2)
            id_competence = int(query_my.value(3))
            self.data['BILANS'].append((id_bilan, bilanName, bilanLabel, id_competence))

    def remplirListes(self, selected=[]):
        if not('BILANS' in self.data):
            return
        self.baseList.clear()
        self.selectionList.clear()
        forOrder = {}
        for (id_bilan, bilanName, bilanLabel, id_competence) in self.data['BILANS']:
            bilanText = utils_functions.u(
                '{0} \t [{1}]').format(bilanName, bilanLabel)
            item = QtWidgets.QListWidgetItem(bilanText)
            item.setToolTip(utils_functions.u(bilanLabel))
            item.setData(QtCore.Qt.UserRole, id_bilan)
            if id_competence < 0:
                item.setFont(self.data['boldFont'])
            if id_bilan in selected:
                forOrder[id_bilan] = item
            else:
                self.baseList.addItem(item)
        for id_bilan in selected:
            self.selectionList.addItem(forOrder[id_bilan])









class SelectItems(DoubleListWidget):

    def remplirListes(self):
        self.baseList.clear()
        self.selectionList.clear()
        self.initialList = []
        query_my = utils_db.query(self.main.db_my)
        # on récupère la liste des items liés au modèle :
        itemsInTemplate = []
        commandLine_my = utils_db.qs_prof_ItemsInTableau.format(self.id_template)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemsInTemplate.append(id_item)
        for id_item in itemsInTemplate:
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'items', 'id_item', id_item)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if query_my.first():
                itemName = query_my.value(1)
                itemLabel = query_my.value(2)
                itemText = utils_functions.u(
                    '{0} \t[{1}]').format(itemName, itemLabel)
                item = QtWidgets.QListWidgetItem(itemText)
                item.setToolTip(itemLabel)
                item.setData(QtCore.Qt.UserRole, (id_item, itemName))
                self.selectionList.addItem(item)
                self.initialList.append((id_item, itemName))
        # on récupère les autres items :
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'UPPER(Name)'), 
            query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            itemText = utils_functions.u(
                '{0} \t[{1}]').format(itemName, itemLabel)
            item = QtWidgets.QListWidgetItem(itemText)
            item.setToolTip(itemLabel)
            item.setData(QtCore.Qt.UserRole, (id_item, itemName))
            if not(id_item in itemsInTemplate):
                self.baseList.addItem(item)

        return True



