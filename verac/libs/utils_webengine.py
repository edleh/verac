# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    permet de gérer les différences entre QtWebKit et QtWebEngine.
    La classe MyWebEngineView remplace QWebView et QWebEngineView.
"""

# importation des modules utiles :
# from __future__ import division

import utils, utils_functions

WEB_ENGINE = ''
def changeWebEngine(newValue):
    global WEB_ENGINE
    if WEB_ENGINE == '':
        WEB_ENGINE = newValue
        if utils.ICI:
            print('*******************************************')
            print('utils_webengine.WEB_ENGINE:', WEB_ENGINE)
            print('*******************************************')

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    try:
        from PyQt5.QtWebEngineWidgets import QWebEngineView
        from PyQt5.QtWebEngineWidgets import QWebEnginePage
        changeWebEngine('WEBENGINE')
    except ImportError:
        from PyQt5.QtWebKitWidgets import QWebView as QWebEngineView
        from PyQt5.QtWebKitWidgets import QWebPage as QWebEnginePage
        changeWebEngine('WEBKIT')
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4.QtWebKit import QWebView as QWebEngineView
    from PyQt4.QtWebKit import QWebPage as QWebEnginePage
    changeWebEngine('WEBKIT')



###########################################################"
#   
###########################################################"


class MyWebEnginePage(QWebEnginePage):
    """
    """
    def __init__(self, parent=None, linksInBrowser=False, askOnLink=False):
        super(MyWebEnginePage, self).__init__(parent)
        self.linksInBrowser = linksInBrowser
        self.askOnLink = askOnLink
        self.parent = parent

    def acceptNavigationRequest(self, url,  _type, isMainFrame):
        if self.linksInBrowser:
            if _type == QWebEnginePage.NavigationTypeLinkClicked:
                if self.askOnLink:
                    message = QtWidgets.QApplication.translate(
                        'main', 'Open this link?')
                    message = utils_functions.u(
                        '{0}<br /><br /><b>{1}</b>').format(
                            message, url.toString())
                    if utils_functions.messageBox(
                        self.parent, 
                        level='question', 
                        message=message, 
                        buttons=['Open', 'Cancel']) \
                            != QtWidgets.QMessageBox.Open:
                                return False
                utils_functions.openInBrowser(url)
                return False
        return True

    def certificateError(self, certificateError):
        print(
            'certificateError:', 
            certificateError.errorDescription(), 
            certificateError.url(), 
            certificateError.isOverridable())
        return True


class MyWebEngineView(QWebEngineView):
    """
    linksInBrowser : les liens sont ouverts dans le navigateur
    askOnLink : confirmation avant d'ouvrir un lien
    """
    def __init__(self, parent=None, linksInBrowser=False, askOnLink=False):
        super(MyWebEngineView, self).__init__(parent)

        self.askOnLink = askOnLink
        self.html = ''
        self.TO_HTML = False
        if WEB_ENGINE == 'WEBENGINE':
            self.setPage(
                MyWebEnginePage(
                    self, 
                    linksInBrowser=linksInBrowser, 
                    askOnLink=askOnLink))
        else:
            if linksInBrowser:
                self.page().setLinkDelegationPolicy(
                    QWebEnginePage.DelegateAllLinks)
                self.linkClicked.connect(self.linkClickedWebKit)

    def linkClickedWebKit(self, url):
        if self.askOnLink:
            message = QtWidgets.QApplication.translate(
                'main', 'Open this link?')
            message = utils_functions.u(
                '{0}<br /><br /><b>{1}</b>').format(
                    message, url.toString())
            if utils_functions.messageBox(
                self, 
                level='question', 
                message=message, 
                buttons=['Open', 'Cancel']) \
                    != QtWidgets.QMessageBox.Open:
                        return
        utils_functions.openInBrowser(url)

    def toHtmlCallBack(self, data):
        #print('MyWebEngineView.callBack:', data)
        self.html = data
        self.TO_HTML = True

    def toHtml(self):
        self.html = ''
        self.TO_HTML = False
        if WEB_ENGINE == 'WEBENGINE':
            self.page().toHtml(self.toHtmlCallBack)
        else:
            self.html = self.page().mainFrame().toHtml()
            self.TO_HTML = True
        while not(self.TO_HTML):
            QtWidgets.QApplication.processEvents()
        return self.html





