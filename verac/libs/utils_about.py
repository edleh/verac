# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    La fenêtre "À propos"
"""

# importation des modules utiles :
import utils, utils_functions
import utils_webengine, utils_filesdirs, utils_markdown

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui


# quelques variables :
PROGNAMEHTML = utils.PROGNAMEHTML
PROGLABEL = utils.PROGLABEL
PROGLABELHTML = utils.PROGLABELHTML
PROGVERSION = utils.PROGVERSION
PROGYEAR = utils.PROGYEAR
PROGMAIL = utils.PROGMAIL
LICENCETITLE = utils.LICENCETITLE

# pous savoir si on appelle le module pour la première fois :
# (utilisé pour modifier la variable PROGVERSION)
FIRST = True



"""
****************************************************
    LA FENÊTRE À PROPOS
****************************************************
"""

class AboutDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, locale='', icon='./images/logo.png'):
        super(AboutDlg, self).__init__(parent)
        self.main = parent

        # mofification de la variable PROGVERSION :
        global PROGVERSION, FIRST
        if FIRST:
            PROGVERSION = utils.PROGVERSION
            FIRST = False

        # En-tête de la fenêtre :
        taille = 128
        logoLabel = QtWidgets.QLabel()
        logoLabel.setMaximumSize(taille, taille)
        logoLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        logoLabel.setPixmap(
            QtGui.QPixmap(icon).scaled(
                taille, 
                taille, 
                QtCore.Qt.KeepAspectRatio, 
                QtCore.Qt.SmoothTransformation))
        titleLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate(
            'main', 
            'About <b>{0} {1}</b>:'
            '<p>{2}</p>'
            '<p></p>').format(
                PROGNAMEHTML, PROGVERSION, PROGLABELHTML))
        titleLabel.setAlignment(
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        titleGroupBox = QtWidgets.QGroupBox()
        titleLayout = QtWidgets.QHBoxLayout()
        titleLayout.addWidget(logoLabel)
        titleLayout.addWidget(titleLabel)
        titleGroupBox.setLayout(titleLayout)

        # Zone d'affichage :
        tabWidget = QtWidgets.QTabWidget()
        mdFile = utils_functions.doLocale(
            locale, 'translations/README', '.md')
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName=mdFile, fileType='MD'),
            QtWidgets.QApplication.translate('main', 'About'))
        mdFile = utils_functions.doLocale(
            locale, 'translations/AUTHORS', '.md')
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName=mdFile, fileType='MD'),
            QtWidgets.QApplication.translate('main', 'Authors'))
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName='COPYING'),
            QtWidgets.QApplication.translate('main', 'License'))
        mdFile = utils_functions.doLocale(
            locale, 'translations/CREDITS', '.md')
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName=mdFile, fileType='MD'),
            QtWidgets.QApplication.translate('main', 'Credits'))

        # Les boutons :
        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('close', ))
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleGroupBox)
        mainLayout.addWidget(tabWidget)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)
        self.setWindowTitle(
            QtWidgets.QApplication.translate(
                'main', 'About {0}').format(utils.PROGNAME))


class FileViewTab(QtWidgets.QWidget):
    """
    pour afficher un fichier txt, html ou md.
    """
    def __init__(self, parent=None, fileName='', fileType='TXT'):
        super(FileViewTab, self).__init__(parent)
        self.main = parent
        # mise en place du conteneur :
        if fileType in ('HTML', 'MD'):
            widget = utils_webengine.MyWebEngineView(
                self, linksInBrowser=True)
            container = QtWidgets.QAbstractScrollArea()
            vBoxLayout = QtWidgets.QVBoxLayout()
            vBoxLayout.addWidget(widget)
            container.setLayout(vBoxLayout)
        else:
            widget = container = QtWidgets.QTextEdit()
            widget.setReadOnly(True)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addWidget(container)
        self.setLayout(mainLayout)
        # ouverture du fichier :
        if fileType == 'HTML':
            fileName = utils_functions.u(
                '{0}/{1}').format(self.main.beginDir, fileName)
            url = QtCore.QUrl().fromLocalFile(fileName)
            widget.load(url)
        elif fileType == 'MD':
            outFileName = utils_markdown.md2html(self.main, fileName)
            url = QtCore.QUrl().fromLocalFile(outFileName)
            widget.load(url)
        else:
            fileContent = utils_filesdirs.readTextFile(fileName)
            widget.setPlainText(fileContent)




"""
****************************************************
    AUTRES TRUCS
****************************************************
"""

def bugReport(main):
    """
    Faire un rapport de bug.
    On affiche la page d'explications sur le wiki
    et on ouvre le dossier config (contenant le fichier verac.log).
    """
    dialog = BugReportDlg(parent=main, helpPage=utils.HELPPAGE_BEGIN + 'help-bug.html')
    dialog.exec_()

class BugReportDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, helpPage=''):
        super(BugReportDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(utils.PROGNAME)

        self.helpPage = helpPage

        messageTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate(
                    'main', 'Make a bug report'))
        message = QtWidgets.QApplication.translate(
            'main',
            '<p align="center">Copy and paste the contents of the log file shown below'
            '<br/>or open configuration file.'
            '<br/>See the help page for further explanation.</p>')
        labelHelp = QtWidgets.QLabel(
            utils_functions.u('{0}{1}').format(messageTitle, message))

        # le TextEdit pour afficher:
        self.textEdit = QtWidgets.QTextEdit()
        self.textEdit.setReadOnly(True)

        #le bouton OK:
        okButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), 
            QtWidgets.QApplication.translate('main', 'Ok'))
        okButton.clicked.connect(self.accept)
        #le bouton aide:
        helpButton = QtWidgets.QPushButton(
            utils.doIcon('help'), 
            QtWidgets.QApplication.translate('main', 'Help'))
        helpButton.setShortcut(
            QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        helpButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Opens contextual help in your browser'))
        helpButton.clicked.connect(self.contextHelp)
        #le bouton ouvrir le dossier:
        openDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'), 
            QtWidgets.QApplication.translate('main', 'Configuration folder'))
        openDirButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Open the configuration folder of VERAC'))
        openDirButton.clicked.connect(self.openConfigDir)

        #une boîte des boutons:
        buttonBox = QtWidgets.QDialogButtonBox()
        buttonBox.addButton(okButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(helpButton, QtWidgets.QDialogButtonBox.ActionRole)
        buttonBox.addButton(openDirButton, QtWidgets.QDialogButtonBox.ActionRole)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(labelHelp, 1, 0)
        grid.addWidget(self.textEdit, 2, 0)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.showLogFile()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openInBrowser(self.helpPage)

    def showLogFile(self):
        self.textEdit.clear()
        utils.changeLogFile(True)
        utils.LOGFILE.flush()
        theFileName = utils.LOGFILENAME
        inFile = QtCore.QFile(theFileName)
        if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(inFile)
            stream.setCodec('UTF-8')
            self.textEdit.setPlainText(stream.readAll())
            inFile.close()
        else:
            utils_functions.myPrint('error in showLogFile')

    def openConfigDir(self):
        dirName = QtCore.QDir(self.main.localConfigDir).absolutePath()
        import utils_filesdirs
        utils_filesdirs.openDir(dirName)


