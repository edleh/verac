# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    La fenêtre de configuration du logiciel.
"""

# importation des modules utiles :
from __future__ import division
import os

import utils, utils_functions, utils_db, utils_calculs
import utils_markdown

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



TITLE_STYLE = '<p align="center"><b>{0}</b></p>'

"""
****************************************************
    FENÊTRE PRINCIPALE DU CONFIGURATION
****************************************************
"""

class ConfigDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, page=''):
        super(ConfigDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Settings'))
        self.helpContextPage = 'parametres'

        # la liste des pages à gauche :
        ICON_SIZE = utils.STYLE['PM_LargeIconSize'] * 3
        if self.main.height() < ICON_SIZE * 4:
            ICON_SIZE = ICON_SIZE // 2
        self.contentsWidget = QtWidgets.QListWidget()
        self.contentsWidget.setFixedWidth(ICON_SIZE + 100)
        self.contentsWidget.setSpacing(6)
        self.contentsWidget.setViewMode(QtWidgets.QListView.IconMode)
        self.contentsWidget.setIconSize(QtCore.QSize(ICON_SIZE, ICON_SIZE))
        self.contentsWidget.setGridSize(
            QtCore.QSize(ICON_SIZE + 70, ICON_SIZE + 40))
        self.contentsWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.contentsWidget.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        # des boutons :
        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('apply', 'reset', 'help', 'close'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]
        self.buttonsList['apply'].clicked.connect(self.doApply)
        self.buttonsList['reset'].clicked.connect(self.doReset)

        # affichage des pages :
        self.pagesWidget = QtWidgets.QStackedWidget()
        pagesIndex = {}

        title = QtWidgets.QApplication.translate(
            'main', 'Folders')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('folder'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        pagesIndex['Dirs'] = self.pagesWidget.addWidget(
            DirsPage(self))

        if self.main.me['userMode'] != 'admin':
            title = QtWidgets.QApplication.translate(
                'main', 'Items calculations')
            listWidgetItem = QtWidgets.QListWidgetItem(
                title, self.contentsWidget)
            listWidgetItem.setIcon(utils.doIcon('configure-calculs-items'))
            listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
            pagesIndex['CalculsItems'] = self.pagesWidget.addWidget(
                CalculsItemsPage(self))

        title = QtWidgets.QApplication.translate(
            'main', 'Balances calculations')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('configure-calculs-bilans'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        pagesIndex['CalculsBilans'] = self.pagesWidget.addWidget(
            CalculsBilansPage(self))

        if self.main.me['userMode'] != 'admin':
            title = QtWidgets.QApplication.translate(
                'main', 'Notes calculations')
            listWidgetItem = QtWidgets.QListWidgetItem(
                title, self.contentsWidget)
            listWidgetItem.setIcon(utils.doIcon('configure-calculs-notes'))
            listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
            pagesIndex['CalculsNotes'] = self.pagesWidget.addWidget(
                CalculsNotesPage(self))

        title = QtWidgets.QApplication.translate(
            'main', 'Keyboard')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('configure-keyboard'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        pagesIndex['Keys'] = self.pagesWidget.addWidget(
            KeysPage(self))

        if self.main.me['userMode'] != 'admin':
            title = QtWidgets.QApplication.translate(
                'main', 'ToolBar')
            listWidgetItem = QtWidgets.QListWidgetItem(
                title, self.contentsWidget)
            listWidgetItem.setIcon(utils.doIcon('configure-toolbar'))
            listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
            pagesIndex['ToolBar'] = self.pagesWidget.addWidget(
                ToolBarPage(self))

        if self.main.me['userMode'] == 'admin':
            title = QtWidgets.QApplication.translate(
                'main', 'Pdf')
            listWidgetItem = QtWidgets.QListWidgetItem(
                title, self.contentsWidget)
            listWidgetItem.setIcon(utils.doIcon('extension-pdf'))
            listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
            pagesIndex['Pdf'] = self.pagesWidget.addWidget(
                PdfPage(self))

        title = QtWidgets.QApplication.translate(
            'main', 'LAN')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('configure-lan'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        pagesIndex['Lan'] = self.pagesWidget.addWidget(
            LanPage(self))

        title = QtWidgets.QApplication.translate(
            'main', 'Others')
        listWidgetItem = QtWidgets.QListWidgetItem(
            title, self.contentsWidget)
        listWidgetItem.setIcon(utils.doIcon('configure-other'))
        listWidgetItem.setTextAlignment(QtCore.Qt.AlignHCenter)
        pagesIndex['Others'] = self.pagesWidget.addWidget(
            OtherPage(self))

        verticalLayout = QtWidgets.QVBoxLayout()
        verticalLayout.addWidget(self.pagesWidget)
        verticalLayout.addLayout(buttonsLayout)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addWidget(self.contentsWidget)
        mainLayout.addLayout(verticalLayout)
        self.setLayout(mainLayout)

        self.contentsWidget.itemClicked.connect(self.changePage)
        if page in pagesIndex:
            self.previous = pagesIndex[page]
        else:
            self.previous = 0
        self.contentsWidget.setCurrentRow(self.previous)
        self.pagesWidget.setCurrentIndex(self.previous)
        mustShowButtons = True
        if type(self.pagesWidget.currentWidget()) in (DirsPage, PdfPage):
            mustShowButtons = False
        self.buttonsList['apply'].setVisible(mustShowButtons)
        self.buttonsList['reset'].setVisible(mustShowButtons)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage)

    def doApply(self):
        self.pagesWidget.currentWidget().doApply()

    def doReset(self):
        self.pagesWidget.currentWidget().doReset()

    def accept(self):
        if self.testChange() == False:
            return
        QtWidgets.QDialog.accept(self)

    def changePage(self, current):
        if self.testChange():
            newPage = self.contentsWidget.row(current)
            self.pagesWidget.setCurrentIndex(newPage)
            mustShowButtons = True
            if type(self.pagesWidget.currentWidget()) in (DirsPage, PdfPage):
                mustShowButtons = False
            self.buttonsList['apply'].setVisible(mustShowButtons)
            self.buttonsList['reset'].setVisible(mustShowButtons)
            if self.pagesWidget.widget(self.previous).isModified:
                self.pagesWidget.widget(self.previous).reInit()
            self.previous = self.contentsWidget.row(current)
        else:
            self.contentsWidget.setCurrentRow(self.previous)

    def testChange(self):
        if self.pagesWidget.currentWidget().isModified:
            # On prévient que les réglages ont été modifés :
            m1 = QtWidgets.QApplication.translate(
                'main', 
                'If you leave this page without clicking'
                '<br/>on the button <b>Apply</b>,'
                '<br/>the modifications will not be recorded.')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p>{1}</p>'
                '<p></p>').format(utils.SEPARATOR_LINE, m1)
            msgBox = QtWidgets.QMessageBox(
                QtWidgets.QMessageBox.Warning,
                utils.PROGNAME, message,
                QtWidgets.QMessageBox.NoButton, 
                self.main)
            msgBox.addButton(
                QtWidgets.QApplication.translate('main', 'Continue'), 
                QtWidgets.QMessageBox.AcceptRole)
            msgBox.addButton(
                QtWidgets.QApplication.translate('main', 'Cancel'), 
                QtWidgets.QMessageBox.RejectRole)
            if msgBox.exec_() == QtWidgets.QMessageBox.RejectRole:
                return False
        return True



"""
****************************************************
    RÉPERTOIRES
****************************************************
"""

class DirsPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(DirsPage, self).__init__(parent)
        self.main = parent.main
        self.isModified = False

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'FOLDERS USED BY VERAC'))
        titleLabel = QtWidgets.QLabel(title)

        # icone et texte des boutons :
        openDirText = QtWidgets.QApplication.translate(
            'main', 'Open folder')
        selectDirText = QtWidgets.QApplication.translate(
            'main', 'Select another folder')
        resetDirText = QtWidgets.QApplication.translate(
            'main', 'Reset')

        filesDirLabelText = utils_functions.u('<b>{0}</b>').format(
            QtWidgets.QApplication.translate(
                'main', 'Files folder'))
        filesDirLabel = QtWidgets.QLabel(filesDirLabelText)
        self.filesDirEdit = QtWidgets.QLineEdit()
        self.filesDirEdit.setReadOnly(True)
        dirName = utils_functions.u(
            QtCore.QDir(self.main.filesDir).absolutePath())
        dirName = os.sep.join(dirName.split('/'))
        self.filesDirEdit.setText(dirName)
        self.openFilesDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'), '')
        self.openFilesDirButton.setToolTip(openDirText)
        self.openFilesDirButton.clicked.connect(self.doOpenDir)
        selectFilesDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-development'), '')
        selectFilesDirButton.setToolTip(selectDirText)
        selectFilesDirButton.clicked.connect(self.doSelectFilesDir)

        configDirLabelText = utils_functions.u('<b>{0}</b>').format(
            QtWidgets.QApplication.translate(
                'main', 'Configuration folder'))
        configDirLabel = QtWidgets.QLabel(configDirLabelText)
        self.configDirEdit = QtWidgets.QLineEdit()
        self.configDirEdit.setReadOnly(True)
        dirName = utils_functions.u(
            QtCore.QDir(self.main.localConfigDir).absolutePath())
        dirName = os.sep.join(dirName.split('/'))
        self.configDirEdit.setText(dirName)
        self.openConfigDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'), '')
        self.openConfigDirButton.setToolTip(openDirText)
        self.openConfigDirButton.clicked.connect(self.doOpenDir)
        selectConfigDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-development'), '')
        selectConfigDirButton.setToolTip(selectDirText)
        selectConfigDirButton.clicked.connect(self.doSelectConfigDir)
        resetConfigDirButton = QtWidgets.QPushButton(
            utils.doIcon('edit-undo'), '')
        resetConfigDirButton.setToolTip(resetDirText)
        resetConfigDirButton.clicked.connect(self.doResetConfigDir)

        tempDirLabelText = utils_functions.u('<b>{0}</b>').format(
            QtWidgets.QApplication.translate(
                'main', 'Temporary folder'))
        tempDirLabel = QtWidgets.QLabel(tempDirLabelText)
        self.tempDirEdit = QtWidgets.QLineEdit()
        self.tempDirEdit.setReadOnly(True)
        dirName = utils_functions.u(
            QtCore.QDir(self.main.tempPath).absolutePath())
        dirName = os.sep.join(dirName.split('/'))
        self.tempDirEdit.setText(dirName)
        self.openTempDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'), '')
        self.openTempDirButton.setToolTip(openDirText)
        self.openTempDirButton.clicked.connect(self.doOpenDir)

        softDirLabelText = utils_functions.u('<b>{0}</b>').format(
            QtWidgets.QApplication.translate(
                'main', 'Software install folder'))
        softDirLabel = QtWidgets.QLabel(softDirLabelText)
        self.softDirEdit = QtWidgets.QLineEdit()
        self.softDirEdit.setReadOnly(True)
        dirName = utils_functions.u(
            QtCore.QDir(self.main.beginDir).absolutePath())
        dirName = os.sep.join(dirName.split('/'))
        self.softDirEdit.setText(dirName)
        self.openSoftDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'), '')
        self.openSoftDirButton.setToolTip(openDirText)
        self.openSoftDirButton.clicked.connect(self.doOpenDir)

        grid = QtWidgets.QGridLayout()
        grid.addWidget(QtWidgets.QLabel(),          1, 0)
        grid.addWidget(filesDirLabel,               10, 0)
        grid.addWidget(self.filesDirEdit,           11, 0, 1, 4)
        grid.addWidget(self.openFilesDirButton,     11, 4)
        if utils.lanFilesDir == '':
            grid.addWidget(selectFilesDirButton,     12, 4)
        grid.addWidget(QtWidgets.QLabel(),          13, 0)
        grid.addWidget(configDirLabel,              20, 0)
        grid.addWidget(self.configDirEdit,          21, 0, 1, 4)
        grid.addWidget(self.openConfigDirButton,    21, 4)
        if utils.lanConfigDir == '':
            grid.addWidget(selectConfigDirButton,   23, 4)
            grid.addWidget(resetConfigDirButton,    23, 3)
        grid.addWidget(QtWidgets.QLabel(),          24, 0)
        grid.addWidget(tempDirLabel,                30, 0)
        grid.addWidget(self.tempDirEdit,            31, 0, 1, 4)
        grid.addWidget(self.openTempDirButton,      31, 4)
        grid.addWidget(QtWidgets.QLabel(),          32, 0)
        grid.addWidget(softDirLabel,                40, 0)
        grid.addWidget(self.softDirEdit,            41, 0, 1, 4)
        grid.addWidget(self.openSoftDirButton,      41, 4)

        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(grid)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)
        mainLayout.addWidget(groupBox)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)

    def reInit(self):#VIDE
        """
        juste pour compatibilité
        """

    def doOpenDir(self):
        if self.sender() == self.openFilesDirButton:
            theDir = self.main.filesDir
        elif self.sender() == self.openConfigDirButton:
            theDir = self.main.localConfigDir
        elif self.sender() == self.openTempDirButton:
            theDir = self.main.tempPath
        elif self.sender() == self.openSoftDirButton:
            theDir = self.main.beginDir
        theDir = QtCore.QDir(theDir).absolutePath()
        import utils_filesdirs
        utils_filesdirs.openDir(theDir)

    def doSelectFilesDir(self):
        """
        aaa
        """
        directory = utils_functions.u(
            QtCore.QDir(self.main.filesDir).absolutePath())
        title = QtWidgets.QApplication.translate(
            'main', 'Select the folder')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            title, 
            directory, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        import utils_filesdirs
        destFile = utils_functions.u('{0}/{1}').format(
            directory, 
            QtCore.QFileInfo(self.main.dbFile_my).fileName())
        utils_filesdirs.removeAndCopy(
            self.main.dbFile_my, destFile)
        self.main.filesDir = directory
        utils_db.changeInConfigTable(
            self.main, 
            self.main.db_localConfig, 
            {'FilesDir': (0, directory)})
        directoryText = os.sep.join(directory.split('/'))
        self.filesDirEdit.setText(directoryText)
        self.main.dbFile_my = utils_functions.u(
            '{0}/{1}.sqlite').format(directory, self.main.dbName_my)

    def doSelectConfigDir(self):
        """
        aaa
        """
        directory = QtCore.QDir(self.main.localConfigDir).absolutePath()
        directory = utils_functions.u('{0}/..').format(directory)
        title = QtWidgets.QApplication.translate(
            'main', 'Select the folder')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            title, 
            directory, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        import utils_export
        import utils_filesdirs
        fileName = utils_functions.u(
            '{0}/files/personal.csv').format(self.main.beginDir)
        theList = [
            ['TEXT', 'NUMBER', 'TEXT'], 
            ['key_name', 'value_int', 'value_text'], 
            ['homeDirPath', 0, directory]]
        ok = utils_export.exportList2csv(self.main, theList, fileName, msgFin=False)
        # création du dossier .Verac :
        utils_filesdirs.createDirs(directory, '.Verac')
        # copie du dossier .Verac (configuration) :
        utils_filesdirs.copyDir(
            self.main.localConfigDir, 
            utils_functions.u('{0}/.Verac').format(directory))
        if ok:
            self.main.localConfigDir = directory
            directoryText = os.sep.join(directory.split('/'))
            self.configDirEdit.setText(directoryText)
            utils_functions.afficheMsgFin(self.main, timer=5)
        else:
            fileName = utils_functions.u(
                '{0}/personal.csv').format(directory)
            utils_export.exportList2csv(self.main, theList, fileName, msgFin=False)
            badDir = utils_functions.u(
                '{0}/files').format(self.main.beginDir)
            m0 = QtWidgets.QApplication.translate(
                'main', 
                'The "personal.csv" file could not be saved '
                'in the <b>{0}</b> folder.')
            m0 = utils_functions.u(m0).format(badDir)
            m1 = QtWidgets.QApplication.translate(
                'main', 
                'This folder may be write protected.')
            m2 = QtWidgets.QApplication.translate(
                'main', 
                'A copy of the file "personal.csv" was saved '
                'in the folder you selected.')
            m3 = QtWidgets.QApplication.translate(
                'main', 
                'So you can place it in the correct folder '
                'to resolve the problem.')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p>{1}<br/>{2}</p>'
                '<p>{3}<br/>{4}</p>').format(utils.SEPARATOR_LINE, m0, m1, m2, m3)
            dialog = utils_functions.MessageWithHelpPageDlg(
                parent=self, 
                level='critical', 
                message=message, 
                helpContextPage='parametres')
            dialog.exec_()
        dirName = QtCore.QDir(directory).absolutePath()
        utils_filesdirs.openDir(dirName)

    def doResetConfigDir(self):
        """
        aaa
        """
        import utils_filesdirs
        directory = utils_filesdirs.createConfigAppDir(
            utils.PROGLINK, beginDir=self.main.beginDir, personal=False).canonicalPath()
        fileName = utils_functions.u(
            '{0}/files/personal.csv').format(self.main.beginDir)
        QtCore.QFile(fileName).remove()
        utils_filesdirs.copyDir(
            self.main.localConfigDir, 
            directory)
        self.main.localConfigDir = directory
        directoryText = os.sep.join(directory.split('/'))
        self.configDirEdit.setText(directoryText)




"""
****************************************************
    CALCULS DES ITEMS
****************************************************
"""

class CalculsItemsPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CalculsItemsPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 
                'HOW ARE TAKEN INTO ACCOUNT ITEMS FOR CALCULATING THE BALANCES'))
        titleLabel = QtWidgets.QLabel(title)

        modeCalculLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate(
                'main', '<b>Retained assessments:</b>'))
        self.modeCalculComboBox = QtWidgets.QComboBox()
        self.modeCalculComboBox.setMinimumWidth(150)
        self.modeCalculComboBox.activated.connect(self.doModified)
        self.modeCalculComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'All assessments'))
        self.modeCalculComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Best assessments'))
        self.modeCalculComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Last assessments'))

        modeCalculMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigModeCalculItems', nbLines=3)

        nbMaxEvalLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate(
                'main', '<b>Maximum number of valuations used:</b>'))
        self.nbMaxEvalSpinBox = QtWidgets.QSpinBox()
        self.nbMaxEvalSpinBox.setRange(0, 10)
        self.nbMaxEvalSpinBox.setSingleStep(1)
        self.nbMaxEvalSpinBox.valueChanged.connect(self.doModified)

        coefLastEvalLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate(
                'main', '<b>Give more weight to the last assessments:</b>'))
        self.coefLastEvalCheckBox = QtWidgets.QCheckBox()
        self.coefLastEvalCheckBox.clicked.connect(self.doModified)

        AbsenceLabel = QtWidgets.QLabel(QtWidgets.QApplication.translate(
            'main', '<b>Validate the item in case of absences:</b>'))
        espaceHtml = '&nbsp;'
        levelValidItemPCLabel = QtWidgets.QLabel(espaceHtml * 10 + \
            QtWidgets.QApplication.translate(
                'main', 
                '<b>Maximum percentage of absences to validate the item:</b>'))
        self.levelValidItemPCSpinBox = QtWidgets.QSpinBox()
        self.levelValidItemPCSpinBox.setRange(0, 100)
        self.levelValidItemPCSpinBox.setSingleStep(10)
        self.levelValidItemPCSpinBox.setSuffix('%')
        self.levelValidItemPCSpinBox.valueChanged.connect(self.doModified)

        levelValidItemNBLabel = QtWidgets.QLabel(espaceHtml * 10 + \
            QtWidgets.QApplication.translate(
                'main', '<b>Minimum number of ratings:</b>'))
        self.levelValidItemNBSpinBox = QtWidgets.QSpinBox()
        self.levelValidItemNBSpinBox.setRange(1, 10)
        self.levelValidItemNBSpinBox.setSingleStep(1)
        self.levelValidItemNBSpinBox.valueChanged.connect(self.doModified)

        opGroupBox = QtWidgets.QGroupBox()
        self.radioOR = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'OR'))
        self.radioAND = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'AND'))
        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(QtWidgets.QLabel(espaceHtml * 10 + '<b></b>'))
        hbox.addWidget(self.radioAND)
        hbox.addWidget(self.radioOR)
        hbox.addStretch(1)
        opGroupBox.setLayout(hbox)
        opGroupBox.setFlat(True)
        self.radioOR.clicked.connect(self.doModified)
        self.radioAND.clicked.connect(self.doModified)

        calculsMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigCalculItems', nbLines=3)

        # on agence tout ça :
        grid = QtWidgets.QVBoxLayout()
        grid.setContentsMargins(0, 0, 0, 0)
        grid.addWidget(titleLabel)

        vLayout = QtWidgets.QVBoxLayout()
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.setContentsMargins(0, 0, 0, 0)
        hLayout.addWidget(modeCalculLabel)
        hLayout.addWidget(self.modeCalculComboBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.setContentsMargins(0, 0, 0, 0)
        hLayout.addWidget(nbMaxEvalLabel)
        hLayout.addWidget(self.nbMaxEvalSpinBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.setContentsMargins(0, 0, 0, 0)
        hLayout.addWidget(coefLastEvalLabel)
        hLayout.addWidget(self.coefLastEvalCheckBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        vLayout.addWidget(modeCalculMdHelpView)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        grid.addWidget(groupBox)

        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(AbsenceLabel)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.setContentsMargins(0, 0, 0, 0)
        hLayout.addWidget(levelValidItemPCLabel)
        hLayout.addWidget(self.levelValidItemPCSpinBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.setContentsMargins(0, 0, 0, 0)
        hLayout.addWidget(opGroupBox)
        hLayout.addWidget(levelValidItemNBLabel)
        hLayout.addWidget(self.levelValidItemNBSpinBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        vLayout.addWidget(calculsMdHelpView)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        grid.addWidget(groupBox)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.addLayout(grid)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):
        """
        mise à jour de l'affichage d'après les paramètres
        contenus dans les bases de config etc
        """
        self.modeCalculComboBox.setCurrentIndex(utils.MODECALCUL)
        self.nbMaxEvalSpinBox.setValue(utils.NBMAXEVAL)
        self.coefLastEvalCheckBox.setChecked(utils.COEFLAST)
        self.levelValidItemPCSpinBox.setValue(utils.levelValidItemPC)
        self.levelValidItemNBSpinBox.setValue(utils.levelValidItemNB)
        if utils.levelValidItemOP == 0:
            self.radioOR.setChecked(True)
        else:
            self.radioAND.setChecked(True)
        self.doModified(modified=False)

    def doReset(self):
        """
        on remet les valeurs par défaut
        """
        self.modeCalculComboBox.setCurrentIndex(utils.MODECALCUL_initial)
        self.nbMaxEvalSpinBox.setValue(utils.NBMAXEVAL_initial)
        self.coefLastEvalCheckBox.setChecked(utils.COEFLAST_initial == 1)
        self.levelValidItemPCSpinBox.setValue(utils.levelValidItemPC_initial)
        self.levelValidItemNBSpinBox.setValue(utils.levelValidItemNB_initial)
        if utils.levelValidItemOP_initial == 0:
            self.radioOR.setChecked(True)
        else:
            self.radioAND.setChecked(True)
        self.doModified()

    def doModified(self, other=True, modified=True):
        self.isModified = modified
        self.parent.buttonsList['apply'].setEnabled(modified)

    def doApply(self):
        """
        on enregistre les modifications
        """
        deletes = [
            'MODECALCUL', 'NBMAXEVAL', 'COEFLAST', 
            'levelValidItemPC', 'levelValidItemNB', 'levelValidItemOP']
        changes = {}

        value = self.modeCalculComboBox.currentIndex()
        utils.changeModeCalcul(value)
        changes['MODECALCUL'] = (value, '')
        value = self.nbMaxEvalSpinBox.value()
        utils.changeNbMaxEval(value)
        changes['NBMAXEVAL'] = (value, '')
        value = 0
        if self.coefLastEvalCheckBox.isChecked():
            value = 1
        utils.changeCoefLast(value)
        changes['COEFLAST'] = (value, '')
        value = self.levelValidItemPCSpinBox.value()
        utils.changeLevelValidItemPC(value)
        changes['levelValidItemPC'] = (value, '')
        value = self.levelValidItemNBSpinBox.value()
        utils.changeLevelValidItemNB(value)
        changes['levelValidItemNB'] = (value, '')
        if self.radioOR.isChecked():
            value = 0
        else:
            value = 1
        utils.changeLevelValidItemOP(value)
        changes['levelValidItemOP'] = (value, '')

        utils_db.deleteFromConfigTable(
            self.main, 
            self.main.db_my, 
            deletes)
        utils_db.changeInConfigTable(
            self.main, 
            self.main.db_my, 
            changes, 
            delete=False)

        self.main.changeDBMyState()
        self.main.mustReflectChanges = True
        self.doModified(modified=False)
        utils_calculs.reinitCalculBilanDic()
        utils_functions.reinitFindColorDic()
        import prof_db
        prof_db.recalcMoyennesItems(self.main)




"""
****************************************************
    CALCULS DES BILANS
****************************************************
"""

class CalculsBilansPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CalculsBilansPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        if self.main.me['userMode'] == 'admin':
            title = utils_functions.u(TITLE_STYLE).format(
                QtWidgets.QApplication.translate(
                    'main', 
                    'HOW ARE CALCULATED THE BALANCES EVALUATED BY SEVERAL TEACHERS'))
        else:
            title = utils_functions.u(TITLE_STYLE).format(
                QtWidgets.QApplication.translate(
                    'main', 'HOW BALANCES ARE CALCULATED'))
        titleLabel = QtWidgets.QLabel(title)

        infOuEgal = QtWidgets.QApplication.translate('main', '<=')
        supOuEgal = QtWidgets.QApplication.translate('main', '>=')

        if self.main.me['userMode'] == 'admin':
            text = QtWidgets.QApplication.translate(
                'main', 'Threshold taken into account:')
            levelX_AdminLabel = QtWidgets.QLabel(
                utils_functions.u('<b>{0}</b>').format(text))
            text = QtWidgets.QApplication.translate('main', 'Assessments')
            levelX_NB_Label = QtWidgets.QLabel(
                utils_functions.u('{0} {1}').format(text, supOuEgal))
            self.levelX_NBSpinBox = QtWidgets.QSpinBox()
            self.levelX_NBSpinBox.setRange(1, 10)
            self.levelX_NBSpinBox.setSingleStep(1)
            self.levelX_NBSpinBox.valueChanged.connect(self.doModified)

        text = QtWidgets.QApplication.translate(
            'main', 'Minimum percentage of items assessed:')
        levelXLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(text))
        text = QtWidgets.QApplication.translate(
            'main', 'Threshold Level')
        levelA_Label = QtWidgets.QLabel(
            utils_functions.u('<b>{0} {1} :</b>').format(text, utils.affichages['A'][0]))
        levelB_Label = QtWidgets.QLabel(
            utils_functions.u('<b>{0} {1} :</b>').format(text, utils.affichages['B'][0]))

        text = utils_functions.u('{0} <').format(utils.affichages['X'][0])
        pcMaxX_Label = QtWidgets.QLabel(text)
        text = utils_functions.u('{0} {1}').format(
            utils.affichages['A'][0], supOuEgal)
        pcMinA_Label = QtWidgets.QLabel(text)
        text = utils_functions.u('{0}+{1} {2}').format(
            utils.affichages['A'][0], utils.affichages['B'][0], supOuEgal)
        pcMinAB_Label = QtWidgets.QLabel(text)
        pcMinAB_Label2 = QtWidgets.QLabel(text)
        text = utils_functions.u('{0} {1}').format(
            utils.affichages['C'][0], infOuEgal)
        pcMaxC_Label = QtWidgets.QLabel(text)
        pcMaxC_Label2 = QtWidgets.QLabel(text)
        text = utils_functions.u('{0} {1}').format(
            utils.affichages['D'][0], infOuEgal)
        pcMaxD_Label = QtWidgets.QLabel(text)
        pcMaxD_Label2 = QtWidgets.QLabel(text)

        self.levelXSpinBox = QtWidgets.QSpinBox()
        self.levelA_MinA_SpinBox = QtWidgets.QSpinBox()
        self.levelA_MinAB_SpinBox = QtWidgets.QSpinBox()
        self.levelA_MaxC_SpinBox = QtWidgets.QSpinBox()
        self.levelA_MaxD_SpinBox = QtWidgets.QSpinBox()
        self.levelB_MinAB_SpinBox = QtWidgets.QSpinBox()
        self.levelB_MaxC_SpinBox = QtWidgets.QSpinBox()
        self.levelB_MaxD_SpinBox = QtWidgets.QSpinBox()
        spinBoxs = (
            self.levelXSpinBox, 
            self.levelA_MinA_SpinBox, 
            self.levelA_MinAB_SpinBox, 
            self.levelA_MaxC_SpinBox, 
            self.levelA_MaxD_SpinBox, 
            self.levelB_MinAB_SpinBox, 
            self.levelB_MaxC_SpinBox, 
            self.levelB_MaxD_SpinBox, 
            )
        for spinBox in spinBoxs:
            spinBox.setRange(0, 100)
            spinBox.setSingleStep(5)
            spinBox.setSuffix('%')
            spinBox.valueChanged.connect(self.doModified)

        if self.main.me['userMode'] == 'admin':
            mdFile = 'translations/helpConfigCalculBilansAdmin'
        else:
            mdFile = 'translations/helpConfigCalculBilans'
        seuilsCalculsMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile=mdFile, nbLines=-2)

        grid = QtWidgets.QGridLayout()
        if self.main.me['userMode'] == 'admin':
            grid.addWidget(levelX_AdminLabel,           3, 0, 1, 1)
        else:
             grid.addWidget(levelXLabel,                3, 0, 1, 1)
        grid.addWidget(pcMaxX_Label,                    2, 4)
        grid.addWidget(self.levelXSpinBox,              3, 4)
        if self.main.me['userMode'] == 'admin':
            grid.addWidget(levelX_NB_Label,             2, 2)
            grid.addWidget(self.levelX_NBSpinBox,       3, 2)
        grid.addWidget(levelA_Label,                    5, 0)
        grid.addWidget(pcMinA_Label,                    4, 1)
        grid.addWidget(pcMinAB_Label,                   4, 2)
        grid.addWidget(pcMaxC_Label,                    4, 3)
        grid.addWidget(pcMaxD_Label,                    4, 4)
        grid.addWidget(self.levelA_MinA_SpinBox,        5, 1)
        grid.addWidget(self.levelA_MinAB_SpinBox,       5, 2)
        grid.addWidget(self.levelA_MaxC_SpinBox,        5, 3)
        grid.addWidget(self.levelA_MaxD_SpinBox,        5, 4)
        grid.addWidget(levelB_Label,                    9, 0)
        grid.addWidget(pcMinAB_Label2,                  8, 1)
        grid.addWidget(pcMaxC_Label2,                   8, 3)
        grid.addWidget(pcMaxD_Label2,                   8, 4)
        grid.addWidget(self.levelB_MinAB_SpinBox,       9, 1)
        grid.addWidget(self.levelB_MaxC_SpinBox,        9, 3)
        grid.addWidget(self.levelB_MaxD_SpinBox,        9, 4)
        grid.addWidget(seuilsCalculsMdHelpView,           30, 0, 1, 5)

        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(grid)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.setContentsMargins(0, 0, 0, 0)
        mainLayout.addWidget(titleLabel)
        mainLayout.addWidget(groupBox)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):
        """
        mise à jour de l'affichage d'après les paramètres
        contenus dans les bases de config etc
        """
        if self.main.me['userMode'] == 'admin':
            self.levelX_NBSpinBox.setValue(utils.levelX_NB)
        self.levelXSpinBox.setValue(utils.levelX)
        self.levelA_MinA_SpinBox.setValue(utils.levelA[0])
        self.levelA_MinAB_SpinBox.setValue(utils.levelA[1])
        self.levelA_MaxC_SpinBox.setValue(utils.levelA[2])
        self.levelA_MaxD_SpinBox.setValue(utils.levelA[3])
        self.levelB_MinAB_SpinBox.setValue(utils.levelB[0])
        self.levelB_MaxC_SpinBox.setValue(utils.levelB[1])
        self.levelB_MaxD_SpinBox.setValue(utils.levelB[2])
        self.doModified(modified=False)

    def doReset(self):
        """
        on remet les valeurs par défaut
        """
        if self.main.me['userMode'] == 'admin':
            self.levelX_NBSpinBox.setValue(utils.levelX_NB_initial)
        self.levelXSpinBox.setValue(utils.levelX_initial)
        self.levelA_MinA_SpinBox.setValue(utils.levelA_initial[0])
        self.levelA_MinAB_SpinBox.setValue(utils.levelA_initial[1])
        self.levelA_MaxC_SpinBox.setValue(utils.levelA_initial[2])
        self.levelA_MaxD_SpinBox.setValue(utils.levelA_initial[3])
        self.levelB_MinAB_SpinBox.setValue(utils.levelB_initial[0])
        self.levelB_MaxC_SpinBox.setValue(utils.levelB_initial[1])
        self.levelB_MaxD_SpinBox.setValue(utils.levelB_initial[2])
        self.doModified()

    def doModified(self, other=True, modified=True):
        self.isModified = modified
        self.parent.buttonsList['apply'].setEnabled(modified)

    def doApply(self):
        """
        on enregistre les modifications
        """
        deletes = [
            'levelX', 
            'levelA_0', 'levelA_1', 'levelA_2', 'levelA_3', 
            'levelB_0', 'levelB_1', 'levelB_2']
        changes = {}
        value = self.levelXSpinBox.value()
        utils.changeLevelX(value)
        changes['levelX'] = (value, '')
        value = self.levelA_MinA_SpinBox.value()
        utils.changeLevelA(0, value)
        changes['levelA_0'] = (value, '')
        value = self.levelA_MinAB_SpinBox.value()
        utils.changeLevelA(1, value)
        changes['levelA_1'] = (value, '')
        value = self.levelA_MaxC_SpinBox.value()
        utils.changeLevelA(2, value)
        changes['levelA_2'] = (value, '')
        value = self.levelA_MaxD_SpinBox.value()
        utils.changeLevelA(3, value)
        changes['levelA_3'] = (value, '')
        value = self.levelB_MinAB_SpinBox.value()
        utils.changeLevelB(0, value)
        changes['levelB_0'] = (value, '')
        value = self.levelB_MaxC_SpinBox.value()
        utils.changeLevelB(1, value)
        changes['levelB_1'] = (value, '')
        value = self.levelB_MaxD_SpinBox.value()
        utils.changeLevelB(2, value)
        changes['levelB_2'] = (value, '')

        if self.main.me['userMode'] == 'admin':
            import admin
            deletes.append('levelX_NB')
            value = self.levelX_NBSpinBox.value()
            utils.changeLevelX_NB(value)
            changes['levelX_NB'] = (value, '')
            utils_db.deleteFromConfigTable(
                self.main, 
                admin.db_admin, 
                deletes, 
                table='config_calculs')
            utils_db.changeInConfigTable(
                self.main, 
                admin.db_admin, 
                changes, 
                table='config_calculs', 
                delete=False)
        else:
            utils_db.deleteFromConfigTable(
                self.main, 
                self.main.db_my, 
                deletes)
            utils_db.changeInConfigTable(
                self.main, 
                self.main.db_my, 
                changes, 
                delete=False)
            self.main.changeDBMyState()
            self.main.mustReflectChanges = True
            utils_calculs.reinitCalculBilanDic()
        self.doModified(modified=False)




"""
****************************************************
    CALCULS DES NOTES
****************************************************
"""

class CalculsNotesPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CalculsNotesPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 
                'HOW NOTES ARE CALCULATED'))
        titleLabel = QtWidgets.QLabel(title)

        text = QtWidgets.QApplication.translate(
                'main', 'Calculation method:')
        modeCalculNotesLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(text))
        self.modeCalculNotesComboBox = QtWidgets.QComboBox()
        self.modeCalculNotesComboBox.setMinimumWidth(150)
        self.modeCalculNotesComboBox.activated.connect(
            self.modeCalculNotesChanged)
        self.modeCalculNotesComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Default'))
        self.modeCalculNotesComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'As DNB'))
        self.modeCalculNotesComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Custom'))

        labels = []
        self.spinBoxs = []
        self.oldValues = []
        for i in range(4):
            label = QtWidgets.QLabel(
                utils_functions.u('<b>{0}</b>').format(
                    utils.affichages[utils.itemsValues[i]][0]))
            labels.append(label)
            spinBox = QtWidgets.QSpinBox()
            spinBox.setRange(0, 100)
            spinBox.setSingleStep(1)
            spinBox.setSuffix('%')
            spinBox.valueChanged.connect(self.doModified)
            self.spinBoxs.append(spinBox)

        modeCalculNotesMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigModeCalculNotes')

        # precisionNotes :
        precisionNotesLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate(
                    'main', 'Precision of calculated notes:')))
        self.precisionNotesSpinBox = QtWidgets.QSpinBox()
        self.precisionNotesSpinBox.setRange(0, 5)
        self.precisionNotesSpinBox.setSingleStep(1)
        self.precisionNotesSpinBox.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 
                'Here you set the number of decimal places for notes.'))
        self.precisionNotesSpinBox.valueChanged.connect(self.doModified)

        # on agence tout ça :
        grid = QtWidgets.QVBoxLayout()
        grid.addWidget(titleLabel)
        vLayout = QtWidgets.QVBoxLayout()
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(modeCalculNotesLabel)
        hLayout.addWidget(self.modeCalculNotesComboBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        for i in range(4):
            hLayout.addWidget(labels[i])
            hLayout.addWidget(self.spinBoxs[i])
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(precisionNotesLabel)
        hLayout.addWidget(self.precisionNotesSpinBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        vLayout.addWidget(modeCalculNotesMdHelpView)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        grid.addWidget(groupBox)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(grid)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):# À FAIRE
        """
        mise à jour de l'affichage d'après les paramètres
        contenus dans les bases de config etc
        """
        self.modeCalculNotesComboBox.setCurrentIndex(
            utils.MODECALCULNOTES)
        self.oldValues = []
        enabled = (utils.MODECALCULNOTES > 1)
        for i in range(4):
            self.spinBoxs[i].setValue(utils.valeursEval[i])
            self.spinBoxs[i].setEnabled(enabled)
            self.oldValues.append(utils.valeursEval[i])
        #self.oldValues = [98, 68, 48, 18]
        self.precisionNotesSpinBox.setValue(
            utils.precisionNotes)
        self.doModified(modified=False)

    def doReset(self):
        """
        on remet les valeurs par défaut
        """
        self.modeCalculNotesComboBox.setCurrentIndex(
            utils.MODECALCULNOTES_initial)
        for i in range(4):
            self.spinBoxs[i].setValue(utils.valeursEval_initial[i])
            self.spinBoxs[i].setEnabled(False)
        self.precisionNotesSpinBox.setValue(
            utils.precisionNotes_initial)
        self.doModified()

    def doModified(self, other=True, modified=True):
        self.isModified = modified
        self.parent.buttonsList['apply'].setEnabled(modified)

    def modeCalculNotesChanged(self):
        value = self.modeCalculNotesComboBox.currentIndex()
        if value == 0:
            values = [100, 67, 33, 0]
        elif value == 1:
            values = [100, 80, 50, 20]
        else:
            values = self.oldValues
        enabled = (value > 1)
        for i in range(4):
            self.spinBoxs[i].setEnabled(enabled)
            self.spinBoxs[i].setValue(values[i])
        self.doModified()

    def doApply(self):
        """
        on enregistre les modifications
        """
        deletes = [
            'MODECALCULNOTES', 
            'valeursEval_0', 'valeursEval_1', 'valeursEval_2', 'valeursEval_3', 
            'precisionNotes']
        changes = {}

        value = self.modeCalculNotesComboBox.currentIndex()
        utils.changeModeCalculNotes(value)
        changes['MODECALCULNOTES'] = (value, '')

        if value > 1:
            for i in range(4):
                value = self.spinBoxs[i].value()
                utils.changeValeursEval(i, value)
                changes['valeursEval_{0}'.format(i)] = (value, '')

        value = self.precisionNotesSpinBox.value()
        utils.changePrecisionNotes(value)
        changes['precisionNotes'] = (value, '')

        utils_db.deleteFromConfigTable(
            self.main, 
            self.main.db_my, 
            deletes)
        utils_db.changeInConfigTable(
            self.main, 
            self.main.db_my, 
            changes, 
            delete=False)

        self.main.changeDBMyState()
        self.main.mustReflectChanges = True
        utils_calculs.reinitCalculBilanDic()
        import prof_db
        prof_db.recalcMoyennesItems(self.main)
        self.doModified(modified=False)



"""
****************************************************
    GESTION DU CLAVIER
****************************************************
"""

class KeysPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(KeysPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'USING THE KEYBOARD IN VERAC'))
        titleLabel = QtWidgets.QLabel(title)

        # les KeyEdit pour les choix des touches du clavier 
        # (2 listes de possibilités) :
        self.listEditsKeys = []
        i = 0
        for value in utils.itemsValues:
            editKey0 = KeyEdit(self, i)
            editKey1 = KeyEdit(self, i + 5)
            self.listEditsKeys.append((editKey0, editKey1))
            i += 1

        # affichage de l'aide :
        keys1MdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigKeys1', nbLines=7)

        # le titre :
        keysTitle = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'Keyboard Keys'))
        keysLabel = QtWidgets.QLabel(keysTitle)

        # On place dans une grille :
        keysGrid = QtWidgets.QGridLayout()
        keysGrid.addWidget(keysLabel, 0, 0, 1, 2)
        for i in range(utils.NB_COLORS + 1):
            keysGrid.addWidget(self.listEditsKeys[i][0], i + 1, 0)
            keysGrid.addWidget(self.listEditsKeys[i][1], i + 1, 1)
        keysGrid.addWidget(keys1MdHelpView, 6, 0, 1, 2)

        # pour choisir les lettres affichées dans l'interface :
        self.configLetters = []
        taille = 18
        i = 0
        for value in utils.itemsValues:
            iconLabel = ColorLabel(self, i, utils.affichages[value][2])
            i += 1
            editName = QtWidgets.QLineEdit()
            editLetter = QtWidgets.QLineEdit()
            validator = QtWidgets.QLabel()
            validator.setMaximumSize(taille, taille)
            validator.setAlignment(
                QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
            self.setOk(validator)
            self.configLetters.append(
                (value, iconLabel, editName, editLetter, validator))
        # on les connecte :
        for configLetter in self.configLetters:
            configLetter[2].textChanged.connect(self.doModified)
            configLetter[3].textChanged.connect(self.editLetterChanged)

        # affichage de l'aide :
        keys2MdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigKeys2', nbLines=7)

        # le titre :
        lettersTitle = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'Showing'))
        lettersLabel = QtWidgets.QLabel(lettersTitle)

        # On place dans une grille :
        lettersGrid = QtWidgets.QGridLayout()
        lettersGrid.addWidget(lettersLabel, 0, 0, 1, 4)
        for i in range(utils.NB_COLORS + 1):
            for j in range(4):
                lettersGrid.addWidget(self.configLetters[i][j + 1], i + 1, j)
        lettersGrid.addWidget(keys2MdHelpView, 6, 0, 1, 4)

        horizontalLayout = QtWidgets.QHBoxLayout()
        horizontalLayout.addLayout(lettersGrid, 1)
        horizontalLayout.addLayout(keysGrid, 1)

        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(horizontalLayout)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)
        mainLayout.addWidget(groupBox)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):
        """
        mise à jour de l'affichage d'après les paramètres
        contenus dans les bases de config etc
        """
        # pour le clavier :
        i = 0
        for value in utils.itemsValues:
            key = utils.affichages[value][4][0]
            keyText = utils.affichages[value][4][1]
            editKey = self.listEditsKeys[i][0]
            editKey.key = key
            editKey.keyText = keyText
            editKey.setText(keyText)
            key = utils.affichages[value][5][0]
            keyText = utils.affichages[value][5][1]
            editKey = self.listEditsKeys[i][1]
            editKey.key = key
            editKey.keyText = keyText
            editKey.setText(keyText)
            i += 1
        # pour l'affichage (couleurs et lettres) :
        for configLetter in self.configLetters:
            colorName = utils.affichages[configLetter[0]][2]
            configLetter[1].colorName = colorName
            configLetter[1].doColor()
            colorText = utils.affichages[configLetter[0]][1]
            configLetter[2].setText(colorText)
            letter = utils.affichages[configLetter[0]][0]
            configLetter[3].setText(letter)
            self.setOk(configLetter[4])
        #utils_functions.createPixmapsItems(self.main)
        self.doModified(modified=False)

    def doReset(self):
        """
        on remet les valeurs par défaut
        """
        # pour le clavier :
        i = 0
        for value in utils.itemsValues:
            key = utils.affichages_initial[value][4][0]
            keyText = utils.affichages_initial[value][4][1]
            editKey = self.listEditsKeys[i][0]
            editKey.key = key
            editKey.keyText = keyText
            editKey.setText(keyText)
            key = utils.affichages_initial[value][5][0]
            keyText = utils.affichages_initial[value][5][1]
            editKey = self.listEditsKeys[i][1]
            editKey.key = key
            editKey.keyText = keyText
            editKey.setText(keyText)
            i += 1
        # pour l'affichage (couleurs et lettres) :
        for configLetter in self.configLetters:
            colorName = utils.affichages_initial[configLetter[0]][2]
            configLetter[1].colorName = colorName
            configLetter[1].doColor()
            colorText = utils.affichages_initial[configLetter[0]][1]
            configLetter[2].setText(colorText)
            letter = utils.affichages_initial[configLetter[0]][0]
            configLetter[3].setText(letter)
            self.setOk(configLetter[4])
        #utils_functions.createPixmapsItems(self.main)
        self.doModified()

    def doModified(self, other=True, modified=True):
        self.isModified = modified
        self.parent.buttonsList['apply'].setEnabled(modified)

    def setOk(self, validator, ok=True):
        taille = 18
        if ok:
            iconFileName = 'dialog-ok-apply'
        else:
            iconFileName = 'dialog-cancel'
        validator.setPixmap(
            utils.doIcon(iconFileName, what='PIXMAP').scaled(
                taille, taille,
                QtCore.Qt.KeepAspectRatio,
                QtCore.Qt.SmoothTransformation))

    def editLetterChanged(self):
        ok = True
        letters = []
        badLetters = []
        for configLetter in self.configLetters:
            letter = configLetter[3].text()
            if len(letter) != 1:
                badLetters.append(letter)
            elif letter in letters:
                badLetters.append(letter)
            else:
                letters.append(letter)
        for configLetter in self.configLetters:
            validator = configLetter[4]
            letter = configLetter[3].text()
            if letter in badLetters:
                ok = False
                self.setOk(validator, ok=False)
            else:
                self.setOk(validator)
        if ok:
            self.doModified()

    def doApply(self):
        """
        on enregistre les modifications
        """
        # on met à jour l'affichage 
        # (touches du clavier, couleurs et lettres) :
        newAffichagesItems = {}
        i = 0
        for configLetter in self.configLetters:
            editKey = self.listEditsKeys[i][0]
            key0Affichage = (editKey.key, editKey.keyText)
            editKey = self.listEditsKeys[i][1]
            key1Affichage = (editKey.key, editKey.keyText)
            newAffichagesItems[configLetter[0]] = (
                configLetter[3].text(), 
                configLetter[2].text(), 
                configLetter[1].colorName, 
                key0Affichage, 
                key1Affichage, 
                '')
            i += 1
        utils.changeAffichages(newAffichagesItems)
        utils_functions.createPixmapsItems(self.main)

        # on enregistre dans la table config :
        deletes = []
        changes = {}
        for i in range(utils.NB_COLORS + 1):
            key_name = utils_functions.u('Key_{0}').format(i)
            deletes.append(key_name)
            key_name = utils_functions.u('Key_{0}').format(i + 5)
            deletes.append(key_name)
            key_name = utils_functions.u('Letter_{0}').format(i)
            deletes.append(key_name)
        for item in utils.itemsValues:
            key0Name = utils_functions.u('{0}_Key0').format(item)
            deletes.append(key0Name)
            key0Affichage = utils.affichages[item][4]
            if key0Affichage != utils.affichages_initial[item][4]:
                changes[key0Name] = (int(key0Affichage[0]), key0Affichage[1])
            key1Name = utils_functions.u('{0}_Key1').format(item)
            deletes.append(key1Name)
            key1Affichage = utils.affichages[item][5]
            if key1Affichage != utils.affichages_initial[item][5]:
                changes[key1Name] = (int(key1Affichage[0]), key1Affichage[1])
            letterName = utils_functions.u('{0}_Letter').format(item)
            deletes.append(letterName)
            letterAffichage = utils.affichages[item][0]
            if letterAffichage != utils.affichages_initial[item][0]:
                changes[letterName] = ('', letterAffichage)
            nameName = utils_functions.u('{0}_Name').format(item)
            deletes.append(nameName)
            nameAffichage = utils.affichages[item][1]
            if nameAffichage != utils.affichages_initial[item][1]:
                changes[nameName] = ('', nameAffichage)
            colorName = utils_functions.u('{0}_Color').format(item)
            deletes.append(colorName)
            colorAffichage = utils.affichages[item][2]
            if colorAffichage != utils.affichages_initial[item][2]:
                changes[colorName] = ('', colorAffichage)

        if self.main.me['userMode'] == 'admin':
            import admin
            utils_db.deleteFromConfigTable(
                self.main, 
                admin.db_admin, 
                deletes, 
                table='config_calculs')
            utils_db.changeInConfigTable(
                self.main, 
                admin.db_admin, 
                changes, 
                table='config_calculs', 
                delete=False)
        else:
            utils_db.deleteFromConfigTable(
                self.main, self.main.db_my, deletes)
            utils_db.changeInConfigTable(
                self.main, self.main.db_my, changes, delete=False)
        if self.main.me['userMode'] != 'admin':
            self.main.changeDBMyState()
        else:
            # on enregistre aussi dans la table 
            # config_affichage de la base commun :
            import admin
            admin.updateTableLettersInCommun(
                self.main, deletes, changes)
            self.main.mustReflectChanges = True
        self.doModified(modified=False)


class KeyEdit(QtWidgets.QLineEdit):
    """
    un QLineEdit pour modifier les touches du clavier utilisées pour évaluer
    """
    def __init__(self, parent=None, index=0, key=QtCore.Qt.Key_V, keyText='V'):
        super(KeyEdit, self).__init__(parent)
        self.parent = parent
        self.index = index
        self.key = key
        self.keyText = keyText
        self.setText(keyText)
        text = QtWidgets.QApplication.translate(
            'main', 'Select and Press a Key for change')
        self.setToolTip(text)

    def keyPressEvent(self, event):
        """
        on vérifie si la touche n'est pas déjà utilisée
        """
        listKeys = []
        for (editKey0, editKey1) in self.parent.listEditsKeys:
            listKeys.append(editKey0.key)
            listKeys.append(editKey1.key)
        if event.key() in listKeys:
            return
        upperKey = event.text().upper()
        if upperKey != '':
            self.key = event.key()
            self.keyText = upperKey
            self.setText(self.keyText)
            self.parent.doModified()


class ColorLabel(QtWidgets.QLabel):
    """
    un QLabel pour modifier les couleurs d'affichage
    """
    def __init__(self, parent=None, index=0, colorName='#00ff00'):
        super(ColorLabel, self).__init__(parent)
        self.parent = parent
        self.index = index
        self.colorName = colorName
        self.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.doColor()
        text = QtWidgets.QApplication.translate(
            'main', 'Click for change')
        self.setToolTip(text)

    def doColor(self):
        color = QtGui.QColor(self.colorName)
        if self.index == utils.NB_COLORS:
            pixmap = utils_functions.createPixmap(
                self.parent.main, color=color, cross=True)
        else:
            pixmap = utils_functions.createPixmap(
                self.parent.main, color=color)
        self.setPixmap(pixmap)

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            text = QtWidgets.QApplication.translate(
                'main', 'Select color')
            color = QtGui.QColor(self.colorName)
            newColor = QtWidgets.QColorDialog.getColor(
                color, self, text)
            if newColor.isValid():
                self.colorName = newColor.name()
                self.doColor()
                self.parent.doModified()
        else:
            QtWidgets.QLabel.mousePressEvent(self, event)



"""
****************************************************
    BARRE D'OUTILS
****************************************************
"""

class ToolBarPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(ToolBarPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'TOOLBAR CONFIGURATION'))
        titleLabel = QtWidgets.QLabel(title)

        # configuration des actions visibles dans les toolBars :
        groupBoxVisibleActions = QtWidgets.QGroupBox()
        title = QtWidgets.QApplication.translate(
            'main', 'VisibleActions')
        titleLabel2 = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(title))
        self.toolbarActionsList = QtWidgets.QListWidget()
        self.toolbarActionsList.itemClicked.connect(self.doModified)
        boldFont = QtGui.QFont()
        boldFont.setBold(True)
        # actions fréquemment utilisées :
        item = QtWidgets.QListWidgetItem(
            QtWidgets.QApplication.translate(
                'main', 'FrequentlyUsedActions:'))
        item.setFont(boldFont)
        self.toolbarActionsList.addItem(item)
        for action in self.main.frequentlyUsedActions:
            item = QtWidgets.QListWidgetItem(
                action[0].icon(), action[0].text().replace('&', ''))
            item.setData(QtCore.Qt.UserRole, action)
            self.toolbarActionsList.addItem(item)
        # actions visibles selon le contexte :
        item = QtWidgets.QListWidgetItem(
            QtWidgets.QApplication.translate(
                'main', 'ContextualActions:'))
        item.setFont(boldFont)
        self.toolbarActionsList.addItem(item)
        for actions in (self.main.contextActions, self.main.contextActions2):
            for action in actions:
                if isinstance(action[0], QtWidgets.QWidgetAction):
                    item = QtWidgets.QListWidgetItem(
                        action[0].defaultWidget().itemIcon(0), 
                        action[0].defaultWidget().toolTip())
                else:
                    item = QtWidgets.QListWidgetItem(
                        action[0].icon(), action[0].text().replace('&', ''))
                item.setData(QtCore.Qt.UserRole, action)
                self.toolbarActionsList.addItem(item)
        # autres actions :
        item = QtWidgets.QListWidgetItem(
            QtWidgets.QApplication.translate('main', 'OtherActions:'))
        item.setFont(boldFont)
        self.toolbarActionsList.addItem(item)
        for actions in (self.main.otherActions, self.main.otherActions2):
            for action in actions:
                if isinstance(action[0], QtWidgets.QWidgetAction):
                    item = QtWidgets.QListWidgetItem(
                        action[0].icon(), 
                        action[0].defaultWidget().toolTip())
                else:
                    item = QtWidgets.QListWidgetItem(
                        action[0].icon(), action[0].text().replace('&', ''))
                item.setData(QtCore.Qt.UserRole, action)
                self.toolbarActionsList.addItem(item)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(titleLabel2)
        vLayout.addWidget(self.toolbarActionsList)
        groupBoxVisibleActions.setLayout(vLayout)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)
        mainLayout.addWidget(groupBoxVisibleActions)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):
        """
        mise à jour de l'affichage d'après les paramètres
        contenus dans les bases de config etc
        """
        for i in range(self.toolbarActionsList.count()):
            item = self.toolbarActionsList.item(i)
            try:
                action = item.data(QtCore.Qt.UserRole)
                state = utils_db.readInConfigDict(
                    self.main.configDict, 
                    action[1], 
                    default_int=action[2])[0]
                if state == 1:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
            except:
                continue
        self.doModified(modified=False)

    def doReset(self):
        """
        on remet les valeurs par défaut
        """
        for i in range(self.toolbarActionsList.count()):
            item = self.toolbarActionsList.item(i)
            try:
                action = item.data(QtCore.Qt.UserRole)
                if action[2] == 1:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
            except:
                continue
        self.doModified()

    def doModified(self, other=True, modified=True):
        self.isModified = modified
        self.parent.buttonsList['apply'].setEnabled(modified)

    def doApply(self):
        """
        on enregistre les modifications
        """
        deletes = []
        changes = {}
        for i in range(self.toolbarActionsList.count()):
            item = self.toolbarActionsList.item(i)
            try:
                action = item.data(QtCore.Qt.UserRole)
                deletes.append(action[1])
                if item.checkState() == QtCore.Qt.Checked:
                    state = 1
                else:
                    state = 0
                if state != action[2]:
                    changes[action[1]] = (state, '')
            except:
                continue
        utils_db.deleteFromConfigTable(
            self.main, self.main.db_my, deletes)
        utils_db.changeInConfigTable(
            self.main, self.main.db_my, changes, delete=False)
        for actionName in deletes:
            if actionName in self.main.configDict:
                del self.main.configDict[actionName]
        for actionName in changes:
            self.main.configDict[actionName] = [changes[actionName][0], '']
        self.main.changeDBMyState()
        self.doModified(modified=False)



"""
****************************************************
    FICHIERS PDF
****************************************************
"""

class PdfPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(PdfPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent
        self.isModified = False

        self.YES = QtWidgets.QApplication.translate('main', 'YES')
        self.NO = QtWidgets.QApplication.translate('main', 'NO')

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'PDF FILES CREATING SETTING'))
        titleLabel = QtWidgets.QLabel(title)

        pdfFoundTitleLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate(
                'main', 'wkhtmltopdf found:'))
        self.pdfFoundLabel = QtWidgets.QLabel()
        self.pdfSearchButton = QtWidgets.QPushButton(
            utils.doIcon('document-preview'), 
            QtWidgets.QApplication.translate('main', 'Search'))
        self.pdfSearchButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Automatically search the file'))
        self.pdfSearchButton.clicked.connect(self.doSearchPdfPath)
        pdfPathTitleLabel = utils_functions.u('<b>{0}</b>').format(
            QtWidgets.QApplication.translate(
                'main', 'wkhtmltopdf path:'))
        pdfPathTitleLabel = QtWidgets.QLabel(pdfPathTitleLabel)
        self.pdfPathEdit = QtWidgets.QLineEdit()
        self.pdfPathEdit.setReadOnly(True)
        self.pdfSelectButton = QtWidgets.QPushButton(
            utils.doIcon('document-preview'), 
            QtWidgets.QApplication.translate('main', 'Select'))
        self.pdfSelectButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Select the file manually'))
        self.pdfSelectButton.clicked.connect(self.doSelectPdfPath)
        testPdfText = QtWidgets.QApplication.translate(
            'main', 'Try creating a PDF file')
        self.testPdfButton = QtWidgets.QPushButton(
            utils.doIcon('extension-pdf'), testPdfText)
        self.testPdfButton.clicked.connect(self.doTestPdf)
        self.doSearchPdfPath(first=True)

        pdfMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigPdf')

        # on agence tout ça :
        grid = QtWidgets.QVBoxLayout()
        grid.addWidget(titleLabel)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(pdfFoundTitleLabel)
        hLayout.addWidget(self.pdfFoundLabel)
        hLayout.addStretch(1)
        grid.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(pdfPathTitleLabel)
        hLayout.addWidget(self.pdfPathEdit)
        grid.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addStretch(1)
        hLayout.addWidget(self.pdfSearchButton)
        hLayout.addWidget(self.pdfSelectButton)
        grid.addLayout(hLayout)
        grid.addWidget(self.testPdfButton)
        grid.addWidget(pdfMdHelpView)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(grid)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)

    def reInit(self):#VIDE
        """
        juste pour compatibilité
        """

    def doSearchPdfPath(self, first=False):
        import utils_pdf
        pdfOk = -1
        textYES = utils_functions.u('<b>{0}</b>').format(self.YES)
        textNO = utils_functions.u('<b>{0}</b>').format(self.NO)
        if not(first):
            utils_pdf.searchWkhtmltopdf()
        if utils_pdf.PDF_OK:
            self.pdfFoundLabel.setText(textYES)
            pdfOk = 1
            self.testPdfButton.setEnabled(True)
        else:
            self.pdfFoundLabel.setText(textNO)
            self.testPdfButton.setEnabled(False)
        wkhtmltopdfBin = utils_pdf.WKHTMLTOPDF_BIN
        self.pdfPathEdit.setText(wkhtmltopdfBin)
        utils_db.changeInConfigTable(
            self.main, 
            self.main.db_localConfig, 
            {'testPdf': (pdfOk, wkhtmltopdfBin)})

    def doSelectPdfPath(self):
        oldPath = self.pdfPathEdit.text()
        utils_functions.afficheMessage(self.main, 'doSelectPdfPath')
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate('main', 'Choose File'),
            self.main.workDir, 
            QtWidgets.QApplication.translate('main', 'All files (*.*)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        import utils_pdf
        utils_pdf.changeGlobals(True, fileName)
        self.pdfPathEdit.setText(fileName)
        utils_db.changeInConfigTable(
            self.main, 
            self.main.db_localConfig, 
            {'testPdf': (1, fileName)})

    def doTestPdf(self):
        utils_functions.doWaitCursor()
        try:
            import utils_pdf
            # création du fichier pdf :
            footer = ('Test de pied de page', '', True)
            testHtmlFileName = self.main.beginDir + "/libs/test_pdf.html"
            testHtmlFileName = QtCore.QFileInfo(testHtmlFileName).absoluteFilePath()
            tempPdfFileName = self.main.tempPath + "/test.pdf"
            utils_pdf.htmlToPdf(
                self.main, testHtmlFileName, tempPdfFileName, footer)
            # on affiche le fichier :
            url = QtCore.QUrl().fromLocalFile(tempPdfFileName)
            QtGui.QDesktopServices.openUrl(url)
        finally:
            utils_functions.restoreCursor()



"""
****************************************************
    RÉSEAU
****************************************************
"""

class LanPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(LanPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        self.YES = QtWidgets.QApplication.translate('main', 'YES')
        self.NO = QtWidgets.QApplication.translate('main', 'NO')

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'LAN SETTING'))
        titleLabel = QtWidgets.QLabel(title)

        # lanConfig:
        lanConfigLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate('main', 'Configuration in LAN')))
        self.lanConfigCheckBox = QtWidgets.QCheckBox(self.NO)
        self.lanConfigCheckBox.stateChanged.connect(
            self.lanConfigCheckBoxStateChanged)
        # création du dossier de config réseau (.Verac) :
        createLanConfigDirText = QtWidgets.QApplication.translate(
            'main', 'Create the configuration folder')
        self.createLanConfigDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-config'), 
            createLanConfigDirText)
        self.createLanConfigDirButton.clicked.connect(
            self.doCreateLanConfigDir)
        # sélection du chemin vers le dossier de config réseau (.Verac) :
        openDirText = QtWidgets.QApplication.translate('main', 'Select the folder')
        self.lanConfigDirEdit = QtWidgets.QLineEdit()
        self.lanConfigDirEdit.setReadOnly(True)
        self.selectLanConfigDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-development'), '')
        self.selectLanConfigDirButton.setToolTip(openDirText)
        self.selectLanConfigDirButton.clicked.connect(
            self.doSelectLanConfigDir)
        # aide :
        lanConfigMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigLanConfig')

        # lanFilesDir:
        lanFilesDirLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate('main', 'Teachers files folder')))
        self.lanFilesDirEdit = QtWidgets.QLineEdit()
        self.lanFilesDirEdit.setReadOnly(True)
        self.lanFilesDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-development'), '')
        self.lanFilesDirButton.setToolTip(openDirText)
        self.lanFilesDirButton.clicked.connect(self.doLanFilesDir)

        # lanWorkDir:
        lanWorkDirLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate('main', 'Working folder in LAN')))
        self.lanWorkDirCheckBox = QtWidgets.QCheckBox(self.NO)
        self.lanWorkDirCheckBox.stateChanged.connect(
            self.lanWorkDirCheckBoxStateChanged)
        # sélection du chemin vers le dossier WorkDir :
        self.lanWorkDirEdit = QtWidgets.QLineEdit()
        self.lanWorkDirEdit.setReadOnly(True)
        self.selectLanWorkDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-development'), '')
        self.selectLanWorkDirButton.setToolTip(openDirText)
        self.selectLanWorkDirButton.clicked.connect(self.doSelectLanWorkDir)
        # empêcher les utilisateurs de changer le chemin :
        lanWorkDirFixedText = QtWidgets.QApplication.translate(
            'main', 'prevent users to change WorkDir')
        self.lanWorkDirFixedCheckBox = QtWidgets.QCheckBox(
            lanWorkDirFixedText)
        # aide :
        lanWorkDirMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigLanWorkDir')

        # paramètres de proxy
        proxyLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate('main', 'Use a proxy')))
        self.proxyCheckBox = QtWidgets.QCheckBox(self.NO)
        proxyHostLabel = QtWidgets.QLabel(
            utils_functions.u('  {0}').format(
                QtWidgets.QApplication.translate('main', 'HTTP Proxy:')))
        self.proxyHostName = QtWidgets.QLineEdit()
        proxyPortLabel = QtWidgets.QLabel(
            utils_functions.u('  {0}').format(
                QtWidgets.QApplication.translate('main', 'Port:')))
        self.proxyPort = QtWidgets.QSpinBox()
        self.proxyPort.setRange(1, 65536)
        self.proxyCheckBox.stateChanged.connect(
            self.proxyCheckBoxStateChanged)
        self.proxyHostName.textChanged.connect(self.doModified)
        self.proxyPort.valueChanged.connect(self.doModified)



        # on agence tout ça :
        hLayout0 = QtWidgets.QHBoxLayout()

        vLayout = QtWidgets.QVBoxLayout()
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(lanConfigLabel)
        hLayout.addWidget(self.lanConfigCheckBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addStretch(1)
        hLayout.addWidget(self.createLanConfigDirButton)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.lanConfigDirEdit)
        hLayout.addWidget(self.selectLanConfigDirButton)
        vLayout.addLayout(hLayout)
        vLayout.addWidget(lanConfigMdHelpView)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        hLayout0.addWidget(groupBox)

        vLayout = QtWidgets.QVBoxLayout()
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(lanFilesDirLabel)
        hLayout.addWidget(self.lanFilesDirEdit)
        if self.main.me['userMode'] == 'admin':
            hLayout.addWidget(self.lanFilesDirButton)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(lanWorkDirLabel)
        hLayout.addWidget(self.lanWorkDirCheckBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.lanWorkDirEdit)
        hLayout.addWidget(self.selectLanWorkDirButton)
        vLayout.addLayout(hLayout)
        vLayout.addWidget(self.lanWorkDirFixedCheckBox)
        vLayout.addWidget(lanWorkDirMdHelpView)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        hLayout0.addWidget(groupBox)

        vLayout = QtWidgets.QVBoxLayout()
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(proxyLabel)
        hLayout.addWidget(self.proxyCheckBox)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(proxyHostLabel)
        hLayout.addWidget(self.proxyHostName)
        vLayout.addLayout(hLayout)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(proxyPortLabel)
        hLayout.addWidget(self.proxyPort)
        hLayout.addStretch(1)
        vLayout.addLayout(hLayout)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)
        mainLayout.addLayout(hLayout0)
        mainLayout.addWidget(groupBox)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):
        """
        mise à jour de l'affichage d'après les paramètres
        contenus dans les bases de config etc
        """
        # lanConfig :
        if utils.lanConfig:
            self.lanConfigCheckBox.setChecked(True)
            self.lanConfigCheckBox.setText(self.YES)
            self.selectLanConfigDirButton.setEnabled(True)
            self.lanConfigDirEdit.setText(utils.lanConfigDir)
        else:
            self.lanConfigCheckBox.setChecked(False)
            self.lanConfigCheckBox.setText(self.NO)
            self.selectLanConfigDirButton.setEnabled(False)
            self.lanConfigDirEdit.setText('')
        # lanFilesDir:
        self.lanFilesDirEdit.setText(utils.lanFilesDir)
        # lanWorkDir :
        if utils.lanWorkDir != '':
            self.lanWorkDirCheckBox.setChecked(True)
            self.lanWorkDirCheckBox.setText(self.YES)
            self.selectLanWorkDirButton.setEnabled(True)
            self.lanWorkDirEdit.setText(utils.lanWorkDir)
            self.lanWorkDirFixedCheckBox.setEnabled(True)
        else:
            self.lanWorkDirCheckBox.setChecked(False)
            self.lanWorkDirCheckBox.setText(self.NO)
            self.selectLanWorkDirButton.setEnabled(False)
            self.lanWorkDirEdit.setText('')
            self.lanWorkDirFixedCheckBox.setEnabled(False)
        # lanWorkDirFixed :
        self.lanWorkDirFixedCheckBox.setChecked(
            utils.lanWorkDirFixed)
        # proxy :
        isProxy = utils_db.readInConfigDB(self.main, 'isProxy')[0]
        (proxyPort, proxyHost, newKey) = utils_db.readInConfigDB(
            self.main, 'ParamProxy')
        self.initProxy = {
            'isProxy': isProxy, 
            'Host': proxyHost, 
            'port': proxyPort}
        self.proxyHostName.setText(self.initProxy['Host'])
        self.proxyPort.setValue(self.initProxy['port'])
        if self.initProxy['isProxy'] == 1:
            self.proxyCheckBox.setText(self.YES)
            self.proxyCheckBox.setChecked(True)
            self.proxyHostName.setEnabled(True)
            self.proxyPort.setEnabled(True)
        else:
            self.proxyCheckBox.setText(self.NO)
            self.proxyCheckBox.setChecked(False)
            self.proxyHostName.setEnabled(False)
            self.proxyPort.setEnabled(False)
        self.doModified(modified=False)

    def doReset(self):
        """
        on remet les valeurs par défaut
        """
        # lanConfig :
        if utils.lanConfig_initial:
            self.lanConfigCheckBox.setChecked(True)
            self.lanConfigCheckBox.setText(self.YES)
            self.selectLanConfigDirButton.setEnabled(True)
            self.lanConfigDirEdit.setText(utils.lanConfigDir_initial)
        else:
            self.lanConfigCheckBox.setChecked(False)
            self.lanConfigCheckBox.setText(self.NO)
            self.selectLanConfigDirButton.setEnabled(False)
            self.lanConfigDirEdit.setText('')
        # lanFilesDir:
        self.lanFilesDirEdit.setText(utils.lanFilesDir_initial)
        # lanWorkDir :
        if utils.lanWorkDir_initial != '':
            self.lanWorkDirCheckBox.setChecked(True)
            self.lanWorkDirCheckBox.setText(self.YES)
            self.selectLanWorkDirButton.setEnabled(True)
            self.lanWorkDirEdit.setText(utils.lanWorkDir_initial)
            self.lanWorkDirFixedCheckBox.setEnabled(True)
        else:
            self.lanWorkDirCheckBox.setChecked(False)
            self.lanWorkDirCheckBox.setText(self.NO)
            self.selectLanWorkDirButton.setEnabled(False)
            self.lanWorkDirEdit.setText('')
            self.lanWorkDirFixedCheckBox.setEnabled(False)
        # lanWorkDirFixed :
        self.lanWorkDirFixedCheckBox.setChecked(
            utils.lanWorkDirFixed_initial)
        # proxy :
        self.initProxy = {
            'isProxy': 0, 
            'Host': '', 
            'port': 1}
        self.proxyHostName.setText('')
        self.proxyPort.setValue(1)
        self.proxyCheckBox.setText(self.NO)
        self.proxyCheckBox.setChecked(False)
        self.proxyHostName.setEnabled(False)
        self.proxyPort.setEnabled(False)
        self.doModified()

    def doModified(self, other=True, modified=True):
        self.isModified = modified
        self.parent.buttonsList['apply'].setEnabled(modified)

    def lanConfigCheckBoxStateChanged(self):
        if self.lanConfigCheckBox.isChecked():
            self.lanConfigCheckBox.setText(self.YES)
            self.selectLanConfigDirButton.setEnabled(True)
        else:
            self.lanConfigCheckBox.setText(self.NO)
            self.selectLanConfigDirButton.setEnabled(False)
        self.doModified()

    def lanWorkDirCheckBoxStateChanged(self):
        if self.lanWorkDirCheckBox.isChecked():
            self.lanWorkDirCheckBox.setText(self.YES)
            self.selectLanWorkDirButton.setEnabled(True)
            self.lanWorkDirFixedCheckBox.setEnabled(True)
        else:
            self.lanWorkDirCheckBox.setText(self.NO)
            self.selectLanWorkDirButton.setEnabled(False)
            self.lanWorkDirFixedCheckBox.setChecked(False)
            self.lanWorkDirFixedCheckBox.setEnabled(False)
        self.doModified()

    def proxyCheckBoxStateChanged(self, checkState):
        if checkState == QtCore.Qt.Checked:
            self.proxyCheckBox.setText(self.YES)
            self.proxyHostName.setEnabled(True)
            self.proxyPort.setEnabled(True)
        else:
            self.proxyCheckBox.setText(self.NO)
            self.proxyHostName.setEnabled(False)
            self.proxyPort.setEnabled(False)
        self.doModified()

    def doSelectLanConfigDir(self):
        if self.lanConfigDirEdit.text() != '':
            directory = self.lanConfigDirEdit.text()
        else:
            directory = QtCore.QDir.homePath()
        lanConfigTitle = QtWidgets.QApplication.translate(
            'main', 'Select The .Verac directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            lanConfigTitle, 
            directory, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        self.lanConfigDirEdit.setText(directory)
        self.doModified()

    def doCreateLanConfigDir(self):
        if self.lanConfigDirEdit.text() != '':
            directory = QtCore.QDir(self.lanConfigDirEdit.text() + '/../..').canonicalPath()
        else:
            directory = QtCore.QDir(self.main.beginDir + '/..').canonicalPath()
        lanConfigTitle = QtWidgets.QApplication.translate(
            'main', 'Select The lanConfig parent directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            lanConfigTitle, 
            directory, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        utils_functions.doWaitCursor()
        try:
            import utils_filesdirs
            # on supprime un éventuelle ancienne version du dossier :
            utils_filesdirs.emptyDir(directory + '/lanConfig')
            QtCore.QDir(directory).rmdir('lanConfig')
            # création du dossier lanConfig :
            utils_filesdirs.createDirs(directory, 'lanConfig/.Verac')
            # copie du dossier .Verac (configuration) :
            utils_filesdirs.copyDir(
                self.main.localConfigDir, directory + '/lanConfig/.Verac')
            # nettoyage de la base config en passant par temp :
            fileConfig = utils_functions.u(
                '{0}/lanConfig/.Verac/config.sqlite').format(directory)
            fileTemp = utils_functions.u(
                '{0}/tempconfig.sqlite').format(self.main.tempPath)
            utils_filesdirs.removeAndCopy(fileConfig, fileTemp)
            (db, dbName) = utils_db.createConnection(self.main, fileTemp)
            query, transactionOK = utils_db.queryTransactionDB(db)
            # on vide les tables inutiles (conn et schedules) :
            query = utils_db.queryExecute(
                [utils_db.qdf_table.format('conn'), utils_db.qdf_table.format('schedules')], 
                query=query)
            # on garde juste actualVersion dans la table versions :
            commandLine = utils_functions.u(
                'DELETE FROM versions '
                'WHERE name!="{0}"').format(self.main.actualVersion['versionName'])
            query = utils_db.queryExecute(commandLine, query=query)
            commandLine = utils_functions.u(
                'UPDATE versions SET id_version=10 '
                'WHERE name="{0}"').format(self.main.actualVersion['versionName'])
            query = utils_db.queryExecute(commandLine, query=query)
            # on met à jour le numéro de actualVersion dans la table config :
            commandLine = (
                'UPDATE config SET value_int=10 '
                'WHERE key_name="actualVersion"')
            query = utils_db.queryExecute(commandLine, query=query)
            # et on garde que les clés utiles :
            commandLine = (
                'DELETE FROM config '
                'WHERE NOT(key_name IN '
                '("actualVersion", "versionDB"))')
            query = utils_db.queryExecute(commandLine, query=query)
            # on termine la transaction :
            utils_db.endTransaction(query, db, transactionOK)
            if query != None:
                query.clear()
                del query
            db.close()
            del db
            utils_db.sqlDatabase.removeDatabase(dbName)
            # et on copie le fichier :
            utils_filesdirs.removeAndCopy(fileTemp, fileConfig)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheMsgFin(self.main, timer=5)
        self.doModified()

    def doSelectLanWorkDir(self):
        if self.lanWorkDirEdit.text() != '':
            directory = self.lanWorkDirEdit.text()
        else:
            directory = QtCore.QDir.homePath()
        lanWorkDirTitle = QtWidgets.QApplication.translate(
            'main', 'Select The Work directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            lanWorkDirTitle, 
            directory, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        self.lanWorkDirEdit.setText(directory)
        self.doModified()

    def doLanFilesDir(self):
        if self.lanFilesDirEdit.text() != '':
            directory = self.lanFilesDirEdit.text()
        else:
            directory = QtCore.QDir(self.main.beginDir).canonicalPath()
        lanWorkDirTitle = QtWidgets.QApplication.translate(
            'main', 'Select The Teachers files directory')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self.main, 
            lanWorkDirTitle, 
            directory, 
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        self.lanFilesDirEdit.setText(directory)
        self.doModified()

    def doApply(self):
        """
        on enregistre les modifications
        """
        if self.lanConfigCheckBox.isChecked():
            utils_db.changeInConfigDB(
                self.main, 'lanConfig', 1, self.lanConfigDirEdit.text())
            utils.changeLanConfig(True, self.lanConfigDirEdit.text())
        else:
            utils_db.changeInConfigDB(self.main, 'lanConfig', -1, '')
            utils.changeLanConfig(False, '')
        if self.lanWorkDirCheckBox.isChecked():
            utils_db.changeInConfigDB(
                self.main, 'lanWorkDir', 1, self.lanWorkDirEdit.text())
            utils.changeLanWorkDir(self.lanWorkDirEdit.text())
        else:
            utils_db.changeInConfigDB(self.main, 'lanWorkDir', -1, '')
            utils.changeLanWorkDir('')
        if self.lanWorkDirFixedCheckBox.isChecked():
            utils_db.changeInConfigDB(self.main, 'lanWorkDirFixed', 1, '')
            utils.changeLanWorkDirFixed(True)
        else:
            utils_db.changeInConfigDB(self.main, 'lanWorkDirFixed', -1, '')
            utils.changeLanWorkDirFixed(False)
        if self.proxyCheckBox.isChecked():
            utils_db.changeInConfigDB(self.main, 'isProxy', 1, '')
        else:
            utils_db.changeInConfigDB(self.main, 'isProxy', 0, '')
        port = self.proxyPort.value()
        host = self.proxyHostName.text()
        utils_db.changeInConfigDB(self.main, 'ParamProxy', port, host)
        self.doModified(modified=False)




"""
****************************************************
    AUTRES TRUCS
****************************************************
"""

class OtherPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(OtherPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        self.YES = QtWidgets.QApplication.translate('main', 'YES')
        self.NO = QtWidgets.QApplication.translate('main', 'NO')

        title = utils_functions.u(TITLE_STYLE).format(
            QtWidgets.QApplication.translate(
                'main', 'OTHER SETTINGS'))
        titleLabel = QtWidgets.QLabel(title)

        # logFileCheckBox:
        logFileLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate('main', 'LogFile')))
        self.logFileCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'use a log file'))
        logFileMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigLogFile')
        self.logFileCheckBox.stateChanged.connect(self.doModified)

        # updatesComboBox:
        updatesLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate('main', 'Updates')))
        self.updatesComboBox = QtWidgets.QComboBox()
        self.updatesComboBox.setMinimumWidth(150)
        self.updatesComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'OneOutOfThree'))
        self.updatesComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Weekly'))
        self.updatesComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Always'))
        self.updatesComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Never'))
        updatesMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigUpdates')
        self.updatesComboBox.activated.connect(self.doModified)

        # recupDetailsCheckBox:
        recupDetailsLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate(
                    'main', 'Collect details of assessments')))
        self.recupDetailsCheckBox = QtWidgets.QCheckBox(self.NO)
        self.recupDetailsCheckBox.stateChanged.connect(
            self.recupDetailsCheckBoxStateChanged)
        recupDetailsMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigRecupDetails')

        # annee_scolaire
        schoolYearDetailsLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate(
                    'main', 'School year')))
        self.schoolYearSpinBox = QtWidgets.QSpinBox()
        self.schoolYearSpinBox.setRange(1800, 3000)
        self.schoolYearSpinBox.valueChanged.connect(self.doModified)
        schoolYearMdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpConfigSchoolYear')

        # testNetDelay :
        testNetDelayLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(
                QtWidgets.QApplication.translate(
                    'main', 'Connection timeout test')))
        self.testNetDelaySpinBox = QtWidgets.QSpinBox()
        self.testNetDelaySpinBox.setRange(1, 60)
        self.testNetDelaySpinBox.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 
                'Time (in seconds) the connection '
                'test facility website.'))
        self.testNetDelaySpinBox.valueChanged.connect(self.doModified)

        # on agence tout ça :
        grid = QtWidgets.QVBoxLayout()
        grid.addWidget(titleLabel)

        hLayout2 = QtWidgets.QHBoxLayout()
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(logFileLabel)
        hLayout.addWidget(self.logFileCheckBox)
        hLayout.addStretch(1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(hLayout)
        vLayout.addWidget(logFileMdHelpView)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        hLayout2.addWidget(groupBox)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(updatesLabel)
        hLayout.addWidget(self.updatesComboBox)
        hLayout.addStretch(1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(hLayout)
        vLayout.addWidget(updatesMdHelpView)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        hLayout2.addWidget(groupBox)

        grid.addLayout(hLayout2)

        if self.main.me['userMode'] == 'admin':
            hLayout = QtWidgets.QHBoxLayout()
            hLayout.addWidget(recupDetailsLabel)
            hLayout.addWidget(self.recupDetailsCheckBox)
            hLayout.addStretch(1)
            vLayout = QtWidgets.QVBoxLayout()
            vLayout.addLayout(hLayout)
            vLayout.addWidget(recupDetailsMdHelpView)
            groupBox = QtWidgets.QGroupBox()
            groupBox.setLayout(vLayout)
            grid.addWidget(groupBox)
        else:
            hLayout = QtWidgets.QHBoxLayout()
            hLayout.addWidget(schoolYearDetailsLabel)
            hLayout.addWidget(self.schoolYearSpinBox)
            hLayout.addStretch(1)
            vLayout = QtWidgets.QVBoxLayout()
            vLayout.addLayout(hLayout)
            vLayout.addWidget(schoolYearMdHelpView)
            groupBox = QtWidgets.QGroupBox()
            groupBox.setLayout(vLayout)
            grid.addWidget(groupBox)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(testNetDelayLabel)
        hLayout.addWidget(self.testNetDelaySpinBox)
        hLayout.addStretch(1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(hLayout)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        grid.addWidget(groupBox)

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(grid)
        mainLayout.addStretch(1)
        self.setLayout(mainLayout)
        self.reInit()

    def reInit(self):
        """
        mise à jour de l'affichage d'après les paramètres
        contenus dans les bases de config etc
        """
        logFile = utils_db.readInConfigDB(self.main, 'logFile')[0]
        self.logFileCheckBox.setChecked(logFile >= 0)
        updatesMode = utils_db.readInConfigDB(self.main, 'UpdatesMode')[0]
        self.updatesComboBox.setCurrentIndex(updatesMode)
        if self.main.me['userMode'] == 'admin':
            if utils.ADMIN_WITH_DETAILS == 1:
                self.recupDetailsCheckBox.setChecked(True)
                self.recupDetailsCheckBox.setText(self.YES)
            else:
                self.recupDetailsCheckBox.setChecked(False)
                self.recupDetailsCheckBox.setText(self.NO)
        else:
            annee_scolaire = utils_db.readInConfigDict(
                self.main.configDict, 
                'annee_scolaire', 
                default_int=0)[0]
            self.schoolYearSpinBox.setValue(annee_scolaire)
        testNetDelay = utils_db.readInConfigDB(
            self.main, 
            'testNetDelay', 
            default_int=utils.testNetDelay_initial)[0]
        self.testNetDelaySpinBox.setValue(testNetDelay)
        self.doModified(modified=False)

    def doReset(self):
        """
        on remet les valeurs par défaut
        """
        if self.main.me['userMode'] == 'admin':
            if utils.ADMIN_WITH_DETAILS == 1:
                self.recupDetailsCheckBox.setChecked(True)
                self.recupDetailsCheckBox.setText(self.YES)
            else:
                self.recupDetailsCheckBox.setChecked(False)
                self.recupDetailsCheckBox.setText(self.NO)
        else:
            annee_scolaire = utils_db.readInConfigDict(
                self.main.configDict, 
                'annee_scolaire', 
                default_int=0)[0]
            self.schoolYearSpinBox.setValue(annee_scolaire)
        logFile = utils_db.readInConfigDB(self.main, 'logFile')[0]
        if logFile >= 0:
            self.logFileCheckBox.setChecked(True)
        else:
            self.logFileCheckBox.setChecked(False)
        self.updatesComboBox.setCurrentIndex(0)
        self.testNetDelaySpinBox.setValue(
            utils.testNetDelay_initial)
        utils.changeTestNetDelay(utils.testNetDelay_initial)
        self.doModified()

    def doModified(self, other=True, modified=True):
        self.isModified = modified
        self.parent.buttonsList['apply'].setEnabled(modified)

    def recupDetailsCheckBoxStateChanged(self):
        if self.recupDetailsCheckBox.isChecked():
            self.recupDetailsCheckBox.setText(self.YES)
        else:
            self.recupDetailsCheckBox.setText(self.NO)
        self.doModified()

    def doApply(self):
        """
        on enregistre les modifications
        """
        if self.main.me['userMode'] == 'admin':
            if self.recupDetailsCheckBox.isChecked():
                value = 1
            else:
                value = 0
            import admin
            utils.changeAdminMustRecupDetails(value)
            utils_db.changeInConfigTable(
                self.main, 
                admin.db_admin, 
                {'adminMustRecupDetails': (value, '')}, 
                table='config_calculs')
        else:
            annee_scolaire = utils_db.readInConfigDict(
                self.main.configDict, 
                'annee_scolaire', 
                default_int=0)[0]
            year = self.schoolYearSpinBox.value()
            if year != annee_scolaire:
                self.main.configDict['annee_scolaire'] = [year, '']
                utils_db.changeInConfigTable(
                    self.main, 
                    self.main.db_my, 
                    {'annee_scolaire': (year, '')})
                if year == self.main.annee_scolaire[0]:
                    self.main.reloadConfigDict()
                    actionsToEnable = (
                        self.main.actionPostDBMy,
                        self.main.actionSaveAndPostDBMy,
                        self.main.actionDownloadSuivisDB,
                        self.main.actionPostSuivis)
                    for action in actionsToEnable:
                        action.setEnabled(True)
                self.main.changeDBMyState()

        if self.logFileCheckBox.isChecked():
            value = 1
            option = True
        else:
            value = -1
            option = False
        utils.changeLogFile(option)
        utils_db.changeInConfigDB(self.main, 'logFile', value, '')
        updatesMode = self.updatesComboBox.currentIndex()
        utils_db.changeInConfigDB(
            self.main, 'UpdatesMode', updatesMode, '')
        testNetDelay = self.testNetDelaySpinBox.value()
        utils_db.changeInConfigDB(
            self.main, 'testNetDelay', testNetDelay, '')
        utils.changeTestNetDelay(testNetDelay)
        self.doModified(modified=False)


