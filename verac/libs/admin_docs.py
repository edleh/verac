# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Tout ce qui concerne la gestion des documents pour les profs et pour les élèves.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_filesdirs
import admin, admin_ftp

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



"""
****************************************************
    FONCTIONS DIVERSES
****************************************************
"""

def verifDocs(main, who='eleves', typeDocument=-1):
    """
    Vérification des documents (élèves ou profs).
    Se fait en 2 temps :
        1.  on cherche les fichiers inscrits dans la base,
            mais qui ne sont pas présents dans le dossier sousDir.
        2.  on cherche les fichiers présents dans le dossier sousDir,
            mais qui ne sont pas inscrits dans la base.
    Renvoie un dictionnaire.
    """
    utils_functions.doWaitCursor()
    if who == 'eleves':
        sousDir = '/protected/documents'
    elif who == 'profs':
        sousDir = '/protected/docsprofs'
    listFileExist = []
    docs = {'OK': [], 'InDBOnly': [], 'FileOnly': []}
    dirLocal = admin.dirLocalPrive + sousDir
    try:
        admin.openDB(main, 'documents')
        query_documents = utils_db.query(admin.db_documents)
        # première partie :
        if typeDocument == -1:
            # on cherche tous les types de documents
            commandLine_documents = utils_db.q_selectAllFromOrder.format(who, 'nomFichier')
        else:
            # on cherche le type de documents demandé
            commandLine_documents = 'SELECT * FROM {0} WHERE type={1} ORDER BY nomFichier'
            commandLine_documents = commandLine_documents.format(who, typeDocument)
        query_documents = utils_db.queryExecute(commandLine_documents, query=query_documents)
        while query_documents.next():
            id_who = int(query_documents.value(0))
            fileName = query_documents.value(1)
            labelDocument = query_documents.value(2)
            typeDoc = int(query_documents.value(3))
            if QtCore.QFileInfo(dirLocal + '/' + fileName).exists():
                docs['OK'].append((id_who, fileName, labelDocument, typeDoc))
                utils_functions.appendUnique(listFileExist, fileName)
            else:
                docs['InDBOnly'].append((id_who, fileName, labelDocument, typeDoc))
        # deuxième partie :
        filesList = QtCore.QDir(dirLocal).entryList(
            ['*'], QtCore.QDir.Files, QtCore.QDir.Name)
        for fileName in filesList:
            if not(fileName in listFileExist) and (fileName != '.htaccess'):
                notInDB = True
                commandLine_documents = utils_db.q_selectAllFromWhereText.format(
                    who, 'nomFichier', fileName)
                query_documents = utils_db.queryExecute(commandLine_documents, query=query_documents)
                while query_documents.next():
                    notInDB = False
                if notInDB:
                    docs['FileOnly'].append(fileName)
    finally:
        utils_functions.restoreCursor()
        return docs


def addDocumentsEleves(main, documentsEleves):
    """
    pour inscrire d'un coup tout un tas de fichiers de même label.
    Typiquement les bulletins qu'on vient de créer.
        chaque élément de la liste documentsEleves contient : 
        [id_eleve, fileName, labelDocument, typeDocument]
    """
    try:
        admin.openDB(main, 'documents')
        query_documents, transactionOK = utils_db.queryTransactionDB(admin.db_documents)
        # création de la table eleves si besoin :
        query_documents = utils_db.queryExecute(
            utils_db.qct_documents_eleves, query=query_documents)
        lines = []
        commandLine_base = 'DELETE FROM eleves WHERE id_eleve={0} AND labelDocument="{1}"'
        for documentEleve in documentsEleves:
            # on récupère le premier dictionnaire, puis id_eleve :
            id_eleve = documentEleve[0]
            fileName = documentEleve[1]
            labelDocument = documentEleve[2]
            typeDocument = documentEleve[3]
            # suppression d'une éventuelle ancienne inscription :
            commandLine_documents = utils_functions.u(commandLine_base).format(id_eleve, labelDocument)
            query_documents = utils_db.queryExecute(commandLine_documents, query=query_documents)
            # et inscription du document :
            document = [id_eleve, fileName, labelDocument, typeDocument]
            lines.append(document)
        commandLine_documents = utils_db.insertInto('eleves', 4)
        query_documents = utils_db.queryExecute(
            {commandLine_documents: lines}, query=query_documents)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(admin.dbFile_documents)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_db.endTransaction(query_documents, admin.db_documents, transactionOK)
        utils_filesdirs.removeAndCopy(admin.dbFileTemp_documents, admin.dbFile_documents)


def addDocumentProfs(main, document):
    """
    pour inscrire un document dans la table profs.
    document est un tuple du type : (idDocument, fileName, labelDocument, typeDocument)
    """
    try:
        admin.openDB(main, 'documents')
        query_documents, transactionOK = utils_db.queryTransactionDB(admin.db_documents)
        # on efface les enregistrements précédents :
        fileName = document[1]
        labelDocument = document[2]
        if labelDocument == '':
            commandLine_documents = utils_db.qdf_whereText.format('profs', 'nomFichier', fileName)
        else:
            commandLine_documents = 'DELETE FROM profs WHERE nomFichier="{0}" OR labelDocument="{1}"'
            commandLine_documents = utils_functions.u(
                commandLine_documents).format(fileName, labelDocument)
        # et on inscrit le document :
        commandLine_documents2 = utils_db.insertInto('profs', 4)
        query_documents = utils_db.queryExecute(
            [commandLine_documents, {commandLine_documents2: document}], 
            query=query_documents)
    finally:
        utils_db.endTransaction(query_documents, admin.db_documents, transactionOK)
        utils_filesdirs.removeAndCopy(admin.dbFileTemp_documents, admin.dbFile_documents)

def uploadDocumentsAndDB(main, msgFin=True):
    """
    envoie sur le site web la base documents et les différents documents (élèves et profs)
    """
    debut = QtCore.QTime.currentTime()
    utils_functions.afficheMessage(
        main, 
        QtWidgets.QApplication.translate('main', 'Sending documents'), 
        tags=('h2'))
    uploadDBDocuments(main, msgFin=False)
    uploadDocuments(main, who='eleves', msgFin=False)
    uploadDocuments(main, who='profs', msgFin=False)
    if msgFin:
        fin = QtCore.QTime.currentTime()
        utils_functions.afficheDuree(main, debut, fin)
        utils_functions.afficheMsgFin(main, timer=5)

def uploadDocuments(main, who='eleves', msgFin=True):
    """
    mise à jour du dossier distant documents ou docsprofs par FTP
    """
    if who == 'eleves':
        sousDir = '/protected/documents'
        fileType = 'docEleve'
        utils_functions.afficheMessage(main, 
            QtWidgets.QApplication.translate('main', 'uploadStudentsDocuments'), tags=('h4'))
    elif who == 'profs':
        sousDir = '/protected/docsprofs'
        fileType = 'docProf'
        utils_functions.afficheMessage(main, 
            QtWidgets.QApplication.translate('main', 'uploadTeachersDocuments'), tags=('h4'))
    dirSite = admin.dirSitePrive + sousDir
    dirLocal = admin.dirLocalPrive + sousDir
    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            reponse = admin_ftp.ftpPutDir(main, dirLocal, dirSite, fileType)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                if msgFin:
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                utils_functions.afficheMsgPb(main)

def uploadDBDocuments(main, msgFin=True):
    """
    envoi de la base documents sur le site
    """
    dirSite = admin.dirSitePrive + '/protected'
    dirLocal = admin.dirLocalPrive + '/protected'
    theFileName = 'documents.sqlite'
    debut = QtCore.QTime.currentTime()
    utils_functions.afficheMessage(main, 
        QtWidgets.QApplication.translate('main', 'uploadDBDocuments'), tags=('h4'))
    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            # on envoie le fichier:
            reponse = admin_ftp.ftpPutFile(main, dirSite, dirLocal, theFileName)
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                if msgFin:
                    fin = QtCore.QTime.currentTime()
                    utils_functions.afficheDuree(main, debut, fin)
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                message = QtWidgets.QApplication.translate('main', 'uploadDBDocuments Failed')
                utils_functions.afficheMsgPb(main, message)



















"""
****************************************************
    FENÊTRE DE GESTION DES DOCUMENTS
****************************************************
"""


def manageDocuments(main, who='personal'):
    """
    lance le dialog de configuration des documents (ManageDocumentsDlg).
    """
    dialog = ManageDocumentsDlg(parent=main, who=who)
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        # si on avait utilisé le bouton appliquer,
        # on proposera l'envoi :
        if dialog.applyed:
            dialog.mustUpload = True
    else:
        main.enableInterface(dialog, lastState)
        # on enregistre les modifications dans les bases :
        for tab in dialog.tabs:
            tab.checkModifications()
        # on met à jour la version locale de la base 
        # documents (depuis le dossier temp) :
        if dialog.modifiedTables['documents']:
            utils_filesdirs.removeAndCopy(
                admin.dbFileTemp_documents, admin.dbFile_documents)
    main.editLog2 = None
    if dialog.mustUpload:
        # on demande s'il faut poster maintenant :
        message = QtWidgets.QApplication.translate(
            'main', 'Would you upload documents to your website now?')
        message = utils_functions.u('<p><b>{0}</b></p>').format(message)
        reponse = utils_functions.messageBox(
            main, level='warning', message=message, buttons=['Yes', 'No'])
        if reponse == QtWidgets.QMessageBox.Yes:
            uploadDocumentsAndDB(main)



class ManageDocumentsDlg(QtWidgets.QDialog):
    """
    """
    def __init__(self, parent=None, who='personal'):
        utils_functions.doWaitCursor()
        super(ManageDocumentsDlg, self).__init__(parent)
        self.main = parent
        self.helpContextPage = ''
        # pour savoir ce qui a été modifié :
        self.modifiedTables = {
            'documents': False, 
            'config': False, 
            'eleves': False, 
            'profs': False, 
            }
        # pour savoir si on a utilisé le bouton "appliquer"
        # et s'il faudra poster sur le site :
        self.applyed = False
        self.mustUpload = False
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate('main', 'Documents management')
        self.setWindowTitle(title)
        # le tabWidget et sa liste d'onglets :
        self.tabWidget = QtWidgets.QTabWidget(self)
        self.tabWidget.currentChanged.connect(self.doCurrentTabChanged)
        self.tabs = []

        # création des onglets :
        self.tabStudentsPersonalDocuments = StudentsPersonalDocumentsDlg(parent=self)
        self.tabWidget.addTab(
            self.tabStudentsPersonalDocuments, 
            QtWidgets.QApplication.translate('main', 'Students personal documents'))
        self.tabs.append(self.tabStudentsPersonalDocuments)
        self.tabStudentsDocuments = StudentsDocumentsDlg(parent=self)
        self.tabWidget.addTab(
            self.tabStudentsDocuments, 
            QtWidgets.QApplication.translate('main', 'Students documents'))
        self.tabs.append(self.tabStudentsDocuments)
        self.tabTeachersDocuments = TeachersDocumentsDlg(parent=self)
        self.tabWidget.addTab(
            self.tabTeachersDocuments, 
            QtWidgets.QApplication.translate('main', 'Teachers documents'))
        self.tabs.append(self.tabTeachersDocuments)
        self.tabByStudent = ByStudentDlg(parent=self)
        self.tabWidget.addTab(
            self.tabByStudent, 
            QtWidgets.QApplication.translate('main', 'By student'))
        self.tabs.append(self.tabByStudent)
        # onglet outils :
        self.tabUtils = DocsUtilsDlg(parent=self)
        self.tabWidget.addTab(
            self.tabUtils, 
            QtWidgets.QApplication.translate('main', 'Tools'))
        self.tabs.append(self.tabUtils)

        # on indique l'onglet de départ :
        if who == 'personal':
            indexTab = 0
        elif who == 'eleves':
            indexTab = 1
        elif who == 'profs':
            indexTab = 2
        self.tabWidget.setCurrentIndex(indexTab)
        # fin de la mise en place :
        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('ok', 'apply', 'cancel', 'help'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]
        self.buttonsList['apply'].clicked.connect(self.doApply)
        self.buttonsList['apply'].setEnabled(False)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.tabWidget)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        utils_functions.restoreCursor()

    def doCurrentTabChanged(self, index):
        self.helpContextPage = self.tabWidget.currentWidget().helpContextPage
        if type(self.tabWidget.currentWidget()) == DocsUtilsDlg:
            self.tabWidget.currentWidget().uploadDocumentsAndDB_Button.clearFocus()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, 'admin')

    def doApply(self):
        """
        bouton "appliquer" les changements sans fermer la fenêtre
        """
        for tab in self.tabs:
            tab.checkModifications()
        self.applyed = True
        self.mustUpload = True
        self.updateButtons(False)
        self.buttonsList['cancel'].clearFocus()

    def updateButtons(self, modified=True):
        self.buttonsList['apply'].setEnabled(modified)



class StudentsPersonalDocumentsDlg(QtWidgets.QDialog):
    """
    Dialogue pour gérer les fichiers personnels des élèves (bulletins etc)
    """
    def __init__(self, parent=None):
        super(StudentsPersonalDocumentsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'manage-documents'
        # pour gérer ce qui est modifié :
        self.modifications = {'modified': False, 'FILES_TO_DELETE': []}
        # pour annuler :
        self.lastData = []

        self.colors = {'OK': 'green', 'InDBOnly': 'red', 'FileOnly': 'magenta'}
        self.toolTips = {
            'OK': '', 
            'InDBOnly': QtWidgets.QApplication.translate(
                'main', 'This file does not exist in the "documents" folder.'), 
            'FileOnly': QtWidgets.QApplication.translate(
                'main', 'This file is not registered in the database.')}
        self.icons = {}
        for color in ('green', 'red', 'magenta'):
            self.icons[color] = utils.doIcon('color-{0}'.format(color))

        # la zone de sélection (liste des fichiers + boutons) :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        self.baseList.itemDoubleClicked.connect(self.itemDoubleClicked)
        self.openDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-eleves'), 
            QtWidgets.QApplication.translate('main', 'documents'),
            toolTip = QtWidgets.QApplication.translate('main', 'OpenDocsElevesDirToolTip'))
        self.openDirButton.clicked.connect(self.openDocsElevesDir)
        self.reloadFilesButton = QtWidgets.QPushButton(
            utils.doIcon('view-refresh'), 
            QtWidgets.QApplication.translate('main', 'reloadFiles'),
            toolTip = QtWidgets.QApplication.translate('main', 'reloadFilesToolTip'))
        self.reloadFilesButton.clicked.connect(self.initialize)
        self.repairAllButton = QtWidgets.QPushButton(
            utils.doIcon('list-repair'), 
            QtWidgets.QApplication.translate('main', 'repairAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'repairAllToolTip'))
        self.repairAllButton.clicked.connect(self.repairAll)
        self.deleteFilesAllButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'), 
            QtWidgets.QApplication.translate('main', 'deleteFilesAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'deleteFilesAllToolTip'))
        self.deleteFilesAllButton.clicked.connect(self.deleteFilesAll)
        self.clearAllButton = QtWidgets.QPushButton(
            utils.doIcon('edit-clear'), 
            QtWidgets.QApplication.translate('main', 'clearAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'clearAllToolTip'))
        self.clearAllButton.clicked.connect(self.clearAll)
        customButtonsLayout0 = QtWidgets.QHBoxLayout()
        customButtonsLayout0.addWidget(self.openDirButton)
        customButtonsLayout0.addWidget(self.reloadFilesButton)
        customButtonsLayout1 = QtWidgets.QHBoxLayout()
        customButtonsLayout1.addWidget(self.repairAllButton)
        customButtonsLayout1.addWidget(self.deleteFilesAllButton)
        customButtonsLayout1.addWidget(self.clearAllButton)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList, 1, 0)
        baseGrid.addLayout(customButtonsLayout0, 2, 0)
        baseGrid.addLayout(customButtonsLayout1, 3, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(
            self, buttons=('repair', 'delete', 'cancel', 'previous', 'next'), layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # élève :
        text = QtWidgets.QApplication.translate('main', 'Student:')
        studentLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.studentEdit = QtWidgets.QLineEdit()
        self.studentEdit.setEnabled(False)
        # label du document :
        text = QtWidgets.QApplication.translate('main', 'Description:')
        labelDocumentLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.labelDocumentEdit = QtWidgets.QLineEdit()
        self.labelDocumentEdit.editingFinished.connect(self.doEdited)

        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,            1, 1)
        editGrid.addWidget(studentLabel,               2, 0)
        editGrid.addWidget(self.studentEdit,           2, 1)
        editGrid.addWidget(labelDocumentLabel,          3, 0)
        editGrid.addWidget(self.labelDocumentEdit,  3, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de documents.eleves
        """
        # on récupère les noms des élèves d'après leurs id 
        # (utile pour les fichiers non inscrits dans la base) :
        students = {}
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            eleveName = query_admin.value(2)
            eleveFirstName = query_admin.value(3)
            eleveClasse = query_admin.value(4)
            itemText = utils_functions.u('{0} {1} [{2}]').format(
                eleveName, eleveFirstName, eleveClasse)
            students[query_admin.value(0)] = itemText
        # on récupère la liste des documents :
        docs = verifDocs(self.main, who='eleves', typeDocument=1)
        # on remplit la liste :
        self.modifications = {'modified': False, 'FILES_TO_DELETE': []}
        self.baseList.clear()
        for docState in ('OK', 'InDBOnly', 'FileOnly'):
            color = self.colors[docState]
            toolTip = self.toolTips[docState]
            for doc in docs[docState]:
                if docState == 'FileOnly':
                    fileName = doc
                    # il faut tester si c'est un document perso ou pas, 
                    # en cherchant l'id_eleve :
                    try:
                        id_eleve = int(fileName.split('-')[0])
                        labelDocument = ''
                        typeDocument = 1
                    except:
                        continue
                else:
                    id_eleve = int(doc[0])
                    fileName = doc[1]
                    labelDocument = doc[2]
                    typeDocument = int(doc[3])
                item = QtWidgets.QListWidgetItem(utils_functions.u(fileName))
                studentName = students.get(id_eleve, '???')
                item.setData(
                    QtCore.Qt.UserRole, 
                    [id_eleve, studentName, fileName, labelDocument, typeDocument, docState])
                item.setIcon(self.icons[color])
                if toolTip != '':
                    item.setToolTip(toolTip)
                self.baseList.addItem(item)
        self.repairAllButton.setEnabled(len(docs['FileOnly']) > 0)
        self.deleteFilesAllButton.setEnabled(len(docs['FileOnly']) > 0)
        self.clearAllButton.setEnabled(len(docs['InDBOnly']) > 0)
        # on termine en sélectionnant le premier item :
        if self.baseList.count() < 1:
            for button in self.editButtons[1]:
                self.editButtons[1][button].setEnabled(False)
        self.baseList.setCurrentRow(0)

    def openDocsElevesDir(self):
        """
        ouvre le dossier documents
        """
        dirLocal = admin.dirLocalPrive + '/protected/documents'
        utils_filesdirs.openDir(dirLocal)

    def repairAll(self):
        """
        pour inscrire dans la base les fichiers qui n'y sont pas
        """
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[5] == 'FileOnly':
                data[5] = 'OK'
                if data[3] == '':
                    data[3] = data[2]
                item.setData(QtCore.Qt.UserRole, data)
                item.setToolTip(self.toolTips['OK'])
                item.setIcon(
                    utils.doIcon('color-{0}'.format(self.colors['OK'])))
        self.editButtons[1]['repair'].setEnabled(False)
        self.repairAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.doModified()

    def deleteFilesAll(self):
        """
        pour supprimer les fichiers qui ne sont pas inscrits dans la base
        """
        filesNumber = self.baseList.count()
        for index in range(filesNumber):
            itemIndex = filesNumber - 1 - index
            item = self.baseList.item(itemIndex)
            data = item.data(QtCore.Qt.UserRole)
            if data[5] == 'FileOnly':
                self.modifications['FILES_TO_DELETE'].append(data[2])
                current = self.baseList.takeItem(itemIndex)
                del(current)
        self.editButtons[1]['delete'].setEnabled(False)
        self.deleteFilesAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.baseList.setCurrentRow(0)
        self.doModified()

    def clearAll(self):
        """
        efface de la base les fichiers inexistants
        """
        filesNumber = self.baseList.count()
        for index in range(filesNumber):
            itemIndex = filesNumber - 1 - index
            item = self.baseList.item(itemIndex)
            data = item.data(QtCore.Qt.UserRole)
            if data[5] == 'InDBOnly':
                current = self.baseList.takeItem(itemIndex)
                del(current)
        self.editButtons[1]['delete'].setEnabled(False)
        self.deleteFilesAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.baseList.setCurrentRow(0)
        self.doModified()

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        try:
            self.lastData = current.data(QtCore.Qt.UserRole)
        except:
            return
        actions = {
            self.studentEdit: ('TEXT', 1),
            self.labelDocumentEdit: ('TEXT', 3),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'COMBOBOX':
                action.setCurrentIndex(self.lastData[actions[action][1]])
            elif actions[action][0] == 'CHECKBOX':
                action.setChecked(self.lastData[actions[action][1]] == 1)
            elif actions[action][0] == 'INTEGER':
                value = self.lastData[actions[action][1]]
                if value < 0:
                    action.setMinimum(-10)
                else:
                    action.setMinimum(0)
                action.setValue(value)
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 0)
        self.editButtons[1]['repair'].setEnabled(self.lastData[5] == 'FileOnly')

    def itemDoubleClicked(self, item):
        """
        on ouvre le fichier
        """
        data = item.data(QtCore.Qt.UserRole)
        if data[5] == 'InDBOnly':
            return
        fileName = utils_functions.u('{0}/protected/documents/{1}').format(
            admin.dirLocalPrive, data[2])
        utils_filesdirs.openFile(fileName)

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        if self.sender() == self.labelDocumentEdit:
            data[3] = newText
            current.setData(QtCore.Qt.UserRole, data)
        self.modifications['modified'] = True
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'repair':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[5] = 'OK'
            current.setData(QtCore.Qt.UserRole, data)
            current.setToolTip(self.toolTips['OK'])
            current.setIcon(
                utils.doIcon('color-{0}'.format(self.colors['OK'])))
            self.editButtons[1]['repair'].setEnabled(False)
            self.modifications['modified'] = True
            self.doModified()
        elif what == 'delete':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.modifications['FILES_TO_DELETE'].append(data[2])
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.modifications['modified'] = True
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()
        self.parent.mustUpload = True

    def checkModifications(self):
        """
        modification des tables et des fichiers concernés par le dialog.
        """
        if not(self.modifications['modified']):
            return
        self.parent.modifiedTables['documents'] = True
        self.parent.modifiedTables['eleves'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[5] != 'FileOnly':
                lines.append((data[0], data[2], data[3], data[4]))
        # on réécrit la table :
        admin.openDB(self.main, 'documents')
        query_documents, transactionOK = utils_db.queryTransactionDB(
            admin.db_documents)
        try:
            # on efface les enregistrements précédents :
            commandLine = utils_db.qdf_where.format('eleves', 'type', 1)
            query_documents = utils_db.queryExecute(commandLine, query=query_documents)
            # on écrit les lignes :
            commandLine = utils_db.insertInto('eleves', 4)
            query_documents = utils_db.queryExecute(
                {commandLine: lines}, query=query_documents)
        finally:
            utils_db.endTransaction(
                query_documents, admin.db_documents, transactionOK)
        # on efface les fichiers supprimés :
        sousDir = '/protected/documents'
        dirLocal = admin.dirLocalPrive + sousDir
        for fileName in self.modifications['FILES_TO_DELETE']:
            QtCore.QFile(dirLocal + '/' + fileName).remove()


class StudentsDocumentsDlg(QtWidgets.QDialog):
    """
    Dialogue pour gérer les fichiers communs des élèves
    """
    def __init__(self, parent=None):
        super(StudentsDocumentsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'manage-documents'
        # pour gérer ce qui est modifié :
        self.modifications = {'modified': False, 'FILES_TO_DELETE': []}
        # pour annuler :
        self.lastData = []

        self.colors = {'OK': 'green', 'InDBOnly': 'red', 'FileOnly': 'magenta'}
        self.toolTips = {
            'OK': '', 
            'InDBOnly': QtWidgets.QApplication.translate(
                'main', 'This file does not exist in the "documents" folder.'), 
            'FileOnly': QtWidgets.QApplication.translate(
                'main', 'This file is not registered in the database.')}
        self.icons = {}
        for color in ('green', 'red', 'magenta'):
            self.icons[color] = utils.doIcon('color-{0}'.format(color))

        # la zone de sélection (liste des fichiers + boutons) :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        self.baseList.itemDoubleClicked.connect(self.itemDoubleClicked)
        self.openDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-eleves'), 
            QtWidgets.QApplication.translate('main', 'documents'),
            toolTip = QtWidgets.QApplication.translate('main', 'OpenDocsElevesDirToolTip'))
        self.openDirButton.clicked.connect(self.openDocsElevesDir)
        self.reloadFilesButton = QtWidgets.QPushButton(
            utils.doIcon('view-refresh'), 
            QtWidgets.QApplication.translate('main', 'reloadFiles'),
            toolTip = QtWidgets.QApplication.translate('main', 'reloadFilesToolTip'))
        self.reloadFilesButton.clicked.connect(self.initialize)
        self.repairAllButton = QtWidgets.QPushButton(
            utils.doIcon('list-repair'), 
            QtWidgets.QApplication.translate('main', 'repairAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'repairAllToolTip'))
        self.repairAllButton.clicked.connect(self.repairAll)
        self.deleteFilesAllButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'), 
            QtWidgets.QApplication.translate('main', 'deleteFilesAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'deleteFilesAllToolTip'))
        self.deleteFilesAllButton.clicked.connect(self.deleteFilesAll)
        self.clearAllButton = QtWidgets.QPushButton(
            utils.doIcon('edit-clear'), 
            QtWidgets.QApplication.translate('main', 'clearAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'clearAllToolTip'))
        self.clearAllButton.clicked.connect(self.clearAll)
        customButtonsLayout0 = QtWidgets.QHBoxLayout()
        customButtonsLayout0.addWidget(self.openDirButton)
        customButtonsLayout0.addWidget(self.reloadFilesButton)
        customButtonsLayout1 = QtWidgets.QHBoxLayout()
        customButtonsLayout1.addWidget(self.repairAllButton)
        customButtonsLayout1.addWidget(self.deleteFilesAllButton)
        customButtonsLayout1.addWidget(self.clearAllButton)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList, 1, 0)
        baseGrid.addLayout(customButtonsLayout0, 2, 0)
        baseGrid.addLayout(customButtonsLayout1, 3, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(
            self, buttons=('add', 'repair', 'delete', 'cancel', 'previous', 'next'), layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # label du document :
        text = QtWidgets.QApplication.translate('main', 'Description:')
        labelDocumentLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.labelDocumentEdit = QtWidgets.QLineEdit()
        self.labelDocumentEdit.editingFinished.connect(self.doEdited)
        # pour qui est ce document :
        text = QtWidgets.QApplication.translate('main', 'Students:')
        studentsLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.allStudentsRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'allStudents'))
        self.allStudentsRadio.setChecked(True)
        self.allStudentsRadio.toggled.connect(self.studentsToggled)
        self.selectedStudentsRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'selectedStudents'))
        self.selectedStudentsRadio.toggled.connect(self.studentsToggled)
        self.selectStudentsButton = QtWidgets.QPushButton(
            utils.doIcon('groupes'), 
            QtWidgets.QApplication.translate('main', 'selectStudents'),
            toolTip = QtWidgets.QApplication.translate('main', 'selectStudentsToolTip'))
        self.selectStudentsButton.clicked.connect(self.selectStudents)
        studentsVbox = QtWidgets.QVBoxLayout()
        studentsVbox.addWidget(self.allStudentsRadio)
        studentsVbox.addWidget(self.selectedStudentsRadio)
        studentsVbox.addWidget(self.selectStudentsButton)
        studentsVbox.addStretch(1)

        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,            1, 1)
        editGrid.addWidget(labelDocumentLabel,      3, 0)
        editGrid.addWidget(self.labelDocumentEdit,  3, 1)
        editGrid.addWidget(studentsLabel,      4, 0)
        editGrid.addLayout(studentsVbox,  5, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de documents.eleves :
        """
        self.fromSoft = True
        # on récupère la liste des documents :
        docs = verifDocs(self.main, who='eleves', typeDocument=0)
        # on remplit la liste :
        self.modifications = {'modified': False, 'FILES_TO_DELETE': []}
        self.baseList.clear()
        for docState in ('OK', 'InDBOnly', 'FileOnly'):
            color = self.colors[docState]
            toolTip = self.toolTips[docState]
            files = {}
            for doc in docs[docState]:
                if docState == 'FileOnly':
                    fileName = doc
                    # il faut tester si c'est un document perso ou pas, 
                    # en cherchant l'id_eleve :
                    try:
                        id_eleve = int(fileName.split('-')[0])
                        continue
                    except:
                        id_eleve = -1
                        labelDocument = ''
                        typeDocument = 1
                else:
                    id_eleve = int(doc[0])
                    fileName = doc[1]
                    labelDocument = doc[2]
                    typeDocument = int(doc[3])
                if fileName in files:
                    files[fileName][0].append(id_eleve)
                else:
                    files[fileName] = [[id_eleve], fileName, labelDocument, typeDocument, docState]
            for fileName in files:
                item = QtWidgets.QListWidgetItem(utils_functions.u(fileName))
                item.setData(QtCore.Qt.UserRole, files[fileName])
                item.setIcon(self.icons[color])
                if toolTip != '':
                    item.setToolTip(toolTip)
                self.baseList.addItem(item)
        self.repairAllButton.setEnabled(len(docs['FileOnly']) > 0)
        self.deleteFilesAllButton.setEnabled(len(docs['FileOnly']) > 0)
        self.clearAllButton.setEnabled(len(docs['InDBOnly']) > 0)
        # on termine en sélectionnant le premier item :
        if self.baseList.count() < 1:
            for button in self.editButtons[1]:
                if button != 'add':
                    self.editButtons[1][button].setEnabled(False)
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def openDocsElevesDir(self):
        """
        ouvre le dossier documents
        """
        dirLocal = admin.dirLocalPrive + '/protected/documents'
        utils_filesdirs.openDir(dirLocal)

    def repairAll(self):
        """
        pour inscrire dans la base les fichiers qui n'y sont pas
        """
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] == 'FileOnly':
                data[4] = 'OK'
                if data[2] == '':
                    data[2] = data[1]
                item.setData(QtCore.Qt.UserRole, data)
                item.setToolTip(self.toolTips['OK'])
                item.setIcon(
                    utils.doIcon('color-{0}'.format(self.colors['OK'])))
        self.editButtons[1]['repair'].setEnabled(False)
        self.repairAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.doModified()

    def deleteFilesAll(self):
        """
        pour supprimer les fichiers qui ne sont pas inscrits dans la base
        """
        filesNumber = self.baseList.count()
        for index in range(filesNumber):
            itemIndex = filesNumber - 1 - index
            item = self.baseList.item(itemIndex)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] == 'FileOnly':
                self.modifications['FILES_TO_DELETE'].append(data[1])
                current = self.baseList.takeItem(itemIndex)
                del(current)
        self.editButtons[1]['delete'].setEnabled(False)
        self.deleteFilesAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.doModified()
        self.baseList.setCurrentRow(0)

    def clearAll(self):
        """
        efface de la base les fichiers inexistants
        """
        filesNumber = self.baseList.count()
        for index in range(filesNumber):
            itemIndex = filesNumber - 1 - index
            item = self.baseList.item(itemIndex)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] == 'InDBOnly':
                current = self.baseList.takeItem(itemIndex)
                del(current)
        self.editButtons[1]['delete'].setEnabled(False)
        self.deleteFilesAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.doModified()
        self.baseList.setCurrentRow(0)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        try:
            self.lastData = current.data(QtCore.Qt.UserRole)
        except:
            return
        self.fromSoft = True
        # mise à jour des boutons radio :
        if self.lastData[0] == [-1]:
            self.allStudentsRadio.setChecked(True)
            self.selectStudentsButton.setEnabled(False)
        else:
            self.selectedStudentsRadio.setChecked(True)
            self.selectStudentsButton.setEnabled(True)
        # et le reste :
        actions = {
            self.labelDocumentEdit: ('TEXT', 2),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'COMBOBOX':
                action.setCurrentIndex(self.lastData[actions[action][1]])
            elif actions[action][0] == 'CHECKBOX':
                action.setChecked(self.lastData[actions[action][1]] == 1)
            elif actions[action][0] == 'INTEGER':
                value = self.lastData[actions[action][1]]
                if value < 0:
                    action.setMinimum(-10)
                else:
                    action.setMinimum(0)
                action.setValue(value)
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 0)
        self.editButtons[1]['repair'].setEnabled(self.lastData[4] == 'FileOnly')
        self.fromSoft = False

    def studentsToggled(self, checked):
        if self.fromSoft:
            return
        if checked:
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if self.sender() == self.allStudentsRadio:
                data[0] = [-1]
                self.selectStudentsButton.setEnabled(False)
            else:
                data[0] = []
                self.selectStudentsButton.setEnabled(True)
            current.setData(QtCore.Qt.UserRole, data)
            self.modifications['modified'] = True
            self.doModified()

    def selectStudents(self):
        idsEleves = []
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        dialog = admin.ListElevesDlg(
            parent=self.main, 
            helpMessage=1, 
            firstSelection=data[0], 
            helpContextPage='manage-documents')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            for i in range(dialog.selectionList.count()):
                item = dialog.selectionList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                idsEleves.append(id_eleve)
            data[0] = idsEleves
            current.setData(QtCore.Qt.UserRole, data)
            self.modifications['modified'] = True
            self.doModified()

    def itemDoubleClicked(self, item):
        """
        on ouvre le fichier
        """
        data = item.data(QtCore.Qt.UserRole)
        if data[4] == 'InDBOnly':
            return
        fileName = utils_functions.u('{0}/protected/documents/{1}').format(
            admin.dirLocalPrive, data[1])
        utils_filesdirs.openFile(fileName)

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        if self.sender() == self.labelDocumentEdit:
            data[2] = newText
            current.setData(QtCore.Qt.UserRole, data)
        self.modifications['modified'] = True
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            newFile = QtWidgets.QFileDialog.getOpenFileName(
                self.main, 
                QtWidgets.QApplication.translate('main', 'Choose File'),
                self.main.workDir, 
                QtWidgets.QApplication.translate('main', 'All files (*.*)'))
            newFile = utils_functions.verifyLibs_fileName(newFile)
            if newFile != '':
                fileName = QtCore.QFileInfo(newFile).fileName()
                newFile2 = utils_functions.u('{0}/protected/documents/{1}').format(
                    admin.dirLocalPrive, fileName)
                utils_filesdirs.removeAndCopy(newFile, newFile2)
                item = QtWidgets.QListWidgetItem(fileName)
                data = [[-1], fileName, '', 0, 'OK']
                item.setData(QtCore.Qt.UserRole, data)
                item.setToolTip(self.toolTips['OK'])
                item.setIcon(
                    utils.doIcon('color-{0}'.format(self.colors['OK'])))
                self.baseList.addItem(item)
                self.baseList.setCurrentRow(self.baseList.count() - 1)
                self.modifications['modified'] = True
                self.doModified()
        elif what == 'repair':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[4] = 'OK'
            current.setData(QtCore.Qt.UserRole, data)
            current.setToolTip(self.toolTips['OK'])
            current.setIcon(
                utils.doIcon('color-{0}'.format(self.colors['OK'])))
            self.editButtons[1]['repair'].setEnabled(False)
            self.modifications['modified'] = True
            self.doModified()
        elif what == 'delete':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.modifications['FILES_TO_DELETE'].append(data[1])
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.modifications['modified'] = True
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()
        self.parent.mustUpload = True

    def checkModifications(self):
        """
        modification des tables et des fichiers concernés par le dialog.
        """
        if not(self.modifications['modified']):
            return
        self.parent.modifiedTables['documents'] = True
        self.parent.modifiedTables['eleves'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] != 'FileOnly':
                for id_eleve in data[0]:
                    lines.append((id_eleve, data[1], data[2], data[3]))
        # on réécrit la table :
        admin.openDB(self.main, 'documents')
        query_documents, transactionOK = utils_db.queryTransactionDB(
            admin.db_documents)
        try:
            # on efface les enregistrements précédents :
            commandLine = utils_db.qdf_where.format('eleves', 'type', 0)
            query_documents = utils_db.queryExecute(commandLine, query=query_documents)
            # on écrit les lignes :
            commandLine = utils_db.insertInto('eleves', 4)
            query_documents = utils_db.queryExecute(
                {commandLine: lines}, query=query_documents)
        finally:
            utils_db.endTransaction(
                query_documents, admin.db_documents, transactionOK)
        # on efface les fichiers supprimés :
        sousDir = '/protected/documents'
        dirLocal = admin.dirLocalPrive + sousDir
        for fileName in self.modifications['FILES_TO_DELETE']:
            QtCore.QFile(dirLocal + '/' + fileName).remove()


class TeachersDocumentsDlg(QtWidgets.QDialog):
    """
    Dialogue pour gérer les fichiers mis à disposition des profs
    """
    def __init__(self, parent=None):
        super(TeachersDocumentsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'manage-documents'
        # pour gérer ce qui est modifié :
        self.modifications = {'modified': False, 'FILES_TO_DELETE': []}
        # pour annuler :
        self.lastData = []

        self.colors = {'OK': 'green', 'InDBOnly': 'red', 'FileOnly': 'magenta'}
        self.toolTips = {
            'OK': '', 
            'InDBOnly': QtWidgets.QApplication.translate(
                'main', 'This file does not exist in the "docsprofs" folder.'), 
            'FileOnly': QtWidgets.QApplication.translate(
                'main', 'This file is not registered in the database.')}
        self.icons = {}
        for color in ('green', 'red', 'magenta'):
            self.icons[color] = utils.doIcon('color-{0}'.format(color))

        # la zone de sélection (liste des fichiers + boutons) :
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        self.baseList.itemDoubleClicked.connect(self.itemDoubleClicked)
        self.openDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder-profs'), 
            QtWidgets.QApplication.translate('main', 'docsprofs'),
            toolTip = QtWidgets.QApplication.translate('main', 'OpenDocsTeachersDirToolTip'))
        self.openDirButton.clicked.connect(self.openDocsProfsDir)
        self.reloadFilesButton = QtWidgets.QPushButton(
            utils.doIcon('view-refresh'), 
            QtWidgets.QApplication.translate('main', 'reloadFiles'),
            toolTip = QtWidgets.QApplication.translate('main', 'reloadFilesToolTip'))
        self.reloadFilesButton.clicked.connect(self.initialize)
        self.repairAllButton = QtWidgets.QPushButton(
            utils.doIcon('list-repair'), 
            QtWidgets.QApplication.translate('main', 'repairAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'repairAllToolTip'))
        self.repairAllButton.clicked.connect(self.repairAll)
        self.deleteFilesAllButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'), 
            QtWidgets.QApplication.translate('main', 'deleteFilesAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'deleteFilesAllToolTip'))
        self.deleteFilesAllButton.clicked.connect(self.deleteFilesAll)
        self.clearAllButton = QtWidgets.QPushButton(
            utils.doIcon('edit-clear'), 
            QtWidgets.QApplication.translate('main', 'clearAll'),
            toolTip = QtWidgets.QApplication.translate('main', 'clearAllToolTip'))
        self.clearAllButton.clicked.connect(self.clearAll)
        customButtonsLayout0 = QtWidgets.QHBoxLayout()
        customButtonsLayout0.addWidget(self.openDirButton)
        customButtonsLayout0.addWidget(self.reloadFilesButton)
        customButtonsLayout1 = QtWidgets.QHBoxLayout()
        customButtonsLayout1.addWidget(self.repairAllButton)
        customButtonsLayout1.addWidget(self.deleteFilesAllButton)
        customButtonsLayout1.addWidget(self.clearAllButton)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.baseList, 1, 0)
        baseGrid.addLayout(customButtonsLayout0, 2, 0)
        baseGrid.addLayout(customButtonsLayout1, 3, 0)

        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(
            self, buttons=('add', 'repair', 'delete', 'cancel', 'previous', 'next'), layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # label du document :
        text = QtWidgets.QApplication.translate('main', 'Description:')
        labelDocumentLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.labelDocumentEdit = QtWidgets.QLineEdit()
        self.labelDocumentEdit.editingFinished.connect(self.doEdited)
        # type du document :
        typeLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Type:')))
        self.typeComboBox = QtWidgets.QComboBox()
        listTypes = (
            (0, QtWidgets.QApplication.translate('main', 'shared documents (booklet ...)')), 
            (1, QtWidgets.QApplication.translate('main', 'results (reports, ...)')), 
            (2, QtWidgets.QApplication.translate('main', 'confidential documents (for teachers only)')))
        for item in listTypes:
            self.typeComboBox.addItem(item[1], item[0])
        self.typeComboBox.activated.connect(self.doChanged)

        # editGrid :
        editGrid = QtWidgets.QGridLayout()
        editGrid.addLayout(buttonsLayout,            1, 1)
        editGrid.addWidget(labelDocumentLabel,      3, 0)
        editGrid.addWidget(self.labelDocumentEdit,  3, 1)
        editGrid.addWidget(typeLabel,      4, 0)
        editGrid.addWidget(self.typeComboBox,  4, 1)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(editGrid)
        vLayout.addStretch(1)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(vLayout, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        lecture de documents.profs :
        """
        self.fromSoft = True
        # on récupère la liste des documents :
        docs = verifDocs(self.main, who='profs')
        # on remplit la liste :
        self.modifications = {'modified': False, 'FILES_TO_DELETE': []}
        self.baseList.clear()
        for docState in ('OK', 'InDBOnly', 'FileOnly'):
            color = self.colors[docState]
            toolTip = self.toolTips[docState]
            for doc in docs[docState]:
                if docState == 'FileOnly':
                    id_doc = -1
                    fileName = doc
                    labelDocument = ''
                    typeDocument = 1
                else:
                    id_doc = int(doc[0])
                    fileName = doc[1]
                    labelDocument = doc[2]
                    typeDocument = int(doc[3])
                item = QtWidgets.QListWidgetItem(utils_functions.u(fileName))
                item.setData(
                    QtCore.Qt.UserRole, 
                    [id_doc, fileName, labelDocument, typeDocument, docState])
                item.setIcon(
                    self.icons[color])
                if toolTip != '':
                    item.setToolTip(toolTip)
                self.baseList.addItem(item)
        self.repairAllButton.setEnabled(len(docs['FileOnly']) > 0)
        self.deleteFilesAllButton.setEnabled(len(docs['FileOnly']) > 0)
        self.clearAllButton.setEnabled(len(docs['InDBOnly']) > 0)
        # on termine en sélectionnant le premier item :
        if self.baseList.count() < 1:
            for button in self.editButtons[1]:
                if button != 'add':
                    self.editButtons[1][button].setEnabled(False)
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def openDocsProfsDir(self):
        """
        ouvre le dossier docsprofs
        """
        dirLocal = admin.dirLocalPrive + '/protected/docsprofs'
        utils_filesdirs.openDir(dirLocal)

    def repairAll(self):
        """
        pour inscrire dans la base les fichiers qui n'y sont pas
        """
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] == 'FileOnly':
                data[4] = 'OK'
                if data[2] == '':
                    data[2] = data[1]
                item.setData(QtCore.Qt.UserRole, data)
                item.setToolTip(self.toolTips['OK'])
                item.setIcon(
                    utils.doIcon('color-{0}'.format(self.colors['OK'])))
        self.editButtons[1]['repair'].setEnabled(False)
        self.repairAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.doModified()

    def deleteFilesAll(self):
        """
        pour supprimer les fichiers qui ne sont pas inscrits dans la base
        """
        filesNumber = self.baseList.count()
        for index in range(filesNumber):
            itemIndex = filesNumber - 1 - index
            item = self.baseList.item(itemIndex)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] == 'FileOnly':
                self.modifications['FILES_TO_DELETE'].append(data[1])
                current = self.baseList.takeItem(itemIndex)
                del(current)
        self.editButtons[1]['delete'].setEnabled(False)
        self.deleteFilesAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.doModified()
        self.baseList.setCurrentRow(0)

    def clearAll(self):
        """
        efface de la base les fichiers inexistants
        """
        filesNumber = self.baseList.count()
        for index in range(filesNumber):
            itemIndex = filesNumber - 1 - index
            item = self.baseList.item(itemIndex)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] == 'InDBOnly':
                current = self.baseList.takeItem(itemIndex)
                del(current)
        self.editButtons[1]['delete'].setEnabled(False)
        self.deleteFilesAllButton.setEnabled(False)
        self.modifications['modified'] = True
        self.doModified()
        self.baseList.setCurrentRow(0)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met les champs et les boutons d'édition à jour.
        """
        try:
            self.lastData = current.data(QtCore.Qt.UserRole)
        except:
            return
        self.fromSoft = True
        actions = {
            self.labelDocumentEdit: ('TEXT', 2),
            self.typeComboBox: ('COMBOBOX', 3),
            self.editButtons[1]['delete']: ('BUTTON', -1)}
        for action in actions:
            if actions[action][0] == 'TEXT':
                action.setText(utils_functions.u(self.lastData[actions[action][1]]))
            elif actions[action][0] == 'COMBOBOX':
                action.setCurrentIndex(self.lastData[actions[action][1]])
            elif actions[action][0] == 'CHECKBOX':
                action.setChecked(self.lastData[actions[action][1]] == 1)
            elif actions[action][0] == 'INTEGER':
                value = self.lastData[actions[action][1]]
                if value < 0:
                    action.setMinimum(-10)
                else:
                    action.setMinimum(0)
                action.setValue(value)
            elif actions[action][0] == 'BUTTON':
                action.setEnabled(self.baseList.count() > 0)
        self.editButtons[1]['repair'].setEnabled(self.lastData[4] == 'FileOnly')
        self.fromSoft = False

    def doChanged(self, value):
        if self.fromSoft:
            return
        if self.sender() == self.typeComboBox:
            newTypeDocument = self.typeComboBox.currentIndex()
            # mise à jour de l'item :
            current = self.baseList.currentItem()
            try:
                data = current.data(QtCore.Qt.UserRole)
            except:
                return
            data[3] = newTypeDocument
            current.setData(QtCore.Qt.UserRole, data)
            self.modifications['modified'] = True
            self.doModified()

    def itemDoubleClicked(self, item):
        """
        on ouvre le fichier
        """
        data = item.data(QtCore.Qt.UserRole)
        if data[4] == 'InDBOnly':
            return
        fileName = utils_functions.u('{0}/protected/docsprofs/{1}').format(
            admin.dirLocalPrive, data[1])
        utils_filesdirs.openFile(fileName)

    def doEdited(self):
        """
        un champ a été modifié. On met à jour les données et l'affichage.
        """
        newText = self.sender().text()
        current = self.baseList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        if self.sender() == self.labelDocumentEdit:
            data[2] = newText
            current.setData(QtCore.Qt.UserRole, data)
        self.modifications['modified'] = True
        self.doModified()

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        what = self.sender().objectName()
        if what == 'add':
            newFile = QtWidgets.QFileDialog.getOpenFileName(
                self.main, 
                QtWidgets.QApplication.translate('main', 'Choose File'),
                self.main.workDir, 
                QtWidgets.QApplication.translate('main', 'All files (*.*)'))
            newFile = utils_functions.verifyLibs_fileName(newFile)
            if newFile != '':
                fileName = QtCore.QFileInfo(newFile).fileName()
                newFile2 = utils_functions.u('{0}/protected/docsprofs/{1}').format(
                    admin.dirLocalPrive, fileName)
                utils_filesdirs.removeAndCopy(newFile, newFile2)
                item = QtWidgets.QListWidgetItem(fileName)
                data = [-1, fileName, '', 0, 'OK']
                item.setData(QtCore.Qt.UserRole, data)
                item.setToolTip(self.toolTips['OK'])
                item.setIcon(
                    utils.doIcon('color-{0}'.format(self.colors['OK'])))
                self.baseList.addItem(item)
                self.baseList.setCurrentRow(self.baseList.count() - 1)
                self.modifications['modified'] = True
                self.doModified()
        elif what == 'repair':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[4] = 'OK'
            current.setData(QtCore.Qt.UserRole, data)
            current.setToolTip(self.toolTips['OK'])
            current.setIcon(
                utils.doIcon('color-{0}'.format(self.colors['OK'])))
            self.editButtons[1]['repair'].setEnabled(False)
            self.modifications['modified'] = True
            self.doModified()
        elif what == 'delete':
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            self.modifications['FILES_TO_DELETE'].append(data[1])
            index = self.baseList.indexFromItem(current).row()
            current = self.baseList.takeItem(index)
            del(current)
            if index == self.baseList.count():
                index -= 1
            self.baseList.setCurrentRow(index)
            self.modifications['modified'] = True
            self.doModified()
        elif what == 'cancel':
            current = self.baseList.currentItem()
            current.setData(QtCore.Qt.UserRole, self.lastData)
            self.doCurrentItemChanged(current, current)
        elif what == 'previous':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() - 1
            if index < 0:
                index = self.baseList.count() - 1
            self.baseList.setCurrentRow(index)
        elif what == 'next':
            current = self.baseList.currentItem()
            index = self.baseList.indexFromItem(current).row() + 1
            if index == self.baseList.count():
                index = 0
            self.baseList.setCurrentRow(index)

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()
        self.parent.mustUpload = True

    def checkModifications(self):
        """
        modification des tables et des fichiers concernés par le dialog.
        """
        if not(self.modifications['modified']):
            return
        self.parent.modifiedTables['documents'] = True
        self.parent.modifiedTables['profs'] = True
        lines = []
        for index in range(self.baseList.count()):
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[4] != 'FileOnly':
                lines.append((data[0], data[1], data[2], data[3]))
        # on réécrit la table :
        admin.openDB(self.main, 'documents')
        query_documents, transactionOK = utils_db.queryTransactionDB(
            admin.db_documents)
        try:
            # on efface les enregistrements précédents :
            commandLine = utils_db.qdf_table.format('profs')
            query_documents = utils_db.queryExecute(commandLine, query=query_documents)
            # on écrit les lignes :
            commandLine = utils_db.insertInto('profs', 4)
            query_documents = utils_db.queryExecute(
                {commandLine: lines}, query=query_documents)
        finally:
            utils_db.endTransaction(
                query_documents, admin.db_documents, transactionOK)
        # on efface les fichiers supprimés :
        sousDir = '/protected/docsprofs'
        dirLocal = admin.dirLocalPrive + sousDir
        for fileName in self.modifications['FILES_TO_DELETE']:
            QtCore.QFile(dirLocal + '/' + fileName).remove()


class ByStudentDlg(QtWidgets.QDialog):
    """
    Dialogue pour afficher les documents par élève.
    """
    def __init__(self, parent=None):
        super(ByStudentDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'manage-documents'

        self.colors = {'OK': 'green', 'InDBOnly': 'red', 'FileOnly': 'magenta'}
        self.toolTips = {
            'OK': '', 
            'InDBOnly': QtWidgets.QApplication.translate(
                'main', 'This file does not exist in the "documents" folder.')}
        self.icons = {}
        for color in ('green', 'red', 'magenta'):
            self.icons[color] = utils.doIcon('color-{0}'.format(color))

        # un dico pour remplacer temporairement la table :
        self.eleves = {}

        # la zone de sélection :
        self.filterComboBox = QtWidgets.QComboBox()
        self.filterComboBox.activated.connect(self.filterComboBoxChanged)
        self.baseList = QtWidgets.QListWidget()
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        baseGrid = QtWidgets.QGridLayout()
        baseGrid.addWidget(self.filterComboBox, 1, 0)
        baseGrid.addWidget(self.baseList,       2, 0)

        # la zone d'édition :
        self.docsList = QtWidgets.QListWidget()
        self.docsList.itemDoubleClicked.connect(self.itemDoubleClicked)
        editGrid = QtWidgets.QGridLayout()
        editGrid.addWidget(self.docsList, 1, 0)

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addLayout(baseGrid, 1, 0)
        grid.addLayout(editGrid, 1, 1)
        self.setLayout(grid)
        self.setMinimumWidth(500)
        self.initialize()

    def initialize(self):
        """
        remplissage du comboBox des classes (filterComboBox)
        et du dico self.eleves.
        """
        # filtre "toutes les classes" :
        self.filterComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'All the classes'))
        self.eleves['ALL'] = []
        # on récupère la liste des classes :
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            classe = query_commun.value(1)
            self.filterComboBox.addItem(classe)
            self.eleves[classe] = []
        # lecture de admin.eleves et remplissage de self.eleves :
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            id_eleve = int(record[0])
            eleveName = record[2]
            eleveFirstName = record[3]
            eleveClasse = record[4]
            itemText = utils_functions.u('{0} {1} [{2}]').format(
                eleveName, eleveFirstName, eleveClasse)
            itemData = [
                itemText, id_eleve, eleveName, eleveFirstName, eleveClasse]
            self.eleves[eleveClasse].append(itemData)
            self.eleves['ALL'].append(itemData)
        self.filterComboBoxChanged(0)

    def filterComboBoxChanged(self, index=-1):
        """
        mise à jour de la liste selon la classe sélectionnée
        """
        if index < 0:
            index = self.filterComboBox.currentIndex()
        else:
            self.filterComboBox.setCurrentIndex(index)
        selected = self.filterComboBox.currentText()
        if index == 0:
            selected = 'ALL'
        self.baseList.clear()
        if not(selected in self.eleves):
            return
        for itemData in self.eleves[selected]:
            item = QtWidgets.QListWidgetItem(itemData[0])
            item.setData(QtCore.Qt.UserRole, itemData)
            self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection ; on met la liste des documents à jour.
        On a besoin d'un try except en cas de changement de sélection de classe.
        """
        self.docsList.clear()
        try:
            data = current.data(QtCore.Qt.UserRole)
        except:
            return
        sousDir = '/protected/documents'
        dirLocal = admin.dirLocalPrive + sousDir
        admin.openDB(self.main, 'documents')
        query_documents = utils_db.query(admin.db_documents)
        commandLine_documents = 'SELECT * FROM eleves WHERE id_eleve IN (-1, {0}) ORDER BY nomFichier'
        commandLine_documents = commandLine_documents.format(data[1])
        query_documents = utils_db.queryExecute(commandLine_documents, query=query_documents)
        while query_documents.next():
            fileName = query_documents.value(1)
            labelDocument = query_documents.value(2)
            typeDoc = int(query_documents.value(3))
            if QtCore.QFileInfo(dirLocal + '/' + fileName).exists():
                docState = 'OK'
            else:
                docState = 'InDBOnly'
            color = self.colors[docState]
            toolTip = ''
            if labelDocument != fileName:
                toolTip = utils_functions.u(labelDocument)
            if docState != 'OK':
                if toolTip != '':
                    toolTip = utils_functions.u('{0}\n{1}').format(
                        toolTip, self.toolTips[docState])
                else:
                    toolTip = utils_functions.u(self.toolTips[docState])
            item = QtWidgets.QListWidgetItem(utils_functions.u(fileName))
            item.setData(
                QtCore.Qt.UserRole, 
                [fileName, labelDocument, typeDoc, docState])
            item.setIcon(self.icons[color])
            if toolTip != '':
                item.setToolTip(toolTip)
            self.docsList.addItem(item)

    def itemDoubleClicked(self, item):
        """
        on ouvre le fichier
        """
        data = item.data(QtCore.Qt.UserRole)
        if data[3] == 'InDBOnly':
            return
        fileName = utils_functions.u('{0}/protected/documents/{1}').format(
            admin.dirLocalPrive, data[0])
        utils_filesdirs.openFile(fileName)

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        return


class DocsUtilsDlg(QtWidgets.QDialog):
    """
    les outils de la page adresses.
    """
    def __init__(self, parent=None):
        super(DocsUtilsDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # page d'aide :
        self.helpContextPage = 'manage-documents'
        # des modifications ont été faites :
        self.modified = False

        # les messages :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)

        # envoi des fichiers :
        self.uploadDocumentsAndDB_Button = QtWidgets.QToolButton()

        # mise en forme des boutons :
        buttons = {
            self.uploadDocumentsAndDB_Button: (
                'net-upload', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Send documents'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'To send the documents and the '
                    'database on the website'), 
                self.doUploadDocumentsAndDB), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize'] * 2
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])

        # première série d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.uploadDocumentsAndDB_Button)
        hLayout0.addStretch(1)

        # mise en place :
        self.statusBar = QtWidgets.QStatusBar()
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addLayout(hLayout0)
        vLayout.addStretch(1)
        grid = QtWidgets.QHBoxLayout()
        grid.addLayout(vLayout)
        grid.addLayout(messagesLayout)
        self.setLayout(grid)

    def doUploadDocumentsAndDB(self):
        if self.parent.buttonsList['apply'].isEnabled():
            self.parent.doApply()
        uploadDocumentsAndDB(self.main)
        self.parent.mustUpload = False

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        self.modified = False


