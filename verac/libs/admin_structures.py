# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Gestion des fichiers de structures profs.
    Ils peuvent être mis à disposition sur le site web de l'établissement.
    Ce fichier contient la partie admin (gestion des fichiers sur le site)
"""

# importation des modules utiles:
from __future__ import division

import utils, utils_functions, utils_db, utils_filesdirs
import admin, admin_ftp
import utils_markdown

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



def manageStructuresFiles(main, msgFin=True):
    """
    lance le dialog de gestion des structures (ManageStructuresFilesDlg).
    Met ensuite à jour la base et le mirroir sur le site de l'établissement.
    """
    # on lance le dialog :
    dialog = ManageStructuresFilesDlg(parent=main)
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        return
    main.enableInterface(dialog, lastState)
    # on enregistre les modifications dans la base structures.sqlite :
    try:
        admin.openDB(main, 'structures')
        query_structures, transactionOK = utils_db.queryTransactionDB(admin.db_structures)
        # création (si besoin) et vidage de la table structures :
        query_structures = utils_db.queryExecute(
            utils_db.qct_structures_structures, query=query_structures)
        query_structures = utils_db.queryExecute(
            utils_db.qdf_table.format('structures'), query=query_structures)
        lines = []
        commandLine_structures = utils_db.insertInto('structures', 4)
        nbSeparators = -1
        for i in range(dialog.listWidget.count()):
            itemWidget = dialog.listWidget.item(i)
            (matiereName, title, fileName) = itemWidget.data(QtCore.Qt.UserRole)
            if matiereName == 'SEPARATOR':
                nbSeparators += 1
                fileName = 'SEPARATOR-{0}'.format(nbSeparators)
            lines.append((matiereName, title, fileName, i))
        query_structures = utils_db.queryExecute(
            {commandLine_structures: lines}, query=query_structures)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(admin.dbFileTemp_structures)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_db.endTransaction(query_structures, admin.db_structures, transactionOK)
        utils_filesdirs.removeAndCopy(admin.dbFileTemp_structures, admin.dbFile_structures)
    # enfin on envoie les fichiers par FTP :
    m1 = QtWidgets.QApplication.translate('main', 'Updating structures files via FTP')
    utils_functions.afficheMessage(main, m1, tags=('h2'))
    if admin_ftp.ftpTest(main):
        username = admin_ftp.FTP_USER
        password = admin_ftp.FTP_PASS
        host = admin_ftp.FTP_HOST
        localdir = admin.dirLocalPrive + '/protected/structures'
        remotedir = admin.dirSitePrive + '/protected/structures'
        admin_ftp.ftpMakeDir(host, username, password, remotedir)
        import sfm
        result = sfm.doAction(
            username=username, 
            password=password, 
            host=host, 
            port=admin_ftp.FTP_PORT, 
            remotedir=remotedir, 
            localdir=localdir)
        utils_functions.myPrint(result)

class ManageStructuresFilesDlg(QtWidgets.QDialog):
    """
    """
    def __init__(self, parent=None):
        utils_functions.doWaitCursor()
        super(ManageStructuresFilesDlg, self).__init__(parent)
        self.main = parent
        self.helpContextPage = 'manage-structures'
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate(
            'main', 'Management of teacher structures')
        self.setWindowTitle(title)

        # un texte d'aide :
        mdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/manageStructures')

        # boutons pour recharger et ouvrir le dossier des structures :
        openStructuresDirButton = QtWidgets.QPushButton(
            utils.doIcon('folder'),
            QtWidgets.QApplication.translate('main', 'Open folder'))
        openStructuresDirButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Open the folder containing the files structures'))
        openStructuresDirButton.clicked.connect(self.openStructuresDir)
        reloadStructuresButton = QtWidgets.QPushButton(
            utils.doIcon('view-refresh'),
            QtWidgets.QApplication.translate('main', 'Reload list'))
        reloadStructuresButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Reloads structures files list'))
        reloadStructuresButton.clicked.connect(self.loadStructures)

        # pour l'affichage des séparateurs :
        self.nbSeparators = -1
        self.boldFont = QtGui.QFont()
        self.boldFont.setBold(True)
        # les boutons pour ajouter ou supprimer des séparateurs :
        addSeparatorButton = QtWidgets.QPushButton(
            utils.doIcon('list-add'),
            QtWidgets.QApplication.translate('main', 'New'))
        addSeparatorButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'To create a new separator'))
        addSeparatorButton.clicked.connect(self.createSeparator)
        self.deleteSeparatorButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'),
            QtWidgets.QApplication.translate('main', 'Delete'))
        self.deleteSeparatorButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Delete the selected separator'))
        self.deleteSeparatorButton.clicked.connect(self.deleteSeparator)
        self.deleteSeparatorButton.setEnabled(False)

        # liste des structures :
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.loadStructures(first=True)
        self.listWidget.currentItemChanged.connect(self.doCurrentItemChanged)
        self.listWidget.itemDoubleClicked.connect(self.itemDoubleClicked)

        # des boutons :
        dialogButtons = utils_functions.createDialogButtons(self, layout=True)
        buttonsLayout = dialogButtons[0]

        # on agence tout ça :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(mdHelpView)
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(addSeparatorButton)
        hLayout.addWidget(self.deleteSeparatorButton)
        hLayout.addStretch(1)
        hLayout.addWidget(openStructuresDirButton)
        hLayout.addWidget(reloadStructuresButton)
        hLayout.addSpacing(20)
        layout.addLayout(hLayout)
        layout.addWidget(self.listWidget)
        layout.addLayout(buttonsLayout)
        self.setLayout(layout)

        utils_functions.restoreCursor()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, 'admin')

    def openStructuresDir(self):
        dirLocal = admin.dirLocalPrive + '/protected/structures'
        dirName = QtCore.QDir(dirLocal).absolutePath()
        utils_filesdirs.openDir(dirName)

    def loadStructures(self, first=False):
        """
        la vérification se fait en 4 temps :
            * d'abord on lit la base structures.sqlite 
              ou le contenu actuel du listWidget
            * ensuite on cherche les fichiers présents dans le dossier structures
            * on ouvre et lit chaque fichier existant
              pour mettre à jour matière et titre si besoin
            * enfin on remplit le listWidget
        """
        utils_functions.doWaitCursor()
        try:
            structuresFiles = {'ORDER': []}
            self.nbSeparators = -1
            dirLocal = admin.dirLocalPrive + '/protected/structures'
            if first:
                # lecture du fichier structures.sqlite :
                admin.openDB(self.main, 'structures')
                query_structures = utils_db.query(admin.db_structures)
                commandLine_structures = utils_db.q_selectAllFromOrder.format(
                    'structures', 'ordre, matiere, fileName')
                query_structures = utils_db.queryExecute(
                    commandLine_structures, query=query_structures)
                while query_structures.next():
                    matiereName = query_structures.value(0)
                    title = query_structures.value(1)
                    fileName = query_structures.value(2)
                    # on n'affichera que les fichiers existants et les séparateurs :
                    fileExist = QtCore.QFileInfo(
                        utils_functions.u('{0}/{1}').format(dirLocal, fileName)).exists()
                    if (matiereName == 'SEPARATOR') or fileExist:
                        structuresFiles['ORDER'].append(fileName)
                        structuresFiles[fileName] = (matiereName, title)
            else:
                # on se base sur le contenu du listWidget :
                for i in range(self.listWidget.count()):
                    itemWidget = self.listWidget.item(i)
                    (matiereName, title, fileName) = itemWidget.data(QtCore.Qt.UserRole)
                    fileExist = QtCore.QFileInfo(
                        utils_functions.u('{0}/{1}').format(dirLocal, fileName)).exists()
                    if (matiereName == 'SEPARATOR') or fileExist:
                        structuresFiles['ORDER'].append(fileName)
                        structuresFiles[fileName] = (matiereName, title)
            self.listWidget.clear()

            # liste des fichiers existants dans le dossier :
            filesList = QtCore.QDir(dirLocal).entryList(
                ['*'], QtCore.QDir.Files, QtCore.QDir.Name)
            for fileName in filesList:
                ignoreFiles = ('.htaccess', 'structures.sqlite')
                if not(fileName in structuresFiles) and not(fileName in ignoreFiles):
                    structuresFiles['ORDER'].append(fileName)
                    structuresFiles[fileName] = ('', '')
            # lecture des fichiers :
            dbFileTemp_structure = utils_functions.u(
                '{0}/structure_file.sqlite').format(self.main.tempPath)
            for fileName in structuresFiles['ORDER']:
                if fileName[:9] == 'SEPARATOR':
                    continue
                utils_filesdirs.removeAndCopy(
                    dirLocal + '/' + fileName, dbFileTemp_structure)
                (db_structure, dbName) = utils_db.createConnection(
                    self.main, dbFileTemp_structure)
                query_structure = utils_db.query(db_structure)
                try:
                    # on commence par vérifier que le fichier est bien un fichier de structure :
                    configDict = utils_db.configTable2Dict(db_structure)
                    versionDB = configDict.get('versionDB', (-1, ''))
                    if versionDB[1] != 'STRUCTURE':
                        continue
                    if versionDB[0] < utils.VERSIONDB_STRUCTURE:
                        # prévoir ici les modifs de version
                        continue
                    # tables nécessaires à l'affichage des pages.
                    # on récupère les autres données de config :
                    matiereName = configDict.get('matiere', (-1, ''))[1]
                    title = configDict.get('title', (-1, ''))[1]
                    structuresFiles[fileName] = (matiereName, title)
                except:
                    continue
                finally:
                    # on ferme la base :
                    if query_structure != None:
                        query_structure.clear()
                        del query_structure
                    db_structure.close()
                    del db_structure
                    utils_db.sqlDatabase.removeDatabase(dbName)
                    utils_functions.restoreCursor()
            # remplissage du listWidget :
            for fileName in structuresFiles['ORDER']:
                (matiereName, title) = structuresFiles[fileName]
                itemData = (matiereName, title, fileName)
                if matiereName == 'SEPARATOR':
                    self.nbSeparators += 1
                    itemData = (matiereName, title, 'SEPARATOR-{0}'.format(self.nbSeparators))
                    itemText = title
                    itemWidget = QtWidgets.QListWidgetItem(itemText)
                    itemWidget.setFont(self.boldFont)
                    itemWidget.setTextAlignment(QtCore.Qt.AlignHCenter)
                    itemWidget.setBackground(QtGui.QBrush(utils.colorLightGray))
                else:
                    if len(matiereName) < 15:
                        tabs0 = 2
                    else:
                        tabs0 = 1
                    if len(fileName) < 27:
                        tabs1 = 2
                    else:
                        tabs1 = 1
                    itemText = utils_functions.u('{0}{1}[{2}]{3}{4}').format(
                        matiereName, '\t' * tabs0, fileName, '\t' * tabs1, title)
                    itemWidget = QtWidgets.QListWidgetItem(itemText)
                itemWidget.setData(QtCore.Qt.UserRole, itemData)
                self.listWidget.addItem(itemWidget)
        finally:
            utils_functions.restoreCursor()

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection.
        Pour un séparateur, le bouton de suppression est actif
        """
        data = current.data(QtCore.Qt.UserRole)
        self.deleteSeparatorButton.setEnabled(data[0] == 'SEPARATOR')

    def itemDoubleClicked(self, item):
        """
        édition du texte du séparateur
        """
        data = item.data(QtCore.Qt.UserRole)
        if data[0] != 'SEPARATOR':
            return
        text = utils_functions.u('{0}{1}').format(
            QtWidgets.QApplication.translate('main', 'Change the text of a separator:'), 
            ' ' * 100)
        newTitle, OK = QtWidgets.QInputDialog.getText(
            self.main, utils.PROGNAME, 
            text, 
            QtWidgets.QLineEdit.Normal, data[1])
        if not(OK):
            return
        if newTitle != data[1]:
            item.setText(newTitle)
            data = (data[0], newTitle, data[2])
            item.setData(QtCore.Qt.UserRole, data)

    def createSeparator(self):
        """
        création d'un nouveau séparateur
        """
        item = self.listWidget.currentItem()
        index = self.listWidget.indexFromItem(item).row()
        self.nbSeparators += 1
        itemText = QtWidgets.QApplication.translate('main', 'NEW SEPARATOR')
        itemData = ('SEPARATOR', itemText, 'SEPARATOR-{0}'.format(self.nbSeparators))
        itemWidget = QtWidgets.QListWidgetItem(itemText)
        itemWidget.setFont(self.boldFont)
        itemWidget.setTextAlignment(QtCore.Qt.AlignHCenter)
        itemWidget.setBackground(QtGui.QBrush(utils.colorLightGray))
        itemWidget.setData(QtCore.Qt.UserRole, itemData)
        self.listWidget.insertItem(index, itemWidget)

    def deleteSeparator(self):
        """
        suppression d'un séparateur
        """
        item = self.listWidget.currentItem()
        data = item.data(QtCore.Qt.UserRole)
        if data[0] != 'SEPARATOR':
            return
        index = self.listWidget.indexFromItem(item).row()
        item = self.listWidget.takeItem(index)
        del(item)








