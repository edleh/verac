# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    On regroupe ici les procédures d'import export
    depuis et vers xml
"""

# importation des modules utiles:
from __future__ import division

import utils, utils_functions, utils_db

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
    from PyQt5 import QtXml
else:
    from PyQt4 import QtCore, QtGui as QtWidgets
    from PyQt4 import QtXml





def readTextElement(xmlItem, tagName):
    element = xmlItem.elementsByTagName(tagName)
    return element.item(0).toElement().text()



###########################################################"
#   EXPORTS VERS FREEMIND (OU FREEPLANE)
###########################################################"

def initDomDocumentFreeplane(main, name, label=''):
    domDocument = QtXml.QDomDocument()

    freeplanemap = domDocument.createElement('map')
    domDocument.appendChild(freeplanemap)
    freeplanemap.setAttribute('version', '0.9.0')

    mapcenter = domDocument.createElement('node')
    freeplanemap.appendChild(mapcenter)
    if label != '':
        name = name + ': ' + label
    mapcenter.setAttribute('TEXT', name)
    mapcenter.setAttribute('COLOR', '#ff6600')

    fontcenter = domDocument.createElement('font')
    mapcenter.appendChild(fontcenter)
    fontcenter.setAttribute('SIZE', '18')
    fontcenter.setAttribute('BOLD', 'true')
    fontcenter.setAttribute('NAME', 'SansSerif')

    return domDocument, freeplanemap, mapcenter


def addBranchFreeplane(main, domDocument, mapcenter, 
                    name, label='', 
                    flag='', color='', evalColor='',
                    position='right'):
    branch = domDocument.createElement('node')
    mapcenter.appendChild(branch)
    if label != '':
        name = name + ' : ' + label
        name = name.replace('\n', '&#xa;')
        name = name.replace('&amp;#xa;', '&#xa;')
    branch.setAttribute('TEXT', name)
    if color != '':
        branch.setAttribute('COLOR', color)
    branch.setAttribute('POSITION', position)
    branch.setAttribute('FOLDED', 'true')
    if color != '':
        fontbranch = domDocument.createElement('font')
        branch.appendChild(fontbranch)
        fontbranch.setAttribute('SIZE', '14')
        fontbranch.setAttribute('BOLD', 'true')
        fontbranch.setAttribute('NAME', 'SansSerif')
    if flag != '':
        icon = domDocument.createElement('icon')
        branch.appendChild(icon)
        icon.setAttribute('BUILTIN', flag)
    if evalColor != '':
        richcontent = domDocument.createElement('richcontent')
        branch.appendChild(richcontent)
        richcontent.setAttribute('TYPE', 'NODE')
        html = domDocument.createElement('html')
        richcontent.appendChild(html)
        head = domDocument.createElement('head')
        html.appendChild(head)
        body = domDocument.createElement('body')
        html.appendChild(body)
        p = domDocument.createElement('p')
        body.appendChild(p)
        font = domDocument.createElement('font')
        font.setAttribute('COLOR', evalColor)
        p.appendChild(font)
        theText = domDocument.createTextNode(
            utils_functions.u(utils.evalColorChar[1]))
        font.appendChild(theText)
        theText = domDocument.createTextNode(' ' + name)
        p.appendChild(theText)
    return branch


def finalizeDomDocumentFreeplane(main, fileName, name, domDocument, freeplanemap):
    outFile = QtCore.QFile(fileName)
    if not outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
        message = QtWidgets.QApplication.translate(
            'main', 'Cannot write file {0}:\n{1}.')
        message = utils_functions.u(message).format(
            fileName, outFile.errorString())
        utils_functions.messageBox(
            self, level='warning', message=message)
        return
    indentSize = 4
    domDocument.save(QtCore.QTextStream(outFile), indentSize)


def exportBilansItems2Freeplane(main, fileName):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'exportBilansItems2Freeplane')
    utils_functions.doWaitCursor()
    try:
        mapName = QtWidgets.QApplication.translate('main', 'Balances')
        domDocument, freeplanemap, mapcenter = initDomDocumentFreeplane(
            main, mapName, '')
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'UPPER(Name)'), 
            db=main.db_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            bilanName = query_my.value(1)
            bilanLabel = query_my.value(2)
            id_competence = int(query_my.value(3))

            total, faits = 0, 0
            position = "right"
            if id_competence > -1:
                position = "left"
            bilanBranch = addBranchFreeplane(
                main, domDocument, mapcenter, 
                bilanName, label=bilanLabel,
                color="#0000ff", position=position)
            commandLine_my2 = (
                'SELECT items.id_item, items.Name, items.Label, item_bilan.coeff '
                'FROM item_bilan, items '
                'WHERE item_bilan.id_bilan={0} AND item_bilan.id_item=items.id_item '
                'ORDER BY UPPER(items.Name)').format(id_bilan)
            query_my2 = utils_db.queryExecute(commandLine_my2, db=main.db_my)
            while query_my2.next():
                id_item = int(query_my2.value(0))
                itemName = query_my2.value(1)
                itemLabel = query_my2.value(2)
                coeff = int(query_my2.value(3))
                addBranchFreeplane(
                    main, domDocument, bilanBranch, 
                    itemName, label=itemLabel, 
                    position=position)
        finalizeDomDocumentFreeplane(
            main, fileName, mapName, domDocument, freeplanemap)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


def exportActualGroup2Freeplane(main, fileName):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'exportActualGroup2Freeplane')
    utils_functions.doWaitCursor()
    try:
        import prof, prof_groupes
        # on récupère les données liées à la sélection :
        dataSelection = prof.diversFromSelection(main)
        # on récupère tous les tableaux du groupe pour la période considérée
        tableaux = prof_groupes.tableauxFromGroupe(
            main, dataSelection[0], dataSelection[3])
        id_tableaux = utils_functions.array2string(tableaux)
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        # ligne de commande pour lister les items :
        commandLine_items = utils_functions.u(
            'SELECT bilans.*, items.* '
            'FROM bilans '
            'JOIN item_bilan ON item_bilan.id_bilan=bilans.id_bilan '
            'JOIN items ON items.id_item=item_bilan.id_item '
            'WHERE bilans.Name=? '
            'ORDER BY items.Name')
        # la base de la ligne de commande pour chercher les évaluations :
        commandLine_evals = utils_functions.u(
            'SELECT value '
            'FROM evaluations '
            'WHERE id_item={0} '
            'AND id_tableau IN ({1}) '
            'AND value!=""')
        # initialisation du fichier :
        mapName = utils_functions.u('{0}\n{1}').format(
            main.groupeButton.text(), 
            utils_functions.changeBadChars(main.periodeButton.text()))
        domDocument, freeplanemap, mapcenter = initDomDocumentFreeplane(
            main, mapName, '')

        columns, rows = prof.calcBilans(main)
        for i in range(3, len(columns)):
            titleColumn = columns[i]['value']
            bilanName = utils_functions.code2name(main, titleColumn)
            query_my = utils_db.queryExecute(
                {commandLine_items: (bilanName, )}, query=query_my)
            bilanFirst = True
            itemsList = []
            total, faits = 0, 0
            position = 'right'
            while query_my.next():
                if bilanFirst:
                    id_bilan = int(query_my.value(0))
                    bilanLabel = query_my.value(2)
                    id_competence = int(query_my.value(3))
                    if id_competence > -1:
                        position = 'left'
                    # on peut créer la branche pour le bilan :
                    bilanBranch = addBranchFreeplane(
                        main, domDocument, mapcenter, 
                        bilanName, label=bilanLabel,
                        color='#0000ff', position=position)
                    bilanFirst = False
                total += 1
                id_item = int(query_my.value(4))
                itemName = query_my.value(5)
                itemLabel = query_my.value(6)
                itemsList.append((id_item, itemName, itemLabel))

            for (id_item, itemName, itemLabel) in itemsList:
                commandLine_my = commandLine_evals.format(id_item, id_tableaux)
                flagImageItem = ''
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                if query_my.first():
                    flagImageItem = 'button_ok'
                    faits += 1
                # on crée la branche pour l'item :
                addBranchFreeplane(
                    main, domDocument, bilanBranch, 
                    itemName, label=itemLabel, 
                    flag=flagImageItem, position=position)
            # on détermine l'icône du bilan :
            if faits > 0:
                if total == faits:
                    flagImageBilan = 'button_ok'#'licq'
                else:
                    flagImageBilan = 'flag-yellow'#'flag-green'
                icon = domDocument.createElement('icon')
                bilanBranch.appendChild(icon)
                icon.setAttribute('BUILTIN', flagImageBilan)
        # on termine :
        finalizeDomDocumentFreeplane(
            main, fileName, mapName, domDocument, freeplanemap)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


def exportAllGroups2Freeplane(main, fileName):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'exportAllGroups2Freeplane')
    utils_functions.doWaitCursor()
    try:
        import prof, prof_groupes
        # initialisation du fichier :
        allGroupsText = QtWidgets.QApplication.translate(
            'main', 'Groups')
        mapName = utils_functions.u('{0}\n{1}').format(
            allGroupsText, 
            utils_functions.changeBadChars(main.periodeButton.text()))
        domDocument, freeplanemap, mapcenter = initDomDocumentFreeplane(
            main, mapName, '')
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)

        periode = utils.selectedPeriod

        # on récupère la liste des groupes :
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format(
                'groupes', 'ordre, Matiere, UPPER(Name)'), 
            query=query_my)
        list_groupes = []
        while query_my.next():
            id_groupe = int(query_my.value(0))
            list_groupes.append(id_groupe)

        for id_groupe in list_groupes:
            # on récupère les données liées à la sélection :
            dataSelection = prof.diversFromSelection(
                main, id_groupe=id_groupe, periode=periode)
            groupeName = dataSelection[1]
            matiereName = dataSelection[2]
            groupBranch = addBranchFreeplane(
                main, domDocument, mapcenter, 
                groupeName, label=matiereName,
                color='#0000ff')
            # on récupère tous les tableaux du groupe pour la période considérée
            tableaux = prof_groupes.tableauxFromGroupe(
                main, dataSelection[0], dataSelection[3])
            id_tableaux = utils_functions.array2string(tableaux)
            # ligne de commande pour lister les items :
            commandLine_items = utils_functions.u(
                'SELECT bilans.*, items.* '
                'FROM bilans '
                'JOIN item_bilan ON item_bilan.id_bilan=bilans.id_bilan '
                'JOIN items ON items.id_item=item_bilan.id_item '
                'WHERE bilans.Name=? '
                'ORDER BY items.Name')
            # la base de la ligne de commande pour chercher les évaluations :
            commandLine_evals = utils_functions.u(
                'SELECT value '
                'FROM evaluations '
                'WHERE id_item={0} '
                'AND id_tableau IN ({1}) '
                'AND value!=""')

            columns, rows = prof.calcBilans(
                main, id_groupe=id_groupe, periode=periode)
            for i in range(3, len(columns)):
                titleColumn = columns[i]['value']
                bilanName = utils_functions.code2name(main, titleColumn)
                query_my = utils_db.queryExecute(
                    {commandLine_items: (bilanName, )}, query=query_my)
                bilanFirst = True
                itemsList = []
                total, faits = 0, 0
                while query_my.next():
                    if bilanFirst:
                        id_bilan = int(query_my.value(0))
                        bilanLabel = query_my.value(2)
                        id_competence = int(query_my.value(3))
                        # on peut créer la branche pour le bilan :
                        bilanBranch = addBranchFreeplane(
                            main, domDocument, groupBranch, 
                            bilanName, label=bilanLabel,
                            color='#0000ff')
                        bilanFirst = False
                    total += 1
                    id_item = int(query_my.value(4))
                    itemName = query_my.value(5)
                    itemLabel = query_my.value(6)
                    itemsList.append((id_item, itemName, itemLabel))

                for (id_item, itemName, itemLabel) in itemsList:
                    commandLine_my = commandLine_evals.format(
                        id_item, id_tableaux)
                    flagImageItem = ''
                    query_my = utils_db.queryExecute(
                        commandLine_my, query=query_my)
                    if query_my.first():
                        flagImageItem = 'button_ok'
                        faits += 1
                    # on crée la branche pour l'item :
                    addBranchFreeplane(
                        main, domDocument, bilanBranch, 
                        itemName, label=itemLabel, 
                        flag=flagImageItem)
                # on détermine l'icône du bilan :
                if faits > 0:
                    if total == faits:
                        flagImageBilan = 'button_ok'#'licq'
                    else:
                        flagImageBilan = 'flag-yellow'#'flag-green'
                    icon = domDocument.createElement('icon')
                    bilanBranch.appendChild(icon)
                    icon.setAttribute('BUILTIN', flagImageBilan)
        # on termine :
        finalizeDomDocumentFreeplane(
            main, fileName, mapName, domDocument, freeplanemap)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


def exportEleves2Freeplane(main, fileName, idsEleves, eleves, configExport):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'exportEleves2Freeplane')
    utils_functions.doWaitCursor()
    try:
        import prof
        onlyOne = (len(idsEleves) == 1)
        onlyActualGroup = (configExport[0] == 0)
        if onlyActualGroup:
            data = prof.diversFromSelection(main)
            actualGroupId = data[0]
            actualGroupName = data[1]
            actualGroupMatiere = data[2]
        # initialisation du fichier :
        if onlyOne:
            eleveText = eleves[idsEleves[0]]
            mapName = utils_functions.u('{0}\n{1}').format(
                eleveText, 
                utils_functions.changeBadChars(main.periodeButton.text()))
            if onlyActualGroup:
                mapName = utils_functions.u(
                    '{0}\n{1}').format(mapName, actualGroupName)
            domDocument, freeplanemap, mapcenter = initDomDocumentFreeplane(
                main, mapName, '')
        else:
            elevesText = QtWidgets.QApplication.translate('main', 'Students')
            mapName = utils_functions.u('{0}\n{1}').format(
                elevesText, 
                utils_functions.changeBadChars(main.periodeButton.text()))
            if onlyActualGroup:
                mapName = utils_functions.u(
                    '{0}\n{1}').format(mapName, actualGroupName)
            domDocument, freeplanemap, mapcenter = initDomDocumentFreeplane(
                main, mapName, '')
        # besoin d'un query :
        query_my = utils_db.query(main.db_my)
        periode = utils.selectedPeriod
        for id_eleve in idsEleves:
            eleve = eleves[id_eleve]
            if onlyOne:
                eleveBranch = mapcenter
            else:
                eleveBranch = addBranchFreeplane(
                    main, domDocument, mapcenter, 
                    eleve,
                    color='#0000ff')
            list_id_groupes, groupes = [], {}
            if onlyActualGroup:
                list_id_groupes = [actualGroupId]
                groupes[actualGroupId] = (
                    actualGroupName, actualGroupMatiere, eleveBranch)
            else:
                # on récupère la liste des groupes pour cet élève :
                list_id_groupes, groupes = [], {}
                commandLine_my = (
                    'SELECT groupes.* FROM groupes '
                    'JOIN groupe_eleve ON groupe_eleve.id_groupe=groupes.id_groupe '
                    'WHERE id_eleve={0} '
                    'ORDER BY groupes.ordre').format(id_eleve)
                query_my = utils_db.queryExecute(commandLine_my, query=query_my)
                while query_my.next():
                    id_groupe = int(query_my.value(0))
                    groupName = query_my.value(1)
                    groupMatiere = query_my.value(2)
                    list_id_groupes.append(id_groupe)
                    groupBranch = addBranchFreeplane(
                        main, domDocument, eleveBranch, 
                        groupName, label=groupMatiere,
                        color='#0000ff')
                    groupes[id_groupe] = (groupName, groupMatiere, groupBranch)

            for id_groupe in list_id_groupes:
                columns, rows = prof.calcEleve(
                    main, id_eleve, id_sens=0, id_groupe=id_groupe)
                bilanBranch = None
                for row in rows:
                    if 'bold' in row[4]:
                        id_bilan = row[0]['value']
                        genre = row[4].get('genre', 2)
                        mustDo = False
                        query_my = utils_db.queryExecute(
                            utils_db.q_selectAllFromWhere.format('bilans', 'id_bilan', id_bilan), 
                            query=query_my)
                        if query_my.first():
                            id_competence = int(query_my.value(3))
                        if id_competence > utils.decalageCFD:
                            if configExport[2]:
                                mustDo = True
                        elif id_competence > utils.decalageBLT:
                            if configExport[1]:
                                mustDo = True
                        elif id_competence > -1:
                            if configExport[2]:
                                mustDo = True
                        else:
                            genre = row[4].get('genre', 2)
                            if configExport[3] and genre == 1:
                                mustDo = True
                            if configExport[4] and genre == 2:
                                mustDo = True
                        if mustDo:
                            bilanName = row[4]['value']
                            bilanLabel = row[5]['value']
                            bilanColor = row[6]['color']
                            bilanValue = row[6]['value']
                            bilanTextColor = row[4]['text-color']
                            bilanBranch = addBranchFreeplane(main, 
                                domDocument, groupes[id_groupe][2], 
                                bilanName, label=bilanLabel,
                                color=bilanTextColor.name(), 
                                evalColor=bilanColor.name())
                        else:
                            bilanBranch = None
                    elif row[5]['value'] != 'HSEPARATOR':
                        itemName = row[4]['value']
                        itemLabel = row[5]['value']
                        itemColor = row[6]['color']
                        itemValue = row[6]['value']
                        if bilanBranch != None:
                            itemBranch = addBranchFreeplane(
                                main, 
                                domDocument, bilanBranch, 
                                itemName, label=itemLabel, 
                                evalColor=itemColor.name())
        # on termine :
        finalizeDomDocumentFreeplane(
            main, fileName, mapName, domDocument, freeplanemap)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()


def exportBilan2Freeplane(main, fileName, id_bilan):
    """
    blablabla
    """
    utils_functions.afficheMessage(main, 'exportBilan2Freeplane')
    utils_functions.doWaitCursor()
    try:
        # on récupère le nom et le label du bilan :
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'bilans', 'id_bilan', id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            mapName = utils_functions.u(
                '{0}\n{1}').format(query_my.value(1), query_my.value(2))
        # initialisation du document :
        domDocument, freeplanemap, mapcenter = initDomDocumentFreeplane(
            main, mapName, '')
        # on récupère les items liés au bilan :
        commandLine_my = (
            'SELECT items.* '
            'FROM item_bilan '
            'JOIN items ON items.id_item=item_bilan.id_item '
            'WHERE item_bilan.id_bilan={0} '
            'ORDER BY UPPER(items.Name)').format(id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            itemName = query_my.value(1)
            itemLabel = query_my.value(2)
            position = "right"
            bilanBranch = addBranchFreeplane(
                main, domDocument, mapcenter, 
                itemName, label=itemLabel,
                color="#0000ff", position=position)
        finalizeDomDocumentFreeplane(
            main, fileName, mapName, domDocument, freeplanemap)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()



###########################################################"
#   IMPORTS ABSENCES DEPUIS SIECLE (VIE SCOLAIRE)
###########################################################"

class importAbsencesFromSIECLE_Dlg(QtWidgets.QDialog):
    """
    Pour choisir ce qui sera importé
    (tous les élèves ou le groupe sélectionné).
    """
    def __init__(self, parent=None, fileName='', elevesInGroup=[]):
        super(importAbsencesFromSIECLE_Dlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Import absences from SIECLE'))
        # une variable
        self.fileName = fileName
        self.elevesInGroup = elevesInGroup

        # la période :
        labelText = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Period:'))
        periodeLabel = QtWidgets.QLabel(labelText)
        self.periodesComboBox = QtWidgets.QComboBox()
        self.periodesComboBox.setMinimumWidth(200)

        # tous les élèves ou seulement le groupe actuel :
        labelText = utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Students to import:'))
        studentsLabel = QtWidgets.QLabel(labelText)
        studentsGroupBox = QtWidgets.QGroupBox()
        self.allStudentsRadio = QtWidgets.QRadioButton(QtWidgets.QApplication.translate(
            'main', 'All students'))
        self.allStudentsRadio.setChecked(True)
        self.actualGroupRadio = QtWidgets.QRadioButton(QtWidgets.QApplication.translate(
            'main', 'Students in the selected group'))
        studentsVbox = QtWidgets.QVBoxLayout()
        studentsVbox.addWidget(self.allStudentsRadio)
        studentsVbox.addWidget(self.actualGroupRadio)
        studentsVbox.addSpacing(10)
        studentsGroupBox.setLayout(studentsVbox)

        # absences justifiées :
        shortText = utils_db.readInConfigTable(
            self.main.db_commun, 'absences-00')[1]
        if shortText == '':
            shortText = QtWidgets.QApplication.translate(
                'main', 'Justified absences')
        labelText = utils_functions.u('<p><b>{0} :</b></p>').format(
            shortText)
        nbAbsLabel = QtWidgets.QLabel(labelText)
        nbAbsGroupBox = QtWidgets.QGroupBox()
        self.nbAbsRadio0 = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Do not import'))
        self.nbAbsRadio1 = QtWidgets.QRadioButton('nbAbs')
        self.nbAbsRadio2 = QtWidgets.QRadioButton('nbNonJustif')
        self.nbAbsRadio3 = QtWidgets.QRadioButton('nbRet')
        self.nbAbsRadio1.setChecked(True)
        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(self.nbAbsRadio0)
        hbox.addWidget(self.nbAbsRadio1)
        hbox.addWidget(self.nbAbsRadio2)
        hbox.addWidget(self.nbAbsRadio3)
        nbAbsGroupBox.setLayout(hbox)

        # absences non justifiées :
        shortText = utils_db.readInConfigTable(
            self.main.db_commun, 'absences-10')[1]
        if shortText == '':
            shortText = QtWidgets.QApplication.translate(
                'main', 'Unjustified absences')
        labelText = utils_functions.u('<p><b>{0} :</b></p>').format(
            shortText)
        nbNonJustifLabel = QtWidgets.QLabel(labelText)
        nbNonJustifGroupBox = QtWidgets.QGroupBox()
        self.nbNonJustifRadio0 = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Do not import'))
        self.nbNonJustifRadio1 = QtWidgets.QRadioButton('nbAbs')
        self.nbNonJustifRadio2 = QtWidgets.QRadioButton('nbNonJustif')
        self.nbNonJustifRadio3 = QtWidgets.QRadioButton('nbRet')
        self.nbNonJustifRadio2.setChecked(True)
        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(self.nbNonJustifRadio0)
        hbox.addWidget(self.nbNonJustifRadio1)
        hbox.addWidget(self.nbNonJustifRadio2)
        hbox.addWidget(self.nbNonJustifRadio3)
        nbNonJustifGroupBox.setLayout(hbox)

        # retards :
        shortText = utils_db.readInConfigTable(
            self.main.db_commun, 'absences-10')[1]
        if shortText == '':
            shortText = QtWidgets.QApplication.translate(
                'main', 'Delays')
        labelText = utils_functions.u('<p><b>{0} :</b></p>').format(
            shortText)
        nbRetLabel = QtWidgets.QLabel(labelText)
        nbRetGroupBox = QtWidgets.QGroupBox()
        self.nbRetRadio0 = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Do not import'))
        self.nbRetRadio1 = QtWidgets.QRadioButton('nbAbs')
        self.nbRetRadio2 = QtWidgets.QRadioButton('nbNonJustif')
        self.nbRetRadio3 = QtWidgets.QRadioButton('nbRet')
        self.nbRetRadio3.setChecked(True)
        hbox = QtWidgets.QHBoxLayout()
        hbox.addWidget(self.nbRetRadio0)
        hbox.addWidget(self.nbRetRadio1)
        hbox.addWidget(self.nbRetRadio2)
        hbox.addWidget(self.nbRetRadio3)
        nbRetGroupBox.setLayout(hbox)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(periodeLabel, 1, 0, 1, 2)
        grid.addWidget(self.periodesComboBox, 2, 1, 1, 2)
        grid.addWidget(studentsLabel, 3, 0, 1, 2)
        grid.addWidget(studentsGroupBox, 4, 1, 1, 2)
        grid.addWidget(nbAbsLabel, 10, 0, 1, 2)
        grid.addWidget(nbAbsGroupBox, 11, 1, 1, 2)
        grid.addWidget(nbNonJustifLabel, 20, 0, 1, 2)
        grid.addWidget(nbNonJustifGroupBox, 21, 1, 1, 2)
        grid.addWidget(nbRetLabel, 30, 0, 1, 2)
        grid.addWidget(nbRetGroupBox, 31, 1, 1, 2)
        grid.addWidget(buttonBox, 90, 0, 1, 3)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # remplissage et préselection de la combo période :
        for periode in range(utils.NB_PERIODES):
            self.periodesComboBox.addItem(utils.PERIODES[periode])
        if utils.selectedPeriod < 0:
            self.periodesComboBox.setCurrentIndex(0)
        else:
            self.periodesComboBox.setCurrentIndex(utils.selectedPeriod)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('vs')

    def accept(self):
        periode = self.periodesComboBox.currentIndex()
        onlyActualGroup = self.actualGroupRadio.isChecked()
        OK = False
        try:
            # récupération des enregistrements actuels :
            query_my = utils_db.query(self.main.db_my)
            oldStudents = {}
            commandLine_my = utils_db.qs_prof_absencesPeriode.format(periode)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(2))
                absences = {}
                for i in range(4):
                    absences[i] = query_my.value(3 + i)
                    if absences[i] != '':
                        absences[i] = int(absences[i])
                    else:
                        absences[i] = 0
                oldStudents[id_eleve] = [absences[0], absences[1], absences[2], absences[3]]
            # récupération des données contenues dans le xml :
            students = {}
            f = QtCore.QFile(self.fileName)
            if not(f.open(QtCore.QIODevice.ReadOnly)):
                return
            doc = QtXml.QDomDocument()
            if not(doc.setContent(f)):
                return
            eleves = doc.elementsByTagName('eleve')
            for j in range(eleves.length()):
                eleve = eleves.item(j).toElement()
                if eleve.isNull():
                    continue
                id_eleve = int(eleve.attribute('elenoet'))
                #eleve_libelle = eleve.attribute('libelle')
                #eleve_nomEleve = eleve.attribute('nomEleve')
                #eleve_prenomEleve = eleve.attribute('prenomEleve')
                absences = oldStudents.get(id_eleve, [0, 0, 0, 0])
                if self.nbAbsRadio1.isChecked():
                    absences[0] = int(eleve.attribute('nbAbs'))
                elif self.nbAbsRadio2.isChecked():
                    absences[0] = int(eleve.attribute('nbNonJustif'))
                elif self.nbAbsRadio3.isChecked():
                    absences[0] = int(eleve.attribute('nbRet'))
                if self.nbNonJustifRadio1.isChecked():
                    absences[1] = int(eleve.attribute('nbAbs'))
                elif self.nbNonJustifRadio2.isChecked():
                    absences[1] = int(eleve.attribute('nbNonJustif'))
                elif self.nbNonJustifRadio3.isChecked():
                    absences[1] = int(eleve.attribute('nbRet'))
                if self.nbRetRadio1.isChecked():
                    absences[2] = int(eleve.attribute('nbAbs'))
                elif self.nbRetRadio2.isChecked():
                    absences[2] = int(eleve.attribute('nbNonJustif'))
                elif self.nbRetRadio3.isChecked():
                    absences[2] = int(eleve.attribute('nbRet'))

                if absences[0] + absences[1] + absences[2] + absences[3] > 0:
                    if onlyActualGroup:
                        if id_eleve in self.elevesInGroup:
                            students[id_eleve] = (absences[0], absences[1], absences[2], absences[3])
                    else:
                        students[id_eleve] = (absences[0], absences[1], absences[2], absences[3])
            # vérification du fichier (au cas où il n'y ait rien dedans) :
            if len(students) < 1:
                message = QtWidgets.QApplication.translate(
                    'main', 
                    'The XML file provided does not seem to be the right one.')
                utils_functions.messageBox(self.main, level='warning', message=message)
                return

            if onlyActualGroup:
                for id_eleve in self.elevesInGroup:
                    if not(id_eleve in students):
                        absences = oldStudents.get(id_eleve, [0, 0, 0, 0])
                        if not(self.nbAbsRadio0.isChecked()):
                            absences[0] = 0
                        if not(self.nbNonJustifRadio0.isChecked()):
                            absences[1] = 0
                        if not(self.nbRetRadio0.isChecked()):
                            absences[2] = 0
                        students[id_eleve] = (absences[0], absences[1], absences[2], absences[3])
            else:
                for id_eleve in oldStudents:
                    if not(id_eleve in students):
                        absences = oldStudents.get(id_eleve, [0, 0, 0, 0])
                        if not(self.nbAbsRadio0.isChecked()):
                            absences[0] = 0
                        if not(self.nbNonJustifRadio0.isChecked()):
                            absences[1] = 0
                        if not(self.nbRetRadio0.isChecked()):
                            absences[2] = 0
                        students[id_eleve] = (absences[0], absences[1], absences[2], absences[3])
            # mise à jour de la base :
            lines = []
            if onlyActualGroup:
                id_students = utils_functions.array2string(self.elevesInGroup)
                commandLine_my = utils_db.qdf_prof_absencesPeriodeEleves.format(
                    periode, id_students)
            else:
                commandLine_my = utils_db.qdf_where.format('absences', 'Periode', periode)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            commandLine_my = utils_db.insertInto('absences')
            for id_eleve in students:
                lines.append(
                    (-1, periode, id_eleve, 
                     students[id_eleve][0], 
                     students[id_eleve][1], 
                     students[id_eleve][2], 
                     students[id_eleve][3]))
            query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
            OK = True
        finally:
            f.close()
            if not(OK):
                message = QtWidgets.QApplication.translate(
                    'main', 'Importing the file failed or you have canceled.')
                utils_functions.messageBox(self.main, level='warning', message=message)
            QtWidgets.QDialog.accept(self)



###########################################################"
#   IMPORTS ADMIN DEPUIS SIECLE
###########################################################"

def teachersFromSTSWeb(main, fileName):
    """
    L'import des profs se fait avec le fichier sts_emp_RNE_aaaa.xml
    où RNE est le n° de RNE de l'établissement et aaaa l'année (septembre).
    Par exemple, à VÉRAC pour 2011-2012, le fichier s'appelle sts_emp_0332706M_2011.xml.
    Mais le fichier contient aussi :
        * les classes
        * les matières
    """
    result = (False, {})
    try:
        f = QtCore.QFile(fileName)
        if not(f.open(QtCore.QIODevice.ReadOnly)):
            return
        doc = QtXml.QDomDocument()
        if not(doc.setContent(f)):
            return

        dataInXML = {
            'classes': [], 
            'matieres': [], 
            'profs': {'ENS': {}, 'EDU': {}, 'DIR': {}}, 
            }
        # *******************************************************
        # RÉCUPÉRATION DES CLASSES
        # *******************************************************
        dataInXML['classes']
        classesList = doc.elementsByTagName('DIVISION')
        for i in range(classesList.length()):
            classe = classesList.item(i).toElement()
            if classe.isNull():
                continue
            classe_code = classe.attribute('CODE')
            dataInXML['classes'].append(classe_code)
        # vérification du fichier (au cas où il n'y ait rien dedans) :
        if len(dataInXML['classes']) < 1:
            message = QtWidgets.QApplication.translate(
                'main', 
                'The XML file provided does not seem to be the right one.')
            utils_functions.messageBox(main, level='warning', message=message)
            return

        # *******************************************************
        # RÉCUPÉRATION DES PROFS ET MATIÈRES
        # *******************************************************
        profsList = doc.elementsByTagName('INDIVIDU')
        for i in range(profsList.length()):
            prof = profsList.item(i).toElement()
            if prof.isNull():
                continue
            prof_id = prof.attribute('ID')
            profType = prof.attribute('TYPE')
            for tagName in ['NOM_USAGE', 'PRENOM', 'DATE_NAISSANCE', 'FONCTION', 'CIVILITE']:
                text = readTextElement(prof, tagName)
                if tagName == 'NOM_USAGE':
                    profNom = text
                elif tagName == 'PRENOM':
                    profPrenom = text
                elif tagName == 'DATE_NAISSANCE':
                    profDateNaiss = text.replace('/', '').replace('-', '')
                elif tagName == 'FONCTION':
                    profFonction = text
                elif tagName == 'CIVILITE':
                    profCivilite = text
            # on ne gardera que la dernière matière s'il y en a plusieurs :
            matiereXmlName = ''
            profMatieresList = prof.elementsByTagName('DISCIPLINE')
            for j in range(profMatieresList.length()):
                matiereElement = profMatieresList.item(j).toElement()
                if matiereElement.isNull():
                    continue
                matiereXmlName = readTextElement(matiereElement, 'LIBELLE_COURT')
            if profFonction in dataInXML['profs']:
                dataInXML['profs'][profFonction][int(prof_id)] = {
                    'NOM': profNom, 
                    'Prenom': profPrenom, 
                    'Matiere': matiereXmlName, 
                    'Type': profType, 
                    'DateNaiss': profDateNaiss, 
                    'Civilite': profCivilite, 
                    }
                if not(matiereXmlName in dataInXML['matieres']) and (matiereXmlName != ''):
                    dataInXML['matieres'].append(matiereXmlName)

        # *******************************************************
        # RÉCUPÉRATION DES PROFS ET MATIÈRES LSU
        # *******************************************************
        matieres = {}
        matieresList = doc.elementsByTagName('MATIERE')
        for i in range(matieresList.length()):
            matiere = matieresList.item(i).toElement()
            if matiere.isNull():
                continue
            codeMatiere = matiere.attribute('CODE')
            matiereCodeGestion = readTextElement(matiere, 'CODE_GESTION')
            matieres[codeMatiere] = matiereCodeGestion

        profsMatieres = {}
        servicesList = doc.elementsByTagName('SERVICE')
        for i in range(servicesList.length()):
            service = servicesList.item(i).toElement()
            if service.isNull():
                continue
            codeMatiere = service.attribute('CODE_MATIERE')
            profsList = service.elementsByTagName('ENSEIGNANT')
            for i in range(profsList.length()):
                prof = profsList.item(i).toElement()
                if prof.isNull():
                    continue
                prof_id = prof.attribute('ID')
                profType = prof.attribute('TYPE')
                matiere = (codeMatiere, matieres.get(codeMatiere, ''))
                if not(int(prof_id) in profsMatieres):
                    profsMatieres[int(prof_id)] = [matiere]
                elif not(matiere in profsMatieres[int(prof_id)]):
                    profsMatieres[int(prof_id)].append(matiere)

        result = (True, dataInXML)
    finally:
        f.close()
        return result

def subjectsFromSIECLE(main, fileName):
    """
    import des matières depuis le fichier Nomenclature.xml.
    On lit d'abord la partie MATIERES pour récupérer CODE_MATIERE et CODE_GESTION, 
    puis la partie PROGRAMMES pour récupérer CODE_MODALITE_ELECT.
    """
    result = (False, {})
    try:
        f = QtCore.QFile(fileName)
        if not(f.open(QtCore.QIODevice.ReadOnly)):
            return
        doc = QtXml.QDomDocument()
        if not(doc.setContent(f)):
            return
        # vérification du fichier :
        xmlBEE = doc.elementsByTagName('BEE_NOMENCLATURES')
        if xmlBEE.isEmpty():
            msgFin = False
            message = QtWidgets.QApplication.translate(
                'main', 
                'The XML file provided does not seem to be the right one.')
            utils_functions.messageBox(
                main, level='warning', message=message)
            return
        dataInXML = {}
        # on récupère le code UAJ de l'établissement :
        try:
            parametres = doc.elementsByTagName('PARAMETRES').item(0).toElement()
            xmlUAJ = readTextElement(parametres, 'UAJ')
        except:
            xmlUAJ = ''
        if len(xmlUAJ) > 0:
            dataInXML['xmlUAJ'] = xmlUAJ
        matieresList = doc.elementsByTagName('MATIERE')
        for i in range(matieresList.length()):
            matiere = matieresList.item(i).toElement()
            if matiere.isNull():
                continue
            codeMatiere = matiere.attribute('CODE_MATIERE')
            codeGestion = readTextElement(matiere, 'CODE_GESTION')
            dataInXML[codeMatiere] = [codeGestion, '']
        programmesList = doc.elementsByTagName('PROGRAMME')
        for i in range(programmesList.length()):
            programme = programmesList.item(i).toElement()
            if programme.isNull():
                continue
            codeMatiere = readTextElement(
                programme, 'CODE_MATIERE')
            modaliteElection = readTextElement(
                programme, 'CODE_MODALITE_ELECT')
            if codeMatiere in dataInXML:
                dataInXML[codeMatiere][1] = modaliteElection
        result = (True, dataInXML)
    finally:
        f.close()
        return result

def studentsFromSIECLE(main, fileName):
    """
    mise à jour des élèves à partir du fichier SIECLE
    """
    result = (False, {})
    try:
        f = QtCore.QFile(fileName)
        if not(f.open(QtCore.QIODevice.ReadOnly)):
            return
        doc = QtXml.QDomDocument()
        if not(doc.setContent(f)):
            return
        # vérification du fichier :
        xmlBEE = doc.elementsByTagName('BEE_ELEVES')
        if xmlBEE.isEmpty():
            msgFin = False
            message = QtWidgets.QApplication.translate(
                'main', 
                'The XML file provided does not seem to be the right one.')
            utils_functions.messageBox(
                main, level='warning', message=message)
            return

        elevesInXML = {}
        elevesList = doc.elementsByTagName('ELEVE')
        structuresEleveList = doc.elementsByTagName('STRUCTURES_ELEVE')
        adressesList = doc.elementsByTagName('ADRESSE')
        for i in range(elevesList.length()):
            eleve = elevesList.item(i).toElement()
            if eleve.isNull():
                continue
            eleve_id = eleve.attribute('ELEVE_ID')
            id_eleve = eleve.attribute('ELENOET')
            EleveNom = ''
            for tagName in ['ID_NATIONAL', 'NOM', 'NOM_DE_FAMILLE', 'PRENOM', 'DATE_NAISS', 'CODE_SEXE']:
                text = readTextElement(eleve, tagName)
                if tagName == 'ID_NATIONAL':
                    EleveNum = text
                elif tagName == 'NOM_DE_FAMILLE':
                    if len(text) > 0:
                        EleveNom = text
                elif tagName == 'PRENOM':
                    ElevePrenom = text
                elif tagName == 'DATE_NAISS':
                    EleveDateNaiss = text.replace('/', '').replace('-', '')
                elif tagName == 'CODE_SEXE':
                    EleveSexe = text
            EleveDateEntree = readTextElement(eleve, 'DATE_ENTREE')
            EleveDateSortie = readTextElement(eleve, 'DATE_SORTIE')

            # on cherche si l'élève a une adresse personnelle :
            nomComplet = utils_functions.u('{0} {1}').format(EleveNom, ElevePrenom)
            adresse = ''
            text = readTextElement(eleve, 'ADRESSE_ID')
            if text != '':
                for j in range(adressesList.length()):
                    adresseElement = adressesList.item(j).toElement()
                    if adresseElement.isNull():
                        continue
                    adresse_id = adresseElement.attribute('ADRESSE_ID')
                    if adresse_id == text:
                        l1 = readTextElement(adresseElement, 'LIGNE1_ADRESSE')
                        l2 = readTextElement(adresseElement, 'LIGNE2_ADRESSE')
                        l3 = readTextElement(adresseElement, 'LIGNE3_ADRESSE')
                        l4 = readTextElement(adresseElement, 'LIGNE4_ADRESSE')
                        cp = readTextElement(adresseElement, 'CODE_POSTAL')
                        lp = readTextElement(adresseElement, 'LIBELLE_POSTAL')
                        adresse = utils_functions.u('{0}|{1}|{2}|{3}|{4} {5}').format(
                            l1, l2, l3, l4, cp, lp)
                        if len(adresse) < 10:
                            adresse = ''
                        break

            anDernier = eleve.elementsByTagName('SCOLARITE_AN_DERNIER').item(0).toElement()
            EleveAnDernier = utils_functions.u('{0}-{1}-{2}-{3}').format(
                #readTextElement(anDernier, 'CODE_MEF'), 
                readTextElement(anDernier, 'SIGLE'), 
                readTextElement(anDernier, 'DENOM_COMPL'), 
                readTextElement(anDernier, 'CODE_COMMUNE_INSEE'), 
                readTextElement(anDernier, 'LL_COMMUNE_INSEE'))
            EleveAnDernier = EleveAnDernier.replace('--', '-')
            for i in range(5):
                EleveAnDernier = EleveAnDernier.replace('--', '-')
            if EleveAnDernier == '-':
                EleveAnDernier = ''

            # on cherche la classe de l'élève :
            EleveClasse = ''
            if structuresEleveList.length() < 1:
                # PB Vincent L
                EleveClasse = '???'
            for j in range(structuresEleveList.length()):
                structuresEleve = structuresEleveList.item(j).toElement()
                if structuresEleve.isNull():
                    continue
                eleve_id2 = structuresEleve.attribute('ELEVE_ID')
                if eleve_id2 == eleve_id:
                    structures = structuresEleve.elementsByTagName('STRUCTURE')
                    for k in range(structures.length()):
                        structure = structures.item(k).toElement()
                        codeStructure = readTextElement(structure, 'CODE_STRUCTURE')
                        typeStructure = readTextElement(structure, 'TYPE_STRUCTURE')
                        if typeStructure == 'D':
                            EleveClasse = codeStructure
                    # pas besoin de continuer de chercher la classe :
                    break

            elevesInXML[int(id_eleve)] = {
                'eleve_id': int(eleve_id), 
                'num': EleveNum, 
                'NOM': EleveNom, 
                'Prenom': ElevePrenom, 
                'Date_naiss': EleveDateNaiss, 
                'nomComplet': nomComplet, 
                'adresse': adresse, 
                'AnDernier': EleveAnDernier, 
                'Classe': EleveClasse, 
                'DATE_ENTREE': EleveDateEntree, 
                'DATE_SORTIE': EleveDateSortie, 
                'sexe': EleveSexe, 
                }

        result = (True, elevesInXML)
    finally:
        f.close()
        return result


def addressesFromSIECLE(main, fileName):
    """
    """
    result = (False, {}, {}, {})
    try:
        f = QtCore.QFile(fileName)
        if not(f.open(QtCore.QIODevice.ReadOnly)):
            return
        doc = QtXml.QDomDocument()
        if not(doc.setContent(f)):
            return
        # vérification du fichier :
        xmlBEE = doc.elementsByTagName('BEE_RESPONSABLES')
        if xmlBEE.isEmpty():
            msgFin = False
            message = QtWidgets.QApplication.translate(
                'main', 
                'The XML file provided does not seem to be the right one.')
            utils_functions.messageBox(main, level='warning', message=message)
            return
        # on y va :
        responsables = {}
        xmlList = doc.elementsByTagName('RESPONSABLE_ELEVE')
        for i in range(xmlList.length()):
            xmlItem = xmlList.item(i).toElement()
            if xmlItem.isNull():
                continue
            text = readTextElement(xmlItem, 'ELEVE_ID')
            eleve_id = int(text)
            text = readTextElement(xmlItem, 'PERSONNE_ID')
            personne_id = int(text)
            text = readTextElement(xmlItem, 'NIVEAU_RESPONSABILITE')
            legal = int(text)
            """
            text = readTextElement(xmlItem, 'RESP_FINANCIER')
            respFinancier = (text == '1')
            """
            text = readTextElement(xmlItem, 'HEBERGE_ELEVE')
            hebergeEleve = (text == 'true')
            #print('NIVEAU_RESPONSABILITE', legal)
            """
            NIVEAU_RESPONSABILITE
            1(LEGAL), 2(PERSONNE EN CHARGE), 3(SIMPLE CONTACT)
            """
            if not(eleve_id in responsables):
                responsables[eleve_id] = {0: -1, 1: -1, 2: -1}
            """
            if respFinancier:
                responsables[eleve_id][0] = personne_id
            """
            if hebergeEleve:
                responsables[eleve_id][0] = personne_id
            elif legal < 3:
                if responsables[eleve_id][1] == -1:
                    responsables[eleve_id][1] = personne_id
                else:
                    responsables[eleve_id][2] = personne_id
        personnes = {}
        xmlList = doc.elementsByTagName('PERSONNE')
        for i in range(xmlList.length()):
            xmlItem = xmlList.item(i).toElement()
            if xmlItem.isNull():
                continue
            personne_id = int(xmlItem.attribute('PERSONNE_ID'))
            civilite = readTextElement(xmlItem, 'LC_CIVILITE')
            if civilite == 'MME':
                civilite = 'Mme'
            nom = readTextElement(xmlItem, 'NOM_USAGE')
            if len(nom) < 1:
                nom = readTextElement(xmlItem, 'NOM_DE_FAMILLE')
            prenom = readTextElement(xmlItem, 'PRENOM')
            nomComplet = utils_functions.u('{0} {1} {2}').format(civilite, nom, prenom)
            text = readTextElement(xmlItem, 'ADRESSE_ID')
            adresse_id = int(text)
            personnes[personne_id] = (nomComplet, adresse_id)
        adresses = {}
        xmlList = doc.elementsByTagName('ADRESSE')
        for i in range(xmlList.length()):
            xmlItem = xmlList.item(i).toElement()
            if xmlItem.isNull():
                continue
            adresse_id = int(xmlItem.attribute('ADRESSE_ID'))
            l1 = readTextElement(xmlItem, 'LIGNE1_ADRESSE')
            l2 = readTextElement(xmlItem, 'LIGNE2_ADRESSE')
            l3 = readTextElement(xmlItem, 'LIGNE3_ADRESSE')
            l4 = readTextElement(xmlItem, 'LIGNE4_ADRESSE')
            cp = readTextElement(xmlItem, 'CODE_POSTAL')
            lp = readTextElement(xmlItem, 'LIBELLE_POSTAL')
            adresse = utils_functions.u('{0}|{1}|{2}|{3}|{4} {5}').format(
                l1, l2, l3, l4, cp, lp)
            if len(adresse) < 10:
                adresse = ''
            adresses[adresse_id] = adresse
        result = (True, responsables, personnes, adresses)
    finally:
        f.close()
        return result


def phonesAndMailsFromSIECLE(main, fileName):
    """
    """
    result = (False, {}, {})
    try:
        f = QtCore.QFile(fileName)
        if not(f.open(QtCore.QIODevice.ReadOnly)):
            return
        doc = QtXml.QDomDocument()
        if not(doc.setContent(f)):
            return
        # vérification du fichier :
        xmlBEE = doc.elementsByTagName('BEE_RESPONSABLES')
        if xmlBEE.isEmpty():
            msgFin = False
            message = QtWidgets.QApplication.translate(
                'main', 
                'The XML file provided does not seem to be the right one.')
            utils_functions.messageBox(main, level='warning', message=message)
            return
        # on y va :
        responsables = {}
        xmlList = doc.elementsByTagName('RESPONSABLE_ELEVE')
        for i in range(xmlList.length()):
            xmlItem = xmlList.item(i).toElement()
            if xmlItem.isNull():
                continue
            text = readTextElement(xmlItem, 'ELEVE_ID')
            eleve_id = int(text)
            text = readTextElement(xmlItem, 'PERSONNE_ID')
            personne_id = int(text)
            text = readTextElement(xmlItem, 'NIVEAU_RESPONSABILITE')
            legal = int(text)
            text = readTextElement(xmlItem, 'RESP_FINANCIER')
            respFinancier = (text == '1')
            text = readTextElement(xmlItem, 'HEBERGE_ELEVE')
            hebergeEleve = (text == 'true')
            #print('NIVEAU_RESPONSABILITE', legal)
            """
            NIVEAU_RESPONSABILITE
            1(LEGAL), 2(PERSONNE EN CHARGE), 3(SIMPLE CONTACT)
            """
            if not(eleve_id in responsables):
                responsables[eleve_id] = {0: -1, 1: -1, 2: -1}
            if respFinancier:
                responsables[eleve_id][0] = personne_id
                """
                if not(hebergeEleve):
                    print(eleve_id, personne_id)
                """
            elif legal < 3:
                if responsables[eleve_id][1] == -1:
                    responsables[eleve_id][1] = personne_id
                else:
                    responsables[eleve_id][2] = personne_id
        personnes = {}
        xmlList = doc.elementsByTagName('PERSONNE')
        for i in range(xmlList.length()):
            xmlItem = xmlList.item(i).toElement()
            if xmlItem.isNull():
                continue
            personne_id = int(xmlItem.attribute('PERSONNE_ID'))
            civilite = readTextElement(xmlItem, 'LC_CIVILITE')
            if civilite == 'MME':
                civilite = 'Mme'
            nom = readTextElement(xmlItem, 'NOM_DE_FAMILLE')
            prenom = readTextElement(xmlItem, 'PRENOM')
            nomComplet = utils_functions.u('{0} {1} {2}').format(civilite, nom, prenom)
            telPerso = readTextElement(xmlItem, 'TEL_PERSONNEL').replace('+33', '0')
            telPortable = readTextElement(xmlItem, 'TEL_PORTABLE').replace('+33', '0')
            telPro = readTextElement(xmlItem, 'TEL_PROFESSIONNEL').replace('+33', '0')
            mail = readTextElement(xmlItem, 'MEL')
            personnes[personne_id] = (nomComplet, telPerso, telPortable, telPro, mail)
        result = (True, responsables, personnes)
    finally:
        f.close()
        return result






###########################################################"
#   EXPORT ADMIN VERS LSU
###########################################################"

def export2LSU(main, fileName, data={}):
    """
    création et mise en forme du fichier xml d'export vers LSU
    """
    result = False
    try:
        doc = QtXml.QDomDocument()
        instr = doc.createProcessingInstruction("xml", "version='1.0' encoding='UTF-8'")
        doc.appendChild(instr)

        lsunBilans = doc.createElement('lsun-bilans')
        doc.appendChild(lsunBilans)
        lsunBilans.setAttribute('xmlns', 'urn:fr:edu:scolarite:lsun:bilans:import')
        lsunBilans.setAttribute('xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance')
        lsunBilans.setAttribute('xsi:schemaLocation', 'urn:fr:edu:scolarite:lsun:bilans:import import-bilan-complet.xsd')
        lsunBilans.setAttribute('schemaVersion', '3.0')

        # entete :
        entete = doc.createElement('entete')
        lsunBilans.appendChild(entete)
        for what in data['entete']['ORDER']:
            element = doc.createElement(what)
            entete.appendChild(element)
            txt = doc.createTextNode(data['entete'][what])
            element.appendChild(txt)

        donnees = doc.createElement('donnees')
        lsunBilans.appendChild(donnees)

        # responsables-etab :
        responsablesEtab = doc.createElement('responsables-etab')
        donnees.appendChild(responsablesEtab)
        for id_prof in data['responsablesEtab']:
            element = doc.createElement('responsable-etab')
            responsablesEtab.appendChild(element)
            elementData = data['responsablesEtab'][id_prof]
            for what in elementData:
                element.setAttribute(what, elementData[what])

        # eleves :
        eleves = doc.createElement('eleves')
        donnees.appendChild(eleves)
        for id_eleve in data['eleves']:
            element = doc.createElement('eleve')
            eleves.appendChild(element)
            elementData = data['eleves'][id_eleve]
            for what in elementData:
                element.setAttribute(what, elementData[what])

        # periodes :
        if len(data['periodes']) > 0:
            periodes = doc.createElement('periodes')
            donnees.appendChild(periodes)
            for periode in data['periodes']:
                element = doc.createElement('periode')
                periodes.appendChild(element)
                elementData = data['periodes'][periode]
                for what in elementData:
                    element.setAttribute(what, elementData[what])

        # disciplines :
        if len(data['disciplines']) > 0:
            disciplines = doc.createElement('disciplines')
            donnees.appendChild(disciplines)
            for id_matiere in data['disciplines']:
                element = doc.createElement('discipline')
                disciplines.appendChild(element)
                elementData = data['disciplines'][id_matiere]
                for what in elementData:
                    element.setAttribute(what, elementData[what])

        # enseignants :
        if len(data['enseignants']) > 0:
            enseignants = doc.createElement('enseignants')
            donnees.appendChild(enseignants)
            for id_prof in data['enseignants']:
                element = doc.createElement('enseignant')
                enseignants.appendChild(element)
                elementData = data['enseignants'][id_prof]
                for what in elementData:
                    element.setAttribute(what, elementData[what])

        # elements-programme :
        if len(data['elementsProgramme']) > 0:
            elementsProgramme = doc.createElement('elements-programme')
            donnees.appendChild(elementsProgramme)
            for id_elem in data['elementsProgramme']:
                element = doc.createElement('element-programme')
                elementsProgramme.appendChild(element)
                elementData = data['elementsProgramme'][id_elem]
                for what in elementData:
                    element.setAttribute(what, elementData[what])

        # ICI cptNum
        # competences-numeriques-communs :
        if len(data['cptNum']['COMMUN']) > 0:
            cptNumCommuns = doc.createElement('competences-numeriques-communs')
            donnees.appendChild(cptNumCommuns)
            for id_elem in data['cptNum']['COMMUN']:
                element = doc.createElement('competences-numeriques-commun')
                cptNumCommuns.appendChild(element)
                elementData = data['cptNum']['COMMUN'][id_elem]
                for what in elementData:
                    if what != 'appreciation':
                        element.setAttribute(what, elementData[what])
                appreciation = elementData.get('appreciation', '')
                if len(appreciation) > 0:
                    txt = doc.createTextNode(appreciation)
                    element.appendChild(txt)

        # parcours-communs :
        if len(data['parcoursCommuns']) > 0:
            parcoursCommuns = doc.createElement('parcours-communs')
            donnees.appendChild(parcoursCommuns)
            for (groupeName, periode) in data['parcoursCommuns']:
                parcoursCommun = doc.createElement('parcours-commun')
                parcoursCommuns.appendChild(parcoursCommun)
                elementData = data['parcoursCommuns'][(groupeName, periode)]
                for what in elementData['attributs']:
                    parcoursCommun.setAttribute(what, elementData['attributs'][what])
                for parcoursType in elementData['parcours']:
                    appreciation = elementData['parcours'][parcoursType]
                    parcours = doc.createElement('parcours')
                    parcoursCommun.appendChild(parcours)
                    parcours.setAttribute('code', parcoursType)
                    txt = doc.createTextNode(appreciation)
                    parcours.appendChild(txt)

        # vies-scolaires-communs :
        if len(data['vsCommuns']) > 0:
            vsCommuns = doc.createElement('vies-scolaires-communs')
            donnees.appendChild(vsCommuns)
            for (groupeName, periode) in data['vsCommuns']:
                element = doc.createElement('vie-scolaire-commun')
                vsCommuns.appendChild(element)
                elementData = data['vsCommuns'][(groupeName, periode)]
                for what in elementData:
                    if what != 'commentaire':
                        element.setAttribute(what, elementData[what])
                commentaireVS = elementData.get('commentaire', '')
                if len(commentaireVS) > 0:
                    commentaire = doc.createElement('commentaire')
                    element.appendChild(commentaire)
                    txt = doc.createTextNode(commentaireVS)
                    commentaire.appendChild(txt)

        # epis :
        if len(data['EPIS']['episRef']) > 0:
            epis = doc.createElement('epis')
            donnees.appendChild(epis)
            for id_epi in data['EPIS']['episRef']:
                element = doc.createElement('epi')
                epis.appendChild(element)
                elementData = data['EPIS']['episRef'][id_epi]
                for what in elementData:
                    if what != 'description':
                        element.setAttribute(what, elementData[what])
                descriptionEpi = elementData.get('description', '')
                if len(descriptionEpi) > 0:
                    description = doc.createElement('description')
                    element.appendChild(description)
                    txt = doc.createTextNode(descriptionEpi)
                    description.appendChild(txt)
        if len(data['EPIS']['episGroupes']) > 0:
            episGroupes = doc.createElement('epis-groupes')
            donnees.appendChild(episGroupes)
            for epi in data['EPIS']['episGroupes']:
                element = doc.createElement('epi-groupe')
                episGroupes.appendChild(element)
                elementData = data['EPIS']['episGroupes'][epi]
                for what in elementData:
                    if not (what in ('commentaire', 'enseignants-disciplines')):
                        element.setAttribute(what, elementData[what])
                commentaireEpiGroupe = elementData.get('commentaire', '')
                if len(commentaireEpiGroupe) > 0:
                    commentaire = doc.createElement('commentaire')
                    element.appendChild(commentaire)
                    txt = doc.createTextNode(commentaireEpiGroupe)
                    commentaire.appendChild(txt)
                if len(elementData['enseignants-disciplines']) > 0:
                    enseignantsDisciplines = doc.createElement('enseignants-disciplines')
                    element.appendChild(enseignantsDisciplines)
                    for (enseignant, discipline) in elementData['enseignants-disciplines']:
                        enseignantDiscipline = doc.createElement('enseignant-discipline')
                        enseignantsDisciplines.appendChild(enseignantDiscipline)
                        enseignantDiscipline.setAttribute('enseignant-ref', enseignant)
                        enseignantDiscipline.setAttribute('discipline-ref', discipline)

        # acc-persos :
        if len(data['APS']['apsRef']) > 0:
            aps = doc.createElement('acc-persos')
            donnees.appendChild(aps)
            for id_ap in data['APS']['apsRef']:
                element = doc.createElement('acc-perso')
                aps.appendChild(element)
                elementData = data['APS']['apsRef'][id_ap]
                for what in elementData:
                    if what != 'description':
                        element.setAttribute(what, elementData[what])
                descriptionAp = elementData.get('description', '')
                if len(descriptionAp) > 0:
                    description = doc.createElement('description')
                    element.appendChild(description)
                    txt = doc.createTextNode(descriptionAp)
                    description.appendChild(txt)
        if len(data['APS']['apsGroupes']) > 0:
            apsGroupes = doc.createElement('acc-persos-groupes')
            donnees.appendChild(apsGroupes)
            for ap in data['APS']['apsGroupes']:
                element = doc.createElement('acc-perso-groupe')
                apsGroupes.appendChild(element)
                elementData = data['APS']['apsGroupes'][ap]
                for what in elementData:
                    if not (what in ('commentaire', 'enseignants-disciplines')):
                        element.setAttribute(what, elementData[what])
                commentaireApGroupe = elementData.get('commentaire', '')
                if len(commentaireApGroupe) > 0:
                    commentaire = doc.createElement('commentaire')
                    element.appendChild(commentaire)
                    txt = doc.createTextNode(commentaireApGroupe)
                    commentaire.appendChild(txt)
                if len(elementData['enseignants-disciplines']) > 0:
                    enseignantsDisciplines = doc.createElement('enseignants-disciplines')
                    element.appendChild(enseignantsDisciplines)
                    for (enseignant, discipline) in elementData['enseignants-disciplines']:
                        enseignantDiscipline = doc.createElement('enseignant-discipline')
                        enseignantsDisciplines.appendChild(enseignantDiscipline)
                        enseignantDiscipline.setAttribute('enseignant-ref', enseignant)
                        enseignantDiscipline.setAttribute('discipline-ref', discipline)

        # bilans-periodiques :
        if len(data['bilans']) > 0:
            bilans = doc.createElement('bilans-periodiques')
            donnees.appendChild(bilans)
            for (periode, id_eleve) in data['bilans']:
                listeAcquis = data['bilans'][(periode, id_eleve)]['OTHERS']
                if len(listeAcquis) > 0:
                    bilan = doc.createElement('bilan-periodique')
                    bilans.appendChild(bilan)
                    elementData = data['bilans'][(periode, id_eleve)]['attributs']
                    for what in elementData:
                        bilan.setAttribute(what, elementData[what])

                    listeAcquis = data['bilans'][(periode, id_eleve)]['OTHERS']
                    listeAcquisElement = doc.createElement('liste-acquis')
                    bilan.appendChild(listeAcquisElement)
                    for id_matiere in listeAcquis:
                        acquis = doc.createElement('acquis')
                        listeAcquisElement.appendChild(acquis)
                        elementData = listeAcquis[id_matiere]['attributs']
                        for what in elementData:
                            acquis.setAttribute(what, elementData[what])
                        appreciation = doc.createElement('appreciation')
                        acquis.appendChild(appreciation)
                        txt = doc.createTextNode(listeAcquis[id_matiere]['appreciation'])
                        appreciation.appendChild(txt)

                    # ICI cptNum
                    # competences-numeriques :
                    cptNum = data['cptNum']['STUDENTS'].get(id_eleve, {})
                    if len(cptNum) > 0:
                        cptNumElement = doc.createElement('competences-numeriques')
                        bilan.appendChild(cptNumElement)
                        # on doit placer l'appréciation en premier :
                        if 'appreciation' in cptNum:
                            appreciationElement = doc.createElement('appreciation-num')
                            cptNumElement.appendChild(appreciationElement)
                            txt = doc.createTextNode(cptNum['appreciation'])
                            appreciationElement.appendChild(txt)
                        for what in cptNum:
                            if what == 'comp-num-commun-refs':
                                cptNumElement.setAttribute(what, cptNum[what])
                            elif what != 'appreciation':
                                element = doc.createElement('competence-num')
                                cptNumElement.appendChild(element)
                                element.setAttribute('code', what)
                                element.setAttribute('niveau', cptNum[what])

                    epis = data['bilans'][(periode, id_eleve)].get('EPIS', {})
                    if len(epis) > 0:
                        episEleve = doc.createElement('epis-eleve')
                        bilan.appendChild(episEleve)
                        for id_epiGroupe in epis:
                            element = doc.createElement('epi-eleve')
                            episEleve.appendChild(element)
                            elementData = epis[id_epiGroupe]['attributs']
                            for what in elementData:
                                element.setAttribute(what, elementData[what])
                            commentaireEpi = epis[id_epiGroupe].get('commentaire', '')
                            if len(commentaireEpi) > 0:
                                commentaire = doc.createElement('commentaire')
                                element.appendChild(commentaire)
                                txt = doc.createTextNode(commentaireEpi)
                                commentaire.appendChild(txt)

                    aps = data['bilans'][(periode, id_eleve)].get('APS', {})
                    if len(aps) > 0:
                        apsEleve = doc.createElement('acc-persos-eleve')
                        bilan.appendChild(apsEleve)
                        for id_apGroupe in aps:
                            element = doc.createElement('acc-perso-eleve')
                            apsEleve.appendChild(element)
                            elementData = aps[id_apGroupe]['attributs']
                            for what in elementData:
                                element.setAttribute(what, elementData[what])
                            commentaireAp = aps[id_apGroupe].get('commentaire', '')
                            if len(commentaireAp) > 0:
                                commentaire = doc.createElement('commentaire')
                                element.appendChild(commentaire)
                                txt = doc.createTextNode(commentaireAp)
                                commentaire.appendChild(txt)

                    listeParcours = data['bilans'][(periode, id_eleve)].get('parcours', {})
                    if len(listeParcours) > 0:
                        parcoursElement = doc.createElement('liste-parcours')
                        bilan.appendChild(parcoursElement)
                        for parcoursType in listeParcours:
                            appreciation = listeParcours[parcoursType]
                            parcours = doc.createElement('parcours')
                            parcoursElement.appendChild(parcours)
                            parcours.setAttribute('code', parcoursType)
                            txt = doc.createTextNode(appreciation)
                            parcours.appendChild(txt)

                    accompagnements = data['bilans'][(periode, id_eleve)].get('accompagnements', {})
                    if len(accompagnements) > 0:
                        accompagnementsElement = doc.createElement('modalites-accompagnement')
                        bilan.appendChild(accompagnementsElement)
                        for accompagnementType in accompagnements:
                            accompagnement = doc.createElement('modalite-accompagnement')
                            accompagnementsElement.appendChild(accompagnement)
                            accompagnement.setAttribute('code', accompagnementType)
                            if accompagnementType == 'PPRE':
                                commentaire = accompagnements[accompagnementType]
                                if len(commentaire) > 0:
                                    commentairePPRE = doc.createElement('complement-ppre')
                                    accompagnement.appendChild(commentairePPRE)
                                    txt = doc.createTextNode(commentaire)
                                    commentairePPRE.appendChild(txt)

                    acquisConseils = doc.createElement('acquis-conseils')
                    bilan.appendChild(acquisConseils)
                    txt = doc.createTextNode(data['bilans'][(periode, id_eleve)]['PP'])
                    acquisConseils.appendChild(txt)

                    devoirsFaitsText = data['bilans'][(periode, id_eleve)].get('devoirs-faits', '')
                    if len(devoirsFaitsText) > 0:
                        devoirsFaits = doc.createElement('devoirs-faits')
                        bilan.appendChild(devoirsFaits)
                        txt = doc.createTextNode(devoirsFaitsText)
                        devoirsFaits.appendChild(txt)

                    vieScolaire = doc.createElement('vie-scolaire')
                    bilan.appendChild(vieScolaire)
                    if 'attributs' in data['bilans'][(periode, id_eleve)]['VS']:
                        elementData = data['bilans'][(periode, id_eleve)]['VS']['attributs']
                        for what in elementData:
                            vieScolaire.setAttribute(what, elementData[what])
                    if 'commentaire' in data['bilans'][(periode, id_eleve)]['VS']:
                        commentaire = doc.createElement('commentaire')
                        vieScolaire.appendChild(commentaire)
                        txt = doc.createTextNode(data['bilans'][(periode, id_eleve)]['VS']['commentaire'])
                        commentaire.appendChild(txt)

                    if len(data['bilans'][(periode, id_eleve)]['SOCLE']) > 0:
                        socle = doc.createElement('socle')
                        bilan.appendChild(socle)
                        for code in data['bilans'][(periode, id_eleve)]['SOCLE']:
                            domaine = doc.createElement('domaine')
                            socle.appendChild(domaine)
                            domaine.setAttribute('code', code)
                            domaine.setAttribute('positionnement', data['bilans'][(periode, id_eleve)]['SOCLE'][code])

                    if id_eleve in data['responsables']:
                        responsables = doc.createElement('responsables')
                        bilan.appendChild(responsables)
                        if 'LEGAL1' in data['responsables'][id_eleve]:
                            responsable = doc.createElement('responsable')
                            responsables.appendChild(responsable)
                            elementData = data['responsables'][id_eleve]['LEGAL1']
                            for what in elementData:
                                if what != 'adresse':
                                    responsable.setAttribute(what, elementData[what])
                            responsable.setAttribute('legal1', 'true')
                            responsable.setAttribute('legal2', 'false')
                            adresse = doc.createElement('adresse')
                            responsable.appendChild(adresse)
                            elementData = data['responsables'][id_eleve]['LEGAL1']['adresse']
                            for what in elementData:
                                adresse.setAttribute(what, elementData[what])
                        if 'LEGAL2' in data['responsables'][id_eleve]:
                            responsable = doc.createElement('responsable')
                            responsables.appendChild(responsable)
                            elementData = data['responsables'][id_eleve]['LEGAL2']
                            for what in elementData:
                                if what != 'adresse':
                                    responsable.setAttribute(what, elementData[what])
                            responsable.setAttribute('legal1', 'false')
                            responsable.setAttribute('legal2', 'true')
                            adresse = doc.createElement('adresse')
                            responsable.appendChild(adresse)
                            elementData = data['responsables'][id_eleve]['LEGAL2']['adresse']
                            for what in elementData:
                                adresse.setAttribute(what, elementData[what])

        # bilans-cycle :
        if len(data['cycle']) > 0:
            bilans = doc.createElement('bilans-cycle')
            donnees.appendChild(bilans)
            for id_eleve in data['cycle']:
                bilan = doc.createElement('bilan-cycle')
                bilans.appendChild(bilan)
                elementData = data['cycle'][id_eleve]['attributs']
                for what in elementData:
                    bilan.setAttribute(what, elementData[what])

                socle = doc.createElement('socle')
                bilan.appendChild(socle)
                for code in data['cycle'][id_eleve]['SOCLE']:
                    domaine = doc.createElement('domaine')
                    socle.appendChild(domaine)
                    domaine.setAttribute('code', code)
                    domaine.setAttribute('positionnement', data['cycle'][id_eleve]['SOCLE'][code])

                synthese = doc.createElement('synthese')
                bilan.appendChild(synthese)
                txt = doc.createTextNode(data['cycle'][id_eleve]['PP'])
                synthese.appendChild(txt)

                (code, positionnement) = data['cycle'][id_eleve]['ENS_COMP']
                if len(code) > 0:
                    enseignementComplement = doc.createElement('enseignement-complement')
                    bilan.appendChild(enseignementComplement)
                    enseignementComplement.setAttribute('code', code)
                    if positionnement > 0:
                        enseignementComplement.setAttribute('positionnement', positionnement)

                if id_eleve in data['responsables']:
                    responsables = doc.createElement('responsables')
                    bilan.appendChild(responsables)
                    if 'LEGAL1' in data['responsables'][id_eleve]:
                        responsable = doc.createElement('responsable')
                        responsables.appendChild(responsable)
                        elementData = data['responsables'][id_eleve]['LEGAL1']
                        for what in elementData:
                            if what != 'adresse':
                                responsable.setAttribute(what, elementData[what])
                        responsable.setAttribute('legal1', 'true')
                        responsable.setAttribute('legal2', 'false')
                        adresse = doc.createElement('adresse')
                        responsable.appendChild(adresse)
                        elementData = data['responsables'][id_eleve]['LEGAL1']['adresse']
                        for what in elementData:
                            adresse.setAttribute(what, elementData[what])
                    if 'LEGAL2' in data['responsables'][id_eleve]:
                        responsable = doc.createElement('responsable')
                        responsables.appendChild(responsable)
                        elementData = data['responsables'][id_eleve]['LEGAL2']
                        for what in elementData:
                            if what != 'adresse':
                                responsable.setAttribute(what, elementData[what])
                        responsable.setAttribute('legal1', 'false')
                        responsable.setAttribute('legal2', 'true')
                        adresse = doc.createElement('adresse')
                        responsable.appendChild(adresse)
                        elementData = data['responsables'][id_eleve]['LEGAL2']['adresse']
                        for what in elementData:
                            adresse.setAttribute(what, elementData[what])

        outFile = QtCore.QFile(fileName)
        if not outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            message = QtWidgets.QApplication.translate(
                'main', 'Cannot write file {0}:\n{1}.')
            message = utils_functions.u(message).format(
                fileName, outFile.errorString())
            utils_functions.messageBox(
                self, level='warning', message=message)
            return
        indentSize = 4
        doc.save(QtCore.QTextStream(outFile), indentSize)
        result = True
    finally:
        return result














