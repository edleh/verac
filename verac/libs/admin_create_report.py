# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
   CRÉATION DES BILANS ET BULLETINS
   (au format html ; on utilise wkhtmltopdf)
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_filesdirs
import utils_markdown, utils_2lists
import admin, admin_docs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui




###########################################################"
#   CONSTANTES ET FONCTIONS UTILES
###########################################################"

def nl2br(text):
    """
    pour insérer des sauts de lignes (<br/>) dans les textes html
    """
    try:
        newText = text.replace('\n', '<br/>')
    except:
        newText = text
        pass
    return newText


STEP_TEXT = QtWidgets.QApplication.translate('main', 'Step')
CLASS_PREFIX = QtWidgets.QApplication.translate('main', 'class')

WHAT_LIST = {
    #'ORDER': ('', 'referentialReports', 'annualReports', 'DNB', ), 
    'ORDER': ('', 'referentialReports', 'annualReports', ), 
    '': QtWidgets.QApplication.translate('main', 'School Reports'), 
    'referentialReports': QtWidgets.QApplication.translate('main', 'Referential Reports'), 
    'annualReports': QtWidgets.QApplication.translate('main', 'Annual Reports'), 
    'DNB': QtWidgets.QApplication.translate('main', 'DNB Reports (and Notanet file)'), 
    }



def doCreateReports(main):
    """
    lance le dialog de création de bulletins.
    """

    def testWkhtmltopdf(main):
        if utils.WKHTMLTOPDF != None:
            return
        testPdf = utils_db.readInConfigTable(main.db_localConfig, 'testPdf')[0]
        utils.changeWkhtmltopdf(testPdf > 0)
        if not(utils.WKHTMLTOPDF):
            link = utils.HELPPAGE_BEGIN + 'help-admin-wkhtmltopdf.html'
            m1 = QtWidgets.QApplication.translate(
                'main', 'WKHTMLTOPDF IS NOT INSTALLED.')
            m2 = QtWidgets.QApplication.translate(
                'main', 
                "It's a tool that improves the quality of pdf files produced.")
            m3 = QtWidgets.QApplication.translate(
                'main', 'Help page explaining how to install it:')
            m4 = QtWidgets.QApplication.translate(
                'main', 'Install wkhtmltopdf.')
            message = utils_functions.u(
                '<p align="center">{0}</p>'
                '<p align="center"><b>{1}</b></p>'
                '<p align="center">{2}</p>'
                '<p align="center">{3}<br/><a href="{4}">{5}</a></p>'
                '<p></p>').format(utils.SEPARATOR_LINE, m1, m2, m3, link, m4)
            utils_functions.messageBox(main, message=message)

    testWkhtmltopdf(main)

    dialog = CreateReportsWizard(parent=main)
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
    else:
        main.enableInterface(dialog, lastState)
    main.editLog2 = None


class CreateReportsWizard(QtWidgets.QDialog):
    """
    le Wizard de création de bulletins.
    On crée tous les widgets nécessaires mais ils seront visibles selon la page.
    Pages :
        0 : type de relevé, modèle, période et élèves
        1 : options des fichiers à créer
        2 : création des fichiers
    """
    def __init__(self, parent=None):
        super(CreateReportsWizard, self).__init__(parent)
        self.main = parent
        # page actuelle et navigation :
        self.page = {'back': -1, 'actual': 0, 'next': 1, 'final': 2}
        # titre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Create bulletins or other reports'))
        self.helpContextPage = 'create-bilans-blt'

        # les données de création :
        self.data = {
            'what': '', 
            'sorted': False, 
            'template': '', 
            'period': utils.selectedPeriod, 

            'details': False, 
            'withDetails': False, 
            'notanet': {'ELEVES': []}, 

            'idsEleves': [], 
            'classPrefix': CLASS_PREFIX, 
            'withAddresses': True, 
            'addressesLines': 10, 
            'onlyClassFile': False, 
            'classColumn': True, 

            'dateNow1': '', 
            'dateNow2': '', 
            'dateNow3': '', 
            'baseNomFichier': '', 
            'labelDocument': '', 
            'mustUpdateAndMove': True, 

            'SCHOOL_DATA': {}, 
            }

        # affichage des pages :
        self.pagesWidget = QtWidgets.QStackedWidget()
        self.pagesWidget.addWidget(WhatPage(self))
        self.pagesWidget.addWidget(ConfigFilesPage(self))
        self.pagesWidget.addWidget(CreateFilesPage(self))

        # des boutons :
        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('back', 'next', 'close', 'help'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]

        # on agence tout ça :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.pagesWidget)
        layout.addLayout(buttonsLayout)
        self.setLayout(layout)

        # on appelle la première page :
        self.reInit()
        self.loadPage()

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, 'admin')

    def reInit(self):
        """
        initialisation de quelques valeurs de data.
        Lues dans la base admin pour la pluspart.
        """
        query_admin = utils_db.query(admin.db_admin)
        configCalculsDic = {}
        commandLine_admin = utils_db.q_selectAllFrom.format('config_calculs')
        query_admin = utils_db.queryExecute(
            commandLine_admin, query=query_admin)
        while query_admin.next():
            configCalculsDic[query_admin.value(0)] = (
                query_admin.value(1), query_admin.value(2))
        self.data['addressesLines'] = configCalculsDic.get('addressesLines', (10, ''))[0]
        self.data['details'] = (utils.ADMIN_WITH_DETAILS == 1)
        self.data['classColumn'] = (utils.ADMIN_NO_CLASS_COLUMN == 0)
        now = QtCore.QDateTime.currentDateTime()
        self.data['dateNow1'] = now.toString('yyyyMMdd')
        self.data['dateNow2'] = now.toString('dd/MM/yyyy')
        self.data['dateNow3'] = now.toString('yyyy')
        # données spécifiques à l'établissement :
        schoolData = (
            'ETAB_NOM', 
            'ETAB_ADRESSE_1', 'ETAB_ADRESSE_2', 'ETAB_ADRESSE_3', 'ETAB_ADRESSE', 
            'ETAB_TELEPHONE', 
            'ACADEMIE', 'DEPARTEMENT', 
            'FICHE BREVET', 'SERIE', 'SESSION', 
            'ANNEE_SCOLAIRE', )
        for champ in schoolData:
            self.data['SCHOOL_DATA'][champ] = ''
        self.data['SCHOOL_DATA']['ETAB_NOM'] = self.main.actualVersion['versionLabel']
        # lecture de admin.config :
        commandLine_admin = utils_db.q_selectAllFrom.format('config')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            self.data['SCHOOL_DATA'][query_admin.value(0)] = query_admin.value(2)
        self.data['SCHOOL_DATA']['ETAB_ADRESSE'] = self.data['SCHOOL_DATA']['ETAB_ADRESSE_1']
        if self.data['SCHOOL_DATA']['ETAB_ADRESSE_2'] != '':
            self.data['SCHOOL_DATA']['ETAB_ADRESSE'] = utils_functions.u(
                '{0}<br/>{1}').format(
                    self.data['SCHOOL_DATA']['ETAB_ADRESSE'], 
                    self.data['SCHOOL_DATA']['ETAB_ADRESSE_2'])
        if self.data['SCHOOL_DATA']['ETAB_ADRESSE_3'] != '':
            self.data['SCHOOL_DATA']['ETAB_ADRESSE'] = utils_functions.u(
                '{0}<br/>{1}').format(
                    self.data['SCHOOL_DATA']['ETAB_ADRESSE'], 
                    self.data['SCHOOL_DATA']['ETAB_ADRESSE_3'])
        self.data['SCHOOL_DATA']['SESSION'] = utils_functions.u(
            'Session JUIN {0}').format(self.main.annee_scolaire[0])
        self.data['SCHOOL_DATA']['ANNEE_SCOLAIRE'] = self.main.annee_scolaire[1]

    def doFinish(self):
        self.accept()

    def doBack(self):
        self.pagesWidget.widget(self.page['actual']).doFinish()
        self.page['next'] = self.page['actual']
        self.page['actual'] = self.page['back']
        self.page['back'] -= 1
        if self.page['back'] < 0:
            self.page['back'] = 0
        self.loadPage()

    def doNext(self):
        self.pagesWidget.widget(self.page['actual']).doFinish()
        self.page['back'] = self.page['actual']
        self.page['actual'] = self.page['next']
        self.page['next'] += 1
        if self.page['next'] > self.page['final']:
            self.page['next'] = self.page['final']
        self.loadPage()

    def loadPage(self):
        """
        appel et mise en place d'une page du wizard.
        On commence par vérifier ce qui doit être affiché et quels boutons sont actifs.
        Ensuite on remplit les données liées à la page.
        """
        utils_functions.doWaitCursor()
        try:
            # widgets visibles et boutons disponibles selon la page :
            self.buttonsList['back'].setEnabled(self.page['actual'] > 0)
            self.buttonsList['next'].setEnabled(self.page['actual'] < self.page['final'])
            self.pagesWidget.widget(self.page['actual']).reInit()
            self.pagesWidget.setCurrentIndex(self.page['actual'])
        finally:
            utils_functions.restoreCursor()





class WhatPage(QtWidgets.QWidget):
    """
    type de relevé (what), modèle, période et élèves.
    what indique le type de relevé ; valeurs possibles :
        * vide : bulletin classique (on utilisera la base resultats.sqlite)
        * referentialReports : relevés de référentiel 
        * annualReports : bilan annuel
        * DNB : fiches brevet (désactivé).
        * sorted : les bulletins sont triés par origine
    """
    def __init__(self, parent=None):
        super(WhatPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = QtWidgets.QApplication.translate(
            'main', 'Reports type, period and students')
        title = utils_functions.u(
            '<h3>{0} 1/3 - {1}</h3>').format(STEP_TEXT, title)
        titleLabel = QtWidgets.QLabel(title)

        # Type de relevés : dans un QScrollArea
        text = QtWidgets.QApplication.translate(
            'main', 'Type of reports')
        self.whatGroupBox = QtWidgets.QGroupBox(text)
        grid = QtWidgets.QVBoxLayout()
        scrollGrid = QtWidgets.QGridLayout()
        scroll = QtWidgets.QScrollArea()
        dummy = QtWidgets.QWidget()
        i = 0
        self.whatRadioButtons = {}
        for what in WHAT_LIST['ORDER']:
            radioButton = QtWidgets.QRadioButton(WHAT_LIST[what])
            self.whatRadioButtons[what] = radioButton
            scrollGrid.addWidget(radioButton, i, 0)
            i += 1
        separatorLabel = QtWidgets.QLabel(
            '<p align="center">{0}</p>'.format(utils.SEPARATOR_LINE))
        scrollGrid.addWidget(separatorLabel, i, 0)
        self.sortedCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'Sort by origin'))
        scrollGrid.addWidget(self.sortedCheckBox, i + 1, 0)
        dummy.setLayout(scrollGrid)
        scroll.setWidget(dummy)
        grid.addWidget(scroll)
        self.whatGroupBox.setLayout(grid)
        self.whatRadioButtons[self.parent.data['what']].setChecked(True)

        # Modèles :
        text = QtWidgets.QApplication.translate(
            'main', 'Template file')
        self.templatesGroupBox = QtWidgets.QGroupBox(text)
        self.templatesList = QtWidgets.QListWidget()
        entryInfoList = QtCore.QDir(
            admin.modelesDir).entryInfoList(['*.html'])
        for fileInfo in entryInfoList:
            fileName = fileInfo.fileName()
            if not fileName in ('fiches_brevet.html', 'temp.html', ):
                item = QtWidgets.QListWidgetItem(fileName)
                fileWithPath = fileInfo.absoluteFilePath()
                if len(self.parent.data['template']) < 1:
                    self.parent.data['template'] = fileWithPath
                item.setData(
                    QtCore.Qt.UserRole, 
                    fileWithPath)
                self.templatesList.addItem(item)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.templatesList)
        self.templatesGroupBox.setLayout(vLayout)
        self.templatesList.setCurrentRow(0)

        # Périodes : elles sont mises dans un QScrollArea
        # au cas où il y en aurait beaucoup (!)
        text = QtWidgets.QApplication.translate(
            'main', 'Period')
        self.periodGroupBox = QtWidgets.QGroupBox(text)
        grid = QtWidgets.QVBoxLayout()
        scrollGrid = QtWidgets.QGridLayout()
        scroll = QtWidgets.QScrollArea()
        dummy = QtWidgets.QWidget()
        i = 2
        if utils.selectedPeriod == 0:
            periods = range(1, utils.NB_PERIODES)
        else:
            periods = range(1, utils.selectedPeriod + 1)
        self.periodRadioButtons = {}
        for p in periods:
            radioButton = QtWidgets.QRadioButton(utils.PERIODES[p])
            self.periodRadioButtons[p] = radioButton
            scrollGrid.addWidget(radioButton, i, 0)
            i += 1
        dummy.setLayout(scrollGrid)
        scroll.setWidget(dummy)
        grid.addWidget(scroll)
        self.periodGroupBox.setLayout(grid)
        self.periodRadioButtons[self.parent.data['period']].setChecked(True)

        # Aide :
        mdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpCreateReports')

        # Élèves :
        text = QtWidgets.QApplication.translate(
            'main', 'Students')
        studentsGroup = QtWidgets.QGroupBox(text)
        helpMessage = QtWidgets.QApplication.translate(
            'main', 
            'To select all, you can leave the empty right list.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpMessage))
        self.selectionWidget = utils_2lists.ChooseStudentsWidget(
            parent = self.main, withComboBox = True)
        grid = QtWidgets.QGridLayout()
        grid.addWidget(helpLabel, 1, 0)
        grid.addWidget(self.selectionWidget, 2, 0)
        studentsGroup.setLayout(grid)

        # on agence tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)

        leftLayout = QtWidgets.QVBoxLayout()
        leftLayout.addWidget(self.whatGroupBox)
        leftLayout.addWidget(self.templatesGroupBox)
        leftLayout.addWidget(self.periodGroupBox)

        rightLayout = QtWidgets.QVBoxLayout()
        rightLayout.addWidget(mdHelpView)
        rightLayout.addWidget(studentsGroup)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addLayout(leftLayout)
        hLayout.addLayout(rightLayout)
        mainLayout.addLayout(hLayout)
        self.setLayout(mainLayout)

        # mise en place des connexions :
        for what in self.whatRadioButtons:
            self.whatRadioButtons[what].toggled.connect(self.doChanged)
        self.sortedCheckBox.stateChanged.connect(self.doChanged)
        self.templatesList.currentItemChanged.connect(self.doChanged)
        for p in self.periodRadioButtons:
            self.periodRadioButtons[p].toggled.connect(self.doChanged)
        self.reInit()
        self.updateData()

    def reInit(self):
        """
        appelé chaque fois qu'on affiche la page
        """
        self.initialData = {
            'what': self.parent.data['what'], 
            'period': self.parent.data['period'], 
            }

    def doFinish(self):
        """
        mise à jour du dico data
        """
        # sélection des élèves à traiter :
        self.parent.data['idsEleves'] = []
        for i in range(self.selectionWidget.selectionList.count()):
            item = self.selectionWidget.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            self.parent.data['idsEleves'].append(id_eleve)
        if len(self.parent.data['idsEleves']) < 1:
            for i in range(self.selectionWidget.baseList.count()):
                item = self.selectionWidget.baseList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                self.parent.data['idsEleves'].append(id_eleve)
        # mise à jour du préfixe de la classe :
        classPrefix = CLASS_PREFIX
        if self.selectionWidget.baseComboBox.currentIndex() > 0:
            className = self.selectionWidget.baseComboBox.currentText()
            classPrefix = utils_functions.doAscii(className, case='lower')
            classPrefix = utils_functions.doOnlyChars(classPrefix)
        self.parent.data['classPrefix'] = classPrefix
        # si what ou la période ont été modifiés :
        self.updateData()

    def updateData(self):
        mustDo = False
        what = self.parent.data['what']
        if what != self.initialData['what']:
            mustDo = True
        if self.parent.data['period'] != self.initialData['period']:
            mustDo = True
        if not(mustDo):
            return

        if what == 'annualReports':
            tr1 = QtWidgets.QApplication.translate('main', 'annual-report')
            tr2 = QtWidgets.QApplication.translate('main', 'ANNUAL REPORT')
            self.parent.data['baseNomFichier'] = utils_functions.u(
                '{0}-{1}').format(tr1, self.parent.data['dateNow1'])
            self.parent.data['labelDocument'] = utils_functions.u(
                '{0}  ({1})').format(tr2, self.parent.data['dateNow2'])
        elif what == 'DNB':
            self.parent.data['baseNomFichier'] = utils_functions.u(
                'fiche-brevet-{0}').format(self.parent.data['dateNow3'])
            self.parent.data['labelDocument'] = utils_functions.u(
                'FICHE SCOLAIRE BREVET  (Session JUIN {0})').format(self.parent.data['dateNow3'])
        elif what == 'referentialReports':
            tr1 = QtWidgets.QApplication.translate('main', 'referential-report')
            tr2 = QtWidgets.QApplication.translate('main', 'REFERENTIAL REPORT')
            self.parent.data['baseNomFichier'] = utils_functions.u(
                '{0}-{1}').format(tr1, self.parent.data['dateNow1'])
            self.parent.data['labelDocument'] = utils_functions.u(
                '{0}  ({1})').format(tr2, self.parent.data['dateNow2'])
        else:
            self.parent.data['baseNomFichier'] = utils_functions.u(
                'bulletin-trimestre-{0}').format(self.parent.data['period'])
            if self.parent.data['period'] == 0:
                texte = 'XXX'
            elif self.parent.data['period'] == 1:
                texte = '1er'
            else:
                texte = '{0}eme'.format(self.parent.data['period'])
            self.parent.data['labelDocument'] = utils_functions.u(
                'BULLETIN DU {0} TRIMESTRE').format(texte)

    def doChanged(self, first=None, second=None):
        """
        un champ a été modifié.
        On met data à jour.
        """
        for what in WHAT_LIST['ORDER']:
            if self.sender() == self.whatRadioButtons[what]:
                # first : checked
                if first:
                    self.parent.data['what'] = what
                    excludes = ('DNB', )
                    self.sortedCheckBox.setEnabled(what not in excludes)
                    self.templatesGroupBox.setEnabled(what not in excludes)
                    excludes = ('referentialReports', 'annualReports', 'DNB', )
                    self.periodGroupBox.setEnabled(what not in excludes)
                return

        if self.sender() == self.sortedCheckBox:
            self.parent.data['sorted'] = self.sortedCheckBox.isChecked()
            return

        if self.sender() == self.templatesList:
            # first: current, second: previous
            fileName = first.data(QtCore.Qt.UserRole)
            self.parent.data['template'] = fileName
            return

        for p in self.periodRadioButtons:
            if self.sender() == self.periodRadioButtons[p]:
                # first : checked
                if first:
                    self.parent.data['period'] = p
                return




class ConfigFilesPage(QtWidgets.QWidget):
    """
    configuration des fichiers.
    """
    def __init__(self, parent=None):
        super(ConfigFilesPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = QtWidgets.QApplication.translate(
            'main', 'Files configuration')
        title = utils_functions.u(
            '<h3>{0} 2/3 - {1}</h3>').format(STEP_TEXT, title)
        titleLabel = QtWidgets.QLabel(title)

        # un RegExp pour les noms de fichiers :
        fileNameRegExp = QtCore.QRegExp('[a-z0-9\_\-\+]*')

        # réglages de base :
        baseGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Files names'))
        self.baseNomFichierLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Base file names:')))
        self.baseNomFichierComboBox = QtWidgets.QComboBox()
        self.baseNomFichierComboBox.setEditable(True)
        self.baseNomFichierComboBox.setValidator(
            QtGui.QRegExpValidator(fileNameRegExp, self.baseNomFichierComboBox))
        self.labelDocumentLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Document Label:')))
        self.labelDocumentComboBox = QtWidgets.QComboBox()
        self.labelDocumentComboBox.setEditable(True)
        self.classPrefixLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Prefix of class file:')))
        self.classPrefixEdit = QtWidgets.QLineEdit()
        self.classPrefixEdit.setValidator(
            QtGui.QRegExpValidator(fileNameRegExp, self.classPrefixEdit))
        self.classLabelDocumentLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate(
                    'main', 'Description of class document:')))
        self.classLabelDocumentEdit = QtWidgets.QLineEdit()
        vBox = QtWidgets.QVBoxLayout()
        vBox.addWidget(self.baseNomFichierLabel)
        vBox.addWidget(self.baseNomFichierComboBox)
        vBox.addWidget(self.labelDocumentLabel)
        vBox.addWidget(self.labelDocumentComboBox)
        vBox.addWidget(self.classPrefixLabel)
        vBox.addWidget(self.classPrefixEdit)
        vBox.addWidget(self.classLabelDocumentLabel)
        vBox.addWidget(self.classLabelDocumentEdit)
        baseGroupBox.setLayout(vBox)

        # on gère le nombre de pages d'impression 
        # (multiples de 2 ou de 4 : RectoVerso + 2pages en 1)
        printGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Print Management'))
        self.on2PagesRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'Printing on 2 pages (2 pages 1 OR both sides)'))
        self.on2PagesRadio.setChecked(True)
        self.on4PagesRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'Printing on 4 pages (2 pages 1 AND both sides)'))
        self.on1PagesRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'Printing without adding blank pages'))
        vBox = QtWidgets.QVBoxLayout()
        vBox.addWidget(self.on2PagesRadio)
        vBox.addWidget(self.on4PagesRadio)
        vBox.addWidget(self.on1PagesRadio)
        printGroupBox.setLayout(vBox)

        # Orientation
        orientationGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Orientation'))
        self.portraitRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Portrait'))
        self.portraitRadio.setChecked(True)
        self.landscapeRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate('main', 'Landscape'))
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget(self.portraitRadio)
        hBox.addWidget(self.landscapeRadio)
        vBox = QtWidgets.QVBoxLayout()
        vBox.addLayout(hBox)
        vBox.addStretch(1)
        orientationGroupBox.setLayout(vBox)

        # réglages supplémentaires pour les fichiers
        docsGroupBox = QtWidgets.QGroupBox(
            QtWidgets.QApplication.translate('main', 'Documents management'))
        self.classColumnCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'Show class results'))
        self.withDetailsCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate('main', 'View Details assessments'))
        self.hideStudentNamesCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Hide the names of students and teachers'))
        self.colorsOnlyCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Show only colors'))
        self.deleteNotEvaluatedLinesCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Delete lines not evaluated'))
        self.onlyClassFileCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Create only the class file'))
        text0 = QtWidgets.QApplication.translate(
            'main', 'Uncheck the box below if test')
        text1 = QtWidgets.QApplication.translate(
            'main', 
            'or whether the documents should not be made available to students')
        text = utils_functions.u(
            '<p align="center"><br/><b>{0}<br/>{1}</b></p>').format(text0, text1)
        self.mustUpdateAndMoveLabel = QtWidgets.QLabel(text)
        self.mustUpdateAndMoveCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Update the documents table and move PDF files'))
        self.withAddressesCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Use addresses'))
        self.addressesLinesLabel = QtWidgets.QLabel(
            QtWidgets.QApplication.translate(
                'main', 'Number lines for addresses:'))
        self.addressesLinesSpinBox = QtWidgets.QSpinBox()
        self.addressesLinesSpinBox.setMinimum(1)
        self.addressesLinesSpinBox.setMaximum(20)
        self.addressesLinesSpinBox.setValue(10)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget(self.addressesLinesLabel)
        hBox.addWidget(self.addressesLinesSpinBox)
        vBox = QtWidgets.QVBoxLayout()
        vBox.addWidget(self.withDetailsCheckBox)
        self.separatorLabel = QtWidgets.QLabel(
            '<p align="center">{0}</p>'.format(utils.SEPARATOR_LINE))
        vBox.addWidget(self.withAddressesCheckBox)
        vBox.addLayout(hBox)
        vBox.addWidget(self.classColumnCheckBox)
        vBox.addWidget(self.hideStudentNamesCheckBox)
        vBox.addWidget(self.colorsOnlyCheckBox)
        vBox.addWidget(self.deleteNotEvaluatedLinesCheckBox)
        vBox.addWidget(self.separatorLabel)
        vBox.addWidget(self.onlyClassFileCheckBox)
        vBox.addWidget(self.mustUpdateAndMoveLabel)
        vBox.addWidget(self.mustUpdateAndMoveCheckBox)
        vBox.addStretch(1)
        docsGroupBox.setLayout(vBox)

        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(baseGroupBox)
        vBoxLeft.addWidget(printGroupBox)
        vBoxLeft.addWidget(orientationGroupBox)
        vBoxRight = QtWidgets.QVBoxLayout()
        vBoxRight.addWidget(docsGroupBox)
        hBox = QtWidgets.QHBoxLayout()
        hBox.addLayout(vBoxLeft)
        hBox.addLayout(vBoxRight)

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)
        mainLayout.addLayout(hBox)
        self.setLayout(mainLayout)
        self.reInit()
        self.labelDocumentComboBox.editTextChanged.connect(
            self.updateClassLabelDocument)
        self.classPrefixEdit.textEdited.connect(
            self.updateClassLabelDocument)

    def reInit(self):
        """
        appelé chaque fois qu'on affiche la page
        """
        # cas particuliers :
        if self.parent.data['what'] in ('DNB', 'referentialReports'):
            if self.parent.data['what'] == 'DNB':
                self.parent.data['withAddresses'] = False
            self.parent.data['withDetails'] = False
            self.parent.data['classColumn'] = False
        if self.parent.data['sorted']:
            self.parent.data['withAddresses'] = False
            self.parent.data['onlyClassFile'] = True

        if self.parent.data['details']:
            self.withDetailsCheckBox.setVisible(True)
            self.withDetailsCheckBox.setChecked(
                self.parent.data['withDetails'])
        else:
            self.withDetailsCheckBox.setVisible(False)
            self.withDetailsCheckBox.setChecked(False)
        if self.parent.data['withAddresses']:
            self.withAddressesCheckBox.setChecked(True)
        else:
            self.withAddressesCheckBox.setChecked(False)
        self.addressesLinesSpinBox.setValue(
            self.parent.data['addressesLines'])

        widgets = (
            self.baseNomFichierLabel, 
            self.baseNomFichierComboBox, 
            self.classPrefixLabel, 
            self.classPrefixEdit, 
            self.classLabelDocumentLabel, 
            self.classLabelDocumentEdit, 
            )
        visible = (self.parent.data['sorted'] == False)
        for widget in widgets:
            widget.setVisible(visible)

        widgets = (
            self.withAddressesCheckBox, 
            self.addressesLinesLabel, 
            self.addressesLinesSpinBox, 
            self.classColumnCheckBox, 
            self.colorsOnlyCheckBox, 
            self.deleteNotEvaluatedLinesCheckBox, 
            self.separatorLabel, 
            self.mustUpdateAndMoveLabel, 
            self.mustUpdateAndMoveCheckBox, 
            )
        for widget in widgets:
            widget.setVisible(False)
        self.mustUpdateAndMoveCheckBox.setChecked(False)
        if self.parent.data['sorted']:
            for widget in widgets:
                widget.setVisible(False)
            self.classColumnCheckBox.setVisible(True)
            self.colorsOnlyCheckBox.setVisible(True)
            self.deleteNotEvaluatedLinesCheckBox.setVisible(True)
            self.onlyClassFileCheckBox.setVisible(False)
        elif self.parent.data['what'] == 'DNB':
            for widget in widgets:
                widget.setVisible(False)
            self.classColumnCheckBox.setVisible(False)
            self.colorsOnlyCheckBox.setVisible(False)
            self.deleteNotEvaluatedLinesCheckBox.setVisible(False)
            self.onlyClassFileCheckBox.setVisible(True)
        else:
            for widget in widgets:
                widget.setVisible(True)
            self.mustUpdateAndMoveCheckBox.setChecked(
                self.parent.data['mustUpdateAndMove'])
            self.classColumnCheckBox.setVisible(True)
            self.colorsOnlyCheckBox.setVisible(True)
            self.deleteNotEvaluatedLinesCheckBox.setVisible(True)
            self.onlyClassFileCheckBox.setVisible(True)

        # on récupère la liste des noms depuis le fichier name_list.txt
        # et on remplit les 2 ComboBox :
        self.baseNomFichierComboBox.clear()
        self.labelDocumentComboBox.clear()
        if self.parent.data['what'] == 'DNB':
            self.baseNomFichierComboBox.addItem(
                self.parent.data['baseNomFichier'])
            self.labelDocumentComboBox.addItem(
                self.parent.data['labelDocument'])
            self.on1PagesRadio.setChecked(True)
        else:
            nameListFile = utils_functions.u(
                '{0}/name_list.txt').format(admin.modelesDir)
            oldFile = utils_functions.u(
                '{0}/nameList.txt').format(admin.modelesDir)
            if QtCore.QFile(oldFile).exists():
                QtCore.QFile(oldFile).copy(nameListFile)
                QtCore.QFile(oldFile).remove()
            inFile = QtCore.QFile(nameListFile)
            if inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
                try:
                    if self.parent.data['what'] in ('annualReports', 'referentialReports'):
                        self.baseNomFichierComboBox.addItem(
                            self.parent.data['baseNomFichier'])
                        self.labelDocumentComboBox.addItem(
                            self.parent.data['labelDocument'])
                    textStream = QtCore.QTextStream(inFile)
                    textStream.setCodec('UTF-8')
                    listForRemplacements = []
                    listForComboBox = []
                    zone = 0
                    while not(textStream.atEnd()):
                        line = textStream.readLine()
                        if '*******' in line:
                            zone += 1
                        else:
                            if line != '':
                                if zone == 0:
                                    line = line.replace('aaaammjj', self.parent.data['dateNow1'])
                                    line = line.replace(
                                        'xxx', utils_functions.u(self.parent.data['period']))
                                    if not(self.parent.data['sorted']):
                                        self.baseNomFichierComboBox.addItem(line)
                                elif zone == 1:
                                    line = line.replace('jj/mm/aaaa', self.parent.data['dateNow2'])
                                    listForComboBox.append(line)
                                else:
                                    listForRemplacements.append(line)
                    inFile.close()
                    for line in listForComboBox:
                        if self.parent.data['period'] > 0:
                            line = line.replace(
                                'XXX', listForRemplacements[self.parent.data['period'] - 1])
                        self.labelDocumentComboBox.addItem(line)
                except:
                    self.baseNomFichierComboBox.addItem(self.parent.data['baseNomFichier'])
                    self.labelDocumentComboBox.addItem(self.parent.data['labelDocument'])
                    pass
            else:
                self.baseNomFichierComboBox.addItem(
                    self.parent.data['baseNomFichier'])
                self.labelDocumentComboBox.addItem(
                    self.parent.data['labelDocument'])
            self.baseNomFichierComboBox.setCurrentIndex(0)
            self.labelDocumentComboBox.setCurrentIndex(0)
            if self.parent.data['classColumn']:
                self.classColumnCheckBox.setChecked(True)
            if self.parent.data['onlyClassFile']:
                self.onlyClassFileCheckBox.setChecked(True)
        self.classPrefixEdit.setText(self.parent.data['classPrefix'])
        self.updateClassLabelDocument()

    def doFinish(self):
        """
        mise à jour du dico data
        """
        self.parent.data['baseNomFichier'] = self.baseNomFichierComboBox.currentText()
        self.parent.data['labelDocument'] = self.labelDocumentComboBox.currentText()
        self.parent.data['classPrefix'] = self.classPrefixEdit.text()
        self.parent.data['classLabelDocument'] = self.classLabelDocumentEdit.text()
        self.parent.data['hideStudentNames'] = self.hideStudentNamesCheckBox.isChecked()
        self.parent.data['colorsOnly'] = self.colorsOnlyCheckBox.isChecked()
        self.parent.data['deleteNotEvaluatedLines'] = self.deleteNotEvaluatedLinesCheckBox.isChecked()
        self.parent.data['printOn4Pages'] = self.on4PagesRadio.isChecked()
        self.parent.data['printOn2Pages'] = self.on2PagesRadio.isChecked()
        if self.portraitRadio.isChecked():
            self.parent.data['orientation'] = 'Portrait'
            self.parent.data['blankPagePdfName'] = 'blank_page.pdf'
        else:
            self.parent.data['orientation'] = 'Landscape'
            self.parent.data['blankPagePdfName'] = 'blank_page_landscape.pdf'
        self.parent.data['withDetails'] = self.withDetailsCheckBox.isChecked()
        self.parent.data['withAddresses'] = self.withAddressesCheckBox.isChecked()
        newAddressesLines = self.addressesLinesSpinBox.value()
        changes = {}
        if newAddressesLines != self.parent.data['addressesLines']:
            self.parent.data['addressesLines'] = newAddressesLines
            changes['addressesLines'] = (self.parent.data['addressesLines'], '')
        self.parent.data['onlyClassFile'] = self.onlyClassFileCheckBox.isChecked()
        self.parent.data['mustUpdateAndMove'] = self.mustUpdateAndMoveCheckBox.isChecked()
        newClassColumn = self.classColumnCheckBox.isChecked()
        if newClassColumn != self.parent.data['classColumn']:
            self.parent.data['classColumn'] = newClassColumn
            if self.parent.data['classColumn']:
                value = 0
            else:
                value = 1
            utils.changeAdminNoClassColumn(value)
            changes['adminNoClassColumn'] = (value, '')
        if len(changes) > 0:
            utils_db.changeInConfigTable(
                self.main, admin.db_admin, changes, table='config_calculs')

    def updateClassLabelDocument(self):
        """
        mise à jour de la description du fichier classe
        """
        text = utils_functions.u('{0} - {1}').format(
            self.classPrefixEdit.text(), 
            self.labelDocumentComboBox.currentText())
        self.classLabelDocumentEdit.setText(text)





class CreateFilesPage(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(CreateFilesPage, self).__init__(parent)
        self.main = parent.main
        self.parent = parent

        title = QtWidgets.QApplication.translate(
            'main', 'Files creation')
        title = utils_functions.u(
            '<h3>{0} 3/3 - {1}</h3>').format(STEP_TEXT, title)
        titleLabel = QtWidgets.QLabel(title)

        # les boutons (création et ouverture du dossier) :
        self.createFiles_Button = QtWidgets.QToolButton()
        self.openFilesDir_Button = QtWidgets.QToolButton()
        # mise en forme des boutons :
        buttons = {
            self.createFiles_Button: (
                'validations', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Create Files'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Launch files making'), 
                self.createReports), 
            self.openFilesDir_Button: (
                'folder', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Open folder'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Open the folder containing the created files'), 
                self.openFilesDir), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize']
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE * 2, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])
        # première série d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.createFiles_Button)
        hLayout0.addWidget(self.openFilesDir_Button)
        hLayout0.addStretch(1)

        # partie DNB : on utilise un QScrollArea
        text = QtWidgets.QApplication.translate(
            'main', 'Configuring School')
        self.dnbGroupBox = QtWidgets.QGroupBox(text)
        # Texte d'explications :
        helpText = QtWidgets.QApplication.translate(
            'main', 
            'Check out the different settings before continuing.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpText))
        # Académie :
        academieText = QtWidgets.QApplication.translate('main', 'Academie:')
        academieLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(academieText))
        self.academieEdit = QtWidgets.QLineEdit()
        # Département :
        departementText = QtWidgets.QApplication.translate(
            'main', 'Departement:')
        departementLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(departementText))
        self.departementEdit = QtWidgets.QLineEdit()
        # Nom de l'établissement :
        schoolLabelText = QtWidgets.QApplication.translate(
            'main', 'Full name of the school:')
        schoolLabelLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(schoolLabelText))
        self.schoolLabelEdit = QtWidgets.QLineEdit()
        # Adresse de l'établissement (sur 3 lignes) :
        adressText = QtWidgets.QApplication.translate('main', 'Adress:')
        adressLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(adressText))
        self.adress1Edit = QtWidgets.QLineEdit()
        self.adress2Edit = QtWidgets.QLineEdit()
        self.adress3Edit = QtWidgets.QLineEdit()
        # Numéro de téléphone de l'établissement :
        telephoneText = QtWidgets.QApplication.translate('main', 'Phone:')
        telephoneLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(telephoneText))
        self.telephoneEdit = QtWidgets.QLineEdit()
        # Titre du document (fiche DNB) :
        documentTitleText = QtWidgets.QApplication.translate(
            'main', 'Document title:')
        documentTitleLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p><b>{0}</b></p>').format(documentTitleText))
        self.documentTitleEdit = QtWidgets.QLineEdit()
        # Série :
        serieText = QtWidgets.QApplication.translate('main', 'Serie:')
        serieLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(serieText))
        self.serieEdit = QtWidgets.QLineEdit()
        # Session :
        sessionText = QtWidgets.QApplication.translate('main', 'Session:')
        sessionLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(sessionText))
        self.sessionEdit = QtWidgets.QLineEdit()
        # Année scolaire :
        anneeScolaireText = QtWidgets.QApplication.translate('main', 'School year:')
        anneeScolaireLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(anneeScolaireText))
        #self.anneeScolaireEdit = QtWidgets.QLineEdit()
        # on place tout dans une grille :
        grid = QtWidgets.QVBoxLayout()
        scrollGrid = QtWidgets.QGridLayout()
        scroll = QtWidgets.QScrollArea()
        dummy = QtWidgets.QWidget()
        scrollGrid.addWidget(academieLabel,             1, 0)
        scrollGrid.addWidget(self.academieEdit,         1, 1)
        scrollGrid.addWidget(departementLabel,          5, 0)
        scrollGrid.addWidget(self.departementEdit,      5, 1)
        scrollGrid.addWidget(schoolLabelLabel,          10, 0)
        scrollGrid.addWidget(self.schoolLabelEdit,      10, 1)
        scrollGrid.addWidget(adressLabel,               15, 0)
        scrollGrid.addWidget(self.adress1Edit,          15, 1)
        scrollGrid.addWidget(self.adress2Edit,          16, 1)
        scrollGrid.addWidget(self.adress3Edit,          17, 1)
        scrollGrid.addWidget(telephoneLabel,            18, 0)
        scrollGrid.addWidget(self.telephoneEdit,        18, 1)
        scrollGrid.addWidget(documentTitleLabel,        20, 0)
        scrollGrid.addWidget(self.documentTitleEdit,    20, 1)
        scrollGrid.addWidget(serieLabel,                25, 0)
        scrollGrid.addWidget(self.serieEdit,            25, 1)
        scrollGrid.addWidget(sessionLabel,              30, 0)
        scrollGrid.addWidget(self.sessionEdit,          30, 1)
        #scrollGrid.addWidget(anneeScolaireLabel,        31, 0)
        #scrollGrid.addWidget(self.anneeScolaireEdit,    31, 1)
        dummy.setLayout(scrollGrid)
        dummy.setMinimumWidth(500)
        scroll.setWidget(dummy)
        grid.addWidget(helpLabel)
        grid.addWidget(scroll)
        self.dnbGroupBox.setLayout(grid)

        # EditLog :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)

        # on agence tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleLabel)

        leftLayout = QtWidgets.QVBoxLayout()
        leftLayout.addLayout(hLayout0)
        leftLayout.addWidget(self.dnbGroupBox)
        leftLayout.addStretch(1)

        rightLayout = QtWidgets.QVBoxLayout()
        rightLayout.addLayout(messagesLayout)

        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addLayout(leftLayout)
        hLayout.addLayout(rightLayout)
        mainLayout.addLayout(hLayout)
        self.setLayout(mainLayout)
        self.reInit()

        lineEdits = (
            self.academieEdit, 
            self.departementEdit, 
            self.schoolLabelEdit, 
            self.adress1Edit, 
            self.adress2Edit, 
            self.adress3Edit, 
            self.telephoneEdit, 
            self.documentTitleEdit, 
            self.serieEdit, 
            self.sessionEdit, 
            #self.anneeScolaireEdit, 
            )
        for widget in lineEdits:
            widget.textEdited.connect(self.doTextEdited)

    def openFilesDir(self):
        """
        ouvre le dossier où les fichiers ont été créés.
        """
        if self.parent.data['what'] == 'DNB':
            dirName = utils_functions.u(
                '{0}/fichiers/').format(admin.adminDir)
        elif self.parent.data['mustUpdateAndMove']:
            dirName = utils_functions.u(
                '{0}/protected/documents/').format(admin.dirLocalPrive)
        else:
            dirName = utils_functions.u(
                '{0}/fichiers/pdf/').format(admin.adminDir)
        utils_filesdirs.openDir(dirName)

    def reInit(self):
        """
        appelé chaque fois qu'on affiche la page
        """
        #print(self.parent.data)
        if self.parent.data['what'] != 'DNB':
            self.dnbGroupBox.setVisible(False)
            return
        self.dnbGroupBox.setVisible(True)
        # mise à jour des QLineEdit :
        self.academieEdit.setText(self.parent.data['SCHOOL_DATA'].get('ACADEMIE', ''))
        self.departementEdit.setText(self.parent.data['SCHOOL_DATA'].get('DEPARTEMENT', ''))
        self.schoolLabelEdit.setText(self.parent.data['SCHOOL_DATA'].get('ETAB_NOM', ''))
        self.adress1Edit.setText(self.parent.data['SCHOOL_DATA'].get('ETAB_ADRESSE_1', ''))
        self.adress2Edit.setText(self.parent.data['SCHOOL_DATA'].get('ETAB_ADRESSE_2', ''))
        self.adress3Edit.setText(self.parent.data['SCHOOL_DATA'].get('ETAB_ADRESSE_3', ''))
        self.telephoneEdit.setText(self.parent.data['SCHOOL_DATA'].get('ETAB_TELEPHONE', ''))
        self.documentTitleEdit.setText(self.parent.data['SCHOOL_DATA'].get('FICHE BREVET', ''))
        self.serieEdit.setText(self.parent.data['SCHOOL_DATA'].get('SERIE', ''))
        self.sessionEdit.setText(self.parent.data['SCHOOL_DATA'].get('SESSION', ''))
        #self.anneeScolaireEdit.setText(self.parent.data['SCHOOL_DATA'].get('ANNEE_SCOLAIRE', ''))

    def doTextEdited(self):
        self.parent.data['SCHOOL_DATA']['ETAB_NOM'] = self.schoolLabelEdit.text()
        self.parent.data['SCHOOL_DATA']['ETAB_ADRESSE'] = self.adress1Edit.text()
        if self.adress2Edit.text() != '':
            self.parent.data['SCHOOL_DATA']['ETAB_ADRESSE'] = utils_functions.u('{0}<br/>{1}').format(
                self.parent.data['SCHOOL_DATA']['ETAB_ADRESSE'], 
                self.adress2Edit.text())
        if self.adress3Edit.text() != '':
            self.parent.data['SCHOOL_DATA']['ETAB_ADRESSE'] = utils_functions.u('{0}<br/>{1}').format(
                self.parent.data['SCHOOL_DATA']['ETAB_ADRESSE'], 
                self.adress3Edit.text())
        self.parent.data['SCHOOL_DATA']['ACADEMIE'] = self.academieEdit.text()
        self.parent.data['SCHOOL_DATA']['DEPARTEMENT'] = self.departementEdit.text()
        self.parent.data['SCHOOL_DATA']['FICHE BREVET'] = self.documentTitleEdit.text()
        self.parent.data['SCHOOL_DATA']['SERIE'] = self.serieEdit.text()
        self.parent.data['SCHOOL_DATA']['SESSION'] = self.sessionEdit.text()
        self.parent.data['SCHOOL_DATA']['ETAB_TELEPHONE'] = self.telephoneEdit.text()
        #self.parent.data['SCHOOL_DATA']['ANNEE_SCOLAIRE'] = self.anneeScolaireEdit.text()

    def doFinish(self):
        """
        juste pour compatibilité
        """

    def createReports(self):
        """
        procédure de création des bulletins
        on passera la main à la procédure createStudentFileFromHtmlTemplate.
        """

        def traiteAffichageValeur(value, classe=False, colorsOnly=False):
            """
            pour gérer la mise en forme de l'affichage
            selon la valeur (couleur par exemple).
            """
            affichageValue = utils_functions.valeur2affichage(value)
            dico = {'A': 'A V', 'B': 'B J', 'C': 'C O', 'D': 'D R', 'X': 'X'}
            if value in dico:
                if colorsOnly:
                    affichageValue = ''
                if classe:
                    result = utils_functions.u(
                        '<td class="{0}">{1}</td>').format(
                            dico[value], affichageValue)
                else:
                    result = utils_functions.u(
                        '<td class="{0}"><b>{1}</b></td>').format(
                            dico[value], affichageValue)
            else:
                result = '<td class="valeur"></td>'
            return result

        def appendUniqueDic(aDic, aParam, aValue):
            """
            pour ajouter un élément à un dictionnaire de façon unique
            """
            listNulValues = ('', '<td class="valeur"></td>')
            value = aDic.get(aParam, '')
            if value in listNulValues:
                aDic[aParam] = aValue

        def doProfName(name, forName):
            """
            pour mettre en forme le nom d'un prof
            """
            if len(forName) > 0:
                profName = utils_functions.u('{0}. {1}').format(forName[0], name)
            else:
                profName = utils_functions.u('{0}').format(name)
            return profName

        def loadDicAdresses(query_admin, id_students, addressesLines):
            dicAdresses = {}
            commandLine = (
                'SELECT * FROM adresses '
                'WHERE use=1 AND id_eleve IN ({0}) '
                'ORDER BY id_eleve, state').format(id_students)
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                state = int(query_admin.value(2))
                nom1 = query_admin.value(3)
                nom2 = query_admin.value(4)
                adresse = query_admin.value(5)
                noms = '<b>'
                if nom1 != '':
                    noms = utils_functions.u('{0}{1}').format(noms, nom1)
                if nom2 != '':
                    noms = utils_functions.u('{0}|{1}').format(noms, nom2)
                adresse = utils_functions.u('{0}</b>###{1}').format(noms, adresse)
                l = [e for e in adresse.split('|') if e != '']
                adresse = utils_functions.u('\n').join(l)
                nbLines = len(l)
                adresse = adresse.replace('###', '\n\n')
                nbLines += 2
                while nbLines < addressesLines - 1:
                    adresse = utils_functions.u('\n{0}\n').format(adresse)
                    nbLines += 2
                while nbLines < addressesLines:
                    adresse = utils_functions.u('{0}\n').format(adresse)
                    nbLines += 1
                if not(id_eleve in dicAdresses):
                    dicAdresses[id_eleve] = []
                dicAdresses[id_eleve].append(adresse)
            return dicAdresses

        def doPhoto(id_eleve):
            """
            pour mettre en forme l'insertion 
            de la photo dans le fichier html
            """
            photo = ''
            photoFileName = utils_functions.u(
                '{0}/protected/photos/{1}.jpeg').format(
                    admin.dirLocalPrive, id_eleve)
            if QtCore.QFile(photoFileName).exists():
                photo = (
                    '<img src="../ftp/secret/verac/protected/photos/{0}.jpeg" '
                    'align=right width=60 height=77>').format(id_eleve)
            return photo


        utils_functions.afficheMessage(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Files Creation (PDF) from a template'), tags=('h2'))
        utils_functions.doWaitCursor()
        # pour calculer la durée :
        debut = QtCore.QTime.currentTime()
        # connexion aux bases de données :
        query_commun = utils_db.query(self.main.db_commun)
        query_admin = utils_db.query(admin.db_admin)
        try:
            # pour récupérer la liste des élèves pour qui il y aurait eu un problème :
            listePourErreurs = []
            # chaque élément correspondra à un élève
            donneesEleves = []

            what = self.parent.data['what']
            template = self.parent.data['template']
            baseNomFichier = self.parent.data['baseNomFichier']
            if what == 'DNB':
                template = utils_functions.u(
                    '{0}/fiches_brevet.html').format(admin.modelesDir)
                # connexion aux bases de données :
                admin.openDB(self.main, 'recup_evals')
                query_recupEvals = utils_db.query(admin.db_recupEvals)
                import utils_calculs, utils_dnb

                commandLineAppProf = utils_functions.u(
                    'SELECT appreciation FROM appreciations '
                    'WHERE id_eleve={0} AND matiere="{1}" AND id_prof={2} '
                    'ORDER BY Periode DESC')

                # On fabrique la liste eleves :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate('main', 'Making the list of students'), 
                    editLog=True)
                # un dictionnaire pour ne calculer les moyennes qu'une fois :
                moyennes = {}
                # pour chaque élève :
                for id_eleve in self.parent.data['idsEleves']:
                    # le premier dictionnaire contient les parties partagées :
                    eleveActuelCommun = {'id_eleve': id_eleve}
                    # le deuxième dictionnaire contient les valeurs des évaluations :
                    eleveActuelCommunValeurs = {}
                    # le troisième dictionnaire contient les données liées aux matières :
                    eleveActuelMatiere = {}
                    # et un dernier pour les détails des bilans communs :
                    eleveActuelCommunDetails = {}

                    # on remplit le premier dictionnaire :
                    for champ in self.parent.data['SCHOOL_DATA']:
                        eleveActuelCommun[champ] = self.parent.data['SCHOOL_DATA'][champ]
                    ok = False
                    commandLine = utils_db.q_selectAllFromWhere.format(
                        'eleves', 'id', id_eleve)
                    query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                    if query_admin.first():
                        ok = True
                        eleveActuelCommun['INE'] = query_admin.value(1)
                        eleveActuelCommun['NOM'] = query_admin.value(2)
                        eleveActuelCommun['Prenom'] = query_admin.value(3)
                        classe = query_admin.value(4)
                        eleveActuelCommun['Classe'] = query_admin.value(4)
                        eleveActuelCommun['NOM Prenom'] = query_admin.value(2) + ' ' + query_admin.value(3)
                        eleveActuelCommun['Date Naissance'] = query_admin.value(6)
                        dateNaissance = query_admin.value(6)
                        dateNaissance = QtCore.QDateTime().fromString(dateNaissance, 'ddMMyyyy')
                        dateNaissance = dateNaissance.toString('dd/MM/yyyy')
                        eleveActuelCommun['Date Naissance'] = dateNaissance
                        eleveActuelCommun['LABEL DOCUMENT'] = self.parent.data['labelDocument']
                        eleveActuelCommun['NomFichier'] = utils_functions.u(
                            '{0}-{1}').format(id_eleve, baseNomFichier)
                        eleveActuelCommun['DateFichier'] = ''
                    if self.parent.data['hideStudentNames']:
                        eleveActuelCommun['NOM'] = 'XXXX'
                        eleveActuelCommun['Prenom'] = 'xxxx'
                        eleveActuelCommun['NOM Prenom'] = 'XXXX xxxx'
                    if not(ok):
                        # on n'a rien trouvé pour cet élève, car il n'est pas dans la base.
                        msg = QtWidgets.QApplication.translate('main', 'NO RESULTS')
                        msg = utils_functions.u('{0} ({1})').format(id_eleve, msg)
                        listePourErreurs.append(msg)
                        continue

                    # affichage du message :
                    msg = utils_functions.u('{0}').format(eleveActuelCommun['NOM Prenom'])
                    utils_functions.afficheMessage(self.main, msg, editLog=True)

                    # pour chaque matière, il faut vérifier le nombre de prof intervenant
                    for matiereCode in admin.MATIERES['ALL']:
                        id_matiere =  admin.MATIERES['Code2Matiere'][matiereCode][0]
                        # on récupère le nom du prof
                        eleveActuelMatiere[id_matiere] = {}
                        commandLine = utils_functions.u(
                            'SELECT id_prof FROM eleve_prof '
                            'WHERE id_eleve={0} AND Matiere="{1}" AND Periode=999').format(
                                id_eleve, matiereCode)
                        query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                        while query_recupEvals.next():
                            id_prof = int(query_recupEvals.value(0))
                            eleveActuelMatiere[id_matiere][id_prof] = {}
                        for id_prof in eleveActuelMatiere[id_matiere]:
                            # on remplit le dictionnaire eleveActuelMatiere
                            # la note, moyenne, écart-type etc s'il y en a :
                            if id_eleve in admin.elevesNotes:
                                note, moyenne, id_groupe = '', '', -1
                                commandLine = utils_functions.u(
                                    'SELECT * FROM notes '
                                    'WHERE id_eleve={0} '
                                    'AND Matiere="{1}" '
                                    'AND id_prof={2} '
                                    'AND Periode=999').format(id_eleve, matiereCode, id_prof)
                                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                if query_recupEvals.first():
                                    note = float(query_recupEvals.value(4))
                                # on cherche maintenant la moyenne du groupe :
                                commandLine = utils_functions.u(
                                    'SELECT * FROM eleve_groupe '
                                    'WHERE id_eleve={0} '
                                    'AND Matiere="{1}" '
                                    'AND id_prof={2} '
                                    'AND Periode=999').format(id_eleve, matiereCode, id_prof)
                                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                if query_recupEvals.first():
                                    id_groupe = int(query_recupEvals.value(2))
                                if (id_prof, id_matiere, id_groupe) in moyennes:
                                    moyenne = moyennes[(id_prof, id_matiere, id_groupe)]
                                elif id_groupe < 0:
                                    moyenne = ''
                                else:
                                    notes = []
                                    commandLine = utils_functions.u(
                                        'SELECT notes.* FROM notes '
                                        'JOIN eleve_groupe ON ('
                                        'eleve_groupe.id_eleve=notes.id_eleve '
                                        'AND eleve_groupe.id_prof=notes.id_prof '
                                        'AND eleve_groupe.Matiere=notes.Matiere) '
                                        'WHERE notes.id_prof={0} '
                                        'AND notes.Matiere="{1}" '
                                        'AND notes.Periode=999 '
                                        'AND eleve_groupe.id_groupe={2} '
                                        'AND eleve_groupe.Periode=999').format(
                                            id_prof, matiereCode, id_groupe)
                                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                    while query_recupEvals.next():
                                        notes.append(int(query_recupEvals.value(4)))
                                    moyenne = round(utils_calculs.average(notes), 1)
                                    moyennes[(id_prof, id_matiere, id_groupe)] = moyenne
                                if note != '':
                                    eleveActuelMatiere[id_matiere][id_prof]['NOTES'] = (note, moyenne)
                            # l'appréciation dans cette matière pour ce prof:
                            appreciation = ''
                            commandLine = commandLineAppProf.format(
                                id_eleve, matiereCode, id_prof)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            while query_recupEvals.next():
                                if len(appreciation) < 1:
                                    appreciation = query_recupEvals.value(0)
                            # on limite le nombre de caractères pour n'avoir qu'une page
                            appreciation = appreciation[:utils_dnb.LIMITE_APPRECIATIONS]
                            eleveActuelMatiere[id_matiere][id_prof]['APPRECIATION'] = appreciation

                    # on ajoute la partie chef d'établissement (avis, ...) :
                    eleveActuelMatiere['AVIS_DNB'] = {}
                    query_recupEvals = utils_db.queryExecute(
                        utils_db.qs_prof_dnbValues.format(id_eleve), 
                        query=query_recupEvals)
                    while query_recupEvals.next():
                        id_dnb = int(query_recupEvals.value(2))
                        value = query_recupEvals.value(3)
                        eleveActuelMatiere['AVIS_DNB'][id_dnb] = value

                    # chaque élément de la liste donneesEleves contient les dictionnaires :
                    donneesEleves.append(
                        (eleveActuelCommun, 
                        eleveActuelCommunValeurs, 
                        eleveActuelMatiere, 
                        eleveActuelCommunDetails))
            elif what == 'annualReports':
                # connexion aux bases de données :
                query_users = utils_db.query(self.main.db_users)
                admin.openDB(self.main, 'recup_evals')
                query_recupEvals = utils_db.query(admin.db_recupEvals)
                import utils_calculs

                # les listes dont on a besoin pour générer les fichiers :
                listCPTBLTCommun = []
                listCPTReferentiel = []
                listCPTConfidentiel = []
                listCPTDetails = []
                dicAnDernier = {}
                dicAdresses = {}

                if self.parent.data['sorted']:
                    baseNomFichier = ''

                # on récupère les titres des CPT partagées :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'The titles of shared competence'), 
                    editLog=True)
                commandLine = 'SELECT * FROM {0} WHERE Competence!=""'
                for table in ('bulletin', 'referentiel', 'confidentiel'):
                    query_commun = utils_db.queryExecute(commandLine.format(table), query=query_commun)
                    while query_commun.next():
                        code = query_commun.value(1)
                        label = query_commun.value(5)
                        classeType = query_commun.value(11)
                        if table == 'bulletin':
                            listCPTBLTCommun.append((code, classeType, label))
                            id_CPT = int(query_commun.value(0)) + utils.decalageBLT
                        elif table == 'referentiel':
                            listCPTReferentiel.append((code, classeType, label))
                            id_CPT = int(query_commun.value(0))
                        else:
                            listCPTConfidentiel.append((code, classeType, label))
                            id_CPT = int(query_commun.value(0)) + utils.decalageCFD
                        listCPTDetails.append((id_CPT, code, code + '-details', classeType))
                # ceux des CPT perso :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'The titles of personal competence'), 
                    editLog=True)
                limiteBLT = self.main.limiteBLTPerso * (utils.NB_PERIODES - 1)
                listCPTBLTPerso = {}
                for matiereCode in admin.MATIERES['BLT']:
                    listCPTBLTPerso[matiereCode] = []
                    for i in range(limiteBLT):
                        cpt = utils_functions.formatPersoCpt(matiereCode, i + 1)
                        listCPTBLTPerso[matiereCode].append(cpt)
                # des lignes de commandes :
                commandLineBilans = utils_functions.u(
                    'SELECT value FROM bilans '
                    'WHERE id_eleve IN ({0}) AND name="{1}"')
                commandLinePersos = utils_functions.u(
                    'SELECT * FROM persos '
                    'WHERE id_eleve=? AND id_prof=? '
                    'AND Matiere=? '
                    'ORDER BY ordre')
                commandLineAppProf = utils_functions.u(
                    'SELECT "appreciation" FROM appreciations '
                    'WHERE id_eleve={0} AND Matiere="{1}" AND id_prof={2} AND Periode=999')
                commandLineApp = utils_functions.u(
                    'SELECT "appreciation", id_prof FROM appreciations '
                    'WHERE id_eleve={0} AND Matiere="{1}" AND Periode=999')
                # On fabrique la liste eleves :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'Making the list of students'), editLog=True)
                # un dictionnaire pour ne calculer les moyennes qu'une fois :
                moyennes = {'NOTES': {}, 'CLASSES': {}, 'COMPETENCES': {}}
                # récupération des adresses :
                if self.parent.data['withAddresses']:
                    id_students = utils_functions.array2string(self.parent.data['idsEleves'])
                    dicAdresses = loadDicAdresses(
                        query_admin, id_students, self.parent.data['addressesLines'])
                # pour chaque élève :
                for id_eleve in self.parent.data['idsEleves']:
                    # le premier dictionnaire contient les parties partagées :
                    eleveActuelCommun = {'id_eleve': id_eleve}
                    eleveActuelCommun['Photo'] = doPhoto(id_eleve)
                    # le deuxième dictionnaire contient les valeurs des évaluations :
                    eleveActuelCommunValeurs = {}
                    # le troisième dictionnaire contient les données liées aux matières :
                    eleveActuelMatiere = {}
                    # et un dernier pour les détails des bilans communs :
                    eleveActuelCommunDetails = {}
                    # on rempli le premier dictionnaire :
                    for champ in self.parent.data['SCHOOL_DATA']:
                        eleveActuelCommun[champ] = self.parent.data['SCHOOL_DATA'][champ]
                    classe = ''
                    commandLine = utils_db.q_selectAllFromWhere.format(
                        'eleves', 'id', id_eleve)
                    query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                    while query_admin.next():
                        eleveActuelCommun['INE'] = query_admin.value(1)
                        eleveActuelCommun['NOM'] = query_admin.value(2)
                        eleveActuelCommun['Prenom'] = query_admin.value(3)
                        eleveActuelCommun['NOM Prenom'] = query_admin.value(2) + ' ' + query_admin.value(3)
                        classe = query_admin.value(4)
                        eleveActuelCommun['Classe'] = classe
                        eleveActuelCommun['DateFichier'] = ''
                        dateNaissance = query_admin.value(6)
                        dateNaissance = QtCore.QDateTime().fromString(dateNaissance, 'ddMMyyyy')
                        dateNaissance = dateNaissance.toString('dd/MM/yyyy')
                        eleveActuelCommun['Date Naissance'] = dateNaissance
                        anDernier = query_admin.value(8)
                        if anDernier == None:
                            anDernier = ''
                        eleveActuelCommun['AnDernier'] = anDernier
                        if anDernier in dicAnDernier:
                            dicAnDernier[anDernier].append(id_eleve)
                        else:
                            dicAnDernier[anDernier] = [id_eleve]
                    eleveActuelCommun['LABEL DOCUMENT'] = self.parent.data['labelDocument']
                    if baseNomFichier != '':
                        eleveActuelCommun['NomFichier'] = utils_functions.u(
                            '{0}-{1}').format(id_eleve, baseNomFichier)
                    else:
                        eleveActuelCommun['NomFichier'] = utils_functions.u(id_eleve)
                    if self.parent.data['hideStudentNames']:
                        eleveActuelCommun['NOM'] = 'XXXX'
                        eleveActuelCommun['Prenom'] = 'xxxx'
                        eleveActuelCommun['NOM Prenom'] = 'XXXX xxxx'
                    if classe == '':
                        # on n'a rien trouvé pour cet élève, car il n'est pas dans
                        # la base resultats.
                        # on recherche le nom de l'élève dans la base users :
                        eleveNomComplet = ''
                        commandLine = utils_db.qs_users_elevesWhereId.format(id_eleve)
                        query_users = utils_db.queryExecute(commandLine, query=query_users)
                        while query_users.next():
                            eleveNomComplet = query_users.value(1)
                            eleveNomComplet += ' ' + query_users.value(2)
                        msg = QtWidgets.QApplication.translate('main', 'NO RESULTS')
                        msg = utils_functions.u('{0} {1} ({2})').format(
                            id_eleve, eleveNomComplet, msg)
                        listePourErreurs.append(msg)
                        continue
                    # on ajoute les adresses de l'élève :
                    if id_eleve in dicAdresses:
                        eleveActuelCommun['ADRESSES'] = dicAdresses[id_eleve]
                    else:
                        eleveActuelCommun['ADRESSES'] = []
                    # on récupère le type de la classe :
                    classeType = 0
                    commandLine_commun = utils_functions.u(
                        'SELECT * FROM classes WHERE Classe="{0}"').format(classe)
                    query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
                    if query_commun.first():
                        classeType = query_commun.value(2)
                    # liste des élèves de la classe :
                    if not(classe in moyennes['CLASSES']):
                        students = []
                        commandLine = utils_functions.u(
                            'SELECT id FROM eleves WHERE Classe="{0}"').format(classe)
                        query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                        while query_admin.next():
                            students.append(query_admin.value(0))
                        id_students = utils_functions.array2string(students)
                        moyennes['CLASSES'][classe] = id_students
                    # remplissage du dictionnaire eleveActuelCommunValeurs :
                    for (cpt, cptClassType, label) in listCPTBLTCommun:
                        eleveActuelCommun[cpt + '-Label'] = label
                        value = ''
                        if cptClassType in (classeType, -1):
                            commandLine = commandLineBilans.format(id_eleve, cpt)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            while query_recupEvals.next():
                                value += query_recupEvals.value(0)
                            value = utils_calculs.moyenneBilan(value)
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt, traiteAffichageValeur(
                                value, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt + '-B', utils_functions.valeur2affichage(value))
                        value = ''
                        if self.parent.data['classColumn']:
                            if (classe, cpt) in moyennes['COMPETENCES']:
                                value = moyennes['COMPETENCES'][(classe, cpt)]
                            else:
                                id_students = moyennes['CLASSES'][classe]
                                commandLine = commandLineBilans.format(id_students, cpt)
                                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                while query_recupEvals.next():
                                    value += query_recupEvals.value(0)
                                value = utils_calculs.moyenneBilan(value)
                        appendUniqueDic(
                            eleveActuelCommunValeurs, 
                            cpt + '-Classe', 
                            traiteAffichageValeur(value, classe=True, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            eleveActuelCommunValeurs, 
                            cpt + '-Classe-B', 
                            utils_functions.valeur2affichage(value))
                    for (cpt, cptClassType, label) in listCPTReferentiel:
                        eleveActuelCommun[cpt + '-Label'] = label
                        value = ''
                        if cptClassType in (classeType, -1):
                            commandLine = commandLineBilans.format(id_eleve, cpt)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            while query_recupEvals.next():
                                value += query_recupEvals.value(0)
                            value = utils_calculs.moyenneBilan(value)
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt, traiteAffichageValeur(
                                value, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt + '-B', utils_functions.valeur2affichage(value))
                    for (cpt, cptClassType, label) in listCPTConfidentiel:
                        eleveActuelCommun[cpt + '-Label'] = label
                        value = ''
                        if cptClassType in (classeType, -1):
                            commandLine = commandLineBilans.format(id_eleve, cpt)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            while query_recupEvals.next():
                                value += query_recupEvals.value(0)
                            value = utils_calculs.moyenneBilan(value)
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt, traiteAffichageValeur(
                                value, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt + '-B', utils_functions.valeur2affichage(value))
                    # on met les correspondances code-matière dans eleveActuelMatiere :
                    if not('Matieres' in eleveActuelMatiere):
                        eleveActuelMatiere['Matieres'] = {}
                    for matiereCode in admin.MATIERES['ALL']:
                        matiereName = admin.MATIERES['Code2Matiere'][matiereCode][1]
                        eleveActuelMatiere['Matieres'][matiereCode + '-Label'] = matiereName
                    # pour chaque matière, il faut vérifier le nombre de prof intervenant
                    for matiereCode in admin.MATIERES['BLT']:
                        # on récupère le nom du prof
                        eleveActuelMatiere[matiereCode] = {}
                        profWithoutData = []
                        commandLine = utils_functions.u(
                            'SELECT DISTINCT id_prof FROM eleve_prof '
                            'WHERE id_eleve={0} AND Matiere="{1}"').format(id_eleve, matiereCode)
                        query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                        while query_recupEvals.next():
                            id_prof = int(query_recupEvals.value(0))
                            eleveActuelMatiere[matiereCode][id_prof] = {}
                            commandLine = utils_db.q_selectAllFromWhere.format(
                                'profs', 'id', id_prof)
                            query_users = utils_db.queryExecute(commandLine, query=query_users)
                            while query_users.next():
                                profNom = doProfName(
                                    query_users.value(1), query_users.value(2))
                                eleveActuelMatiere[matiereCode][id_prof]['profNom'] = profNom
                        for id_prof in eleveActuelMatiere[matiereCode]:
                            # on remplit le dictionnaire eleveActuelMatiere
                            actualDic = eleveActuelMatiere[matiereCode][id_prof]
                            persos = {'NAMES': [], }
                            i = 0
                            query_recupEvals = utils_db.queryExecute(
                                {commandLinePersos: (id_eleve, id_prof, matiereCode)}, 
                                query=query_recupEvals)
                            while query_recupEvals.next():
                                name = query_recupEvals.value(3)
                                label = query_recupEvals.value(4)
                                value = query_recupEvals.value(5)
                                groupeValue = query_recupEvals.value(6)
                                if not(name in persos['NAMES']):
                                    persos['NAMES'].append(name)
                                    i += 1
                                    cpt = utils_functions.formatPersoCpt(matiereCode, i)
                                    persos[cpt] = [label, value, groupeValue]
                                else:
                                    cpt = utils_functions.formatPersoCpt(matiereCode, i)
                                    persos[cpt][1] += value
                                    persos[cpt][2] += groupeValue
                            for cpt in listCPTBLTPerso[matiereCode]:
                                label, value, groupeValue = '', '', ''
                                if cpt in persos:
                                    label = persos[cpt][0]
                                    value = utils_calculs.moyenneBilan(
                                        persos[cpt][1])
                                    if self.parent.data['classColumn']:
                                        groupeValue = utils_calculs.moyenneBilan(
                                            persos[cpt][2])
                                if not(cpt in actualDic) and (value != ''):
                                    actualDic[cpt + '-Label'] = label
                                    actualDic[cpt] = traiteAffichageValeur(
                                        value, colorsOnly=self.parent.data['colorsOnly'])
                                    actualDic[cpt + '-Classe'] = traiteAffichageValeur(
                                        groupeValue, classe=True, colorsOnly=self.parent.data['colorsOnly'])
                                    actualDic[cpt + '-B'] = utils_functions.valeur2affichage(value)
                                    actualDic[cpt + '-Classe-B'] = utils_functions.valeur2affichage(
                                        groupeValue)
                            # la note, moyenne, écart-type etc s'il y en a :
                            if id_eleve in admin.elevesNotes:
                                note, moyenne, ecart, mini, maxi = '', '', '', '', ''
                                id_groupe = -1
                                commandLine = utils_functions.u(
                                    'SELECT value '
                                    'FROM notes '
                                    'WHERE id_eleve={0} '
                                    'AND Matiere="{1}" '
                                    'AND id_prof={2} '
                                    'AND Periode=999').format(id_eleve, matiereCode, id_prof)
                                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                if query_recupEvals.first():
                                    note = float(query_recupEvals.value(0))
                                # on cherche maintenant la moyenne du groupe :
                                commandLine = utils_functions.u(
                                    'SELECT * FROM eleve_groupe '
                                    'WHERE id_eleve={0} '
                                    'AND Matiere="{1}" '
                                    'AND id_prof={2} '
                                    'AND Periode=999').format(id_eleve, matiereCode, id_prof)
                                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                if query_recupEvals.first():
                                    id_groupe = int(query_recupEvals.value(2))
                                    if (id_prof, matiereCode, id_groupe) in moyennes['NOTES']:
                                        (moyenne, ecart, mini, maxi) = moyennes['NOTES'][
                                            (id_prof, matiereCode, id_groupe)]
                                    else:
                                        notes = []
                                        commandLine = utils_functions.u(
                                            'SELECT notes.* FROM notes '
                                            'JOIN eleve_groupe ON ('
                                            'eleve_groupe.id_eleve=notes.id_eleve '
                                            'AND eleve_groupe.id_prof=notes.id_prof '
                                            'AND eleve_groupe.Matiere=notes.Matiere) '
                                            'WHERE notes.id_prof={0} '
                                            'AND notes.Matiere="{1}" '
                                            'AND notes.Periode=999 '
                                            'AND eleve_groupe.id_groupe={2} '
                                            'AND eleve_groupe.Periode=999').format(
                                                id_prof, matiereCode, id_groupe)
                                        query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                        while query_recupEvals.next():
                                            notes.append(int(query_recupEvals.value(4)))
                                        # on calcule les valeurs :
                                        moyenne = utils_calculs.average(notes)
                                        ecart = utils_calculs.standardDeviation(notes, moyenne)
                                        moyenne = round(moyenne, utils.precisionNotes)
                                        ecart = round(ecart, utils.precisionNotes)
                                        try:
                                            mini, maxi = utils_calculs.minMax(notes)
                                        except:
                                            pass
                                        moyennes['NOTES'][(id_prof, matiereCode, id_groupe)] = (
                                            moyenne, ecart, mini, maxi)
                                if note != '':
                                    notes = (note, moyenne, ecart, mini, maxi)
                                    actualDic['NOTES'] = notes
                            # l'appréciation dans cette matière pour ce prof:
                            appreciation = ''
                            commandLine = commandLineAppProf.format(id_eleve, matiereCode, id_prof)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            if query_recupEvals.first():
                                appreciation = query_recupEvals.value(0)
                            actualDic[matiereCode + '-appreciation'] = appreciation
                            # récupération du label de la matière si des valeurs existent :
                            if len(actualDic) > 0:
                                matiereLabel = admin.MATIERES['Code2Matiere'][matiereCode][2]
                                actualDic['matiereLabel'] = matiereLabel
                            else:
                                if not(id_prof in profWithoutData):
                                    profWithoutData.append(id_prof)
                        for id_prof in profWithoutData:
                            del eleveActuelMatiere[matiereCode][id_prof]
                    # on ajoute les matières spéciales (PP, Vie scolaire, ...)
                    # au dictionnaire eleveActuelCommun :
                    for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
                        eleveActuelCommun[matiereCode] = {}
                        actualDic = eleveActuelCommun[matiereCode]
                        if matiereCode == 'PP':
                            actualDic['Label'] = 'Observations du conseil de classe'
                            actualDic['Repere'] = 'PROF PRINCIPAL'
                        elif matiereCode == 'VS':
                            actualDic['Label'] = 'Vie scolaire'
                            actualDic['Repere'] = 'VIE SCOLAIRE'
                        else:
                            actualDic['Label'] = ''
                            actualDic['Repere'] = ''
                        # on récupère l'appréciation et le nom du prof :
                        appreciation, id_prof, profNom = '', -1, ''
                        commandLine = commandLineApp.format(id_eleve, matiereCode)
                        query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                        if query_recupEvals.first():
                            appreciation = query_recupEvals.value(0)
                            id_prof = int(query_recupEvals.value(1))
                        actualDic['appreciation'] = appreciation
                        eleveActuelCommun[matiereCode + '-appreciation'] = appreciation
                        # les évaluations :
                        persos = {'NAMES': [], 'COMPETENCES': [], }
                        i = 0
                        commandLinePersos2 = utils_functions.u(
                            'SELECT * FROM persos '
                            'WHERE id_eleve={0} AND Matiere="{1}" '
                            'ORDER BY ordre')
                        commandLine = commandLinePersos2.format(id_eleve, matiereCode)
                        query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                        while query_recupEvals.next():
                            name = query_recupEvals.value(3)
                            label = query_recupEvals.value(4)
                            value = query_recupEvals.value(5)
                            groupeValue = query_recupEvals.value(6)
                            if not(name in persos['NAMES']):
                                persos['NAMES'].append(name)
                                i += 1
                                cpt = utils_functions.formatPersoCpt(matiereCode, i)
                                persos[cpt] = [label, value, groupeValue]
                            else:
                                cpt = utils_functions.formatPersoCpt(matiereCode, i)
                                persos[cpt][1] += value
                                persos[cpt][2] += groupeValue
                        for cpt in persos['COMPETENCES']:
                            label, value, groupeValue = '', '', ''
                            if cpt in persos:
                                label = persos[cpt][0]
                                value = utils_calculs.moyenneBilan(
                                    persos[cpt][1])
                                if self.parent.data['classColumn']:
                                    groupeValue = utils_calculs.moyenneBilan(
                                        persos[cpt][2])
                            if not(cpt in actualDic) and (value != ''):
                                actualDic[cpt + '-Label'] = label
                                actualDic[cpt] = traiteAffichageValeur(
                                    value, colorsOnly=self.parent.data['colorsOnly'])
                                actualDic[cpt + '-Classe'] = traiteAffichageValeur(
                                    groupeValue, classe=True, colorsOnly=self.parent.data['colorsOnly'])
                                actualDic[cpt + '-B'] = utils_functions.valeur2affichage(value)
                                actualDic[cpt + '-Classe-B'] = utils_functions.valeur2affichage(
                                    groupeValue)
                        # la note, moyenne, écart-type etc s'il y en a :
                        if id_eleve in admin.elevesNotes:
                            note, moyenne, ecart, mini, maxi = '', '', '', '', ''
                            id_groupe = -1
                            commandLine = utils_functions.u(
                                'SELECT value '
                                'FROM notes '
                                'WHERE id_eleve={0} '
                                'AND Matiere="{1}" '
                                'AND id_prof={2} '
                                'AND Periode=999').format(id_eleve, matiereCode, id_prof)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            if query_recupEvals.first():
                                note = float(query_recupEvals.value(0))
                            # on cherche maintenant la moyenne du groupe :
                            commandLine = utils_functions.u(
                                'SELECT * FROM eleve_groupe '
                                'WHERE id_eleve={0} '
                                'AND Matiere="{1}" '
                                'AND id_prof={2} '
                                'AND Periode=999').format(id_eleve, matiereCode, id_prof)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            if query_recupEvals.first():
                                id_groupe = int(query_recupEvals.value(2))
                                if (id_prof, matiereCode, id_groupe) in moyennes['NOTES']:
                                    (moyenne, ecart, mini, maxi) = moyennes['NOTES'][
                                        (id_prof, matiereCode, id_groupe)]
                                else:
                                    notes = []
                                    commandLine = utils_functions.u(
                                        'SELECT notes.* FROM notes '
                                        'JOIN eleve_groupe ON ('
                                        'eleve_groupe.id_eleve=notes.id_eleve '
                                        'AND eleve_groupe.id_prof=notes.id_prof '
                                        'AND eleve_groupe.Matiere=notes.Matiere) '
                                        'WHERE notes.id_prof={0} '
                                        'AND notes.Matiere="{1}" '
                                        'AND notes.Periode=999 '
                                        'AND eleve_groupe.id_groupe={2} '
                                        'AND eleve_groupe.Periode=999').format(
                                            id_prof, matiereCode, id_groupe)
                                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                                    while query_recupEvals.next():
                                        notes.append(int(query_recupEvals.value(4)))
                                    # on calcule les valeurs :
                                    moyenne = utils_calculs.average(notes)
                                    ecart = utils_calculs.standardDeviation(notes, moyenne)
                                    moyenne = round(moyenne, utils.precisionNotes)
                                    ecart = round(ecart, utils.precisionNotes)
                                    try:
                                        mini, maxi = utils_calculs.minMax(notes)
                                    except:
                                        pass
                                    moyennes['NOTES'][(id_prof, matiereCode, id_groupe)] = (
                                        moyenne, ecart, mini, maxi)
                            if note != '':
                                notes = (note, moyenne, ecart, mini, maxi)
                                actualDic['NOTES'] = notes
                        # on récupère le nom du prof :
                        if id_prof > -1:
                            commandLine = 'SELECT NOM FROM profs WHERE id={0}'.format(id_prof)
                            query_users = utils_db.queryExecute(commandLine, query=query_users)
                            while query_users.next():
                                profNom = utils_functions.u(query_users.value(0))
                        actualDic['profNom'] = profNom
                    try:
                        classe = utils_functions.u(classe)
                    except:
                        # s'il y a une erreur, c'est que l'élève n'a pas été trouvé
                        message = QtWidgets.QApplication.translate(
                            'main', 
                            'No Data For: {0}').format(eleveActuelCommun['NOM Prenom'])
                        utils_functions.afficheMsgPb(self.main, message, withMessageBox=False)
                        continue
                    # on ajoute les absences et retards :
                    absences = [0, 0, 0, 0]
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-01')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of half-days of justified absence')
                    eleveActuelCommun['absences-0Label'] = longText
                    eleveActuelCommun['absences-0'] = 0
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-11')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of half-days of unjustified absence')
                    eleveActuelCommun['absences-1Label'] = longText
                    eleveActuelCommun['absences-1'] = 0
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-21')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of delays')
                    eleveActuelCommun['absences-2Label'] = longText
                    eleveActuelCommun['absences-2'] = 0
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-31')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of classroom hours missed')
                    eleveActuelCommun['absences-3Label'] = longText
                    eleveActuelCommun['absences-3'] = 0
                    commandLine = utils_db.q_selectAllFromWhere.format(
                        'absences', 'id_eleve', id_eleve)
                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                    while query_recupEvals.next():
                        absences[0] += int(query_recupEvals.value(3))
                        absences[1] += int(query_recupEvals.value(4))
                        absences[2] += int(query_recupEvals.value(5))
                        absences[3] += int(query_recupEvals.value(6))
                    for i in range(4):
                        if absences[i] > 0:
                            eleveActuelCommun['absences-{0}'.format(i)] = absences[i]

                    for cpt in listCPTDetails:
                        replaceWith = ''
                        code = cpt[2]
                        eleveActuelCommunValeurs[code] = ''
                    # chaque élément de la liste donneesEleves contient les dictionnaires :
                    donneesEleves.append(
                        (eleveActuelCommun, 
                        eleveActuelCommunValeurs, 
                        eleveActuelMatiere, 
                        eleveActuelCommunDetails))
            elif what == 'referentialReports':
                # on propose de télécharger la base referential_validations :
                espaces = '&nbsp;' * 20
                m0 = QtWidgets.QApplication.translate(
                    'main', 'Before use the following database:')
                m1 = '{0}<b>referential_validations</b>'.format(espaces)
                m2 = QtWidgets.QApplication.translate(
                    'main', 'You must download the latest version (which is the website).')
                m3 = QtWidgets.QApplication.translate(
                    'main', 'Would you download it now?')
                message = utils_functions.u(
                    '<p>{0}'
                    '<br/>{1}.</p>'
                    '<p>{2}</p>'
                    '<p><br/><b>{3}</b></p>').format(m0, m1, m2, m3)
                reponse = utils_functions.messageBox(
                    self.main, level='warning', message=message, buttons=['Yes', 'No'])
                if reponse == QtWidgets.QMessageBox.Yes:
                    admin.downloadDB(self.main, db='referential_validations', msgFin=False)
                # connexion aux bases de données :
                query_users = utils_db.query(self.main.db_users)
                admin.openDB(self.main, 'referential_propositions')
                query_propositions = utils_db.query(admin.db_referentialPropositions)
                admin.openDB(self.main, 'referential_validations')
                query_validations = utils_db.query(admin.db_referentialValidations)

                # les listes dont on a besoin pour générer les fichiers :
                listCPTReferentiel = []
                listCPTDetails = []
                dicAnDernier = {}
                dicAdresses = {}

                if self.parent.data['sorted']:
                    baseNomFichier = ''

                # on récupère les titres des CPT partagées :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'The titles of shared competence'), 
                    editLog=True)
                commandLine = 'SELECT * FROM {0}'# WHERE Competence!=""'
                for table in ('referentiel', ):
                    query_commun = utils_db.queryExecute(commandLine.format(table), query=query_commun)
                    while query_commun.next():
                        code = query_commun.value(1)
                        label = query_commun.value(5)
                        classeType = query_commun.value(11)
                        listCPTReferentiel.append((code, classeType, label))
                        id_CPT = int(query_commun.value(0))
                        listCPTDetails.append((id_CPT, code, code + '-details', classeType))
                # On fabrique la liste eleves :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'Making the list of students'), editLog=True)
                # récupération des évaluations :
                referentialData = {}
                id_students = utils_functions.array2string(self.parent.data['idsEleves'])
                commandLine = (
                    'SELECT * FROM bilans_propositions '
                    'WHERE id_eleve IN ({0})').format(id_students)
                query_propositions = utils_db.queryExecute(commandLine, query=query_propositions)
                while query_propositions.next():
                    id_eleve = int(query_propositions.value(0))
                    bilan_name = query_propositions.value(2)
                    value = query_propositions.value(3)
                    referentialData[(id_eleve, bilan_name)] = value
                commandLine = (
                    'SELECT * FROM bilans_validations '
                    'WHERE id_eleve IN ({0})').format(id_students)
                query_validations = utils_db.queryExecute(commandLine, query=query_validations)
                while query_validations.next():
                    id_eleve = int(query_validations.value(0))
                    bilan_name = query_validations.value(2)
                    value = query_validations.value(5)
                    referentialData[(id_eleve, bilan_name)] = value
                # récupération des adresses :
                if self.parent.data['withAddresses']:
                    dicAdresses = loadDicAdresses(
                        query_admin, id_students, self.parent.data['addressesLines'])
                # pour chaque élève :
                for id_eleve in self.parent.data['idsEleves']:
                    # le premier dictionnaire contient les parties partagées :
                    eleveActuelCommun = {'id_eleve': id_eleve}
                    eleveActuelCommun['Photo'] = doPhoto(id_eleve)
                    # le deuxième dictionnaire contient les valeurs des évaluations :
                    eleveActuelCommunValeurs = {}
                    # le troisième dictionnaire contient les données liées aux matières :
                    eleveActuelMatiere = {}
                    # et un dernier pour les détails des bilans communs :
                    eleveActuelCommunDetails = {}
                    # on rempli le premier dictionnaire :
                    for champ in self.parent.data['SCHOOL_DATA']:
                        eleveActuelCommun[champ] = self.parent.data['SCHOOL_DATA'][champ]
                    classe = ''
                    commandLine = utils_db.q_selectAllFromWhere.format(
                        'eleves', 'id', id_eleve)
                    query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                    while query_admin.next():
                        eleveActuelCommun['INE'] = query_admin.value(1)
                        eleveActuelCommun['NOM'] = query_admin.value(2)
                        eleveActuelCommun['Prenom'] = query_admin.value(3)
                        eleveActuelCommun['NOM Prenom'] = query_admin.value(2) + ' ' + query_admin.value(3)
                        classe = query_admin.value(4)
                        eleveActuelCommun['Classe'] = classe
                        eleveActuelCommun['DateFichier'] = ''
                        dateNaissance = query_admin.value(6)
                        dateNaissance = QtCore.QDateTime().fromString(dateNaissance, 'ddMMyyyy')
                        dateNaissance = dateNaissance.toString('dd/MM/yyyy')
                        eleveActuelCommun['Date Naissance'] = dateNaissance
                        anDernier = query_admin.value(8)
                        if anDernier == None:
                            anDernier = ''
                        eleveActuelCommun['AnDernier'] = anDernier
                        if anDernier in dicAnDernier:
                            dicAnDernier[anDernier].append(id_eleve)
                        else:
                            dicAnDernier[anDernier] = [id_eleve]
                    eleveActuelCommun['LABEL DOCUMENT'] = self.parent.data['labelDocument']
                    if baseNomFichier != '':
                        eleveActuelCommun['NomFichier'] = utils_functions.u(
                            '{0}-{1}').format(id_eleve, baseNomFichier)
                    else:
                        eleveActuelCommun['NomFichier'] = utils_functions.u(id_eleve)
                    if self.parent.data['hideStudentNames']:
                        eleveActuelCommun['NOM'] = 'XXXX'
                        eleveActuelCommun['Prenom'] = 'xxxx'
                        eleveActuelCommun['NOM Prenom'] = 'XXXX xxxx'
                    if classe == '':
                        # on n'a rien trouvé pour cet élève, car il n'est pas dans
                        # la base resultats.
                        # on recherche le nom de l'élève dans la base users :
                        eleveNomComplet = ''
                        commandLine = utils_db.qs_users_elevesWhereId.format(id_eleve)
                        query_users = utils_db.queryExecute(commandLine, query=query_users)
                        while query_users.next():
                            eleveNomComplet = query_users.value(1)
                            eleveNomComplet += ' ' + query_users.value(2)
                        msg = QtWidgets.QApplication.translate('main', 'NO RESULTS')
                        msg = utils_functions.u('{0} {1} ({2})').format(
                            id_eleve, eleveNomComplet, msg)
                        listePourErreurs.append(msg)
                        continue
                    # on ajoute les adresses de l'élève :
                    if id_eleve in dicAdresses:
                        eleveActuelCommun['ADRESSES'] = dicAdresses[id_eleve]
                    else:
                        eleveActuelCommun['ADRESSES'] = []
                    # on récupère le type de la classe :
                    classeType = 0
                    commandLine_commun = utils_functions.u(
                        'SELECT * FROM classes WHERE Classe="{0}"').format(classe)
                    query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
                    if query_commun.first():
                        classeType = query_commun.value(2)
                    # remplissage du dictionnaire eleveActuelCommunValeurs :
                    for (cpt, cptClassType, label) in listCPTReferentiel:
                        eleveActuelCommun[cpt + '-Label'] = label
                        value = ''
                        if cptClassType in (classeType, -1):
                            value = referentialData.get((id_eleve, cpt), '')
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt, traiteAffichageValeur(
                                value, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            eleveActuelCommunValeurs, cpt + '-B', utils_functions.valeur2affichage(value))
                    try:
                        classe = utils_functions.u(classe)
                    except:
                        # s'il y a une erreur, c'est que l'élève n'a pas été trouvé
                        message = QtWidgets.QApplication.translate(
                            'main', 
                            'No Data For: {0}').format(eleveActuelCommun['NOM Prenom'])
                        utils_functions.afficheMsgPb(self.main, message, withMessageBox=False)
                        continue
                    for cpt in listCPTDetails:
                        replaceWith = ''
                        code = cpt[2]
                        eleveActuelCommunValeurs[code] = ''
                    # chaque élément de la liste donneesEleves contient les dictionnaires :
                    donneesEleves.append(
                        (eleveActuelCommun, 
                        eleveActuelCommunValeurs, 
                        eleveActuelMatiere, 
                        eleveActuelCommunDetails))
            else:
                # bulletin ou relevé classique.
                # connexion aux bases de données :
                query_users = utils_db.query(self.main.db_users)
                if self.parent.data['period'] == utils.selectedPeriod:
                    admin.openDB(self.main, 'resultats')
                    query_resultats = utils_db.query(admin.db_resultats)
                else:
                    # on copie les bases resultats-x dans temp :
                    dbFileName = 'resultats-{0}.sqlite'.format(self.parent.data['period'])
                    dbFile = '{0}/protected/{1}'.format(
                        admin.dirLocalPrive, dbFileName)
                    if not(QtCore.QFileInfo(dbFile).exists()):
                        dbFile = '{0}/protected/resultats.sqlite'.format(admin.dirLocalPrive)
                    dbFileTemp = '{0}/{1}'.format(self.main.tempPath, dbFileName)
                    utils_filesdirs.removeAndCopy(dbFile, dbFileTemp)
                    (db_resultatsX, dbNameX) = utils_db.createConnection(
                        self.main, dbFileTemp)
                    query_resultats = utils_db.query(db_resultatsX)

                if self.parent.data['withDetails']:
                    admin.openDB(self.main, 'recup_evals')
                    query_recupEvals = utils_db.query(admin.db_recupEvals)
                    import utils_calculs

                # les listes dont on a besoin pour générer les fichiers :
                listCPTBLTCommun = []
                listCPTReferentiel = []
                listCPTConfidentiel = []
                listCPTDetails = []
                dicAnDernier = {}
                dicAdresses = {}

                if self.parent.data['sorted']:
                    baseNomFichier = ''

                # on récupère les titres des CPT partagées :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'The titles of shared competence'), 
                    editLog=True)
                commandLine = 'SELECT * FROM {0} WHERE Competence!=""'
                for table in ('bulletin', 'referentiel', 'confidentiel'):
                    query_commun = utils_db.queryExecute(commandLine.format(table), query=query_commun)
                    while query_commun.next():
                        code = query_commun.value(1)
                        label = query_commun.value(5)
                        classeType = query_commun.value(11)
                        if table == 'bulletin':
                            listCPTBLTCommun.append((code, classeType, label))
                            id_CPT = int(query_commun.value(0)) + utils.decalageBLT
                        elif table == 'referentiel':
                            listCPTReferentiel.append((code, classeType, label))
                            id_CPT = int(query_commun.value(0))
                        else:
                            listCPTConfidentiel.append((code, classeType, label))
                            id_CPT = int(query_commun.value(0)) + utils.decalageCFD
                        listCPTDetails.append((id_CPT, code, code + '-details', classeType))
                # ceux des CPT perso :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'The titles of personal competence'), 
                    editLog=True)
                limiteBLT = self.main.limiteBLTPerso
                listCPTBLTPerso = {}
                for matiereCode in admin.MATIERES['BLT']:
                    listCPTBLTPerso[matiereCode] = []
                    for i in range(limiteBLT):
                        if i < 9:
                            listCPTBLTPerso[matiereCode].append(
                                utils_functions.u(
                                    '{0}-0{1}').format(matiereCode, i + 1))
                        else:
                            listCPTBLTPerso[matiereCode].append(
                                utils_functions.u(
                                    '{0}-{1}').format(matiereCode, i + 1))
                # On fabrique la liste eleves :
                utils_functions.afficheMessage(
                    self.main, 
                    QtWidgets.QApplication.translate(
                        'main', 'Making the list of students'), editLog=True)
                id_students = utils_functions.array2string(self.parent.data['idsEleves'])
                # récupération des adresses :
                if self.parent.data['withAddresses']:
                    dicAdresses = loadDicAdresses(
                        query_admin, id_students, self.parent.data['addressesLines'])

                elevesCommun = {}
                elevesCommunValeurs = {}
                elevesMatiere = {}
                elevesCommunDetails = {}
                for id_eleve in self.parent.data['idsEleves']:
                    elevesCommun[id_eleve] = {'id_eleve': id_eleve}
                    elevesCommun[id_eleve]['Photo'] = doPhoto(id_eleve)
                    elevesCommunValeurs[id_eleve] = {}
                    elevesMatiere[id_eleve] = {}
                    elevesCommunDetails[id_eleve] = {}
                    for champ in self.parent.data['SCHOOL_DATA']:
                        elevesCommun[id_eleve][champ] = self.parent.data['SCHOOL_DATA'][champ]

                commandLine = utils_db.q_selectAllFromWhere.format(
                    'eleves', 'id', id_students)
                query_admin = utils_db.queryExecute(commandLine, query=query_admin)
                while query_admin.next():
                    id_eleve = int(query_admin.value(0))
                    elevesCommun[id_eleve]['INE'] = query_admin.value(1)
                    elevesCommun[id_eleve]['NOM'] = query_admin.value(2)
                    elevesCommun[id_eleve]['Prenom'] = query_admin.value(3)
                    elevesCommun[id_eleve]['NOM Prenom'] = query_admin.value(2) + ' ' + query_admin.value(3)
                    elevesCommun[id_eleve]['Classe'] = query_admin.value(4)
                    elevesCommun[id_eleve]['DateFichier'] = ''
                    dateNaissance = query_admin.value(6)
                    dateNaissance = QtCore.QDateTime().fromString(dateNaissance, 'ddMMyyyy')
                    dateNaissance = dateNaissance.toString('dd/MM/yyyy')
                    elevesCommun[id_eleve]['Date Naissance'] = dateNaissance
                    anDernier = query_admin.value(8)
                    if anDernier == None:
                        anDernier = ''
                    elevesCommun[id_eleve]['AnDernier'] = anDernier
                    if anDernier in dicAnDernier:
                        dicAnDernier[anDernier].append(id_eleve)
                    else:
                        dicAnDernier[anDernier] = [id_eleve]
                    elevesCommun[id_eleve]['LABEL DOCUMENT'] = self.parent.data['labelDocument']
                    if baseNomFichier != '':
                        elevesCommun[id_eleve]['NomFichier'] = utils_functions.u(
                            '{0}-{1}').format(id_eleve, baseNomFichier)
                    else:
                        elevesCommun[id_eleve]['NomFichier'] = utils_functions.u(id_eleve)
                commandLine = utils_db.q_selectAllFromWhere.format(
                    'eleves', 'id_eleve', id_students)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_eleve = int(query_resultats.value(0))
                    elevesCommun[id_eleve]['DateFichier'] = query_resultats.value(1)
                    elevesCommun[id_eleve]['NOM Prenom'] = query_resultats.value(3)
                    elevesCommun[id_eleve]['Date Naissance'] = query_resultats.value(4)
                if self.parent.data['hideStudentNames']:
                    for id_eleve in self.parent.data['idsEleves']:
                        elevesCommun[id_eleve]['NOM'] = 'XXXX'
                        elevesCommun[id_eleve]['Prenom'] = 'xxxx'
                        elevesCommun[id_eleve]['NOM Prenom'] = 'XXXX xxxx'

                for id_eleve in self.parent.data['idsEleves']:
                    # on ajoute les adresses de l'élève :
                    elevesCommun[id_eleve]['ADRESSES'] = dicAdresses.get(id_eleve, [])
                    # on vérifie encore la classe :
                    classe = elevesCommun[id_eleve].get('Classe', '')
                    if classe == '':
                        # on n'a rien trouvé pour cet élève, car il n'est pas dans
                        # la base resultats.
                        # on recherche le nom de l'élève dans la base users :
                        eleveNomComplet = ''
                        commandLine = utils_db.qs_users_elevesWhereId.format(id_eleve)
                        query_users = utils_db.queryExecute(commandLine, query=query_users)
                        while query_users.next():
                            eleveNomComplet = query_users.value(1)
                            eleveNomComplet += ' ' + query_users.value(2)
                        msg = QtWidgets.QApplication.translate('main', 'NO RESULTS')
                        msg = utils_functions.u('{0} {1} ({2})').format(
                            id_eleve, eleveNomComplet, msg)
                        listePourErreurs.append(msg)
                        continue
                    # on récupère l'id_classe de la classe de l'élève :
                    elevesCommun[id_eleve]['id_classe'] = 0
                    commandLine = utils_functions.u(
                        'SELECT "id_eleve" '
                        'FROM eleves WHERE id_eleve<0 AND Classe="{0}"').format(classe)
                    query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                    while query_resultats.next():
                        elevesCommun[id_eleve]['id_classe'] = int(query_resultats.value(0))
                    # on récupère le type de la classe :
                    elevesCommun[id_eleve]['classeType'] = 0
                    commandLine_commun = utils_functions.u(
                        'SELECT * FROM classes WHERE Classe="{0}"').format(classe)
                    query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
                    if query_commun.first():
                        elevesCommun[id_eleve]['classeType'] = query_commun.value(2)

                tempDic = {}
                commandLine = utils_db.q_selectAllFromWhere.format(
                    'bilans', 'id_eleve', id_students)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_eleve = int(query_resultats.value(0))
                    cpt = query_resultats.value(1)
                    value = query_resultats.value(2)
                    if not(id_eleve in tempDic):
                        tempDic[id_eleve] = {}
                    tempDic[id_eleve][cpt] = value
                commandLine = 'SELECT * FROM bilans WHERE id_eleve<0'
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_classe = int(query_resultats.value(0))
                    cpt = query_resultats.value(1)
                    value = query_resultats.value(2)
                    if not(id_classe in tempDic):
                        tempDic[id_classe] = {}
                    tempDic[id_classe][cpt] = value
                # pour chaque élève :
                for id_eleve in self.parent.data['idsEleves']:
                    id_classe = elevesCommun[id_eleve]['id_classe']
                    classeType = elevesCommun[id_eleve]['classeType']
                    # remplissage du dictionnaire elevesCommunValeurs :
                    for (cpt, cptClassType, label) in listCPTBLTCommun:
                        elevesCommun[id_eleve][cpt + '-Label'] = label
                        value = ''
                        if cptClassType in (classeType, -1):
                            try:
                                value = tempDic[id_eleve][cpt]
                            except:
                                pass
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], cpt, traiteAffichageValeur(
                                value, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], cpt + '-B', utils_functions.valeur2affichage(value))
                        value = ''
                        if self.parent.data['classColumn']:
                            if (id_classe < 0) and (cptClassType in (classeType, -1)):
                                try:
                                    value = tempDic[id_classe][cpt]
                                except:
                                    pass
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], 
                            cpt + '-Classe', 
                            traiteAffichageValeur(value, classe=True, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], 
                            cpt + '-Classe-B', 
                            utils_functions.valeur2affichage(value))
                    for (cpt, cptClassType, label) in listCPTReferentiel:
                        elevesCommun[id_eleve][cpt + '-Label'] = label
                        value = ''
                        if cptClassType in (classeType, -1):
                            try:
                                value = tempDic[id_eleve][cpt]
                            except:
                                pass
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], cpt, traiteAffichageValeur(
                                value, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], cpt + '-B', utils_functions.valeur2affichage(value))
                    for (cpt, cptClassType, label) in listCPTConfidentiel:
                        elevesCommun[id_eleve][cpt + '-Label'] = label
                        value = ''
                        if cptClassType in (classeType, -1):
                            try:
                                value = tempDic[id_eleve][cpt]
                            except:
                                pass
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], cpt, traiteAffichageValeur(
                                value, colorsOnly=self.parent.data['colorsOnly']))
                        appendUniqueDic(
                            elevesCommunValeurs[id_eleve], cpt + '-B', utils_functions.valeur2affichage(value))
                # on ajoute les 8 composantes du LSU :
                commandLineLSU = (
                    'SELECT * FROM lsu '
                    'WHERE id_eleve IN ({0}) AND lsuWhat="socle"')
                commandLine = commandLineLSU.format(id_students)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_eleve = int(query_resultats.value(1))
                    cpt = query_resultats.value(2)
                    value = query_resultats.value(3)
                    appendUniqueDic(
                        elevesCommunValeurs[id_eleve], 'LSU_' + cpt, traiteAffichageValeur(
                            value, colorsOnly=self.parent.data['colorsOnly']))
                    appendUniqueDic(
                        elevesCommunValeurs[id_eleve], 'LSU_' + cpt + '-B', utils_functions.valeur2affichage(value))

                # remplissage du dictionnaire elevesCommunDetails :
                commandLine = utils_db.q_selectAllFromWhere.format(
                    'bilans_details', 'id_eleve', id_students)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_eleve = int(query_resultats.value(0))
                    name = query_resultats.value(1)
                    id_prof = int(query_resultats.value(2))
                    matiereCode = query_resultats.value(3)
                    value = query_resultats.value(4)
                    title = utils_functions.u('{0}-{1}').format(name, matiereCode)
                    elevesCommunDetails[id_eleve][title] = traiteAffichageValeur(
                        value, colorsOnly=self.parent.data['colorsOnly'])
                    elevesCommunDetails[id_eleve][title + "-B"] = utils_functions.valeur2affichage(value)

                tempDic = {}
                for id_eleve in self.parent.data['idsEleves']:
                    tempDic[id_eleve] = {
                        'appreciations': {}, 
                        'persos': {}, 
                        'profs': {}, 
                        }
                commandLine = utils_db.q_selectAllFromWhere.format(
                    'appreciations', 'id_eleve', id_students)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_eleve = int(query_resultats.value(0))
                    matiereCode = query_resultats.value(1)
                    appreciation = query_resultats.value(2)
                    id_prof = int(query_resultats.value(3))
                    if not(matiereCode in tempDic[id_eleve]['appreciations']):
                        tempDic[id_eleve]['appreciations'][matiereCode] = {}
                    tempDic[id_eleve]['appreciations'][matiereCode][id_prof] = appreciation
                commandLine = utils_db.q_selectAllFromWhere.format(
                    'persos', 'id_eleve', id_students)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_eleve = int(query_resultats.value(0))
                    name = query_resultats.value(1)
                    label = query_resultats.value(2)
                    value = query_resultats.value(3)
                    groupeValue = query_resultats.value(4)
                    id_prof = int(query_resultats.value(5))
                    if not(id_prof in tempDic[id_eleve]['persos']):
                        tempDic[id_eleve]['persos'][id_prof] = {}
                    tempDic[id_eleve]['persos'][id_prof][name] = (label, value, groupeValue)
                commandLine = utils_db.q_selectAllFromWhere.format(
                    'eleve_prof', 'id_eleve', id_students)
                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                while query_resultats.next():
                    id_prof = int(query_resultats.value(0))
                    id_eleve = int(query_resultats.value(1))
                    matiereCode = query_resultats.value(2)
                    if not(matiereCode in tempDic[id_eleve]['profs']):
                        tempDic[id_eleve]['profs'][matiereCode] = []
                    if not(id_prof in tempDic[id_eleve]['profs'][matiereCode]):
                        tempDic[id_eleve]['profs'][matiereCode].append(id_prof)

                for id_eleve in self.parent.data['idsEleves']:
                    # on met les correspondances code-matière dans elevesMatiere :
                    if not('Matieres' in elevesMatiere[id_eleve]):
                        elevesMatiere[id_eleve]['Matieres'] = {}
                    for matiereCode in admin.MATIERES['ALL']:
                        matiereName = admin.MATIERES['Code2Matiere'][matiereCode][1]
                        elevesMatiere[id_eleve]['Matieres'][matiereCode + '-Label'] = matiereName
                    # pour chaque matière, il faut vérifier le nombre de prof intervenant
                    for matiereCode in admin.MATIERES['BLT']:
                        # on récupère le nom du prof
                        elevesMatiere[id_eleve][matiereCode] = {}
                        profWithoutData = []
                        try:
                            profs = tempDic[id_eleve]['profs'][matiereCode]
                        except:
                            profs = []
                        for id_prof in profs:
                            elevesMatiere[id_eleve][matiereCode][id_prof] = {}
                            commandLine = utils_db.q_selectAllFromWhere.format(
                                'profs', 'id', id_prof)
                            query_users = utils_db.queryExecute(commandLine, query=query_users)
                            while query_users.next():
                                profNom = doProfName(
                                    query_users.value(1), query_users.value(2))
                                elevesMatiere[id_eleve][matiereCode][id_prof]['profNom'] = profNom
                        for id_prof in elevesMatiere[id_eleve][matiereCode]:
                            # on remplit le dictionnaire elevesMatiere
                            actualDic = elevesMatiere[id_eleve][matiereCode][id_prof]
                            for cpt in listCPTBLTPerso[matiereCode]:
                                finded = False
                                label, value, groupeValue = '', '', ''
                                try:
                                    (label, value, groupeValue) = tempDic[id_eleve]['persos'][id_prof][cpt]
                                    finded = True
                                except:
                                    pass
                                if not(finded):
                                    cptEX = utils_functions.u('{0}{1}').format(cpt.split('-')[0], int(cpt.split('-')[1]))
                                    try:
                                        (label, value, groupeValue) = tempDic[id_eleve]['persos'][id_prof][cptEX]
                                    except:
                                        pass
                                if not(self.parent.data['classColumn']):
                                    groupeValue = ''
                                if not(cpt in actualDic) and (value != ''):
                                    if self.parent.data['withDetails']:
                                        label = addDetails(
                                            self.main, cpt, label, id_prof, matiereCode, id_eleve, colorsOnly=self.parent.data['colorsOnly'])
                                    actualDic[cpt + '-Label'] = label
                                    actualDic[cpt] = traiteAffichageValeur(
                                        value, colorsOnly=self.parent.data['colorsOnly'])
                                    actualDic[cpt + '-Classe'] = traiteAffichageValeur(
                                        groupeValue, classe=True, colorsOnly=self.parent.data['colorsOnly'])
                                    actualDic[cpt + '-B'] = utils_functions.valeur2affichage(value)
                                    actualDic[cpt + '-Classe-B'] = utils_functions.valeur2affichage(groupeValue)
                            # la note, moyenne, écart-type etc s'il y en a :
                            if id_eleve in admin.elevesNotes:
                                note, moyenne, ecart, mini, maxi = '', '', '', '', ''
                                commandLineNoteProf = utils_functions.u(
                                    'SELECT notes.value,  notes_classes.* '
                                    'FROM notes '
                                    'JOIN notes_classes ON ('
                                    'notes_classes.id_classe=notes.id_classe '
                                    'AND notes_classes.matiere=notes.matiere '
                                    'AND notes_classes.id_prof=notes.id_prof) '
                                    'WHERE notes.id_eleve={0} '
                                    'AND notes.matiere="{1}" '
                                    'AND notes.id_prof={2}')
                                commandLine = commandLineNoteProf.format(id_eleve, matiereCode, id_prof)
                                query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                                if query_resultats.first():
                                    note = float(query_resultats.value(0))
                                    moyenne = float(query_resultats.value(3))
                                    ecart = float(query_resultats.value(4))
                                    mini = float(query_resultats.value(5))
                                    maxi = float(query_resultats.value(6))
                                if note != '':
                                    notes = (note, moyenne, ecart, mini, maxi)
                                    actualDic['NOTES'] = notes
                            # l'appréciation dans cette matière pour ce prof:
                            appreciation = ''
                            try:
                                appreciation = tempDic[id_eleve]['appreciations'][matiereCode][id_prof]
                            except:
                                pass
                            actualDic[matiereCode + '-appreciation'] = appreciation
                            # récupération du label de la matière si des valeurs existent :
                            if len(actualDic) > 0:
                                matiereLabel = admin.MATIERES['Code2Matiere'][matiereCode][2]
                                actualDic['matiereLabel'] = matiereLabel
                            else:
                                if not(id_prof in profWithoutData):
                                    profWithoutData.append(id_prof)
                        for id_prof in profWithoutData:
                            del elevesMatiere[id_eleve][matiereCode][id_prof]
                    # on ajoute les matières spéciales (PP, Vie scolaire, ...)
                    # au dictionnaire elevesCommun :
                    for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
                        elevesCommun[id_eleve][matiereCode] = {}
                        if matiereCode == 'PP':
                            elevesCommun[id_eleve][matiereCode]['Label'] = 'Observations du conseil de classe'
                            elevesCommun[id_eleve][matiereCode]['Repere'] = 'PROF PRINCIPAL'
                        elif matiereCode == 'VS':
                            elevesCommun[id_eleve][matiereCode]['Label'] = 'Vie scolaire'
                            elevesCommun[id_eleve][matiereCode]['Repere'] = 'VIE SCOLAIRE'
                        else:
                            elevesCommun[id_eleve][matiereCode]['Label'] = ''
                            elevesCommun[id_eleve][matiereCode]['Repere'] = ''
                        # on récupère l'appréciation et le nom du prof :
                        appreciation, id_prof, profNom = '', -1, ''
                        try:
                            id_prof = tempDic[id_eleve]['profs'][matiereCode][0]
                        except:
                            pass
                        try:
                            for id_prof2 in tempDic[id_eleve]['appreciations'][matiereCode]:
                                appreciation = tempDic[id_eleve]['appreciations'][matiereCode][id_prof2]
                                if id_prof < 0:
                                    id_prof = id_prof2
                        except:
                            pass
                        elevesCommun[id_eleve][matiereCode]['appreciation'] = appreciation
                        elevesCommun[id_eleve][matiereCode + '-appreciation'] = appreciation
                        # les évaluations :
                        for i in range(limiteBLT):
                            finded = False
                            label, value, groupeValue = '', '', ''
                            if i < 9:
                                cpt = utils_functions.u(
                                    '{0}-0{1}').format(matiereCode, i + 1)
                            else:
                                cpt = utils_functions.u(
                                    '{0}-{1}').format(matiereCode, i + 1)
                            try:
                                (label, value, groupeValue) = tempDic[id_eleve]['persos'][id_prof][cpt]
                                finded = True
                            except:
                                pass
                            if not(finded):
                                cptEX = utils_functions.u('{0}{1}').format(matiereCode, i + 1)
                                try:
                                    (label, value, groupeValue) = tempDic[id_eleve]['persos'][id_prof][cptEX]
                                except:
                                    pass
                            if not(self.parent.data['classColumn']):
                                groupeValue = ''
                            if not(cpt in elevesCommun[id_eleve][matiereCode]) and (value != ''):
                                elevesCommun[id_eleve][matiereCode][cpt + '-Label'] = label
                                elevesCommun[id_eleve][matiereCode][cpt] = traiteAffichageValeur(
                                    value, colorsOnly=self.parent.data['colorsOnly'])
                                elevesCommun[id_eleve][matiereCode][cpt + '-Classe'] = traiteAffichageValeur(
                                    groupeValue, classe=True, colorsOnly=self.parent.data['colorsOnly'])
                                elevesCommun[id_eleve][matiereCode][cpt + '-B'] = utils_functions.valeur2affichage(value)
                                elevesCommun[id_eleve][matiereCode][cpt + '-Classe-B'] = utils_functions.valeur2affichage(groupeValue)
                        # la note, moyenne, écart-type etc s'il y en a :
                        if id_eleve in admin.elevesNotes:
                            note, moyenne, ecart, mini, maxi = '', '', '', '', ''
                            commandLineNoteProf = utils_functions.u(
                                'SELECT notes.value,  notes_classes.* '
                                'FROM notes '
                                'JOIN notes_classes ON ('
                                'notes_classes.id_classe=notes.id_classe '
                                'AND notes_classes.matiere=notes.matiere '
                                'AND notes_classes.id_prof=notes.id_prof) '
                                'WHERE notes.id_eleve={0} '
                                'AND notes.matiere="{1}"')
                            commandLine = commandLineNoteProf.format(id_eleve, matiereCode)
                            query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                            if query_resultats.first():
                                note = float(query_resultats.value(0))
                                moyenne = float(query_resultats.value(3))
                                ecart = float(query_resultats.value(4))
                                mini = float(query_resultats.value(5))
                                maxi = float(query_resultats.value(6))
                                if id_prof < 0:
                                    id_prof = int(query_resultats.value(7))
                            if note != '':
                                notes = (note, moyenne, ecart, mini, maxi)
                                elevesCommun[id_eleve][matiereCode]['NOTES'] = notes
                        # on récupère le nom du prof :
                        if id_prof > -1:
                            commandLine = 'SELECT NOM FROM profs WHERE id={0}'.format(id_prof)
                            query_users = utils_db.queryExecute(commandLine, query=query_users)
                            while query_users.next():
                                profNom = utils_functions.u(query_users.value(0))
                        elevesCommun[id_eleve][matiereCode]['profNom'] = profNom
                    try:
                        classe = utils_functions.u(elevesCommun[id_eleve]['Classe'])
                    except:
                        # s'il y a une erreur, c'est que l'élève n'a pas été trouvé
                        message = QtWidgets.QApplication.translate(
                            'main', 
                            'No Data For: {0}').format(elevesCommun[id_eleve]['NOM Prenom'])
                        utils_functions.afficheMsgPb(self.main, message, withMessageBox=False)
                        continue
                    # on ajoute les absences et retards :
                    absences = [0, 0, 0, 0]
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-01')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of half-days of justified absence')
                    elevesCommun[id_eleve]['absences-0Label'] = longText
                    elevesCommun[id_eleve]['absences-0'] = 0
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-11')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of half-days of unjustified absence')
                    elevesCommun[id_eleve]['absences-1Label'] = longText
                    elevesCommun[id_eleve]['absences-1'] = 0
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-21')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of delays')
                    elevesCommun[id_eleve]['absences-2Label'] = longText
                    elevesCommun[id_eleve]['absences-2'] = 0
                    longText = utils_db.readInConfigTable(self.main.db_commun, 'absences-31')[1]
                    if longText == '':
                        longText = QtWidgets.QApplication.translate(
                            'main', 'Number of classroom hours missed')
                    elevesCommun[id_eleve]['absences-3Label'] = longText
                    elevesCommun[id_eleve]['absences-3'] = 0
                    commandLine = utils_db.q_selectAllFromWhere.format(
                        'absences', 'id_eleve', id_eleve)
                    query_resultats = utils_db.queryExecute(commandLine, query=query_resultats)
                    if query_resultats.first():
                        absences[0] = int(query_resultats.value(1))
                        absences[1] = int(query_resultats.value(2))
                        absences[2] = int(query_resultats.value(3))
                        absences[3] = int(query_resultats.value(4))
                    for i in range(4):
                        if absences[i] > 0:
                            elevesCommun[id_eleve]['absences-{0}'.format(i)] = absences[i]
                    for cpt in listCPTDetails:
                        replaceWith = ''
                        if self.parent.data['withDetails']:
                            id_CPT = cpt[0]
                            commandLine = utils_functions.u(
                                'SELECT DISTINCT details_links.itemLabel, '
                                'details_values.value '
                                'FROM details_values '
                                'JOIN details_links '
                                'ON details_links.id_item=details_values.id_item '
                                'AND details_links.id_prof=details_values.id_prof '
                                'WHERE details_values.id_eleve={0} '
                                'AND details_links.id_competence={1}').format(id_eleve, id_CPT)
                            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                            replaceWith = utils_functions.u('\
                                <br/><table align=right width=600 cellpadding=2 cellspacing=0 border="1">')
                            nothing = True
                            while query_recupEvals.next():
                                itemLabel = query_recupEvals.value(0)
                                value = query_recupEvals.value(1)
                                value = utils_calculs.calculBilan(value, admin=True)
                                value = traiteAffichageValeur(value, colorsOnly=self.parent.data['colorsOnly'])
                                if value != '':
                                    nothing = False
                                    replaceWith = replaceWith + utils_functions.u('\
                                            <tr>\
                                                <td>{0}</td>\
                                                {1}\
                                            </tr>').format(itemLabel, value)
                            replaceWith = replaceWith + utils_functions.u('\
                                </table>')
                            if nothing:
                                replaceWith = ''
                            code = cpt[2]
                            elevesCommunValeurs[id_eleve][code] = replaceWith
                        else:
                            code = cpt[2]
                            elevesCommunValeurs[id_eleve][code] = ''

                for id_eleve in self.parent.data['idsEleves']:
                    # chaque élément de la liste donneesEleves contient les dictionnaires :
                    donneesEleves.append(
                        (elevesCommun[id_eleve], 
                        elevesCommunValeurs[id_eleve], 
                        elevesMatiere[id_eleve], 
                        elevesCommunDetails[id_eleve]))

                # on ferme la base resultats-x :
                if self.parent.data['period'] < utils.selectedPeriod:
                    if query_resultats != None:
                        query_resultats.clear()
                        del query_resultats
                    db_resultatsX.close()
                    del db_resultatsX
                    utils_db.sqlDatabase.removeDatabase(dbNameX)

            # on passe à la création des fichiers :
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate('main', 'Creating files:'), 
                editLog=True, 
                p=True)
            # liste des fichiers pdf créés :
            pdfFiles = {'WITH_ADDRESSES': [], 'WITHOUT_ADDRESSES': []}
            # pour l'affichage :
            nbEleves = len(donneesEleves)
            numEleve = 0
            # on traite chaque élève :
            for eleveActuel in donneesEleves:
                numEleve = numEleve + 1
                data = (template, eleveActuel, numEleve, nbEleves)
                createStudentFileFromHtmlTemplate(
                    self.main, 
                    data, 
                    listePourErreurs, 
                    pdfFiles, 
                    self.parent.data['orientation'], 
                    deleteNotEvaluatedLines=self.parent.data['deleteNotEvaluatedLines'], 
                    classColumn=self.parent.data['classColumn'], 
                    hideStudentNames=self.parent.data['hideStudentNames'], 
                    what=what, 
                    notanet=self.parent.data['notanet'], 
                    withAddresses=self.parent.data['withAddresses'])

            # finalisation :
            if not(self.parent.data['sorted']):
                if self.parent.data['mustUpdateAndMove']:
                    documentsEleves = []
                    for eleveActuel in donneesEleves:
                        documentEleve = []
                        eleveActuelCommun = eleveActuel[0]
                        documentEleve.append(eleveActuelCommun['id_eleve'])
                        documentEleve.append(eleveActuelCommun['NomFichier'] + '.pdf')
                        documentEleve.append(self.parent.data['labelDocument'])
                        # pour un document personnel, type vaut 1 :
                        documentEleve.append(1)
                        documentsEleves.append(documentEleve)
                    admin_docs.addDocumentsEleves(self.main, documentsEleves)

            # création du fichier pdf de tous :
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate(
                    'main', 'Create the class file'), 
                editLog=True, 
                p=True)
            pdfDir = admin.adminDir + '/fichiers/pdf/'

            if what == 'DNB':
                try:
                    PYTHONVERSION = utils.PYTHONVERSION
                    from PyPDF2 import PdfFileMerger, PdfFileReader
                    QtCore.QDir.setCurrent(admin.modelesDir)
                    if PYTHONVERSION >= 30:
                        blankPagePdf = open(self.parent.data['blankPagePdfName'], 'rb')
                    else:
                        blankPagePdf = file(self.parent.data['blankPagePdfName'], 'rb')
                    QtCore.QDir.setCurrent(pdfDir)
                    classFileName = utils_functions.u(
                        '{0}-{1}.pdf').format(self.parent.data['classPrefix'], baseNomFichier)
                    if PYTHONVERSION >= 30:
                        outputPdfStream = open(classFileName, 'wb')
                    else:
                        outputPdfStream = file(classFileName, 'wb')
                    merger = PdfFileMerger(strict=False)
                    # on récupère les fichiers pour pouvoir les refermer :
                    files = []
                    for pdfFile in pdfFiles['WITHOUT_ADDRESSES']:
                        if PYTHONVERSION >= 30:
                            f = open(pdfFile, 'rb')
                        else:
                            f = file(pdfFile, 'rb')
                        files.append(f)
                        merger.append(f, import_bookmarks=False)
                        inputPdf = PdfFileReader(f)
                        nbPages = inputPdf.getNumPages()
                        del inputPdf
                        # si nbPages est impair, on fait un saut de page
                        # (utile pour l'impression) :
                        if self.parent.data['printOn2Pages']:
                            if (nbPages % 2 != 0):
                                merger.append(blankPagePdf, import_bookmarks=False)
                        elif self.parent.data['printOn4Pages']:
                            manque = 4 - nbPages % 4
                            if manque != 4:
                                for i in range(manque):
                                    merger.append(blankPagePdf, import_bookmarks=False)
                    merger.write(outputPdfStream)
                    # on referme proprement les fichiers :
                    outputPdfStream.close()
                    for f in files:
                        f.close()
                    blankPagePdf.close()
                finally:
                    QtCore.QDir.setCurrent(self.main.beginDir)

                # on crée aussi le fichier d'export NOTANET :
                notanetFileName = utils_functions.u(
                    '{0}/fichiers/notanet.txt').format(admin.adminDir)
                QtCore.QFile(notanetFileName).remove()
                # Notanet veut que ce soit trié par INE puis par id_matiere :
                self.parent.data['notanet']['ELEVES'].sort()
                lines = ''
                first = True
                for eleveIne in self.parent.data['notanet']['ELEVES']:
                    for line in self.parent.data['notanet'][eleveIne]:
                        if first:
                            first = False
                            lines = utils_functions.u('{0}{1}').format(lines, line)
                        else:
                            lines = utils_functions.u('{0}\n{1}').format(lines, line)
                # ce n'est dit nulle part mais je parie que c'est du windows qui est attendu :
                notanetFile = QtCore.QFile(notanetFileName)
                if notanetFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                    stream = QtCore.QTextStream(notanetFile)
                    stream.setCodec('ISO 8859-15')
                    stream << lines
                    notanetFile.close()

            elif self.parent.data['sorted']:
                try:
                    PYTHONVERSION = utils.PYTHONVERSION
                    from PyPDF2 import PdfFileMerger, PdfFileReader
                    QtCore.QDir.setCurrent(admin.modelesDir)
                    if PYTHONVERSION >= 30:
                        blankPagePdf = open(self.parent.data['blankPagePdfName'], 'rb')
                    else:
                        blankPagePdf = file(self.parent.data['blankPagePdfName'], 'rb')
                    QtCore.QDir.setCurrent(pdfDir)
                    for anDernier in dicAnDernier:
                        if anDernier != '':
                            classFileName = utils_functions.u('{0}.pdf').format(anDernier)
                        else:
                            unknown = QtWidgets.QApplication.translate('main', 'UNKNOWN')
                            classFileName = utils_functions.u('{0}.pdf').format(unknown)
                        if PYTHONVERSION >= 30:
                            outputPdfStream = open(classFileName, 'wb')
                        else:
                            outputPdfStream = file(classFileName, 'wb')
                        merger = PdfFileMerger(strict=False)
                        # on récupère les fichiers pour pouvoir les refermer :
                        files = []
                        for idEleve in dicAnDernier[anDernier]:
                            pdfFile = utils_functions.u('{0}.pdf').format(idEleve)
                            if pdfFile in pdfFiles['WITHOUT_ADDRESSES']:
                                if PYTHONVERSION >= 30:
                                    f = open(pdfFile, 'rb')
                                else:
                                    f = file(pdfFile, 'rb')
                                files.append(f)
                                merger.append(f, import_bookmarks=False)
                                inputPdf = PdfFileReader(f)
                                nbPages = inputPdf.getNumPages()
                                del inputPdf
                                # si nbPages est impair, on fait un saut de page
                                # (utile pour l'impression) :
                                if self.parent.data['printOn2Pages']:
                                    if (nbPages % 2 != 0):
                                        merger.append(blankPagePdf, import_bookmarks=False)
                                elif self.parent.data['printOn4Pages']:
                                    manque = 4 - nbPages % 4
                                    if manque != 4:
                                        for i in range(manque):
                                            merger.append(blankPagePdf, import_bookmarks=False)
                        merger.write(outputPdfStream)
                        # on referme proprement les fichiers :
                        outputPdfStream.close()
                        for f in files:
                            f.close()
                    blankPagePdf.close()
                finally:
                    QtCore.QDir.setCurrent(self.main.beginDir)

            else:
                try:
                    PYTHONVERSION = utils.PYTHONVERSION
                    from PyPDF2 import PdfFileMerger, PdfFileReader
                    QtCore.QDir.setCurrent(admin.modelesDir)
                    if PYTHONVERSION >= 30:
                        blankPagePdf = open(self.parent.data['blankPagePdfName'], 'rb')
                    else:
                        blankPagePdf = file(self.parent.data['blankPagePdfName'], 'rb')
                    QtCore.QDir.setCurrent(pdfDir)
                    classFileName = utils_functions.u(
                        '{0}-{1}.pdf').format(self.parent.data['classPrefix'], baseNomFichier)
                    if PYTHONVERSION >= 30:
                        outputPdfStream = open(classFileName, 'wb')
                    else:
                        outputPdfStream = file(classFileName, 'wb')
                    merger = PdfFileMerger(strict=False)
                    # on récupère les fichiers pour pouvoir les refermer :
                    files = []
                    if self.parent.data['withAddresses']:
                        pdfFilesList = pdfFiles['WITH_ADDRESSES']
                    else:
                        pdfFilesList = pdfFiles['WITHOUT_ADDRESSES']
                    for pdfFile in pdfFilesList:
                        if PYTHONVERSION >= 30:
                            f = open(pdfFile, 'rb')
                        else:
                            f = file(pdfFile, 'rb')
                        files.append(f)
                        merger.append(f, import_bookmarks=False)
                        inputPdf = PdfFileReader(f)
                        nbPages = inputPdf.getNumPages()
                        del inputPdf
                        # si nbPages est impair, on fait un saut de page
                        # (utile pour l'impression) :
                        if self.parent.data['printOn2Pages']:
                            if (nbPages % 2 != 0):
                                merger.append(blankPagePdf, import_bookmarks=False)
                        elif self.parent.data['printOn4Pages']:
                            manque = 4 - nbPages % 4
                            if manque != 4:
                                for i in range(manque):
                                    merger.append(blankPagePdf, import_bookmarks=False)
                    merger.write(outputPdfStream)
                    # on referme proprement les fichiers :
                    outputPdfStream.close()
                    blankPagePdf.close()
                    for f in files:
                        f.close()
                finally:
                    QtCore.QDir.setCurrent(self.main.beginDir)

            # suppression des fichiers individuels :
            for pdfFile in pdfFiles['WITH_ADDRESSES']:
                src = pdfDir + pdfFile
                QtCore.QFile(src).remove()
            pdfFiles['WITH_ADDRESSES'] = []
            if self.parent.data['onlyClassFile']:
                for pdfFile in pdfFiles['WITHOUT_ADDRESSES']:
                    src = pdfDir + pdfFile
                    QtCore.QFile(src).remove()
                pdfFiles['WITHOUT_ADDRESSES'] = []

            if self.parent.data['mustUpdateAndMove']:
                for pdfFile in pdfFiles['WITHOUT_ADDRESSES']:
                    src = pdfDir + pdfFile
                    dst = admin.dirLocalPrive + '/protected/documents/' + pdfFile
                    fileDst = QtCore.QFile(dst)
                    if fileDst.exists():
                        fileDst.remove()
                    QtCore.QFile(src).copy(dst)
                    QtCore.QFile(src).remove()

                src = pdfDir + classFileName
                dst = admin.dirLocalPrive + '/protected/docsprofs/' + classFileName
                fileDst = QtCore.QFile(dst)
                if fileDst.exists():
                    fileDst.remove()
                QtCore.QFile(src).copy(dst)
                QtCore.QFile(src).remove()
                profsDocument = (-1, classFileName, self.parent.data['classLabelDocument'], 1)
                admin_docs.addDocumentProfs(self.main, profsDocument)

            # c'est fini :
            utils_functions.afficheMessage(
                self.main, 
                QtWidgets.QApplication.translate('main', 'FINISHED'), 
                tags=('h2'))
            fin = QtCore.QTime.currentTime()
            utils_functions.afficheDuree(self.main, debut, fin)
        except:
            utils_functions.afficheMsgPb(self.main)
        finally:
            utils_functions.restoreCursor()
            if len(listePourErreurs) > 0:
                utils_functions.afficheMessage(self.main, 
                ['listePourErreurs :', listePourErreurs], editLog=True)





















































































































































































































































def addDetails(main, cpt, label, id_prof, matiereCode, id_eleve, colorsOnly=False):

    def traiteAffichageValeurDetails(value, colorsOnly=False):
        """
        pour gérer la mise en forme de l'affichage
        selon la valeur (couleur par exemple).
        """
        affichageValue = utils_functions.valeur2affichage(value)
        value = utils_calculs.moyenneItem(value)
        dico = {'A': 'A V', 'B': 'B J', 'C': 'C O', 'D': 'D R', 'X': 'X'}
        if value in dico:
            if colorsOnly:
                affichageValue = ''
            result = utils_functions.u(
                '<td class="{0}"><b>{1}</b></td>').format(
                    dico[value], affichageValue)
        else:
            result = '<td class="valeur"></td>'
        return result

    import utils_calculs
    itemsLabels = []
    items = {}
    id_bilan = -1
    commandLine = utils_functions.u(
        'SELECT * FROM persos '
        'WHERE id_eleve=? AND id_prof=? '
        'AND Matiere=? AND Periode IN (0,?) '
        'AND Label=?')
    query_recupEvals = utils_db.query(admin.db_recupEvals)
    query_recupEvals = utils_db.queryExecute(
        {commandLine: (id_eleve, id_prof, matiereCode, utils.selectedPeriod, label)}, 
        query=query_recupEvals)
    if query_recupEvals.first():
        id_bilan = int(query_recupEvals.value(2))
    if id_bilan == -1:
        return label

    profxx = ''
    for prof in admin.PROFS:
        if prof[0] == id_prof:
            profxx = prof[1] + '.sqlite'
    if profxx == '':
        return label

    try:
        # on ouvre le fichier :
        fileProfxx = admin.dirLocalProfxx + profxx
        (db_profxx, dbName) = utils_db.createConnection(main, fileProfxx)
        query_profxx = utils_db.query(db_profxx)
        commandLine = utils_functions.u(
            'SELECT items.Label, evaluations.value '
            'FROM item_bilan '
            'JOIN evaluations ON evaluations.id_item=item_bilan.id_item '
            'JOIN items ON items.id_item=item_bilan.id_item '
            'JOIN tableaux ON tableaux.id_tableau=evaluations.id_tableau '
            'WHERE evaluations.id_eleve={0} AND item_bilan.id_bilan={1} '
            'AND tableaux.Public=1').format(id_eleve, id_bilan)
        query_profxx = utils_db.queryExecute(commandLine, query=query_profxx)
        while query_profxx.next():
            itemLabel = query_profxx.value(0)
            itemValue = query_profxx.value(1)
            if not (itemLabel in itemsLabels):
                itemsLabels.append(itemLabel)
                items[itemLabel] = itemValue
            else:
                items[itemLabel] = utils_functions.u('{0}{1}').format(
                    items[itemLabel], itemValue)
        # on referme la base prof :
        query_profxx.clear()
        del query_profxx
        db_profxx.close()
        del db_profxx
        utils_db.sqlDatabase.removeDatabase(dbName)
    except:
        return label

    itemsLabels = sorted(itemsLabels)
    tableRows = ''
    for itemLabel in itemsLabels:
        itemValue = traiteAffichageValeurDetails(items[itemLabel], colorsOnly=colorsOnly)
        tableRows = utils_functions.u(
            '{0}\
            <tr>\
            <td>{1}</td>{2}\
            </tr>').format(tableRows, itemLabel, itemValue)
    labelWithDetails = utils_functions.u(
        '{0}<br/>\
        <table align=right width=600 cellpadding=2 cellspacing=0 border="1">\
        {1}\
        </table>').format(label, tableRows)
    return labelWithDetails


def createStudentFileFromHtmlTemplate(
        main, data, listePourErreurs, pdfFiles, orientation='Portrait', 
        what='', notanet={'ELEVES': []}, 
        deleteNotEvaluatedLines=False, classColumn=True, hideStudentNames=False, 
        withAddresses=False):

    if what == 'DNB':
        import utils_dnb

    def decimal2affichage(value=0):
        text = ''
        try:
            if value > -1:
                if value - int(value) == 0:
                    value = int(value)
                text = '{0}'.format(value)
                text = text.replace('.', utils.DECIMAL_SEPARATOR)
        except:
            pass
        return text

    def doNotanetLine(eleveIne, id_matiere, arrondi, noteEleveText):
        if arrondi > -1:
            # on transforme en texte avec 2 chiffres après le point :
            notanetText = '{0}'.format(arrondi)
            notanetText = notanetText.replace(utils.DECIMAL_SEPARATOR, '.')
            notanet = notanetText.split('.')
            if len(notanet) == 1:
                notanetText = '{0}.00'.format(notanet[0])
            else:
                notanetText = '{0}.{1}0'.format(notanet[0], notanet[1][0])
        else:
            notanetText = noteEleveText
        notanetLine = '{0}|{1}|{2}|'.format(eleveIne, id_matiere, notanetText)
        return notanetLine

    def addToNotanet(eleveIne, notanetLine):
        if eleveIne in notanet['ELEVES']:
            notanet[eleveIne].append(notanetLine)
        else:
            notanet['ELEVES'].append(eleveIne)
            notanet[eleveIne] = [notanetLine, ]

    def dataDNB(id_matiere, eleveActuelMatiere, eleveName, total):
        # la note et le texte si c'est un truc genre AB DI etc :
        noteEleve, noteEleveText = -1, ''
        arrondi = -1
        noteClasse = -1
        appreciation = ''
        matiereName = ''
        noteBase = utils_dnb.listeMatieresDNB[id_matiere][2]
        # on commence par récupérer noteEleve, noteClasse, appreciation et matiereName :
        matieresCodes = utils_dnb.listeMatieresDNB[id_matiere][4]
        for matiereCode in matieresCodes:
            matiereId =  admin.MATIERES['Code2Matiere'][matiereCode][0]
            if matiereId in eleveActuelMatiere:
                for id_prof in eleveActuelMatiere[matiereId]:
                    datasExists = False
                    if (noteEleve < 0) \
                        and ('NOTES' in eleveActuelMatiere[matiereId][id_prof]):
                        (noteEleve, noteClasse) = eleveActuelMatiere[matiereId][id_prof]['NOTES']
                        datasExists = True
                    if (appreciation == '') \
                        and ('APPRECIATION' in eleveActuelMatiere[matiereId][id_prof]):
                        appreciation = eleveActuelMatiere[matiereId][id_prof]['APPRECIATION']
                        datasExists = True
                    # on récupère le nom (dans la base commun) de la matière
                    # pour les LV et les options :
                    if datasExists:
                        matiereName = admin.MATIERES['Code2Matiere'][matiereCode][1]
        if noteEleve < 0:
            # si on n'a rien trouvé et que c'est une matière qui compte,
            # on demande s'il faut mettre AB ou autre :
            if utils_dnb.listeMatieresDNB[id_matiere][1] == 'POINTS':
                if len(utils_dnb.listeMatieresDNB[id_matiere][3]) > 1:
                    utils_functions.restoreCursor()
                    textChoice = QtWidgets.QApplication.translate(
                        'main', 
                        '<p>No note in subject:<br/><b>{0}</b>'
                        '<br/>for student:<br/><b>{1}</b>.'
                        '<br/><br/>Select what to write:</p>')
                    choices = []
                    for choice in utils_dnb.listeMatieresDNB[id_matiere][3]:
                        choiceTraduc = utils_functions.u(utils_dnb.traductionsDNB[choice])
                        choices.append(utils_functions.u('{0} ({1})').format(choice, choiceTraduc))
                    choice, ok = QtWidgets.QInputDialog.getItem(
                        main, 
                        utils.PROGNAME, 
                        utils_functions.u(textChoice).format(
                            utils_dnb.listeMatieresDNB[id_matiere][0], eleveName), 
                        choices, 0, False)
                    utils_functions.doWaitCursor()
                    if ok:
                        noteEleveText = choice[:2]
                else:
                    noteEleveText = utils_dnb.listeMatieresDNB[id_matiere][3][0]
                if noteEleveText == 'AB':
                    # si l'élève est marqué AB, c'est comme s'il avait 0 :
                    total[1] += noteBase
            elif utils_dnb.listeMatieresDNB[id_matiere][1] == 'NOTNONCA':
                noteEleveText = 'AB'
        else:
            # on a bien trouvé une note.
            if utils_dnb.listeMatieresDNB[id_matiere][1] == 'PTSUP':
                # on ne garde que les points supplémentaires :
                arrondi = utils_dnb.arrondirAuDemiPointSuperieur(noteEleve) - noteBase
                if arrondi > 0:
                    total[0] += arrondi
                else:
                    arrondi = 0.0
            else:
                arrondi = utils_dnb.arrondirAuDemiPointSuperieur(noteEleve)
                if utils_dnb.listeMatieresDNB[id_matiere][1] == 'POINTS':
                    total[0] += arrondi
                    total[1] += noteBase
        result = (noteEleve, noteEleveText, 
                  arrondi, 
                  noteClasse, 
                  appreciation, 
                  total, matiereName)
        return result

    def effaceReperesInutiles(text):
        # on efface les repères qui pourraient être encore là
        # (par exemple si la vie scolaire n'a pas été évaluée) :
        tempList = []
        first = True
        # on coupe sur la chaine "${" :
        for part in text.split('${'):
            if first:
                # on garde le premier morceau tel quel :
                tempList.append(part)
                first = False
            elif part[:8] == 'Adresse}':
                newPart = utils_functions.u('{0}{1}').format('${', part)
                tempList.append(newPart)
            else:
                # on garde ce qui suit le premier "}" :
                tempList.append('}'.join(part.split('}')[1:]))
        # on recolle les morceaux :
        # ICI nouvelle version des modèles
        newtext = utils_functions.u('').join(tempList)
        #newtext = utils_functions.u('<td></td>').join(tempList)
        return newtext

    def doDeleteNotEvaluatedLines(text):
        """
        efface les lignes vides des compétences partagées.
        On coupe sur le repère <td class="valeur"></td>
        (case vide).
        On efface avant jsuqu'au dernier <tr>
        et après jusqu'au premier </tr>.
        Le premier et le dernier morceau sont traités à part.
        Enfin il faut prendre en compte le cas de 2 cases vides 
        successives dans la même ligne (élève + classe non évalués).
        """
        newtext = ''
        tempList = []
        # on coupe sur les cases vides :
        parts = text.split('<td class="valeur"></td>')
        if len(parts) < 2:
            tempList.append(parts[0])
        else:
            for i in range(len(parts)):
                newPart = ''
                if i == 0:
                    newPart = '<tr>'.join(parts[i].split('<tr>')[:-1])
                elif i == len(parts) - 1:
                    newPart = '</tr>'.join(parts[i].split('</tr>')[1:])
                else:
                    # pour écarter le cas de 2 cases successives :
                    test = parts[i].replace('\n', '').replace(' ', '')
                    if test != '':
                        newPart = '<tr>'.join(parts[i].split('<tr>')[:-1])
                        newPart = '</tr>'.join(newPart.split('</tr>')[1:])
                tempList.append(newPart)
        # on recolle les morceaux :
        newtext = utils_functions.u('').join(tempList)
        return newtext


    # les imports pour Html :
    import utils_html, utils_pdf

    fileName = data[0]
    eleveActuel = data[1]
    numEleve = data[2]
    nbEleves = data[3]

    inFile = QtCore.QFile(fileName)
    if not inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        message = QtWidgets.QApplication.translate(
            'main', 'Cannot read file {0}:\n{1}.').format(fileName, inFile.errorString())
        utils_functions.messageBox(main, level='warning', message=message)
        return
    indentSize = 4
    textStream = QtCore.QTextStream(inFile)
    textStream.setCodec('UTF-8')
    htmlModele = textStream.readAll()
    inFile.close()

    try:
        # on récupère les dictionnaires :
        eleveActuelCommun = eleveActuel[0]
        eleveActuelCommunValeurs = eleveActuel[1]
        eleveActuelMatiere = eleveActuel[2]
        eleveActuelCommunDetails = eleveActuel[3]

        # ouverture du fichier modèle :
        htmlCurrent = utils_functions.u(htmlModele)
        # affichage du message :
        msg = utils_functions.u('{0} ({1}/{2})').format(
            eleveActuelCommun['NOM Prenom'], numEleve, nbEleves)
        utils_functions.afficheMessage(main, msg, editLog=True)

        eleveActuelMatiere['Matieres'] = {}
        # remplacements d'après les dictionnaires 
        # (sauf eleveActuelMatiere et ADRESSES) :
        for dico in (eleveActuelCommun, 
                     eleveActuelCommunValeurs, 
                     eleveActuelCommunDetails, 
                     eleveActuelMatiere['Matieres'], ):
            for content in dico:
                if content != 'ADRESSES':
                    # ICI nouvelle version des modèles
                    replaceWhat = "${" + content + "}"
                    replaceWith = dico[content]
                    try:
                        if replaceWith[:10] == '<td class=':
                            replaceWhatWithTD = "<td>${" + content + "}</td>"
                            if replaceWhatWithTD in htmlCurrent:
                                replaceWhat = replaceWhatWithTD
                    except:
                        pass
                    replaceWith = nl2br(replaceWith)
                    htmlCurrent = htmlCurrent.replace(
                        utils_functions.u(replaceWhat), utils_functions.u(replaceWith))

        # des textes utilisés plusieurs fois :
        titlesNotes = (
            QtWidgets.QApplication.translate('main', 'Average score:'),
            QtWidgets.QApplication.translate('main', 'standard deviation:'),
            QtWidgets.QApplication.translate('main', 'mini score:'),
            QtWidgets.QApplication.translate('main', 'maxi score:'),
            QtWidgets.QApplication.translate('main', 'VS note'),
            )
        if classColumn:
            classeText = utils_functions.u('<th class="titre">{0}</th>').format(
                QtWidgets.QApplication.translate('main', 'Class'))
            htmlNotesLigneBase =  '\n        <tr>'
            htmlNotesLigneBase += '\n            <td align="right"><b>{0}</b></td>'
            htmlNotesLigneBase += '\n            <td align="center"><b>{1}</b></td>'
            htmlNotesLigneBase += '\n            <td align="center"><b>{2}</b></td>'
            htmlNotesLigneBase += '\n        </tr>'
            htmlNotesLigneBase += '\n        <tr>'
            htmlNotesLigneBase += '\n            <td class="notes" colspan="3" align="right">{3}</td>'
            htmlNotesLigneBase += '\n        </tr>'
            htmlAppreciationLigneBase =  '\n        <tr>'
            htmlAppreciationLigneBase += '\n            <td class="appreciation" colspan="3">{0}</td>'
            htmlAppreciationLigneBase += '\n        </tr>'
            htmlNotesLigneBaseVS =  '\n        <tr>'
            htmlNotesLigneBaseVS += '\n            <td align="left">{0}</td>'
            htmlNotesLigneBaseVS += '\n            <td align="center"><b>{1}</b></td>'
            htmlNotesLigneBaseVS += '\n            <td align="center"><b>{2}</b></td>'
            htmlNotesLigneBaseVS += '\n        </tr>'
            htmlAbsencesLigneBase =  '\n        <tr>'
            htmlAbsencesLigneBase += '\n            <td align="left" colspan="3">{0}</td>'
            htmlAbsencesLigneBase += '\n        </tr>'
            htmlLigneBase =  '\n        <tr>'
            htmlLigneBase += '\n            <td>{0}</td>'
            htmlLigneBase += '\n            {1}'
            htmlLigneBase += '\n            {2}'
            htmlLigneBase += '\n        </tr>'
        else:
            classeText = ''
            htmlNotesLigneBase =  '\n        <tr>'
            htmlNotesLigneBase += '\n            <td align="right"><b>{0}</b></td>'
            htmlNotesLigneBase += '\n            <td align="center"><b>{1}</b></td>'
            htmlNotesLigneBase += '\n        </tr>'
            htmlAppreciationLigneBase =  '\n        <tr>'
            htmlAppreciationLigneBase += '\n            <td class="appreciation" colspan="2">{0}</td>'
            htmlAppreciationLigneBase += '\n        </tr>'
            htmlNotesLigneBaseVS = ''
            htmlAbsencesLigneBase =  '\n        <tr>'
            htmlAbsencesLigneBase += '\n            <td align="left" colspan="2">{0}</td>'
            htmlAbsencesLigneBase += '\n        </tr>'
            htmlLigneBase =  '\n        <tr>'
            htmlLigneBase += '\n            <td>{0}</td>'
            htmlLigneBase += '\n            {1}'
            htmlLigneBase += '\n        </tr>'
        htmlTableauBase =  '\n<br/>'
        htmlTableauBase += '\n<div style="page-break-inside:avoid">'
        htmlTableauBase += '\n    <table width=1000 cellpadding=2 cellspacing=0 border="2">'
        htmlTableauBase += '\n        <col width=800>'
        htmlTableauBase += '\n        <col width=120>'
        htmlTableauBase += '\n        <col width=80>'
        htmlTableauBase += '\n        <tr>'
        htmlTableauBase += '\n            <th class="titre" align=left>{0}</th>'
        htmlTableauBase += '\n            <th class="titre">{1}</th>'
        htmlTableauBase += '\n            {2}'
        htmlTableauBase += '\n        </tr>'
        htmlTableauFin =  '\n    </table>'
        htmlTableauFin += '\n</div>'

        """
        Dans la partie suivante, on traite chaque matière.
        """
        if what == 'DNB':
            total = [0.0, 0]
            # on récupère la liste triée des id des matières (indispensable pour NOTANET) :
            listeMatieresSorted = []
            for id_matiere in utils_dnb.listeMatieresDNB:
                listeMatieresSorted.append(id_matiere)
            listeMatieresSorted.sort()
            for id_matiere in listeMatieresSorted:
                try:
                    matiereLabel = utils_dnb.listeMatieresDNB[id_matiere][0]
                    data = dataDNB(
                        id_matiere, eleveActuelMatiere, eleveActuelCommun['NOM Prenom'], total)
                    noteEleve, noteEleveText = data[0], data[1]
                    arrondi = data[2]
                    noteClasse = data[3]
                    appreciation = data[4]
                    total, matiereName = data[5], data[6]
                    notanetLine = doNotanetLine(
                        eleveActuelCommun['INE'], id_matiere, arrondi, noteEleveText)
                    if utils_dnb.listeMatieresDNB[id_matiere][1] == 'POINTS':
                        # cas d'une matière de type "POINTS" (matière normale) :
                        addToNotanet(eleveActuelCommun['INE'], notanetLine)
                        if arrondi > -1:
                            noteEleveText = decimal2affichage(noteEleve)
                            noteClasseText = decimal2affichage(noteClasse)
                            arrondiText = decimal2affichage(arrondi)
                            noteBase = utils_dnb.listeMatieresDNB[id_matiere][2]
                            noteEleveBaseText = '{0} / {1}'.format(arrondiText, noteBase)
                        else:
                            if noteClasse > -1:
                                noteClasseText = decimal2affichage(noteClasse)
                            else:
                                noteClasseText = ''
                            noteEleveBaseText = ''
                        if id_matiere in utils_dnb.mustAddMatiereName:
                            htmlMatiere = utils_functions.u(utils_dnb.htmlMatiereWithName).format(
                                matiereLabel, matiereName, 
                                noteClasseText, noteEleveText, appreciation, noteEleveBaseText)
                        else:
                            htmlMatiere = utils_functions.u(utils_dnb.htmlMatierePoints).format(
                                matiereLabel, noteClasseText, noteEleveText, 
                                appreciation, noteEleveBaseText)
                        htmlMatiere = htmlMatiere + '${RESULTATS PAR MATIERE}'
                        # on remplace :
                        htmlCurrent = htmlCurrent.replace(
                            '${RESULTATS PAR MATIERE}', utils_functions.u(htmlMatiere))
                    elif utils_dnb.listeMatieresDNB[id_matiere][1] == 'PTSUP':
                        # cas d'une matière de type "PTSUP" (option) :
                        if arrondi > -1:
                            addToNotanet(eleveActuelCommun['INE'], notanetLine)
                            noteClasseText = decimal2affichage(noteClasse)
                            noteEleveText = decimal2affichage(noteEleve)
                            arrondiText = decimal2affichage(arrondi)
                            htmlMatiere = utils_functions.u(utils_dnb.htmlMatierePtSup).format(
                                matiereLabel, matiereName, noteClasseText, noteEleveText,
                                appreciation, utils_dnb.POINTS_SUP_TEXT, arrondiText)
                            htmlMatiere = htmlMatiere + '${RESULTATS PAR MATIERE}'
                            # on remplace :
                            htmlCurrent = htmlCurrent.replace(
                                '${RESULTATS PAR MATIERE}', utils_functions.u(htmlMatiere))
                    elif utils_dnb.listeMatieresDNB[id_matiere][1] == 'NOTNONCA':
                        # cas d'une matière de type "NOTNONCA" (HG etc) :
                        addToNotanet(eleveActuelCommun['INE'], notanetLine)
                        if arrondi > -1:
                            noteEleveText = decimal2affichage(noteEleve)
                            noteClasseText = decimal2affichage(noteClasse)
                            arrondiText = decimal2affichage(arrondi)
                            noteBase = utils_dnb.listeMatieresDNB[id_matiere][2]
                        else:
                            if noteClasse > -1:
                                noteClasseText = decimal2affichage(noteClasse)
                            else:
                                noteClasseText = ''
                        htmlMatiere = utils_functions.u(utils_dnb.htmlMatiereNotNonCa).format(
                            matiereLabel, noteClasseText, noteEleveText, appreciation)
                        htmlMatiere = htmlMatiere + '${NOTNONCA}'
                        # on remplace :
                        htmlCurrent = htmlCurrent.replace(
                            '${NOTNONCA}', utils_functions.u(htmlMatiere))
                except:
                    # en cas de plantage :
                    # on met le nom de l'élève dans la liste listePourErreurs
                    utils_functions.afficheMsgPb(main, 'except', withMessageBox=False)
                    msg = utils_functions.u('{0} {1} {2}').format(
                        eleveActuelCommun['id_eleve'], 
                        eleveActuelCommun['NOM Prenom'], 
                        matiereLabel)
                    listePourErreurs.append(msg)
                    continue

            # on ajoute la validation du niveau A2 :
            a2Result = eleveActuelMatiere['AVIS_DNB'].get(1, '')
            a2Correspondance = {'A': 'VA', 'B': 'VA', 'C': 'NV', 'D': 'NV'}
            a2Result = a2Correspondance.get(a2Result, 'AB')
            notanetLine = '{0}|{1}|{2}|'.format(eleveActuelCommun['INE'], 130, a2Result)
            addToNotanet(eleveActuelCommun['INE'], notanetLine)
            htmlMatiere = utils_functions.u(utils_dnb.htmlNiveauA2).format(
                utils_dnb.traductionsDNB['SOCLE_A2'], utils_dnb.traductionsDNB[a2Result])
            # on remplace :
            htmlCurrent = htmlCurrent.replace('${NIVEAU_A2}', utils_functions.u(htmlMatiere))

            # on ajoute le total des points :
            replaceWhat = "${TOTAL POINTS}"
            totalText = '{0} / {1}'.format(total[0], total[1])
            totalText = totalText.replace('.', utils.DECIMAL_SEPARATOR)
            htmlCurrent = htmlCurrent.replace(replaceWhat, totalText)
            notanetLine = '{0}|{1}|{2}0|'.format(eleveActuelCommun['INE'], 'TOT', total[0])
            addToNotanet(eleveActuelCommun['INE'], notanetLine)

            # on ajoute la validation du socle (obsolète ?) :
            socleResult = eleveActuelMatiere['AVIS_DNB'].get(0, '')
            socleCorrespondance = {'': 'AB', 
                                   'A': 'MS', 'B': 'MS', 
                                   'C': 'ME', 'D': 'ME'}
            socleResult = socleCorrespondance.get(socleResult, 'MN')
            htmlCurrent = htmlCurrent.replace('${SOCLE}', socleResult)

            # on ajoute l'avis du conseil de classe et du chef d'établissement :
            AvisConseilClasse = eleveActuelMatiere['AVIS_DNB'].get(2, '')
            avisCorrespondance = {'A':utils_dnb.OPINION_A, 'B':utils_dnb.OPINION_B,
                                  'C':utils_dnb.OPINION_C, 'D':utils_dnb.OPINION_D}
            AvisConseilClasse = avisCorrespondance.get(AvisConseilClasse, '')
            htmlCurrent = htmlCurrent.replace('${AvisConseilClasse}', AvisConseilClasse)
            AvisChefEtab = eleveActuelMatiere['AVIS_DNB'].get(3, '')
            htmlCurrent = htmlCurrent.replace('${AvisChefEtab}', AvisChefEtab)

            # on efface les repères au cas où (${RESULTATS PAR MATIERE}, ...) :
            htmlCurrent = effaceReperesInutiles(htmlCurrent)

        else:
            # on fait une copie de la liste des matières :
            listeMatieresTemp = admin.MATIERES['BLT'][:]
            lastProfName = ''
            if what == 'annualReports':
                limiteBLT = main.limiteBLTPerso * (utils.NB_PERIODES - 1)
            else:
                limiteBLT = main.limiteBLTPerso

            while len(listeMatieresTemp) > 0:
                # on va s'occuper de la première matière de la liste :
                matiereCode = listeMatieresTemp[0]
                if not(matiereCode in eleveActuelMatiere) or (len(eleveActuelMatiere[matiereCode]) < 1):
                    del listeMatieresTemp[0]
                    continue
                numProf = 0
                for id_prof in eleveActuelMatiere[matiereCode]:
                    numProf += 1
                    # dernier donnera le nombre de lignes du tableau à créer
                    # ou 0 s'il n'y a qu'une appréciation ou des notes
                    # ou -1 s'il n'y a rien
                    dernier = -1
                    for i in range(limiteBLT):
                        matiereKey = utils_functions.formatPersoCpt(matiereCode, i + 1)
                        matiereClasseKey = utils_functions.u('{0}-Classe').format(matiereKey)
                        matiereLabelKey = utils_functions.u('{0}-Label').format(matiereKey)
                        try:
                            if eleveActuelMatiere[matiereCode][id_prof][matiereLabelKey] != '':
                                if eleveActuelMatiere[matiereCode][id_prof][matiereKey] != '<td class="valeur"></td>':
                                    dernier = i + 1
                                elif eleveActuelMatiere[matiereCode][id_prof][matiereClasseKey] != '<td class="valeur"></td>':
                                    dernier = i + 1
                        except:
                            continue
                    # note, moyenne, etc s'il y en a :
                    # notes = (note, moyenne, ecart, mini, maxi)
                    htmlNotesLigne = ''
                    if 'NOTES' in eleveActuelMatiere[matiereCode][id_prof]:
                        notes = eleveActuelMatiere[matiereCode][id_prof]['NOTES']
                        espaces = '&nbsp;' * 10
                        notesLigne = ''
                        for i in range(3):
                            if notesLigne == '':
                                notesLigne = utils_functions.u('{0} {1}').format(
                                    titlesNotes[1 + i], decimal2affichage(notes[2 + i]))
                            else:
                                notesLigne = utils_functions.u('{0}{1}{2} {3}').format(
                                    notesLigne, 
                                    espaces, 
                                    titlesNotes[1 + i], 
                                    decimal2affichage(notes[2 + i]))
                        htmlNotesLigne = utils_functions.u(htmlNotesLigneBase).format(
                            titlesNotes[0], 
                            decimal2affichage(notes[0]), 
                            decimal2affichage(notes[1]), 
                            notesLigne)
                        if dernier == -1:
                            dernier = 0
                    # on calcule l'appréciation :
                    htmlAppreciationLigne = ''
                    appreciationLigne = utils_functions.u(
                        eleveActuelMatiere[matiereCode][id_prof][matiereCode + '-appreciation'])
                    appreciationLigne = nl2br(appreciationLigne)
                    if appreciationLigne != '':
                        htmlAppreciationLigne = utils_functions.u(
                            htmlAppreciationLigneBase).format(appreciationLigne)
                        if dernier == -1:
                            dernier = 0
                    # si dernier est toujours à -1, c'est que l'élève ne fait pas cette matière
                    # donc on ne fait pas de tableau, 
                    # et on retire la matière de la liste listeMatieresTemp :
                    if dernier == -1:
                        if numProf >= len(eleveActuelMatiere[matiereCode]):
                            del listeMatieresTemp[0]
                        continue
                    try:
                        # récupération du label de la matière :
                        try:
                            matiereLabel = eleveActuelMatiere[matiereCode][id_prof]["matiereLabel"]
                        except:
                            matiereLabel = ''
                        # récupération du nom du prof :
                        try:
                            profNom = eleveActuelMatiere[matiereCode][id_prof]['profNom']
                        except:
                            profNom = ''
                        # cas où l'on n'écrit pas le nom du prof (anonymé, perso ou idem pour écoles) :
                        if main.actualVersion['versionName'] == 'perso':
                            profNom = ''
                        elif hideStudentNames:
                            profNom = 'Y. YYYYY'
                        elif lastProfName != '':
                            if profNom == lastProfName:
                                profNom = ''
                        # début du tableau :
                        if profNom != '':
                            lastProfName = profNom
                            titre = matiereLabel + '&nbsp;' * 5 + '(' + profNom + ')'
                        else:
                            titre = matiereLabel
                        eleveText = QtWidgets.QApplication.translate('main', 'Student')
                        htmlTableau = utils_functions.u(htmlTableauBase).format(
                            titre, eleveText, classeText)
                        # on remplit les lignes du tableau :
                        for i in range(dernier):
                            try:
                                matiereKey = utils_functions.formatPersoCpt(matiereCode, i + 1)
                                matiereClasseKey = utils_functions.u('{0}-Classe').format(matiereKey)
                                matiereLabelKey = utils_functions.u('{0}-Label').format(matiereKey)
                                labelLigne = eleveActuelMatiere[matiereCode][id_prof][matiereLabelKey]
                                valueEleveLigne = eleveActuelMatiere[matiereCode][id_prof][matiereKey]
                                valueClasseLigne = eleveActuelMatiere[matiereCode][id_prof][matiereClasseKey]
                                htmlLigne = utils_functions.u(htmlLigneBase).format(
                                    labelLigne, valueEleveLigne, valueClasseLigne)
                                htmlTableau = utils_functions.u('{0}{1}').format(htmlTableau, htmlLigne)
                            except:
                                continue
                        # on ajoute la note et l'appréciation :
                        htmlTableau = utils_functions.u('{0}{1}{2}').format(htmlTableau, htmlNotesLigne, htmlAppreciationLigne)
                        # on termine la table et on remet le repère pour le tableau suivant :
                        htmlTableau = utils_functions.u('{0}{1}').format(htmlTableau, htmlTableauFin)
                        htmlTableau = htmlTableau + '${RESULTATS PAR MATIERE}'
                        # on remplace :
                        htmlCurrent = htmlCurrent.replace('${RESULTATS PAR MATIERE}', htmlTableau)
                        # si on arrive ici, c'est que ça s'est bien passé
                        # on supprime la matière de la liste listeMatieresTemp :
                        if numProf >= len(eleveActuelMatiere[matiereCode]):
                            del listeMatieresTemp[0]
                    except:
                        # en cas de plantage :
                        # on met le nom de l'élève dans la liste listePourErreurs
                        utils_functions.afficheMsgPb(main, 'except', withMessageBox=False)
                        msg = utils_functions.u('{0} {1} {2} {3}').format(
                            eleveActuelCommun['id_eleve'], 
                            eleveActuelCommun['NOM Prenom'], 
                            matiereCode, 
                            id_prof)
                        listePourErreurs.append(msg)
                        if numProf >= len(eleveActuelMatiere[matiereCode]):
                            del listeMatieresTemp[0]
                        continue

                    # pour permettre des bulletins n'utilisant pas le repère
                    # ${RESULTATS PAR MATIERE} :
                    # ICI nouvelle version des modèles
                    for content in eleveActuelMatiere[matiereCode][id_prof]:
                        replaceWhat = "${" + content + "}"
                        replaceWith = eleveActuelMatiere[matiereCode][id_prof][content]
                        try:
                            if replaceWith[:10] == '<td class=':
                                replaceWhatWithTD = "<td>${" + content + "}</td>"
                                if replaceWhatWithTD in htmlCurrent:
                                    replaceWhat = replaceWhatWithTD
                        except:
                            pass
                        replaceWith = nl2br(replaceWith)
                        htmlCurrent = htmlCurrent.replace(
                            utils_functions.u(replaceWhat), utils_functions.u(replaceWith))

            # on peut effacer le repère ${RESULTATS PAR MATIERE} :
            htmlCurrent = htmlCurrent.replace(
                '${RESULTATS PAR MATIERE}', utils_functions.u(''))

        """
        On s'occupe des matières spéciales
        """
        for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
            if not(matiereCode in eleveActuelCommun):
                continue
            if len(eleveActuelCommun[matiereCode]) < 1:
                # on remplace :
                repere = '${' + eleveActuelCommun[matiereCode]['Repere'] + '}'
                htmlCurrent = htmlCurrent.replace(repere, '')
                continue
            # dernier donnera le nombre de lignes du tableau à créer
            # ou 0 s'il n'y a qu'une appréciation ou des notes
            # ou -1 s'il n'y a rien
            dernier = -1
            for i in range(limiteBLT):
                matiereKey = utils_functions.formatPersoCpt(matiereCode, i + 1)
                matiereClasseKey = utils_functions.u('{0}-Classe').format(matiereKey)
                matiereLabelKey = utils_functions.u('{0}-Label').format(matiereKey)
                try:
                    if eleveActuelCommun[matiereCode][matiereLabelKey] != '':
                        if eleveActuelCommun[matiereCode][matiereKey] != '<td class="valeur"></td>':
                            dernier = i + 1
                        elif eleveActuelCommun[matiereCode][matiereClasseKey] != '<td class="valeur"></td>':
                            dernier = i + 1
                except:
                    continue
            # note, moyenne, etc s'il y en a :
            htmlNotesLigne = ''
            if 'NOTES' in eleveActuelCommun[matiereCode]:
                notes = eleveActuelCommun[matiereCode]['NOTES']
                htmlNotesLigne = utils_functions.u(htmlNotesLigneBaseVS).format(
                    titlesNotes[4], 
                    decimal2affichage(notes[0]), 
                    decimal2affichage(notes[1]))
                if dernier == -1:
                    dernier = 0
            # on calcule l'appréciation :
            htmlAppreciationLigne = ''
            appreciationLigne = utils_functions.u(
                eleveActuelCommun[matiereCode]['appreciation'])
            appreciationLigne = nl2br(appreciationLigne)
            if appreciationLigne != '':
                htmlAppreciationLigne = utils_functions.u(
                    htmlAppreciationLigneBase).format(appreciationLigne)
                if dernier == -1:
                    dernier = 0
            # on calcule le nombre d'absences :
            htmlAbsencesLigne = ''
            absencesText = ''
            espaces = '&nbsp;' * 10
            if eleveActuelCommun['absences-0'] > 0:
                absencesText = utils_functions.u('{0} : {1}').format(
                    eleveActuelCommun['absences-0Label'], 
                    eleveActuelCommun['absences-0'])
            if eleveActuelCommun['absences-1'] > 0:
                if len(absencesText) > 0:
                    absencesText = utils_functions.u('{0}{1}').format(
                        absencesText, espaces)
                absencesText = utils_functions.u('{0}{1} : {2}').format(
                    absencesText, 
                    eleveActuelCommun['absences-1Label'], 
                    eleveActuelCommun['absences-1'])
            absencesText2 = ''
            if eleveActuelCommun['absences-2'] > 0:
                absencesText2 = utils_functions.u('{0} : {1}').format(
                    eleveActuelCommun['absences-2Label'], 
                    eleveActuelCommun['absences-2'])
            if eleveActuelCommun['absences-3'] > 0:
                if len(absencesText2) > 0:
                    absencesText2 = utils_functions.u('{0}{1}').format(
                        absencesText2, espaces)
                absencesText2 = utils_functions.u('{0}{1} : {2}').format(
                    absencesText2, 
                    eleveActuelCommun['absences-3Label'], 
                    eleveActuelCommun['absences-3'])
            if len(absencesText) > 0:
                if len(absencesText2) > 0:
                    absencesText = utils_functions.u('{0}<br/>{1}').format(
                        absencesText, absencesText2)
            elif len(absencesText2) > 0:
                absencesText = absencesText2;
            if len(absencesText) > 0:
                htmlAbsencesLigne = utils_functions.u(
                    htmlAbsencesLigneBase).format(absencesText)
                if dernier == -1:
                    dernier = 0
            # si dernier est toujours à -1, c'est que l'élève ne fait pas cette matière
            # donc on ne fait pas de tableau,
            if dernier == -1:
                continue
            try:
                # début du tableau :
                matiereLabel = eleveActuelCommun[matiereCode]['Label']
                eleveText = QtWidgets.QApplication.translate('main', 'Student')
                htmlTableau = utils_functions.u(htmlTableauBase).format(
                    matiereLabel, eleveText, classeText)
                # on remplit les lignes du tableau :
                for i in range(dernier):
                    matiereKey = utils_functions.formatPersoCpt(matiereCode, i + 1)
                    matiereClasseKey = utils_functions.u('{0}-Classe').format(matiereKey)
                    matiereLabelKey = utils_functions.u('{0}-Label').format(matiereKey)
                    labelLigne = eleveActuelCommun[matiereCode][matiereLabelKey]
                    valueEleveLigne = eleveActuelCommun[matiereCode][matiereKey]
                    valueClasseLigne = eleveActuelCommun[matiereCode][matiereClasseKey]
                    htmlLigne = utils_functions.u(htmlLigneBase).format(
                        labelLigne, valueEleveLigne, valueClasseLigne)
                    htmlTableau = utils_functions.u('{0}{1}').format(htmlTableau, htmlLigne)
                # on ajoute la note et l'appréciation :
                htmlTableau = utils_functions.u(
                    '{0}{1}{2}{3}').format(
                        htmlTableau, 
                        htmlNotesLigne, 
                        htmlAbsencesLigne, 
                        htmlAppreciationLigne)
                # on termine la table et on remet le repère pour le tableau suivant :
                htmlTableau = utils_functions.u('{0}{1}').format(htmlTableau, htmlTableauFin)
                if matiereCode == 'VS':
                    htmlVS = utils_functions.u('\n<!--B1-->\n<h2>VIE SCOLAIRE</h2>')
                    htmlTableau = utils_functions.u('{0}{1}').format(htmlVS, htmlTableau)
                # on remplace :
                repere = '${' + eleveActuelCommun[matiereCode]['Repere'] + '}'
                htmlCurrent = htmlCurrent.replace(repere, htmlTableau)
            except:
                # en cas de plantage :
                # on met le nom de l'élève dans la liste listePourErreurs
                utils_functions.afficheMsgPb(main, 'except', withMessageBox=False)
                msg = utils_functions.u('{0} {1} {2} {3}').format(
                    eleveActuelCommun['id_eleve'], 
                    eleveActuelCommun['NOM Prenom'], 
                    matiereCode, 
                    id_prof)
                listePourErreurs.append(msg)
                continue

        # on enregistre htmlCurrent dans un fichier html temporaire :
        if not('ADRESSES' in eleveActuelCommun):
            eleveActuelCommun['ADRESSES'] = ['', ]
        elif not(withAddresses):
            eleveActuelCommun['ADRESSES'] = ['', ]
        elif len(eleveActuelCommun['ADRESSES']) < 1:
            eleveActuelCommun['ADRESSES'] = ['', '', ]
        else:
            eleveActuelCommun['ADRESSES'].insert(0, '')
        numAdresse = -1
        nbAdresses = len(eleveActuelCommun['ADRESSES'])
        for adresse in eleveActuelCommun['ADRESSES']:
            if hideStudentNames:
                adresse = '\n\n<b>M. XXX xxxx\nMme XXX xxxx</b>\n\n'
                adresse += '5 rue XXXXXX\n33240 VERAC\n\n\n'
            numAdresse += 1
            htmlCurrentTemp = htmlCurrent.replace(
                '${Adresse}', nl2br(adresse))
            # on efface les repères qui pourraient être encore là :
            htmlCurrentTemp = effaceReperesInutiles(htmlCurrentTemp)
            if deleteNotEvaluatedLines:
                htmlCurrentTemp = doDeleteNotEvaluatedLines(htmlCurrentTemp)
            if utils.OS_NAME[0] == 'win':
                htmlCurrentTemp = utils_html.encodeHtml(htmlCurrentTemp)
            tempFileName = admin.modelesDir + '/temp.html'
            if QtCore.QFile(tempFileName).exists():
                QtCore.QFile(tempFileName).remove()
            outFile = QtCore.QFile(tempFileName)
            if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                stream = QtCore.QTextStream(outFile)
                stream.setCodec('UTF-8')
                stream << htmlCurrentTemp
                outFile.close()
            tempFileName = QtCore.QFileInfo(tempFileName).absoluteFilePath()

            # fichier pdf à créer :
            fin = ''
            if numAdresse > 0:
                fin = '-a{0}'.format(numAdresse)
            pdfFileName = utils_functions.u(
                '{0}{1}.pdf').format(eleveActuelCommun['NomFichier'], fin)
            pdfDir = utils_functions.u(
                '{0}/fichiers/pdf/').format(admin.adminDir)
            fileNamePdfEleve = utils_functions.u(
                '{0}{1}').format(pdfDir, pdfFileName)
            tempFileNamePdfEleve = utils_functions.u(
                '{0}/{1}').format(main.tempPath, pdfFileName)
            if numAdresse < 1:
                pdfFiles['WITHOUT_ADDRESSES'].append(pdfFileName)
            else:
                pdfFiles['WITH_ADDRESSES'].append(pdfFileName)

            # création du fichier pdf :
            # d'abord dans temp car wkhtmltopdf n'aime pas les chemins accentués ou avec espaces
            if what == 'DNB':
                footer = ('', '', False)
            else:
                footer = (
                    eleveActuelCommun['NOM Prenom'] + ' ' + eleveActuelCommun['Classe'], 
                    eleveActuelCommun['LABEL DOCUMENT'], 
                    True)
            utils_pdf.htmlToPdf(main, tempFileName, tempFileNamePdfEleve, footer, orientation)
            utils_filesdirs.removeAndCopy(tempFileNamePdfEleve, fileNamePdfEleve)

    except:
        msg = utils_functions.u('{0} {1}').format(
            eleveActuelCommun['id_eleve'], 
            eleveActuelCommun['NOM Prenom'])
        listePourErreurs.append(msg)
        utils_functions.afficheMsgPb(main, 'except', withMessageBox=False)





###########################################################"
#   EXPORTATIONS ODS
###########################################################"

def exportNotes2ods(main, All=False):

    # sélection des élèves à traiter :
    idsEleves = []
    if All == False:
        dialog = admin.ListElevesDlg(
            parent=main, 
            helpMessage=10, 
            helpContextPage='export-notes-ods')
        lastState = main.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            main.enableInterface(dialog, lastState)
            return
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            idsEleves.append(id_eleve)
        if len(idsEleves) < 1:
            for i in range(dialog.baseList.count()):
                item = dialog.baseList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                idsEleves.append(id_eleve)
        main.enableInterface(dialog, lastState)

    odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
    proposedName = utils_functions.u(
        '{0}/fichiers/notes.ods').format(admin.adminDir)
    odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
    fileName = QtWidgets.QFileDialog.getSaveFileName(main, odsTitle, proposedName, odsExt)
    fileName = utils_functions.verifyLibs_fileName(fileName)
    if fileName == '':
        return

    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
    try:
        # connexion aux bases de données :
        query_admin = utils_db.query(admin.db_admin)
        # on a besoin de la base recup_evals pour les résultats annuels :
        admin.openDB(main, 'recup_evals')
        query_recupEvals = utils_db.query(admin.db_recupEvals)

        # liste des périodes à exporter :
        periodes = []
        if utils.selectedPeriod == 0:
            for periode in range(utils.NB_PERIODES):
                periodes.append(utils.PERIODES[periode])
        else:
            for periode in range(1, utils.selectedPeriod + 1):
                periodes.append(utils.PERIODES[periode])
        periodes.append(utils.PERIODES[999])

        # récupération des données :
        id_students = utils_functions.array2string(idsEleves)
        datas = {}
        for id_eleve in idsEleves:
            datas[id_eleve] = {}
            for periode in periodes:
                datas[id_eleve][periode] = {}
                for matiereCode in admin.MATIERES['BLT']:
                    datas[id_eleve][periode][matiereCode] = ''
                for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
                    datas[id_eleve][periode][matiereCode] = ''
        # Nom prénom classe :
        commandLine = 'SELECT * FROM eleves WHERE id IN ({0})'.format(id_students)
        query_admin = utils_db.queryExecute(commandLine, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            datas[id_eleve]['NOM'] = query_admin.value(2)
            datas[id_eleve]['Prenom'] = query_admin.value(3)
            datas[id_eleve]['Classe'] = query_admin.value(4)
        # les notes :
        commandLine = 'SELECT * FROM notes WHERE id_eleve IN ({0})'.format(id_students)
        query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
        while query_recupEvals.next():
            id_prof = int(query_recupEvals.value(0))
            id_eleve = int(query_recupEvals.value(1))
            matiereCode = query_recupEvals.value(2)
            periode = int(query_recupEvals.value(3))
            periode = utils.PERIODES[periode]
            try:
                value = float(query_recupEvals.value(4))
            except:
                value = ''
            datas[id_eleve][periode][matiereCode] = value

        # on y va :
        import utils_export
        nameText = QtWidgets.QApplication.translate('main', 'NAME')
        firstNameText = QtWidgets.QApplication.translate('main', 'FirstName')
        classText = QtWidgets.QApplication.translate('main', 'Class')
        codeText = QtWidgets.QApplication.translate('main', 'CODE')
        subjectText = QtWidgets.QApplication.translate('main', 'SUBJECT')
        subjectsText = QtWidgets.QApplication.translate('main', 'Subjects')
        for periode in periodes:
            # un onglet par période :
            theList = []
            line = []
            line.append({'value': nameText, 'alignHorizontal': 'center', 
                'bold': True, 'columnWidth': '3.0cm'})
            line.append({'value': firstNameText, 'alignHorizontal': 'center', 
                'bold': True, 'columnWidth': '3.0cm'})
            line.append({'value': classText, 'alignHorizontal': 'center', 
                'bold': True, 'columnWidth': '1.5cm'})
            for matiereCode in admin.MATIERES['BLT']:
                line.append({'value': matiereCode, 'alignHorizontal': 'center', 
                    'bold': True, 'columnWidth': '1.5cm'})
            for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
                line.append({'value': matiereCode, 'alignHorizontal': 'center', 
                    'bold': True, 'columnWidth': '1.5cm'})
            theList.append(line)
            for id_eleve in idsEleves:
                line = []
                line.append({'value': datas[id_eleve]['NOM'], 'alignHorizontal': 'left'})
                line.append({'value': datas[id_eleve]['Prenom'], 'alignHorizontal': 'left'})
                line.append({'value': datas[id_eleve]['Classe'], 'alignHorizontal': 'center'})
                for matiereCode in admin.MATIERES['BLT']:
                    value = datas[id_eleve][periode][matiereCode]
                    line.append({'value': value, 'type': utils.NOTE_PERSO})
                for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
                    value = datas[id_eleve][periode][matiereCode]
                    line.append({'value': value, 'type': utils.NOTE_PERSO})
                theList.append(line)
            exportDatas[periode] = theList
            exportDatas['TABLES_LIST'].append(periode)
            exportDatas['TABLES_PARAMS'][periode] = {'title': periode}

        # on ajoute un onglet pour la correspondance code-matière :
        theList = []
        line = []
        line.append({'value': codeText, 'alignHorizontal': 'center', 
            'bold': True, 'columnWidth': '2.5cm'})
        line.append({'value': subjectText, 'alignHorizontal': 'center', 
            'bold': True, 'columnWidth': '4.0cm'})
        theList.append(line)
        for matiereCode in admin.MATIERES['BLT']:
            line = []
            line.append({'value': matiereCode, 'alignHorizontal': 'left'})
            line.append({
                'value': admin.MATIERES['Code2Matiere'][matiereCode][1], 
                'alignHorizontal': 'left'})
            theList.append(line)
        for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
            line = []
            line.append({'value': matiereCode, 'alignHorizontal': 'left'})
            line.append({
                'value': admin.MATIERES['Code2Matiere'][matiereCode][1], 
                'alignHorizontal': 'left'})
            theList.append(line)
        exportDatas['subjects'] = theList
        exportDatas['TABLES_LIST'].append('subjects')
        exportDatas['TABLES_PARAMS']['subjects'] = {'title': subjectsText}

        # on exporte en ods :
        utils_functions.restoreCursor()
        utils_export.exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
        utils_functions.doWaitCursor()
    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()

def exportStudentsResults2ods(main):
    """
    """
    # sélection des élèves :
    students = {'ORDER': []}
    dialog = admin.ListElevesDlg(
        parent=main, 
        helpMessage=11, 
        helpContextPage='export-students-results2ods')
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        return
    for i in range(dialog.selectionList.count()):
        item = dialog.selectionList.item(i)
        id_eleve = item.data(QtCore.Qt.UserRole)
        students['ORDER'].append(id_eleve)
        students[id_eleve] = {'name': item.text(), 'results': []}
    if len(students['ORDER']) < 1:
        for i in range(dialog.baseList.count()):
            item = dialog.baseList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            students['ORDER'].append(id_eleve)
            students[id_eleve] = {'name': item.text(), 'results': []}
    main.enableInterface(dialog, lastState)
    if len(students['ORDER']) < 1:
        id_eleves = ''
        commandLine_propositions_eleves = ''
    else:
        id_eleves = utils_functions.array2string(students['ORDER'])
        commandLine_propositions_eleves = 'AND id_eleve IN ({2}) '

    # sélection des bilans :
    bilans = {'ORDER': []}
    dialog = admin.ListCPTDlg(
        parent=main, helpContextPage='export-students-results2ods')
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        return
    for i in range(dialog.selectionList.count()):
        item = dialog.selectionList.item(i)
        (name, title) = item.data(QtCore.Qt.UserRole)
        bilans['ORDER'].append(name)
    if len(bilans['ORDER']) < 1:
        for i in range(dialog.baseList.count()):
            item = dialog.baseList.item(i)
            (name, title) = item.data(QtCore.Qt.UserRole)
            bilans['ORDER'].append(name)
    for name in dialog.bilans:
        bilans[name] = dialog.bilans[name]
    main.enableInterface(dialog, lastState)
    if len(bilans['ORDER']) < 1:
        bilans_names = ''
        commandLine_propositions_bilans = ''
    else:
        bilans_names = utils_functions.array2string(bilans['ORDER'], text=True)
        commandLine_propositions_bilans = 'AND bilan_name IN ({1}) '

    # sélection des matières :
    matieres = {'ORDER': []}
    dialog = admin.ListMatieresDlg(
        parent=main, helpContextPage='export-students-results2ods')
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
        return
    for i in range(dialog.selectionList.count()):
        item = dialog.selectionList.item(i)
        (name, title) = item.data(QtCore.Qt.UserRole)
        matieres['ORDER'].append(name)
    if len(matieres['ORDER']) < 1:
        for i in range(dialog.baseList.count()):
            item = dialog.baseList.item(i)
            (name, title) = item.data(QtCore.Qt.UserRole)
            matieres['ORDER'].append(name)
    for name in dialog.matieres:
        matieres[name] = dialog.matieres[name]
    main.enableInterface(dialog, lastState)
    if len(matieres['ORDER']) < 1:
        matieres_names = ''
        commandLine_propositions_matieres = ''
    else:
        matieres_names = utils_functions.array2string(matieres['ORDER'], text=True)
        commandLine_propositions_matieres = 'AND Matiere IN ({0}) '

    # on y va :
    utils_functions.doWaitCursor()
    try:
        admin.openDB(main, 'referential_propositions')
        query_propositions = utils_db.query(admin.db_referentialPropositions)
        commandLine_propositions = (
            'SELECT * FROM bilans_details '
            'WHERE 1=1 '
            '{0}{1}{2}'
            'ORDER BY id_eleve, bilan_name, annee_scolaire, Matiere').format(
                commandLine_propositions_matieres, 
                commandLine_propositions_bilans, 
                commandLine_propositions_eleves)
        commandLine_propositions = utils_functions.u(commandLine_propositions).format(
            matieres_names, bilans_names, id_eleves)
        query_propositions = utils_db.queryExecute(
            commandLine_propositions, query=query_propositions)
        while query_propositions.next():
            id_eleve = int(query_propositions.value(0))
            annee_scolaire = query_propositions.value(1)
            matiereCode = query_propositions.value(3)
            nom_prof = query_propositions.value(4)
            bilan_name = query_propositions.value(5)
            value = query_propositions.value(6)
            result = (bilan_name, annee_scolaire, matiereCode, nom_prof, value)
            if not(id_eleve in students):
                students['ORDER'].append(id_eleve)
                students[id_eleve] = {'name': item.text(), 'results': []}
            students[id_eleve]['results'].append(result)
    finally:
        utils_functions.restoreCursor()

    # exportation en ods :
    what = 'results'
    translatedWhat = QtWidgets.QApplication.translate('main', 'results')
    odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
    proposedName = utils_functions.u(
        '{0}/fichiers/{1}.ods').format(admin.adminDir, translatedWhat)
    odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
    fileName = QtWidgets.QFileDialog.getSaveFileName(main, odsTitle, proposedName, odsExt)
    fileName = utils_functions.verifyLibs_fileName(fileName)
    if fileName == '':
        return
    OK = False
    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
    try:
        import utils_export
        theList = []
        # les titres :
        line = [
            {
                'value': QtWidgets.QApplication.translate('main', 'STUDENT'), 
                'columnWidth': '5.0cm', 'alignHorizontal': 'left', 'bold': True},
            {
                'value': QtWidgets.QApplication.translate('main', 'Balance'), 
                'columnWidth': '2.0cm', 'bold': True},
            {
                'value': QtWidgets.QApplication.translate('main', 'Year'), 
                'columnWidth': '2.0cm', 'bold': True},
            {
                'value': QtWidgets.QApplication.translate('main', 'Subject'), 
                'columnWidth': '4.0cm', 'bold': True},
            {
                'value': QtWidgets.QApplication.translate('main', 'Teacher'), 
                'columnWidth': '5.0cm', 'bold': True},
            {
                'value': QtWidgets.QApplication.translate('main', 'Result'), 
                    'columnWidth': '2.0cm', 'bold': True},
            {
                'value': QtWidgets.QApplication.translate('main', 'Title'), 
                    'columnWidth': '10.0cm', 'bold': True},
            ]
        theList.append(line)
        for id_eleve in students['ORDER']:
            studentName  = students[id_eleve]['name']
            if len(students[id_eleve]['results']) < 1:
                # s'il n'y a aucun résultat, on indique le nom de l'élève
                line = [
                    {'value': studentName},
                    {'value': ''},
                    {'value': ''},
                    {'value': ''},
                    {'value': ''},
                    {'value': ''},
                    {'value': ''},
                    ]
                theList.append(line)
            else:
                for result in students[id_eleve]['results']:
                    value = result[4]
                    try:
                        color = utils.affichages[value][2]
                    except:
                        color = utils.colorWhite.name()
                    value = utils_functions.valeur2affichage(value)
                    line = [
                        {'value': studentName},
                        {'value': result[0]},
                        {'value': result[1]},
                        {'value': matieres[result[2]]},
                        {'value': result[3]},
                        {
                            'value': value, 
                            'cellColor': color, 
                            'alignHorizontal': 'center', 
                            'bold': True},
                        {'value': bilans[result[0]]},
                        ]
                    theList.append(line)
        exportDatas[what] = theList
        exportDatas['TABLES_LIST'].append(what)
        exportDatas['TABLES_PARAMS'][what] = {'title': translatedWhat}
        # on exporte en ods :
        utils_functions.restoreCursor()
        utils_export.exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
        utils_functions.doWaitCursor()
        OK = True
    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()

def exportBLTEvals2ods(main, All=False):
    """
    création d'un fichier ODS contenant les détails 
    des évaluations du bulletin.
    Il y a un onglet par classe.
    """
    odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
    periode = utils_functions.changeBadChars(main.periodeButton.text())
    bulletins = QtWidgets.QApplication.translate('main', 'bulletins')
    proposedName = utils_functions.u(
        '{0}/fichiers/{1}_{2}.ods').format(admin.adminDir, bulletins, periode)
    odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
    fileName = QtWidgets.QFileDialog.getSaveFileName(
        main, odsTitle, proposedName, odsExt)
    fileName = utils_functions.verifyLibs_fileName(fileName)
    if fileName == '':
        return

    import utils_export
    utils_functions.doWaitCursor()
    exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
    try:
        # connexion aux bases de données :
        query_admin = utils_db.query(admin.db_admin)
        query_commun = utils_db.query(main.db_commun)
        admin.openDB(main, 'resultats')
        query_resultats = utils_db.query(admin.db_resultats)
        # récupération des classes :
        classes = {'LIST': [], }
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            classe = query_commun.value(1)
            classeType = int(query_commun.value(2))
            classes['LIST'].append(classe)
            classes[classe] = classeType
        for classe in classes['LIST']:
            exportDatas['TABLES_LIST'].append(classe)
            exportDatas['TABLES_PARAMS'][classe] = {'title': classe}

        # récupération des profs :
        profs = {}
        for (id_prof, file_prof, nom, prenom) in admin.PROFS:
            profs[id_prof] = utils_functions.u('{0} {1}').format(nom, prenom)

        # commandes utiles :
        commandLine_idClasse = utils_functions.u(
            'SELECT "id_eleve" '
            'FROM eleves '
            'WHERE id_eleve<0 AND Classe="{0}"')
        commandLine_cpt = (
            'SELECT DISTINCT * FROM bulletin '
            'WHERE Competence!="" '
            'AND (classeType={0} OR classeType=-1) '
            'ORDER BY ordre')
        commandLine_results = utils_functions.u(
            'SELECT * FROM bilans '
            'WHERE id_eleve IN ({0}) '
            'AND name IN ({1}) '
            'ORDER BY id_eleve DESC')
        commandLine_details = utils_functions.u(
            'SELECT * FROM bilans_details '
            'WHERE id_eleve IN ({0}) '
            'AND name IN ({1}) '
            'ORDER BY matiere')
        commandLine_matieres = (
            'SELECT * FROM eleve_prof '
            'WHERE id_eleve IN ({0})')
        commandLine_resultsMatieres = (
            'SELECT * FROM syntheses '
            'WHERE id_eleve IN ({0}) '
            'ORDER BY id_eleve DESC')
        commandLine_detailsMatieres = (
            'SELECT * FROM persos '
            'WHERE id_eleve IN ({0}) '
            'ORDER BY name')

        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Export bulletin assessments to ODS'), 
            tags=('h2'))
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Creating a tab for each class'), 
            tags=('h4'))
        # les onglets des classes :
        for classe in classes['LIST']:
            utils_functions.afficheMessage(main, classe, editLog=True)
            # id de la classe :
            id_classe = 0
            query_resultats = utils_db.queryExecute(
                commandLine_idClasse.format(classe), 
                query=query_resultats)
            while query_resultats.next():
                id_classe = int(query_resultats.value(0))
            # liste des élèves :
            students = {'LIST': [], }
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (classe, )}, 
                order=['NOM', 'Prenom'], 
                query=query_admin)
            for record in queryList:
                students['LIST'].append(record[0])
                #students[record[0]] = (record[2], record[3])
                students[record[0]] = utils_functions.u('{0} {1}').format(record[2], record[3])
            id_students = utils_functions.array2string(students['LIST'])
            id_students = '{0}, {1}'.format(id_students, id_classe)

            # les colonnes (bilans) :
            columns = {'BALANCES': [], 'SUBJECTS': [], }
            query_commun = utils_db.queryExecute(
                commandLine_cpt.format(classes[classe]), 
                query=query_commun)
            while query_commun.next():
                name = query_commun.value(1)
                label = query_commun.value(5)
                columns['BALANCES'].append(name)
                columns[name] = {'label': label, 'details': {'LIST': [], }, }
            columnsText = utils_functions.array2string(columns['BALANCES'], text=True)
            # résultats (bilans) :
            results = {'LIST': [], }
            for id_student in students['LIST']:
                results[id_student] = {}
            results[id_classe] = {}
            query_resultats = utils_db.queryExecute(
                commandLine_results.format(id_students, columnsText), 
                query=query_resultats)
            while query_resultats.next():
                id_student = int(query_resultats.value(0))
                name = query_resultats.value(1)
                value = query_resultats.value(2)
                results[id_student][name] = value
                if id_student > -1:
                    if not(value in columns[name]):
                        columns[name][value] = 0
                    columns[name][value] += 1
            # détails des résultats (bilans) :
            details = {}
            for name in columns['BALANCES']:
                details[name] = {'LIST': [], }
            query_resultats = utils_db.queryExecute(
                commandLine_details.format(id_students, columnsText), 
                query=query_resultats)
            while query_resultats.next():
                id_student = int(query_resultats.value(0))
                name = query_resultats.value(1)
                id_prof = int(query_resultats.value(2))
                matiere = query_resultats.value(3)
                value = query_resultats.value(4)
                if not(name in columns):
                    continue
                if not((matiere, id_prof) in columns[name]['details']):
                    columns[name]['details']['LIST'].append((matiere, id_prof))
                    columns[name]['details'][(matiere, id_prof)] = {}
                columns[name]['details'][(matiere, id_prof)][id_student] = value
                if not(value in columns[name]['details'][(matiere, id_prof)]):
                    columns[name]['details'][(matiere, id_prof)][value] = 0
                columns[name]['details'][(matiere, id_prof)][value] += 1

            # les matières :
            matieres = {'LIST': [], 'PERSOS': {}}
            query_resultats = utils_db.queryExecute(
                commandLine_matieres.format(id_students), 
                query=query_resultats)
            while query_resultats.next():
                id_prof = int(query_resultats.value(0))
                id_student = int(query_resultats.value(1))
                matiere = query_resultats.value(2)
                if not(matiere in matieres):
                    matieres[matiere] = {'LIST': [], 'VALID': []}
                if not(id_prof in matieres[matiere]):
                    matieres[matiere]['LIST'].append(id_prof)
                    matieres[matiere][id_prof] = []
                matieres[matiere][id_prof].append(id_student)
            for matiere in admin.MATIERES['BLT']:
                if matiere in matieres:
                    matieres['LIST'].append(matiere)
                    matieres['PERSOS'][matiere] = []
                    for i in range(main.limiteBLTPerso):
                        perso = utils_functions.u(
                            '{0}{1}').format(matiere, i + 1)
                        matieres['PERSOS'][perso] = matiere
                        if i < 9:
                            perso = utils_functions.u(
                                '{0}-0{1}').format(matiere, i + 1)
                        else:
                            perso = utils_functions.u(
                                '{0}-{1}').format(matiere, i + 1)
                        matieres['PERSOS'][perso] = matiere
            for (matiere, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
                if matiere in matieres:
                    matieres['LIST'].append(matiere)
                    matieres['PERSOS'][matiere] = []
                    for i in range(main.limiteBLTPerso):
                        perso = utils_functions.u(
                            '{0}{1}').format(matiere, i + 1)
                        matieres['PERSOS'][perso] = matiere
                        if i < 9:
                            perso = utils_functions.u(
                                '{0}-0{1}').format(matiere, i + 1)
                        else:
                            perso = utils_functions.u(
                                '{0}-{1}').format(matiere, i + 1)
                        matieres['PERSOS'][perso] = matiere
            # résultats (matières) :
            query_resultats = utils_db.queryExecute(
                commandLine_resultsMatieres.format(id_students), 
                query=query_resultats)
            while query_resultats.next():
                id_student = int(query_resultats.value(0))
                matiere = query_resultats.value(1)
                value = query_resultats.value(2)
                id_prof = int(query_resultats.value(3))
                if not(matiere in matieres['LIST']):
                    continue
                if not(id_prof in matieres[matiere]['VALID']):
                    matieres[matiere]['VALID'].append(id_prof)
                results[id_student][(matiere, id_prof)] = value
                if id_student > -1:
                    if not((matiere, id_prof) in columns):
                        columns[(matiere, id_prof)] = {'label': label, 'details': {'LIST': [], }, }
                    if not(value in columns[(matiere, id_prof)]):
                        columns[(matiere, id_prof)][value] = 0
                    columns[(matiere, id_prof)][value] += 1
            # les colonnes (matières) :
            for matiere in matieres['LIST']:
                for id_prof in matieres[matiere]['LIST']:
                    if id_prof in matieres[matiere]['VALID']:
                        columns['SUBJECTS'].append((matiere, id_prof))
                        label = matiere
                        if not((matiere, id_prof) in columns):
                            columns[(matiere, id_prof)] = {'label': label, 'details': {'LIST': [], }, }
            # détails des résultats (matières) :
            details = {}
            for (matiere, id_prof) in columns['SUBJECTS']:
                details[(matiere, id_prof)] = {'LIST': [], }
            query_resultats = utils_db.queryExecute(
                commandLine_detailsMatieres.format(id_students), 
                query=query_resultats)
            while query_resultats.next():
                id_student = int(query_resultats.value(0))
                perso = query_resultats.value(1)
                label = query_resultats.value(2)
                value = query_resultats.value(3)
                id_prof = int(query_resultats.value(5))
                matiere = matieres['PERSOS'].get(perso, '')
                if not((matiere, id_prof) in columns):
                    continue
                if not(perso in matieres['PERSOS'][matiere]):
                    matieres['PERSOS'][matiere].append(perso)
                if not(perso in columns[(matiere, id_prof)]['details']):
                    columns[(matiere, id_prof)]['details']['LIST'].append(perso)
                    columns[(matiere, id_prof)]['details'][perso] = {}
                columns[(matiere, id_prof)]['details'][perso][id_student] = value
                if not(value in columns[(matiere, id_prof)]['details'][perso]):
                    columns[(matiere, id_prof)]['details'][perso][value] = 0
                columns[(matiere, id_prof)]['details'][perso][value] += 1

            # calcul des pourcentages :
            for name in columns['BALANCES']:
                values = {}
                total = 0
                for value in utils.itemsValues:
                    values[value] = columns[name].get(value, 0)
                    total += values[value]
                if total > 0:
                    for value in utils.itemsValues:
                        percent = int(round(values[value] * 100 / total))
                        if percent > 0:
                            columns[name][value] = percent
                for (matiere, id_prof) in columns[name]['details']['LIST']:
                    values = {}
                    total = 0
                    for value in utils.itemsValues:
                        values[value] = columns[name]['details'][(matiere, id_prof)].get(value, 0)
                        total += values[value]
                    if total > 0:
                        for value in utils.itemsValues:
                            percent = int(round(values[value] * 100 / total))
                            if percent > 0:
                                columns[name]['details'][(matiere, id_prof)][value] = percent
            for (matiere, id_prof) in columns['SUBJECTS']:
                values = {}
                total = 0
                for value in utils.itemsValues:
                    values[value] = columns[(matiere, id_prof)].get(value, 0)
                    total += values[value]
                if total > 0:
                    for value in utils.itemsValues:
                        percent = int(round(values[value] * 100 / total))
                        if percent > 0:
                            columns[(matiere, id_prof)][value] = percent
                for perso in columns[(matiere, id_prof)]['details']['LIST']:
                    values = {}
                    total = 0
                    for value in utils.itemsValues:
                        values[value] = columns[(matiere, id_prof)]['details'][perso].get(value, 0)
                        total += values[value]
                    if total > 0:
                        for value in utils.itemsValues:
                            percent = int(round(values[value] * 100 / total))
                            if percent > 0:
                                columns[(matiere, id_prof)]['details'][perso][value] = percent

            # on peut créer le fichier ods :
            theList = []
            columnWidth = '1.0cm'
            detailsWidth = '0.5cm'
            detailsFont = '6'
            separatorWidth = '0.1cm'
            separatorColor = utils.colorGray.name()
            separatorCell = {'value': '', 'cellColor':separatorColor}
            # ligne des titres :
            titles = []
            titles.append({
                'value': QtWidgets.QApplication.translate('main', 'STUDENT'), 
                'bold': True, 
                'alignHorizontal': 'left', 
                'columnWidth': '6.0cm', 
                'rowHeight': '2.0cm'})
            for name in columns['BALANCES']:
                titles.append({
                    'value': name, 
                    'alignHorizontal': 'center', 
                    'rotation': '90', 
                    'bold': True, 
                    'columnWidth': columnWidth})
                for (matiere, id_prof) in columns[name]['details']['LIST']:
                    titles.append({
                        'value': matiere, 
                        'alignHorizontal': 'center', 
                        'rotation': '90', 
                        'bold': False, 
                        'fontSize':detailsFont, 
                        'columnWidth':detailsWidth})
            titles.append({
                'value': '', 
                'columnWidth':separatorWidth, 
                'cellColor':separatorColor})
            for (matiere, id_prof) in columns['SUBJECTS']:
                titles.append({
                    'value': matiere, 
                    'alignHorizontal': 'center', 
                    'rotation': '90', 
                    'bold': True, 
                    'columnWidth': columnWidth})
                for perso in columns[(matiere, id_prof)]['details']['LIST']:
                    titles.append({
                        'value': perso, 
                        'alignHorizontal': 'center', 
                        'rotation': '90', 
                        'bold': False, 
                        'fontSize':detailsFont, 
                        'columnWidth':detailsWidth})
            percentsColumns = {
                'BILANS': {'total': 0, }, 
                'DETAILS': {'total': 0, }}
            titles.append({
                'value': '', 
                'columnWidth':separatorWidth, 
                'cellColor':separatorColor})
            for v in utils.itemsValues:
                titles.append({
                    'value': utils_functions.u(utils.affichages[v][0]), 
                    'alignHorizontal': 'center', 
                    'bold': True, 
                    'columnWidth': columnWidth})
            for v in utils.itemsValues:
                titles.append({
                    'value': utils_functions.u(utils.affichages[v][0]), 
                    'alignHorizontal': 'center', 
                    'fontSize':detailsFont, 
                    'columnWidth':detailsWidth})
            theList.append(titles)
            # lignes des élèves :
            for id_student in students['LIST']:
                percentsColumns['BILANS']['total'] = 0
                percentsColumns['DETAILS']['total'] = 0
                for v in utils.itemsValues:
                    percentsColumns['BILANS'][v] = 0
                    percentsColumns['DETAILS'][v] = 0
                line = []
                line.append({'value': students[id_student]})
                for name in columns['BALANCES']:
                    value = results[id_student].get(name, '')
                    if value != '':
                        percentsColumns['BILANS'][value] += 1
                    try:
                        color = utils.affichages[value][2]
                    except:
                        color = utils.colorWhite.name()
                    value = utils_functions.valeur2affichage(value)
                    line.append({
                        'value': value, 
                        'cellColor':color, 
                        'alignHorizontal': 'center', 
                        'bold': True})
                    for (matiere, id_prof) in columns[name]['details']['LIST']:
                        value = columns[name]['details'][(matiere, id_prof)].get(id_student, '')
                        if value != '':
                            percentsColumns['DETAILS'][value] += 1
                        try:
                            color = utils.affichages[value][2]
                        except:
                            color = utils.colorWhite.name()
                        value = utils_functions.valeur2affichage(value)
                        line.append({
                            'value': value, 
                            'cellColor':color, 
                            'fontSize':detailsFont, 
                            'alignHorizontal': 'center', 
                            'bold': False})
                line.append(separatorCell)
                for (matiere, id_prof) in columns['SUBJECTS']:
                    value = results[id_student].get((matiere, id_prof), '')
                    if value != '':
                        percentsColumns['BILANS'][value] += 1
                    try:
                        color = utils.affichages[value][2]
                    except:
                        color = utils.colorWhite.name()
                    value = utils_functions.valeur2affichage(value)
                    line.append({
                        'value': value, 
                        'cellColor':color, 
                        'alignHorizontal': 'center', 
                        'bold': True})
                    for perso in columns[(matiere, id_prof)]['details']['LIST']:
                        value = columns[(matiere, id_prof)]['details'][perso].get(id_student, '')
                        if value != '':
                            percentsColumns['DETAILS'][value] += 1
                        try:
                            color = utils.affichages[value][2]
                        except:
                            color = utils.colorWhite.name()
                        value = utils_functions.valeur2affichage(value)
                        line.append({
                            'value': value, 
                            'cellColor':color, 
                            'fontSize':detailsFont, 
                            'alignHorizontal': 'center', 
                            'bold': False})
                line.append(separatorCell)
                for v in utils.itemsValues:
                    percentsColumns['BILANS']['total'] += percentsColumns['BILANS'][v]
                    percentsColumns['DETAILS']['total'] += percentsColumns['DETAILS'][v]
                total = percentsColumns['BILANS']['total']
                if total > 0:
                    for v in utils.itemsValues:
                        percent = int(round(
                            percentsColumns['BILANS'][v] * 100 / total))
                        percentsColumns['BILANS'][v] = percent
                total = percentsColumns['DETAILS']['total']
                if total > 0:
                    for v in utils.itemsValues:
                        percent = int(round(
                            percentsColumns['DETAILS'][v] * 100 / total))
                        percentsColumns['DETAILS'][v] = percent
                for v in utils.itemsValues:
                    value = percentsColumns['BILANS'][v]
                    if value > 0:
                        line.append({
                            'value': percentsColumns['BILANS'][v], 
                            'type': utils.INTEGER, 
                            'alignHorizontal': 'center', 
                            'bold': True})
                    else:
                        line.append({'value': ''})
                for v in utils.itemsValues:
                    value = percentsColumns['DETAILS'][v]
                    if value > 0:
                        line.append({
                            'value': percentsColumns['DETAILS'][v], 
                            'type': utils.INTEGER, 
                            'fontSize':detailsFont, 
                            'alignHorizontal': 'center'})
                    else:
                        line.append({'value': ''})
                theList.append(line)
            # ligne de séparation :
            line = [{
                'value': '', 
                'cellColor': separatorColor, 
                'rowHeight': separatorWidth}]
            for name in columns['BALANCES']:
                line.append(separatorCell)
                for (matiere, id_prof) in columns[name]['details']['LIST']:
                    line.append(separatorCell)
            line.append(separatorCell)
            for (matiere, id_prof) in columns['SUBJECTS']:
                line.append(separatorCell)
                for perso in columns[(matiere, id_prof)]['details']['LIST']:
                    line.append(separatorCell)
            line.append(separatorCell)
            for v in utils.itemsValues:
                line.append(separatorCell)
                line.append(separatorCell)
            theList.append(line)
            # ligne du groupe :
            line = [{
                'value': QtWidgets.QApplication.translate('main', 'GROUP'), 
                'bold': True}]
            for name in columns['BALANCES']:
                value = results[id_classe].get(name, '')
                try:
                    color = utils.affichages[value][2]
                except:
                    color = utils.colorWhite.name()
                value = utils_functions.valeur2affichage(value)
                line.append({
                    'value': value, 
                    'cellColor':color, 
                    'alignHorizontal': 'center', 
                    'bold': True})
                for (matiere, id_prof) in columns[name]['details']['LIST']:
                    line.append({'value': ''})
            line.append(separatorCell)
            for (matiere, id_prof) in columns['SUBJECTS']:
                value = results[id_classe].get((matiere, id_prof), '')
                if value == '':
                    value = results[id_classe].get((matiere, -1), '')
                try:
                    color = utils.affichages[value][2]
                except:
                    color = utils.colorWhite.name()
                value = utils_functions.valeur2affichage(value)
                line.append({
                    'value': value, 
                    'cellColor':color, 
                    'alignHorizontal': 'center', 
                    'bold': True})
                for perso in columns[(matiere, id_prof)]['details']['LIST']:
                    line.append({'value': ''})
            line.append(separatorCell)
            theList.append(line)
            # lignes des pourcentages :
            for v in utils.itemsValues:
                text = utils_functions.u(
                    '{0} (%)').format(utils.affichages[v][0])
                line = [{'value': text, 'bold': True, }, ]
                for name in columns['BALANCES']:
                    value = columns[name].get(v, 0)
                    if value > 0:
                        line.append({
                            'value': value, 
                            'alignHorizontal': 'center', 
                            'type': utils.INTEGER, 
                            'bold': True})
                    else:
                        line.append({'value': ''})
                    for (matiere, id_prof) in columns[name]['details']['LIST']:
                        value = columns[name]['details'][(matiere, id_prof)].get(v, 0)
                        if value > 0:
                            line.append({
                                'value': value, 
                                'fontSize':detailsFont, 
                                'alignHorizontal': 'center', 
                                'type': utils.INTEGER})
                        else:
                            line.append({'value': ''})
                line.append(separatorCell)
                for (matiere, id_prof) in columns['SUBJECTS']:
                    value = columns[(matiere, id_prof)].get(v, 0)
                    if value > 0:
                        line.append({
                            'value': value, 
                            'alignHorizontal': 'center', 
                            'type': utils.INTEGER, 
                            'bold': True})
                    else:
                        line.append({'value': ''})
                    for perso in columns[(matiere, id_prof)]['details']['LIST']:
                        value = columns[(matiere, id_prof)]['details'][perso].get(v, 0)
                        if value > 0:
                            line.append({
                                'value': value, 
                                'fontSize':detailsFont, 
                                'alignHorizontal': 'center', 
                                'type': utils.INTEGER})
                        else:
                            line.append({'value': ''})
                line.append(separatorCell)
                theList.append(line)
            # ligne des profs :
            line = [{'value': '', 'rowHeight': '4.0cm'}, ]
            for name in columns['BALANCES']:
                line.append({'value': '', })
                for (matiere, id_prof) in columns[name]['details']['LIST']:
                    line.append({
                        'value': profs[id_prof], 
                        'fontSize':detailsFont, 
                        'alignHorizontal': 'center', 
                        'rotation': '90'})
            line.append(separatorCell)
            for (matiere, id_prof) in columns['SUBJECTS']:
                line.append({
                    'value': profs[id_prof], 
                    'fontSize':detailsFont, 
                    'alignHorizontal': 'center', 
                    'rotation': '90'})
                for perso in columns[(matiere, id_prof)]['details']['LIST']:
                    line.append({'value': ''})
            line.append(separatorCell)
            theList.append(line)
            exportDatas[classe] = theList
        # on termine :
        utils_export.exportDic2ods(main, exportDatas, fileName, msgFin=False)
        utils_functions.afficheMsgFinOpen(main, fileName)
    except:
        message = QtWidgets.QApplication.translate(
            'main', 'Failed to save {0}').format(fileName)
        utils_functions.afficheMsgPb(main, message)
    finally:
        utils_functions.restoreCursor()




###########################################################"
#   AIDE À LA CRÉATION DES MODÈLES
###########################################################"

def showFields(main, msgFin=True):
    """
    affiche la liste des champs disponibles
    """

    def printFields(main, fields=()):
        for field in fields:
            if field != '':
                utils_functions.afficheMessage(main, '${' + field + '}', editLog=True)
            else:
                utils_functions.afficheMessage(main, '-'*10, editLog=True)

    utils_functions.doWaitCursor()
    main.editLog.clear()
    try:
        # connexion aux bases de données :
        query_commun = utils_db.query(main.db_commun)
        # titre :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'FIELDS AVAILABLE FOR MODELS'),
           tags=('h1'))
        # Établissement :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Fields for the school:'),
            tags=('h2'))
        fields = (
            'ACADEMIE',
            'DEPARTEMENT',
            'ETAB_NOM',
            'ETAB_ADRESSE',
            'ETAB_TELEPHONE',
            'ANNEE_SCOLAIRE', )
        printFields(main, fields)
        # Document :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Fields for the document:'),
            tags=('h2'))
        fields = (
            'LABEL DOCUMENT',
            '',
            'RESULTATS PAR MATIERE',
            '',
            'SERIE',
            'SESSION',
            'TOTAL POINTS',
            'NOTNONCA', )
        printFields(main, fields)
        # Élève :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Fields for the student:'),
            tags=('h2'))
        fields = (
            'id_eleve',
            'INE',
            'NomFichier',
            'DateFichier',
            'Classe',
            'NOM Prenom',
            'NOM',
            'Prenom',
            'Date Naissance',
            'Photo',
            'AnDernier',
            'Adresse', )
        printFields(main, fields)
        # Intitulés des matières :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Labels of Subjects:'),
            tags=('h2'))
        fields = []
        for matiereCode in admin.MATIERES['ALL']:
            fields.append(matiereCode + '-Label')
        printFields(main, fields)
        # VS et PP :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Fields for special subjects:'),
            tags=('h2'))
        fields = ['VIE SCOLAIRE', 'PROF PRINCIPAL']
        for (matiereCode, temp_id_prof) in admin.MATIERES['BLTSpeciales']:
            field = matiereCode + '-appreciation'
            fields.append(field)
        for i in range(4):
            fields.append('absences-{0}Label'.format(i))
            fields.append('absences-{0}'.format(i))
        printFields(main, fields)

        # Champs liés à une CPT partagée :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Fields available for each shared competences:'),
            tags=('h2'))
        # structure des champs :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Where XXX is the name of competence:'),
            tags=('h3'))
        fields = (
            'XXX-Label',
            'XXX',
            'XXX-B',
            'XXX-Classe',
            'XXX-Classe-B',
            'XXX-details', )
        printFields(main, fields)
        # bulletin :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'Names from table "bulletin" (shared competences from bulletin):'),
            tags=('h3'))
        commandLine = 'SELECT * FROM bulletin WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        namesBLT = []
        names = ''
        while query_commun.next():
            name = query_commun.value(1)
            namesBLT.append(name)
            if names == '':
                names = utils_functions.u('{0}').format(name)
            else:
                names = utils_functions.u('{0}, {1}').format(names, name)
        utils_functions.afficheMessage(main, names, editLog=True)
        # referentiel :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'Names from table "referentiel" (competences from referential):'),
            tags=('h3'))
        commandLine = 'SELECT * FROM referentiel WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        namesREF = []
        names = ''
        while query_commun.next():
            name = query_commun.value(1)
            namesREF.append(name)
            if names == '':
                names = utils_functions.u('{0}').format(name)
            else:
                names = utils_functions.u('{0}, {1}').format(names, name)
        utils_functions.afficheMessage(main, names, editLog=True)
        # confidentiel :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'Names from table "confidentiel" (confidential competences):'),
            tags=('h3'))
        commandLine = 'SELECT * FROM confidentiel WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        namesCFD = []
        names = ''
        while query_commun.next():
            name = query_commun.value(1)
            namesCFD.append(name)
            if names == '':
                names = utils_functions.u('{0}').format(name)
            else:
                names = utils_functions.u('{0}, {1}').format(names, name)
        utils_functions.afficheMessage(main, names, editLog=True)
        # suivi :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Names from table "suivi" (followed competences):'),
            tags=('h3'))
        commandLine = 'SELECT * FROM suivi WHERE Competence!=""'
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        namesSVI = []
        names = ''
        while query_commun.next():
            name = query_commun.value(1)
            namesSVI.append(name)
            if names == '':
                names = utils_functions.u('{0}').format(name)
            else:
                names = utils_functions.u('{0}, {1}').format(names, name)
        utils_functions.afficheMessage(main, names, editLog=True)
        utils_functions.afficheMessage(
            main, '*'*50, editLog=True, p=True)

        # pour les résultats des matières (bilans persos) :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Fields for subjects (personal bilans of teachers) :'),
            tags=('h2'))
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'Where XXX is the name of subject and nn a 2-digit number under limiteBLTPerso:'),
            tags=('h3'))
        fields = (
            'XXX-appreciation',
            'XXX-nn',
            'XXX-nn-B',
            'XXX-nn-Classe',
            'XXX-nn-Classe-B',
            'XXX-nn-Label', )
        printFields(main, fields)
        # noms :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Available names of Subjects:'),
            tags=('h3'))
        namesMatieres = []
        names = ''
        for matiereCode in admin.MATIERES['BLT']:
            namesMatieres.append(matiereCode)
            if names == '':
                names = utils_functions.u('{0}').format(matiereCode)
            else:
                names = utils_functions.u('{0}, {1}').format(names, matiereCode)
        utils_functions.afficheMessage(main, names, editLog=True)
        utils_functions.afficheMessage(
            main, '*'*50, editLog=True, p=True)

        # pour les résultats des matières (bilans persos) :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 'Details of the results by subject for shared competences:'),
            tags=('h2'))
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate(
                'main', 
                'Where XXX is the name of competence and YYY is the name of subject:'),
            tags=('h3'))
        fields = (
            'XXX-YYY',
            'XXX-YYY-B', )
        printFields(main, fields)
        # noms :
        utils_functions.afficheMessage(
            main, 
            QtWidgets.QApplication.translate('main', 'Available names:'),
            tags=('h3'))
        for namesList in (namesBLT, namesREF, namesCFD, namesSVI):
            names = ''
            for cpt in namesList:
                for matiereCode in namesMatieres:
                    name = utils_functions.u('{0}-{1}').format(cpt, matiereCode)
                    if names == '':
                        names = utils_functions.u('{0}').format(name)
                    else:
                        names = utils_functions.u('{0}, {1}').format(names, name)
            utils_functions.afficheMessage(main, names, editLog=True)

        utils_functions.afficheMessage(main, 'FINI', tags=('h2'))
        if msgFin:
            utils_functions.afficheMsgFin(main, timer=5)
    except:
        utils_functions.afficheMsgPb(main)
    finally:
        utils_functions.restoreCursor()


def table2HtmlModele(main, table='bulletin', classeType=-1):
    """
    Aide à la création d'un modèle html de bilan, bulletin ou référentiel.
    """

    def doInDiv(title):
        result = '\n'
        result += '\n<div style="page-break-inside:avoid">'
        result += utils_functions.u('\n    <h3>{0}</h3>').format(title)
        return result

    def doInTable(table, title):
        result = '\n    <table width=1000 cellpadding=2 cellspacing=0 border="2">'
        if table == 'bulletin':
            result += '\n        <col width=800>'
            result += '\n        <col width=120>'
            result += '\n        <col width=80>'
        else:
            result += '\n        <col width=880>'
            result += '\n        <col width=120>'
        result += '\n        <tr>'
        result += utils_functions.u('\n            <th class="titre" align=left>{0}</th>').format(title)
        if table == 'bulletin':
            eleveText = QtWidgets.QApplication.translate('main', 'Student')
            classeText = QtWidgets.QApplication.translate('main', 'Class')
            result += utils_functions.u('\n            <th class="titre">{0}</th>').format(eleveText)
            result += utils_functions.u('\n            <th class="titre">{0}</th>').format(classeText)
        else:
            result += '\n            <th class="titre"></th>'
        result += '\n        </tr>'
        return result

    htmlText = ''
    inTable = False
    inDiv = False
    query_commun = utils_db.query(main.db_commun)
    commandLine = (
        'SELECT * FROM {0} '
        'WHERE (classeType={1} OR classeType=-1) '
        'ORDER BY ordre').format(table, classeType)
    query_commun = utils_db.queryExecute(commandLine, query=query_commun)
    while query_commun.next():
        code = query_commun.value(1)
        field = utils_functions.u('${' + code + '}')
        if table == 'bulletin':
            fieldClasse = utils_functions.u('${' + code + '-Classe}')
        fieldDetails = utils_functions.u('${' + code + '-details}')
        Titre1 = query_commun.value(2)
        Titre2 = query_commun.value(3)
        Titre3 = query_commun.value(4)
        Competence = query_commun.value(5)
        T1 = query_commun.value(6)
        T2 = query_commun.value(7)
        T3 = query_commun.value(8)
        Cpt = query_commun.value(9)
        if Cpt != '':
            if not(inDiv):
                htmlText += doInDiv('')
                inDiv = True
            if not(inTable):
                htmlText += doInTable(table, '')
                inTable = True
            htmlText += '\n        <tr>'
            htmlText += utils_functions.u('\n            <td>{0}').format(Competence)
            htmlText += utils_functions.u('\n            {0}</td>').format(fieldDetails)
            htmlText += utils_functions.u('\n            <td>{0}</td>').format(field)
            if table == 'bulletin':
                htmlText += utils_functions.u('\n            <td>{0}</td>').format(fieldClasse)
            htmlText += '\n        </tr>'
        elif T3 != '':
            if inTable:
                htmlText += '\n    </table>'
                inTable = False
            if not(inDiv):
                htmlText += doInDiv('')
                inDiv = True
            htmlText += doInTable(table, Titre3)
            inTable = True
        elif T2 != '':
            if inTable:
                htmlText += '\n    </table>'
                inTable = False
            if inDiv:
                htmlText += '\n</div>'
                inDiv = False
            htmlText += doInDiv(Titre2)
            inDiv = True
        elif T1 != '':
            if inTable:
                htmlText += '\n    </table>'
                inTable = False
            if inDiv:
                htmlText += '\n</div>'
                inDiv = False
            for i in range(10):
                htmlText += '\n'
            htmlText += utils_functions.u('\n<!--{0}-->').format(code)
            htmlText += utils_functions.u('\n<h2>{0}</h2>').format(Titre1)
    if inTable:
        htmlText += '\n    </table>'
    if inDiv:
        htmlText += '\n</div>'
    for i in range(2):
        htmlText += '\n'
    return htmlText


class ConfigureHtmlModeleDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None):
        super(ConfigureHtmlModeleDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Configuring the html template'))

        # choix de la table à utiliser :
        tableLabel = QtWidgets.QLabel(utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Table to use:')))
        self.tableComboBox = QtWidgets.QComboBox()
        self.tableComboBox.setMinimumWidth(200)
        for table in ('bulletin', 'referentiel', 'confidentiel'):
            self.tableComboBox.addItem(table)

        # choix du type de classe :
        classTypeLabel = QtWidgets.QLabel(utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Class type:')))
        self.classTypeComboBox = QtWidgets.QComboBox()
        self.classTypeComboBox.setMinimumWidth(200)

        # choix du fichier modèle de base :
        baseLabel = QtWidgets.QLabel(utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Base model:')))
        self.baseComboBox = QtWidgets.QComboBox()
        self.baseComboBox.setMinimumWidth(200)

        # choix du fichier modèle de synthèse :
        syntheseLabel = QtWidgets.QLabel(utils_functions.u('<p><b>{0}</b></p>').format(
            QtWidgets.QApplication.translate('main', 'Synthesis model:')))
        self.syntheseComboBox = QtWidgets.QComboBox()
        self.syntheseComboBox.setMinimumWidth(200)

        # avec ou sans les composantes du socle (LSU) :
        self.socleComponentsCheckBox = QtWidgets.QCheckBox(
            QtWidgets.QApplication.translate(
                'main', 'Components of the socle'))
        self.socleComponentsCheckBox.setChecked(True)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(tableLabel, 1, 0)
        grid.addWidget(self.tableComboBox, 1, 1)
        grid.addWidget(classTypeLabel, 2, 0)
        grid.addWidget(self.classTypeComboBox, 2, 1)
        grid.addWidget(baseLabel, 3, 0)
        grid.addWidget(self.baseComboBox, 3, 1)
        grid.addWidget(syntheseLabel, 4, 0)
        grid.addWidget(self.syntheseComboBox, 4, 1)
        grid.addWidget(self.socleComponentsCheckBox, 5, 1)
        grid.addWidget(buttonBox, 10, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

        # on remplit les comboBox :
        query_commun = utils_db.query(self.main.db_commun)
        commandLine = utils_db.q_selectAllFromOrder.format(
            'classestypes', 'id')
        query_commun = utils_db.queryExecute(commandLine, query=query_commun)
        while query_commun.next():
            id = int(query_commun.value(0))
            name = query_commun.value(1)
            self.classTypeComboBox.addItem(utils_functions.u(name), id)
        entryInfoList = QtCore.QDir(
            admin.modelesDir).entryInfoList(['modele_base*.modl'])
        for fileInfo in entryInfoList:
            self.baseComboBox.addItem(fileInfo.fileName())
        self.syntheseComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'No synthesis'))
        entryInfoList = QtCore.QDir(
            admin.modelesDir).entryInfoList(['modele_synthese*.modl'])
        for fileInfo in entryInfoList:
            self.syntheseComboBox.addItem(fileInfo.fileName())

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('edit-modeles', 'admin')


def createHtmlModele(main, msgFin=True):
    """
    Créer son propre modèle html de bulletin ou autre.
    * D'abord on sélectionne le nom du fichier à créer.
    * Ensuite on ouvre modèle vide (modele_base*.modl) et on récupère son contenu.
    * On y remplace "<!--PARTIE À REMPLACER-->" par 
    le retour de la procédure table2HtmlModele.
    * Enfin on enregistre le fichier.
    """
    import utils_html

    # définition des réglages :
    classeType = -1
    baseFileName = ''
    syntheseFileName = ''
    dialog = ConfigureHtmlModeleDlg(parent=main)
    if dialog.exec_() == QtWidgets.QDialog.Accepted:
        table = dialog.tableComboBox.currentText()
        index = dialog.classTypeComboBox.currentIndex()
        classeType = dialog.classTypeComboBox.itemData(index)
        index = dialog.baseComboBox.currentIndex()
        baseFileName = dialog.baseComboBox.itemText(index)
        index = dialog.syntheseComboBox.currentIndex()
        if index > 0:
            syntheseFileName = dialog.syntheseComboBox.itemText(index)
        socleComponents = dialog.socleComponentsCheckBox.isChecked()
    else:
        return

    # choix du fichier à enregistrer :
    fileName = QtWidgets.QFileDialog.getSaveFileName(
        main, 
        QtWidgets.QApplication.translate('main', 'Save html File'),
        admin.modelesDir, 
        QtWidgets.QApplication.translate('main', 'html files (*.html)'))
    fileName = utils_functions.verifyLibs_fileName(fileName)
    if fileName == '':
        return

    # récupération du contenu de la base du modèle :
    sourceFile = utils_functions.u('{0}/{1}').format(admin.modelesDir, baseFileName)
    inFile = QtCore.QFile(sourceFile)
    if not inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
        message = QtWidgets.QApplication.translate(
            'main', 'Cannot read file {0}:\n{1}.').format(
                sourceFile, inFile.errorString())
        utils_functions.messageBox(main, level='warning', message=message)
        return
    indentSize = 4
    textStream = QtCore.QTextStream(inFile)
    textStream.setCodec('UTF-8')
    htmlCurrent = utils_functions.u(textStream.readAll())
    inFile.close()

    # lancement de table2HtmlModele et récupération du contenu :
    replaceWith = table2HtmlModele(main, table, classeType)
    # Remplacement du repère par le contenu du editLog :
    htmlCurrent = htmlCurrent.replace(
        '<!--RESULTATS COMMUNS-->', utils_functions.u(replaceWith))

    # si ce n'est pas la table bulletin, il n'y a pas de matières ni de Vie scolaire :
    if table == 'bulletin':
        replaceWith = utils_functions.u('${RESULTATS PAR MATIERE}')
    else:
        replaceWith = utils_functions.u('')
    htmlCurrent = htmlCurrent.replace(
        '<!--RESULTATS PAR MATIERE-->', utils_functions.u(replaceWith))
    if table == 'bulletin':
        replaceWith = utils_functions.u('${VIE SCOLAIRE}')
    else:
        replaceWith = utils_functions.u('')
    htmlCurrent = htmlCurrent.replace(
        '<!--VIE SCOLAIRE-->', utils_functions.u(replaceWith))

    # composantes du socle :
    if socleComponents:
        sourceFile = utils_functions.u(
            '{0}/modele_composantes_socle.modl').format(admin.modelesDir)
        inFile = QtCore.QFile(sourceFile)
        if not inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            message = QtWidgets.QApplication.translate(
                'main', 'Cannot read file {0}:\n{1}.').format(
                    sourceFile, inFile.errorString())
            utils_functions.messageBox(main, level='warning', message=message)
            return
        indentSize = 4
        textStream = QtCore.QTextStream(inFile)
        textStream.setCodec('UTF-8')
        replaceWith = utils_functions.u(textStream.readAll())
        inFile.close()
    else:
        replaceWith = ''
    htmlCurrent = htmlCurrent.replace('<!--COMPOSANTES SOCLE-->', replaceWith)
    if utils.OS_NAME[0] == 'win':
        htmlCurrent = utils_html.encodeHtml(htmlCurrent)

    # récupération du contenu du modèle de synthèse :
    if syntheseFileName != '':
        sourceFile = admin.modelesDir + utils_functions.u('/{0}').format(syntheseFileName)
        inFile = QtCore.QFile(sourceFile)
        if not inFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            message = QtWidgets.QApplication.translate(
                'main', 'Cannot read file {0}:\n{1}.').format(
                    sourceFile, inFile.errorString())
            utils_functions.messageBox(main, level='warning', message=message)
            return
        indentSize = 4
        textStream = QtCore.QTextStream(inFile)
        textStream.setCodec('UTF-8')
        replaceWith = utils_functions.u(textStream.readAll())
        inFile.close()
    else:
        replaceWith = ''
    htmlCurrent = htmlCurrent.replace('<!--SYNTHESE-->', replaceWith)
    if utils.OS_NAME[0] == 'win':
        htmlCurrent = utils_html.encodeHtml(htmlCurrent)

    # enregistrement du fichier :
    outFile = QtCore.QFile(fileName)
    if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
        stream = QtCore.QTextStream(outFile)
        stream.setCodec('UTF-8')
        stream << htmlCurrent
        outFile.close()

    # terminé :
    if msgFin:
        utils_functions.afficheMsgFin(main, timer=5)


