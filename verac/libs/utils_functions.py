# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Ce module contient des fonctions utiles au programme.
"""


# importation des modules utiles :
from __future__ import division, print_function
import sys
import os
import logging

# importation des modules perso :
import utils

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



"""
****************************************************
    POUR L'AFFICHAGE DES TEXTES
****************************************************
"""

def myPrint(*args):
    if len(args) > 1:
        print(args)
    else:
        arg = args[0]
        try:
            print(arg)
        except:
            try:
                print(u(arg))
            except:
                try:
                    print(s(arg))
                except:
                    print('PB in myPrint')

def u(text):
    """
    retourne une version unicode de text
    returns a unicode version of text
    """
    result = text
    if utils.PYTHONVERSION >= 30:
        try:
            if isinstance(text, QtCore.QByteArray):
                result = bytes(text).decode('utf-8')
            elif not(isinstance(text, str)):
                result = str(text)
        except:
            try:
                if not(isinstance(text, str)):
                    result = str(text)
            except:
                myPrint('ERROR utils.u', type(text), text)
        # résolution d'un problème avec les QByteArray 
        # (b'aaa' au lieu de 'aaa') :
        if result[:2] in ("b'", 'b"'):
            result = result[2:-1]
    else:
        try:
            result = unicode(text)
        except:
            if isinstance(text, str):
                result = text.decode('utf-8')
            elif isinstance(text, QtCore.QByteArray):
                result = str(text).decode('utf-8')
            else:
                myPrint('ERROR utils.u', type(text), text)
    return result

def s(text, encoding=''):
    # retourne une version str de text
    if utils.PYTHONVERSION >= 30:
        if isinstance(text, str):
            if encoding == '':
                return text
            else:
                return text.encode(encoding)
        else:
            try:
                return str(text)
            except:
                myPrint('ERROR utils.s', type(text), text)
                return text
    else:
        try:
            return text.encode('utf8')
        except:
            if isinstance(text, str):
                return text
            else:
                try:
                    return str(text)
                except:
                    myPrint('ERROR utils.s', type(text), text)
                    return text

def toLong(text):
    # retourne une version int ou long de text
    if utils.PYTHONVERSION >= 30:
        return int(text)
    else:
        return long(text)

def doAscii(text, case='upper'):
    REPLACEMENTS = {
        'A': u('ÁÀÂÃÄÅáàâãäå'),
        'AE': u('Ææ'), 
        'C': u('Çç'), 
        'E': u('ÉÈÊËéèêë'), 
        'I': u('ÍÌÎÏíìîï'), 
        'N': u('Ññ'), 
        'O': u('ÓÒÔÕÖóòôõö'), 
        'OE': u('Œœ'), 
        'SS': u('ẞß'), 
        'U': u('ÚÙÛÜúùûü'), 
        'Y': u('ÝŸýÿ'), 
        '': u('°'), 
        }
    result = u(text)
    for replaceWith in REPLACEMENTS:
        for replaceWhat in REPLACEMENTS[replaceWith]:
            result = result.replace(replaceWhat, replaceWith)
    if case == 'upper':
        result = result.upper()
    elif case == 'lower':
        result = result.lower()
    return result

def doOnlyChars(text, only=''):
    if only == '':
        only = 'abcdefghijklmnopqrstuvwxyz0123456789_-+'
    result = u('')
    for char in text:
        if char in only:
            result = u('{0}{1}').format(result, char)
    return result



"""
****************************************************
    CONVERSIONS
****************************************************
"""

def valeur2affichage(itemText):
    newItemText = ''
    OK = True
    for i in itemText:
        if i in utils.itemsValues:
            newItemText = newItemText + utils.affichages[i][0]
        else:
            OK = False
            return itemText
    if OK:
        return newItemText
    else:
        return itemText

def affichage2valeur(itemText):
    """
    retourne les valeurs internes (A, B, ...) 
    en fonction des valeurs affichées
    """
    newItemText = ''
    for char in itemText:
        for affichage in utils.affichages:
            if char == utils.affichages[affichage][0]:
                newItemText = newItemText + affichage
    return newItemText

findColorDic = {}
def reinitFindColorDic():
    global findColorDic
    findColorDic = {}

def findColor(text, mustCalculIfMany=True):
    """
    Retourne la couleur de la case correspondant au texte affiché.
    text : le texte dont on veut la couleur
    mustCalculIfMany : au cas où text contient plusieurs lettres,
        indique s'il faut calculer la couleur d'après les réglages
        où seulement d'après la dernière lettre.
    """
    color = utils.colorWhite
    try:
        if text == '':
            pass
        elif text in utils.itemsValues:
            color = utils.affichages[text][3]
        elif mustCalculIfMany:
            # un dictionnaire devrait accélérer un peu l'affichage
            global findColorDic
            try:
                color = findColorDic[text]
            except:
                import utils_calculs
                value = utils_calculs.moyenneItem(text)
                color = utils.affichages[value][3]
                findColorDic[text] = color
        else:
            lastChar = text[-1]
            if lastChar in utils.itemsValues:
                color = utils.affichages[lastChar][3]
    except:
        pass
    return color

def code2name(main, code):
    #convertit les codes (ITEM_n ou LABEL_n) en leurs vrais noms
    import utils_db
    name = code
    if code[:5] == "ITEM_":
        id_item = code[5:]
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'items', 'id_item', id_item)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        if query_my.first():
            name = query_my.value(1)
    elif code[:6] == "BILAN_":
        id_bilan = code[6:]
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'bilans', 'id_bilan', id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            name = query_my.value(1)
    return name

def name2code(main, name, item=True):
    #convertit les vrais noms en codes (ITEM_n ou LABEL_n)
    import utils_db
    code = name
    if item:
        commandLine_my = utils_db.qs_prof_AllWhereName.format('items')
        query_my = utils_db.queryExecute(
            {commandLine_my: (name, )}, db=main.db_my)
        while query_my.next():
            id_item = int(query_my.value(0))
            code = u('ITEM_{0}').format(id_item)
    else:
        commandLine_my = utils_db.qs_prof_AllWhereName.format('bilans')
        query_my = utils_db.queryExecute(
            {commandLine_my: (name, )}, db=main.db_my)
        while query_my.next():
            id_bilan = int(query_my.value(0))
            code = u('BILAN_{0}').format(id_bilan)
    return code

def bilanCode2ClassTypeName(main, bilanCode):
    import utils_db
    classTypeName = ''
    try:
        query_commun = utils_db.query(main.db_commun)
        # on teste dans le bulletin :
        commandLine_commun = utils_db.qs_commun_classeType.format(
            'bulletin')
        query_commun = utils_db.queryExecute(
            {commandLine_commun: (bilanCode, )}, query=query_commun)
        while query_commun.next():
            classType = int(query_commun.value(0))
            name = classType2Name(main, classType)
            classTypeName = u('{0} ({1})').format(classTypeName, name)
        if classTypeName != '':
            return classTypeName
        # puis dans le referentiel :
        commandLine_commun = utils_db.qs_commun_classeType.format(
            'referentiel')
        query_commun = utils_db.queryExecute(
            {commandLine_commun: (bilanCode, )}, query=query_commun)
        while query_commun.next():
            classType = int(query_commun.value(0))
            name = classType2Name(main, classType)
            classTypeName = u('{0} ({1})').format(classTypeName, name)
        if classTypeName != '':
            return classTypeName
        # puis dans le confidentiel :
        commandLine_commun = utils_db.qs_commun_classeType.format(
            'confidentiel')
        query_commun = utils_db.queryExecute(
            {commandLine_commun: (bilanCode, )}, query=query_commun)
        while query_commun.next():
            classType = int(query_commun.value(0))
            name = classType2Name(main, classType)
            classTypeName = u('{0} ({1})').format(classTypeName, name)
        return classTypeName
    except:
        return ''

def classType2Name(main, classType):
    if classType == -2:
        return QtWidgets.QApplication.translate('main', 'nowhere')
    elif classType == -1:
        return QtWidgets.QApplication.translate('main', 'everywhere')
    else:
        import utils_db
        name = ''
        commandLine_commun = utils_db.q_selectAllFromWhere.format(
            'classestypes', 'id', classType)
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=main.db_commun)
        while query_commun.next():
            name = query_commun.value(1)
        return name

def name2ClassType(main, name):
    if name == QtWidgets.QApplication.translate('main', 'nowhere'):
        return -2
    elif name == QtWidgets.QApplication.translate('main', 'everywhere'):
        return -1
    else:
        import utils_db
        classType = 0
        commandLine_commun = u('SELECT * FROM classestypes WHERE name=?')
        query_commun = utils_db.queryExecute(
            {commandLine_commun: (name, )}, db=main.db_commun)
        while query_commun.next():
            classType = query_commun.value(0)
        return classType

def equivalence(main, table, id_bilan=-999, code=''):
    # ICI bilans_liens
    """
    En cas de bulletins (ou autres) multiples, renvoie l'id et le code
    du bilan équivalent à celui demandé.
    table : indique la table concernée (bulletin, referentiel ou confidentiel)
    id_bilan, code : du bilan demandé (au moins un des 2 doit être renseigné)
    retourne (id, code) du bilan équivalent.
    Rappels : 2 bilans sont équivalents s'ils ont le même code.
    Le bilan retourné est celui qui a le plus petit id.
    Cela permet de garder une unicité.
    """
    import utils_db
    id1 = id_bilan
    code1 = code
    query_commun = utils_db.query(main.db_commun)
    if (id1 != -999) and (code1 == ''):
        # si on a juste passé l'id, on récupère le code :
        commandLine_commun = utils_db.q_selectAllFromWhere.format(
            table, 'id', id1)
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        if query_commun.first():
            code1 = query_commun.value(1)
    elif (id1 == -999) and (code1 != ''):
        # si on a juste passé le code, on récupère l'id :
        commandLine_commun = u('SELECT * FROM {0} WHERE code=?')
        query_commun = utils_db.queryExecute(
            {commandLine_commun.format(table): (code1, )}, 
            query=query_commun)
        if query_commun.first():
            id1 = int(query_commun.value(0))
    commandLine_commun = (
        'SELECT * FROM equivalences WHERE tableName=? AND id2=?')
    query_commun = utils_db.queryExecute(
        {commandLine_commun: (table, id_bilan)}, query=query_commun)
    if query_commun.first():
        code1 = query_commun.value(1)
        id1 = int(query_commun.value(2))
    return (id1, code1)

def pg2selection(periode, id_groupe):
    # on calcule le faux id_tableau (id_selection) :
    id_selection = (10 + periode) * 1000 + id_groupe
    return id_selection

def selection2pg(id_selection):
    id_groupe = id_selection % 1000
    periode = (id_selection - id_groupe) // 1000 - 10
    return (periode, id_groupe)

def formatPersoCpt(matiereCode, i):
    """
    aaa = utils_functions.formatPersoCpt(matiereCode, i)
    """
    if i < 10:
        result = u('{0}-0{1}').format(matiereCode, i)
    else:
        result = u('{0}-{1}').format(matiereCode, i)
    return result


"""
****************************************************
    MESSAGES, BOUTONS, ...
****************************************************
"""

def nothingForNow(main):
    """
    usage : utils_functions.nothingForNow(self)
    """
    title = QtWidgets.QApplication.translate(
        'main', 'Nothing')
    message = QtWidgets.QApplication.translate(
        'main', 'Nothing for Now.')
    QtWidgets.QMessageBox.information(
        main, title, message)

def underDevelopment(main, complement=''):
    """
    usage : utils_functions.underDevelopment(self)
    """
    title = QtWidgets.QApplication.translate(
        'main', 'Under Development')
    message = QtWidgets.QApplication.translate(
        'main', 'This part is still under development.')
    if len(complement) > 0:
        message = u('{0}\n\n{1}').format(message, complement)
    QtWidgets.QMessageBox.warning(
        main, title, message)

def notInDemo(main):
    """
    usage : utils_functions.notInDemo(self)
    """
    title = QtWidgets.QApplication.translate(
        'main', 'NotInDemo')
    message = QtWidgets.QApplication.translate(
        'main', 'This action is disabled in the demo version.')
    QtWidgets.QMessageBox.information(
        main, title, message)

NOWAITCURSOR = False
def changeNoWaitCursor(newNoWaitCursor):
    global NOWAITCURSOR
    NOWAITCURSOR = newNoWaitCursor

def doWaitCursor():
    if not(utils.NOGUI) and not(NOWAITCURSOR):
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)

def restoreCursor():
    if not(utils.NOGUI) and not(NOWAITCURSOR):
        QtWidgets.QApplication.restoreOverrideCursor()

def messageBox(main, 
               level='information', title='', message='', 
               detailedText='', timer=0, 
               buttons=['Ok', ], 
               defaultButton=QtWidgets.QMessageBox.NoButton):
    """
    """

    timerMessage = QtWidgets.QApplication.translate(
        'main',
        'Closing of message in {0}s.')

    def timerCloseMessageBox(event):
        event.accept()
        main.messageBoxTimer.stop()

    def timerTimeOut():
        main.messageBoxTimeToWait -= 1
        messageBox.setInformativeText(
            timerMessage.format(main.messageBoxTimeToWait))
        if main.messageBoxTimeToWait == 0:
            messageBox.closeEvent = timerCloseMessageBox
            messageBox.close()

    if utils.NOGUI:
        return QtWidgets.QMessageBox.Ok
    # on teste l'aspect du curseur (doit être normal) :
    try:
        waitCursor = (
            QtWidgets.QApplication.overrideCursor().shape() == \
                QtCore.Qt.WaitCursor)
    except:
        waitCursor = False
    if waitCursor:
        QtWidgets.QApplication.restoreOverrideCursor()
    # gestion des boutons (standards ou persos) :
    buttonsDic = {
        'Ok': QtWidgets.QMessageBox.Ok,
        'Yes': QtWidgets.QMessageBox.Yes,
        'No': QtWidgets.QMessageBox.No,
        'NoToAll': QtWidgets.QMessageBox.NoToAll,
        'Cancel': QtWidgets.QMessageBox.Cancel,
        'Open': QtWidgets.QMessageBox.Open,
        'Save': QtWidgets.QMessageBox.Save,
        'Discard': QtWidgets.QMessageBox.Discard,
        'Abort': QtWidgets.QMessageBox.Abort,
        'Close': QtWidgets.QMessageBox.Close,
        'Help': QtWidgets.QMessageBox.Help,
        'Ignore': QtWidgets.QMessageBox.Ignore,
        }
    buttonsToAdd = []
    standardButtons = QtWidgets.QMessageBox.NoButton
    for button in buttons:
        if button in buttonsDic:
            standardButtons = standardButtons | buttonsDic[button]
        else:
            # les boutons persos seront ajoutés plus tard :
            buttonsToAdd.append(button)
    # titre de la fenêtre :
    titlesDic = {
        'information': QtWidgets.QApplication.translate(
            'main', 'information message'),
        'question': QtWidgets.QApplication.translate(
            'main', 'question message'),
        'warning': QtWidgets.QApplication.translate(
            'main', 'warning message'),
        'critical': QtWidgets.QApplication.translate(
            'main', 'critical message'),
        }
    if title == '':
        title = u('{0} ({1})').format(utils.PROGNAME, titlesDic[level])
    # icône :
    iconsDic = {
        'information': QtWidgets.QMessageBox.Information,
        'question': QtWidgets.QMessageBox.Question,
        'warning': QtWidgets.QMessageBox.Warning,
        'critical': QtWidgets.QMessageBox.Critical,
        }
    icon = iconsDic[level]
    if timer > 0:
        main.messageBoxTimeToWait = timer
        main.messageBoxTimer = QtCore.QTimer(main)
    # on peut créer la boîte de dialogue :
    messageBox = QtWidgets.QMessageBox(
        icon, title, message, standardButtons, main)
    # on ajoute les boutons persos :
    for button in buttonsToAdd:
        if isinstance(button, tuple):
            theButton = QtWidgets.QPushButton(
                utils.doIcon(button[0]), button[1])
            messageBox.addButton(theButton, button[2])
        else:
            messageBox.addButton(button, QtWidgets.QMessageBox.NoRole)
    # le texte détaillé s'il existe :
    if detailedText != '':
        messageBox.setDetailedText(detailedText)
    # on affiche la boîte :
    if timer > 0:
        main.messageBoxTimer.timeout.connect(timerTimeOut)
        main.messageBoxTimer.start(1000)
        messageBox.setInformativeText(
            timerMessage.format(main.messageBoxTimeToWait))
    result = messageBox.exec_()
    if timer > 0:
        try:
            main.messageBoxTimer.stop()
        except:
            pass
        main.messageBoxTimer = None
    # on remet le curseur wait si besoin :
    if waitCursor:
        QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.WaitCursor)
    return result

class MessageWithHelpPageDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, 
                 level='information', title='', message='', 
                 helpContextPage=''):
        super(MessageWithHelpPageDlg, self).__init__(parent)

        self.main = parent

        # titre de la fenêtre :
        titlesDic = {
            'information': QtWidgets.QApplication.translate(
                'main', 'information message'),
            'question': QtWidgets.QApplication.translate(
                'main', 'question message'),
            'warning': QtWidgets.QApplication.translate(
                'main', 'warning message'),
            'critical': QtWidgets.QApplication.translate(
                'main', 'critical message'),
            }
        if title == '':
            title = u('{0} ({1})').format(utils.PROGNAME, titlesDic[level])
        self.setWindowTitle(title)

        self.helpContextPage = helpContextPage

        label = QtWidgets.QLabel(message)

        dialogButtons = createDialogButtons(self, buttons=('ok', 'help'))
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(label, 1, 0, 1, 2)
        grid.addWidget(buttonBox, 9, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        openContextHelp(self.helpContextPage)

def afficheMessage(main, messages=[], 
                    console=False, statusBar=False, 
                    editLog=False, html=False, tags=(), p=False):
    """
    Pour afficher un message (ou une liste de messages).
        utils_functions.afficheMessage(main, 'blablabla')
        utils_functions.afficheMessage(main, ['blablabla1', 'blablabla2'])
    Arguments :
        console : le message sera aussi affiché dans la console
        statusBar : le message sera aussi affiché dans la StatusBar 
            de la fenêtre principale
        editLog : (en admin) pour afficher dans le QTextEdit
        html : (en admin) le message est formaté en html
        tags : (en admin) le message doit être entouré des 
            tags indiquées (par exemple h4)
        p : on insère un saut de paragraphe avant le texte 
            (main.editLog.append(''))
    """
    # s'il n'y a qu'un message, on remplit la liste :
    if not(isinstance(messages, list)):
        messages = [messages, ]
    if not(isinstance(tags, tuple)):
        tags = (tags, )
    if (tags != ()) or (statusBar):
        editLog = True
    # on ajoute les décorations pour les titres en console 
    # et fichier rapport.txt :
    messages2 = []
    if 'h1' in tags:
        messages2.append('')
        messages2.append('@' * 25)
        messages2.extend(messages)
        messages2.append('@' * 25)
        messages2.append('')
    elif 'h2' in tags:
        messages2.append('')
        messages2.append('=' * 30)
        messages2.extend(messages)
        messages2.append('=' * 30)
    elif 'h3' in tags:
        messages2.append('')
        messages2.append('-' * 20)
        messages2.extend(messages)
        messages2.append('-' * 20)
    else:
        messages2.extend(messages)
    # affichage dans la console :
    if utils.MODEBAVARD or console:
        for message in messages2:
            myPrint(message)
        print('')
    # Si on a lancé en NOGUI (pas d'interface graphique),
    # on écrit les messages dans le fichier outFile (rapport.txt) :
    if utils.NOGUI:
        for message in messages2:
            try:
                main.outFileText = u(
                    '{0}{1}\n').format(main.outFileText, message)
            except:
                pass
        try:
            main.outFileText = u('{0}\n').format(main.outFileText)
        except:
            pass
        # on ne va pas plus loin :
        return
    QtWidgets.QApplication.processEvents()
    # affichage dans la StatusBar (seulement le premier 
    # message en cas de liste) :
    if statusBar:
        afficheStatusBar(main, messages[0])
    # affichage dans le QTextEdit (admin seulement) :
    if utils.SHOWLOG and editLog:
        try:
            if html or (len(tags) > 0):
                p = True
            if p:
                main.editLog.append('')
                if main.editLog2 != None:
                    main.editLog2.append('')
            if html:
                for message in messages:
                    main.editLog.insertHtml(u('{0}').format(message))
                    if main.editLog2 != None:
                        main.editLog2.insertHtml(u('{0}').format(message))
            elif len(tags) > 0:
                msg = ''
                for balise in tags:
                    msg = u('{0}<{1}>').format(msg, balise)
                first = True
                for message in messages:
                    if first:
                        msg = u('{0}{1}').format(msg, message)
                        first = False
                    else:
                        msg = u('{0}<br/>{1}').format(msg, message)
                for balise in tags[::-1]:
                    msg = u('{0}</{1}>').format(msg, balise)
                main.editLog.insertHtml(msg)
                if main.editLog2 != None:
                    main.editLog2.insertHtml(msg)
            else:
                msg = ''
                for message in messages:
                    msg = u('{0}<br/>{1}').format(msg, message)
                main.editLog.insertHtml(msg)
                if main.editLog2 != None:
                    main.editLog2.insertHtml(msg)
            main.editLog.moveCursor(QtGui.QTextCursor.End)
            if main.editLog2 != None:
                main.editLog2.moveCursor(QtGui.QTextCursor.End)
        except:
            logging.error('SHOWLOG except')

def afficheStatusBar(main, message=''):
    # pour afficher message dans la StatusBar de la fenêtre principale
    try:
        main.statusBar().showMessage(u(message))
    except:
        pass

def afficheTime(main, titre=''):
    #utils_functions.afficheTime(main, 'blablabla')
    if utils.MODEBAVARD:
        maintenant = QtCore.QTime.currentTime()
        dureeTotale = main.timeDepart.msecsTo(maintenant)
        duree = main.timePrecedent.msecsTo(maintenant)
        main.timePrecedent = maintenant
        myPrint(titre, ' DUREE: ', duree, ' DUREE TOTALE: ', dureeTotale)

def afficheDuree(main, debut, fin):
    duree = QtCore.QTime(0, 0, 0, 0).addSecs(debut.secsTo(fin))
    h, m, s = duree.hour(), duree.minute(), duree.second()
    text = QtWidgets.QApplication.translate('main', 'DURATION:')
    msg = u('{0} {1}h {2}min {3}s').format(text, h, m, s)
    afficheMessage(main, msg, statusBar=True, p=True)

def text2Log(main, text=''):
    """
    permet d'afficher un texte dans le log
    SANS qu'il soit interprété.
    Par exemple, on peut ainsi faire afficher
        <b>aaa</b>
    au lieu de aaa en gras.
    """
    main.editLog.append('')
    main.editLog.insertPlainText(u(text))

def afficheMsgFin(main, message='', timer=0):
    restoreCursor()
    afficheStatusBar(main)
    endMessage = QtWidgets.QApplication.translate('main', 'DONE!')
    if message != '':
        message = u('<p>{0}</p>').format(message)
    allMessage = u(
        '<p align="center">{0}</p>'
        '<p align="center"><b>{1}</b></p>'
        '{2}<p></p>').format(utils.SEPARATOR_LINE, endMessage, message)
    if timer > 0:
        messageBox(
            main, title=utils.PROGNAME, message=allMessage, timer=timer)
    else:
        QtWidgets.QMessageBox.information(main, utils.PROGNAME, allMessage)

def afficheMsgFinOpen(main, fileName, message=''):
    restoreCursor()
    afficheStatusBar(main)
    endMessage = QtWidgets.QApplication.translate('main', 'DONE!')
    openMessage = QtWidgets.QApplication.translate(
        'main', 
        'You can open the file directly<br/>'
        'if the file type is associated with a software.')
    if message != '':
        message = u('<p>{0}</p>').format(message)
    allMessage = u(
        '<p align="center">{0}</p>'
        '<p align="center"><b>{1}</b></p>'
        '<p>{2}</p>'
        '{3}<p></p>').format(
            utils.SEPARATOR_LINE, endMessage, openMessage, message)
    reply = QtWidgets.QMessageBox.information(
        main, utils.PROGNAME, allMessage,
        QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Open)
    if reply == QtWidgets.QMessageBox.Open:
        import utils_filesdirs
        utils_filesdirs.openFile(fileName)

def afficheMsgFinOpenDir(main, directory, message=''):
    restoreCursor()
    afficheStatusBar(main)
    endMessage = QtWidgets.QApplication.translate('main', 'DONE!')
    openMessage = QtWidgets.QApplication.translate(
        'main', 'You can open the directory.')
    if message != '':
        message = u('<p>{0}</p>').format(message)
    allMessage = u(
        '<p align="center">{0}</p>'
        '<p align="center"><b>{1}</b></p>'
        '<p>{2}</p>'
        '{3}<p></p>').format(
            utils.SEPARATOR_LINE, endMessage, openMessage, message)
    reply = QtWidgets.QMessageBox.information(
        main, utils.PROGNAME, allMessage,
        QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Open)
    if reply == QtWidgets.QMessageBox.Open:
        import utils_filesdirs
        utils_filesdirs.openDir(directory)

def afficheMsgPb(main, what='', withMessageBox=True):
    """
    Affiche dans la console (et donc le fichier log si actif)
    les raisons d'une erreur après un except.
    Par défaut (withMessageBox=True), affiche aussi une boîte
    de dialogue indiquant le problème.
    L'argument what permet d'indiquer la procédure où a eu lieu le except.
    """
    restoreCursor()
    import traceback
    message = QtWidgets.QApplication.translate(
        'main', 'There was a problem')
    if what != '':
        message = u(
            '<p><b>{0}</b></p>'
            '<p>_______________________________________________</p>'
            '<p>{1}</p>').format(message, what)
    else:
        message = u(
            '<p><b>{0}</b>'
            '<br/>_______________________________________________</p>').format(
                message)
    try:
        eType, eValue = sys.exc_info()[:2]
        if eType == None:
            eType = ''
        else:
            eType = u(eType.__name__)
    except:
        eType = ''
    try:
        eValue = u(sys.exc_info()[1])
    except:
        eValue = ''
    messageTraceback = QtWidgets.QApplication.translate(
        'main', 'Traceback:')
    messageFileName = QtWidgets.QApplication.translate(
        'main', 'FileName:')
    messageLineNumber = QtWidgets.QApplication.translate(
        'main', 'LineNumber:')
    messageFunctionName = QtWidgets.QApplication.translate(
        'main', 'FunctionName:')
    messageText = QtWidgets.QApplication.translate(
        'main', 'Text:')
    try:
        messageDetails = u(
            '{0}\n{1}\n{2}\n').format(messageTraceback, eType, eValue)
    except:
        messageDetails = u(
            '{0}\n{1}\n').format(messageTraceback, eType)
    # pour la sortie console ou fichier log :
    afficheMessage(
        main, ['@@@@@@@@@@@@@@@@', what, messageTraceback], console=True)
    myPrint(eType)
    myPrint(eValue)

    tb = traceback.extract_tb(sys.exc_info()[2])
    for i in tb:
        eFileName = i[0]
        eLineNumber = i[1]
        eFunctionName = i[2]
        eText = u(i[3])
        try:
            messageDetails = u(
                '{0}\n{1} {2}\n{3} {4}\n{5} {6}\n{7} {8}\n').format(
                    messageDetails, 
                    messageFileName, eFileName,
                    messageLineNumber, eLineNumber,
                    messageFunctionName, eFunctionName,
                    messageText, eText)
        except:
            messageDetails = messageDetails
        # pour la sortie console ou fichier log :
        myPrint(eFileName)
        myPrint(eFunctionName)
        myPrint(eLineNumber, ' : ', eText)
        myPrint('')

    if withMessageBox and not(utils.NOGUI):
        messageBox = QtWidgets.QMessageBox(
            QtWidgets.QMessageBox.Warning, 
            utils.PROGNAME, message, 
            QtWidgets.QMessageBox.NoButton, main)
        messageBox.setMinimumWidth(800)
        messageBox.setDetailedText(messageDetails)
        restoreCursor()
        messageBox.exec_()

    if utils.USELOGFILE:
        utils.LOGFILE.flush()

def createDialogButtons(dialog, 
                        layout=False, 
                        buttons=('ok', 'cancel', 'help')):
    """
    Création de boutons OK, Cancel et Help pour les dialogs.
    Retourne un QDialogButtonBox ou un QHBoxLayout (si layout=True).
    Boutons possibles : 
        ok, apply, cancel, reset, close, 
        yes, no, help, back, next, last, finish
    """
    buttonsList = {}
    # le bouton OK :
    if 'ok' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Ok')
        dialog.okButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), text)
        dialog.okButton.clicked.connect(dialog.accept)
        buttonsList['ok'] = dialog.okButton
    # le bouton Fermer :
    if 'close' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Close')
        dialog.okButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-close'), text)
        dialog.okButton.clicked.connect(dialog.accept)
        buttonsList['close'] = dialog.okButton
    # le bouton Appliquer :
    if 'apply' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Apply')
        dialog.applyButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), text)
        # la connexion se fait dans le dialog
        buttonsList['apply'] = dialog.applyButton
    # le bouton annuler :
    if 'cancel' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Cancel')
        dialog.cancelButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-cancel'), text)
        dialog.cancelButton.clicked.connect(dialog.close)
        buttonsList['cancel'] = dialog.cancelButton
    # le bouton réinitialiser :
    if 'reset' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Reset')
        dialog.resetButton = QtWidgets.QPushButton(
            utils.doIcon('edit-undo'), text)
        # la connexion se fait dans le dialog
        #dialog.resetButton.clicked.connect(dialog.reset)
        buttonsList['reset'] = dialog.resetButton
    # les boutons OUI et NON :
    if 'yes' in buttons:
        text = QtWidgets.QApplication.translate('main', 'YES')
        dialog.yesButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), text)
        dialog.yesButton.clicked.connect(dialog.accept)
        buttonsList['yes'] = dialog.yesButton
    if 'no' in buttons:
        text = QtWidgets.QApplication.translate('main', 'NO')
        dialog.noButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-cancel'), text)
        dialog.noButton.clicked.connect(dialog.close)
        buttonsList['no'] = dialog.noButton
    # le bouton aide :
    if 'help' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Help')
        dialog.helpButton = QtWidgets.QPushButton(
            utils.doIcon('help'), text)
        dialog.helpButton.setShortcut(
            QtGui.QKeySequence(QtGui.QKeySequence.HelpContents))
        dialog.helpButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Opens contextual help in your browser'))
        dialog.helpButton.clicked.connect(dialog.contextHelp)
        buttonsList['help'] = dialog.helpButton
    # les boutons back, next, last et finish (pour les wizard) :
    if 'back' in buttons:
        text = QtWidgets.QApplication.translate('main', '< Back')
        dialog.backButton = QtWidgets.QPushButton(text)
        dialog.backButton.clicked.connect(dialog.doBack)
        buttonsList['back'] = dialog.backButton
    if 'next' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Next >')
        dialog.nextButton = QtWidgets.QPushButton(text)
        dialog.nextButton.clicked.connect(dialog.doNext)
        buttonsList['next'] = dialog.nextButton
    if 'last' in buttons:
        text = QtWidgets.QApplication.translate('main', '>>')
        dialog.lastButton = QtWidgets.QPushButton(text)
        dialog.lastButton.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Skip to the last step.'))
        dialog.lastButton.clicked.connect(dialog.doLast)
        buttonsList['last'] = dialog.lastButton
    if 'finish' in buttons:
        text = QtWidgets.QApplication.translate('main', 'Finish')
        dialog.finishButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-ok-apply'), text)
        dialog.finishButton.clicked.connect(dialog.doFinish)
        buttonsList['finish'] = dialog.finishButton

    # une boîte des boutons :
    if layout:
        buttonsObject = QtWidgets.QHBoxLayout()
        buttonsObject.addStretch(1)
        for button in buttons:
            buttonsObject.addWidget(buttonsList[button])
    else:
        buttonsObject = QtWidgets.QDialogButtonBox()
        for button in buttons:
            buttonsObject.addButton(
                buttonsList[button], 
                QtWidgets.QDialogButtonBox.ActionRole)
    return (buttonsObject, buttonsList)

def createEditButtons(
    dialog, 
    layout=False, 
    buttons=('add', 'delete', 'cancel', 'previous', 'next')):
    """
    Création de boutons pour les dialogs.
    Retourne un QDialogButtonBox ou un QHBoxLayout (si layout=True)
    Boutons possibles : 
        add, insert, repair, delete, cancel, previous, next, 
    à ajouter ? : expand, collapse
    """
    buttonsList = {}
    if 'add' in buttons:
        dialog.addButton = QtWidgets.QPushButton(
            utils.doIcon('list-add'), 
            '',
            toolTip = QtWidgets.QApplication.translate(
                'main', 'Add New Item'))
        dialog.addButton.setObjectName('add')
        # la connexion se fait dans le dialog
        buttonsList['add'] = dialog.addButton
    if 'insert' in buttons:
        dialog.insertButton = QtWidgets.QPushButton(
            utils.doIcon('list-insert'), 
            '',
            toolTip = QtWidgets.QApplication.translate(
                'main', 'Insert New Item (before the selected item)'))
        dialog.insertButton.setObjectName('insert')
        # la connexion se fait dans le dialog
        buttonsList['insert'] = dialog.insertButton
    if 'repair' in buttons:
        dialog.addButton = QtWidgets.QPushButton(
            utils.doIcon('list-repair'), 
            '',
            toolTip = QtWidgets.QApplication.translate(
                'main', 'Repair the selected item'))
        dialog.addButton.setObjectName('repair')
        # la connexion se fait dans le dialog
        buttonsList['repair'] = dialog.addButton
    if 'delete' in buttons:
        dialog.deleteButton = QtWidgets.QPushButton(
            utils.doIcon('list-delete'), 
            '',
            toolTip = QtWidgets.QApplication.translate(
                'main', 'Delete the selected item'))
        dialog.deleteButton.setObjectName('delete')
        buttonsList['delete'] = dialog.deleteButton
    if 'cancel' in buttons:
        dialog.cancelButton = QtWidgets.QPushButton(
            utils.doIcon('edit-undo'), 
            '',
            toolTip = QtWidgets.QApplication.translate(
                'main', 'Cancel ongoing changes'))
        dialog.cancelButton.setObjectName('cancel')
        buttonsList['cancel'] = dialog.cancelButton
    if 'previous' in buttons:
        dialog.previousButton = QtWidgets.QPushButton(
            utils.doIcon('go-previous'), 
            '',
            toolTip = QtWidgets.QApplication.translate('main', 'Previous'))
        dialog.previousButton.setObjectName('previous')
        buttonsList['previous'] = dialog.previousButton
    if 'next' in buttons:
        dialog.nextButton = QtWidgets.QPushButton(
            utils.doIcon('go-next'), 
            '',
            toolTip = QtWidgets.QApplication.translate('main', 'Next'))
        dialog.nextButton.setObjectName('next')
        buttonsList['next'] = dialog.nextButton

    # une boîte des boutons :
    if layout:
        buttonsObject = QtWidgets.QHBoxLayout()
        buttonsObject.addStretch(1)
        for button in buttons:
            buttonsObject.addWidget(buttonsList[button])
    else:
        buttonsObject = QtWidgets.QDialogButtonBox()
        for button in buttons:
            buttonsObject.addButton(
                buttonsList[button], 
                QtWidgets.QDialogButtonBox.ActionRole)
    return (buttonsObject, buttonsList)



"""
****************************************************
    GESTION D'UN PROGRESSDIALOG
****************************************************
"""

VALUEPROGRESS = 0
TOTALPROGRESS = 1

def initProgressDialog(main, title='', totalProgress=1):
    # création du progressDialog :
    global VALUEPROGRESS, TOTALPROGRESS
    VALUEPROGRESS = 0
    TOTALPROGRESS = totalProgress
    message = u(
        '{0}/{1} ({2})').format(VALUEPROGRESS, TOTALPROGRESS, title)
    if utils.NOGUI:
        progressDialog = None
        afficheMessage(main, message, console=True)
    else:
        progressDialog = QtWidgets.QProgressDialog(main)
        progressDialog.setCancelButton(None)
        progressDialog.setWindowTitle(title)
        progressDialog.setMinimumWidth(500)
        progressDialog.setMaximum(TOTALPROGRESS)
        progressDialog.setValue(VALUEPROGRESS)
        progressDialog.setLabelText(message)
        progressDialog.show()
        QtWidgets.QApplication.processEvents()
    return progressDialog

def incrementeProgressDialog(main, progressDialog, message=''):
    global VALUEPROGRESS
    VALUEPROGRESS += 1
    message = u(
        '{0}/{1} ({2})').format(
            VALUEPROGRESS, TOTALPROGRESS, message)
    if progressDialog == None:
        afficheMessage(main, message, console=True)
    else:
        progressDialog.setValue(VALUEPROGRESS)
        progressDialog.setLabelText(message)
        QtWidgets.QApplication.processEvents()

def endProgressDialog(main, progressDialog):
    if progressDialog != None:
        progressDialog.hide()
        del progressDialog



"""
****************************************************
    DIVERS
****************************************************
"""

def doLocale(locale, beginFileName, endFileName, defaultFileName=''):
    """
    Teste l'existence d'un fichier localisé.
    Par exemple, insère _fr_FR ou _fr entre beginFileName et endFileName.
    Renvoie le fichier par défaut sinon.
    """
    # on teste d'abord avec locale (par exemple fr_FR) :
    localeFileName = u(
        '{0}_{1}{2}').format(beginFileName, locale, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # ensuite avec lang (par exemple fr) :
    lang = locale.split('_')[0]
    localeFileName = u(
        '{0}_{1}{2}').format(beginFileName, lang, endFileName)
    if QtCore.QFileInfo(localeFileName).exists():
        return localeFileName
    # si defaultFileName est spécifié :
    if defaultFileName != '':
        return u(defaultFileName)
    # sinon on renvoie le fichier de départ :
    localeFileName = u('{0}{1}').format(beginFileName, endFileName)
    return localeFileName

def mostRecentDate(firstDate, secondDate):
    """
    compare 2 dates au format dd/MM/yyyy.
    Renvoie :
        1 si la première est la plus récente
        2 si la seconde est la plus récente
        0 si elles sont identiques.
    Gère aussi les cas où l'une des dates n'est pas correctement formatée :
        si une seule est incorrecte, l'utre est déclarée plus récente
        si les 2 sont incorrectes, elles sont déclarées identiques.

    et renvoie 1 si le 1er est plus récent
    -1 si c'est le second, 0 si identique
    """
    def date2Int(date):
        try:
            result = date.split('/')
            result.reverse()
            result = ''.join(result)
            result = int(result)
        except:
            result = 0
        return result

    first = date2Int(firstDate)
    second = date2Int(secondDate)
    if first > second:
        result = 1
    elif second > first:
        result = 2
    else:
        result = 0
    return result

def readDatesDBFile(main, datesDBFileDir):
    """
    lecture du fichier dates_db.html 
    (contient les dates des bases commun et users).
    Retourne une liste des 2 dates.
    """
    dates_db = ['0', '0']
    datesDBFile = u('{0}dates_db.html').format(datesDBFileDir)
    if QtCore.QFileInfo(datesDBFile).exists():
        datesDBFile = QtCore.QFile(datesDBFile)
        if datesDBFile.open(QtCore.QFile.ReadOnly | QtCore.QFile.Text):
            try:
                stream = QtCore.QTextStream(datesDBFile)
                stream.setCodec('UTF-8')
                dates_db = stream.readAll().split('|')
            except:
                pass
            finally:
                datesDBFile.close()
    return dates_db

def doEncode(inText, algo=''):
    """
    retourne une version encodée de inText.
    Prévoir un passage à Sha256, voir plus complexe ?
    http://pythoncentral.io/hashing-strings-with-python
    """
    import hashlib
    inText = u(inText)
    if algo == 'sha256':
        result = hashlib.sha256(inText.encode('utf-8')).hexdigest()
        result = u(result)
    elif algo == 'sha1':
        result = hashlib.sha1(inText.encode('utf-8')).hexdigest()
        result = u(result)
    else:
        outText1 = hashlib.sha1(inText.encode('utf-8')).hexdigest()
        outText1 = u(outText1)
        outText256 = hashlib.sha256(inText.encode('utf-8')).hexdigest()
        outText256 = u(outText256)
        result = [outText1, outText256]
    #print('>>>>>>>>> doEncode:', inText, '->', result)
    return result

def openInBrowser(url):
    QtGui.QDesktopServices.openUrl(QtCore.QUrl(url))

def openContextHelp(contextPage, baseUrl='prof', pageId=''):
    if baseUrl == 'admin':
        url = utils.HELPPAGE_BEGIN + 'help-admin'
    elif baseUrl == 'prof':
        url = utils.HELPPAGE_BEGIN + 'help-prof'
    else:
        url = '{0}{1}.html'.format(utils.HELPPAGE_BEGIN, baseUrl)
    if contextPage != '':
        if len(pageId) > 0:
            pageId = '#{0}'.format(pageId)
        url = '{0}-{1}.html{2}'.format(url, contextPage, pageId)
    #print('openContextHelp', url)
    openInBrowser(url)


def appendUnique(aList, aValue):
    """
    pour ajouter un élément à une liste de façon unique
    """
    if not(aValue in aList):
        aList.append(aValue)

def removeAll(aList, aValue):
    while aList.count(aValue) > 0:
        aList.remove(aValue)

def addSlash(aDir):
    """
    pour ajouter un / à la fin d'un nom de dossier si besoin
    aDir = utils_functions.addSlash(aDir)
    """
    if aDir[-1] != '/':
        aDir = aDir + '/'
    return aDir

def removeSlash(aDir):
    """
    pour supprimer l'éventuel / à la fin d'un nom de dossier
    aDir = utils_functions.removeSlash(aDir)
    """
    if len(aDir) > 0:
        if aDir[-1] == '/':
            aDir = aDir[:-1]
    return aDir

def getComputerInfos():
    computerInfos = utils.COMPUTER_INFO
    if computerInfos == '':
        import platform
        computerName = platform.node()
        if 'LOGNAME' in os.environ:
            userName = os.environ['LOGNAME']
        elif 'USER' in os.environ:
            userName = os.environ['USER']
        elif 'USERNAME' in os.environ:
            userName = os.environ['USERNAME']
        else:
            userName = ''
        try:
            computerInfos = u('{0}|{1}').format(computerName, userName)
        except:
            computerInfos = '|'
    return computerInfos

def countLinesInHtml(content=''):
    """
    calcule le nombre de lignes à prévoir dans un texte formaté en html.
    Permet de calculer la hauteur :
        nbLines = utils_functions.countLinesInHtml(content)
        helpEdit.setHtml(content)
        pixelSize = QtGui.QFontInfo(helpEdit.currentFont()).pixelSize()
        helpEdit.setFixedHeight(nbLines * pixelSize)
        ou
        helpEdit.setMinimumHeight(nbLines * pixelSize)
    """
    result = 3 * content.count('<p>')
    result += content.count('<li>')
    result += content.count('<br/>')
    result += 2
    return result

def array2string(array, key=-1, separator=', ', text=False):
    """
    crée une liste (texte) à partir d'un tableau.
    La liste est un texte contenant les éléments du tableau,
    séparés par une virgule (par défaut).
    Utilisé surtout pour les requêtes SQL avec des "IN (...)".
    """
    result = ''
    if text:
        if key < 0:
            result = u(separator).join(
                u('"{0}"').format(e) for e in array)
        else:
            result = u(separator).join(
                u('"{0}"').format(e[key]) for e in array)
    else:
        if key < 0:
            result = separator.join('{0}'.format(e) for e in array)
        else:
            result = separator.join('{0}'.format(e[key]) for e in array)
    return result

def string2array(string, separator=', ', integer=False):
    """
    crée un tableau à partir d'une liste (texte).
    La liste est un texte contenant les éléments du tableau,
    séparés par une virgule (par défaut).
    """
    result = []
    if isinstance(string, int):
        result = [string]
    elif integer:
        result = [int(e) for e in string.split(', ')]
    else:
        result = [e for e in string.split(', ')]
    return result

def verifyLibs_fileName(fileName):
    """
    selon la version de PyQt, QFileDialog renvoie soit 
    * un string : fileName
    * un tuple : (fileName, extension)
    """
    if isinstance(fileName, tuple):
        fileName = fileName[0]
    return fileName

def verifyLibs_toInt(what):
    """
    Pour traiter les différences entre versions de Python et de PyQt.
    Utilisé surtout lors d'une réponse php.
    """
    result = -1
    result = u('{0}').format(what)
    try:
        result = int(result)
    except:
        try:
            result = what.toInt()[0]
        except:
            result = -1
    return result

def calcAnneeScolaire():
    """
    calcul de l'année scolaire (année et titre)
    au cas où elle n'existerait pas 
    dans la table commun.config
    """
    annee_scolaire = 0
    currentDate = QtCore.QDateTime.currentDateTime().date()
    year = currentDate.year()
    # à partir du mois d'août, c'est l'année suivante :
    if currentDate.month() > 7:
        year += 1
    annee_scolaire = '{0}-{1}'.format(year - 1, year)
    return (year, annee_scolaire)

def changeBadChars(badText):
    """
    pour les profs ayant mis des / dans les noms par exemple
    """
    result = badText
    badChars = ('/', '\\', '.', ':', '+', )
    for char in badChars:
        result = result.replace(char, '-')
    for char in ('&', ):
        result = result.replace(char, '')
    return result

TESTCOMPTEUR = 0
def testCompteur(arg=''):
    """
    un compteur pour faire des tests d'appels aux fonctions
    permet d'afficher en console le nombre d'appels
    et donc de dénicher ce qui fait perdre en réactivité
    et la fonction qui va avec
    il suffit de mettre utils.testCompteur('truc à afficher éventuellement')
    là où on soupçonne un appel trop fréquent
    """
    global TESTCOMPTEUR
    TESTCOMPTEUR += 1
    myPrint('Compteur : ', TESTCOMPTEUR, '   ', arg)



"""
****************************************************
    DIVERS SPÉCIFIQUES AU LOGICIEL
****************************************************
"""

def loadMatieres(main):
    """
    retourne les listes des matières séparées en plusieurs parties :
        * la liste de toutes les matières (ALL)
        * la liste des matières qui apparaissent dans le bulletin (BLT)
        * la liste des matières "spéciales" 
            (PP et Vie scolaire) (BLTSpeciales)
        * la liste des autres matières (OTHERS)
        * un dictionnaire pour relier les noms aux codes (Matiere2Code)
        * un dictionnaire pour relier les codes aux matières (Code2Matiere)
    """
    import utils_db
    MATIERES = {}
    try:
        query_commun = utils_db.query(main.db_commun)

        # la liste de toutes les matières :
        MATIERES['ALL'] = []
        # la liste des matières qui apparaissent dans le bulletin :
        MATIERES['BLT'] = []
        # celle des matières spéciales (PP, ...) :
        MATIERES['BLTSpeciales'] = []
        # et les autres :
        MATIERES['OTHERS'] = []
        # pour passer des noms des matières à leurs codes :
        MATIERES['Matiere2Code'] = {}
        MATIERES['Code2Matiere'] = {}
        # remplissage des listes :
        query_commun = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format(
                'matieres', 'OrdreBLT, Matiere'), 
            query=query_commun)
        while query_commun.next():
            id_matiere = int(query_commun.value(0))
            matiereName = query_commun.value(1)
            matiereCode = query_commun.value(2)
            matiereLabel = query_commun.value(3)
            ordreBLT = 0
            if query_commun.value(4) != '':
                ordreBLT = int(query_commun.value(4))
            else:
                ordreBLT = 0
            id_prof = query_commun.value(5)
            # BLT est la liste des matières qui apparaissent dans le bulletin
            if ordreBLT > 0:
                MATIERES['BLT'].append(matiereCode)
            elif ordreBLT < 0:
                # BLTSpeciales ont une appréciation mais sont placées à part
                # (PP : pas de CPT perso, 
                # mais peuvent intervenir dans la partie partagée)
                # on récupère la matière et l'id du prof (10000 pour PP)
                if id_prof == '':
                    # l'id_prof sera donc négatif :
                    id_prof = ordreBLT
                else:
                    id_prof = int(id_prof)
                MATIERES['BLTSpeciales'].append((matiereCode, id_prof))
            else:
                MATIERES['OTHERS'].append(matiereCode)
            # récupération des codes des matières :
            MATIERES['Matiere2Code'][matiereName] = matiereCode
            MATIERES['Code2Matiere'][matiereCode] = (
                id_matiere, matiereName, matiereLabel)
        MATIERES['ALL'].extend(MATIERES['BLT'])
        for (matiereCode, id_prof) in MATIERES['BLTSpeciales']:
            MATIERES['ALL'].append(matiereCode)
        MATIERES['ALL'].extend(MATIERES['OTHERS'])
    finally:
        return MATIERES

def eleveNameFromId(main, id_eleve):
    import utils_db
    result = ''
    commandLine_users = utils_db.qs_users_elevesWhereId.format(id_eleve)
    query_users = utils_db.queryExecute(
        commandLine_users, db=main.db_users)
    while query_users.next():
        # on recrée le texte "NOM Prénom Classe" :
        if utils.DOC:
            result = u('DROU-{0} {1} {2}').format(
                id_eleve, 
                QtWidgets.QApplication.translate('main', 'Leo'), 
                query_users.value(3))
        else:
            result = u('{0} {1} {2}').format(
                query_users.value(1), 
                query_users.value(2), 
                query_users.value(3))
    return result

def readConfigCalculs(main):
    import utils_db
    if main.me['userMode'] != 'admin':
        db = main.db_my
        table = "config"
        whats = {
            'MODECALCUL': (utils.MODECALCUL, utils.changeModeCalcul), 
            'MODECALCULNOTES': (
                utils.MODECALCULNOTES, utils.changeModeCalculNotes), 
            'NBMAXEVAL': (utils.NBMAXEVAL, utils.changeNbMaxEval), 
            'COEFLAST': (utils.COEFLAST, utils.changeCoefLast), 
            'levelValidItemPC': (
                utils.levelValidItemPC, utils.changeLevelValidItemPC), 
            'levelValidItemNB': (
                utils.levelValidItemNB, utils.changeLevelValidItemNB), 
            'levelValidItemOP': (
                utils.levelValidItemOP, utils.changeLevelValidItemOP), 
            'precisionNotes': (
                utils.precisionNotes, utils.changePrecisionNotes), 
            'levelX': (utils.levelX, utils.changeLevelX), 
            }
    else:
        import admin
        db = admin.db_admin
        table = "config_calculs"
        whats = {
            'levelX': (utils.levelX, utils.changeLevelX),
            'levelX_NB': (utils.levelX_NB, utils.changeLevelX_NB),
            'adminMustRecupDetails': (
                utils.ADMIN_WITH_DETAILS, utils.changeAdminMustRecupDetails), 
            'adminNoClassColumn': (
                utils.ADMIN_NO_CLASS_COLUMN, utils.changeAdminNoClassColumn), 
            }
    for what in whats:
        value_int = utils_db.readInConfigTable(
            db, what, table=table, default_int=whats[what][0])[0]
        whats[what][1](value_int)

    # levelA :
    for i in range(4):
        value_int = utils_db.readInConfigTable(
            db, 
            'levelA_{0}'.format(i), 
            table=table, 
            default_int=utils.levelA[i])[0]
        utils.changeLevelA(i, value_int)
    # levelB :
    for i in range(3):
        value_int = utils_db.readInConfigTable(
            db, 
            'levelB_{0}'.format(i), 
            table=table, 
            default_int=utils.levelB[i])[0]
        utils.changeLevelB(i, value_int)

    # valeursEval (si valeurs personnalisées) :
    if main.me['userMode'] != 'admin':
        if utils.MODECALCULNOTES > 1:
            for i in range(4):
                value_int = utils_db.readInConfigTable(
                    db, 
                    'valeursEval_{0}'.format(i), 
                    table=table, 
                    default_int=utils.valeursEval[i])[0]
                utils.changeValeursEval(i, value_int)

    # on récupère la configuration de l'affichage 
    # (lettres, couleurs et touches) :
    if not(utils.NOGUI):
        createPixmapsItems(main, initial=True)
    newAffichagesItems = {}
    newAffichagesItemsInitial = {}
    forOldBase = {'A': 0, 'B': 1, 'C': 2, 'D': 3, 'X': 4}
    for item in utils.itemsValues:
        letterName = u('{0}_Letter').format(item)
        letterAffichage = ''
        nameName = u('{0}_Name').format(item)
        nameAffichage = ''
        colorName = u('{0}_Color').format(item)
        colorAffichage = ''
        key0Name = u('{0}_Key0').format(item)
        key0Affichage = (0, '', False)
        key1Name = u('{0}_Key1').format(item)
        key1Affichage = (0, '', False)
        # pour un prof, on récupère d'abord celles définies 
        # par l'admin dans la base commun :
        if main.me['userMode'] != 'admin':
            letterAffichage = utils_db.readInConfigTable(
                main.db_commun, letterName, table='config_affichage')[1]
            nameAffichage = utils_db.readInConfigTable(
                main.db_commun, nameName, table='config_affichage')[1]
            colorAffichage = utils_db.readInConfigTable(
                main.db_commun, colorName, table='config_affichage')[1]
            key0Affichage = utils_db.readInConfigTable(
                main.db_commun, key0Name, table='config_affichage')
            key1Affichage = utils_db.readInConfigTable(
                main.db_commun, key1Name, table='config_affichage')
            if letterAffichage == '':
                # pour compatibilité :
                letterAffichage = utils_db.readInConfigTable(
                    main.db_commun, item, table='config_affichage')[1]
            newAffichagesItemsInitial[item] = (
                letterAffichage, 
                nameAffichage, 
                colorAffichage,
                (key0Affichage[0], key0Affichage[1]), 
                (key1Affichage[0], key1Affichage[1]), 
                '')
        # dans tous las cas, on récupère les choix personnels :
        value_text = utils_db.readInConfigTable(
            db, letterName, table=table)[1]
        if value_text == '':
            # pour compatibilité
            letterNameOld = u('Letter_{0}').format(forOldBase[item])
            value_text = utils_db.readInConfigTable(
                db, letterNameOld, table=table)[1]
        if value_text != '':
            letterAffichage = value_text
        nameAffichage = utils_db.readInConfigTable(
            db, nameName, table=table, default_text=nameAffichage)[1]
        colorAffichage = utils_db.readInConfigTable(
            db, colorName, table=table, default_text=colorAffichage)[1]
        key0Affichage = utils_db.readInConfigTable(
            db, key0Name, table=table, 
            default_int=key0Affichage[0], default_text=key0Affichage[1])
        key1Affichage = utils_db.readInConfigTable(
            db, key1Name, table=table, 
            default_int=key1Affichage[0], default_text=key1Affichage[1])
        newAffichagesItems[item] = (
            letterAffichage, 
            nameAffichage, 
            colorAffichage,
            (key0Affichage[0], key0Affichage[1]), 
            (key1Affichage[0], key1Affichage[1]), 
            '')
    # pour un prof, on récupère les modifications définies par l'admin 
    # dans le dico utils.affichages_initial :
    if main.me['userMode'] != 'admin':
        utils.changeAffichagesInitial(newAffichagesItemsInitial)
        createPixmapsItems(main, initial=True)
    # on met à jour le dico utils.affichages :
    utils.changeAffichages(newAffichagesItems)
    if not(utils.NOGUI):
        createPixmapsItems(main)

def createPixmap(main, size=18, color=QtGui.QColor('#00ff00'), cross=False):
    """
    """
    points = (
        [
            QtCore.QPoint(0, 4), 
            QtCore.QPoint(4, 0), 
            QtCore.QPoint(17, 13), 
            QtCore.QPoint(13, 17)], 
        [
            QtCore.QPoint(13, 0), 
            QtCore.QPoint(17, 4), 
            QtCore.QPoint(4, 17), 
            QtCore.QPoint(0, 13)], 
        [
            QtCore.QPoint(2, 5), 
            QtCore.QPoint(6, 1), 
            QtCore.QPoint(19, 14), 
            QtCore.QPoint(15, 18)], 
        [
            QtCore.QPoint(15, 1), 
            QtCore.QPoint(19, 5), 
            QtCore.QPoint(6, 18), 
            QtCore.QPoint(2, 14)])

    pixmap = QtGui.QPixmap(size, size)
    pixmap.fill(QtCore.Qt.transparent)
    painter = QtGui.QPainter(pixmap)
    painter.setPen(QtCore.Qt.NoPen)
    painter.setBrush(QtCore.Qt.darkGray)
    if cross:
        painter.drawPolygon(QtGui.QPolygon(points[2]))
        painter.drawPolygon(QtGui.QPolygon(points[3]))
    else:
        painter.drawEllipse(2, 2, 15, 15)
    painter.setBrush(color)
    if cross:
        painter.drawPolygon(QtGui.QPolygon(points[0]))
        painter.drawPolygon(QtGui.QPolygon(points[1]))
    else:
        painter.drawEllipse(0, 0, 15, 15)
    painter.end()
    return pixmap

def createPixmapsItems(main, initial=False):
    pixmaps = []
    for value in utils.itemsValues:
        color = utils.affichages[value][3]
        if value == utils.itemsValues[-1]:
            pixmaps.append(createPixmap(main, color=color, cross=True))
        else:
            pixmaps.append(createPixmap(main, color=color))
    utils.changePixmapsItems(pixmaps, initial=initial)


