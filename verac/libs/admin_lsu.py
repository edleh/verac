# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Tout ce qui concerne l'export vers LSU (Livret Scolaire Unique).
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_filesdirs
import utils_markdown, utils_2lists, utils_export
import admin
import utils_calendar

# PyQt5 ou PyQt4 :
XML_PATTERNS = True
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtXml
    try:
        # sudo apt install python3-pyqt5.qtxmlpatterns
        from PyQt5 import QtXmlPatterns
    except:
        XML_PATTERNS = False
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4 import QtXml
    try:
        from PyQt4 import QtXmlPatterns
    except:
        XML_PATTERNS = False


ICONS = {
    'green': utils.doIcon('color-green'),
    'no': utils.doIcon('color-no'),
    }



def doExport2LSU(main):
    """
    lance le dialog de l'export (Export2LSUDlg).
    """
    dialog = Export2LSUDlg(parent=main)
    lastState = main.disableInterface(dialog)
    if dialog.exec_() != QtWidgets.QDialog.Accepted:
        main.enableInterface(dialog, lastState)
    else:
        main.enableInterface(dialog, lastState)
    main.editLog2 = None


if XML_PATTERNS:
    class MessageHandler(QtXmlPatterns.QAbstractMessageHandler):

        def __init__(self):
            super(MessageHandler, self).__init__()

            self.m_description = ""
            self.m_sourceLocation = QtXmlPatterns.QSourceLocation()

        def statusMessage(self):
            return self.m_description

        def line(self):
            return self.m_sourceLocation.line()

        def column(self):
            return self.m_sourceLocation.column()

        def handleMessage(self, type, description, identifier, sourceLocation):
            self.m_description = description
            self.m_sourceLocation = sourceLocation




class Export2LSUDlg(QtWidgets.QDialog):
    """
    """
    def __init__(self, parent=None):
        utils_functions.doWaitCursor()
        super(Export2LSUDlg, self).__init__(parent)
        self.main = parent
        self.helpContextPage = 'export-lsu'
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate(
            'main', 'Export LSU (Livret Scolaire Unique)')
        self.setWindowTitle(title)
        # le tabWidget et sa liste d'onglets :
        self.tabWidget = QtWidgets.QTabWidget(self)
        self.tabs = []

        # onglets Vérifications :
        self.checksDialogs = (
            ChecksDlg_1, 
            ChecksDlg_2, 
            ChecksDlg_3, 
            ChecksDlg_EPI, 
            ChecksDlg_AP, 
            ChecksDlg_CN, 
            )
        checksText = QtWidgets.QApplication.translate(
            'main', 'Checks')
        for i in range(len(self.checksDialogs)):
            tabChecks = self.checksDialogs[i](parent=self)
            self.tabs.append(tabChecks)
            title = tabChecks.title
            if title == '':
                title = utils_functions.u('{0} {1}').format(checksText, i + 1)
            self.tabWidget.addTab(tabChecks, title)
 
        # onglet Création du fichier :
        self.tabFileCreation = FileCreationDlg(parent=self)
        self.tabWidget.addTab(
            self.tabFileCreation, 
            QtWidgets.QApplication.translate('main', 'File creation'))
        self.tabs.append(self.tabFileCreation)

        # on indique l'onglet de départ :
        self.currentTabIndex = 0
        self.tabWidget.setCurrentIndex(self.currentTabIndex)
        self.rejectTabChanged = False
        # fin de la mise en place :
        dialogButtons = utils_functions.createDialogButtons(
            self, 
            layout=True, 
            buttons=('apply', 'close', 'help'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]
        self.buttonsList['apply'].clicked.connect(self.doApply)
        self.buttonsList['apply'].setEnabled(False)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.tabWidget)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

        self.tabWidget.currentChanged.connect(self.doCurrentTabChanged)
        utils_functions.restoreCursor()

    def doCurrentTabChanged(self, index):
        """
        on change d'onglet.
        Si l'onglet précédent a eu des modifications,
        on propose d'annuler.
        """
        if not(self.rejectTabChanged):
            if self.tabs[self.currentTabIndex].modified:
                oldTabIndex = self.currentTabIndex
                # On prévient que les réglages ont été modifés :
                m1 = QtWidgets.QApplication.translate(
                    'main', 
                    'If you change tab without clicking'
                    '<br/>on the button <b>Apply</b>,'
                    '<br/>the modifications will not be recorded.')
                message = utils_functions.u(
                    '<p align="center">{0}</p>'
                    '<p>{1}</p>'
                    '<p></p>').format(utils.SEPARATOR_LINE, m1)
                msgBox = QtWidgets.QMessageBox(
                    QtWidgets.QMessageBox.Warning,
                    utils.PROGNAME, message,
                    QtWidgets.QMessageBox.NoButton, 
                    self.main)
                msgBox.addButton(
                    QtWidgets.QApplication.translate('main', 'Continue'), 
                    QtWidgets.QMessageBox.AcceptRole)
                msgBox.addButton(
                    QtWidgets.QApplication.translate('main', 'Cancel'), 
                    QtWidgets.QMessageBox.RejectRole)
                if msgBox.exec_() == QtWidgets.QMessageBox.RejectRole:
                    self.rejectTabChanged = True
                    self.tabWidget.setCurrentIndex(oldTabIndex)
                    return
        if type(self.tabWidget.currentWidget()) in self.checksDialogs:
            if not(self.rejectTabChanged):
                self.updateButtons(False)
                self.tabWidget.currentWidget().initialize()
                self.buttonsList['apply'].setVisible(True)
                self.updateButtons(
                    self.tabWidget.currentWidget().modified)
        elif type(self.tabWidget.currentWidget()) == FileCreationDlg:
            self.updateButtons(False)
            self.tabWidget.currentWidget().export2LSU_Button.clearFocus()
            self.buttonsList['apply'].setVisible(False)
        self.rejectTabChanged = False
        self.currentTabIndex = index

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(self.helpContextPage, 'admin')

    def doApply(self):
        """
        bouton "appliquer" les changements sans fermer la fenêtre
        """
        self.tabWidget.currentWidget().checkModifications()
        self.updateButtons(False)
        self.buttonsList['help'].clearFocus()

    def updateButtons(self, modified=True):
        self.buttonsList['apply'].setEnabled(modified)
































class ChecksDlg_1(QtWidgets.QDialog):
    """
    Onglet de vérifications n°1.
        * code UAI de l'établissement
        * dates rentrée et fins de périodes
        * matières
    """
    def __init__(self, parent=None):
        super(ChecksDlg_1, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        self.title = ''
        # des modifications ont été faites :
        self.modified = False

        # Divers : Numéro UAI (ex RNE) de l'établissement :
        text = QtWidgets.QApplication.translate(
            'main', 'Various')
        variousGroup = QtWidgets.QGroupBox(text)
        text = QtWidgets.QApplication.translate(
            'main', 'School Code:')
        codeUAILabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.codeUAIEdit = QtWidgets.QLineEdit()
        # dates de rentrée et des fins de périodes :
        text = QtWidgets.QApplication.translate(
            'main', 'Back to School and End of Periods:')
        periodsLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        text = QtWidgets.QApplication.translate(
            'main', 'Back to School')
        rentreeLabel = QtWidgets.QLabel(text)
        periods = range(1, utils.NB_PERIODES)
        self.periodsEdits = {}
        grid = QtWidgets.QGridLayout()
        grid.addWidget(codeUAILabel,            1, 0)
        grid.addWidget(self.codeUAIEdit,        1, 1, 1, 2)
        grid.addWidget(periodsLabel,            2, 0, 1, 3)
        # les périodes sont mises dans un QScrollArea
        # au cas où il y en aurait beaucoup (!) :
        scrollGrid = QtWidgets.QGridLayout()
        scroll = QtWidgets.QScrollArea()
        dummy = QtWidgets.QWidget()
        scrollGrid.addWidget(rentreeLabel, 0, 0)
        periodEdit = utils_calendar.PyDateEdit(self.main)
        periodEdit.setDisplayFormat('dd/MM/yyyy')
        scrollGrid.addWidget(periodEdit, 0, 1)
        self.periodsEdits[0] = periodEdit
        i = 1
        for p in periods:
            periodLabel = QtWidgets.QLabel(utils.PERIODES[p])
            scrollGrid.addWidget(periodLabel, i, 0)
            periodEdit = utils_calendar.PyDateEdit(self.main)
            periodEdit.setDisplayFormat('dd/MM/yyyy')
            scrollGrid.addWidget(periodEdit, i, 1)
            self.periodsEdits[p] = periodEdit
            i += 1
        scrollGrid.setColumnMinimumWidth(2, 50)
        dummy.setLayout(scrollGrid)
        scroll.setWidget(dummy)
        grid.addWidget(scroll, 3, 1, 1, 2)
        variousGroup.setLayout(grid)

        # Matières :
        text = QtWidgets.QApplication.translate(
            'main', 'Subjects')
        matieresGroup = QtWidgets.QGroupBox(text)
        # liste :
        self.baseList = QtWidgets.QListWidget()
        # SIECLE CODE_GESTION
        text = 'SIECLE CODE_GESTION :'
        siecleNameLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.siecleNameEdit = QtWidgets.QLineEdit()
        # SIECLE MATIERE CODE
        text = 'SIECLE MATIERE CODE :'
        siecleCodeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.siecleCodeEdit = QtWidgets.QLineEdit()
        # SIECLE MODALITE_ELECTION
        text = 'SIECLE MODALITE_ELECTION :'
        modaliteLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.modaliteComboBox = QtWidgets.QComboBox()
        self.modalites = {
            'ORDER': ['S', 'O', 'F', 'L', 'R', 'X', ], 
            'S': QtWidgets.QApplication.translate('main', 'S: core'), 
            'O': QtWidgets.QApplication.translate('main', 'O: mandatory option'), 
            'F': QtWidgets.QApplication.translate('main', 'F: optional extra'), 
            'L': QtWidgets.QApplication.translate('main', 'L: Academic addition to the program'), 
            'R': QtWidgets.QApplication.translate('main', 'R: religious education'), 
            'X': QtWidgets.QApplication.translate('main', 'X: specific measure'), }
       # import des matieres depuis SIECLE :
        self.subjectsFromSIECLE_Button = QtWidgets.QToolButton()
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize'] * 2
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        self.subjectsFromSIECLE_Button.setIcon(
            utils.doIcon('extension-xml'))
        self.subjectsFromSIECLE_Button.setIconSize(
            QtCore.QSize(ICON_SIZE, ICON_SIZE))
        self.subjectsFromSIECLE_Button.setText(
            QtWidgets.QApplication.translate(
                'main', 
                'Update subjects from SIECLE'))
        toolTip = QtWidgets.QApplication.translate(
            'main', 
            'Update subjects from Nomenclature.xml '
            'file exported from SIECLE')
        self.subjectsFromSIECLE_Button.setStatusTip(toolTip)
        self.subjectsFromSIECLE_Button.setToolTip(toolTip)
        self.subjectsFromSIECLE_Button.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        # on met tout dans une grille :
        grid2 = QtWidgets.QGridLayout()
        grid2.addWidget(siecleNameLabel,        2, 1, 1, 2)
        grid2.addWidget(self.siecleNameEdit,    3, 2)
        grid2.addWidget(siecleCodeLabel,        4, 1, 1, 2)
        grid2.addWidget(self.siecleCodeEdit,    5, 2)
        grid2.addWidget(modaliteLabel,          6, 1, 1, 2)
        grid2.addWidget(self.modaliteComboBox,  7, 2)
        vBox = QtWidgets.QVBoxLayout()
        vBox.addLayout(grid2)
        vBox.addWidget(self.subjectsFromSIECLE_Button)
        vBox.addStretch(1)
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.baseList,   1, 0)
        grid.addLayout(vBox,            1, 1)
        matieresGroup.setLayout(grid)

        # Aide :
        mdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpLSU')

        # on agence tout ça :
        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(mdHelpView)
        vBoxLeft.addWidget(variousGroup)
        vBoxLeft.addStretch(1)
        vBoxRight = QtWidgets.QVBoxLayout()
        vBoxRight.addWidget(matieresGroup)
        #vBoxRight.addStretch(1)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addLayout(vBoxLeft)
        mainLayout.addLayout(vBoxRight)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les tables admin.config et admin.lsu
        """
        self.fromSoft = True
        self.modified = False
        query_admin = utils_db.query(admin.db_admin)

        # code UAI de l'établissement :
        texts = {}
        commandLine_admin = utils_db.q_selectAllFrom.format('config')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            texts[query_admin.value(0)] = query_admin.value(2)
        self.codeUAIEdit.setText(texts.get('ETAB_CODE', ''))

        # les dates des périodes :
        lsuTable = {}
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'dates')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            p = int(query_admin.value(1))
            lsu2 = query_admin.value(2)
            lsuTable[p] = lsu2
        for p in self.periodsEdits:
            if p in lsuTable:
                date = QtCore.QDate().fromString(lsuTable[p], 'yyyy-MM-dd')
            else:
                date = QtCore.QDate().currentDate()
            self.periodsEdits[p].setDate(date)

        # les matières :
        self.modaliteComboBox.clear()
        for modalite in self.modalites['ORDER']:
            self.modaliteComboBox.addItem(self.modalites[modalite], modalite)
        self.baseList.clear()
        lsuTable = {}
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'matiere')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            matiereCode = query_admin.value(1)
            lsu2 = query_admin.value(2)
            lsu3 = query_admin.value(3)
            lsu4 = query_admin.value(4)
            lsuTable[matiereCode] = (lsu2, lsu3, lsu4)
        notHere = ['AP_LSU', 'PAR_LSU', ]
        for epi in utils.LSU['EPIS']:
            notHere.append(epi)
        for matiereCode in admin.MATIERES['BLT']:
            if not(matiereCode in notHere):
                (id_matiere, matiereName, matiereLabel) = admin.MATIERES['Code2Matiere'][matiereCode]
                (lsu2, lsu3, lsu4) = lsuTable.get(matiereCode, ('', '', 'S'))
                item = QtWidgets.QListWidgetItem(matiereName)
                item.setData(
                    QtCore.Qt.UserRole, 
                    [id_matiere, matiereCode, matiereName, matiereLabel, lsu2, lsu3, lsu4])
                if len(lsu3) > 0:
                    color = 'green'
                else:
                    color = 'no'
                item.setIcon(ICONS[color])
                self.baseList.addItem(item)
        self.baseList.setCurrentRow(0)
        self.fromSoft = False

    def createConnexions(self):
        self.baseList.currentItemChanged.connect(self.doCurrentItemChanged)
        lineEdits = (#QLineEdit
            self.codeUAIEdit, 
            self.siecleNameEdit, 
            self.siecleCodeEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.textEdited.connect(self.doChanged)
        for p in self.periodsEdits:
            self.periodsEdits[p].dateChanged.connect(self.doChanged)
        self.modaliteComboBox.activated.connect(self.doChanged)
        self.subjectsFromSIECLE_Button.clicked.connect(self.subjectsFromSIECLE)

    def subjectsFromSIECLE(self):
        """
        importation des profs depuis le fichier
        xml de SIECLE.
        """
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open xml or zip File'),
            admin.adminDir, 
            QtWidgets.QApplication.translate(
                'main', 'xml or zip files (*.xml *.zip)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        if QtCore.QFileInfo(fileName).suffix() == 'zip':
            result = utils_filesdirs.unzipFile(
                fileName, self.main.tempPath)
            fileName = self.main.tempPath + '/' + result[1]
        if fileName == '':
            return
        utils_functions.doWaitCursor()
        OK = False
        try:
            import utils_export_xml
            (ok, dataInXML) = utils_export_xml.subjectsFromSIECLE(self.main, fileName)
            if not(ok):
                return
            self.fromSoft = True
            # on récupère le code UAJ de l'établissement :
            xmlUAJ = dataInXML.get('xmlUAJ', '')
            if len(xmlUAJ) > 0:
                del(dataInXML['xmlUAJ'])
                if xmlUAJ != self.codeUAIEdit.text():
                    self.codeUAIEdit.setText(xmlUAJ)
            for index in range(self.baseList.count()):
                # 'matiere', matiereName, siecleName, siecleCode, modalite
                item = self.baseList.item(index)
                data = item.data(QtCore.Qt.UserRole)
                lsu3 = data[5]
                if (lsu3 in dataInXML):
                    lsu2 = dataInXML[lsu3][0]
                    lsu4 = dataInXML[lsu3][1]
                    item.setData(
                        QtCore.Qt.UserRole, 
                        [data[0], data[1], data[2], data[3], lsu2, lsu3, lsu4])
                    color = 'green'
                    del(dataInXML[lsu3])
                else:
                    item.setData(
                        QtCore.Qt.UserRole, 
                        [data[0], data[1], data[2], data[3], '', '', 'S'])
                    color = 'no'
                item.setIcon(ICONS[color])
            self.doModified()
            OK = True
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(self.main)
            self.baseList.setCurrentRow(0)
            self.fromSoft = False
            if not(OK):
                message = QtWidgets.QApplication.translate(
                    'main', 'Importing the file failed or you have canceled.')
                utils_functions.messageBox(self.main, level='warning', message=message)
            elif len(dataInXML) > 0:
                m0 = QtWidgets.QApplication.translate(
                    'main', 
                    'Subjects in the file have not been assigned.')
                m1 = QtWidgets.QApplication.translate(
                    'main', 
                    'You can see the list below and assign them by hand.')
                headMessage = utils_functions.u(
                    '<b>{0}</b><p>{1}</p>').format(m0, m1)
                message = ''
                for lsu3 in dataInXML:
                    message = utils_functions.u('{0}{1}\t{2}\t{3}\n').format(
                        message, 
                        dataInXML[lsu3][0], 
                        lsu3, 
                        dataInXML[lsu3][1])
                utils_functions.messageBox(
                    self.main, message=headMessage, detailedText=message)
            else:
                utils_functions.afficheMsgFin(self.main, timer=5)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection.
        On met les champs à jour.
        """
        self.fromSoft = True
        try:
            lastData = current.data(QtCore.Qt.UserRole)
            actions = {
                self.siecleNameEdit: ('TEXT', 4),
                self.siecleCodeEdit: ('TEXT', 5),
                self.modaliteComboBox: ('COMBOBOX', 6), }
            for action in actions:
                if actions[action][0] == 'TEXT':
                    action.setText(utils_functions.u(lastData[actions[action][1]]))
                elif actions[action][0] == 'COMBOBOX':
                    modalite = lastData[actions[action][1]]
                    index = action.findData(modalite, QtCore.Qt.UserRole)
                    action.setCurrentIndex(index)
        except:
            pass
        self.fromSoft = False

    def doChanged(self, value=-1):
        """
        un champ a été modifié.
        On met à jour les données et l'affichage.
        """
        if self.fromSoft:
            return
        if type(self.sender()) == QtWidgets.QLineEdit:
            if not(self.sender().isModified()):
                return

        if self.sender() == self.modaliteComboBox:
            try:
                modalite = self.modaliteComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = self.modaliteComboBox.currentIndex()
                modalite = self.modaliteComboBox.itemData(index)
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[6] = modalite
            current.setData(QtCore.Qt.UserRole, data)
            if len(data[5]) > 0:
                color = 'green'
            else:
                color = 'no'
            current.setIcon(ICONS[color])
            self.doModified()
        elif self.sender() == self.codeUAIEdit:
            self.doModified()
        elif type(self.sender()) == utils_calendar.PyDateEdit:
            self.doModified()
        else:
            newText = self.sender().text()
            current = self.baseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if self.sender() == self.siecleNameEdit:
                data[4] = newText
                current.setData(QtCore.Qt.UserRole, data)
            elif self.sender() == self.siecleCodeEdit:
                data[5] = newText
                current.setData(QtCore.Qt.UserRole, data)
            if len(data[5]) > 0:
                color = 'green'
            else:
                color = 'no'
            current.setIcon(ICONS[color])
            self.doModified()

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        commandLines_admin = []
        lines = []
        commandLineInsert = utils_db.insertInto('lsu', 6)

        # le code UAI de l'établissement :
        if self.codeUAIEdit.isModified():
            commandLineDelete = utils_db.qdf_whereText.format(
                'config', 'key_name', 'ETAB_CODE')
            commandLineInsert2 = utils_functions.u(
                'INSERT INTO config VALUES ("{0}", "", "{1}")').format(
                    'ETAB_CODE', self.codeUAIEdit.text())
            commandLines_admin.extend([commandLineDelete, commandLineInsert2])

        # les dates des périodes :
        commandLines_admin.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'dates'))
        for p in self.periodsEdits:
            date = self.periodsEdits[p].date().toString('yyyy-MM-dd')
            lines.append(('dates', p, date, '', '', ''))

        # les matières :
        commandLines_admin.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'matiere'))
        for index in range(self.baseList.count()):
            # 'matiere', matiereName, siecleName, siecleCode, modalite
            item = self.baseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if len(data[5]) > 0:
                lines.append(('matiere', data[1], data[4], data[5], data[6], ''))

        # écriture et enregistrement :
        if len(commandLines_admin) > 0:
            utils_functions.doWaitCursor()
            query_admin, transactionOK_admin = utils_db.queryTransactionDB(
                admin.db_admin)
            try:
                query_admin = utils_db.queryExecute(
                    commandLines_admin, query=query_admin)
                if len(lines) > 0:
                    query_admin = utils_db.queryExecute(
                        {commandLineInsert: lines}, query=query_admin)
                # on exporte en csv :
                for table in ('lsu', ):
                    fileName = utils_functions.u(
                        '{0}/admin_tables/{1}.csv').format(admin.csvDir, table)
                    utils_export.exportTable2Csv(
                        self.main, admin.db_admin, table, fileName, msgFin=False)
            finally:
                utils_db.endTransaction(
                    query_admin, admin.db_admin, transactionOK_admin)
                utils_filesdirs.removeAndCopy(
                    admin.dbFileTemp_admin, admin.dbFile_admin)
            utils_functions.restoreCursor()

        self.modified = False
































class ChecksDlg_2(QtWidgets.QDialog):
    """
    Onglet de vérifications n°2.
        * responsables de l'établissement (signent les bulletins)
        * profs
    """
    def __init__(self, parent=None):
        super(ChecksDlg_2, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        self.title = ''
        # des modifications ont été faites :
        self.modified = False

        # Responsables (signent les bulletins) :
        text = QtWidgets.QApplication.translate(
            'main', 'Officials')
        responsablesEtabGroup = QtWidgets.QGroupBox(text)
        # liste des classes + responsable :
        self.responsablesEtabBaseList = QtWidgets.QListWidget()
        self.responsablesEtabBaseList.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection)
        # liste déroulante des responsables de l'établissement :
        self.responsablesEtabComboBox = QtWidgets.QComboBox()
        # on met tout dans le QGroupBox :
        vBox = QtWidgets.QVBoxLayout()
        vBox.addWidget(self.responsablesEtabComboBox)
        vBox.addWidget(self.responsablesEtabBaseList)
        responsablesEtabGroup.setLayout(vBox)

        # Types d'évaluations (positionnements ou notes) :
        text = QtWidgets.QApplication.translate(
            'main', 'Types of assessments')
        evaluationTypeGroup = QtWidgets.QGroupBox(text)
        # liste des classes + type :
        self.evaluationTypeBaseList = QtWidgets.QListWidget()
        self.evaluationTypeBaseList.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection)
        # liste déroulante des types :
        self.evaluationTypeComboBox = QtWidgets.QComboBox()
        vBox = QtWidgets.QVBoxLayout()
        vBox.addWidget(self.evaluationTypeComboBox)
        vBox.addWidget(self.evaluationTypeBaseList)
        evaluationTypeGroup.setLayout(vBox)

        # Types de classes et cycles :
        text = QtWidgets.QApplication.translate(
            'main', 'Types of classes and cycles')
        classType2CycleGroup = QtWidgets.QGroupBox(text)
        # liste des classes + type :
        self.classType2CycleBaseList = QtWidgets.QListWidget()
        self.classType2CycleBaseList.setSelectionMode(
            QtWidgets.QAbstractItemView.ExtendedSelection)
        # liste déroulante des types :
        self.classType2CycleComboBox = QtWidgets.QComboBox()
        vBox = QtWidgets.QVBoxLayout()
        vBox.addWidget(self.classType2CycleComboBox)
        vBox.addWidget(self.classType2CycleBaseList)
        classType2CycleGroup.setLayout(vBox)

        # Profs :
        text = QtWidgets.QApplication.translate(
            'main', 'Teachers')
        profsGroup = QtWidgets.QGroupBox(text)
        # liste :
        self.profsBaseList = QtWidgets.QListWidget()
        # STS INDIVIDU ID
        text = 'STS INDIVIDU ID :'
        profStsIdLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.profStsIdEdit = QtWidgets.QLineEdit()
        # STS INDIVIDU TYPE
        text = 'STS INDIVIDU TYPE :'
        profStsTypeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.profStsTypeComboBox = QtWidgets.QComboBox()
        self.profStsTypes = ['epp', 'local', ]
        # import des profs depuis STSWeb :
        self.teachersFromSTSWeb_Button = QtWidgets.QToolButton()
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize'] * 2
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        self.teachersFromSTSWeb_Button.setIcon(
            utils.doIcon('xml-to-database-prof'))
        self.teachersFromSTSWeb_Button.setIconSize(
            QtCore.QSize(ICON_SIZE, ICON_SIZE))
        self.teachersFromSTSWeb_Button.setText(
            QtWidgets.QApplication.translate(
                'main', 
                'Update teachers from STSWeb'))
        self.teachersFromSTSWeb_Button.setStatusTip(
            QtWidgets.QApplication.translate(
                'main', 
                'Update teachers from sts_emp_RNE_aaaa.xml '
                'file exported from STSWeb'))
        self.teachersFromSTSWeb_Button.setToolButtonStyle(
            QtCore.Qt.ToolButtonTextUnderIcon)
        # on met tout dans une grille :
        grid2 = QtWidgets.QGridLayout()
        grid2.addWidget(profStsIdLabel,             2, 1, 1, 2)
        grid2.addWidget(self.profStsIdEdit,         3, 2)
        grid2.addWidget(profStsTypeLabel,           6, 1, 1, 2)
        grid2.addWidget(self.profStsTypeComboBox,   7, 2)
        vBox = QtWidgets.QVBoxLayout()
        vBox.addLayout(grid2)
        if utils.ICI:
            vBox.addWidget(self.teachersFromSTSWeb_Button)
        vBox.addStretch(1)
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.profsBaseList,  1, 0)
        grid.addLayout(vBox,                1, 1)
        profsGroup.setLayout(grid)

        # on agence tout ça :
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addWidget(responsablesEtabGroup)
        centenVBox = QtWidgets.QVBoxLayout()
        centenVBox.addWidget(evaluationTypeGroup)
        centenVBox.addWidget(classType2CycleGroup)
        mainLayout.addLayout(centenVBox)
        mainLayout.addWidget(profsGroup)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les tables commun.classes, admin.profs et admin.lsu
        """
        self.fromSoft = True
        self.modified = False
        query_admin = utils_db.query(admin.db_admin)

        # on a besoin de récupérer la liste des profs
        # (utilisé pour les 2 parties) :
        defautResponsable = -1
        self.profsData = {'ENS': [], 'EDU': [], 'DIR': [], 'ENS_EDU': [], 'DATA': {}}
        self.profsData['DATA'][-1] = ('', '', '', '')
        # on récupère le nom de la matière Vie Scolaire (à traiter à part) :
        VSName = admin.MATIERES['Code2Matiere'].get(
            'VS', (-2, 'Vie scolaire', 'Vie scolaire'))[1]
        commandLine_admin = utils_db.q_selectAllFrom.format('profs')
        queryList = utils_db.query2List(
            commandLine_admin, order=['NOM', 'Prenom'], query=query_admin)
        for record in queryList:
            id_prof = int(record[0])
            num = record[1]
            nom = record[2]
            prenom = record[3]
            matiere = record[4]
            self.profsData['DATA'][id_prof] = (num, nom, prenom, matiere)
            if id_prof >= utils.decalageChefEtab:
                self.profsData['DIR'].append(id_prof)
                if defautResponsable < 0:
                    defautResponsable = id_prof
            elif id_prof < utils.decalagePP:
                matiereName = admin.MATIERES['Code2Matiere'].get(
                    matiere, (0, '', ''))[1]
                if matiereName != VSName:
                    self.profsData['ENS'].append(id_prof)
                    self.profsData['ENS_EDU'].append(id_prof)
                else:
                    self.profsData['EDU'].append(id_prof)
        for id_prof in self.profsData['EDU']:
            self.profsData['ENS_EDU'].append(id_prof)

        # les responsables de l'établissement :
        self.responsablesEtabComboBox.clear()
        for id_prof in self.profsData['DIR']:
            (num, nom, prenom, matiere) = self.profsData['DATA'][id_prof]
            text = utils_functions.u('{0} {1}').format(
                nom, prenom)
            self.responsablesEtabComboBox.addItem(text, id_prof)
        self.responsablesEtabComboBox.insertSeparator(1000)
        for id_prof in self.profsData['EDU']:
            (num, nom, prenom, matiere) = self.profsData['DATA'][id_prof]
            text = utils_functions.u('{0} {1}').format(
                nom, prenom)
            self.responsablesEtabComboBox.addItem(text, id_prof)
        self.responsablesEtabComboBox.insertSeparator(1000)
        for id_prof in self.profsData['ENS']:
            (num, nom, prenom, matiere) = self.profsData['DATA'][id_prof]
            text = utils_functions.u('{0} {1}').format(
                nom, prenom)
            self.responsablesEtabComboBox.addItem(text, id_prof)
        self.responsablesEtabBaseList.clear()
        lsuTable = {}
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'responsable')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            classe = query_admin.value(1)
            id_prof = int(query_admin.value(2))
            lsuTable[classe] = id_prof
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=self.main.db_commun)
        while query_commun.next():
            id_classe = int(query_commun.value(0))
            classe = query_commun.value(1)
            lsu2 = lsuTable.get(classe, defautResponsable)
            # en cas de changement de chef :
            if not(lsu2 in self.profsData['DATA']):
                lsu2 = defautResponsable
            (num, nom, prenom, matiere) = self.profsData['DATA'][lsu2]
            text = utils_functions.u('{0} : {1} {2}').format(
                classe, nom, prenom)
            item = QtWidgets.QListWidgetItem(text)
            item.setData(
                QtCore.Qt.UserRole, 
                [id_classe, classe, lsu2, nom, prenom])
            self.responsablesEtabBaseList.addItem(item)

        # les types d'évaluation :
        positioningText = QtWidgets.QApplication.translate('main', 'positioning')
        notesText = QtWidgets.QApplication.translate('main', 'notes')
        self.evaluationTypeComboBox.clear()
        self.evaluationTypeComboBox.addItem(positioningText, 0)
        self.evaluationTypeComboBox.addItem(notesText, 1)
        self.evaluationTypeBaseList.clear()
        lsuTable = {}
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'evaluationType')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            classe = query_admin.value(1)
            id_type = int(query_admin.value(2))
            lsuTable[classe] = id_type
        commandLine_commun = (
            'SELECT * FROM classes '
            'WHERE notes=1 AND ordre<9999 '
            'ORDER BY ordre')
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            id_classe = int(query_commun.value(0))
            classe = query_commun.value(1)
            lsu2 = lsuTable.get(classe, 0)
            if lsu2 < 1:
                text = utils_functions.u(
                    '{0} : {1}').format(classe, positioningText)
            else:
                text = utils_functions.u(
                    '{0} : {1}').format(classe, notesText)
            item = QtWidgets.QListWidgetItem(text)
            item.setData(
                QtCore.Qt.UserRole, 
                [id_classe, classe, lsu2])
            self.evaluationTypeBaseList.addItem(item)

        # les types de classes et les cycles :
        cycleText = QtWidgets.QApplication.translate('main', 'cycle')
        self.classType2CycleComboBox.clear()
        for i in (3, 4):
            self.classType2CycleComboBox.addItem(
                utils_functions.u('{0} {1}').format(cycleText, i), i)
        self.classType2CycleBaseList.clear()
        lsuTable = {}
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'classType2Cycle')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_classType = int(query_admin.value(1))
            id_cycle = int(query_admin.value(2))
            lsuTable[id_classType] = id_cycle
        commandLine_commun = utils_db.q_selectAllFrom.format('classestypes')
        query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
        while query_commun.next():
            id_classType = int(query_commun.value(0))
            classType = query_commun.value(1)
            lsu2 = lsuTable.get(id_classType, -1)
            if lsu2 < 0:
                if classType == 'cycle 3':
                    lsu2 = 3
                elif classType == 'cycle 4':
                    lsu2 = 4
                else:
                    lsu2 = 3
            text = utils_functions.u(
                '{0} : {1}').format(
                    classType, 
                    utils_functions.u('{0} {1}').format(cycleText, lsu2))
            item = QtWidgets.QListWidgetItem(text)
            item.setData(
                QtCore.Qt.UserRole, 
                [id_classType, classType, lsu2])
            self.classType2CycleBaseList.addItem(item)

        # les profs :
        self.profStsTypeComboBox.clear()
        for profStsType in self.profStsTypes:
            self.profStsTypeComboBox.addItem(profStsType, profStsType)
        self.profsBaseList.clear()
        lsuTable = {}
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'prof')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            id_prof = int(query_admin.value(1))
            lsu2 = query_admin.value(2)
            lsu3 = query_admin.value(3)
            lsuTable[id_prof] = (lsu2, lsu3)
        for id_prof in self.profsData['ENS']:
            (num, nom, prenom, matiere) = self.profsData['DATA'][id_prof]
            (lsu2, lsu3) = lsuTable.get(id_prof, ('', 'epp'))
            matiereName = admin.MATIERES['Code2Matiere'].get(matiere, (0, '', ''))[1]
            text = utils_functions.u('{0} {1} [{2}]').format(
                nom, prenom, matiereName)
            item = QtWidgets.QListWidgetItem(text)
            item.setData(
                QtCore.Qt.UserRole, 
                [id_prof, num, nom, prenom, matiere, lsu2, lsu3])
            if len(lsu2) > 0:
                color = 'green'
            else:
                color = 'no'
            item.setIcon(ICONS[color])
            self.profsBaseList.addItem(item)
        self.profsBaseList.setCurrentRow(0)
        self.fromSoft = False

    def createConnexions(self):
        self.responsablesEtabBaseList.currentItemChanged.connect(self.doCurrentItemChanged)
        self.evaluationTypeBaseList.currentItemChanged.connect(self.doCurrentItemChanged)
        self.classType2CycleBaseList.currentItemChanged.connect(self.doCurrentItemChanged)
        self.profsBaseList.currentItemChanged.connect(self.doCurrentItemChanged)
        lineEdits = (#QLineEdit
            self.profStsIdEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.textEdited.connect(self.doChanged)
        self.responsablesEtabComboBox.activated.connect(self.doChanged)
        self.evaluationTypeComboBox.activated.connect(self.doChanged)
        self.classType2CycleComboBox.activated.connect(self.doChanged)
        self.profStsTypeComboBox.activated.connect(self.doChanged)
        self.teachersFromSTSWeb_Button.clicked.connect(self.teachersFromSTSWeb)

    def teachersFromSTSWeb(self):
        """
        importation des profs depuis le fichier
        xml de SIECLE.
        """
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, 
            QtWidgets.QApplication.translate(
                'main', 'Open xml or zip File'),
            admin.adminDir, 
            QtWidgets.QApplication.translate(
                'main', 'xml or zip files (*.xml *.zip)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        if QtCore.QFileInfo(fileName).suffix() == 'zip':
            result = utils_filesdirs.unzipFile(
                fileName, self.main.tempPath)
            fileName = self.main.tempPath + '/' + result[1]
        if fileName == '':
            return
        OK, data = admin.teachersFromSTSWeb(self.main, fileName)
        # on recharge les données du deuxième onglet :
        if OK:
            #print(data)
            return
            self.initialize(data=data)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection.
        On met les champs à jour.
        """
        self.fromSoft = True
        try:
            lastData = current.data(QtCore.Qt.UserRole)
            if self.sender() == self.responsablesEtabBaseList:
                actions = {
                    self.responsablesEtabComboBox: ('COMBOBOX', 2), }
                for action in actions:
                    if actions[action][0] == 'COMBOBOX':
                        id_prof = lastData[actions[action][1]]
                        index = action.findData(id_prof, QtCore.Qt.UserRole)
                        action.setCurrentIndex(index)
            elif self.sender() == self.evaluationTypeBaseList:
                actions = {
                    self.evaluationTypeComboBox: ('COMBOBOX', 2), }
                for action in actions:
                    if actions[action][0] == 'COMBOBOX':
                        id_type = lastData[actions[action][1]]
                        index = action.findData(id_type, QtCore.Qt.UserRole)
                        action.setCurrentIndex(index)
            elif self.sender() == self.classType2CycleBaseList:
                actions = {
                    self.classType2CycleComboBox: ('COMBOBOX', 2), }
                for action in actions:
                    if actions[action][0] == 'COMBOBOX':
                        id_cycle = lastData[actions[action][1]]
                        index = action.findData(id_cycle, QtCore.Qt.UserRole)
                        action.setCurrentIndex(index)
            elif self.sender() == self.profsBaseList:
                actions = {
                    self.profStsIdEdit: ('TEXT', 5),
                    self.profStsTypeComboBox: ('COMBOBOX', 6), }
                for action in actions:
                    if actions[action][0] == 'TEXT':
                        action.setText(utils_functions.u(lastData[actions[action][1]]))
                    elif actions[action][0] == 'COMBOBOX':
                        profStsType = lastData[actions[action][1]]
                        index = action.findData(profStsType, QtCore.Qt.UserRole)
                        action.setCurrentIndex(index)
        except:
            pass
        self.fromSoft = False

    def doChanged(self, value=-1):
        """
        un champ a été modifié.
        On met à jour les données et l'affichage.
        """
        if self.fromSoft:
            return
        if type(self.sender()) == QtWidgets.QLineEdit:
            if not(self.sender().isModified()):
                return

        if self.sender() == self.responsablesEtabComboBox:
            try:
                id_prof = self.responsablesEtabComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = self.responsablesEtabComboBox.currentIndex()
                id_prof = self.responsablesEtabComboBox.itemData(index)
            profName = self.responsablesEtabComboBox.currentText()
            selected = self.responsablesEtabBaseList.selectedItems()
            for item in selected:
                data = item.data(QtCore.Qt.UserRole)
                data[2] = id_prof
                (num, nom, prenom, matiere) = self.profsData['DATA'][id_prof]
                data[3] = nom
                data[4] = prenom
                item.setData(QtCore.Qt.UserRole, data)
                text = utils_functions.u('{0} : {1}').format(
                    data[1], profName)
                item.setText(text)
            self.doModified()
        elif self.sender() == self.evaluationTypeComboBox:
            try:
                id_type = self.evaluationTypeComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = self.evaluationTypeComboBox.currentIndex()
                id_type = self.evaluationTypeComboBox.itemData(index)
            typeText = self.evaluationTypeComboBox.currentText()
            selected = self.evaluationTypeBaseList.selectedItems()
            for item in selected:
                data = item.data(QtCore.Qt.UserRole)
                data[2] = id_type
                item.setData(QtCore.Qt.UserRole, data)
                text = utils_functions.u(
                    '{0} : {1}').format(data[1], typeText)
                item.setText(text)
            self.doModified()
        elif self.sender() == self.classType2CycleComboBox:
            try:
                id_cycle = self.classType2CycleComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = self.classType2CycleComboBox.currentIndex()
                id_cycle = self.classType2CycleComboBox.itemData(index)
            cycleText = self.classType2CycleComboBox.currentText()
            selected = self.classType2CycleBaseList.selectedItems()
            for item in selected:
                data = item.data(QtCore.Qt.UserRole)
                data[2] = id_cycle
                item.setData(QtCore.Qt.UserRole, data)
                text = utils_functions.u(
                    '{0} : {1}').format(data[1], cycleText)
                item.setText(text)
            self.doModified()
        elif self.sender() == self.profStsTypeComboBox:
            try:
                profStsType = self.profStsTypeComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = self.profStsTypeComboBox.currentIndex()
                profStsType = self.profStsTypeComboBox.itemData(index)
            current = self.profsBaseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            data[6] = profStsType
            current.setData(QtCore.Qt.UserRole, data)
            self.doModified()
        else:
            newText = self.sender().text()
            current = self.profsBaseList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if self.sender() == self.profStsIdEdit:
                data[5] = newText
                current.setData(QtCore.Qt.UserRole, data)
            if len(data[5]) > 0:
                color = 'green'
            else:
                color = 'no'
            current.setIcon(ICONS[color])
            self.doModified()

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        commandLines_admin = []
        lines = []
        commandLineInsert = utils_db.insertInto('lsu', 6)

        # les responsables de l'établissement :
        commandLines_admin.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'responsable'))
        for index in range(self.responsablesEtabBaseList.count()):
            # 'responsable', classe, id_prof
            item = self.responsablesEtabBaseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[2] > -1:
                lsu2 = '{0}'.format(data[2])
                lines.append(('responsable', data[1], lsu2, '', '', ''))

        # les types d'évaluation :
        commandLines_admin.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'evaluationType'))
        for index in range(self.evaluationTypeBaseList.count()):
            # 'evaluationType', classe, id_type
            item = self.evaluationTypeBaseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[2] > -1:
                lsu2 = '{0}'.format(data[2])
                lines.append(('evaluationType', data[1], lsu2, '', '', ''))

        # les types de classes et cycles :
        commandLines_admin.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'classType2Cycle'))
        for index in range(self.classType2CycleBaseList.count()):
            # 'classType2Cycle', id_classType, cycle
            item = self.classType2CycleBaseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if data[2] > -1:
                lsu2 = '{0}'.format(data[2])
                lines.append(('classType2Cycle', data[0], lsu2, '', '', ''))

        # les profs :
        commandLines_admin.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'prof'))
        for index in range(self.profsBaseList.count()):
            # 'prof', id_prof, profStsId, profStsType
            item = self.profsBaseList.item(index)
            data = item.data(QtCore.Qt.UserRole)
            if len(data[5]) > 0:
                lsu1 = '{0}'.format(data[0])
                lines.append(('prof', lsu1, data[5], data[6], '', ''))

        # écriture et enregistrement :
        if len(commandLines_admin) > 0:
            utils_functions.doWaitCursor()
            query_admin, transactionOK_admin = utils_db.queryTransactionDB(
                admin.db_admin)
            try:
                query_admin = utils_db.queryExecute(
                    commandLines_admin, query=query_admin)
                if len(lines) > 0:
                    query_admin = utils_db.queryExecute(
                        {commandLineInsert: lines}, query=query_admin)
                # on exporte en csv :
                for table in ('lsu', ):
                    fileName = utils_functions.u(
                        '{0}/admin_tables/{1}.csv').format(admin.csvDir, table)
                    utils_export.exportTable2Csv(
                        self.main, admin.db_admin, table, fileName, msgFin=False)
            finally:
                utils_db.endTransaction(
                    query_admin, admin.db_admin, transactionOK_admin)
                utils_filesdirs.removeAndCopy(
                    admin.dbFileTemp_admin, admin.dbFile_admin)
            utils_functions.restoreCursor()

        self.modified = False
































class ChecksDlg_3(QtWidgets.QDialog):
    """
    Onglet de vérifications n°3.
        * 8 composantes du socle
        * parcours
        * enseignements de complément (Latin etc)
    """
    def __init__(self, parent=None):
        super(ChecksDlg_3, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        self.title = ''
        # des modifications ont été faites :
        self.modified = False
        self.whatIsModified = {
            'socle': False, 'parcours': False, 'ensComp': False}

        # les 8 composantes du socle :
        text = QtWidgets.QApplication.translate(
            'main', '8 components of the referential')
        socleGroup = QtWidgets.QGroupBox(text)
        helpMessage = QtWidgets.QApplication.translate(
            'main', 
            'Double-click on an element allows you to edit.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpMessage))
        self.socleList = QtWidgets.QListWidget()
        grid = QtWidgets.QGridLayout()
        grid.addWidget(helpLabel, 1, 0)
        grid.addWidget(self.socleList,   2, 0)
        socleGroup.setLayout(grid)

        # les parcours :
        text = QtWidgets.QApplication.translate(
            'main', 'Educational Tours')
        parcoursGroup = QtWidgets.QGroupBox(text)
        self.parcoursList = QtWidgets.QListWidget()
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.parcoursList,   1, 0)
        parcoursGroup.setLayout(grid)
        parcoursGroup.setMaximumWidth(500)

        # les enseignements de complément :
        text = QtWidgets.QApplication.translate(
            'main', 'Additional Teaching')
        ensCompGroup = QtWidgets.QGroupBox(text)
        self.ensCompList = QtWidgets.QListWidget()
        # matières :
        text = QtWidgets.QApplication.translate(
            'main', 'Subjects:')
        matieresLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.matieresList = QtWidgets.QListWidget()
        self.matieresList.setMaximumWidth(200)
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.ensCompList,   1, 0)
        grid.addWidget(self.matieresList,  1, 1)
        ensCompGroup.setLayout(grid)

        # on agence tout ça :
        hBoxTop = QtWidgets.QHBoxLayout()
        hBoxTop.addWidget(socleGroup)
        hBox2 = QtWidgets.QHBoxLayout()
        hBox2.addWidget(parcoursGroup)
        hBox2.addWidget(ensCompGroup)
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(hBoxTop)
        mainLayout.addLayout(hBox2)
        #mainLayout.addStretch(1)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les tables admin.lsu, commun.lsu
        et éventuellement commun.referentiel
        """
        self.fromSoft = True
        self.modified = False
        self.whatIsModified = {
            'socle': False, 'parcours': False, 'ensComp': False}
        query_admin = utils_db.query(admin.db_admin)
        query_commun = utils_db.query(self.main.db_commun)

        lsuTables = {
            'socle': {}, 'parcours': {}, 'matiere': {}, 'ensComp': {}}
        commandLine = (
            'SELECT * FROM lsu '
            'WHERE lsuWhat IN ("socle", "parcours", "matiere", "ensComp")')
        query_admin = utils_db.queryExecute(
            commandLine, query=query_admin)
        while query_admin.next():
            lsuWhat = query_admin.value(0)
            if lsuWhat in lsuTables:
                lsu1 = query_admin.value(1)
                lsu2 = query_admin.value(2)
                lsu3 = query_admin.value(3)
                lsu4 = query_admin.value(4)
                lsuTables[lsuWhat][lsu1] = (lsu2, lsu3, lsu4, 'green')
        query_commun = utils_db.queryExecute(
            commandLine, query=query_commun)
        while query_commun.next():
            lsuWhat = query_commun.value(0)
            if lsuWhat in lsuTables:
                lsu2 = query_commun.value(2)
                lsu3 = query_commun.value(3)
                lsuTables[lsuWhat][lsu2] = (lsu3, 'green')

        # liste des 8 composantes du socle :
        self.socleList.clear()
        # en cas de première utilisation :
        if len(lsuTables['socle']) < 1:
            commandLine_commun = utils_db.q_selectAllFrom.format('referentiel')
            query_commun = utils_db.queryExecute(
                commandLine_commun, query=query_commun)
            while query_commun.next():
                idCpt = int(query_commun.value(0))
                code = query_commun.value(1)
                (lsuCode, index) = utils.LSU[
                    'SOCLE_COMPONENTS']['REFERENTIAL'].get(code, ('', 0))
                if index > 0:
                    lsuTables['socle'][lsuCode] = (
                        code, query_commun.value(index), '', 'no')
        # les 8 composantes du socle :
        for lsu1 in utils.LSU['SOCLE_COMPONENTS']['ORDER_REFERENTIAL']:
            (lsu2, label, index) = utils.LSU['SOCLE_COMPONENTS'][lsu1]
            (lsu2, lsu3, lsu4, color) = lsuTables['socle'].get(
                lsu1, 
                ('', '', '', 'no'))
            if color == 'no':
                self.whatIsModified['socle'] = True
                self.modified = True
            if len(label) < 25:
                tabs = '\t\t'
            else:
                tabs = '\t'
            text = utils_functions.u('{0} : {1} {2} \t [{3}]').format(
                label, tabs, lsu2, lsu3)
            item = QtWidgets.QListWidgetItem(text)
            item.setData(
                QtCore.Qt.UserRole, 
                [lsu1, label, lsu2, lsu3])
            item.setIcon(ICONS[color])
            self.socleList.addItem(item)
        self.socleList.setCurrentRow(0)

        # liste des parcours :
        self.parcoursList.clear()
        for lsu2 in utils.LSU['PARCOURS']:
            (lsu3, color) = lsuTables['parcours'].get(
                lsu2, 
                (utils.LSU['TRANSLATIONS'][lsu2], 'no'))
            if color == 'no':
                self.whatIsModified['parcours'] = True
                self.modified = True
            text = utils_functions.u('{0} :  \t {1}').format(
                lsu2, lsu3)
            item = QtWidgets.QListWidgetItem(text)
            item.setData(
                QtCore.Qt.UserRole, 
                [lsu2, lsu3])
            item.setIcon(ICONS[color])
            self.parcoursList.addItem(item)

        # récupération des matières utilisables :
        self.matieresList.clear()
        for matiereCode in admin.MATIERES['BLT']:
            (id_matiere, matiereName, matiereLabel) = admin.MATIERES[
                'Code2Matiere'][matiereCode]
            (lsu2, lsu3, lsu4, color) = lsuTables['matiere'].get(
                matiereCode, 
                ('', '', 'S', ''))
            if len(lsu3) > 0:
                item = QtWidgets.QListWidgetItem(matiereName)
                item.setData(
                    QtCore.Qt.UserRole, 
                    matiereCode)
                item.setCheckState(QtCore.Qt.Unchecked)
                self.matieresList.addItem(item)
        # liste des enseignements de complément (sauf AUC) :
        self.ensCompList.clear()
        for lsu1 in utils.LSU['ENS_COMPLEMENT'][1:]:
            (lsu2, lsu3, lsu4, color) = lsuTables['ensComp'].get(
                lsu1, 
                ('', '', '', 'no'))
            if (lsu1 == 'LCA') and (len(lsu2) < 1):
                commandLine = (
                    'SELECT * FROM lsu '
                    'WHERE lsuWhat="matiere" AND lsu2 LIKE "LCA%"')
                query_admin = utils_db.queryExecute(
                    commandLine, query=query_admin)
                while query_admin.next():
                    replaceWhat = query_admin.value(1)
                    if len(lsu2) < 1:
                        lsu2 = replaceWhat
                    else:
                        lsu2 = utils_functions.u('{0}|{1}').format(lsu2, replaceWhat)
            if color == 'no':
                self.whatIsModified['ensComp'] = True
                self.modified = True
            text = utils_functions.u('{0} : {1} [{2}]').format(
                lsu1, utils.LSU['TRANSLATIONS'][lsu1], lsu2)
            item = QtWidgets.QListWidgetItem(text)
            item.setData(
                QtCore.Qt.UserRole, 
                [lsu1, utils.LSU['TRANSLATIONS'][lsu1], lsu2])
            item.setIcon(ICONS[color])
            self.ensCompList.addItem(item)
        self.fromSoft = False

    def createConnexions(self):
        self.socleList.itemDoubleClicked.connect(self.doChooseFromList)
        self.ensCompList.currentItemChanged.connect(self.doCurrentItemChanged)
        self.matieresList.itemClicked.connect(self.doChanged)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection.
        On met les champs à jour.
        """
        self.fromSoft = True
        try:
            lastData = current.data(QtCore.Qt.UserRole)
            matieres = lastData[2].split('|')
            for index in range(self.matieresList.count()):
                item = self.matieresList.item(index)
                matiereCode = item.data(QtCore.Qt.UserRole)
                if matiereCode in matieres:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
        except:
            pass
        self.fromSoft = False

    def doChanged(self, value=-1):
        """
        un champ a été modifié.
        On met à jour les données et l'affichage.
        """
        if self.fromSoft:
            return
        matieres = ''
        for index in range(self.matieresList.count()):
            item = self.matieresList.item(index)
            if item.checkState() == QtCore.Qt.Checked:
                matiereCode = item.data(QtCore.Qt.UserRole)
                matieres = utils_functions.u('{0}|{1}').format(matieres, matiereCode)
        matieres = matieres[1:]
        current = self.ensCompList.currentItem()
        data = current.data(QtCore.Qt.UserRole)
        if data[2] != matieres:
            data[2] = matieres
            current.setData(QtCore.Qt.UserRole, data)
            text = utils_functions.u('{0} : {1} [{2}]').format(
                data[0], data[1], data[2])
            current.setText(text)
            self.doModified(what='ensComp')

    def doChooseFromList(self):
        """
        appel d'un dialog spécifique
        """
        import prof_itemsbilans
        dialog = prof_itemsbilans.ChooseCptFromListDlg(
            parent=self.main, tableName='referentiel', onlyCpt=False)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            for selectedIndex in dialog.cptView.selectedIndexes():
                if selectedIndex.column() == 0:
                    item = selectedIndex.data(QtCore.Qt.UserRole)
                    id_cpt = item.itemData[3]
            commandLine_commun = utils_db.q_selectAllFromWhere.format(
                'referentiel', 'id', id_cpt)
            query_commun = utils_db.queryExecute(
                commandLine_commun, db=self.main.db_commun)
            while query_commun.next():
                lsu2 = query_commun.value(1)
                if len(query_commun.value(2)) > 0:
                    lsu3 = query_commun.value(2)
                elif len(query_commun.value(3)) > 0:
                    lsu3 = query_commun.value(3)
                elif len(query_commun.value(4)) > 0:
                    lsu3 = query_commun.value(4)
                elif len(query_commun.value(5)) > 0:
                    lsu3 = query_commun.value(5)
                else:
                    lsu3 = ''
            current = self.socleList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            lsu1 = data[0]
            label = data[1]
            data[2] = lsu2
            data[3] = lsu3
            current.setData(QtCore.Qt.UserRole, data)
            if len(label) < 25:
                tabs = '\t\t'
            else:
                tabs = '\t'
            text = utils_functions.u('{0} : {1} {2} \t [{3}]').format(
                label, tabs, lsu2, lsu3)
            current.setText(text)
            self.doModified(what='socle')

    def doModified(self, what='socle'):
        self.whatIsModified[what] = True
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        commandLines_admin = []
        lines_admin = []
        commandLines_commun = []
        lines_commun = []

        # les 8 composantes du socle :
        if self.whatIsModified['socle']:
            commandLines_admin.append(
                utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'socle'))
            for index in range(self.socleList.count()):
                # 'socle', codeXML, codeVERAC, labelVERAC
                item = self.socleList.item(index)
                data = item.data(QtCore.Qt.UserRole)
                lines_admin.append(('socle', data[0], data[2], data[3], '', ''))
        # les parcours :
        if self.whatIsModified['parcours']:
            commandLines_commun.append(
                utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'parcours'))
            for index in range(self.parcoursList.count()):
                item = self.parcoursList.item(index)
                data = item.data(QtCore.Qt.UserRole)
                lines_commun.append(('parcours', '', data[0], data[1], '', '', '', '', ''))
        # les enseignements de complément :
        if self.whatIsModified['ensComp']:
            commandLines_admin.append(
                utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'ensComp'))
            for index in range(self.ensCompList.count()):
                item = self.ensCompList.item(index)
                data = item.data(QtCore.Qt.UserRole)
                lines_admin.append(('ensComp', data[0], data[2], '', '', ''))

        # écriture et enregistrement :
        if len(commandLines_admin) > 0:
            utils_functions.doWaitCursor()
            query_admin, transactionOK_admin = utils_db.queryTransactionDB(
                admin.db_admin)
            try:
                query_admin = utils_db.queryExecute(
                    commandLines_admin, query=query_admin)
                if len(lines_admin) > 0:
                    query_admin = utils_db.queryExecute(
                        {utils_db.insertInto('lsu', 6): lines_admin}, 
                        query=query_admin)
                # on exporte en csv :
                for table in ('lsu', ):
                    fileName = utils_functions.u(
                        '{0}/admin_tables/{1}.csv').format(admin.csvDir, table)
                    utils_export.exportTable2Csv(
                        self.main, admin.db_admin, table, fileName, msgFin=False)
            finally:
                utils_db.endTransaction(
                    query_admin, admin.db_admin, transactionOK_admin)
                utils_filesdirs.removeAndCopy(
                    admin.dbFileTemp_admin, admin.dbFile_admin)
            utils_functions.restoreCursor()
        if len(commandLines_commun) > 0:
            utils_functions.doWaitCursor()
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
            try:
                query_commun = utils_db.queryExecute(
                    commandLines_commun, query=query_commun)
                if len(lines_commun) > 0:
                    query_commun = utils_db.queryExecute(
                        {utils_db.insertInto('lsu', 9): lines_commun}, 
                        query=query_commun)
                # on exporte en csv :
                for table in ('lsu', ):
                    fileName = utils_functions.u(
                        '{0}/commun_tables/{1}.csv').format(admin.csvDir, table)
                    utils_export.exportTable2Csv(
                        self.main, self.main.db_commun, table, fileName, msgFin=False)
            finally:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
                utils_filesdirs.removeAndCopy(self.main.dbFileTemp_commun, self.main.dbFile_commun)
                utils_filesdirs.removeAndCopy(self.main.dbFileTemp_commun, admin.dbFileFtp_commun)
                utils_functions.restoreCursor()
                admin.mustUploadDBs(self.main, databases=['commun'], msgFin=False)

        self.whatIsModified = {
            'socle': False, 'parcours': False, 'ensComp': False}
        self.modified = False
































class ChecksDlg_EPI(QtWidgets.QDialog):
    """
    Onglet de vérifications n°4.
        * les EPI de référence
    """
    def __init__(self, parent=None):
        super(ChecksDlg_EPI, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        self.title = 'EPI'
        # des modifications ont été faites :
        self.modified = False

        self.episMatieresOk = True
        self.episData = {}

        # Les epis de référence :
        text = QtWidgets.QApplication.translate(
            'main', 
            'EPI management (Interdisciplinary Teaching Practices) of the establishment')
        episGroup = QtWidgets.QGroupBox(text)
        # liste :
        self.episList = QtWidgets.QListWidget()
        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(
            self, buttons=('add', 'delete'), layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # thématique :
        text = QtWidgets.QApplication.translate(
            'main', 'Thematic:')
        themeLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.themeComboBox = QtWidgets.QComboBox()
        # intitulé :
        text = QtWidgets.QApplication.translate(
            'main', 'Title:')
        intituleLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.intituleEdit = QtWidgets.QLineEdit()
        # matières :
        text = QtWidgets.QApplication.translate(
            'main', 'Subjects:')
        matieresLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.matieresList = QtWidgets.QListWidget()
        # description :
        text = QtWidgets.QApplication.translate(
            'main', 'Description:')
        descriptionLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.descriptionEdit = QtWidgets.QTextEdit()

        # avertissement (si pas de matières EPI) :
        self.mdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpLSU-noEpis')
        self.mdHelpView.setVisible(False)

        # on met tout dans une grille :
        vBox1 = QtWidgets.QVBoxLayout()
        vBox1.addWidget(self.mdHelpView)
        vBox1.addWidget(self.episList)
        grid2 = QtWidgets.QGridLayout()
        grid2.addLayout(buttonsLayout,    1, 2)
        grid2.addWidget(themeLabel,        2, 1, 1, 2)
        grid2.addWidget(self.themeComboBox,    3, 2)
        grid2.addWidget(intituleLabel,        4, 1, 1, 2)
        grid2.addWidget(self.intituleEdit,    5, 2)
        grid2.addWidget(matieresLabel,          6, 1, 1, 2)
        grid2.addWidget(self.matieresList,  7, 2)
        grid2.addWidget(descriptionLabel,          8, 1, 1, 2)
        grid2.addWidget(self.descriptionEdit,  9, 2)
        vBox2 = QtWidgets.QVBoxLayout()
        vBox2.addLayout(grid2)
        vBox2.addStretch(1)
        grid = QtWidgets.QGridLayout()
        grid.addLayout(vBox1,            1, 0)
        grid.addLayout(vBox2,            1, 1)
        episGroup.setLayout(grid)

        # on agence tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(episGroup)
        #mainLayout.addStretch(1)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les tables commun.lsu et commun.matieres
        """
        self.fromSoft = True
        self.modified = False

        self.episData = {'THEMES': {}, }

        # récupération-vérification des 8 matières EPI :
        self.episMatieresOk = True
        for theme in utils.LSU['EPIS']:
            (matiereId, matiereName, matiereLabel) = admin.MATIERES['Code2Matiere'].get(theme, (0, '', ''))
            if matiereId > 0:
                self.episData['THEMES'][theme] = (matiereId, matiereName, matiereLabel)
                self.episData[theme] = []
            else:
                self.episMatieresOk = False
                self.fromSoft = False
                # on affiche le message d'avertissement :
                self.mdHelpView.setVisible(True)
                self.episList.setVisible(False)
                return

        # récupération des matières utilisables :
        self.matieresList.clear()
        lsuTable = {}
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'matiere')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            matiereCode = query_admin.value(1)
            lsu2 = query_admin.value(2)
            lsu3 = query_admin.value(3)
            lsu4 = query_admin.value(4)
            lsuTable[matiereCode] = (lsu2, lsu3, lsu4)
        for matiereCode in admin.MATIERES['BLT']:
            (id_matiere, matiereName, matiereLabel) = admin.MATIERES['Code2Matiere'][matiereCode]
            (lsu2, lsu3, lsu4) = lsuTable.get(matiereCode, ('', '', 'S'))
            if len(lsu3) > 0:
                item = QtWidgets.QListWidgetItem(matiereName)
                item.setData(
                    QtCore.Qt.UserRole, 
                    matiereCode)
                item.setCheckState(QtCore.Qt.Unchecked)
                self.matieresList.addItem(item)

        # récupération des epis existants :
        commandLine_commun = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'epis')
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=self.main.db_commun)
        self.idMax = 0
        while query_commun.next():
            id_epi = int(query_commun.value(1)) # id
            epiTheme = query_commun.value(2) # thematique
            epiName = query_commun.value(3) # intitule
            epiMatieres = query_commun.value(4) # discipline-refs
            epiLabel = query_commun.value(5) # description
            self.episData[epiTheme].append([id_epi, epiName, epiMatieres, epiLabel])
            if id_epi > self.idMax:
                self.idMax = id_epi

        # les thèmes :
        self.themeComboBox.clear()
        for theme in utils.LSU['EPIS']:
            text = self.episData['THEMES'][theme][2]
            self.themeComboBox.addItem(text, theme)
        index = self.reloadEpisList()
        self.episList.setCurrentRow(index)
        self.fromSoft = False

    def reloadEpisList(self, idEpiToFind=-1):
        if not(self.episMatieresOk):
            return
        self.episList.clear()
        boldFont = QtGui.QFont()
        boldFont.setBold(True)
        index = -1
        for theme in utils.LSU['EPIS']:
            # on commence par afficher le titre :
            text = self.episData['THEMES'][theme][2]
            item = QtWidgets.QListWidgetItem(text)
            item.setForeground(QtGui.QBrush(utils.colorGray))
            item.setFont(boldFont)
            item.setFlags(QtCore.Qt.NoItemFlags)
            self.episList.addItem(item)
            for [id_epi, epiName, epiMatieres, epiLabel] in self.episData[theme]:
                item = QtWidgets.QListWidgetItem(
                    utils_functions.u('  {0}').format(epiName))
                item.setData(
                    QtCore.Qt.UserRole, 
                    [id_epi, theme, epiName, epiMatieres, epiLabel])
                self.episList.addItem(item)
                itemIndex = self.episList.indexFromItem(item).row()
                if idEpiToFind < 0:
                    index = itemIndex
                elif id_epi == idEpiToFind:
                    index = itemIndex
        canDelete = (self.episList.count() > len(utils.LSU['EPIS']))
        self.editButtons[1]['delete'].setEnabled(canDelete)
        return index

    def createConnexions(self):
        if not(self.episMatieresOk):
            return
        self.episList.currentItemChanged.connect(self.doCurrentItemChanged)
        lineEdits = (#QLineEdit
            self.intituleEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.textEdited.connect(self.doChanged)
        self.themeComboBox.activated.connect(self.doChanged)
        self.matieresList.itemClicked.connect(self.doChanged)
        self.descriptionEdit.textChanged.connect(self.doChanged)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection.
        On met les champs à jour.
        """
        if not(self.episMatieresOk):
            return
        self.fromSoft = True
        try:
            lastData = current.data(QtCore.Qt.UserRole)
            actions = {
                self.intituleEdit: ('TEXT', 2),
                self.descriptionEdit: ('TEXTEDIT', 4),
                self.themeComboBox: ('COMBOBOX', 1), }
            for action in actions:
                if actions[action][0] == 'TEXT':
                    action.setText(
                        utils_functions.u(lastData[actions[action][1]]))
                elif actions[action][0] == 'TEXTEDIT':
                    action.setPlainText(
                        utils_functions.u(lastData[actions[action][1]]))
                elif actions[action][0] == 'COMBOBOX':
                    theme = lastData[actions[action][1]]
                    index = action.findData(theme, QtCore.Qt.UserRole)
                    action.setCurrentIndex(index)
            matieres = lastData[3].split('|')
            for index in range(self.matieresList.count()):
                item = self.matieresList.item(index)
                matiereCode = item.data(QtCore.Qt.UserRole)
                if matiereCode in matieres:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
        except:
            pass
        self.fromSoft = False

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        if not(self.episMatieresOk):
            return
        what = self.sender().objectName()
        if what == 'add':
            text = QtWidgets.QApplication.translate('main', 'New Record')
            try:
                theme = self.themeComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = self.themeComboBox.currentIndex()
                theme = self.themeComboBox.itemData(index)
            if not(theme in utils.LSU['EPIS']):
                theme = utils.LSU['EPIS'][-1]
            self.idMax += 1
            self.episData[theme].append([self.idMax, text, '', ''])
            index = self.reloadEpisList(self.idMax)
            self.episList.setCurrentRow(index)
            self.doModified()

        elif what == 'delete':
            current = self.episList.currentItem()
            try:
                data = current.data(QtCore.Qt.UserRole)
                theme = data[1]
                self.episData[theme].remove([data[0], data[2], data[3], data[4]])
            except:
                pass
            index = self.reloadEpisList()
            self.episList.setCurrentRow(index)
            self.doModified()

    def doChanged(self, value=-1):
        """
        un champ a été modifié.
        On met à jour les données et l'affichage.
        """
        if not(self.episMatieresOk):
            return
        if self.fromSoft:
            return
        if type(self.sender()) == QtWidgets.QLineEdit:
            if not(self.sender().isModified()):
                return

        if self.sender() == self.themeComboBox:
            # pour un changement de thème on rechargera la liste :
            try:
                theme = self.themeComboBox.currentData(QtCore.Qt.UserRole)
            except:
                index = self.themeComboBox.currentIndex()
                theme = self.themeComboBox.itemData(index)
            current = self.episList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            oldTheme = data[1]
            self.episData[oldTheme].remove([data[0], data[2], data[3], data[4]])
            self.episData[theme].append([data[0], data[2], data[3], data[4]])
            index = self.reloadEpisList(data[0])
            self.episList.setCurrentRow(index)
            self.doModified()
        elif self.sender() == self.matieresList:
            matieres = ''
            for index in range(self.matieresList.count()):
                item = self.matieresList.item(index)
                if item.checkState() == QtCore.Qt.Checked:
                    matiereCode = item.data(QtCore.Qt.UserRole)
                    matieres = utils_functions.u('{0}|{1}').format(matieres, matiereCode)
            matieres = matieres[1:]
            current = self.episList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if data[3] != matieres:
                data[3] = matieres
                current.setData(QtCore.Qt.UserRole, data)
                for epi in self.episData[data[1]]:
                    if epi[0] == data[0]:
                        epi[1] = data[2]
                        epi[2] = data[3]
                        epi[3] = data[4]
                self.doModified()
        else:
            try:
                newText = self.sender().text()
            except:
                newText = self.sender().toPlainText()
            current = self.episList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if self.sender() == self.intituleEdit:
                data[2] = newText
                current.setData(QtCore.Qt.UserRole, data)
                current.setText(utils_functions.u('  {0}').format(newText))
            elif self.sender() == self.descriptionEdit:
                data[4] = newText
                current.setData(QtCore.Qt.UserRole, data)
            for epi in self.episData[data[1]]:
                if epi[0] == data[0]:
                    epi[1] = data[2]
                    epi[2] = data[3]
                    epi[3] = data[4]
            self.doModified()

    def doModified(self):
        if not(self.episMatieresOk):
            return
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.episMatieresOk):
            return
        if not(self.modified):
            return
        commandLines_commun = []
        lines = []

        # les epis :
        commandLines_commun.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'epis'))
        commandLineInsert = utils_db.insertInto('lsu', 9)
        for epiTheme in utils.LSU['EPIS']:
            for epi in self.episData[epiTheme]:
                lines.append(('epis', epi[0], epiTheme, epi[1], epi[2], epi[3], '', '', ''))

        # écriture et enregistrement :
        if len(commandLines_commun) > 0:
            utils_functions.doWaitCursor()
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
            try:
                query_commun = utils_db.queryExecute(
                    commandLines_commun, query=query_commun)
                if len(lines) > 0:
                    query_commun = utils_db.queryExecute(
                        {commandLineInsert: lines}, query=query_commun)
                # on exporte en csv :
                for table in ('lsu', ):
                    fileName = utils_functions.u(
                        '{0}/commun_tables/{1}.csv').format(admin.csvDir, table)
                    utils_export.exportTable2Csv(
                        self.main, self.main.db_commun, table, fileName, msgFin=False)
            finally:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
                utils_filesdirs.removeAndCopy(self.main.dbFileTemp_commun, self.main.dbFile_commun)
                utils_filesdirs.removeAndCopy(self.main.dbFileTemp_commun, admin.dbFileFtp_commun)
                utils_functions.restoreCursor()
                admin.mustUploadDBs(self.main, databases=['commun'], msgFin=False)

        self.modified = False
































class ChecksDlg_AP(QtWidgets.QDialog):
    """
    Onglet de vérifications n°5.
        * les AP de référence
    """
    def __init__(self, parent=None):
        super(ChecksDlg_AP, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        self.title = 'AP'
        # des modifications ont été faites :
        self.modified = False

        self.apsMatieresOk = True
        self.apsData = {}

        # Les AP de référence :
        text = QtWidgets.QApplication.translate(
            'main', 
            'AP management (Personalized support) of the establishment')
        apsGroup = QtWidgets.QGroupBox(text)
        # liste :
        self.apsList = QtWidgets.QListWidget()
        # la zone d'édition :
        self.editButtons = utils_functions.createEditButtons(
            self, buttons=('add', 'delete'), layout=True)
        buttonsLayout = self.editButtons[0]
        for button in self.editButtons[1]:
            self.editButtons[1][button].clicked.connect(self.doEditButton)
        # intitulé :
        text = QtWidgets.QApplication.translate(
            'main', 'Title:')
        intituleLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.intituleEdit = QtWidgets.QLineEdit()
        # matières :
        text = QtWidgets.QApplication.translate(
            'main', 'Subjects:')
        matieresLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.matieresList = QtWidgets.QListWidget()
        # description :
        text = QtWidgets.QApplication.translate(
            'main', 'Description:')
        descriptionLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.descriptionEdit = QtWidgets.QTextEdit()

        # avertissement (si pas de matière AP_LSU) :
        self.mdHelpView = utils_markdown.MarkdownHelpViewer(
            self.main, mdFile='translations/helpLSU-noAP')
        self.mdHelpView.setVisible(False)

        # on met tout dans une grille :
        vBox1 = QtWidgets.QVBoxLayout()
        vBox1.addWidget(self.mdHelpView)
        vBox1.addWidget(self.apsList)
        grid2 = QtWidgets.QGridLayout()
        grid2.addLayout(buttonsLayout,    1, 2)
        grid2.addWidget(intituleLabel,        4, 1, 1, 2)
        grid2.addWidget(self.intituleEdit,    5, 2)
        grid2.addWidget(matieresLabel,          6, 1, 1, 2)
        grid2.addWidget(self.matieresList,  7, 2)
        grid2.addWidget(descriptionLabel,          8, 1, 1, 2)
        grid2.addWidget(self.descriptionEdit,  9, 2)
        vBox2 = QtWidgets.QVBoxLayout()
        vBox2.addLayout(grid2)
        vBox2.addStretch(1)
        grid = QtWidgets.QGridLayout()
        grid.addLayout(vBox1,            1, 0)
        grid.addLayout(vBox2,            1, 1)
        apsGroup.setLayout(grid)

        # on agence tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(apsGroup)
        #mainLayout.addStretch(1)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les tables commun.lsu et commun.matieres
        """
        self.fromSoft = True
        self.modified = False

        self.apsData = {'AP_LSU': (0, '', ''), 'APS': []}

        # récupération-vérification de la matière AP_LSU :
        self.apsMatieresOk = True
        (matiereId, matiereName, matiereLabel) = admin.MATIERES['Code2Matiere'].get('AP_LSU', (0, '', ''))
        if matiereId > 0:
            self.apsData['AP_LSU'] = (matiereId, matiereName, matiereLabel)
        else:
            self.apsMatieresOk = False
            self.fromSoft = False
            # on affiche le message d'avertissement :
            self.mdHelpView.setVisible(True)
            self.apsList.setVisible(False)
            return

        # récupération des matières utilisables :
        self.matieresList.clear()
        lsuTable = {}
        query_admin = utils_db.query(admin.db_admin)
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'matiere')
        query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
        while query_admin.next():
            matiereCode = query_admin.value(1)
            lsu2 = query_admin.value(2)
            lsu3 = query_admin.value(3)
            lsu4 = query_admin.value(4)
            lsuTable[matiereCode] = (lsu2, lsu3, lsu4)
        for matiereCode in admin.MATIERES['BLT']:
            (id_matiere, matiereName, matiereLabel) = admin.MATIERES['Code2Matiere'][matiereCode]
            (lsu2, lsu3, lsu4) = lsuTable.get(matiereCode, ('', '', 'S'))
            if len(lsu3) > 0:
                item = QtWidgets.QListWidgetItem(matiereName)
                item.setData(
                    QtCore.Qt.UserRole, 
                    matiereCode)
                item.setCheckState(QtCore.Qt.Unchecked)
                self.matieresList.addItem(item)

        # récupération des aps existants :
        commandLine_commun = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'aps')
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=self.main.db_commun)
        self.idMax = 0
        while query_commun.next():
            id_ap = int(query_commun.value(1)) # id
            apName = query_commun.value(3) # intitule
            apMatieres = query_commun.value(4) # discipline-refs
            apLabel = query_commun.value(5) # description
            self.apsData['APS'].append([id_ap, apName, apMatieres, apLabel])
            if id_ap > self.idMax:
                self.idMax = id_ap

        index = self.reloadApsList()
        self.apsList.setCurrentRow(index)
        self.fromSoft = False

    def reloadApsList(self, idApToFind=-1):
        if not(self.apsMatieresOk):
            return
        self.apsList.clear()
        index = -1

        for [id_ap, apName, apMatieres, apLabel] in self.apsData['APS']:
            item = QtWidgets.QListWidgetItem(apName)
            item.setData(
                QtCore.Qt.UserRole, 
                [id_ap, apName, apMatieres, apLabel])
            self.apsList.addItem(item)
            itemIndex = self.apsList.indexFromItem(item).row()
            if idApToFind < 0:
                index = itemIndex
            elif id_ap == idApToFind:
                index = itemIndex
        canDelete = (self.apsList.count() > 0)
        self.editButtons[1]['delete'].setEnabled(canDelete)
        return index

    def createConnexions(self):
        if not(self.apsMatieresOk):
            return
        self.apsList.currentItemChanged.connect(self.doCurrentItemChanged)
        lineEdits = (#QLineEdit
            self.intituleEdit, 
            )
        for lineEdit in lineEdits:
            lineEdit.textEdited.connect(self.doChanged)
        self.matieresList.itemClicked.connect(self.doChanged)
        self.descriptionEdit.textChanged.connect(self.doChanged)

    def doCurrentItemChanged(self, current, previous):
        """
        changement de sélection.
        On met les champs à jour.
        """
        if not(self.apsMatieresOk):
            return
        self.fromSoft = True
        try:
            lastData = current.data(QtCore.Qt.UserRole)
            actions = {
                self.intituleEdit: ('TEXT', 1),
                self.descriptionEdit: ('TEXTEDIT', 3), }
            for action in actions:
                if actions[action][0] == 'TEXT':
                    action.setText(
                        utils_functions.u(lastData[actions[action][1]]))
                elif actions[action][0] == 'TEXTEDIT':
                    action.setPlainText(
                        utils_functions.u(lastData[actions[action][1]]))
            matieres = lastData[2].split('|')
            for index in range(self.matieresList.count()):
                item = self.matieresList.item(index)
                matiereCode = item.data(QtCore.Qt.UserRole)
                if matiereCode in matieres:
                    item.setCheckState(QtCore.Qt.Checked)
                else:
                    item.setCheckState(QtCore.Qt.Unchecked)
        except:
            pass
        self.fromSoft = False

    def doEditButton(self):
        """
        actions des boutons add, delete, etc...
        """
        if not(self.apsMatieresOk):
            return
        what = self.sender().objectName()
        if what == 'add':
            text = QtWidgets.QApplication.translate('main', 'New Record')
            self.idMax += 1
            self.apsData['APS'].append([self.idMax, text, '', ''])
            index = self.reloadApsList(self.idMax)
            self.apsList.setCurrentRow(index)
            self.doModified()

        elif what == 'delete':
            current = self.apsList.currentItem()
            try:
                data = current.data(QtCore.Qt.UserRole)
                self.apsData['APS'].remove([data[0], data[1], data[2], data[3]])
            except:
                pass
            index = self.reloadApsList()
            self.apsList.setCurrentRow(index)
            self.doModified()

    def doChanged(self, value=-1):
        """
        un champ a été modifié.
        On met à jour les données et l'affichage.
        """
        if not(self.apsMatieresOk):
            return
        if self.fromSoft:
            return
        if type(self.sender()) == QtWidgets.QLineEdit:
            if not(self.sender().isModified()):
                return

        if self.sender() == self.matieresList:
            matieres = ''
            for index in range(self.matieresList.count()):
                item = self.matieresList.item(index)
                if item.checkState() == QtCore.Qt.Checked:
                    matiereCode = item.data(QtCore.Qt.UserRole)
                    matieres = utils_functions.u('{0}|{1}').format(matieres, matiereCode)
            matieres = matieres[1:]
            current = self.apsList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if data[2] != matieres:
                data[2] = matieres
                current.setData(QtCore.Qt.UserRole, data)
                for ap in self.apsData['APS']:
                    if ap[0] == data[0]:
                        ap[1] = data[1]
                        ap[2] = data[2]
                        ap[3] = data[3]
                self.doModified()
        else:
            try:
                newText = self.sender().text()
            except:
                newText = self.sender().toPlainText()
            current = self.apsList.currentItem()
            data = current.data(QtCore.Qt.UserRole)
            if self.sender() == self.intituleEdit:
                data[1] = newText
                current.setData(QtCore.Qt.UserRole, data)
                current.setText(newText)
            elif self.sender() == self.descriptionEdit:
                data[3] = newText
                current.setData(QtCore.Qt.UserRole, data)
            for ap in self.apsData['APS']:
                if ap[0] == data[0]:
                    ap[1] = data[1]
                    ap[2] = data[2]
                    ap[3] = data[3]
            self.doModified()

    def doModified(self):
        if not(self.apsMatieresOk):
            return
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.apsMatieresOk):
            return
        if not(self.modified):
            return
        commandLines_commun = []
        lines = []

        # les aps :
        commandLines_commun.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'aps'))
        commandLineInsert = utils_db.insertInto('lsu', 9)
        for ap in self.apsData['APS']:
            lines.append(('aps', ap[0], '', ap[1], ap[2], ap[3], '', '', ''))

        # écriture et enregistrement :
        if len(commandLines_commun) > 0:
            utils_functions.doWaitCursor()
            query_commun, transactionOK_commun = utils_db.queryTransactionDB(
                self.main.db_commun)
            try:
                query_commun = utils_db.queryExecute(
                    commandLines_commun, query=query_commun)
                if len(lines) > 0:
                    query_commun = utils_db.queryExecute(
                        {commandLineInsert: lines}, query=query_commun)
                # on exporte en csv :
                for table in ('lsu', ):
                    fileName = utils_functions.u(
                        '{0}/commun_tables/{1}.csv').format(admin.csvDir, table)
                    utils_export.exportTable2Csv(
                        self.main, self.main.db_commun, table, fileName, msgFin=False)
            finally:
                utils_db.endTransaction(
                    query_commun, self.main.db_commun, transactionOK_commun)
                utils_filesdirs.removeAndCopy(self.main.dbFileTemp_commun, self.main.dbFile_commun)
                utils_filesdirs.removeAndCopy(self.main.dbFileTemp_commun, admin.dbFileFtp_commun)
                utils_functions.restoreCursor()
                admin.mustUploadDBs(self.main, databases=['commun'], msgFin=False)

        self.modified = False
































class ChecksDlg_CN(QtWidgets.QDialog):
    # ICI cptNum
    """
    Onglet de vérifications n°6.
        * les compétences numériques
    """
    def __init__(self, parent=None):
        super(ChecksDlg_CN, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        self.title = 'CN'
        # des modifications ont été faites :
        self.modified = False
        self.cptNumData = {'appreciation': '', 'classes': []}

        # le titre :
        text = QtWidgets.QApplication.translate(
            'main', 
            'Digital skills management')
        cptNumGroup = QtWidgets.QGroupBox(text)

        # appréciation commune :
        text = QtWidgets.QApplication.translate(
            'main', 'Appreciation for the class:')
        appreciationLabel = QtWidgets.QLabel(
            utils_functions.u('<b>{0}</b>').format(text))
        self.appreciationEdit = QtWidgets.QTextEdit()

        # liste des classes :
        text = QtWidgets.QApplication.translate(
            'main', 'Classes:')
        classesLabel = QtWidgets.QLabel(utils_functions.u(
            '<b>{0}</b>').format(text))
        self.classesList = QtWidgets.QListWidget()

        # on met tout dans une grille :
        grid1 = QtWidgets.QGridLayout()
        grid1.addWidget(appreciationLabel,      1, 1, 1, 2)
        grid1.addWidget(self.appreciationEdit,  2, 2)
        vBox1 = QtWidgets.QVBoxLayout()
        vBox1.addLayout(grid1)
        vBox1.addStretch(1)
        grid2 = QtWidgets.QGridLayout()
        grid2.addWidget(classesLabel,      1, 1, 1, 2)
        grid2.addWidget(self.classesList,  2, 2)
        vBox2 = QtWidgets.QVBoxLayout()
        vBox2.addLayout(grid2)
        vBox2.addStretch(1)
        grid = QtWidgets.QGridLayout()
        grid.addLayout(vBox1,            1, 0)
        grid.addLayout(vBox2,            1, 1)
        cptNumGroup.setLayout(grid)

        # on agence tout ça :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(cptNumGroup)
        #mainLayout.addStretch(1)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        #self.initialize()
        self.createConnexions()

    def initialize(self):
        """
        on lit les tables admin.lsu, commun.classes
        """
        self.fromSoft = True
        self.modified = False

        query_admin = utils_db.query(admin.db_admin)

        self.classesList.clear()
        self.cptNumData = {'appreciation': '', 'classes': []}
        classesText = ''
        self.cptNumData['appreciation'] = QtWidgets.QApplication.translate(
            'main', 'Appreciation missing.')
        commandLine_admin = utils_db.q_selectAllFromWhereText.format(
            'lsu', 'lsuWhat', 'cpt-num')
        query_admin = utils_db.queryExecute(
            commandLine_admin, query=query_admin)
        while query_admin.next():
            self.cptNumData['appreciation'] = query_admin.value(1)
            classesText = query_admin.value(2)
        self.cptNumData['classes'] = classesText.split('|')
        if len(classesText) < 1:
            self.doModified()

        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, db=self.main.db_commun)
        while query_commun.next():
            id_classe = int(query_commun.value(0))
            classeName = query_commun.value(1)
            classeType = int(query_commun.value(2))
            if (classeType == 0) and (len(classesText) < 1):
                self.cptNumData['classes'].append(classeName)
            item = QtWidgets.QListWidgetItem(classeName)
            item.setData(
                QtCore.Qt.UserRole, classeName)
            if classeName in self.cptNumData['classes']:
                item.setCheckState(QtCore.Qt.Checked)
            else:
                item.setCheckState(QtCore.Qt.Unchecked)
            self.classesList.addItem(item)

        self.appreciationEdit.setText(self.cptNumData['appreciation'])

        self.fromSoft = False

    def createConnexions(self):
        self.classesList.itemClicked.connect(self.doChanged)
        self.appreciationEdit.textChanged.connect(self.doChanged)

    def doChanged(self, value=-1):
        """
        un champ a été modifié.
        On met à jour les données et l'affichage.
        """
        if self.fromSoft:
            return

        if self.sender() == self.classesList:
            self.cptNumData['classes'] = []
            for index in range(self.classesList.count()):
                item = self.classesList.item(index)
                if item.checkState() == QtCore.Qt.Checked:
                    classeName = item.data(QtCore.Qt.UserRole)
                    self.cptNumData['classes'].append(classeName)
            self.doModified()
        elif self.sender() == self.appreciationEdit:
            try:
                newText = self.sender().text()
            except:
                newText = self.sender().toPlainText()
            self.cptNumData['appreciation'] = newText
            self.doModified()

    def doModified(self):
        self.modified = True
        self.parent.updateButtons()

    def checkModifications(self):
        """
        modification des tables concernées par le dialog.
        """
        if not(self.modified):
            return
        commandLines_admin = []
        lines = []
        commandLineInsert = utils_db.insertInto('lsu', 6)

        # ligne cpt-num :
        commandLines_admin.append(
            utils_db.qdf_whereText.format('lsu', 'lsuWhat', 'cpt-num'))
        classesText = utils_functions.u('|').join(self.cptNumData['classes'])
        lines.append((
            'cpt-num', self.cptNumData['appreciation'], classesText, '', '', ''))

        # écriture et enregistrement :
        if len(commandLines_admin) > 0:
            utils_functions.doWaitCursor()
            query_admin, transactionOK_admin = utils_db.queryTransactionDB(
                admin.db_admin)
            try:
                query_admin = utils_db.queryExecute(
                    commandLines_admin, query=query_admin)
                if len(lines) > 0:
                    query_admin = utils_db.queryExecute(
                        {commandLineInsert: lines}, query=query_admin)
                # on exporte en csv :
                for table in ('lsu', ):
                    fileName = utils_functions.u(
                        '{0}/admin_tables/{1}.csv').format(admin.csvDir, table)
                    utils_export.exportTable2Csv(
                        self.main, admin.db_admin, table, fileName, msgFin=False)
            finally:
                utils_db.endTransaction(
                    query_admin, admin.db_admin, transactionOK_admin)
                utils_filesdirs.removeAndCopy(
                    admin.dbFileTemp_admin, admin.dbFile_admin)
            utils_functions.restoreCursor()

        self.modified = False

































class FileCreationDlg(QtWidgets.QDialog):
    """
    Onglet de création du fichier xml.
    """
    def __init__(self, parent=None):
        super(FileCreationDlg, self).__init__(parent)
        self.parent = parent
        self.main = parent.main
        # des modifications ont été faites :
        self.modified = False

        # Périodes :
        text = QtWidgets.QApplication.translate(
            'main', 'Periods')
        periodsGroup = QtWidgets.QGroupBox(text)
        helpMessage = QtWidgets.QApplication.translate(
            'main', 
            'You must select at least one period '
            'or end-of-cycle balances.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpMessage))
        grid = QtWidgets.QGridLayout()
        grid.addWidget(helpLabel, 1, 0, 1, 2)
        i = 2
        if utils.selectedPeriod == 0:
            periods = range(1, utils.NB_PERIODES)
        else:
            periods = range(1, utils.selectedPeriod + 1)
        self.checkBoxs = {}
        for p in periods:
            checkBox = QtWidgets.QCheckBox(utils.PERIODES[p])
            checkBox.stateChanged.connect(self.doChanged)
            self.checkBoxs[p] = checkBox
            grid.addWidget(checkBox, i, 0)
            i += 1
        text = QtWidgets.QApplication.translate(
            'main', 'End-of-cycle balances')
        checkBox = QtWidgets.QCheckBox(text)
        checkBox.stateChanged.connect(self.doChanged)
        self.checkBoxs[999] = checkBox
        grid.addWidget(checkBox, 2, 1)
        periodsGroup.setLayout(grid)

        # Compétences numériques :
        text = QtWidgets.QApplication.translate(
            'main', 'Digital skills')
        cptNumGroup = QtWidgets.QGroupBox(text)
        grid = QtWidgets.QGridLayout()
        text = QtWidgets.QApplication.translate(
            'main', 
            'Insert the levels of mastery of digital skills')
        self.checkBoxCptNum = QtWidgets.QCheckBox(text)
        grid.addWidget(self.checkBoxCptNum, 1, 0)
        cptNumGroup.setLayout(grid)

        # Élèves :
        text = QtWidgets.QApplication.translate(
            'main', 'Students')
        studentsGroup = QtWidgets.QGroupBox(text)
        helpMessage = QtWidgets.QApplication.translate(
            'main', 
            'To select all, you can leave the empty right list.')
        helpLabel = QtWidgets.QLabel(
            utils_functions.u(
                '<p align="center"><b>{0}</b></p>').format(helpMessage))
        self.selectionWidget = utils_2lists.ChooseStudentsWidget(
            parent = self.main, withComboBox = True)
        grid = QtWidgets.QGridLayout()
        grid.addWidget(helpLabel, 1, 0)
        grid.addWidget(self.selectionWidget, 2, 0)
        studentsGroup.setLayout(grid)

        # création du fichier :
        self.export2LSU_Button = QtWidgets.QToolButton()
        self.checkFile_Button = QtWidgets.QToolButton()
        # mise en forme des boutons :
        buttons = {
            self.export2LSU_Button: (
                'en', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Create File'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'To create the xml file to export LSU'), 
                self.export2LSU), 
            self.checkFile_Button: (
                'xml-check', 
                QtWidgets.QApplication.translate(
                    'main', 
                    'Check File'), 
                QtWidgets.QApplication.translate(
                    'main', 
                    'To check if an export xml file to LSU is valid'), 
                self.checkFile), 
            }
        ICON_SIZE = utils.STYLE['PM_MessageBoxIconSize'] * 2
        if self.main.height() < ICON_SIZE * 2:
            ICON_SIZE = ICON_SIZE // 2
        for button in buttons:
            button.setIcon(
                utils.doIcon(buttons[button][0]))
            button.setIconSize(
                QtCore.QSize(ICON_SIZE, ICON_SIZE))
            button.setText(buttons[button][1])
            button.setStatusTip(buttons[button][2])
            button.setToolButtonStyle(
                QtCore.Qt.ToolButtonTextUnderIcon)
            button.clicked.connect(buttons[button][3])
        # première série d'actions :
        hLayout0 = QtWidgets.QHBoxLayout()
        hLayout0.addWidget(self.export2LSU_Button)
        hLayout0.addWidget(self.checkFile_Button)
        hLayout0.addStretch(1)

        # les messages :
        messagesTitle = QtWidgets.QApplication.translate(
            'main', 'MESSAGES WINDOW')
        messagesTitle = utils_functions.u(
            '<p align="center"><b>{0}</b></p>').format(messagesTitle)
        messagesLabel = QtWidgets.QLabel(messagesTitle)
        messagesEdit = QtWidgets.QTextEdit()
        messagesEdit.setReadOnly(True)
        self.main.editLog2 = messagesEdit
        messagesLayout = QtWidgets.QVBoxLayout()
        messagesLayout.addWidget(messagesLabel)
        messagesLayout.addWidget(messagesEdit)


        # on agence tout ça :
        vBoxLeft = QtWidgets.QVBoxLayout()
        vBoxLeft.addWidget(periodsGroup)
        vBoxLeft.addWidget(cptNumGroup)
        vBoxLeft.addWidget(studentsGroup)
        vBoxRight = QtWidgets.QVBoxLayout()
        vBoxRight.addLayout(hLayout0)
        vBoxRight.addLayout(messagesLayout)
        #vBoxRight.addStretch(1)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addLayout(vBoxLeft)
        mainLayout.addLayout(vBoxRight)

        # mise en place finale :
        self.setLayout(mainLayout)
        self.setMinimumWidth(600)
        self.doChanged(0)

    def doChanged(self, value):
        """
        pour n'activer le bouton d'export
        que si une période est sélectionnée
        """
        result = False
        for p in self.checkBoxs:
            result = (result or self.checkBoxs[p].isChecked())
        self.export2LSU_Button.setEnabled(result)

    def checkFile(self):
        """
        vérification d'un fichier xml
        d'export LSU
        """
        if not(XML_PATTERNS):
            return
        utils_functions.afficheMessage(
            self.main, 
            messages="<h2>Vérification du fichier</h2>", 
            editLog=True, 
            p=True)

        xmlTitle = QtWidgets.QApplication.translate('main', 'XML File')
        xmlExt = QtWidgets.QApplication.translate('main', 'xml files (*.xml)')
        proposedDir = utils_functions.u(
            '{0}/fichiers/xml').format(admin.adminDir)
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self.main, xmlTitle, proposedDir, xmlExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return

        xsdFile = utils_functions.u('{0}/files/lsu-import-complet-strict.xsd').format(self.main.beginDir)
        url = QtCore.QUrl().fromLocalFile(xsdFile)
        schema = QtXmlPatterns.QXmlSchema()
        messageHandler = MessageHandler()
        schema.setMessageHandler(messageHandler)
        schema.load(url)

        if schema.isValid():
            #utils_functions.afficheMessage(self.main, 'schema.isValid', editLog=True)
            f = QtCore.QFile(fileName)
            if f.open(QtCore.QIODevice.ReadOnly):
                utils_functions.doWaitCursor()
                try:
                    validator = QtXmlPatterns.QXmlSchemaValidator(schema)
                    if validator.validate(QtCore.QUrl().fromLocalFile(fileName)):
                        msgValid = QtWidgets.QApplication.translate(
                            'main', 'File is valid')
                        msg = utils_functions.u(
                            '<h4>{0}</h4>').format(msgValid)
                        utils_functions.afficheMessage(
                            self.main, msg, editLog=True)
                    else:
                        msgInvalid = QtWidgets.QApplication.translate(
                            'main', 'File is invalid')
                        msgLineNumber = QtWidgets.QApplication.translate(
                            'main', 'line')
                        msg = utils_functions.u(
                            '<h4>{0}</h4><br/><b>{1} {2}:</b>{3}').format(
                                msgInvalid, 
                                msgLineNumber, 
                                messageHandler.line(), 
                                messageHandler.statusMessage())
                        utils_functions.afficheMessage(
                            self.main, msg, editLog=True)
                finally:
                    utils_functions.restoreCursor()

    def export2LSU(self):
        """
        création du fichier xml
        """

        def verifDic(dic, *args):
            """
            une fonction qui crée les clés éventuellement 
            manquantes dans un dictionnaire
            """
            actual = dic
            for arg in args:
                if not(arg in actual):
                    actual[arg] = {}
                actual = actual[arg]

        def explodeName(nomComplet):
            """
            une fonction qui sépare civilité, nom et prénom.
            Utilisé pour les responsables
            """
            if nomComplet[:2] == 'M.':
                civilite = 'M'
                nomPrenom = nomComplet[3:]
            else:
                civilite = 'MME'
                nomPrenom = nomComplet[4:]
            nomPrenom = nomPrenom.split(' ')
            nom = []
            prenom = []
            for e in nomPrenom:
                if (e[-1] == e[-1].upper()):
                    nom.append(e)
                else:
                    prenom.append(e)
            nom = ' '.join(nom)
            prenom = ' '.join(prenom)
            return (civilite, nom, prenom)

        def replaceBadChars(text):
            """
            une fonction qui arrange les textes type appréciations.
            Remplace les caractères qui pourraient poser problème
            dans le fichier xml
            """
            newText = text.replace('\n', ' ').replace('\r', '').replace('\t', ' ')
            newText = newText.replace('"', '').replace('<', '').replace('>', '')
            newText = newText.replace('\x00', '')
            return newText

        def average(liste=[], precision=0):
            """
            retourne la moyenne d'une liste de nombres
            """
            result = False
            if len(liste) > 0:
                result = sum(liste) * 1.0 / len(liste)
                result = round(result, precision)
            return result

        def lsuDate2Integer(value):
            """
            transforme une date au format 'yyyy-MM-dd'
            en entier yyyyMMdd.
            Pour comparaisons
            """
            try:
                result = int(value.replace('-', ''))
            except:
                result = -1
            return result

        def integer2LsuDate(value):
            """
            transforme une date entière à 8 chiffres
            en date texte au format 'yyyy-MM-dd'
            """
            result = '{0}'.format(value)
            result = '{0}-{1}-{2}'.format(result[:4], result[4:6], result[6:])
            return result

        def matieresProfs2List(matieresProfs, matiere2id):
            """
            transforme la liste des couples matière+ prof
            formatée en MAT1#xx|MAT2#yy|...
            en une liste [(ENS_xx, DI_aa), (ENS_yy, DI_bb), ...]
            Exemple :
                FRA#37|HG#28
                [('ENS_37', 'DI_8'), ('ENS_28', 'DI_9')]
            """
            result = []
            for matiereProf in matieresProfs.split('|'):
                if len(matiereProf) > 0:
                    l = matiereProf.split('#')
                    try:
                        id_matiere = matiere2id[l[0]]
                        id_matiere = 'DI_{0}'.format(id_matiere)
                        id_prof = int(l[1])
                        id_prof = 'ENS_{0}'.format(id_prof)
                        result.append((id_prof, id_matiere))
                    except:
                        pass
            return result

        xmlTitle = QtWidgets.QApplication.translate('main', 'XML File')
        today = QtCore.QDate.currentDate().toString('yyyy-MM-dd')
        proposedName = utils_functions.u(
            '{0}/fichiers/xml/import-lsun-{1}.xml').format(admin.adminDir, today)
        xmlExt = QtWidgets.QApplication.translate('main', 'xml files (*.xml)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, xmlTitle, proposedName, xmlExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return


        utils_functions.afficheMessage(
            self.main, 
            messages="<h2>Fabrication du fichier</h2>", 
            editLog=True, 
            p=True)
        utils_functions.doWaitCursor()
        OK = False
        data = {}
        temp = {
            'classe2responsableEtab': {}, 
            'classe2evaluationType': {}, 
            'eleve2idbe': {}, 
            'eleve2dates': {}, 
            'eleve2classe': {}, 
            'classe2eleves': {}, 
            'classe2classe': {}, 
            'classe2cycle': {}, 
            'id2matiere': {}, 
            'matiere2id': {}, 
            'elementProgramme2id': {}, 
            'perso2matiere': {}, 
            'prof2matiere': {}, 
            'matiere2EnseignementComplement': {}, 
            'enseignementsComplements': {}, 
            }
        noComment = QtWidgets.QApplication.translate(
            'main', 'Appreciation missing.')
        noElemsProg = QtWidgets.QApplication.translate(
            'main', 'Missing Program Elements.')
        participeDevoirsFaits = QtWidgets.QApplication.translate(
            'main', 'Participates in the Homework done scheme.')
        query_admin = utils_db.query(admin.db_admin)
        query_recupEvals = utils_db.query(admin.db_recupEvals)
        query_commun = utils_db.query(self.main.db_commun)

        # pour les positionnements, on a besoin d'interroger 
        # les bases resultats-x.sqlite.
        # Du coup on récupère les périodes de suite :
        query_p = {'PERIODS': [], 'DB_NAMES': {}}
        data['periodes'] = {}
        for p in self.checkBoxs:
            if self.checkBoxs[p].isChecked():
                query_p['PERIODS'].append(p)
                if p < 999:
                    # O : id, indice, millesime, nb-periodes
                    data['periodes'][p] = {
                        'id': 'P_{0}'.format(p), 
                        'indice': p, 
                        'millesime': self.main.annee_scolaire[0] - 1, 
                        'nb-periodes': utils.NB_PERIODES - 1}
                # on copie les bases resultats dans temp :
                dbFile = '{0}/protected/resultats-{1}.sqlite'.format(admin.dirLocalPrive, p)
                if not(QtCore.QFileInfo(dbFile).exists()):
                    dbFile = '{0}/protected/resultats.sqlite'.format(admin.dirLocalPrive)
                dbFileTemp = '{0}/resultats-{1}.sqlite'.format(self.main.tempPath, p)
                utils_filesdirs.removeAndCopy(dbFile, dbFileTemp)
                # pour ne pas avoir d'erreur du type "is still in use",
                # on est obligé de définir chaque base (db_x) de manière statique
                if p == 1:
                    (db_1, dbName) = utils_db.createConnection(
                        self.main, dbFileTemp)
                    query_p[1] = utils_db.query(db_1)
                    query_p['DB_NAMES'][1] = dbName
                elif p == 2:
                    (db_2, dbName) = utils_db.createConnection(
                        self.main, dbFileTemp)
                    query_p[2] = utils_db.query(db_2)
                    query_p['DB_NAMES'][2] = dbName
                elif p == 3:
                    (db_3, dbName) = utils_db.createConnection(
                        self.main, dbFileTemp)
                    query_p[3] = utils_db.query(db_3)
                    query_p['DB_NAMES'][3] = dbName
                elif p == 999:
                    (db_999, dbName) = utils_db.createConnection(
                        self.main, dbFileTemp)
                    query_p[999] = utils_db.query(db_999)
                    query_p['DB_NAMES'][999] = dbName
        periodsForIn = utils_functions.array2string(query_p['PERIODS'])

        # autres trucs utiles :
        value2pos = {'D': 1, 'C': 2, 'B': 3, 'A': 4}

        try:

            # entete :
            data['entete'] = {
                'ORDER': ('editeur', 'application', 'appliVersion', 'etablissement'), 
                'editeur': 'Pascal PETER', 
                'application': utils.PROGNAME, 
                'appliVersion': utils.PROGVERSION, 
                'etablissement': utils_db.readInConfigTable(admin.db_admin, 'ETAB_CODE')[1], 
                }

            # responsablesEtab :
            data['responsablesEtab'] = {}
            for index in range(self.parent.tabs[1].responsablesEtabBaseList.count()):
                item = self.parent.tabs[1].responsablesEtabBaseList.item(index)
                itemData = item.data(QtCore.Qt.UserRole)
                id_classe = itemData[0]
                classe = itemData[1]
                id_prof = itemData[2]
                nom = itemData[3]
                prenom = itemData[4]
                if id_prof >= -1:
                    # O : id, libelle
                    data['responsablesEtab'][id_prof] = {
                        'id': 'RESP_{0}'.format(id_prof), 
                        'libelle': utils_functions.u('{0} {1}').format(prenom, nom)[:100]}
                    temp['classe2responsableEtab'][classe] = id_prof

            # evaluationType :
            for index in range(self.parent.tabs[1].evaluationTypeBaseList.count()):
                item = self.parent.tabs[1].evaluationTypeBaseList.item(index)
                itemData = item.data(QtCore.Qt.UserRole)
                id_classe = itemData[0]
                classe = itemData[1]
                id_type = itemData[2]
                temp['classe2evaluationType'][classe] = id_type

            # classe2cycle :
            for index in range(self.parent.tabs[1].classType2CycleBaseList.count()):
                temp['temp'] = {}
                item = self.parent.tabs[1].classType2CycleBaseList.item(index)
                itemData = item.data(QtCore.Qt.UserRole)
                id_classType = itemData[0]
                cycle = itemData[2]
                temp['temp'][id_classType] = cycle
            commandLine_commun = utils_db.q_selectAllFrom.format('classes')
            query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
            while query_commun.next():
                id_classe = int(query_commun.value(0))
                classe = query_commun.value(1)
                id_classType = int(query_commun.value(2))
                temp['classe2cycle'][classe] = temp['temp'].get(id_classType, 3)

            # on récupère les noms officiels des classes
            # depuis la table admin.replacements :
            replaceWhere = 'xmlClasses'
            commandLine_admin = utils_db.q_selectAllFromWhereText.format(
                'replacements', 'replaceWhere', replaceWhere)
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                replaceWhat = query_admin.value(1)
                replaceWith = query_admin.value(2)
                temp['classe2classe'][replaceWith] = replaceWhat[:20]

            # eleves :
            data['eleves'] = {}
            # on récupère les ids des élèves sélectionnés
            # ou tous les élèves si aucun n'est sélectionné
            # en tenant compte de la liste déroulante des classes :
            idsEleves = []
            for i in range(self.selectionWidget.selectionList.count()):
                item = self.selectionWidget.selectionList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                idsEleves.append(id_eleve)
            if len(idsEleves) < 1:
                for i in range(self.selectionWidget.baseList.count()):
                    item = self.selectionWidget.baseList.item(i)
                    id_eleve = item.data(QtCore.Qt.UserRole)
                    idsEleves.append(id_eleve)
            # on récupère les "id-be" des élèves (POURRA ÊTRE SUPPRIMÉ APRÈS) :
            commandLine_admin = 'SELECT DISTINCT id_eleve, eleve_id FROM adresses'
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                eleve_id = int(query_admin.value(1))
                temp['eleve2idbe'][id_eleve] = eleve_id
            # on récupère les données des élèves depuis
            # la table admin.eleves et on remplit data :
            commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                if id_eleve in idsEleves:
                    num = query_admin.value(1)
                    nom = query_admin.value(2)
                    prenom = query_admin.value(3)
                    classe = query_admin.value(4)
                    eleve_id = int(query_admin.value(9))
                    if eleve_id < 0:
                        eleve_id = temp['eleve2idbe'].get(id_eleve, id_eleve)
                    eleveDateEntree = int(query_admin.value(10))
                    eleveDateSortie = int(query_admin.value(11))
                    # O : id, id-be, code-division
                    # N : nom, prenom
                    data['eleves'][id_eleve] = {
                        'id': 'EL_{0}'.format(id_eleve), 
                        'id-be': eleve_id, 
                        'nom': nom[:100], 
                        'prenom': prenom[:100], 
                        'code-division': temp['classe2classe'].get(classe, classe[:20])}
                    temp['eleve2classe'][id_eleve] = classe
                    if classe in temp['classe2eleves']:
                        temp['classe2eleves'][classe].append(id_eleve)
                    else:
                        temp['classe2eleves'][classe] = [id_eleve, ]
                    temp['eleve2dates'][id_eleve] = (eleveDateEntree, eleveDateSortie)
            studentsForIn = utils_functions.array2string(data['eleves'])

            # disciplines :
            data['disciplines'] = {}
            for index in range(self.parent.tabs[0].baseList.count()):
                item = self.parent.tabs[0].baseList.item(index)
                itemData = item.data(QtCore.Qt.UserRole)
                id_matiere = itemData[0]
                matiere = itemData[1]
                code = itemData[5]
                modalite = itemData[6]
                libelle = itemData[3]
                if len(code) > 0:
                    # O : id, code, modalite-election, libelle
                    # REMARQUE : comment récupérer modalite-election ???
                    data['disciplines'][id_matiere] = {
                        'id': 'DI_{0}'.format(id_matiere), 
                        'code': code, 
                        'modalite-election': modalite, 
                        'libelle': libelle[:40]}
                    temp['id2matiere'][id_matiere] = matiere
                    temp['matiere2id'][matiere] = id_matiere
            matieresForIn = utils_functions.array2string(temp['matiere2id'], text=True)

            # enseignants :
            data['enseignants'] = {}
            for index in range(self.parent.tabs[1].profsBaseList.count()):
                item = self.parent.tabs[1].profsBaseList.item(index)
                itemData = item.data(QtCore.Qt.UserRole)
                id_prof = itemData[0]
                nom = itemData[2]
                prenom = itemData[3]
                matiere = itemData[4]
                profStsId = itemData[5]
                profStsType = itemData[6]
                if len(profStsId) > 0:
                    # O : id, type, id-sts
                    # N : nom, prenom, [civilite]
                    # REMARQUE : type et id-sts sont récupérés dans sts_emp_RNE_aaaa.xml
                    data['enseignants'][id_prof] = {
                        'id': 'ENS_{0}'.format(id_prof), 
                        'type': profStsType, 
                        'id-sts': profStsId, 
                        'nom': nom[:100], 
                        'prenom': prenom[:100]}
                    temp['prof2matiere'][id_prof] = matiere
            # ICI on ajoute le prof ULIS à la main (à améliorer)
            id_prof = 10600
            nom = 'LARONDE'
            prenom = 'Frédéric'
            matiere = 'PP'
            profStsId = 10600
            profStsType = 'autre'
            data['enseignants'][id_prof] = {
                'id': 'ENS_{0}'.format(id_prof), 
                'type': profStsType, 
                'id-sts': profStsId, 
                'nom': nom[:100], 
                'prenom': prenom[:100]}


            # elementsProgramme :
            data['elementsProgramme'] = {}
            id_elem = 0
            # au cas où le prof les ait oublié :
            data['elementsProgramme'][id_elem] = {
                'id': 'EP_{0}'.format(id_elem), 
                'libelle': noElemsProg}
            temp['elementProgramme2id'][noElemsProg] = id_elem
            # récupération des bilans persos (profils) :
            commandLine = 'SELECT DISTINCT Label FROM persos'
            for periode in query_p['PERIODS']:
                if periode < 999:
                    query_p[periode] = utils_db.queryExecute(commandLine, query=query_p[periode])
                    while query_p[periode].next():
                        label = query_p[periode].value(0)
                        if not(label in temp['elementProgramme2id']):
                            id_elem += 1
                            libelle = replaceBadChars(label)
                            # O : id, libelle
                            data['elementsProgramme'][id_elem] = {
                                'id': 'EP_{0}'.format(id_elem), 
                                'libelle': libelle[:300]}
                            temp['elementProgramme2id'][label] = id_elem

            # ICI cptNum
            # cptNum (compétences numériques) :
            data['cptNum'] = {'COMMUN': {}, 'STUDENTS': {}}
            if self.checkBoxCptNum.isChecked():
                # on récupère l'appréciation "commun" dans admin.lsu :
                id_elem = 0
                cptNumClasses = ''
                cptNumAppreciation = QtWidgets.QApplication.translate(
                    'main', 'Appreciation missing.')
                commandLine_admin = utils_db.q_selectAllFromWhereText.format(
                    'lsu', 'lsuWhat', 'cpt-num')
                query_admin = utils_db.queryExecute(
                    commandLine_admin, query=query_admin)
                while query_admin.next():
                    cptNumAppreciation = query_admin.value(1)
                    cptNumClasses = query_admin.value(2)
                cptNumAppreciation = replaceBadChars(cptNumAppreciation)
                cptNumClasses = cptNumClasses.split('|')
                for classe in cptNumClasses:
                    if len(classe) > 0:
                        id_elem += 1
                        # O : id, code-structure, type-structure
                        data['cptNum']['COMMUN'][classe] = {
                            'id': 'CNC_{0}'.format(id_elem), 
                            'code-structure': temp['classe2classe'].get(classe, classe[:20]), 
                            'type-structure': 'D', 
                            'appreciation': cptNumAppreciation[:600]}
                # on récupère les évaluations individuelles :
                commandLine = (
                    'SELECT * FROM lsu WHERE lsuWhat="cpt-num"')
                query_recupEvals = utils_db.queryExecute(
                    commandLine, query=query_recupEvals)
                while query_recupEvals.next():
                    id_prof = int(query_recupEvals.value(0))
                    id_eleve = int(query_recupEvals.value(2))
                    if id_eleve in data['eleves']:
                        what = query_recupEvals.value(3)
                        value = query_recupEvals.value(4)
                        classe = temp['eleve2classe'][id_eleve]
                        cycle = temp['classe2cycle'].get(classe, 3)
                        if (classe in cptNumClasses) and (cycle == 3):
                            if not(id_eleve in data['cptNum']['STUDENTS']):
                                communRefs = data['cptNum']['COMMUN'][classe]['id']
                                data['cptNum']['STUDENTS'][id_eleve] = {'comp-num-commun-refs': communRefs}
                            if what == 'appreciation':
                                value = value[:600]
                            else:
                                # pour un niveau, on vérifie que 
                                # la valeur est entre 1 et 3 :
                                value = int(value)
                                if value < 1:
                                    value = 1
                                elif value > 3:
                                    value = 3
                                value = '{0}'.format(value)
                            data['cptNum']['STUDENTS'][id_eleve][what] = value
                """
                # partie à désactiver aucasoù ou à supprimer.
                # pour mettre des évaluations et une appréciation à tous
                for id_eleve in data['eleves']:
                    classe = temp['eleve2classe'][id_eleve]
                    cycle = temp['classe2cycle'].get(classe, 3)
                    if (classe in cptNumClasses) and (cycle == 3):
                        if not(id_eleve in data['cptNum']['STUDENTS']):
                            communRefs = data['cptNum']['COMMUN'][classe]['id']
                            data['cptNum']['STUDENTS'][id_eleve] = {'comp-num-commun-refs': communRefs}
                        if not('appreciation' in data['cptNum']['STUDENTS'][id_eleve]):
                            data['cptNum']['STUDENTS'][id_eleve]['appreciation'] = cptNumAppreciation[:600]
                        for what in utils.LSU['CPT_NUM']:
                            if len(what) > 6:
                                if not(what in data['cptNum']['STUDENTS'][id_eleve]):
                                    data['cptNum']['STUDENTS'][id_eleve][what] = 2
                """
                #print(data['cptNum'])

            # parcoursCommuns :
            data['parcoursCommuns'] = {}
            parcoursCommuns = {'GROUPS': {}, 'STUDENTS': {}, 'PARCOURS': {}}
            # on commence par les appréciations groupes :
            commandLine = (
                'SELECT * FROM appreciations '
                'WHERE Matiere="PAR_LSU" '
                'AND id_eleve<0')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_eleve = int(query_recupEvals.value(1))
                id_groupe = - id_eleve - 1
                appreciation = replaceBadChars(query_recupEvals.value(2))
                matiere = query_recupEvals.value(3)
                periode = int(query_recupEvals.value(4))
                if periode in query_p['PERIODS']:
                    id_groupe = - id_eleve - 1
                    verifDic(parcoursCommuns['GROUPS'], (id_prof, id_groupe))
                    parcoursCommuns[
                        'GROUPS'][(id_prof, id_groupe)][periode] = appreciation
            # on récupère les parcours (surtout leur type) :
            commandLine = (
                'SELECT * FROM lsu WHERE lsuWhat="parcours"')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_groupe = int(query_recupEvals.value(2))
                groupeName = query_recupEvals.value(3)
                parcoursType = query_recupEvals.value(5)
                parcoursCommuns[
                    'PARCOURS'][(id_prof, id_groupe)] = (groupeName, parcoursType)
            # on peut remplir la partie parcoursCommuns :
            for (id_prof, id_groupe) in parcoursCommuns['GROUPS']:
                if (id_prof, id_groupe) in parcoursCommuns['PARCOURS']:
                    (groupeName, parcoursType) = parcoursCommuns[
                        'PARCOURS'][(id_prof, id_groupe)]
                    for periode in parcoursCommuns['GROUPS'][(id_prof, id_groupe)]:
                        appreciation = parcoursCommuns[
                            'GROUPS'][(id_prof, id_groupe)][periode]
                        if not((groupeName, periode) in data['parcoursCommuns']):
                            data['parcoursCommuns'][(groupeName, periode)] = {
                                'attributs': {
                                    'periode-ref': 'P_{0}'.format(periode), 
                                    'code-division': temp['classe2classe'].get(
                                        groupeName, groupeName[:20]), 
                                    },
                                'parcours': {}
                                }
                        data['parcoursCommuns'][(groupeName, periode)][
                            'parcours'][parcoursType] = appreciation[:600]
            # on récupère les appréciations élèves :
            commandLine = (
                'SELECT appreciations.*, eleve_groupe.id_groupe, eleve_groupe.groupeName '
                'FROM appreciations '
                'JOIN eleve_groupe '
                'ON eleve_groupe.id_prof=appreciations.id_prof '
                'AND eleve_groupe.id_eleve=appreciations.id_eleve '
                'AND eleve_groupe.Periode=appreciations.Periode '
                'AND eleve_groupe.Matiere=appreciations.Matiere '
                'WHERE appreciations.Matiere="PAR_LSU" '
                'AND appreciations.id_eleve>-1')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_eleve = int(query_recupEvals.value(1))
                appreciation = replaceBadChars(query_recupEvals.value(2))
                matiere = query_recupEvals.value(3)
                periode = int(query_recupEvals.value(4))
                id_groupe = int(query_recupEvals.value(5))
                groupeName = query_recupEvals.value(6)
                if (id_prof, id_groupe) in parcoursCommuns['PARCOURS']:
                    (groupeName, parcoursType) = parcoursCommuns[
                        'PARCOURS'][(id_prof, id_groupe)]
                    try:
                        if periode in parcoursCommuns['GROUPS'][(id_prof, id_groupe)]:
                            verifDic(parcoursCommuns['STUDENTS'], (periode, id_eleve))
                            parcoursCommuns['STUDENTS'][(periode, id_eleve)][parcoursType] = appreciation
                    except:
                        pass

            # modalites-accompagnement :
            accompagnements = {}
            devoirsFaits = {}
            commandLine = (
                'SELECT * FROM lsu '
                'WHERE lsuWhat="accompagnement"')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(2))
                lsuWhat = query_recupEvals.value(3)
                if lsuWhat == 'DF':
                    """
                    description = replaceBadChars(query_recupEvals.value(4))
                    if len(description) < 1:
                        description = participeDevoirsFaits
                    devoirsFaits[id_eleve] = description
                    """
                    devoirsFaits[id_eleve] = participeDevoirsFaits
                else:
                    if lsuWhat == 'PPRE':
                        description = replaceBadChars(query_recupEvals.value(4))
                    else:
                        description = ''
                    if not(id_eleve in accompagnements):
                        accompagnements[id_eleve] = {}
                    accompagnements[id_eleve][lsuWhat] = description

            # vies-scolaires-communs :
            data['vsCommuns'] = {}
            for periode in data['periodes']:
                for groupeName in temp['classe2eleves']:
                    data['vsCommuns'][(groupeName, periode)] = {
                        'code-division': temp['classe2classe'].get(groupeName, groupeName[:20]), 
                        'periode-ref': 'P_{0}'.format(periode), 
                        'commentaire': noComment}
            commandLine = (
                'SELECT DISTINCT eleve_groupe.Matiere, '
                'eleve_groupe.groupeName, '
                'eleve_groupe.Periode, '
                'appreciations.appreciation '
                'FROM eleve_groupe '
                'JOIN appreciations ON '
                '(appreciations.id_prof=eleve_groupe.id_prof '
                'AND appreciations.Periode=eleve_groupe.Periode '
                'AND appreciations.Matiere=eleve_groupe.Matiere '
                'AND appreciations.id_eleve=-eleve_groupe.id_groupe-1) '
                'WHERE eleve_groupe.Matiere IN ("VS")')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                matiere = query_recupEvals.value(0)
                groupeName = query_recupEvals.value(1)
                periode = int(query_recupEvals.value(2))
                appreciation = replaceBadChars(query_recupEvals.value(3))
                if periode in data['periodes']:
                    if groupeName in temp['classe2eleves']:
                        data['vsCommuns'][(groupeName, periode)] = {
                            'code-division': temp['classe2classe'].get(groupeName, groupeName[:20]), 
                            'periode-ref': 'P_{0}'.format(periode), 
                            'commentaire': appreciation[:600]}

            # EPIS :
            data['EPIS'] = {
                'episRef': {}, 
                'episGroupes': {}, 
                'eleve2epis': {}, 
                'prof2epis': {}, }
            if self.parent.tabs[3].episMatieresOk:
                # récupération des epis de référence :
                commandLine_commun = utils_db.q_selectAllFromWhereText.format(
                    'lsu', 'lsuWhat', 'epis')
                query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
                while query_commun.next():
                    id_epi = int(query_commun.value(1)) # id
                    epiTheme = query_commun.value(2) # thematique
                    epiName = replaceBadChars(query_commun.value(3)) # intitule
                    epiMatieres = query_commun.value(4) # discipline-refs
                    matieres = epiMatieres.split('|')
                    epiMatieres = ''
                    for matiere in matieres:
                        id_matiere = 'DI_{0}'.format(
                            temp['matiere2id'][matiere])
                        epiMatieres = '{0}{1} '.format(epiMatieres, id_matiere)
                    epiMatieres = epiMatieres[:-1]
                    epiLabel = replaceBadChars(query_commun.value(5)) # description
                    data['EPIS']['episRef'][id_epi] = {
                        'id': 'EPI_{0}'.format(id_epi), 
                        'thematique': epiTheme, 
                        'intitule': epiName[:150], 
                        'discipline-refs': epiMatieres, 
                        'description': epiLabel[:600]}
                # récupération des epis-groupes :
                commandLineBase = (
                    'SELECT DISTINCT '
                    'lsu.*, '
                    'eleve_groupe.id_eleve '
                    'FROM lsu '
                    'JOIN eleve_groupe '
                    'ON eleve_groupe.id_prof=lsu.id_prof '
                    'AND eleve_groupe.id_groupe=lsu.lsu1 '
                    'AND eleve_groupe.Matiere=lsu.lsu3 '
                    'WHERE lsu.lsuWhat="epis" '
                    'AND eleve_groupe.id_eleve IN ({0})')
                commandLine = commandLineBase.format(
                    studentsForIn)
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                while query_recupEvals.next():
                    id_prof = int(query_recupEvals.value(0))
                    id_groupe = int(query_recupEvals.value(2))
                    groupeName = query_recupEvals.value(3)
                    matiereCode = query_recupEvals.value(4)
                    epiRef = int(query_recupEvals.value(5))
                    matieresProfs = query_recupEvals.value(6)
                    description = replaceBadChars(query_recupEvals.value(7))
                    id_eleve = int(query_recupEvals.value(10))
                    profsMatieres = matieresProfs2List(matieresProfs, temp['matiere2id'])
                    id_epiGroupe = id_prof * 1000000 + epiRef * 1000 + id_groupe
                    if not(id_epiGroupe in data['EPIS']['episGroupes']):
                        for (id_prof_lsu, id_matiere) in profsMatieres:
                            id_prof = int(id_prof_lsu[4:])
                            if not(id_prof in data['EPIS']['prof2epis']):
                                data['EPIS']['prof2epis'][id_prof] = [id_epiGroupe, ]
                            else:
                                data['EPIS']['prof2epis'][id_prof].append(id_epiGroupe)
                        data['EPIS']['episGroupes'][id_epiGroupe] = {
                            'id': 'EPI_GROUPE_{0}'.format(id_epiGroupe), 
                            'intitule': groupeName[:150], 
                            'epi-ref': 'EPI_{0}'.format(epiRef), 
                            'commentaire': description[:600], 
                            'enseignants-disciplines': profsMatieres
                            }
                    else:
                        if len(data['EPIS']['episGroupes'][id_epiGroupe]['intitule']) < 1:
                            data['EPIS']['episGroupes'][id_epiGroupe]['intitule'] = groupeName[:150]
                        if len(data['EPIS']['episGroupes'][id_epiGroupe]['commentaire']) < 1:
                            data['EPIS']['episGroupes'][id_epiGroupe]['commentaire'] = description[:600]
                    if not(id_eleve in data['EPIS']['eleve2epis']):
                        data['EPIS']['eleve2epis'][id_eleve] = [id_epiGroupe, ]
                    elif not(id_epiGroupe in data['EPIS']['eleve2epis'][id_eleve]):
                        data['EPIS']['eleve2epis'][id_eleve].append(id_epiGroupe)

            # APS :
            data['APS'] = {
                'apsRef': {}, 
                'apsGroupes': {}, 
                'eleve2aps': {}, 
                'prof2aps': {}, }
            if self.parent.tabs[4].apsMatieresOk:
                # récupération des aps de référence :
                commandLine_commun = utils_db.q_selectAllFromWhereText.format(
                    'lsu', 'lsuWhat', 'aps')
                query_commun = utils_db.queryExecute(commandLine_commun, query=query_commun)
                while query_commun.next():
                    id_ap = int(query_commun.value(1)) # id
                    apName = query_commun.value(3) # intitule
                    apMatieres = query_commun.value(4) # discipline-refs
                    matieres = apMatieres.split('|')
                    apMatieres = ''
                    for matiere in matieres:
                        id_matiere = 'DI_{0}'.format(
                            temp['matiere2id'][matiere])
                        apMatieres = '{0}{1} '.format(apMatieres, id_matiere)
                    apMatieres = apMatieres[:-1]
                    apLabel = replaceBadChars(query_commun.value(5)) # description
                    data['APS']['apsRef'][id_ap] = {
                        'id': 'ACC_PERSO_{0}'.format(id_ap), 
                        'intitule': apName[:150], 
                        'discipline-refs': apMatieres, 
                        'description': apLabel[:600]}
                # récupération des aps-groupes :
                commandLineBase = (
                    'SELECT DISTINCT '
                    'lsu.*, '
                    'eleve_groupe.id_eleve '
                    'FROM lsu '
                    'JOIN eleve_groupe '
                    'ON eleve_groupe.id_prof=lsu.id_prof '
                    'AND eleve_groupe.id_groupe=lsu.lsu1 '
                    'AND eleve_groupe.Matiere="AP_LSU" '
                    'WHERE lsu.lsuWhat="aps" '
                    'AND eleve_groupe.id_eleve IN ({0})')
                commandLine = commandLineBase.format(
                    studentsForIn)
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                while query_recupEvals.next():
                    id_prof = int(query_recupEvals.value(0))
                    id_groupe = int(query_recupEvals.value(2))
                    groupeName = query_recupEvals.value(3)
                    apRef = int(query_recupEvals.value(5))
                    description = replaceBadChars(query_recupEvals.value(7))
                    id_eleve = int(query_recupEvals.value(10))
                    # devrait suffire :
                    matiere = temp['prof2matiere'].get(id_prof, '')
                    id_matiere = 'DI_{0}'.format(
                        temp['matiere2id'].get(matiere, 'XXX'))
                    matieres = data['APS']['apsRef'][apRef]['discipline-refs'].split(' ')
                    if id_matiere in matieres:
                        profsMatieres = [(
                            'ENS_{0}'.format(id_prof), 
                            id_matiere)]
                    else:
                        profsMatieres = [(
                            'ENS_{0}'.format(id_prof), 
                            matieres[0])]
                    id_apGroupe = id_prof * 1000000 + apRef * 1000 + id_groupe
                    if not(id_apGroupe in data['APS']['apsGroupes']):
                        if not(id_prof in data['APS']['prof2aps']):
                            data['APS']['prof2aps'][id_prof] = [id_apGroupe, ]
                        else:
                            data['APS']['prof2aps'][id_prof].append(id_apGroupe)
                        data['APS']['apsGroupes'][id_apGroupe] = {
                            'id': 'ACC_PERSO_GROUPE_{0}'.format(id_apGroupe), 
                            'intitule': groupeName[:150], 
                            'acc-perso-ref': 'ACC_PERSO_{0}'.format(apRef), 
                            'commentaire': description[:600], 
                            'enseignants-disciplines': profsMatieres
                            }
                    else:
                        if len(data['APS']['apsGroupes'][id_apGroupe]['intitule']) < 1:
                            data['APS']['apsGroupes'][id_apGroupe]['intitule'] = groupeName[:150]
                        if len(data['APS']['apsGroupes'][id_apGroupe]['commentaire']) < 1:
                            data['APS']['apsGroupes'][id_apGroupe]['commentaire'] = description[:600]
                    if not(id_eleve in data['APS']['eleve2aps']):
                        data['APS']['eleve2aps'][id_eleve] = [id_apGroupe, ]
                    elif not(id_apGroupe in data['APS']['eleve2aps'][id_eleve]):
                        data['APS']['eleve2aps'][id_eleve].append(id_apGroupe)

            # dates :
            data['dates'] = {}
            lsuTable = {}
            commandLine_admin = utils_db.q_selectAllFromWhereText.format(
                'lsu', 'lsuWhat', 'dates')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                p = int(query_admin.value(1))
                lsu2 = query_admin.value(2)
                lsuTable[p] = lsu2
            for p in range(1, utils.NB_PERIODES):
                data['dates'][p] = [today, today]
                if p - 1 in lsuTable:
                    if p == 1:
                        beginDate = lsuTable[p - 1]
                    else:
                        beginDate = QtCore.QDate().fromString(lsuTable[p - 1], 'yyyy-MM-dd')
                        beginDate = beginDate.addDays(1)
                        beginDate = beginDate.toString('yyyy-MM-dd')
                    data['dates'][p][0] = beginDate
                if p in lsuTable:
                    endDate = lsuTable[p]
                    data['dates'][p][1] = endDate

            # responsables :
            data['responsables'] = {}
            # on récupère les données depuis
            # la table admin.adresses :
            commandLine_admin = (
                'SELECT * FROM adresses '
                'WHERE use=1 AND state>1 '
                'ORDER BY id_eleve, state')
            query_admin = utils_db.queryExecute(commandLine_admin, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                if id_eleve in idsEleves:
                    adresseOk = True
                    state = int(query_admin.value(2))
                    nom1 = query_admin.value(3)
                    nom2 = query_admin.value(4)
                    adresse = query_admin.value(5)
                    lines = adresse.split('|')
                    adresse = {}
                    adresse['ligne1'] = ''
                    i = 1
                    if len(lines) > 1:
                        for line in lines[:-1]:
                            if len(line) > 0:
                                adresse['ligne{0}'.format(i)] = line[:50]
                    adresse['code-postal'] = ''
                    adresse['commune'] = ''
                    if len(lines[-1]) > 0:
                        adresse['code-postal'] = lines[-1].split(' ')[0]
                        adresse['commune'] = lines[-1][len(adresse['code-postal']) + 1:]
                        adresse['code-postal'] = adresse['code-postal'][:10]
                        adresse['commune'] = adresse['commune'][:100]
                    for test in ('ligne1', 'code-postal', 'commune'):
                        if len(adresse[test]) < 1:
                            adresseOk = False
                    if adresseOk:
                        verifDic(data['responsables'], id_eleve)
                        if state in (10, 11):
                            (civilite, nom, prenom) = explodeName(nom1)
                            # O : civilite, nom, prenom, [adresse]
                            # N : legal1, legal2, lien-parente
                            data['responsables'][id_eleve]['LEGAL1'] = {
                                'civilite': civilite, 
                                'nom': nom[:100], 
                                'prenom': prenom[:100]}
                            data['responsables'][id_eleve]['LEGAL1']['adresse'] = adresse
                        elif state in (20, 21):
                            (civilite, nom, prenom) = explodeName(nom1)
                            data['responsables'][id_eleve]['LEGAL2'] = {
                                'civilite': civilite, 
                                'nom': nom[:100], 
                                'prenom': prenom[:100]}
                            data['responsables'][id_eleve]['LEGAL2']['adresse'] = adresse
                        elif state in (50, 51):
                            (civilite, nom, prenom) = explodeName(nom1)
                            data['responsables'][id_eleve]['LEGAL1'] = {
                                'civilite': civilite, 
                                'nom': nom[:100], 
                                'prenom': prenom[:100]}
                            data['responsables'][id_eleve]['LEGAL1']['adresse'] = adresse
                            (civilite, nom, prenom) = explodeName(nom2)
                            data['responsables'][id_eleve]['LEGAL2'] = {
                                'civilite': civilite, 
                                'nom': nom[:100], 
                                'prenom': prenom[:100]}
                            data['responsables'][id_eleve]['LEGAL2']['adresse'] = adresse
                        elif state in (100, 101):
                            # legal à pas dans l'export :
                            (civilite, nom, prenom) = explodeName(nom1)
                            """
                            data['responsables'][id_eleve]['LEGAL0'] = {
                                'civilite': civilite, 
                                'nom': nom[:100], 
                                'prenom': prenom[:100]}
                            data['responsables'][id_eleve]['LEGAL0']['adresse'] = adresse
                            """

            # bilans périodiques :
            data['bilans'] = {}
            matieresData = {}
            # récupération des appréciations PP et VS :
            commandLineBase = utils_functions.u(
                'SELECT * FROM appreciations '
                'WHERE id_eleve IN ({0}) '
                'AND Periode IN ({1}) '
                'AND Matiere IN ({2})')
            commandLine = commandLineBase.format(
                studentsForIn, periodsForIn, '"PP", "VS"')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                appreciation = replaceBadChars(query_recupEvals.value(2))
                matiereCode = query_recupEvals.value(3)
                periode = int(query_recupEvals.value(4))
                verifDic(matieresData, (periode, id_eleve))
                if matiereCode == 'PP':
                    matieresData[(periode, id_eleve)]['PP'] = appreciation
                else:
                    matieresData[(periode, id_eleve)]['VS'] = {'appreciation': appreciation, }
            # récupération des autres appréciations :
            commandLine = commandLineBase.format(
                studentsForIn, periodsForIn, matieresForIn)
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_eleve = int(query_recupEvals.value(1))
                appreciation = replaceBadChars(query_recupEvals.value(2))
                matiereCode = query_recupEvals.value(3)
                periode = int(query_recupEvals.value(4))
                verifDic(matieresData, (periode, id_eleve), 'OTHERS', matiereCode, id_prof)
                matieresData[(periode, id_eleve)]['OTHERS'][matiereCode][id_prof]['appreciation'] = appreciation
            # récupération des "éléments de programme"
            for i in range(self.main.limiteBLTPerso):
                for matiereCode in temp['matiere2id']:
                    perso = utils_functions.u(
                        '{0}{1}').format(matiereCode, i + 1)
                    temp['perso2matiere'][perso] = matiereCode
                    perso = utils_functions.formatPersoCpt(matiereCode, i + 1)
                    temp['perso2matiere'][perso] = matiereCode
            commandLineBase = utils_functions.u(
                'SELECT * FROM persos '
                'WHERE id_eleve IN ({0}) '
                'ORDER BY id_eleve, id_prof, name')
            commandLine = commandLineBase.format(studentsForIn)
            for periode in query_p['PERIODS']:
                if periode < 999:
                    query_p[periode] = utils_db.queryExecute(commandLine, query=query_p[periode])
                    while query_p[periode].next():
                        id_prof = int(query_p[periode].value(5))
                        id_eleve = int(query_p[periode].value(0))
                        label = query_p[periode].value(2)
                        name = query_p[periode].value(1)
                        matiereCode = temp['perso2matiere'].get(name, '')
                        if len(matiereCode) > 0:
                            id_elem = temp['elementProgramme2id'].get(label, 0)
                            verifDic(matieresData, (periode, id_eleve), 'OTHERS', matiereCode, id_prof)
                            if 'elemsProg' in matieresData[(periode, id_eleve)]['OTHERS'][matiereCode][id_prof]:
                                utils_functions.appendUnique(
                                    matieresData[(periode, id_eleve)]['OTHERS'][matiereCode][id_prof]['elemsProg'], 
                                    id_elem)
                            else:
                                matieresData[(periode, id_eleve)]['OTHERS'][
                                    matiereCode][id_prof]['elemsProg'] = [id_elem, ]

            # la même commande de base pour les EPI et l'AP :
            commandLineBase = (
                'SELECT DISTINCT eleve_groupe.*, '
                'appreciations1.appreciation, appreciations2.appreciation '
                'FROM eleve_groupe '
                'LEFT JOIN appreciations AS appreciations1 '
                'ON appreciations1.id_prof=eleve_groupe.id_prof '
                'AND appreciations1.Matiere=eleve_groupe.Matiere '
                'AND appreciations1.Periode=eleve_groupe.Periode '
                'AND appreciations1.id_eleve=eleve_groupe.id_eleve '

                'LEFT JOIN appreciations AS appreciations2 '
                'ON appreciations2.id_prof=eleve_groupe.id_prof '
                'AND appreciations2.Matiere=eleve_groupe.Matiere '
                'AND appreciations2.Periode=eleve_groupe.Periode '
                'AND appreciations2.id_eleve=-eleve_groupe.id_groupe-1 '

                'WHERE eleve_groupe.Matiere LIKE "{0}%" '
                'AND eleve_groupe.id_eleve IN ({1}) '
                'AND eleve_groupe.Periode IN (0, {2})')

            # epis-eleve :
            commandLine = commandLineBase.format(
                'EPI_', studentsForIn, periodsForIn)
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_eleve = int(query_recupEvals.value(1))
                matiereCode = query_recupEvals.value(4)
                periode = int(query_recupEvals.value(5))
                appreciation = ''
                try:
                    appreciation = replaceBadChars(query_recupEvals.value(6))
                except:
                    pass
                if len(appreciation) < 1:
                    try:
                        appreciation = replaceBadChars(query_recupEvals.value(7))
                    except:
                        pass
                if len(appreciation) > 0:
                    # on essaye de trouver le groupe d'epi :
                    if id_eleve in data['EPIS']['eleve2epis']:
                        for id_epiGroupe in data['EPIS']['eleve2epis'][id_eleve]:
                            if (id_epiGroupe in data['EPIS']['prof2epis'][id_prof]):
                                epiRef = data['EPIS']['episGroupes'][id_epiGroupe]['epi-ref']
                                id_epi = int(epiRef[4:])
                                epiTheme = data['EPIS']['episRef'][id_epi]['thematique']
                                if epiTheme == matiereCode:
                                    verifDic(matieresData, (periode, id_eleve), 'EPIS')
                                    # on met les appréciations bout à bout :
                                    if not(id_epiGroupe in matieresData[(periode, id_eleve)]['EPIS']):
                                        matieresData[(periode, id_eleve)]['EPIS'][id_epiGroupe] = appreciation
                                    else:
                                        matieresData[(periode, id_eleve)]['EPIS'][id_epiGroupe] = utils_functions.u(
                                            '{0} --- {1}').format(
                                                matieresData[(periode, id_eleve)]['EPIS'][id_epiGroupe], 
                                                appreciation)

            # acc-persos-eleve :
            commandLine = commandLineBase.format(
                'AP_LSU', studentsForIn, periodsForIn)
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_eleve = int(query_recupEvals.value(1))
                matiereCode = query_recupEvals.value(4)
                periode = int(query_recupEvals.value(5))
                appreciation = ''
                try:
                    appreciation = replaceBadChars(query_recupEvals.value(6))
                except:
                    pass
                if len(appreciation) < 1:
                    try:
                        appreciation = replaceBadChars(query_recupEvals.value(7))
                    except:
                        pass
                if len(appreciation) > 0:
                    # on essaye de trouver le groupe d'ap :
                    if id_eleve in data['APS']['eleve2aps']:
                        for id_apGroupe in data['APS']['eleve2aps'][id_eleve]:
                            if (id_apGroupe in data['APS']['prof2aps'][id_prof]):
                                verifDic(matieresData, (periode, id_eleve), 'APS')
                                # on met les appréciations bout à bout :
                                if not(id_apGroupe in matieresData[(periode, id_eleve)]['APS']):
                                    matieresData[(periode, id_eleve)]['APS'][id_apGroupe] = appreciation
                                else:
                                    matieresData[(periode, id_eleve)]['APS'][id_apGroupe] = utils_functions.u(
                                        '{0} --- {1}').format(
                                            matieresData[(periode, id_eleve)]['APS'][id_apGroupe], 
                                            appreciation)

            # récupération des notes (provisoire) :
            commandLineBase = utils_functions.u(
                'SELECT * FROM notes '
                'WHERE id_eleve IN ({0}) '
                'AND Periode IN ({1}) '
                'AND Matiere IN ({2})')
            commandLine = commandLineBase.format(
                studentsForIn, periodsForIn, matieresForIn)
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_eleve = int(query_recupEvals.value(1))
                matiereCode = query_recupEvals.value(2)
                periode = int(query_recupEvals.value(3))
                try:
                    value = float(query_recupEvals.value(4))
                except:
                    value = ''
                verifDic(
                    matieresData, (periode, id_eleve), 'OTHERS', matiereCode, id_prof)
                matieresData[(periode, id_eleve)]['OTHERS'][
                    matiereCode][id_prof]['note'] = value

            # on récupère les enseignements de complément
            # depuis la table admin.lsu :
            commandLine_admin = (
                'SELECT * FROM lsu '
                'WHERE lsuWhat="ensComp"')
            query_admin = utils_db.queryExecute(
                commandLine_admin, query=query_admin)
            while query_admin.next():
                replaceWith = query_admin.value(1)
                matieres = query_admin.value(2)
                if len(matieres) > 0:
                    matieres = matieres.split('|')
                    temp['enseignementsComplements'][replaceWith] = {}
                    for replaceWhat in matieres:
                        temp['matiere2EnseignementComplement'][replaceWhat] = replaceWith
            # au cas où le latin manquerait encore :
            if not('LCA' in temp['enseignementsComplements']):
                commandLine_admin = (
                    'SELECT * FROM lsu '
                    'WHERE lsuWhat="matiere" AND lsu2 LIKE "LCA%"')
                query_admin = utils_db.queryExecute(
                    commandLine_admin, query=query_admin)
                while query_admin.next():
                    replaceWhat = query_admin.value(1)
                    if not('LCA' in temp['enseignementsComplements']):
                        temp['enseignementsComplements']['LCA'] = {}
                    temp['matiere2EnseignementComplement'][replaceWhat] = 'LCA'
            enseignementComplementForIn = utils_functions.array2string(
                temp['matiere2EnseignementComplement'], 
                text=True)
            socleData = {}
            commandLineBase = (
                'SELECT * FROM syntheses '
                'WHERE id_eleve IN ({0}) '
                'AND name IN ({1})')
            commandLineLSU = (
                'SELECT * FROM lsu '
                'WHERE id_eleve IN ({0}) AND lsuWhat="socle"')
            for periode in query_p['PERIODS']:
                if periode < 999:
                    commandLine = commandLineBase.format(
                        studentsForIn, matieresForIn)
                    query_p[periode] = utils_db.queryExecute(commandLine, query=query_p[periode])
                    while query_p[periode].next():
                        id_eleve = int(query_p[periode].value(0))
                        matiereCode = query_p[periode].value(1)
                        value = query_p[periode].value(2)
                        id_prof = int(query_p[periode].value(3))
                        if value in value2pos:
                            verifDic(
                                matieresData, (periode, id_eleve), 'OTHERS', matiereCode, id_prof)
                            matieresData[(periode, id_eleve)]['OTHERS'][
                                matiereCode][id_prof]['positionnement'] = value2pos[value]
                    commandLine = commandLineLSU.format(studentsForIn)
                    query_p[periode] = utils_db.queryExecute(commandLine, query=query_p[periode])
                    while query_p[periode].next():
                        id_eleve = int(query_p[periode].value(1))
                        cpt = query_p[periode].value(2)
                        value = query_p[periode].value(3)
                        if value in value2pos:
                            if not ((periode, id_eleve) in socleData):
                                socleData[(periode, id_eleve)] = {}
                            socleData[(periode, id_eleve)][cpt] = value2pos[value]
                else:
                    # on vérifie aussi s'il y a validation du positionnement :
                    commandLine = (
                        'SELECT * FROM persos '
                        'WHERE id_eleve IN ({0}) '
                        'AND Matiere IN ({1})').format(
                        studentsForIn, enseignementComplementForIn)
                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                    while query_recupEvals.next():
                        id_eleve = int(query_recupEvals.value(1))
                        value = query_recupEvals.value(5)
                        matiere = query_recupEvals.value(7)
                        ensComp = temp['matiere2EnseignementComplement'].get(matiere, '')
                        if len(ensComp) > 0:
                            if not(id_eleve in temp['enseignementsComplements'][ensComp]):
                                temp['enseignementsComplements'][ensComp][id_eleve] = ''
                            if value in value2pos:
                                temp['enseignementsComplements'][ensComp][id_eleve] += value
                    # on vérifie aussi s'il y a validation du positionnement :
                    commandLine = (
                        'SELECT * FROM lsu '
                        'WHERE lsuWhat="positionnement" '
                        'AND lsu3 IN ({0}) '
                        'AND lsu2 IN ({1})').format(
                        studentsForIn, enseignementComplementForIn)
                    query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                    while query_recupEvals.next():
                        id_eleve = int(query_recupEvals.value(4))
                        value = query_recupEvals.value(5)
                        matiere = query_recupEvals.value(3)
                        ensComp = temp['matiere2EnseignementComplement'].get(matiere, '')
                        if len(ensComp) > 0:
                            if value in value2pos:
                                temp['enseignementsComplements'][ensComp][id_eleve] = value
                    import utils_calculs
                    for ensComp in temp['enseignementsComplements']:
                        for id_eleve in temp['enseignementsComplements'][ensComp]:
                            value = utils_calculs.moyenneBilan(
                                temp['enseignementsComplements'][ensComp][id_eleve])
                            pos = value2pos.get(value, 1)
                            if pos > 2:
                                temp['enseignementsComplements'][ensComp][id_eleve] = 2
                            else:
                                temp['enseignementsComplements'][ensComp][id_eleve] = 1

                    commandLine = commandLineLSU.format(studentsForIn)
                    query_p[periode] = utils_db.queryExecute(commandLine, query=query_p[periode])
                    while query_p[periode].next():
                        id_eleve = int(query_p[periode].value(1))
                        cpt = query_p[periode].value(2)
                        value = query_p[periode].value(3)
                        if value == 'X':
                            value = utils.LSU['LSU_X']
                        if value in value2pos:
                            if not ((periode, id_eleve) in socleData):
                                socleData[(periode, id_eleve)] = {}
                            socleData[(periode, id_eleve)][cpt] = value2pos[value]

            # récupération des absences :
            commandLineBase = utils_functions.u(
                'SELECT * FROM absences '
                'WHERE id_eleve IN ({0}) '
                'AND Periode IN ({1})')
            commandLine = commandLineBase.format(
                studentsForIn, periodsForIn)
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_eleve = int(query_recupEvals.value(1))
                periode = int(query_recupEvals.value(2))
                abs0 = int(query_recupEvals.value(3))
                abs1 = int(query_recupEvals.value(4))
                abs2 = int(query_recupEvals.value(5))
                abs3 = int(query_recupEvals.value(6))
                verifDic(matieresData, (periode, id_eleve), 'VS')
                matieresData[(periode, id_eleve)]['VS']['nb-abs-justifiees'] = abs0
                matieresData[(periode, id_eleve)]['VS']['nb-abs-injustifiees'] = abs1
                matieresData[(periode, id_eleve)]['VS']['nb-retards'] = abs2
                matieresData[(periode, id_eleve)]['VS']['nb-heures-manquees'] = abs3

            for periode in data['periodes']:
                for id_eleve in data['eleves']:
                    classe = temp['eleve2classe'][id_eleve]
                    responsableEtab = temp['classe2responsableEtab'][classe]
                    evaluationType = temp['classe2evaluationType'].get(classe, 0)
                    # O : eleve-ref, periode-ref, date-conseil-classe, date-scolarite, date-verrou, responsable-etab-ref
                    # N : [prof-princ-refs]
                    dateScolarite = data['dates'][periode][0]
                    (eleveDateEntree, eleveDateSortie) = temp['eleve2dates'][id_eleve]
                    # on ne met pas un élève qui est parti avant le début de la période :
                    if eleveDateSortie > 0:
                        if eleveDateSortie < lsuDate2Integer(dateScolarite):
                            continue
                    # et s'il est arrivé après le début, on corrige dateScolarite :
                    if lsuDate2Integer(dateScolarite) < eleveDateEntree:
                        dateScolarite = integer2LsuDate(eleveDateEntree)
                    data['bilans'][(periode, id_eleve)] = {
                        'attributs': {
                            'eleve-ref': 'EL_{0}'.format(id_eleve), 
                            'periode-ref': 'P_{0}'.format(periode), 
                            'date-conseil-classe': data['dates'][periode][1], 
                            'date-scolarite': dateScolarite, 
                            'date-verrou': data['dates'][periode][1], 
                            'responsable-etab-ref': 'RESP_{0}'.format(responsableEtab)}, 
                        'OTHERS': {}, 
                        'VS': {}, 
                        'PP': '', 
                        'SOCLE': {}, 
                        'EPIS': {}, 
                        'APS': {}, 
                        }

                    if (periode, id_eleve) in matieresData:
                        others = matieresData[(periode, id_eleve)].get('OTHERS', {})
                        for matiereCode in others:
                            # LSU ne permet qu'un prof par matière !!!
                            # on doit donc cumuler s'il y en a 2 ou plus
                            id_matiere = temp['matiere2id'][matiereCode]
                            profs = ''
                            elemsProg = []
                            notes = []
                            appreciation = ''
                            positionnements = []
                            for id_prof in others[matiereCode]:
                                profs = '{0} ENS_{1}'.format(profs, id_prof)
                                elemsProg_prof = others[matiereCode][id_prof].get(
                                    'elemsProg', [])
                                for elem in elemsProg_prof:
                                    if not(elem in elemsProg):
                                        elemsProg.append(elem)
                                note_prof = others[matiereCode][id_prof].get('note', '')
                                if note_prof != '':
                                    notes.append(note_prof)
                                appreciation_prof = others[matiereCode][id_prof].get(
                                    'appreciation', '')
                                # on met les appréciations bout à bout :
                                if len(appreciation_prof) > 0:
                                    if len(appreciation) < 1:
                                        appreciation = appreciation_prof
                                    else:
                                        appreciation = utils_functions.u('{0} --- {1}').format(
                                            appreciation, appreciation_prof)
                                positionnement_prof = others[matiereCode][id_prof].get(
                                    'positionnement', 0)
                                if positionnement_prof > 0:
                                    positionnements.append(positionnement_prof)
                            profs = profs[1:]
                            elemsProgRefs = ''
                            for elem in elemsProg:
                                elemsProgRefs = '{0} EP_{1}'.format(elemsProgRefs, elem)
                            elemsProgRefs = elemsProgRefs[1:]
                            if len(elemsProgRefs) < 1:
                                elemsProgRefs = 'EP_0'
                            note = 10
                            if len(notes) > 0:
                                note = average(notes, precision=1)
                            if len(appreciation) < 1:
                                appreciation = noComment
                            positionnement = 0
                            if len(positionnements) > 0:
                                # faire une moyenne : utils_calculs
                                positionnement = int(average(positionnements, precision=0))
                            # O : discipline-ref, enseignant-refs, element-programme-refs
                            # N : moyenne-eleve, moyenne-structure, 
                            #     [eleve-non-note], [structure-non-notee], [positionnement]
                            data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere] = {
                                'attributs': {
                                    'discipline-ref': 'DI_{0}'.format(id_matiere), 
                                    'enseignant-refs': profs, 
                                    'element-programme-refs': elemsProgRefs, 
                                    }, 
                                'appreciation': appreciation[:600], 
                                }
                            if evaluationType < 1:
                                if positionnement > 0:
                                    data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere][
                                        'attributs']['positionnement'] = positionnement
                                else:
                                    data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere][
                                        'attributs']['eleve-non-note'] = 1
                            else:
                                data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere][
                                    'attributs']['moyenne-eleve'] = '{0}/20'.format(note)
                                # ICI À FAIRE
                                data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere][
                                    'attributs']['moyenne-structure'] = '10/20'

                        epis = matieresData[(periode, id_eleve)].get('EPIS', {})
                        for id_epiGroupe in epis:
                            epiGroupeRef = 'EPI_GROUPE_{0}'.format(id_epiGroupe)
                            appreciation = epis[id_epiGroupe]
                            if len(appreciation) < 1:
                                appreciation = noComment
                            data['bilans'][(periode, id_eleve)]['EPIS'][id_epiGroupe] = {
                                'attributs': {
                                    'epi-groupe-ref': epiGroupeRef, 
                                    }, 
                                'commentaire': appreciation[:600], 
                                }

                        aps = matieresData[(periode, id_eleve)].get('APS', {})
                        for id_apGroupe in aps:
                            apGroupeRef = 'ACC_PERSO_GROUPE_{0}'.format(id_apGroupe)
                            appreciation = aps[id_apGroupe]
                            if len(appreciation) < 1:
                                appreciation = noComment
                            data['bilans'][(periode, id_eleve)]['APS'][id_apGroupe] = {
                                'attributs': {
                                    'acc-perso-groupe-ref': apGroupeRef, 
                                    }, 
                                'commentaire': appreciation[:600], 
                                }

                        for parcoursType in utils.LSU['PARCOURS']:
                            appreciation = ''
                            if (periode, id_eleve) in parcoursCommuns['STUDENTS']:
                                appreciation = parcoursCommuns[
                                    'STUDENTS'][(periode, id_eleve)].get(parcoursType, appreciation)
                            if len(appreciation) < 1:
                                if (classe, periode) in data['parcoursCommuns']:
                                    appreciation = data[
                                        'parcoursCommuns'][(classe, periode)]['parcours'].get(
                                            parcoursType, appreciation)
                            if len(appreciation) > 0:
                                verifDic(data['bilans'][(periode, id_eleve)], 'parcours', parcoursType)
                                data['bilans'][(periode, id_eleve)]['parcours'][parcoursType] = appreciation[:600]

                        if id_eleve in accompagnements:
                            data['bilans'][(periode, id_eleve)]['accompagnements'] = {}
                            for lsuWhat in accompagnements[id_eleve]:
                                data['bilans'][(periode, id_eleve)][
                                    'accompagnements'][lsuWhat] = accompagnements[id_eleve][lsuWhat][:600]

                        pp = matieresData[(periode, id_eleve)].get('PP', '')
                        if len(pp) < 1:
                            pp = noComment
                        data['bilans'][(periode, id_eleve)]['PP'] = pp[:1000]

                        if id_eleve in devoirsFaits:
                            data['bilans'][(periode, id_eleve)][
                                'devoirs-faits'] = devoirsFaits[id_eleve][:600]

                        vs = matieresData[(periode, id_eleve)].get('VS', {})
                        # O : nb-retards, nb-abs-justifiees, nb-abs-injustifiees
                        # N : nb-heures-manquees, commentaire
                        data['bilans'][(periode, id_eleve)]['VS'] = {
                            'attributs': {
                                'nb-abs-justifiees': vs.get('nb-abs-justifiees', 0), 
                                'nb-abs-injustifiees': vs.get('nb-abs-injustifiees', 0), 
                                'nb-retards': vs.get('nb-retards', 0)}, 
                            }
                        nbHeuresManquees = vs.get('nb-heures-manquees', 0)
                        if nbHeuresManquees > 0:
                            data['bilans'][(periode, id_eleve)][
                                'VS']['attributs']['nb-heures-manquees'] = nbHeuresManquees
                        commentaire = vs.get('appreciation', '')[:600]
                        if len(commentaire) > 0:
                            data['bilans'][(periode, id_eleve)][
                                'VS']['commentaire'] = commentaire

                        socle = socleData.get((periode, id_eleve), {})
                        # Attention : ne sera accepté que si les 8 sont présents
                        if len(socle) == 8:
                            data['bilans'][(periode, id_eleve)]['SOCLE'] = {}
                            for bilanName in socle:
                                data['bilans'][(periode, id_eleve)][
                                    'SOCLE'][bilanName] = socle[bilanName]

            deletes = {'SUBJECTS': {}, 'STUDENTS': []}
            for (periode, id_eleve) in data['bilans']:
                for id_matiere in data['bilans'][(periode, id_eleve)]['OTHERS']:
                    # s'il n'y a ni évaluation, ni appréciation, on vire la matière :
                    if 'eleve-non-note' in data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere]['attributs']:
                        if data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere]['appreciation'] == noComment:
                            if not((periode, id_eleve) in deletes['SUBJECTS']):
                                deletes['SUBJECTS'][(periode, id_eleve)] = [id_matiere, ]
                            else:
                                deletes['SUBJECTS'][(periode, id_eleve)].append(id_matiere)
            # on efface les matières inutiles :
            for (periode, id_eleve) in deletes['SUBJECTS']:
                inDeletes = len(deletes['SUBJECTS'][(periode, id_eleve)])
                inData = len(data['bilans'][(periode, id_eleve)]['OTHERS'])
                if inDeletes < inData:
                    for id_matiere in deletes['SUBJECTS'][(periode, id_eleve)]:
                        # on efface la matière :
                        del data['bilans'][(periode, id_eleve)]['OTHERS'][id_matiere]
                        msg = utils_functions.u(
                            'pas de positionnement : P_{0}, {1} {2}, {3}, {4}').format(
                                periode, 
                                data['eleves'][id_eleve]['nom'], 
                                data['eleves'][id_eleve]['prenom'], 
                                data['eleves'][id_eleve]['code-division'], 
                                temp['id2matiere'][id_matiere])
                        utils_functions.afficheMessage(
                            self.main, msg, editLog=True)
                else:
                    # on effacera l'élève :
                    deletes['STUDENTS'].append((periode, id_eleve))
            # on efface les élèves inutiles :
            for (periode, id_eleve) in deletes['STUDENTS']:
                del data['bilans'][(periode, id_eleve)]
                msg = utils_functions.u(
                    "<b>pas d'acquis :</b> P_{0}, {1} {2}, {3}").format(
                        periode, 
                        data['eleves'][id_eleve]['nom'], 
                        data['eleves'][id_eleve]['prenom'], 
                        data['eleves'][id_eleve]['code-division'])
                utils_functions.afficheMessage(
                    self.main, msg, editLog=True)

            # bilan de cycle :
            data['cycle'] = {}
            if self.checkBoxs[999].isChecked():
                syntheses = {}
                commandLine = utils_functions.u(
                    'SELECT * FROM appreciations '
                    'WHERE id_eleve IN ({0}) AND matiere="PP" '
                    'ORDER BY Periode').format(studentsForIn)
                query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
                while query_recupEvals.next():
                    id_eleve = int(query_recupEvals.value(1))
                    appreciation = replaceBadChars(query_recupEvals.value(2))
                    if len(appreciation) > 0:
                        syntheses[id_eleve] = appreciation[:1500]
                for id_eleve in data['eleves']:
                    socle = socleData.get((999, id_eleve), {})
                    for bilanName in utils.LSU['SOCLE_COMPONENTS']['ORDER_LSU']:
                        if not bilanName in socle:
                            socle[bilanName] = value2pos[utils.LSU['LSU_X']]
                    # Attention : ne sera accepté que si les 8 sont présents
                    if len(socle) == 8:
                        classe = temp['eleve2classe'][id_eleve]
                        responsableEtab = temp['classe2responsableEtab'][classe]
                        cycle = temp['classe2cycle'].get(classe, 3)
                        # O : eleve-ref, cycle, millesime, date-creation, date-verrou, responsable-etab-ref
                        # N : [prof-princ-refs]
                        data['cycle'][id_eleve] = {
                            'attributs': {
                                'eleve-ref': 'EL_{0}'.format(id_eleve), 
                                'cycle': cycle, 
                                'millesime': self.main.annee_scolaire[0] - 1, 
                                'date-creation': today, 
                                'date-verrou': data['dates'][utils.NB_PERIODES - 1][1], 
                                'responsable-etab-ref': 'RESP_{0}'.format(responsableEtab)}, 
                            'PP': syntheses.get(id_eleve, noComment), 
                            'SOCLE': {}, 
                            'ENS_COMP': ('', -1), 
                            }
                        for bilanName in socle:
                            data['cycle'][id_eleve][
                                'SOCLE'][bilanName] = socle[bilanName]
                        # un seul ensComp possible par élève 
                        # (on garde au hasard une des meilleures valeurs) :
                        if cycle == 4:
                            enseignementComplement = 'AUC'
                            pos = -1
                            for ensComp in temp['enseignementsComplements']:
                                if id_eleve in temp['enseignementsComplements'][ensComp]:
                                    newPos = temp['enseignementsComplements'][ensComp][id_eleve]
                                    if newPos > pos:
                                        enseignementComplement = ensComp
                                        pos = newPos
                            data['cycle'][id_eleve]['ENS_COMP'] = (enseignementComplement, pos)


                # ICI cptNum
                # s'il n'y a pas de bilan périodique,
                # on efface les données inutiles :
                if len(data['periodes']) < 1:
                    data['disciplines'] = {}
                    data['enseignants'] = {}
                    data['elementsProgramme'] = {}
                    data['cptNum'] = {'COMMUN': {}, 'STUDENTS': {}}
                    data['parcoursCommuns'] = {}
                    data['vsCommuns'] = {}
                    data['EPIS'] = {'episRef': {}, 'episGroupes': {}, }
                    data['APS'] = {'apsRef': {}, 'apsGroupes': {}, }


            utils_functions.afficheMessage(self.main, '<br/>', editLog=True)
            import utils_export_xml
            OK = utils_export_xml.export2LSU(self.main, fileName, data)
        finally:
            # on ferme les bases resultats-x :
            for p in query_p['PERIODS']:
                if query_p[p] != None:
                    query_p[p].clear()
                    del query_p[p]
                if p == 1:
                    db_1.close()
                    del db_1
                elif p == 2:
                    db_2.close()
                    del db_2
                elif p == 3:
                    db_3.close()
                    del db_3
                elif p == 999:
                    db_999.close()
                    del db_999
                utils_db.sqlDatabase.removeDatabase(query_p['DB_NAMES'][p])

            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(self.main)
            if not(OK):
                message = QtWidgets.QApplication.translate(
                    'main', 'Exporting the file failed or you have canceled.')
                utils_functions.messageBox(self.main, level='warning', message=message)
            else:
                utils_functions.afficheMsgFin(self.main, timer=5)
            return OK








