# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
Gestion des connexions et échanges par FTP.
"""

# importation des modules utiles :
from __future__ import division, print_function
import ftplib
import datetime
import time
# ICI LFTP
import os, subprocess

import utils, utils_functions, utils_db, utils_filesdirs
import admin

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
else:
    from PyQt4 import QtCore, QtGui as QtWidgets



###########################################################"
#
#   VARIABLES GLOBALES DE CONNEXION FTP
#
###########################################################"

FTP_HOST, FTP_USER, FTP_PASS = '', '', ''
FTP_HOST_OK, FTP_PASS_EXISTS, FTP_LOGIN_OK = False, False, False
FTP_PORT = 21
FTP_DELAY = 10

# ICI SFTP
SFTP = True
#SFTP = False
if SFTP:
    try:
        import paramiko
    except:
        SFTP = False
print('SFTP:', SFTP)
FTP_SFTP = False

# ICI LFTP
LFTP = False
possiblePaths = ('/usr/bin', '/usr/local/bin', '/opt', )
for possiblePath in possiblePaths:
    fileName = os.path.join(possiblePath, 'lftp')
    if os.path.exists(fileName):
        LFTP = True
        break
print('LFTP:', LFTP)


def ftpUpdateConfig(
    newFtpHost=None, newFtpUser=None, newFtpPass=None, 
    newFtpPort=None, newFtpDelay=None, newSftp=None):
    """
    fonction permettant de modifier les variables globales
    depuis un autre module (admin à priori)
    """
    global FTP_HOST, FTP_USER, FTP_PASS, FTP_PORT, FTP_DELAY, SFTP
    if newFtpHost != None:
        FTP_HOST = newFtpHost
    if newFtpUser != None:
        FTP_USER = newFtpUser
    if newFtpPass != None:
        FTP_PASS = newFtpPass
    if newFtpPort != None:
        FTP_PORT = newFtpPort
    if newFtpDelay != None:
        FTP_DELAY = newFtpDelay
    # ICI SFTP
    if newSftp != None:
        if newSftp == True:
            try:
                import paramiko
                SFTP = True
            except:
                SFTP = False
        else:
            SFTP = False



###########################################################"
#
#   TESTS DE CONNEXION FTP ET GESTION DU MOT DE PASSE
#
###########################################################"

def ftpTestHost(host):
    # test du host :
    # PB 1&1 :
    result = False
    # ICI SFTP
    if SFTP:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(host, username='?', password='?')
        except paramiko.AuthenticationException:
            result = True
        except:
            pass
        ssh.close()
    else:
        t = 0
        while (t < 10) and (result == False):
            t += 1
            try:
                if t > 1:
                    time.sleep(FTP_DELAY)
                ftp = ftplib.FTP(host, '', '', '', 5)
                ftp.quit()
                result = True
            except:
                pass
    return result

def ftpTestLogin(host, user, mdp):
    # test de login :
    result = False
    # ICI SFTP
    if SFTP:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(host, username=user, password=mdp)
            result = True
        except:
            pass
        ssh.close()
    else:
        try:
            ftp = ftplib.FTP(host, '', '', '', 5)
            ftp.connect(host, FTP_PORT)
            msg = ftp.login(user, mdp)
            ftp.quit()
            result = True
        except:
            print('ERROR IN ftpTestLogin')
            pass
    return result

def ftpTest(main, askPassword=True, force=False):
    """
    teste si la connexion FTP est possible 
    et met à jour l'état des 3 variables globales 
    (FTP_HOST_OK, FTP_PASS_EXISTS, FTP_LOGIN_OK).
    L'état est affiché dans la fenêtre admin (et la console).
    **Utilisation :**
        FTP_LOGIN_OK = ftpTest(main)
    """
    global FTP_HOST_OK, FTP_PASS_EXISTS, FTP_LOGIN_OK
    # traductions :
    msgOK = QtWidgets.QApplication.translate('main', 'OK')
    msgNO = QtWidgets.QApplication.translate('main', 'NO')
    msgEmpty = QtWidgets.QApplication.translate('main', 'Empty')
    msgResult = QtWidgets.QApplication.translate(
        'main', 
        '<b>FTP connection test:</b> (Host: {0}, Password: {1}, Login: {2})')
    # réponses par défaut :
    ftpHostOk_msg = msgOK
    ftpMdpExists_msg = msgEmpty
    ftpLoginOk_msg = msgNO
    try:
        # pour éviter le test si on n'a pas mis en place de FTP :
        if FTP_HOST in ('ftp.adresse', ''):
            ftpHostOk_msg = msgNO
            return
        # test de connexion (le host est-il valable ?) :
        host = FTP_HOST
        if not(FTP_HOST_OK):
            FTP_HOST_OK = ftpTestHost(host)
        if not(FTP_HOST_OK):
            ftpHostOk_msg = msgNO
            return
        # test d'existence du mot de passe :
        if FTP_PASS == '':
            FTP_PASS_EXISTS = False
            if askPassword:
                ftpRequestPassword(main)
        if FTP_PASS != '':
            FTP_PASS_EXISTS = True
            ftpMdpExists_msg = msgOK
            # test de login :
            if force or not(FTP_LOGIN_OK):
                FTP_LOGIN_OK = ftpTestLogin(host, FTP_USER, FTP_PASS)
            if FTP_LOGIN_OK:
                ftpLoginOk_msg = msgOK
    except:
        print('ERROR IN ftpTest')
    finally:
        msg = utils_functions.u(msgResult).format(
            ftpHostOk_msg, ftpMdpExists_msg, ftpLoginOk_msg)
        utils_functions.afficheMessage(main, msg, editLog=True)
        return FTP_LOGIN_OK

def ftpRequestPassword(main):
    """
    boîte de dialogue pour saisir le mot de passe FTP.
    Peut être appelée depuis la procédure précédente ou directement
    depuis l'interface admin.
    """
    global FTP_PASS
    result = False
    # on teste l'aspect du curseur (doit être normal) :
    try:
        waitCursor = (
            QtWidgets.QApplication.overrideCursor().shape() == \
                QtCore.Qt.WaitCursor)
    except:
        waitCursor = False
    if waitCursor:
        QtWidgets.QApplication.restoreOverrideCursor()
    text, ok = QtWidgets.QInputDialog.getText(
        main, 
        utils.PROGNAME, 
        QtWidgets.QApplication.translate(
            'main', 'FTP Password:'), 
        QtWidgets.QLineEdit.Password, 
        '')
    if ok and text != '':
        FTP_PASS = text
        result = True
    # on remet le curseur wait si besoin :
    if waitCursor:
        QtWidgets.QApplication.setOverrideCursor(
            QtCore.Qt.WaitCursor)
    return result

def ftpTestDirExists(host, user, mdp, dirs=[]):
    # s'il n'y a qu'un dossier, on remplit la liste :
    if not(isinstance(dirs, list)):
        dirs = [dirs, ]
    results = {'HOST': False, 'LOGIN': False}
    for aDir in dirs:
        results[aDir] = False
    #print('ftpTestDirExists :', dirs)
    # ICI SFTP
    if SFTP:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(host, username=user, password=mdp)
            results['HOST'] = True
            results['LOGIN'] = True
        except:
            return results
        sftp = ssh.open_sftp()
        for aDir in dirs:
            aDir2 = utils_functions.removeSlash(aDir)
            if aDir2[0] == '/':
                aDir2 = aDir2[1:]
            try:
                sftp.listdir(aDir2)
                results[aDir] = True
            except:
                pass
        sftp.close()
        ssh.close()
    else:
        try:
            ftp = ftplib.FTP(host, '', '', '', 5)
            ftp.connect(host, FTP_PORT)
            results['HOST'] = True
            if not(ftp.login(user, mdp)):
                return results
            results['LOGIN'] = True
            for aDir in dirs:
                aDir2 = utils_functions.removeSlash(aDir)
                if ftp.cwd(aDir2):
                    results[aDir] = True
                else:
                    # PB pluneret 
                    # (le changement de dossier ne se fait pas en une seule fois) :
                    if ftp.pwd() == aDir2:
                        results[aDir] = True
                    else:
                        vazy = True
                        for d in aDir2.split('/'):
                            if d != '' and vazy:
                                # on récupère la liste des dossiers
                                # pour vérifier que celui demandé existe :
                                ftpList = []
                                try:
                                    ftp.dir('-a ', ftpList.append)
                                except ftplib.error_temp:
                                    ftp.dir(ftpList.append)
                                if len(ftpList) < 1:
                                    ftp.retrlines('LIST', ftpList.append)
                                dirsInDir = []
                                ftpList = formatFtpList(ftpList)
                                for [name, isDir, size, yyyy, MMdd, hhmm] in ftpList:
                                    if name in ('.', '..'):
                                        continue
                                    try:
                                        ftp.cwd(name)
                                        ftp.cwd('..')
                                        # c'est bien un répertoire :
                                        dirsInDir.append(name)
                                    except:
                                        pass
                                if d in dirsInDir:
                                    ftp.cwd(d)
                                else:
                                    vazy = False
        except:
            pass
        finally:
            try:
                ftp.quit()
            except:
                try:
                    ftp.close()
                except:
                    pass
                pass
    return results

def ftpMakeDir(host, user, mdp, theDir):
    result = False
    theDir = utils_functions.removeSlash(theDir)
    #print('ftpMakeDir:', theDir)
    # ICI SFTP
    if SFTP:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh.connect(host, username=user, password=mdp)
        except:
            return
        sftp = ssh.open_sftp()
        if theDir[0] == '/':
            theDir = theDir[1:]
        try:
            sftp.mkdir(theDir)
        except IOError:
            pass
        result = True
        sftp.close()
        ssh.close()
    else:
        try:
            ftp = ftplib.FTP(host, '', '', '', 5)
            ftp.connect(host, FTP_PORT)
            if not(ftp.login(user, mdp)):
                return
            ftp.mkd(theDir)
            result = True
        except:
            pass
        finally:
            try:
                ftp.quit()
            except:
                pass
    return result



###########################################################"
#
#   CLASSE DE GESTION DU FTP
#
#   permet d'éviter les problèmes de déconnexion.
#   d'après http://python.developpez.com/faq/?page=FTP
#
###########################################################"

class GDDFTP(ftplib.FTP):

    def __init__(self, main, host, user, password, port=FTP_PORT, reconnect=True):
        """
        on initialise les variables
        """
        ftplib.FTP.__init__(self, '')
        self.main = main
        self.host = host
        self.user = user
        self.password = password
        self.port = port
        self.reconnect(first=True)
        self.reconnected = False

    def reconnect(self, first=False):
        """
        fonction de reconnexion appelée en cas de besoin (par doCommand, etc)
        """
        time.sleep(FTP_DELAY)
        print('admin_ftp.GDDFTP.reconnect')
        self.connect(self.host, self.port)
        msg = self.login(self.user, self.password)
        utils_functions.afficheMessage(self.main, msg, editLog=True)
        if not(first):
            self.reconnected = True

    def doCommand(self, command, *args):
        """
        execute une commande FTP avec reconnexion si besoin
        """
        #print(command, *args)
        result = False
        try:
            result = command(*args)
        except:
            self.reconnect()
            result = command(*args)
        finally:
            return result

    def doCwd(self, aDir):
        """
        changement de dossier courant
        """
        result = False
        if self.doCommand(self.cwd, aDir):
            return True
        # PB pluneret (le changement de dossier ne se fait pas en une seule fois) :
        aDir = utils_functions.removeSlash(aDir)
        if self.pwd() != aDir:
            for d in aDir.split('/'):
                if d != '':
                    result = self.doCommand(self.cwd, d)
        else:
            result = True
        return result

    def doList(self):
        """
        liste le contenu du dossier courant
        """
        ftpList = []
        try:
            self.doCommand(self.dir, '-a ', ftpList.append)
        except ftplib.error_temp:
            self.doCommand(self.dir, ftpList.append)
        if len(ftpList) < 1:
            self.doCommand(self.retrlines, 'LIST', ftpList.append)
        #print(ftpList)
        return ftpList



###########################################################"
#
#   TÉLÉCHARGEMENT ET ENVOI D'UN FICHIER
#
###########################################################"

# ICI LFTP
def ftpGetFile(main, dirSite, dirLocal, theFileName, testSize=-1):
    if LFTP:
        return ftpGetFile_LFTP(main, dirSite, dirLocal, theFileName, testSize)
    else:
        return ftpGetFile_OLD(main, dirSite, dirLocal, theFileName, testSize)

def ftpGetFile_LFTP(main, dirSite, dirLocal, theFileName, testSize=-1):
    """
    téléchargement d'un fichier en FTP
    """
    try:
        msg = QtWidgets.QApplication.translate(
            'main', 'Download the file: {0}')
        msg = utils_functions.u(msg).format(theFileName)
        utils_functions.afficheMessage(main, msg, editLog=True, p=True)
        # on récupère le fichier à télécharger dans temp :
        dirLocal = utils_functions.addSlash(dirLocal)
        localFile = dirLocal + theFileName
        tempFile = utils_functions.u(
            '{0}/down/{1}').format(main.tempPath, theFileName)
        dirSite = utils_functions.addSlash(dirSite)
        siteFile = dirSite + theFileName
        if SFTP:
            host = 'sftp://{0}'.format(FTP_HOST)
        else:
            host = 'ftp://{0}'.format(FTP_HOST)
        commandLine = 'lftp -e "open $HOST; user $USER $PASS; set net:max-retries 2; get $DIRSITE -o $DIRLOCAL; bye"'
        commandLine = 'DIRLOCAL={0} && DIRSITE={1} && HOST={2} && USER={3} && PASS={4} && {5}'.format(
            tempFile, 
            siteFile, 
            host, 
            FTP_USER, 
            FTP_PASS, 
            commandLine)
        #print(commandLine)
        reponse = subprocess.call(commandLine, shell=True)
        utils_functions.afficheMessage(main, reponse, editLog=True)
        # on remplace enfin localFile par tempFile :
        utils_filesdirs.removeAndCopy(
            tempFile, localFile, testSize=testSize)
        return True
    except:
        return False

def ftpGetFile_OLD(main, dirSite, dirLocal, theFileName, testSize=-1):
    """
    téléchargement d'un fichier en FTP
    """
    if dirSite[0] == '/':
        dirSite = dirSite[1:]
    try:
        msg = QtWidgets.QApplication.translate(
            'main', 'Download the file: {0}')
        msg = utils_functions.u(msg).format(theFileName)
        utils_functions.afficheMessage(main, msg, editLog=True, p=True)
        # on récupère le fichier à télécharger dans temp :
        dirLocal = utils_functions.addSlash(dirLocal)
        localFile = dirLocal + theFileName
        tempFile = utils_functions.u(
            '{0}/down/{1}').format(main.tempPath, theFileName)
        # ICI SFTP
        if SFTP:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(FTP_HOST, username=FTP_USER, password=FTP_PASS)
            sftp = ssh.open_sftp()
            sftp.get(
                utils_functions.u(
                    '{0}/{1}').format(dirSite, theFileName), 
                tempFile)
            sftp.close()
            ssh.close()
        else:
            # connexion FTP :
            ftp = GDDFTP(main, FTP_HOST, FTP_USER, FTP_PASS)
            # on se place dans le bon dossier FTP :
            ftp.doCwd(dirSite)
            # download du fichier :
            ftp.doCommand(ftp.sendcmd, 'TYPE I')
            ftp.doCommand(ftp.sendcmd, 'PASV')
            msg = ftp.doCommand(
                ftp.retrbinary, 
                'RETR ' + theFileName, 
                open(tempFile, 'wb').write)
            utils_functions.afficheMessage(main, msg, editLog=True)
            ftp.quit()
        # on remplace enfin localFile par tempFile :
        utils_filesdirs.removeAndCopy(
            tempFile, localFile, testSize=testSize)
        return True
    except:
        return False


# ICI LFTP
def ftpPutFile(main, dirSite, dirLocal, theFileName):
    if LFTP:
        return ftpPutFile_LFTP(main, dirSite, dirLocal, theFileName)
    else:
        return ftpPutFile_OLD(main, dirSite, dirLocal, theFileName)

def ftpPutFile_LFTP(main, dirSite, dirLocal, theFileName):
    """
    envoi d'un fichier en FTP
    """
    try:
        msg = QtWidgets.QApplication.translate(
            'main', 'Upload the file: {0}')
        msg = utils_functions.u(msg).format(theFileName)
        utils_functions.afficheMessage(main, msg, editLog=True)
        # on copie le fichier à envoyer dans temp :
        dirLocal = utils_functions.addSlash(dirLocal)
        localFile = utils_functions.u(
            '{0}{1}').format(dirLocal, theFileName)
        tempFile = utils_functions.u(
            '{0}/up/{1}').format(main.tempPath, theFileName)
        utils_filesdirs.removeAndCopy(localFile, tempFile)
        dirSite = utils_functions.addSlash(dirSite)
        if SFTP:
            host = 'sftp://{0}'.format(FTP_HOST)
        else:
            host = 'ftp://{0}'.format(FTP_HOST)
        commandLine = 'lftp -e "open $HOST; user $USER $PASS; set net:max-retries 2; put -O $DIRSITE $DIRLOCAL; bye"'
        commandLine = 'DIRLOCAL={0} && DIRSITE={1} && HOST={2} && USER={3} && PASS={4} && {5}'.format(
            tempFile, 
            dirSite, 
            host, 
            FTP_USER, 
            FTP_PASS, 
            commandLine)
        #print(commandLine)
        reponse = subprocess.call(commandLine, shell=True)
        utils_functions.afficheMessage(main, reponse, editLog=True)
        return True
    except:
        return False

def ftpPutFile_OLD(main, dirSite, dirLocal, theFileName):
    """
    envoi d'un fichier en FTP
    """
    if dirSite[0] == '/':
        dirSite = dirSite[1:]
    try:
        msg = QtWidgets.QApplication.translate(
            'main', 'Upload the file: {0}')
        msg = utils_functions.u(msg).format(theFileName)
        utils_functions.afficheMessage(main, msg, editLog=True)
        # on copie le fichier à envoyer dans temp :
        dirLocal = utils_functions.addSlash(dirLocal)
        localFile = utils_functions.u(
            '{0}{1}').format(dirLocal, theFileName)
        tempFile = utils_functions.u(
            '{0}/up/{1}').format(main.tempPath, theFileName)
        utils_filesdirs.removeAndCopy(localFile, tempFile)
        # ICI SFTP
        if SFTP:
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(FTP_HOST, username=FTP_USER, password=FTP_PASS)
            sftp = ssh.open_sftp()
            sftp.put(
                tempFile, 
                utils_functions.u(
                    '{0}/{1}').format(dirSite, theFileName))
            sftp.close()
            ssh.close()
        else:
            # connexion FTP :
            ftp = GDDFTP(main, FTP_HOST, FTP_USER, FTP_PASS)
            # on se place dans le bon dossier FTP :
            ftp.doCwd(dirSite)
            # on envoie le fichier et on quitte :
            ftp.doCommand(ftp.sendcmd, 'TYPE I')
            ftp.doCommand(ftp.sendcmd, 'PASV')
            msg = ftp.doCommand(ftp.storbinary, 'STOR ' + theFileName, open(tempFile, 'rb'))
            utils_functions.afficheMessage(main, msg, editLog=True)
            ftp.quit()
        return True
    except:
        return False



###########################################################"
#
#   DIVERSES FONCTIONS UTILES
#
###########################################################"

def int2Int(inInt):
    """
    formate l'affichage d'un entier avec 2 chiffres (pour les jours du mois)
    """
    i = int(inInt)
    if i < 10:
        outInt = '0{0}'.format(i)
    else:
        outInt = inInt
    return outInt

month2Int = {
    'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04',
    'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 
    'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}

def formatFtpList(ftpList):
    """
    remise en forme des retours FTP de la commande LIST.
    Cette commande n'est pas normalisée et dépend du serveur.
    Donc cette procédure ne fonctionnera peut-être pas toujours.
    À adapter en cas de besoin (PB DELAUNAY).
    """
    newList = []
    if len(ftpList) < 1:
        return newList
    # on récupère la longueur des lignes (différents types de serveurs FTP) :
    ftpType = len(ftpList[0].split())
    for line in ftpList:
        parts = line.split()
        if ftpType == 9:
            # FTP standard
            permissions = parts[0]
            size = parts[4]
            month = month2Int[parts[5]]
            day = int2Int(parts[6])
            yearOrTime = parts[7]
            name = parts[8]
            MMdd = '{0}{1}'.format(month, day)
            if yearOrTime.find(':') == -1:
                yyyy = yearOrTime
                hhmm = '0000'
            else:
                # attention au changement d'année :
                yyyy = '{0}'.format(datetime.date.today().year)
                hour, minute = [int2Int(s) for s in yearOrTime.split(':')]
                hhmm = '{0}{1}'.format(hour, minute)
            isDir = False
            if permissions.startswith('d'):
                isDir = True
            newList.append([name, isDir, size, yyyy, MMdd, hhmm])
        elif ftpType == 4:
            # Microsoft FTP Service
            theDate = parts[0]
            theTime = parts[1]
            dirOrSize = parts[2]
            name = parts[3]
            isDir = False
            size = '0'
            if dirOrSize == '<DIR>':
                isDir = True
            else:
                size = dirOrSize
            month, day, year = [s for s in theDate.split('-')]
            if len(year) < 4:
                yyyy = '20{0}'.format(year)
            else:
                yyyy = year
            MMdd = '{0}{1}'.format(month, day)
            hhmm = '0000'
            newList.append([name, isDir, size, yyyy, MMdd, hhmm])
    return newList



###########################################################"
#
#   TÉLÉCHARGEMENT DES FICHIERS PROFS
#
###########################################################"

# ICI LFTP
def ftpGetProfxxDir_LFTP(main):
    if SFTP:
        host = 'sftp://{0}'.format(FTP_HOST)
    else:
        host = 'ftp://{0}'.format(FTP_HOST)
    dirLocal = admin.dirLocalProfxx
    dirSite = admin.dirSitePrive + '/up/files/'
    commandLine = 'lftp -e "open $HOST; user $USER $PASS; set net:max-retries 2; mirror -e $DIRSITE $DIRLOCAL; bye"'
    commandLine = 'DIRLOCAL={0} && DIRSITE={1} && HOST={2} && USER={3} && PASS={4} && {5}'.format(
        dirLocal, 
        dirSite, 
        host, 
        FTP_USER, 
        FTP_PASS, 
        commandLine)
    messages = ['ftpGetProfxxDir_LFTP']
    #messages.append(commandLine)
    #reponse = os.system(commandLine)
    #reponse = subprocess.check_output(commandLine)
    reponse = subprocess.call(commandLine, shell=True)
    messages.append(reponse)
    messages.append('LFTP FAIT')
    utils_functions.afficheMessage(main, messages, editLog=True)

def ftpGetProfxxDir(main, dirTemp, recupLevel=0, idsProfs=[-1]):
    """
    Télécharge les fichiers profs par FTP dans le dossier local dirTemp.
    On teste les dates des fichiers dans la table admin.ftpGetDir pour savoir s'il faut télécharger.
    On met à jour la table admin.ftpGetDir.
    Si recupLevel > RECUP_LEVEL_SIMPLE, on télécharge tous les fichiers.
    Retours :
        * reponse : si ça a marché
        * newFiles : liste des fichiers téléchargés
    """
    try:
        reponse, newFiles = False, {}
        dirSite = admin.dirSitePrive + '/up/files/'
        dirTemp = utils_functions.addSlash(dirTemp)
        msg = QtWidgets.QApplication.translate('main', 'Recovery folder: ') + dirSite
        utils_functions.afficheMessage(main, msg, editLog=True)

        # connexion FTP :
        ftp = GDDFTP(main, FTP_HOST, FTP_USER, FTP_PASS)
        # on se place dans le bon dossier FTP :
        ftp.doCwd(dirSite)
        #print('dirSite :', dirSite)
        # on récupère le contenu :
        ftpList = ftp.doList()
        files = {}
        ftpList = formatFtpList(ftpList)

        lines = []
        profs = ''
        for [profxx, isDir, size, yyyy, MMdd, hhmm] in ftpList:
            #print('LIGNE :', profxx, isDir, size, yyyy, MMdd, hhmm)
            try:
                extension = profxx.split('.')[1]
            except:
                extension = ''
            if extension != 'sqlite':
                continue
            mustDown = False
            id_prof = -1
            for prof in admin.PROFS:
                if profxx == utils_functions.u(prof[1] + '.sqlite'):
                    id_prof = prof[0]
                    profName = utils_functions.u('{0} {1}').format(prof[2], prof[3])
            if id_prof == -1:
                msg = QtWidgets.QApplication.translate('main', 'INCONSISTENT FILE: ')
                msg = utils_functions.u('<b>{0}</b>{1}').format(msg, profxx)
                utils_functions.afficheMessage(main, msg, editLog=True)
                continue
            if (idsProfs == [-1]) or (id_prof in idsProfs):
                messages = [utils_functions.u('<b>{0}</b>').format(profxx), profName]
                mtime = yyyy + MMdd + hhmm
                dateSite = QtCore.QDateTime().fromString(mtime, 'yyyyMMddhhmm')
                # on règle le problème du changement d'année:
                # le fichier ne peut être plus récent qu'aujourd'hui:
                maintenant = QtCore.QDateTime.currentDateTime()
                if maintenant < dateSite:
                    #utils_functions.myPrint('CHANGEMENT ANNEE')
                    yyyy = int(yyyy) - 1
                    mtime = utils_functions.u(yyyy) + MMdd + hhmm
                    dateSite = QtCore.QDateTime().fromString(mtime, 'yyyyMMddhhmm')
                dateSiteLL = utils_functions.toLong(dateSite.toString('yyyyMMddhhmm'))
                # récupération des dates inscrites dans la base:
                dateDB, dateDBLL, dateDBLocal, dateDBLocalLL = admin.getDatesDB(main, profxx)
                # doit-on télécharger :
                if recupLevel > utils.RECUP_LEVEL_SIMPLE:
                    mustDown = True
                else:
                    if dateDBLL < dateSiteLL:
                        mustDown = True
                        messages.append(dateSite.toString('dd-MM-yyyy  hh:mm') + '   (dateSite)')
                        messages.append(dateDB.toString('dd-MM-yyyy  hh:mm') + '   (dateDB)')
                if mustDown:
                    messages.append('DOWNLOAD ' + profxx)
                    # download du fichier:
                    siteFile = dirSite + profxx
                    localFile = dirTemp + profxx
                    msg = ftp.retrbinary('RETR ' + siteFile, open(localFile, 'wb').write)
                    messages.append(msg)
                    if profs == '':
                        profs = '"{0}"'.format(profxx)
                    else:
                        profs = '{0}, "{1}"'.format(profs, profxx)
                    lines.append(('fileProf', profxx, dateSite.toString('yyyyMMddhhmm')))
                    newFiles[profxx] = (id_prof, dateDBLL, dateDBLocalLL, dateSiteLL)
                else:
                    messages.append('INUTILE')
                utils_functions.afficheMessage(main, messages, editLog=True)
        ftp.quit()

        # mise à jour de la base admin :
        if profs != '':
            query_admin, transactionOK = utils_db.queryTransactionDB(admin.db_admin)
            try:
                commandLine = utils_db.qct_admin_ftpGet
                query_admin = utils_db.queryExecute(
                    commandLine, query=query_admin)
                commandLine = utils_db.qdf_admin_ftpGet.format('fileProf', profs)
                query_admin = utils_db.queryExecute(
                    commandLine, query=query_admin)
                commandLine = utils_db.insertInto(
                    'ftpGetDir', utils_db.listTablesAdmin['ftpGetDir'][1])
                query_admin = utils_db.queryExecute({commandLine: lines}, query=query_admin)
            finally:
                utils_db.endTransaction(query_admin, admin.db_admin, transactionOK)
                utils_filesdirs.removeAndCopy(admin.dbFileTemp_admin, admin.dbFile_admin)

        reponse = True
    except:
        utils_functions.afficheMsgPb(main, 'ftpGetProfxxDir')
        reponse = False
    finally:
        return reponse, newFiles



###########################################################"
#
#   ENVOI DES FICHIERS DOCUMENTS
#
###########################################################"

# ICI LFTP
def ftpPutDir(main, dirLocal, dirSite, fileType=''):
    if LFTP:
        return ftpPutDir_LFTP(main, dirLocal, dirSite)
    else:
        return ftpPutDir_OLD(main, dirLocal, dirSite, fileType='')

def ftpPutDir_LFTP(main, dirLocal, dirSite):
    if SFTP:
        host = 'sftp://{0}'.format(FTP_HOST)
    else:
        host = 'ftp://{0}'.format(FTP_HOST)
    commandLine = 'lftp -e "open $HOST; user $USER $PASS; set net:max-retries 2; mirror -e -R $DIRLOCAL $DIRSITE; bye"'
    commandLine = 'DIRLOCAL={0} && DIRSITE={1} && HOST={2} && USER={3} && PASS={4} && {5}'.format(
        dirLocal, 
        dirSite, 
        host, 
        FTP_USER, 
        FTP_PASS, 
        commandLine)
    #print(commandLine)
    messages = ['ftpPutDir_LFTP']
    #messages.append(commandLine)
    #reponse = os.system(commandLine)
    #reponse = subprocess.check_output(commandLine)
    reponse = subprocess.call(commandLine, shell=True)
    messages.append(reponse)
    messages.append('LFTP FAIT')
    utils_functions.afficheMessage(main, messages, editLog=True)
    return True

def ftpPutDir_OLD(main, dirLocal, dirSite, fileType=''):
    """
    Envoi par FTP du dossier local dirLocal vers le dossier distant dirSite.
    Si fileType = 'docProf' ou 'docEleve', c'est qu'on envoie les documents.
    Dans ce cas, on gère la table admin.ftpPutDir.
    Si fileType = '', on se base sur les fichiers présents dans dirLocal.
    """
    try:
        msg = QtWidgets.QApplication.translate('main', 'Updating the folder: ') + dirSite
        utils_functions.afficheMessage(main, msg, editLog=True, p=True)

        useFileType = (fileType != '')
        if useFileType:
            query_admin, transactionOK = utils_db.queryTransactionDB(admin.db_admin)
            commandLine = utils_db.qct_admin_ftpPut
            query_admin = utils_db.queryExecute(
                commandLine, query=query_admin)
            mustUpdateDB = False

        # liste des fichiers déjà sur le site:
        oldFiles = []

        # connexion FTP :
        ftp = GDDFTP(main, FTP_HOST, FTP_USER, FTP_PASS)
        # on se place dans le bon dossier FTP :
        ftp.doCwd(dirSite)
        # on récupère le contenu :
        ftpList = ftp.doList()
        ftpList = formatFtpList(ftpList)
        for [name, isDir, size, yyyy, MMdd, hhmm] in ftpList:
            if isDir or (name in ('.', '..')):
                continue
            oldFiles.append(name)
            mustRemove, mustUp = False, False
            mtime = yyyy + MMdd + hhmm
            msg = utils_functions.u('{0} {1} {2}').format(name, mtime, size)
            utils_functions.afficheMessage(main, msg, editLog=True, p=True)
            dateSite = QtCore.QDateTime().fromString(mtime, 'yyyyMMddhhmm')
            # on règle le problème du changement d'année:
            # le fichier ne peut être plus récent qu'aujourd'hui:
            maintenant = QtCore.QDateTime.currentDateTime()
            if maintenant < dateSite:
                utils_functions.myPrint('PB ANNEE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                yyyy = int(yyyy) - 1
                mtime = utils_functions.u(yyyy) + MMdd + hhmm
                dateSite = QtCore.QDateTime().fromString(mtime, 'yyyyMMddhhmm')
            dateSiteLL = utils_functions.toLong(dateSite.toString('yyyyMMddhhmm'))

            # récupération de dateLocal dans la base (si existe):
            # une date bidon pour un nouveau fichier :
            dateLocalLL = 300010101010
            if useFileType:
                commandLine = utils_db.qs_admin_ftpPut.format(fileType, name)
                query_admin = utils_db.queryExecute(
                    commandLine, query=query_admin)
                while query_admin.next():
                    dateLocalLL = query_admin.value(0)
            dateLocal = QtCore.QDateTime().fromString(utils_functions.u(dateLocalLL), 'yyyyMMddhhmm')

            fileInfo = QtCore.QFileInfo(dirLocal + '/' + name)
            if fileInfo.exists():
                if dateLocalLL < 300010101010:
                    utils_functions.afficheMessage(
                        main, 
                        [dateSite.toString('dd-MM-yyyy  hh:mm') + '   (dateSite)', 
                        dateLocal.toString('dd-MM-yyyy  hh:mm') + '   (dateLocal)'], 
                        editLog=True)
                if dateLocalLL > dateSiteLL:
                    mustUp = True
            else:
                mustRemove = True

            if mustUp:
                utils_functions.afficheMessage(main, 'MUST UP ' + name, editLog=True)
                # upload du fichier:
                siteFile = dirSite + '/' + name
                localFile = dirLocal + '/' + name
                ftp.doCommand(ftp.sendcmd, 'TYPE I')
                ftp.doCommand(ftp.sendcmd, 'PASV')
                msg = ftp.doCommand(ftp.storbinary, 'STOR ' + siteFile, open(localFile, 'rb'))
                utils_functions.afficheMessage(main, msg, editLog=True)
                if useFileType:
                    mustUpdateDB = True
            elif mustRemove:
                utils_functions.afficheMessage(main, 'MUST REMOVE ' + name, editLog=True)
                # suppression du fichier:
                siteFile = dirSite + '/' + name
                msg = ftp.doCommand(ftp.delete, siteFile)
                utils_functions.afficheMessage(main, msg, editLog=True)
                # mise à jour de la base :
                if useFileType:
                    commandLine = utils_db.qdf_admin_ftpPut.format(fileType, name)
                    query_admin = utils_db.queryExecute(
                        commandLine, query=query_admin)
                    mustUpdateDB = True
            else:
                utils_functions.afficheMessage(main, 'INUTILE', editLog=True)

        # on cherche les nouveaux fichiers:
        filesList = QtCore.QDir(dirLocal).entryList(
            ['*'], QtCore.QDir.Files, QtCore.QDir.Name)
        for name in filesList:
            if not(name in oldFiles):
                utils_functions.afficheMessage(main, [name, 'NEW FILE'], editLog=True, p=True)
                # upload du fichier:
                siteFile = dirSite + '/' + name
                localFile = dirLocal + '/' + name
                ftp.doCommand(ftp.sendcmd, 'TYPE I')
                ftp.doCommand(ftp.sendcmd, 'PASV')
                msg = ftp.doCommand(ftp.storbinary, 'STOR ' + siteFile, open(localFile, 'rb'))
                utils_functions.afficheMessage(main, msg, editLog=True)
                mustUpdateDB = True

        # on doit ensuite récupérer les nouvelles dates du site,
        # pour mettre la base à jour:
        if useFileType and mustUpdateDB:
            utils_functions.afficheMessage(main, 'UPDATING DATABASE', editLog=True, p=True)
            # on récupère à nouveau le contenu (il a changé) :
            ftpList = ftp.doList()
            ftpList = formatFtpList(ftpList)
            lines = []
            for [name, isDir, size, yyyy, MMdd, hhmm] in ftpList:
                if isDir or (name in ('.', '..')):
                    continue
                mtime = yyyy + MMdd + hhmm
                dateSite = QtCore.QDateTime().fromString(mtime, 'yyyyMMddhhmm')
                # on règle le problème du changement d'année:
                # le fichier ne peut être plus récent qu'aujourd'hui:
                maintenant = QtCore.QDateTime.currentDateTime()
                if maintenant < dateSite:
                    yyyy = int(yyyy) - 1
                    mtime = utils_functions.u(yyyy) + MMdd + hhmm
                    dateSite = QtCore.QDateTime().fromString(mtime, 'yyyyMMddhhmm')
                dateSite = dateSite.toString('yyyyMMddhhmm')

                # mise à jour de la base:
                commandLine = utils_db.qdf_admin_ftpPut.format(fileType, name)
                query_admin = utils_db.queryExecute(
                    commandLine, query=query_admin)
                lines.append((fileType, name, dateSite))
            commandLine = utils_db.insertInto(
                'ftpPutDir', utils_db.listTablesAdmin['ftpPutDir'][1])
            query_admin = utils_db.queryExecute(
                {commandLine: lines}, query=query_admin)

        ftp.quit()
        return True
    except:
        utils_functions.afficheMsgPb(main)
        return False
    finally:
        if useFileType:
            utils_db.endTransaction(query_admin, admin.db_admin, transactionOK)
            utils_filesdirs.removeAndCopy(admin.dbFileTemp_admin, admin.dbFile_admin)



