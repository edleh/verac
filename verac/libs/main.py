# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Contient l'interface graphique.
    La plupart des actions renvoient à des fonctions 
    qui sont dans les modules :
        prof, admin et les utils_aaa
"""

# importation des modules utiles :
from __future__ import division, print_function
import sys

# importation des modules perso :
import utils, utils_functions, utils_webengine, utils_db
import prof, prof_db
import utils_help, utils_web, utils_filesdirs
import utils_markdown, utils_connect
if utils.PYTHONVERSION >= 30:
    import utils_spell

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    from PyQt5 import QtNetwork
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4 import QtNetwork




class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, HERE, splash, FIRST, parent=None):
        """
        mise en place de l'interface
        plus des variables utiles
        """
        utils_functions.afficheMessage(self, '__init__', console=True)
        QtWidgets.QMainWindow.__init__(self, parent)

        # initialisation au lancement 
        # (dont la première connexion aux bases users et commun) :
        self.initialisation = True
        if utils.MODEBAVARD:
            print('locale:', utils.LOCALE)
        self.initSettings(HERE, FIRST)
        utils.loadSupportedImageFormats()
        utils_functions.afficheTime(self, 'initSettings')

        # connexion
        # (dont la deuxième connexion aux bases users et commun) :
        self.connectUser(splash)
        utils_functions.afficheTime(self, 'connectUser')
        splash.show()

        # mise en place de l'interface :
        self.createTranslations()
        self.createInterface()
        self.createActions()
        utils_functions.afficheTime(self, 'interface')

        # fin de la configuration
        # (dont la troisième connexion aux bases users 
        # et commun pour l'admin) :
        self.closing = False
        self.endSettings()
        utils_functions.afficheTime(self, 'endSettings')

        #self.setMinimumWidth(400)
        if utils.MODEBAVARD:
            rect = QtWidgets.QApplication.desktop().availableGeometry()
            self.resize(int(3 * rect.width() / 4), int(3 * rect.height() / 4))
        utils_functions.afficheTime(self, 'fin de __init__')
        splash.finish(self)

        # bug Windows qui déclenche des KeyPressEvent sans raison :
        self.noKeyPressEvent = False

        self.initialisation = False
        if self.mustDownloadBaseMy:
            prof_db.downloadBase_my(self, verif=False, msgFin=False)



    ###########################################
    # Gestion de la config :
    ###########################################

    def initSettings(self, HERE, FIRST):
        """
        au lancement, on définit les variables, les chemins, etc
        On ne sait pas encore qui sera l'utilisateur.
        """
        utils_functions.afficheMessage(self, 'initSettings', console=True)

        # pour faire des tests de durée 
        # (voir la fonction utils_functions.afficheTime) :
        self.timeDepart = QtCore.QTime.currentTime()
        self.timePrecedent = self.timeDepart

        # quelques renseignements pour le fichier log (en cas de bug) :
        osVersion = 'osVersion : {0}{1}'.format(
            utils.OS_NAME[0], utils.OS_NAME[1])
        pythonVersionNumber = 'pythonVersionNumber : {0}'.format(
            utils.PYTHONVERSION)
        pythonVersion = 'pythonVersion : ' + sys.version
        qtVersion = 'qtVersion : ' + utils.qtVersion
        utils_functions.afficheMessage(
            self, 
            [
                osVersion, 
                pythonVersionNumber, 
                pythonVersion, 
                qtVersion, 
                utils.PYQT], 
            console=True)

        # i18n :
        utils.initTranslations(self)

        # initialisation des bases de données:
        self.dbFile_users, self.dbFileTemp_users, self.db_users = '', '', None
        self.dbFile_commun, self.dbFileTemp_commun, self.db_commun = '', '', None
        self.dbFile_my, self.dbName_my, self.db_my = '', '', None
        self.configDict = {}
        self.versionDBMyIsOk = True

        # le dossier des fichiers etc :
        try:
            utils_functions.afficheMessage(
                self, 
                utils_functions.u(
                    'HERE : {0}').format(utils_functions.u(HERE)))
        except:
            pass
        # dossier du logiciel :
        self.beginDir = QtCore.QDir.currentPath()
        # dossier de travail (pour enregistrer, ouvrir, etc) :
        self.workDir = QtCore.QDir.homePath()
        # dossier des fichiers profs (profxx.sqlite) :
        self.filesDir = QtCore.QDir.homePath()

        # récupération de la version de VÉRAC :
        utils.changeProgVersion(self)
        utils_functions.afficheMessage(
            self, 
            utils_functions.u(
                'PROGVERSION : {0}').format(utils.PROGVERSION),
            console=True)

        # le programme est-il déjà lancé ?
        if utils_filesdirs.testLockFile(self) == False:
            sys.exit(1)

        # création du dossier temporaire et de ses sous-dossiers :
        self.tempPath = utils_filesdirs.createTempAppDir(
            utils.PROGLINK + '-temp')
        utils_functions.afficheMessage(
            self, 
            utils_functions.u('self.tempPath: {0}').format(self.tempPath))
        utils_filesdirs.createDirs(self.tempPath, 'down')
        utils_filesdirs.createDirs(self.tempPath, 'up')

        # La base config.
        # ----------------------------------------------------------
        # À priori, elle est dans le home.
        # Mais dans le cas d'une installation réseau, 
        # elle peut être en partie déportée :
        #   * une partie commune en lecture seule (db_lanConfig)
        #   * une partie liée au poste (db_localConfig) qui existe 
        #       toujours et reste dans le home
        # Enfin, toujours pour un réseau, on garde 2 possibilités 
        # qui permettent d'installer verac dans le réseau 
        # ou sur chaque poste :
        #   * verac lancé en réseau :
        #     - si un dossier "lanConfig" est placé à côté de l'install,
        #       il contient forcément db_lanConfig
        #       on a donc lanConfig/.Verac à côté du dossier verac.
        #     - si un dossier "files" est placé à côté de l'install,
        #       il contiendra les fichiers profxx.sqlite.
        #       C'est donc lanFilesDir (variable globale dans utils).
        #       Le dossier "files" doit être autorisé en 
        #       écriture pour les profs.
        #     - si l'un des dossiers ci-dessus n'existe pas,
        #       on cherchera si son chemin existe dans db_localConfig
        #       (db_localConfig pour garder la possibilité d'avoir 
        #       un réseau hétérogène,
        #       mais il faudra adapter à chaque type de poste)
        #   * verac est installé sur chaque poste :
        #     on ouvre db_localConfig (dans home/.Verac ou home/.config/Verac)
        #     on y lit les chemins vers les dossiers s'ils y sont définis
        # Enfin il y a le cas d'une année archivée. 
        #     On ne prend que ce qui est  à côté du logiciel. 
        # ----------------------------------------------------------
        # Dans tous les cas (sauf archive ou sera redéfini), 
        # localConfig est dans le home :
        self.localConfigDir = utils_filesdirs.createConfigAppDir(
            utils.PROGLINK, beginDir=self.beginDir).canonicalPath()
        dbFile_localConfig = self.localConfigDir + '/config.sqlite'
        self.db_localConfig = None
        # S'il y a un dossier lanConfig à côté du logiciel,
        # on est forcément en réseau :
        self.db_lanConfig = None
        theDir = self.beginDir + '/../lanConfig'
        if QtCore.QDir(theDir).exists():
            lanConfigDir = QtCore.QDir(theDir).canonicalPath() + '/.Verac'
            # on a touvé db_lanConfig et on s'y connecte dans temp :
            utils.changeLanConfig(True, lanConfigDir, changeInitial=True)
            dbFile_lanConfig = lanConfigDir + '/config.sqlite'
            fileTemp = utils_functions.u(
                '{0}/lanConfig.sqlite').format(self.tempPath)
            utils_filesdirs.removeAndCopy(dbFile_lanConfig, fileTemp)
            self.db_lanConfig = utils_db.createConnection(
                self, fileTemp, 'lanConfig')[0]
            utils.changeInstallType(utils.INSTALLTYPE_LAN)
        # S'il y a un dossier files à côté du logiciel,
        # on est forcément en réseau :
        theDir = self.beginDir + '/../files'
        if QtCore.QDir(theDir).exists():
            theDir = QtCore.QDir(theDir).canonicalPath()
            utils.changeLanFilesDir(theDir, changeInitial=True)
            utils.changeInstallType(utils.INSTALLTYPE_LAN)
            self.filesDir = theDir
        # Si on est pas en réseau, on lance peut-être une archive :
        if utils.installType != utils.INSTALLTYPE_LAN:
            theDir = self.beginDir + '/../.Verac'
            if QtCore.QDir(theDir).exists():
                theDir = QtCore.QDir(theDir).canonicalPath()
                if (theDir != self.localConfigDir):
                    # on teste s'il y a un dossier verac_admin 
                    # (archive admin) :
                    theDirArchive = self.beginDir + '/../verac_admin'
                    if QtCore.QDir(theDirArchive).exists():
                        utils.changeInstallType(utils.INSTALLTYPE_ARCHIVE)
                    else:
                        # sinon s'il y a un profxx.sqlite :
                        entries = QtCore.QDir(
                            self.beginDir + '/..').entryInfoList(
                                QtCore.QDir.NoDotAndDotDot | QtCore.QDir.Files)
                        for entryInfo in entries:
                            path = entryInfo.absoluteFilePath()
                            if entryInfo.isFile():
                                if entryInfo.suffix() == 'sqlite':
                                    utils.changeInstallType(
                                        utils.INSTALLTYPE_ARCHIVE)
        if utils.installType == utils.INSTALLTYPE_ARCHIVE:
            # donc c'est bien une archive qui est lancée :
            self.filesDir = self.beginDir + '/..'
            self.localConfigDir = theDir
            dbFile_localConfig = self.localConfigDir + '/config.sqlite'
            # on se connecte à db_localConfig :
            self.db_localConfig = utils_db.createConnection(
                self, dbFile_localConfig, 'localConfig')[0]
        else:
            # si on ne lance pas pour la première fois :
            if not(FIRST):
                # on se connecte à db_localConfig :
                fileTemp = utils_functions.u(
                    '{0}/localConfig.sqlite').format(self.tempPath)
                if utils_filesdirs.removeAndCopy(dbFile_localConfig, fileTemp):
                    self.db_localConfig = utils_db.createConnection(
                        self, fileTemp, 'localConfig')[0]
                else:
                    # config foirée :
                    FIRST = True
            # si on lance pour la première fois :
            if FIRST:
                QtCore.QFile(
                    self.beginDir + '/files/config.sqlite').copy(
                        dbFile_localConfig)
                # on se connecte à db_localConfig :
                fileTemp = utils_functions.u(
                    '{0}/localConfig.sqlite').format(self.tempPath)
                utils_filesdirs.removeAndCopy(dbFile_localConfig, fileTemp)
                self.db_localConfig = utils_db.createConnection(
                    self, fileTemp, 'localConfig')[0]
                utils_db.changeInConfigTable(
                    self, 
                    self.db_localConfig, 
                    {'FilesDir': ('', self.filesDir)})
                # on crèe les versions perso et demo :
                utils_filesdirs.createDirs(self.localConfigDir, 'perso')
                QtCore.QFile(
                    self.beginDir + '/files/perso/users.sqlite').copy(
                        self.localConfigDir + '/perso/users.sqlite')
                QtCore.QFile(
                    self.beginDir + '/files/perso/commun.sqlite').copy(
                        self.localConfigDir + '/perso/commun.sqlite')
                utils_filesdirs.createDirs(self.localConfigDir, 'demo')
                QtCore.QFile(
                    self.beginDir + '/files/demo/users.sqlite').copy(
                        self.localConfigDir + '/demo/users.sqlite')
                QtCore.QFile(
                    self.beginDir + '/files/demo/commun.sqlite').copy(
                        self.localConfigDir + '/demo/commun.sqlite')
                # on sélectionne a priori la version perso :
                utils_db.changeInConfigTable(
                    self, 
                    self.db_localConfig, 
                    {'actualVersion': (0, '')})
            # si le dossier lanFilesDir n'existe pas, on regarde
            # s'il est défini (dans lanConfig ou localConfig) :
            if utils.lanFilesDir == '':
                theDir = utils_db.readInConfigDB(self, 'lanFilesDir')[1]
                if theDir != '':
                    if QtCore.QDir(theDir).exists():
                        utils.changeLanFilesDir(theDir, changeInitial=True)
                        self.filesDir = theDir
            # on cherche si lanWorkDir est défini 
            # (dans lanConfig ou localConfig) :
            theDir = utils_db.readInConfigDB(self, 'lanWorkDir')[1]
            if theDir != '':
                if QtCore.QDir(theDir).exists():
                    utils.changeLanWorkDir(theDir, changeInitial=True)
                    self.workDir = theDir
        # en attendant mieux :
        utils_functions.afficheMessage(
            self, 'installType : {0}'.format(utils.installType))
        utils_functions.afficheMessage(
            self, 
            utils_functions.u(
                'localConfigDir : {0}').format(self.localConfigDir))
        utils_functions.afficheMessage(
            self, 
            utils_functions.u(
                'lanConfigDir : {0}').format(utils.lanConfigDir))
        utils_functions.afficheMessage(
            self, 
            utils_functions.u(
                'lanFilesDir : {0}').format(utils.lanFilesDir))
        utils_functions.afficheMessage(
            self, 
            utils_functions.u('filesDir : {0}').format(self.filesDir))
        utils_functions.afficheMessage(
            self, 
            utils_functions.u('lanWorkDir : {0}').format(utils.lanWorkDir))
        utils_functions.afficheMessage(
            self, 
            utils_functions.u('workDir : {0}').format(self.workDir))
        utils_functions.myPrint(utils_db.sqlDatabase.connectionNames())

        # redirection des sorties console vers le fichier de log :
        useLogFile = utils_db.readInConfigDB(self, 'logFile')[0]
        if useLogFile < 0:
            utils.changeLogFile(False)

        # on lit la version à préselectionner lors de la connexion :
        self.actualVersion = utils_connect.readActualVersion(self)

        # première définition des bases users et commun :
        if utils.lanConfigDir != '':
            theDir = utils.lanConfigDir
        else:
            theDir = self.localConfigDir
        # On utilise les bases users et commun dans temp
        # (risque de problème en cas de réseau) :
        self.dbFile_users = theDir + '/' + self.actualVersion['versionName'] + '/users.sqlite'
        self.dbFileTemp_users = self.tempPath + '/users.sqlite'
        utils_filesdirs.removeAndCopy(self.dbFile_users, self.dbFileTemp_users)
        self.db_users = utils_db.createConnection(self, self.dbFileTemp_users)[0]
        self.dbFile_commun = theDir + '/' + self.actualVersion['versionName'] + '/commun.sqlite'
        self.dbFileTemp_commun = self.tempPath + '/commun.sqlite'
        utils_filesdirs.removeAndCopy(
            self.dbFile_commun, self.dbFileTemp_commun)
        self.db_commun = utils_db.createConnection(
            self, self.dbFileTemp_commun)[0]
        # lecture des paramètres de l'établissement 
        # depuis commun.config :
        config = {}
        query_commun = utils_db.query(self.db_commun)
        commandLine_commun = utils_db.q_selectAllFrom.format('config')
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            config[query_commun.value(0)] = (
                query_commun.value(1), query_commun.value(2))
        self.siteUrlBase = config.get('siteUrlBase', (0, ''))[1]
        self.siteUrlPublic = config.get('siteUrlPublic', (0, ''))[1]
        # au cas où commun.sqlite aurait été massacrée :
        if self.siteUrlPublic == '':
            self.siteUrlPublic = self.actualVersion['versionUrl']
        self.siteUrlPublic = utils_functions.removeSlash(
            self.siteUrlPublic)
        self.limiteBLTPerso = config.get('limiteBLTPerso', (4, ''))[0]
        self.annee_scolaire = config.get('annee_scolaire', (0, ''))
        if self.annee_scolaire[0] < 1:
            self.annee_scolaire = utils_functions.calcAnneeScolaire()

        # pour définir les paramètres de proxy
        value_int = utils_db.readInConfigDB(self, 'isProxy')[0]
        if (value_int == 1):
            (proxyPort, hostName, newKey) = utils_db.readInConfigDB(
                self, 'ParamProxy')
            proxy = QtNetwork.QNetworkProxy()
            proxy.setType(QtNetwork.QNetworkProxy.HttpProxy)
            #proxy.setType(QtNetwork.QNetworkProxy.Socks5Proxy)
            proxy.setHostName(hostName)
            proxy.setPort(proxyPort)
            QtNetwork.QNetworkProxy.setApplicationProxy(proxy)

        # pour savoir si l'ordi est connecté à internet :
        if utils.installType == utils.INSTALLTYPE_ARCHIVE:
            utils_functions.afficheMessage(self, 'ARCHIVE')
            self.NetOK = False
        else:
            self.NetOK = True
        # délai de test de connexion au site web :
        value_int = utils_db.readInConfigDB(
            self, 
            'testNetDelay', 
            default_int=utils.testNetDelay_initial)[0]
        utils.changeTestNetDelay(value_int)

        # Est-t'on admin ?
        adminDir = utils_db.readInConfigTable(self.db_localConfig, self.actualVersion['versionName'])[1]
        self.adminOk = (adminDir != '')

        # lors des modifications (évaluations, appréciations), il faudra relancer les calculs
        # des bilans et autres.
        # le dictionnaire whatIsModified sert à ne pas faire trop de calculs inutiles
        # la première sous-liste contient les id_eleve, la deuxième les id_item
        # et la variable actualTableau sert à savoir si le tableau actuel a été modifié :
        self.whatIsModified = {'id_eleves': [], 'id_items': [], 'actualTableau': False}

        # self.me contiendra les renseignement sur l'utilisateur connecté :
        self.me = {
            'userMode': '', 
            'userName': '', 
            'userId': -1, 
            'user': '', 
            'Mdp': '', 
            'Matiere': '', 
            'other': '', 
            'localDateTime': -1, 
            'netDateTime': -1, 
            }

        # page d'aide contextuelle (le sommaire au départ) :
        self.helpContext = ('', '')
        # si téléchargement initial de la base perso demandé :
        self.mustDownloadBaseMy = False

    def connectUser(self, splash):
        """
        On lance le dialogue de connexion
        """
        self.mustChangeMdp = False
        utils_functions.afficheMessage(self, 'connectUser')
        dialog = utils_connect.ConnectDlg(parent=self, adminOk=self.adminOk)
        splash.hide()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # le titre de la fenêtre :
            self.windowTitleBase = utils_functions.u('{0}  {1}  [{2}]  [{3}]  ').format(
                utils.PROGNAME, 
                utils.PROGVERSION, 
                self.actualVersion['versionName'], 
                self.me['userName'])
            self.adminOk = dialog.adminOk
            self.db_myIsModified = False
            self.suivisIsModified = False
            self.viewType = utils.VIEW_ITEMS
            self.changeDBMyState(isModified=False, force=True)
            utils.changePeriodes(self)
            if self.me['userMode'] != 'admin':
                self.me['MATIERES'] = utils_functions.loadMatieres(self)
                if self.me['userId'] >= utils.decalageChefEtab:
                    self.me['userMode'] = 'director'
                elif self.me['userId'] >= utils.decalagePP:
                    self.me['userMode'] = 'PP'
                elif self.me['Matiere'] == self.me['MATIERES']['Code2Matiere']['VS'][1]:
                    self.me['userMode'] = 'VS'
        else:
            utils_filesdirs.deleteLockFile()
            sys.exit(1)

    def endSettings(self):
        """
        On sait maintenant qui est connecté et quelle version est lancée.
        On termine la configuration en en tenant compte.
        Le dico messagesToDisplay sert à repérer les messages 
        d'avertissement à afficher,
        pour finalement n'en afficher qu'un seul (le plus important).
        """
        utils_functions.afficheMessage(self, 'endSettings', console=True)
        messagesToDisplay = {
            'SCHOOL_YEAR': '',
            'SIMPLIFIED_UI': '',
            'MUST_DOWNLOAD': ''}

        # si un prof se connecte, on retrouve sa base ou on la crée :
        if self.me['userMode'] != 'admin':
            if QtCore.QFileInfo(self.dbFile_my).exists():
                dbFile_myTemp = utils_functions.u('{0}/{1}.sqlite').format(
                    self.tempPath, self.dbName_my)
                utils_filesdirs.removeAndCopy(self.dbFile_my, dbFile_myTemp)
                db_myTemp = utils_db.createConnection(self, dbFile_myTemp)[0]
                self.db_my = utils_db.copyDBMy(self, dbFile_myTemp, 'db_memory')
                db_myTemp.close()
                del db_myTemp
                utils_db.sqlDatabase.removeDatabase(self.dbName_my)
            else:
                self.db_my = utils_db.createConnection(self, 'db_memory')[0]
                prof_db.createBase_my(self)
            message = self.reloadConfigDict(showMessage=False)
            messagesToDisplay['SCHOOL_YEAR'] = message
            self.doSwitchInterface()
            self.createMenus()
            self.createToolBar()
            # message d'avertissement si UI simplifiée :
            if self.actionSwitchInterface.isChecked():
                m1 = QtWidgets.QApplication.translate(
                    'main', 'VERAC will be displayed in a simplified interface.')
                m2 = QtWidgets.QApplication.translate(
                    'main', 'It presents the actions needed to get started.')
                m3= QtWidgets.QApplication.translate(
                    'main', 
                    'Remember to enable the complete interface '
                    'once you are more comfortable with the software.')
                message = utils_functions.u(
                    '<p align="center">{0}<br/>{1}<br/><br/>{2}</p>').format(m1, m2, m3)
                if message != '':
                    messagesToDisplay['SIMPLIFIED_UI'] = message
        else:
            self.createMenus()
            self.createToolBar()

        # certaines actions ne sont utilisables que dans certaines versions :
        actionsToDisable = []
        # pas d'internet ou version perso :
        if not(self.NetOK) or (self.actualVersion['versionName'] == 'perso'):
            if self.me['userMode'] != 'admin':
                actionsToDisable.extend([
                    self.actionPostDBMy,
                    self.actionSaveAndPostDBMy,
                    self.actionCompareDBMy,
                    self.actionDownloadDBMy,
                    self.actionTestUpdateDBUsersCommun,
                    self.actionDownloadSuivisDB,
                    self.actionPostSuivis])
                if not(self.NetOK):
                    actionsToDisable.append(self.actionCreateGroupTrombinoscope)
            self.siteUrlBase = utils_db.readInConfigTable(
                self.db_commun, 'siteUrlBase')[1]
            if  (self.actualVersion['versionName'] == 'perso') and (self.me['userMode'] != 'admin'):
                actionsToDisable.append(self.actionChangeMdp)
            actionsToDisable.append(self.actionGotoSiteEtab)
            if self.me['userMode'] == 'admin':
                # les actions à désactiver pour up/downloader en admin :
                actionsToDisable.extend([
                    self.actionUploadDBCommun_admin,
                    self.actionDownloadDBUsers_admin,
                    self.actionUploadDBUsers_admin,
                    self.actionUploadDBResultats_admin,
                    ])
        # à désactiver uniquement en mode perso + admin :
        if (self.actualVersion['versionName'] == 'perso') and self.me['userMode'] == 'admin':
            actionsToDisable.extend([
                self.actionLaunchScheduler_admin,
                self.actionCreateConnectionFile_admin,
                self.actionShowMdpProfsNoChange_admin,
                self.actionUpdateBilansSelect_admin,
                #self.actionDocumentsManagement_admin,
                self.actionVerifProfsFiles_admin,
                self.actionCheckSchoolReports_admin,
                self.actionWebSiteConfig,
                self.actionWebSiteState,
                self.actionTestUpdateWebSite,
                self.actionUpdateWebSite,
                self.actionShowCompteurAnalyze_admin,
                self.actionGestStructuresProfs_admin,
                self.actionRedoMdp_admin,
                ])
        # en réseau, seul l'admin pourra mettre VÉRAC à jour :
        if (utils.installType == utils.INSTALLTYPE_LAN) and (self.me['userMode'] != 'admin'):
            actionsToDisable.append(self.actionUpdateVerac)
        # on désactive les actions repérées :
        for action in actionsToDisable:
            action.setEnabled(False)

        # différence entre les interfaces prof et admin :
        if self.me['userMode'] != 'admin':
            # on lit la configuration choisie pour les calculs :
            utils_functions.readConfigCalculs(self)
            # les périodes protégées :
            prof_db.initProtectedPeriods(self)
            self.initPeriodeMenu()
            # quelques variables :
            self.id_tableau = -1
            self.id_groupe = -1
            self.public = 1
            self.tableauxInCompil = []
            self.tableauxForItem = {}
            # upgrade de la base prof :
            import utils_upgrades
            self.versionDBMyIsOk = utils_upgrades.upgradeProf(self)
            if not(self.versionDBMyIsOk):
                self.actionAutoSaveDBMy.setChecked(False)
                actionsToDisable = (
                    self.actionSaveDBMy,
                    self.actionPostDBMy,
                    self.actionSaveAsDBMy,
                    self.actionAutoSaveDBMy,
                    self.actionSaveAndPostDBMy,
                    self.actionCompareDBMy,
                    self.actionDownloadDBMy,
                    self.actionTestUpdateDBUsersCommun,
                    self.actionDownloadSuivisDB,
                    self.actionPostSuivis)
                for action in actionsToDisable:
                    action.setEnabled(False)

            # on met en place le ProfTableView :
            self.model = None
            self.view = prof.ProfTableView(self)
            self.view.setHorizontalHeader(prof.MyHeaderView(self))
            self.view.setModel(self.model)

            # pour éditer les appréciations dans la vue élève :
            # on utilise un QScrollArea 
            # et un accordéon pour les appréciations précédentes
            self.editAppreciationGroupBox = QtWidgets.QGroupBox()
            scroll = QtWidgets.QScrollArea()
            dummy = QtWidgets.QWidget()
            vLayout = QtWidgets.QVBoxLayout()
            self.accordion = {}
            for p in range(1, utils.NB_PERIODES):
                self.accordion[p] = [utils.PERIODES[p], None, None]
                # un chechBox pour chaque période :
                checkBox = QtWidgets.QCheckBox(self.accordion[p][0])
                checkBox.stateChanged.connect(self.doShowAccordion)
                self.accordion[p][1] = checkBox
                vLayout.addWidget(checkBox)
                # un textEdit en ReadOnly pour l'appréciation :
                textEdit = QtWidgets.QTextEdit()
                textEdit.setReadOnly(True)
                textEdit.setVisible(False)
                self.accordion[p][2] = textEdit
                vLayout.addWidget(textEdit)
            vLayout.addStretch()
            dummy.setLayout(vLayout)
            dummy.setMinimumWidth(300)
            dummy.setMinimumHeight(utils.NB_PERIODES * 100)
            scroll.setWidget(dummy)
            scroll.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
            # l'appréciation en cours dans un SpellTextEdit :
            if utils.PYTHONVERSION >= 30:
                self.editAppreciation = utils_spell.SpellTextEdit(self)
            else:
                self.editAppreciation = QtWidgets.QTextEdit(self)
            self.editAppreciation.textChanged.connect(self.doAppreciationChanged)
            # un splitter horizontal pour jouer avec l'affichage :
            hsplitter = QtWidgets.QSplitter(self)
            hsplitter.setOrientation(QtCore.Qt.Horizontal)
            hsplitter.addWidget(self.editAppreciation)
            hsplitter.addWidget(scroll)
            # on partage l'espace disponible entre les 2 :
            desktopWidth = QtWidgets.QApplication.desktop().availableGeometry().width()
            hsplitter.setSizes([int(desktopWidth * 0.6), int(desktopWidth * 0.4)])
            hLayout = QtWidgets.QHBoxLayout()
            hLayout.addWidget(hsplitter)
            self.editAppreciationGroupBox.setLayout(hLayout)
            dummy.setMinimumWidth(int(desktopWidth * 0.4 - 70))
            hsplitter.splitterMoved.connect(self.accordionSplitterMoved)

            # on agence tout ça avec un deuxième splitter vertical :
            vsplitter = QtWidgets.QSplitter(self)
            vsplitter.setOrientation(QtCore.Qt.Vertical)
            vsplitter.addWidget(self.view)
            vsplitter.addWidget(self.editAppreciationGroupBox)
            # on partage l'espace disponible :
            desktopHeight = QtWidgets.QApplication.desktop().availableGeometry().height()
            vsplitter.setSizes([int(desktopHeight * 0.8), int(desktopHeight * 0.2)])

            # un WebView pour afficher l'aide contextuelle :
            self.helpWebView = utils_webengine.MyWebEngineView(self)

            # le stackedWidget et la topBar :
            self.stackedWidget = QtWidgets.QStackedWidget()
            self.stackedWidget.addWidget(vsplitter)
            self.stackedWidget.addWidget(self.helpWebView)
            if self.NetOK:
                # pour visiter le site web de l'établissement :
                self.siteWebView = utils_webengine.MyWebEngineView(self)
                self.stackedWidget.addWidget(self.siteWebView)
            vBoxLayout = QtWidgets.QVBoxLayout()
            vBoxLayout.setContentsMargins(0, 0, 0, 0)
            vBoxLayout.addWidget(self.topBar)
            vBoxLayout.addWidget(self.stackedWidget)
            groupBox = QtWidgets.QGroupBox()
            groupBox.setLayout(vBoxLayout)
            groupBox.setFlat(True)
            self.setCentralWidget(groupBox)

            # on place les barres d'outils :
            self.doReloadLayout(what=self.actualToolBarState)

            utils_functions.restoreCursor()

            # affichage ou non des élèves supprimés des groupes :
            mustShowDeletedEleves = utils_db.readInConfigDict(
                self.configDict, 'showDeletedEleves')[0]
            if mustShowDeletedEleves == 1:
                self.groupeMenuActions = []
                self.actionShowDeletedEleves.setChecked(True)
                self.doShowDeletedEleves()
                self.changeDBMyState(isModified=False)

            # on initialise la sélection.
            self.lastTableauForSelection = {}
            lastTableaux = utils_db.readInConfigDict(
                self.configDict, 'lastTableauForSelection')[1]
            lastTableaux = lastTableaux.split('#')
            for lastTableauText in lastTableaux:
                lastTableau = lastTableauText.split('|')
                if len(lastTableau) > 2:
                    self.lastTableauForSelection[
                        int(lastTableau[0]), int(lastTableau[1])] = int(lastTableau[2])
            # Pour une première connexion, on place sur la première période :
            firstPeriod = 1
            if utils.NB_PERIODES < 2:
                firstPeriod = 0
            selectedPeriod = utils_db.readInConfigDict(
                self.configDict, 'SelectedPeriode', default_int=firstPeriod)[0]
            utils.changeSelectedPeriod(selectedPeriod)
            self.updatePeriodeMenu(selectedPeriod)
            self.id_groupe = utils_db.readInConfigDict(
                self.configDict, 'SelectedGroupe', default_int=-1)[0]
            import prof_groupes
            prof_groupes.initGroupeMenu(self)
            selectedTableau = utils_db.readInConfigDict(
                self.configDict, 'SelectedTableau')[0]
            prof.tableauxInit(self, idTableauToSelect=selectedTableau, mustReloadView=False)
            self.changeValueInit()
            selectedView = utils_db.readInConfigDict(
                self.configDict, 'SelectedView')[0]
            self.doShow(viewType=selectedView)

            # on inscrit l'état du mot de passe (pour l'admin ; sécurité)
            if self.mustChangeMdp:
                utils_connect.mustChangeMdp(self)
                utils_db.changeInConfigTable(
                    self, self.db_my, {'mustChangeMdp': (1, '')})
            else:
                utils_db.changeInConfigTable(
                    self, self.db_my, {'mustChangeMdp': (0, '')})
            # détection de la touche Ctrl (pour le presse-papier) :
            self.doClipBoard = False
            # affichage des nouveautés ou d'une page d'aide aléatoire :
            #   * pas en cas de premier lancement
            #   * forcé après un upgrade de VÉRAC
            #   * aléatoire sinon
            mustShowNews = utils_db.readInConfigTable(
                self.db_localConfig, 'mustShowNews')[0]
            if len(self.groupeMenuActions) > 1:
                if mustShowNews == 1:
                    utils_db.changeInConfigTable(
                        self, self.db_localConfig, {'mustShowNews': (0, '')})
                    self.doShow(viewType=utils.VIEW_NEWS)
                else:
                    import random
                    if random.randint(0, 5) == 0:
                        self.doShow(viewType=utils.VIEW_RANDOM_HELP)
            # la base distante est-elle plus récente ?
            if self.NetOK:
                # on compare les dates avant pour prévenir les erreurs :
                comparaison = prof_db.compareDatesBases_my(self)
                # affichage du message final :
                if comparaison['whoIsMostRecent'] == 'REMOTE':
                    m1 = QtWidgets.QApplication.translate('main', 'localDate')
                    m2 = QtWidgets.QApplication.translate('main', 'netDate')
                    whoIsMostRecentMsg = QtWidgets.QApplication.translate(
                        'main', 'The distant file seems most recent.')
                    m3 = QtWidgets.QApplication.translate(
                        'main', 'You should download it.')
                    m4 = QtWidgets.QApplication.translate(
                        'main', '("File > DownloadDBMy" menu)')
                    message = utils_functions.u(
                        '<p align="center">{0}</p>'
                        '<p><b>{5}<br/>{6}</b><br/>{7}</p>'
                        '<p></p>'
                        '<table border="0">'
                        '<tr><td>{1} : </td><td><b>{2}</b></td></tr>'
                        '<tr><td>{3} : </td><td><b>{4}</b></td></tr>'
                        '</table>').format(
                            utils.SEPARATOR_LINE, 
                            m1, comparaison['localDateTimeStr'], m2, comparaison['netDateTimeStr'], 
                            whoIsMostRecentMsg, m3, m4)
                    messagesToDisplay['MUST_DOWNLOAD'] = message

            # on affiche seulement le message le plus important :
            if messagesToDisplay['MUST_DOWNLOAD'] != '':
                downloadNowText = QtWidgets.QApplication.translate('main', 'Download Now')
                reply = utils_functions.messageBox(
                    self, 
                    level='warning', 
                    message=messagesToDisplay['MUST_DOWNLOAD'], 
                    buttons=[
                        'Ok', 
                        ('net-download', downloadNowText, QtWidgets.QMessageBox.YesRole)])
                if reply == QtWidgets.QMessageBox.AcceptRole:
                    self.mustDownloadBaseMy = True
            elif messagesToDisplay['SCHOOL_YEAR'] != '':
                title = utils_functions.u('{0} ({1})').format(
                    utils.PROGNAME, QtWidgets.QApplication.translate(
                        'main', 'question message'))
                reply = utils_functions.messageBox(
                    self, 
                    level='warning', 
                    message=messagesToDisplay['SCHOOL_YEAR'], 
                    buttons=['Ok', ])
            elif messagesToDisplay['SIMPLIFIED_UI'] != '':
                dialog = utils_functions.MessageWithHelpPageDlg(
                    parent=self, 
                    message=messagesToDisplay['SIMPLIFIED_UI'], 
                    helpContextPage='interface')
                dialog.exec_()

        else:
            # on est en admin:
            import admin

            # on a besoin du editLog tout de suite (pour afficher les messages) :
            self.editLog = QtWidgets.QTextEdit()
            self.editLog.setReadOnly(True)
            self.editLog2 = None
            # le bouton pour effacer les messages :
            self.clearEditLogPushButton = QtWidgets.QPushButton(
                utils.doIcon('edit-clear'),
                QtWidgets.QApplication.translate('main', 'ClearEditLog'))
            self.clearEditLogPushButton.clicked.connect(self.doClearEditLog)
            # on agence tout ça :
            vLayout = QtWidgets.QVBoxLayout()
            vLayout.addWidget(self.editLog)
            vLayout.addWidget(self.clearEditLogPushButton)
            logGroupBox = QtWidgets.QGroupBox()
            logGroupBox.setLayout(vLayout)

            # le HelpViewer pour afficher l'aide contextuelle :
            self.helpViewer = utils_help.HelpViewer(self, frameStyle=self.editLog.frameStyle())
            # le menu HelpMenuTreeWidget placé à gauche :
            self.menuLeft = utils_help.HelpMenuTreeWidget(self, self.helpViewer)

            # un splitter horizontal pour agencer les 3
            # (menuLeft, helpViewer et logGroupBox) :
            hsplitter = QtWidgets.QSplitter(self)
            hsplitter.setOrientation(QtCore.Qt.Horizontal)
            hsplitter.addWidget(self.menuLeft)
            hsplitter.addWidget(self.helpViewer)
            hsplitter.addWidget(logGroupBox)
            # on partage l'espace disponible entre les 3 :
            desktopWidth = QtWidgets.QApplication.desktop().availableGeometry().width()
            if desktopWidth < 1024:
                menuLeftWidth = 170
                helpViewerWidth = 400
                logWidth = 300
            else:
                menuLeftWidth = 200
                helpViewerWidth = 450
                logWidth = 350
            reste = desktopWidth - menuLeftWidth - helpViewerWidth - logWidth
            if reste > 300:
                helpViewerWidth = helpViewerWidth + int(2 * reste / 3)
                logWidth = logWidth + int(reste / 3)
            elif reste > 0:
                helpViewerWidth = helpViewerWidth + int(reste / 2)
                logWidth = logWidth + int(reste / 2)
            hsplitter.setSizes([menuLeftWidth, helpViewerWidth, logWidth])

            # le stackedWidget est le widget central :
            self.stackedWidget = QtWidgets.QStackedWidget()
            self.stackedWidget.addWidget(hsplitter)
            self.setCentralWidget(self.stackedWidget)

            # initialisation de admin (avait besoin du editLog) :
            admin.initConfig(self)
            # on rempli le menu OpenADir :
            self.initOpenADirMenu()

            # On utilise les bases users et commun dans temp
            # (donc la troisième connexion aux bases users et commun).
            # On se base aussi sur les versions des 2 bases placées dans le dossier verac_admin,
            # et pas celles qui sont dans le dossier .Verac (dossier de config).
            # La raison est qu'en cas de partage du dossier verac_admin sur un réseau,
            # une modif faite sur un poste sera ainsi prise en compte sur les autres.
            utils_db.closeConnection(self, dbName='users')
            utils_filesdirs.removeAndCopy(admin.dbFileFtp_users, self.dbFileTemp_users)
            self.db_users = utils_db.createConnection(self, self.dbFileTemp_users)[0]
            utils_db.closeConnection(self, dbName='commun')
            utils_filesdirs.removeAndCopy(admin.dbFileFtp_commun, self.dbFileTemp_commun)
            self.db_commun = utils_db.createConnection(self, self.dbFileTemp_commun)[0]

            # on lit la configuration choisie pour les calculs :
            utils_functions.readConfigCalculs(self)
            # mise à jour des bases admin :
            import utils_upgrades
            utils_upgrades.upgradeAdmin(self)

            # test de wkhtmltopdf (si pas déjà fait) :
            testPdf = utils_db.readInConfigTable(self.db_localConfig, 'testPdf')
            import utils_pdf
            if (testPdf[0] == 1) and (QtCore.QFileInfo(testPdf[1]).exists()):
                utils_pdf.changeGlobals(True, testPdf[1])
            else:
                utils_pdf.searchWkhtmltopdf()
                if utils_pdf.PDF_OK:
                    pdfOk = 1
                else:
                    pdfOk = -1
                utils_db.changeInConfigTable(
                    self, 
                    self.db_localConfig, 
                    {'testPdf': (pdfOk, utils_pdf.WKHTMLTOPDF_BIN)})

        # tester s'il y a une mise à jour ?
        # ou une mise à jour des bases commun, users et suivis ?
        # ou lancer une vérification de la base prof ?
        #   * update : mise à jour de VÉRAC
        #   * DB : mise à jour des bases commun, users et suivis
        #   * repair : vérification-réparation de la base prof
        #   * mustDo : les vérifications sont à faire (dépend des réglages dans config)
        #   * updateGroupClass : si mustDo est à False, il faut peut-être vérifier
        #       les groupes-classes (cas des comptes lancés rarement, genre PP)
        tests = {
            'update': True, 
            'DB': True, 
            'repair': True, 
            'mustDo': False, 
            'updateGroupClass': False}
        if utils.installType == utils.INSTALLTYPE_ARCHIVE:
            # pour une archive on ne fait rien :
            tests['update'] = False
            tests['DB'] = False
            tests['repair'] = False
        elif utils.installType == utils.INSTALLTYPE_LAN:
            # en réseau, seul l'admin met VÉRAC à jour :
            if self.me['userMode'] != 'admin':
                tests['update'] = False
        if self.me['userMode'] == 'admin':
            # DB et repair ne concernent que les profs :
            tests['DB'] = False
            tests['repair'] = False
        if self.actualVersion['versionName'] in ['perso', 'demo']:
            # on ne teste pas les bases :
            tests['DB'] = False
        if not(self.NetOK):
            # pas de connexion :
            tests['update'] = False
            tests['DB'] = False
        # on teste via la base config s'il faut passer à l'action (mustDo) :
        if tests['update'] or tests['DB'] or tests['repair']:
            updatesMode = utils_db.readInConfigDB(self, 'UpdatesMode')[0]
            if updatesMode == 0:
                # une fois sur 3 :
                utilisations = utils_db.readInConfigTable(
                    self.db_localConfig, 'Utilisations')[0]
                utilisations += 1
                if utilisations > 2:
                    tests['mustDo'] = True
                    utilisations = 0
                utils_db.changeInConfigTable(
                    self, self.db_localConfig, {'Utilisations': (utilisations, '')})
            elif updatesMode == 1:
                # chaque semaine :
                aujourdhui = QtCore.QDate.currentDate()
                aujourdhuiText = aujourdhui.toString('yyyyMMdd')
                lastUpdate = utils_db.readInConfigTable(
                    self.db_localConfig, 'LastUpdate', default_text='20000101')[1]
                lastUpdate = QtCore.QDate.fromString(lastUpdate, 'yyyyMMdd')
                if lastUpdate.daysTo(aujourdhui) > 6:
                    tests['mustDo'] = True
                    utils_db.changeInConfigTable(
                        self, self.db_localConfig, {'LastUpdate': ('', aujourdhuiText)})
            elif updatesMode == 2:
                # toujours :
                tests['mustDo'] = True
        tests['updateGroupClass'] = tests['DB']
        utils_functions.myPrint('TESTS :', tests)
        if tests['mustDo']:
            if tests['update']:
                # y a t il une nouvelle version de VÉRAC ?
                utils_web.testUpdateVerac(self)
            if tests['DB']:
                if utils.installType == utils.INSTALLTYPE_LAN:
                    # en réseau, on ne teste que la base suivis :
                    prof_db.downloadSuivisDB(self, msgFin=False)
                else:
                    # on teste les bases commun, users et suivis :
                    prof_db.testUpdateDBUsersCommunSuivis(self)
                    tests['updateGroupClass'] = False
            if tests['repair']:
                # faut-il réparer la base prof :
                prof_db.checkCommunBilans(self, msgFin=False)
                # on recalcule tous les groupes de temps en temps :
                prof.calcAllGroupes(self, withProgress=True, msgFin=False)
        # enfin on teste s'il faut mettre à jour les groupes-classes
        # (cas des profs qui travaillent surtout en ligne 
        # ou des comptes lancés rarement, genre PP)
        if tests['updateGroupClass']:
            mustUpdateGroupClass = False
            where = utils_db.readInConfigTable(
                self.db_my, 'datetime')[1]
            if where == 'WEB':
                mustUpdateGroupClass = True
            else:
                aujourdhui = QtCore.QDateTime.currentDateTime()
                # on récupère la date de dernière utilisation :
                lastDateTime = self.me['localDateTime']
                if self.mustDownloadBaseMy:
                    lastDateTime = self.me['netDateTime']
                if lastDateTime in (-1, '-1'):
                    lastDateTime = aujourdhui
                else:
                    lastDateTime = QtCore.QDateTime.fromString(
                        '{0}'.format(lastDateTime), 'yyyyMMddhhmm')
                # on compare :
                if lastDateTime.daysTo(aujourdhui) > 30:
                    mustUpdateGroupClass = True
            if mustUpdateGroupClass:
                import prof_groupes
                prof_groupes.updateGroupClass(self)

    def interfaceLoaded(self):
        """
        fonction appelée une fois que l'interface est affichée.
        Permet de régler les positionnements des éléments
        """
        #print('.................... interfaceLoaded ....................')
        if self.me['userMode'] != 'admin':
            self.accordionSplitterMoved()

    def reloadConfigDict(self, showMessage=True):
        """
        pour réinitialiser l'interface prof en fonction de la configuration.
        Utilisé au lancement et lorsqu'on a téléchargé la base prof.
        La variable showMessage sert à ne pas afficher immédiatement 
        le message lors du premier lancement, pour éviter les messages inutiles.
        """
        message = ''
        self.configDict = utils_db.configTable2Dict(self.db_my)
        annee_scolaire = utils_db.readInConfigDict(
            self.configDict, 'annee_scolaire', default_int=0)[0]
        if annee_scolaire < 1:
            annee_scolaire = utils_functions.calcAnneeScolaire()[0]
            self.configDict['annee_scolaire'] = [annee_scolaire, '']
            utils_db.changeInConfigTable(self, self.db_my, {'annee_scolaire': (annee_scolaire, '')})
        if self.actualVersion['versionName'] != 'perso':
            if annee_scolaire < self.annee_scolaire[0]:
                """
                désactiver les envois et prévenir, 
                en donnant la possibilité de modifier en cas d'erreur
                """
                title = utils_functions.u('{0} ({1})').format(
                    utils.PROGNAME, QtWidgets.QApplication.translate(
                        'main', 'question message'))
                m1 = QtWidgets.QApplication.translate(
                    'main', 
                    'The administrator of your institution has made<br/> '
                    'a change of school year.<br/><br/>'
                    'You should do a <b>"clean for another year"</b><br/> '
                    '(action available in the <b>"File"</b> menu) before sending your file.<br/><br/>'
                    'If you think this is due to an error,<br/> '
                    'you can also fix the school year included in your file<br/> '
                    '(action available via the <b>"Tools -> Settings"</b> menu, <b>"Other"</b> tab).')
                message = utils_functions.u(
                    '<p align="center">{0}</p>'
                    '<p align="center">{1}</p>').format(utils.SEPARATOR_LINE, m1)
                if showMessage:
                    reply = utils_functions.messageBox(
                        self, 
                        level='warning', 
                        message=message, 
                        buttons=['Ok', ])
                self.actionAutoSaveDBMy.setChecked(False)
                actionsToDisable = (
                    self.actionPostDBMy,
                    self.actionSaveAndPostDBMy,
                    self.actionDownloadSuivisDB,
                    self.actionPostSuivis)
                for action in actionsToDisable:
                    action.setEnabled(False)
            elif annee_scolaire > self.annee_scolaire[0]:
                """
                prévenir, 
                en donnant la possibilité de modifier en cas d'erreur
                """
                title = utils_functions.u('{0} ({1})').format(
                    utils.PROGNAME, QtWidgets.QApplication.translate(
                        'main', 'question message'))
                m1 = QtWidgets.QApplication.translate(
                    'main', 
                    'The school year included in your file is later than your institution.<br/> '
                    'Your feedback will not be recovered until the establishment<br/> '
                    'has not made the change of school year.<br/><br/>'
                    'If you think this is due to an error,<br/> '
                    'you can correct the school year included in your file<br/> '
                    '(action available via the <b>"Tools -> Settings"</b> menu, <b>"Other"</b> tab).')
                message = utils_functions.u(
                    '<p align="center">{0}</p>'
                    '<p align="center">{1}</p>').format(utils.SEPARATOR_LINE, m1)
                if showMessage:
                    reply = utils_functions.messageBox(
                        self, 
                        level='warning', 
                        message=message, 
                        buttons=['Ok', ])

        # interface complète ou simplifiée ?
        simplifiedInterface = utils_db.readInConfigDict(
            self.configDict, 'SimplifiedInterface', default_int=1)[0]
        self.actionSwitchInterface.setChecked(simplifiedInterface == 1)

        # on place les barres d'outils :
        self.mainWindowState = {'initial': [1, 1, 1, '']}
        self.actualToolBarState = utils_db.readInConfigDict(
            self.configDict, 'actualToolBarsState', default_text='initial')[1]
        if not(self.actualToolBarState in ('initial', 'saved', 'last')):
            self.actualToolBarState = 'initial'
        if not(self.initialisation):
            self.doReloadLayout(what=self.actualToolBarState)

        # affichage ou non des élèves supprimés des groupes :
        mustShowDeletedEleves = utils_db.readInConfigDict(
            self.configDict, 'showDeletedEleves')[0]
        self.actionShowDeletedEleves.setChecked(mustShowDeletedEleves == 1)

        if not(self.initialisation):
            # on initialise la sélection :
            selectedPeriod = utils_db.readInConfigDict(self.configDict, 'SelectedPeriode')[0]
            utils.changeSelectedPeriod(selectedPeriod)
            self.updatePeriodeMenu(selectedPeriod)
            self.id_groupe = utils_db.readInConfigDict(
                self.configDict, 'SelectedGroupe', default_int=-1)[0]
            import prof_groupes
            prof_groupes.initGroupeMenu(self)
            selectedTableau = utils_db.readInConfigDict(self.configDict, 'SelectedTableau')[0]
            prof.tableauxInit(self, idTableauToSelect=selectedTableau, mustReloadView=False)
            self.changeValueInit()
            selectedView = utils_db.readInConfigDict(self.configDict, 'SelectedView')[0]
            self.doShow(viewType=selectedView)

        # la touche Shift doit elle être enfoncée ?
        shift = utils_db.readInConfigDict(self.configDict, 'shift')[0]
        self.actionShift.setChecked(shift == 1)
        self.doShift()
        # l'enregistrement automatique est-il activé ?
        autosave = utils_db.readInConfigDict(self.configDict, 'autosave')[0]
        self.actionAutoSaveDBMy.setChecked(autosave == 1)
        self.doAutoSaveDBMy()
        # changement de case sélectionnée automatique lors des évaluations ?
        # 0 : non (par défaut)
        # 1 : de haut en bas puis de gauche à droite
        # 2 : de gauche à droite puis de haut en bas
        self.autoselect = utils_db.readInConfigDict(self.configDict, 'autoselect')[0]
        if not(self.initialisation):
            self.initialisation = True
            self.doAutoselect()
            self.initialisation = False
        else:
            self.doAutoselect()

        if not(self.initialisation):
            utils_functions.readConfigCalculs(self)
            prof_db.initProtectedPeriods(self)
            self.initPeriodeMenu()
            self.createToolBar()
            self.doSwitchInterface()
        return message


    def closeEvent(self, event):
        utils_functions.afficheMessage(self, 'closeEvent', console=True)
        if self.maybeSave():
            self.closing = True
            # fermeture des bases encore ouvertes :
            if self.me['userMode'] != 'admin':
                self.view.setModel(None)
                del self.model
            # base localConfig :
            self.db_localConfig.close()
            del self.db_localConfig
            utils_db.sqlDatabase.removeDatabase('localConfig')
            fileTemp = utils_functions.u('{0}/localConfig.sqlite').format(self.tempPath)
            dbFile_localConfig = self.localConfigDir + '/config.sqlite'
            if utils.installType != utils.INSTALLTYPE_ARCHIVE:
                utils_filesdirs.removeAndCopy(fileTemp, dbFile_localConfig)
            # base lanConfig :
            if self.db_lanConfig != None:
                self.db_lanConfig.close()
                del self.db_lanConfig
                utils_db.sqlDatabase.removeDatabase('lanConfig')
            # base users et commun :
            utils_db.closeConnection(self, dbName='users')
            utils_db.closeConnection(self, dbName='commun')
            if self.me['userMode'] == 'admin':
                import admin
                # base admin :
                admin.db_admin.close()
                del admin.db_admin
                utils_db.sqlDatabase.removeDatabase('admin')
                # base resultats :
                if admin.db_resultats != None:
                    admin.db_resultats.close()
                    del admin.db_resultats
                    utils_db.sqlDatabase.removeDatabase('resultats')
                # base documents :
                if admin.db_documents != None:
                    admin.db_documents.close()
                    del admin.db_documents
                    utils_db.sqlDatabase.removeDatabase('documents')
                # base compteur :
                if admin.db_compteur != None:
                    admin.db_compteur.close()
                    del admin.db_compteur
                    utils_db.sqlDatabase.removeDatabase('compteur')
                # base configweb :
                if admin.db_configweb != None:
                    admin.db_configweb.close()
                    del admin.db_configweb
                    utils_db.sqlDatabase.removeDatabase('configweb')
                # base recup_evals :
                if admin.db_recupEvals != None:
                    admin.db_recupEvals.close()
                    del admin.db_recupEvals
                    utils_db.sqlDatabase.removeDatabase('recup_evals')
                # base referential_propositions :
                if admin.db_referentialPropositions != None:
                    admin.db_referentialPropositions.close()
                    del admin.db_referentialPropositions
                    utils_db.sqlDatabase.removeDatabase('referential_propositions')
                # base referential_validations :
                if admin.db_referentialValidations != None:
                    admin.db_referentialValidations.close()
                    del admin.db_referentialValidations
                    utils_db.sqlDatabase.removeDatabase('referential_validations')
                # base structures :
                if admin.db_structures != None:
                    admin.db_structures.close()
                    del admin.db_structures
                    utils_db.sqlDatabase.removeDatabase('structures')
            else:
                # base prof en mémoire vive :
                self.db_my.close()
                del self.db_my
                utils_db.sqlDatabase.removeDatabase('qt_sql_default_connection')

            # en admin, il faut récupérer la base admin depuis temp :
            if self.me['userMode'] == 'admin':
                import admin
                utils_filesdirs.removeAndCopy(admin.dbFileTemp_admin, admin.dbFile_admin)
            # suppression du dossier temporaire :
            utils_filesdirs.emptyDir(self.tempPath)
            tempDir = QtCore.QDir.temp()
            tempDir.rmdir(utils.PROGLINK)
            # on continue :
            event.accept()
        else:
            event.ignore()

    def maybeSave(self):
        reponse = True
        if (self.me['userMode'] != 'admin') and (self.versionDBMyIsOk):
            reponse = prof.testSuivisModified(self)
            prof.testAppreciationMustBeSaved(self)
            if self.db_myIsModified:
                utils_functions.afficheMessage(self, 'maybeSave', console=True)
                message = QtWidgets.QApplication.translate(
                    'main', 
                    'The persoBase has been modified.\n'
                    'Do you want to save your changes?')
                reponseMustSave = utils_functions.messageBox(
                    self, level='warning', message=message,
                    buttons=['Save', 'Discard', 'Cancel'])
                if reponseMustSave == QtWidgets.QMessageBox.Cancel:
                    reponse = False
                elif reponseMustSave == QtWidgets.QMessageBox.Save:
                    self.doSaveDBMy()
                    utils_db.changeInConfigDB(self, 'FilesDir', '', self.filesDir)
        return reponse

    def doTest(self):
        import utils_tests
        utils_tests.test(self, self.me['userMode'])








    ###########################################
    ###########################################
    # Administrateur:
    ###########################################
    ###########################################

    # ------------------------
    # dans fichier:
    # ------------------------
    def doOpenAdminDir_admin(self):
        import admin
        utils_filesdirs.openDir(admin.adminDir)

    def initOpenADirMenu(self):
        import admin
        dirsList = [
            (admin.adminDir,
            QtWidgets.QApplication.translate('main', 'verac_admin (AdminDir)'),
            'folder-admin'), 
            (admin.adminDir + '/ftp/secret/verac/up/files/',
            QtWidgets.QApplication.translate('main', 'files (ProfsFilesDir)'),
            'folder-profxx'),
            (admin.adminDir + '/fichiers/pdf/',
            QtWidgets.QApplication.translate('main', 'pdf (PdfFilesDir)'),
            'folder-pdf'),
            (admin.adminDir + '/ftp/secret/verac/protected/documents/',
            QtWidgets.QApplication.translate('main', 'documents (DocsElevesDir)'),
            'folder-eleves'),
            (admin.adminDir + '/ftp/secret/verac/protected/docsprofs/',
            QtWidgets.QApplication.translate('main', 'docsprofs (DocsProfsDir)'),
            'folder-profs'),
            ]
        for (dirName, label, iconFile) in dirsList:
            newAct = QtWidgets.QAction(label, self, icon=utils.doIcon(iconFile))
            newAct.setData((dirName))
            newAct.triggered.connect(self.openADirChanged)
            self.openADirMenu.addAction(newAct)

    def openADirChanged(self):
        (dirName) = self.sender().data()
        utils_filesdirs.openDir(dirName)



    def doLaunchScheduler_admin(self):
        """
        lance le planificateur de récupérations
        """
        # app est l'executable de Python :
        app = sys.executable
        if utils.OS_NAME[0] == 'win':
            app = self.app.replace('ython.exe', 'ythonw.exe')
        arg = utils_functions.u(
            '{0}/libs/verac_scheduler.pyw').format(self.beginDir)
        import subprocess
        pid = subprocess.Popen([app, arg]).pid



    def doClearEditLog(self):
        self.editLog.clear()




    # ------------------------
    # dans administration:
    # ------------------------

    # configuration:

    # création des bases:

    def doEditCsvFile_admin(self):
        import admin
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            admin.csvDir, 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName != '':
            admin.editCsvFile(self, fileName)

    def doEditDBFile_admin(self):
        import admin_db
        dialog = admin_db.DBFileEditorDlg(parent=self)
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)





    def doCreateAdminTableFromCsv(self):
        import admin
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            admin.csvDir + '/admin_tables', 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        tableName = QtCore.QFileInfo(fileName).baseName()
        admin.createTableFromCsv(self, tableName, 'admin')

    def doCreateCommunTableFromCsv(self):
        import admin
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            admin.csvDir + '/commun_tables', 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        tableName = QtCore.QFileInfo(fileName).baseName()
        admin.createTableFromCsv(self, tableName, 'commun')

    def doCreateAllCommunTablesFromCsv(self):
        import admin
        admin.createTableFromCsv(self, '', 'commun')

    def doGestAddresses(self):
        import admin
        admin.manageDB(self, what='Addresses')

    def doGestPhotos(self):
        import admin_photos
        admin_photos.managePhotos(self)

    # gestion des utilisateurs:
    def doDownloadDBUsers_admin(self):
        import admin
        admin.downloadDB(self)

    def doUploadDBUsers_admin(self):
        import admin
        admin.uploadDB(self, db='users')

    def doConfigurationSchool_admin(self):
        import admin
        admin.manageDB(self, what='ConfigurationSchool')

    def doClasses_admin(self):
        import admin
        admin.manageDB(self, what='Classes')

    def doSharedCompetences_admin(self):
        import admin
        admin.manageDB(self, what='SharedCompetences')

    def doGestUsers_admin(self):
        import admin
        admin.manageDB(self, what='Users')

    def doShowMdpProfsNoChange_admin(self):
        import admin
        admin.showMdpProfsNoChange(self)

    def doCreateConnectionFile_admin(self):
        import admin
        admin.createConnectionFile(self)

    def doUploadDBCommun_admin(self):
        import admin
        admin.uploadDB(self, db='commun')

    def doCreateYearArchiveAndClear_admin(self):
        import admin
        admin.createYearArchive(self, msgFin=False)
        admin.clearForNewYear(self)

    def doCreateYearArchive_admin(self):
        import admin
        admin.createYearArchive(self)

    def doClearForNewYear_admin(self):
        import admin
        admin.clearForNewYear(self)



    # ------------------------
    # dans Edition:
    # ------------------------


    # updateBilans:
    def doUpdateBilans_admin(self):
        import admin_calc_results
        admin_calc_results.updateBilans(self)

    def doUpdateBilansComplete_admin(self):
        import admin_calc_results
        #admin_calc_results.updateBilans(self, recupLevel=utils.RECUP_LEVEL_ALL)
        admin_calc_results.updateBilans(self, recupLevel=utils.RECUP_LEVEL_REFERENTIAL)

    def doUpdateBilansSelect_admin(self):
        import admin_calc_results
        admin_calc_results.updateBilans(
            self, recupLevel=utils.RECUP_LEVEL_SELECT, idsProfs=[], idsEleves=[])

    def doCalculateReferential_admin(self):
        import admin_calc_results
        reponse = admin_calc_results.calculateReferentialPropositions(
            self, idsEleves=[], msgFin=False)
        if reponse == False:
            return
        message = QtWidgets.QApplication.translate(
            'main', 'Would you post the referential DB?')
        message = utils_functions.u('<p><b>{0}</b></p>').format(message)
        reply = utils_functions.messageBox(
            self, level='question', message=message, buttons=['Yes', 'No'])
        if reply == QtWidgets.QMessageBox.Yes:
            admin_calc_results.uploadDBResultats(self, what='referential_propositions')

    def doClearReferential_admin(self):
        import admin_calc_results
        admin_calc_results.doClearReferential(self)

    def doUploadDBResultats_admin(self):
        import admin_calc_results
        message = QtWidgets.QApplication.translate(
            'main', 'Would you also post the referential DB?')
        message = utils_functions.u('<p><b>{0}</b></p>').format(message)
        reply = utils_functions.messageBox(
            self, level='question', message=message, buttons=['Yes', 'No'])
        if reply == QtWidgets.QMessageBox.Yes:
            admin_calc_results.uploadDBResultats(self)
        else:
            admin_calc_results.uploadDBResultats(self, what='resultats')

    # CreateBilansBLT:
    def doCreateReports_admin(self):
        import admin_create_report
        admin_create_report.doCreateReports(self)

    def doExportNotes2ods_admin(self):
        import admin_create_report
        admin_create_report.exportNotes2ods(self)

    def doExportStudentsResults2ods_admin(self):
        import admin_create_report
        admin_create_report.exportStudentsResults2ods(self)

    def doExportBLTEvals2ods_admin(self):
        import admin_create_report
        admin_create_report.exportBLTEvals2ods(self)

    def doShowFields_admin(self):
        import admin_create_report
        admin_create_report.showFields(self)

    def doCreateHtmlModele_admin(self):
        import admin_create_report
        admin_create_report.createHtmlModele(self)

    def doExport2LSU_admin(self):
        import admin_lsu
        admin_lsu.doExport2LSU(self)






    # ------------------------
    # dans VÉRAC:
    # ------------------------

    def doVerifProfsFiles_admin(self):
        import admin
        admin.verifProfsFiles(self)

    def doCheckSchoolReports_admin(self):
        import admin_calc_results
        admin_calc_results.checkSchoolReports(self)

    def doLastReport_admin(self):
        import admin_calc_results
        admin_calc_results.doLastReport(self)

    def doRedoMdp_admin(self):
        import admin_ftp
        admin_ftp.ftpRequestPassword(self)


    # ------------------------
    # aaaaaaaaaaaaaaaaaa:
    # ------------------------

    def doDocumentsManagement_admin(self):
        import admin_docs
        admin_docs.manageDocuments(self)

    def doSwitchToNextPeriod_admin(self):
        import admin
        admin.switchToNextPeriod(self)

    def doListProtectedPeriods_admin(self):
        import admin
        admin.listProtectedPeriods(self)

    def doLockedClasses_admin(self):
        import admin
        admin.listLockedClasses(self)

    def doWebSiteState(self):
        import admin
        downloaded = admin.mustDownloadDBs(self, databases=['configweb'])
        admin.manageDB(self, what='WebSite', indexTab=1, modified=downloaded)

    def doWebSiteConfig(self):
        import admin
        downloaded = admin.mustDownloadDBs(self, databases=['configweb'])
        admin.manageDB(self, what='WebSite', modified=downloaded)

    def doShowCompteurAnalyze_admin(self):
        import admin_compteur
        message = QtWidgets.QApplication.translate(
            'main', 
            '<p><b>Would you download the database compteur'
            '<br/> before proceeding?</b></p>')
        reponse = utils_functions.messageBox(
            self, level='warning', message=message, buttons=['Yes', 'No'])
        if reponse == QtWidgets.QMessageBox.Yes:
            admin_compteur.downloadCompteurDB(self, msgFin=False)
        admin_compteur.showCompteurAnalyze(self)

    def doGestStructuresProfs_admin(self):
        import admin_structures
        admin_structures.manageStructuresFiles(self)





    ###########################################
    ###########################################
    # Prof:
    ###########################################
    ###########################################


    # ------------------------
    # dans Fichiers:
    # ------------------------

    # gestion de la base perso:
    def changeDBMyState(self, isModified=True, force=False):
        if self.viewType == utils.VIEW_SUIVIS:
            return
        if (isModified == self.db_myIsModified) and (force == False):
            return
        utils_functions.afficheMessage(self, 'changeDBMyState', console=True)
        self.db_myIsModified = isModified
        if self.me['userMode'] == 'prof':
            if isModified:
                self.windowTitleAll = self.windowTitleBase + '*' + self.dbFile_my
            else:
                self.windowTitleAll = self.windowTitleBase + self.dbFile_my
        else:
            self.windowTitleAll = self.windowTitleBase
        try:
            # pour ne pas planter lors du premier appel
            # (le stackedWidget n'existe pas encore)
            if self.stackedWidget.currentIndex() < 2:
                self.setWindowTitle(self.windowTitleAll)
        except:
            self.setWindowTitle(self.windowTitleAll)
            pass

    def doSaveDBMy(self):
        utils_functions.afficheMessage(self, 'doSaveDBMy', console=True)
        prof.calcAll(self, forcer=True)
        # sauvegarde de la période et du tableau sélectionnés :
        viewType = self.viewType
        if viewType == utils.VIEW_WEBSITE:
            viewType = utils.VIEW_RANDOM_HELP
        lastTableaux = []
        for (id_periode, id_groupe) in self.lastTableauForSelection:
            lastTableau = '{0}|{1}|{2}'.format(
                id_periode, 
                id_groupe, 
                self.lastTableauForSelection[id_periode, id_groupe])
            lastTableaux.append(lastTableau)
        lastTableaux = '#'.join(lastTableaux)
        changes = {
            'SelectedPeriode': (utils.selectedPeriod, ''), 
            'SelectedGroupe': (self.id_groupe, ''), 
            'SelectedTableau': (self.id_tableau, ''), 
            'lastTableauForSelection': ('', lastTableaux), 
            'SelectedView': (viewType, ''), 
            'actualToolBarsState': ('', self.actualToolBarState), 
            }
        utils_db.changeInConfigTable(self, self.db_my, changes)
        utils_db.sqlDatabase.removeDatabase(self.dbName_my)
        prof_db.saveBase_my(self)
        self.changeDBMyState(isModified=False)

    def doSaveAsDBMy(self):
        utils_functions.afficheMessage(self, 'doSaveAsDBMy', console=True)
        prof.calcAll(self, forcer=True)
        prof_db.saveAsBase_my(self)

    def doPostDBMy(self):
        if self.actualVersion['versionName'] == 'demo':
            utils_functions.notInDemo(self)
            return
        utils_functions.afficheMessage(self, 'doPostDBMy', console=True)
        prof.calcAll(self, forcer=True)
        prof_db.postDBMy(self)

    def doSaveAndPostDBMy(self):
        if self.actualVersion['versionName'] == 'demo':
            utils_functions.notInDemo(self)
            return
        utils_functions.afficheMessage(self, 'doSaveAndPostDBMy', console=True)
        prof.calcAll(self, forcer=True)
        # sauvegarde de la période et du tableau sélectionnés :
        viewType = self.viewType
        if viewType == utils.VIEW_WEBSITE:
            viewType = utils.VIEW_RANDOM_HELP
        lastTableaux = []
        for (id_periode, id_groupe) in self.lastTableauForSelection:
            lastTableau = '{0}|{1}|{2}'.format(
                id_periode, 
                id_groupe, 
                self.lastTableauForSelection[id_periode, id_groupe])
            lastTableaux.append(lastTableau)
        lastTableaux = '#'.join(lastTableaux)
        changes = {
            'SelectedPeriode': (utils.selectedPeriod, ''), 
            'SelectedGroupe': (self.id_groupe, ''), 
            'SelectedTableau': (self.id_tableau, ''), 
            'lastTableauForSelection': ('', lastTableaux), 
            'SelectedView': (viewType, ''), 
            'actualToolBarsState': ('', self.actualToolBarState), 
            }
        utils_db.changeInConfigTable(self, self.db_my, changes)
        utils_db.sqlDatabase.removeDatabase(self.dbName_my)
        date = prof_db.saveBase_my(self)
        self.changeDBMyState(isModified=False)
        prof_db.postDBMy(self, date=date)

    def doAutoSaveDBMy(self):
        if self.actionAutoSaveDBMy.isChecked():
            utils_functions.afficheMessage(self, 'AUTOSAVE YES', console=True)
            self.autoSaveTimer = QtCore.QTimer(self)
            self.autoSaveTimer.timeout.connect(self.autoSaveTimerIsOut)
            # le timer se déclenchera toutes les 5 min :
            self.autoSaveDelay = 5 * 60 * 1000
            self.autoSaveTimer.start(self.autoSaveDelay)
        else:
            utils_functions.afficheMessage(self, 'AUTOSAVE NO', console=True)
            try:
                self.autoSaveTimer.stop()
            except:
                pass
            self.autoSaveTimer = None

    def autoSaveTimerIsOut(self):
        if self.me['userMode'] == 'admin':
            return
        if self.db_myIsModified:
            self.doSaveDBMy()

    def doCompareDBMy(self):
        utils_functions.afficheMessage(self, 'doCompareDBMy', console=True)
        prof_db.compareBase_my(self)

    def doDownloadDBMy(self):
        utils_functions.afficheMessage(self, 'doDownloadDBMy', console=True)
        prof_db.downloadBase_my(self)

    def doPostSuivis(self):
        if self.actualVersion['versionName'] == 'demo':
            utils_functions.notInDemo(self)
            return
        utils_functions.afficheMessage(self, 'doPostSuivis', console=True)
        prof.postSuivis(self)

    def doSwitchInterface(self):
        if self.actionSwitchInterface.isChecked():
            simplifiedInterface = 1
            if not(self.initialisation):
                utils_db.deleteFromConfigTable(self, self.db_my, ['SimplifiedInterface'])
            self.actionSwitchInterface.setStatusTip(
                QtWidgets.QApplication.translate('main', 'SwitchToCompleteInterfaceStatusTip'))
            self.actionSwitchInterface.setText(
                QtWidgets.QApplication.translate('main', 'SwitchToCompleteInterface'))
        else:
            simplifiedInterface = 0
            if not(self.initialisation):
                utils_db.changeInConfigTable(self, self.db_my, {'SimplifiedInterface': (0, '')})
            self.actionSwitchInterface.setStatusTip(
                QtWidgets.QApplication.translate('main', 'SwitchToSimplifiedInterfaceStatusTip'))
            self.actionSwitchInterface.setText(
                QtWidgets.QApplication.translate('main', 'SwitchToSimplifiedInterface'))
        self.configDict['SimplifiedInterface'] = [simplifiedInterface, '']
        if not(self.initialisation):
            self.createMenus()
            self.doShow(doReload=True)
            self.changeDBMyState()


    # divers:
    def doChangeMdp(self):
        if self.actualVersion['versionName'] == 'demo':
            utils_functions.notInDemo(self)
            return
        utils_functions.afficheMessage(self, 'doChangeMdp', console=True)
        utils_connect.changeMdp(self)

    def doGotoSiteEtab(self):
        utils_functions.afficheMessage(self, 'doGotoSiteEtab', console=True)
        utils_web.gotoSiteEtab(self)

    def doTestUpdateDBUsersCommun(self):
        utils_functions.afficheMessage(self, 'doTestUpdateDBUsersCommun', console=True)
        prof_db.testUpdateDBUsersCommunSuivis(self, testOnly=True)

    def doCheckCommunBilans(self):
        utils_functions.afficheMessage(self, 'doCheckCommunBilans', console=True)
        prof_db.checkCommunBilans(self)

    def doDownloadSuivisDB(self):
        utils_functions.afficheMessage(self, 'doDownloadSuivisDB', console=True)
        prof_db.downloadSuivisDB(self)



    def doTestUpdateVerac(self):
        utils_functions.afficheMessage(self, 'doTestUpdateVerac', console=True)
        utils_web.testUpdateVerac(self, messageIfOK=True)

    def doUpdateVerac(self):
        utils_functions.afficheMessage(self, 'doUpdateVerac', console=True)
        utils_web.updateVerac(self)

    def doTestUpdateWebSite(self):
        utils_functions.afficheMessage(self, 'doTestUpdateWebSite', console=True)
        utils_web.testUpdateVerac(self, doVerac=False, doAdmin=False, messageIfOK=True)

    def doUpdateWebSite(self):
        utils_functions.afficheMessage(self, 'doUpdateWebSite', console=True)
        import admin_web
        admin_web.updateVeracWebSite(self)

    def doConfig(self):
        self.mustReflectChanges = False
        import utils_config
        dialog = utils_config.ConfigDlg(parent=self)
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)
        if self.closing:
            return
        if self.me['userMode'] == 'prof':
            self.createToolBar()
        if self.mustReflectChanges:
            if self.me['userMode'] != 'admin':
                prof.calcAllGroupes(self, withProgress=True)
                prof.viewChanged(self)
                utils_functions.afficheStatusBar(self)
            else:
                # on met à jour configweb :
                import admin_web
                admin_web.createConfigWebDB(self, doConfig=True, postDB=False)
                import admin
                admin.mustUploadDBs(self, databases=['commun', 'configweb'])


    def doCreateEtab(self):
        prof.createEtab(self)

    def doAdminVersionPerso(self):
        prof.adminVersionPerso(self)

    def doReturnVersionPerso(self):
        prof.returnVersionPerso(self)

    def doCreateYearArchive(self):
        prof_db.createYearArchive(self)

    def doClearForNewYear(self):
        prof_db.clearForNewYear(self)

    def doProtectedPeriods(self):
        prof_db.changeProtectedPeriods(self)

    # ------------------------
    # dans Outils:
    # ------------------------

    # gérer les groupes d'élèves:
    def doGestGroupes(self):
        import prof_groupes
        dialog = prof_groupes.GestGroupesDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            prof_groupes.initGroupeMenu(self)
            self.id_groupe = dialog.id_groupe
            self.updateGroupeMenu(id_groupe=dialog.id_groupe)
            prof.tableauxInit(self)
            self.doShow(doReload=True)
            self.changeDBMyState()
        self.enableInterface(dialog, lastState)

    # créer plusieurs groupes-classes en une fois :
    def doCreateMultipleGroupesClasses(self):
        import prof_groupes
        dialog = prof_groupes.CreateMultipleGroupesClassesDlg(parent=self)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        isClasse = 1
        groupeMatiere = dialog.matieresComboBox.currentText()
        groupeBaseName = dialog.nameEdit.text()
        selectedClasses = dialog.classesListWidget.selectedItems()
        query_my = utils_db.query(self.db_my)
        query_users = utils_db.query(self.db_users)
        # servira à afficher le premier groupe créé à la fin :
        firstIdGroupe = -1
        id_groupe = -1
        lines = {'groupe_eleve': [], 'groupes': []}
        for selectedClasse in selectedClasses:
            classeName = selectedClasse.text()
            id_classe = dialog.idFromClasse[classeName]
            if groupeBaseName == '':
                groupeName = classeName
            else:
                groupeName = utils_functions.u('{0}-{1}').format(groupeBaseName, classeName)
            if firstIdGroupe < 0:
                # on cherche un id pour le nouveau groupe :
                query_my = utils_db.queryExecute(
                    'SELECT id_groupe FROM groupes', query=query_my)
                id_groupe = 0
                while query_my.next():
                    id_groupe = int(query_my.value(0))
                    id_groupe += 1
                firstIdGroupe = id_groupe
            else:
                id_groupe += 1
            # il faut ajouter les élèves de la classe à la liste
            commandLine = 'SELECT * FROM eleves WHERE Classe=?'
            queryList = utils_db.query2List(
                {commandLine: (classeName, )}, 
                order=['NOM', 'Prenom'], 
                query=query_users)
            ordre = 1
            for record in queryList:
                id_eleve = int(record[0])
                listArgs = (id_groupe, id_eleve, ordre)
                lines['groupe_eleve'].append(listArgs)
                ordre += 1
            # on enregistre dans la base
            listArgs = (id_groupe, groupeName, groupeMatiere, isClasse, classeName, 0)
            lines['groupes'].append(listArgs)
        for table in ('groupe_eleve', 'groupes'):
            commandLine_my = utils_db.insertInto(table)
            query_my = utils_db.queryExecute(
                {commandLine_my: lines[table]}, query=query_my)
        prof_groupes.initGroupeMenu(self)
        self.id_groupe = firstIdGroupe
        self.updateGroupeMenu(id_groupe=firstIdGroupe)
        prof.tableauxInit(self)
        self.doShow(doReload=True)
        self.changeDBMyState()


    # pour lancer GestGroupesDlg directement avec la fenêtre de création d'un groupe :
    def doCreateGroupe(self):
        import prof_groupes
        dialog = prof_groupes.GestGroupesDlg(parent=self, doCreate=True)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            prof_groupes.initGroupeMenu(self)
            self.id_groupe = dialog.id_groupe
            self.updateGroupeMenu(id_groupe=dialog.id_groupe)
            prof.tableauxInit(self)
            self.doShow(doReload=True)
            self.changeDBMyState()
        self.enableInterface(dialog, lastState)


    # affichage ou non des élèves supprimés des groupes :
    def doShowDeletedEleves(self):
        utils_functions.afficheMessage(self, 'doShowDeletedEleves', console=True)
        self.doShow(doReload=True)
        self.changeDBMyState()

    def doClearDeletedEleves(self):
        """
        pour effacer toutes les traces d'élèves retirés des groupes
        """
        utils_functions.afficheMessage(self, 'doClearDeletedEleves', console=True)
        import prof_groupes
        dialog = prof_groupes.ClearDeletedElevesDlg(parent=self)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        utils_functions.doWaitCursor()
        try:
            query_my = utils_db.query(self.db_my)
            commandLine1 = (
                'DELETE FROM groupe_eleve '
                'WHERE id_eleve={0} AND id_groupe={1}')
            commandLine2 = (
                'DELETE FROM profils '
                'WHERE id_eleve={0} AND id_groupe={1}')
            commandLine3 = (
                'DELETE FROM profil_who '
                'WHERE id_who={0} AND profilType="STUDENTS"')
            commandLine4_debut = (
                'DELETE FROM evaluations '
                'WHERE id_eleve={0} AND ({1})')
            commandLine5 = (
                'DELETE FROM appreciations '
                'WHERE id_eleve={0} AND Matiere="{1}"')
            for index in range(dialog.elevesListWidget.count()):
                selectedEleve = dialog.elevesListWidget.item(index)
                if selectedEleve.checkState() == QtCore.Qt.Checked:
                    data = selectedEleve.data(QtCore.Qt.UserRole)
                    commandLine_my1 = commandLine1.format(data[0], data[1])
                    commandLine_my2 = commandLine2.format(data[0], data[1])
                    commandLine_my3 = commandLine3.format(data[0])
                    listTableaux = prof_groupes.tableauxFromGroupe(
                        self, data[1], publicOnly=False)
                    commandLine4_fin = ''
                    separator = ''
                    for id_tableau in listTableaux:
                        commandLine4_fin = '{0}{1}id_tableau={2}'.format(
                            commandLine4_fin, separator, id_tableau)
                        separator = ' OR '
                    commandLine_my4 = commandLine4_debut.format(
                        data[0], commandLine4_fin)
                    commandLine_my5 = utils_functions.u(commandLine5).format(
                        data[0], data[2])
                    commandLines = [
                        commandLine_my1, 
                        commandLine_my2, 
                        commandLine_my3,
                        commandLine_my4, 
                        commandLine_my5]
                    query_my = utils_db.queryExecute(commandLines, query=query_my)
        finally:
            utils_functions.restoreCursor()
            self.changeDBMyState()

    # gérer les items:
    def doGestItems(self):
        import prof_itemsbilans
        prof_itemsbilans.verificationTable(self)
        # recupération des tables :
        self.initialTables = prof_itemsbilans.saveTables(
            self, 
            tables=prof_itemsbilans.TABLES_FOR_ITEMS_BILANS)
        dialog = prof_itemsbilans.GestItemsDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, self.initialTables)
        self.enableInterface(dialog, lastState)

    # gérer les liens items-bilans:
    def doGestItemsBilans(self):
        import prof_itemsbilans
        prof_itemsbilans.verificationTable(self)
        # recupération des tables :
        self.initialTables = prof_itemsbilans.saveTables(
            self, 
            tables=prof_itemsbilans.TABLES_FOR_ITEMS_BILANS)
        dialog = prof_itemsbilans.GestItemsBilansDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, self.initialTables)
        self.enableInterface(dialog, lastState)

    # gérer les liens items-bilans par une table :
    def doItemsBilansTable(self):
        import prof_itemsbilans
        prof_itemsbilans.verificationTable(self)
        # recupération des tables :
        self.initialTables = prof_itemsbilans.saveTables(
            self, 
            tables=prof_itemsbilans.TABLES_FOR_ITEMS_BILANS)
        dialog = prof_itemsbilans.ItemsBilansTableDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, self.initialTables)
        self.enableInterface(dialog, lastState)

    # relier plusieurs items à un bilan :
    def doLinkItemsToBilan(self):
        import prof_itemsbilans
        prof_itemsbilans.verificationTable(self)
        # recupération des tables :
        initialTables = prof_itemsbilans.saveTables(
            self, tables=('bilans', 'item_bilan'))
        dialog = prof_itemsbilans.LinkItemsToBilanDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            prof_itemsbilans.linkItemsToBilan(self, dialog)
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, initialTables)
        self.enableInterface(dialog, lastState)

    # relier des bilans :
    def doLinkBilans(self):
        import prof_itemsbilans
        prof_itemsbilans.verificationTable(self)
        # recupération des tables :
        initialTables = prof_itemsbilans.saveTables(
            self, tables=('item_bilan', 'bilan_bilan'))
        dialog = prof_itemsbilans.LinkBilansDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, initialTables)
        self.enableInterface(dialog, lastState)

    # gérer les conseils:
    def doGestAdvices(self):
        import prof_itemsbilans
        # recupération des tables :
        initialTables = prof_itemsbilans.saveTables(self, tables=('comments', ))
        dialog = prof_itemsbilans.GestAdvicesDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, initialTables)
        self.enableInterface(dialog, lastState)

    # gérer les tableaux :
    def doCreateTableau(self):
        dialog = prof.NewTableauDlg(parent=self)
        dialog.show()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            index = dialog.groupesComboBox.currentIndex()
            id_groupe = dialog.groupesComboBox.itemData(index, QtCore.Qt.UserRole)
            tableauName = dialog.nameEdit.text()
            tableauLabel = dialog.labelEdit.text()
            tableauPeriode = dialog.periodesComboBox.currentIndex()
            if dialog.publicCheckBox.isChecked():
                public = 1
            else:
                public = 0
            id_tableau = prof.createNewTableau(
                self, id_groupe, tableauName, tableauLabel, tableauPeriode, public)
            if id_tableau > -1:
                lines = []
                if len(dialog.list_items) > 0:
                    for (id_item, ordre) in dialog.list_items:
                        lines.append((id_tableau, id_item, ordre))
                elif dialog.id_template != 0:
                    lines.append((id_tableau, dialog.id_template, 0))
                if len(lines) > 0:
                    query_my = utils_db.query(self.db_my)
                    commandLine_my = utils_db.insertInto('tableau_item')
                    query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
                self.changeDBMyState()
                prof.calcAll(self)
            self.doShow()

    def doCopyTableau(self):
        if prof.copyTableau(self):
            self.changeDBMyState()
            prof.calcAll(self, forcer=True)

    def doSupprTableau(self):
        prof.supprTableau(self)
        self.changeDBMyState()

    def doCopyItemsFromTo(self):
        if prof.copyItemsFromTo(self):
            self.changeDBMyState()

    def doMergeTableaux(self):
        if prof.mergeTableaux(self):
            self.changeDBMyState()

    def doSortGroupesTableaux(self):
        import prof_itemsbilans, prof_groupes
        # recupération des tables :
        initialTables = prof_itemsbilans.saveTables(
            self, tables=('groupes', 'tableaux'))
        dialog = prof.ListGroupesTableauxDlg(parent=self)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # on enregistre les modifications :
            dialog.saveTableaux()
            dialog.saveGroupes()
            prof_groupes.initGroupeMenu(self)
            prof.tableauxInit(self)
            self.doShow(doReload=True)
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, initialTables)

    def doCreateMultipleTableaux(self):
        dialog = prof.CreateMultipleTableauxDlg(parent=self)
        dialog.show()
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            tableaux = []
            tableauBaseName = dialog.nameEdit.text()
            tableauLabel = dialog.labelEdit.text()
            tableauPeriode = dialog.periodesComboBox.currentIndex()
            if dialog.publicCheckBox.isChecked():
                public = 1
            else:
                public = 0
            selectedGroupes = dialog.groupesListWidget.selectedItems()
            for selectedGroupe in selectedGroupes:
                itemText = selectedGroupe.text()
                if len(itemText.split('(')) > 1:
                    groupe = itemText.split('(')[0][:-1]
                    id_groupe = dialog.idFromGroupe[itemText]
                    tableauName = utils_functions.u('{0}-{1}').format(tableauBaseName, groupe)
                    id_tableau = prof.createNewTableau(
                        self, id_groupe, tableauName, tableauLabel, tableauPeriode, public)
                    if id_tableau > -1:
                        tableaux.append(id_tableau)
            lines = []
            if len(dialog.list_items) > 0:
                for (id_item, ordre) in dialog.list_items:
                    for id_tableau in tableaux:
                        lines.append((id_tableau, id_item, ordre))
            elif dialog.id_template != 0:
                for id_tableau in tableaux:
                    lines.append((id_tableau, dialog.id_template, 0))
            if len(lines) > 0:
                query_my = utils_db.query(self.db_my)
                commandLine_my = utils_db.insertInto('tableau_item')
                query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
            # terminé :
            self.changeDBMyState()
            self.doShow()

    # gérer les modèles de tableaux :
    def doTableauxTemplates(self):
        dialog = prof.ManageTemplatesDlg(parent=self)
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)

    # créer un modèle de tableau d'après le tableau actuel :
    def doCreateTemplateFromTableau(self):
        prof.createTemplateFromTableau(self)
        prof.tableauxChanged(self)

    # lier le tableau actuel à un modèle de tableau :
    def doLinkTableauTemplate(self):
        prof.linkTableauTemplate(self)
        prof.tableauxInit(self)

    # délier le tableau actuel de son modèle de tableau :
    def doUnlinkTableauTemplate(self):
        prof.unlinkTableauTemplate(self)
        prof.tableauxChanged(self)




    # gérer le tableau actuel:
    def doEditActualTableau(self):
        if prof.editActualTableau(self):
            prof.calcAll(self, forcer=True)
            self.changeDBMyState()



    def doRemoveEleves(self):
        if self.viewType != utils.VIEW_ITEMS:
            self.doShow()
        if prof.removeEleves(self):
            self.changeDBMyState()
            prof.calcAll(self, forcer=True)

    def doListItems(self):
        if self.viewType != utils.VIEW_ITEMS:
            self.doShow()
        dialog = prof.ListItemsDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            if prof.refreshListItems(self, dialog):
                if self.viewType == utils.VIEW_ITEMS:
                    self.actionUndo.setEnabled(False)
                    self.actionRedo.setEnabled(False)
                    self.undoList, self.redoList = [], []
                self.changeDBMyState()
        self.enableInterface(dialog, lastState)

    def doListVisibleItems(self):
        if self.viewType != utils.VIEW_ITEMS:
            self.doShow()
        dialog = prof.ListVisibleItemsDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.enableInterface(dialog, lastState)
            prof.refreshListVisibleItems(self, dialog)
        else:
            self.enableInterface(dialog, lastState)

    def doHideEmptyColumns(self):
        if self.viewType != utils.VIEW_ITEMS:
            return
        prof.hideEmptyColumns(self)

    def doManageProfiles(self):
        import prof_itemsbilans, prof_groupes
        data = prof.diversFromSelection(self)
        matiereName = data[2]
        # recupération des tables :
        self.initialTables = prof_itemsbilans.saveTables(
            self, tables=('profils', 'profil_who', 'profil_bilan_BLT'))
        dialog = prof_groupes.ManageProfilesDlg(
            parent=self, matiereName=matiereName)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            dialog.saveProfilChanges(dialog.profilSelectionWidget.currentItem())
            self.changeDBMyState()
            # on recalcule tous les groupes :
            prof.calcAllGroupes(self, withProgress=True, msgFin=False)
            prof.viewChanged(self)
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, self.initialTables)
        self.enableInterface(dialog, lastState)

    def doAddNote(self):
        dialog = prof.EditNoteDlg(parent=self)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            id_note = prof.addNote(self, dialog.data)
            dialog.data[2] = id_note
            prof.recalculeNote(self, dialog.data)
            self.doShow(doReload=True)
            self.changeDBMyState()

    def doEditNote(self):
        # on vérifie qu'une note est sélectionnée :
        if len(self.view.selectedIndexes()) < 1:
            return
        column = self.view.selectedIndexes()[-1].column()
        if column == self.model.columnCount() - 1:
            return
        # lancement du dialog :
        data = list(self.model.columns[column]['other'])
        dialog = prof.EditNoteDlg(parent=self, data=data)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            prof.editNote(self, data, dialog.data)
            prof.recalculeNote(self, dialog.data)
            self.doShow(doReload=True)
            self.changeDBMyState()

    def doDeleteNote(self):
        # on vérifie qu'une note est sélectionnée :
        if len(self.view.selectedIndexes()) < 1:
            return
        # récupération des données :
        column = self.view.selectedIndexes()[-1].column()
        if column == self.model.columnCount() - 1:
            return
        data = list(self.model.columns[column]['other'])
        message = QtWidgets.QApplication.translate(
            'main', 
            '<p><b>Delete This Note:</b></p> {0} <p><b>Continue ?</b></p>').format(data[3])
        if utils_functions.messageBox(
            self, level='question', message=message, 
            buttons=['Yes', 'No']) != QtWidgets.QMessageBox.Yes:
            return
        prof.deleteNote(self, data)
        self.doShow(doReload=True)
        self.changeDBMyState()

    def doOrderNotes(self):
        import prof_itemsbilans
        # recupération des tables :
        initialTables = prof_itemsbilans.saveTables(self, tables=('notes', ))
        dialog = prof.OrderListDlg(parent=self, what='notes')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # on enregistre les modifications :
            dialog.saveList()
            self.doShow(doReload=True)
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, initialTables)




    def doCreateCount(self):
        dialog = prof.EditCountDlg(parent=self)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            prof.createCount(self, dialog.data)
            self.doShow(doReload=True)
            prof.recalculeCountsResults(self, countsToUpdate={})
            self.changeDBMyState()

    def doCreateMultipleCounts(self):
        dialog = prof.CreateMultipleCountsDlg(parent=self)
        lastState = self.disableInterface(dialog)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.doShow(doReload=True)
            prof.recalculeCountsResults(self, countsToUpdate={})
            self.changeDBMyState()
            #prof.viewChanged(self)
        self.enableInterface(dialog, lastState)

    def doEditCount(self):
        # on vérifie qu'un comptage est sélectionné :
        if len(self.view.selectedIndexes()) < 1:
            return
        column = self.view.selectedIndexes()[-1].column()
        if column == self.model.columnCount() - 1:
            return
        # lancement du dialog :
        data = list(self.model.columns[column]['other'])
        dialog = prof.EditCountDlg(parent=self, data=data)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            prof.editCount(self, dialog.oldName, dialog.data)
            self.doShow(doReload=True)
            prof.recalculeCountsResults(self, countsToUpdate={})
            self.changeDBMyState()

    def doDeleteCount(self):
        # on vérifie qu'un comptage est sélectionné :
        if len(self.view.selectedIndexes()) < 1:
            return
        # récupération des données :
        column = self.view.selectedIndexes()[-1].column()
        data = list(self.model.columns[column]['other'])
        message = QtWidgets.QApplication.translate(
            'main', 
            '<p><b>Delete This Count:</b></p> {0} <p><b>Continue ?</b></p>').format(data[3])
        if utils_functions.messageBox(
            self, level='question', message=message, 
            buttons=['Yes', 'No']) != QtWidgets.QMessageBox.Yes:
            return
        self.noKeyPressEvent = True
        prof.deleteCount(self, data)
        self.doShow(doReload=True)
        self.changeDBMyState()
        self.noKeyPressEvent = False

    def doOrderCounts(self):
        import prof_itemsbilans
        # recupération des tables :
        initialTables = prof_itemsbilans.saveTables(self, tables=('counts', ))
        dialog = prof.OrderListDlg(parent=self, what='counts')
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            # on enregistre les modifications :
            dialog.saveList()
            self.doShow(doReload=True)
            self.changeDBMyState()
        else:
            # on restaure les tables initiales :
            prof_itemsbilans.restoreTables(self, initialTables)

    def doGestElevesSuivis(self):
        import prof_suivis
        dialog = prof_suivis.GestElevesSuivisDlg(parent=self)
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)


    # import-export:

    def doProposedName(self, extension='', what='VIEW', other=''):
        """
        """
        proposedName = ''
        if what == 'GROUP':
            dataSelection = prof.diversFromSelection(self)
            matiereName = dataSelection[2]
            groupe = utils_functions.changeBadChars(dataSelection[1])
            periode = utils_functions.changeBadChars(self.periodeButton.text())
            proposedName = utils_functions.u('{0}/{1}_{2}_{3}.{4}').format(
                self.workDir, matiereName, groupe, periode, extension)
        elif what == 'ALLGROUPS':
            allGroupsText = QtWidgets.QApplication.translate('main', 'Groups')
            periode = utils_functions.changeBadChars(self.periodeButton.text())
            proposedName = utils_functions.u('{0}/{1}_{2}.{3}').format(
                self.workDir, allGroupsText, periode, extension)
        elif what == 'ELEVES':
            elevesText = QtWidgets.QApplication.translate('main', 'Students')
            groupe = utils_functions.changeBadChars(prof.diversFromSelection(self)[1])
            periode = utils_functions.changeBadChars(self.periodeButton.text())
            proposedName = utils_functions.u('{0}/{1}_{2}_{3}.{4}').format(
                self.workDir, elevesText, groupe, periode, extension)
        elif what == 'ELEVE':
            periode = utils_functions.changeBadChars(self.periodeButton.text())
            proposedName = utils_functions.u('{0}/{1}_{2}.{3}').format(
                self.workDir, other, periode, extension)
        elif self.viewType > -1:
            dataSelection = prof.diversFromSelection(self, self.id_tableau)
            matiereName = dataSelection[2]
            groupe = utils_functions.changeBadChars(dataSelection[1])
            periode = utils_functions.changeBadChars(self.periodeButton.text())
            tableau = utils_functions.changeBadChars(dataSelection[4])
            view = utils_functions.changeBadChars(self.viewButton.text())
            if self.viewType == utils.VIEW_ELEVE:
                eleve = self.changeEleveComboBox.currentText()
                proposedName = '{0}/{1}_{2}_{3}_{4}_{5}.{6}'
                proposedName = utils_functions.u(proposedName).format(
                    self.workDir, matiereName, groupe, periode, view, eleve, extension)
            elif self.viewType in utils.VIEWS_IF_TABLEAU:
                proposedName = '{0}/{1}_{2}_{3}_{4}_{5}.{6}'
                proposedName = utils_functions.u(proposedName).format(
                    self.workDir, matiereName, groupe, periode, tableau, view, extension)
            else:
                proposedName = '{0}/{1}_{2}_{3}_{4}.{5}'
                proposedName = utils_functions.u(proposedName).format(
                    self.workDir, matiereName, groupe, periode, view, extension)
        return proposedName

    def doExportActualVue2pdf(self):
        pdfTitle = QtWidgets.QApplication.translate('main', 'pdf File')
        proposedName = self.doProposedName(extension='pdf')
        pdfExt = QtWidgets.QApplication.translate('main', 'pdf files (*.pdf)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, pdfTitle, proposedName, pdfExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportQui2Quoi(
            self, 
            paramsQui={'qui': 'actualVue'}, 
            paramsQuoi={'quoi': 'PDF', 'fileName': fileName}, 
            msgFin=True)

    def doExportActualVue2Print(self):
        import utils_export
        utils_export.exportQui2Quoi(
            self, 
            paramsQui={'qui': 'actualVue'}, 
            paramsQuoi={'quoi': 'PRINT'}, 
            msgFin=True)

    def doExportActualVue2ods(self):
        odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
        proposedName = self.doProposedName(extension='ods')
        odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportQui2Quoi(
            self, 
            paramsQui={'qui': 'actualVue'}, 
            paramsQuoi={'quoi': 'ODS', 'fileName': fileName}, 
            msgFin=True)

    def doExportActualGroup2ods(self):
        odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
        proposedName = self.doProposedName(extension='ods', what='GROUP')
        odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportQui2Quoi(
            self, 
            paramsQui={'qui': 'actualGroup'}, 
            paramsQuoi={'quoi': 'ODS', 'fileName': fileName}, 
            msgFin=True)

    def doExportStructure2ods(self):
        odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u(
            '{0}/export_structure.ods').format(self.workDir)
        odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportStructure2ods(self, fileName)

    def doExportItemsList2ods(self):
        odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u(
            '{0}/export_items.ods').format(self.workDir)
        odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportItemsList2ods(self, fileName)

    def doCreateStructureFile(self):
        import utils_structures
        wizard = utils_structures.CreateStructureFileWizard(self)
        lastState = self.disableInterface(wizard)
        if wizard.exec_() == QtWidgets.QDialog.Accepted:
            wizard.createStructureFile()
        self.enableInterface(wizard, lastState)

    def doImportStructureFile(self):
        import utils_structures
        wizard = utils_structures.ImportStructureFileWizard(self)
        lastState = self.disableInterface(wizard)
        if wizard.exec_() == QtWidgets.QDialog.Accepted:
            wizard.importStructureFile()
        self.enableInterface(wizard, lastState)

    def doExportBilansItems2Freeplane(self):
        mmTitle = QtWidgets.QApplication.translate('main', 'Save Freeplane File')
        proposedName = utils_functions.u('{0}/BilansItems.mm').format(self.workDir)
        mmExt = QtWidgets.QApplication.translate('main', 'mm files (*.mm)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, mmTitle, proposedName, mmExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export_xml
        utils_export_xml.exportBilansItems2Freeplane(self, fileName)

    def doExportActualGroup2Freeplane(self):
        mmTitle = QtWidgets.QApplication.translate('main', 'Save Freeplane File')
        proposedName = self.doProposedName(extension='mm', what='GROUP')
        mmExt = QtWidgets.QApplication.translate('main', 'mm files (*.mm)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, mmTitle, proposedName, mmExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export_xml
        utils_export_xml.exportActualGroup2Freeplane(self, fileName)

    def doExportAllGroups2Freeplane(self):
        mmTitle = QtWidgets.QApplication.translate('main', 'Save Freeplane File')
        proposedName = self.doProposedName(extension='mm', what='ALLGROUPS')
        mmExt = QtWidgets.QApplication.translate('main', 'mm files (*.mm)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, mmTitle, proposedName, mmExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export_xml
        utils_export_xml.exportAllGroups2Freeplane(self, fileName)

    def doExportEleves2Freeplane(self):
        # sélection des élèves à traiter :
        idsEleves, eleves = [], {}
        dialog = prof.ListElevesDlg(
            parent=self, 
            eleves2Freeplane=True, 
            helpMessage=10, 
            helpContextPage='export-eleves-freeplane')
        lastState = self.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            self.enableInterface(dialog, lastState)
            return
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            eleveName = item.text()
            idsEleves.append(id_eleve)
            eleves[id_eleve] = eleveName
        configExport = (
            dialog.comboBoxGroup.currentIndex(),
            dialog.communBLTCheckBox.isChecked(),
            dialog.communOtherCheckBox.isChecked(),
            dialog.persoBLTCheckBox.isChecked(),
            dialog.persoOtherCheckBox.isChecked())
        if len(idsEleves) < 1:
            for i in range(dialog.baseList.count()):
                item = dialog.baseList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                eleveName = item.text()
                idsEleves.append(id_eleve)
                eleves[id_eleve] = eleveName
        self.enableInterface(dialog, lastState)
        mmTitle = QtWidgets.QApplication.translate('main', 'Save Freeplane File')
        if len(idsEleves) == 1:
            proposedName = self.doProposedName(
                extension='mm', what='ELEVE', other=eleveName)
        else:
            proposedName = self.doProposedName(
                extension='mm', what='ELEVES')
        mmExt = QtWidgets.QApplication.translate('main', 'mm files (*.mm)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, mmTitle, proposedName, mmExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export_xml
        utils_export_xml.exportEleves2Freeplane(self, fileName, idsEleves, eleves, configExport)

    def doExportEleves2Dirs(self):
        # sélection des élèves à traiter :
        eleves = {'ORDER' : []}
        dialog = prof.ListElevesDlg(
            parent=self, 
            helpMessage=10, 
            helpContextPage='export-eleves2dirs')
        lastState = self.disableInterface(dialog)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            self.enableInterface(dialog, lastState)
            return
        for i in range(dialog.selectionList.count()):
            item = dialog.selectionList.item(i)
            id_eleve = item.data(QtCore.Qt.UserRole)
            eleveName = item.text()
            eleves['ORDER'].append(id_eleve)
            eleves[id_eleve] = eleveName
        if len(eleves['ORDER']) < 1:
            for i in range(dialog.baseList.count()):
                item = dialog.baseList.item(i)
                id_eleve = item.data(QtCore.Qt.UserRole)
                eleveName = item.text()
                eleves['ORDER'].append(id_eleve)
                eleves[id_eleve] = eleveName
        self.enableInterface(dialog, lastState)
        # il faut aussi sélectionner le dossier de départ :
        title = QtWidgets.QApplication.translate(
            'main', 'Choose the Directory where the subdirectorys will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self, 
            title,
            QtCore.QDir.homePath(),
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        # on y va :
        import utils_export
        utils_export.exportEleves2Dirs(self, directory, eleves)

    def doExportAllEleves2ods(self):
        odsTitle = QtWidgets.QApplication.translate('main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u(
            '{0}/eleves-{1}.ods').format(self.workDir, self.actualVersion['versionName'])
        odsExt = QtWidgets.QApplication.translate('main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(self, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.studentsExportAll2ods(self, fileName)

    def doExportBilan2Freeplane(self):
        import prof_itemsbilans
        comment1 = QtWidgets.QApplication.translate(
            'main', 'Select the assessment you want to export.')
        comment2 = QtWidgets.QApplication.translate(
            'main', 'All items related to this assessment will be in the Freeplane file.')
        comments = utils_functions.u(
            '<p align="center">{0}<br/>{1}</p>'
            '<p align="center">---------------------------<br/></p>').format(comment1, comment2)
        dialog = prof_itemsbilans.ChooseBilanDlg(parent=self, 
            comments=comments, helpContextPage='export-bilan-freeplane')
        dialog.setWindowTitle(QtWidgets.QApplication.translate('main', 'ExportBilan2Freeplane'))
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        id_bilan = dialog.comboBox.itemData(dialog.comboBox.currentIndex())
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'bilans', 'id_bilan', id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, db=self.db_my)
        while query_my.next():
            bilanName = query_my.value(1)

        mmTitle = QtWidgets.QApplication.translate('main', 'Save Freeplane File')
        proposedName = utils_functions.u(
            '{0}/{1}.mm').format(self.workDir, bilanName)
        mmExt = QtWidgets.QApplication.translate('main', 'mm files (*.mm)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(self, mmTitle, proposedName, mmExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export_xml
        utils_export_xml.exportBilan2Freeplane(self, fileName, id_bilan)

    def doExportBilan2Dirs(self):
        # on fait d'abord sélectionner le bilan de départ
        # et le formatage des noms de dossiers à créer :
        import prof_itemsbilans
        comment1 = QtWidgets.QApplication.translate(
            'main', 'Select the assessment you want to export.')
        comment2 = QtWidgets.QApplication.translate(
            'main', 'All items related to this assessment will give a subfolder.')
        comments = utils_functions.u(
            '<p align="center">{0}<br/>{1}</p>').format(comment1, comment2)
        comment1 = QtWidgets.QApplication.translate(
            'main', 'Specify the format of folder names to create.')
        comment2 = QtWidgets.QApplication.translate(
            'main', '{0}: balance name, {1}: balance title, {2}: item name, {3}: item title')
        comment3 = QtWidgets.QApplication.translate(
            'main', 'See help for more explanation.')
        dirsNamesComments = utils_functions.u(
            '<p align="center">---------------------------<br/></p>'
            '<p align="center">{0}<br/>{1}</p>'
            '<p align="center"><b>{2}</b></p>').format(comment1, comment2, comment3)
        dialog = prof_itemsbilans.ChooseBilanForDirsDlg(
            parent=self, 
            comments=comments, 
            dirsNamesComments=dirsNamesComments,
            helpContextPage='export-bilan-dirs')
        dialog.setWindowTitle(QtWidgets.QApplication.translate('main', 'Export a balance in tree'))
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        id_bilan = dialog.comboBox.itemData(dialog.comboBox.currentIndex())
        parameters = (dialog.bilanDirNameEdit.text(), dialog.itemsDirNameEdit.text())
        commandLine_my = utils_db.q_selectAllFromWhere.format(
            'bilans', 'id_bilan', id_bilan)
        query_my = utils_db.queryExecute(commandLine_my, db=self.db_my)
        while query_my.next():
            bilanName = query_my.value(1)
        # il faut aussi sélectionner le dossier de départ :
        title = QtWidgets.QApplication.translate(
            'main', 'Choose the Directory where the subdirectorys will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self, 
            title,
            QtCore.QDir.homePath(),
            QtWidgets.QFileDialog.DontResolveSymlinks | QtWidgets.QFileDialog.ShowDirsOnly)
        if directory == '':
            return
        # on y va :
        import utils_export
        utils_export.exportBilan2Dirs(self, directory, id_bilan, parameters)
        # et on inscrit le formatage dans la base du prof :
        changes = {
            'bilanDirNameFormat': ('', parameters[0]),
            'itemsDirNameFormat': ('', parameters[1])}
        utils_db.changeInConfigTable(self, self.db_my, changes)

    def doExportBilansItems2Csv(self):
        csvTitle = QtWidgets.QApplication.translate('main', 'Save csv File')
        matiereName = prof.diversFromSelection(self)[2]
        matiereCode = self.me['MATIERES']['Matiere2Code'].get(matiereName, '')
        proposedName = utils_functions.u(
            '{0}/{1}-Liste_CPT.csv').format(self.workDir, matiereCode)
        csvExt = QtWidgets.QApplication.translate('main', 'csv files (*.csv)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(self, csvTitle, proposedName, csvExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportBilansItems2Csv(self, fileName)

    def doImportBilansItemsFromCsv(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            self.workDir, 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        if utils_export.importBilansItemsFromCsv(self, fileName):
            self.changeDBMyState()

    def doExportTemplates2Csv(self):
        # on sélectionne les templates à exporter :
        dialog = prof.SelectTemplatesDlg(parent=self)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        selectedTemplates = []
        for template in dialog.templatesListWidget.selectedItems() :
            id_template = template.data(QtCore.Qt.UserRole)
            selectedTemplates.append(id_template)
        # on sélectionne le fichier csv :
        csvTitle = QtWidgets.QApplication.translate('main', 'Save csv File')
        matiereName = prof.diversFromSelection(self)[2]
        matiereCode = self.me['MATIERES']['Matiere2Code'].get(matiereName, '')
        proposedName = utils_functions.u(
            '{0}/{1}-templates.csv').format(self.workDir, matiereCode)
        csvExt = QtWidgets.QApplication.translate('main', 'csv files (*.csv)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, csvTitle, proposedName, csvExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        # on exporte :
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportTemplates2Csv(
            self, fileName, selectedTemplates=selectedTemplates)

    def doImportTemplatesFromCsv(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            self.workDir, 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        if utils_export.importTemplatesFromCsv(self, fileName):
            self.changeDBMyState()

    def doExportCounts2Csv(self):
        # on sélectionne les comptages à exporter :
        dialog = prof.SelectCountsDlg(parent=self)
        if dialog.exec_() != QtWidgets.QDialog.Accepted:
            return
        selectedCounts = []
        for count in dialog.countsListWidget.selectedItems() :
            data = count.data(QtCore.Qt.UserRole)
            selectedCounts.append(data)
        # on sélectionne le fichier csv :
        csvTitle = QtWidgets.QApplication.translate('main', 'Save csv File')
        matiereName = prof.diversFromSelection(self)[2]
        matiereCode = self.me['MATIERES']['Matiere2Code'].get(matiereName, '')
        proposedName = utils_functions.u(
            '{0}/{1}-counts.csv').format(self.workDir, matiereCode)
        csvExt = QtWidgets.QApplication.translate('main', 'csv files (*.csv)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, csvTitle, proposedName, csvExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        # on exporte :
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportCounts2Csv(self, fileName, selectedCounts=selectedCounts)

    def doImportCountsFromCsv(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            self.workDir, 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        if utils_export.importCountsFromCsv(self, fileName):
            self.doShow(doReload=True)
            self.changeDBMyState()

    def doImportAbsencesFromSIECLE(self):
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open xml or zip File'),
            self.workDir, 
            QtWidgets.QApplication.translate('main', 'xml or zip files (*.xml *.zip)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        if QtCore.QFileInfo(fileName).suffix() == 'zip':
            result = utils_filesdirs.unzipFile(fileName, self.tempPath)
            fileName = self.tempPath + '/' + result[1]
        import utils_export_xml
        elevesInGroup = prof.listStudentsInGroup(self, id_groupe=self.id_groupe)
        elevesInGroup = [e[0] for e in elevesInGroup]
        dialog = utils_export_xml.importAbsencesFromSIECLE_Dlg(
            parent=self, fileName=fileName, elevesInGroup=elevesInGroup)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            self.doShow(doReload=True)
            self.changeDBMyState()

    def doExportEleves2Csv(self):
        csvTitle = QtWidgets.QApplication.translate('main', 'Save csv File')
        proposedName = utils_functions.u('{0}/Liste_Eleves.csv').format(self.workDir)
        csvExt = QtWidgets.QApplication.translate('main', 'csv files (*.csv)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, csvTitle, proposedName, csvExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportEleves2Csv(self, fileName)

    def doImportElevesFromCsv(self):
        proposedName = utils_functions.u('{0}/Liste_Eleves.csv').format(self.workDir)
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            proposedName, 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.importElevesFromCsv(self, fileName)
        self.changeDBMyState()
        utils_filesdirs.removeAndCopy(self.dbFileTemp_users, self.dbFile_users)
        utils_filesdirs.removeAndCopy(self.dbFileTemp_commun, self.dbFile_commun)

    def doExportReferentiel2Csv(self):
        csvTitle = QtWidgets.QApplication.translate('main', 'Save csv File')
        proposedName = utils_functions.u('{0}/referentiel.csv').format(self.workDir)
        csvExt = QtWidgets.QApplication.translate('main', 'csv files (*.csv)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self, csvTitle, proposedName, csvExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.exportTable2Csv(self, self.db_commun, 'referentiel', fileName)

    def doImportReferentielFromCsv(self):
        proposedName = utils_functions.u('{0}/referentiel.csv').format(self.workDir)
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            proposedName, 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_export
        utils_export.importReferentielFromCsv(self, fileName)
        self.changeDBMyState()
        utils_filesdirs.removeAndCopy(self.dbFileTemp_commun, self.dbFile_commun)

    def doEditCsvFile(self):
        utils_functions.afficheMessage(self, 'doEditCsvFile', console=True)
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate('main', 'Open csv File'),
            self.workDir, 
            QtWidgets.QApplication.translate('main', 'csv files (*.csv)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.workDir = QtCore.QFileInfo(fileName).absolutePath()
        import utils_csveditor
        dialog = utils_csveditor.CsvEditorDlg(parent=self, csvFileName=fileName)
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)

    def doCreateGroupTrombinoscope(self):
        """
        crée le trombinoscope du groupe sélectionné.
        On commence par envoyer la demande au site,
        puis on affiche le fichier html dans un WebView 
        pour enfin le transformer en pdf
        """
        utils_functions.doWaitCursor()
        try:
            # récupération de la sélection :
            data = prof.diversFromSelection(self)
            id_groupe = data[0]
            groupeName = utils_functions.changeBadChars(data[1])
            elevesInGroup = prof.listStudentsInGroup(self, id_groupe=id_groupe)
            id_students = utils_functions.array2string(elevesInGroup, key=0, separator='|')
            commandLine_my = utils_db.q_selectAllFromWhere.format(
                'groupes', 'id_groupe', id_groupe)
            query_my = utils_db.queryExecute(commandLine_my, db=self.db_my)
            while query_my.next():
                isClasse = int(query_my.value(3))
            # envoi de la demande de création du trombinoscope :
            theUrl = '{0}/pages/verac_getTrombinoscope.php'.format(self.siteUrlPublic)
            postData = utils_functions.u(
                'login={0}&password={1}&groupName={2}&students={3}&class={4}&tr={5}').format(
                    self.me['user'], 
                    self.me['Mdp'], 
                    groupeName, 
                    id_students, 
                    isClasse, 
                    QtWidgets.QApplication.translate('main', 'students'))
            httpClient = utils_web.HttpClient(self)
            httpClient.launch(theUrl, postData)
            while httpClient.state == utils_web.STATE_LAUNCHED:
                QtWidgets.QApplication.processEvents()
            if httpClient.state != utils_web.STATE_OK:
                return
        finally:
            utils_functions.restoreCursor()

        # on crée maintenant le pdf :
        import utils_pdf
        theUrl = '{0}/tmp/{1}-temp.html'.format(self.siteUrlPublic, self.me['userId'])
        pdfTitle = QtWidgets.QApplication.translate('main', 'pdf File')
        proposedName = utils_functions.u('{0}/{1}_{2}.pdf').format(
            self.workDir, 
            QtWidgets.QApplication.translate('main', 'trombinoscope'), 
            groupeName)
        pdfExt = QtWidgets.QApplication.translate('main', 'pdf files (*.pdf)')
        pdfFileName = QtWidgets.QFileDialog.getSaveFileName(
            self, pdfTitle, proposedName, pdfExt)
        pdfFileName = utils_functions.verifyLibs_fileName(pdfFileName)
        if pdfFileName == '':
            return
        utils_functions.doWaitCursor()
        try:
            utils_pdf.webPageToPdf(self, theUrl, pdfFileName)
        finally:
            utils_functions.restoreCursor()
        utils_functions.afficheMsgFinOpen(self, pdfFileName)





    # ------------------------------------------------------------
    # changement de la sélection (période, groupe, tableau) :
    # ------------------------------------------------------------

    def initPeriodeMenu(self):
        self.periodeMenuActions = []
        self.periodeMenu.clear()
        italic = QtGui.QFont()
        italic.setItalic(True)
        if self.me['userMode'] != 'admin':
            allPeriodesText = utils.PERIODES[-1]
            newAct = QtWidgets.QAction(allPeriodesText, self, checkable=True)
            newAct.setData(('periode', -1, allPeriodesText, '', []))
            newAct.triggered.connect(self.selectionChanged)
            self.periodeMenu.addAction(newAct)
            self.periodeMenuActions.append(newAct)
            for indexPeriode in range(utils.NB_PERIODES):
                periode = utils.PERIODES[indexPeriode]
                doItalic = utils.PROTECTED_PERIODS.get(indexPeriode, False)
                newAct = QtWidgets.QAction(periode, self, checkable=True)
                newAct.setData(('periode', indexPeriode, periode, '', []))
                newAct.triggered.connect(self.selectionChanged)
                doItalic = utils.PROTECTED_PERIODS.get(indexPeriode, False)
                if doItalic:
                    newAct.setFont(italic)
                self.periodeMenu.addAction(newAct)
                self.periodeMenuActions.append(newAct)
            annualReportText = utils.PERIODES[999]
            newAct = QtWidgets.QAction(annualReportText, self, checkable=True)
            newAct.setData(('periode', 999, annualReportText, '', []))
            newAct.triggered.connect(self.selectionChanged)
            self.periodeMenu.addAction(newAct)
            self.periodeMenuActions.append(newAct)
        else:
            # en admin, il n'y a pas l'entrée "All Periodes" :
            for indexPeriode in range(utils.NB_PERIODES):
                periode = utils.PERIODES[indexPeriode]
                newAct = QtWidgets.QAction(periode, self, checkable=True)
                newAct.setData(('periode', indexPeriode, periode, '', []))
                newAct.triggered.connect(self.selectionChanged)
                doItalic = utils.PROTECTED_PERIODS.get(indexPeriode, False)
                if doItalic:
                    newAct.setFont(italic)
                self.periodeMenu.addAction(newAct)
                self.periodeMenuActions.append(newAct)

    def selectionChanged(self):
        """
        On a changé de période, groupe ou tableau.
        5 paramètres sont reçus :
            * actionType : 'periode', 'groupe' ou 'tableau'
            * actionIndex : l'index de l'objet (periode, groupe ou tableau)
            * actionLabel : le nom à afficher dans le bouton
            * actionIcon : le nom de l'icône
            * actionList : une liste (peut servir)
        """
        if not(prof.testSuivisModified(self)):
            return
        prof.testAppreciationMustBeSaved(self)
        (actionType, actionIndex, actionLabel, actionIcon, actionList) = self.sender().data()

        # le menu périodes existe aussi en mode admin :
        if self.me['userMode'] == 'admin':
            import admin
            oldPeriode = utils.selectedPeriod
            newPeriode = actionIndex
            # on vérifie que l'admin ne confond pas avec la bascule à la période suivante :
            if (newPeriode > 1) and not(admin.archivedPeriode[newPeriode - 1]):
                try:
                    self.menuLeft.doSelectItem(('Periods', 'SwitchToNextPeriod'))
                except:
                    pass
                m1 = QtWidgets.QApplication.translate('main', '{0} period is not archived.')
                m1 = utils_functions.u(m1.format(utils.PERIODES[newPeriode - 1]))
                m2 = QtWidgets.QApplication.translate(
                    'main', 
                    'You should archive this period before selecting the next.'
                    '<br/>See help automatically displayed in the central part of the interface.')
                m3 = QtWidgets.QApplication.translate(
                    'main', 'Are you certain to want to continue?')
                message = utils_functions.u(
                    '<p align="center">{0}</p>'
                    '<p align="center"><b>{1}</p></b>'
                    '<p>{2}</p>'
                    '<p>{3}</p>').format(utils.SEPARATOR_LINE, m1, m2, m3)
                if utils_functions.messageBox(
                    self, level='warning', 
                    message=message, buttons=['Yes', 'No']) != QtWidgets.QMessageBox.Yes:
                    self.updatePeriodeMenu(oldPeriode)
                    return
            utils.changeSelectedPeriod(newPeriode)
            self.updatePeriodeMenu(newPeriode)
            utils_db.changeInConfigTable(self, admin.db_admin, 
                {'SelectedPeriod': (utils.selectedPeriod, '')}, table='config_calculs')
            if admin.db_resultats != None:
                admin.db_resultats.close()
                del admin.db_resultats
                admin.db_resultats = None
                utils_db.sqlDatabase.removeDatabase('resultats')
                if oldPeriode > -1:
                    utils_db.sqlDatabase.removeDatabase('resultats-{0}'.format(oldPeriode))
                utils_db.closeConnection(self, dbName='commun')
            admin.initPeriode(self, periodeEx=oldPeriode)
            return

        # le reste est en mode prof :
        prof.calcAll(self)
        if actionType == 'periode':
            # on change de période :
            newPeriode = actionIndex
            utils.changeSelectedPeriod(newPeriode)
            self.updatePeriodeMenu(newPeriode)
            prof.tableauxInit(self, newPeriode, self.id_groupe, mustReloadView=False)
            self.doShow(doReload=True)
        elif actionType == 'groupe':
            # on change de groupe :
            self.id_groupe = actionIndex
            self.updateGroupeMenu(self.id_groupe)
            prof.tableauxInit(self, utils.selectedPeriod, self.id_groupe, mustReloadView=False)
            self.doShow(doReload=True)
        elif actionType == 'tableau':
            # on change de tableau :
            self.id_tableau = actionIndex
            self.tableauxInCompil = actionList
            self.updateTableauMenu(self.id_tableau)
            prof.tableauxChanged(self)
            self.doShow(doReload=True)
            lastTableau = '{0}|{1}|{2}'.format(
                utils.selectedPeriod, self.id_groupe, self.id_tableau)
            self.lastTableauForSelection[
                utils.selectedPeriod, self.id_groupe] = self.id_tableau
        elif actionType == 'view':
            # on change de vue :
            self.doShow(viewType=actionIndex)

    def updatePeriodeMenu(self, newPeriode=0):
        for action in self.periodeMenuActions:
            if action.data()[1] == newPeriode:
                action.setChecked(True)
                self.periodeButton.setText(action.data()[2])
            else:
                action.setChecked(False)

    def updateGroupeMenu(self, id_groupe=0):
        for action in self.groupeMenuActions:
            if action.data()[1] == id_groupe:
                action.setChecked(True)
                self.groupeButton.setText(action.data()[2])
            else:
                action.setChecked(False)

    def updateTableauMenu(self, id_tableau=0):
        for action in self.tableauMenuActions:
            if action.data()[1] == id_tableau:
                action.setChecked(True)
                tableauName = action.data()[2]
                tableauLabel = action.data()[3]
                self.tableauButton.setText(tableauName)
                self.tableauButton.setToolTip(tableauLabel)
            else:
                action.setChecked(False)

    # ------------------------
    # dans le tableau actuel:
    # ------------------------

    def doShow(
        self, doReload=False, noRecalc=False, viewType=utils.VIEW_ITEMS):
        """
        Mise à jour de l'affichage.
        si doReload est à True, on essaye de garder la même vue
        (changement de tableau ou de période par exemple)
        sinon, viewType indique ce qui doit être affiché (Items, Bilans, etc).
        Dans un deuxième temps (une fois viewType déterminé), on gère
        ce qui doit être disponible.
        """
        if utils.ICI:
            print('doShow', doReload)
        interface = utils_db.readInConfigDict(
            self.configDict, 'SimplifiedInterface', default_int=1)[0]
        if doReload:
            viewType = self.viewType
        # valeurs par défaut de l'affichage :
        visibles = {
            self.actionChangeValue:False, 
            self.changeValueComboBox:False, 
            self.actionUndo:False, 
            self.actionRedo:False, 
            self.actionLock:False, 
            self.actionShift:False, 
            self.actionAutoselect:False, 
            self.actionRecalc:False, 
            self.actionHideEmptyColumns:False, 
            self.actionValuesUp:False, 
            self.actionValuesDown:False, 
            self.actionSimplify:False, 
            self.actionClearIfUnfavorable:False, 
            self.actionElevePrevious:False, 
            self.actionChangeEleve:False, 
            self.changeEleveComboBox:False, 
            self.eleveSensComboBox:False, 
            self.actionEleveNext:False, 
            self.elevePreviousButton:False, 
            self.eleveNextButton:False, 
            self.editAppreciationGroupBox:False, 
            }
        enableds = {
            self.groupeButton:True, 
            self.groupeMenu:True, 
            self.periodeButton:True, 
            self.periodeMenu:True, 
            self.tableauButton:True, 
            self.tableauMenu:True, 
            self.actionCut:False, 
            self.actionCopy:False, 
            self.actionPaste:False, 
            self.actionSelectAll:False, 
            self.actionUndo:False, 
            self.actionRedo:False, 
            self.fontSizeSlider:False, 
            self.actionFontSizeMore:False, 
            self.actionFontSizeLess:False, 
            self.actionPostSuivis:False, 
            self.notesMenu:False, 
            self.notesMenu2:False, 
            self.countsMenu:False, 
            self.countsMenu2:False, 
            self.actualTableauMenu:False, 
            }
        for objet in self.viewMenuActions[interface]:
            if objet != None:
                enableds[objet] = True
                if objet.data()[1] in (
                    utils.VIEW_ABSENCES, 
                    utils.VIEW_ACCOMPAGNEMENT, 
                    utils.VIEW_CPT_NUM, 
                    utils.VIEW_DNB):
                    visibles[objet] = True
                if (objet.data()[1] == utils.VIEW_COUNTS) \
                    and (interface == 1) \
                    and (self.me['userMode'] != 'PP'):
                    visibles[objet] = False
        # le PP a une action de plus pour gérer les suivis :
        if self.me['userMode'] == 'PP':
            enableds[self.actionGestElevesSuivis] = (
                self.id_groupe != -1)
        # des cas utiles à repérer :
        noGroup = (len(self.groupeMenuActions) == 1)

        # PREMIÈRE PARTIE : DÉTERMINATION DE viewType.
        # selon la sélection, (groupe et période),
        # certaines actions sont rendues inactives 
        # et viewType doit être modifié :
        viewsToDisable = []
        # selon le groupe :
        if noGroup:
            # il n'y a pas de groupe, donc pas de tableau non plus :
            viewsToDisable.extend(list(utils.VIEWS_IF_GROUP))
            viewsToDisable.extend(list(utils.VIEWS_IF_TABLEAU))
            viewsToDisable.append(utils.VIEW_SUIVIS)
        elif self.id_groupe == -1:
            # on a sélectionné tous les groupes :
            viewsToDisable.extend(list(utils.VIEWS_IF_GROUP))
            if utils.selectedPeriod == 999:
                # on a sélectionné bilan annuel :
                viewsToDisable.extend(list(utils.VIEWS_IF_TABLEAU))
        else:
            # donc un groupe est sélectionné
            if utils.selectedPeriod == -1:
                # on a sélectionné toutes les périodes :
                viewsToDisable.extend(list(utils.VIEWS_IF_GROUP))
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_APPRECIATIONS)
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_ACCOMPAGNEMENT)
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_CPT_NUM)
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_DNB)
            elif utils.selectedPeriod == 999:
                # on a sélectionné la période Bilan Annuel :
                viewsToDisable.extend(list(utils.VIEWS_IF_TABLEAU))
                viewsToDisable.extend(list(utils.VIEWS_IF_GROUP))
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_BULLETIN)
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_APPRECIATIONS)
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_ACCOMPAGNEMENT)
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_CPT_NUM)
                utils_functions.removeAll(
                    viewsToDisable, utils.VIEW_DNB)
        # si la liste des tableaux est vide :
        if self.id_tableau == -1:
            viewsToDisable.extend(list(utils.VIEWS_IF_TABLEAU))
        # absences seulement pour la vie scolaire :
        matiereName = prof.diversFromSelection(self)[2]
        if matiereName != self.me['MATIERES']['Code2Matiere']['VS'][1]:
            viewsToDisable.append(utils.VIEW_ABSENCES)
        if (matiereName == self.me['MATIERES']['Code2Matiere']['VS'][1]) \
            or (self.me['userMode'] == 'VS'):
            visibles[self.actionImportAbsencesFromSIECLE] = True
        else:
            visibles[self.actionImportAbsencesFromSIECLE] = False
        # ACCOMPAGNEMENT seulement pour chef d'établissement, 
        # Vie scolaire et PP :
        if not(self.me['userMode'] in ('director', 'VS', 'PP')):
            viewsToDisable.append(utils.VIEW_ACCOMPAGNEMENT)
        # CPT_NUM seulement pour le PP :
        # ICI cptNum
        if not(utils.ICI):
            if self.me['userMode'] != 'PP':
                viewsToDisable.append(utils.VIEW_CPT_NUM)
        # DNB seulement pour le chef d'établissement :
        if self.me['userMode'] != 'director':
            viewsToDisable.append(utils.VIEW_DNB)
        # on efface les doublons :
        viewsToDisable = list(set(viewsToDisable))
        # on peut en déduire la liste des vues disponibles :
        viewsOk = []
        for objet in self.viewMenuActions[interface]:
            if objet != None:
                if objet.data()[1] in viewsToDisable:
                    enableds[objet] = False
                    if objet.data()[1] in (
                        utils.VIEW_ABSENCES, 
                        utils.VIEW_ACCOMPAGNEMENT, 
                        utils.VIEW_CPT_NUM, 
                        utils.VIEW_DNB):
                        visibles[objet] = False
                elif objet.data()[1] > -1:
                    viewsOk.append(objet.data()[1])
        # si viewType n'est pas disponible :
        #   * s'il reste des actions disponibles, on prend la première
        #   * sinon, on retourne utils.VIEW_NOTHING 
        #       (aucune action n'est disponible)
        #   * il y a aussi des cas particuliers à gérer
        #       pour ne pas rester sur la vue NOTHING :
        if viewType in (utils.VIEW_NOTHING, ):
            viewType = utils.VIEW_ITEMS
        if not(viewType in utils.VIEWS_NO):
            if not(viewType in viewsOk):
                if len(viewsOk) > 0:
                    viewType = viewsOk[0]
                else:
                    viewType = utils.VIEW_NOTHING
                # pour ne pas afficher SUIVIS lorsque 
                # c'est la seule qui reste :
                if viewType in (utils.VIEW_SUIVIS, ):
                    viewType = utils.VIEW_NOTHING
        # on vérifie si la dernière saisie est validée :
        if not(doReload):
            prof.testLastNumberValidated(self)
        # maintenant, viewType est connu :
        self.viewType = viewType
        self.helpContext = (utils.VIEWS_HELPPAGE.get(viewType, ''), '')

        # DEUXIÈME PARTIE : DÉTERMINATION DES ACTIONS DISPONIBLES.
        # mise à jour du menu view et du bouton correspondant :
        for action in self.viewMenuActions[interface]:
            if action != None:
                if action.data()[1] == viewType:
                    action.setChecked(True)
                    self.viewButton.setText(action.data()[2])
                    self.viewButton.setIcon(utils.doIcon(action.data()[3]))
                else:
                    action.setChecked(False)
        if viewType == utils.VIEW_NOTHING:
            self.viewButton.setText('')
            self.viewButton.setIcon(utils.doIcon('dialog-cancel'))
        # mise à jour des comboBox de sélection disponibles
        # (groupe-période-tableau) :
        if noGroup:
            # pas de groupe ; on désactive période et tableau
            enableds[self.periodeButton] = False
            enableds[self.periodeMenu] = False
            enableds[self.tableauButton] = False
            enableds[self.tableauMenu] = False
        elif viewType in utils.VIEWS_IF_GROUP:
            # vue de type groupe ; on désactive tableau s'il y en a
            # (pour permettre la création d'un tableau s'il n'y en a pas)
            if self.id_tableau != -1:
                enableds[self.tableauButton] = False
                enableds[self.tableauMenu] = False
        elif viewType == utils.VIEW_SUIVIS:
            # idem en vue suivis
            if self.id_tableau != -1:
                enableds[self.tableauButton] = False
                enableds[self.tableauMenu] = False
        if viewType in (
            utils.VIEW_SUIVIS, 
            utils.VIEW_ACCOMPAGNEMENT, 
            utils.VIEW_CPT_NUM, 
            utils.VIEW_DNB):
            # en vues suivis et dnb, on désactive aussi période
            enableds[self.periodeButton] = False
            enableds[self.periodeMenu] = False
        # mise à jour des autres actions disponibles :
        if not(viewType in utils.VIEWS_NO):
            enableds[self.actionSelectAll] = True
            enableds[self.actionCopy] = True
            enableds[self.fontSizeSlider] = True
            enableds[self.actionFontSizeMore] = True
            enableds[self.actionFontSizeLess] = True
            if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
                if viewType in utils.VIEWS_WITH_EVALUATIONS:
                    self.undoList, self.redoList = [], []
                if viewType == utils.VIEW_ELEVE:
                    visibles[self.actionElevePrevious] = True
                    visibles[self.actionChangeEleve] = True
                    visibles[self.changeEleveComboBox] = True
                    visibles[self.eleveSensComboBox] = True
                    visibles[self.actionEleveNext] = True
                    visibles[self.elevePreviousButton] = True
                    visibles[self.eleveNextButton] = True
                    self.changeEleveInit()
                    if self.id_sens > 1:
                        visibles[self.editAppreciationGroupBox] = True
                elif viewType == utils.VIEW_SUIVIS:
                    enableds[self.actionPostSuivis] = True
            else:
                if viewType in utils.VIEWS_WITH_EVALUATIONS:
                    visibles[self.actionLock] = True
                    visibles[self.actionAutoselect] = True
                    if viewType in utils.VIEWS_WITH_COLORS:
                        state = utils_db.readInConfigDict(
                            self.configDict, 
                            'actionChangeValue', 
                            default_int=1)[0]
                        if state == 1:
                            visibles[self.actionChangeValue] = True
                            visibles[self.changeValueComboBox] = True
                    if viewType in (utils.VIEW_ABSENCES, utils.VIEW_COUNTS, utils.VIEW_CPT_NUM):
                        visibles[self.actionValuesUp] = True
                        visibles[self.actionValuesDown] = True
                    visibles[self.actionUndo] = True
                    visibles[self.actionRedo] = True
                    self.undoList, self.redoList = [], []
                    enableds[self.actionCut] = True
                    enableds[self.actionPaste] = True
                viewsWithRecalc = (
                    utils.VIEW_STATS_TABLEAU, 
                    utils.VIEW_BILANS, 
                    utils.VIEW_BULLETIN, 
                    utils.VIEW_STATS_GROUPE, 
                    utils.VIEW_ELEVE, 
                    utils.VIEW_NOTES)
                if viewType in viewsWithRecalc:
                    visibles[self.actionRecalc] = True
                if viewType in utils.VIEWS_WITH_APPRECIATIONS:
                    enableds[self.actionPaste] = True
                if viewType == utils.VIEW_ITEMS:
                    visibles[self.actionShift] = True
                    visibles[self.actionSimplify] = True
                    visibles[self.actionClearIfUnfavorable] = True
                    visibles[self.actionHideEmptyColumns] = True
                elif viewType == utils.VIEW_ELEVE:
                    visibles[self.actionElevePrevious] = True
                    visibles[self.actionChangeEleve] = True
                    visibles[self.changeEleveComboBox] = True
                    visibles[self.eleveSensComboBox] = True
                    visibles[self.actionEleveNext] = True
                    visibles[self.elevePreviousButton] = True
                    visibles[self.eleveNextButton] = True
                    self.changeEleveInit()
                    if self.id_sens > 1:
                        visibles[self.editAppreciationGroupBox] = True
                elif viewType == utils.VIEW_SUIVIS:
                    enableds[self.actionPostSuivis] = True
                elif viewType == utils.VIEW_NOTES:
                    enableds[self.notesMenu] = True
                    enableds[self.notesMenu2] = True
                elif viewType == utils.VIEW_COUNTS:
                    visibles[self.actionShift] = True
                    enableds[self.countsMenu] = True
                    enableds[self.countsMenu2] = True
                enableds[self.actualTableauMenu] = True

        # TROISIÈME PARTIE : MISE À JOUR DE L'AFFICHAGE.
        # on recalcule le tableau actuel si besoin :
        if not(noRecalc):
            prof.calcAll(self)
        # affichage en fonction de viewType :
        if viewType == utils.VIEW_NOTHING:
            if self.stackedWidget.currentIndex() != 1:
                self.stackedWidget.setCurrentIndex(1)
            templateFile = utils_functions.doLocale(
                utils.LOCALE, 'files/md/messages', '.html')
            template = QtCore.QFileInfo(templateFile).baseName()
            if noGroup:
                mdFile = utils_functions.doLocale(
                    utils.LOCALE, 'translations/profViewFirst', '.md')
                outFileName = utils_markdown.md2html(
                    self, mdFile, template=template)
            else:
                mdFile = utils_functions.doLocale(
                    utils.LOCALE, 'translations/profViewNothing', '.md')
                outFileName = utils_markdown.md2html(
                    self, mdFile, template=template)
            url = QtCore.QUrl().fromLocalFile(outFileName)
            self.helpWebView.load(url)
        elif viewType == utils.VIEW_NEWS:
            if self.stackedWidget.currentIndex() != 1:
                self.stackedWidget.setCurrentIndex(1)
            contextUrl = '{0}main-todo.html'.format(utils.HELPPAGE_BEGIN)
            url = utils_web.doUrl(contextUrl)
            self.helpWebView.load(url)
        elif viewType == utils.VIEW_DBSTATE:
            if self.stackedWidget.currentIndex() != 1:
                self.stackedWidget.setCurrentIndex(1)
            prof.doDBStateFile(self)
            htmlFile = '{0}/md/profDBState.html'.format(self.tempPath)
            url = QtCore.QUrl().fromLocalFile(htmlFile)
            self.helpWebView.load(url)
        elif viewType == utils.VIEW_WEBSITE:
            if self.stackedWidget.currentIndex() != 2:
                self.stackedWidget.setCurrentIndex(2)
            utils_web.connectToSiteEtab(self)
        elif viewType == utils.VIEW_RANDOM_HELP:
            import random
            if self.stackedWidget.currentIndex() != 1:
                self.stackedWidget.setCurrentIndex(1)
            page = random.choice(utils.RANDOM_HELP_PAGES)
            if page == 'news':
                contextUrl = '{0}main-todo.html'.format(
                    utils.HELPPAGE_BEGIN)
            elif page in ('tech-calculs', ):
                contextUrl = '{0}help-{1}.html'.format(
                    utils.HELPPAGE_BEGIN, page)
            elif page in ('lsu', ):
                contextUrl = '{0}main-lsu.html'.format
                (utils.HELPPAGE_BEGIN, page)
            else:
                contextUrl = '{0}help-prof-{1}.html'.format(
                    utils.HELPPAGE_BEGIN, page)
            if utils.ICI:
                print(contextUrl, type(contextUrl))
            url = utils_web.doUrl(contextUrl)
            self.helpWebView.load(url)
        elif viewType == utils.VIEW_ELEVE:
            if self.stackedWidget.currentIndex() != 0:
                self.stackedWidget.setCurrentIndex(0)
            self.showEleve()
        else:
            if self.stackedWidget.currentIndex() != 0:
                self.stackedWidget.setCurrentIndex(0)
            prof.viewChanged(self, noRecalc=noRecalc)
            # s'il n'y a pas de lignes à afficher :
            if len(self.model.rows) == 0:
                if viewType == utils.VIEW_SUIVIS:
                    if self.stackedWidget.currentIndex() != 1:
                        self.stackedWidget.setCurrentIndex(1)
                    templateFile = utils_functions.doLocale(
                        utils.LOCALE, 'files/md/messages', '.html')
                    template = QtCore.QFileInfo(templateFile).baseName()
                    mdFile = utils_functions.doLocale(
                        utils.LOCALE, 'translations/profNoSuivis', '.md')
                    outFileName = utils_markdown.md2html(
                        self, mdFile, template=template)
                    url = QtCore.QUrl().fromLocalFile(outFileName)
                    self.helpWebView.load(url)
        # visibles et enableds sont maintenant connus :
        for objet in visibles:
            objet.setVisible(visibles[objet])
        for objet in enableds:
            objet.setEnabled(enableds[objet])
        # pour ne pas afficher les séparateurs inutiles de la barre d'outils :
        previousWasSeparator = True
        lastSeparators = []
        for action in self.toolBar.actions():
            if action.isSeparator():
                if previousWasSeparator:
                    # on n'affiche qu'un séparateur :
                    action.setVisible(False)
                else:
                    # d'abord on le rend visible au cas où :
                    action.setVisible(True)
                    # et on le met dans la liste des derniers séparateurs :
                    lastSeparators.append(action)
                previousWasSeparator = True
            elif action.isVisible():
                # on réinitialise la liste des derniers séparateurs :
                lastSeparators = []
                previousWasSeparator = False
        for action in lastSeparators:
            action.setVisible(False)


    # bouton de recalcul des bilans, stats etc:
    def doRecalc(self):
        prof.calcAll(self, forcer=True)
        if self.viewType == utils.VIEW_ELEVE:
            self.showEleve()
        else:
            prof.viewChanged(self)


    def doRecalcAllGroupes(self):
        utils_functions.afficheMessage(
            self, 'doRecalcAllGroupes', console=True)
        prof.calcAllGroupes(self, withProgress=True)
        prof.viewChanged(self)

    def changeEleveInit(self):
        """
        on essaye de récupérer le même élève 
        (en cas de changement de période par exemple)
        """
        try:
            id_eleve = self.changeEleveComboBox.itemData(
                self.changeEleveComboBox.currentIndex(), QtCore.Qt.UserRole)
        except:
            id_eleve = -1
        # lastEleve permet d'enregistrer les modifs de l'appréciation
        # surtout en changeant d'élève par la combobox
        self.lastEleve = id_eleve
        self.changeEleveComboBox.clear()
        elevesInGroup = prof.listStudentsInGroup(
            self, id_groupe=self.id_groupe)
        index = 0
        indexToSelect = 0
        for (id_eleve2, ordre, eleveName) in elevesInGroup:
            self.changeEleveComboBox.addItem(eleveName, id_eleve2)
            if id_eleve == id_eleve2:
                indexToSelect = index
            index += 1
        self.changeEleveComboBox.setCurrentIndex(indexToSelect)

    def changeEleveChanged(self):
        prof.testAppreciationMustBeSaved(self)
        self.showEleve()

    def eleveSensChanged(self):
        prof.testAppreciationMustBeSaved(self)
        currentIndex = self.eleveSensComboBox.currentIndex()
        self.id_sens = self.eleveSensComboBox.itemData(currentIndex)
        self.showEleve()

    def doElevePrevious(self):
        prof.testAppreciationMustBeSaved(self)
        self.changeEleveComboBox.setCurrentIndex(
            self.changeEleveComboBox.currentIndex() - 1)
        if self.changeEleveComboBox.currentIndex() < 0:
            self.changeEleveComboBox.setCurrentIndex(
                self.changeEleveComboBox.count() - 1)
        self.showEleve()

    def doEleveNext(self):
        prof.testAppreciationMustBeSaved(self)
        self.changeEleveComboBox.setCurrentIndex(
            self.changeEleveComboBox.currentIndex() + 1)
        if self.changeEleveComboBox.currentIndex() < 0:
            self.changeEleveComboBox.setCurrentIndex(0)
        self.showEleve()

    def showEleve(self):
        eleve = self.changeEleveComboBox.currentText()
        id_eleve = self.changeEleveComboBox.itemData(
            self.changeEleveComboBox.currentIndex(), QtCore.Qt.UserRole)
        # lastEleve permet d'enregistrer les modifs de l'appréciation
        # surtout en changeant d'élève par la combobox
        self.lastEleve = id_eleve
        prof.viewChanged(self, id_eleve=id_eleve)

    # évaluations:
    def changeValueInit(self):
        self.changeValueComboBox.clear()
        for i in range(utils.NB_COLORS + 1):
            self.changeValueComboBox.addItem(
                QtGui.QIcon(utils.pixmapsItems[utils.itemsValues[i]]), 
                utils.affichages[utils.itemsValues[i]][1], 
                i)
        self.changeValueComboBox.insertSeparator(7)
        for i in range(utils.NB_COLORS + 1):
            self.changeValueComboBox.addItem(
                QtGui.QIcon(utils.pixmapsItems[utils.itemsValues[i]]), 
                '+' + utils.affichages[utils.itemsValues[i]][1], 
                100 + i)
        self.changeValueComboBox.insertSeparator(12)
        self.changeValueComboBox.addItem(
            utils.doIcon('edit-clear'),
            QtWidgets.QApplication.translate('main', 'Clear'),
            -1)
        self.changeValueComboBox.addItem(
            utils.doIcon('edit-copy'), 
            QtWidgets.QApplication.translate('main', 'Copy'),
            -2)
        self.changeValueComboBox.addItem(
            utils.doIcon('edit-paste'), 
            QtWidgets.QApplication.translate('main', 'Paste'),
            -3)
        self.doAdd = False

    def changeValueChanged(self):
        i = self.changeValueComboBox.currentIndex()
        i = self.changeValueComboBox.itemData(i)
        self.changeValue(i)

    def contextMenuEvent(self, event):
        # pas de menu pour l'admin :
        if self.me['userMode'] == 'admin':
            return
        # création et remplissage du menu :
        menu = QtWidgets.QMenu(self)
        if self.viewType > -1:
            if utils.PROTECTED_PERIODS.get(utils.selectedPeriod, False):
                # pour savoir si une case est sélectionnée :
                hasSelection = self.view.selectionState['hasSelection']
                menu.addAction(self.actionCut)
                menu.addAction(self.actionCopy)
                menu.addAction(self.actionPaste)
                menu.addAction(self.actionSelectAll)
                menu.addSeparator()
                if self.viewType == utils.VIEW_ITEMS:
                    menu.addAction(self.actionSimplify)
                    menu.addAction(self.actionClearIfUnfavorable)
                elif self.viewType == utils.VIEW_NOTES:
                    menu.addMenu(self.notesMenu)
                elif self.viewType == utils.VIEW_ABSENCES:
                    menu.addAction(self.actionValuesUp)
                    menu.addAction(self.actionValuesDown)
                elif self.viewType == utils.VIEW_CPT_NUM:
                    menu.addAction(self.actionValuesUp)
                    menu.addAction(self.actionValuesDown)
                elif self.viewType == utils.VIEW_COUNTS:
                    menu.addAction(self.actionSimplify)
                    menu.addAction(self.actionClearIfUnfavorable)
                    menu.addAction(self.actionValuesUp)
                    menu.addAction(self.actionValuesDown)
                    menu.addMenu(self.countsMenu)
                menu.addSeparator()
                if self.viewType == utils.VIEW_ITEMS:
                    menu.addMenu(self.actualTableauMenu)
                menu.addSeparator()
            else:
                # pour savoir si une case est sélectionnée :
                hasSelection = self.view.selectionState['hasSelection']
                if len(self.view.selectionState['evals']) > 0:
                    # il faut afficher le menu d'évaluations :
                    for i in range(utils.NB_COLORS + 1):
                        newAction = QtWidgets.QAction(
                            utils.affichages[utils.itemsValues[i]][1], self)
                        newAction.setIcon(
                            QtGui.QIcon(
                                utils.pixmapsItems[utils.itemsValues[i]]))
                        newAction.setData(i)
                        newAction.triggered.connect(self.doPopUpMenu)
                        menu.addAction(newAction)
                    menu.addSeparator()
                    for i in range(utils.NB_COLORS + 1):
                        newAction = QtWidgets.QAction(
                            '+ ' + utils.affichages[utils.itemsValues[i]][1], 
                            self)
                        newAction.setIcon(
                            QtGui.QIcon(
                                utils.pixmapsItems[utils.itemsValues[i]]))
                        newAction.setData(100 + i)
                        newAction.triggered.connect(self.doPopUpMenu)
                        menu.addAction(newAction)
                    menu.addSeparator()
                menu.addAction(self.actionCut)
                menu.addAction(self.actionCopy)
                menu.addAction(self.actionPaste)
                menu.addAction(self.actionSelectAll)
                menu.addSeparator()
                if self.viewType == utils.VIEW_ITEMS:
                    menu.addAction(self.actionSimplify)
                    menu.addAction(self.actionClearIfUnfavorable)
                elif self.viewType == utils.VIEW_NOTES:
                    menu.addMenu(self.notesMenu)
                elif self.viewType == utils.VIEW_ABSENCES:
                    menu.addAction(self.actionValuesUp)
                    menu.addAction(self.actionValuesDown)
                elif self.viewType == utils.VIEW_CPT_NUM:
                    menu.addAction(self.actionValuesUp)
                    menu.addAction(self.actionValuesDown)
                elif self.viewType == utils.VIEW_COUNTS:
                    menu.addAction(self.actionSimplify)
                    menu.addAction(self.actionClearIfUnfavorable)
                    menu.addAction(self.actionValuesUp)
                    menu.addAction(self.actionValuesDown)
                    menu.addMenu(self.countsMenu)
                menu.addSeparator()
                if self.viewType == utils.VIEW_ITEMS:
                    menu.addMenu(self.actualTableauMenu)
                menu.addSeparator()
        # dans tous les cas, on affiche ce qui suit :
        menu.addMenu(self.toolBarsMenu)
        # on affiche le menu :
        menu.exec_(event.globalPos())


    def doPopUpMenu(self):
        action = self.sender()
        i = action.data()
        self.changeValue(i)


    def changeValue(self, index):
        if index > 99:
            value = utils.itemsValues[index - 100]
            prof.changeValue(self, value, doAdd=True)
            if self.viewType != utils.VIEW_SUIVIS:
                self.whatIsModified['actualTableau'] = True
        elif index > -1:
            value = utils.itemsValues[index]
            prof.changeValue(self, value)
            if self.viewType != utils.VIEW_SUIVIS:
                self.whatIsModified['actualTableau'] = True
        elif index == -1:
            prof.copy(self)
            prof.changeValue(self, '')
            if self.viewType != utils.VIEW_SUIVIS:
                self.whatIsModified['actualTableau'] = True
        elif index == -2:
            prof.copy(self)
        elif index == -3:
            prof.paste(self)
            if self.viewType != utils.VIEW_SUIVIS:
                self.whatIsModified['actualTableau'] = True


    def doUndo(self):
        prof.undo(self)
        if self.viewType != utils.VIEW_SUIVIS:
            self.whatIsModified['actualTableau'] = True

    def doRedo(self):
        prof.redo(self)
        if self.viewType != utils.VIEW_SUIVIS:
            self.whatIsModified['actualTableau'] = True

    def doSelectAll(self):
        self.view.selectAll()

    def doSimplify(self):
        prof.simplify(self)
        self.whatIsModified['actualTableau'] = True

    def doClearIfUnfavorable(self):
        prof.clearIfUnfavorable(self)
        self.whatIsModified['actualTableau'] = True

    def doLock(self):
        if self.actionLock.isChecked():
            self.actionLock.setStatusTip(
                QtWidgets.QApplication.translate('main', 'UnLockStatusTip'))
            self.actionLock.setText(
                QtWidgets.QApplication.translate('main', 'Unlock'))
        else:
            self.actionLock.setStatusTip(
                QtWidgets.QApplication.translate('main', 'LockStatusTip'))
            self.actionLock.setText(
                QtWidgets.QApplication.translate('main', 'Lock'))

    def doShift(self):
        if self.actionShift.isChecked():
            self.actionShift.setStatusTip(
                QtWidgets.QApplication.translate('main', 'UnShiftStatusTip'))
            self.actionShift.setText(
                QtWidgets.QApplication.translate('main', 'UnShift'))
        else:
            self.actionShift.setStatusTip(
                QtWidgets.QApplication.translate('main', 'ShiftStatusTip'))
            self.actionShift.setText(
                QtWidgets.QApplication.translate('main', 'Shift'))

    def doAutoselect(self):
        if not(self.initialisation):
            self.autoselect += 1
            if self.autoselect == 3:
                self.autoselect = 0
        if self.autoselect == 0:
            self.actionAutoselect.setIcon(utils.doIcon('autoselect-no'))
            self.actionAutoselect.setText(
                QtWidgets.QApplication.translate('main', 'Autoselect-no'))
        elif self.autoselect == 1:
            self.actionAutoselect.setIcon(utils.doIcon('autoselect-1'))
            self.actionAutoselect.setText(
                QtWidgets.QApplication.translate('main', 'Autoselect-1'))
        else:
            self.actionAutoselect.setIcon(utils.doIcon('autoselect-2'))
            self.actionAutoselect.setText(
                QtWidgets.QApplication.translate('main', 'Autoselect-2'))

    def doValuesUp(self):
        prof.valuesUpDown(self, 1)

    def doValuesDown(self):
        prof.valuesUpDown(self, -1)

    def keyPressEvent(self, event):
        if event.key() == utils.KEY_ESCAPE:
            self.close()

    def toolBarsChanged(self):
        """
        on modifie l'affichage des toolBars
        """
        if self.sender().data() == 0:
            menuBarVisible = self.sender().isChecked()
            self.menuBar().setVisible(menuBarVisible)
            self.allMenuButton.setVisible(not(menuBarVisible))
        elif self.sender().data() == 1:
            if self.sender().isChecked():
                self.toolBar.setVisible(True)
                if self.me['userMode'] != 'admin':
                    self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)
                else:
                    self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
            else:
                self.removeToolBar(self.toolBar)

    def doReloadLayout(self, what='initial'):
        """
        on replace les barres d'outils en fonction du paramètre what
            initial : tel qu'à l'origine
            saved : comme enregistré dans le fichier prof
            last : état précédent (utilisé avec stackedWidget)
        """
        if self.sender() == self.actionLoadInitialLayout:
            what = 'initial'
        elif self.sender() == self.actionLoadSavedLayout:
            what = 'saved'
        if what != 'last':
            self.actualToolBarState = what
        if what == 'saved':
            if not('saved' in self.mainWindowState):
                # on lit la base prof
                self.mainWindowState['saved'] = [1, 1, 1, '']
                # état du menu :
                self.mainWindowState['saved'][0] = utils_db.readInConfigDict(
                    self.configDict, 'menuBarState', default_int=1)[0]
                # état de la barre principale :
                self.mainWindowState['saved'][1] = utils_db.readInConfigDict(
                    self.configDict, 'toolBarState', default_int=1)[0]
                # état de la fenêtre :
                state = utils_db.readInConfigDict(
                    self.configDict, 'MainWindowState')[1]
                if utils.PYTHONVERSION >= 30:
                    # résolution d'un problème avec les QByteArray
                    #(b'aaa' au lieu de 'aaa') :
                    if state[:2] in ("b'", 'b"'):
                        state = state[2:-1]
                #state = QtCore.QByteArray().fromHex(state)
                state = QtCore.QByteArray().fromHex(
                    QtCore.QByteArray().append(state))
                self.mainWindowState['saved'][3] = state
        # état du menu :
        menuBarState = (self.mainWindowState[what][0] == 1)
        self.toolBarsMenuActions[0].setChecked(menuBarState)
        self.menuBar().setVisible(menuBarState)
        self.allMenuButton.setVisible(not(menuBarState))
        # état de la barre principale :
        toolBarState = (self.mainWindowState[what][1] == 1)
        self.toolBarsMenuActions[1].setChecked(toolBarState)
        self.toolBar.setVisible(toolBarState)
        if toolBarState:
            if self.me['userMode'] != 'admin':
                self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)
            else:
                self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)
        else:
            self.removeToolBar(self.toolBar)
        self.restoreState(
            QtCore.QByteArray().append(self.mainWindowState[what][3]))

    def doSaveCurrentLayout(self, what='saved'):
        """
        on enregistre l'état des barres d'outils 
        en fonction du paramètre what
            saved : enregistré dans le fichier prof
            last : état actuel (utilisé avec stackedWidget)
        """
        if self.sender() == self.actionSaveCurrentLayout:
            what = 'saved'
        self.mainWindowState[what] = [0, 0, 0, self.saveState()]
        if self.menuBar().isVisible():
            self.mainWindowState[what][0] = 1
        if self.toolBar.isVisible():
            self.mainWindowState[what][1] = 1
        if what == 'saved':
            # on enregistre dans la base prof :
            changes = {
                'menuBarState': (self.mainWindowState['saved'][0], ''),
                'toolBarState': (self.mainWindowState['saved'][1], ''),
                'MainWindowState': (
                    0, 
                    self.mainWindowState['saved'][3].toHex())}
            utils_db.changeInConfigTable(self, self.db_my, changes)
            self.changeDBMyState()

    def doConfigureToolBar(self):
        self.mustReflectChanges = False
        import utils_config
        dialog = utils_config.ConfigDlg(parent=self, page='ToolBar')
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)
        self.createToolBar()

    def changeFontSize(self, value):
        """
        on empêche le curseur de changer d'aspect
        sinon c'est pas trop joli
        """
        utils_functions.changeNoWaitCursor(True)
        try:
            self.model.boldFont.setPointSize(value)
            self.model.normalFont.setPointSize(value)
            self.doShow(doReload=True, noRecalc=True)
            if self.fontSizeSlider.value() != value:
                self.fontSizeSlider.setValue(value)
            translatedMessage = QtWidgets.QApplication.translate(
                'main', 'FontSize: {0}')
            translatedMessage = utils_functions.u(
                translatedMessage).format(value)
            self.fontSizeSlider.setToolTip(translatedMessage)
        finally:
            utils_functions.changeNoWaitCursor(False)

    def fontSizeSliderReleased(self):
        self.doShow(doReload=True, noRecalc=True)

    def doFontSizeMore(self):
        newValue = self.fontSizeSlider.value() + 1
        if newValue <= self.fontSizeSlider.maximum():
            self.changeFontSize(newValue)

    def doFontSizeLess(self):
        newValue = self.fontSizeSlider.value() - 1
        if newValue >= self.fontSizeSlider.minimum():
            self.changeFontSize(newValue)




    ###########################################
    ###########################################
    # Tout le monde:
    ###########################################
    ###########################################




    # ------------------------
    # aaa:
    # ------------------------


    ###########################################
    # aide etc:
    ###########################################

    def about(self):
        """
        Affiche la fenêtre À propos
        """
        import utils_about
        dialog = utils_about.AboutDlg(self, utils.LOCALE)
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)

    def contextHelpPage(self):
        """
        Ouvre l'aide contextuelle.
            * indiquée par le tupple self.helpContext = ('page', 'base').
        Si 'base' est vide, on affiche une page liée à l'utilisateur 
        (prof ou admin).
        Mais on peut préciser 'base' pour ouvrir par exemple 
        une page d'aide prof alors qu'on est connecté en admin.
        """
        base = self.helpContext[1]
        if base == '':
            base = self.me['userMode']
            if base != 'admin':
                base = 'prof'
        utils_functions.openContextHelp(self.helpContext[0], base)

    def helpContentsPage(self):
        """
        blablabla
        """
        utils_functions.openInBrowser(
            utils.HELPPAGE_BEGIN + 'help-homepage.html')

    def helpProjetPage(self):
        """
        blablabla
        """
        utils_functions.openInBrowser(utils.PROGWEB)

    def bugReport(self):
        """
        Faire un rapport de bug.
        """
        import utils_about
        dialog = utils_about.BugReportDlg(
            parent=self, helpPage=utils.HELPPAGE_BEGIN + 'help-bug.html')
        lastState = self.disableInterface(dialog)
        dialog.exec_()
        self.enableInterface(dialog, lastState)

    def doCreateLinuxDesktopFile(self):
        """
        crée un lanceur (fichier .desktop).
        """
        title = QtWidgets.QApplication.translate(
            'main', 
            'Choose the Directory where the desktop file will be created')
        directory = QtWidgets.QFileDialog.getExistingDirectory(
            self, 
            title,
            QtCore.QDir.homePath(),
            QtWidgets.QFileDialog.DontResolveSymlinks | \
                QtWidgets.QFileDialog.ShowDirsOnly)
        if directory != '':
            utils_functions.doWaitCursor()
            try:
                result = utils_filesdirs.createLinuxDesktopFile(
                    self.beginDir, 
                    directory, 
                    'verac', 
                    utils.PROGNAME)
                if self.me['userMode'] == 'admin':
                    # aussi pour le sheduler :
                    title = QtWidgets.QApplication.translate(
                        'main', 'VERAC Scheduler')
                    utils_filesdirs.createLinuxDesktopFile(
                        self.beginDir, 
                        directory, 
                        'verac_scheduler', 
                        QtWidgets.QApplication.translate(
                            'main', 'VERAC Scheduler'))
            finally:
                utils_functions.restoreCursor()

    def doCreateWinShortcutFile(self):
        """
        crée un raccourci (fichier .lnk).
        """
        try:
            import pythoncom
        except ImportError:
            message = QtWidgets.QApplication.translate(
                'main', 'You must install pywin32 to do this action.')
            allMessage = utils_functions.u(
                '<p align="center">__________________________</p>'
                '<p align="center"><b>{0}</b></p>'
                '<p></p>').format(message)
            QtWidgets.QMessageBox.warning(
                self, utils.PROGNAME, allMessage)
            return
        utils_functions.doWaitCursor()
        try:
            title = QtWidgets.QApplication.translate(
                'main', 'Shortcut to')
            utils_filesdirs.createWinShortcutFile(
                progFileName='Verac', 
                progName=utils.PROGNAME, 
                progDescription=utils_functions.u('{0} {1}').format(
                    title, 
                    utils.PROGNAME), 
                progIcon='verac')
            if self.me['userMode'] == 'admin':
                # aussi pour le sheduler :
                import os
                path = utils_functions.u('{0}libs').format(os.sep)
                utils_filesdirs.createWinShortcutFile(
                    progFileName='verac_scheduler', 
                    progName='verac_scheduler', 
                    progDescription=utils_functions.u('{0} {1}').format(
                        title, 
                        QtWidgets.QApplication.translate(
                            'main', 'VERAC Scheduler')), 
                    progIcon='verac_scheduler', 
                    path=path)

        finally:
            utils_functions.restoreCursor()



    # ------------------------
    # aaa:
    # ------------------------

    ###########################################
    # création de l'interface, des actions et des menus:
    ###########################################

    def createTranslations(self):
        """
        résoudre les erreurs de traductions
        après ouverture d'un dialog système
        (Qt semble écraser les traductions)
        Items → Éléments
        """
        self.TR_ITEMS = QtWidgets.QApplication.translate('main', 'Items')

    def disableInterface(self, dialog):
        """
        Cache les barres d'outils lors de l'appel d'un dialog
        affiché dans le stackedWidget.
        On retourne aussi :
            lastIndex : le widget affiché précédemment dans le stackedWidget
            lastText : le texte du statusBar
        """
        if self.me['userMode'] != 'admin':
            self.doSaveCurrentLayout(what='last')
        lastIndex = self.stackedWidget.currentIndex()
        lastText = self.statusBarLabel.text()
        self.setWindowTitle(dialog.windowTitle())
        if self.me['userMode'] != 'admin':
            for widget in (self.menuBar(), self.toolBar, self.topBar):
                widget.setVisible(False)
        else:
            for widget in (self.menuBar(), self.toolBar):
                widget.setVisible(False)
        self.statusBarLabel.setText('')
        self.mainStatusBar.showMessage('')
        newIndex = self.stackedWidget.addWidget(dialog)
        self.stackedWidget.setCurrentIndex(newIndex)
        return (lastIndex, lastText)

    def enableInterface(self, dialog, lastState=(0, '')):
        """
        Replace les barres d'outils après l'appel d'un dialog
        affiché dans le stackedWidget.
        On remet aussi l'état précédent via lastState :
            widget affiché dans le stackedWidget
            texte du statusBar
        et le titre de la fenêtre 
        (qui a pu être changé si on a modifié la base)
        """
        if self.closing:
            return
        self.stackedWidget.removeWidget(dialog)
        self.stackedWidget.setCurrentIndex(lastState[0])
        self.statusBarLabel.setText(lastState[1])
        self.setWindowTitle(self.windowTitleAll)
        if self.me['userMode'] != 'admin':
            for widget in (self.menuBar(), self.toolBar, self.topBar):
                widget.setVisible(True)
        else:
            for widget in (self.menuBar(), self.toolBar):
                widget.setVisible(True)
        if self.me['userMode'] != 'admin':
            self.doReloadLayout(what='last')
            self.doShow(doReload=True)

    def createInterface(self):
        """
        blablabla
        """
        # le reste de l'interface:
        self.mainStatusBar = QtWidgets.QStatusBar()
        self.setStatusBar(self.mainStatusBar)
        self.statusBarLabel = QtWidgets.QLabel()
        self.statusBarLabel.setMinimumWidth(100)
        self.mainStatusBar.addPermanentWidget(self.statusBarLabel)

        self.toolBar = QtWidgets.QToolBar(
            QtWidgets.QApplication.translate('main', 'ToolBar'))
        self.toolBar.setObjectName('toolBar')
        self.toolBar.setIconSize(
            QtCore.QSize(
                utils.STYLE['PM_LargeIconSize'], 
                utils.STYLE['PM_LargeIconSize']))
        if self.me['userMode'] != 'admin':
            self.addToolBar(QtCore.Qt.LeftToolBarArea, self.toolBar)
            self.topBarLayouts = {
                'ORDER': ('default', 'changeEleve'), 
                'default': QtWidgets.QHBoxLayout(), 
                'changeEleve': QtWidgets.QHBoxLayout(), 
                }
            vBoxLayout = QtWidgets.QVBoxLayout()
            vBoxLayout.setContentsMargins(0, 0, 0, 0)
            for what in self.topBarLayouts['ORDER']:
                self.topBarLayouts[what].setContentsMargins(0, 0, 0, 0)
                vBoxLayout.addLayout(self.topBarLayouts[what])
            self.topBar = QtWidgets.QGroupBox()
            self.topBar.setLayout(vBoxLayout)
            self.topBar.setFlat(True)
        else:
            self.addToolBar(QtCore.Qt.TopToolBarArea, self.toolBar)

    def doAppreciationChanged(self):
        prof.doAppreciationChanged(self)

    def createActions(self):
        """
        blablabla
        """

        if self.me['userMode'] == 'admin':
            #################################################
            # actions admin:
            #################################################

            # ------------------------
            # dans fichier:
            # ------------------------
            self.actionOpenAdminDir_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'OpenAdminDir'), 
                self, 
                icon=utils.doIcon('folder-admin'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'OpenAdminDirStatusTip'))
            self.actionOpenAdminDir_admin.triggered.connect(
                self.doOpenAdminDir_admin)

            self.actionLaunchScheduler_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'LaunchScheduler'), 
                self, 
                icon=utils.doIcon('recovery-scheduler'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'LaunchSchedulerStatusTip'))
            self.actionLaunchScheduler_admin.triggered.connect(
                self.doLaunchScheduler_admin)


            # ------------------------
            # dans administration:
            # ------------------------

            # configuration:

            # création des bases:
            self.actionEditCsvFile_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'EditCsvFile'), 
                self, 
                icon=utils.doIcon('csv-edit'),
                statusTip=QtWidgets.QApplication.translate('main', 'EditCsvFileStatusTip'))
            self.actionEditCsvFile_admin.triggered.connect(self.doEditCsvFile_admin)

            self.actionEditDBFile_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'EditDBFile'), 
                self, 
                icon=utils.doIcon('server-database'),
                statusTip=QtWidgets.QApplication.translate('main', 'EditDBFileStatusTip'))
            self.actionEditDBFile_admin.triggered.connect(self.doEditDBFile_admin)

            self.actionCreateAdminTableFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateAdminTableFromCsv'), 
                self, 
                icon=utils.doIcon('csv-to-database'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'CreateAdminTableFromCsvStatusTip'))
            self.actionCreateAdminTableFromCsv.triggered.connect(self.doCreateAdminTableFromCsv)

            self.actionCreateCommunTableFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateCommunTableFromCsv'), 
                self, 
                icon=utils.doIcon('csv-to-database'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'CreateCommunTableFromCsvStatusTip'))
            self.actionCreateCommunTableFromCsv.triggered.connect(
                self.doCreateCommunTableFromCsv)

            self.actionCreateAllCommunTablesFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateAllCommunTablesFromCsv'), 
                self, 
                icon=utils.doIcon('csv-to-database'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'CreateAllCommunTablesFromCsvStatusTip'))
            self.actionCreateAllCommunTablesFromCsv.triggered.connect(
                self.doCreateAllCommunTablesFromCsv)

            self.actionGestUsers_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Users management'), 
                self, 
                icon=utils.doIcon('users'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'To manage the list of users '
                    '(teachers and students)'))
            self.actionGestUsers_admin.triggered.connect(self.doGestUsers_admin)

            self.actionGestAddresses = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Management of addresses'), 
                self, 
                icon=utils.doIcon('addresses'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'To manage manually the addresses of students'))
            self.actionGestAddresses.triggered.connect(self.doGestAddresses)

            self.actionGestPhotos = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Management of photos'), 
                self, 
                icon=utils.doIcon('photos'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'To manage the photos of students'))
            self.actionGestPhotos.triggered.connect(self.doGestPhotos)


            # gestion des utilisateurs:
            self.actionDownloadDBUsers_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'DownloadDBUsers'), 
                self, 
                icon=utils.doIcon('svn-update'),
                statusTip=QtWidgets.QApplication.translate('main', 'DownloadDBUsersStatusTip'))
            self.actionDownloadDBUsers_admin.triggered.connect(self.doDownloadDBUsers_admin)

            self.actionUploadDBUsers_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'UploadDBUsers'), 
                self, 
                icon=utils.doIcon('svn-commit'),
                statusTip=QtWidgets.QApplication.translate('main', 'UploadDBUsersStatusTip'))
            self.actionUploadDBUsers_admin.triggered.connect(self.doUploadDBUsers_admin)



            self.actionConfigurationSchool_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Configuring School'), 
                self, 
                icon=utils.doIcon('school'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'General configuration of the school (name, address, ...)'))
            self.actionConfigurationSchool_admin.triggered.connect(
                self.doConfigurationSchool_admin)

            self.actionClasses_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Classes'), 
                self, 
                icon=utils.doIcon('groupes'),
                statusTip=QtWidgets.QApplication.translate('main', 'ClassesStatusTip'))
            self.actionClasses_admin.triggered.connect(self.doClasses_admin)

            self.actionSharedCompetences_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Shared competences'), 
                self, 
                icon=utils.doIcon('certificate-edit'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'To change the list of shared competences'))
            self.actionSharedCompetences_admin.triggered.connect(
                self.doSharedCompetences_admin)



            self.actionShowMdpProfsNoChange_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ShowMdpProfsNoChange'), 
                self, 
                icon=utils.doIcon('password-prof'),
                statusTip=QtWidgets.QApplication.translate('main', 'ShowMdpProfsNoChangeStatusTip'))
            self.actionShowMdpProfsNoChange_admin.triggered.connect(self.doShowMdpProfsNoChange_admin)


            self.actionCreateConnectionFile_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateConnectionFile'), 
                self, 
                icon=utils.doIcon('extension-tgz'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateConnectionFileStatusTip'))
            self.actionCreateConnectionFile_admin.triggered.connect(
                self.doCreateConnectionFile_admin)



            self.actionUploadDBCommun_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'UploadDBCommun'), 
                self, 
                icon=utils.doIcon('svn-commit'),
                statusTip=QtWidgets.QApplication.translate('main', 'UploadDBCommunStatusTip'))
            self.actionUploadDBCommun_admin.triggered.connect(self.doUploadDBCommun_admin)



            self.actionCreateYearArchiveAndClear_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateYearArchiveAndClear'), 
                self, 
                icon=utils.doIcon('year-new'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'CreateYearArchiveAndClearStatusTip'))
            self.actionCreateYearArchiveAndClear_admin.triggered.connect(
                self.doCreateYearArchiveAndClear_admin)

            self.actionCreateYearArchive_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateYearArchive'), 
                self, 
                icon=utils.doIcon('extension-tgz'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateYearArchiveStatusTip'))
            self.actionCreateYearArchive_admin.triggered.connect(self.doCreateYearArchive_admin)

            self.actionClearForNewYear_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Cleaning for a new year'), 
                self, 
                icon=utils.doIcon('edit-clear'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Clears the current year data (databases, files, etc.)'))
            self.actionClearForNewYear_admin.triggered.connect(self.doClearForNewYear_admin)




            # ------------------------
            # dans Edition:
            # ------------------------

            # updateBilans:
            self.actionUpdateBilans_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Update results'), 
                self, 
                icon=utils.doIcon('update-results'),
                statusTip=QtWidgets.QApplication.translate('main', 'Update results (only new files)'))
            self.actionUpdateBilans_admin.triggered.connect(self.doUpdateBilans_admin)

            self.actionUpdateBilansComplete_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Update Results (recalculate everything)'), 
                self, 
                icon=utils.doIcon('update-results'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Update Results (recalculate everything)'))
            self.actionUpdateBilansComplete_admin.triggered.connect(
                self.doUpdateBilansComplete_admin)

            self.actionUpdateBilansSelect_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Update results of a selection'), 
                self, 
                icon=utils.doIcon('update-results'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Select the teachers and students which we want to update the results'))
            self.actionUpdateBilansSelect_admin.triggered.connect(
                self.doUpdateBilansSelect_admin)

            self.actionCalculateReferential_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Calculation of the referential'), 
                self, 
                icon=utils.doIcon('xcalc'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Calculates the validation proposals of the competences referential'))
            self.actionCalculateReferential_admin.triggered.connect(
                self.doCalculateReferential_admin)

            self.actionClearReferential_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Clear the referential'), 
                self, 
                icon=utils.doIcon('clear'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'To erase records from previous years'))
            self.actionClearReferential_admin.triggered.connect(
                self.doClearReferential_admin)

            self.actionUploadDBResultats_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Post the results DB'), 
                self, 
                icon=utils.doIcon('svn-commit'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Post the results DB (and possible proposals to the referential)'))
            self.actionUploadDBResultats_admin.triggered.connect(self.doUploadDBResultats_admin)





            # CreateBilansBLT :
            self.actionCreateReports_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Create reports'), 
                self, 
                icon=utils.doIcon('extension-pdf'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Create bulletins or other reports'))
            self.actionCreateReports_admin.triggered.connect(
                self.doCreateReports_admin)

            self.actionExportNotes2ods_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportNotes2ods'), 
                self, 
                icon=utils.doIcon('extension-ods'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportNotes2odsStatusTip'))
            self.actionExportNotes2ods_admin.triggered.connect(self.doExportNotes2ods_admin)

            self.actionExportStudentsResults2ods_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Export results from referential to ods'), 
                self, 
                icon=utils.doIcon('extension-ods'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Export results of a list of students to an ODF Spreadsheet File '
                    '(takes account of previous years)'))
            self.actionExportStudentsResults2ods_admin.triggered.connect(
                self.doExportStudentsResults2ods_admin)

            self.actionExportBLTEvals2ods_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Export bulletin assessments to ODS'), 
                self, 
                icon=utils.doIcon('extension-ods'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Create an ODS file containing details of '
                    'assessments of the bulletin '
                    '(one tab per class).'))
            self.actionExportBLTEvals2ods_admin.triggered.connect(self.doExportBLTEvals2ods_admin)

            self.actionShowFields_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ShowFields'), 
                self, 
                icon=utils.doIcon('text-xml'),
                statusTip=QtWidgets.QApplication.translate('main', 'ShowFieldsStatusTip'))
            self.actionShowFields_admin.triggered.connect(self.doShowFields_admin)

            self.actionCreateHtmlModele_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Create a school report or other record template'), 
                self, 
                icon=utils.doIcon('extension-html'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Create an HTML template '
                    '(for a school report, balances report, referential report, etc.)'))
            self.actionCreateHtmlModele_admin.triggered.connect(
                self.doCreateHtmlModele_admin)

            self.actionExport2LSU_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate(
                    'main', 'Export LSU'), 
                self, 
                icon=utils.doIcon('en'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'To export results to LSU (Livret Scolaire Unique)'))
            self.actionExport2LSU_admin.triggered.connect(self.doExport2LSU_admin)



            # ------------------------
            # dans VÉRAC:
            # ------------------------


            self.actionVerifProfsFiles_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Checking teachers files'), 
                self, 
                icon=utils.doIcon('verif-prof'),
                statusTip=QtWidgets.QApplication.translate('main', 'List files and provides information on their content'))
            self.actionVerifProfsFiles_admin.triggered.connect(self.doVerifProfsFiles_admin)

            self.actionCheckSchoolReports_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CheckSchoolReports'), 
                self, 
                icon=utils.doIcon('verif-bulletin'),
                statusTip=QtWidgets.QApplication.translate('main', 'CheckSchoolReportsStatusTip'))
            self.actionCheckSchoolReports_admin.triggered.connect(self.doCheckSchoolReports_admin)

            self.actionLastReport_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'LastReport'), 
                self, 
                icon=utils.doIcon('view-pim-journal'),
                statusTip=QtWidgets.QApplication.translate('main', 'LastReportStatusTip'))
            self.actionLastReport_admin.triggered.connect(self.doLastReport_admin)

            self.actionRedoMdp_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'RedoMdp'), 
                self, 
                icon=utils.doIcon('dialog-password'),
                statusTip=QtWidgets.QApplication.translate('main', 'RedoMdpStatusTip'))
            self.actionRedoMdp_admin.triggered.connect(self.doRedoMdp_admin)


            # ------------------------
            # aaaaaaaaaaaaaaaaaaaa:
            # ------------------------

            self.actionDocumentsManagement_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Documents management'), 
                self, 
                icon=utils.doIcon('documents'),
                statusTip=QtWidgets.QApplication.translate('main', 'DocumentsManagementStatusTip'))
            self.actionDocumentsManagement_admin.triggered.connect(self.doDocumentsManagement_admin)


            # archives :
            self.actionSwitchToNextPeriod_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SwitchToNextPeriod'), 
                self, 
                icon=utils.doIcon('year-new'),
                statusTip=QtWidgets.QApplication.translate('main', 'SwitchToNextPeriodStatusTip'))
            self.actionSwitchToNextPeriod_admin.triggered.connect(self.doSwitchToNextPeriod_admin)

            self.actionListProtectedPeriods_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate(
                    'main', 'List of Protected periods'), 
                self, 
                icon=utils.doIcon('object-locked'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'To view and edit the list of protected periods '
                    '(common and results bases)'))
            self.actionListProtectedPeriods_admin.triggered.connect(self.doListProtectedPeriods_admin)

            self.actionLockedClasses_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate(
                    'main', 'Locked classes'), 
                self, 
                icon=utils.doIcon('object-locked'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'To view and edit the list of locked classes'))
            self.actionLockedClasses_admin.triggered.connect(self.doLockedClasses_admin)


            self.actionTestUpdateWebSite = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Test version of the website'), 
                self, 
                icon=utils.doIcon('database-compare'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Check for an update of the web site'))
            self.actionTestUpdateWebSite.triggered.connect(self.doTestUpdateWebSite)

            self.actionUpdateWebSite = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Update website'), 
                self, 
                icon=utils.doIcon('net-download'))
            self.actionUpdateWebSite.triggered.connect(self.doUpdateWebSite)

            self.actionWebSiteState = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Website state'), 
                self, 
                icon=utils.doIcon('net-config'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'To change what is available on the website'))
            self.actionWebSiteState.triggered.connect(self.doWebSiteState)

            self.actionWebSiteConfig = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Website configuration'), 
                self, 
                icon=utils.doIcon('net-config'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'To update or change the configuration of your website'))
            self.actionWebSiteConfig.triggered.connect(self.doWebSiteConfig)

            # compteur :
            self.actionShowCompteurAnalyze_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ShowCompteurAnalyze'), 
                self, 
                icon=utils.doIcon('statistics'),
                statusTip=QtWidgets.QApplication.translate('main', 'ShowCompteurAnalyzeStatusTip'))
            self.actionShowCompteurAnalyze_admin.triggered.connect(self.doShowCompteurAnalyze_admin)

            # gestion des fichiers de structures pour les profs :
            self.actionGestStructuresProfs_admin = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Management of teacher structures'), 
                self, 
                icon=utils.doIcon('structures'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Structures files made available for teachers on the school website'))
            self.actionGestStructuresProfs_admin.triggered.connect(self.doGestStructuresProfs_admin)

        else:
            #################################################
            # actions prof:
            #################################################
            self.actionSaveDBMy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SaveDBMy'), 
                self, 
                icon=utils.doIcon('document-save'), 
                shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Save), 
                statusTip=QtWidgets.QApplication.translate('main', 'SaveDBMyStatusTip'))
            self.actionSaveDBMy.triggered.connect(self.doSaveDBMy)

            self.actionPostDBMy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'PostDBMy'), 
                self, 
                icon=utils.doIcon('net-upload'),
                statusTip=QtWidgets.QApplication.translate('main', 'PostDBMyStatusTip'))
            self.actionPostDBMy.triggered.connect(self.doPostDBMy)

            self.actionSaveAndPostDBMy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SaveAndPostDBMy'), 
                self, 
                icon=utils.doIcon('save-and-post'),
                statusTip=QtWidgets.QApplication.translate('main', 'SaveAndPostDBMyStatusTip'))
            self.actionSaveAndPostDBMy.triggered.connect(self.doSaveAndPostDBMy)

            self.actionAutoSaveDBMy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'AutoSaveDBMy'), 
                self, 
                icon=utils.doIcon('auto-save'),
                statusTip=QtWidgets.QApplication.translate('main', 'AutoSaveDBMyStatusTip'),
                checkable=True, 
                checked=False)
            self.actionAutoSaveDBMy.triggered.connect(self.doAutoSaveDBMy)

            self.actionSaveAsDBMy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SaveAsDBMy...'), 
                self, 
                icon=utils.doIcon('document-save-as'),
                statusTip=QtWidgets.QApplication.translate('main', 'SaveAsDBMyStatusTip'))
            self.actionSaveAsDBMy.triggered.connect(self.doSaveAsDBMy)

            self.actionCompareDBMy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CompareDBMy'), 
                self, 
                icon=utils.doIcon('database-compare'),
                statusTip=QtWidgets.QApplication.translate('main', 'CompareDBMyStatusTip'))
            self.actionCompareDBMy.triggered.connect(self.doCompareDBMy)

            self.actionDownloadDBMy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'DownloadDBMy'), 
                self, 
                icon=utils.doIcon('net-download'),
                statusTip=QtWidgets.QApplication.translate('main', 'DownloadDBMyStatusTip'))
            self.actionDownloadDBMy.triggered.connect(self.doDownloadDBMy)

            self.actionPostSuivis = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'PostSuivis'), 
                self, 
                icon=utils.doIcon('net-upload'),
                statusTip=QtWidgets.QApplication.translate('main', 'PostSuivisStatusTip'))
            self.actionPostSuivis.triggered.connect(self.doPostSuivis)

            self.actionSwitchInterface = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SwitchToCompleteInterface'), 
                self, 
                icon=utils.doIcon('interface-switch'),
                statusTip=QtWidgets.QApplication.translate('main', 'SwitchToCompleteInterfaceStatusTip'),
                checkable=True, 
                checked=False)
            self.actionSwitchInterface.triggered.connect(self.doSwitchInterface)


            self.actionChangeMdp = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Change password'), 
                self, 
                icon=utils.doIcon('dialog-password'),
                statusTip=QtWidgets.QApplication.translate('main', 'ChangeMdpStatusTip'))
            self.actionChangeMdp.triggered.connect(self.doChangeMdp)

            self.actionTestUpdateDBUsersCommun = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'TestUpdateDBUsersCommun'), 
                self, 
                icon=utils.doIcon('net-download'),
                statusTip=QtWidgets.QApplication.translate('main', 'TestUpdateDBUsersCommunStatusTip'))
            self.actionTestUpdateDBUsersCommun.triggered.connect(self.doTestUpdateDBUsersCommun)

            self.actionCheckCommunBilans = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CheckCommunBilans'), 
                self, 
                icon=utils.doIcon('bilan-check'),
                statusTip=QtWidgets.QApplication.translate('main', 'CheckCommunBilansStatusTip'))
            self.actionCheckCommunBilans.triggered.connect(self.doCheckCommunBilans)

            self.actionDownloadSuivisDB = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'DownloadSuivisDB'), 
                self, 
                icon=utils.doIcon('net-download'),
                statusTip=QtWidgets.QApplication.translate('main', 'DownloadSuivisDBStatusTip'))
            self.actionDownloadSuivisDB.triggered.connect(self.doDownloadSuivisDB)




            self.actionCreateEtab = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateEtab'), 
                self, 
                icon=utils.doIcon('preferences-other'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateEtabStatusTip'))
            self.actionCreateEtab.triggered.connect(self.doCreateEtab)

            self.actionAdminVersionPerso = QtWidgets.QAction(
                QtWidgets.QApplication.translate(
                    'main', 'Become administrator of the Personal Version'), 
                self, 
                icon=utils.doIcon('preferences-other'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Allows you to manage the lists of students, edit bulletins, etc.StatusTip'))
            self.actionAdminVersionPerso.triggered.connect(self.doAdminVersionPerso)

            self.actionReturnVersionPerso = QtWidgets.QAction(
                QtWidgets.QApplication.translate(
                    'main', 'Return to the simple management'), 
                self, 
                icon=utils.doIcon('preferences-other'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Return to the simple management of personal Version'))
            self.actionReturnVersionPerso.triggered.connect(self.doReturnVersionPerso)


            # définition des actions du menu view :
            viewTypeDic = {
                utils.VIEW_NEWS: (
                    QtWidgets.QApplication.translate('main', 'News'), 
                    'help-news',
                    QtWidgets.QApplication.translate('main', 'ShowNewsStatusTip')),
                utils.VIEW_DBSTATE: (
                    QtWidgets.QApplication.translate('main', 'DBState'), 
                    'database-check',
                    QtWidgets.QApplication.translate('main', 'ShowDBStateStatusTip')),
                utils.VIEW_RANDOM_HELP: (
                    QtWidgets.QApplication.translate('main', 'RandomHelp'), 
                    'help-random',
                    QtWidgets.QApplication.translate('main', 'ShowRandomHelpStatusTip')),
                utils.VIEW_WEBSITE: (
                    QtWidgets.QApplication.translate('main', 'GotoSiteEtab'), 
                    'net-go',
                    QtWidgets.QApplication.translate('main', 'GotoSiteEtabStatusTip')),

                utils.VIEW_ITEMS: (
                    self.TR_ITEMS, 
                    'items', 
                    QtWidgets.QApplication.translate('main', 'ShowItemsStatusTip')),
                utils.VIEW_STATS_TABLEAU: (
                    QtWidgets.QApplication.translate('main', 'StatsTableau'), 
                    'statistics', 
                    QtWidgets.QApplication.translate('main', 'ShowStatsTableauStatusTip')),
                utils.VIEW_BILANS: (
                    QtWidgets.QApplication.translate('main', 'Balances'), 
                    'bilans',
                    QtWidgets.QApplication.translate('main', 'ShowBilansStatusTip')),
                utils.VIEW_BULLETIN: (
                    QtWidgets.QApplication.translate('main', 'Bulletin'), 
                    'bulletin', 
                    QtWidgets.QApplication.translate('main', 'ShowBulletinStatusTip')),
                utils.VIEW_APPRECIATIONS: (
                    QtWidgets.QApplication.translate('main', 'Appreciations'), 
                    'appreciations', 
                    QtWidgets.QApplication.translate('main', 'ShowAppreciationsStatusTip')),
                utils.VIEW_STATS_GROUPE: (
                    QtWidgets.QApplication.translate('main', 'StatsGroupe'), 
                    'statistics', 
                    QtWidgets.QApplication.translate('main', 'ShowStatsGroupeStatusTip')),
                utils.VIEW_ELEVE: (
                    QtWidgets.QApplication.translate('main', 'Per student'), 
                    'eleve', 
                    QtWidgets.QApplication.translate('main', 'ShowEleveStatusTip')),
                utils.VIEW_NOTES: (
                    QtWidgets.QApplication.translate('main', 'Notes'), 
                    'notes', 
                    QtWidgets.QApplication.translate('main', 'ShowNotesStatusTip')),
                utils.VIEW_SUIVIS: (
                    QtWidgets.QApplication.translate('main', 'Followed students'), 
                    'suivis', 
                    QtWidgets.QApplication.translate('main', 'ShowSuivisStatusTip')),
                utils.VIEW_ABSENCES: (
                    QtWidgets.QApplication.translate('main', 'Absences'), 
                    'absences', 
                    QtWidgets.QApplication.translate('main', 'ShowAbsencesStatusTip')),
                utils.VIEW_COUNTS: (
                    QtWidgets.QApplication.translate('main', 'Counts'), 
                    'counter', 
                    QtWidgets.QApplication.translate('main', 'ShowCountsStatusTip')),
                utils.VIEW_ACCOMPAGNEMENT: (
                    QtWidgets.QApplication.translate('main', 'Guiding'), 
                    'guiding', 
                    QtWidgets.QApplication.translate('main', 'ShowGuidingStatusTip')),
                utils.VIEW_CPT_NUM: (
                    QtWidgets.QApplication.translate('main', 'Digital skills'), 
                    'digital-skills', 
                    QtWidgets.QApplication.translate('main', 'ShowDigitalSkillsStatusTip')),
                utils.VIEW_DNB: (
                    QtWidgets.QApplication.translate('main', 'DNB Opinion'), 
                    'validations', 
                    QtWidgets.QApplication.translate('main', 'ShowDNBStatusTip')),
                }
            self.viewMenuActions = ([], [])
            for viewType in utils.VIEWS_MENU:
                if viewType == utils.VIEW_WEBSITE and not(self.NetOK):
                    continue
                elif viewType != utils.VIEW_SEPARATOR:
                    (name, iconFile, statusTip) = viewTypeDic[viewType]
                    newAct = QtWidgets.QAction(
                        name, 
                        self, 
                        icon=utils.doIcon(iconFile), 
                        statusTip=statusTip, 
                        checkable=True, 
                        checked=False)
                    newAct.setData(('view', viewType, name, iconFile, []))
                    newAct.triggered.connect(self.selectionChanged)
                    self.viewMenuActions[0].append(newAct)
                else:
                    self.viewMenuActions[0].append(None)
            for viewType in utils.VIEWS_MENU_SIMPLIFIED:
                if viewType != utils.VIEW_SEPARATOR:
                    (name, iconFile, statusTip) = viewTypeDic[viewType]
                    newAct = QtWidgets.QAction(
                        name, 
                        self, 
                        icon=utils.doIcon(iconFile), 
                        statusTip=statusTip, 
                        checkable=True, 
                        checked=False)
                    newAct.setData(('view', viewType, name, iconFile, []))
                    newAct.triggered.connect(self.selectionChanged)
                    self.viewMenuActions[1].append(newAct)
                else:
                    self.viewMenuActions[1].append(None)

            self.actionGestGroupes = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Manage groups of students'), 
                self, 
                icon=utils.doIcon('groupes'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Create or modify the groups of students'))
            self.actionGestGroupes.triggered.connect(self.doGestGroupes)

            self.actionCreateMultipleGroupesClasses = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateMultipleGroupesClasses'), 
                self, 
                icon=utils.doIcon('groupes'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateMultipleGroupesClassesStatusTip'))
            self.actionCreateMultipleGroupesClasses.triggered.connect(self.doCreateMultipleGroupesClasses)

            self.actionCreateGroupe = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Create Group'), 
                self, 
                icon=utils.doIcon('groupe-add'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Directly opens the window for creating a new group'))
            self.actionCreateGroupe.triggered.connect(self.doCreateGroupe)

            self.actionShowDeletedEleves = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ShowDeletedEleves'), 
                self, 
                icon=utils.doIcon('groupes'),
                statusTip=QtWidgets.QApplication.translate('main', 'ShowDeletedElevesStatusTip'),
                checkable=True)
            self.actionShowDeletedEleves.triggered.connect(self.doShowDeletedEleves)

            self.actionClearDeletedEleves = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Clear students deleted from groups'), 
                self, 
                icon=utils.doIcon('eleves-clear'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Clear data of students deleted from groups'))
            self.actionClearDeletedEleves.triggered.connect(self.doClearDeletedEleves)

            self.actionGestItems = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Manage items'), 
                self, 
                icon=utils.doIcon('items'),
                statusTip=QtWidgets.QApplication.translate('main', 'GestItemsStatusTip'))
            self.actionGestItems.triggered.connect(self.doGestItems)

            self.actionGestItemsBilans = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Manage items and balances'), 
                self, 
                icon=utils.doIcon('items'),
                statusTip=QtWidgets.QApplication.translate('main', 'GestItemsBilansStatusTip'))
            self.actionGestItemsBilans.triggered.connect(self.doGestItemsBilans)

            self.actionItemsBilansTable = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ItemsBilansTable'), 
                self, 
                icon=utils.doIcon('items'),
                statusTip=QtWidgets.QApplication.translate('main', 'ItemsBilansTableStatusTip'))
            self.actionItemsBilansTable.triggered.connect(self.doItemsBilansTable)

            self.actionLinkItemsToBilan = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Link several items to a balance'), 
                self, 
                icon=utils.doIcon('items_to_bilan'),
                statusTip=QtWidgets.QApplication.translate('main', 'LinkItemsToBilanStatusTip'))
            self.actionLinkItemsToBilan.triggered.connect(self.doLinkItemsToBilan)

            self.actionLinkBilans = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'LinkBilans'), 
                self, 
                icon=utils.doIcon('bilans-link'),
                statusTip=QtWidgets.QApplication.translate('main', 'LinkBilansStatusTip'))
            self.actionLinkBilans.triggered.connect(self.doLinkBilans)

            self.actionGestAdvices = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Manage advices'), 
                self, 
                icon=utils.doIcon('comments'),
                statusTip=QtWidgets.QApplication.translate('main', 'GestAdvicesStatusTip'))
            self.actionGestAdvices.triggered.connect(self.doGestAdvices)


            self.actionCreateTableau = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Create a table'), 
                self, 
                icon=utils.doIcon('tableau-add'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateTableauStatusTip'))
            self.actionCreateTableau.triggered.connect(self.doCreateTableau)

            self.actionCopyTableau = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CopyTableau'), 
                self, 
                icon=utils.doIcon('tableau-copy'),
                statusTip=QtWidgets.QApplication.translate('main', 'CopyTableauStatusTip'))
            self.actionCopyTableau.triggered.connect(self.doCopyTableau)

            self.actionSupprTableau = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SupprTableau'), 
                self, 
                icon=utils.doIcon('tableau-delete'),
                statusTip=QtWidgets.QApplication.translate('main', 'SupprTableauStatusTip'))
            self.actionSupprTableau.triggered.connect(self.doSupprTableau)

            self.actionCopyItemsFromTo = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Copy the items in a table to others'), 
                self, 
                icon=utils.doIcon('tableau-copy-items'),
                statusTip=QtWidgets.QApplication.translate('main', 'CopyItemsFromToStatusTip'))
            self.actionCopyItemsFromTo.triggered.connect(self.doCopyItemsFromTo)

            self.actionMergeTableaux = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Merge 2 tables'), 
                self, 
                icon=utils.doIcon('tableaux-merge'),
                statusTip=QtWidgets.QApplication.translate('main', 'MergeTableauxStatusTip'))
            self.actionMergeTableaux.triggered.connect(self.doMergeTableaux)

            self.actionSortGroupesTableaux = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Order of groups and tables'), 
                self, 
                icon=utils.doIcon('tableaux-sort'),
                statusTip=QtWidgets.QApplication.translate('main', 'SortGroupesTableauxStatusTip'))
            self.actionSortGroupesTableaux.triggered.connect(self.doSortGroupesTableaux)

            self.actionCreateMultipleTableaux = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateMultipleTableaux'), 
                self, 
                icon=utils.doIcon('tableaux-add'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateMultipleTableauxStatusTip'))
            self.actionCreateMultipleTableaux.triggered.connect(self.doCreateMultipleTableaux)

            self.actionTableauxTemplates = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Tables templates'), 
                self, 
                icon=utils.doIcon('template'),
                statusTip=QtWidgets.QApplication.translate('main', 'Tables templatesStatusTip'))
            self.actionTableauxTemplates.triggered.connect(self.doTableauxTemplates)

            self.actionCreateTemplateFromTableau = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateTemplateFromTableau'), 
                self, 
                icon=utils.doIcon('template-add'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateTemplateFromTableauStatusTip'))
            self.actionCreateTemplateFromTableau.triggered.connect(self.doCreateTemplateFromTableau)

            self.actionLinkTableauTemplate = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'LinkTableauTemplate'), 
                self, 
                icon=utils.doIcon('template-link'),
                statusTip=QtWidgets.QApplication.translate('main', 'LinkTableauTemplateStatusTip'))
            self.actionLinkTableauTemplate.triggered.connect(self.doLinkTableauTemplate)

            self.actionUnlinkTableauTemplate = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'UnlinkTableauTemplate'), 
                self, 
                icon=utils.doIcon('template-unlink'),
                statusTip=QtWidgets.QApplication.translate('main', 'UnlinkTableauTemplateStatusTip'))
            self.actionUnlinkTableauTemplate.triggered.connect(self.doUnlinkTableauTemplate)




            self.actionEditActualTableau = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'EditActualTableau'), 
                self, 
                icon=utils.doIcon('tableau-edit'),
                statusTip=QtWidgets.QApplication.translate('main', 'EditActualTableauStatusTip'))
            self.actionEditActualTableau.triggered.connect(self.doEditActualTableau)

            self.actionListItems = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Modify the list of items (current table)'), 
                self, 
                icon=utils.doIcon('items'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Change the list of items in the current table (to add, remove, reorder)'))
            self.actionListItems.triggered.connect(self.doListItems)

            self.actionListVisibleItems = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Choose the displayed items'), 
                self, 
                icon=utils.doIcon('items'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Managing the display of the current items array (visibility and order)'))
            self.actionListVisibleItems.triggered.connect(self.doListVisibleItems)

            self.actionHideEmptyColumns = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Hide empty columns'), 
                self, 
                icon=utils.doIcon('items'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Displays only columns with assessments'))
            self.actionHideEmptyColumns.triggered.connect(self.doHideEmptyColumns)

            self.actionManageProfiles = QtWidgets.QAction(
                QtWidgets.QApplication.translate(
                    'main', 'Manage profiles (which will be on the bulletins)'), 
                self, 
                icon=utils.doIcon('bulletin'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Create or edit profiles, balance sheets shown in the bulletins, etc.'))
            self.actionManageProfiles.triggered.connect(self.doManageProfiles)

            self.actionGestElevesSuivis = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'GestElevesSuivis'), 
                self, 
                icon=utils.doIcon('suivis'),
                statusTip=QtWidgets.QApplication.translate('main', 'GestElevesSuivisStatusTip'))
            self.actionGestElevesSuivis.triggered.connect(self.doGestElevesSuivis)


            self.actionExportActualVue2pdf = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportActualVue2pdf'), 
                self, 
                icon=utils.doIcon('extension-pdf'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportActualVue2pdfStatusTip'))
            self.actionExportActualVue2pdf.triggered.connect(self.doExportActualVue2pdf)

            self.actionExportActualVue2Print = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportActualVue2Print'), 
                self, 
                icon=utils.doIcon('document-print'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportActualVue2PrintStatusTip'))
            self.actionExportActualVue2Print.triggered.connect(self.doExportActualVue2Print)

            self.actionExportActualVue2ods = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportActualVue2ods'), 
                self, 
                icon=utils.doIcon('extension-ods'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportActualVue2odsStatusTip'))
            self.actionExportActualVue2ods.triggered.connect(self.doExportActualVue2ods)

            self.actionExportActualGroup2ods = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportActualGroup2ods'), 
                self, 
                icon=utils.doIcon('extension-ods'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportActualGroup2odsStatusTip'))
            self.actionExportActualGroup2ods.triggered.connect(self.doExportActualGroup2ods)

            self.actionExportStructure2ods = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportStructure2ods'), 
                self, 
                icon=utils.doIcon('extension-ods'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportStructure2odsStatusTip'))
            self.actionExportStructure2ods.triggered.connect(self.doExportStructure2ods)

            self.actionExportItemsList2ods = QtWidgets.QAction(
                QtWidgets.QApplication.translate(
                    'main', 'Create ODS spreadsheet for an assessment'), 
                self, 
                icon=utils.doIcon('extension-ods'), 
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Create ODS spreadsheet with a list of items and the valuation levels, '
                    'ready to be placed on your statements'))
            self.actionExportItemsList2ods.triggered.connect(self.doExportItemsList2ods)

            self.actionCreateStructureFile = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Create a structure file'), 
                self, 
                icon=utils.doIcon('svn-commit'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'A structure file can be exchanged with colleagues'))
            self.actionCreateStructureFile.triggered.connect(self.doCreateStructureFile)

            self.actionImportStructureFile = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Import a structure file'), 
                self, 
                icon=utils.doIcon('svn-update'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Import a structure made by a colleague'))
            self.actionImportStructureFile.triggered.connect(self.doImportStructureFile)

            self.actionExportAllEleves2ods = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Export Lists of students to ods'), 
                self, 
                icon=utils.doIcon('extension-ods'),
                statusTip=QtWidgets.QApplication.translate('main', 'Export Lists of students of the school in an ODF spreadsheet file (* .ods)'))
            self.actionExportAllEleves2ods.triggered.connect(self.doExportAllEleves2ods)

            self.actionExportBilansItems2Freeplane = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportBilansItems2Freeplane'), 
                self, 
                icon=utils.doIcon('extension-freeplane'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportBilansItems2FreeplaneStatusTip'))
            self.actionExportBilansItems2Freeplane.triggered.connect(self.doExportBilansItems2Freeplane)

            self.actionExportActualGroup2Freeplane = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportActualGroup2Freeplane'), 
                self, 
                icon=utils.doIcon('extension-freeplane'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportActualGroup2FreeplaneStatusTip'))
            self.actionExportActualGroup2Freeplane.triggered.connect(self.doExportActualGroup2Freeplane)

            self.actionExportAllGroups2Freeplane = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportAllGroups2Freeplane'), 
                self, 
                icon=utils.doIcon('extension-freeplane'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportAllGroups2FreeplaneStatusTip'))
            self.actionExportAllGroups2Freeplane.triggered.connect(self.doExportAllGroups2Freeplane)

            self.actionExportEleves2Freeplane = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportEleves2Freeplane'), 
                self, 
                icon=utils.doIcon('extension-freeplane'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportEleves2FreeplaneStatusTip'))
            self.actionExportEleves2Freeplane.triggered.connect(self.doExportEleves2Freeplane)

            self.actionExportBilan2Freeplane = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportBilan2Freeplane'), 
                self, 
                icon=utils.doIcon('extension-freeplane'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportBilan2FreeplaneStatusTip'))
            self.actionExportBilan2Freeplane.triggered.connect(self.doExportBilan2Freeplane)

            self.actionCreateGroupTrombinoscope = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Trombinoscope of the current group'), 
                self, 
                icon=utils.doIcon('trombinoscope'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    "To create a PDF file with the selected group's trombinoscope"))
            self.actionCreateGroupTrombinoscope.triggered.connect(self.doCreateGroupTrombinoscope)

            self.actionExportEleves2Dirs = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Export students in subfolders'), 
                self, 
                icon=utils.doIcon('students-tree'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Create subfolders '
                    'from a list of students'))
            self.actionExportEleves2Dirs.triggered.connect(self.doExportEleves2Dirs)

            self.actionExportBilan2Dirs = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Export a balance in tree'), 
                self, 
                icon=utils.doIcon('bilan-tree'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Create a tree (folder and subfolders) '
                    'from the balance sheet and associated items'))
            self.actionExportBilan2Dirs.triggered.connect(self.doExportBilan2Dirs)


            self.actionExportBilansItems2Csv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportBilansItems2Csv'), 
                self, 
                icon=utils.doIcon('extension-csv'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportBilansItems2CsvStatusTip'))
            self.actionExportBilansItems2Csv.triggered.connect(self.doExportBilansItems2Csv)

            self.actionImportBilansItemsFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ImportBilansItemsFromCsv'), 
                self, 
                icon=utils.doIcon('document-open'),
                statusTip=QtWidgets.QApplication.translate('main', 'ImportBilansItemsFromCsvStatusTip'))
            self.actionImportBilansItemsFromCsv.triggered.connect(self.doImportBilansItemsFromCsv)

            self.actionExportTemplates2Csv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportTemplates2Csv'), 
                self, 
                icon=utils.doIcon('extension-csv'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportTemplates2CsvStatusTip'))
            self.actionExportTemplates2Csv.triggered.connect(self.doExportTemplates2Csv)

            self.actionImportTemplatesFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ImportTemplatesFromCsv'), 
                self, 
                icon=utils.doIcon('document-open'),
                statusTip=QtWidgets.QApplication.translate('main', 'ImportTemplatesFromCsvStatusTip'))
            self.actionImportTemplatesFromCsv.triggered.connect(self.doImportTemplatesFromCsv)


            self.actionExportCounts2Csv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportCounts2Csv'), 
                self, 
                icon=utils.doIcon('extension-csv'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportCounts2CsvStatusTip'))
            self.actionExportCounts2Csv.triggered.connect(self.doExportCounts2Csv)

            self.actionImportCountsFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ImportCountsFromCsv'), 
                self, 
                icon=utils.doIcon('document-open'),
                statusTip=QtWidgets.QApplication.translate('main', 'ImportCountsFromCsvStatusTip'))
            self.actionImportCountsFromCsv.triggered.connect(self.doImportCountsFromCsv)


            self.actionImportAbsencesFromSIECLE = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Import absences from SIECLE'), 
                self, 
                icon=utils.doIcon('xml-to-absences'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Import absences from eleves_date_heure.xml file exported from SIECLE'))
            self.actionImportAbsencesFromSIECLE.triggered.connect(self.doImportAbsencesFromSIECLE)


            self.actionExportEleves2Csv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportEleves2Csv'), 
                self, 
                icon=utils.doIcon('extension-csv'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportEleves2CsvStatusTip'))
            self.actionExportEleves2Csv.triggered.connect(self.doExportEleves2Csv)

            self.actionImportElevesFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ImportElevesFromCsv'), 
                self, 
                icon=utils.doIcon('document-open'),
                statusTip=QtWidgets.QApplication.translate('main', 'ImportElevesFromCsvStatusTip'))
            self.actionImportElevesFromCsv.triggered.connect(self.doImportElevesFromCsv)

            self.actionExportReferentiel2Csv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ExportReferentiel2Csv'), 
                self, 
                icon=utils.doIcon('extension-csv'),
                statusTip=QtWidgets.QApplication.translate('main', 'ExportReferentiel2CsvStatusTip'))
            self.actionExportReferentiel2Csv.triggered.connect(self.doExportReferentiel2Csv)

            self.actionImportReferentielFromCsv = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ImportReferentielFromCsv'), 
                self, 
                icon=utils.doIcon('document-open'),
                statusTip=QtWidgets.QApplication.translate('main', 'ImportReferentielFromCsvStatusTip'))
            self.actionImportReferentielFromCsv.triggered.connect(self.doImportReferentielFromCsv)

            self.actionEditCsvFile = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'EditCsvFile'), 
                self, 
                icon=utils.doIcon('csv-edit'),
                statusTip=QtWidgets.QApplication.translate('main', 'EditCsvFileStatusTip'))
            self.actionEditCsvFile.triggered.connect(self.doEditCsvFile)

            self.actionUndo = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Undo'), 
                self, 
                icon=utils.doIcon('edit-undo'),
                shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Undo), 
                statusTip=QtWidgets.QApplication.translate('main', 'UndoStatusTip'))
            self.actionUndo.triggered.connect(self.doUndo)

            self.actionRedo = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Redo'), 
                self, 
                icon=utils.doIcon('edit-redo'),
                shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Redo), 
                statusTip=QtWidgets.QApplication.translate('main', 'RedoStatusTip'))
            self.actionRedo.triggered.connect(self.doRedo)

            self.actionCut = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Cut'), 
                self, 
                icon=utils.doIcon('edit-cut'),
                shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Cut), 
                statusTip=QtWidgets.QApplication.translate('main', 'CutStatusTip'))
            self.actionCut.setData(-1)
            self.actionCut.triggered.connect(self.doPopUpMenu)

            self.actionCopy = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Copy'), 
                self, 
                icon=utils.doIcon('edit-copy'),
                shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Copy), 
                statusTip=QtWidgets.QApplication.translate('main', 'CopyStatusTip'))
            self.actionCopy.setData(-2)
            self.actionCopy.triggered.connect(self.doPopUpMenu)

            self.actionPaste = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Paste'), 
                self, 
                icon=utils.doIcon('edit-paste'),
                shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Paste), 
                statusTip=QtWidgets.QApplication.translate('main', 'PasteStatusTip'))
            self.actionPaste.setData(-3)
            self.actionPaste.triggered.connect(self.doPopUpMenu)

            self.actionSelectAll = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SelectAll'), 
                self, 
                icon=utils.doIcon('edit-select-all'), 
                shortcut=QtGui.QKeySequence(QtGui.QKeySequence.SelectAll), 
                statusTip=QtWidgets.QApplication.translate('main', 'SelectAllStatusTip'))
            self.actionSelectAll.triggered.connect(self.doSelectAll)

            self.actionSimplify = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Simplify'), 
                self, 
                icon=utils.doIcon('simplify'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Simplifies the selection '
                    '(items values will be replaced by their average)'))
            self.actionSimplify.triggered.connect(self.doSimplify)

            self.actionClearIfUnfavorable = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Clear if unfavorable'), 
                self, 
                icon=utils.doIcon('clear'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 
                    'Clear evaluation if it is unfavorable '
                    'to the results of the student on this item'))
            self.actionClearIfUnfavorable.triggered.connect(self.doClearIfUnfavorable)


            self.actionElevePrevious = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ElevePrevious'), 
                self, 
                icon=utils.doIcon('eleve-previous'),
                statusTip=QtWidgets.QApplication.translate('main', 'ElevePreviousStatusTip'))
            self.actionElevePrevious.triggered.connect(self.doElevePrevious)

            self.actionEleveNext = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'EleveNext'), 
                self, 
                icon=utils.doIcon('eleve-next'),
                statusTip=QtWidgets.QApplication.translate('main', 'EleveNextStatusTip'))
            self.actionEleveNext.triggered.connect(self.doEleveNext)

            self.actionRecalc = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Recalc'), 
                self, 
                icon=utils.doIcon('xcalc'),
                statusTip=QtWidgets.QApplication.translate('main', 'RecalcStatusTip'))
            self.actionRecalc.triggered.connect(self.doRecalc)

            self.actionRecalcAllGroupes = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'RecalcAllGroupes'), 
                self, 
                icon=utils.doIcon('xcalc'),
                statusTip=QtWidgets.QApplication.translate('main', 'RecalcAllGroupesStatusTip'))
            self.actionRecalcAllGroupes.triggered.connect(self.doRecalcAllGroupes)

            self.actionLock = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Lock'), 
                self, 
                icon=utils.doIcon('object-locked'),
                statusTip=QtWidgets.QApplication.translate('main', 'LockStatusTip'),
                checkable=True, 
                checked=False)
            self.actionLock.triggered.connect(self.doLock)

            self.actionShift = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Shift'), 
                self, 
                icon=utils.doIcon('touche-shift'),
                statusTip=QtWidgets.QApplication.translate('main', 'ShiftStatusTip'),
                checkable=True, 
                checked=False)
            self.actionShift.triggered.connect(self.doShift)

            self.actionAutoselect = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Autoselect-no'), 
                self, 
                icon=utils.doIcon('autoselect-no'),
                statusTip=QtWidgets.QApplication.translate('main', 'AutoselectStatusTip'))
            self.actionAutoselect.triggered.connect(self.doAutoselect)

            self.actionAddNote = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'AddNote'), 
                self, 
                icon=utils.doIcon('note-add'),
                statusTip=QtWidgets.QApplication.translate('main', 'AddNoteStatusTip'))
            self.actionAddNote.triggered.connect(self.doAddNote)

            self.actionEditNote = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'EditNote'), 
                self, 
                icon=utils.doIcon('note-edit'),
                statusTip=QtWidgets.QApplication.translate('main', 'EditNoteStatusTip'))
            self.actionEditNote.triggered.connect(self.doEditNote)

            self.actionDeleteNote = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'DeleteNote'), 
                self, 
                icon=utils.doIcon('note-delete'),
                statusTip=QtWidgets.QApplication.translate('main', 'DeleteNoteStatusTip'))
            self.actionDeleteNote.triggered.connect(self.doDeleteNote)

            self.actionOrderNotes = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'OrderNotes'), 
                self, 
                icon=utils.doIcon('notes-order'),
                statusTip=QtWidgets.QApplication.translate('main', 'OrderNotesStatusTip'))
            self.actionOrderNotes.triggered.connect(self.doOrderNotes)


            self.actionCreateCount = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'CreateCount'), 
                self, 
                icon=utils.doIcon('count-add'),
                statusTip=QtWidgets.QApplication.translate('main', 'CreateCountStatusTip'))
            self.actionCreateCount.triggered.connect(self.doCreateCount)

            self.actionCreateMultipleCounts = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'Create multiple counts'), 
                self, 
                icon=utils.doIcon('count-add'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'creates counts for several couples group+period'))
            self.actionCreateMultipleCounts.triggered.connect(self.doCreateMultipleCounts)

            self.actionEditCount = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'EditCount'), 
                self, 
                icon=utils.doIcon('count-edit'),
                statusTip=QtWidgets.QApplication.translate('main', 'EditCountStatusTip'))
            self.actionEditCount.triggered.connect(self.doEditCount)

            self.actionDeleteCount = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'DeleteCount'), 
                self, 
                icon=utils.doIcon('count-delete'),
                statusTip=QtWidgets.QApplication.translate('main', 'DeleteCountStatusTip'))
            self.actionDeleteCount.triggered.connect(self.doDeleteCount)

            self.actionOrderCounts = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'OrderCounts'), 
                self, 
                icon=utils.doIcon('counts-order'),
                statusTip=QtWidgets.QApplication.translate('main', 'OrderCountsStatusTip'))
            self.actionOrderCounts.triggered.connect(self.doOrderCounts)

            self.actionValuesUp = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ValuesUp'), 
                self, 
                icon=utils.doIcon('arrow-up-double'),
                statusTip=QtWidgets.QApplication.translate('main', 'ValuesUpStatusTip'))
            self.actionValuesUp.triggered.connect(self.doValuesUp)

            self.actionValuesDown = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ValuesDown'), 
                self, 
                icon=utils.doIcon('arrow-down-double'),
                statusTip=QtWidgets.QApplication.translate('main', 'ValuesDownStatusTip'))
            self.actionValuesDown.triggered.connect(self.doValuesDown)



            self.actionFontSizeMore = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'FontSizeMore'), 
                self, 
                icon=utils.doIcon('format-font-size-more'))
            self.actionFontSizeMore.triggered.connect(self.doFontSizeMore)

            self.actionFontSizeLess = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'FontSizeLess'), 
                self, 
                icon=utils.doIcon('format-font-size-less'))
            self.actionFontSizeLess.triggered.connect(self.doFontSizeLess)


            self.actionLoadInitialLayout = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'LoadInitialLayout'), 
                self, 
                icon=utils.doIcon('toolbar-refresh'),
                statusTip=QtWidgets.QApplication.translate('main', 'LoadInitialLayoutStatusTip'))
            self.actionLoadInitialLayout.triggered.connect(self.doReloadLayout)

            self.actionLoadSavedLayout = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'LoadSavedLayout'), 
                self, 
                icon=utils.doIcon('toolbar-bookmark'),
                statusTip=QtWidgets.QApplication.translate('main', 'LoadSavedLayoutStatusTip'))
            self.actionLoadSavedLayout.triggered.connect(self.doReloadLayout)

            self.actionSaveCurrentLayout = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'SaveCurrentLayout'), 
                self, 
                icon=utils.doIcon('toolbar-save'),
                statusTip=QtWidgets.QApplication.translate('main', 'SaveCurrentLayoutStatusTip'))
            self.actionSaveCurrentLayout.triggered.connect(self.doSaveCurrentLayout)

            self.actionConfigureToolBar = QtWidgets.QAction(
                QtWidgets.QApplication.translate('main', 'ConfigureToolBar'), 
                self, 
                icon=utils.doIcon('toolbar-configure'),
                statusTip=QtWidgets.QApplication.translate(
                    'main', 'Select size of icons, actions displayed, ...'))
            self.actionConfigureToolBar.triggered.connect(self.doConfigureToolBar)



        #################################################
        # actions pour tous :
        #################################################
        self.actionQuit = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'E&xit'), 
            self, 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.Quit), 
            icon=utils.doIcon('application-exit'),
            statusTip=QtWidgets.QApplication.translate('main', 'QuitStatusTip'),
            triggered=self.close)

        self.actionContextHelpPage = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'ContextHelpPage'), 
            self, 
            shortcut=QtGui.QKeySequence(QtGui.QKeySequence.HelpContents),
            icon=utils.doIcon('help'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 'Opens contextual help in your browser'),
            triggered=self.contextHelpPage)

        self.actionHelpContents = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'HelpPageContents'), 
            self, 
            icon=utils.doIcon('help'),
            statusTip=QtWidgets.QApplication.translate('main', 'HelpPageContentsStatusTip'),
            triggered=self.helpContentsPage)

        self.actionProjetPage = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'ProjetPage'), 
            self, 
            icon=utils.doIcon('help'),
            statusTip=QtWidgets.QApplication.translate('main', 'ProjetPageStatusTip'))
        self.actionProjetPage.triggered.connect(self.helpProjetPage)

        self.actionAbout = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', '&About'), 
            self, 
            icon=utils.doIcon('help-about'),
            statusTip=QtWidgets.QApplication.translate('main', 'AboutStatusTip'))
        self.actionAbout.triggered.connect(self.about)

        self.actionGotoSiteEtab = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'GotoSiteEtab'), 
            self, 
            icon=utils.doIcon('net-go'),
            statusTip=QtWidgets.QApplication.translate('main', 'GotoSiteEtabStatusTip'))
        self.actionGotoSiteEtab.triggered.connect(self.doGotoSiteEtab)

        self.actionTestUpdateVerac = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'TestUpdateVerac'), 
            self, 
            icon=utils.doIcon('database-compare'),
            statusTip=QtWidgets.QApplication.translate('main', 'TestUpdateVeracStatusTip'))
        self.actionTestUpdateVerac.triggered.connect(self.doTestUpdateVerac)

        self.actionUpdateVerac = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'UpdateVerac'), 
            self, 
            icon=utils.doIcon('net-download'),
            statusTip=QtWidgets.QApplication.translate('main', 'UpdateVeracStatusTip'))
        self.actionUpdateVerac.triggered.connect(self.doUpdateVerac)

        self.actionConfig = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Parameters'), 
            self, 
            icon=utils.doIcon('configure'),
            statusTip=QtWidgets.QApplication.translate('main', 'ConfigStatusTip'))
        self.actionConfig.triggered.connect(self.doConfig)


        self.actionCreateYearArchive = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'CreateYearArchive'), 
            self, 
            icon=utils.doIcon('extension-tgz'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 'CreateYearArchiveStatusTip'))
        self.actionCreateYearArchive.triggered.connect(self.doCreateYearArchive)

        self.actionClearForNewYear = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Cleaning for a new year'), 
            self, 
            icon=utils.doIcon('edit-clear'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 
                'Clears the current year data (groups, assessments, appreciations ...)'))
        self.actionClearForNewYear.triggered.connect(self.doClearForNewYear)

        self.actionProtectedPeriods = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Protected periods'), 
            self, 
            icon=utils.doIcon('object-locked'),
            statusTip=QtWidgets.QApplication.translate('main', 'ProtectedPeriodsStatusTip'))
        self.actionProtectedPeriods.triggered.connect(self.doProtectedPeriods)


        self.actionCreateLinuxDesktopFile = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Create a launcher'), 
            self, 
            icon=utils.doIcon('logo_linux'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 
                'To create a launcher (*.desktop file) in the folder of your choice.'))
        self.actionCreateLinuxDesktopFile.triggered.connect(self.doCreateLinuxDesktopFile)

        self.actionCreateWinShortcutFile = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Create a shortcut'), 
            self, 
            icon=utils.doIcon('logo_windows'),
            statusTip=QtWidgets.QApplication.translate(
                'main', 
                'To create a shortcut (*.lnk file) on your Desktop.'))
        self.actionCreateWinShortcutFile.triggered.connect(self.doCreateWinShortcutFile)


        self.actionBugReport = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Make a bug report'), 
            self, 
            icon=utils.doIcon('tools-report-bug'),
            statusTip=QtWidgets.QApplication.translate('main', 'BugReportStatusTip'))
        self.actionBugReport.triggered.connect(self.bugReport)

        self.actionTest = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'Tests'), 
            self, 
            icon=utils.doIcon('document-open'))
        self.actionTest.triggered.connect(self.doTest)



        if self.me['userMode'] != 'admin':
            #################################################
            # actions en double dans les menus
            # (non supporté par certains systèmes) :
            #################################################

            self.actionClearForNewYear2 = QtWidgets.QAction(self)
            self.actionExportActualVue2Print2 = QtWidgets.QAction(self)
            self.actionCreateTableau2 = QtWidgets.QAction(self)
            self.notesMenu2 = QtWidgets.QMenu(self)
            self.countsMenu2 = QtWidgets.QMenu(self)
            actions2 = {
                self.actionClearForNewYear2: (self.actionClearForNewYear, self.doClearForNewYear),
                self.actionExportActualVue2Print2: (self.actionExportActualVue2Print, self.doExportActualVue2Print),
                self.actionCreateTableau2: (self.actionCreateTableau, self.doCreateTableau),
                }
            for action2 in actions2:
                action2.setText(actions2[action2][0].text())
                action2.setIcon(actions2[action2][0].icon())
                action2.setStatusTip(actions2[action2][0].statusTip())
                action2.triggered.connect(actions2[action2][1])


    def createMenus(self):
        """
        blablabla
        """
        if self.initialisation:
            self.periodeMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'Period'), 
                self, 
                icon=utils.doIcon('view-calendar'))
        if self.me['userMode'] == 'admin':
            # ------------------------
            # fileMenu :
            # ------------------------
            self.fileMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', '&File'), self)
            self.openADirMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'OpenADir'), 
                self, 
                icon=utils.doIcon('folder'))
            self.fileMenu.addMenu(self.openADirMenu)
            self.fileMenu.addAction(self.actionLaunchScheduler_admin)
            self.fileMenu.addSeparator()
            self.fileMenu.addAction(self.actionQuit)

            # ------------------------
            # organizationMenu :
            # ------------------------
            self.organizationMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'Organization'), self)
            self.organizationMenu.addAction(self.actionConfigurationSchool_admin)
            self.organizationMenu.addAction(self.actionClasses_admin)
            self.organizationMenu.addAction(self.actionSharedCompetences_admin)

            self.organizationMenu.addSeparator()
            self.organizationMenu.addAction(self.actionUploadDBCommun_admin)
            self.organizationMenu.addSeparator()
            self.organizationMenu.addAction(self.actionCreateConnectionFile_admin)
            self.newYearMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'New year'), 
                self, 
                icon=utils.doIcon('year-new'))
            self.organizationMenu.addMenu(self.newYearMenu)
            self.newYearMenu.addAction(self.actionCreateYearArchive_admin)
            self.newYearMenu.addAction(self.actionClearForNewYear_admin)

            # ------------------------
            # usersMenu :
            # ------------------------
            self.usersMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'Users'), self)
            self.usersMenu.addAction(self.actionGestUsers_admin)
            self.usersMenu.addAction(self.actionGestAddresses)
            self.usersMenu.addAction(self.actionGestPhotos)
            self.usersMenu.addSeparator()
            self.usersMenu.addAction(self.actionShowMdpProfsNoChange_admin)
            self.usersMenu.addSeparator()
            self.usersMenu.addAction(self.actionDownloadDBUsers_admin)
            self.usersMenu.addAction(self.actionUploadDBUsers_admin)

            # ------------------------
            # resultsMenu :
            # ------------------------
            self.resultsMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'Results'), self)
            # updateBilans :
            self.updateBilansMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'Update results'), 
                self, 
                icon=utils.doIcon('update-results'))
            self.resultsMenu.addMenu(self.updateBilansMenu)
            self.updateBilansMenu.addAction(self.actionUpdateBilans_admin)
            self.updateBilansMenu.addAction(self.actionUpdateBilansComplete_admin)
            self.updateBilansMenu.addAction(self.actionUpdateBilansSelect_admin)
            self.updateBilansMenu.addSeparator()
            self.updateBilansMenu.addAction(self.actionCalculateReferential_admin)
            self.updateBilansMenu.addAction(self.actionClearReferential_admin)
            self.updateBilansMenu.addAction(self.actionUploadDBResultats_admin)
            # CreateBilansBLT :
            self.createBilansBLTMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'CreateBilansBLT'), 
                self, 
                icon=utils.doIcon('validations'))
            self.resultsMenu.addMenu(self.createBilansBLTMenu)
            self.createBilansBLTMenu.addAction(self.actionCreateReports_admin)
            self.createBilansBLTMenu.addSeparator()
            self.createBilansBLTMenu.addAction(self.actionExportNotes2ods_admin)
            self.createBilansBLTMenu.addAction(self.actionExportStudentsResults2ods_admin)
            self.createBilansBLTMenu.addAction(self.actionExportBLTEvals2ods_admin)
            self.createBilansBLTMenu.addSeparator()
            self.createBilansBLTMenu.addAction(self.actionCreateHtmlModele_admin)
            self.createBilansBLTMenu.addAction(self.actionShowFields_admin)
            # gestDocs :
            self.resultsMenu.addAction(self.actionDocumentsManagement_admin)
            # periodes et archives :
            self.periodesMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'Periods'), 
                self, 
                icon=utils.doIcon('year-new'))
            self.resultsMenu.addMenu(self.periodesMenu)
            self.periodesMenu.addAction(self.actionSwitchToNextPeriod_admin)
            self.periodesMenu.addAction(self.actionListProtectedPeriods_admin)
            self.periodesMenu.addAction(self.actionLockedClasses_admin)
            self.resultsMenu.addSeparator()
            self.resultsMenu.addAction(self.actionExport2LSU_admin)
            self.resultsMenu.addSeparator()
            self.resultsMenu.addAction(self.actionVerifProfsFiles_admin)
            self.resultsMenu.addAction(self.actionCheckSchoolReports_admin)
            self.resultsMenu.addAction(self.actionLastReport_admin)

            # ------------------------
            # webSiteMenu :
            # ------------------------
            self.webSiteMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'WebSite'), self)
            self.webSiteMenu.addAction(self.actionWebSiteConfig)
            self.webSiteMenu.addAction(self.actionWebSiteState)
            self.webSiteMenu.addAction(self.actionTestUpdateWebSite)
            self.webSiteMenu.addAction(self.actionUpdateWebSite)
            self.webSiteMenu.addSeparator()
            self.webSiteMenu.addAction(self.actionGotoSiteEtab)
            self.webSiteMenu.addSeparator()
            self.webSiteMenu.addAction(self.actionShowCompteurAnalyze_admin)
            self.webSiteMenu.addSeparator()
            self.webSiteMenu.addAction(self.actionGestStructuresProfs_admin)
            self.webSiteMenu.addSeparator()
            self.webSiteMenu.addAction(self.actionRedoMdp_admin)

            # ------------------------
            # utilsMenu :
            # ------------------------
            self.utilsMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'Utils'), self)
            self.utilsMenu.addAction(self.actionTestUpdateVerac)
            self.utilsMenu.addAction(self.actionUpdateVerac)
            self.utilsMenu.addSeparator()
            self.utilsMenu.addAction(self.actionConfig)
            self.utilsMenu.addAction(self.actionEditCsvFile_admin)
            self.utilsMenu.addAction(self.actionEditDBFile_admin)
            # création des bases par fichier csv :
            self.utilsCreateDbsFromCsvMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', 'CreateDbsFromCsv'), 
                self, 
                icon=utils.doIcon('server-database'))
            self.utilsMenu.addMenu(self.utilsCreateDbsFromCsvMenu)
            self.utilsCreateDbsFromCsvMenu.addAction(self.actionCreateAdminTableFromCsv)
            self.utilsCreateDbsFromCsvMenu.addAction(self.actionCreateCommunTableFromCsv)
            self.utilsCreateDbsFromCsvMenu.addAction(self.actionCreateAllCommunTablesFromCsv)
            self.utilsMenu.addSeparator()
            if utils.OS_NAME[0] == 'linux':
                self.utilsMenu.addAction(self.actionCreateLinuxDesktopFile)
            elif utils.OS_NAME[0] == 'win':
                self.utilsMenu.addAction(self.actionCreateWinShortcutFile)
            """
            elif utils.OS_NAME[0] in ('powerpc', 'mac'):
                self.utilsMenu.addAction(self.actionCreateLinkFileMac)
            """

        else:
            # prof :
            interface = utils_db.readInConfigDict(
                self.configDict, 'SimplifiedInterface', default_int=1)[0]
            simplifiedInterface = (interface == 1)
            # ------------------------
            # fileMenu :
            # ------------------------
            if self.initialisation:
                self.fileMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', '&File'), self)
            else:
                self.fileMenu.clear()
            if simplifiedInterface:
                if self.actualVersion['versionName'] == 'perso':
                    self.fileMenu.addAction(self.actionSaveDBMy)
                else:
                    if not(self.NetOK):
                        self.fileMenu.addAction(self.actionSaveDBMy)
                    self.fileMenu.addAction(self.actionSaveAndPostDBMy)
                    self.fileMenu.addSeparator()
                    self.fileMenu.addAction(self.actionCompareDBMy)
                    self.fileMenu.addAction(self.actionDownloadDBMy)
                self.fileMenu.addSeparator()
                self.fileMenu.addAction(self.actionExportActualVue2Print)
                self.fileMenu.addSeparator()
                self.fileMenu.addAction(self.actionQuit)
            else:
                self.fileMenu.addAction(self.actionSaveDBMy)
                self.fileMenu.addAction(self.actionPostDBMy)
                self.fileMenu.addAction(self.actionSaveAndPostDBMy)
                self.fileMenu.addAction(self.actionAutoSaveDBMy)
                self.fileMenu.addSeparator()
                self.fileMenu.addAction(self.actionCompareDBMy)
                self.fileMenu.addAction(self.actionDownloadDBMy)
                self.fileMenu.addSeparator()
                self.fileMenu.addAction(self.actionPostSuivis)
                self.fileMenu.addSeparator()
                self.fileMenu.addAction(self.actionSaveAsDBMy)
                self.fileMenu.addAction(self.actionCreateYearArchive)
                self.fileMenu.addAction(self.actionClearForNewYear)
                self.fileMenu.addSeparator()
                self.fileMenu.addAction(self.actionExportActualVue2Print)
                self.fileMenu.addSeparator()
                self.fileMenu.addAction(self.actionQuit)

            # ------------------------
            # editMenu :
            # ------------------------
            if self.initialisation:
                self.editMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', '&Edit'), self)
            else:
                self.editMenu.clear()
            self.editMenu.addAction(self.actionUndo)
            self.editMenu.addAction(self.actionRedo)
            self.editMenu.addSeparator()
            self.editMenu.addAction(self.actionCut)
            self.editMenu.addAction(self.actionCopy)
            self.editMenu.addAction(self.actionPaste)
            self.editMenu.addAction(self.actionSelectAll)
            self.editMenu.addSeparator()
            self.editMenu.addAction(self.actionLock)
            self.editMenu.addAction(self.actionShift)
            self.editMenu.addAction(self.actionAutoselect)
            self.editMenu.addAction(self.actionRecalc)
            self.editMenu.addSeparator()
            self.editMenu.addAction(self.actionValuesUp)
            self.editMenu.addAction(self.actionValuesDown)
            self.editMenu.addSeparator()
            self.editMenu.addAction(self.actionSimplify)
            self.editMenu.addAction(self.actionClearIfUnfavorable)
            # sous-menu notes :
            if self.initialisation:
                self.notesMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Notes'), self, 
                    icon=utils.doIcon('notes'))
                self.notesMenu.addAction(self.actionAddNote)
                self.notesMenu.addAction(self.actionEditNote)
                self.notesMenu.addAction(self.actionDeleteNote)
                self.notesMenu.addAction(self.actionOrderNotes)
            self.editMenu.addMenu(self.notesMenu)
            # sous-menu counts :
            if self.initialisation:
                self.countsMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Counts'), self, 
                    icon=utils.doIcon('counter'))
                self.countsMenu.addAction(self.actionCreateCount)
                self.countsMenu.addAction(self.actionCreateMultipleCounts)
                self.countsMenu.addAction(self.actionEditCount)
                self.countsMenu.addAction(self.actionDeleteCount)
                self.countsMenu.addAction(self.actionOrderCounts)
            self.editMenu.addMenu(self.countsMenu)

            # ------------------------
            # showMenu :
            # ------------------------
            if self.initialisation:
                self.showMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Sho&w'), self)
            else:
                self.showMenu.clear()
            # sous-menu periode :
            self.showMenu.addMenu(self.periodeMenu)
            # sous-menu groupe :
            if self.initialisation:
                self.groupeMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Group'), self, 
                    icon=utils.doIcon('groupes'))
            self.showMenu.addMenu(self.groupeMenu)
            # sous-menu tableau :
            if self.initialisation:
                self.tableauMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Tableau'), self, 
                    icon=utils.doIcon('tableaux'))
            self.showMenu.addMenu(self.tableauMenu)
            # sous-menu vue :
            self.showMenu.addSeparator()
            if self.initialisation:
                self.viewMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'View'), self, 
                    icon=utils.doIcon('view'))
            else:
                self.viewMenu.clear()
            for action in self.viewMenuActions[interface]:
                if action == None:
                    self.viewMenu.addSeparator()
                else:
                    self.viewMenu.addAction(action)
            self.showMenu.addMenu(self.viewMenu)
            if self.initialisation:
                self.toolBarsMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'MenuAndToolBar'), 
                    self, 
                    icon=utils.doIcon('toolbar'))
                newAct0 = QtWidgets.QAction(
                    QtWidgets.QApplication.translate('main', 'MenuBar'), 
                    self, 
                    icon=utils.doIcon('show-menu'),
                    checkable=True)
                newAct0.setChecked(True)
                newAct0.setData(0)
                newAct0.triggered.connect(self.toolBarsChanged)
                self.toolBarsMenu.addAction(newAct0)
                newAct1 = QtWidgets.QAction(
                    self.toolBar.windowTitle(), 
                    self, 
                    icon=utils.doIcon('show-toolbar'),
                    checkable=True)
                newAct1.setChecked(True)
                newAct1.setData(1)
                newAct1.triggered.connect(self.toolBarsChanged)
                self.toolBarsMenu.addAction(newAct1)
                self.toolBarsMenuActions = (newAct0, newAct1)
                self.toolBarsMenu.addSeparator()
                self.toolBarsMenu.addAction(self.actionConfigureToolBar)
                self.toolBarsMenu.addSeparator()
                self.toolBarsMenu.addAction(self.actionLoadInitialLayout)
                self.toolBarsMenu.addAction(self.actionLoadSavedLayout)
                self.toolBarsMenu.addAction(self.actionSaveCurrentLayout)
            if not(simplifiedInterface):
                self.showMenu.addSeparator()
                # augmenter/diminuer la taille de police :
                self.showMenu.addAction(self.actionFontSizeMore)
                self.showMenu.addAction(self.actionFontSizeLess)
                # sous-menu barres d'outils :
                self.showMenu.addSeparator()
                self.showMenu.addAction(self.actionSwitchInterface)
                self.showMenu.addMenu(self.toolBarsMenu)
                # trier les groupes et tableaux :
                self.showMenu.addAction(self.actionSortGroupesTableaux)
            else:
                self.showMenu.addSeparator()
                self.showMenu.addAction(self.actionSwitchInterface)

            # ------------------------
            # evalsMenu :
            # ------------------------
            if self.initialisation:
                self.evalsMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'E&vals'), self)
            else:
                self.evalsMenu.clear()
            if simplifiedInterface:
                self.itemsMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'ItemsBilans'), self, 
                    icon=utils.doIcon('items'))
                self.actualTableauMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'ActualTableau'), self, 
                    icon=utils.doIcon('tableaux'))
                self.evalsMenu.addAction(self.actionGestGroupes)
                self.evalsMenu.addSeparator()
                self.evalsMenu.addAction(self.actionGestItems)
                self.evalsMenu.addAction(self.actionGestItemsBilans)
                self.evalsMenu.addSeparator()
                self.evalsMenu.addAction(self.actionCreateTableau2)
                self.evalsMenu.addAction(self.actionEditActualTableau)
                self.evalsMenu.addAction(self.actionListItems)
                self.evalsMenu.addSeparator()
                self.evalsMenu.addAction(self.actionManageProfiles)
            else:
                groupesMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'GroupesEleves'), self, 
                    icon=utils.doIcon('groupes'))
                self.evalsMenu.addMenu(groupesMenu)
                groupesMenu.addAction(self.actionGestGroupes)
                groupesMenu.addAction(self.actionCreateMultipleGroupesClasses)
                groupesMenu.addAction(self.actionShowDeletedEleves)
                groupesMenu.addAction(self.actionClearDeletedEleves)
                groupesMenu.addSeparator()
                groupesMenu.addAction(self.actionRecalcAllGroupes)
                self.itemsMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'ItemsBilans'), self, 
                    icon=utils.doIcon('items'))
                self.evalsMenu.addMenu(self.itemsMenu)
                self.itemsMenu.addAction(self.actionGestItems)
                self.itemsMenu.addAction(self.actionGestItemsBilans)
                self.itemsMenu.addAction(self.actionItemsBilansTable)
                self.itemsMenu.addAction(self.actionLinkItemsToBilan)
                self.itemsMenu.addAction(self.actionLinkBilans)
                self.itemsMenu.addAction(self.actionGestAdvices)
                # sous-menu tableaux :
                tableauxMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Tableaux'), self, 
                    icon=utils.doIcon('tableaux'))
                self.evalsMenu.addMenu(tableauxMenu)
                tableauxMenu.addAction(self.actionCreateTableau2)
                tableauxMenu.addAction(self.actionCreateMultipleTableaux)
                tableauxMenu.addSeparator()
                tableauxMenu.addAction(self.actionCopyItemsFromTo)
                tableauxMenu.addAction(self.actionMergeTableaux)
                # sous-menu tableau actuel :
                self.actualTableauMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'ActualTableau'), self, 
                    icon=utils.doIcon('tableaux'))
                self.evalsMenu.addMenu(self.actualTableauMenu)
                self.actualTableauMenu.addAction(self.actionEditActualTableau)
                self.actualTableauMenu.addSeparator()
                self.actualTableauMenu.addAction(self.actionListVisibleItems)
                self.actualTableauMenu.addAction(self.actionHideEmptyColumns)
                self.actualTableauMenu.addAction(self.actionListItems)
                self.actualTableauMenu.addSeparator()
                self.actualTableauMenu.addAction(self.actionCreateTemplateFromTableau)
                self.actualTableauMenu.addAction(self.actionLinkTableauTemplate)
                self.actualTableauMenu.addAction(self.actionUnlinkTableauTemplate)
                self.actualTableauMenu.addSeparator()
                self.actualTableauMenu.addAction(self.actionCopyTableau)
                self.actualTableauMenu.addAction(self.actionSupprTableau)
                # gestion des modèles de tableaux :
                self.evalsMenu.addAction(self.actionTableauxTemplates)
                # sous-menu bulletin :
                self.evalsMenu.addAction(self.actionManageProfiles)
                # sous-menu notes :
                menus2 = {
                    self.notesMenu2: self.notesMenu,
                    self.countsMenu2: self.countsMenu,
                    }
                for menu2 in menus2:
                    menu2.setTitle(menus2[menu2].title())
                    menu2.setIcon(menus2[menu2].icon())
                self.evalsMenu.addMenu(self.notesMenu2)
                self.notesMenu2.addAction(self.actionAddNote)
                self.notesMenu2.addAction(self.actionEditNote)
                self.notesMenu2.addAction(self.actionDeleteNote)
                self.notesMenu2.addAction(self.actionOrderNotes)
                # sous-menu counts :
                self.evalsMenu.addMenu(self.countsMenu2)
                self.countsMenu2.addAction(self.actionCreateCount)
                self.countsMenu2.addAction(self.actionCreateMultipleCounts)
                self.countsMenu2.addAction(self.actionEditCount)
                self.countsMenu2.addAction(self.actionDeleteCount)
                self.countsMenu2.addAction(self.actionOrderCounts)

                # Gestion des élèves suivis (pour les PP seulement) :
                if self.me['userMode'] == 'PP':
                    self.evalsMenu.addAction(self.actionGestElevesSuivis)

            # ------------------------
            # utilsMenu :
            # ------------------------
            if self.initialisation:
                self.utilsMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', '&Utils'), self)
            else:
                self.utilsMenu.clear()
            if simplifiedInterface:
                self.utilsMenu.addAction(self.actionGotoSiteEtab)
                self.utilsMenu.addAction(self.actionChangeMdp)
                self.utilsMenu.addSeparator()
                self.utilsMenu.addAction(self.actionTestUpdateDBUsersCommun)
                self.utilsMenu.addAction(self.actionTestUpdateVerac)
            else:
                self.utilsMenu.addAction(self.actionGotoSiteEtab)
                self.utilsMenu.addAction(self.actionChangeMdp)
                self.utilsMenu.addSeparator()
                self.utilsMenu.addAction(self.actionTestUpdateDBUsersCommun)
                self.utilsMenu.addAction(self.actionCheckCommunBilans)
                self.utilsMenu.addAction(self.actionDownloadSuivisDB)
                self.utilsMenu.addSeparator()
                self.utilsMenu.addAction(self.actionTestUpdateVerac)
                self.utilsMenu.addAction(self.actionUpdateVerac)
                self.utilsMenu.addSeparator()
                self.utilsMenu.addAction(self.actionProtectedPeriods)
                self.utilsMenu.addAction(self.actionClearForNewYear2)
                self.utilsMenu.addSeparator()
                self.utilsMenu.addAction(self.actionConfig)
                self.utilsMenu.addAction(self.actionCreateEtab)
                if (self.actualVersion['versionName'] == 'perso'):
                    if self.adminOk:
                        self.utilsMenu.addAction(self.actionReturnVersionPerso)
                    else:
                        self.utilsMenu.addAction(self.actionAdminVersionPerso)
                if utils.OS_NAME[0] == 'linux':
                    self.utilsMenu.addSeparator()
                    self.utilsMenu.addAction(self.actionCreateLinuxDesktopFile)
                elif utils.OS_NAME[0] == 'win':
                    self.utilsMenu.addSeparator()
                    self.utilsMenu.addAction(self.actionCreateWinShortcutFile)
                """
                elif utils.OS_NAME[0] in ('powerpc', 'mac'):
                    self.utilsMenu.addAction(self.actionCreateLinkFileMac)
                """

            # ------------------------
            # importExportMenu :
            # ------------------------
            if self.initialisation:
                self.importExportMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', '&ImportExport'), self)
            else:
                self.importExportMenu.clear()
            if simplifiedInterface:
                # sous-menu ActualView :
                self.importExportMenu.addAction(self.actionExportActualVue2ods)
                self.importExportMenu.addAction(self.actionExportActualVue2Print2)
                self.importExportMenu.addSeparator()
                # sous-menu Groupes :
                self.importExportMenu.addAction(self.actionExportActualGroup2ods)
                self.importExportMenu.addSeparator()
                # sous-menu Élèves :
                self.importExportMenu.addAction(self.actionExportEleves2Freeplane)
                self.importExportMenu.addAction(self.actionExportAllEleves2ods)
                if (self.actualVersion['versionName'] == 'perso') and not(self.adminOk):
                    self.importExportMenu.addAction(self.actionExportEleves2Csv)
                    self.importExportMenu.addAction(self.actionImportElevesFromCsv)
                self.importExportMenu.addAction(self.actionImportAbsencesFromSIECLE)
                self.importExportMenu.addSeparator()
                # sous-menu Structure :
                self.importExportMenu.addAction(self.actionImportStructureFile)
                self.importExportMenu.addAction(self.actionExportStructure2ods)
                self.importExportMenu.addAction(self.actionImportBilansItemsFromCsv)
                self.importExportMenu.addAction(self.actionImportTemplatesFromCsv)
                if (self.actualVersion['versionName'] == 'perso') and not(self.adminOk):
                    self.importExportMenu.addAction(self.actionExportReferentiel2Csv)
                    self.importExportMenu.addAction(self.actionImportReferentielFromCsv)
            else:
                # sous-menu ActualView :
                self.importExportViewMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'ActualView'), self, 
                    icon=utils.doIcon('view'))
                self.importExportMenu.addMenu(self.importExportViewMenu)
                self.importExportViewMenu.addAction(self.actionExportActualVue2ods)
                self.importExportViewMenu.addSeparator()
                self.importExportViewMenu.addAction(self.actionExportActualVue2Print2)
                self.importExportViewMenu.addAction(self.actionExportActualVue2pdf)
                # sous-menu Groupes :
                self.importExportGroupsMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Groups'), self, 
                    icon=utils.doIcon('groupes'))
                self.importExportMenu.addMenu(self.importExportGroupsMenu)
                self.importExportGroupsMenu.addAction(self.actionExportActualGroup2ods)
                self.importExportGroupsMenu.addAction(self.actionExportActualGroup2Freeplane)
                self.importExportGroupsMenu.addAction(self.actionCreateGroupTrombinoscope)
                self.importExportGroupsMenu.addSeparator()
                self.importExportGroupsMenu.addAction(self.actionExportAllGroups2Freeplane)
                # sous-menu Élèves :
                self.importExportElevesMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Students'), self, 
                    icon=utils.doIcon('eleve'))
                self.importExportMenu.addMenu(self.importExportElevesMenu)
                self.importExportElevesMenu.addAction(self.actionExportEleves2Freeplane)
                self.importExportElevesMenu.addAction(self.actionExportEleves2Dirs)
                self.importExportElevesMenu.addSeparator()
                self.importExportElevesMenu.addAction(self.actionExportAllEleves2ods)
                if (self.actualVersion['versionName'] == 'perso') and not(self.adminOk):
                    self.importExportElevesMenu.addAction(self.actionExportEleves2Csv)
                    self.importExportElevesMenu.addAction(self.actionImportElevesFromCsv)
                    self.importExportElevesMenu.addSeparator()
                self.importExportElevesMenu.addAction(self.actionImportAbsencesFromSIECLE)
                # sous-menu Structure :
                self.importExportStructureMenu = QtWidgets.QMenu(
                    QtWidgets.QApplication.translate('main', 'Structure'), self
                    , icon=utils.doIcon('structure'))
                self.importExportMenu.addMenu(self.importExportStructureMenu)
                self.importExportStructureMenu.addAction(self.actionImportStructureFile)
                self.importExportStructureMenu.addAction(self.actionCreateStructureFile)
                self.importExportStructureMenu.addSeparator()
                self.importExportStructureMenu.addAction(self.actionExportStructure2ods)
                self.importExportStructureMenu.addSeparator()
                self.importExportStructureMenu.addAction(self.actionExportItemsList2ods)
                self.importExportStructureMenu.addSeparator()
                self.importExportStructureMenu.addAction(self.actionExportBilansItems2Csv)
                self.importExportStructureMenu.addAction(self.actionImportBilansItemsFromCsv)
                self.importExportStructureMenu.addAction(self.actionExportBilansItems2Freeplane)
                self.importExportStructureMenu.addSeparator()
                self.importExportStructureMenu.addAction(self.actionExportBilan2Freeplane)
                self.importExportStructureMenu.addAction(self.actionExportBilan2Dirs)
                self.importExportStructureMenu.addSeparator()
                self.importExportStructureMenu.addAction(self.actionExportTemplates2Csv)
                self.importExportStructureMenu.addAction(self.actionImportTemplatesFromCsv)
                self.importExportStructureMenu.addSeparator()
                self.importExportStructureMenu.addAction(self.actionExportCounts2Csv)
                self.importExportStructureMenu.addAction(self.actionImportCountsFromCsv)
                if (self.actualVersion['versionName'] == 'perso') and not(self.adminOk):
                    self.importExportStructureMenu.addSeparator()
                    self.importExportStructureMenu.addAction(self.actionExportReferentiel2Csv)
                    self.importExportStructureMenu.addAction(self.actionImportReferentielFromCsv)
                # éditeur de csv :
                self.importExportMenu.addSeparator()
                self.importExportMenu.addAction(self.actionEditCsvFile)

        # ------------------------
        # helpMenu (admin + prof) :
        # ------------------------
        if self.initialisation:
            self.helpMenu = QtWidgets.QMenu(
                QtWidgets.QApplication.translate('main', '&Help'), self)
        else:
            self.helpMenu.clear()
        self.helpMenu.addAction(self.actionContextHelpPage)
        self.helpMenu.addAction(self.actionHelpContents)
        self.helpMenu.addAction(self.actionProjetPage)
        self.helpMenu.addAction(self.actionBugReport)
        self.helpMenu.addSeparator()
        self.helpMenu.addAction(self.actionAbout)
        self.helpMenu.addSeparator()
        self.helpMenu.addAction(self.actionTest)

        # on place les menus dans la barre et dans allMenu :
        if self.me['userMode'] == 'admin':
            menus = (
                self.fileMenu, 
                self.organizationMenu, 
                self.usersMenu, 
                self.resultsMenu, 
                self.webSiteMenu, 
                self.utilsMenu,
                self.helpMenu)
        else:
            # prof :
            menus = (
                self.fileMenu, 
                self.editMenu, 
                self.showMenu, 
                self.evalsMenu, 
                self.utilsMenu, 
                self.importExportMenu,
                self.helpMenu)
        if self.initialisation:
            self.allMenu = QtWidgets.QMenu()
            self.allMenuButton = QtWidgets.QWidgetAction(self)
            self.allMenuButton.setIcon(utils.doIcon('favorites'))
            self.allMenuButton.setToolTip(QtWidgets.QApplication.translate('main', 'All Menus Button'))
            self.allMenuButton.setVisible(False)
        else:
            self.allMenu.clear()
        for menu in menus:
            self.menuBar().addMenu(menu)
            self.allMenu.addMenu(menu)
        self.allMenuButton.setMenu(self.allMenu)

    def createToolBar(self):
        """
        blablabla
        """
        if self.closing:
            return
        if self.me['userMode'] == 'admin':
            self.toolBar.setIconSize(
                QtCore.QSize(
                    utils.STYLE['PM_LargeIconSize'], 
                    utils.STYLE['PM_LargeIconSize']))
            self.toolBar.addAction(self.actionQuit)
            self.openADirButton = QtWidgets.QPushButton(
                utils.doIcon('folder'), 
                QtWidgets.QApplication.translate('main', 'OpenADir'))
            self.openADirButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'OpenADirToolTip'))
            self.openADirButton.setMenu(self.openADirMenu)
            self.toolBar.addWidget(self.openADirButton)
            #self.toolBar.addAction(self.actionLaunchScheduler_admin)
            self.toolBar.addSeparator()

            self.periodeButton = QtWidgets.QPushButton()
            self.periodeButton.setToolTip(
                QtWidgets.QApplication.translate('main', 'ChangePeriode'))
            self.periodeButton.setMenu(self.periodeMenu)
            self.initPeriodeMenu()
            #self.periodeButton .setMinimumWidth(200)
            self.periodeLabel = QtWidgets.QLabel()
            self.periodeLabel.setMinimumWidth(150)
            periodeGrid = QtWidgets.QGridLayout()
            periodeGrid.addWidget(self.periodeButton, 0, 0)
            periodeGrid.addWidget(self.periodeLabel, 1, 0)
            periodeGroupBox = QtWidgets.QGroupBox()
            periodeGroupBox.setLayout(periodeGrid)
            periodeGroupBox.setFlat(True)
            self.toolBar.addWidget(periodeGroupBox)

            self.toolBar.addAction(self.actionUpdateBilans_admin)
            self.toolBar.addSeparator()
            self.toolBar.addAction(self.actionContextHelpPage)

        else:
            # prof :
            if not(self.initialisation):
                selections = (
                    self.groupeButton.text(),
                    self.periodeButton.text(),
                    self.tableauButton.text(),
                    )
                self.configDict = utils_db.configTable2Dict(self.db_my)
                self.toolBar.clear()
            self.toolBar.setIconSize(
                QtCore.QSize(
                    utils.STYLE['PM_LargeIconSize'], 
                    utils.STYLE['PM_LargeIconSize']))

            self.frequentlyUsedActions = (
                (self.actionQuit, 'actionQuit', 1),
                (self.actionSaveDBMy, 'actionSaveDBMy', 0),
                (self.actionSaveAndPostDBMy, 'actionSaveAndPostDBMy', 0),
                (self.actionDownloadDBMy, 'actionDownloadDBMy', 0),
                (self.actionSwitchInterface, 'actionSwitchInterface', 1),
                (self.actionLoadInitialLayout, 'actionLoadInitialLayout', 0),
                (self.actionLoadSavedLayout, 'actionLoadSavedLayout', 0),
                (self.actionGotoSiteEtab, 'GotoSiteEtab', 0),
                )
            for action in self.frequentlyUsedActions:
                state = utils_db.readInConfigDict(
                    self.configDict, action[1], default_int=action[2])[0]
                if state == 1:
                    self.toolBar.addAction(action[0])
            self.toolBar.addAction(self.allMenuButton)
            self.toolBar.addSeparator()

            # ******************************************************
            # le groupe de listes déroulantes pour la sélection :
            # ******************************************************
            if self.initialisation:
                # menu période :
                self.periodeButton = QtWidgets.QPushButton()
                self.periodeButton.setToolTip(QtWidgets.QApplication.translate('main', 'Choose period'))
                self.periodeButton.setMenu(self.periodeMenu)
                # menu groupe :
                self.groupeButton = QtWidgets.QPushButton()
                self.groupeButton.setToolTip(QtWidgets.QApplication.translate('main', 'ChooseGroup'))
                self.groupeButton.setMenu(self.groupeMenu)
                # menu tableau :
                self.tableauButton = QtWidgets.QPushButton()
                self.tableauButton.setMenu(self.tableauMenu)
                # menu vue :
                self.viewButton = QtWidgets.QPushButton()
                self.viewButton.setToolTip(QtWidgets.QApplication.translate('main', 'View'))
                self.viewButton.setMenu(self.viewMenu)
                self.viewButton.setObjectName('ViewButton')
                # on les met dans la topBar :
                self.topBarLayouts['default'].addWidget(self.periodeButton)
                self.topBarLayouts['default'].addWidget(self.groupeButton)
                self.topBarLayouts['default'].addWidget(self.viewButton)
                self.topBarLayouts['default'].addWidget(self.tableauButton)

            # ******************************************************
            # Des actions affichées selon le contexte :
            # ******************************************************
            if self.initialisation:
                # la liste déroulante pour évaluer (ira dans la topBar) :
                self.changeValueComboBox = QtWidgets.QComboBox()
                self.changeValueComboBox.setMaxVisibleItems(20)
                self.changeValueComboBox.setToolTip(
                    QtWidgets.QApplication.translate('main', 'ItemComboBox'))
                self.changeValueComboBox.setStatusTip(
                    QtWidgets.QApplication.translate('main', 'ItemComboBoxStatusTip'))
                self.changeValueComboBox.activated.connect(self.changeValueChanged)
                self.actionChangeValue = QtWidgets.QWidgetAction(self)
                self.actionChangeValue.setDefaultWidget(self.changeValueComboBox)
            self.contextActions = (
                (self.actionChangeValue, 'actionChangeValue', 1, self.changeValueComboBox), 
                (self.actionUndo, 'actionUndo', 1), 
                (self.actionRedo, 'actionRedo', 1), 
                (self.actionLock, 'actionLock', 0), 
                (self.actionShift, 'actionShift', 0), 
                (self.actionAutoselect, 'actionAutoselect', 0), 
                (self.actionRecalc, 'actionRecalc', 1), 
                (self.actionHideEmptyColumns, 'actionHideEmptyColumns', 0), 
                )
            for action in self.contextActions:
                if self.initialisation:
                    if len(action) > 3:
                        self.topBarLayouts['default'].addWidget(action[3])
                        action[3].setVisible(False)
                state = utils_db.readInConfigDict(
                    self.configDict, action[1], default_int=action[2])[0]
                if len(action) > 3:
                    action[3].setVisible(state == 1)
                elif state == 1:
                    self.toolBar.addAction(action[0])
            self.toolBar.addSeparator()
            self.contextActions2 = (
                (self.actionValuesUp, 'actionValuesUp', 1),
                (self.actionValuesDown, 'actionValuesDown', 1),
                )
            for action in self.contextActions2:
                state = utils_db.readInConfigDict(
                    self.configDict, action[1], default_int=action[2])[0]
                if state == 1:
                    self.toolBar.addAction(action[0])

            # ******************************************************
            # le groupe pour la vue Élève :
            # ******************************************************
            if self.initialisation:
                # sélection de l'élève dans une liste déroulante :
                self.changeEleveComboBox = QtWidgets.QComboBox()
                self.changeEleveComboBox.setMinimumWidth(200)
                self.changeEleveComboBox.setToolTip(
                    QtWidgets.QApplication.translate('main', 'EleveComboBox'))
                self.changeEleveComboBox.setStatusTip(
                    QtWidgets.QApplication.translate('main', 'EleveComboBoxStatusTip'))
                self.changeEleveComboBox.activated.connect(self.changeEleveChanged)
                # sélection du sens d'affichage :
                self.eleveSensComboBox = QtWidgets.QComboBox()
                self.eleveSensComboBox.setMinimumWidth(200)
                self.eleveSensComboBox.setToolTip(
                    QtWidgets.QApplication.translate('main', 'EleveSens'))
                self.eleveSensComboBox.setStatusTip(
                    QtWidgets.QApplication.translate('main', 'EleveSensStatusTip'))
                self.eleveSensComboBox.activated.connect(self.eleveSensChanged)
                sens = ((0, QtWidgets.QApplication.translate('main', 'Balances > Items')),
                        (1, QtWidgets.QApplication.translate('main', 'Items > Balances')),
                        (2, QtWidgets.QApplication.translate('main', 'Balances > Items + Opinions')),
                        (3, QtWidgets.QApplication.translate('main', 'Items > Balances + Opinions')))
                for (id_sens, text_sens) in sens:
                    self.eleveSensComboBox.addItem(text_sens, id_sens)
                self.id_sens = 2
                self.eleveSensComboBox.setCurrentIndex(2)
            # tout ça dans la topBar :
            eleveLayout = QtWidgets.QVBoxLayout()
            eleveLayout.setContentsMargins(0, 0, 0, 0)
            if self.initialisation:
                # le bouton "élève précédent" :
                self.elevePreviousButton = QtWidgets.QPushButton(
                    utils.doIcon('eleve-previous'), '')
                self.elevePreviousButton.setToolTip(
                    QtWidgets.QApplication.translate('main', 'ElevePreviousStatusTip'))
                self.elevePreviousButton.clicked.connect(self.doElevePrevious)
                # le bouton "élève suivant" :
                self.eleveNextButton = QtWidgets.QPushButton(
                    utils.doIcon('eleve-next'), '')
                self.eleveNextButton.setToolTip(
                    QtWidgets.QApplication.translate('main', 'EleveNextStatusTip'))
                self.eleveNextButton.clicked.connect(self.doEleveNext)
                # on place tout cela :
                self.topBarLayouts['changeEleve'].addWidget(self.elevePreviousButton)
                self.topBarLayouts['changeEleve'].addWidget(self.changeEleveComboBox)
                self.topBarLayouts['changeEleve'].addWidget(self.eleveNextButton)
                self.topBarLayouts['changeEleve'].addWidget(self.eleveSensComboBox)
            vGroupBox = QtWidgets.QGroupBox()
            vGroupBox.setLayout(eleveLayout)
            vGroupBox.setFlat(True)
            self.actionChangeEleve = QtWidgets.QWidgetAction(self)
            self.actionChangeEleve.setDefaultWidget(vGroupBox)

            self.otherActions = (
                (self.actionExportActualVue2ods, 'actionExportActualVue2ods', 0),
                (self.actionExportActualVue2Print, 'ExportActualVue2Print', 0),
                (self.actionExportActualGroup2ods, 'actionExportActualGroup2ods', 0),
                (self.actionExportActualGroup2Freeplane, 'actionExportActualGroup2Freeplane', 0),
                (self.actionExportStructure2ods, 'actionExportStructure2ods', 0),
                )
            for action in self.otherActions:
                state = utils_db.readInConfigDict(
                    self.configDict, action[1], default_int=action[2])[0]
                if state == 1:
                    self.toolBar.addAction(action[0])

            self.toolBar.addAction(self.actionContextHelpPage)

            # ******************************************************
            # Le slider de la taille des polices :
            # ******************************************************
            if self.initialisation:
                self.fontSizeSlider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
                self.fontSizeSlider.setFocusPolicy(QtCore.Qt.StrongFocus)
                self.fontSizeSlider.setSingleStep(1)
                self.fontSizeSlider.setPageStep(4)
                pointSize = QtWidgets.QApplication.font().pointSize()
                self.fontSizeSlider.setMinimum(pointSize // 2)
                self.fontSizeSlider.setMaximum(pointSize * 2)
                self.fontSizeSlider.setValue(pointSize)
                translatedMessage = QtWidgets.QApplication.translate(
                    'main', 'FontSize: {0}')
                translatedMessage = utils_functions.u(
                    translatedMessage).format(pointSize)
                self.fontSizeSlider.setToolTip(translatedMessage)
                self.fontSizeSlider.valueChanged.connect(self.changeFontSize)
                self.fontSizeSlider.sliderReleased.connect(self.fontSizeSliderReleased)
                self.fontSizeSlider.setMaximumWidth(200)
                self.actionFontSizeSlider = QtWidgets.QWidgetAction(self)
                self.actionFontSizeSlider.setDefaultWidget(self.fontSizeSlider)
                self.actionFontSizeSlider.setObjectName('actionFontSizeSlider')
                self.actionFontSizeSlider.setIcon(utils.doIcon('format-font-size-more'))
            self.otherActions2 = (
                (self.actionFontSizeSlider, 'actionFontSizeSlider', 0, self.fontSizeSlider),
                )
            for action in self.otherActions2:
                if self.initialisation:
                    self.topBarLayouts['default'].addWidget(action[3])
                    action[3].setVisible(False)
                state = utils_db.readInConfigDict(
                    self.configDict, action[1], default_int=action[2])[0]
                action[3].setVisible(state == 1)

            if self.initialisation:
                for what in self.topBarLayouts['ORDER']:
                    self.topBarLayouts[what].addStretch(1)

            if not(self.initialisation):
                self.groupeButton.setText(selections[0])
                self.periodeButton.setText(selections[1])
                self.tableauButton.setText(selections[2])
                self.doShow(doReload=True)





    def doShowAccordion(self):
        index = -1
        for i in range(len(self.accordion)):
            if self.sender() == self.accordion[i + 1][1]:
                index = i + 1
        if index < 0:
            return
        widget = self.accordion[index][2]
        widget.setVisible(not(widget.isVisible()))

    def accordionSplitterMoved(self):
        w = self.accordion[1][2].parent().parent().width() - 5
        self.accordion[1][2].parent().setMinimumWidth(w)
        self.accordion[1][2].parent().setMaximumWidth(w)


