# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Gestion des fichiers de structures profs.
    Ils peuvent être mis à disposition sur le site web de l'établissement.
    Ce fichier contient la partie prof (création et importation)
"""

# importation des modules utiles:
from __future__ import division, print_function

import utils, utils_functions, utils_webengine, utils_db
import utils_2lists, utils_htmleditor
import utils_markdown

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



###########################################################"
#   DES FONCTIONS UTILES AUX 2 WIZARDS
###########################################################"

def remplirMatieresComboBox(wizard):
    # est-on PP ou en version perso :
    isPP = (wizard.main.me['userMode'] == 'PP')
    isPerso = (wizard.main.actualVersion['versionName'] == 'perso')
    # seul le PP n'a pas les matières "normales" au début :
    if not(isPP):
        for matiereCode in wizard.main.me['MATIERES']['BLT']:
            matiereName = wizard.main.me['MATIERES']['Code2Matiere'].get(
                matiereCode, (0, '', ''))[1]
            wizard.matieresComboBox.addItem(matiereName)
        wizard.matieresComboBox.insertSeparator(1000)
    # le PP et la version perso ont la matière PP :
    if isPP or isPerso:
        wizard.matieresComboBox.addItem(
            wizard.main.me['MATIERES']['Code2Matiere']['PP'][1])
        wizard.matieresComboBox.insertSeparator(1000)
    # tout le monde a la matière VS :
    wizard.matieresComboBox.addItem(
        wizard.main.me['MATIERES']['Code2Matiere']['VS'][1])
    # tout le monde a les matières hors bulletin :
    for matiereCode in wizard.main.me['MATIERES']['OTHERS']:
        matiereName = wizard.main.me['MATIERES']['Code2Matiere'].get(
            matiereCode, (0, '', ''))[1]
        wizard.matieresComboBox.addItem(matiereName)
    # le PP a les matières "normales" à la fin :
    if isPP:
        wizard.matieresComboBox.insertSeparator(1000)
        for matiereCode in wizard.main.me['MATIERES']['BLT']:
            matiereName = wizard.main.me['MATIERES']['Code2Matiere'].get(
                matiereCode, (0, '', ''))[1]
            wizard.matieresComboBox.addItem(matiereName)

###########################################################"
#   CRÉATION D'UN FICHIER DE STRUCTURE
###########################################################"

class CreateStructureFileWizard(QtWidgets.QDialog):
    """
    le Wizard de création d'un fichier de structure.
    On crée tous les widgets nécessaires 
    mais ils seront visibles selon la page.
    Pages :
        0 : page d'explications
        1 : sélection des items
        2 : sélection des bilans
        3 : sélection du profil
        4 : sélection des modèles
        5 : sélection des comptages
        99 : matière, titre et description de la structure
    """
    def __init__(self, main=None):
        super(CreateStructureFileWizard, self).__init__(main)
        self.main = main
        # page actuelle et navigation :
        self.page = {
            'back': -1, 
            'actual': 0, 
            'next': 1, 
            'last': 5, 
            'final': 99}
        # variables du Wizard :
        self.reInit()
        # titre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Create a structure file'))
        # titre de l'étape en  cours :
        self.titleLabel = QtWidgets.QLabel()
        # un texte d'aide :
        self.markdownHelpViewer = utils_markdown.MarkdownHelpViewer(
            self.main)

        # les icones :
        self.icons = {}
        colors = (
            'red', 'yellow', 'blue', 'blue2', 'magenta', 'green', 'cyan')
        for color in colors:
                self.icons[color] = utils.doIcon('color-{0}'.format(color))

        # un bouton pour ouvrir un fichier existant (page 0) :
        self.openExistingFileButton = QtWidgets.QPushButton(
            utils.doIcon('database-open'), 
            QtWidgets.QApplication.translate(
                'main', 
                'Open an existing file'))
        self.openExistingFileButton.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 
                'Opens an existing file structure already to change it.'))
        self.openExistingFileButton.clicked.connect(self.openExistingFile)

        # la double liste de sélection  (pages 1 à 5):
        self.selectionWidget = utils_2lists.DoubleListWidget(self.main)

        # la liste des matières disponibles (page finale) :
        self.matiereLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Subject:')))
        self.matieresComboBox = QtWidgets.QComboBox()
        remplirMatieresComboBox(self)
        self.matieresComboBox.setMinimumWidth(200)
        self.matieresComboBox.activated.connect(self.matiereChanged)

        # un QLineEdit pour le titre du fichier de structure (page finale) :
        self.titleStructureLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Title:')))
        self.titleStructureEdit = QtWidgets.QLineEdit()
        self.titleStructureEdit.textEdited.connect(self.titleEdited)

        # un WebViewWidget pour la description (page finale) :
        self.descriptionLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Description:')))
        self.descriptionWidget = utils_htmleditor.WebViewWidget(
            parent=self.main, askOnLink=False)

        # des boutons :
        dialogButtons = utils_functions.createDialogButtons(
            self, 
            layout=True, 
            buttons=('back', 'next', 'finish', 'cancel'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]

        # on agence tout ça :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.titleLabel)
        layout.addWidget(self.markdownHelpViewer)
        layout.addWidget(self.selectionWidget)
        layout.addWidget(self.openExistingFileButton)
        # matière et titre :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.matiereLabel)
        hLayout.addWidget(self.matieresComboBox)
        hLayout.addWidget(self.titleStructureLabel)
        hLayout.addWidget(self.titleStructureEdit)
        layout.addLayout(hLayout)
        # description :
        layout.addWidget(self.descriptionLabel)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.descriptionWidget)
        groupBox = QtWidgets.QGroupBox()
        groupBox.setLayout(vLayout)
        layout.addWidget(groupBox)
        # les boutons :
        layout.addLayout(buttonsLayout)
        self.setLayout(layout)

        # on appelle la première page :
        self.loadPage()

    def doBack(self):
        self.testRecord()
        self.page['next'] = self.page['actual']
        self.page['actual'] = self.page['back']
        self.page['back'] -= 1
        if self.page['back'] == self.page['final'] - 1:
            self.page['back'] = self.page['last']
        self.loadPage()

    def doNext(self):
        self.testRecord()
        self.page['back'] = self.page['actual']
        self.page['actual'] = self.page['next']
        self.page['next'] += 1
        if self.page['next'] == self.page['last'] + 1:
            self.page['next'] = self.page['final']
        self.loadPage()

    def matiereChanged(self):
        self.mustRecord = True

    def titleEdited(self):
        self.mustRecord = True
        self.testFinishButton()

    def testFinishButton(self):
        """
        on vérifie qu'un titre a bien été donné
        """
        if self.titleStructureEdit.text() == '':
            canFinish = False
        else:
            canFinish = True
        self.buttonsList['finish'].setEnabled(canFinish)

    def doFinish(self):
        self.testRecord()
        self.accept()

    def loadPage(self):
        """
        appel et mise en place d'une page du wizard.
        On commence par vérifier ce qui doit 
        être affiché et quels boutons sont actifs.
        Ensuite on remplit les données liées à la page.
        """
        utils_functions.doWaitCursor()
        try:
            # widgets visibles et boutons disponibles selon la page :
            self.openExistingFileButton.setVisible(self.page['actual'] == 0)
            self.selectionWidget.setVisible(
                self.page['actual'] in (1, 2, 3, 4, 5))
            for widget in (
                self.matiereLabel, self.matieresComboBox,
                self.titleStructureLabel, self.titleStructureEdit,
                self.descriptionLabel, self.descriptionWidget):
                widget.setVisible(self.page['actual'] == self.page['final'])
            self.buttonsList['back'].setEnabled(self.page['actual'] > 0)
            self.buttonsList['next'].setEnabled(
                self.page['actual'] < self.page['final'])
            self.buttonsList['finish'].setEnabled(
                self.page['actual'] == self.page['final'])
            if self.page['actual'] == 0:
                # page d'explications :
                self.titleLabel.setText(
                    utils_functions.u('<h1>{0}</h1>').format(
                        QtWidgets.QApplication.translate(
                            'main', 'Introduction')))
                self.markdownHelpViewer.openMdFile(
                    './translations/createStructureFile0', nbLines=-2)
            elif self.page['actual'] == 1:
                # sélection des items ; on remplit la liste :
                self.titleLabel.setText(
                    utils_functions.u('<h1>{0}</h1>').format(
                        QtWidgets.QApplication.translate(
                            'main', 'Step 1 - items selection')))
                self.markdownHelpViewer.openMdFile(
                    './translations/createStructureFile1')
                self.selectionWidget.baseList.clear()
                self.selectionWidget.selectionList.clear()
                query_my = utils_db.query(self.main.db_my)
                query_my = utils_db.queryExecute(
                    utils_db.q_selectAllFromOrder.format(
                        'items', 'UPPER(Name)'), 
                    query=query_my)
                while query_my.next():
                    id_item = int(query_my.value(0))
                    itemName = query_my.value(1)
                    itemLabel = query_my.value(2)
                    itemData = (id_item, itemName, itemLabel)
                    itemText = utils_functions.u('{0} \t[{1}]').format(
                        itemName, itemLabel)
                    itemWidget = QtWidgets.QListWidgetItem(itemText)
                    itemWidget.setData(QtCore.Qt.UserRole, itemData)
                    if itemData in self.items:
                        self.selectionWidget.selectionList.addItem(itemWidget)
                    else:
                        self.selectionWidget.baseList.addItem(itemWidget)
                self.selectionWidget.mustRecord = False
            elif self.page['actual'] == 2:
                # sélection des bilans ; on remplit la liste :
                self.titleLabel.setText(utils_functions.u(
                    '<h1>{0}</h1>').format(
                        QtWidgets.QApplication.translate(
                            'main', 'Step 2 - balances selection')))
                self.markdownHelpViewer.openMdFile(
                    './translations/createStructureFile2')
                self.selectionWidget.baseList.clear()
                self.selectionWidget.selectionList.clear()
                query_my = utils_db.query(self.main.db_my)
                query_my = utils_db.queryExecute(
                    utils_db.q_selectAllFromOrder.format(
                        'bilans', 'UPPER(Name)'), 
                    query=query_my)
                while query_my.next():
                    id_bilan = int(query_my.value(0))
                    bilanName = query_my.value(1)
                    bilanLabel = query_my.value(2)
                    id_competence = int(query_my.value(3))
                    # couleur du bilan :
                    if id_competence == -1:
                        # bilan perso :
                        color = 'blue'
                    elif id_competence == -2:
                        # bilan perso non calculé :
                        color = 'blue2'
                    elif id_competence > utils.decalageCFD:
                        # confidentiel :
                        color = 'cyan'
                    elif id_competence > utils.decalageBLT:
                        # bulletin :
                        color = 'green'
                    else:
                        # referentiel :
                        color = 'magenta'
                    bilanData = (
                        id_bilan, bilanName, bilanLabel, id_competence)
                    # on place le bilan dans la bonne liste :
                    bilanText = utils_functions.u('{0} \t[{1}]').format(
                        bilanName, bilanLabel)
                    bilanWidget = QtWidgets.QListWidgetItem(bilanText)
                    bilanWidget.setData(QtCore.Qt.UserRole, bilanData)
                    bilanWidget.setIcon(
                        self.icons[color])
                    if bilanData in self.bilans:
                        self.selectionWidget.selectionList.addItem(
                            bilanWidget)
                    else:
                        self.selectionWidget.baseList.addItem(
                            bilanWidget)
                self.selectionWidget.mustRecord = False
            elif self.page['actual'] == 3:
                # sélection du profil ; on remplit la liste :
                trMessage = QtWidgets.QApplication.translate(
                    'main', 
                    'Step 3 - default profile selection (for bulletins)')
                self.titleLabel.setText(utils_functions.u(
                    '<h1>{0}</h1>').format(trMessage))
                self.markdownHelpViewer.openMdFile(
                    './translations/createStructureFile3')
                # remplissage de la liste des bilans 
                # disponibles pour le profil :
                self.selectionWidget.baseList.clear()
                self.selectionWidget.selectionList.clear()
                # une liste temporaire pour retrouver l'ordre du profil :
                tempList = []
                for bilanData in self.bilans:
                    id_bilan = bilanData[0]
                    bilanName = bilanData[1]
                    bilanLabel = bilanData[2]
                    id_competence = bilanData[3]
                    # couleur et classType du bilan :
                    if id_competence == -1:
                        # bilan perso :
                        color = 'blue'
                    elif id_competence == -2:
                        # bilan perso non calculé :
                        continue
                    elif id_competence > utils.decalageCFD:
                        # confidentiel :
                        continue
                    elif id_competence > utils.decalageBLT:
                        # bulletin :
                        continue
                    else:
                        # referentiel :
                        color = 'magenta'
                    # on place le bilan dans la bonne liste :
                    if bilanData in self.profil:
                        tempList.append(bilanData)
                    else:
                        bilanText = utils_functions.u('{0} \t[{1}]').format(
                            bilanName, bilanLabel)
                        bilanWidget = QtWidgets.QListWidgetItem(bilanText)
                        bilanWidget.setData(QtCore.Qt.UserRole, bilanData)
                        bilanWidget.setIcon(
                            self.icons[color])
                        self.selectionWidget.baseList.addItem(bilanWidget)
                for bilanData in self.profil:
                    if bilanData in tempList:
                        bilanName = bilanData[1]
                        bilanLabel = bilanData[2]
                        id_competence = bilanData[3]
                        # couleur du bilan :
                        if id_competence == -1:
                            # bilan perso :
                            color = 'blue'
                        else:
                            # referentiel :
                            color = 'magenta'
                        bilanText = utils_functions.u('{0} \t[{1}]').format(
                            bilanName, bilanLabel)
                        bilanWidget = QtWidgets.QListWidgetItem(bilanText)
                        bilanWidget.setData(QtCore.Qt.UserRole, bilanData)
                        bilanWidget.setIcon(
                            self.icons[color])
                        self.selectionWidget.selectionList.addItem(
                            bilanWidget)
                self.selectionWidget.mustRecord = False
            elif self.page['actual'] == 4:
                # sélection des modèles (et tableaux) ; on remplit la liste.
                # On vérifie aussi les items liés afin de 
                # les ajouter à la liste des items :
                trMessage = QtWidgets.QApplication.translate(
                    'main', 'Step 4 - tables models selection')
                self.titleLabel.setText(
                    utils_functions.u('<h1>{0}</h1>').format(trMessage))
                self.markdownHelpViewer.openMdFile(
                    './translations/createStructureFile4')
                # remplissage de la liste des tableaux :
                self.selectionWidget.baseList.clear()
                self.selectionWidget.selectionList.clear()
                query_my = utils_db.query(self.main.db_my)
                boldFont = QtGui.QFont()
                boldFont.setBold(True)
                # on commence par les modèles de tableaux :
                item = QtWidgets.QListWidgetItem(
                    QtWidgets.QApplication.translate(
                        'main', 'TABLES TEMPLATES'))
                item.setFont(boldFont)
                item.setTextAlignment(QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.NoItemFlags)
                self.selectionWidget.baseList.addItem(item)
                commandLine_my = utils_db.q_selectAllFromOrder.format(
                    'templates', 'ordre, UPPER(Name)')
                query_my = utils_db.queryExecute(
                    commandLine_my, query=query_my)
                while query_my.next():
                    id_template = int(query_my.value(0))
                    templateName = query_my.value(1)
                    templateLabel = query_my.value(3)
                    templateData = (id_template, templateName, templateLabel)
                    templateText = utils_functions.u('{0} \t[{1}]').format(
                        templateName, templateLabel)
                    templateWidget = QtWidgets.QListWidgetItem(templateText)
                    templateWidget.setData(QtCore.Qt.UserRole, templateData)
                    if templateData in self.templates:
                        self.selectionWidget.selectionList.addItem(
                            templateWidget)
                    else:
                        self.selectionWidget.baseList.addItem(templateWidget)
                # on ajoute les tableaux sans modèle :
                item = QtWidgets.QListWidgetItem(
                    QtWidgets.QApplication.translate(
                        'main', 'TABLES WITHOUT TEMPLATES'))
                item.setFont(boldFont)
                item.setTextAlignment(QtCore.Qt.AlignHCenter)
                item.setFlags(QtCore.Qt.NoItemFlags)
                self.selectionWidget.baseList.addItem(item)
                commandLine_my = (
                    'SELECT DISTINCT tableaux.* '
                    'FROM tableau_item '
                    'JOIN tableaux '
                    'ON tableaux.id_tableau=tableau_item.id_tableau '
                    'WHERE tableau_item.id_tableau>-1 '
                    'AND tableau_item.id_item>-1')
                query_my = utils_db.queryExecute(
                    commandLine_my, query=query_my)
                while query_my.next():
                    id_tableau = int(query_my.value(0))
                    tableauName = query_my.value(1)
                    tableauLabel = query_my.value(6)
                    templateData = (id_tableau, tableauName, tableauLabel)
                    tableauText = utils_functions.u('{0} \t[{1}]').format(
                        tableauName, tableauLabel)
                    tableauWidget = QtWidgets.QListWidgetItem(tableauText)
                    tableauWidget.setData(QtCore.Qt.UserRole, templateData)
                    if templateData in self.templates:
                        self.selectionWidget.selectionList.addItem(
                            tableauWidget)
                    else:
                        self.selectionWidget.baseList.addItem(tableauWidget)
                self.selectionWidget.mustRecord = False
            elif self.page['actual'] == 5:
                # sélection des comptages ; on remplit la liste.
                self.titleLabel.setText(
                    utils_functions.u('<h1>{0}</h1>').format(
                        QtWidgets.QApplication.translate(
                            'main', 'Step 5 - counts selection')))
                self.markdownHelpViewer.openMdFile(
                    './translations/createStructureFile5')
                # remplissage de la liste des comptages :
                self.selectionWidget.baseList.clear()
                self.selectionWidget.selectionList.clear()
                query_my = utils_db.query(self.main.db_my)
                query_my = utils_db.queryExecute(
                    utils_db.qs_prof_counts3, query=query_my)
                while query_my.next():
                    countName = query_my.value(0)
                    sens = int(query_my.value(1))
                    calcul = int(query_my.value(2))
                    countLabel = query_my.value(3)
                    id_item = int(query_my.value(4))
                    countData = (countName, sens, calcul, countLabel, id_item)
                    countText = utils_functions.u('{0} \t[{1}]').format(
                        countName, countLabel)
                    countWidget = QtWidgets.QListWidgetItem(countText)
                    countWidget.setData(QtCore.Qt.UserRole, countData)
                    if countData in self.counts:
                        self.selectionWidget.selectionList.addItem(
                            countWidget)
                    else:
                        self.selectionWidget.baseList.addItem(countWidget)
                self.selectionWidget.mustRecord = False
            elif self.page['actual'] == self.page['final']:
                # matière, titre et description de la structure :
                self.titleLabel.setText(
                    utils_functions.u('<h1>{0}</h1>').format(
                        QtWidgets.QApplication.translate(
                            'main', 
                            'Last step - subject, title and description')))
                self.markdownHelpViewer.openMdFile(
                    './translations/createStructureFile99', nbLines=-3)
                index = self.matieresComboBox.findText(self.matiereName)
                if index > -1:
                    self.matieresComboBox.setCurrentIndex(index)
                self.titleStructureEdit.setText(self.title)
                self.descriptionWidget.webView.setHtml(self.description)
                self.mustRecord = False
                self.testFinishButton()
        finally:
            utils_functions.restoreCursor()

    def testRecord(self):
        """
        mise à jour des variables du wizard avant de changer de page.
        """
        if self.page['actual'] == 0:
            # page d'explications ; rien à enregistrer :
            return
        elif self.page['actual'] == self.page['final']:
            # matière, titre et description de la structure :
            if not(self.mustRecord):
                return
            index = self.matieresComboBox.currentIndex()
            self.matiereName = self.matieresComboBox.currentText()
            self.title = self.titleStructureEdit.text()
            description = self.descriptionWidget.webView.toHtml()
            self.description = utils_htmleditor.formatAdvice(
                description, blankTarget=False)
            self.mustRecord = False
        if not(self.selectionWidget.mustRecord):
            return
        if self.page['actual'] == 1:
            # sélection des items :
            self.items = []
            for i in range(self.selectionWidget.selectionList.count()):
                itemWidget = self.selectionWidget.selectionList.item(i)
                itemData = itemWidget.data(QtCore.Qt.UserRole)
                self.items.append(tuple(itemData))
            self.selectionWidget.mustRecord = False
        elif self.page['actual'] == 2:
            # sélection des bilans :
            self.bilans = []
            for i in range(self.selectionWidget.selectionList.count()):
                bilanWidget = self.selectionWidget.selectionList.item(i)
                bilanData = bilanWidget.data(QtCore.Qt.UserRole)
                self.bilans.append(tuple(bilanData))
            self.selectionWidget.mustRecord = False
        elif self.page['actual'] == 3:
            # sélection du profil :
            self.profil = []
            for i in range(self.selectionWidget.selectionList.count()):
                bilanWidget = self.selectionWidget.selectionList.item(i)
                bilanData = bilanWidget.data(QtCore.Qt.UserRole)
                self.profil.append(tuple(bilanData))
            self.selectionWidget.mustRecord = False
        elif self.page['actual'] == 4:
            # sélection des modèles :
            self.templates = []
            query_my = utils_db.query(self.main.db_my)
            for i in range(self.selectionWidget.selectionList.count()):
                templateWidget = self.selectionWidget.selectionList.item(i)
                templateData = templateWidget.data(QtCore.Qt.UserRole)
                self.templates.append(tuple(templateData))
                # vérification des items :
                commandLine_my = utils_db.qs_prof_tableauItem2.format(
                    templateData[0])
                query_my = utils_db.queryExecute(
                    commandLine_my, query=query_my)
                while query_my.next():
                    id_item = int(query_my.value(0))
                    itemName = query_my.value(1)
                    itemLabel = query_my.value(2)
                    itemData = (id_item, itemName, itemLabel)
                    if not(itemData in self.items):
                        self.items.append(tuple(itemData))
            self.selectionWidget.mustRecord = False
        elif self.page['actual'] == 5:
            # sélection des comptages :
            self.counts = []
            for i in range(self.selectionWidget.selectionList.count()):
                countWidget = self.selectionWidget.selectionList.item(i)
                countData = countWidget.data(QtCore.Qt.UserRole)
                self.counts.append(tuple(countData))
            self.selectionWidget.mustRecord = False

    def reInit(self):
        """
        on réinitialise les variables du Wizard :
            fileName : nom du fichier
            matiereName, title, description : description de la structure
            items, bilans, profil, templates : contenu des tables 
                indispensables pour afficher la structure
            mustRecord : pour savoir si on a modifié quelquechose
        """
        # on réinitialise les variables du Wizard :
        self.fileName = ''
        self.items = []
        self.bilans = []
        self.profil = []
        self.templates = []
        self.counts = []
        self.matiereName = self.main.me['Matiere']
        self.title = ''
        self.description = ''
        self.mustRecord = False

    def openExistingFile(self):
        """
        ouverture d'un fichier de structure déjà existant.
        """
        fileName = QtWidgets.QFileDialog.getOpenFileName(
            self, QtWidgets.QApplication.translate(
                'main', 'Open sqlite File'), 
            self.main.workDir, 
            QtWidgets.QApplication.translate(
                'main', 'sqlite files (*.sqlite)'))
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        self.main.workDir = QtCore.QFileInfo(fileName).absolutePath()
        # on ouvre le fichier et on met à jour les données :
        import utils_filesdirs
        ok = False
        self.reInit()
        message = QtWidgets.QApplication.translate(
            'main', 
            'The structure of the file that you selected is not correct.')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>').format(
                utils.SEPARATOR_LINE, message)
        utils_functions.doWaitCursor()
        dbFileTemp_structure = utils_functions.u(
            '{0}/structure_file.sqlite').format(self.main.tempPath)
        utils_filesdirs.removeAndCopy(fileName, dbFileTemp_structure)
        db_structure = utils_db.createConnection(
            self.main, dbFileTemp_structure)[0]
        try:
            # on commence par vérifier que le fichier 
            # est bien un fichier de structure :
            configDict = utils_db.configTable2Dict(db_structure)
            query, transactionOK = utils_db.queryTransactionDB(db_structure)
            versionDB = configDict.get('versionDB', (-1, ''))
            if versionDB[1] != 'STRUCTURE':
                return
            if versionDB[0] < utils.VERSIONDB_STRUCTURE:
                # prévoir ici les modifs de version
                return
            # on récupère les autres données de config :
            self.matiereName = configDict.get('matiere', (-1, ''))[1]
            self.title = configDict.get('title', (-1, ''))[1]
            self.description = configDict.get('description', (-1, ''))[1]
            query = utils_db.query(db_structure)
            # récupération de la table items :
            self.items = utils_db.table2List('items', query=query)
            # récupération de la table bilans :
            self.bilans = utils_db.table2List('bilans', query=query)
            # récupération de la table profil_bilan_BLT :
            self.profil = []
            bilanDic = {}
            for bilanData in self.bilans:
                bilanDic[bilanData[0]] = bilanData
            lines = utils_db.table2List(
                'profil_bilan_BLT', query=query, order='ordre')
            order = 0
            for lineData in lines:
                bilanData = bilanDic.get(lineData[1], None)
                if bilanData != None:
                    self.profil.append(tuple(bilanData))
            # récupération de la table templates :
            self.templates = []
            lines = utils_db.table2List(
                'templates', query=query, order='ordre')
            for lineData in lines:
                self.templates.append(
                    (lineData[0], lineData[1], lineData[3]))
            # récupération de la table counts :
            self.counts = []
            lines = utils_db.table2List(
                'counts', query=query, order='ordre')
            for lineData in lines:
                try:
                    self.counts.append((
                        lineData[3], 
                        lineData[4], 
                        lineData[5], 
                        lineData[7], 
                        lineData[8]))
                except:
                    self.counts.append((
                        lineData[3], 
                        lineData[4], 
                        lineData[5], 
                        lineData[7], 
                        -1))
            # fin :
            self.fileName = fileName
            ok = True
        except:
            print('EXCEPT in openExistingFile')
        finally:
            utils_db.endTransaction(query, db_structure, transactionOK)
            utils_functions.restoreCursor()
            if not(ok):
                utils_functions.messageBox(
                    self.main, level='warning', message=message)
            else:
                self.doNext()

    def createStructureFile(self):
        """
        création du fichier sqlite de la structure.
        """
        import utils_filesdirs
        # s'il faut donner un nom au fichier :
        if self.fileName == '':
            saveTitle = QtWidgets.QApplication.translate(
                'main', 'Save sqlite File')
            matiereCode = self.main.me['MATIERES']['Matiere2Code'].get(
                self.matiereName, '')
            proposedName = utils_functions.u(
                '{0}/{1}_{2}-structure.sqlite').format(
                    self.main.workDir, matiereCode, self.main.me['userId'])
            saveExt = QtWidgets.QApplication.translate(
                'main', 'sqlite files (*.sqlite)')
            fileName = QtWidgets.QFileDialog.getSaveFileName(
                self.main, saveTitle, proposedName, saveExt)
            fileName = utils_functions.verifyLibs_fileName(fileName)
            if fileName != '':
                self.main.workDir = QtCore.QFileInfo(fileName).absolutePath()
                self.fileName = fileName
        if self.fileName == '':
            return
        # on travaille dans temp :
        utils_functions.doWaitCursor()
        dbFileTemp_structure = utils_functions.u(
            '{0}/structure_file.sqlite').format(self.main.tempPath)
        if QtCore.QFile(dbFileTemp_structure).exists():
            removeOK = QtCore.QFile(dbFileTemp_structure).remove()
            if not(removeOK):
                utils_functions.myPrint(
                    'REMOVE ERROR : ', dbFileTemp_structure)
        db_structure = utils_db.createConnection(
            self.main, dbFileTemp_structure)[0]
        try:
            query, transactionOK = utils_db.queryTransactionDB(db_structure)
            query_my = utils_db.query(self.main.db_my)
            # suppression des tables (en cas d'ouverture 
            # d'un fichier déjà existant) :
            listCommands = (
                utils_db.q_dropTable.format('config'),
                utils_db.q_dropTable.format('items'),
                utils_db.q_dropTable.format('bilans'),
                utils_db.q_dropTable.format('profil_bilan_BLT'),
                utils_db.q_dropTable.format('templates'),
                utils_db.q_dropTable.format('item_bilan'),
                utils_db.q_dropTable.format('bilan_bilan'),
                utils_db.q_dropTable.format('comments'),
                utils_db.q_dropTable.format('tableau_item'),
                utils_db.q_dropTable.format('counts'),
                )
            query = utils_db.queryExecute(listCommands, query=query)
            # création des tables :
            listCommands = (
                utils_db.qct_config, 
                utils_db.qct_prof_items,
                utils_db.qct_prof_bilans,
                utils_db.qct_prof_profilBilanBLT,
                utils_db.qct_prof_templates,
                utils_db.qct_prof_itemBilan,
                utils_db.qct_prof_bilanBilan,
                utils_db.qct_prof_comments,
                utils_db.qct_prof_tableauItem,
                utils_db.qct_prof_counts,
                )
            query = utils_db.queryExecute(listCommands, query=query)
            # remplissage de la table config :
            lines = []
            lines.append((
                'versionDB', utils.VERSIONDB_STRUCTURE, 'STRUCTURE'))
            lines.append(('matiere', '', self.matiereName))
            lines.append(('title', '', self.title))
            lines.append(('description', '', self.description))
            commandLine = utils_db.insertInto('config')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table items et récupération des id :
            lines = []
            itemsIds = []
            for itemData in self.items:
                lines.append(itemData)
                itemsIds.append(itemData[0])
            commandLine = utils_db.insertInto('items')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table bilans et récupération des id :
            lines = []
            bilansIds = []
            for bilanData in self.bilans:
                lines.append(bilanData)
                bilansIds.append(bilanData[0])
            commandLine = utils_db.insertInto('bilans')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table profil_bilan_BLT :
            lines = []
            order = 0
            for bilanData in self.profil:
                lines.append((-1, bilanData[0], order))
                order += 1
            commandLine = utils_db.insertInto('profil_bilan_BLT')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table templates et récupération des id.
            # on cherche un id_template (< 0) 
            # disponible pour les tableaux sans modèle.
            # on a aussi besoin d'un dico (id2id) pour relier 
            # les anciens et nouveaux ids :
            lines = []
            templatesIds = []
            id2id = {}
            order = 0
            new_id_template = -1
            commandLine_my = utils_db.q_selectAllFromOrder.format(
                'templates', 'id_template')
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if query_my.first():
                new_id_template = int(query_my.value(0)) - 1
            for templateData in self.templates:
                id_template = templateData[0]
                templateName = templateData[1]
                templateLabel = templateData[2]
                if id_template > -1:
                    id_template = new_id_template
                    new_id_template -= 1
                lines.append((
                    id_template, templateName, order, templateLabel))
                templatesIds.append(templateData[0])
                id2id[templateData[0]] = id_template
                order += 1
            commandLine = utils_db.insertInto('templates')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # pour les requêtes (IN...) :
            id_items = utils_functions.array2string(itemsIds)
            id_bilans = utils_functions.array2string(bilansIds)
            id_templates = utils_functions.array2string(templatesIds)
            # remplissage de la table item_bilan :
            lines = []
            commandLine_my = (
                'SELECT * FROM item_bilan '
                'WHERE id_item IN ({0}) '
                'AND id_bilan IN ({1})').format(id_items, id_bilans)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(0))
                id_bilan = int(query_my.value(1))
                coeff = int(query_my.value(2))
                lines.append((id_item, id_bilan, coeff))
            commandLine = utils_db.insertInto('item_bilan')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table bilan_bilan :
            lines = []
            commandLine_my = (
                'SELECT * FROM bilan_bilan '
                'WHERE id_bilan1 IN ({0}) '
                'AND id_bilan2 IN ({1})').format(id_bilans, id_bilans)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_bilan1 = int(query_my.value(0))
                id_bilan2 = int(query_my.value(1))
                lines.append((id_bilan1, id_bilan2))
            commandLine = utils_db.insertInto('bilan_bilan')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table comments (items puis bilans) :
            lines = []
            commandLine_my = (
                'SELECT * FROM comments '
                'WHERE isItem=1 '
                'AND id IN ({0})').format(id_items)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_item = int(query_my.value(1))
                comment = query_my.value(2)
                lines.append((1, id_item, comment))
            commandLine_my = (
                'SELECT * FROM comments '
                'WHERE isItem=0 '
                'AND id IN ({0})').format(id_bilans)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_bilan = int(query_my.value(1))
                comment = query_my.value(2)
                lines.append((0, id_bilan, comment))
            commandLine = utils_db.insertInto('comments')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table tableau_item :
            lines = []
            commandLine_my = (
                'SELECT * FROM tableau_item '
                'WHERE id_tableau IN ({0})').format(id_templates)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_tableau = int(query_my.value(0))
                id_item = int(query_my.value(1))
                ordre = int(query_my.value(2))
                id_template = id2id[id_tableau]
                lines.append((id_template, id_item, ordre))
            commandLine = utils_db.insertInto('tableau_item')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
            # remplissage de la table counts :
            lines = []
            order = 0
            for countData in self.counts:
                lines.append((
                    -1, -1, -1, 
                    countData[0], 
                    countData[1], 
                    countData[2], 
                    order, 
                    countData[3], 
                    countData[4]))
                order += 1
            commandLine = utils_db.insertInto('counts')
            query = utils_db.queryExecute({commandLine: lines}, query=query)
        finally:
            utils_db.endTransaction(query, db_structure, transactionOK)
            utils_filesdirs.removeAndCopy(
                dbFileTemp_structure, self.fileName)
            utils_functions.restoreCursor()




###########################################################"
#   IMPORTATION D'UN FICHIER DE STRUCTURE
###########################################################"

class ImportStructureFileWizard(QtWidgets.QDialog):
    """
    le Wizard d'importation d'un fichier de structure.
    On crée tous les widgets nécessaires mais 
    ils seront visibles selon la page.
    Pages :
        0 : introduction et choix de l'origine du fichier
        1 : fichier web (établissement ou VÉRAC)
        2 : description du fichier
        3 : items
        4 : bilans
        5 : modèles
        6 : profil
        7 : comptages
        99 : validation
    """
    def __init__(self, main=None):
        super(ImportStructureFileWizard, self).__init__(main)
        self.main = main
        # page actuelle et navigation :
        self.page = {
            'back': -1, 
            'actual': 0, 
            'next': 2, 
            'last': 7, 
            'final': 99}
        # d'où provient le fichier (local, site étab ou site VÉRAC) :
        self.getFileMode = 'openFile'
        # variables du Wizard :
        self.reInit()
        # titre :
        self.setWindowTitle(QtWidgets.QApplication.translate(
            'main', 'Import a structure file'))
        # titre de l'étape en  cours :
        self.titleLabel = QtWidgets.QLabel()
        # un texte d'aide :
        self.markdownHelpViewer = utils_markdown.MarkdownHelpViewer(
            self.main)

        # les icones :
        self.icons = {}
        colors = ('red', 'yellow', 'blue', 'blue2', 'magenta', 'green', 'cyan')
        for color in colors:
            self.icons[color] = utils.doIcon('color-{0}'.format(color))

        # 3 radio-boutons pour sélectionner la suite (page 0) :
        self.openFileRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'Open File'))
        self.openFileRadio.setIcon(utils.doIcon('database-open'))
        self.openFileRadio.setToolTip(QtWidgets.QApplication.translate(
            'main', 'Opens a file on your computer.'))
        self.openFileRadio.setChecked(True)
        self.openFileRadio.toggled.connect(self.radioToggled)
        self.schoolWebsiteRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'School Website'))
        self.schoolWebsiteRadio.setIcon(utils.doIcon('net-go'))
        self.schoolWebsiteRadio.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Search on the school website.'))
        self.schoolWebsiteRadio.toggled.connect(self.radioToggled)
        self.veracWebsiteRadio = QtWidgets.QRadioButton(
            QtWidgets.QApplication.translate(
                'main', 'VERAC Website'))
        self.veracWebsiteRadio.setIcon(utils.doIcon('logo', ext='png'))
        self.veracWebsiteRadio.setToolTip(
            QtWidgets.QApplication.translate(
                'main', 'Search on the VERAC website.'))
        self.veracWebsiteRadio.toggled.connect(self.radioToggled)

        # la liste des matières disponibles (page 2) :
        self.matiereLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Subject:')))
        self.matieresComboBox = QtWidgets.QComboBox()
        remplirMatieresComboBox(self)
        self.matieresComboBox.setMinimumWidth(200)
        self.matieresComboBox.activated.connect(self.matiereChanged)

        # un QLineEdit pour le titre du fichier de structure (page 2) :
        self.titleStructureLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Title:')))
        self.titleStructureEdit = QtWidgets.QLineEdit()
        self.titleStructureEdit.setReadOnly(True)

        # un WebView pour la description (page 2) :
        self.descriptionLabel = QtWidgets.QLabel(
            utils_functions.u('<p><b>{0}</b></p>').format(
                QtWidgets.QApplication.translate('main', 'Description:')))
        self.descriptionWidget = utils_webengine.MyWebEngineView(
            self, linksInBrowser=True)

        # une liste pour afficher les structures, items, etc 
        # (pages 1, 3 à 7) :
        self.listWidget = QtWidgets.QListWidget()
        self.listWidget.itemSelectionChanged.connect(
            self.listSelectionChanged)

        # des boutons :
        dialogButtons = utils_functions.createDialogButtons(
            self, 
            layout=True, 
            buttons=('back', 'next', 'last', 'finish', 'cancel'))
        buttonsLayout = dialogButtons[0]
        self.buttonsList = dialogButtons[1]

        # on agence tout ça :
        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.titleLabel)
        layout.addWidget(self.markdownHelpViewer)
        # les 3 choix (page 0) :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.openFileRadio)
        hLayout.addWidget(self.schoolWebsiteRadio)
        hLayout.addWidget(self.veracWebsiteRadio)
        self.radioGroupBox = QtWidgets.QGroupBox()
        self.radioGroupBox.setLayout(hLayout)
        layout.addWidget(self.radioGroupBox)
        # liste (pages 1, 3 à 7) :
        layout.addWidget(self.listWidget)
        # matière et titre (page 2) :
        hLayout = QtWidgets.QHBoxLayout()
        hLayout.addWidget(self.matiereLabel)
        hLayout.addWidget(self.matieresComboBox)
        hLayout.addWidget(self.titleStructureLabel)
        hLayout.addWidget(self.titleStructureEdit)
        layout.addLayout(hLayout)
        # description (page 2) :
        layout.addWidget(self.descriptionLabel)
        vLayout = QtWidgets.QVBoxLayout()
        vLayout.addWidget(self.descriptionWidget)
        self.descriptionGroupBox = QtWidgets.QGroupBox()
        self.descriptionGroupBox.setLayout(vLayout)
        layout.addWidget(self.descriptionGroupBox)
        # les boutons :
        layout.addLayout(buttonsLayout)
        self.setLayout(layout)

        # les titres des pages :
        self.pagesTitles = {
            0: QtWidgets.QApplication.translate('main', 'Introduction'), 
            10: QtWidgets.QApplication.translate('main', 'School Website'), 
            11: QtWidgets.QApplication.translate('main', 'VERAC Website'), 
            2: QtWidgets.QApplication.translate('main', 'Description'), 
            3: self.main.TR_ITEMS, 
            4: QtWidgets.QApplication.translate('main', 'Balances'), 
            5: QtWidgets.QApplication.translate('main', 'Tables templates'), 
            6: QtWidgets.QApplication.translate(
                'main', 'Default profile (for bulletins)'), 
            7: QtWidgets.QApplication.translate('main', 'Counts'), 
            99: QtWidgets.QApplication.translate('main', 'Last step'), 
            }

        # on appelle la première page :
        self.loadPage()

    def resizeEvent(self, event):
        """
        calcul de la taille des icônes de la page 0
        """
        if self.page['actual'] == 0:
            ICON_SIZE = self.width() // 12
            for widget in (
                self.openFileRadio, 
                self.schoolWebsiteRadio, 
                self.veracWebsiteRadio):
                    widget.setIconSize(QtCore.QSize(ICON_SIZE, ICON_SIZE))
            super(ImportStructureFileWizard, self).resizeEvent(event)
            self.update()

    def listSelectionChanged(self):
        """
        page 1 (fichier web) : pour ne rendre disponible le bouton suivant 
        que si un fichier est sélectionné
        """
        if self.page['actual'] == 1:
            itemWidget = self.listWidget.currentItem()
            try:
                (matiereName, title, fileName, subDir) = itemWidget.data(
                    QtCore.Qt.UserRole)
                self.buttonsList['next'].setEnabled(True)
            except:
                self.buttonsList['next'].setEnabled(False)

    def doBack(self):
        # si on avait cliqué sur le bouton "dernière étape"
        # puis qu'on revient, il faudra avancer à nouveau d'une seule étape :
        if (self.page['actual'] == self.page['final']) \
            and (self.page['back'] < self.page['last']):
                self.page['next'] = self.page['back'] + 1
        else:
            self.page['next'] = self.page['actual']
        # si on a sélectionné un fichier et qu'on revient en arrière,
        # on doit passer la page web :
        if (self.getFileMode == 'openFile') and (self.page['actual'] == 2):
            self.page['actual'] = 0
        else:
            self.page['actual'] = self.page['back']
        self.page['back'] -= 1
        if self.page['back'] == self.page['final'] - 1:
            self.page['back'] = self.page['last']
        self.loadPage()

    def doNext(self):
        """
        avant de passer à la page suivante, si on est à la page 0 ou 1,
        on vérifie le fichier.
        """
        if (self.page['actual'] == 0) and (self.getFileMode == 'openFile'):
            # si on a choisi un fichier local, 
            # on le sélectionne avant de continuer :
            fileName = QtWidgets.QFileDialog.getOpenFileName(
                self, 
                QtWidgets.QApplication.translate(
                    'main', 'Open sqlite File'), 
                self.main.workDir, 
                QtWidgets.QApplication.translate(
                    'main', 'sqlite files (*.sqlite)'))
            fileName = utils_functions.verifyLibs_fileName(fileName)
            if fileName == '':
                return
            self.main.workDir = QtCore.QFileInfo(fileName).absolutePath()
            self.fileName = fileName
        elif self.page['actual'] == 1:
            # si on passe par le web, on télécharge dans temp :
            import utils_web
            itemWidget = self.listWidget.currentItem()
            try:
                (matiereName, title, fileName, subDir) = itemWidget.data(
                    QtCore.Qt.UserRole)
            except:
                message = QtWidgets.QApplication.translate(
                    'main', 
                    'You must select a structure before continuing!')
                utils_functions.messageBox(
                    self.main, level='warning', message=message)
                return
            destDir = utils_functions.addSlash(self.main.tempPath)
            theFileExists = False
            if self.getFileMode == 'schoolWebsite':
                # téléchargement du fichier en interrogeant le site :
                if utils_web.downloadFileFromSecretDir(
                    self.main, 
                    '/protected/structures/', 
                    fileName, 
                    toDir=destDir):
                        theFileExists = True
            else:
                # téléchargement du fichier :
                utils_functions.doWaitCursor()
                try:
                    theUrl = '{0}structures/{1}/'.format(
                        utils.UPDATEURL, subDir)
                    downLoader = utils_web.UpDownLoader(
                        self.main, 
                        fileName, 
                        destDir, 
                        theUrl, 
                        down=True, 
                        showProgress=True)
                    downLoader.launch()
                    while downLoader.state == utils_web.STATE_LAUNCHED:
                        QtWidgets.QApplication.processEvents()
                    if downLoader.state == utils_web.STATE_OK:
                        theFileExists = True
                        OK = 1
                finally:
                    utils_functions.restoreCursor()
            self.fileName = utils_functions.u('{0}/{1}').format(
                self.main.tempPath, fileName)
        self.page['back'] = self.page['actual']
        self.page['actual'] = self.page['next']
        self.page['next'] += 1
        if self.page['next'] == self.page['last'] + 1:
            self.page['next'] = self.page['final']
        self.loadPage()

    def radioToggled(self, checked):
        if checked:
            if self.sender() == self.openFileRadio:
                self.getFileMode = 'openFile'
                self.page['next'] = 2
            elif self.sender() == self.schoolWebsiteRadio:
                self.getFileMode = 'schoolWebsite'
                self.page['next'] = 1
            else:
                self.getFileMode = 'veracWebsite'
                self.page['next'] = 1

    def doLast(self):
        self.page['next'] = self.page['final']
        self.doNext()

    def matiereChanged(self):
        self.matiereName = self.matieresComboBox.currentText()

    def doFinish(self):
        self.accept()

    def loadPage(self):
        """
        appel et mise en place d'une page du wizard.
        On commence par vérifier ce qui doit être affiché 
        et quels boutons sont actifs.
        Ensuite on remplit les données liées à la page.
        """
        utils_functions.doWaitCursor()
        try:
            # widgets visibles et boutons disponibles selon la page :
            for widget in (
                self.radioGroupBox,
                self.openFileRadio,
                self.schoolWebsiteRadio,
                self.veracWebsiteRadio):
                widget.setVisible(self.page['actual'] == 0)
            self.listWidget.setVisible(
                self.page['actual']  in (1, 3, 4, 5, 6, 7))
            for widget in (
                self.matiereLabel, 
                self.matieresComboBox,
                self.titleStructureLabel, 
                self.titleStructureEdit,
                self.descriptionLabel, 
                self.descriptionGroupBox, 
                self.descriptionWidget):
                    widget.setVisible(self.page['actual'] == 2)
            self.buttonsList['back'].setEnabled(self.page['actual'] > 0)
            self.buttonsList['next'].setEnabled(
                self.page['actual'] < self.page['final'])
            self.buttonsList['last'].setEnabled(
                self.page['actual']  in (2, 3, 4, 5, 6))
            self.buttonsList['finish'].setEnabled(
                self.page['actual'] == self.page['final'])
            pageTitle = self.pagesTitles.get(self.page['actual'], '')
            self.titleLabel.setText(
                utils_functions.u('<h1>{0}</h1>').format(pageTitle))
            if self.page['actual'] == 0:
                # page d'explications :
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile0', nbLines=-2)
                self.reInit()
            elif self.page['actual'] == 1:
                import utils_web
                # liste des fichiers web disponibles
                # en fonction de la page demandée 
                # (schoolWebsite ou veracWebsite) :
                self.reInit()
                self.listWidget.setSelectionMode(
                    QtWidgets.QAbstractItemView.SingleSelection)
                if self.getFileMode == 'schoolWebsite':
                    structures = {'LIST': [-1], -1: ('', '', [])}
                else:
                    structures = {'LIST': [], }
                # téléchargement dans temp :
                theFileName = 'structures.sqlite'
                destDir = utils_functions.addSlash(self.main.tempPath)
                theFileExists = False
                if self.getFileMode == 'schoolWebsite':
                    self.titleLabel.setText(
                        utils_functions.u('<h1>{0}</h1>').format(
                            self.pagesTitles[10]))
                    self.markdownHelpViewer.openMdFile(
                        './translations/importStructureFile1a')
                    # téléchargement du fichier structures.sqlite 
                    # en interrogeant le site :
                    if utils_web.downloadFileFromSecretDir(
                        self.main, 
                        '/protected/structures/', 
                        theFileName, 
                        toDir=destDir):
                            theFileExists = True
                else:
                    self.titleLabel.setText(
                        utils_functions.u('<h1>{0}</h1>').format(
                            self.pagesTitles[11]))
                    self.markdownHelpViewer.openMdFile(
                        './translations/importStructureFile1b')
                    # téléchargement du fichier structures.sqlite :
                    utils_functions.doWaitCursor()
                    try:
                        theUrl = utils.UPDATEURL + 'structures/'
                        downLoader = utils_web.UpDownLoader(
                            self.main, 
                            theFileName, 
                            destDir, 
                            theUrl, 
                            down=True, 
                            showProgress=True)
                        downLoader.launch()
                        while downLoader.state == utils_web.STATE_LAUNCHED:
                            QtWidgets.QApplication.processEvents()
                        if downLoader.state == utils_web.STATE_OK:
                            theFileExists = True
                            OK = 1
                    finally:
                        utils_functions.restoreCursor()
                # lecture du fichier structures.sqlite :
                if theFileExists:
                    dbFile_structures = utils_functions.u(
                        '{0}/structures.sqlite').format(self.main.tempPath)
                    # on se connecte à structures.sqlite pour remplir 
                    # self.structures :
                    (db_structures, dbName) = utils_db.createConnection(
                        self.main, dbFile_structures)
                    query_structures = utils_db.query(db_structures)
                    try:
                        if self.getFileMode == 'schoolWebsite':
                            cmdLine = utils_db.q_selectAllFromOrder.format(
                                'structures', 
                                'ordre, matiere, fileName')
                            query_structures = utils_db.queryExecute(
                                cmdLine, query=query_structures)
                            while query_structures.next():
                                matiereName = query_structures.value(0)
                                title = query_structures.value(1)
                                fileName = query_structures.value(2)
                                structures[-1][2].append((
                                    matiereName, title, fileName))
                        else:
                            cmdLine = utils_db.q_selectAllFromOrder.format(
                                'structures', 
                                'what, ordre, matiere, fileName')
                            query_structures = utils_db.queryExecute(
                                cmdLine, query=query_structures)
                            while query_structures.next():
                                what = int(query_structures.value(0))
                                matiereName = query_structures.value(1)
                                title = query_structures.value(2)
                                fileName = query_structures.value(3)
                                ordre = int(query_structures.value(4))
                                if not(what in structures['LIST']):
                                    structures['LIST'].append(what)
                                    structures[what] = (
                                        matiereName, fileName, [])
                                else:
                                    structures[what][2].append((
                                        matiereName, title, fileName))
                    finally:
                        # on ferme la base :
                        if query_structures != None:
                            query_structures.clear()
                            del query_structures
                        db_structures.close()
                        del db_structures
                        utils_db.sqlDatabase.removeDatabase(dbName)
                # remplissage de la liste des structures :
                self.listWidget.clear()
                boldFont = QtGui.QFont()
                boldFont.setBold(True)
                for what in structures['LIST']:
                    if len(structures[what][0]) > 1:
                        # on commence par afficher le titre :
                        text = utils_functions.u(
                            '<h1 align="center">{0}</h1>').format(
                                structures[what][0])
                        itemWidget = QtWidgets.QListWidgetItem('\n')
                        itemWidget.setBackground(
                            QtGui.QBrush(QtGui.QColor('#dddddd')))
                        itemWidget.setFlags(QtCore.Qt.NoItemFlags)
                        itemLabel = QtWidgets.QLabel(text)
                        self.listWidget.addItem(itemWidget)
                        self.listWidget.setItemWidget(itemWidget, itemLabel)
                    for itemData in structures[what][2]:
                        matiereName = itemData[0]
                        title = itemData[1]
                        fileName = itemData[2]
                        if matiereName == 'SEPARATOR':
                            itemText = title
                            itemWidget = QtWidgets.QListWidgetItem(itemText)
                            itemWidget.setFont(boldFont)
                            itemWidget.setTextAlignment(
                                QtCore.Qt.AlignHCenter)
                            itemWidget.setBackground(
                                QtGui.QBrush(utils.colorLightGray))
                            itemWidget.setForeground(
                                QtGui.QBrush(utils.colorBlack))
                            itemWidget.setFlags(QtCore.Qt.NoItemFlags)
                            self.listWidget.addItem(itemWidget)
                        else:
                            # calcul du décalage entre matière et titre :
                            spaces = round(70 - 2.5 * len(matiereName))
                            spaces = int(spaces) * '&nbsp;'
                            # le \n sert à augmenter la hauteur du itemWidget :
                            itemWidget = QtWidgets.QListWidgetItem('\n')
                            data = (
                                matiereName, 
                                title, 
                                fileName, 
                                structures[what][1])
                            itemWidget.setData(QtCore.Qt.UserRole, data)
                            # on affiche un label avec formatage à 
                            # la place du itemWidget :
                            itemLabel = QtWidgets.QLabel(
                                utils_functions.u(
                                    '<big><b>{0} :</b>{1}{2}</big>').format(
                                        matiereName, spaces, title))
                            self.listWidget.addItem(itemWidget)
                            self.listWidget.setItemWidget(
                                itemWidget, itemLabel)
                self.listSelectionChanged()

            elif self.page['actual'] == 2:
                # description complète de la structure :
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile2', nbLines=-3)
                # on ouvre le fichier et on met à jour les données :
                self.openStructureFile(self.fileName)
                index = self.matieresComboBox.findText(self.matiereName)
                if index > -1:
                    self.matieresComboBox.setCurrentIndex(index)
                self.titleStructureEdit.setText(self.title)
                if utils_webengine.WEB_ENGINE == 'WEBENGINE':
                    # on crée le fichier html :
                    htmlResult = utils_functions.u(
                        '<!DOCTYPE html> '
                        '<html> '
                        '<body>{0}</body> '
                        '</html> ').format(self.description)
                    if utils.OS_NAME[0] == 'win':
                        import utils_html
                        htmlResult = utils_html.encodeHtml(htmlResult)
                    # on enregistre dans un fichier html temporaire :
                    htmlFileName = self.main.tempPath + '/temp.html'
                    outFile = QtCore.QFile(htmlFileName)
                    if outFile.open(
                        QtCore.QFile.WriteOnly | QtCore.QFile.Text):
                            stream = QtCore.QTextStream(outFile)
                            stream.setCodec('UTF-8')
                            stream << htmlResult
                            outFile.close()
                    htmlFileName = QtCore.QFileInfo(
                        htmlFileName).absoluteFilePath()
                    # on charge le fichier :
                    url = QtCore.QUrl().fromLocalFile(htmlFileName)
                    self.descriptionWidget.load(url)
                else:
                    self.descriptionWidget.setHtml(self.description)
            elif self.page['actual'] == 3:
                # items :
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile3')
                self.listWidget.setSelectionMode(
                    QtWidgets.QAbstractItemView.NoSelection)
                self.listWidget.clear()
                for itemData in self.items:
                    id_item = itemData[0]
                    itemName = itemData[1]
                    itemLabel = itemData[2]
                    itemText = utils_functions.u(
                        '{0} \t[{1}]').format(itemName, itemLabel)
                    itemWidget = QtWidgets.QListWidgetItem(itemText)
                    itemWidget.setData(QtCore.Qt.UserRole, itemData)
                    self.listWidget.addItem(itemWidget)
            elif self.page['actual'] == 4:
                # bilans :
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile4')
                self.listWidget.setSelectionMode(
                    QtWidgets.QAbstractItemView.NoSelection)
                self.listWidget.clear()
                for bilanData in self.bilans:
                    id_bilan = bilanData[0]
                    bilanName = bilanData[1]
                    bilanLabel = bilanData[2]
                    id_competence = bilanData[3]
                    # couleur du bilan :
                    if id_competence == -1:
                        # bilan perso :
                        color = 'blue'
                    elif id_competence == -2:
                        # bilan perso non calculé :
                        color = 'blue2'
                    elif id_competence > utils.decalageCFD:
                        # confidentiel :
                        color = 'cyan'
                    elif id_competence > utils.decalageBLT:
                        # bulletin :
                        color = 'green'
                    else:
                        # referentiel :
                        color = 'magenta'
                    bilanText = utils_functions.u(
                        '{0} \t[{1}]').format(bilanName, bilanLabel)
                    bilanWidget = QtWidgets.QListWidgetItem(bilanText)
                    bilanWidget.setData(QtCore.Qt.UserRole, bilanData)
                    bilanWidget.setIcon(
                        self.icons[color])
                    self.listWidget.addItem(bilanWidget)
            elif self.page['actual'] == 5:
                # modèles :
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile5')
                self.listWidget.setSelectionMode(
                    QtWidgets.QAbstractItemView.NoSelection)
                self.listWidget.clear()
                for templateData in self.templates:
                    id_template = templateData[0]
                    templateName = templateData[1]
                    templateLabel = templateData[2]
                    templateText = utils_functions.u('{0} \t[{1}]').format(
                        templateName, templateLabel)
                    templateWidget = QtWidgets.QListWidgetItem(templateText)
                    templateWidget.setData(QtCore.Qt.UserRole, templateData)
                    self.listWidget.addItem(templateWidget)
            elif self.page['actual'] == 6:
                # profil :
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile6')
                self.listWidget.setSelectionMode(
                    QtWidgets.QAbstractItemView.NoSelection)
                self.listWidget.clear()
                for bilanData in self.profil:
                    bilanName = bilanData[1]
                    bilanLabel = bilanData[2]
                    id_competence = bilanData[3]
                    # couleur du bilan :
                    if id_competence == -1:
                        # bilan perso :
                        color = 'blue'
                    else:
                        # referentiel :
                        color = 'magenta'
                    bilanText = utils_functions.u(
                        '{0} \t[{1}]').format(bilanName, bilanLabel)
                    bilanWidget = QtWidgets.QListWidgetItem(bilanText)
                    bilanWidget.setData(QtCore.Qt.UserRole, bilanData)
                    bilanWidget.setIcon(
                        self.icons[color])
                    self.listWidget.addItem(bilanWidget)
            elif self.page['actual'] == 7:
                # comptages :
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile7')
                self.listWidget.setSelectionMode(
                    QtWidgets.QAbstractItemView.NoSelection)
                self.listWidget.clear()
                for countData in self.counts:
                    countName = countData[0]
                    countLabel = countData[3]
                    countText = utils_functions.u(
                        '{0} \t[{1}]').format(countName, countLabel)
                    countWidget = QtWidgets.QListWidgetItem(countText)
                    countWidget.setData(QtCore.Qt.UserRole, countData)
                    self.listWidget.addItem(countWidget)
            elif self.page['actual'] == self.page['final']:
                # validation :
                modifs = self.doLastStep()
                # try-except si on a annulé :
                try:
                    args = (
                        modifs['newItems'], modifs['updatedItems'], 
                        modifs['newBilans'], modifs['updatedBilans'], 
                        modifs['newTemplates'], modifs['updatedTemplates'], 
                        modifs['newCounts'], modifs['updatedCounts'])
                except:
                    args = (0, 0, 0, 0, 0, 0, 0, 0, )
                self.markdownHelpViewer.openMdFile(
                    './translations/importStructureFile99', 
                    args=args, 
                    nbLines=-2)
        finally:
            utils_functions.restoreCursor()

    def doLastStep(self):
        """
        partie la plus longue.
        On vérifie ce qui sera véritablement importé.
        En cas de doute, on demande à l'utilisateur.
        Les commandes sont inscrites dans une liste (self.dbCommands)
        et ne seront exécutées qu'ensuite (après validation du wizard).
        Un dico (self.dbInserts) gère les insertions.
        On retourne un dictionnaire (result) indiquant ce qui a été modifié.
        """
        import prof_itemsbilans
        self.dbCommands = []
        self.dbInserts = {}
        self.id2id = {'items': {}, 'bilans': {}, 'templates': {}, }
        result = {
            'newItems': 0, 'updatedItems': 0, 
            'newBilans': 0, 'updatedBilans': 0, 
            'newTemplates': 0, 'updatedTemplates': 0,
            'newCounts': 0, 'updatedCounts': 0}
        query_my = utils_db.query(self.main.db_my)
        query_commun = utils_db.query(self.main.db_commun)

        # pour les messages en cas de doublette :
        qName = (
            '<p><b>{0}</b></p>'
            '<p><b>{1}</b> : {2}'
            '<br/>{3}'
            '<br/><b>{4}</b> : {5}</p>')
        qLabel = (
            '<p><b>{0}</b></p>'
            '<p>{1} : <b>{2}</b>'
            '<br/>{3}'
            '<br/>{4} : <b>{5}</b></p>')
        q2 = QtWidgets.QApplication.translate('main', 'in:')

        # ***********************************************
        # vérification des items :
        # ***********************************************
        modifItemNameNoToAll = False
        commandLine_insert = utils_db.insertInto('items')
        self.dbInserts[commandLine_insert] = []
        # on cherche le premier id_item disponible dans db_my :
        id_item_new = 0
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('items', 'id_item'), 
            query=query_my)
        if query_my.last():
            id_item_new = int(query_my.value(0)) + 1
        for itemData in self.items:
            id_item = itemData[0]
            itemName = itemData[1]
            itemLabel = itemData[2]
            etat = 'NEW'
            # est-il déjà là ?
            commandLine_test = utils_db.qs_prof_AllWhereNameLabel.format(
                'items')
            query_my = utils_db.queryExecute(
                {commandLine_test: (itemName, itemLabel)}, query=query_my)
            if query_my.first():
                etat = 'DEJA_LA'
                id_item_my = int(query_my.value(0))
                self.id2id['items'][id_item] = id_item_my
            # le label est modifié ?
            if etat == 'NEW':
                commandLine_test = utils_db.qs_prof_AllWhereName.format(
                    'items')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (itemName, )}, query=query_my)
                if query_my.first():
                    etat = 'LABEL_MODIF'
                    id_item_my = int(query_my.value(0))
                    itemLabelEX = query_my.value(2)
                    self.id2id['items'][id_item] = id_item_my
            # le nom est modifié ?
            if etat == 'NEW':
                commandLine_test = utils_db.qs_prof_AllWhereLabel.format(
                    'items')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (itemLabel, )}, query=query_my)
                if query_my.first():
                    etat = 'NAME_MODIF'
                    id_item_my = int(query_my.value(0))
                    itemNameEX = query_my.value(1)
                    self.id2id['items'][id_item] = id_item_my
            # on passe à l'action :
            if etat == 'NAME_MODIF':
                if not(modifItemNameNoToAll):
                    # on demande confirmation avant de modifier l'item :
                    q1 = QtWidgets.QApplication.translate(
                        'main', 
                        'Modify this item Name ?')
                    question = utils_functions.u(qName).format(
                        q1, itemNameEX, itemLabel, q2, itemName, itemLabel)
                    reply = utils_functions.messageBox(
                        self.main, level='question', message=question,
                        buttons=['Yes', 'NoToAll', 'No', 'Cancel'])
                    if reply == QtWidgets.QMessageBox.NoToAll:
                        modifItemNameNoToAll = True
                        etat = 'NEW'
                    elif reply == QtWidgets.QMessageBox.Yes:
                        commandLine = utils_db.qdf_where.format(
                            'items', 'id_item', id_item_my)
                        self.dbCommands.append(commandLine)
                        self.dbCommands.append('INSERT')
                        self.dbInserts[commandLine_insert].append(
                            (id_item_my, itemName, itemLabel))
                        result['updatedItems'] += 1
                    elif reply == QtWidgets.QMessageBox.No:
                        # on aura donc 2 items de même label :
                        etat = 'NEW'
                    else:
                        utils_functions.restoreCursor()
                        message = QtWidgets.QApplication.translate(
                            'main', 
                            'Import Canceled')
                        utils_functions.messageBox(
                            self.main, level='warning', message=message)
                        return
                else:
                    etat = 'NEW'
            elif etat == 'LABEL_MODIF':
                # on demande confirmation avant de modifier l'item :
                q1 = QtWidgets.QApplication.translate(
                    'main', 
                    'Modify this item Label ?')
                question = utils_functions.u(qLabel).format(
                    q1, itemName, itemLabelEX, q2, itemName, itemLabel)
                reply = utils_functions.messageBox(
                    self.main, level='question', message=question,
                    buttons=['Yes', 'No', 'Cancel'])
                if reply == QtWidgets.QMessageBox.Yes:
                    commandLine = utils_db.qdf_where.format(
                        'items', 'id_item', id_item_my)
                    self.dbCommands.append(commandLine)
                    self.dbCommands.append('INSERT')
                    self.dbInserts[commandLine_insert].append(
                        (id_item_my, itemName, itemLabel))
                    result['updatedItems'] += 1
                elif reply == QtWidgets.QMessageBox.No:
                    # pas 2 items du même nom, donc on passe :
                    pass
                else:
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 
                        'Import Canceled')
                    utils_functions.messageBox(
                        self.main, level='warning', message=message)
                    return
            if etat == 'NEW':
                # on crée le nouvel item :
                id_item_my = id_item_new
                id_item_new += 1
                self.id2id['items'][id_item] = id_item_my
                self.dbCommands.append('INSERT')
                self.dbInserts[commandLine_insert].append(
                    (id_item_my, itemName, itemLabel))
                result['newItems'] += 1

        # ***********************************************
        # vérification des bilans :
        # ***********************************************
        modifBilanNameNoToAll = False
        commandLine_CPT = 'SELECT * FROM {0} WHERE code=? AND Competence=?'
        commandLine_insert = utils_db.insertInto('bilans')
        self.dbInserts[commandLine_insert] = []
        # on cherche le premier id_bilan disponible dans db_my :
        id_bilan_new = 0
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format('bilans', 'id_bilan'), 
            query=query_my)
        if query_my.last():
            id_bilan_new = int(query_my.value(0)) + 1
        for bilanData in self.bilans:
            id_bilan = bilanData[0]
            bilanName = bilanData[1]
            bilanLabel = bilanData[2]
            id_competence = -1#bilanData[3]
            etat = 'NEW'
            # est-il déjà là ?
            commandLine_test = utils_db.qs_prof_AllWhereNameLabel.format(
                'bilans')
            query_my = utils_db.queryExecute(
                {commandLine_test: (bilanName, bilanLabel)}, query=query_my)
            if query_my.first():
                etat = 'DEJA_LA'
                id_bilan_my = int(query_my.value(0))
                self.id2id['bilans'][id_bilan] = id_bilan_my
            # est-ce un bilan du bulletin ?
            commandLine_test = utils_functions.u(
                commandLine_CPT).format('bulletin')
            query_commun = utils_db.queryExecute(
                {commandLine_test: (bilanName, bilanLabel)}, 
                query=query_commun)
            if query_commun.first():
                if etat == 'NEW':
                    etat = 'NEW-COMMON_CPT'
                id_competence = utils.decalageBLT + int(query_commun.value(0))
            # est-ce un bilan du référentiel ?
            commandLine_test = utils_functions.u(
                commandLine_CPT).format('referentiel')
            query_commun = utils_db.queryExecute(
                {commandLine_test: (bilanName, bilanLabel)}, 
                query=query_commun)
            if query_commun.first():
                if etat == 'NEW':
                    etat = 'NEW-COMMON_CPT'
                id_competence = int(query_commun.value(0))
            # est-ce un bilan confidentiel ?
            commandLine_test = utils_functions.u(
                commandLine_CPT).format('confidentiel')
            query_commun = utils_db.queryExecute(
                {commandLine_test: (bilanName, bilanLabel)}, 
                query=query_commun)
            if query_commun.first():
                if etat == 'NEW':
                    etat = 'NEW-COMMON_CPT'
                id_competence = utils.decalageCFD + int(query_commun.value(0))
            # on corrige une éventuelle erreur d'id_competence :
            if (etat == 'DEJA_LA') and (id_competence != bilanData[3]):
                commandLine = utils_db.qdf_where.format(
                    'bilans', 'id_bilan', id_bilan_my)
                self.dbCommands.append(commandLine)
                self.dbCommands.append('INSERT')
                self.dbInserts[commandLine_insert].append(
                    (id_bilan_my, bilanName, bilanLabel, id_competence))
                result['updatedBilans'] += 1
            # le label est modifié ?
            if etat == 'NEW':
                commandLine_test = utils_db.qs_prof_AllWhereName.format(
                    'bilans')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (bilanName, )}, query=query_my)
                if query_my.first():
                    etat = 'LABEL_MODIF'
                    id_bilan_my = int(query_my.value(0))
                    bilanLabelEX = query_my.value(2)
                    self.id2id['bilans'][id_bilan] = id_bilan_my
            # le nom est modifié ?
            if etat == 'NEW':
                commandLine_test = utils_db.qs_prof_AllWhereLabel.format(
                    'bilans')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (bilanLabel, )}, query=query_my)
                if query_my.first():
                    etat = 'NAME_MODIF'
                    id_bilan_my = int(query_my.value(0))
                    bilanNameEX = query_my.value(1)
                    self.id2id['bilans'][id_bilan] = id_bilan_my
            # on passe à l'action :
            if etat == 'NAME_MODIF':
                if not(modifBilanNameNoToAll):
                    # on demande confirmation avant de modifier le bilan :
                    q1 = QtWidgets.QApplication.translate(
                        'main', 
                        'Modify this bilan Name ?')
                    question = utils_functions.u(qName).format(
                        q1, bilanNameEX, bilanLabel, q2, bilanName, bilanLabel)
                    reply = utils_functions.messageBox(
                        self.main, level='question', message=question,
                        buttons=['Yes', 'NoToAll', 'No', 'Cancel'])
                    if reply == QtWidgets.QMessageBox.NoToAll:
                        modifBilanNameNoToAll = True
                        etat = 'NEW'
                    elif reply == QtWidgets.QMessageBox.Yes:
                        commandLine = utils_db.qdf_where.format(
                            'bilans', 'id_bilan', id_bilan_my)
                        self.dbCommands.append(commandLine)
                        self.dbCommands.append('INSERT')
                        self.dbInserts[commandLine_insert].append((
                            id_bilan_my, 
                            bilanName, 
                            bilanLabel, 
                            id_competence))
                        result['updatedBilans'] += 1
                    elif reply == QtWidgets.QMessageBox.No:
                        # on aura donc 2 bilans de même label :
                        etat = 'NEW'
                    else:
                        utils_functions.restoreCursor()
                        message = QtWidgets.QApplication.translate(
                            'main', 
                            'Import Canceled')
                        utils_functions.messageBox(
                            self.main, level='warning', message=message)
                        return
                else:
                    etat = 'NEW'
            elif etat == 'LABEL_MODIF':
                # on demande confirmation avant de modifier le bilan :
                q1 = QtWidgets.QApplication.translate(
                    'main', 
                    'Modify this bilan Label ?')
                question = utils_functions.u(qLabel).format(
                    q1, bilanName, bilanLabelEX, q2, bilanName, bilanLabel)
                reply = utils_functions.messageBox(
                    self.main, level='question', message=question,
                    buttons=['Yes', 'No', 'Cancel'])
                if reply == QtWidgets.QMessageBox.Yes:
                    commandLine = utils_db.qdf_where.format(
                        'bilans', 'id_bilan', id_bilan_my)
                    self.dbCommands.append(commandLine)
                    self.dbCommands.append('INSERT')
                    self.dbInserts[commandLine_insert].append(
                        (id_bilan_my, bilanName, bilanLabel, id_competence))
                    result['updatedBilans'] += 1
                elif reply == QtWidgets.QMessageBox.No:
                    # pas 2 bilans du même nom, donc on passe :
                    pass
                else:
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 
                        'Import Canceled')
                    utils_functions.messageBox(
                        self.main, level='warning', message=message)
                    return
            if etat in ('NEW', 'NEW-COMMON_CPT'):
                # on crée le nouveau bilan :
                id_bilan_my = id_bilan_new
                id_bilan_new += 1
                self.id2id['bilans'][id_bilan] = id_bilan_my
                self.dbCommands.append('INSERT')
                self.dbInserts[commandLine_insert].append(
                    (id_bilan_my, bilanName, bilanLabel, id_competence))
                result['newBilans'] += 1

        # ***********************************************
        # vérification du profil par défaut :
        # ***********************************************
        if len(self.profil) > 0:
            commandLine_insert1 = utils_db.insertInto('profils')
            commandLine_insert2 = utils_db.insertInto('profil_bilan_BLT')
            self.dbInserts[commandLine_insert1] = []
            self.dbInserts[commandLine_insert2] = []
            id_profil_my = 0
            replaceOld = True
            # on cherche s'il y en avait déjà un :
            commandLine_test = utils_functions.u(
                'SELECT * FROM profils WHERE Matiere=? AND id_profil<0')
            query_my = utils_db.queryExecute(
                {commandLine_test: (self.matiereName, )}, 
                query=query_my)
            if query_my.first():
                # on en a trouvé un ; on demande s'il faut le remplacer :
                id_profil_my = int(query_my.value(0))
                q0 = (
                    '<p>{0} <b>{1}</b>.</p>'
                    '<br/>{2}')
                q1 = QtWidgets.QApplication.translate(
                    'main', 
                    'There is already a default profile for this subject:')
                q2 = QtWidgets.QApplication.translate(
                    'main', 
                    'Do you want to replace it with the one contained '
                    'in the structure to import?')
                question = utils_functions.u(q0).format(
                    q1, self.matiereName, q2)
                reply = utils_functions.messageBox(
                    self.main, level='question', message=question,
                    buttons=['Yes', 'No'])
                if reply == QtWidgets.QMessageBox.No:
                    replaceOld = False
                if replaceOld:
                    commandLine = utils_db.qdf_where.format(
                        'profils', 'id_profil', id_profil_my)
                    self.dbCommands.append(commandLine)
                    commandLine = utils_db.qdf_where.format(
                        'profil_bilan_BLT', 'id_profil', id_profil_my)
                    self.dbCommands.append(commandLine)
            else:
                # sinon on cherche le premier id négatif 
                # disponible dans db_my :
                query_my = utils_db.queryExecute(
                    utils_db.q_selectAllFromOrder.format(
                        'profils', 'id_profil'), 
                    query=query_my)
                if query_my.first():
                    id_profil_my = int(query_my.value(0)) - 1
            # au cas où :
            if id_profil_my > -1:
                id_profil_my = -1
            # on peut inscrire le profil :
            if replaceOld:
                self.dbCommands.append('INSERT')
                self.dbInserts[commandLine_insert1].append((
                    id_profil_my, 
                    'DEFAULT', 
                    self.matiereName, 
                    'DEFAULT'))
                ordre = 0
                for profilData in self.profil:
                    id_bilan = profilData[0]
                    id_bilan_my = self.id2id['bilans'][id_bilan]
                    self.dbCommands.append('INSERT')
                    self.dbInserts[commandLine_insert2].append(
                        (id_profil_my, id_bilan_my, ordre))
                    ordre += 1

        # ***********************************************
        # vérification des modèles :
        # ***********************************************
        modifTemplateNameNoToAll = False
        commandLine_insert = utils_db.insertInto('templates')
        self.dbInserts[commandLine_insert] = []
        # on cherche le premier id_item disponible dans db_my :
        id_template_new = -1
        ordre = 0
        query_my = utils_db.queryExecute(
            utils_db.q_selectAllFromOrder.format(
                'templates', 'id_template'), 
            query=query_my)
        if query_my.first():
            id_template_new = int(query_my.value(0)) - 1
        for templateData in self.templates:
            id_template = templateData[0]
            templateName = templateData[1]
            templateLabel = templateData[2]
            etat = 'NEW'
            # est-il déjà là ?
            commandLine_test = utils_db.qs_prof_AllWhereNameLabel.format(
                'templates')
            query_my = utils_db.queryExecute(
                {commandLine_test: (templateName, templateLabel)}, 
                query=query_my)
            if query_my.first():
                etat = 'DEJA_LA'
                id_template_my = int(query_my.value(0))
                self.id2id['templates'][id_template] = id_template_my
            # le label est modifié ?
            if etat == 'NEW':
                commandLine_test = utils_db.qs_prof_AllWhereName.format(
                    'templates')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (templateName, )}, query=query_my)
                if query_my.first():
                    etat = 'LABEL_MODIF'
                    id_template_my = int(query_my.value(0))
                    templateLabelEX = query_my.value(3)
                    self.id2id['templates'][id_template] = id_template_my
            # le nom est modifié ?
            if etat == 'NEW':
                commandLine_test = utils_db.qs_prof_AllWhereLabel.format(
                    'templates')
                query_my = utils_db.queryExecute(
                    {commandLine_test: (templateLabel, )}, query=query_my)
                if query_my.first():
                    etat = 'NAME_MODIF'
                    id_template_my = int(query_my.value(0))
                    templateNameEX = query_my.value(1)
                    self.id2id['templates'][id_template] = id_template_my
            # on passe à l'action :
            if etat == 'NAME_MODIF':
                if not(modifTemplateNameNoToAll):
                    # on demande confirmation avant de modifier le modèle :
                    q1 = QtWidgets.QApplication.translate(
                        'main', 
                        'Modify this template Name ?')
                    question = utils_functions.u(qName).format(
                        q1, 
                        templateNameEX, 
                        templateLabel, 
                        q2, 
                        templateName, 
                        templateLabel)
                    reply = utils_functions.messageBox(
                        self.main, level='question', message=question,
                        buttons=['Yes', 'NoToAll', 'No', 'Cancel'])
                    if reply == QtWidgets.QMessageBox.NoToAll:
                        modifTemplateNameNoToAll = True
                        etat = 'NEW'
                    elif reply == QtWidgets.QMessageBox.Yes:
                        commandLine = utils_db.qdf_where.format(
                            'templates', 'id_template', id_template_my)
                        self.dbCommands.append(commandLine)
                        self.dbCommands.append('INSERT')
                        self.dbInserts[commandLine_insert].append((
                            id_template_my, 
                            templateName, 
                            ordre, 
                            templateLabel))
                        result['updatedTemplates'] += 1
                        ordre += 1
                    elif reply == QtWidgets.QMessageBox.No:
                        # on aura donc 2 modèles de même label :
                        etat = 'NEW'
                    else:
                        utils_functions.restoreCursor()
                        message = QtWidgets.QApplication.translate(
                            'main', 'Import Canceled')
                        utils_functions.messageBox(
                            self.main, level='warning', message=message)
                        return
                else:
                    etat = 'NEW'
            elif etat == 'LABEL_MODIF':
                # on demande confirmation avant de modifier le modèle :
                q1 = QtWidgets.QApplication.translate(
                    'main', 'Modify this template Label ?')
                question = utils_functions.u(qLabel).format(
                    q1, 
                    templateName, 
                    templateLabelEX, 
                    q2, 
                    templateName, 
                    templateLabel)
                reply = utils_functions.messageBox(
                    self.main, level='question', message=question,
                    buttons=['Yes', 'No', 'Cancel'])
                if reply == QtWidgets.QMessageBox.Yes:
                    commandLine = utils_db.qdf_where.format(
                        'templates', 'id_template', id_template_my)
                    self.dbCommands.append(commandLine)
                    self.dbCommands.append('INSERT')
                    self.dbInserts[commandLine_insert].append(
                        (id_template_my, templateName, ordre, templateLabel))
                    result['updatedTemplates'] += 1
                    ordre += 1
                elif reply == QtWidgets.QMessageBox.No:
                    # pas 2 modèles du même nom, donc on passe :
                    pass
                else:
                    utils_functions.restoreCursor()
                    message = QtWidgets.QApplication.translate(
                        'main', 'Import Canceled')
                    utils_functions.messageBox(
                        self.main, level='warning', message=message)
                    return
            if etat == 'NEW':
                # on crée le nouveau modèle :
                id_template_my = id_template_new
                id_template_new -= 1
                self.id2id['templates'][id_template] = id_template_my
                self.dbCommands.append('INSERT')
                self.dbInserts[commandLine_insert].append(
                    (id_template_my, templateName, ordre, templateLabel))
                result['newTemplates'] += 1
                ordre += 1

        # ***********************************************
        # vérification des comptages :
        # ***********************************************
        commandLine_insert = utils_db.insertInto('counts')
        self.dbInserts[commandLine_insert] = []
        commandLine_delete = (
            'DELETE FROM counts '
            'WHERE id_groupe=-1 AND Periode=-1 AND id_count=-1')
        # récupération depuis la table counts :
        oldCounts = {}
        lines = utils_db.table2List(
            'counts', query=query_my, order='ordre')
        for lineData in lines:
            if (lineData[0] < 0) and (lineData[1] < 0) and (lineData[2] < 0):
                oldCounts[lineData[3]] = [
                    lineData[4], 
                    lineData[5], 
                    lineData[6], 
                    lineData[7], 
                    lineData[8]]
        order = 0
        for countData in self.counts:
            countName = countData[0]
            sens = countData[1]
            calcul = countData[2]
            countLabel = countData[3]
            id_item_my = self.id2id['items'].get(countData[4], -1)
            # est-il déjà là ?
            if countName in oldCounts:
                if oldCounts[countName][0] != sens:
                    result['updatedCounts'] += 1
                elif oldCounts[countName][1] != calcul:
                    result['updatedCounts'] += 1
                elif oldCounts[countName][3] != countLabel:
                    result['updatedCounts'] += 1
                elif oldCounts[countName][4] != id_item_my:
                    result['updatedCounts'] += 1
                oldCounts[countName] = [
                    sens, calcul, order, countLabel, id_item_my]
            else:
                oldCounts[countName] = [
                    sens, calcul, order, countLabel, id_item_my]
                result['newCounts'] += 1
            order += 1
        # on vide et réécrit la table counts :
        self.dbCommands.append(commandLine_delete)
        for countName in oldCounts:
            self.dbCommands.append('INSERT')
            self.dbInserts[commandLine_insert].append((
                -1, 
                -1, 
                -1, 
                countName, 
                oldCounts[countName][0], 
                oldCounts[countName][1], 
                oldCounts[countName][2], 
                oldCounts[countName][3], 
                oldCounts[countName][4]))

        # ***********************************************
        # vérification des liens items-bilans :
        # ***********************************************
        commandLine_insert = utils_db.insertInto('item_bilan')
        self.dbInserts[commandLine_insert] = []
        dico_my = {}
        commandLine = utils_db.q_selectAllFrom.format('item_bilan')
        query_my = utils_db.queryExecute(commandLine, query=query_my)
        while query_my.next():
            id_item_my = int(query_my.value(0))
            id_bilan_my = int(query_my.value(1))
            coeff_my = int(query_my.value(2))
            dico_my[(id_item_my, id_bilan_my)] = coeff_my
        for (id_item, id_bilan) in self.item_bilan:
            coeff = self.item_bilan[(id_item, id_bilan)]
            id_item_my = self.id2id['items'][id_item]
            id_bilan_my = self.id2id['bilans'][id_bilan]
            if not((id_item_my, id_bilan_my) in dico_my):
                self.dbCommands.append('INSERT')
                self.dbInserts[commandLine_insert].append(
                    (id_item_my, id_bilan_my, coeff))
            elif dico_my[(id_item_my, id_bilan_my)] != coeff:
                commandLine = utils_db.qdf_prof_itemBilan3.format(
                    id_item_my, id_bilan_my)
                self.dbCommands.append(commandLine)
                self.dbCommands.append('INSERT')
                self.dbInserts[commandLine_insert].append(
                    (id_item_my, id_bilan_my, coeff))

        # ***********************************************
        # vérification des liens bilans-bilans :
        # ***********************************************
        commandLine_insert = utils_db.insertInto('bilan_bilan')
        self.dbInserts[commandLine_insert] = []
        bilan_bilan_my = []
        paths = prof_itemsbilans.createPathsList(self.main)[0]
        commandLine = utils_db.q_selectAllFrom.format('bilan_bilan')
        query_my = utils_db.queryExecute(commandLine, query=query_my)
        while query_my.next():
            id_bilan1_my = int(query_my.value(0))
            id_bilan2_my = int(query_my.value(1))
            bilan_bilan_my.append((id_bilan1_my, id_bilan2_my))
        for (id_bilan1, id_bilan2) in self.bilan_bilan:
            id_bilan1_my = self.id2id['bilans'][id_bilan1]
            id_bilan2_my = self.id2id['bilans'][id_bilan2]
            if (id_bilan1_my, id_bilan2_my) in bilan_bilan_my:
                continue
            elif (id_bilan2_my, id_bilan1_my) in bilan_bilan_my:
                continue
            else:
                # on vérifie qu'il n'y aura pas de liens circulaires.
                # liste des id_bilan2 à ne pas accepter :
                idToIgnore = [id_bilan1_my]
                for path in paths :
                    if id_bilan1_my in path:
                        for id_bilan in path:
                            if not(id_bilan in idToIgnore):
                                idToIgnore.append(id_bilan)
                # on crée le nouveau lien :
                if not(id_bilan2_my in idToIgnore):
                    self.dbCommands.append('INSERT')
                    self.dbInserts[commandLine_insert].append(
                        (id_bilan1_my, id_bilan2_my))

        # ***********************************************
        # vérification des conseils (table comments) :
        # ***********************************************
        commandLine_insert = utils_db.insertInto('comments')
        self.dbInserts[commandLine_insert] = []
        commandLine_delete = (
            'DELETE FROM comments '
            'WHERE isItem={0} AND id={1}')
        # d'abord on vide les anciens conseils :
        comments_my = []
        for (isItem, id_truc, comment) in self.comments:
            if isItem == 1:
                id_truc_my = self.id2id['items'][id_truc]
            else:
                id_truc_my = self.id2id['bilans'][id_truc]
            if not((isItem, id_truc_my) in comments_my):
                comments_my.append((isItem, id_truc_my))
                commandLine = utils_functions.u(
                    commandLine_delete).format(
                        isItem, id_truc_my)
                self.dbCommands.append(commandLine)
        # ensuite on inscrit les nouveaux conseils :
        for (isItem, id_truc, comment) in self.comments:
            if isItem == 1:
                id_truc_my = self.id2id['items'][id_truc]
            else:
                id_truc_my = self.id2id['bilans'][id_truc]
            self.dbCommands.append('INSERT')
            self.dbInserts[commandLine_insert].append(
                (isItem, id_truc_my, comment))

        # ***********************************************
        # inscription des liens modèle-items :
        # ***********************************************
        commandLine_insert = utils_db.insertInto('tableau_item')
        self.dbInserts[commandLine_insert] = []
        # d'abord on vide les anciens liens :
        tableau_item_my = []
        for (id_tableau, id_item, ordre) in self.tableau_item:
            id_tableau_my = self.id2id['templates'][id_tableau]
            if not(id_tableau_my in tableau_item_my):
                tableau_item_my.append(id_tableau_my)
                commandLine = utils_db.qdf_where.format(
                    'tableau_item', 'id_tableau', id_tableau_my)
                self.dbCommands.append(commandLine)
        # ensuite on inscrit les nouveaux liens :
        for (id_tableau, id_item, ordre) in self.tableau_item:
            id_tableau_my = self.id2id['templates'][id_tableau]
            id_item_my = self.id2id['items'][id_item]
            self.dbCommands.append('INSERT')
            self.dbInserts[commandLine_insert].append(
                (id_tableau_my, id_item_my, ordre))

        return result

    def reInit(self):
        """
        on réinitialise les variables du Wizard :
            fileName : nom du fichier
            matiereName, title, description : description de la structure
            items, bilans, profil, templates, counts : contenu des tables 
                indispensables pour afficher la structure
            item_bilan, bilan_bilan, comments, tableau_item : 
                autres tables utilisées pour récupérer
                la structure (donc utilisées à la fin)
            dbCommands : une liste des lignes à effectuer 
                dans la base du prof.
                Elle sera remplie à la dernière étape du wizard.
        """
        self.fileName = ''
        self.matiereName = self.main.me['Matiere']
        self.title = ''
        self.description = ''
        self.items = []
        self.bilans = []
        self.profil = []
        self.templates = []
        self.counts = []
        self.item_bilan = {}
        self.bilan_bilan = []
        self.comments = []
        self.tableau_item = []
        self.dbCommands = []
        self.dbInserts = {}

    def openStructureFile(self, fileName):
        """
        ouverture d'un fichier de structure.
        """
        import utils_filesdirs
        ok = False
        self.reInit()
        message = QtWidgets.QApplication.translate(
            'main', 
            'The structure of the file that you selected is not correct.')
        message = utils_functions.u(
            '<p align="center">{0}</p>'
            '<p align="center">{1}</p>').format(
                utils.SEPARATOR_LINE, message)
        utils_functions.doWaitCursor()
        dbFileTemp_structure = utils_functions.u(
            '{0}/structure_file.sqlite').format(self.main.tempPath)
        utils_filesdirs.removeAndCopy(fileName, dbFileTemp_structure)
        (db_structure, dbName) = utils_db.createConnection(
            self.main, dbFileTemp_structure)
        query_structure = utils_db.query(db_structure)
        try:
            # on commence par vérifier que le fichier 
            # est bien un fichier de structure :
            configDict = utils_db.configTable2Dict(db_structure)
            versionDB = configDict.get('versionDB', (-1, ''))
            if versionDB[1] != 'STRUCTURE':
                return
            if versionDB[0] < utils.VERSIONDB_STRUCTURE:
                # prévoir ici les modifs de version
                return
            # tables nécessaires à l'affichage des pages.
            # on récupère les autres données de config :
            self.matiereName = configDict.get('matiere', (-1, ''))[1]
            self.title = configDict.get('title', (-1, ''))[1]
            self.description = configDict.get('description', (-1, ''))[1]
            # récupération de la table items :
            self.items = utils_db.table2List(
                'items', query=query_structure)
            # récupération de la table bilans :
            self.bilans = utils_db.table2List(
                'bilans', query=query_structure)
            # récupération de la table profil_bilan_BLT :
            self.profil = []
            bilanDic = {}
            for bilanData in self.bilans:
                bilanDic[bilanData[0]] = bilanData
            lines = utils_db.table2List(
                'profil_bilan_BLT', query=query_structure, order='ordre')
            order = 0
            for lineData in lines:
                bilanData = bilanDic.get(lineData[1], None)
                if bilanData != None:
                    self.profil.append(tuple(bilanData))
            # récupération de la table templates :
            self.templates = []
            lines = utils_db.table2List(
                'templates', query=query_structure, order='ordre')
            for lineData in lines:
                self.templates.append((
                    lineData[0], 
                    lineData[1], 
                    lineData[3]))
            # récupération de la table counts :
            self.counts = []
            lines = utils_db.table2List(
                'counts', query=query_structure, order='ordre')
            for lineData in lines:
                try:
                    self.counts.append((
                        lineData[3], 
                        lineData[4], 
                        lineData[5], 
                        lineData[7], 
                        lineData[8]))
                except:
                    self.counts.append((
                        lineData[3], 
                        lineData[4], 
                        lineData[5], 
                        lineData[7], 
                        -1))
            # tables supplémentaires (utilisées à la dernière étape).
            # récupération de la table item_bilan :
            self.item_bilan = {}
            commandLine = utils_db.q_selectAllFrom.format('item_bilan')
            query_structure = utils_db.queryExecute(
                commandLine, query=query_structure)
            while query_structure.next():
                id_item = int(query_structure.value(0))
                id_bilan = int(query_structure.value(1))
                coeff = int(query_structure.value(2))
                self.item_bilan[(id_item, id_bilan)] = coeff
            # récupération de la table bilan_bilan :
            self.bilan_bilan = utils_db.table2List(
                'bilan_bilan', query=query_structure)
            # récupération de la table comments :
            self.comments = utils_db.table2List(
                'comments', query=query_structure)
            # récupération de la table tableau_item :
            self.tableau_item = utils_db.table2List(
                'tableau_item', query=query_structure, order='ordre')
            # on a terminé :
            self.fileName = fileName
            ok = True
        except:
            print('EXCEPT in openStructureFile')
        finally:
            # on ferme la base :
            if query_structure != None:
                query_structure.clear()
                del query_structure
            db_structure.close()
            del db_structure
            utils_db.sqlDatabase.removeDatabase(dbName)
            utils_functions.restoreCursor()
            if not(ok):
                utils_functions.messageBox(
                    self.main, level='warning', message=message)

    def importStructureFile(self):
        """
        après avoir validé le wizard d'importation,
        on écrit vraiment dans la base prof.
        """
        if len(self.dbCommands) > 0:
            utils_functions.doWaitCursor()
            dbCommands = []
            for dbCommand in self.dbCommands:
                if dbCommand != 'INSERT':
                    dbCommands.append(dbCommand)
            dbCommands.append(self.dbInserts)
            query_my, transactionOK = utils_db.queryTransactionDB(
                self.main.db_my)
            try:
                query_my = utils_db.queryExecute(
                    dbCommands, query=query_my)
            finally:
                utils_db.endTransaction(
                    query_my, self.main.db_my, transactionOK)
                utils_functions.restoreCursor()
                self.main.changeDBMyState()



