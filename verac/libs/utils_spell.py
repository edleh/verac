# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    pour gérer la correction orthographique
    d'après :
    http://john.nachtimwald.com/2009/08/22/qplaintextedit-with-in-line-spell-check/
"""

# importation des modules utiles :
from __future__ import division
import re

import utils, utils_functions
from spellchecker import SpellChecker

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



class SpellTextEdit(QtWidgets.QTextEdit):
    """
    A QTextEdit-based editor that supports syntax highlighting and
    spellchecking out of the box
    """
    def __init__(self, parent=None):
        super(SpellTextEdit, self).__init__(parent)
        self.initDict()
        self.popup_menu = QtWidgets.QMenu(self)

    def initDict(self):
        # on teste d'abord avec locale (par exemple fr_FR),
        # puis avec lang (par exemple fr) :
        language = 'en'
        path = 'libs/spellchecker/resources/{0}.json.gz'
        localeFileName = utils_functions.u(path).format(utils.LOCALE)
        if QtCore.QFileInfo(localeFileName).exists():
            language = utils.LOCALE
        else:
            lang = utils.LOCALE.split('_')[0]
            langFileName = utils_functions.u(path).format(lang)
            if QtCore.QFileInfo(langFileName).exists():
                language = lang

        self.spell = SpellChecker(language=language)
        self.highlighter = SpellHighlighter(self)
        if self.spell:
            self.highlighter.setDict(self.spell)
            self.highlighter.rehighlight()

    def killDict(self):
        #print('Disabling spellchecker')
        self.highlighter.setDocument(None)
        self.spell = None

    def mousePressEvent(self, event):
        if event.button() == QtCore.Qt.RightButton:
            # Rewrite the mouse event to a left button event so the cursor is
            # moved to the location of the pointer.
            event = QtGui.QMouseEvent(
                QtCore.QEvent.MouseButtonPress, 
                event.pos(), 
                QtCore.Qt.LeftButton, 
                QtCore.Qt.LeftButton, 
                QtCore.Qt.NoModifier)
        QtWidgets.QTextEdit.mousePressEvent(self, event)

    def contextMenuEvent(self, event):
        self.popup_menu.clear()
        if utils.OS_NAME[0] != 'win':
            self.popup_menu = self.createStandardContextMenu()
            first = self.popup_menu.actions()[0]

        # Select the word under the cursor.
        cursor = self.textCursor()
        cursor.select(QtGui.QTextCursor.WordUnderCursor)
        self.setTextCursor(cursor)

        # Check if the selected word is misspelled and offer spelling
        # suggestions if it is.
        if self.textCursor().hasSelection():
            text = utils_functions.u(self.textCursor().selectedText())
            istitle = text.istitle()
            if len(self.spell.known([text, ])) < 1:
                for word in self.spell.candidates(text):
                    if istitle:
                        newAct = QtWidgets.QAction(word.title(), self)
                    else:
                        newAct = QtWidgets.QAction(word, self)
                    newAct.triggered.connect(self.menuSelected)
                    if utils.OS_NAME[0] == 'win':
                        self.popup_menu.addAction(newAct)
                    else:
                        self.popup_menu.insertAction(first, newAct)
            if utils.OS_NAME[0] != 'win':
                self.popup_menu.insertSeparator(first)

        self.popup_menu.exec_(event.globalPos())

    def menuSelected(self):
        word = self.sender().text()
        self.correctWord(word)

    def correctWord(self, word):
        """
        Replaces the selected text with word.
        """
        cursor = self.textCursor()
        cursor.beginEditBlock()
        cursor.removeSelectedText()
        cursor.insertText(word)
        cursor.endEditBlock()



class SpellHighlighter(QtGui.QSyntaxHighlighter):

    def __init__(self, *args):
        super(SpellHighlighter, self).__init__(*args)
        self.spell = None
        self.regExp = re.compile('^[-+]?([0-9,.]*)$')

    def setDict(self, spell):
        self.spell = spell

    def highlightBlock(self, text):
        if not self.spell:
            return
        text = utils_functions.u(text)
        textFormat = QtGui.QTextCharFormat()
        textFormat.setUnderlineColor(QtCore.Qt.red)
        textFormat.setUnderlineStyle(QtGui.QTextCharFormat.WaveUnderline)
        for word_object in re.finditer(r'\w+', text, re.UNICODE):
            if self.regExp.match(word_object.group()) == None:
                if len(self.spell.known([word_object.group().lower(), ])) < 1:
                    self.setFormat(
                        word_object.start(), 
                        word_object.end() - word_object.start(), 
                        textFormat)

