# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Récupération et affichage de la base compteur.sqlite.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db
import admin, admin_ftp

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui





NB_WEEKS = 13





def downloadCompteurDB(main, msgFin=True):
    dirSite = admin.dirSitePrive + '/public'
    dirLocal = admin.dirLocalPrive + '/public'
    fileName = "compteur.sqlite"

    if admin_ftp.ftpTest(main):
        utils_functions.doWaitCursor()
        try:
            reponse = False
            # on télécharge le fichier compteur.sqlite 
            # dans admin.dirLocalPrive/public :
            reponse = admin_ftp.ftpGetFile(
                main, dirSite, dirLocal, fileName)
            if reponse and (admin.db_compteur != None):
                # on déconnecte :
                admin.db_compteur.close()
                del admin.db_compteur
                utils_db.sqlDatabase.removeDatabase('compteur')
                admin.db_compteur = None
        finally:
            utils_functions.restoreCursor()
            utils_functions.afficheStatusBar(main)
            if reponse:
                if msgFin:
                    utils_functions.afficheMsgFin(main, timer=5)
            else:
                utils_functions.afficheMsgPb(main)


def showCompteurAnalyze(main):
    dialog = ShowCompteurAnalyzeDlg(parent=main)
    lastState = main.disableInterface(dialog)
    dialog.exec_()
    main.enableInterface(dialog, lastState)


class ShowCompteurAnalyzeDlg(QtWidgets.QDialog):
    """
    affichage des données contenues dans la base compteur.sqlite
    """
    def __init__(self, parent=None):
        super(ShowCompteurAnalyzeDlg, self).__init__(parent)

        self.main = parent
        # le titre de la fenêtre :
        title = QtWidgets.QApplication.translate(
            'main', 'ShowCompteurAnalyze')
        self.setWindowTitle(title)

        self.helpContextPage = 'compteur'
        self.helpBaseUrl = 'admin'

        # le comboBox pour choisir l'affichage :
        self.whatComboBox = QtWidgets.QComboBox()
        self.whatComboBox.setMinimumWidth(200)
        self.whatComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Profs'))
        self.whatComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Students'))
        self.whatComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Classes'))
        self.whatComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'Errors'))
        self.whatComboBox.activated.connect(self.whatChanged)

        # le comboBox pour choisir la classe :
        self.classComboBox = QtWidgets.QComboBox()
        self.classComboBox.setMinimumWidth(200)
        # filtre "toutes les classes" :
        self.classComboBox.addItem(
            QtWidgets.QApplication.translate('main', 'All the classes'))
        # on récupère la liste des classes :
        query_commun = utils_db.query(self.main.db_commun)
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            classe = query_commun.value(1)
            self.classComboBox.addItem(classe)
        self.classComboBox.activated.connect(self.classChanged)

        # le bouton ODS :
        odsButton = QtWidgets.QPushButton(
            utils.doIcon('extension-ods'), 
            QtWidgets.QApplication.translate(
                'main', 'CompteurAnalyze2ods'))
        odsButton.clicked.connect(self.compteurAnalyze2ods)
        # on agence tout ça :
        toolsLayout = QtWidgets.QHBoxLayout()
        toolsLayout.addWidget(self.whatComboBox)
        toolsLayout.addWidget(self.classComboBox)
        toolsLayout.addStretch(1)
        toolsLayout.addWidget(odsButton)

        self.compteurView = QtWidgets.QTableView()
        self.model = CompteurTableModel(self.main)
        self.compteurView.setModel(self.model)
        self.whatChanged()

        dialogButtons = utils_functions.createDialogButtons(
            self, layout=True, buttons=('close', 'help'))
        buttonsLayout = dialogButtons[0]

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addLayout(toolsLayout)
        mainLayout.addWidget(self.compteurView)
        mainLayout.addLayout(buttonsLayout)
        self.setLayout(mainLayout)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp(
            self.helpContextPage, self.helpBaseUrl)

    def whatChanged(self):
        index = self.whatComboBox.currentIndex()
        utils_functions.doWaitCursor()
        try:
            if index == 1:
                if self.classComboBox.currentIndex() < 1:
                    self.model.setTable(
                        'eleves', 
                        theClass='ALL')
                else:
                    self.model.setTable(
                        'eleves', 
                        theClass=self.classComboBox.currentText())
            elif index == 2:
                self.model.setTable('classes')
            elif index == 3:
                self.model.setTable('errors')
            else:
                self.model.setTable('profs')
            self.classComboBox.setVisible(index == 1)
            self.compteurView.setColumnHidden(0, True)
            self.compteurView.horizontalHeader().setMinimumSectionSize(30)
            self.compteurView.resizeColumnsToContents()
            self.compteurView.resizeRowsToContents()
            self.compteurView.setAlternatingRowColors(True)
            if index == 3:
                for i in range(1, self.model.columnCount()):
                    self.compteurView.setColumnWidth(i, 200)
            else:
                for i in range(1, self.model.columnCount()):
                    self.compteurView.setColumnWidth(i, 50)
                self.compteurView.setColumnWidth(
                    self.model.columnCount() - 1, 100)
        except:
            pass
        finally:
            utils_functions.restoreCursor()

    def classChanged(self):
        index = self.classComboBox.currentIndex()
        utils_functions.doWaitCursor()
        try:
            if index < 1:
                self.model.setTable(
                    'eleves', 
                    theClass='ALL')
            else:
                self.model.setTable(
                    'eleves', 
                    theClass=self.classComboBox.currentText())
        except:
            pass
        finally:
            utils_functions.restoreCursor()

    def compteurAnalyze2ods(self):
        """
        """
        odsTitle = QtWidgets.QApplication.translate(
            'main', 'ODF Spreadsheet File')
        proposedName = utils_functions.u(
            '{0}/fichiers/compteur_web.ods').format(admin.adminDir)
        odsExt = QtWidgets.QApplication.translate(
            'main', 'ods files (*.ods)')
        fileName = QtWidgets.QFileDialog.getSaveFileName(
            self.main, odsTitle, proposedName, odsExt)
        fileName = utils_functions.verifyLibs_fileName(fileName)
        if fileName == '':
            return
        OK = False
        utils_functions.doWaitCursor()
        exportDatas = {'TABLES_LIST': [], 'TABLES_PARAMS': {}}
        traductions = {
            'profs': QtWidgets.QApplication.translate('main', 'Profs'), 
            'eleves': QtWidgets.QApplication.translate('main', 'Students'), 
            'classes': QtWidgets.QApplication.translate('main', 'Classes'), 
            'errors': QtWidgets.QApplication.translate('main', 'Errors')}
        try:
            import utils_export
            for what in ('profs', 'eleves', 'classes', 'errors'):
                theList = []
                if what == 'profs':
                    columns, rows = calcCompteurProfs(self.main)
                elif what == 'eleves':
                    columns, rows = calcCompteurEleves(self.main)
                elif what == 'classes':
                    columns, rows = calcCompteurClasses(self.main)
                elif what == 'errors':
                    columns, rows = calcCompteurErrors(self.main)
                line = []
                for title in columns:
                    lineDico = {'value': title['value']}
                    if what in ('profs', 'eleves', 'classes'):
                        if title == columns[0]:
                            lineDico['columnWidth'] = '5.0cm'
                        elif title == columns[-1]:
                            lineDico['columnWidth'] = '2.0cm'
                            if what == 'profs':
                                lineDico['alignHorizontal'] = 'center'
                            else:
                                lineDico['alignHorizontal'] = 'right'
                        else:
                            lineDico['columnWidth'] = '1.0cm'
                            if what == 'profs':
                                lineDico['alignHorizontal'] = 'center'
                            else:
                                lineDico['alignHorizontal'] = 'right'
                        line.append(lineDico)
                    elif what in ('errors', ):
                        if title != columns[0]:
                            lineDico['columnWidth'] = '5.0cm'
                            lineDico['alignHorizontal'] = 'center'
                            line.append(lineDico)
                theList.append(line)
                for aLine in rows:
                    line = []
                    if what in ('profs', 'eleves', 'classes'):
                        line.append({'value': aLine[0]['value']})
                    if what in ('eleves', 'classes'):
                        for cell in aLine[1:]:
                            if cell['value'] != '':
                                line.append({
                                    'value': cell['value'], 
                                    'type': utils.INTEGER})
                            else:
                                line.append({'value': ''})
                    else:
                        for cell in aLine[1:]:
                            line.append({
                                'value': cell['value'], 
                                'alignHorizontal': 'center'})
                    theList.append(line)
                exportDatas[what] = theList
                exportDatas['TABLES_LIST'].append(what)
                exportDatas['TABLES_PARAMS'][what] = {
                    'title': traductions[what]}
            # on exporte en ods :
            utils_functions.restoreCursor()
            utils_export.exportDic2ods(
                self.main, exportDatas, fileName, msgFin=False)
            utils_functions.afficheMsgFinOpen(self.main, fileName)
            utils_functions.doWaitCursor()
            OK = True
        except:
            utils_functions.afficheMsgPb(self.main)
        finally:
            utils_functions.restoreCursor()
            return OK













def getWeekData(weekNumber, lastlundi, i):
    decalage = 1 + i - NB_WEEKS
    numWeek = weekNumber + decalage
    if numWeek < 1:
        numWeek += 52
    lundi = lastlundi.addDays(7 * decalage)
    dimanche = lundi.addDays(6)
    toolTip = QtWidgets.QApplication.translate(
        'main', 
        'Week {0} :\n  from \t{1}\n  to \t{2}').format(
            numWeek, 
            lundi.toString('dd-MM-yyyy'), 
            dimanche.toString('dd-MM-yyyy'))
    toolTip2 = QtWidgets.QApplication.translate(
        'main', 
        'Week {0} (from {1} to {2})').format(
            numWeek, 
            lundi.toString('dd-MM-yyyy'), 
            dimanche.toString('dd-MM-yyyy'))
    return (numWeek, toolTip, toolTip2)






def calcCompteurProfs(main):
    utils_functions.doWaitCursor()
    columns = [{
        'value': QtWidgets.QApplication.translate('main', 'Prof')}]
    rows = []
    try:
        admin.openDB(main, 'compteur')
        query_compteur = utils_db.query(admin.db_compteur)

        aujourdhui = QtCore.QDate.currentDate()
        weekNumber = aujourdhui.weekNumber()[0]
        lastlundi = aujourdhui.addDays(1 - aujourdhui.dayOfWeek())
        weeks = {}
        for i in range(NB_WEEKS):
            (numWeek, toolTip, toolTip2) = getWeekData(
                weekNumber, lastlundi, i)
            weeks[numWeek] = toolTip2
            columns.append({
                'value': numWeek, 
                'toolTip': toolTip})
        columns.append({
            'value': QtWidgets.QApplication.translate('main', 'Total')})

        compteurProfs = {}
        for (id_prof, file_prof, nom, prenom) in admin.PROFS:
            compteurProfs[id_prof] = [(nom, prenom), {}, {}]

        query_compteur = utils_db.queryExecute(
            'SELECT * FROM users WHERE id_prof>-1', 
            query=query_compteur)
        while query_compteur.next():
            try:
                id_prof = int(query_compteur.value(0))
                week = int(query_compteur.value(2))
                month = int(query_compteur.value(3))
                year = int(query_compteur.value(4))
                total = int(query_compteur.value(5))
                if week in weeks:
                    if id_prof in compteurProfs:
                        if week in compteurProfs[id_prof][1]:
                            compteurProfs[id_prof][1][week] += total
                        else:
                            compteurProfs[id_prof][1][week] = total
                    else:
                        compteurProfs[id_prof] = [
                            ('AAA', 'bbb'), 
                            {week: total}, 
                            {}]
            except:
                pass

        query_compteur = utils_db.queryExecute(
            utils_db.q_selectAllFrom.format('upload'), 
            query=query_compteur)
        while query_compteur.next():
            try:
                id_prof = int(query_compteur.value(0))
                week = int(query_compteur.value(1))
                month = int(query_compteur.value(2))
                year = int(query_compteur.value(3))
                total = int(query_compteur.value(4))
                if week in weeks:
                    if id_prof in compteurProfs:
                        if week in compteurProfs[id_prof][2]:
                            compteurProfs[id_prof][2][week] += total
                        else:
                            compteurProfs[id_prof][2][week] = total
                    else:
                        compteurProfs[id_prof] = [
                            ('AAA', 'bbb'), 
                            {}, 
                            {week: total}]
            except:
                pass

        for (id_prof, file_prof, nom, prenom) in admin.PROFS:
            totalLine = [0, 0]
            visitName = QtWidgets.QApplication.translate('main', 'visit')
            visitsName = QtWidgets.QApplication.translate('main', 'visits')
            uploadName = QtWidgets.QApplication.translate('main', 'upload')
            uploadsName = QtWidgets.QApplication.translate('main', 'uploads')
            listArgs = [{
                'value': utils_functions.u('{0} {1}').format(prenom, nom)}]
            for i in range(NB_WEEKS):
                listArgs.append({'value': ''})
            for week in compteurProfs[id_prof][1]:
                total = compteurProfs[id_prof][1][week]
                totalLine[0] += total
                index = NB_WEEKS - weekNumber + week
                if index > NB_WEEKS:
                    index -= 52
                listArgs[index]['week'] = week
                listArgs[index]['value'] = '{0}|0'.format(total)
                if total == 1:
                    listArgs[index]['toolTip'] = utils_functions.u(
                        '{0} {1} \n').format(total, visitName)
                else:
                    listArgs[index]['toolTip'] = utils_functions.u(
                        '{0} {1}\n').format(total, visitsName)
            for week in compteurProfs[id_prof][2]:
                total = compteurProfs[id_prof][2][week]
                totalLine[1] += total
                index = NB_WEEKS - weekNumber + week
                if index > NB_WEEKS:
                    index -= 52
                if index < 1:
                    index = 1
                listArgs[index]['week'] = week
                if listArgs[index]['value'] == '':
                    listArgs[index]['value'] = '0|{0}'.format(total)
                    if total == 1:
                        listArgs[index]['toolTip'] = utils_functions.u(
                            '\n{0} {1}').format(total, uploadName)
                    else:
                        listArgs[index]['toolTip'] = utils_functions.u(
                            '\n{0} {1}').format(total, uploadsName)
                else:
                    listArgs[index]['value'] = '{0}|{1}'.format(
                        listArgs[index]['value'][:-2], total)
                    if total == 1:
                        listArgs[index]['toolTip'] = utils_functions.u(
                            '{0}\n{1} {2}').format(
                                listArgs[index]['toolTip'][:-1], 
                                total, 
                                uploadName)
                    else:
                        listArgs[index]['toolTip'] = utils_functions.u(
                            '{0}\n{1} {2}').format(
                                listArgs[index]['toolTip'][:-1], 
                                total, 
                                uploadsName)
            for i in range(NB_WEEKS):
                toolTip = listArgs[i + 1].get('toolTip', '')
                if len(toolTip) > 0:
                    weekTitle = weeks[listArgs[i + 1]['week']]
                    who = listArgs[0]['value']
                    listArgs[i + 1]['toolTip'] = utils_functions.u(
                        '{0}\n{1}\n{2}').format(who, weekTitle, toolTip)

            # mise en forme de la dernière colonne (total) :
            totalTip = ''
            if totalLine[0] == 1:
                totalTip = utils_functions.u(
                    '{0} {1}').format(totalLine[0], visitName)
            else:
                totalTip = utils_functions.u(
                    '{0} {1}').format(totalLine[0], visitsName)
            if totalLine[1] == 1:
                totalTip = utils_functions.u(
                    '{0}\n{1} {2}').format(
                        totalTip, totalLine[1], uploadName)
            else:
                totalTip = utils_functions.u(
                    '{0}\n{1} {2}').format(
                        totalTip, totalLine[1], uploadsName)
            listArgs.append({
                'value': '{0}|{1}'.format(totalLine[0], totalLine[1]), 
                'toolTip': totalTip})
            rows.append(listArgs)

    finally:
        utils_functions.restoreCursor()
        return columns, rows





def calcCompteurEleves(main, theClass='ALL'):
    utils_functions.doWaitCursor()
    columns = [{
        'value': QtWidgets.QApplication.translate('main', 'Student')}]
    rows = []
    try:
        admin.openDB(main, 'compteur')
        query_compteur = utils_db.query(admin.db_compteur)
        query_admin = utils_db.query(admin.db_admin)

        elevesList = []
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        queryList = utils_db.query2List(
            commandLine_admin, 
            order=['Classe', 'NOM', 'Prenom'], 
            query=query_admin)
        for record in queryList:
            id_eleve = int(record[0])
            nom = record[2]
            prenom = record[3]
            classe = record[4]
            eleveNom = utils_functions.u(
                '{0} {1} {2}').format(nom, prenom, classe)
            if (theClass == 'ALL') or (classe == theClass):
                elevesList.append((id_eleve, eleveNom))

        aujourdhui = QtCore.QDate.currentDate()
        weekNumber = aujourdhui.weekNumber()[0]
        lastlundi = aujourdhui.addDays(1 - aujourdhui.dayOfWeek())
        weeks = {}
        for i in range(NB_WEEKS):
            (numWeek, toolTip, toolTip2) = getWeekData(
                weekNumber, lastlundi, i)
            weeks[numWeek] = toolTip2
            columns.append({
                'value': numWeek, 
                'toolTip': toolTip})
        columns.append({
            'value': QtWidgets.QApplication.translate('main', 'Total')})

        compteurEleves = {}

        query_compteur = utils_db.queryExecute(
            'SELECT * FROM users WHERE id_eleve>-1', 
            query=query_compteur)
        while query_compteur.next():
            try:
                id_eleve = int(query_compteur.value(1))
                week = int(query_compteur.value(2))
                month = int(query_compteur.value(3))
                year = int(query_compteur.value(4))
                total = int(query_compteur.value(5))
                if week in weeks:
                    if id_eleve in compteurEleves:
                        if week in compteurEleves[id_eleve]:
                            compteurEleves[id_eleve][week] += total
                        else:
                            compteurEleves[id_eleve][week] = total
                    else:
                        compteurEleves[id_eleve] = {week: total}
            except:
                pass

        for (id_eleve, eleveNom) in elevesList:
            totalLine = 0
            listArgs = [{'value': eleveNom}]
            for i in range(NB_WEEKS):
                listArgs.append({'value': ''})
            try:
                for week in compteurEleves[id_eleve]:
                    total = compteurEleves[id_eleve][week]
                    totalLine += total
                    index = NB_WEEKS - weekNumber + week
                    if index > NB_WEEKS:
                        index -= 52
                    listArgs[index]['value'] = '{0}'.format(total)
                    listArgs[index]['week'] = week
            except:
                pass
            for i in range(NB_WEEKS):
                value = listArgs[i + 1].get('value', '')
                if len(value) > 0:
                    weekTitle = weeks[listArgs[i + 1]['week']]
                    who = listArgs[0]['value']
                    listArgs[i + 1]['toolTip'] = utils_functions.u(
                        '{0}\n{1}').format(who, weekTitle)
            # mise en forme de la dernière colonne (total) :
            listArgs.append({'value': '{0}'.format(totalLine)})
            rows.append(listArgs)

    finally:
        utils_functions.restoreCursor()
        return columns, rows





def calcCompteurClasses(main):
    utils_functions.doWaitCursor()
    columns = [{
        'value': QtWidgets.QApplication.translate('main', 'Class')}]
    rows = []
    try:
        admin.openDB(main, 'compteur')
        query_compteur = utils_db.query(admin.db_compteur)
        query_admin = utils_db.query(admin.db_admin)
        query_commun = utils_db.query(main.db_commun)

        classesList = []
        commandLine_commun = utils_db.qs_commun_classes
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            classe = query_commun.value(1)
            classesList.append(classe)

        eleveClasse = {}
        commandLine_admin = utils_db.q_selectAllFrom.format('eleves')
        query_admin = utils_db.queryExecute(
            commandLine_admin, query=query_admin)
        while query_admin.next():
            id_eleve = int(query_admin.value(0))
            classe = query_admin.value(4)
            eleveClasse[id_eleve] = classe

        aujourdhui = QtCore.QDate.currentDate()
        weekNumber = aujourdhui.weekNumber()[0]
        lastlundi = aujourdhui.addDays(1 - aujourdhui.dayOfWeek())
        weeks = {}
        for i in range(NB_WEEKS):
            (numWeek, toolTip, toolTip2) = getWeekData(
                weekNumber, lastlundi, i)
            weeks[numWeek] = toolTip2
            columns.append({
                'value': numWeek, 
                'toolTip': toolTip})
        columns.append({
            'value': QtWidgets.QApplication.translate('main', 'Total')})

        compteurClasses = {}

        query_compteur = utils_db.queryExecute(
            'SELECT * FROM users WHERE id_eleve>-1', 
            query=query_compteur)
        while query_compteur.next():
            try:
                id_eleve = int(query_compteur.value(1))
                week = int(query_compteur.value(2))
                total = int(query_compteur.value(5))
                classe = eleveClasse[id_eleve]
                if week in weeks:
                    if classe in compteurClasses:
                        if week in compteurClasses[classe]:
                            compteurClasses[classe][week] += total
                        else:
                            compteurClasses[classe][week] = total
                    else:
                        compteurClasses[classe] = {week : total}
            except:
                pass

        for classe in classesList:
            totalLine = 0
            listArgs = [{'value': classe}]
            for i in range(NB_WEEKS):
                listArgs.append({'value': ''})
            try:
                for week in compteurClasses[classe]:
                    total = compteurClasses[classe][week]
                    totalLine += total
                    index = NB_WEEKS - weekNumber + week
                    if index > NB_WEEKS:
                        index -= 52
                    listArgs[index]['value'] = '{0}'.format(total)
                    listArgs[index]['week'] = week
            except:
                pass
            for i in range(NB_WEEKS):
                value = listArgs[i + 1].get('value', '')
                if len(value) > 0:
                    weekTitle = weeks[listArgs[i + 1]['week']]
                    who = listArgs[0]['value']
                    listArgs[i + 1]['toolTip'] = utils_functions.u(
                        '{0}\n{1}').format(who, weekTitle)
            # mise en forme de la dernière colonne (total) :
            listArgs.append({'value': '{0}'.format(totalLine)})
            rows.append(listArgs)

    finally:
        utils_functions.restoreCursor()
        return columns, rows





def calcCompteurErrors(main):
    utils_functions.doWaitCursor()
    columns = [{
        'value': QtWidgets.QApplication.translate('main', 'Errors')}]
    rows = []
    try:
        admin.openDB(main, 'compteur')
        query_compteur = utils_db.query(admin.db_compteur)

        columns.append({
            'value': QtWidgets.QApplication.translate('main', 'IpAdress')})
        columns.append({
            'value': QtWidgets.QApplication.translate('main', 'Login')})
        columns.append({
            'value': QtWidgets.QApplication.translate('main', 'TimeStamp')})

        query_compteur = utils_db.queryExecute(
            utils_db.q_selectAllFrom.format('error'), 
            query=query_compteur)
        while query_compteur.next():
            ip = query_compteur.value(0)
            login = query_compteur.value(1)
            timestamp = int(query_compteur.value(2))
            timestamp = QtCore.QDateTime()
            timestamp.setTime_t(int(query_compteur.value(2)))
            timestamp = timestamp.toString('dd-MM-yyyy  hh:mm')
            rows.append([
                {'value': ''}, 
                {'value': ip}, 
                {'value': login}, 
                {'value': timestamp}])
    finally:
        utils_functions.restoreCursor()
        return columns, rows










class CompteurTableModel(QtCore.QAbstractTableModel):

    def __init__(self, main=None):
        super(CompteurTableModel, self).__init__(main)
        self.main = main
        self.rows = []
        self.columns = []
        self.boldFont = QtGui.QFont()
        self.boldFont.setBold(True)
        self.alignLeft = int(
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.alignCenter = int(
            QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        self.alignRight = int(
            QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        self.defaultFlag = QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable
        self.noChangeFlag = QtCore.Qt.ItemIsSelectable

    def setTable(self, what='profs', theClass='ALL'):
        self.beginResetModel()
        try:
            if what == 'profs':
                self.columns, self.rows = calcCompteurProfs(self.main)
                self.previousSection = -1
            elif what == 'eleves':
                self.columns, self.rows = calcCompteurEleves(
                    self.main, theClass=theClass)
                self.previousSection = -1
            elif what == 'classes':
                self.columns, self.rows = calcCompteurClasses(self.main)
                self.previousSection = -1
            elif what == 'errors':
                self.columns, self.rows = calcCompteurErrors(self.main)
                self.previousSection = -1
        finally:
            self.endResetModel()

    def rowCount(self, index=QtCore.QModelIndex()):
        return len(self.rows)

    def columnCount(self, index=QtCore.QModelIndex()):
        return len(self.columns)

    def data(self, index, role=QtCore.Qt.DisplayRole):
        column, row = index.column(), index.row()
        if role == QtCore.Qt.TextAlignmentRole:
            if column == 0:
                return self.alignLeft
            else:
                return self.alignCenter
        elif role == QtCore.Qt.DisplayRole:
            return self.rows[row][column]['value']
        elif role == QtCore.Qt.ToolTipRole:
            if 'toolTip' in self.rows[row][column]:
                return utils_functions.u(self.rows[row][column]['toolTip'])
        else:
            return

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.TextAlignmentRole:
            # alignement :
            if orientation == QtCore.Qt.Horizontal:
                return self.alignCenter
            return self.alignLeft
        elif role == QtCore.Qt.DisplayRole:
            # affichage :
            if orientation == QtCore.Qt.Horizontal:
                return self.columns[section]['value']
            else:
                titleRow = self.rows[section][0]['value']
                return titleRow
        elif role == QtCore.Qt.ToolTipRole:
            # affichage du toolTip seulement pour les titres de colonnes :
            if orientation == QtCore.Qt.Horizontal:
                if 'toolTip' in self.columns[section]:
                    return utils_functions.u(self.columns[section]['toolTip'])
                else:
                    return ''
            else:
                return  ''
        else:
            return

    def flags(self, index):
        return self.defaultFlag



def cleanCompteurSiteEtab(main, nb_semaines=13):
    """
    Demande de nettoyage de la base compteur.
    On vide tout ce qui est antérieur à 13 semaines.
    """
    import utils_web
    result = False
    theUrl = main.siteUrlPublic + "/pages/verac_cleanCompteur.php"
    postData = utils_functions.u('nb_semaines={0}').format(nb_semaines)
    httpClient = utils_web.HttpClient(main)
    httpClient.launch(theUrl, postData)
    while httpClient.state == utils_web.STATE_LAUNCHED:
        QtWidgets.QApplication.processEvents()
    if httpClient.state == utils_web.STATE_OK:
        try:
            result = True
        except:
            pass
    return result


