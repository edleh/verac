# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    gestion de l'aide contextuelle affichée dans l'interface admin.
    Il y a :
        * un menu (placé à gauche) sous forme de treeview
        * une zone d'affichage des pages d'aide (webview)
        * un bouton pour lancer des actions
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_webengine, utils_web

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



"""
Types d'actions associées aux items.
On peut les combiner.
"""
ACTION_NO = 1
ACTION_URL = 2
ACTION_DO = 4





##################################################################"
#
#       LES ITEMS DU MENU SONT DES QTreeWidgetItem
#
##################################################################"

class HelpMenuTreeItem(QtWidgets.QTreeWidgetItem):
    """
    Les items du HelpMenuTreeWidget.
    Ils contiennent un titre, une icône, un tooltip, et une liste data.
    Structure de data :
        [ACTION, paramètres pour l'action]
        exemples :
        [ACTION_URL, 'scheduler', 'admin'] ouvre la page scheduler de l'aide admin
        [ACTION_DO, action] lance une QAction
        [ACTION_DO + ACTION_URL, action, 'scheduler', 'admin'] fait les 2
        [ACTION_NO] pour ne rien faire
    """
    def __init__(self, title='', parent=None, 
                icon=None, toolTip='', data=[ACTION_NO]):
        """
        initialisation
        """
        super(HelpMenuTreeItem, self).__init__(parent)
        self.setText(0, title)
        self.parent = parent
        if icon != None:
            self.setIcon(0, icon)
        if toolTip != '':
            self.setToolTip(0, toolTip)
        self.setTextAlignment(0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        self.setData(0, QtCore.Qt.UserRole, data)





##################################################################"
#
#       LE MENU EST UN QTreeWidget
#
##################################################################"

class HelpMenuTreeWidget(QtWidgets.QTreeWidget):
    """
    Un TreeWidget pour afficher le menu.
    """
    def __init__(self, parent=None, helpWebView=None):
        """
        initialisation
        """
        super(HelpMenuTreeWidget, self).__init__(parent)
        self.main = parent
        self.helpWebView = helpWebView
        # quelques réglages :
        self.setColumnCount(1)
        self.setHeaderLabel('')
        self.setIconSize(
            QtCore.QSize(
                utils.STYLE['PM_ToolBarIconSize'], 
                utils.STYLE['PM_ToolBarIconSize']))
        self.currentItemChanged.connect(self.selectItem)
        # enfin on crée les items (procédure séparée ; en fin du module) :
        createMenu(self)

    def selectItem(self, current, previous):
        """
        Un item est sélectionné.
        On effectue les actions qui lui sont liées (via son data).
        On vide la liste data au fur et à mesure ; cela permet de gérer plusieurs actions.
        """
        if not current:
            current = previous
        data = current.data(0, QtCore.Qt.UserRole)
        actionType = data.pop(0)
        action = None
        if actionType >= ACTION_DO:
            actionType = actionType - ACTION_DO
            action = data.pop(0)
            #action.activate(QtWidgets.QAction.Trigger)
        if actionType >= ACTION_URL:
            actionType = actionType - ACTION_URL
            contextPage = data.pop(0)
            baseUrl = data.pop(0)
            if baseUrl == 'prof':
                contextUrl = '{0}help-prof-{1}.html'.format(utils.HELPPAGE_BEGIN, contextPage)
            elif baseUrl == 'admin':
                contextUrl = '{0}help-admin-{1}.html'.format(utils.HELPPAGE_BEGIN, contextPage)
            else:
                contextUrl = '{0}{1}-{2}.html'.format(utils.HELPPAGE_BEGIN, baseUrl, contextPage)
            if self.helpWebView != None:
                url = utils_web.doUrl(contextUrl)
                self.helpWebView.load(url)

        self.helpWebView.updateButton(action)

    def doSelectItem(self, itemPath=()):
        """
        pour sélectionner automatiquement un item d'après le chemin qui y mène.
        Par exemple, pour sélectionner "Basculer vers la période suivante" :
            main.menuLeft.doSelectItem(('Periods', 'SwitchToNextPeriod'))
        """
        ok = True
        for itemText in itemPath:
            if not(ok):
                break
            title = QtWidgets.QApplication.translate('main', itemText)
            try:
                for i in range(currentItem.childCount()):
                    aChild = currentItem.child(i)
                    if aChild.text(0) == title:
                        new = aChild
                        break
                try:
                    currentItem = new
                    self.expandItem(currentItem)
                    self.setCurrentItem(currentItem, 0)
                except:
                    ok = False
            except:
                currentItem = self.findItems(title, QtCore.Qt.MatchExactly, 0)[0]
                self.expandItem(currentItem)
                self.setCurrentItem(currentItem, 0)



##################################################################"
#
#       UN OBJET DÉDIÉ À L'AFFICHAGE DE L'AIDE
#
##################################################################"

class HelpViewer(QtWidgets.QFrame):
    """
    Il contient :
        un WebView dont les liens sont désactivés
        un pushButton pour lancer les actions
    """
    def __init__(self, parent=None, frameStyle=None):
        """
        initialisation
        """
        super(HelpViewer, self).__init__(parent)
        self.parent = parent

        # le webview :
        self.webView = utils_webengine.MyWebEngineView(self.parent)
        self.url = QtCore.QUrl()

        # le bouton pour lancer les actions :
        self.pushButton = QtWidgets.QPushButton()
        self.pushButton.clicked.connect(self.doAction)
        # self.action pour savoir quelle action est liée au bouton :
        self.action = None

        # on agence tout ça :
        vBoxLayout = QtWidgets.QVBoxLayout()
        vBoxLayout.addWidget(self.webView)
        vBoxLayout.addWidget(self.pushButton)
        self.setLayout(vBoxLayout)
        if frameStyle == None:
            frameStyle = QtWidgets.QFrame.StyledPanel | QtWidgets.QFrame.Raised
        self.setFrameStyle(frameStyle)

    def load(self, url):
        # affichage d'une page Web :
        self.url = url
        self.webView.load(url)

    def updateButton(self, action=None):
        """
        Mise à jour du bouton lors de la sélection
        d'un item du menu.
        """
        self.action = action
        if action == None:
            self.pushButton.setText(QtWidgets.QApplication.translate('main', 'No Action'))
            self.pushButton.setToolTip('')
            self.pushButton.setIcon(utils.doIcon('dialog-cancel'))
            self.pushButton.setEnabled(False)
        else:
            self.pushButton.setText(action.text())
            self.pushButton.setToolTip(action.statusTip())
            self.pushButton.setIcon(action.icon())
            self.pushButton.setEnabled(True)

    def doAction(self):
        # on lance l'action liée à l'item sélectionné :
        self.action.activate(QtWidgets.QAction.Trigger)




##################################################################"
#
#       LA PROCÉDURE DE CRÉATION DES ITEMS DU MENU :
#
##################################################################"

def createMenu(parent):

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # item de départ
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    title = QtWidgets.QApplication.translate('main', 'Index')
    icon = utils.doIcon('help-about')
    toolTip = QtWidgets.QApplication.translate('main', 'Index and news')
    listWidgetItem0 = HelpMenuTreeItem(
        title, parent, icon, toolTip,
        [ACTION_URL, 'todo', 'main'])

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # Interface
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    title = QtWidgets.QApplication.translate('main', 'Interface')
    icon = utils.doIcon('help')
    toolTip = QtWidgets.QApplication.translate('main', 'Presentation of the administrator interface')
    listWidgetItem0 = HelpMenuTreeItem(
        title, parent, icon, toolTip,
        [ACTION_URL, 'admin-interface', 'admin'])

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # Scheduler
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    action = parent.main.actionLaunchScheduler_admin
    title = QtWidgets.QApplication.translate('main', 'Scheduler')
    listWidgetItem0 = HelpMenuTreeItem(
        title, parent, action.icon(), action.statusTip(),
        [ACTION_DO + ACTION_URL, action, 'scheduler', 'admin'])

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # Résultats
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    title = QtWidgets.QApplication.translate('main', 'Results')
    icon = utils.doIcon('update-results')
    toolTip = QtWidgets.QApplication.translate(
        'main', 'Calculate student results, create bulletins, ...')
    listWidgetItem0 = HelpMenuTreeItem(
        title, parent, icon, toolTip,
        [ACTION_URL, 'resultats-documents', 'admin'])

    # sous-item UpdateBilans :
    action = parent.main.actionUpdateBilans_admin
    title = QtWidgets.QApplication.translate('main', 'Update')
    icon = action.icon()
    toolTip = QtWidgets.QApplication.translate('main', 'Update results')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_DO + ACTION_URL, action, 'update-bilans', 'admin'])

    # sous-item CreateBilans :
    action = parent.main.actionCreateReports_admin
    title = QtWidgets.QApplication.translate('main', 'CreateBilansBLT')
    icon = utils.doIcon('validations')
    toolTip = QtWidgets.QApplication.translate('main', 'Create bulletins or other reports')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_DO + ACTION_URL, action, 'create-bilans-blt', 'admin'])

    # sous-item Export2LSU :
    action = parent.main.actionExport2LSU_admin
    title = QtWidgets.QApplication.translate('main', 'Export LSU')
    icon = utils.doIcon('en')
    toolTip = QtWidgets.QApplication.translate('main', 'To export results to LSU (Livret Scolaire Unique)')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_DO + ACTION_URL, action, 'export-lsu', 'admin'])

    # sous-item EditModeles :
    title = QtWidgets.QApplication.translate('main', 'EditModeles')
    icon = utils.doIcon('certificate-edit')
    toolTip = QtWidgets.QApplication.translate(
        'main', 'Edit bulletins models, make new ...')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_URL, 'edit-modeles', 'admin'])

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # Documents
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    title = QtWidgets.QApplication.translate('main', 'Documents')
    icon = utils.doIcon('documents')
    toolTip = QtWidgets.QApplication.translate('main', 'DocumentsToolTip')
    listWidgetItem0 = HelpMenuTreeItem(
        title, parent, icon, toolTip,
        [ACTION_URL, 'manage-documents', 'admin'])

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # Bases
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    title = QtWidgets.QApplication.translate('main', 'Administration')
    icon = utils.doIcon('server-database')
    toolTip = QtWidgets.QApplication.translate('main', 'DBToolTip')
    listWidgetItem0 = HelpMenuTreeItem(
        title, parent, icon, toolTip,
        [ACTION_URL, 'config-etab', 'admin'])

    # sous-item actionManageDB :
    #action = parent.main.actionManageDB
    title = QtWidgets.QApplication.translate('main', 'GestDirect')
    icon = action.icon()
    toolTip = QtWidgets.QApplication.translate('main', 'DBDirectToolTip')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_URL, 'gest-direct-users', 'admin'])

    # sous-item actionEditCsvFile_admin :
    action = parent.main.actionEditCsvFile_admin
    title = QtWidgets.QApplication.translate('main', 'CsvFiles')
    icon = action.icon()
    toolTip = action.statusTip()
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_DO + ACTION_URL, action, 'csv-editor', 'admin'])

    # sous-item SIECLE :
    """
    action = parent.main.actionUpdateElevesFromSIECLE
    title = QtWidgets.QApplication.translate('main', 'SIECLE')
    icon = action.icon()
    toolTip = QtWidgets.QApplication.translate('main', 'SIECLEToolTip')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_DO + ACTION_URL, action, 'eleves-siecle', 'admin'])
    """

    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # Périodes
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    title = QtWidgets.QApplication.translate('main', 'Periods')
    icon = utils.doIcon('year-new')
    toolTip = QtWidgets.QApplication.translate('main', 'PeriodsToolTip')
    listWidgetItem0 = HelpMenuTreeItem(
        title, parent, icon, toolTip,
        [ACTION_URL, 'periods', 'admin'])

    # sous-item Bascule Période :
    action = parent.main.actionSwitchToNextPeriod_admin
    title = QtWidgets.QApplication.translate('main', 'SwitchToNextPeriod')
    icon = action.icon()
    toolTip = QtWidgets.QApplication.translate('main', 'SwitchToNextPeriodToolTip')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_DO + ACTION_URL, action, 'switch-to-next-period', 'admin'])

    # sous-item Changement d'année scolaire :
    action = parent.main.actionCreateYearArchiveAndClear_admin
    title = QtWidgets.QApplication.translate('main', 'New year')
    icon = utils.doIcon('year-new')
    toolTip = QtWidgets.QApplication.translate('main', 'Change of school year')
    listWidgetItem = HelpMenuTreeItem(
        title, listWidgetItem0, icon, toolTip,
        [ACTION_DO + ACTION_URL, action, 'new-year', 'admin'])










    """
    # sous-item aaa :
    action = parent.main.aaa
    title = action.text()#QtWidgets.QApplication.translate('main', '')
    listWidgetItem = HelpMenuTreeItem(title, listWidgetItem0,
                        action.icon(), action.statusTip(),
        [ACTION_DO + ACTION_URL, action, '', 'admin'])
    """


    """
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    # Tests
    # @@@@@@@@@@@@@@@@@@@@@@@@@@@
    title = 'Tests'
    icon = utils.doIcon('tools-report-bug')
    toolTip = 'Juste pour faire des essais'
    listWidgetItem0 = HelpMenuTreeItem(title, parent, icon, toolTip,
        [ACTION_URL, 'modif-user', 'admin'])
    """

















    # on sélectionne le premier item :
    parent.setCurrentItem(parent.itemAt(0, 0))
    parent.expandItem(parent.currentItem())



