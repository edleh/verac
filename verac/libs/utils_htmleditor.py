# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Un éditeur html (utilisé pour écrire les commentaires).

    d'après :
        http://labs.trolltech.com/blogs/2009/03/12/wysiwyg-html-editor
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_webengine, utils_web

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
    try:
        from PyQt5.QtWebEngineWidgets import QWebEnginePage
    except ImportError:
        from PyQt5.QtWebKitWidgets import QWebPage as QWebEnginePage
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui
    from PyQt4.QtWebKit import QWebPage as QWebEnginePage



"""
###########################################################
###########################################################

                HtmlEditor

###########################################################
###########################################################
"""

class WebViewWidget(QtWidgets.QWidget):

    def __init__(
            self, parent=None, content='', 
            askOnLink=True, allHeaders=False):
        super(WebViewWidget, self).__init__(parent)
        self.main = parent

        self.waiting = True
        self.allHeaders = allHeaders
        # le WebView pour afficher:
        self.webView = utils_webengine.MyWebEngineView(
            self, linksInBrowser=True, askOnLink=askOnLink)
        if utils_webengine.WEB_ENGINE == 'WEBENGINE':
            self.webView.loadFinished.connect(self.loadFinished)
        else:
            self.webView.page().setContentEditable(True)
        self.webView.setHtml(content)
        self.webView.page().selectionChanged.connect(self.adjustActions)

        self.menuBar = QtWidgets.QMenuBar(self)
        self.toolBar = QtWidgets.QToolBar(
            QtWidgets.QApplication.translate('main', 'Editor Bar'), 
            self)
        ICON_SIZE = utils.STYLE['PM_SmallIconSize']
        self.toolBar.setIconSize(
            QtCore.QSize(ICON_SIZE, ICON_SIZE))
        self.createActionsMenusButtons()

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.menuBar, 0, 1)
        grid.addWidget(self.toolBar, 1, 1)
        grid.addWidget(self.webView, 5, 1)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.waiting = False

    def loadFinished(self):
        js = "document.documentElement.contentEditable = true;"
        self.webView.page().runJavaScript(js)

    def forwardAction(self, action1, action2):
        action1.triggered.connect(self.webView.pageAction(action2).trigger)
        self.webView.pageAction(action2).changed.connect(self.adjustActions)

    def followEnable(self, a1, a2):
        a1.setEnabled(self.webView.pageAction(a2).isEnabled())

    def followCheck(self, a1, a2):
        a1.setChecked(self.webView.pageAction(a2).isChecked())

    def adjustActions(self):
        self.followEnable(self.actionEditCut, QWebEnginePage.Cut)
        self.followEnable(self.actionEditCopy, QWebEnginePage.Copy)
        self.followEnable(self.actionEditPaste, QWebEnginePage.Paste)
        try:
            self.followCheck(
                self.actionFormatBold, 
                QWebEnginePage.ToggleBold)
            self.followCheck(
                self.actionFormatItalic, 
                QWebEnginePage.ToggleItalic)
            self.followCheck(
                self.actionFormatUnderline, 
                QWebEnginePage.ToggleUnderline)
        except:
            pass

    def createActionsMenusButtons(self):

        self.actionEditCut = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'EditCut'), 
            self)
        self.actionEditCut.setIcon(utils.doIcon('edit-cut'))

        self.actionEditCopy = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'EditCopy'), 
            self)
        self.actionEditCopy.setIcon(utils.doIcon('edit-copy'))

        self.actionEditPaste = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'EditPaste'), 
            self)
        self.actionEditPaste.setIcon(utils.doIcon('edit-paste'))


        self.actionFormatBold = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatBold'), 
            self)
        self.actionFormatBold.setIcon(utils.doIcon('format-text-bold'))

        self.actionFormatItalic = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatItalic'), 
            self)
        self.actionFormatItalic.setIcon(utils.doIcon('format-text-italic'))

        self.actionFormatUnderline = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatUnderline'), 
            self)
        self.actionFormatUnderline.setIcon(
            utils.doIcon('format-text-underline'))

        # these are forward to internal QWebView
        self.forwardAction(self.actionEditCut, QWebEnginePage.Cut)
        self.forwardAction(self.actionEditCopy, QWebEnginePage.Copy)
        self.forwardAction(self.actionEditPaste, QWebEnginePage.Paste)
        try:
            self.forwardAction(
                self.actionFormatBold, 
                QWebEnginePage.ToggleBold)
        except:
            self.actionFormatBold.triggered.connect(self.formatBold)
        try:
            self.forwardAction(
                self.actionFormatItalic, 
                QWebEnginePage.ToggleItalic)
        except:
            self.actionFormatItalic.triggered.connect(self.formatItalic)
        try:
            self.forwardAction(
                self.actionFormatUnderline, 
                QWebEnginePage.ToggleUnderline)
        except:
            self.actionFormatUnderline.triggered.connect(self.formatUnderline)

        self.actionFormatStrikethrough = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatStrikethrough'), 
            self)
        self.actionFormatStrikethrough.setIcon(
            utils.doIcon('format-text-strikethrough'))
        self.actionFormatStrikethrough.triggered.connect(
            self.formatStrikeThrough)

        self.actionFormatAlignLeft = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatAlignLeft'), 
            self)
        self.actionFormatAlignLeft.setIcon(
            utils.doIcon('format-justify-left'))
        self.actionFormatAlignLeft.triggered.connect(self.formatAlignLeft)

        self.actionFormatAlignCenter = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatAlignCenter'), 
            self)
        self.actionFormatAlignCenter.setIcon(
            utils.doIcon('format-justify-center'))
        self.actionFormatAlignCenter.triggered.connect(self.formatAlignCenter)

        self.actionFormatAlignRight = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatAlignRight'), 
            self)
        self.actionFormatAlignRight.setIcon(
            utils.doIcon('format-justify-right'))
        self.actionFormatAlignRight.triggered.connect(self.formatAlignRight)

        self.actionFormatAlignJustify = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatAlignJustify'), 
            self)
        self.actionFormatAlignJustify.setIcon(
            utils.doIcon('format-justify-fill'))
        self.actionFormatAlignJustify.triggered.connect(
            self.formatAlignJustify)


        self.actionFormatIncreaseIndent = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatIncreaseIndent'), 
            self)
        self.actionFormatIncreaseIndent.setIcon(
            utils.doIcon('format-indent-more'))
        self.actionFormatIncreaseIndent.triggered.connect(
            self.formatIncreaseIndent)

        self.actionFormatDecreaseIndent = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatDecreaseIndent'), 
            self)
        self.actionFormatDecreaseIndent.setIcon(
            utils.doIcon('format-indent-less'))
        self.actionFormatDecreaseIndent.triggered.connect(
            self.formatDecreaseIndent)

        self.actionFormatNumberedList = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatNumberedList'), 
            self)
        self.actionFormatNumberedList.setIcon(
            utils.doIcon('format-list-ordered'))
        self.actionFormatNumberedList.triggered.connect(
            self.formatNumberedList)

        self.actionFormatBulletedList = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'FormatBulletedList'), 
            self)
        self.actionFormatBulletedList.setIcon(
            utils.doIcon('format-list-unordered'))
        self.actionFormatBulletedList.triggered.connect(
            self.formatBulletedList)



        self.actionInsertImage = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'InsertImage'), 
            self)
        self.actionInsertImage.setIcon(utils.doIcon('insert-image'))
        self.actionInsertImage.triggered.connect(self.insertImage)

        self.actionCreateLink = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'CreateLink'), 
            self)
        self.actionCreateLink.setIcon(utils.doIcon('link-insert'))
        self.actionCreateLink.triggered.connect(self.createLink)


        self.actionStyleParagraph = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'StyleParagraph'), 
            self)
        #self.actionStyleParagraph.setIcon(utils.doIcon(''))
        self.actionStyleParagraph.triggered.connect(self.styleParagraph)

        self.actionStyleHeading1 = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'StyleHeading1'), 
            self)
        #self.actionStyleHeading1.setIcon(utils.doIcon(''))
        self.actionStyleHeading1.triggered.connect(self.styleHeading1)

        self.actionStyleHeading2 = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'StyleHeading2'), 
            self)
        #self.actionStyleHeading2.setIcon(utils.doIcon(''))
        self.actionStyleHeading2.triggered.connect(self.styleHeading2)

        self.actionStyleHeading3 = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'StyleHeading3'), 
            self)
        #self.actionStyleHeading3.setIcon(utils.doIcon(''))
        self.actionStyleHeading3.triggered.connect(self.styleHeading3)

        self.actionStyleHeading4 = QtWidgets.QAction(
            QtWidgets.QApplication.translate('main', 'StyleHeading4'), 
            self)
        #self.actionStyleHeading4.setIcon(utils.doIcon(''))
        self.actionStyleHeading4.triggered.connect(self.styleHeading4)


        """
        format-font-size-less format-font-size-more format-text-color
        format-text-subscript format-text-superscript
        insert-horizontal-rule   insert-table

        #formatFontName formatFontSize formatTextColor formatBackgroundColor

        self.actionStylePreformatted.setText(QtWidgets.QApplication.translate('main', 'Pre&formatted', None, QtWidgets.QApplication.UnicodeUTF8))
        self.actionStyleAddress.setText(QtWidgets.QApplication.translate('main', '&Address', None, QtWidgets.QApplication.UnicodeUTF8))
        self.actionFormatFontName.setText(QtWidgets.QApplication.translate('main', '&Font Name...', None, QtWidgets.QApplication.UnicodeUTF8))
        self.actionFormatTextColor.setText(QtWidgets.QApplication.translate('main', 'Text &Color...', None, QtWidgets.QApplication.UnicodeUTF8))
        self.actionFormatBackgroundColor.setText(QtWidgets.QApplication.translate('main', 'Bac&kground Color...', None, QtWidgets.QApplication.UnicodeUTF8))
        self.actionFormatFontSize.setText(QtWidgets.QApplication.translate('main', 'Font Si&ze...', None, QtWidgets.QApplication.UnicodeUTF8))
        self.actionInsertHtml.setText(QtWidgets.QApplication.translate('main', 'Insert HTML...', None, QtWidgets.QApplication.UnicodeUTF8))
        self.actionInsertHtml.setToolTip(QtWidgets.QApplication.translate('main', 'Insert HTML', None, QtWidgets.QApplication.UnicodeUTF8))
        """



        self.editMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', '&Edit'), 
            self)
        self.menuBar.addMenu(self.editMenu)
        self.formatMenu = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', '&Format'), 
            self)
        self.menuBar.addMenu(self.formatMenu)

        self.editMenu.addAction(self.actionEditCut)
        self.toolBar.addAction(self.actionEditCut)
        self.editMenu.addAction(self.actionEditCopy)
        self.toolBar.addAction(self.actionEditCopy)
        self.editMenu.addAction(self.actionEditPaste)
        self.toolBar.addAction(self.actionEditPaste)
        self.editMenu.addSeparator()
        self.toolBar.addSeparator()
        #self.editMenu.addAction(self.actionInsertImage)
        #self.toolBar.addAction(self.actionInsertImage)
        self.editMenu.addAction(self.actionCreateLink)
        self.toolBar.addAction(self.actionCreateLink)


        self.formatMenu.addAction(self.actionFormatBold)
        self.toolBar.addAction(self.actionFormatBold)
        self.formatMenu.addAction(self.actionFormatItalic)
        self.toolBar.addAction(self.actionFormatItalic)
        self.formatMenu.addAction(self.actionFormatUnderline)
        self.toolBar.addAction(self.actionFormatUnderline)
        self.formatMenu.addAction(self.actionFormatStrikethrough)
        self.toolBar.addAction(self.actionFormatStrikethrough)
        self.formatMenu.addSeparator()
        self.toolBar.addSeparator()
        self.formatMenu.addAction(self.actionFormatAlignLeft)
        self.toolBar.addAction(self.actionFormatAlignLeft)
        self.formatMenu.addAction(self.actionFormatAlignCenter)
        self.toolBar.addAction(self.actionFormatAlignCenter)
        self.formatMenu.addAction(self.actionFormatAlignRight)
        self.toolBar.addAction(self.actionFormatAlignRight)
        self.formatMenu.addAction(self.actionFormatAlignJustify)
        self.toolBar.addAction(self.actionFormatAlignJustify)
        self.formatMenu.addSeparator()
        self.toolBar.addSeparator()
        self.formatMenu.addAction(self.actionFormatIncreaseIndent)
        self.toolBar.addAction(self.actionFormatIncreaseIndent)
        self.formatMenu.addAction(self.actionFormatDecreaseIndent)
        self.toolBar.addAction(self.actionFormatDecreaseIndent)
        self.formatMenu.addAction(self.actionFormatNumberedList)
        self.toolBar.addAction(self.actionFormatNumberedList)
        self.formatMenu.addAction(self.actionFormatBulletedList)
        self.toolBar.addAction(self.actionFormatBulletedList)
        self.formatMenu.addSeparator()

        self.menuStyle = QtWidgets.QMenu(
            QtWidgets.QApplication.translate('main', '&Style'), 
            self)
        self.menuBar.addMenu(self.menuStyle)
        self.menuStyle.addAction(self.actionStyleParagraph)
        if self.allHeaders:
            self.menuStyle.addAction(self.actionStyleHeading1)
            self.menuStyle.addAction(self.actionStyleHeading2)
        self.menuStyle.addAction(self.actionStyleHeading3)
        self.menuStyle.addAction(self.actionStyleHeading4)




    def execCommand(self, cmd, arg=None):
        if arg == None:
            js = utils_functions.u(
                'document.execCommand("{0}", false, null)').format(cmd)
        else:
            js = utils_functions.u(
                'document.execCommand("{0}", false, "{1}")').format(cmd, arg)
        if utils_webengine.WEB_ENGINE == 'WEBENGINE':
            self.webView.page().runJavaScript(js)
        else:
            self.webView.page().mainFrame().evaluateJavaScript(js)



    def formatBold(self):
        self.execCommand("bold")

    def formatItalic(self):
        self.execCommand("italic")

    def formatUnderline(self):
        self.execCommand("underline")

    def formatStrikeThrough(self):
        self.execCommand("strikeThrough")


    def formatAlignLeft(self):
        self.execCommand("justifyLeft")

    def formatAlignCenter(self):
        self.execCommand("justifyCenter")

    def formatAlignRight(self):
        self.execCommand("justifyRight")

    def formatAlignJustify(self):
        self.execCommand("justifyFull")

    def formatIncreaseIndent(self):
        self.execCommand("indent")

    def formatDecreaseIndent(self):
        self.execCommand("outdent")

    def formatNumberedList(self):
        self.execCommand("insertOrderedList")

    def formatBulletedList(self):
        self.execCommand("insertUnorderedList")


    def insertImage(self):
        pass

    def createLink(self):
        link, ok = QtWidgets.QInputDialog.getText(
            self, 
            utils.PROGNAME,
            QtWidgets.QApplication.translate('main', 'Enter URL:') + ' ' * 60, 
            QtWidgets.QLineEdit.Normal,
            "http://")
        if ok and link != '':
            url = utils_web.doUrl(link)
            if url.isValid():
                self.execCommand("createLink", link)

    def styleParagraph(self):
        self.execCommand("formatBlock", "p")

    def styleHeading1(self):
        self.execCommand("formatBlock", "h1")

    def styleHeading2(self):
        self.execCommand("formatBlock", "h2")

    def styleHeading3(self):
        self.execCommand("formatBlock", "h3")

    def styleHeading4(self):
        self.execCommand("formatBlock", "h4")







class HtmlEditorDlg(QtWidgets.QDialog):

    def __init__(self, parent=None, content='', allHeaders=False):
        super(HtmlEditorDlg, self).__init__(parent)
        self.main = parent

        # le titre de la fenêtre :
        self.setWindowTitle(
            QtWidgets.QApplication.translate('main', 'Html Editor'))
        self.webViewWidget = WebViewWidget(
            parent=self.main, content=content, allHeaders=allHeaders)

        dialogButtons = utils_functions.createDialogButtons(self)
        buttonBox = dialogButtons[0]

        # on place tout dans une grille :
        grid = QtWidgets.QGridLayout()
        grid.addWidget(self.webViewWidget, 0, 1)
        grid.addWidget(buttonBox, 9, 0, 1, 2)

        # la grille est le widget de base :
        self.setLayout(grid)
        self.setMinimumWidth(400)

    def contextHelp(self):
        # ouvre la page d'aide associée
        utils_functions.openContextHelp('gest-comments')




def formatAdvice(advice, blankTarget=True):
    """
    fonction de mise en forme d'un conseil avant enregistrement.
    On retire les éléments html inutiles (début et fin)
    et on ajoute target="_blank" aux liens.
    """
    # on retire le début et la fin :
    #<html><head></head><body>...</body></html>
    advice = advice.replace(' contenteditable="true"', '')
    advice = advice.replace('<html><head></head><body>', '')
    advice = advice.replace('</body></html>', '')

    # on ajoute target="_blank" aux liens :
    if blankTarget:
        advice = advice.replace('<a href=', '<a target="_blank" href=')

    if advice == '<br>':
        advice = ''
    return advice



