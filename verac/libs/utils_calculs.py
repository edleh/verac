# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Fonctions de calculs (moyennes, etc)
"""

# importation des modules utiles :
from __future__ import division
import math

import utils, utils_functions



###########################################################"
#   FONCTIONS DIVERSES
###########################################################"

def value2List(text):
    """
    renvoie le nombre de chaque charactères pour une chaîne donnée
    """
    result = [0 for i in range(utils.NB_COLORS + 1)]
    for char in text:
        if char in utils.itemsValues:
            result[utils.itemsValues.index(char)] += 1
    return result

def valuesInPercentages(values):
    """
    renvoie le pourcentage de chaque valeur par rapport au total.
    C'est ici qu'on tient compte des X :
        * d'abord on calcule le % de X
        * puis les autres % sont calculés sans tenir compte des X
        * enfin le % de X est utilisé pour relativiser les autres valeurs
          (une fraction des V devient J et une fraction des R devient O)
        * dans la liste retournée, le % de X initial est conservé
    """
    result = [0 for i in range(utils.NB_COLORS + 1)]
    total = sum(values)
    if total < 1:
        # il n'y a rien :
        return result
    # calcul du % de X :
    result[-1] = int(round(values[-1] * 100 / total))
    # total des autres :
    total = sum(values[0:-1])
    if total < 1:
        # il n'y a que des X :
        return result
    # calcul des % sans tenir compte des X :
    for i in range(utils.NB_COLORS):
        result[i] = int(round(values[i] * 100 / total))
    # on recentre les % en fonction des X :
    if result[-1] > 0:
        V2J = int(round(result[0] * result[-1] / 100))
        result[0] -= V2J
        result[1] += V2J
        R2O = int(round(result[3] * result[-1] / 100))
        result[3] -= R2O
    # pour retomber sur 100 % :
    result[2] = 100 - (result[0] + result[1] + result[3])
    return result

def testA(valuesPC):
    """
    test des conditions pour attribuer "A".
    Les valeurs sont données en % (valuesPC)
    """
    result = False
    if valuesPC[0] >= utils.levelA[0] \
        and (valuesPC[0] + valuesPC[1]) >= utils.levelA[1] \
        and valuesPC[2] <= utils.levelA[2] \
        and valuesPC[3] <= utils.levelA[3]:
            result = True
    return result

def testD(valuesPC):
    result = False
    if valuesPC[3] >= utils.levelA[0] \
        and (valuesPC[2] + valuesPC[3]) >= utils.levelA[1] \
        and valuesPC[1] <= utils.levelA[2] \
        and valuesPC[0] <= utils.levelA[3]:
            result = True
    return result

def testB(valuesPC):
    result = False
    if (valuesPC[0] + valuesPC[1]) >= utils.levelB[0] \
        and valuesPC[2] <= utils.levelB[1] \
        and valuesPC[3] <= utils.levelB[2]:
            result = True
    return result



###########################################################"
#   ITEMS
###########################################################"

def calculItemValue(valueItem, coeff=1):
    """
    calcule la valeur retenue pour l'item en fonction des paramètres
    (nombre d'absence autorisées, nombre d'évaluations retenues, ...).
    """
    value = ''
    values = value2List(valueItem)

    # itemMayBeStored indique si l'item peut être pris en compte
    # en fonction des seuils d'évaluations minimaux :
    itemMayBeStored = False
    if values[-1] == 0:
        itemMayBeStored = True
    else:
        # on vérifie les pourcentages d'évaluations par rapport aux non évalués
        pc = False
        if int(round(100 * values[-1] / sum(values))) < utils.levelValidItemPC:
            pc = True
        # on vérifie si le nbre mini d'évaluation est respecté
        nb = False
        if sum(values[0:-1]) >= utils.levelValidItemNB:
            nb = True
        if utils.levelValidItemOP == 0:
            if pc or nb:
                itemMayBeStored = True
        else:
            if pc and nb:
                itemMayBeStored = True

    if not(itemMayBeStored):
        # s'il y a trop d'absences on renvoie "X" :
        value = utils.itemsValues[-1]
    elif utils.MODECALCUL == 0 or utils.NBMAXEVAL == 0:
        value = valueItem
    elif sum(values) <= utils.NBMAXEVAL:
        # s'il y a moins d'évaluations que le nbre maxi
        value = valueItem
    elif utils.MODECALCUL == 1:
    # si on choisit l'option BESTVALUES
        nbchar = 0
        while (nbchar < utils.NBMAXEVAL):
            for numchar in range(utils.NB_COLORS + 1):
                for i in range(values[numchar]):
                    if nbchar >= utils.NBMAXEVAL:
                        break
                    value += utils.itemsValues[numchar]
                    nbchar += 1
    elif utils.MODECALCUL == 2:
    # si on choisit l'option LASTVALUES
        nbchar = 0
        numchar = 1
        isXInItem = False
        while nbchar < (utils.NBMAXEVAL):
            char = valueItem[len(valueItem) - numchar]
            if char != utils.itemsValues[-1]:
                value = char + value
                nbchar += 1
            else:
                isXInItem = True
            numchar += 1
            if numchar > len(valueItem):
                nbchar = utils.NBMAXEVAL
        if isXInItem and value == '':
            value = utils.itemsValues[-1]
    else:
        value = valueItem

    # coefficiente plus fortement les 4 dernières évaluations (option COEFLAST):
    if (utils.COEFLAST == 1):
        debut = 0
        newValue = ''
        coef = 1
        if len(value) > 5:
            debut = len(value) - 5
            newValue = value[0:debut]
        else:
            coef = 6 - len(value)
            for char in value[debut:len(value)]:
                for i in range(coef):
                    newValue += char
                coef += 1
            value = newValue

    values = value2List(value)

    # pondère le poids de chaque item pour le calcul du bilan
    if coeff > 0 and len(value) > 0:
        rapport = int(round(100 / len(value)))
        if abs(100 - rapport * len(value)) > abs(100 - (rapport + 1) * len(value)):
            rapport += 1
        values = [elem * rapport * coeff for elem in values]

    return values

def moyenneItem(text):
    """
    renvoie la "moyenne" de la chaîne de caractères 
    en fonctions des valeurs définies pour chaque lettre
    et des seuils d'acquisition définis (utile pour choisir la couleur de la cellule).
    S'il n'y a rien, on renvoie une chaîne vide.
    """
    result = ''
    values = calculItemValue(text, -1)
    result = calculBilan(values)
    return result



###########################################################"
#   BILANS
###########################################################"

calculBilanDic = {}

def reinitCalculBilanDic():
    global calculBilanDic
    calculBilanDic = {}

def calculBilan(values, admin=False):
    """
    on utilise le dictionnaire calculBilanDic
    pour ne pas recalculer systématiquement
    (permet d'accélérer un peu l'affichage)
    """
    global calculBilanDic
    result = ''
    if not(isinstance(values, list)):
        values = value2List(values)
    if sum(values) < 1:
        return result
    values2 = ''
    for n in values:
        values2 = '{0}|{1}'.format(values2, n)
    if (values2 in calculBilanDic):
        return calculBilanDic[values2]
    valuesPC = valuesInPercentages(values)
    if admin:
        if sum(values[0:-1]) < utils.levelX_NB:
            # pas assez d'évaluations :
            return utils.itemsValues[-1]
    if valuesPC[-1] >= utils.levelX:
        # trop de X :
        result = utils.itemsValues[-1]
    elif testA(valuesPC):
        result = utils.itemsValues[0]
    elif testD(valuesPC):
        result = utils.itemsValues[3]
    elif testB(valuesPC):
        result = utils.itemsValues[1]
    else:
        result = utils.itemsValues[2]
    calculBilanDic[values2] = result
    return result


moyenneBilanDic = {}

def moyenneBilan(valeur):
    """
    on utilise le dictionnaire moyenneBilanDic
    pour ne pas recalculer systématiquement
    (permet d'accélérer un peu l'affichage).
    Par rapport à calculBilan, cette fonction ne tient pas compte des seuils.
    Par exemple, la condition "pas un seul R pour avoir V" sera ignorée.
    Utilisé plutôt pour les "moyennes" de groupes ou pour les bilans
    des compétences partagées
    (un élève avec un R donné par un seul prof et beaucoup 
    de V par tous les autres pourra avoir V, 
    surtout pour le référentiel où le R peut dater).
    On tient compte des X pour recentrer si besoin, 
    mais on ne met pas X s'il y a des évaluations
    """
    global moyenneBilanDic
    newValeur = ''
    # on teste si la valeur est déjà dans le dictionnaire :
    try:
        newValeur = moyenneBilanDic[valeur]
        return newValeur
    except:
        pass
    values = value2List(valeur)
    if sum(values[0:-1]) > 0:
        # on utilise les valeurs par défaut :
        valeursEval = [100, 67, 33, 0, 0]
        result = 0
        for i in range(utils.NB_COLORS + 1):
            result += values[i] * valeursEval[i]
        result = result / sum(values[0:-1])
        for i in range(utils.NB_COLORS - 1, -1, -1):
            if result >= utils.levelItem[i]:
                newValeur = utils.itemsValues[i]
    elif values[-1] > 0:
        newValeur = utils.itemsValues[-1]
    else:
        newValeur = ''
    moyenneBilanDic[valeur] = newValeur
    return newValeur



###########################################################"
#   NOTES ET COMPTAGES
###########################################################"

def average(liste=[]):
    """
    retourne la moyenne d'une liste de nombres
    """
    if len(liste) < 1:
        return False
    return sum(liste) * 1.0 / len(liste)

def minMax(liste=[]):
    """
    retourne les valeurs minimale et maximale d'une liste de nombres
    """
    if len(liste) < 1:
        return False
    liste.sort()
    return liste[0], liste[-1]

def standardDeviation(liste=[], avg=''):
    """
    retourne l'écart-type d'une liste de nombres
    """
    if len(liste) < 1:
        return False
    if avg == '':
        avg = average(liste)
    variance = 0.0
    for x in liste:
        variance += (x - avg)**2
    variance = variance / len(liste)
    return math.sqrt(variance)

def calculMedian(liste=[]):
    """
    retourne la médiane d'une liste de nombres
    """
    indiceM, median = 0, False
    total = len(liste)
    if total > 0:
        liste.sort()
        indiceM = total / 2
        if int(indiceM) == indiceM:
            indiceM = int(indiceM)
            median = (liste[indiceM - 1] + liste[indiceM]) / 2
        else:
            indiceM = int(indiceM)
            median = liste[indiceM]
    return (indiceM, median)

def medianAndQuartiles(liste=[]):
    """
    retourne la médiane et les quartiles d'une liste de nombres
    """
    premierQ, median, troisiemeQ = False, False, False
    total = len(liste)
    if total > 0:
        liste.sort()
        (indiceM, median) = calculMedian(liste)
        premierQ = calculMedian(liste[:indiceM])[1]
        if total % 2 == 0:
            troisiemeQ = calculMedian(liste[indiceM:])[1]
        else:
            troisiemeQ = calculMedian(liste[indiceM + 1:])[1]
        if total == 2:
            troisiemeQ = liste[1]
    return [premierQ, median, troisiemeQ]

def calculNote(main, value='', values=False):
    note = ''
    # on sépare entre les valeurs possibles :
    if not(values):
        values = value2List(value)
    total = sum(values)
    if values[-1] > 0:
        # calcul du % de X :
        pcX = values[-1] / total
        # on recentre les valeurs en fonction du % de X :
        V2J = values[0] * pcX
        values[0] -= V2J
        values[1] += V2J
        R2O = values[3] * pcX
        values[3] -= R2O
        values[2] += R2O
        # on recalcule le total sans tenir compte des X :
        total = sum(values[0:-1])
    # on peut calculer la note :
    if total > 0:
        note = 0
        for i in range(utils.NB_COLORS):
            note += values[i] * utils.valeursEval[i] / 5
        note = note / total
        note = round(note, utils.precisionNotes)
    return note

def calculStats(main, value):
    statsList = []
    # on sépare entre les valeurs possibles :
    values = value2List(value)
    # on ajoute les quantités de chaque :
    for i in range(utils.NB_COLORS, -1, -1):
        valeur = values[i]
        if valeur > 0:
            statsList.append(valeur)
        else:
            statsList.append('')
    # et en pourcentages :
    total = sum(values)
    if total > 0:
        for i in range(utils.NB_COLORS, -1, -1):
            valeur = int(round(100 * values[i] / total))
            if valeur > 0:
                statsList.append(valeur)
            else:
                statsList.append('')
    else:
        for i in range(utils.NB_COLORS + 1):
            statsList.append('')
    # calcul et ajout de la note :
    note = calculNote(main, values=values)
    statsList.append(note)
    return statsList


