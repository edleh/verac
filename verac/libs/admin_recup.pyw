#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    Lance une récupération pour la version passée en paramètre
    et crée un fichier rapport.txt dans le dossier admin.
    Ce programme est lancé par le planificateur de tâches.
    Il quitte une fois le travail accompli.
    Il fonctionne en mode console, donc peut être utilisé sur un serveur
    sans interface X.
"""

# importation des modules utiles :
from __future__ import division
import sys
import os

# récupération du chemin :
#HERE = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
HERE = os.path.abspath(os.path.join(os.path.dirname(sys.argv[0]), '..'))
# ajout du chemin au path (+ libs) :
sys.path.insert(0, HERE)
sys.path.insert(0, HERE + os.sep + 'libs')
# on démarre dans le bon dossier :
os.chdir(HERE)

# importation des modules perso :
import utils, utils_functions, utils_db, utils_filesdirs
import admin, admin_calc_results, admin_ftp

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
else:
    from PyQt4 import QtCore, QtGui as QtWidgets

# on est en NOGUI (pas d'interface) :
utils.changeNoGui(True)




# ******************************************************************
# LA CLASSE DE RÉCUPÉRATION
# ******************************************************************

class RecupObject(QtCore.QObject):
    def __init__(self):
        super(RecupObject, self).__init__()

        # on est admin ; il faut le définir dans un self.me minimal :
        self.me = {
            'userMode': 'admin', 'userName': 'Administrateur', 
            'userId': -1, 'user': '', 'Mdp': '', 'Matiere': '', 
            'other': ''}

        # initialisation des bases de données :
        self.db_localConfig = None
        self.dbFile_users, self.dbFileTemp_users = '', ''
        self.db_users = None
        self.dbFile_commun, self.dbFileTemp_commun = '', ''
        self.db_commun = None

        # le dossier des fichiers etc :
        self.beginDir = QtCore.QDir.currentPath()

        # admin_recup ne peut être lancé que depuis un poste admin.
        # Donc on utilise db_localConfig qui est dans le home :
        self.localConfigDir = utils_filesdirs.createConfigAppDir(
            utils.PROGLINK, beginDir=self.beginDir).canonicalPath()

        # création du dossier temporaire :
        self.tempPath = utils_filesdirs.createTempAppDir(
            utils.PROGLINK + '_recup')
        utils_filesdirs.createDirs(self.tempPath, 'up')
        utils_filesdirs.createDirs(self.tempPath, 'down')

        # on y copie la base config :
        dbFile_localConfig = utils_functions.u(
            '{0}/config.sqlite').format(self.localConfigDir)
        dbFile_localConfigTemp = utils_functions.u(
            '{0}/config.sqlite').format(self.tempPath)
        utils_filesdirs.removeAndCopy(
            dbFile_localConfig, dbFile_localConfigTemp)
        self.db_localConfig = utils_db.createConnection(
            self, dbFile_localConfigTemp)[0]

        # on lit la version à récupérer (dernier argument) :
        versionName = sys.argv[-1]
        self.actualVersion = {
            'id_version': 0, 
            'versionName': versionName, 
            'versionUrl': '', 
            'versionLabel': ''}

        # pour savoir si l'ordi est connecté à internet :
        self.NetOK = True

        # première définition de la base commun 
        # (on en a besoin dans admin.initConfig) :
        self.dbFile_commun = utils_functions.u(
            '{0}/{1}/commun.sqlite').format(
                self.localConfigDir, 
                self.actualVersion['versionName'])
        self.dbFileTemp_commun = utils_functions.u(
            '{0}/commun.sqlite').format(self.tempPath)
        utils_filesdirs.removeAndCopy(
            self.dbFile_commun, self.dbFileTemp_commun)
        self.db_commun = utils_db.createConnection(
            self, self.dbFileTemp_commun)[0]

        # viewType sert aux upgrade des fichiers profs trop vieux :
        self.viewType = utils.VIEW_ITEMS
        # on initialise l'unité admin :
        admin.initConfig(self)
        utils.initTranslations(self)
        utils_functions.readConfigCalculs(self)

        # définition des bases users et commun et connexion dans temp :
        # (on utilise celles qui sont dans verac_admin)
        self.dbFileTemp_users = utils_functions.u(
            '{0}/users.sqlite').format(self.tempPath)
        utils_filesdirs.removeAndCopy(
            admin.dbFileFtp_users, self.dbFileTemp_users)
        self.db_users = utils_db.createConnection(
            self, self.dbFileTemp_users)[0]
        utils_db.closeConnection(self, dbName='commun')
        utils_filesdirs.removeAndCopy(
            admin.dbFileFtp_commun, self.dbFileTemp_commun)
        self.db_commun = utils_db.createConnection(
            self, self.dbFileTemp_commun)[0]

        # lecture des paramètres de l'établissement 
        # depuis commun.config :
        config = {}
        query_commun = utils_db.query(self.db_commun)
        commandLine_commun = utils_db.q_selectAllFrom.format('config')
        query_commun = utils_db.queryExecute(
            commandLine_commun, query=query_commun)
        while query_commun.next():
            config[query_commun.value(0)] = (
                query_commun.value(1), query_commun.value(2))
        self.siteUrlBase = config.get('siteUrlBase', (0, ''))[1]
        self.siteUrlPublic = config.get('siteUrlPublic', (0, ''))[1]
        self.siteUrlPublic = utils_functions.removeSlash(
            self.siteUrlPublic)
        self.limiteBLTPerso = config.get('limiteBLTPerso', (4, ''))[0]
        self.annee_scolaire = config.get('annee_scolaire', (0, ''))
        if self.annee_scolaire[0] < 1:
            self.annee_scolaire = utils_functions.calcAnneeScolaire()

        # on enregistre les messages dans le fichier rapport.txt :
        self.outFileText = ''

        # on écrit l'entête du fichier (titre, date, ...) :
        maintenant = QtCore.QDateTime.currentDateTime().toString(
            'yyyy-MM-dd hh:mm')
        utils_functions.afficheMessage(
            self, 
            [
                QtWidgets.QApplication.translate('main', 'MESSAGES WINDOW'), 
                maintenant], 
            tags=('h1'))

        # enfin on lance la récup :
        QtCore.QTimer.singleShot(1, self.doRecup)


    def doRecup(self):
        """
        Lance une récupération
        """
        reponse = False
        # on lance la récup :
        if 'RECUP_LEVEL_REFERENTIAL' in sys.argv:
            reponse = admin_calc_results.updateBilans(
                self, 
                recupLevel=utils.RECUP_LEVEL_REFERENTIAL, 
                msgFin=False)
        else:
            reponse = admin_calc_results.updateBilans(
                self, 
                msgFin=False)

        # on recopie la base admin :
        utils_filesdirs.removeAndCopy(
            admin.dbFileTemp_admin, admin.dbFile_admin)

        # on referme les bases :
        self.db_localConfig.close()
        del self.db_localConfig
        utils_db.sqlDatabase.removeDatabase('config')
        utils_db.closeConnection(self, dbName='users')
        utils_db.closeConnection(self, dbName='commun')
        # base recup_evals :
        if admin.db_recupEvals != None:
            admin.db_recupEvals.close()
            del admin.db_recupEvals
            utils_db.sqlDatabase.removeDatabase('recup_evals')
        # base admin :
        if admin.db_admin != None:
            admin.db_admin.close()
            del admin.db_admin
            utils_db.sqlDatabase.removeDatabase('admin')
        # base resultats :
        if admin.db_resultats != None:
            admin.db_resultats.close()
            del admin.db_resultats
            utils_db.sqlDatabase.removeDatabase('resultats')
        # base referential_propositions :
        if admin.db_referentialPropositions != None:
            admin.db_referentialPropositions.close()
            del admin.db_referentialPropositions
            utils_db.sqlDatabase.removeDatabase('referential_propositions')

        # un dernier message (nom de l'établissement et état de la récup) :
        if reponse:
            message = QtWidgets.QApplication.translate(
                'main', 'The recovery went well.')
        else:
            message = QtWidgets.QApplication.translate(
                'main', 'There was a problem during recovery.')
        utils_functions.afficheMessage(
            self, 
            [
                utils_functions.u(
                    'VERSION : {0}').format(
                        self.actualVersion['versionName']), 
                message], 
            tags=('h2'), 
            console=True)

        # on referme le fichier rapport.txt :
        fileName = utils_functions.u(
            '{0}/rapport.txt').format(self.tempPath)
        outFile = QtCore.QFile(fileName)
        if outFile.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            stream = QtCore.QTextStream(outFile)
            stream.setCodec('UTF-8')
            stream << self.outFileText
            outFile.close()
        utils_filesdirs.removeAndCopy(
            fileName, 
            utils_functions.u('{0}/rapport.txt').format(admin.adminDir))

        if admin_ftp.ftpTest(self):
            # on envoie le fichier rapport-x.log créé d'après rapport.txt :
            dirSite = utils_functions.u(
                '{0}/protected').format(admin.dirSitePrive)
            dirLocal = utils_functions.u(
                '{0}/protected').format(admin.dirLocalPrive)
            dayOfWeek = QtCore.QDate.currentDate().dayOfWeek()
            REPORT_FILENAME = 'rapport-{0}.log'.format(dayOfWeek)
            theFileName = utils_functions.u(
                '{0}/protected/{1}').format(
                    admin.dirLocalPrive, REPORT_FILENAME)
            utils_filesdirs.removeAndCopy(
                utils_functions.u('{0}/rapport.txt').format(self.tempPath), 
                theFileName)
            admin_ftp.ftpPutFile(self, dirSite, dirLocal, REPORT_FILENAME)

        # on quitte :
        sys.exit()


    def changeDBMyState(self, isModified=True, force=False):
        """
        Juste pour compatibilité
        """
        return True



# ******************************************************************
# LANCEMENT DU PROGRAMME
# ******************************************************************

if __name__ == '__main__':
    app = QtCore.QCoreApplication(sys.argv)

    ###########################################
    # Installation de l'internationalisation:
    ###########################################
    locale = QtCore.QLocale.system().name()
    QtTranslationsPath = QtCore.QLibraryInfo.location(
        QtCore.QLibraryInfo.TranslationsPath)
    qtTranslator = QtCore.QTranslator()
    if qtTranslator.load('qt_' + locale, QtTranslationsPath):
        app.installTranslator(qtTranslator)
    trans_dir = QtCore.QDir('./translations')
    localefile = trans_dir.filePath(utils.PROGLINK + '_' + locale)
    appTranslator = QtCore.QTranslator()
    if appTranslator.load(localefile, ''):
        app.installTranslator(appTranslator)

    # on ne lance que s'il y a un argument (versionName) :
    if len(sys.argv) > 1:
        recupObject = RecupObject()
        sys.exit(app.exec_())


