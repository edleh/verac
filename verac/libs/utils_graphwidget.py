# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    un GraphWidget avec des noeuds et des liens.
    Utilisé dans le dialogue de gestion des items et bilans.
"""


# importation des modules utiles :
from __future__ import division
import math

import utils, utils_functions

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets, QtGui
else:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui



NODE_DEFAULT_SIZE = 5
NODE_SELECTED_SIZE = 7
NODES_COLORS = {'yellow': (QtCore.Qt.yellow, QtCore.Qt.darkYellow),
                'blue': (QtCore.Qt.blue, QtCore.Qt.darkBlue),
                'blue2': ('#6a6aff', '#6a6a80'),
                'green': (QtCore.Qt.green, QtCore.Qt.darkGreen),
                'magenta': (QtCore.Qt.magenta, QtCore.Qt.darkMagenta),
                'cyan': (QtCore.Qt.cyan, QtCore.Qt.darkCyan),
                'red': (QtCore.Qt.red, QtCore.Qt.darkRed),
                }

EDGE_ARROW_SIZE = 5
EDGE_DEFAULT_COLOR = QtCore.Qt.gray
EDGE_SELECTED_COLOR = QtCore.Qt.black



class Node(QtWidgets.QGraphicsItem):
    Type = QtWidgets.QGraphicsItem.UserType + 1

    def __init__(self, scene, pos=(0, 0), color='yellow', data=None, genre=None):
        super(Node, self).__init__()

        self.scene = scene
        self.edgeList = []
        self.setPos(pos[0], pos[1])
        self.oldPos = self.pos()

        self.color = color
        self.data = data
        self.genre = genre
        self.text = data[1]
        self.textItem = QtWidgets.QGraphicsSimpleTextItem(data[1], self)
        self.textItem.setPos(QtCore.QPointF(-16, -32))

        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        self.setAcceptHoverEvents(True)
        self.setZValue(1)

        # pour distinguer les simples et doubles clics :
        self.doubleClic = False

        self.isSelected = False
        self.size = NODE_DEFAULT_SIZE

    def type(self):
        return Node.Type

    def setText(self, text):
        newData = (self.data[0], text, self.data[2], self.data[3])
        self.data = newData
        self.text = text
        self.textItem.setText(text)

    def addEdge(self, edge):
        self.edgeList.append(edge)
        edge.adjust()

    def edges(self):
        return self.edgeList

    def boundingRect(self):
        adjust = 1.0
        return QtCore.QRectF(-self.size - adjust, -self.size - adjust,
                             2.1*self.size + adjust, 2.1*self.size + adjust)

    def shape(self):
        path = QtGui.QPainterPath()
        path.addEllipse(-self.size, -self.size, 2*self.size, 2*self.size)
        return path

    def paint(self, painter, option, widget):
        painter.setPen(QtCore.Qt.NoPen)
        painter.setBrush(QtCore.Qt.darkGray)
        size7 = int(-0.7 * self.size)
        size2 = 2 * self.size
        size3 = int(0.3 * self.size)
        painter.drawEllipse(size7, size7, size2, size2)
        if self.color in NODES_COLORS:
            color = NODES_COLORS[self.color][0]
            darkcolor = NODES_COLORS[self.color][1]
        else :
            color = NODES_COLORS[0][0]
            darkcolor = NODES_COLORS[0][1]
        gradient = QtGui.QRadialGradient(-size3, -size3, self.size)
        if option.state & QtWidgets.QStyle.State_Sunken:
            gradient.setCenter(size3, size3)
            gradient.setFocalPoint(size3, size3)
            gradient.setColorAt(1, QtGui.QColor(color).lighter(120))
            gradient.setColorAt(0, QtGui.QColor(darkcolor).lighter(120))
        else:
            gradient.setColorAt(0, QtGui.QColor(color))
            gradient.setColorAt(1, QtGui.QColor(darkcolor))
        painter.setBrush(QtGui.QBrush(gradient))
        painter.setPen(QtGui.QPen(QtCore.Qt.black, 0))
        painter.drawEllipse(-self.size, -self.size, size2, size2)

    def itemChange(self, change, value):
        if change == QtWidgets.QGraphicsItem.ItemPositionHasChanged:
            for edge in self.edgeList:
                edge.adjust()
        return super(Node, self).itemChange(change, value)

    def select(self, value):
        self.isSelected = value
        if value:
            self.size = NODE_SELECTED_SIZE
            self.scene.selectedGraphicsItem = self
            self.scene.lastGraphicsItem = self
        else:
            self.size = NODE_DEFAULT_SIZE
            self.scene.selectedGraphicsItem = None
        zValue = 0
        if value:
            zValue = 0.5
        for edge in self.edgeList:
            edge.setZValue(zValue)
            edge.isShowed = value
            edge.adjust()

    def hoverEnterEvent(self, event):
        self.select(True)
        super(Node, self).hoverEnterEvent(event)

    def hoverLeaveEvent(self, event):
        self.select(False)
        super(Node, self).hoverLeaveEvent(event)

    def mousePressEvent(self, event):
        # on récupère la position de départ
        # pour distinguer le clic du déplacement :
        if event.button() == QtCore.Qt.LeftButton:
            self.oldPos = self.pos()
        super(Node, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == QtCore.Qt.LeftButton:
            delta = math.hypot(self.pos().x() - self.oldPos.x(), 
                                self.pos().y() -self.oldPos.y())
            # si delta est petit, on a fait un clic :
            if delta < 1.0:
                QtCore.QTimer.singleShot(200, self.timerFinished)
        super(Node, self).mouseReleaseEvent(event)

    def mouseDoubleClickEvent(self, event):
        self.doubleClic = True
        self.scene.main.beginNode = None
        QtCore.QTimer.singleShot(200, self.timerFinished)
        self.scene.main.nodeMouseDoubleClickEvent(event, self)

    def timerFinished(self):
        if not(self.doubleClic):
            # donc c'est bien un simple clic
            if self.scene.main.beginNode != None:
                if self.scene.main.beginNode != self:
                    self.scene.main.doLinkOrUnlinkEnd(self)
                self.scene.main.beginNode = None
            else:
                self.scene.main.beginNode = self
        self.doubleClic = False



class Edge(QtWidgets.QGraphicsItem):
    Type = QtWidgets.QGraphicsItem.UserType + 2

    def __init__(self, scene, sourceNode, destNode, 
                        defaultColor=EDGE_DEFAULT_COLOR, selectedColor=EDGE_SELECTED_COLOR, 
                        width=1, double=True):
        super(Edge, self).__init__()

        self.scene = scene
        self.arrowSize = EDGE_ARROW_SIZE
        self.color = defaultColor
        self.defaultColor = defaultColor
        self.selectedColor = selectedColor
        self.sourcePoint = QtCore.QPointF()
        self.destPoint = QtCore.QPointF()
        self.isShowed = False
        self.isSelected = False
        self.width = width
        self.double = double
        self.setZValue(0)

        self.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable)
        self.setCacheMode(QtWidgets.QGraphicsItem.DeviceCoordinateCache)
        self.setAcceptHoverEvents(True)

        self.source = sourceNode
        self.dest = destNode
        self.source.addEdge(self)
        self.dest.addEdge(self)
        self.adjust()

    def type(self):
        return Edge.Type

    def sourceNode(self):
        return self.source

    def setSourceNode(self, node):
        self.source = node
        self.adjust()

    def destNode(self):
        return self.dest

    def setDestNode(self, node):
        self.dest = node
        self.adjust()

    def adjust(self):
        if not self.source or not self.dest:
            return
        line = QtCore.QLineF(self.mapFromItem(self.source, 0, 0),
                            self.mapFromItem(self.dest, 0, 0))
        length = line.length()
        self.prepareGeometryChange()
        if length > 20.0:
            edgeOffset = QtCore.QPointF((line.dx() * 10) / length,
                                        (line.dy() * 10) / length)

            self.sourcePoint = line.p1() + edgeOffset
            self.destPoint = line.p2() - edgeOffset
        else:
            self.sourcePoint = line.p1()
            self.destPoint = line.p1()

    def boundingRect(self):
        if not self.source or not self.dest:
            return QtCore.QRectF()
        extra = 1.0#(self.width + 20) / 2.0
        p1 = self.sourcePoint
        p2 = self.destPoint
        return QtCore.QRectF(p1, QtCore.QSizeF(p2.x() - p1.x(), p2.y() - p1.y())).normalized().adjusted(-extra, -extra, extra, extra)

    def paint(self, painter, option, widget):
        if not self.source or not self.dest:
            return

        if self.isShowed:
            color = self.selectedColor
        else:
            color = self.defaultColor
        if self.isSelected:
            width = self.width * 4
        else:
            width = self.width

        # Draw the line itself.
        line = QtCore.QLineF(self.sourcePoint, self.destPoint)
        if line.length() == 0.0:
            return

        painter.setPen(QtGui.QPen(color, width, QtCore.Qt.SolidLine,
                QtCore.Qt.RoundCap, QtCore.Qt.RoundJoin))
        painter.drawLine(line)

        # Draw the arrows if there's enough room.
        angle = math.acos(line.dx() / line.length())
        pi = math.pi
        if line.dy() >= 0:
            angle = 2.0 * pi - angle

        sourceArrowP1 = self.sourcePoint + QtCore.QPointF(math.sin(angle + pi / 3) * self.arrowSize,
                                                          math.cos(angle + pi / 3) * self.arrowSize)
        sourceArrowP2 = self.sourcePoint + QtCore.QPointF(math.sin(angle + pi - pi / 3) * self.arrowSize,
                                                          math.cos(angle + pi - pi / 3) * self.arrowSize)
        destArrowP1 = self.destPoint + QtCore.QPointF(math.sin(angle - pi / 3) * self.arrowSize,
                                                      math.cos(angle - pi / 3) * self.arrowSize)
        destArrowP2 = self.destPoint + QtCore.QPointF(math.sin(angle - pi + pi / 3) * self.arrowSize,
                                                      math.cos(angle - pi + pi / 3) * self.arrowSize)

        painter.setBrush(color)
        if self.double:
            painter.drawPolygon(QtGui.QPolygonF([line.p1(), sourceArrowP1, sourceArrowP2]))
        painter.drawPolygon(QtGui.QPolygonF([line.p2(), destArrowP1, destArrowP2]))

    def select(self, value):
        self.isSelected = value
        if value:
            self.scene.selectedGraphicsItem = self
            self.scene.lastGraphicsItem = self
        else:
            self.scene.selectedGraphicsItem = None

    def hoverEnterEvent(self, event):
        self.select(True)
        super(Edge, self).hoverEnterEvent(event)

    def hoverLeaveEvent(self, event):
        self.select(False)
        super(Edge, self).hoverLeaveEvent(event)




class GraphicsScene(QtWidgets.QGraphicsScene):
    def __init__(self, parent=None):
        super(GraphicsScene, self).__init__(parent)
        self.main = parent.main
        self.setItemIndexMethod(QtWidgets.QGraphicsScene.NoIndex)
        self.setSceneRect(-200, -200, 400, 400)
        self.mustWait = False
        self.selectedGraphicsItem = None
        self.lastGraphicsItem = None
        self.tempLine = None

    def mouseMoveEvent(self, event):
        if self.main.beginNode != None:
            if self.tempLine == None:
                self.tempLine = QtWidgets.QGraphicsLineItem(QtCore.QLineF(
                    self.main.beginNode.scenePos(), event.scenePos()))
                self.tempLine.setPen(QtGui.QPen(QtCore.Qt.red, 2))
                self.addItem(self.tempLine)
            else:
                newLine = QtCore.QLineF(self.tempLine.line().p1(), event.scenePos())
                self.tempLine.setLine(newLine)
        elif self.tempLine != None:
            self.main.beginNode = None
            self.removeItem(self.tempLine)
            self.tempLine = None
        super(GraphicsScene, self).mouseMoveEvent(event)

    def contextMenuEvent(self, event):
        self.main.itemContextMenuEvent(event, self.selectedGraphicsItem)

    def mouseReleaseEvent(self, event):
        if (event.button() == QtCore.Qt.LeftButton) and (self.main.beginNode != None):
            if (self.selectedGraphicsItem == None) \
            or not(isinstance(self.selectedGraphicsItem, Node)):
                self.main.beginNode = None
                self.removeItem(self.tempLine)
                self.tempLine = None
        super(GraphicsScene, self).mouseReleaseEvent(event)




class GraphWidget(QtWidgets.QGraphicsView):
    """
    Le GraphWidget ne sert qu'à l'affichage de la scène.
    """
    def __init__(self, parent=None):
        super(GraphWidget, self).__init__()
        self.main = parent

        scene = GraphicsScene(self)
        self.setScene(scene)
        self.setCacheMode(QtWidgets.QGraphicsView.CacheBackground)
        self.setViewportUpdateMode(QtWidgets.QGraphicsView.BoundingRectViewportUpdate)
        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QtWidgets.QGraphicsView.AnchorViewCenter)
        self.scale(0.8, 0.8)

    def wheelEvent(self, event):
        if utils.PYQT == 'PYQT5':
            angle = event.angleDelta().y()
        else:
            angle = event.delta()
        if angle > 0:
            scale = 1.1
        else:
            scale = 10 / 11
        self.scale(scale, scale)


