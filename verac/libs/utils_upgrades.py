# -*- coding: utf-8 -*-

#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------

"""
DESCRIPTION :
    procédures de mises à jour des bases de données.
"""

# importation des modules utiles :
from __future__ import division

import utils, utils_functions, utils_db, utils_calculs, utils_filesdirs

# PyQt5 ou PyQt4 :
if utils.PYQT == 'PYQT5':
    from PyQt5 import QtCore, QtWidgets
else:
    from PyQt4 import QtCore, QtGui as QtWidgets



##################################################################'
##################################################################'
##################################################################'
#
#       BASE PROF :
#
##################################################################'
##################################################################'
##################################################################'


def upgradeProf(main, fileProfxx=None, profxxNoExt=''):
    """
    Mise à jour de la base prof.
    La base peut être passée en paramètre, 
    ce qui permet à l'admin de mettre toutes les bases à jour lors d'une récup.
    """
    if fileProfxx != None:
        # c'est l'admin qui appelle la fonction :
        fileProfxxTemp = utils_functions.u('{0}/{1}.sqlite').format(main.tempPath, profxxNoExt)
        utils_filesdirs.removeAndCopy(fileProfxx, fileProfxxTemp)
        (main.db_my, dbName) = utils_db.createConnection(main, fileProfxxTemp)
    versionDB = utils_db.readVersionDB(main.db_my)
    if versionDB == utils.VERSIONDB_PROF:
        # base à la bonne version : rien à faire
        return True
    elif versionDB > utils.VERSIONDB_PROF:
        # on ouvre une base récente avec une vieille version de VÉRAC
        if fileProfxx != None:
            # si c'est l'admin, on laisse tomber en espérant qu'il met à jour
            # (la récup automatique ne gère pas les messages)
            return True
        else:
            # on ouvre la base avec une version obsolète de VÉRAC.
            # On prévient qu'il faut mettre VÉRAC à jour :
            m0 = QtWidgets.QApplication.translate(
                'main', 'THE VERAC VERSION IS TOO OLD.')
            m1 = QtWidgets.QApplication.translate(
                'main', 'VERAC version installed on this computer is too old')
            m2 = QtWidgets.QApplication.translate(
                'main', 'and may damage your database.')
            m3 = QtWidgets.QApplication.translate(
                'main', 'Update VERAC before continuing.')
            m4 = QtWidgets.QApplication.translate(
                'main', '("Utils > UpdateVerac" or "Utils > TestUpdateVerac" menu)')
            message = utils_functions.u(
                '<p><b>{0}</b></p>'
                '<p></p><p>{1}<br/>{2}</p>'
                '<p><b>{3}</b><br/>{4}</p>').format(m0, m1, m2, m3, m4)
            utils_functions.messageBox(main, level='critical', message=message)
            # et on retourne False pour désactiver les envois :
            return False

    # donc il faut upgrader la base
    import prof
    query_my = None

    # passage de la base à la version 18 :
    if versionDB < 18:
        debut = QtCore.QTime.currentTime()
        utils_functions.doWaitCursor()
        try:
            query_my, transactionOK = utils_db.queryTransactionDB(
                main.db_my)
            # récupération de la table absences dans une liste :
            newTable = []
            commandLine = utils_db.q_selectAllFrom.format('absences')
            query_my = utils_db.queryExecute(commandLine, query=query_my)
            while query_my.next():
                id_groupe = int(query_my.value(0))
                periode = int(query_my.value(1))
                id_eleve = int(query_my.value(2))
                abs0 = int(query_my.value(3))
                abs1 = int(query_my.value(4))
                newTable.append((id_groupe, periode, id_eleve, abs0, abs1, 0, 0))
            # on efface et recrée la table admin.eleves :
            commandLine = utils_db.qdf_table.format('absences')
            query_my = utils_db.queryExecute(commandLine, query=query_my)
            # on remplit la table :
            commandLine = utils_db.insertInto('absences', 7)
            for data in newTable:
                query_my.prepare(commandLine)
                for i in range(7):
                    query_my.addBindValue(data[i])
                query_my.exec_()
            # on met à jour le numéro de version :
            utils_db.changeInConfigTable(
                main, main.db_my, {'versionDB': (18, '')})
            # il faudra enregistrer :
            main.changeDBMyState()
        finally:
            utils_db.endTransaction(
                query_my, main.db_my, transactionOK)
            utils_functions.restoreCursor()
            fin = QtCore.QTime.currentTime()
            duree = debut.msecsTo(fin)
            utils_functions.myPrint('DUREE: ', duree)
    # passage de la base à la version 19 :
    if versionDB < 19:
        debut = QtCore.QTime.currentTime()
        utils_functions.doWaitCursor()
        try:
            query_my, transactionOK = utils_db.queryTransactionDB(
                main.db_my)
            # on crée la table lsu :
            commandLine = utils_db.listTablesProf['lsu'][0]
            query_my = utils_db.queryExecute(commandLine, query=query_my)
            # on met à jour le numéro de version :
            utils_db.changeInConfigTable(
                main, main.db_my, {'versionDB': (19, '')})
            # il faudra enregistrer :
            main.changeDBMyState()
        finally:
            utils_db.endTransaction(
                query_my, main.db_my, transactionOK)
            utils_functions.restoreCursor()
            fin = QtCore.QTime.currentTime()
            duree = debut.msecsTo(fin)
            utils_functions.myPrint('DUREE: ', duree)

    if fileProfxx != None:
        # on referme la base prof :
        if query_my != None:
            query_my.clear()
            del query_my
        main.db_my.close()
        del main.db_my
        utils_db.sqlDatabase.removeDatabase(dbName)
        utils_filesdirs.removeAndCopy(fileProfxxTemp, fileProfxx)
    return True






##################################################################'
##################################################################'
##################################################################'
#
#       BASES ADMIN :
#
##################################################################'
##################################################################'
##################################################################'

def upgradeAdmin(main):
    """
    Mises à jour des bases de l'admin :
    admin, resultats, commun, users, etc
    """
    import admin, utils_export

    #########################################################"
    # passage de la base admin à la version 9 (2017-09-21)
    # passage de la base admin à la version 10 (2018-11-28)
    #########################################################"
    versionDB_admin = utils_db.readVersionDB(admin.db_admin)
    utils_functions.myPrint('versionDB_admin : ', versionDB_admin)
    if versionDB_admin < 9:
        try:
            utils_functions.myPrint('mise a jour db_admin')
            utils_functions.doWaitCursor()
            query_admin, transactionOK = utils_db.queryTransactionDB(
                admin.db_admin)
            # ------------------------------------------------------------
            # on recrée la table eleves
            # ------------------------------------------------------------
            # récupération de la table dans une liste :
            newTable = []
            commandLine = utils_db.q_selectAllFrom.format('eleves')
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            while query_admin.next():
                id_eleve = int(query_admin.value(0))
                num = query_admin.value(1)
                nom = query_admin.value(2)
                prenom = query_admin.value(3)
                classe = query_admin.value(4)
                login = query_admin.value(5)
                dateNaissance = query_admin.value(6)
                mdp = query_admin.value(7)
                anDernier = query_admin.value(8)
                eleve_id = query_admin.value(9)
                dateEntree = query_admin.value(10)
                dateSortie = query_admin.value(11)
                sexe = -1
                data = (
                    id_eleve, num, nom, prenom, classe, login, dateNaissance, mdp, 
                    anDernier, eleve_id, dateEntree, dateSortie, sexe)
                utils_functions.appendUnique(newTable, data)
            # on efface et recrée la table :
            commandLine = utils_db.q_dropTable.format('eleves')
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            commandLine = utils_db.qct_admin_eleves
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            commandLine = utils_db.insertInto('eleves', 13)
            for data in newTable:
                query_admin.prepare(commandLine)
                for i in range(13):
                    query_admin.addBindValue(data[i])
                query_admin.exec_()
            # on exporte en csv :
            for table in ('eleves', ):
                fileName = utils_functions.u('{0}/admin_tables/{1}.csv').format(admin.csvDir, table)
                utils_export.exportTable2Csv(
                    main, admin.db_admin, table, fileName, msgFin=False)
            # on met à jour le numéro de version :
            utils_db.changeInConfigTable(
                main, admin.db_admin, {'versionDB': (9, '')})
        finally:
            utils_db.endTransaction(
                query_admin, admin.db_admin, transactionOK)
            utils_filesdirs.removeAndCopy(admin.dbFileTemp_admin, admin.dbFile_admin)
            utils_functions.restoreCursor()
    if versionDB_admin < 10:
        try:
            utils_functions.myPrint('mise a jour db_admin')
            utils_functions.doWaitCursor()
            query_admin, transactionOK = utils_db.queryTransactionDB(
                admin.db_admin)
            # ------------------------------------------------------------
            # on recrée la table config_calculs
            # ------------------------------------------------------------
            # récupération de la table dans une liste :
            newTable = []
            commandLine = utils_db.q_selectAllFrom.format('config_calculs')
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            while query_admin.next():
                key_name = query_admin.value(0)
                value_int = int(query_admin.value(1))
                value_text = query_admin.value(2)
                key_name = key_name.replace('ProtectResult_', 'ProtectedPeriod_')
                key_name = key_name.replace('SelectedPeriode', 'SelectedPeriod')
                data = (key_name, value_int, value_text)
                newTable.append(data)
            # on efface et recrée la table :
            commandLine = utils_db.qdf_table.format('config_calculs')
            query_admin = utils_db.queryExecute(commandLine, query=query_admin)
            commandLine = utils_db.insertInto('config_calculs', 3)
            for data in newTable:
                query_admin.prepare(commandLine)
                for i in range(3):
                    query_admin.addBindValue(data[i])
                query_admin.exec_()
            # on met à jour le numéro de version :
            utils_db.changeInConfigTable(
                main, admin.db_admin, {'versionDB': (10, '')})
        finally:
            utils_db.endTransaction(
                query_admin, admin.db_admin, transactionOK)
            utils_filesdirs.removeAndCopy(admin.dbFileTemp_admin, admin.dbFile_admin)
            utils_functions.restoreCursor()



    ################################################################"
    # passage de la base recup_evals à la version 9 (2016-07-19)
    # passage de la base recup_evals à la version 10 (2016-10-29)
    ################################################################"
    admin.openDB(main, 'recup_evals')
    versionDB_recupEvals = utils_db.readVersionDB(admin.db_recupEvals)
    utils_functions.myPrint('versionDB_recupEvals : ', versionDB_recupEvals)
    if versionDB_recupEvals < 9:
        try:
            utils_functions.myPrint('mise a jour db_recupEvals')
            utils_functions.doWaitCursor()
            query_recupEvals, transactionOK = utils_db.queryTransactionDB(
                admin.db_recupEvals)
            # ------------------------------------------------------------
            # on recrée la table absences
            # ------------------------------------------------------------
            # récupération de la table dans une liste :
            newTable = []
            commandLine = utils_db.q_selectAllFrom.format('absences')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            while query_recupEvals.next():
                id_prof = int(query_recupEvals.value(0))
                id_eleve = int(query_recupEvals.value(1))
                Periode = int(query_recupEvals.value(2))
                abs0 = int(query_recupEvals.value(3))
                abs1 = int(query_recupEvals.value(4))
                data = (id_prof, id_eleve, Periode, abs0, abs1, 0, 0)
                utils_functions.appendUnique(newTable, data)
            # on efface et recrée la table :
            commandLine = utils_db.q_dropTable.format('absences')
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            commandLine = utils_db.qct_recupEvals_absences
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            commandLine = utils_db.insertInto('absences', 7)
            for data in newTable:
                query_recupEvals.prepare(commandLine)
                for i in range(7):
                    query_recupEvals.addBindValue(data[i])
                query_recupEvals.exec_()
            # ------------------------------------------------------------
            # on met à jour le numéro de version :
            # ------------------------------------------------------------
            utils_db.changeInConfigTable(
                main, admin.db_recupEvals, {'versionDB': (9, '')})
        finally:
            utils_db.endTransaction(
                query_recupEvals, admin.db_recupEvals, transactionOK)
            utils_filesdirs.removeAndCopy(
                admin.dbFileTemp_recupEvals, admin.dbFile_recupEvals)
            utils_functions.restoreCursor()
    if versionDB_recupEvals < 10:
        try:
            utils_functions.myPrint('mise a jour db_recupEvals')
            utils_functions.doWaitCursor()
            query_recupEvals, transactionOK = utils_db.queryTransactionDB(
                admin.db_recupEvals)
            # ------------------------------------------------------------
            # on crée la table lsu
            # ------------------------------------------------------------
            commandLine = utils_db.qct_recupEvals_lsu
            query_recupEvals = utils_db.queryExecute(commandLine, query=query_recupEvals)
            # ------------------------------------------------------------
            # on met à jour le numéro de version :
            # ------------------------------------------------------------
            utils_db.changeInConfigTable(
                main, admin.db_recupEvals, {'versionDB': (10, '')})
        finally:
            utils_db.endTransaction(
                query_recupEvals, admin.db_recupEvals, transactionOK)
            utils_filesdirs.removeAndCopy(
                admin.dbFileTemp_recupEvals, admin.dbFile_recupEvals)
            utils_functions.restoreCursor()



    ################################################################"
    # passage de la base referential_propositions à la version 2 (2015-07-07)
    ################################################################"
    """
    admin.openDB(main, 'referential_propositions')
    versionDB_referentialPropositions = utils_db.readVersionDB(
        admin.db_referentialPropositions)
    utils_functions.myPrint(
        'versionDB_referentialPropositions : ', versionDB_referentialPropositions)
    if versionDB_referentialPropositions < 2:
        try:
            utils_functions.myPrint('mise a jour db_referentialPropositions')
            utils_functions.doWaitCursor()
            query_referentialPropositions, transactionOK = utils_db.queryTransactionDB(
                admin.db_referentialPropositions)
            # ------------------------------------------------------------
            # on supprime les tables global_... et bilans_validations
            # ------------------------------------------------------------
            commandLine = utils_db.q_dropTable.format('global_validations')
            query_referentialPropositions = utils_db.queryExecute(commandLine, query=query_referentialPropositions)
            commandLine = utils_db.q_dropTable.format('global_propositions')
            query_referentialPropositions = utils_db.queryExecute(commandLine, query=query_referentialPropositions)
            commandLine = utils_db.q_dropTable.format('bilans_validations')
            query_referentialPropositions = utils_db.queryExecute(commandLine, query=query_referentialPropositions)

            # on met à jour le numéro de version :
            utils_db.changeInConfigTable(
                main, admin.db_referentialPropositions, {'versionDB': (2, '')})
        finally:
            utils_db.endTransaction(
                query_referentialPropositions, 
                admin.db_referentialPropositions, 
                transactionOK)
            utils_filesdirs.removeAndCopy(
                admin.dbFileTemp_referentialPropositions, 
                admin.dbFile_referentialPropositions)
            utils_functions.restoreCursor()
    """



    #########################################################"
    # passage de la base commun à la version 8 (2016-10-25)
    # passage de la base commun à la version 9 (2017-03-26)
    # passage de la base commun à la version 10 (2018-06-30)
    #########################################################"
    versionDB_commun = utils_db.readVersionDB(main.db_commun)
    utils_functions.myPrint('versionDB_commun : ', versionDB_commun)
    if versionDB_commun < 8:
        # création du progressDialog :
        title = utils_functions.u('{0} : {1} {2}').format(utils.PROGNAME, 
            QtWidgets.QApplication.translate('main', 'DataBase Upgrade'),
            'commun')
        progressDialog = utils_functions.initProgressDialog(main, title, 2)
        try:
            utils_functions.myPrint('mise a jour db_commun')
            utils_functions.doWaitCursor()
            query_commun, transactionOK = utils_db.queryTransactionDB(main.db_commun)

            # on crée la table lsu :
            commandLine = utils_db.listTablesCommun['lsu'][0]
            query_commun = utils_db.queryExecute(commandLine, query=query_commun)
 
            utils_db.changeInConfigTable(main, main.db_commun, {'versionDB': (8, '')})
            # on exporte en csv :
            for table in ('lsu', ):
                fileName = utils_functions.u('{0}/commun_tables/{1}.csv').format(admin.csvDir, table)
                utils_export.exportTable2Csv(main, main.db_commun, table, fileName, msgFin=False)

        finally:
            utils_db.endTransaction(query_commun, main.db_commun, transactionOK)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, admin.dbFileFtp_commun)
            utils_functions.restoreCursor()
            utils_functions.endProgressDialog(main, progressDialog)
            if utils.NOGUI:
                admin.uploadDB(main, db='commun', msgFin=False)
            else:
                admin.mustUploadDBs(main, databases=['commun'], msgFin=False)
    if versionDB_commun < 9:
        # création du progressDialog :
        title = utils_functions.u('{0} : {1} {2}').format(utils.PROGNAME, 
            QtWidgets.QApplication.translate('main', 'DataBase Upgrade'),
            'commun')
        progressDialog = utils_functions.initProgressDialog(main, title, 2)
        try:
            utils_functions.myPrint('mise a jour db_commun')
            utils_functions.doWaitCursor()
            query_commun, transactionOK = utils_db.queryTransactionDB(main.db_commun)

            # ------------------------------------------------------------
            # on ajoute la matière PAR_LSU à la table matieres
            # ------------------------------------------------------------
            id_matiere = 0
            commandLine = utils_db.q_selectAllFromOrder.format('matieres', 'id')
            query_commun = utils_db.queryExecute(commandLine, query=query_commun)
            while query_commun.next():
                id_matiere = int(query_commun.value(0))
            id_matiere += 1
            commandLine = utils_db.insertInto('matieres', 6)
            query_commun.prepare(commandLine)
            query_commun.addBindValue(id_matiere)
            query_commun.addBindValue(utils.LSU['TRANSLATIONS']['PAR_LSU'])
            query_commun.addBindValue('PAR_LSU')
            query_commun.addBindValue(utils.LSU['TRANSLATIONS']['PAR_LSU'])
            query_commun.addBindValue(22)
            query_commun.addBindValue('')
            query_commun.exec_()
            # ------------------------------------------------------------
            # on ajoute les 4 parcours à la table lsu
            # ------------------------------------------------------------
            lines = []
            for parcours in utils.LSU['PARCOURS']:
                lines.append((
                    'parcours', '', parcours, utils.LSU['TRANSLATIONS'][parcours], '', '', '', '', ''))
            commandLine = utils_db.insertInto('lsu', 9)
            for data in lines:
                query_commun.prepare(commandLine)
                for i in range(9):
                    query_commun.addBindValue(data[i])
                query_commun.exec_()
 
            utils_db.changeInConfigTable(main, main.db_commun, {'versionDB': (9, '')})
            # on exporte en csv :
            for table in ('matieres', 'lsu', ):
                fileName = utils_functions.u('{0}/commun_tables/{1}.csv').format(admin.csvDir, table)
                utils_export.exportTable2Csv(main, main.db_commun, table, fileName, msgFin=False)

        finally:
            utils_db.endTransaction(query_commun, main.db_commun, transactionOK)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, admin.dbFileFtp_commun)
            utils_functions.restoreCursor()
            utils_functions.endProgressDialog(main, progressDialog)
            if utils.NOGUI:
                admin.uploadDB(main, db='commun', msgFin=False)
            else:
                admin.mustUploadDBs(main, databases=['commun'], msgFin=False)
    if versionDB_commun < 10:
        # création du progressDialog :
        title = utils_functions.u('{0} : {1} {2}').format(utils.PROGNAME, 
            QtWidgets.QApplication.translate('main', 'DataBase Upgrade'),
            'commun')
        progressDialog = utils_functions.initProgressDialog(main, title, 2)
        try:
            utils_functions.myPrint('mise a jour db_commun')
            utils_functions.doWaitCursor()
            query_commun, transactionOK = utils_db.queryTransactionDB(main.db_commun)

            # ------------------------------------------------------------
            # on supprime la table equivalences
            # ------------------------------------------------------------
            commandLine = utils_db.q_dropTable.format('equivalences')
            query_commun = utils_db.queryExecute(commandLine, query=query_commun)
            # ------------------------------------------------------------
            # on crée la table bilans_liens
            # ------------------------------------------------------------
            commandLine = utils_db.listTablesCommun['bilans_liens'][0]
            query_commun = utils_db.queryExecute(commandLine, query=query_commun)
 
            utils_db.changeInConfigTable(main, main.db_commun, {'versionDB': (10, '')})
            # on exporte en csv :
            for table in ('bilans_liens', ):
                fileName = utils_functions.u('{0}/commun_tables/{1}.csv').format(admin.csvDir, table)
                utils_export.exportTable2Csv(main, main.db_commun, table, fileName, msgFin=False)

        finally:
            utils_db.endTransaction(query_commun, main.db_commun, transactionOK)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, main.dbFile_commun)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_commun, admin.dbFileFtp_commun)
            utils_functions.restoreCursor()
            utils_functions.endProgressDialog(main, progressDialog)
            if utils.NOGUI:
                admin.uploadDB(main, db='commun', msgFin=False)
            else:
                admin.mustUploadDBs(main, databases=['commun'], msgFin=False)



    #########################################################"
    # passage de la base users à la version 5 (2017-09-216)
    #########################################################"
    versionDB_users = utils_db.readVersionDB(main.db_users)
    utils_functions.myPrint('versionDB_users : ', versionDB_users)
    if versionDB_users < 5:
        # création du progressDialog :
        admin.downloadDB(main, msgFin=False)
        title = utils_functions.u('{0} : {1} {2}').format(utils.PROGNAME, 
            QtWidgets.QApplication.translate('main', 'DataBase Upgrade'),
            'users')
        progressDialog = utils_functions.initProgressDialog(main, title, 2)
        try:
            utils_functions.myPrint('mise a jour db_users')
            utils_functions.doWaitCursor()
            query_users, transactionOK = utils_db.queryTransactionDB(main.db_users)

            # ------------------------------------------------------------
            # on recrée la table eleves
            # ------------------------------------------------------------
            # récupération de la table dans une liste :
            newTable = []
            commandLine = utils_db.q_selectAllFrom.format('eleves')
            query_users = utils_db.queryExecute(commandLine, query=query_users)
            while query_users.next():
                id_eleve = int(query_users.value(0))
                nom = query_users.value(1)
                prenom = query_users.value(2)
                classe = query_users.value(3)
                login = query_users.value(4)
                mdp = query_users.value(5)
                initialMdp = query_users.value(6)
                sexe = -1
                dateNaissance = ''
                data = (
                    id_eleve, nom, prenom, classe, 
                    login, mdp, initialMdp, 
                    sexe, dateNaissance)
                utils_functions.appendUnique(newTable, data)
            # on efface et recrée la table :
            commandLine = utils_db.q_dropTable.format('eleves')
            query_users = utils_db.queryExecute(commandLine, query=query_users)
            commandLine = utils_db.qct_users_eleves
            query_users = utils_db.queryExecute(commandLine, query=query_users)
            commandLine = utils_db.insertInto('eleves', 9)
            for data in newTable:
                query_users.prepare(commandLine)
                for i in range(9):
                    query_users.addBindValue(data[i])
                query_users.exec_()
            # on met à jour le numéro de version :
            utils_db.changeInConfigTable(
                main, main.db_users, {'versionDB': (5, '')})

        finally:
            utils_db.endTransaction(query_users, main.db_users, transactionOK)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_users, main.dbFile_users)
            utils_filesdirs.removeAndCopy(main.dbFileTemp_users, admin.dbFileFtp_users)
            utils_functions.restoreCursor()
            utils_functions.endProgressDialog(main, progressDialog)
            if utils.NOGUI:
                admin.uploadDB(main, db='users', msgFin=False)
            else:
                admin.mustUploadDBs(main, databases=['users'], msgFin=False)




    #########################################################"
    # DIVERS (temporaire en général)
    #########################################################"

    # vérification du fichier verac_admin/ftp/secret/verac/.htaccess (janvier 2016)
    """
    destFile = utils_functions.u('{0}/.htaccess').format(admin.dirLocalPrive)
    if not(QtCore.QFile(destFile).exists()):
        originFile = utils_functions.u('{0}/_private/.htaccess').format(admin.dirLocalPublic)
        copyOK = QtCore.QFile(originFile).copy(destFile)
        if copyOK:
            import admin_ftp
            dirSite = admin.dirSitePrive
            dirLocal = admin.dirLocalPrive
            admin_ftp.ftpPutFile(main, dirSite, dirLocal, '.htaccess')
    """

    # création du dossier verac_admin/ftp/secret/verac/protected/photos/ (janvier 2016)
    """
    destDir = QtCore.QDir(utils_functions.u('{0}/protected/photos').format(admin.dirLocalPrive))
    if not(destDir.exists()):
        QtCore.QDir(utils_functions.u('{0}/protected').format(admin.dirLocalPrive)).mkdir('photos')
    """









