Pour créer des bulletins (ou autres relevés) à partir de modèles html, **VÉRAC** a besoin d'utiliser **wkhtmltopdf**.  
wkhtmltopdf doit donc être détecté.

Vous pouvez vérifier ici si wkhtmltopdf est opérationnel, et faire un test de création de fichier pdf.
