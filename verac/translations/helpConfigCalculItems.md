**Confirm an item in the absence:**  
Choose the maximum percentage allowed of absence **and / or** the minimum number of assessments for the item to be taken into account.

Difference **AND** and **OR**:  
* **AND**: 2 conditions must be met (number and %) for the item account; otherwise it is X which is guarded
* **OR**: just one of the two conditions for this account.
