# VÉRAC

#### Vers une Évaluation Réussie Avec les Compétences

* **Website:** https://verac.tuxfamily.org
* **Email:** verac at tuxfamily.org
* **License:** GNU General Public License (version 3)
* **Copyright:** (c) 2009-2022 VÉRAC authors
