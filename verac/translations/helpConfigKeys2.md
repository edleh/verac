You can choose above the letters which will be used in the interface of **VÉRAC**, as well as their names and colors.

If you are administrator, your choices will be registered in the base **commun**.  
It will thus have to be placed at the disposal of the users after any modification  
(menu **Administration → DbsMenu → UploadDBCommun**).
