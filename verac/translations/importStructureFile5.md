You can consult below the list of tables templates that will be imported.

If some of them already exist in your current structure, they will not be imported.

If in doubt you will be asked what to do in the last step.

You can skip to the last step in checking the box at the bottom right.
