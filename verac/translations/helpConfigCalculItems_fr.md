**Valider un item en cas d'absence :**  
Choisissez le pourcentage maximal autorisé d'absence **et / ou** le nombre minimum d'évaluations réalisées pour que l'item soit pris en compte.

Différence entre **ET** et **OU** :
* **ET** : les 2 conditions doivent être remplies (% et nombre) pour que l'item compte ; sinon c'est X qui est gardé
* **OU** : il suffit de l'une des 2 conditions pour que ça compte.
