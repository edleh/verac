Un fichier de structure vous permet d'exporter une partie de votre structure (items, bilans, tableaux, etc).  
Il ne contient pas ce qui est lié à vos évaluations (groupes, etc).

Un fichier de structure peut être échangé avec un collègue ou mis à disposition sur le site de votre établissement.  
C'est un moyen pratique pour aider les nouveaux collègues de votre établissement.

Cet assistant va vous guider étape par étape dans le processus de fabrication d'un tel fichier.

* **Étape 1** – sélection des items
* **Étape 2** – sélection des bilans
* **Étape 3** – sélection d'un profil par défaut (pour les bulletins)
* **Étape 4** – sélection des modèles de tableaux
* **Étape 5** – sélection des comptages
* **Étape 6** – matière, titre et description



Pour terminer il vous faudra choisir le nom et l'emplacement du fichier à créer.

Vous pouvez aussi ouvrir et modifier un fichier de structure existant à l'aide du bouton ci-dessous.
