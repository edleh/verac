You can consult below the list of items that will be imported.

If some of them already exist in your current structure, they will not be imported.

If in doubt (for example if an existing item already has the same name but a different description) you will be asked what to do in the last step.

You can skip to the last step in checking the box at the bottom right.
