The first two steps are used to configure the files to be created.

The last step lets you start the procedure.

Click the **Help** button to display the detailed help in your browser.
