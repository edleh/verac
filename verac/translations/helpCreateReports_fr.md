Les deux premières étapes servent à configurer les fichiers à créer.

La dernière étape permet de lancer la procédure.

Cliquez sur le bouton **Aide** pour afficher l'aide détaillée dans votre navigateur.
