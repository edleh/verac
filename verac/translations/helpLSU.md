The first tabs are used to verify the data required for the export to LSU.

The last tab lets you start the procedure.

Click the **Help** button to display the detailed help in your browser.
