When a **balance is assessed by several professors** (shared part of the newsletter, repository, ...), you can define how **VÉRAC** attributes the final level of the student.

Just set the **maximum** percentages (condition **smaller than**) and **minimum** (condition **greater than**) authorized for each class indicated.

**VÉRAC** testing conditions one after the other:
* It verifies that the **student was evaluated enough times** so that the balance is representative  
(Minimum number of evaluations or maximum percentage of X)
* Then tests the condition of the **best level of acquisition** (green)
* Then, it tests that the good level **less** (red) using the same rules (but transposed)
* If neither of these conditions is not performed, then tests the condition of the **upper intermediate level** (yellow)
* If any condition is met, then the student is in **middle level less** (orange).
