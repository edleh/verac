A structure file may contain
* items
* balances
* tables templates
* a default profile (for bulletins)
* count.


This allows you to recover at your disposal by a colleague structure.

Start by choosing between three options provided below
* open a structure file is on your computer
* download an available structure on your school website
* download an available structure on VÉRAC website.
