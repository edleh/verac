Les premiers onglets servent à vérifier les données nécessaires à l'export vers LSU.

Le dernier onglet permet de lancer la procédure.

Cliquez sur le bouton **Aide** pour afficher l'aide détaillée dans votre navigateur.
