Le numéro de l'année scolaire doit correspondre à la fin de celle-ci (par exemple 2017 pour 2016-2017).

Vous pouvez corriger la valeur ci-dessus si elle n'est pas correcte.
