# La structure est prête à être importée dans votre base de données.


Récapitulatif des modifications qui seront effectuées :
* nouveaux items : {0}
* items modifiés : {1}
* nouveaux bilans : {2}
* bilans modifiés : {3}
* nouveaux modèles de tableaux : {4}
* modèles de tableaux modifiés : {5}
* nouveaux comptages : {6}
* comptages modifiés : {7}
