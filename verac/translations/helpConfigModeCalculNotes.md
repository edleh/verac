Choose in the combobox the way of calculating of your notes.  
Each of the four levels of evaluation corresponds to a coefficient expressed as a percentage.

* **Default**: levels are uniformly distributed between 0 and 100
* **As DNB**: levels are divided as to the assessment of the common core national certificate  
(see here: http://www.education.gouv.fr/cid2619/le-diplome-national-du-brevet.html)
* **Custom**: define yourself coefficients.

You can also set the number of decimal places for notes.










