Sélectionnez les comptages que vous voulez exporter en les déplaçant dans la liste située à droite.

Utilisez les touches Ctrl et Shift du clavier pour réaliser une sélection multiple.
