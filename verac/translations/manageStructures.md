The list below shows the structures files in the subfolder **verac_admin/ftp/secret/verac/protected/structures**.

To change the list of files:
* open the subfolder **structures** and place your structures files (or delete those that are obsolete)
* reload the list
* you can also re-order (with the mouse)and add separators
* double-click a separator to change the text
* validate.

Your files will then be posted on the website of your school and teachers can download them.
