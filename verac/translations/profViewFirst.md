## It seems that you launch VÉRAC for the first time.

You can:
* start by creating a **group** ("Assessments → Manage groups of students" menu)
* consult online help, and in particular the **[Catch in fast hand](https://verac.tuxfamily.org/site/help-prof-prise-en-main-rapide)**
* download your database if you just changed computer ("File → DownloadDBMy" menu).
