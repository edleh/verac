Lorsqu'un **bilan regroupe plusieurs items**, vous pouvez définir comment **VÉRAC** attribue le niveau final à l'élève.

Il suffit de définir les pourcentages **maximaux** (condition **plus petit que**) et **minimaux** (condition **plus grand que**) autorisés pour chaque catégorie indiquée.

**VÉRAC** teste les conditions les unes après les autres :
* il vérifie que **l'élève a été évalué suffisamment de fois** pour que le bilan soit représentatif (pourcentage maximal de X),
* puis il teste la condition du **meilleur niveau d'acquisition** (vert)
* ensuite, il teste celle du **moins bon niveau** (rouge) en utilisant les mêmes régles (mais transposées)
* si aucune de ces deux conditions n'est réalisée, il teste alors la condition du **niveau intermédiaire supérieur** (jaune)
* si aucune condition n'est remplie, l'élève se situe alors en **niveau intermédiaire inférieur** (orange).
