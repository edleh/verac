Si vous cocher cette case, les détails des évaluations des professeurs seront récupérés par **VÉRAC**.

Cela affichera dans les bulletins (et autres relevés) les items ayant participé au calcul de chaque bilan.  
Cela donnera des bulletins plus détaillés, mais bien plus longs. Le temps de calcul aussi sera plus long.

Si vous n'avez pas besoin de cette fonctionnalité, il vaut mieux décocher cette case.
