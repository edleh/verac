### Gestion de l'AP impossible

La matière AP_LSU doit être créée afin de rendre cette gestion disponible.

Voir la page d'aide : [export vers LSU (Livret Scolaire Unique)](https://verac.tuxfamily.org/site/help-admin-export-lsu.html#matieres).





