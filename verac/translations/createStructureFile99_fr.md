Sélectionnez la **matière** et donnez un **titre** clair à votre structure ; ce titre sera affiché dans la liste des structures disponibles.

Enfin, donnez une **description** la plus complète possible de la structure ; elle permettra aux collègues de comprendre son organisation.  
Vous pouvez mettre en forme le texte de la description.
