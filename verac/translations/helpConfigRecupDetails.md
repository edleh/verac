If you check this box, the details of the evaluations of teachers will be recovered by **VÉRAC**.

This will display in newsletters (and other statements) items involved in calculating each balance sheet.  
This will give more detailed bulletins, but much longer. The calculation time also will be longer.

If you do not need this feature, it is better to uncheck this box.
