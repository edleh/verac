Un fichier de structure peut contenir
* des items
* des bilans
* des modèles de tableaux
* un profil par défaut (pour les bulletins)
* des comptages.


Cela vous permet de récupérer une structure mise à votre disposition par un collègue.

Commencez par choisir entre les 3 possibilités proposées ci-dessous
* ouvrir un fichier de structure qui est sur votre ordinateur
* télécharger une structure disponible sur le site web de votre établissement
* télécharger une structure disponible sur le site web de VÉRAC.
