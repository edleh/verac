SOURCES	     += ../Verac.pyw

SOURCES      += ../libs/main.py

SOURCES      += ../libs/admin.py
SOURCES      += ../libs/admin_calc_results.py
SOURCES      += ../libs/admin_compteur.py
SOURCES      += ../libs/admin_create_report.py
SOURCES      += ../libs/admin_db.py
SOURCES      += ../libs/admin_docs.py
SOURCES      += ../libs/admin_edit.py
SOURCES      += ../libs/admin_photos.py
SOURCES      += ../libs/admin_lsu.py
SOURCES      += ../libs/admin_ftp.py
SOURCES      += ../libs/admin_recup.pyw
SOURCES      += ../libs/verac_scheduler.pyw
SOURCES      += ../libs/admin_structures.py
SOURCES      += ../libs/admin_web.py

SOURCES      += ../libs/prof.py
SOURCES      += ../libs/prof_db.py
SOURCES      += ../libs/prof_groupes.py
SOURCES      += ../libs/prof_itemsbilans.py
SOURCES      += ../libs/prof_suivis.py

SOURCES      += ../libs/utils.py
SOURCES      += ../libs/utils_2lists.py
SOURCES      += ../libs/utils_about.py
SOURCES      += ../libs/utils_config.py
SOURCES      += ../libs/utils_connect.py
SOURCES      += ../libs/utils_csveditor.py
SOURCES      += ../libs/utils_dnb.py
SOURCES      += ../libs/utils_export.py
SOURCES      += ../libs/utils_filesdirs.py
SOURCES      += ../libs/utils_functions.py
SOURCES      += ../libs/utils_help.py
SOURCES      += ../libs/utils_htmleditor.py
SOURCES      += ../libs/utils_structures.py
SOURCES      += ../libs/utils_upgrades.py
SOURCES      += ../libs/utils_export_xml.py
SOURCES      += ../libs/utils_web.py
SOURCES      += ../libs/utils_webengine.py

TRANSLATIONS += ../translations/Verac.ts
TRANSLATIONS += ../translations/Verac_fr.ts
