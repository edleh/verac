Un profil est une liste des bilans qui apparaîtront sur le bulletin pour une matière.  
Dans **VÉRAC**, vous pouvez créer plusieurs profils et attribuer des profils différents à vos groupes (voire à chacun de vos élèves).

Vous pouvez sélectionner ci-dessous un **profil par défaut** qui sera proposé lorsqu'un groupe sera créé.  
Pour cela, sélectionnez les bilans que vous voulez mettre dans ce profil en les déplaçant dans la liste située à droite.
