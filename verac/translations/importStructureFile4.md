You can consult below the list of balances that will be imported.

If some of them already exist in your current structure, they will not be imported.

If in doubt (for example if an existing balance already has the same name but a different description) you will be asked what to do in the last step.

**Color icons:**
* **blue:** personal balance; 
* **light blue:** unrated personal balance; 
* **cyan:** confidential balance; 
* **green:** balance of the shared part of the bulletin; 
* **magenta:** balance of the referential.

You can skip to the last step in checking the box at the bottom right.
