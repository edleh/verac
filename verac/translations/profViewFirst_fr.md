## Il semble que vous lanciez VÉRAC pour la première fois.

Vous pouvez :
* commencer par créer un **groupe** (menu "Évaluations → Gérer les groupes d'élèves")
* consulter l'aide en ligne, et en particulier la **[prise en main rapide](https://verac.tuxfamily.org/site/help-prof-prise-en-main-rapide)**
* télécharger votre base de donnée si vous avez juste changé d'ordinateur (menu "Fichiers → Télécharger la base distante").
