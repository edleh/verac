Select the items you want to export by moving them to the list on the right.

Use the Ctrl and Shift keys to make multiple selections.

**Note: **at Step 4 (tables models selection) items linked to the models you select will be added automatically.
