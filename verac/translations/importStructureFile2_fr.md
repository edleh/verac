Une description détaillée de la structure est affichée ci-dessous.

**Remarque : **il vous est possible de modifier la matière pour laquelle la structure sera importée (cela ne concerne que le profil par défaut si la matière est présente sur les bulletins).

Vous pouvez passer directement à la dernière étape en cochant la case située en bas à droite.
