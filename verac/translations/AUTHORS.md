# Authors of VÉRAC

#### Programming
* Pascal Peter       (2009-2022)
* Romain Ferry       (2010-2012)
* Christophe Labatut (2014)

#### SVG Images
* Sylvain ETIENNE    (2016)

#### Localization
* ...
