# Les outils utilisés pour développer VÉRAC

#### Outils de base
* [Python](https://www.python.org) : langage de programmation
* [Qt](https://www.qt.io) : "toolkit" très complet (interface graphique et tout un tas de choses)
* [PyQt](https://riverbankcomputing.com) : lien entre Python et Qt
* [SQLite](https://www.sqlite.org) : bases de données

#### Autres bibliothèques utilisées et trucs divers
* [ODSlib](https://pypi.python.org/pypi/odslib) : pour manipuler les fichiers ODS
* [PyPDF2](https://github.com/mstamy2/PyPDF2) : pour manipuler les fichiers PDF
* [wkhtmltopdf](https://wkhtmltopdf.org) : pour créer des fichiers pdf depuis les modèles html
* [Simple FTP Mirror](https://git.joonis.de/snippets/3) : pour la mise à jour du site web via FTP
* [pyspellchecker](https://github.com/barrust/pyspellchecker) : pour le correcteur d'orthographe
* [marked](https://github.com/markedjs/marked) : pour afficher les fichiers Markdown

#### Pour l'interface web
* [PHP](https://secure.php.net/) : langage de programmation
* [JavaScript](https://developer.mozilla.org/fr/docs/Web/JavaScript) : langage de programmation
* [Bootstrap](https://getbootstrap.com) : un framework CSS/JS
* [Chart.js](http://www.chartjs.org) : camemberts et radars
* [Sortable](https://github.com/RubaXa/Sortable) : listes réorganisables par drag&drop (trombinoscopes)
* [Fixed-table](https://github.com/kevkan/fixed-table) : tables avec titres et première colonne

#### Logiciels tiers utilisés
* [LibreOffice](https://www.libreoffice.org) : exports divers en ODF
* [Freeplane](https://www.freeplane.org) : exports en cartes heuristiques
* [Blender](https://www.blender.org) : pour la création du logo
* [GIMP](https://www.gimp.org) : manipulation des images

#### Hébergement et site du projet
* [TuxFamily](https://www.tuxfamily.org) : l'hébergement libre pour les gens libres

#### Divers
* [GNU GPL 3](https://www.gnu.org/copyleft/gpl.html) : licence publique générale GNU
