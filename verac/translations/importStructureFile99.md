# The structure is ready to be imported into your database.


Summary of changes to be carried out:
* new items: {0}
* modified items: {1}
* new balances: {2}
* modified balances: {3}
* new tables templates: {4}
* modified tables templates: {5}
* new counts: {6}
* modified counts: {7}
