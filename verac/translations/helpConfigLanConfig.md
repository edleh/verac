If **VÉRAC** is installed in network, simplest is to place the configuration directory (**.Verac**) in a directory **lanConfig** located beside the installation directory. The configuration directory will then automatically be detected.  
The button **"Create the configuration directory"** enables you to create this directory easily.

If not (**VÉRAC** installed on each station), you can define a site network for the directory of configuration, in order to divide it.  
In this case, strip the box above and select the path of the directory **.Verac**.










