Select the balances you want to export by moving them to the list on the right.  
Use the Ctrl and Shift keys to make multiple selections.

**Color icons:**
* **blue:** personal balance; 
* **light blue:** unrated personal balance; 
* **cyan:** confidential balance; 
* **green:** balance of the shared part of the bulletin; 
* **magenta:** balance of the referential.
