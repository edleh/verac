# VÉRAC

#### Vers une Évaluation Réussie Avec les Compétences

* **Site Web :** https://verac.tuxfamily.org
* **Email :** verac at tuxfamily.org
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 2009-2022 VÉRAC auteurs
