Choisissez dans la liste déroulante le mode de calcul de vos notes.  
Chacun des 4 niveaux d'évaluation correspond à un coefficient exprimé en pourcentage.

* **Par défaut** : les niveaux sont répartis régulièrement entre 0 et 100
* **Comme pour le DNB** : les niveaux sont répartis comme pour l'évaluation du socle commun au diplôme national du brevet  
(voir ici : http://www.education.gouv.fr/cid2619/le-diplome-national-du-brevet.html)
* **Personnalisé** : définissez vous-même les coefficients.

Vous pouvez aussi régler le nombre de décimales après la virgule pour les notes.










