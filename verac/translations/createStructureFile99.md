Select the **subject** and give a clear **title** to your structure; this title will be displayed in the list of available structures.

Finally, give as complete a **description** as possible of the structure; it will enable colleagues to understand its organization.  
You can format the text of the description.
