Vous pouvez consulter ci-dessous la liste des modèles de tableaux qui seront importés.

Si certains d'entre eux existent déjà dans votre structure actuelle, ils ne seront pas importés.

En cas de doute il vous sera demandé quoi faire à la dernière étape.

Vous pouvez passer directement à la dernière étape en cochant la case située en bas à droite.
