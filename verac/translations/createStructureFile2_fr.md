Sélectionnez les bilans que vous voulez exporter en les déplaçant dans la liste située à droite.  
Utilisez les touches Ctrl et Shift du clavier pour réaliser une sélection multiple.

**Couleurs des icônes :**
* **bleu :** bilan personnel ; 
* **bleu clair :** bilan personnel non évalué ; 
* **cyan :** bilan confidentiel ; 
* **vert :** bilan de la partie partagée du bulletin ; 
* **magenta :** bilan du référentiel.
