To create newsletters (or other reports) from templates html, **VÉRAC** needs to use **wkhtmltopdf**.  
wkhtmltopdf must be detected.

You can check here if wkhtmltopdf is operational, and do a test for creating PDF files.
