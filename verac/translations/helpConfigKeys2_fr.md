Vous pouvez choisir ci-dessus les lettres qui seront affichées dans l'interface de **VÉRAC**, ainsi que leurs noms et couleurs.

Si vous êtes administrateur, vos choix seront inscrits dans la base **commun**.  
Il faudra donc la mettre à disposition des utilisateurs après toute modification  
(menu **Administration → Bases de données → Poster la base commun**).
