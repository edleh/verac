A structure file allows you to export a part of your structure (items, balances, tables, etc).  
It does not contain that is related to your assessments (groups, etc).

A structure file can be exchanged with a colleague or made available on the website of your school.  
This is a convenient way to help new colleagues at your school.

This wizard will guide you step by step in the manufacturing process of such a file.

* **Step 1** – items selection
* **Step 2** – balances selection
* **Step 3** – default profile selection (for bulletins)
* **Step 4** – tables models selection
* **Step 5** – counts selection
* **Step 6** – subject, title and description



Finally you must choose the name and location of the file to create.

You can also open and edit an existing structure file using the button below.
