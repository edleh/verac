Choisissez dans la liste déroulante le mode de calcul de vos bilans.  
Si vous avez évalué plusieurs fois le même item, en ajoutant toutes ces évaluations bout à bout, vous pouvez choisir ici comment cela sera traité par **VÉRAC** lors des calculs de bilans.

* **Toutes les évaluations** : toutes les évaluations saisies dans vos items seront prises en compte
* **Les meilleures évaluations** : seules les meilleures évaluations seront gardées
* **Les dernières évaluations** : seules les dernières évaluations seront gardées.

Pour les 2 dernières options, renseignez le nombre maximum d'évaluations retenues.  
Si on choisit 0, alors tout est pris en compte.

Si vous souhaitez que les dernières évaluations comptent davantage, sélectionnez l'option :  
**Donner plus d'importance aux dernières évaluations**.  
Cela attribue un coefficient dégressif (1 ; 0,8 ; 0,6 ; 0,4 ; 0,2 ; 0,2 ...) aux évaluations.
