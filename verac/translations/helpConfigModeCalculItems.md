Choose in the combobox the way of calculating of your bilans.  
If you evaluated several times the same one item, by adding all these evaluations end to end, you can choose here how that will be treated by **VÉRAC** at the time as of calculations of bilans.

* **All evaluations**: all the evaluations seized in your items will be taken into account
* **Best evaluations**: only the best evaluations will be kept
* **Last evaluations**: only the last evaluations will be kept.

For the last 2 options, fill the maximum number of selected evaluations.  
If you choose 0, then everything is taken into account.

If you want the latest assessments rely more, select:  
**Give more weight to recent assessments**.  
This assigns a decreasing coefficient (1.0, 0.8, 0.6, 0.4, 0.2, 0.2 ...) assessments.
