<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="2.0">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/prof_suivis.py" line="128"/>
        <source>The file was sent successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="131"/>
        <source>A problem occurred during the transfer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="680"/>
        <source>Choose a Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="693"/>
        <source>Save tar.gz File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="531"/>
        <source>tar.gz files (*.tar.gz)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="706"/>
        <source>copy of the data in the temporary folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="721"/>
        <source>creation of the archive file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="66"/>
        <source>CreateEtab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="74"/>
        <source>If you already have a &lt;b&gt;verac_admin&lt;/b&gt; folder &lt;br/&gt;configured for your school, &lt;br/&gt;click the &lt;b&gt;Continue&lt;/b&gt; button to select it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="245"/>
        <source>Continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="413"/>
        <source>DownloadVeracAdmin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="963"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="862"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="454"/>
        <source>Select the verac_admin directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="469"/>
        <source>Select where you want to create the verac_admin directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="377"/>
        <source>VeracAdminDir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="596"/>
        <source>Do you want to change workdir into admin_verac directory ?&lt;br/&gt; This will avoid to copy your personnal database into it before any action as admin.&lt;p&gt;&lt;b&gt;WARNING : &lt;/b&gt;&lt;/p&gt; workdir is updated for &lt;b&gt;ALL&lt;/b&gt; versions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2752"/>
        <source>ChooseItemsFromBLT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2758"/>
        <source>ChooseItemsFromCPT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2764"/>
        <source>ChooseItemsFromCFD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2944"/>
        <source>Choose competence from bulletin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2948"/>
        <source>Choose competence from referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2952"/>
        <source>Choose a confidential competence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="852"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="384"/>
        <source>Hierarchy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="384"/>
        <source>Competence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="384"/>
        <source>ClassType</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2725"/>
        <source>Create an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2956"/>
        <source>Create a balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3230"/>
        <source>Add a balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="447"/>
        <source>Edit an item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="452"/>
        <source>Edit a balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="477"/>
        <source>Base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="484"/>
        <source>Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2970"/>
        <source>Balance:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2898"/>
        <source>Link several items to a balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="902"/>
        <source>Manage items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3543"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="883"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2755"/>
        <source>ChooseItemsFromBLTStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2761"/>
        <source>ChooseItemsFromCPTStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2767"/>
        <source>ChooseItemsFromCFDStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3134"/>
        <source>Remove a balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="150"/>
        <source>Balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1322"/>
        <source>Manage items and balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1563"/>
        <source>LinkAll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1374"/>
        <source>LinkAllStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1570"/>
        <source>UnlinkAll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1379"/>
        <source>UnlinkAllStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1414"/>
        <source>Links</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1491"/>
        <source>Start to create or delete a link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1498"/>
        <source>Ending create or delete a link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1505"/>
        <source>Abandon the creation or delete link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3308"/>
        <source>Manage advices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3314"/>
        <source>Show Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3319"/>
        <source>Show Balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3336"/>
        <source>Save changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3341"/>
        <source>Erase advice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="472"/>
        <source>Advice:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="663"/>
        <source>Please choose a name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="624"/>
        <source>All the classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1039"/>
        <source>Subject:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="120"/>
        <source>Group:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="712"/>
        <source>Create a table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1022"/>
        <source>Public table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1136"/>
        <source>All subjects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1305"/>
        <source>Clone Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1433"/>
        <source>Copy the items in a table to others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1523"/>
        <source>&lt;p&gt;&lt;/p&gt;&lt;p&gt;The &lt;b&gt;items&lt;/b&gt; from the &lt;b&gt;fist&lt;/b&gt; table&lt;/p&gt;&lt;p&gt;will be recopied into the &lt;b&gt;seconds&lt;/b&gt; tables.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;But no item will be deleted in the &lt;b&gt;seconds&lt;/b&gt;.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1641"/>
        <source>&lt;p&gt;&lt;b&gt;Added {0} items&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1643"/>
        <source>&lt;p&gt;in table: &lt;b&gt;{1}&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1649"/>
        <source>Nothing to add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1669"/>
        <source>&lt;p&gt;All the &lt;b&gt;evaluations&lt;/b&gt; of the &lt;b&gt;fist&lt;/b&gt; table&lt;br/&gt;will be recopied in the &lt;b&gt;second&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;If an item is evaluated in the 2 tables, these evaluations will be added.&lt;br/&gt;The &lt;b&gt;first&lt;/b&gt; table will be declared &lt;b&gt;private&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The two tables must match the &lt;b&gt;same group&lt;/b&gt; of students.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1677"/>
        <source>Merge 2 tables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5162"/>
        <source>tablesCompilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2002"/>
        <source>tablesCompilationStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="237"/>
        <source>public</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="239"/>
        <source>private</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2431"/>
        <source>Choose period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2561"/>
        <source>Modify the list of items (current table)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2701"/>
        <source>Choose the displayed items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2708"/>
        <source>Hidden Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2710"/>
        <source>Visibles Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="714"/>
        <source>Student</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_recup.pyw" line="181"/>
        <source>MESSAGES WINDOW</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="518"/>
        <source>Cannot read file {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="941"/>
        <source>Select the class name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="949"/>
        <source>&lt;p align=&quot;center&quot;&gt;The name of class &lt;b&gt;{0}&lt;/b&gt; is in the file,&lt;br/&gt;but does not exist in your configuration.&lt;/p&gt;&lt;p&gt;Select the right name in the list below &lt;br/&gt; (you can also add this class) :&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="968"/>
        <source>Do not import this student</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="965"/>
        <source>Add this class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="975"/>
        <source>Name of the new class:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2155"/>
        <source>Class type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1013"/>
        <source>Apply to all students from this class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1016"/>
        <source>Apply only to this student</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2438"/>
        <source>NEW STUDENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2439"/>
        <source>DELETED STUDENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2440"/>
        <source>UPDATED STUDENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2558"/>
        <source>NEW TEACHER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2559"/>
        <source>DELETED TEACHER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2560"/>
        <source>UPDATED TEACHER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3436"/>
        <source>Create an archive of the current year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3509"/>
        <source>Cleaning for a new year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3514"/>
        <source>reset periods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3557"/>
        <source>removing obsolete databases</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3635"/>
        <source>removing obsolete local files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3819"/>
        <source>removing obsolete remote files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3982"/>
        <source>Protected period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3985"/>
        <source>Unprotected period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4140"/>
        <source>Create an archive of results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4157"/>
        <source>Sending archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4199"/>
        <source>Sending archive failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="886"/>
        <source>Protected periods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4273"/>
        <source>List of Protected periods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4305"/>
        <source>unprotect the period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4324"/>
        <source>&lt;p&gt;Periode unlocked&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4329"/>
        <source>&lt;p&gt;Periode is already unlocked&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4343"/>
        <source>&lt;p&gt;&lt;b&gt;{0} is locked&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Do you want to unlock this period&lt;br/&gt;before making an update of the results ?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4388"/>
        <source>&lt;p&gt;Periods are locked:&lt;/p&gt;&lt;ul&gt;{0}&lt;/ul&gt;&lt;p&gt;Only periods that are &lt;b&gt;NOT locked&lt;/b&gt; will be updated.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="406"/>
        <source>List of downloaded files profs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="424"/>
        <source>Downloading Teachers files failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="445"/>
        <source>Retrieving files Teachers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="225"/>
        <source>List of teachers whose files were recovered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="236"/>
        <source>List of teachers problematic files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="1850"/>
        <source>Calculation of results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2095"/>
        <source>Recovery of sub-sections of bulletin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2197"/>
        <source>Calculating students assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2291"/>
        <source>Calculating classes assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2431"/>
        <source>Calculating classes synthesis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2595"/>
        <source>The creation of the results failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2617"/>
        <source>Calculation of the referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="807"/>
        <source>Teachers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="595"/>
        <source>Failed to save {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3176"/>
        <source>Upload the database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3205"/>
        <source>Upload of the resultats DB failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4649"/>
        <source>Checking teachers files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10034"/>
        <source>File exists:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10038"/>
        <source>No file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10061"/>
        <source>File date:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10064"/>
        <source>Nothing in file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10069"/>
        <source>Software version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10079"/>
        <source>MUST REPAIR DB PROF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10238"/>
        <source>List of Tables:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="788"/>
        <source>Recovery folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="934"/>
        <source>Updating the folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5031"/>
        <source>Students list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5040"/>
        <source>List:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5041"/>
        <source>Todo:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="5145"/>
        <source>List of teachers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="550"/>
        <source>Files configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="659"/>
        <source>Update the documents table and move PDF files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="598"/>
        <source>Print Management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="600"/>
        <source>Printing on 2 pages (2 pages 1 OR both sides)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="604"/>
        <source>Printing on 4 pages (2 pages 1 AND both sides)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="607"/>
        <source>Printing without adding blank pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5622"/>
        <source>Configuring the html template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5646"/>
        <source>Synthesis model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5690"/>
        <source>No synthesis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5732"/>
        <source>Save html File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5732"/>
        <source>html files (*.html)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10003"/>
        <source>Cannot read file {0}:
{1}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="1836"/>
        <source>Cannot write file {0}:
{1}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1273"/>
        <source>Files Creation (PDF) from a template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2715"/>
        <source>No Data For: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="208"/>
        <source>Classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1808"/>
        <source>ClassesTypes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="323"/>
        <source>Subjects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="956"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4188"/>
        <source>This Id is already in use, choose another.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4216"/>
        <source>This login is already in use, choose another.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3202"/>
        <source>All Subjects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3809"/>
        <source>Id:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3815"/>
        <source>Num:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="460"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3825"/>
        <source>FirstName:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="140"/>
        <source>Login:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1249"/>
        <source>Initial password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1900"/>
        <source>Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1904"/>
        <source>Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2130"/>
        <source>Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="478"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="481"/>
        <source>AdviceEditStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5874"/>
        <source>BaseSiteFtp:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5885"/>
        <source>FTP User:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5915"/>
        <source>DirSitePublic:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5928"/>
        <source>DirSiteSecret:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5941"/>
        <source>WebSiteUrlBase:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5952"/>
        <source>SiteUrlPublic:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="383"/>
        <source>prefixProfFiles:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1053"/>
        <source>Add New Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1080"/>
        <source>Delete the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1088"/>
        <source>Cancel ongoing changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1096"/>
        <source>Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1103"/>
        <source>Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="227"/>
        <source>uploadDBDocuments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="243"/>
        <source>uploadDBDocuments Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="97"/>
        <source>ShowCompteurAnalyze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="239"/>
        <source>Profs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="721"/>
        <source>Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="134"/>
        <source>CompteurAnalyze2ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="422"/>
        <source>ODF Spreadsheet File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="424"/>
        <source>ods files (*.ods)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="733"/>
        <source>Prof</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="334"/>
        <source>Week {0} :
  from <byte value="x9"/>{1}
  to <byte value="x9"/>{2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="430"/>
        <source>visit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="431"/>
        <source>visits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="432"/>
        <source>upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="433"/>
        <source>uploads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5530"/>
        <source>Class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="727"/>
        <source>IpAdress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="729"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="731"/>
        <source>TimeStamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="103"/>
        <source>ConfigDlg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2369"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="113"/>
        <source>All times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="114"/>
        <source>Every 6 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="115"/>
        <source>Every 12 hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="116"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="154"/>
        <source>ChooseTypeSchedule</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="170"/>
        <source>Schools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="184"/>
        <source>Initial time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="252"/>
        <source>ReportDlg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="495"/>
        <source>VERAC is launched</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="62"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="499"/>
        <source>RecoveryIsComplete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="501"/>
        <source>VeracIsClosed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_recup.pyw" line="241"/>
        <source>The recovery went well.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_recup.pyw" line="244"/>
        <source>There was a problem during recovery.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="340"/>
        <source>nowhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="342"/>
        <source>everywhere</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="428"/>
        <source>Nothing for Now.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="426"/>
        <source>Nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="820"/>
        <source>DONE!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="848"/>
        <source>There was a problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="872"/>
        <source>Traceback:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="874"/>
        <source>FileName:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="876"/>
        <source>LineNumber:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="878"/>
        <source>FunctionName:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="880"/>
        <source>Text:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="269"/>
        <source>FOLDERS USED BY VERAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="213"/>
        <source>Open folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="301"/>
        <source>Configuration folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="324"/>
        <source>Temporary folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="339"/>
        <source>Software install folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="978"/>
        <source>YES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="984"/>
        <source>NO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1727"/>
        <source>PDF FILES CREATING SETTING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1732"/>
        <source>wkhtmltopdf found:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1743"/>
        <source>wkhtmltopdf path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1756"/>
        <source>Try creating a PDF file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1871"/>
        <source>LAN SETTING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1877"/>
        <source>Configuration in LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1892"/>
        <source>Select the folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1916"/>
        <source>Working folder in LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1930"/>
        <source>prevent users to change WorkDir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="970"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2173"/>
        <source>Select The .Verac directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2262"/>
        <source>Select The Work directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2342"/>
        <source>OTHER SETTINGS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2348"/>
        <source>LogFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2376"/>
        <source>Collect details of assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="536"/>
        <source>HOW ARE TAKEN INTO ACCOUNT ITEMS FOR CALCULATING THE BALANCES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="542"/>
        <source>&lt;b&gt;Retained assessments:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="548"/>
        <source>All assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="550"/>
        <source>Best assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="552"/>
        <source>Last assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="558"/>
        <source>&lt;b&gt;Maximum number of valuations used:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="566"/>
        <source>&lt;b&gt;Give more weight to the last assessments:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="575"/>
        <source>&lt;b&gt;Maximum percentage of absences to validate the item:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="585"/>
        <source>&lt;b&gt;Minimum number of ratings:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="594"/>
        <source>OR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="596"/>
        <source>AND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="769"/>
        <source>HOW ARE CALCULATED THE BALANCES EVALUATED BY SEVERAL TEACHERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="774"/>
        <source>HOW BALANCES ARE CALCULATED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="799"/>
        <source>Threshold Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="795"/>
        <source>Minimum percentage of items assessed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="779"/>
        <source>&lt;=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="780"/>
        <source>&gt;=</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="783"/>
        <source>Threshold taken into account:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="787"/>
        <source>Assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1199"/>
        <source>USING THE KEYBOARD IN VERAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1219"/>
        <source>Keyboard Keys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1258"/>
        <source>Showing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="92"/>
        <source>Folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="102"/>
        <source>Items calculations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="111"/>
        <source>Balances calculations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="130"/>
        <source>Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="150"/>
        <source>Pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="159"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="949"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="150"/>
        <source>Remember User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="195"/>
        <source>View password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="198"/>
        <source>No internet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1294"/>
        <source>Change password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1348"/>
        <source>The password is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1353"/>
        <source>2 entries are different</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1404"/>
        <source>The password was changed successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1407"/>
        <source>The local password (on this computer) was changed successfully.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1410"/>
        <source>The password change failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1420"/>
        <source>&lt;p&gt;For better security, you must &lt;b&gt;change your password&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;(menu &quot;Utils &gt; Change password&quot;)&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="176"/>
        <source>Failed to print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2155"/>
        <source>Import Canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="2688"/>
        <source>Failed to open {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="355"/>
        <source>Period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2341"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="735"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="235"/>
        <source>No Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="263"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="265"/>
        <source>Index and news</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="273"/>
        <source>Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="275"/>
        <source>Presentation of the administrator interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="284"/>
        <source>Scheduler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="292"/>
        <source>Results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="294"/>
        <source>Calculate student results, create bulletins, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="302"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="304"/>
        <source>Update results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="311"/>
        <source>CreateBilansBLT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="313"/>
        <source>Create bulletins or other reports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="328"/>
        <source>EditModeles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="330"/>
        <source>Edit bulletins models, make new ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="339"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="341"/>
        <source>DocumentsToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="351"/>
        <source>DBToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="360"/>
        <source>DBDirectToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="446"/>
        <source>Enter URL:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="485"/>
        <source>Html Editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="660"/>
        <source>A new version of VERAC is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="734"/>
        <source>Must update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="662"/>
        <source>(&quot;Utils &gt; UpdateVerac&quot; menu)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="736"/>
        <source>What&apos;s new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="697"/>
        <source>A new version of the verac_admin folder is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="699"/>
        <source>Go to the download page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="733"/>
        <source>A new version of the web interface is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="857"/>
        <source>Update Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="971"/>
        <source>You should start again the software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="560"/>
        <source>DataBase Upgrade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="266"/>
        <source>CSV file editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="295"/>
        <source>Insert a row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="300"/>
        <source>Remove a row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="383"/>
        <source>Position : ({0},{1})   Value : {2}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2358"/>
        <source>Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2363"/>
        <source>OneOutOfThree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2365"/>
        <source>Weekly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2367"/>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="979"/>
        <source>CreateMultipleTableaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1009"/>
        <source>CreateMultipleTableauxBaseNameToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="809"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="810"/>
        <source>Yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="811"/>
        <source>Orange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="812"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="813"/>
        <source>Not Rated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7528"/>
        <source>Note calculated from the items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7524"/>
        <source>Note calculated from the balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Label of Item or Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Value of Item or Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7378"/>
        <source>RecalcAllGroupes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7386"/>
        <source>Recalc Groupe : {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1027"/>
        <source>ChooseItems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1029"/>
        <source>ChooseItemsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1051"/>
        <source>Precision of calculated notes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="302"/>
        <source>DownloadProfxxFilesLocal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="347"/>
        <source>DownloadProfxxFilesWeb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="310"/>
        <source>DownloadProfxxFilesLan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1884"/>
        <source>Create the configuration folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2190"/>
        <source>Select The lanConfig parent directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2279"/>
        <source>Select The Teachers files directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1905"/>
        <source>Teachers files folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="667"/>
        <source>Please choose a label.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2905"/>
        <source>ShowMdpProfsNoChange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5211"/>
        <source>no template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2975"/>
        <source>ChooseTemplate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3174"/>
        <source>ManageTemplates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3178"/>
        <source>&lt;p&gt;&lt;b&gt;Templates:&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3216"/>
        <source>&lt;p&gt;&lt;b&gt;LinkedTableaux:&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3222"/>
        <source>&lt;p&gt;&lt;b&gt;Items:&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3186"/>
        <source>NewTemplate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3198"/>
        <source>DeleteTemplate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3204"/>
        <source>EditTemplate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1034"/>
        <source>ChooseTemplateStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3327"/>
        <source>{0}item {1} in tableau {2}
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="158"/>
        <source>NO FILE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="317"/>
        <source>The 2 files have the same date.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="371"/>
        <source>The local file seems most recent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="329"/>
        <source>The distant file seems most recent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2487"/>
        <source>Are you sure you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="658"/>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4609"/>
        <source>password status (requires download of the database users)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4613"/>
        <source>date of prof file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4617"/>
        <source>VERAC version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4621"/>
        <source>prof version of the base (requires download the file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4629"/>
        <source>display the list of tables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="728"/>
        <source>Create or modify the groups of students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="772"/>
        <source>ChooseItemsStatusTip1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="777"/>
        <source>ChooseTemplateStatusTip1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3091"/>
        <source>LinkBilans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3144"/>
        <source>Linked Balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="247"/>
        <source>ListTooOldProfs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="653"/>
        <source>GROUP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2392"/>
        <source>&lt;p&gt;&lt;b&gt;These balances can not be connected!&lt;/b&gt;&lt;/p&gt;&lt;p&gt;There are already connected by a path.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="4086"/>
        <source>&lt;p&gt;&lt;b&gt;Do you want to remove the links between &lt;br/&gt;{0} items and {1} balances?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="4146"/>
        <source>&lt;p&gt;&lt;b&gt;Do you want to remove the links with &lt;br/&gt;{0} other balances?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="388"/>
        <source>Periods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="390"/>
        <source>PeriodsToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="397"/>
        <source>SwitchToNextPeriod</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="399"/>
        <source>SwitchToNextPeriodToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6787"/>
        <source>&lt;p&gt;The viewSuivis has been modified.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Would you upload the changes?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="731"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="732"/>
        <source>Horaire</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="466"/>
        <source>Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="165"/>
        <source>GestElevesSuivis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="1090"/>
        <source>NewLabel2:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="751"/>
        <source>Remark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="452"/>
        <source>This action is disabled in the demo version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="450"/>
        <source>NotInDemo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="177"/>
        <source>DefineSuivis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="178"/>
        <source>ConsultSuivis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="278"/>
        <source>&lt;p&gt;The ElevesSuivis list has been modified.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Would you save the changes?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="183"/>
        <source>SaveDefine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="200"/>
        <source>LoadConsult</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="458"/>
        <source>ALL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="482"/>
        <source>pdf File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="489"/>
        <source>pdf files (*.pdf)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2804"/>
        <source>Save Freeplane File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2807"/>
        <source>mm files (*.mm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2612"/>
        <source>Save csv File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2617"/>
        <source>csv files (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="11091"/>
        <source>Do you want to change all dates?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="219"/>
        <source>INTERNET CONNECTION PROBLEM!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="700"/>
        <source>HelpPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3591"/>
        <source>CreateNote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3589"/>
        <source>EditNote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3618"/>
        <source>CalculatedNote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3621"/>
        <source>ChooseCalculatedNoteModeStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9114"/>
        <source>CalculatedNoteModeItems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9116"/>
        <source>CalculatedNoteModeBilans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6201"/>
        <source>&lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; is not a valid number!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6040"/>
        <source>&lt;b&gt;{0}/{1}&lt;/b&gt;&lt;br/&gt; is not a valid rating!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9151"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5702"/>
        <source>DeleteNote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>standard deviation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>Average score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>mini score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>maxi score:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="479"/>
        <source>Annual Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="617"/>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="619"/>
        <source>Portrait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="622"/>
        <source>Landscape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="639"/>
        <source>Hide the names of students and teachers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="977"/>
        <source>Check out the different settings before continuing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="405"/>
        <source>Academie:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="410"/>
        <source>Departement:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="388"/>
        <source>Adress:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1013"/>
        <source>Document title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1020"/>
        <source>Serie:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1025"/>
        <source>Session:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="395"/>
        <source>Phone:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="439"/>
        <source>This part is still under development.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="437"/>
        <source>Under Development</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1603"/>
        <source>Class with notes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1608"/>
        <source>Check this box if the class must have notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="349"/>
        <source>Administration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="358"/>
        <source>GestDirect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="897"/>
        <source>&lt;p&gt;Names (in file) that does not exist in your &lt;br/&gt;configuration are already joined to name that exists.&lt;/p&gt;&lt;p&gt;Do you wish to reset this ?&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1080"/>
        <source>Select the subject name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1087"/>
        <source>&lt;p align=&quot;center&quot;&gt;The subject name &lt;b&gt;{0}&lt;/b&gt; is in the file,&lt;br/&gt;but does not exist in your configuration.&lt;/p&gt;&lt;p&gt;Select the right name in the list below &lt;br/&gt; (you can also add this subject) :&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1099"/>
        <source>Add this subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1106"/>
        <source>Name of the new subject:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="885"/>
        <source>&lt;p&gt;&lt;b&gt;Classes Names Replacement:&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="890"/>
        <source>&lt;p&gt;&lt;b&gt;Subjects Names Replacement:&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="878"/>
        <source>createReplacement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="895"/>
        <source>&lt;p&gt;&lt;b&gt;Teachers Names Replacement:&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1191"/>
        <source>Select the {0} name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1212"/>
        <source>Add this {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1221"/>
        <source>Name of the new {0}:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="367"/>
        <source>CsvFiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1512"/>
        <source>EditThisItem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1519"/>
        <source>DeleteThisItem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1526"/>
        <source>EditThisBilan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="994"/>
        <source>RemoveThisBilan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1533"/>
        <source>DeleteThisBilan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="145"/>
        <source>Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1548"/>
        <source>DeleteThisLink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1555"/>
        <source>EditLinkCoeff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1540"/>
        <source>ShowLinkedBilansToThis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1606"/>
        <source>ShowAllLinkedBilans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="300"/>
        <source>Website state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="602"/>
        <source>Date of current version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="604"/>
        <source>Date of your version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="606"/>
        <source>Your VERAC version is the last</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="651"/>
        <source>Your verac_admin dir version is the last</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="653"/>
        <source>Your Web interface version is the last</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="205"/>
        <source>Make a bug report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="208"/>
        <source>&lt;p align=&quot;center&quot;&gt;Copy and paste the contents of the log file shown below&lt;br/&gt;or open configuration file.&lt;br/&gt;See the help page for further explanation.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="238"/>
        <source>Open the configuration folder of VERAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="200"/>
        <source>Debugging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1577"/>
        <source>DeleteAll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1392"/>
        <source>DeleteAllStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1598"/>
        <source>Export2Csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1398"/>
        <source>Export2CsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1584"/>
        <source>SelectBilans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1591"/>
        <source>SelectItems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2485"/>
        <source>All items displayed and balances will be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2710"/>
        <source>ItemsBilansTable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3542"/>
        <source>Coeff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10239"/>
        <source>MATIERE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10240"/>
        <source>GROUPE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10241"/>
        <source>PERIODE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="578"/>
        <source>NAME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10243"/>
        <source>ETAT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2820"/>
        <source>Select the assessment you want to export.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2787"/>
        <source>All items related to this assessment will be in the Freeplane file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5438"/>
        <source>ExportBilan2Freeplane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="290"/>
        <source>ChooseBilan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10075"/>
        <source>Version of the teacher DataBase:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1824"/>
        <source>Order of groups and tables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1836"/>
        <source>Tableaux:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="959"/>
        <source>&lt;p&gt;The password you used to log is saved on your computer,&lt;br/&gt;&lt;b&gt;but is not the same as that registered on the website of the school.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Perhaps you have changed the password when your computer&lt;br/&gt;was not connected to the Internet?&lt;/p&gt;&lt;p&gt;To solve this problem, you should:&lt;ul&gt;&lt;li&gt;You can reconnect with the password from the website&lt;/li&gt;&lt;li&gt;Or go to the web interface and change your website password.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3013"/>
        <source>SelectTemplates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3015"/>
        <source>Select the templates you want to export.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6773"/>
        <source>&lt;p&gt;You forgot to select the &lt;b&gt;timing&lt;/b&gt;&lt;br/&gt;of your assessments.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="1881"/>
        <source>Calculating the table: &lt;b&gt;{0}&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3224"/>
        <source>CheckSchoolReportsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5074"/>
        <source>Absences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="54"/>
        <source>All Groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1161"/>
        <source>Edit group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="140"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="137"/>
        <source>Changes the selected group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1038"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; &lt;br/&gt; is already recorded in this group: &lt;b&gt;{1}&lt;/b&gt;. &lt;br/&gt; Subject: &lt;b&gt;{2}&lt;/b&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="698"/>
        <source>&lt;p&gt;After this operation, you cannot&lt;br/&gt; modify any more either evaluations&lt;br/&gt; or appreciations for the students&lt;br/&gt; deleted by the groups.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;It is advised to update your&lt;br/&gt; seizures before beginning.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Do you want to continue&lt;br/&gt; the update of lists?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="719"/>
        <source>&lt;p&gt;Before updating the users group,&lt;br/&gt; you need to download the latest version&lt;br/&gt; (which is the website).&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Would you download the database users&lt;br/&gt; before proceeding?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1158"/>
        <source>Create Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1179"/>
        <source>Group-class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1631"/>
        <source>Please choose a subject.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1660"/>
        <source>This name is already in use. Please choose another.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1687"/>
        <source>CreateMultipleGroupesClasses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1803"/>
        <source>Students removed from the groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4543"/>
        <source>Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="131"/>
        <source>To create a new group of students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>VS note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Name of Item or Balance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="1248"/>
        <source>{0} at {1}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10755"/>
        <source>Opinion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="206"/>
        <source>ExportActualVue2Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="208"/>
        <source>ExportActualVue2PrintStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="212"/>
        <source>ExportActualVue2pdf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="214"/>
        <source>ExportActualVue2pdfStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="218"/>
        <source>ExportAll2ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="221"/>
        <source>ExportAll2odsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1749"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="1022"/>
        <source>Deselect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="1030"/>
        <source>ChangeLabel2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="509"/>
        <source>defines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5739"/>
        <source>DeleteCount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4223"/>
        <source>EditCount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4226"/>
        <source>CreateCount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4258"/>
        <source>Growing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4259"/>
        <source>Decreasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9578"/>
        <source>Growing Sense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9582"/>
        <source>Decreasing Sense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="696"/>
        <source>Delays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9470"/>
        <source>Number of delays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9586"/>
        <source>Average and Standard deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9590"/>
        <source>Median and Quartiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4689"/>
        <source>OrderCounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4691"/>
        <source>OrderNotes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1381"/>
        <source>ShowIntermediateLinks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1613"/>
        <source>ShowIntermediateLinksStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="193"/>
        <source>Chronological</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="195"/>
        <source>ChronologicalStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="802"/>
        <source>Unable to establish a database connection.
({0})

Click Cancel to exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1532"/>
        <source>Would you also post the referential DB?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1705"/>
        <source>&lt;b&gt;Should we remove students who are not in the file?&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;(in general, this is to do that earlier this year)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4328"/>
        <source>FirstName</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4330"/>
        <source>CODE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4331"/>
        <source>SUBJECT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="637"/>
        <source>View Details assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="305"/>
        <source>Groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="798"/>
        <source>You can open the file directly&lt;br/&gt;if the file type is associated with a software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="339"/>
        <source>ActualGroup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="341"/>
        <source>AllGroupsForEachSstudent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="347"/>
        <source>CompetencesOtherShared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="350"/>
        <source>CompetencesPersoBLT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="344"/>
        <source>CompetencesBulletinShared</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10155"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="184"/>
        <source>Empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="185"/>
        <source>&lt;b&gt;FTP connection test:&lt;/b&gt; (Host: {0}, Password: {1}, Login: {2})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="242"/>
        <source>FTP Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="529"/>
        <source>Download the file: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="622"/>
        <source>Upload the file: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3502"/>
        <source>&lt;p&gt;No note in subject:&lt;br/&gt;&lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt;for student:&lt;br/&gt;&lt;b&gt;{1}&lt;/b&gt;.&lt;br/&gt;&lt;br/&gt;Select what to write:&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9919"/>
        <source>Socle3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9922"/>
        <source>NiveauA2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9928"/>
        <source>If a remark is written, it will replace the opinion.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9930"/>
        <source>Remark (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4828"/>
        <source>LIST EMPTY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2270"/>
        <source>Making the list of students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2814"/>
        <source>Creating files:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="144"/>
        <source>VeryFavorable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="145"/>
        <source>Favorable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="146"/>
        <source>MustProve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="147"/>
        <source>Unfavorable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2351"/>
        <source>NO RESULTS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="139"/>
        <source>EMPTY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="801"/>
        <source>VERAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1525"/>
        <source>Click for change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1492"/>
        <source>Select and Press a Key for change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1541"/>
        <source>Select color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="242"/>
        <source>Remove the selected school</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="522"/>
        <source>Recup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="522"/>
        <source>RecupStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="530"/>
        <source>LastReport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="530"/>
        <source>LastReportStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="538"/>
        <source>GotoSiteEtab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="538"/>
        <source>GotoSiteEtabStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="546"/>
        <source>Verac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="546"/>
        <source>VeracStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="554"/>
        <source>ConfigStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="562"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="562"/>
        <source>AboutStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="569"/>
        <source>HelpPageStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="576"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5816"/>
        <source>QuitStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1939"/>
        <source>Use a proxy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1943"/>
        <source>HTTP Proxy:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1947"/>
        <source>Port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="127"/>
        <source>Select the version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="431"/>
        <source>Select a teacher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="226"/>
        <source>Select another folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="154"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="384"/>
        <source>Add a school (internet address)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="387"/>
        <source>Add a school (file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="531"/>
        <source>Open New School File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="575"/>
        <source>Are you certain to want to remove this school: &lt;br/&gt;&lt;b&gt;{0}&lt;b&gt; ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="573"/>
        <source>Remove a school</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1821"/>
        <source>Choose File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1821"/>
        <source>All files (*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="105"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="118"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="113"/>
        <source>License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="133"/>
        <source>About {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="82"/>
        <source>Editor Bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="133"/>
        <source>EditCut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="138"/>
        <source>EditCopy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="143"/>
        <source>EditPaste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="149"/>
        <source>FormatBold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="154"/>
        <source>FormatItalic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="159"/>
        <source>FormatUnderline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="188"/>
        <source>FormatStrikethrough</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="196"/>
        <source>FormatAlignLeft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="203"/>
        <source>FormatAlignCenter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="210"/>
        <source>FormatAlignRight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="217"/>
        <source>FormatAlignJustify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="226"/>
        <source>FormatIncreaseIndent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="234"/>
        <source>FormatDecreaseIndent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="242"/>
        <source>FormatNumberedList</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="250"/>
        <source>FormatBulletedList</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="260"/>
        <source>InsertImage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="266"/>
        <source>CreateLink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="273"/>
        <source>StyleParagraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="279"/>
        <source>StyleHeading1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="285"/>
        <source>StyleHeading2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="291"/>
        <source>StyleHeading3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="297"/>
        <source>StyleHeading4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="323"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="327"/>
        <source>&amp;Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="376"/>
        <source>&amp;Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="280"/>
        <source>Downloading {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="283"/>
        <source>Uploading {0}.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="331"/>
        <source>You should download it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="846"/>
        <source>ClearEditLog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1285"/>
        <source>The persoBase has been modified.
Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1326"/>
        <source>verac_admin (AdminDir)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1329"/>
        <source>files (ProfsFilesDir)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1332"/>
        <source>pdf (PdfFilesDir)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1335"/>
        <source>documents (DocsElevesDir)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1338"/>
        <source>docsprofs (DocsProfsDir)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="497"/>
        <source>Open Database File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="497"/>
        <source>Database files (*.sqlite)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2984"/>
        <source>Open csv File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1091"/>
        <source>Open xml or zip File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1091"/>
        <source>xml or zip files (*.xml *.zip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="478"/>
        <source>All Periodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3816"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5588"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5597"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3982"/>
        <source>UnLockStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3984"/>
        <source>Unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5663"/>
        <source>LockStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5663"/>
        <source>Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3994"/>
        <source>UnShiftStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3996"/>
        <source>UnShift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5672"/>
        <source>ShiftStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5672"/>
        <source>Shift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5681"/>
        <source>Autoselect-no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4015"/>
        <source>Autoselect-1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4019"/>
        <source>Autoselect-2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6822"/>
        <source>FontSize: {0}</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4246"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4442"/>
        <source>OpenAdminDir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4442"/>
        <source>OpenAdminDirStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4451"/>
        <source>LaunchScheduler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4451"/>
        <source>LaunchSchedulerStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="387"/>
        <source>OpenDBStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5556"/>
        <source>EditCsvFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5556"/>
        <source>EditCsvFileStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4482"/>
        <source>CreateAdminTableFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4482"/>
        <source>CreateAdminTableFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4490"/>
        <source>CreateCommunTableFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4490"/>
        <source>CreateCommunTableFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4499"/>
        <source>CreateAllCommunTablesFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4499"/>
        <source>CreateAllCommunTablesFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4562"/>
        <source>Update students from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4562"/>
        <source>Update students from ElevesAvecAdresses.xml file exported from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="832"/>
        <source>Update teachers from STSWeb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="836"/>
        <source>Update teachers from sts_emp_RNE_aaaa.xml file exported from STSWeb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4538"/>
        <source>DownloadDBUsers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4538"/>
        <source>DownloadDBUsersStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4545"/>
        <source>UploadDBUsers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4545"/>
        <source>UploadDBUsersStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2875"/>
        <source>Select competences of the school report list among the proposed models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2885"/>
        <source>Select referential among the proposed models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2894"/>
        <source>Select confidential competences among the proposed models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2904"/>
        <source>Select followed competences among the proposed models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4581"/>
        <source>ShowMdpProfsNoChangeStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4592"/>
        <source>Reset password (teachers)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4592"/>
        <source>Reset password (teachers selected from a list)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4601"/>
        <source>Change password (teachers)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4601"/>
        <source>Change password (teachers selected from a list)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4572"/>
        <source>Reset password (students)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4572"/>
        <source>Reset password (students selected from a list)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4599"/>
        <source>UploadDBCommun</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4599"/>
        <source>UploadDBCommunStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4608"/>
        <source>CreateYearArchiveAndClear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4608"/>
        <source>CreateYearArchiveAndClearStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5883"/>
        <source>CreateYearArchive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5883"/>
        <source>CreateYearArchiveStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4624"/>
        <source>Clears the current year data (databases, files, etc.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4640"/>
        <source>Update results (only new files)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4647"/>
        <source>Update Results (recalculate everything)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4656"/>
        <source>Update results of a selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4656"/>
        <source>Select the teachers and students which we want to update the results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4666"/>
        <source>Calculates the validation proposals of the competences referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4684"/>
        <source>Post the results DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4684"/>
        <source>Post the results DB (and possible proposals to the referential)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4697"/>
        <source>Create reports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4735"/>
        <source>ShowFields</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4735"/>
        <source>ShowFieldsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4742"/>
        <source>Create a school report or other record template</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4742"/>
        <source>Create an HTML template (for a school report, balances report, referential report, etc.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4706"/>
        <source>ExportNotes2ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4706"/>
        <source>ExportNotes2odsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4770"/>
        <source>List files and provides information on their content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4777"/>
        <source>CheckSchoolReports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4791"/>
        <source>RedoMdp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4791"/>
        <source>RedoMdpStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4812"/>
        <source>SwitchToNextPeriodStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4819"/>
        <source>To view and edit the list of protected periods (common and results bases)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4855"/>
        <source>To change what is available on the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4872"/>
        <source>ShowCompteurAnalyzeStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4892"/>
        <source>SaveDBMy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4892"/>
        <source>SaveDBMyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4900"/>
        <source>PostDBMy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4900"/>
        <source>PostDBMyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4907"/>
        <source>SaveAndPostDBMy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4907"/>
        <source>SaveAndPostDBMyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4923"/>
        <source>SaveAsDBMy...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4923"/>
        <source>SaveAsDBMyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4930"/>
        <source>CompareDBMy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4930"/>
        <source>CompareDBMyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4937"/>
        <source>DownloadDBMy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4937"/>
        <source>DownloadDBMyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4944"/>
        <source>PostSuivis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4944"/>
        <source>PostSuivisStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4961"/>
        <source>ChangeMdpStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4975"/>
        <source>CheckCommunBilans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4975"/>
        <source>CheckCommunBilansStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4982"/>
        <source>DownloadSuivisDB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4982"/>
        <source>DownloadSuivisDBStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5861"/>
        <source>TestUpdateVerac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5861"/>
        <source>TestUpdateVeracStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5868"/>
        <source>UpdateVerac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5868"/>
        <source>UpdateVeracStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5875"/>
        <source>Parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4992"/>
        <source>CreateEtabStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4999"/>
        <source>Become administrator of the Personal Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4999"/>
        <source>Allows you to manage the lists of students, edit bulletins, etc.StatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5021"/>
        <source>ShowNewsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5021"/>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5038"/>
        <source>ShowItemsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5042"/>
        <source>ShowStatsTableauStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5042"/>
        <source>StatsTableau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5046"/>
        <source>ShowBilansStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5050"/>
        <source>ShowBulletinStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5050"/>
        <source>Bulletin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5054"/>
        <source>ShowAppreciationsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5054"/>
        <source>Appreciations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5058"/>
        <source>ShowStatsGroupeStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5058"/>
        <source>StatsGroupe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5062"/>
        <source>ShowEleveStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5066"/>
        <source>ShowNotesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7520"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5070"/>
        <source>ShowSuivisStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5074"/>
        <source>ShowAbsencesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5078"/>
        <source>ShowCountsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1118"/>
        <source>Counts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5090"/>
        <source>ShowDNBStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5137"/>
        <source>CreateMultipleGroupesClassesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5152"/>
        <source>ShowDeletedEleves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5152"/>
        <source>ShowDeletedElevesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1799"/>
        <source>Clear students deleted from groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5160"/>
        <source>Clear data of students deleted from groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5168"/>
        <source>GestItemsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5175"/>
        <source>GestItemsBilansStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5182"/>
        <source>ItemsBilansTableStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5189"/>
        <source>LinkItemsToBilanStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5196"/>
        <source>LinkBilansStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5203"/>
        <source>GestAdvicesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5211"/>
        <source>CreateTableauStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5218"/>
        <source>CopyTableau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5218"/>
        <source>CopyTableauStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5225"/>
        <source>SupprTableau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5225"/>
        <source>SupprTableauStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5232"/>
        <source>CopyItemsFromToStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5239"/>
        <source>MergeTableauxStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5246"/>
        <source>SortGroupesTableauxStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5253"/>
        <source>CreateMultipleTableauxStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1115"/>
        <source>Tables templates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5260"/>
        <source>Tables templatesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5267"/>
        <source>CreateTemplateFromTableau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5267"/>
        <source>CreateTemplateFromTableauStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5274"/>
        <source>LinkTableauTemplate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5274"/>
        <source>LinkTableauTemplateStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5281"/>
        <source>UnlinkTableauTemplate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5281"/>
        <source>UnlinkTableauTemplateStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5298"/>
        <source>Change the list of items in the current table (to add, remove, reorder)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1939"/>
        <source>Manage profiles (which will be on the bulletins)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5324"/>
        <source>Create or edit profiles, balance sheets shown in the bulletins, etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5333"/>
        <source>GestElevesSuivisStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5355"/>
        <source>ExportActualVue2ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5355"/>
        <source>ExportActualVue2odsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5362"/>
        <source>ExportActualGroup2ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5362"/>
        <source>ExportActualGroup2odsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5369"/>
        <source>ExportStructure2ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5369"/>
        <source>ExportStructure2odsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5410"/>
        <source>ExportBilansItems2Freeplane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5410"/>
        <source>ExportBilansItems2FreeplaneStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5417"/>
        <source>ExportActualGroup2Freeplane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5417"/>
        <source>ExportActualGroup2FreeplaneStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5424"/>
        <source>ExportAllGroups2Freeplane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5424"/>
        <source>ExportAllGroups2FreeplaneStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5431"/>
        <source>ExportEleves2Freeplane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5431"/>
        <source>ExportEleves2FreeplaneStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5438"/>
        <source>ExportBilan2FreeplaneStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5475"/>
        <source>ExportBilansItems2Csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5475"/>
        <source>ExportBilansItems2CsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5482"/>
        <source>ImportBilansItemsFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5482"/>
        <source>ImportBilansItemsFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5489"/>
        <source>ExportTemplates2Csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5489"/>
        <source>ExportTemplates2CsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5496"/>
        <source>ImportTemplatesFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5496"/>
        <source>ImportTemplatesFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5528"/>
        <source>ExportEleves2Csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5528"/>
        <source>ExportEleves2CsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5535"/>
        <source>ImportElevesFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5535"/>
        <source>ImportElevesFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5542"/>
        <source>ExportReferentiel2Csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5542"/>
        <source>ExportReferentiel2CsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5549"/>
        <source>ImportReferentielFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5549"/>
        <source>ImportReferentielFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6701"/>
        <source>ItemComboBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6703"/>
        <source>ItemComboBoxStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5563"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5563"/>
        <source>UndoStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5571"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5571"/>
        <source>RedoStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5579"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5579"/>
        <source>CutStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5588"/>
        <source>CopyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5597"/>
        <source>PasteStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5606"/>
        <source>SelectAll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5606"/>
        <source>SelectAllStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5614"/>
        <source>Simplify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5614"/>
        <source>Simplifies the selection (items values will be replaced by their average)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5642"/>
        <source>EleveNext</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6781"/>
        <source>EleveNextStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5649"/>
        <source>Recalc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5649"/>
        <source>RecalcStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5656"/>
        <source>RecalcAllGroupesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5681"/>
        <source>AutoselectStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5688"/>
        <source>AddNote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5688"/>
        <source>AddNoteStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5695"/>
        <source>EditNoteStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5702"/>
        <source>DeleteNoteStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5709"/>
        <source>OrderNotesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5717"/>
        <source>CreateCountStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5732"/>
        <source>EditCountStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5739"/>
        <source>DeleteCountStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5746"/>
        <source>OrderCountsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5753"/>
        <source>ValuesUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5753"/>
        <source>ValuesUpStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5760"/>
        <source>ValuesDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5760"/>
        <source>ValuesDownStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5769"/>
        <source>FontSizeMore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5775"/>
        <source>FontSizeLess</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5840"/>
        <source>ProjetPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5840"/>
        <source>ProjetPageStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5891"/>
        <source>Clears the current year data (groups, assessments, appreciations ...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5908"/>
        <source>Create a launcher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5908"/>
        <source>To create a launcher (*.desktop file) in the folder of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5917"/>
        <source>Create a shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5917"/>
        <source>To create a shortcut (*.lnk file) on your Desktop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5927"/>
        <source>BugReportStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5863"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6131"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6605"/>
        <source>OpenADir</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6104"/>
        <source>CreateDbsFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6095"/>
        <source>Utils</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6220"/>
        <source>Sho&amp;w</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6234"/>
        <source>Tableau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6685"/>
        <source>View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="140"/>
        <source>ToolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6257"/>
        <source>MenuBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6301"/>
        <source>E&amp;vals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6323"/>
        <source>GroupesEleves</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6333"/>
        <source>ItemsBilans</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6344"/>
        <source>Tableaux</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6354"/>
        <source>ActualTableau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6403"/>
        <source>&amp;Utils</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6449"/>
        <source>&amp;ImportExport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6479"/>
        <source>ActualView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6512"/>
        <source>Structure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6547"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6584"/>
        <source>All Menus Button</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6608"/>
        <source>OpenADirToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6616"/>
        <source>ChangePeriode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6678"/>
        <source>ChooseGroup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6747"/>
        <source>EleveComboBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6749"/>
        <source>EleveComboBoxStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6755"/>
        <source>EleveSens</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6757"/>
        <source>EleveSensStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Balances &gt; Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Items &gt; Balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Balances &gt; Items + Opinions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Items &gt; Balances + Opinions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="159"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="68"/>
        <source>DNB_MATIERE-FRANCAIS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="69"/>
        <source>DNB_MATIERE-MATHS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="70"/>
        <source>DNB_MATIERE-LV1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="71"/>
        <source>DNB_MATIERE-SVT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="72"/>
        <source>DNB_MATIERE-PHYSIQUE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="73"/>
        <source>DNB_MATIERE-EPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="74"/>
        <source>DNB_MATIERE-ARTS_P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="75"/>
        <source>DNB_MATIERE-MUSIQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="76"/>
        <source>DNB_MATIERE-TECHNO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="77"/>
        <source>DNB_MATIERE-LV2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="78"/>
        <source>DNB_MATIERE-VIE_SCOL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="79"/>
        <source>DNB_MATIERE-OPTION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="83"/>
        <source>DNB_MATIERE-SOCLE_A2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="81"/>
        <source>DNB_MATIERE-HG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="82"/>
        <source>DNB_MATIERE-EDUC_CIVIQUE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="56"/>
        <source>DNB_CODE-AB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="57"/>
        <source>DNB_CODE-DI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="58"/>
        <source>DNB_CODE-NN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="60"/>
        <source>DNB_CODE-MS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="61"/>
        <source>DNB_CODE-ME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="62"/>
        <source>DNB_CODE-MN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="64"/>
        <source>DNB_CODE-VA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="65"/>
        <source>DNB_CODE-NV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="218"/>
        <source>extra points</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="253"/>
        <source>NIVEAU_A2_TEXT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5824"/>
        <source>ContextHelpPage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="867"/>
        <source>Opens contextual help in your browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5833"/>
        <source>HelpPageContents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5833"/>
        <source>HelpPageContentsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="80"/>
        <source>DNB_MATIERE-SOCLE_B2I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="353"/>
        <source>CompetencesPersoOther</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="818"/>
        <source>INCONSISTENT FILE: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5029"/>
        <source>ShowRandomHelpStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5029"/>
        <source>RandomHelp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6077"/>
        <source>WebSite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4863"/>
        <source>Website configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4863"/>
        <source>To update or change the configuration of your website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4841"/>
        <source>Test version of the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4841"/>
        <source>Check for an update of the web site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4849"/>
        <source>Update website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="735"/>
        <source>(&quot;WebSite &gt; Update website&quot; menu)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="981"/>
        <source>UNABLE TO UNTAR THE NEW VERSION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="983"/>
        <source>Do you have the privileges to write into the installation directory?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="986"/>
        <source>You can also download the new version from the &lt;b&gt;{0}&lt;/b&gt; site:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="999"/>
        <source>UNABLE TO DOWNLOAD THE NEW VERSION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3491"/>
        <source>Your data will be re-initialized to begin one New Year&apos;s Day school.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="769"/>
        <source>Are you certain to want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="231"/>
        <source>If you leave this page without clicking&lt;br/&gt;on the button &lt;b&gt;Apply&lt;/b&gt;,&lt;br/&gt;the modifications will not be recorded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="765"/>
        <source>Your data (except substructure) will be re-initialized to begin one New Year&apos;s Day school.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="375"/>
        <source>localDate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="377"/>
        <source>netDate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3208"/>
        <source>{0} period is not archived.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3210"/>
        <source>You should archive this period before selecting the next.&lt;br/&gt;See help automatically displayed in the central part of the interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5830"/>
        <source>ENTERING LOCKED!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1334"/>
        <source>Start again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1332"/>
        <source>The name &lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; is already used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="395"/>
        <source>Before continue:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="396"/>
        <source>Must sure you ave a &lt;b&gt;verac_admin&lt;/b&gt; directory.&lt;br/&gt;You can also download it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1070"/>
        <source>Error, try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="722"/>
        <source>Not Admin of:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3336"/>
        <source>&lt;b&gt;One item to be removed is evaluated&lt;/b&gt; in tables related to this model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3341"/>
        <source>&lt;b&gt;{0} items to be removed are evaluated&lt;/b&gt; in tables related to this model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3345"/>
        <source>If you continue, &lt;b&gt;these evaluations will be removed&lt;/b&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="161"/>
        <source>download verac_web</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="167"/>
        <source>update /verac_admin/ftp/verac</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="193"/>
        <source>update web site via FTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="292"/>
        <source>download:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="307"/>
        <source>upload:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="366"/>
        <source>finished DOWN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="370"/>
        <source>finished UP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="393"/>
        <source>cancel DOWN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="398"/>
        <source>cancel UP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="206"/>
        <source>Processing Summary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="207"/>
        <source>Directories created:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="208"/>
        <source>Directories removed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="209"/>
        <source>Directories total:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="210"/>
        <source>Files created:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="211"/>
        <source>Files updated:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="212"/>
        <source>Files removed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="213"/>
        <source>Files total:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="214"/>
        <source>Bytes transfered:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="215"/>
        <source>Bytes total:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="216"/>
        <source>Time started:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="217"/>
        <source>Time finished:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="218"/>
        <source>Duration:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5992"/>
        <source>Organization</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6013"/>
        <source>Users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="973"/>
        <source>Configuring School</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4554"/>
        <source>General configuration of the school (name, address, ...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4563"/>
        <source>ClassesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4570"/>
        <source>Shared competences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4570"/>
        <source>To change the list of shared competences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="169"/>
        <source>School</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="181"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="98"/>
        <source>Full name of the school:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="102"/>
        <source>SchoolShortName:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3217"/>
        <source>The following database has been changed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3220"/>
        <source>It must be sent to your website.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3222"/>
        <source>Would you upload it now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3225"/>
        <source>The following databases have been modified:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3230"/>
        <source>They must be sent to your website.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3232"/>
        <source>Would you upload them now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4508"/>
        <source>Users management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4508"/>
        <source>To manage the list of users (teachers and students)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2450"/>
        <source>New Record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1047"/>
        <source>OrdreBLT:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="293"/>
        <source>Setting up the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6219"/>
        <source>The last directory&apos;s name must be &lt;b&gt;verac&lt;/&gt;!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6229"/>
        <source>The proposed (before /verac) path is incorrect!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6239"/>
        <source>Do you want to update the website now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6297"/>
        <source>The proposed (before /secret) path is incorrect!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6321"/>
        <source>Do you want to update the secret directory now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="446"/>
        <source>Students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3204"/>
        <source>No Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3848"/>
        <source>Initial&lt;br/&gt;Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3830"/>
        <source>Class:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3841"/>
        <source>Birthday:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3882"/>
        <source>LastYear:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6306"/>
        <source>The secret directory must not be placed in the public directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5864"/>
        <source>Send or update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="90"/>
        <source>Otherwise, fill in the 2 fields below, &lt;br/&gt;then click the &lt;b&gt;DownloadVeracAdmin&lt;/b&gt; button.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="109"/>
        <source>Click &lt;b&gt;Help&lt;/b&gt; button for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2486"/>
        <source>The code of the first level must not be empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="222"/>
        <source>Competences of the school report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="230"/>
        <source>Competences of referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2894"/>
        <source>Confidential competences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="246"/>
        <source>Follow competences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2494"/>
        <source>The first item in the list must remain the first level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2706"/>
        <source>Should also remove sub-items?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="168"/>
        <source>Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1311"/>
        <source>limiteBLTPerso:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1318"/>
        <source>limiteBLTPersoToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2146"/>
        <source>SubHeading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2148"/>
        <source>SubHeadingCheckBoxStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3325"/>
        <source>Remember to assign a valid subject.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3319"/>
        <source>The &lt;b&gt;{0}&lt;/b&gt; subject&lt;br/&gt; (attributed to teacher &lt;b&gt;{1} {2}&lt;/b&gt;)&lt;br/&gt; does not exist!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="597"/>
        <source>information message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="599"/>
        <source>question message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="601"/>
        <source>warning message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="603"/>
        <source>critical message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5934"/>
        <source>Tests</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5025"/>
        <source>ShowDBStateStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10015"/>
        <source>DBState</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10024"/>
        <source>General:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10048"/>
        <source>Password state:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10050"/>
        <source>MUST BE CHANGED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10092"/>
        <source>Structure an state:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10143"/>
        <source>SUBJECT:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10144"/>
        <source>GROUP:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10145"/>
        <source>class-group:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10146"/>
        <source>PERIOD:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10147"/>
        <source>Tables List:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10148"/>
        <source>private table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10149"/>
        <source>State assessments:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10152"/>
        <source>Shared section of the bulletin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10153"/>
        <source>Disciplinary section of the bulletin:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10154"/>
        <source>Referentiel:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10156"/>
        <source>NOTHING</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5504"/>
        <source>ExportCounts2Csv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5504"/>
        <source>ExportCounts2CsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5511"/>
        <source>ImportCountsFromCsv</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5511"/>
        <source>ImportCountsFromCsvStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4927"/>
        <source>SelectCounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4929"/>
        <source>Select the counts you want to export.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4934"/>
        <source>&lt;p&gt;&lt;b&gt;Counts:&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2842"/>
        <source>CreateTemplate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3441"/>
        <source>The name &lt;b&gt;{0}&lt;/b&gt; is already used.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2386"/>
        <source>&lt;p&gt;&lt;b&gt;Delete This Note:&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continue ?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2455"/>
        <source>&lt;p&gt;&lt;b&gt;Delete This Count:&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continue ?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="624"/>
        <source>Period:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2142"/>
        <source>&lt;p&gt;&lt;b&gt;This Item is used in Tables or Templates:&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continue ?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="87"/>
        <source>About &lt;b&gt;{0} {1}&lt;/b&gt;:&lt;p&gt;{2}&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1298"/>
        <source>New password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1303"/>
        <source>Confirm:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1300"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="396"/>
        <source>Would you download the base?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="123"/>
        <source>Version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4266"/>
        <source>Calculation:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3606"/>
        <source>Coeff:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4254"/>
        <source>Sense:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2936"/>
        <source>coefficient:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1828"/>
        <source>Groups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1697"/>
        <source>Classes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="562"/>
        <source>Base file names:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="569"/>
        <source>Document Label:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="574"/>
        <source>Prefix of class file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="580"/>
        <source>Description of class document:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1710"/>
        <source>BaseName:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1438"/>
        <source>From:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1446"/>
        <source>To:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1678"/>
        <source>Merge:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1681"/>
        <source>In:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2976"/>
        <source>Template:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3499"/>
        <source>Unlink {0} tableaux ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1405"/>
        <source>Delete this table?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2644"/>
        <source>Remove {0} items ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2646"/>
        <source>Their scores will be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="865"/>
        <source>The {0} name is already there.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="868"/>
        <source>Again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="443"/>
        <source>You will remove the group {0} and all the tables and evaluations relating to it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4589"/>
        <source>CreateConnectionFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4589"/>
        <source>CreateConnectionFileStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="118"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="434"/>
        <source>Enter your login name &lt;br/&gt;or select it in the comboBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="132"/>
        <source>Url:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="136"/>
        <source>Enter the web address of the school</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2386"/>
        <source>The structure of the file that you selected is not correct.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1262"/>
        <source>EditActualTableau</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5291"/>
        <source>EditActualTableauStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1885"/>
        <source>Modify this bilan Name ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1922"/>
        <source>Modify this bilan Label ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1720"/>
        <source>Modify this item Name ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1754"/>
        <source>Modify this item Label ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1665"/>
        <source>in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4611"/>
        <source>Export Lists of students to ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4611"/>
        <source>Export Lists of students of the school in an ODF spreadsheet file (* .ods)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="571"/>
        <source>All Students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="585"/>
        <source>FIRSTNAME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="591"/>
        <source>CLASS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="2524"/>
        <source>SelectGroupsPeriods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="2576"/>
        <source>Select couples group+period for which you want &lt;br/&gt;to import the counts contained &lt;br/&gt;in the CSV file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4914"/>
        <source>AutoSaveDBMy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4914"/>
        <source>AutoSaveDBMyStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5144"/>
        <source>Directly opens the window for creating a new group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="128"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="143"/>
        <source>Deletes the selected group and all associated tables and evaluations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="215"/>
        <source>Files folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="489"/>
        <source>Last test:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="491"/>
        <source>Last recup:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="493"/>
        <source>Retrieving ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="576"/>
        <source>QuitSchedulerStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3048"/>
        <source>LOGIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3051"/>
        <source>INITIAL PASSWORD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1629"/>
        <source>&lt;p&gt;&lt;b&gt;Would you download the database compteur&lt;br/&gt; before proceeding?&lt;/b&gt;&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="110"/>
        <source>Authors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3230"/>
        <source>NO NOTES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3232"/>
        <source>NO PERSONAL BALANCE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5635"/>
        <source>ElevePrevious</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6775"/>
        <source>ElevePreviousStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="324"/>
        <source>Remember to send it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="326"/>
        <source>(&quot;File &gt; SaveAndPostDBMy&quot; menu)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="333"/>
        <source>(&quot;File &gt; DownloadDBMy&quot; menu)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4968"/>
        <source>TestUpdateDBUsersCommun</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4968"/>
        <source>TestUpdateDBUsersCommunStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="499"/>
        <source>Your Commun DB version is the last</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="501"/>
        <source>Your Users DB version is the last</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="503"/>
        <source>Your Commun DB must be updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="505"/>
        <source>Your Users DB must be updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3857"/>
        <source>The password has been changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3859"/>
        <source>The password has not been changed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3864"/>
        <source>ReinitPassword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3870"/>
        <source>ChangePassword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3424"/>
        <source>NewTemplateFromBilan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3413"/>
        <source>Select an assessment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3415"/>
        <source>All items related to this assessment will be added to the template.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="94"/>
        <source>Help page explaining how to install it:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9118"/>
        <source>CalculatedNoteModePersos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9120"/>
        <source>CalculatedNoteModeBLT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="546"/>
        <source>The DB {0} has been modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="550"/>
        <source>Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="440"/>
        <source>Execute query</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="391"/>
        <source>ShowTables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="392"/>
        <source>ShowTablesStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="397"/>
        <source>ShowSql</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="398"/>
        <source>ShowSqlStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="662"/>
        <source>results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="683"/>
        <source>this DataBase is not editable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="687"/>
        <source>ERROR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="682"/>
        <source>ERROR:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="661"/>
        <source>OK:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="570"/>
        <source>This dataBase is not editable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4475"/>
        <source>EditDBFile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4475"/>
        <source>EditDBFileStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="374"/>
        <source>DBFileEditor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="386"/>
        <source>Open DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="414"/>
        <source>Insert a new row into the table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="419"/>
        <source>Delete the selected row</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="459"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5464"/>
        <source>Export a balance in tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2852"/>
        <source>Choose the Directory where the subdirectorys will be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5464"/>
        <source>Create a tree (folder and subfolders) from the balance sheet and associated items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="307"/>
        <source>Bilan directory name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="311"/>
        <source>Items directorys names:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="821"/>
        <source>You can open the directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2822"/>
        <source>All items related to this assessment will give a subfolder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2826"/>
        <source>Specify the format of folder names to create.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2828"/>
        <source>{0}: balance name, {1}: balance title, {2}: item name, {3}: item title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2830"/>
        <source>See help for more explanation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5782"/>
        <source>LoadInitialLayout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5782"/>
        <source>LoadInitialLayoutStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5789"/>
        <source>LoadSavedLayout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5789"/>
        <source>LoadSavedLayoutStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5796"/>
        <source>SaveCurrentLayout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5796"/>
        <source>SaveCurrentLayoutStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9611"/>
        <source>personal evaluation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2429"/>
        <source>CHANGES:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2430"/>
        <source>NO CHANGE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5256"/>
        <source>FIELDS AVAILABLE FOR MODELS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5261"/>
        <source>Fields for the school:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5274"/>
        <source>Fields for the document:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5289"/>
        <source>Fields for the student:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5317"/>
        <source>Fields for special subjects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5331"/>
        <source>Fields available for each shared competences:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5337"/>
        <source>Where XXX is the name of competence:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5351"/>
        <source>Names from table &quot;bulletin&quot; (shared competences from bulletin):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5370"/>
        <source>Names from table &quot;referentiel&quot; (competences from referential):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5389"/>
        <source>Names from table &quot;confidentiel&quot; (confidential competences):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5408"/>
        <source>Names from table &quot;suivi&quot; (followed competences):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5308"/>
        <source>Labels of Subjects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5429"/>
        <source>Fields for subjects (personal bilans of teachers) :</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5434"/>
        <source>Where XXX is the name of subject and nn a 2-digit number under limiteBLTPerso:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5449"/>
        <source>Available names of Subjects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5466"/>
        <source>Details of the results by subject for shared competences:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5471"/>
        <source>Where XXX is the name of competence and YYY is the name of subject:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5482"/>
        <source>Available names:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="511"/>
        <source>Would you like to upgrade now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5900"/>
        <source>ProtectedPeriodsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="326"/>
        <source>Documents management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4803"/>
        <source>DocumentsManagementStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="335"/>
        <source>Students personal documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="340"/>
        <source>Students documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="345"/>
        <source>Teachers documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="482"/>
        <source>Student:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1633"/>
        <source>This file does not exist in the &quot;documents&quot; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1240"/>
        <source>This file is not registered in the database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="806"/>
        <source>documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="806"/>
        <source>OpenDocsElevesDirToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1255"/>
        <source>reloadFiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1255"/>
        <source>reloadFilesToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1260"/>
        <source>repairAll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1260"/>
        <source>repairAllToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1265"/>
        <source>deleteFilesAll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1265"/>
        <source>deleteFilesAllToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1270"/>
        <source>clearAll</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1270"/>
        <source>clearAllToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1071"/>
        <source>Repair the selected item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1055"/>
        <source>Description:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1238"/>
        <source>This file does not exist in the &quot;docsprofs&quot; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1250"/>
        <source>docsprofs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1250"/>
        <source>OpenDocsTeachersDirToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1304"/>
        <source>shared documents (booklet ...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1304"/>
        <source>results (reports, ...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1304"/>
        <source>confidential documents (for teachers only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="856"/>
        <source>Students:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="859"/>
        <source>allStudents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="863"/>
        <source>selectedStudents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="866"/>
        <source>selectStudents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="866"/>
        <source>selectStudentsToolTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="178"/>
        <source>Sending documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="197"/>
        <source>uploadStudentsDocuments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="202"/>
        <source>uploadTeachersDocuments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="296"/>
        <source>Would you upload documents to your website now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3743"/>
        <source>Do you want to change the ids of leading teachers &lt;br/&gt; and reset their passwords?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3746"/>
        <source>ids offset:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1752"/>
        <source>Select the file manually</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="89"/>
        <source>WKHTMLTOPDF IS NOT INSTALLED.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="91"/>
        <source>It&apos;s a tool that improves the quality of pdf files produced.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="96"/>
        <source>Install wkhtmltopdf.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="735"/>
        <source>You also want to start cleaning up your file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="737"/>
        <source>This is done only to change the school year.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="486"/>
        <source>Uncalculated:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="489"/>
        <source>If this box is checked, this report will not be calculated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="513"/>
        <source>Download anyway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3845"/>
        <source>must be of the form ddmmyyyy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4241"/>
        <source>The birthday &lt;b&gt;{0}&lt;/b&gt; is not valid.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4243"/>
        <source>It must be of the form &lt;b&gt;ddmmyyyy&lt;/b&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3108"/>
        <source>Before changing the following database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2011"/>
        <source>You must download the latest version (which is the website).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2013"/>
        <source>Would you download it now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3116"/>
        <source>Before changing the following databases:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3123"/>
        <source>Would you download them now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1061"/>
        <source>New students:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1077"/>
        <source>Deleted students:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1084"/>
        <source>Kept students:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="635"/>
        <source>Show class results</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1616"/>
        <source>ClassDesabledCheckBoxStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1612"/>
        <source>Desabled:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6543"/>
        <source>General Restrictions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6545"/>
        <source>Pages authorized for students (all periods)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6547"/>
        <source>Restrictions relating only to the current period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="350"/>
        <source>By student</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5803"/>
        <source>ConfigureToolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5803"/>
        <source>Select size of icons, actions displayed, ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1567"/>
        <source>TOOLBAR CONFIGURATION</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1574"/>
        <source>VisibleActions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1583"/>
        <source>FrequentlyUsedActions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1594"/>
        <source>ContextualActions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4951"/>
        <source>SwitchToCompleteInterfaceStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4951"/>
        <source>SwitchToCompleteInterface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1805"/>
        <source>SwitchToSimplifiedInterfaceStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1807"/>
        <source>SwitchToSimplifiedInterface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1611"/>
        <source>OtherActions:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6253"/>
        <source>MenuAndToolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="476"/>
        <source>Advices to progress that students can see in the web interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5307"/>
        <source>Managing the display of the current items array (visibility and order)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="114"/>
        <source>Create a structure file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5387"/>
        <source>A structure file can be exchanged with colleagues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="998"/>
        <source>Import a structure file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5395"/>
        <source>Import a structure made by a colleague</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1109"/>
        <source>Introduction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="274"/>
        <source>Step 1 - items selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="303"/>
        <source>Step 2 - balances selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="355"/>
        <source>Step 3 - default profile selection (for bulletins)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="425"/>
        <source>Step 4 - tables models selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="528"/>
        <source>Last step - subject, title and description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1048"/>
        <source>Title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="572"/>
        <source>&lt;b&gt;Validate the item in case of absences:&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="130"/>
        <source>Open an existing file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="135"/>
        <source>Opens an existing file structure already to change it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1180"/>
        <source>Open sqlite File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1180"/>
        <source>sqlite files (*.sqlite)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="438"/>
        <source>TABLES TEMPLATES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="464"/>
        <source>TABLES WITHOUT TEMPLATES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1013"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1017"/>
        <source>Opens a file on your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1110"/>
        <source>School Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1025"/>
        <source>Search on the school website.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1111"/>
        <source>VERAC Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1033"/>
        <source>Search on the VERAC website.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1112"/>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1119"/>
        <source>Last step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1015"/>
        <source>Skip to the last step.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1116"/>
        <source>Default profile (for bulletins)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1200"/>
        <source>You must select a structure before continuing!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2087"/>
        <source>Modify this template Name ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2129"/>
        <source>Modify this template Label ?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1981"/>
        <source>There is already a default profile for this subject:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1984"/>
        <source>Do you want to replace it with the one contained in the structure to import?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="116"/>
        <source>Management of teacher structures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4880"/>
        <source>Structures files made available for teachers on the school website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="88"/>
        <source>Updating structures files via FTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="128"/>
        <source>Open the folder containing the files structures</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="131"/>
        <source>Reload list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="134"/>
        <source>Reloads structures files list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1003"/>
        <source>&lt; Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1008"/>
        <source>Next &gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1020"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="750"/>
        <source>Save sqlite File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1013"/>
        <source>&gt;&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4930"/>
        <source>STUDENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4546"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4552"/>
        <source>Teacher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4555"/>
        <source>Result</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4713"/>
        <source>Export results from referential to ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4713"/>
        <source>Export results of a list of students to an ODF Spreadsheet File (takes account of previous years)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4558"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="5207"/>
        <source>Balances selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="5247"/>
        <source>Subjects selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="497"/>
        <source>Step 5 - counts selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4456"/>
        <source>Create multiple counts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5724"/>
        <source>creates counts for several couples group+period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4460"/>
        <source>Select couples group+period for which you want to create the counts:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4470"/>
        <source>Select the counts you want to create by moving them to the list on the right:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9613"/>
        <source>Linked Item:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4318"/>
        <source>NO LINKED ITEM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9594"/>
        <source>No calculation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="617"/>
        <source>Import absences from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5519"/>
        <source>Import absences from eleves_date_heure.xml file exported from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="631"/>
        <source>Students to import:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="635"/>
        <source>All students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="638"/>
        <source>Students in the selected group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="702"/>
        <source>Do not import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="863"/>
        <source>Importing the file failed or you have canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3350"/>
        <source>The {0} file was created in the verac_admin directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1987"/>
        <source>Rename this profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2587"/>
        <source>Create a new profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1997"/>
        <source>Delete this profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2016"/>
        <source>&lt;p align=&quot;center&quot;&gt;The number of assessments posted in the bulletin is &lt;b&gt;limited to {0}&lt;/b&gt; for each subject.&lt;br/&gt;If you choose some more, only the first {0} evaluated assessments will be posted (for each student).&lt;br/&gt;The balance whose names are displayed in bold are your personal balance.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2050"/>
        <source>Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2057"/>
        <source>Groups or students that this profile is assigned</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2228"/>
        <source>Default profile for this subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2242"/>
        <source>STANDARD PROFILES FOR GROUPS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2263"/>
        <source>SPECIFIC PROFILES FOR STUDENTS:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2399"/>
        <source>Rename a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2591"/>
        <source>Profile name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2597"/>
        <source>Profile type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2601"/>
        <source>standard profile for groups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2604"/>
        <source>specific profile for students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3600"/>
        <source>Base:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3350"/>
        <source>Advice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9925"/>
        <source>Advise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5062"/>
        <source>Per student</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5070"/>
        <source>Followed students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5090"/>
        <source>DNB Opinion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="826"/>
        <source>Do you want to add it anyway?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="78"/>
        <source>THE VERAC VERSION IS TOO OLD.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="80"/>
        <source>VERAC version installed on this computer is too old</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="82"/>
        <source>and may damage your database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="84"/>
        <source>Update VERAC before continuing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="86"/>
        <source>(&quot;Utils &gt; UpdateVerac&quot; or &quot;Utils &gt; TestUpdateVerac&quot; menu)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="1493"/>
        <source>Updating the recup_evals database.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="174"/>
        <source>Other actions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="177"/>
        <source>Show/Hide other actions available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="212"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="223"/>
        <source>Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="231"/>
        <source>Configuration folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="282"/>
        <source>Files folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="99"/>
        <source>Manage groups of students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="189"/>
        <source>Administrator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="406"/>
        <source>New year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="408"/>
        <source>Change of school year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1062"/>
        <source>Insert New Item (before the selected item)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2700"/>
        <source>The first item can not be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6287"/>
        <source>The path must begin with a /</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5626"/>
        <source>Table to use:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="651"/>
        <source>Uncheck the box below if test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="653"/>
        <source>or whether the documents should not be made available to students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="149"/>
        <source>Updates class groups from the download list.
Attention you can not add feedback to students removed from the group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1810"/>
        <source>&lt;p&gt;Select the students you want to permanently delete.&lt;/p&gt;&lt;p&gt;All the traces (assessments, opinions, etc) &lt;br/&gt;of this students will be &lt;b&gt;definitively&lt;/b&gt; deleted.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="335"/>
        <source>Download Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5009"/>
        <source>Return to the simple management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5009"/>
        <source>Return to the simple management of personal Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2857"/>
        <source>Create the class file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3074"/>
        <source>FINISHED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="474"/>
        <source>annual-report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="475"/>
        <source>ANNUAL REPORT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="486"/>
        <source>referential-report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="487"/>
        <source>REFERENTIAL REPORT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2228"/>
        <source>The titles of shared competence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2251"/>
        <source>The titles of personal competence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2008"/>
        <source>Before use the following database:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="102"/>
        <source>Referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="110"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="114"/>
        <source>Follows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="118"/>
        <source>Opinions (on groups)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="122"/>
        <source>Papers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="126"/>
        <source>Calculator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="73"/>
        <source>the site is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="77"/>
        <source>students have access to the site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="82"/>
        <source>students can view the details of shared competences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="87"/>
        <source>students  see the &quot;Class&quot; column</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="131"/>
        <source>students have access to the actual period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="136"/>
        <source>students see the opinions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="141"/>
        <source>students see the notes (for classes with notes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="92"/>
        <source>teachers can validate the referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4337"/>
        <source>Items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5169"/>
        <source>The &lt;b&gt;{0}&lt;/b&gt; class&lt;br/&gt; (attributed to student &lt;b&gt;{1} {2}&lt;/b&gt;)&lt;br/&gt; does not exist!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5176"/>
        <source>Remember to assign a valid class or to create this class.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1058"/>
        <source>Here you set the number of decimal places for notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1703"/>
        <source>The names of created groups begin by what you specify here and will be followed by the class name.&lt;br/&gt;&lt;br/&gt;If you leave this field blank, the group name will be that of the class.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="1345"/>
        <source>Columns titles:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5901"/>
        <source>FTP Port:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5904"/>
        <source>The default FTP port is 21. &lt;br/&gt;Do not change unless you are sure it is necessary.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10921"/>
        <source>Edit this evaluation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10929"/>
        <source>&lt;p&gt;You can use the following characters, &lt;br/&gt;but respect the case (upper or lower case):&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;b&gt;{0}&lt;/b&gt;.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>VERAC will be displayed in a simplified interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>It presents the actions needed to get started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>Remember to enable the complete interface once you are more comfortable with the software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="204"/>
        <source>Remove the user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="207"/>
        <source>Remove the selected user from the comboBox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4518"/>
        <source>Management of addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4518"/>
        <source>To manage manually the addresses of students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5514"/>
        <source>Update addresses from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="1293"/>
        <source>The XML file provided does not seem to be the right one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="280"/>
        <source>Addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4945"/>
        <source>perso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4948"/>
        <source>other</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4971"/>
        <source>Personal address of the student:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4980"/>
        <source>First address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4989"/>
        <source>Second address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4998"/>
        <source>Other address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4946"/>
        <source>1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4947"/>
        <source>2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1518"/>
        <source>Would you post the referential DB?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="270"/>
        <source>Place your selection in the list on the right.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="273"/>
        <source>Place and order your selection in the list on the right.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="276"/>
        <source>To select all, you can leave the empty right list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1331"/>
        <source>School year:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1338"/>
        <source>Use the year to end of the school year.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1342"/>
        <source>School year title:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="662"/>
        <source>Use addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="560"/>
        <source>Files names</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="648"/>
        <source>Create only the class file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="665"/>
        <source>Number lines for addresses:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="149"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5514"/>
        <source>Update addresses from ResponsablesAvecAdresses.xml file exported from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2861"/>
        <source>Choose a list of shared competences among the proposed models</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2875"/>
        <source>School report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2904"/>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1214"/>
        <source>Do not import this</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3033"/>
        <source>All Teachers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4621"/>
        <source>Export Lists of teachers to ods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4621"/>
        <source>Export Lists of teachers of the school in an ODF spreadsheet file (* .ods)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="642"/>
        <source>Show only colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5316"/>
        <source>Hide empty columns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5316"/>
        <source>Displays only columns with assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1560"/>
        <source>Leo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2351"/>
        <source>use a log file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5963"/>
        <source>Default language of the website:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5978"/>
        <source>FTP access (or SFTP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6001"/>
        <source>Installation folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6017"/>
        <source>HTTP access (or HTTPS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="1048"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="1048"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="132"/>
        <source>Management of photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4527"/>
        <source>To manage the photos of students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1813"/>
        <source>Send documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1813"/>
        <source>To send the documents and the database on the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="60"/>
        <source>Sending photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="106"/>
        <source>Would you upload photos to your website now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="142"/>
        <source>Trombinoscope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="296"/>
        <source>You can view below photos &lt;br/&gt;and modify them one by one by drag and drop from your computer&apos;s file browser.&lt;br/&gt;A mass import procedure is available in the Tools tab.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="636"/>
        <source>Import photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="850"/>
        <source>Import photos from a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="654"/>
        <source>Send photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="654"/>
        <source>To send the photos on the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="788"/>
        <source>Select the folder containing the photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="855"/>
        <source>Allocated photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="856"/>
        <source>Unassigned photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="857"/>
        <source>Balance sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="688"/>
        <source>Indicated below the file name format to import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="692"/>
        <source>Use the following benchmarks (see help for more explanation):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="695"/>
        <source>: the student&apos;s name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="697"/>
        <source>: her first name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="699"/>
        <source>: her class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="701"/>
        <source>: her identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="704"/>
        <source>[NAME]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="705"/>
        <source>[FirstName]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="706"/>
        <source>[Class]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="707"/>
        <source>[ID]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="777"/>
        <source>The proposed format contains errors.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="980"/>
        <source>Class Trombinoscope:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="429"/>
        <source>Trombinoscope of all students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="304"/>
        <source>PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="308"/>
        <source>Create a pdf file of this trombinoscope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="645"/>
        <source>Clear photos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="951"/>
        <source>Clear photos of students who left the school</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="956"/>
        <source>Number of photos deleted:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5624"/>
        <source>Clear if unfavorable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5624"/>
        <source>Clear evaluation if it is unfavorable to the results of the student on this item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1736"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1739"/>
        <source>Automatically search the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2398"/>
        <source>Connection timeout test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2404"/>
        <source>Time (in seconds) the connection test facility website.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5696"/>
        <source>phones</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5703"/>
        <source>LEGAL REPRESENTATIVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5704"/>
        <source>OTHER REPRESENTATIVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5705"/>
        <source>PERSONAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5706"/>
        <source>PORTABLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5707"/>
        <source>PROFESSIONAL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5524"/>
        <source>Addresses, Phones and Mails SIECLE to ODS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5524"/>
        <source>Export as ODF data (addresses + phone numbers + mails) in the file ResponsablesAvecAdresses.xml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5695"/>
        <source>addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5701"/>
        <source>ADDRESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5702"/>
        <source>OTHER ADDRESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4690"/>
        <source>Export bulletin assessments to ODS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4724"/>
        <source>Create an ODS file containing details of assessments of the bulletin (one tab per class).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4620"/>
        <source>bulletins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4695"/>
        <source>Creating a tab for each class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5697"/>
        <source>mails</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5708"/>
        <source>MAIL ADDRESS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3679"/>
        <source>The number of the school year will be incremented by the value indicated below.&lt;br/&gt;It must be the end of the new school year (eg 2017 for 2016-2017)&lt;br/&gt; and should not be changed during the school year.&lt;br/&gt;You can correct the value proposed below if it is not correct.&lt;br/&gt;&lt;br/&gt;By cons, you can change the title of the school year as you see fit&lt;br/&gt; in the general configuration of the facility.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1074"/>
        <source>The administrator of your institution has made&lt;br/&gt; a change of school year.&lt;br/&gt;&lt;br/&gt;You should do a &lt;b&gt;&quot;clean for another year&quot;&lt;/b&gt;&lt;br/&gt; (action available in the &lt;b&gt;&quot;File&quot;&lt;/b&gt; menu) before sending your file.&lt;br/&gt;&lt;br/&gt;If you think this is due to an error,&lt;br/&gt; you can also fix the school year included in your file&lt;br/&gt; (action available via the &lt;b&gt;&quot;Tools -&gt; Settings&quot;&lt;/b&gt; menu, &lt;b&gt;&quot;Other&quot;&lt;/b&gt; tab).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1108"/>
        <source>The school year included in your file is later than your institution.&lt;br/&gt; Your feedback will not be recovered until the establishment&lt;br/&gt; has not made the change of school year.&lt;br/&gt;&lt;br/&gt;If you think this is due to an error,&lt;br/&gt; you can correct the school year included in your file&lt;br/&gt; (action available via the &lt;b&gt;&quot;Tools -&gt; Settings&quot;&lt;/b&gt; menu, &lt;b&gt;&quot;Other&quot;&lt;/b&gt; tab).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5640"/>
        <source>Base model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="275"/>
        <source>Various</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2387"/>
        <source>School year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1362"/>
        <source>Absences and delays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6556"/>
        <source>Message for userss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="650"/>
        <source>Justified absences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9448"/>
        <source>Number of half-days of justified absence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="673"/>
        <source>Unjustified absences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9459"/>
        <source>Number of half-days of unjustified absence</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9477"/>
        <source>Hours missed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9481"/>
        <source>Number of classroom hours missed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="320"/>
        <source>Export LSU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="322"/>
        <source>To export results to LSU (Livret Scolaire Unique)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="745"/>
        <source>Create ODS spreadsheet for an assessment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5376"/>
        <source>Create ODS spreadsheet with a list of items and the valuation levels, ready to be placed on your statements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3143"/>
        <source>XML File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3147"/>
        <source>xml files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="749"/>
        <source>Select below the items you want by moving them to the right list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="880"/>
        <source>Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="816"/>
        <source>Very good control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="817"/>
        <source>Satisfactory control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="818"/>
        <source>Delicate control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="819"/>
        <source>Insufficient control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="820"/>
        <source>The student was marked missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="932"/>
        <source>COLORS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="990"/>
        <source>LEGENDS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="121"/>
        <source>Notes calculations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1011"/>
        <source>HOW NOTES ARE CALCULATED</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1017"/>
        <source>Calculation method:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1025"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1027"/>
        <source>As DNB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1029"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="278"/>
        <source>School Code:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="146"/>
        <source>To create a new separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="152"/>
        <source>Delete the selected separator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="327"/>
        <source>Change the text of a separator:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="348"/>
        <source>NEW SEPARATOR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="4572"/>
        <source>Exporting the file failed or you have canceled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="111"/>
        <source>Export LSU (Livret Scolaire Unique)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="127"/>
        <source>Checks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="139"/>
        <source>File creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="345"/>
        <source>S: core</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="346"/>
        <source>O: mandatory option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="347"/>
        <source>F: optional extra</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="348"/>
        <source>L: Academic addition to the program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="349"/>
        <source>R: religious education</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="350"/>
        <source>X: specific measure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="175"/>
        <source>If you change tab without clicking&lt;br/&gt;on the button &lt;b&gt;Apply&lt;/b&gt;,&lt;br/&gt;the modifications will not be recorded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2899"/>
        <source>Create File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2899"/>
        <source>To create the xml file to export LSU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="761"/>
        <source>Officials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1188"/>
        <source>teacher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1189"/>
        <source>director</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1198"/>
        <source>&lt;p align=&quot;center&quot;&gt;The name of {0}:&lt;br/&gt;&lt;b&gt;{1}&lt;/b&gt;&lt;br/&gt;is in the file,&lt;br/&gt;but does not exist in your configuration.&lt;/p&gt;&lt;p&gt;Select the right name in the list below &lt;br/&gt; (you can also add this) :&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="663"/>
        <source>To create pdf files of trombinoscopes of all classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="663"/>
        <source>Classes PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="475"/>
        <source>The &quot;personal.csv&quot; file could not be saved in the &lt;b&gt;{0}&lt;/b&gt; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="480"/>
        <source>This folder may be write protected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="483"/>
        <source>A copy of the file &quot;personal.csv&quot; was saved in the folder you selected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="487"/>
        <source>So you can place it in the correct folder to resolve the problem.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5454"/>
        <source>Export students in subfolders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5454"/>
        <source>Create subfolders from a list of students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4625"/>
        <source>school year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="284"/>
        <source>Back to School and End of Periods:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="288"/>
        <source>Back to School</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1381"/>
        <source>8 components of the referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1384"/>
        <source>Double-click on an element allows you to edit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2836"/>
        <source>You must select at least one period or end-of-cycle balances.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1814"/>
        <source>EPI management (Interdisciplinary Teaching Practices) of the establishment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1827"/>
        <source>Thematic:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1205"/>
        <source>Subjects:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1111"/>
        <source>Choose Teacher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1200"/>
        <source>EPI reference of the school:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1530"/>
        <source>no EPI for this thematic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1535"/>
        <source>select EPI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1113"/>
        <source>Teachers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1207"/>
        <source>double-click to select a teacher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2245"/>
        <source>AP management (Personalized support) of the establishment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1235"/>
        <source>AP reference of the school:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1549"/>
        <source>no AP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1554"/>
        <source>select AP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3446"/>
        <source>Appreciation missing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3182"/>
        <source>Missing Program Elements.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2908"/>
        <source>Check File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2908"/>
        <source>To check if an export xml file to LSU is valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="360"/>
        <source>Update subjects from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="364"/>
        <source>Update subjects from Nomenclature.xml file exported from SIECLE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="558"/>
        <source>Subjects in the file have not been assigned.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="561"/>
        <source>You can see the list below and assign them by hand.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="248"/>
        <source>Open other teacher file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3017"/>
        <source>File is valid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3024"/>
        <source>File is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3026"/>
        <source>line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="777"/>
        <source>Types of assessments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="963"/>
        <source>positioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="964"/>
        <source>notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2954"/>
        <source>Calculation of the components of the socle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2857"/>
        <source>End-of-cycle balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="106"/>
        <source>Components of the socle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="792"/>
        <source>Types of classes and cycles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1000"/>
        <source>cycle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7522"/>
        <source>Number of balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7523"/>
        <source>Percentage of balances</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7526"/>
        <source>Number of items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7527"/>
        <source>Percentage of items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="830"/>
        <source>LSU_PAR_LSU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="831"/>
        <source>LSU_PAR_AVN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="832"/>
        <source>LSU_PAR_CIT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="833"/>
        <source>LSU_PAR_ART</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="834"/>
        <source>LSU_PAR_SAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1261"/>
        <source>Type of educational path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="836"/>
        <source>LSU_ACC_PAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="837"/>
        <source>LSU_ACC_PAI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="838"/>
        <source>LSU_ACC_PPRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="839"/>
        <source>LSU_ACC_PPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="840"/>
        <source>LSU_ACC_ULIS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="841"/>
        <source>LSU_ACC_UPE2A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="842"/>
        <source>LSU_ACC_SEGPA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="846"/>
        <source>LSU_COMP_AUC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="847"/>
        <source>LSU_COMP_LCA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="848"/>
        <source>LSU_COMP_LCR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="849"/>
        <source>LSU_COMP_PRO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="850"/>
        <source>LSU_COMP_LSF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="851"/>
        <source>LSU_COMP_LVE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8354"/>
        <source>Positioning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8443"/>
        <source>validated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8355"/>
        <source>can be validated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5082"/>
        <source>Guiding</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5082"/>
        <source>ShowGuidingStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9730"/>
        <source>You can give a detailed description of the guiding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8439"/>
        <source>calculated by VERAC.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1397"/>
        <source>Educational Tours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1407"/>
        <source>Additional Teaching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="340"/>
        <source>Week {0} (from {1} to {2})</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="478"/>
        <source>Closing of message in {0}s.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="199"/>
        <source>Recovery level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="192"/>
        <source>Complete calculations every time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="63"/>
        <source>Step</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="64"/>
        <source>class</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="69"/>
        <source>School Reports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="70"/>
        <source>Referential Reports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="297"/>
        <source>Reports type, period and students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="304"/>
        <source>Type of reports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="321"/>
        <source>Sort by origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="331"/>
        <source>Template file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="923"/>
        <source>Files creation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="934"/>
        <source>Create Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="934"/>
        <source>Launch files making</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="943"/>
        <source>Open the folder containing the created files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="71"/>
        <source>Annual Reports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="72"/>
        <source>DNB Reports (and Notanet file)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2950"/>
        <source>UNKNOWN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="763"/>
        <source>DURATION:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3812"/>
        <source>Clear the referential</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4675"/>
        <source>To erase records from previous years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3476"/>
        <source>Selection of cleaning type and students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3531"/>
        <source>Check the classes to be processed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3490"/>
        <source>Type of cleaning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3495"/>
        <source>socle only - previous years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3497"/>
        <source>all referential - previous years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3499"/>
        <source>socle only - all years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3501"/>
        <source>all referential - all years</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3515"/>
        <source>Selection (classes or students)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3664"/>
        <source>Launch the procedure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3664"/>
        <source>Launch the referential cleanup procedure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3673"/>
        <source>Post the DB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3673"/>
        <source>Post the referential DB to your website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3653"/>
        <source>Cleaning and posting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="596"/>
        <source>SEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="601"/>
        <source>BIRTH</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4294"/>
        <source>Shortcut to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4283"/>
        <source>You must install pywin32 to do this action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4553"/>
        <source>Display only if there is a problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="672"/>
        <source>students</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3115"/>
        <source>trombinoscope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5445"/>
        <source>Trombinoscope of the current group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5445"/>
        <source>To create a PDF file with the selected group&apos;s trombinoscope</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4307"/>
        <source>VERAC Scheduler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_webengine.py" line="133"/>
        <source>Open this link?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="645"/>
        <source>Delete lines not evaluated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="843"/>
        <source>LSU_ACC_CTR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3184"/>
        <source>Participates in the Homework done scheme.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="844"/>
        <source>LSU_ACC_DF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_filesdirs.py" line="138"/>
        <source>The scheduler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_filesdirs.py" line="142"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; seems to be already in use.&lt;br/&gt;If you are sure this is not the case, you can continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils_filesdirs.py" line="147"/>
        <source>Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="853"/>
        <source>LSU_COMP_CHK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="852"/>
        <source>LSU_COMP_LCE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4448"/>
        <source>Locked classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4830"/>
        <source>To view and edit the list of locked classes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4453"/>
        <source>Place the classes to be locked in the right list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2866"/>
        <source>Digital skills</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5086"/>
        <source>ShowDigitalSkillsStatusTip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="855"/>
        <source>LSU_CN_INF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="856"/>
        <source>LSU_CN_INF_MEN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="857"/>
        <source>LSU_CN_INF_GER</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="858"/>
        <source>LSU_CN_INF_TRA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="859"/>
        <source>LSU_CN_COM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="860"/>
        <source>LSU_CN_COM_INT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="861"/>
        <source>LSU_CN_COM_PAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="862"/>
        <source>LSU_CN_COM_COL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="863"/>
        <source>LSU_CN_COM_SIN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="864"/>
        <source>LSU_CN_CRE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="865"/>
        <source>LSU_CN_CRE_TEX</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="866"/>
        <source>LSU_CN_CRE_MUL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="867"/>
        <source>LSU_CN_CRE_ADA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="868"/>
        <source>LSU_CN_CRE_PRO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="869"/>
        <source>LSU_CN_PRO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="870"/>
        <source>LSU_CN_PRO_SEC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="871"/>
        <source>LSU_CN_PRO_DON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="872"/>
        <source>LSU_CN_PRO_SAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="873"/>
        <source>LSU_CN_ENV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="874"/>
        <source>LSU_CN_ENV_RES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="875"/>
        <source>LSU_CN_ENV_EVO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2619"/>
        <source>Digital skills management</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2625"/>
        <source>Appreciation for the class:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2870"/>
        <source>Insert the levels of mastery of digital skills</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5870"/>
        <source>SFTP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5867"/>
        <source>Use SFTP:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
