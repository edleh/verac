La liste ci-dessous indique les fichiers de structures présents dans le sous-dossier **verac_admin/ftp/secret/verac/protected/structures**.

Pour modifier la liste des fichiers :
* ouvrez le sous-dossier **structures** et placez-y vos fichiers de structures (ou supprimez ceux qui sont obsolètes)
* recharger la liste
* vous pouvez également la réordonner (avec la souris) et ajouter des séparateurs
* double-cliquez sur un séparateur pour en modifier le texte
* validez.

Vos fichiers seront ensuite postés sur le site web de votre établissement et les professeurs pourront les télécharger.
