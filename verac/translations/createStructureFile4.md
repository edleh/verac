Select the tables models you want to export by moving them to the list on the right.

Use the Ctrl and Shift keys to make multiple selections.

You can also select the tables that are not related to models; they will automatically be converted into model tables in the structure file.
