<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>main</name>
    <message>
        <location filename="../libs/utils_functions.py" line="428"/>
        <source>Nothing for Now.</source>
        <translation>Rien pour l&apos;instant.</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="426"/>
        <source>Nothing</source>
        <translation>Rien</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="150"/>
        <source>Remember User</source>
        <translation>Se souvenir de l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="118"/>
        <source>Connection</source>
        <translation>Connexion</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="712"/>
        <source>Create a table</source>
        <translation>Créer un tableau</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="852"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="963"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5031"/>
        <source>Students list</source>
        <translation>Liste des élèves</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="902"/>
        <source>Manage items</source>
        <translation>Gérer les items</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3230"/>
        <source>Add a balance</source>
        <translation>Ajouter un bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2725"/>
        <source>Create an item</source>
        <translation>Créer un item</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2948"/>
        <source>Choose competence from referential</source>
        <translation>Choisir une compétence du référentiel</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2956"/>
        <source>Create a balance</source>
        <translation>Créer un bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2561"/>
        <source>Modify the list of items (current table)</source>
        <translation>Modifier la liste des items (du tableau actuel)</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3134"/>
        <source>Remove a balance</source>
        <translation>Retirer un bilan</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="195"/>
        <source>View password</source>
        <translation>Voir le mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1405"/>
        <source>Delete this table?</source>
        <translation>Supprimer ce tableau ?</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="447"/>
        <source>Edit an item</source>
        <translation>Éditer un item</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="452"/>
        <source>Edit a balance</source>
        <translation>Éditer un bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="868"/>
        <source>Again.</source>
        <translation>Recommencer.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1305"/>
        <source>Clone Table</source>
        <translation>Cloner un tableau</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="714"/>
        <source>Student</source>
        <translation>Élève</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="518"/>
        <source>Cannot read file {0}.</source>
        <translation>Impossible d&apos;ouvrir le fichier {0}.</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="680"/>
        <source>Choose a Directory</source>
        <translation>Choisir un dossier</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="595"/>
        <source>Failed to save {0}</source>
        <translation>Impossible d&apos;enregistrer le fichier {0}</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2701"/>
        <source>Choose the displayed items</source>
        <translation>Choisir les items affichés</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="128"/>
        <source>The file was sent successfully.</source>
        <translation>Le fichier a été transmis avec succès.</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="131"/>
        <source>A problem occurred during the transfer.</source>
        <translation>Un problème est apparu pendant le transfert.</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2944"/>
        <source>Choose competence from bulletin</source>
        <translation>Choisir une compétence du bulletin</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1348"/>
        <source>The password is empty</source>
        <translation>Le mot de passe est vide</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1353"/>
        <source>2 entries are different</source>
        <translation>Les 2 saisies sont différentes</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1294"/>
        <source>Change password</source>
        <translation>Changer de mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1404"/>
        <source>The password was changed successfully.</source>
        <translation>Le mot de passe a été changé avec succès.</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1410"/>
        <source>The password change failed.</source>
        <translation>Le changement de mot de passe a échoué.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10755"/>
        <source>Opinion</source>
        <translation>Appréciation</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="5145"/>
        <source>List of teachers</source>
        <translation>Liste des professeurs</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="123"/>
        <source>Version:</source>
        <translation>Version :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="62"/>
        <source>Settings</source>
        <translation>Configuration</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="301"/>
        <source>Configuration folder</source>
        <translation>Dossier de configuration</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="324"/>
        <source>Temporary folder</source>
        <translation>Dossier temporaire</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="339"/>
        <source>Software install folder</source>
        <translation>Dossier d&apos;installation du logiciel</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="245"/>
        <source>Continue</source>
        <translation>Continuer</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="454"/>
        <source>Select the verac_admin directory</source>
        <translation>Sélectionner le dossier verac_admin</translation>
    </message>
    <message>
        <location filename="../libs/admin_recup.pyw" line="181"/>
        <source>MESSAGES WINDOW</source>
        <translation>FENÊTRE DES MESSAGES</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="445"/>
        <source>Retrieving files Teachers</source>
        <translation>Récupération des fichiers Profs</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="1850"/>
        <source>Calculation of results</source>
        <translation>Calcul des résultats</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3176"/>
        <source>Upload the database</source>
        <translation>Envoi de la base de données</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="132"/>
        <source>Url:</source>
        <translation>Adresse :</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="446"/>
        <source>Enter URL:</source>
        <translation>Adresse du lien :</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1298"/>
        <source>New password:</source>
        <translation>Nouveau mot de passe :</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1303"/>
        <source>Confirm:</source>
        <translation>Confirmer :</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="477"/>
        <source>Base</source>
        <translation>Base</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3543"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="883"/>
        <source>Label</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4337"/>
        <source>Items</source>
        <translation>Items</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="150"/>
        <source>Balances</source>
        <translation>Bilans</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2142"/>
        <source>&lt;p&gt;&lt;b&gt;This Item is used in Tables or Templates:&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continue ?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Cet item est utilisé dans les Tableaux ou Modèles :&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continuer ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="466"/>
        <source>Label:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2936"/>
        <source>coefficient:</source>
        <translation>coefficient :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3308"/>
        <source>Manage advices</source>
        <translation>Gérer les conseils</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2708"/>
        <source>Hidden Items</source>
        <translation>Items cachés</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2710"/>
        <source>Visibles Items</source>
        <translation>Items affichés</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="562"/>
        <source>Base file names:</source>
        <translation>Base des noms des fichiers :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="569"/>
        <source>Document Label:</source>
        <translation>Description du document :</translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="788"/>
        <source>Recovery folder: </source>
        <translation>Récupération du dossier : </translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="934"/>
        <source>Updating the folder: </source>
        <translation>Mise à jour du dossier : </translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5040"/>
        <source>List:</source>
        <translation>Liste :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5041"/>
        <source>Todo:</source>
        <translation>À faire :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3314"/>
        <source>Show Items</source>
        <translation>Afficher les items</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3319"/>
        <source>Show Balances</source>
        <translation>Afficher les bilans</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3336"/>
        <source>Save changes</source>
        <translation>Enregistrer les changements</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="472"/>
        <source>Advice:</source>
        <translation>Conseil :</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="198"/>
        <source>No internet</source>
        <translation>Pas d&apos;internet</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="2688"/>
        <source>Failed to open {0}</source>
        <translation>Impossible d&apos;ouvrir le fichier {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="485"/>
        <source>Html Editor</source>
        <translation>Éditeur html</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2348"/>
        <source>LogFile</source>
        <translation>Fichier log</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1433"/>
        <source>Copy the items in a table to others</source>
        <translation>Copier les items d&apos;un tableau dans d&apos;autres</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1438"/>
        <source>From:</source>
        <translation>Depuis :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1446"/>
        <source>To:</source>
        <translation>Vers :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1523"/>
        <source>&lt;p&gt;&lt;/p&gt;&lt;p&gt;The &lt;b&gt;items&lt;/b&gt; from the &lt;b&gt;fist&lt;/b&gt; table&lt;/p&gt;&lt;p&gt;will be recopied into the &lt;b&gt;seconds&lt;/b&gt; tables.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;But no item will be deleted in the &lt;b&gt;seconds&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;/p&gt;&lt;p&gt;Les &lt;b&gt;items&lt;/b&gt; du &lt;b&gt;premier&lt;/b&gt; tableau&lt;/p&gt;&lt;p&gt; seront recopiés dans les &lt;b&gt;seconds&lt;/b&gt; tableaux.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Mais aucun item ne sera détruit dans les &lt;b&gt;seconds&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1649"/>
        <source>Nothing to add</source>
        <translation>Rien à ajouter</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="660"/>
        <source>A new version of VERAC is available</source>
        <translation>Une nouvelle version de VÉRAC est disponible</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="734"/>
        <source>Must update</source>
        <translation>Pensez à mettre à jour</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="736"/>
        <source>What&apos;s new</source>
        <translation>Voir ce qu&apos;il y a de nouveau</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="384"/>
        <source>Competence</source>
        <translation>Compétence</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1420"/>
        <source>&lt;p&gt;For better security, you must &lt;b&gt;change your password&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;(menu &quot;Utils &gt; Change password&quot;)&lt;/p&gt;</source>
        <translation>&lt;p&gt;Pour une meilleure sécurité, vous devez &lt;b&gt;changer votre mot de passe&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;(menu &quot;Outils &gt; Changer de mot de passe&quot;)&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="984"/>
        <source>NO</source>
        <translation>NON</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="978"/>
        <source>YES</source>
        <translation>OUI</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="484"/>
        <source>Selection</source>
        <translation>Sélection</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2970"/>
        <source>Balance:</source>
        <translation>Bilan :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2898"/>
        <source>Link several items to a balance</source>
        <translation>Relier plusieurs items à un bilan</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2155"/>
        <source>Import Canceled</source>
        <translation>Importation annulée</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1322"/>
        <source>Manage items and balances</source>
        <translation>Gérer les items et les bilans</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="225"/>
        <source>List of teachers whose files were recovered</source>
        <translation>Liste des profs dont les fichiers ont été récupérés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3341"/>
        <source>Erase advice</source>
        <translation>Effacer le conseil</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1491"/>
        <source>Start to create or delete a link</source>
        <translation>Début d&apos;un lien à créer ou supprimer</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1498"/>
        <source>Ending create or delete a link</source>
        <translation>Fin d&apos;un lien à créer ou supprimer</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1505"/>
        <source>Abandon the creation or delete link</source>
        <translation>Abandonner la création ou suppression de lien</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="406"/>
        <source>List of downloaded files profs</source>
        <translation>Liste des fichiers profs téléchargés</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="236"/>
        <source>List of teachers problematic files</source>
        <translation>Liste des fichiers profs posant problème</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5732"/>
        <source>html files (*.html)</source>
        <translation>fichiers html (*.html)</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1273"/>
        <source>Files Creation (PDF) from a template</source>
        <translation>Création des fichiers (PDF) depuis un modèle</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10003"/>
        <source>Cannot read file {0}:
{1}.</source>
        <translation>Impossible d&apos;ouvrir le fichier {0} :
{1}.</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="1836"/>
        <source>Cannot write file {0}:
{1}.</source>
        <translation>Impossible d&apos;enregistrer le fichier {0} :
{1}.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="697"/>
        <source>A new version of the verac_admin folder is available</source>
        <translation>Une nouvelle version du dossier verac_admin est disponible</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="699"/>
        <source>Go to the download page</source>
        <translation>Aller sur la page de téléchargement</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="733"/>
        <source>A new version of the web interface is available</source>
        <translation>Une nouvelle version de l&apos;interface Web est disponible</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="424"/>
        <source>Downloading Teachers files failed</source>
        <translation>Le téléchargement des fichiers profs a échoué</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2595"/>
        <source>The creation of the results failed</source>
        <translation>La création des résultats a échoué</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3205"/>
        <source>Upload of the resultats DB failed</source>
        <translation>L&apos;envoi de la base resultats a échoué</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="848"/>
        <source>There was a problem</source>
        <translation>Il y a eu un problème</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="269"/>
        <source>FOLDERS USED BY VERAC</source>
        <translation>DOSSIERS UTILISÉS PAR VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="213"/>
        <source>Open folder</source>
        <translation>Ouvrir le dossier</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2342"/>
        <source>OTHER SETTINGS</source>
        <translation>AUTRES CONFIGURATIONS</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="92"/>
        <source>Folders</source>
        <translation>Dossiers</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="548"/>
        <source>All assessments</source>
        <translation>Toutes les évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="550"/>
        <source>Best assessments</source>
        <translation>Les meilleures évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="552"/>
        <source>Last assessments</source>
        <translation>Les dernières évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="970"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="558"/>
        <source>&lt;b&gt;Maximum number of valuations used:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Nombre maximum d&apos;évaluations retenues :&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="575"/>
        <source>&lt;b&gt;Maximum percentage of absences to validate the item:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Pourcentage maximum d&apos;absences pour valider l&apos;item :&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="585"/>
        <source>&lt;b&gt;Minimum number of ratings:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Nombre minimum d&apos;évaluations :&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="795"/>
        <source>Minimum percentage of items assessed:</source>
        <translation>Pourcentage minimum d&apos;items évalués :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1727"/>
        <source>PDF FILES CREATING SETTING</source>
        <translation>CONFIGURATION DE LA CRÉATION DES FICHIERS PDF</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1732"/>
        <source>wkhtmltopdf found:</source>
        <translation>wkhtmltopdf trouvé :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1743"/>
        <source>wkhtmltopdf path:</source>
        <translation>Chemin de wkhtmltopdf :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1756"/>
        <source>Try creating a PDF file</source>
        <translation>Tester la création d&apos;un fichier pdf</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="542"/>
        <source>&lt;b&gt;Retained assessments:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Évaluations retenues :&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="799"/>
        <source>Threshold Level</source>
        <translation>Seuils Niveau</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="536"/>
        <source>HOW ARE TAKEN INTO ACCOUNT ITEMS FOR CALCULATING THE BALANCES</source>
        <translation>COMMENT SONT PRIS EN COMPTE LES ITEMS POUR CALCULER LES BILANS</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="769"/>
        <source>HOW ARE CALCULATED THE BALANCES EVALUATED BY SEVERAL TEACHERS</source>
        <translation>COMMENT SONT CALCULÉS LES BILANS ÉVALUÉS PAR PLUSIEURS PROFS</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="774"/>
        <source>HOW BALANCES ARE CALCULATED</source>
        <translation>COMMENT SONT CALCULÉS LES BILANS</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="783"/>
        <source>Threshold taken into account:</source>
        <translation>Seuil de prise en compte :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="787"/>
        <source>Assessments</source>
        <translation>Évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="102"/>
        <source>Items calculations</source>
        <translation>Calculs Items</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="111"/>
        <source>Balances calculations</source>
        <translation>Calculs Bilans</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="150"/>
        <source>Pdf</source>
        <translation>Pdf</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="779"/>
        <source>&lt;=</source>
        <translation>≤</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="780"/>
        <source>&gt;=</source>
        <translation>≥</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="550"/>
        <source>Files configuration</source>
        <translation>Configuration des fichiers</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="659"/>
        <source>Update the documents table and move PDF files</source>
        <translation>Mettre à jour la table des documents et déplacer les fichiers PDF</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1900"/>
        <source>Up</source>
        <translation>En haut</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1904"/>
        <source>Down</source>
        <translation>En bas</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2376"/>
        <source>Collect details of assessments</source>
        <translation>Récupérer les détails des évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="594"/>
        <source>OR</source>
        <translation>OU</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="596"/>
        <source>AND</source>
        <translation>ET</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2715"/>
        <source>No Data For: {0}</source>
        <translation>Aucune données pour : {0}</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4649"/>
        <source>Checking teachers files</source>
        <translation>Vérification des fichiers profs</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1407"/>
        <source>The local password (on this computer) was changed successfully.</source>
        <translation>Le mot de passe local (sur cet ordinateur) a été changé avec succès.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10034"/>
        <source>File exists:</source>
        <translation>Existence du fichier :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10038"/>
        <source>No file</source>
        <translation>Pas de fichier</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10061"/>
        <source>File date:</source>
        <translation>Date du fichier :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10064"/>
        <source>Nothing in file</source>
        <translation>Le fichier est vide</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10069"/>
        <source>Software version:</source>
        <translation>Version du logiciel :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10238"/>
        <source>List of Tables:</source>
        <translation>Liste des tableaux :</translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="560"/>
        <source>DataBase Upgrade</source>
        <translation>Mise à jour de la base de données</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1641"/>
        <source>&lt;p&gt;&lt;b&gt;Added {0} items&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;{0} items ont été ajoutés&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="237"/>
        <source>public</source>
        <translation>public</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="239"/>
        <source>private</source>
        <translation>privé</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1039"/>
        <source>Subject:</source>
        <translation>Matière :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="662"/>
        <source>(&quot;Utils &gt; UpdateVerac&quot; menu)</source>
        <translation>(menu &quot;Outils → Mettre VÉRAC à jour&quot;)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="566"/>
        <source>&lt;b&gt;Give more weight to the last assessments:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Donner plus d&apos;importance aux dernières évaluations :&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2431"/>
        <source>Choose period</source>
        <translation>Choisir la période</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4140"/>
        <source>Create an archive of results</source>
        <translation>Création d&apos;une archive des résultats</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4157"/>
        <source>Sending archive</source>
        <translation>Envoi de l&apos;archive</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4199"/>
        <source>Sending archive failed</source>
        <translation>L&apos;envoi de l&apos;archive a échoué</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="857"/>
        <source>Update Now</source>
        <translation>Mettre à jour maintenant</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1677"/>
        <source>Merge 2 tables</source>
        <translation>Fusionner 2 tableaux</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1678"/>
        <source>Merge:</source>
        <translation>Fusionner :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1681"/>
        <source>In:</source>
        <translation>Dans :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="872"/>
        <source>Traceback:</source>
        <translation>Traçage de l&apos;erreur :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="874"/>
        <source>FileName:</source>
        <translation>Fichier :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="876"/>
        <source>LineNumber:</source>
        <translation>Ligne :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="878"/>
        <source>FunctionName:</source>
        <translation>Fonction :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="880"/>
        <source>Text:</source>
        <translation>Texte :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="820"/>
        <source>DONE!</source>
        <translation>TERMINÉ !</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="176"/>
        <source>Failed to print</source>
        <translation>L&apos;impression a échoué</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="971"/>
        <source>You should start again the software.</source>
        <translation>Vous devriez relancer le logiciel.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1643"/>
        <source>&lt;p&gt;in table: &lt;b&gt;{1}&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;dans le tableau : &lt;b&gt;{1}&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="956"/>
        <source>Apply</source>
        <translation>Appliquer</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1199"/>
        <source>USING THE KEYBOARD IN VERAC</source>
        <translation>UTILISATION DU CLAVIER DANS VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1219"/>
        <source>Keyboard Keys</source>
        <translation>Touches du clavier</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1258"/>
        <source>Showing</source>
        <translation>Affichage</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="130"/>
        <source>Keyboard</source>
        <translation>Clavier</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="949"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="355"/>
        <source>Period</source>
        <translation>Période</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="735"/>
        <source>Subject</source>
        <translation>Matière</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="862"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1824"/>
        <source>Order of groups and tables</source>
        <translation>Ordre des groupes et tableaux</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1022"/>
        <source>Public table</source>
        <translation>Tableau public</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4188"/>
        <source>This Id is already in use, choose another.</source>
        <translation>Cet Id est déjà utilisé, choisissez-en un autre.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4216"/>
        <source>This login is already in use, choose another.</source>
        <translation>Ce login est déjà utilisé, choisissez-en un autre.</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1053"/>
        <source>Add New Item</source>
        <translation>Ajouter un nouvel élément</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1080"/>
        <source>Delete the selected item</source>
        <translation>Supprimer l&apos;élément sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1088"/>
        <source>Cancel ongoing changes</source>
        <translation>Annuler les modifications en cours</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1096"/>
        <source>Previous</source>
        <translation>Précédent</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1103"/>
        <source>Next</source>
        <translation>Suivant</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3809"/>
        <source>Id:</source>
        <translation>Identifiant :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="460"/>
        <source>Name:</source>
        <translation>Nom :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3825"/>
        <source>FirstName:</source>
        <translation>Prénom :</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="140"/>
        <source>Login:</source>
        <translation>Utilisateur :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1249"/>
        <source>Initial password:</source>
        <translation>Mot de passe initial :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="120"/>
        <source>Group:</source>
        <translation>Groupe :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2952"/>
        <source>Choose a confidential competence</source>
        <translation>Choisir une compétence confidentielle</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3982"/>
        <source>Protected period</source>
        <translation>Période protégée</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3985"/>
        <source>Unprotected period</source>
        <translation>Période non protégée</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="227"/>
        <source>uploadDBDocuments</source>
        <translation>Envoi de la base documents</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="243"/>
        <source>uploadDBDocuments Failed</source>
        <translation>L&apos;envoi de la base documents a échoué</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="886"/>
        <source>Protected periods</source>
        <translation>Périodes protégées</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4273"/>
        <source>List of Protected periods</source>
        <translation>Liste des périodes protégées</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4305"/>
        <source>unprotect the period</source>
        <translation>déprotéger la période</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4324"/>
        <source>&lt;p&gt;Periode unlocked&lt;/p&gt;</source>
        <translation>&lt;p&gt;Période déprotégée&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4329"/>
        <source>&lt;p&gt;Periode is already unlocked&lt;/p&gt;</source>
        <translation>&lt;p&gt;Cette période est déjà déprotégée&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4343"/>
        <source>&lt;p&gt;&lt;b&gt;{0} is locked&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Do you want to unlock this period&lt;br/&gt;before making an update of the results ?&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;{0} est protégé&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Voulez-vous déprotéger cette période &lt;br/&gt;avant de faire une mise à jour des résultats ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2752"/>
        <source>ChooseItemsFromBLT</source>
        <translation>Bulletin</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2758"/>
        <source>ChooseItemsFromCPT</source>
        <translation>Référentiel</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2764"/>
        <source>ChooseItemsFromCFD</source>
        <translation>Confidentiel</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2755"/>
        <source>ChooseItemsFromBLTStatusTip</source>
        <translation>Choisir des compétences du bulletin</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2761"/>
        <source>ChooseItemsFromCPTStatusTip</source>
        <translation>Choisir des compétences du référentiel</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2767"/>
        <source>ChooseItemsFromCFDStatusTip</source>
        <translation>Choisir des compétences confidentielles</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1563"/>
        <source>LinkAll</source>
        <translation>Tout relier</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1374"/>
        <source>LinkAllStatusTip</source>
        <translation>Relier tous les items affichés avec tous les bilans affichés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1570"/>
        <source>UnlinkAll</source>
        <translation>Tout délier</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1379"/>
        <source>UnlinkAllStatusTip</source>
        <translation>Supprimer tous les liens affichés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1414"/>
        <source>Links</source>
        <translation>Liens</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="574"/>
        <source>Prefix of class file:</source>
        <translation>Préfixe du fichier de la classe :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="580"/>
        <source>Description of class document:</source>
        <translation>Description du document de la classe :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="941"/>
        <source>Select the class name</source>
        <translation>Sélectionner le nom de la classe</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="624"/>
        <source>All the classes</source>
        <translation>Toutes les classes</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4388"/>
        <source>&lt;p&gt;Periods are locked:&lt;/p&gt;&lt;ul&gt;{0}&lt;/ul&gt;&lt;p&gt;Only periods that are &lt;b&gt;NOT locked&lt;/b&gt; will be updated.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Des périodes sont protégées :&lt;/p&gt;&lt;ul&gt;{0}&lt;/ul&gt;&lt;p&gt;Seules les périodes qui ne sont &lt;b&gt;PAS protégées&lt;/b&gt; seront mises à jour.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="807"/>
        <source>Teachers</source>
        <translation>Professeurs</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="208"/>
        <source>Classes</source>
        <translation>Classes</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="323"/>
        <source>Subjects</source>
        <translation>Matières</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3202"/>
        <source>All Subjects</source>
        <translation>Toutes les matières</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2130"/>
        <source>Code:</source>
        <translation>Code :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1318"/>
        <source>limiteBLTPersoToolTip</source>
        <translation>Indiquez le nombre maximum de bilans autorisés sur les bulletins pour une matière</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="495"/>
        <source>VERAC is launched</source>
        <translation>VÉRAC est lancé</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="499"/>
        <source>RecoveryIsComplete</source>
        <translation>La récupération est terminée</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="501"/>
        <source>VeracIsClosed</source>
        <translation>VÉRAC est fermé</translation>
    </message>
    <message>
        <location filename="../libs/admin_recup.pyw" line="241"/>
        <source>The recovery went well.</source>
        <translation>La récupération s&apos;est bien passée.</translation>
    </message>
    <message>
        <location filename="../libs/admin_recup.pyw" line="244"/>
        <source>There was a problem during recovery.</source>
        <translation>Il y a eu un problème lors de la récupération.</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="103"/>
        <source>ConfigDlg</source>
        <translation>Configuration des planifications</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2369"/>
        <source>Never</source>
        <translation>Jamais</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="113"/>
        <source>All times</source>
        <translation>Toutes les heures</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="114"/>
        <source>Every 6 hours</source>
        <translation>Toutes les 6 heures</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="115"/>
        <source>Every 12 hours</source>
        <translation>Toutes les 12 heures</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="116"/>
        <source>Daily</source>
        <translation>Tous les jours</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="154"/>
        <source>ChooseTypeSchedule</source>
        <translation>Choisir le type de planification</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="170"/>
        <source>Schools</source>
        <translation>Établissements</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="184"/>
        <source>Initial time</source>
        <translation>Heure initiale</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="273"/>
        <source>Interface</source>
        <translation>Interface</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="275"/>
        <source>Presentation of the administrator interface</source>
        <translation>Présentation de l&apos;interface administrateur</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="235"/>
        <source>No Action</source>
        <translation>Aucune action</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="284"/>
        <source>Scheduler</source>
        <translation>Planificateur</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="292"/>
        <source>Results</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="294"/>
        <source>Calculate student results, create bulletins, ...</source>
        <translation>Calculer les résultats des élèves, créer des bulletins, ...</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="302"/>
        <source>Update</source>
        <translation>Mettre à jour</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="304"/>
        <source>Update results</source>
        <translation>Mettre à jour les résultats</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="311"/>
        <source>CreateBilansBLT</source>
        <translation>Bulletins et relevés</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="313"/>
        <source>Create bulletins or other reports</source>
        <translation>Créer des bulletins ou autres relevés</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="328"/>
        <source>EditModeles</source>
        <translation>Modèles</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="330"/>
        <source>Edit bulletins models, make new ...</source>
        <translation>Modifier les modèles de bulletins, en faire de nouveaux, ...</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="339"/>
        <source>Documents</source>
        <translation>Documents</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="341"/>
        <source>DocumentsToolTip</source>
        <translation>Gestion des documents mis à disposition des utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="351"/>
        <source>DBToolTip</source>
        <translation>Gérer les bases de données</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="360"/>
        <source>DBDirectToolTip</source>
        <translation>Gérer directement les bases de données</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="478"/>
        <source>Edit</source>
        <translation>Éditer</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="481"/>
        <source>AdviceEditStatusTip</source>
        <translation>Éditer le conseil (WYSIWYG)</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="897"/>
        <source>&lt;p&gt;Names (in file) that does not exist in your &lt;br/&gt;configuration are already joined to name that exists.&lt;/p&gt;&lt;p&gt;Do you wish to reset this ?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Il existe déjà une correspondance avec les noms du fichier &lt;br/&gt; qui n&apos;existent pas dans votre configuration.&lt;/p&gt;&lt;p&gt;Voulez-vous la recréer ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1013"/>
        <source>Apply to all students from this class</source>
        <translation>Appliquer à tous les élèves de cette classe</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1016"/>
        <source>Apply only to this student</source>
        <translation>Appliquer uniquement à cet élève</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="596"/>
        <source>Do you want to change workdir into admin_verac directory ?&lt;br/&gt; This will avoid to copy your personnal database into it before any action as admin.&lt;p&gt;&lt;b&gt;WARNING : &lt;/b&gt;&lt;/p&gt; workdir is updated for &lt;b&gt;ALL&lt;/b&gt; versions.</source>
        <translation>Voulez-vous modifier le dossier de travail afin qu&apos;il soit dans le dossier verac_admin ?&lt;br/&gt; Cela vous évitera de devoir y copier vos données personnelles avant toute action en tant qu&apos;administrateur.&lt;p&gt;&lt;b&gt;ATTENTION : &lt;/b&gt;&lt;/p&gt; le dossier de travail sera modifié pour &lt;b&gt;TOUTES&lt;/b&gt; les versions.</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="600"/>
        <source>Printing on 2 pages (2 pages 1 OR both sides)</source>
        <translation>Impression sur 2 pages (2 pages en 1 OU recto-verso)</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="604"/>
        <source>Printing on 4 pages (2 pages 1 AND both sides)</source>
        <translation>Impression sur 4 pages (2 pages en 1 ET recto-verso)</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="607"/>
        <source>Printing without adding blank pages</source>
        <translation>Impression sans ajout de pages vierges</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="598"/>
        <source>Print Management</source>
        <translation>Gestion de l&apos;impression</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1161"/>
        <source>Edit group</source>
        <translation>Éditer le groupe</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="140"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1158"/>
        <source>Create Group</source>
        <translation>Créer un groupe</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1179"/>
        <source>Group-class</source>
        <translation>groupe-classe</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1660"/>
        <source>This name is already in use. Please choose another.</source>
        <translation>Ce nom est déjà utilisé. Merci d&apos;en choisir un autre.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3815"/>
        <source>Num:</source>
        <translation>Numéro :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1808"/>
        <source>ClassesTypes</source>
        <translation>Types de classes</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2155"/>
        <source>Class type:</source>
        <translation>Type de classe :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="340"/>
        <source>nowhere</source>
        <translation>nulle part</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="342"/>
        <source>everywhere</source>
        <translation>partout</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="384"/>
        <source>Hierarchy</source>
        <translation>Hiérarchie</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="384"/>
        <source>ClassType</source>
        <translation>Type de classe</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="263"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="265"/>
        <source>Index and news</source>
        <translation>Index et nouveautés</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1631"/>
        <source>Please choose a subject.</source>
        <translation>Veuillez choisir une matière.</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="663"/>
        <source>Please choose a name.</source>
        <translation>Veuillez choisir un nom.</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="99"/>
        <source>Manage groups of students</source>
        <translation>Gérer les groupes d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4543"/>
        <source>Balance</source>
        <translation>Bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1136"/>
        <source>All subjects</source>
        <translation>Toutes les matières</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5874"/>
        <source>BaseSiteFtp:</source>
        <translation>Adresse du serveur FTP :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5885"/>
        <source>FTP User:</source>
        <translation>Utilisateur FTP :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5915"/>
        <source>DirSitePublic:</source>
        <translation>Chemin du dossier public :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5928"/>
        <source>DirSiteSecret:</source>
        <translation>Chemin du dossier secret :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1311"/>
        <source>limiteBLTPerso:</source>
        <translation>Nombre maximal de bilans pour une matière :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5941"/>
        <source>WebSiteUrlBase:</source>
        <translation>Adresse de base du site web :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5952"/>
        <source>SiteUrlPublic:</source>
        <translation>Adresse du site public :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="383"/>
        <source>prefixProfFiles:</source>
        <translation>Préfixe des fichiers profs :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1669"/>
        <source>&lt;p&gt;All the &lt;b&gt;evaluations&lt;/b&gt; of the &lt;b&gt;fist&lt;/b&gt; table&lt;br/&gt;will be recopied in the &lt;b&gt;second&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;If an item is evaluated in the 2 tables, these evaluations will be added.&lt;br/&gt;The &lt;b&gt;first&lt;/b&gt; table will be declared &lt;b&gt;private&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;The two tables must match the &lt;b&gt;same group&lt;/b&gt; of students.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Toutes les &lt;b&gt;évaluations&lt;/b&gt; du &lt;b&gt;premier&lt;/b&gt; tableau &lt;br/&gt;seront recopiées dans le &lt;b&gt;deuxième&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Si un item est évalué dans les 2 tableaux, les évaluations seront ajoutées.&lt;br/&gt;Le &lt;b&gt;premier&lt;/b&gt; tableau sera déclaré &lt;b&gt;privé&lt;/b&gt;.&lt;/p&gt;&lt;p&gt;Les 2 tableaux doivent correspondre au &lt;b&gt;même groupe&lt;/b&gt; d&apos;élèves.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="54"/>
        <source>All Groups</source>
        <translation>Tous les groupes</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2341"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10079"/>
        <source>MUST REPAIR DB PROF</source>
        <translation>LA BASE DOIT ÊTRE RÉPARÉE (VERSION TROP ANCIENNE)</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2095"/>
        <source>Recovery of sub-sections of bulletin</source>
        <translation>Récupération des sous-rubriques du bulletin</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2197"/>
        <source>Calculating students assessments</source>
        <translation>Calcul des bilans des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2291"/>
        <source>Calculating classes assessments</source>
        <translation>Calcul des bilans des classes</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2431"/>
        <source>Calculating classes synthesis</source>
        <translation>Calcul des synthèses des classes</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5622"/>
        <source>Configuring the html template</source>
        <translation>Configuration du modèle html</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5690"/>
        <source>No synthesis</source>
        <translation>Pas de synthèse</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5732"/>
        <source>Save html File</source>
        <translation>Enregistrer un fichier html</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5646"/>
        <source>Synthesis model:</source>
        <translation>Modèle de synthèse :</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="406"/>
        <source>New year</source>
        <translation>Nouvelle année</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="408"/>
        <source>Change of school year</source>
        <translation>Changement d&apos;année scolaire</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="531"/>
        <source>tar.gz files (*.tar.gz)</source>
        <translation>fichiers tar.gz (*.tar.gz)</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3436"/>
        <source>Create an archive of the current year</source>
        <translation>Création d&apos;une archive de l&apos;année actuelle</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="706"/>
        <source>copy of the data in the temporary folder</source>
        <translation>copie des données dans le dossier temporaire</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="721"/>
        <source>creation of the archive file</source>
        <translation>création du fichier archive</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="693"/>
        <source>Save tar.gz File</source>
        <translation>Enregistrer un fichier tar.gz</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="698"/>
        <source>&lt;p&gt;After this operation, you cannot&lt;br/&gt; modify any more either evaluations&lt;br/&gt; or appreciations for the students&lt;br/&gt; deleted by the groups.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;It is advised to update your&lt;br/&gt; seizures before beginning.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Do you want to continue&lt;br/&gt; the update of lists?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Après cette manoeuvre, vous ne pourrez&lt;br/&gt; plus modifier ni évaluations ni&lt;br/&gt; appréciations pour les élèves&lt;br/&gt; supprimés des groupes.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Il est conseillé de mettre à jour&lt;br/&gt; vos saisies avant de commencer&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Voulez-vous continuer la&lt;br/&gt; mise à jour des listes ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="719"/>
        <source>&lt;p&gt;Before updating the users group,&lt;br/&gt; you need to download the latest version&lt;br/&gt; (which is the website).&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Would you download the database users&lt;br/&gt; before proceeding?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Avant de modifier les groupes,&lt;br/&gt; il faut télécharger la dernière version&lt;br/&gt; (qui est celle du site Web).&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Voulez-vous télécharger la base users&lt;br/&gt;  avant de continuer ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3509"/>
        <source>Cleaning for a new year</source>
        <translation>Nettoyage pour une nouvelle année</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3514"/>
        <source>reset periods</source>
        <translation>réinitialisation des périodes</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3557"/>
        <source>removing obsolete databases</source>
        <translation>suppression des bases de données obsolètes</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3635"/>
        <source>removing obsolete local files</source>
        <translation>suppression des fichiers locaux obsolètes</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3819"/>
        <source>removing obsolete remote files</source>
        <translation>suppression des fichiers distants obsolètes</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1038"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; &lt;br/&gt; is already recorded in this group: &lt;b&gt;{1}&lt;/b&gt;. &lt;br/&gt; Subject: &lt;b&gt;{2}&lt;/b&gt;.</source>
        <translation>&lt;b&gt;{0}&lt;/b&gt; &lt;br/&gt; est déjà inscrit dans ce groupe : &lt;b&gt;{1}&lt;/b&gt;. &lt;br/&gt; Matière : &lt;b&gt;{2}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="949"/>
        <source>&lt;p align=&quot;center&quot;&gt;The name of class &lt;b&gt;{0}&lt;/b&gt; is in the file,&lt;br/&gt;but does not exist in your configuration.&lt;/p&gt;&lt;p&gt;Select the right name in the list below &lt;br/&gt; (you can also add this class) :&lt;/p&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;Le nom de classe &lt;b&gt;{0}&lt;/b&gt; est dans le fichier,&lt;br/&gt;mais n&apos;existe pas dans votre configuration.&lt;/p&gt;&lt;p&gt;Veuillez sélectionner le bon nom dans la liste ci-dessous &lt;br/&gt; (vous pouvez aussi ajouter cette classe) :&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="968"/>
        <source>Do not import this student</source>
        <translation>Ne pas importer cet élève</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="965"/>
        <source>Add this class</source>
        <translation>Ajouter cette classe (la créer)</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="975"/>
        <source>Name of the new class:</source>
        <translation>Nom de la nouvelle classe :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="377"/>
        <source>VeracAdminDir</source>
        <translation>Dossier verac_admin</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="413"/>
        <source>DownloadVeracAdmin</source>
        <translation>Télécharger le dossier verac_admin</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="469"/>
        <source>Select where you want to create the verac_admin directory</source>
        <translation>Sélectionnez l&apos;emplacement où vous voulez créer le dossier verac_admin</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2438"/>
        <source>NEW STUDENT</source>
        <translation>NOUVEL ÉLÈVE</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2439"/>
        <source>DELETED STUDENT</source>
        <translation>ÉLÈVE SUPPRIMÉ</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2440"/>
        <source>UPDATED STUDENT</source>
        <translation>ÉLÈVE MIS À JOUR</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2558"/>
        <source>NEW TEACHER</source>
        <translation>NOUVEAU PROF</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2559"/>
        <source>DELETED TEACHER</source>
        <translation>PROF SUPPRIMÉ</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2560"/>
        <source>UPDATED TEACHER</source>
        <translation>PROF MIS À JOUR</translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="266"/>
        <source>CSV file editor</source>
        <translation>Éditeur de fichier CSV</translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="295"/>
        <source>Insert a row</source>
        <translation>Insérer une ligne</translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="300"/>
        <source>Remove a row</source>
        <translation>Supprimer une ligne</translation>
    </message>
    <message>
        <location filename="../libs/utils_csveditor.py" line="383"/>
        <source>Position : ({0},{1})   Value : {2}</source>
        <translation>Position : ({0},{1})   Valeur : {2}</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="66"/>
        <source>CreateEtab</source>
        <translation>Créer un établissement</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="74"/>
        <source>If you already have a &lt;b&gt;verac_admin&lt;/b&gt; folder &lt;br/&gt;configured for your school, &lt;br/&gt;click the &lt;b&gt;Continue&lt;/b&gt; button to select it.</source>
        <translation>Si vous avez déjà un dossier &lt;b&gt;verac_admin&lt;/b&gt;  &lt;br/&gt;configuré pour votre établissement, &lt;br/&gt;cliquez sur le bouton &lt;b&gt;Continuer&lt;/b&gt; afin de le sélectionner.</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="143"/>
        <source>Deletes the selected group and all associated tables and evaluations</source>
        <translation>Supprime le groupe sélectionné ainsi que tous les tableaux et évaluations associés</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="137"/>
        <source>Changes the selected group</source>
        <translation>Permet de modifier le groupe sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="252"/>
        <source>ReportDlg</source>
        <translation>Rapport de récupération</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5162"/>
        <source>tablesCompilation</source>
        <translation>Compilation</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2002"/>
        <source>tablesCompilationStatusTip</source>
        <translation>Compilation de tous les tableaux publics pour le groupe et la période sélectionnés</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="97"/>
        <source>ShowCompteurAnalyze</source>
        <translation>Afficher les analyses du site Web</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="239"/>
        <source>Profs</source>
        <translation>Profs</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="721"/>
        <source>Errors</source>
        <translation>Erreurs</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="733"/>
        <source>Prof</source>
        <translation>Prof</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="334"/>
        <source>Week {0} :
  from 	{1}
  to 	{2}</source>
        <translation>Semaine {0} :
  du 	{1}
  au 	{2}</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5530"/>
        <source>Class</source>
        <translation>Classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="727"/>
        <source>IpAdress</source>
        <translation>Adresse IP</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="729"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="731"/>
        <source>TimeStamp</source>
        <translation>Horodatage</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="430"/>
        <source>visit</source>
        <translation>visite</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="431"/>
        <source>visits</source>
        <translation>visites</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="432"/>
        <source>upload</source>
        <translation>envoi</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="433"/>
        <source>uploads</source>
        <translation>envois</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="134"/>
        <source>CompteurAnalyze2ods</source>
        <translation>Exporter l&apos;analyse en fichier ods</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="422"/>
        <source>ODF Spreadsheet File</source>
        <translation>Fichier tableur ODF</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="424"/>
        <source>ods files (*.ods)</source>
        <translation>fichiers ods (*.ods)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1871"/>
        <source>LAN SETTING</source>
        <translation>CONFIGURATION RÉSEAU</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1877"/>
        <source>Configuration in LAN</source>
        <translation>Configuration en réseau</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1892"/>
        <source>Select the folder</source>
        <translation>Sélectionner le dossier</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1916"/>
        <source>Working folder in LAN</source>
        <translation>Dossier de travail en réseau</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="159"/>
        <source>LAN</source>
        <translation>Réseau</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2173"/>
        <source>Select The .Verac directory</source>
        <translation>Sélectionner le dossier .Verac</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2262"/>
        <source>Select The Work directory</source>
        <translation>Sélectionner le dossier de travail</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1930"/>
        <source>prevent users to change WorkDir</source>
        <translation>empêcher les utilisateurs de modifier le dossier de travail</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2617"/>
        <source>Calculation of the referential</source>
        <translation>Calcul du référentiel</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2358"/>
        <source>Updates</source>
        <translation>Mises à jour</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2363"/>
        <source>OneOutOfThree</source>
        <translation>Une fois sur trois</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2365"/>
        <source>Weekly</source>
        <translation>Chaque semaine</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2367"/>
        <source>Always</source>
        <translation>Toujours</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="979"/>
        <source>CreateMultipleTableaux</source>
        <translation>Créer plusieurs tableaux</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1710"/>
        <source>BaseName:</source>
        <translation>Base des noms :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1009"/>
        <source>CreateMultipleTableauxBaseNameToolTip</source>
        <translation>Les noms des tableaux créés commenceront
par ce que vous indiquerez ici
et seront suivis du nom du groupe</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="809"/>
        <source>Green</source>
        <translation>Vert</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="810"/>
        <source>Yellow</source>
        <translation>Jaune</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="811"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="812"/>
        <source>Red</source>
        <translation>Rouge</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="813"/>
        <source>Not Rated</source>
        <translation>Non évalué</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7528"/>
        <source>Note calculated from the items</source>
        <translation>Note calculée à partir des items</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7524"/>
        <source>Note calculated from the balances</source>
        <translation>Note calculée à partir des bilans</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Label of Item or Balance</source>
        <translation>Description de l&apos;item ou du bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Value of Item or Balance</source>
        <translation>Valeur de l&apos;item ou du bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7378"/>
        <source>RecalcAllGroupes</source>
        <translation>Recalculer tous les groupes</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7386"/>
        <source>Recalc Groupe : {0}</source>
        <translation>Calcul du groupe : {0}</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1027"/>
        <source>ChooseItems</source>
        <translation>Choisir les items</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1029"/>
        <source>ChooseItemsStatusTip</source>
        <translation>Pour sélectionner dès maintenant les items 
qui seront placés dans les nouveaux tableaux</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1051"/>
        <source>Precision of calculated notes:</source>
        <translation>Précision des notes calculées :</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="302"/>
        <source>DownloadProfxxFilesLocal</source>
        <translation>Récupération des fichiers des professeurs 
(depuis le dossier verac_admin)</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="347"/>
        <source>DownloadProfxxFilesWeb</source>
        <translation>Téléchargement des fichiers des professeurs 
(depuis le site Web)</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="310"/>
        <source>DownloadProfxxFilesLan</source>
        <translation>Synchronisation des dossiers réseau et local
(contenant les fichiers des professeurs)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1884"/>
        <source>Create the configuration folder</source>
        <translation>Créer le dossier de configuration</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2190"/>
        <source>Select The lanConfig parent directory</source>
        <translation>Sélectionner le dossier parent du dossier lanConfig (qui sera créé)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2279"/>
        <source>Select The Teachers files directory</source>
        <translation>Sélectionner le dossier des fichiers profs</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1905"/>
        <source>Teachers files folder</source>
        <translation>Dossier des fichiers profs</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1697"/>
        <source>Classes:</source>
        <translation>Classes :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1687"/>
        <source>CreateMultipleGroupesClasses</source>
        <translation>Créer plusieurs groupes-classes</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="667"/>
        <source>Please choose a label.</source>
        <translation>Veuillez choisir une description.</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1803"/>
        <source>Students removed from the groups:</source>
        <translation>Élèves retirés des groupes :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2905"/>
        <source>ShowMdpProfsNoChange</source>
        <translation>Profs n&apos;ayant pas changé de mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5211"/>
        <source>no template</source>
        <translation>Pas de modèle</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2975"/>
        <source>ChooseTemplate</source>
        <translation>Choisir un modèle</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2976"/>
        <source>Template:</source>
        <translation>Modèle :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3174"/>
        <source>ManageTemplates</source>
        <translation>Gérer les modèles de tableaux</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3178"/>
        <source>&lt;p&gt;&lt;b&gt;Templates:&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Modèles de tableaux :&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3216"/>
        <source>&lt;p&gt;&lt;b&gt;LinkedTableaux:&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Tableaux liés :&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3222"/>
        <source>&lt;p&gt;&lt;b&gt;Items:&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Items :&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3186"/>
        <source>NewTemplate</source>
        <translation>Nouveau modèle</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3198"/>
        <source>DeleteTemplate</source>
        <translation>Supprimer le modèle sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3204"/>
        <source>EditTemplate</source>
        <translation>Éditer le modèle sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3499"/>
        <source>Unlink {0} tableaux ?</source>
        <translation>Délier {0} tableaux ?</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1034"/>
        <source>ChooseTemplateStatusTip</source>
        <translation>Pour sélectionner dès maintenant le modèle 
qui sera appliqué aux nouveaux tableaux</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3327"/>
        <source>{0}item {1} in tableau {2}
</source>
        <translation>{0}item {1} dans le tableau {2}
</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="158"/>
        <source>NO FILE</source>
        <translation>PAS DE FICHIER</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="317"/>
        <source>The 2 files have the same date.</source>
        <translation>Les 2 fichiers ont la même date.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="371"/>
        <source>The local file seems most recent.</source>
        <translation>Le fichier local semble le plus récent.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="329"/>
        <source>The distant file seems most recent.</source>
        <translation>Le fichier distant semble le plus récent.</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2487"/>
        <source>Are you sure you want to continue?</source>
        <translation>Êtes-vous certain de vouloir continuer ?</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="658"/>
        <source>Total</source>
        <translation>Total</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4609"/>
        <source>password status (requires download of the database users)</source>
        <translation>état du mot de passe (nécessite le téléchargement de la base users)</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4613"/>
        <source>date of prof file</source>
        <translation>date du fichier prof</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4617"/>
        <source>VERAC version</source>
        <translation>version de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4621"/>
        <source>prof version of the base (requires download the file)</source>
        <translation>version de la base prof (nécessite le téléchargement du fichier)</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4629"/>
        <source>display the list of tables</source>
        <translation>afficher la liste des tableaux</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="728"/>
        <source>Create or modify the groups of students</source>
        <translation>Créer ou modifier les groupes d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="772"/>
        <source>ChooseItemsStatusTip1</source>
        <translation>Pour sélectionner dès maintenant les items 
qui seront placés dans le nouveau tableau</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="777"/>
        <source>ChooseTemplateStatusTip1</source>
        <translation>Pour sélectionner dès maintenant le modèle 
qui sera appliqué au nouveau tableau</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3091"/>
        <source>LinkBilans</source>
        <translation>Relier des bilans entre eux</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3144"/>
        <source>Linked Balances</source>
        <translation>Bilans liés</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="247"/>
        <source>ListTooOldProfs</source>
        <translation>Liste des profs dont les fichiers sont d&apos;une ancienne version de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="653"/>
        <source>GROUP</source>
        <translation>GROUPE</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2392"/>
        <source>&lt;p&gt;&lt;b&gt;These balances can not be connected!&lt;/b&gt;&lt;/p&gt;&lt;p&gt;There are already connected by a path.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Ces bilans ne peuvent pas être reliés !&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Il sont déjà reliés par un chemin.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="4086"/>
        <source>&lt;p&gt;&lt;b&gt;Do you want to remove the links between &lt;br/&gt;{0} items and {1} balances?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Voulez-vous supprimer les liens entre &lt;br/&gt;{0} items et {1} bilans ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="4146"/>
        <source>&lt;p&gt;&lt;b&gt;Do you want to remove the links with &lt;br/&gt;{0} other balances?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Voulez-vous supprimer les liens avec &lt;br/&gt;{0} autres bilans ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="388"/>
        <source>Periods</source>
        <translation>Périodes</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="390"/>
        <source>PeriodsToolTip</source>
        <translation>Gérer les périodes (changements, archives, ...)</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="397"/>
        <source>SwitchToNextPeriod</source>
        <translation>Période suivante</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="399"/>
        <source>SwitchToNextPeriodToolTip</source>
        <translation>Basculer à la période suivante (archivage + sélection de la période suivante)</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6787"/>
        <source>&lt;p&gt;The viewSuivis has been modified.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Would you upload the changes?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;La vue Suivis a été modifiée.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Voulez-vous poster les changements ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="731"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="732"/>
        <source>Horaire</source>
        <translation>Horaire</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="165"/>
        <source>GestElevesSuivis</source>
        <translation>Gestion des élèves suivis</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="1090"/>
        <source>NewLabel2:</source>
        <translation>Nouvelle description complémentaire :</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="751"/>
        <source>Remark</source>
        <translation>Remarque</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="452"/>
        <source>This action is disabled in the demo version.</source>
        <translation>Cette action est désactivée dans la version démo.</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="450"/>
        <source>NotInDemo</source>
        <translation>Version démo</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="177"/>
        <source>DefineSuivis</source>
        <translation>Définir les élèves suivis</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="178"/>
        <source>ConsultSuivis</source>
        <translation>Consulter les suivis</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="278"/>
        <source>&lt;p&gt;The ElevesSuivis list has been modified.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Would you save the changes?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;La liste des élèves suivis a été modifiée.&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Voulez-vous enregistrer les changements ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="396"/>
        <source>Would you download the base?</source>
        <translation>Voulez-vous télécharger la base de données ?</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="183"/>
        <source>SaveDefine</source>
        <translation>Sauvegarder les définitions</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="200"/>
        <source>LoadConsult</source>
        <translation>Télécharger la base de données</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="458"/>
        <source>ALL</source>
        <translation>TOUT</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="482"/>
        <source>pdf File</source>
        <translation>Fichier pdf</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="489"/>
        <source>pdf files (*.pdf)</source>
        <translation>fichiers pdf (*.pdf)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2804"/>
        <source>Save Freeplane File</source>
        <translation>Enregistrer un fichier Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2807"/>
        <source>mm files (*.mm)</source>
        <translation>fichiers mm (*.mm)</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2612"/>
        <source>Save csv File</source>
        <translation>Enregistrer un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2617"/>
        <source>csv files (*.csv)</source>
        <translation>fichiers csv (*.csv)</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="11091"/>
        <source>Do you want to change all dates?</source>
        <translation>Voulez-vous modifier toutes les dates ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="219"/>
        <source>INTERNET CONNECTION PROBLEM!</source>
        <translation>PROBLÈME DE CONNEXION INTERNET !</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="700"/>
        <source>HelpPage</source>
        <translation>Aide en ligne</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3591"/>
        <source>CreateNote</source>
        <translation>Créer une note</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3589"/>
        <source>EditNote</source>
        <translation>Éditer une note</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3606"/>
        <source>Coeff:</source>
        <translation>Coefficient :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3618"/>
        <source>CalculatedNote</source>
        <translation>Note calculée par VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3621"/>
        <source>ChooseCalculatedNoteModeStatusTip</source>
        <translation>Choisir le mode de calcul de la note</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9114"/>
        <source>CalculatedNoteModeItems</source>
        <translation>Note calculée d&apos;après les items</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9116"/>
        <source>CalculatedNoteModeBilans</source>
        <translation>Note calculée d&apos;après les bilans</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6201"/>
        <source>&lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; is not a valid number!</source>
        <translation>&lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; n&apos;est pas un nombre valable !</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6040"/>
        <source>&lt;b&gt;{0}/{1}&lt;/b&gt;&lt;br/&gt; is not a valid rating!</source>
        <translation>&lt;b&gt;{0}/{1}&lt;/b&gt;&lt;br/&gt; n&apos;est pas une note valable !</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9151"/>
        <source>Average</source>
        <translation>Moyenne</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2386"/>
        <source>&lt;p&gt;&lt;b&gt;Delete This Note:&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continue ?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Supprimer cette note :&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continuer ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5702"/>
        <source>DeleteNote</source>
        <translation>Supprimer une note</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>standard deviation:</source>
        <translation>écart-type :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>Average score:</source>
        <translation>Note moyenne :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>mini score:</source>
        <translation>note mini :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>maxi score:</source>
        <translation>note maxi :</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="479"/>
        <source>Annual Report</source>
        <translation>Bilan annuel</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="617"/>
        <source>Orientation</source>
        <translation>Orientation</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="619"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="622"/>
        <source>Landscape</source>
        <translation>Paysage</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="639"/>
        <source>Hide the names of students and teachers</source>
        <translation>Masquer les noms des élèves et des profs</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="977"/>
        <source>Check out the different settings before continuing.</source>
        <translation>Vérifiez bien les différents paramètres avant de continuer.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="405"/>
        <source>Academie:</source>
        <translation>Académie :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="410"/>
        <source>Departement:</source>
        <translation>Département :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="388"/>
        <source>Adress:</source>
        <translation>Adresse :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1013"/>
        <source>Document title:</source>
        <translation>Titre du document :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1020"/>
        <source>Serie:</source>
        <translation>Série :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="1025"/>
        <source>Session:</source>
        <translation>Session :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="395"/>
        <source>Phone:</source>
        <translation>Numéro de téléphone :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="439"/>
        <source>This part is still under development.</source>
        <translation>Cette partie est encore en développement.</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="437"/>
        <source>Under Development</source>
        <translation>En développement</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1603"/>
        <source>Class with notes:</source>
        <translation>Classe à notes :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1608"/>
        <source>Check this box if the class must have notes</source>
        <translation>Cochez cette case si la classe doit avoir des notes</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1705"/>
        <source>&lt;b&gt;Should we remove students who are not in the file?&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;(in general, this is to do that earlier this year)</source>
        <translation>&lt;b&gt;Faut-il supprimer les élèves qui ne sont pas dans le fichier ?&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;(en général, cela n&apos;est à faire qu&apos;en début d&apos;année)</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="349"/>
        <source>Administration</source>
        <translation>Administration</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="358"/>
        <source>GestDirect</source>
        <translation>Gestion directe</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1080"/>
        <source>Select the subject name</source>
        <translation>Sélectionner le nom de la matière</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1087"/>
        <source>&lt;p align=&quot;center&quot;&gt;The subject name &lt;b&gt;{0}&lt;/b&gt; is in the file,&lt;br/&gt;but does not exist in your configuration.&lt;/p&gt;&lt;p&gt;Select the right name in the list below &lt;br/&gt; (you can also add this subject) :&lt;/p&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;Le nom de matière &lt;b&gt;{0}&lt;/b&gt; est dans le fichier,&lt;br/&gt;mais n&apos;existe pas dans votre configuration.&lt;/p&gt;&lt;p&gt;Veuillez sélectionner le bon nom dans la liste ci-dessous &lt;br/&gt; (vous pouvez aussi ajouter cette matière) :&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1099"/>
        <source>Add this subject</source>
        <translation>Ajouter cette matière (la créer)</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1106"/>
        <source>Name of the new subject:</source>
        <translation>Nom de la nouvelle matière :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="885"/>
        <source>&lt;p&gt;&lt;b&gt;Classes Names Replacement:&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Remplacement des noms de classes :&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="890"/>
        <source>&lt;p&gt;&lt;b&gt;Subjects Names Replacement:&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Remplacement des noms de matières :&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="878"/>
        <source>createReplacement</source>
        <translation>Recréer la table de remplacements</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="895"/>
        <source>&lt;p&gt;&lt;b&gt;Teachers Names Replacement:&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Remplacement des noms de profs :&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1191"/>
        <source>Select the {0} name</source>
        <translation>Sélectionner le nom du {0}</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1198"/>
        <source>&lt;p align=&quot;center&quot;&gt;The name of {0}:&lt;br/&gt;&lt;b&gt;{1}&lt;/b&gt;&lt;br/&gt;is in the file,&lt;br/&gt;but does not exist in your configuration.&lt;/p&gt;&lt;p&gt;Select the right name in the list below &lt;br/&gt; (you can also add this) :&lt;/p&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;Le nom de {0} :&lt;br/&gt;&lt;b&gt;{1}&lt;/b&gt;&lt;br/&gt;est dans le fichier,&lt;br/&gt;mais n&apos;existe pas dans votre configuration.&lt;/p&gt;&lt;p&gt;Veuillez sélectionner le bon nom dans la liste ci-dessous &lt;br/&gt; (vous pouvez aussi l&apos;ajouter) :&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1212"/>
        <source>Add this {0}</source>
        <translation>Ajouter ce {0} (le créer)</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1221"/>
        <source>Name of the new {0}:</source>
        <translation>Nom du nouveau {0} :</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="367"/>
        <source>CsvFiles</source>
        <translation>Fichiers CSV</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1512"/>
        <source>EditThisItem</source>
        <translation>Éditer cet item</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1519"/>
        <source>DeleteThisItem</source>
        <translation>Supprimer cet item</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1526"/>
        <source>EditThisBilan</source>
        <translation>Éditer ce bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="994"/>
        <source>RemoveThisBilan</source>
        <translation>Retirer ce bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1533"/>
        <source>DeleteThisBilan</source>
        <translation>Supprimer ce bilan</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="145"/>
        <source>Password:</source>
        <translation>Mot de passe :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1548"/>
        <source>DeleteThisLink</source>
        <translation>Supprimer ce lien</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1555"/>
        <source>EditLinkCoeff</source>
        <translation>Modifier le coefficient</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1540"/>
        <source>ShowLinkedBilansToThis</source>
        <translation>Afficher les bilans liés à ce bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1606"/>
        <source>ShowAllLinkedBilans</source>
        <translation>Afficher tous les bilans liés entre eux</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="300"/>
        <source>Website state</source>
        <translation>État du site web</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="602"/>
        <source>Date of current version: </source>
        <translation>Date de la version actuelle : </translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="604"/>
        <source>Date of your version: </source>
        <translation>Date de votre version : </translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="606"/>
        <source>Your VERAC version is the last</source>
        <translation>Vous avez la dernière version de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="651"/>
        <source>Your verac_admin dir version is the last</source>
        <translation>Vous avez la dernière version du dossier verac_admin</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="653"/>
        <source>Your Web interface version is the last</source>
        <translation>Vous avez la dernière version de l&apos;interface Web</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="205"/>
        <source>Make a bug report</source>
        <translation>Faire un rapport de bug</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="208"/>
        <source>&lt;p align=&quot;center&quot;&gt;Copy and paste the contents of the log file shown below&lt;br/&gt;or open configuration file.&lt;br/&gt;See the help page for further explanation.&lt;/p&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;Copiez-collez le contenu du fichier log affiché ci-dessous&lt;br/&gt;ou ouvrez le dossier de configuration.&lt;br/&gt;Voir la page d&apos;aide pour plus d&apos;explications.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="238"/>
        <source>Open the configuration folder of VERAC</source>
        <translation>Ouvrir le dossier de configuration de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="200"/>
        <source>Debugging</source>
        <translation>Débogage</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1577"/>
        <source>DeleteAll</source>
        <translation>Tout supprimer</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1392"/>
        <source>DeleteAllStatusTip</source>
        <translation>Supprimer tous les items et bilans affichés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1598"/>
        <source>Export2Csv</source>
        <translation>Exporter en csv</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1398"/>
        <source>Export2CsvStatusTip</source>
        <translation>Exporte la structure des items et bilans affichés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1584"/>
        <source>SelectBilans</source>
        <translation>Sélectionner les bilans liés aux items affichés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1591"/>
        <source>SelectItems</source>
        <translation>Sélectionner les items liés aux bilans affichés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2485"/>
        <source>All items displayed and balances will be deleted.</source>
        <translation>Tous les items et bilans affichés vont être supprimés.</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="2710"/>
        <source>ItemsBilansTable</source>
        <translation>Tableau des items et bilans</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3542"/>
        <source>Coeff</source>
        <translation>Coeff</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10239"/>
        <source>MATIERE</source>
        <translation>MATIÈRE</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10240"/>
        <source>GROUPE</source>
        <translation>GROUPE</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10241"/>
        <source>PERIODE</source>
        <translation>PÉRIODE</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="578"/>
        <source>NAME</source>
        <translation>NOM</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10243"/>
        <source>ETAT</source>
        <translation>ÉTAT</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2820"/>
        <source>Select the assessment you want to export.</source>
        <translation>Sélectionnez le bilan que vous voulez exporter.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2787"/>
        <source>All items related to this assessment will be in the Freeplane file.</source>
        <translation>Tous les items liés à ce bilan seront dans le fichier Freeplane.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5438"/>
        <source>ExportBilan2Freeplane</source>
        <translation>Exporter un bilan vers Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="290"/>
        <source>ChooseBilan</source>
        <translation>Choisir un bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10075"/>
        <source>Version of the teacher DataBase:</source>
        <translation>Version de la base prof :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1828"/>
        <source>Groups:</source>
        <translation>Groupes :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1836"/>
        <source>Tableaux:</source>
        <translation>Tableaux :</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="959"/>
        <source>&lt;p&gt;The password you used to log is saved on your computer,&lt;br/&gt;&lt;b&gt;but is not the same as that registered on the website of the school.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Perhaps you have changed the password when your computer&lt;br/&gt;was not connected to the Internet?&lt;/p&gt;&lt;p&gt;To solve this problem, you should:&lt;ul&gt;&lt;li&gt;You can reconnect with the password from the website&lt;/li&gt;&lt;li&gt;Or go to the web interface and change your website password.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;Le mot de passe que vous avez utilisé pour vous connecter est enregistré sur votre ordinateur,&lt;br/&gt;&lt;b&gt;mais n&apos;est pas le même que celui enregistré sur le site web de l&apos;établissement scolaire.&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Peut-être avez vous changé ce mot de passe alors que votre ordinateur &lt;br/&gt;n&apos;était pas relié à Internet ?&lt;/p&gt;&lt;p&gt;Pour régler ce problème, vous devriez :&lt;ul&gt;&lt;li&gt;soit vous reconnecter avec le mot de passe du site web de l&apos;établissement&lt;/li&gt;&lt;li&gt;soit aller dans l&apos;interface web et y changer votre mot de passe.&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3013"/>
        <source>SelectTemplates</source>
        <translation>Sélectionner les modèles de tableaux</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3015"/>
        <source>Select the templates you want to export.</source>
        <translation>Sélectionnez les modèles de tableaux que vous voulez exporter.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="6773"/>
        <source>&lt;p&gt;You forgot to select the &lt;b&gt;timing&lt;/b&gt;&lt;br/&gt;of your assessments.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Vous avez oublié de sélectionner les &lt;b&gt;horaires&lt;/b&gt;&lt;br/&gt;de vos évaluations.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="1881"/>
        <source>Calculating the table: &lt;b&gt;{0}&lt;/b&gt;</source>
        <translation>Calcul de la table : &lt;b&gt;{0}&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3224"/>
        <source>CheckSchoolReportsStatusTip</source>
        <translation>Liste par classe des matières qui apparaissent sur les bulletins</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5074"/>
        <source>Absences</source>
        <translation>Absences</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="131"/>
        <source>To create a new group of students</source>
        <translation>Pour créer un nouveau groupe d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3665"/>
        <source>VS note</source>
        <translation>Note de vie scolaire</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8667"/>
        <source>Name of Item or Balance</source>
        <translation>Nom de l&apos;item ou du bilan</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="1248"/>
        <source>{0} at {1}</source>
        <translation>{0} à {1}</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="206"/>
        <source>ExportActualVue2Print</source>
        <translation>Imprimer la vue actuelle</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="208"/>
        <source>ExportActualVue2PrintStatusTip</source>
        <translation>Imprimer la vue actuellement affichée</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="212"/>
        <source>ExportActualVue2pdf</source>
        <translation>Exporter la vue actuelle en pdf</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="214"/>
        <source>ExportActualVue2pdfStatusTip</source>
        <translation>Exporter la vue actuellement affichée vers un fichier pdf</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="218"/>
        <source>ExportAll2ods</source>
        <translation>Exporter tout en ods</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="221"/>
        <source>ExportAll2odsStatusTip</source>
        <translation>Exporter toutes les vues en un fichier tableur ODF (*.ods)</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1749"/>
        <source>Select</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="1022"/>
        <source>Deselect</source>
        <translation>Désélectionner</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="1030"/>
        <source>ChangeLabel2</source>
        <translation>Changer la description complémentaire</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="509"/>
        <source>defines</source>
        <translation>définitions</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2455"/>
        <source>&lt;p&gt;&lt;b&gt;Delete This Count:&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continue ?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Supprimer ce comptage :&lt;/b&gt;&lt;/p&gt; {0} &lt;p&gt;&lt;b&gt;Continuer ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5739"/>
        <source>DeleteCount</source>
        <translation>Supprimer un comptage</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4223"/>
        <source>EditCount</source>
        <translation>Éditer un comptage</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4226"/>
        <source>CreateCount</source>
        <translation>Créer un comptage</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4254"/>
        <source>Sense:</source>
        <translation>Sens :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4258"/>
        <source>Growing</source>
        <translation>Croissant</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4259"/>
        <source>Decreasing</source>
        <translation>Décroissant</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9578"/>
        <source>Growing Sense</source>
        <translation>sens croissant</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9582"/>
        <source>Decreasing Sense</source>
        <translation>sens décroissant</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="696"/>
        <source>Delays</source>
        <translation>Retards</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9470"/>
        <source>Number of delays</source>
        <translation>Nombre de retards</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4266"/>
        <source>Calculation:</source>
        <translation>Calcul :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9586"/>
        <source>Average and Standard deviation</source>
        <translation>Moyenne et Écart-type</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9590"/>
        <source>Median and Quartiles</source>
        <translation>Médiane et Quartiles</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4689"/>
        <source>OrderCounts</source>
        <translation>Ordre d&apos;affichage des comptages</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4691"/>
        <source>OrderNotes</source>
        <translation>Ordre d&apos;affichage des notes</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1381"/>
        <source>ShowIntermediateLinks</source>
        <translation>Liens intermédiaires</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="1613"/>
        <source>ShowIntermediateLinksStatusTip</source>
        <translation>Afficher les liens intermédiaires</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="193"/>
        <source>Chronological</source>
        <translation>Ordre chronologique</translation>
    </message>
    <message>
        <location filename="../libs/prof_suivis.py" line="195"/>
        <source>ChronologicalStatusTip</source>
        <translation>inverse l&apos;ordre d&apos;affichage des dates</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3230"/>
        <source>NO NOTES</source>
        <translation>PAS DE NOTES</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3232"/>
        <source>NO PERSONAL BALANCE</source>
        <translation>PAS DE BILANS PERSONNELS</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="802"/>
        <source>Unable to establish a database connection.
({0})

Click Cancel to exit.</source>
        <translation>Impossible d&apos;établir une connexion avec la base de données.
({0})

Cliquez sur Annuler pour sortir.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1532"/>
        <source>Would you also post the referential DB?</source>
        <translation>Voulez-vous aussi poster la base du référentiel ?</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4328"/>
        <source>FirstName</source>
        <translation>Prénom</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4330"/>
        <source>CODE</source>
        <translation>CODE</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4331"/>
        <source>SUBJECT</source>
        <translation>MATIÈRE</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="637"/>
        <source>View Details assessments</source>
        <translation>Afficher les détails des évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="305"/>
        <source>Groups</source>
        <translation>Groupes</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="798"/>
        <source>You can open the file directly&lt;br/&gt;if the file type is associated with a software.</source>
        <translation>Vous pouvez ouvrir le fichier directement&lt;br/&gt;si ce type de fichier est associé à un logiciel.</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="339"/>
        <source>ActualGroup</source>
        <translation>Groupe actuel</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="341"/>
        <source>AllGroupsForEachSstudent</source>
        <translation>Tous les groupes auxquels appartient chaque élève</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="347"/>
        <source>CompetencesOtherShared</source>
        <translation>Autres compétences partagées</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="350"/>
        <source>CompetencesPersoBLT</source>
        <translation>Compétences personnelles du bulletin</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="344"/>
        <source>CompetencesBulletinShared</source>
        <translation>Compétences de la partie partagée du bulletin</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10155"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="184"/>
        <source>Empty</source>
        <translation>Vide</translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="185"/>
        <source>&lt;b&gt;FTP connection test:&lt;/b&gt; (Host: {0}, Password: {1}, Login: {2})</source>
        <translation>&lt;b&gt;Test de connexion FTP :&lt;/b&gt; (Hôte : {0}, Mot de passe : {1}, Connexion : {2})</translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="242"/>
        <source>FTP Password:</source>
        <translation>Mot de passe FTP :</translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="529"/>
        <source>Download the file: {0}</source>
        <translation>Téléchargement du fichier : {0}</translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="622"/>
        <source>Upload the file: {0}</source>
        <translation>Envoi du fichier : {0}</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3502"/>
        <source>&lt;p&gt;No note in subject:&lt;br/&gt;&lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt;for student:&lt;br/&gt;&lt;b&gt;{1}&lt;/b&gt;.&lt;br/&gt;&lt;br/&gt;Select what to write:&lt;/p&gt;</source>
        <translation>&lt;p&gt;Pas de note dans la matière :&lt;br/&gt;&lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt;pour l&apos;élève :&lt;br/&gt;&lt;b&gt;{1}&lt;/b&gt;.&lt;br/&gt;&lt;br/&gt;Sélectionnez ce qu&apos;il faut écrire :&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9919"/>
        <source>Socle3</source>
        <translation>Socle3</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9922"/>
        <source>NiveauA2</source>
        <translation>NiveauA2</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9928"/>
        <source>If a remark is written, it will replace the opinion.</source>
        <translation>Si une remarque est écrite, elle remplacera l&apos;avis.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9930"/>
        <source>Remark (optional)</source>
        <translation>Remarque (optionnel)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4828"/>
        <source>LIST EMPTY</source>
        <translation>LISTE VIDE</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2270"/>
        <source>Making the list of students</source>
        <translation>Fabrication de la liste des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2814"/>
        <source>Creating files:</source>
        <translation>Création des fichiers :</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="144"/>
        <source>VeryFavorable</source>
        <translation>Avis très favorable</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="145"/>
        <source>Favorable</source>
        <translation>Avis favorable</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="146"/>
        <source>MustProve</source>
        <translation>Doit faire ses preuves</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="147"/>
        <source>Unfavorable</source>
        <translation>Avis défavorable</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2351"/>
        <source>NO RESULTS</source>
        <translation>PAS DE RESULTATS</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="139"/>
        <source>EMPTY</source>
        <translation>VIDE</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="801"/>
        <source>VERAC</source>
        <translation>VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1525"/>
        <source>Click for change</source>
        <translation>Cliquez pour modifier</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1492"/>
        <source>Select and Press a Key for change</source>
        <translation>Sélectionnez et appuyez sur une touche du clavier pour modifier</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1541"/>
        <source>Select color</source>
        <translation>Sélectionner une couleur</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="242"/>
        <source>Remove the selected school</source>
        <translation>Supprimer l&apos;établissement sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="522"/>
        <source>Recup</source>
        <translation>Récupération</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="522"/>
        <source>RecupStatusTip</source>
        <translation>Mettre à jour les résultats</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="530"/>
        <source>LastReport</source>
        <translation>Rapport</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="530"/>
        <source>LastReportStatusTip</source>
        <translation>Voir le rapport de la dernière récupération</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="538"/>
        <source>GotoSiteEtab</source>
        <translation>Site web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="538"/>
        <source>GotoSiteEtabStatusTip</source>
        <translation>Visiter le site web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="546"/>
        <source>Verac</source>
        <translation>VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="546"/>
        <source>VeracStatusTip</source>
        <translation>Lancer VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="554"/>
        <source>ConfigStatusTip</source>
        <translation>Préférences et configuration du logiciel</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="562"/>
        <source>&amp;About</source>
        <translation>À &amp;propos</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="562"/>
        <source>AboutStatusTip</source>
        <translation>Affiche la fenêtre &quot;À propos&quot; de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="569"/>
        <source>HelpPageStatusTip</source>
        <translation>Ouvre l&apos;aide en ligne dans votre navigateur</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="576"/>
        <source>E&amp;xit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5816"/>
        <source>QuitStatusTip</source>
        <translation>Quitter VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1939"/>
        <source>Use a proxy</source>
        <translation>Utiliser un proxy</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1943"/>
        <source>HTTP Proxy:</source>
        <translation>Proxy HTTP :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1947"/>
        <source>Port:</source>
        <translation>Port :</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="127"/>
        <source>Select the version</source>
        <translation>Sélectionnez la version</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="431"/>
        <source>Select a teacher</source>
        <translation>Sélectionnez un professeur</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="226"/>
        <source>Select another folder</source>
        <translation>Sélectionner un autre dossier</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="154"/>
        <source>Connect</source>
        <translation>Se connecter</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="189"/>
        <source>Administrator</source>
        <translation>Administrateur</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="384"/>
        <source>Add a school (internet address)</source>
        <translation>Ajouter un établissement (par adresse internet)</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="387"/>
        <source>Add a school (file)</source>
        <translation>Ajouter un établissement (par fichier)</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="531"/>
        <source>Open New School File</source>
        <translation>Ouvrir un fichier de nouvel établissement</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="575"/>
        <source>Are you certain to want to remove this school: &lt;br/&gt;&lt;b&gt;{0}&lt;b&gt; ?</source>
        <translation>Êtes-vous certain de vouloir supprimer cet établissement : &lt;br/&gt;&lt;b&gt;{0}&lt;b&gt; ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="573"/>
        <source>Remove a school</source>
        <translation>Supprimer un établisssement</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1821"/>
        <source>Choose File</source>
        <translation>Choisir un fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1821"/>
        <source>All files (*.*)</source>
        <translation>Tous fichiers (*.*)</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="87"/>
        <source>About &lt;b&gt;{0} {1}&lt;/b&gt;:&lt;p&gt;{2}&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</source>
        <translation>À propos de &lt;b&gt;{0} {1}&lt;/b&gt; :&lt;p&gt;{2}&lt;/p&gt;&lt;p&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="105"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="118"/>
        <source>Credits</source>
        <translation>Crédits</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="113"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="133"/>
        <source>About {0}</source>
        <translation>À propos de {0}</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="82"/>
        <source>Editor Bar</source>
        <translation>Barre d&apos;édition</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="133"/>
        <source>EditCut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="138"/>
        <source>EditCopy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="143"/>
        <source>EditPaste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="149"/>
        <source>FormatBold</source>
        <translation>Gras</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="154"/>
        <source>FormatItalic</source>
        <translation>Italique</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="159"/>
        <source>FormatUnderline</source>
        <translation>Souligné</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="188"/>
        <source>FormatStrikethrough</source>
        <translation>Barré</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="196"/>
        <source>FormatAlignLeft</source>
        <translation>Aligné à gauche</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="203"/>
        <source>FormatAlignCenter</source>
        <translation>Centré</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="210"/>
        <source>FormatAlignRight</source>
        <translation>Aligné à droite</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="217"/>
        <source>FormatAlignJustify</source>
        <translation>Justifié</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="226"/>
        <source>FormatIncreaseIndent</source>
        <translation>Augmenter le retrait</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="234"/>
        <source>FormatDecreaseIndent</source>
        <translation>Diminuer le retrait</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="242"/>
        <source>FormatNumberedList</source>
        <translation>Numérotation</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="250"/>
        <source>FormatBulletedList</source>
        <translation>Puces</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="260"/>
        <source>InsertImage</source>
        <translation>Insérer une image</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="266"/>
        <source>CreateLink</source>
        <translation>Créer un lien</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="273"/>
        <source>StyleParagraph</source>
        <translation>Paragraphe</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="279"/>
        <source>StyleHeading1</source>
        <translation>Titre 1</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="285"/>
        <source>StyleHeading2</source>
        <translation>Titre 2</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="291"/>
        <source>StyleHeading3</source>
        <translation>Titre 3</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="297"/>
        <source>StyleHeading4</source>
        <translation>Titre 4</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="323"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="327"/>
        <source>&amp;Format</source>
        <translation>&amp;Format</translation>
    </message>
    <message>
        <location filename="../libs/utils_htmleditor.py" line="376"/>
        <source>&amp;Style</source>
        <translation>&amp;Style</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="280"/>
        <source>Downloading {0}.</source>
        <translation>Téléchargement de {0}.</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="283"/>
        <source>Uploading {0}.</source>
        <translation>Envoi de {0}.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="331"/>
        <source>You should download it.</source>
        <translation>Vous devriez le télécharger.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="846"/>
        <source>ClearEditLog</source>
        <translation>Effacer les messages</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1285"/>
        <source>The persoBase has been modified.
Do you want to save your changes?</source>
        <translation>La base personnelle a été modifiée.
Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1326"/>
        <source>verac_admin (AdminDir)</source>
        <translation>verac_admin (dossier administrateur)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1329"/>
        <source>files (ProfsFilesDir)</source>
        <translation>files (dossier des fichiers des profs)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1332"/>
        <source>pdf (PdfFilesDir)</source>
        <translation>pdf (dossier des fichiers pdf)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1335"/>
        <source>documents (DocsElevesDir)</source>
        <translation>documents (dossier des documents pour les élèves)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1338"/>
        <source>docsprofs (DocsProfsDir)</source>
        <translation>docsprofs (dossier des documents pour les profs)</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="497"/>
        <source>Open Database File</source>
        <translation>Ouvrir une base de données</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="497"/>
        <source>Database files (*.sqlite)</source>
        <translation>Fichiers base de données (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2984"/>
        <source>Open csv File</source>
        <translation>Ouvrir un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1091"/>
        <source>Open xml or zip File</source>
        <translation>Ouvrir un fichier xml ou zip</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1091"/>
        <source>xml or zip files (*.xml *.zip)</source>
        <translation>fichiers xml ou zip (*.xml *.zip)</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="478"/>
        <source>All Periodes</source>
        <translation>Toutes les périodes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3816"/>
        <source>Clear</source>
        <translation>Effacer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5588"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5597"/>
        <source>Paste</source>
        <translation>Coller</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3982"/>
        <source>UnLockStatusTip</source>
        <translation>Déverrouiller les modifications</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3984"/>
        <source>Unlock</source>
        <translation>Déverrouiller</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5663"/>
        <source>LockStatusTip</source>
        <translation>Verrouiller les modifications</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5663"/>
        <source>Lock</source>
        <translation>Verrouiller</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3994"/>
        <source>UnShiftStatusTip</source>
        <translation>Désactive l&apos;inversion de la touche Majuscule</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3996"/>
        <source>UnShift</source>
        <translation>Enlever Majuscule</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5672"/>
        <source>ShiftStatusTip</source>
        <translation>Simule la touche Majuscule (inversion de son fonctionnement) ; cela évite de la tenir enfoncée</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5672"/>
        <source>Shift</source>
        <translation>Touche Majuscule (Shift)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5681"/>
        <source>Autoselect-no</source>
        <translation>Sélection automatique : désactivée</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4015"/>
        <source>Autoselect-1</source>
        <translation>Sélection automatique : de haut en bas puis de gauche à droite</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4019"/>
        <source>Autoselect-2</source>
        <translation>Sélection automatique : de gauche à droite puis de haut en bas</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6822"/>
        <source>FontSize: {0}</source>
        <translation>Taille des polices d&apos;écriture : {0}</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4246"/>
        <source>Choose the Directory where the desktop file will be created</source>
        <translation>Choisissez le dossier où le raccourcis sera créé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4442"/>
        <source>OpenAdminDir</source>
        <translation>Ouvrir le dossier verac_admin</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4442"/>
        <source>OpenAdminDirStatusTip</source>
        <translation>Ouvre le dossier administrateur dans votre gestionnaire de fichiers</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4451"/>
        <source>LaunchScheduler</source>
        <translation>Lancer le planificateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4451"/>
        <source>LaunchSchedulerStatusTip</source>
        <translation>Lancer le planificateur de récupérations automatiques</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="387"/>
        <source>OpenDBStatusTip</source>
        <translation>Ouvrir une base de données SQLITE</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5556"/>
        <source>EditCsvFile</source>
        <translation>Éditer un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5556"/>
        <source>EditCsvFileStatusTip</source>
        <translation>Une interface pour modifier directement des fichiers csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4482"/>
        <source>CreateAdminTableFromCsv</source>
        <translation>Créer une table de la base admin</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4482"/>
        <source>CreateAdminTableFromCsvStatusTip</source>
        <translation>Créer une table de la base admin depuis le fichier csv correspondant</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4490"/>
        <source>CreateCommunTableFromCsv</source>
        <translation>Créer une table de la base commun</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4490"/>
        <source>CreateCommunTableFromCsvStatusTip</source>
        <translation>Créer une table de la base commun depuis le fichier csv correspondant</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4499"/>
        <source>CreateAllCommunTablesFromCsv</source>
        <translation>Créer toutes les tables de la base commun</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4499"/>
        <source>CreateAllCommunTablesFromCsvStatusTip</source>
        <translation>Créer en une fois toutes les tables de la base commun à partir des fichiers csv correspondants</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4562"/>
        <source>Update students from SIECLE</source>
        <translation>Mettre à jour les élèves depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4562"/>
        <source>Update students from ElevesAvecAdresses.xml file exported from SIECLE</source>
        <translation>Mettre à jour les élèves depuis le fichier ElevesAvecAdresses.xml exporté depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="832"/>
        <source>Update teachers from STSWeb</source>
        <translation>Mettre à jour les profs depuis STSWeb</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="836"/>
        <source>Update teachers from sts_emp_RNE_aaaa.xml file exported from STSWeb</source>
        <translation>Mettre à jour les profs depuis le fichier sts_emp_RNE_aaaa.xml exporté depuis STSWeb</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4581"/>
        <source>Update students from BASE ELEVES</source>
        <translation type="obsolete">Mettre à jour les élèves depuis BASE ÉLÈVES</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4581"/>
        <source>Update students from CSVExtraction.csv file exported from BASE ELEVES</source>
        <translation type="obsolete">Mettre à jour les élèves depuis le fichier CSVExtraction.csv créé par BASE ÉLÈVES</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4538"/>
        <source>DownloadDBUsers</source>
        <translation>Télécharger la base des utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4538"/>
        <source>DownloadDBUsersStatusTip</source>
        <translation>Télécharger la base des utilisateurs (à faire avant de la modifier)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4545"/>
        <source>UploadDBUsers</source>
        <translation>Poster la base des utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4545"/>
        <source>UploadDBUsersStatusTip</source>
        <translation>Poster la base des utilisateurs (à faire après modifications)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2875"/>
        <source>Select competences of the school report list among the proposed models</source>
        <translation>Choisir la liste des compétences du bulletin parmi les modèles proposés</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2885"/>
        <source>Select referential among the proposed models</source>
        <translation>Choisir le référentiel parmi les modèles proposés</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2894"/>
        <source>Select confidential competences among the proposed models</source>
        <translation>Choisir les compétences confidentielles parmi les modèles proposés</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2904"/>
        <source>Select followed competences among the proposed models</source>
        <translation>Choisir les compétences suivies parmi les modèles proposés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4581"/>
        <source>ShowMdpProfsNoChangeStatusTip</source>
        <translation>Affiche la liste des professeurs n&apos;ayant pas changé leur mot de passe (d&apos;après la base users)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4592"/>
        <source>Reset password (teachers)</source>
        <translation>Réinitialiser des mots de passe (profs)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4592"/>
        <source>Reset password (teachers selected from a list)</source>
        <translation>Réinitialiser des mots de passe (profs sélectionnés dans une liste)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4601"/>
        <source>Change password (teachers)</source>
        <translation>Changer des mots de passe (profs)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4601"/>
        <source>Change password (teachers selected from a list)</source>
        <translation>Changer des mots de passe (profs sélectionnés dans une liste)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4572"/>
        <source>Reset password (students)</source>
        <translation>Réinitialiser des mots de passe (élèves)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4572"/>
        <source>Reset password (students selected from a list)</source>
        <translation>Réinitialiser des mots de passe (élèves sélectionnés dans une liste)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4589"/>
        <source>CreateConnectionFile</source>
        <translation>Créer le fichier de connexion</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4589"/>
        <source>CreateConnectionFileStatusTip</source>
        <translation>Créer le fichier de connexion à l&apos;établissement (*.tar.gz)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4599"/>
        <source>UploadDBCommun</source>
        <translation>Poster la base commun</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4599"/>
        <source>UploadDBCommunStatusTip</source>
        <translation>Poster la base commun</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4608"/>
        <source>CreateYearArchiveAndClear</source>
        <translation>Créer une archive et nettoyer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4608"/>
        <source>CreateYearArchiveAndClearStatusTip</source>
        <translation>Enchaîne les 2 actions (archivage de l&apos;année actuelle et nettoyage pour une nouvelle année)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5883"/>
        <source>CreateYearArchive</source>
        <translation>Créer une archive de l&apos;année actuelle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5883"/>
        <source>CreateYearArchiveStatusTip</source>
        <translation>Créer une version archivée de toutes les données (et même du logiciel)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4624"/>
        <source>Clears the current year data (databases, files, etc.)</source>
        <translation>Efface les données concernant l&apos;année actuelle (bases, fichiers, etc)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4640"/>
        <source>Update results (only new files)</source>
        <translation>Mettre à jour les résultats (seulement les nouveaux fichiers)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4647"/>
        <source>Update Results (recalculate everything)</source>
        <translation>Mettre à jour les résultats (tout recalculer)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4656"/>
        <source>Update results of a selection</source>
        <translation>Mettre à jour les résultats d&apos;une sélection</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4656"/>
        <source>Select the teachers and students which we want to update the results</source>
        <translation>Sélectionner les profs et les élèves dont on veut mettre à jour les résultats</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4666"/>
        <source>Calculates the validation proposals of the competences referential</source>
        <translation>Calcule les propositions de validation du référentiel de compétences</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4684"/>
        <source>Post the results DB</source>
        <translation>Poster la base des résultats</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4684"/>
        <source>Post the results DB (and possible proposals to the referential)</source>
        <translation>Poster la base des résultats (et éventuellement des propositions pour le référentiel)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4697"/>
        <source>Create reports</source>
        <translation>Créer des bulletins</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4735"/>
        <source>ShowFields</source>
        <translation>Voir les champs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4735"/>
        <source>ShowFieldsStatusTip</source>
        <translation>Voir la liste des champs disponibles dans vos modèles</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4742"/>
        <source>Create a school report or other record template</source>
        <translation>Créer un modèle de bulletin ou autre relevé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4742"/>
        <source>Create an HTML template (for a school report, balances report, referential report, etc.)</source>
        <translation>Créer un modèle html (pour un bulletin, relevé de bilans, relévé du référentiel, etc)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4706"/>
        <source>ExportNotes2ods</source>
        <translation>Exporter des notes en ods</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4706"/>
        <source>ExportNotes2odsStatusTip</source>
        <translation>Exporter les notes d&apos;une sélection d&apos;élèves en un fichier tableur ODF (*.ods)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4770"/>
        <source>List files and provides information on their content</source>
        <translation>Liste les fichiers et donne des renseignements sur leur contenu</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4777"/>
        <source>CheckSchoolReports</source>
        <translation>Vérification des bulletins</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4791"/>
        <source>RedoMdp</source>
        <translation>Resaisir le mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4791"/>
        <source>RedoMdpStatusTip</source>
        <translation>Resaisir le mot de passe FTP (en cas d&apos;erreur)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4812"/>
        <source>SwitchToNextPeriodStatusTip</source>
        <translation>Crée une archive de la période actuelle et sélectionne la période suivante</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4819"/>
        <source>To view and edit the list of protected periods (common and results bases)</source>
        <translation>Pour afficher et modifier la liste des périodes protégées (bases commun et resultats)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4855"/>
        <source>To change what is available on the website</source>
        <translation>Pour modifier ce qui est disponible sur le site Web</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4872"/>
        <source>ShowCompteurAnalyzeStatusTip</source>
        <translation>Pour visualiser les statistiques de visites du site Web (à partir de la base compteur)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4892"/>
        <source>SaveDBMy</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4892"/>
        <source>SaveDBMyStatusTip</source>
        <translation>Enregistrer la base de données personnelle localement (sur l&apos;ordinateur que vous utilisez)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4900"/>
        <source>PostDBMy</source>
        <translation>Envoyer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4900"/>
        <source>PostDBMyStatusTip</source>
        <translation>Envoyer la base de données personnelle sur le site Web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4907"/>
        <source>SaveAndPostDBMy</source>
        <translation>Enregistrer et envoyer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4907"/>
        <source>SaveAndPostDBMyStatusTip</source>
        <translation>Enregistrer la base de données personnelle et l&apos;envoyer sur le site Web</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4923"/>
        <source>SaveAsDBMy...</source>
        <translation>Enregistrer la base de données personnelle sous ...</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4923"/>
        <source>SaveAsDBMyStatusTip</source>
        <translation>Pour faire une sauvegarde de la base de données personnelle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4930"/>
        <source>CompareDBMy</source>
        <translation>Comparer les dates des bases locales et distantes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4930"/>
        <source>CompareDBMyStatusTip</source>
        <translation>Comparer les dates des bases locales et distantes (savoir laquelle est la plus récente)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4937"/>
        <source>DownloadDBMy</source>
        <translation>Télécharger la base distante</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4937"/>
        <source>DownloadDBMyStatusTip</source>
        <translation>Télécharger la base distante</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4944"/>
        <source>PostSuivis</source>
        <translation>Envoyer le suivi des élèves</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4944"/>
        <source>PostSuivisStatusTip</source>
        <translation>Envoyer vos évaluations et appréciations de la vue Suivis sur le site Web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4961"/>
        <source>ChangeMdpStatusTip</source>
        <translation>Changer de mot de passe (sur le site et sur cet ordinateur)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4975"/>
        <source>CheckCommunBilans</source>
        <translation>Vérifier les bilans communs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4975"/>
        <source>CheckCommunBilansStatusTip</source>
        <translation>Vérifie la cohérence des bilans issus du bulletin, référentiel ou confidentiel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4982"/>
        <source>DownloadSuivisDB</source>
        <translation>Télécharger la base des élèves suivis</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4982"/>
        <source>DownloadSuivisDBStatusTip</source>
        <translation>Télécharger la base des élèves suivis</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5861"/>
        <source>TestUpdateVerac</source>
        <translation>Tester votre version de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5861"/>
        <source>TestUpdateVeracStatusTip</source>
        <translation>Vérifier si une mise à jour de VÉRAC est disponible</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5868"/>
        <source>UpdateVerac</source>
        <translation>Mettre VÉRAC à jour</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5868"/>
        <source>UpdateVeracStatusTip</source>
        <translation>Télécharger et mettre à jour la version de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5875"/>
        <source>Parameters</source>
        <translation>Paramètres</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4992"/>
        <source>CreateEtabStatusTip</source>
        <translation>Créer un nouvel établissement dont vous serez l&apos;administrateur (à partir du dossier verac_admin)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4999"/>
        <source>Become administrator of the Personal Version</source>
        <translation>Devenir administrateur de la version perso</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4999"/>
        <source>Allows you to manage the lists of students, edit bulletins, etc.StatusTip</source>
        <translation>Permet de gérer les listes d&apos;élèves, d&apos;éditer des bulletins, etc</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5021"/>
        <source>ShowNewsStatusTip</source>
        <translation>Afficher les nouveautés de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5021"/>
        <source>News</source>
        <translation>Nouveautés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5038"/>
        <source>ShowItemsStatusTip</source>
        <translation>Afficher les items du tableau actuel et pouvoir les modifier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5042"/>
        <source>ShowStatsTableauStatusTip</source>
        <translation>Afficher les statistiques du tableau actuel (calculées sur les items)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5042"/>
        <source>StatsTableau</source>
        <translation>Statistiques (tableau)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5046"/>
        <source>ShowBilansStatusTip</source>
        <translation>Afficher les bilans auxquels participe le tableau actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5050"/>
        <source>ShowBulletinStatusTip</source>
        <translation>Afficher le bulletin pour la période et le groupe d&apos;élèves sélectionnés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5050"/>
        <source>Bulletin</source>
        <translation>Bulletin</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5054"/>
        <source>ShowAppreciationsStatusTip</source>
        <translation>Afficher les appréciations du groupe d&apos;élèves sélectionné (toutes périodes)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5054"/>
        <source>Appreciations</source>
        <translation>Appréciations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5058"/>
        <source>ShowStatsGroupeStatusTip</source>
        <translation>Afficher les statistiques du groupe actuel (calculées sur les bilans)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5058"/>
        <source>StatsGroupe</source>
        <translation>Statistiques (groupe)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5062"/>
        <source>Per student</source>
        <translation>Par élève</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5062"/>
        <source>ShowEleveStatusTip</source>
        <translation>Afficher les détails des résultats d&apos;un élève</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5066"/>
        <source>ShowNotesStatusTip</source>
        <translation>Afficher les notes pour la période et le groupe d&apos;élèves sélectionnés</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7520"/>
        <source>Notes</source>
        <translation>Notes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5070"/>
        <source>Followed students</source>
        <translation>Élèves suivis</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5070"/>
        <source>ShowSuivisStatusTip</source>
        <translation>Afficher les élèves du groupe pour lesquels le Prof Principal souhaite un suivi</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5074"/>
        <source>ShowAbsencesStatusTip</source>
        <translation>Afficher les absences et retards pour la période et le groupe d&apos;élèves sélectionnés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5078"/>
        <source>ShowCountsStatusTip</source>
        <translation>Pour transformer des comptages en évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1118"/>
        <source>Counts</source>
        <translation>Comptages</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5090"/>
        <source>DNB Opinion</source>
        <translation>Avis DNB</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5090"/>
        <source>ShowDNBStatusTip</source>
        <translation>Pour saisir votre avis et la validation du socle sur les fiches DNB</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5137"/>
        <source>CreateMultipleGroupesClassesStatusTip</source>
        <translation>Pour créer en une seule fois plusieurs groupes similaires pour des classes différentes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5144"/>
        <source>Directly opens the window for creating a new group</source>
        <translation>Ouvre directement la fenêtre de création d&apos;un nouveau groupe</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5152"/>
        <source>ShowDeletedEleves</source>
        <translation>Afficher les élèves retirés des groupes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5152"/>
        <source>ShowDeletedElevesStatusTip</source>
        <translation>Affiche ou non dans les différentes vues les élèves qui ont été retirés des groupes</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1799"/>
        <source>Clear students deleted from groups</source>
        <translation>Effacer les élèves retirés des groupes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5160"/>
        <source>Clear data of students deleted from groups</source>
        <translation>Effacer les données des élèves retirés des groupes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5168"/>
        <source>GestItemsStatusTip</source>
        <translation>Gérer les items, les bilans et les relier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5175"/>
        <source>GestItemsBilansStatusTip</source>
        <translation>Gérer les items, les bilans et les relier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5182"/>
        <source>ItemsBilansTableStatusTip</source>
        <translation>Gérer les items, les bilans et leurs liens à l&apos;aide d&apos;un tableau</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5189"/>
        <source>LinkItemsToBilanStatusTip</source>
        <translation>Pour relier en une fois plusieurs items à un même bilan</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5196"/>
        <source>LinkBilansStatusTip</source>
        <translation>Si 2 bilans sont liés, il suffit de relier des items au premier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5203"/>
        <source>GestAdvicesStatusTip</source>
        <translation>Écrire des conseils liés à vos items et bilans que les élèves pourront voir dans l&apos;interface Web</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5211"/>
        <source>CreateTableauStatusTip</source>
        <translation>Créer un nouveau tableau d&apos;évaluations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5218"/>
        <source>CopyTableau</source>
        <translation>Cloner le tableau actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5218"/>
        <source>CopyTableauStatusTip</source>
        <translation>Créer un nouveau tableau à partir du tableau actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5225"/>
        <source>SupprTableau</source>
        <translation>Supprimer le tableau actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5225"/>
        <source>SupprTableauStatusTip</source>
        <translation>Supprimer le tableau actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5232"/>
        <source>CopyItemsFromToStatusTip</source>
        <translation>Ajoute à des tableaux les items d&apos;un autre et qu&apos;ils n&apos;ont pas</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5239"/>
        <source>MergeTableauxStatusTip</source>
        <translation>Fusionner 2 tableaux pour n&apos;en faire qu&apos;un seul</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5246"/>
        <source>SortGroupesTableauxStatusTip</source>
        <translation>Organiser l&apos;ordre d&apos;affichage des groupes et des tableaux</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5253"/>
        <source>CreateMultipleTableauxStatusTip</source>
        <translation>Pour créer en une seule fois plusieurs tableaux similaires pour des groupes différents</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1115"/>
        <source>Tables templates</source>
        <translation>Modèles de tableaux</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5260"/>
        <source>Tables templatesStatusTip</source>
        <translation>Pour gérer les modèles de tableaux et les tableaux qui leur sont liés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5267"/>
        <source>CreateTemplateFromTableau</source>
        <translation>Créer un modèle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5267"/>
        <source>CreateTemplateFromTableauStatusTip</source>
        <translation>Pour créer un nouveau modèle de tableau d&apos;après le tableau actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5274"/>
        <source>LinkTableauTemplate</source>
        <translation>Lier à un modèle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5274"/>
        <source>LinkTableauTemplateStatusTip</source>
        <translation>Permet de lier le tableau actuel à un modèle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5281"/>
        <source>UnlinkTableauTemplate</source>
        <translation>Délier du modèle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5281"/>
        <source>UnlinkTableauTemplateStatusTip</source>
        <translation>Pour que le tableau actuel ne soit plus lié à son modèle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5298"/>
        <source>Change the list of items in the current table (to add, remove, reorder)</source>
        <translation>Permet de modifier la liste des items du tableau actuel (pour en ajouter, en enlever, les réordonner)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5307"/>
        <source>Managing the display of the current items array (visibility and order)</source>
        <translation>Gestion de l&apos;affichage des items du tableau actuel (visibilité et ordre)</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1939"/>
        <source>Manage profiles (which will be on the bulletins)</source>
        <translation>Gérer les profils (ce qui sera sur les bulletins)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5324"/>
        <source>Create or edit profiles, balance sheets shown in the bulletins, etc.</source>
        <translation>Créer ou modifier les profils, les bilans affichés dans le bulletin, etc.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5333"/>
        <source>GestElevesSuivisStatusTip</source>
        <translation>Pour sélectionner les élèves et les compétences pour lesquels vous voulez un suivi</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5355"/>
        <source>ExportActualVue2ods</source>
        <translation>Exporter la vue actuelle en ods</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5355"/>
        <source>ExportActualVue2odsStatusTip</source>
        <translation>Exporter la vue actuellement affichée en un fichier tableur ODF (*.ods)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5362"/>
        <source>ExportActualGroup2ods</source>
        <translation>Exporter le groupe actuel en ods</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5362"/>
        <source>ExportActualGroup2odsStatusTip</source>
        <translation>Exporte la sélection (groupe + période) en fichier ods</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5369"/>
        <source>ExportStructure2ods</source>
        <translation>Exporter la structure en ods</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5369"/>
        <source>ExportStructure2odsStatusTip</source>
        <translation>Exporter la structure en un fichier tableur ODF (*.ods)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5410"/>
        <source>ExportBilansItems2Freeplane</source>
        <translation>Exporter bilans et items vers Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5410"/>
        <source>ExportBilansItems2FreeplaneStatusTip</source>
        <translation>Exporter les bilans et les items vers Freeplane (logiciel de création de cartes heuristiques)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5417"/>
        <source>ExportActualGroup2Freeplane</source>
        <translation>Exporter le groupe actuel vers Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5417"/>
        <source>ExportActualGroup2FreeplaneStatusTip</source>
        <translation>Exporte les évaluations (bilans + items) de la sélection (groupe + période) en fichier Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5424"/>
        <source>ExportAllGroups2Freeplane</source>
        <translation>Exporter tous les groupes vers Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5424"/>
        <source>ExportAllGroups2FreeplaneStatusTip</source>
        <translation>Exporte les évaluations de tous vos groupes pour la période sélectionnée en fichier Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5431"/>
        <source>ExportEleves2Freeplane</source>
        <translation>Exporter des élèves vers Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5431"/>
        <source>ExportEleves2FreeplaneStatusTip</source>
        <translation>Exporter les résultats d&apos;une liste d&apos;élèves vers Freeplane</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5438"/>
        <source>ExportBilan2FreeplaneStatusTip</source>
        <translation>Exporter un bilan et les items associés vers Freeplane (logiciel de création de cartes heuristiques)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5475"/>
        <source>ExportBilansItems2Csv</source>
        <translation>Exporter bilans et items en csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5475"/>
        <source>ExportBilansItems2CsvStatusTip</source>
        <translation>Exporter les bilans et les items au format csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5482"/>
        <source>ImportBilansItemsFromCsv</source>
        <translation>Importer bilans et items depuis csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5482"/>
        <source>ImportBilansItemsFromCsvStatusTip</source>
        <translation>Importer les bilans et les items depuis un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5489"/>
        <source>ExportTemplates2Csv</source>
        <translation>Exporter des modèles en csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5489"/>
        <source>ExportTemplates2CsvStatusTip</source>
        <translation>Exporter une liste de modèles de tableaux au format csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5496"/>
        <source>ImportTemplatesFromCsv</source>
        <translation>Importer des modèles depuis csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5496"/>
        <source>ImportTemplatesFromCsvStatusTip</source>
        <translation>Importer une liste de modèles de tableaux depuis un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5528"/>
        <source>ExportEleves2Csv</source>
        <translation>Exporter la liste des élèves en csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5528"/>
        <source>ExportEleves2CsvStatusTip</source>
        <translation>Exporter la liste des élèves au format csv (peut être modifiée avec LibreOffice par exemple)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5535"/>
        <source>ImportElevesFromCsv</source>
        <translation>Importer la liste des élèves depuis csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5535"/>
        <source>ImportElevesFromCsvStatusTip</source>
        <translation>Importer la liste des élèves depuis un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5542"/>
        <source>ExportReferentiel2Csv</source>
        <translation>Exporter le référentiel en csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5542"/>
        <source>ExportReferentiel2CsvStatusTip</source>
        <translation>Exporter le référentiel au format csv (peut être modifié avec LibreOffice par exemple)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5549"/>
        <source>ImportReferentielFromCsv</source>
        <translation>Importer le référentiel depuis csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5549"/>
        <source>ImportReferentielFromCsvStatusTip</source>
        <translation>Importer le référentiel depuis un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6701"/>
        <source>ItemComboBox</source>
        <translation>Évaluer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6703"/>
        <source>ItemComboBoxStatusTip</source>
        <translation>Cette liste déroulante permet de modifier les cases sélectionnés</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5563"/>
        <source>Undo</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5563"/>
        <source>UndoStatusTip</source>
        <translation>Annuler la dernière action</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5571"/>
        <source>Redo</source>
        <translation>Refaire</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5571"/>
        <source>RedoStatusTip</source>
        <translation>Refaire la dernière action annulée</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5579"/>
        <source>Cut</source>
        <translation>Couper</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5579"/>
        <source>CutStatusTip</source>
        <translation>Efface la sélection et la copie dans le presse-papier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5588"/>
        <source>CopyStatusTip</source>
        <translation>Copie la sélection dans le presse-papier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5597"/>
        <source>PasteStatusTip</source>
        <translation>Colle le contenu du presse-papier dans la sélection</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5606"/>
        <source>SelectAll</source>
        <translation>Tout sélectionner</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5606"/>
        <source>SelectAllStatusTip</source>
        <translation>Sélectionne toutes les cases du tableau</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5614"/>
        <source>Simplify</source>
        <translation>Simplifier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5614"/>
        <source>Simplifies the selection (items values will be replaced by their average)</source>
        <translation>Simplifie la sélection (les valeurs des items seront remplacées par leur moyenne)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5642"/>
        <source>EleveNext</source>
        <translation>Élève suivant</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6781"/>
        <source>EleveNextStatusTip</source>
        <translation>Passer à l&apos;élève suivant (ou revenir au premier de la liste)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5649"/>
        <source>Recalc</source>
        <translation>Recalculer tout</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5649"/>
        <source>RecalcStatusTip</source>
        <translation>Recalculer les bilans, le bulletin, etc</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5656"/>
        <source>RecalcAllGroupesStatusTip</source>
        <translation>Recalcule les résultats de tous les groupes (cette procédure peut être longue)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5681"/>
        <source>AutoselectStatusTip</source>
        <translation>Sélection automatique de la case suivante : cliquez pour changer de mode</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5688"/>
        <source>AddNote</source>
        <translation>Ajouter une note</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5688"/>
        <source>AddNoteStatusTip</source>
        <translation>Ajouter une note à la sélection (groupe et période)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5695"/>
        <source>EditNoteStatusTip</source>
        <translation>Permet de modifier la note sélectionnée (nom, base, coefficient, ...)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5702"/>
        <source>DeleteNoteStatusTip</source>
        <translation>Supprimer la note actuellement sélectionnée</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5709"/>
        <source>OrderNotesStatusTip</source>
        <translation>Pour modifier l&apos;ordre d&apos;affichage des notes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5717"/>
        <source>CreateCountStatusTip</source>
        <translation>Pour ajouter un nouveau comptage</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5732"/>
        <source>EditCountStatusTip</source>
        <translation>Permet de modifier le comptage sélectionné (nom et sens)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5739"/>
        <source>DeleteCountStatusTip</source>
        <translation>Pour supprimer le comptage sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5746"/>
        <source>OrderCountsStatusTip</source>
        <translation>Pour modifier l&apos;ordre d&apos;affichage des comptages</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5753"/>
        <source>ValuesUp</source>
        <translation>Incrémenter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5753"/>
        <source>ValuesUpStatusTip</source>
        <translation>Augmente de 1 toutes les cases sélectionnées</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5760"/>
        <source>ValuesDown</source>
        <translation>Décrémenter</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5760"/>
        <source>ValuesDownStatusTip</source>
        <translation>Diminue de 1 toutes les cases sélectionnées</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5769"/>
        <source>FontSizeMore</source>
        <translation>Augmenter la taille des polices</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5775"/>
        <source>FontSizeLess</source>
        <translation>Diminuer la taille des polices</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5840"/>
        <source>ProjetPage</source>
        <translation>Page du projet</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5840"/>
        <source>ProjetPageStatusTip</source>
        <translation>Ouvre le site internet du projet dans votre navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5891"/>
        <source>Clears the current year data (groups, assessments, appreciations ...)</source>
        <translation>Efface les données concernant l&apos;année actuelle (groupes, évaluations, appréciations...) mais ni les items ni les bilans</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5908"/>
        <source>Create a launcher</source>
        <translation>Créer un lanceur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5908"/>
        <source>To create a launcher (*.desktop file) in the folder of your choice.</source>
        <translation>Pour créer un lanceur (fichier *.desktop) dans le dossier de votre choix.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5917"/>
        <source>Create a shortcut</source>
        <translation>Créer un raccourci</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5917"/>
        <source>To create a shortcut (*.lnk file) on your Desktop.</source>
        <translation>Pour créer un raccourci (fichier *.lnk) sur votre bureau.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5927"/>
        <source>BugReportStatusTip</source>
        <translation>Pensez à bien décrire les circonstances du problème rencontré</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5863"/>
        <source>Test</source>
        <translation>Tester</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6131"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6605"/>
        <source>OpenADir</source>
        <translation>Ouvrir un dossier</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6104"/>
        <source>CreateDbsFromCsv</source>
        <translation>Création des bases par fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4508"/>
        <source>Users management</source>
        <translation>Gestion des utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6095"/>
        <source>Utils</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6220"/>
        <source>Sho&amp;w</source>
        <translation>Afficha&amp;ge</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6234"/>
        <source>Tableau</source>
        <translation>Tableau</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6685"/>
        <source>View</source>
        <translation>Choisir la vue</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="140"/>
        <source>ToolBar</source>
        <translation>Barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6257"/>
        <source>MenuBar</source>
        <translation>Menu</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6301"/>
        <source>E&amp;vals</source>
        <translation>É&amp;valuations</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6323"/>
        <source>GroupesEleves</source>
        <translation>Groupes d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6333"/>
        <source>ItemsBilans</source>
        <translation>Items et bilans</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6344"/>
        <source>Tableaux</source>
        <translation>Tableaux</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6354"/>
        <source>ActualTableau</source>
        <translation>Tableau actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6403"/>
        <source>&amp;Utils</source>
        <translation>O&amp;utils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6449"/>
        <source>&amp;ImportExport</source>
        <translation>&amp;Import-Export</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6479"/>
        <source>ActualView</source>
        <translation>Vue actuelle</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6512"/>
        <source>Structure</source>
        <translation>Structure</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6547"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6584"/>
        <source>All Menus Button</source>
        <translation>Tous les menus</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6608"/>
        <source>OpenADirToolTip</source>
        <translation>Ouvre un dossier souvent utilisé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6616"/>
        <source>ChangePeriode</source>
        <translation>Changer de période</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6678"/>
        <source>ChooseGroup</source>
        <translation>Choisir le groupe d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6747"/>
        <source>EleveComboBox</source>
        <translation>Sélectionner un élève</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6749"/>
        <source>EleveComboBoxStatusTip</source>
        <translation>Cette liste déroulante permet de sélectionner un autre élève</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6755"/>
        <source>EleveSens</source>
        <translation>Sens d&apos;affichage</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6757"/>
        <source>EleveSensStatusTip</source>
        <translation>Cette liste déroulante permet de choisir le sens d&apos;affichage des résultats</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Balances &gt; Items</source>
        <translation>Bilans → Items</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Items &gt; Balances</source>
        <translation>Items → Bilans</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Balances &gt; Items + Opinions</source>
        <translation>Bilans → Items + Appréciation</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6760"/>
        <source>Items &gt; Balances + Opinions</source>
        <translation>Items → Bilans + Appréciation</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="159"/>
        <source>Abort</source>
        <translation>Abandonner</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="68"/>
        <source>DNB_MATIERE-FRANCAIS</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="69"/>
        <source>DNB_MATIERE-MATHS</source>
        <translation>Mathématiques</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="70"/>
        <source>DNB_MATIERE-LV1</source>
        <translation>Première langue vivante étrangère</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="71"/>
        <source>DNB_MATIERE-SVT</source>
        <translation>Sciences de la vie et de la Terre</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="72"/>
        <source>DNB_MATIERE-PHYSIQUE</source>
        <translation>Physique-chimie</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="73"/>
        <source>DNB_MATIERE-EPS</source>
        <translation>Éducation physique et sportive</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="74"/>
        <source>DNB_MATIERE-ARTS_P</source>
        <translation>Arts plastiques</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="75"/>
        <source>DNB_MATIERE-MUSIQ</source>
        <translation>Éducation musicale</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="76"/>
        <source>DNB_MATIERE-TECHNO</source>
        <translation>Technologie</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="77"/>
        <source>DNB_MATIERE-LV2</source>
        <translation>Deuxième langue vivante</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="78"/>
        <source>DNB_MATIERE-VIE_SCOL</source>
        <translation>Vie scolaire</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="79"/>
        <source>DNB_MATIERE-OPTION</source>
        <translation>Option facultative</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="80"/>
        <source>DNB_MATIERE-SOCLE_B2I</source>
        <translation>Socle B2I</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="83"/>
        <source>DNB_MATIERE-SOCLE_A2</source>
        <translation>Niveau A2 du CECRL en langue régionale</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="81"/>
        <source>DNB_MATIERE-HG</source>
        <translation>Histoire-Géographie</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="82"/>
        <source>DNB_MATIERE-EDUC_CIVIQUE</source>
        <translation>Éducation civique</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="56"/>
        <source>DNB_CODE-AB</source>
        <translation>absent</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="57"/>
        <source>DNB_CODE-DI</source>
        <translation>dispensé</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="58"/>
        <source>DNB_CODE-NN</source>
        <translation>non noté</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="60"/>
        <source>DNB_CODE-MS</source>
        <translation>maîtrise du socle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="61"/>
        <source>DNB_CODE-ME</source>
        <translation>maîtrise d&apos;éléments du socle</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="62"/>
        <source>DNB_CODE-MN</source>
        <translation>maîtrise du socle non évaluée</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="64"/>
        <source>DNB_CODE-VA</source>
        <translation>validé</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="65"/>
        <source>DNB_CODE-NV</source>
        <translation>non validé</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="218"/>
        <source>extra points</source>
        <translation>points supplémentaires</translation>
    </message>
    <message>
        <location filename="../libs/utils_dnb.py" line="253"/>
        <source>NIVEAU_A2_TEXT</source>
        <translation>Niveau A2 du CECRL en langue régionale</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5824"/>
        <source>ContextHelpPage</source>
        <translation>Aide contextuelle</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="867"/>
        <source>Opens contextual help in your browser</source>
        <translation>Ouvre l&apos;aide contextuelle dans votre navigateur</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5833"/>
        <source>HelpPageContents</source>
        <translation>Sommaire de l&apos;aide</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5833"/>
        <source>HelpPageContentsStatusTip</source>
        <translation>Ouvre le sommaire de l&apos;aide en ligne dans votre navigateur</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="353"/>
        <source>CompetencesPersoOther</source>
        <translation>Autres compétences personnelles</translation>
    </message>
    <message>
        <location filename="../libs/admin_ftp.py" line="818"/>
        <source>INCONSISTENT FILE: </source>
        <translation>FICHIER INCOHÉRENT : </translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5029"/>
        <source>ShowRandomHelpStatusTip</source>
        <translation>Affiche une page de l&apos;aide choisie au hasard</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5029"/>
        <source>RandomHelp</source>
        <translation>Une page au hasard</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6077"/>
        <source>WebSite</source>
        <translation>Site web</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4863"/>
        <source>Website configuration</source>
        <translation>Configuration du site web</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4863"/>
        <source>To update or change the configuration of your website</source>
        <translation>Pour mettre à jour ou modifier la configuration de votre site web</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4841"/>
        <source>Test version of the website</source>
        <translation>Tester la version du site web</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4841"/>
        <source>Check for an update of the web site</source>
        <translation>Vérifier si une mise à jour du site web est disponible</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4849"/>
        <source>Update website</source>
        <translation>Mettre à jour le site web</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="735"/>
        <source>(&quot;WebSite &gt; Update website&quot; menu)</source>
        <translation>(menu &quot;Site web → Mettre à jour le site web&quot;)</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="981"/>
        <source>UNABLE TO UNTAR THE NEW VERSION</source>
        <translation>IMPOSSIBLE DE DÉCOMPRESSER LA NOUVELLE VERSION</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="983"/>
        <source>Do you have the privileges to write into the installation directory?</source>
        <translation>Avez-vous les droits d&apos;écriture dans le dossier d&apos;installation ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="986"/>
        <source>You can also download the new version from the &lt;b&gt;{0}&lt;/b&gt; site:</source>
        <translation>Vous pouvez aussi télécharger la nouvelle version depuis le site de &lt;b&gt;{0}&lt;/b&gt; :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="999"/>
        <source>UNABLE TO DOWNLOAD THE NEW VERSION</source>
        <translation>IMPOSSIBLE DE TÉLÉCHARGER LA NOUVELLE VERSION</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3491"/>
        <source>Your data will be re-initialized to begin one New Year&apos;s Day school.</source>
        <translation>Vos données vont être réinitialisées pour commencer une nouvelle année scolaire.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="769"/>
        <source>Are you certain to want to continue?</source>
        <translation>Êtes-vous sûr de vouloir continuer ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="231"/>
        <source>If you leave this page without clicking&lt;br/&gt;on the button &lt;b&gt;Apply&lt;/b&gt;,&lt;br/&gt;the modifications will not be recorded.</source>
        <translation>Si vous quittez cette page sans cliquer&lt;br/&gt;sur le bouton &lt;b&gt;Appliquer&lt;/b&gt;,&lt;br/&gt;les modifications ne seront pas enregistrées.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="765"/>
        <source>Your data (except substructure) will be re-initialized to begin one New Year&apos;s Day school.</source>
        <translation>Vos données (sauf celles concernant la structure) vont être réinitialisées pour commencer une nouvelle année scolaire.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3208"/>
        <source>{0} period is not archived.</source>
        <translation>La période {0} n&apos;est pas archivée.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3210"/>
        <source>You should archive this period before selecting the next.&lt;br/&gt;See help automatically displayed in the central part of the interface.</source>
        <translation>Vous devriez archiver cette période avant de sélectionner la suivante.&lt;br/&gt;Voir l&apos;aide affichée automatiquement dans la partie centrale de l&apos;interface.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="5830"/>
        <source>ENTERING LOCKED!</source>
        <translation>SAISIE VERROUILLÉE !</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1334"/>
        <source>Start again.</source>
        <translation>Recommencez.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1332"/>
        <source>The name &lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; is already used.</source>
        <translation>Le nom &lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; est déjà utilisé.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="395"/>
        <source>Before continue:</source>
        <translation>Avant de continuer :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="396"/>
        <source>Must sure you ave a &lt;b&gt;verac_admin&lt;/b&gt; directory.&lt;br/&gt;You can also download it.</source>
        <translation>Vérifiez que vous avez un dossier &lt;b&gt;verac_admin&lt;/b&gt;.&lt;br/&gt;Vous pouvez aussi le télécharger.</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="1070"/>
        <source>Error, try again!</source>
        <translation>Erreur ; recommencez !</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="722"/>
        <source>Not Admin of:</source>
        <translation>Vous n&apos;êtes pas administrateur de :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3336"/>
        <source>&lt;b&gt;One item to be removed is evaluated&lt;/b&gt; in tables related to this model.</source>
        <translation>&lt;b&gt;Un item à supprimer est évalué&lt;/b&gt; dans des tableaux liés à ce modèle.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3341"/>
        <source>&lt;b&gt;{0} items to be removed are evaluated&lt;/b&gt; in tables related to this model.</source>
        <translation>&lt;b&gt;{0} items à supprimer sont évalués&lt;/b&gt; dans des tableaux liés à ce modèle.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3345"/>
        <source>If you continue, &lt;b&gt;these evaluations will be removed&lt;/b&gt;.</source>
        <translation>Si vous continuez, &lt;b&gt;ces évaluations seront supprimées&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="161"/>
        <source>download verac_web</source>
        <translation>téléchargement du dossier verac_web</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="167"/>
        <source>update /verac_admin/ftp/verac</source>
        <translation>mise à jour du dossier /verac_admin/ftp/verac</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="193"/>
        <source>update web site via FTP</source>
        <translation>mise à jour du site web via FTP</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="292"/>
        <source>download:</source>
        <translation>téléchargement :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="307"/>
        <source>upload:</source>
        <translation>envoi :</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="366"/>
        <source>finished DOWN</source>
        <translation>téléchargement terminé</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="370"/>
        <source>finished UP</source>
        <translation>envoi terminé</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="393"/>
        <source>cancel DOWN</source>
        <translation>téléchargement annulé</translation>
    </message>
    <message>
        <location filename="../libs/utils_web.py" line="398"/>
        <source>cancel UP</source>
        <translation>envoi annulé</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="206"/>
        <source>Processing Summary</source>
        <translation>Résumé du traitement</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="207"/>
        <source>Directories created:</source>
        <translation>Dossiers créés :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="208"/>
        <source>Directories removed:</source>
        <translation>Dossiers supprimés :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="209"/>
        <source>Directories total:</source>
        <translation>Nombre total de dossiers :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="210"/>
        <source>Files created:</source>
        <translation>Fichiers créés :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="211"/>
        <source>Files updated:</source>
        <translation>Fichiers mis à jour :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="212"/>
        <source>Files removed:</source>
        <translation>Fichiers supprimés :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="213"/>
        <source>Files total:</source>
        <translation>Nombre total de fichiers :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="214"/>
        <source>Bytes transfered:</source>
        <translation>Octets transférés :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="215"/>
        <source>Bytes total:</source>
        <translation>Octets au total :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="216"/>
        <source>Time started:</source>
        <translation>Heure de début :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="217"/>
        <source>Time finished:</source>
        <translation>Heure de fin :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="218"/>
        <source>Duration:</source>
        <translation>Durée :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5992"/>
        <source>Organization</source>
        <translation>Structure</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6013"/>
        <source>Users</source>
        <translation>Utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="973"/>
        <source>Configuring School</source>
        <translation>Configuration de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4554"/>
        <source>General configuration of the school (name, address, ...)</source>
        <translation>Configuration générale de l&apos;établissement (nom, adresse, ...)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4563"/>
        <source>ClassesStatusTip</source>
        <translation>Noms des classes et types de classes</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4570"/>
        <source>Shared competences</source>
        <translation>Compétences partagées</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4570"/>
        <source>To change the list of shared competences</source>
        <translation>Pour modifier les listes de compétences partagées</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="169"/>
        <source>School</source>
        <translation>Établissement</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="181"/>
        <source>Hours</source>
        <translation>Horaires</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="98"/>
        <source>Full name of the school:</source>
        <translation>Nom complet de l&apos;établissement :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="102"/>
        <source>SchoolShortName:</source>
        <translation>Nom court de l&apos;établissement :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3217"/>
        <source>The following database has been changed:</source>
        <translation>La base de données suivante a été modifiée :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3220"/>
        <source>It must be sent to your website.</source>
        <translation>Elle doit être envoyée sur votre site web.</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3222"/>
        <source>Would you upload it now?</source>
        <translation>Voulez-vous la poster maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3225"/>
        <source>The following databases have been modified:</source>
        <translation>Les bases de données suivantes ont été modifiées :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3230"/>
        <source>They must be sent to your website.</source>
        <translation>Elles doivent être envoyées sur votre site web.</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3232"/>
        <source>Would you upload them now?</source>
        <translation>Voulez-vous les poster maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4508"/>
        <source>To manage the list of users (teachers and students)</source>
        <translation>Pour gérer la liste des utilisateurs (profs et élèves)</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2450"/>
        <source>New Record</source>
        <translation>Nouvel enregistrement</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1047"/>
        <source>OrdreBLT:</source>
        <translation>Ordre d&apos;affichage&lt;br/&gt; dans le bulletin :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="293"/>
        <source>Setting up the website</source>
        <translation>Mise en place du site web</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6219"/>
        <source>The last directory&apos;s name must be &lt;b&gt;verac&lt;/&gt;!</source>
        <translation>Le nom du dernier dossier doit être &lt;b&gt;verac&lt;/&gt; !</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6229"/>
        <source>The proposed (before /verac) path is incorrect!</source>
        <translation>Le chemin proposé (avant /verac) est incorrect !</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6239"/>
        <source>Do you want to update the website now?</source>
        <translation>Voulez-vous mettre à jour le site web maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6297"/>
        <source>The proposed (before /secret) path is incorrect!</source>
        <translation>Le chemin proposé (avant /secret) est incorrect !</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6321"/>
        <source>Do you want to update the secret directory now?</source>
        <translation>Voulez-vous mettre à jour le dossier secret maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="446"/>
        <source>Students</source>
        <translation>Élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3204"/>
        <source>No Subject</source>
        <translation>Pas de matière</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3848"/>
        <source>Initial&lt;br/&gt;Password:</source>
        <translation>Mot de passe&lt;br/&gt;initial :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3830"/>
        <source>Class:</source>
        <translation>Classe :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3841"/>
        <source>Birthday:</source>
        <translation>Date de naissance :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3882"/>
        <source>LastYear:</source>
        <translation>An dernier :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6306"/>
        <source>The secret directory must not be placed in the public directory!</source>
        <translation>Le dossier secret ne doit pas être placé dans le dossier public !</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5864"/>
        <source>Send or update</source>
        <translation>Envoyer ou mettre à jour</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="90"/>
        <source>Otherwise, fill in the 2 fields below, &lt;br/&gt;then click the &lt;b&gt;DownloadVeracAdmin&lt;/b&gt; button.</source>
        <translation>Sinon, remplissez les 2 champs ci-dessous, &lt;br/&gt;puis cliquez sur le bouton &lt;b&gt;Télécharger le dossier verac_admin&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="109"/>
        <source>Click &lt;b&gt;Help&lt;/b&gt; button for more details.</source>
        <translation>Cliquez sur le bouton &lt;b&gt;Aide&lt;/b&gt; pour plus de détails.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2894"/>
        <source>Confidential competences</source>
        <translation>Compétences confidentielles</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="246"/>
        <source>Follow competences</source>
        <translation>Compétences suivies</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2486"/>
        <source>The code of the first level must not be empty.</source>
        <translation>Le code du premier niveau ne doit pas être vide.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="222"/>
        <source>Competences of the school report</source>
        <translation>Compétences du bulletin</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="230"/>
        <source>Competences of referential</source>
        <translation>Compétences du référentiel</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2494"/>
        <source>The first item in the list must remain the first level.</source>
        <translation>Le premier item de la liste doit rester de premier niveau.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2706"/>
        <source>Should also remove sub-items?</source>
        <translation>Faut-il supprimer aussi les sous-items ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="168"/>
        <source>Others</source>
        <translation>Autres</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2146"/>
        <source>SubHeading</source>
        <translation>sous-rubrique</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2148"/>
        <source>SubHeadingCheckBoxStatusTip</source>
        <translation>Cocher la case pour que ce titre soit une sous-rubrique (radar, etc)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3325"/>
        <source>Remember to assign a valid subject.</source>
        <translation>Pensez à lui attribuer une matière valide.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3319"/>
        <source>The &lt;b&gt;{0}&lt;/b&gt; subject&lt;br/&gt; (attributed to teacher &lt;b&gt;{1} {2}&lt;/b&gt;)&lt;br/&gt; does not exist!</source>
        <translation>La matière &lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; (attribuée au prof &lt;b&gt;{1} {2}&lt;/b&gt;)&lt;br/&gt; n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="597"/>
        <source>information message</source>
        <translation>message d&apos;information</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="599"/>
        <source>question message</source>
        <translation>question</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="601"/>
        <source>warning message</source>
        <translation>attention</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="603"/>
        <source>critical message</source>
        <translation>message critique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5934"/>
        <source>Tests</source>
        <translation>Tests</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5025"/>
        <source>ShowDBStateStatusTip</source>
        <translation>Afficher des informations sur l&apos;état de votre fichier</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10015"/>
        <source>DBState</source>
        <translation>État de la base</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10024"/>
        <source>General:</source>
        <translation>Généralités :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10048"/>
        <source>Password state:</source>
        <translation>État du mot de passe :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10050"/>
        <source>MUST BE CHANGED</source>
        <translation>DOIT ÊTRE CHANGÉ</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10092"/>
        <source>Structure an state:</source>
        <translation>Structure et état :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10143"/>
        <source>SUBJECT:</source>
        <translation>MATIÈRE :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10144"/>
        <source>GROUP:</source>
        <translation>GROUPE :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10145"/>
        <source>class-group:</source>
        <translation>groupe-classe :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10146"/>
        <source>PERIOD:</source>
        <translation>PÉRIODE :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10147"/>
        <source>Tables List:</source>
        <translation>Liste des tableaux :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10148"/>
        <source>private table</source>
        <translation>tableau privé</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10149"/>
        <source>State assessments:</source>
        <translation>État des évaluations :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10152"/>
        <source>Shared section of the bulletin:</source>
        <translation>Partie partagée du bulletin :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10153"/>
        <source>Disciplinary section of the bulletin:</source>
        <translation>Partie disciplinaire du bulletin :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10154"/>
        <source>Referentiel:</source>
        <translation>Référentiel :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10156"/>
        <source>NOTHING</source>
        <translation>RIEN</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5504"/>
        <source>ExportCounts2Csv</source>
        <translation>Exporter des comptages en csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5504"/>
        <source>ExportCounts2CsvStatusTip</source>
        <translation>Exporter une liste de comptages au format csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5511"/>
        <source>ImportCountsFromCsv</source>
        <translation>Importer des comptages depuis csv</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5511"/>
        <source>ImportCountsFromCsvStatusTip</source>
        <translation>Importer une liste de comptages depuis un fichier csv</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4927"/>
        <source>SelectCounts</source>
        <translation>Sélectionner des comptages</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4929"/>
        <source>Select the counts you want to export.</source>
        <translation>Sélectionnez les comptages que vous voulez exporter.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4934"/>
        <source>&lt;p&gt;&lt;b&gt;Counts:&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Comptages:&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2842"/>
        <source>CreateTemplate</source>
        <translation>Créer un modèle</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3441"/>
        <source>The name &lt;b&gt;{0}&lt;/b&gt; is already used.</source>
        <translation>Le nom &lt;b&gt;{0}&lt;/b&gt; est déjà utilisé.</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="624"/>
        <source>Period:</source>
        <translation>Période :</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1300"/>
        <source>Type:</source>
        <translation>Type :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2644"/>
        <source>Remove {0} items ?</source>
        <translation>Retirer {0} items ?</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="2646"/>
        <source>Their scores will be deleted.</source>
        <translation>Leurs scores seront supprimés.</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="865"/>
        <source>The {0} name is already there.</source>
        <translation>Le nom {0} est déjà là.</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="443"/>
        <source>You will remove the group {0} and all the tables and evaluations relating to it.</source>
        <translation>Vous allez supprimer le groupe {0} ainsi que tous les tableaux et évaluations le concernant.</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="434"/>
        <source>Enter your login name &lt;br/&gt;or select it in the comboBox</source>
        <translation>Entrez votre login &lt;br/&gt;ou sélectionnez-le dans la liste déroulante</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="136"/>
        <source>Enter the web address of the school</source>
        <translation>Entrez l&apos;adresse web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2386"/>
        <source>The structure of the file that you selected is not correct.</source>
        <translation>La structure du fichier que vous avez sélectionné n&apos;est pas correcte.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="1262"/>
        <source>EditActualTableau</source>
        <translation>Éditer le tableau actuel (nom, période, etc)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5291"/>
        <source>EditActualTableauStatusTip</source>
        <translation>Permet de modifier le tableau sélectionné (nom, description, période, public/privé)</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1885"/>
        <source>Modify this bilan Name ?</source>
        <translation>Modifier le nom de ce bilan ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1922"/>
        <source>Modify this bilan Label ?</source>
        <translation>Modifier la description de ce bilan ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1720"/>
        <source>Modify this item Name ?</source>
        <translation>Modifier le nom de cet item ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1754"/>
        <source>Modify this item Label ?</source>
        <translation>Modifier la description de cet item ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1665"/>
        <source>in:</source>
        <translation>en :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4611"/>
        <source>Export Lists of students to ods</source>
        <translation>Exporter les listes d&apos;élèves en ods</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4611"/>
        <source>Export Lists of students of the school in an ODF spreadsheet file (* .ods)</source>
        <translation>Exporter les listes d&apos;élèves de l&apos;établissement en un fichier tableur ODF (*.ods)</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="571"/>
        <source>All Students</source>
        <translation>Tous les élèves</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="585"/>
        <source>FIRSTNAME</source>
        <translation>PRÉNOM</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="591"/>
        <source>CLASS</source>
        <translation>CLASSE</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="2524"/>
        <source>SelectGroupsPeriods</source>
        <translation>Sélection des groupes et périodes</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="2576"/>
        <source>Select couples group+period for which you want &lt;br/&gt;to import the counts contained &lt;br/&gt;in the CSV file:</source>
        <translation>Sélectionnez les couples groupe+période pour lesquels &lt;br/&gt;vous voulez importer les comptages &lt;br/&gt;contenus dans le fichier CSV :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4914"/>
        <source>AutoSaveDBMy</source>
        <translation>Enregistrement automatique</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4914"/>
        <source>AutoSaveDBMyStatusTip</source>
        <translation>Si cette action est activée, votre base sera enregistrée toutes les 5 minutes</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="128"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="215"/>
        <source>Files folder:</source>
        <translation>Dossier des fichiers :</translation>
    </message>
    <message>
        <location filename="../libs/utils_filesdirs.py" line="142"/>
        <source>&lt;b&gt;{0}&lt;/b&gt; seems to be already in use.&lt;br/&gt;If you are sure this is not the case, you can continue.</source>
        <translation>&lt;b&gt;{0}&lt;/b&gt; semble être déjà en cours d&apos;utilisation.&lt;br/&gt;Si vous êtes certain que tel n&apos;est pas le cas, vous pouvez continuer.</translation>
    </message>
    <message>
        <location filename="../libs/utils_filesdirs.py" line="147"/>
        <source>Do you want to continue?</source>
        <translation>Voulez-vous continuer ?</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="489"/>
        <source>Last test:</source>
        <translation>Dernier test :</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="491"/>
        <source>Last recup:</source>
        <translation>Dernière récupération :</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="493"/>
        <source>Retrieving ...</source>
        <translation>Récupération en cours...</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="576"/>
        <source>QuitSchedulerStatusTip</source>
        <translation>Quitter le planificateur de récupérations</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3048"/>
        <source>LOGIN</source>
        <translation>LOGIN</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3051"/>
        <source>INITIAL PASSWORD</source>
        <translation>MOT DE PASSE INITIAL</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1629"/>
        <source>&lt;p&gt;&lt;b&gt;Would you download the database compteur&lt;br/&gt; before proceeding?&lt;/b&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Voulez-vous télécharger la base compteur&lt;br/&gt; avant de continuer ?&lt;/b&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_about.py" line="110"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5635"/>
        <source>ElevePrevious</source>
        <translation>Élève précédent</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6775"/>
        <source>ElevePreviousStatusTip</source>
        <translation>Revenir à l&apos;élève précédent (ou aller à la fin de la liste)</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="375"/>
        <source>localDate</source>
        <translation>date de la base locale</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="377"/>
        <source>netDate</source>
        <translation>date de la base distante</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="324"/>
        <source>Remember to send it.</source>
        <translation>Pensez à l&apos;envoyer.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="326"/>
        <source>(&quot;File &gt; SaveAndPostDBMy&quot; menu)</source>
        <translation>(menu &quot;Fichier → Enregistrer et envoyer la base de données personnelle&quot;)</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="333"/>
        <source>(&quot;File &gt; DownloadDBMy&quot; menu)</source>
        <translation>(menu &quot;Fichier → Télécharger la base distante&quot;)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4968"/>
        <source>TestUpdateDBUsersCommun</source>
        <translation>Vérifier les bases commun et utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4968"/>
        <source>TestUpdateDBUsersCommunStatusTip</source>
        <translation>Vérifier les bases commun et utilisateurs sont à jour</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="499"/>
        <source>Your Commun DB version is the last</source>
        <translation>Votre base commun est à jour</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="501"/>
        <source>Your Users DB version is the last</source>
        <translation>Votre base des utilisateurs est à jour</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="503"/>
        <source>Your Commun DB must be updated</source>
        <translation>Votre base commun doit être mise à jour</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="505"/>
        <source>Your Users DB must be updated</source>
        <translation>Votre base des utilisateurs doit être mise à jour</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3857"/>
        <source>The password has been changed.</source>
        <translation>Le mot de passe a été changé.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3859"/>
        <source>The password has not been changed!</source>
        <translation>Le mot de passe n&apos;a pas été changé !</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3864"/>
        <source>ReinitPassword</source>
        <translation>Réinitialiser le mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3870"/>
        <source>ChangePassword</source>
        <translation>Changer le mot de passe</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3424"/>
        <source>NewTemplateFromBilan</source>
        <translation>Nouveau modèle d&apos;après un bilan</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3413"/>
        <source>Select an assessment.</source>
        <translation>Sélectionnez un bilan.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3415"/>
        <source>All items related to this assessment will be added to the template.</source>
        <translation>Tous les items liés à ce bilan seront dans le modèle de tableau.</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="94"/>
        <source>Help page explaining how to install it:</source>
        <translation>Page d&apos;aide expliquant comment l&apos;installer :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9118"/>
        <source>CalculatedNoteModePersos</source>
        <translation>Note calculée d&apos;après les bilans personnels</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9120"/>
        <source>CalculatedNoteModeBLT</source>
        <translation>Note calculée d&apos;après les bilans sélectionnés pour le bulletin</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="546"/>
        <source>The DB {0} has been modified.</source>
        <translation>La base {0} a été modifiée.</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="550"/>
        <source>Do you want to save your changes?</source>
        <translation>Voulez-vous enregistrer les changements ?</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="440"/>
        <source>Execute query</source>
        <translation>Exécuter la requête</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="391"/>
        <source>ShowTables</source>
        <translation>Voir les tables</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="392"/>
        <source>ShowTablesStatusTip</source>
        <translation>Afficher les tables de la base de données</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="397"/>
        <source>ShowSql</source>
        <translation>Requêtes SQL</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="398"/>
        <source>ShowSqlStatusTip</source>
        <translation>Faire des requêtes SQL sur la base de données</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="662"/>
        <source>results</source>
        <translation>résultats</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="683"/>
        <source>this DataBase is not editable.</source>
        <translation>cette base de données n&apos;est pas éditable.</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="687"/>
        <source>ERROR</source>
        <translation>ERREUR</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="682"/>
        <source>ERROR:</source>
        <translation>ERREUR :</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="661"/>
        <source>OK:</source>
        <translation>OK :</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="570"/>
        <source>This dataBase is not editable.</source>
        <translation>Cette base de données n&apos;est pas éditable.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4475"/>
        <source>EditDBFile</source>
        <translation>Éditer une base de données</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4475"/>
        <source>EditDBFileStatusTip</source>
        <translation>Éditer ou consulter une base de données SQLITE</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="374"/>
        <source>DBFileEditor</source>
        <translation>Éditeur de bases de données SQLITE</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="386"/>
        <source>Open DB</source>
        <translation>Ouvrir une base</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="414"/>
        <source>Insert a new row into the table</source>
        <translation>Insérer une nouvelle ligne dans la table</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="419"/>
        <source>Delete the selected row</source>
        <translation>Supprimer la ligne sélectionnée</translation>
    </message>
    <message>
        <location filename="../libs/admin_db.py" line="459"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5464"/>
        <source>Export a balance in tree</source>
        <translation>Exporter un bilan en arborescence</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2852"/>
        <source>Choose the Directory where the subdirectorys will be created</source>
        <translation>Choisissez le dossier où l&apos;arborescence sera créée</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5464"/>
        <source>Create a tree (folder and subfolders) from the balance sheet and associated items</source>
        <translation>Crée une arborescence (dossier et sous-dossiers) d&apos;après le bilan et les items associés</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="307"/>
        <source>Bilan directory name:</source>
        <translation>Nom du dossier bilan :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="311"/>
        <source>Items directorys names:</source>
        <translation>Noms des sous-dosssiers items :</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="821"/>
        <source>You can open the directory.</source>
        <translation>Vous pouvez ouvrir le dossier.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2822"/>
        <source>All items related to this assessment will give a subfolder.</source>
        <translation>Tous les items reliés à ce bilan donneront un sous-dossier.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2826"/>
        <source>Specify the format of folder names to create.</source>
        <translation>Indiquez les formats des noms de dossiers à créer.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2828"/>
        <source>{0}: balance name, {1}: balance title, {2}: item name, {3}: item title</source>
        <translation>{0} : nom du bilan, {1} : intitulé du bilan, {2} : nom de l&apos;item, {3} : intitulé de l&apos;item</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="2830"/>
        <source>See help for more explanation.</source>
        <translation>Voir l&apos;aide pour plus d&apos;explications.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5782"/>
        <source>LoadInitialLayout</source>
        <translation>Charger l&apos;agencement initial</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5782"/>
        <source>LoadInitialLayoutStatusTip</source>
        <translation>Replace la barre d&apos;outils et le menu comme au premier lancement de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5789"/>
        <source>LoadSavedLayout</source>
        <translation>Charger l&apos;agencement enregistré</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5789"/>
        <source>LoadSavedLayoutStatusTip</source>
        <translation>Recharge la position et l&apos;état de la barre d&apos;outils et du menu tel qu&apos;enregistrés dans votre base</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5796"/>
        <source>SaveCurrentLayout</source>
        <translation>Enregistrer l&apos;agencement actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5796"/>
        <source>SaveCurrentLayoutStatusTip</source>
        <translation>Enregistrer la position et l&apos;état de la barre d&apos;outils et du menu (il faudra enregistrer votre base)</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9611"/>
        <source>personal evaluation</source>
        <translation>Évaluation personnelle</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2429"/>
        <source>CHANGES:</source>
        <translation>MODIFICATIONS EFFECTUÉES :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="2430"/>
        <source>NO CHANGE</source>
        <translation>AUCUNE MODIFICATION</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5256"/>
        <source>FIELDS AVAILABLE FOR MODELS</source>
        <translation>CHAMPS DISPONIBLES POUR LES MODÈLES</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5261"/>
        <source>Fields for the school:</source>
        <translation>Champs concernant l&apos;établissement :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5274"/>
        <source>Fields for the document:</source>
        <translation>Champs concernant le document :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5289"/>
        <source>Fields for the student:</source>
        <translation>Champs concernant l&apos;élève :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5317"/>
        <source>Fields for special subjects:</source>
        <translation>Champs pour les matières spéciales :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5331"/>
        <source>Fields available for each shared competences:</source>
        <translation>Champs disponibles pour chaque compétence partagée :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5337"/>
        <source>Where XXX is the name of competence:</source>
        <translation>Où XXX désigne le nom de la compétence :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5351"/>
        <source>Names from table &quot;bulletin&quot; (shared competences from bulletin):</source>
        <translation>Noms issus de la table &quot;bulletin&quot; (compétences partagées du bulletin) :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5370"/>
        <source>Names from table &quot;referentiel&quot; (competences from referential):</source>
        <translation>Noms issus de la table &quot;referentiel&quot; (compétences du référentiel) :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5389"/>
        <source>Names from table &quot;confidentiel&quot; (confidential competences):</source>
        <translation>Noms issus de la table &quot;confidentiel&quot; (compétences confidentielles) :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5408"/>
        <source>Names from table &quot;suivi&quot; (followed competences):</source>
        <translation>Noms issus de la table &quot;suivi&quot; (compétences suivies) :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5308"/>
        <source>Labels of Subjects:</source>
        <translation>Intitulés des matières :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5429"/>
        <source>Fields for subjects (personal bilans of teachers) :</source>
        <translation>Champs pour les matières (bilans personnels des profs) :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5434"/>
        <source>Where XXX is the name of subject and nn a 2-digit number under limiteBLTPerso:</source>
        <translation>Où XXX est le nom de la matière et nn un nombre à 2 chiffres inférieur à limiteBLTPerso :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5449"/>
        <source>Available names of Subjects:</source>
        <translation>Noms de matières disponibles :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5466"/>
        <source>Details of the results by subject for shared competences:</source>
        <translation>Détails des résultats par matière pour les compétences partagées :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5471"/>
        <source>Where XXX is the name of competence and YYY is the name of subject:</source>
        <translation>Où XXX est le nom de la compétence et YYY celui de la matière :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5482"/>
        <source>Available names:</source>
        <translation>Noms disponibles :</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="511"/>
        <source>Would you like to upgrade now?</source>
        <translation>Voulez-vous mettre à jour maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5900"/>
        <source>ProtectedPeriodsStatusTip</source>
        <translation>Pour afficher et modifier la liste des périodes protégées</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="326"/>
        <source>Documents management</source>
        <translation>Gestion des documents</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4803"/>
        <source>DocumentsManagementStatusTip</source>
        <translation>Pour gérer les documents mis à disposition des profs et des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="335"/>
        <source>Students personal documents</source>
        <translation>Documents personnels des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="340"/>
        <source>Students documents</source>
        <translation>Documents pour les élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="345"/>
        <source>Teachers documents</source>
        <translation>Documents pour les profs</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="482"/>
        <source>Student:</source>
        <translation>Élève :</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1633"/>
        <source>This file does not exist in the &quot;documents&quot; folder.</source>
        <translation>Ce fichier n&apos;existe pas dans le dossier &quot;documents&quot;.</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1240"/>
        <source>This file is not registered in the database.</source>
        <translation>Ce fichier n&apos;est pas inscrit dans la base de données.</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="806"/>
        <source>documents</source>
        <translation>documents</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="806"/>
        <source>OpenDocsElevesDirToolTip</source>
        <translation>Ouvre le dossier contenant les fichiers des documents des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1255"/>
        <source>reloadFiles</source>
        <translation>Recharger</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1255"/>
        <source>reloadFilesToolTip</source>
        <translation>Recharge la liste des fichiers</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1260"/>
        <source>repairAll</source>
        <translation>Réparer</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1260"/>
        <source>repairAllToolTip</source>
        <translation>Inscrit dans la base de données tous les fichiers qui n&apos;y sont pas</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1265"/>
        <source>deleteFilesAll</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1265"/>
        <source>deleteFilesAllToolTip</source>
        <translation>Supprime les fichiers qui ne sont pas inscrits dans la base de données</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1270"/>
        <source>clearAll</source>
        <translation>Nettoyer</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1270"/>
        <source>clearAllToolTip</source>
        <translation>Efface de la base de données les références à des fichiers inexistants</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1071"/>
        <source>Repair the selected item</source>
        <translation>Réparer l&apos;élément sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1055"/>
        <source>Description:</source>
        <translation>Description :</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1238"/>
        <source>This file does not exist in the &quot;docsprofs&quot; folder.</source>
        <translation>Ce fichier n&apos;existe pas dans le dossier &quot;docsprofs&quot;.</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1250"/>
        <source>docsprofs</source>
        <translation>docsprofs</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1250"/>
        <source>OpenDocsTeachersDirToolTip</source>
        <translation>Ouvre le dossier contenant les fichiers des documents des profs</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1304"/>
        <source>shared documents (booklet ...)</source>
        <translation>documents partagés (livret, ...)</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1304"/>
        <source>results (reports, ...)</source>
        <translation>résultats (bulletins, ...)</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1304"/>
        <source>confidential documents (for teachers only)</source>
        <translation>documents confidentiels (seulement pour les profs)</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="856"/>
        <source>Students:</source>
        <translation>Élèves :</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="859"/>
        <source>allStudents</source>
        <translation>pour tous les élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="863"/>
        <source>selectedStudents</source>
        <translation>pour une sélection d&apos;élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="866"/>
        <source>selectStudents</source>
        <translation>sélectionner les élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="866"/>
        <source>selectStudentsToolTip</source>
        <translation>pour sélectionner la liste des élèves ayant accès à ce document</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="178"/>
        <source>Sending documents</source>
        <translation>Envoi des documents</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="197"/>
        <source>uploadStudentsDocuments</source>
        <translation>Envoi des documents pour les élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="202"/>
        <source>uploadTeachersDocuments</source>
        <translation>Envoi des documents pour les professeurs</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="296"/>
        <source>Would you upload documents to your website now?</source>
        <translation>Voulez-vous envoyer les documents sur votre site web maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3743"/>
        <source>Do you want to change the ids of leading teachers &lt;br/&gt; and reset their passwords?</source>
        <translation>Voulez-vous modifier les ids de profs principaux &lt;br/&gt; et réinitialiser leurs mots de passe ?</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3746"/>
        <source>ids offset:</source>
        <translation>décalage des ids :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1752"/>
        <source>Select the file manually</source>
        <translation>Sélectionner le fichier manuellement</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="89"/>
        <source>WKHTMLTOPDF IS NOT INSTALLED.</source>
        <translation>WKHTMLTOPDF N&apos;EST PAS INSTALLÉ.</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="91"/>
        <source>It&apos;s a tool that improves the quality of pdf files produced.</source>
        <translation>C&apos;est un outils qui améliore la qualité des fichiers pdf fabriqués.</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="96"/>
        <source>Install wkhtmltopdf.</source>
        <translation>Installer wkhtmltopdf.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="735"/>
        <source>You also want to start cleaning up your file?</source>
        <translation>Voulez-vous aussi lancer un nettoyage de votre fichier ?</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="737"/>
        <source>This is done only to change the school year.</source>
        <translation>Ceci n&apos;est à faire qu&apos;au changement d&apos;année scolaire.</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="486"/>
        <source>Uncalculated:</source>
        <translation>Non calculé :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="489"/>
        <source>If this box is checked, this report will not be calculated.</source>
        <translation>Si cette case est cochée, ce bilan ne sera pas calculé.</translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="513"/>
        <source>Download anyway</source>
        <translation>Télécharger quand même</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="3845"/>
        <source>must be of the form ddmmyyyy</source>
        <translation>doit être de la forme jjmmaaaa</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4241"/>
        <source>The birthday &lt;b&gt;{0}&lt;/b&gt; is not valid.</source>
        <translation>La date de naissance &lt;b&gt;{0}&lt;/b&gt; n&apos;est pas valable.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4243"/>
        <source>It must be of the form &lt;b&gt;ddmmyyyy&lt;/b&gt;.</source>
        <translation>Elle doit être de la forme &lt;b&gt;jjmmaaaa&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3108"/>
        <source>Before changing the following database:</source>
        <translation>Avant de modifier la base de données suivante :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2011"/>
        <source>You must download the latest version (which is the website).</source>
        <translation>Il faut télécharger la dernière version (qui est celle du site Web).</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2013"/>
        <source>Would you download it now?</source>
        <translation>Voulez-vous la télécharger maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3116"/>
        <source>Before changing the following databases:</source>
        <translation>Avant de modifier les bases de données suivantes :</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3123"/>
        <source>Would you download them now?</source>
        <translation>Voulez-vous les télécharger maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1061"/>
        <source>New students:</source>
        <translation>Nouveaux élèves :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1077"/>
        <source>Deleted students:</source>
        <translation>Élèves supprimés :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1084"/>
        <source>Kept students:</source>
        <translation>Élèves conservés :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="635"/>
        <source>Show class results</source>
        <translation>Afficher les résultats de la classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1616"/>
        <source>ClassDesabledCheckBoxStatusTip</source>
        <translation>Cocher la case pour désactiver la classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1612"/>
        <source>Desabled:</source>
        <translation>Désactivée :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6543"/>
        <source>General Restrictions</source>
        <translation>Restrictions générales</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6545"/>
        <source>Pages authorized for students (all periods)</source>
        <translation>Pages autorisées aux élèves (toutes périodes)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6547"/>
        <source>Restrictions relating only to the current period</source>
        <translation>Restrictions ne concernant que la période actuelle</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="350"/>
        <source>By student</source>
        <translation>Par élève</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5803"/>
        <source>ConfigureToolBar</source>
        <translation>Configurer la barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5803"/>
        <source>Select size of icons, actions displayed, ...</source>
        <translation>Choisir la dimension des icônes, les actions affichées, ...</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1567"/>
        <source>TOOLBAR CONFIGURATION</source>
        <translation>CONFIGURATION DE LA BARRE D&apos;OUTILS</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1574"/>
        <source>VisibleActions</source>
        <translation>Actions visibles</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1583"/>
        <source>FrequentlyUsedActions:</source>
        <translation>Actions fréquemment utilisées :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1594"/>
        <source>ContextualActions:</source>
        <translation>Actions affichées selon le contexte :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4951"/>
        <source>SwitchToCompleteInterfaceStatusTip</source>
        <translation>L&apos;interface complète active toutes les actions disponibles dans VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4951"/>
        <source>SwitchToCompleteInterface</source>
        <translation>Passer à l&apos;interface complète</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1805"/>
        <source>SwitchToSimplifiedInterfaceStatusTip</source>
        <translation>L&apos;interface simplifiée n&apos;affiche que les actions essentielles de VÉRAC (plus facile pour débuter)</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1807"/>
        <source>SwitchToSimplifiedInterface</source>
        <translation>Passer à l&apos;interface simplifiée</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1611"/>
        <source>OtherActions:</source>
        <translation>Autres actions :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="6253"/>
        <source>MenuAndToolBar</source>
        <translation>Menu et barre d&apos;outils</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="476"/>
        <source>Advices to progress that students can see in the web interface.</source>
        <translation>Des conseils pour progresser que les élèves peuvent voir dans l&apos;interface web.</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="114"/>
        <source>Create a structure file</source>
        <translation>Créer un fichier de structure</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5387"/>
        <source>A structure file can be exchanged with colleagues</source>
        <translation>Un fichier de structure peut être échangé avec vos collègues</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="998"/>
        <source>Import a structure file</source>
        <translation>Importer un fichier de structure</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5395"/>
        <source>Import a structure made by a colleague</source>
        <translation>Importer une structure réalisée par un collègue</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1109"/>
        <source>Introduction</source>
        <translation>Introduction</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="274"/>
        <source>Step 1 - items selection</source>
        <translation>Étape 1 – sélection des items</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="303"/>
        <source>Step 2 - balances selection</source>
        <translation>Étape 2 – sélection des bilans</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="355"/>
        <source>Step 3 - default profile selection (for bulletins)</source>
        <translation>Étape 3 – sélection d&apos;un profil par défaut (pour les bulletins)</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="425"/>
        <source>Step 4 - tables models selection</source>
        <translation>Étape 4 – sélection des modèles de tableaux</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="528"/>
        <source>Last step - subject, title and description</source>
        <translation>Dernière étape – matière, titre et description</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1048"/>
        <source>Title:</source>
        <translation>Titre :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="572"/>
        <source>&lt;b&gt;Validate the item in case of absences:&lt;/b&gt;</source>
        <translation>&lt;b&gt;Valider l&apos;item en cas d&apos;absences :&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="130"/>
        <source>Open an existing file</source>
        <translation>Ouvrir un fichier existant</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="135"/>
        <source>Opens an existing file structure already to change it.</source>
        <translation>Ouvre un fichier de structure existant déjà afin de le modifier.</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1180"/>
        <source>Open sqlite File</source>
        <translation>Ouvrir un fichier sqlite</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1180"/>
        <source>sqlite files (*.sqlite)</source>
        <translation>fichiers sqlite (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="438"/>
        <source>TABLES TEMPLATES</source>
        <translation>MODÈLES DE TABLEAUX</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="464"/>
        <source>TABLES WITHOUT TEMPLATES</source>
        <translation>TABLEAUX SANS MODÈLES</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="750"/>
        <source>Save sqlite File</source>
        <translation>Enregistrer un fichier sqlite</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1013"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1017"/>
        <source>Opens a file on your computer.</source>
        <translation>Ouvre un fichier présent sur votre ordinateur.</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1110"/>
        <source>School Website</source>
        <translation>Site web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1025"/>
        <source>Search on the school website.</source>
        <translation>Chercher sur le site de l&apos;établissement.</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1111"/>
        <source>VERAC Website</source>
        <translation>Site de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1033"/>
        <source>Search on the VERAC website.</source>
        <translation>Chercher sur le site web de VÉRAC.</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1112"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1119"/>
        <source>Last step</source>
        <translation>Dernière étape</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1015"/>
        <source>Skip to the last step.</source>
        <translation>Passer directement à la dernière étape.</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1116"/>
        <source>Default profile (for bulletins)</source>
        <translation>Profil par défaut (pour les bulletins)</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1200"/>
        <source>You must select a structure before continuing!</source>
        <translation>Vous devez sélectionner une structure avant de continuer !</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2087"/>
        <source>Modify this template Name ?</source>
        <translation>Modifier le nom de ce modèle de tableau ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="2129"/>
        <source>Modify this template Label ?</source>
        <translation>Modifier la description de ce modèle de tableau ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1981"/>
        <source>There is already a default profile for this subject:</source>
        <translation>Il y a déjà un profil par défaut pour cette matière :</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="1984"/>
        <source>Do you want to replace it with the one contained in the structure to import?</source>
        <translation>Voulez-vous le remplacer par celui contenu dans la structure à importer ?</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="116"/>
        <source>Management of teacher structures</source>
        <translation>Gestion des structures profs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4880"/>
        <source>Structures files made available for teachers on the school website</source>
        <translation>Fichiers de structures mis à disposition des professeurs sur le site web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="88"/>
        <source>Updating structures files via FTP</source>
        <translation>Mise à jour des fichiers de structures via FTP</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="128"/>
        <source>Open the folder containing the files structures</source>
        <translation>Ouvre le dossier contenant les fichiers de structures</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="131"/>
        <source>Reload list</source>
        <translation>Recharger la liste</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="134"/>
        <source>Reloads structures files list</source>
        <translation>Recharge la liste des fichiers de structures</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1003"/>
        <source>&lt; Back</source>
        <translation>&lt; Précédent</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1008"/>
        <source>Next &gt;</source>
        <translation>Suivant &gt;</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1020"/>
        <source>Finish</source>
        <translation>Terminer</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1013"/>
        <source>&gt;&gt;</source>
        <translation>&gt;&gt;</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4930"/>
        <source>STUDENT</source>
        <translation>ÉLÈVE</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4546"/>
        <source>Year</source>
        <translation>Année</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4552"/>
        <source>Teacher</source>
        <translation>Professeur</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4555"/>
        <source>Result</source>
        <translation>Résultat</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4713"/>
        <source>Export results from referential to ods</source>
        <translation>Exporter des résultats du référentiel en ods</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4713"/>
        <source>Export results of a list of students to an ODF Spreadsheet File (takes account of previous years)</source>
        <translation>Exporter des résultats d&apos;une liste d&apos;élèves en un fichier tableur ODF (tient compte des années précédentes)</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4558"/>
        <source>Title</source>
        <translation>Titre</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="5207"/>
        <source>Balances selection</source>
        <translation>Sélection des bilans</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="5247"/>
        <source>Subjects selection</source>
        <translation>Sélection des matières</translation>
    </message>
    <message>
        <location filename="../libs/utils_structures.py" line="497"/>
        <source>Step 5 - counts selection</source>
        <translation>Étape 5 – sélection des comptages</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4456"/>
        <source>Create multiple counts</source>
        <translation>Créer plusieurs comptages</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5724"/>
        <source>creates counts for several couples group+period</source>
        <translation>permet de créer des comptages pour plusieurs couples groupe+période</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4460"/>
        <source>Select couples group+period for which you want to create the counts:</source>
        <translation>Sélectionnez les couples groupe+période pour lesquels vous voulez créer les comptages :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4470"/>
        <source>Select the counts you want to create by moving them to the list on the right:</source>
        <translation>Sélectionnez les comptages que vous voulez créer en les déplaçant dans la liste située à droite :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9613"/>
        <source>Linked Item:</source>
        <translation>Item lié :</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="4318"/>
        <source>NO LINKED ITEM</source>
        <translation>PAS D&apos;ITEM LIÉ</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9594"/>
        <source>No calculation</source>
        <translation>Aucun calcul</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="617"/>
        <source>Import absences from SIECLE</source>
        <translation>Importer les absences depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5519"/>
        <source>Import absences from eleves_date_heure.xml file exported from SIECLE</source>
        <translation>Importer les absences depuis le fichier eleves_date_heure.xml exporté depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="631"/>
        <source>Students to import:</source>
        <translation>Élèves à importer :</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="635"/>
        <source>All students</source>
        <translation>Tous les élèves</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="638"/>
        <source>Students in the selected group</source>
        <translation>Élèves du groupe sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="702"/>
        <source>Do not import</source>
        <translation>Ne pas importer</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="863"/>
        <source>Importing the file failed or you have canceled.</source>
        <translation>L&apos;importation du fichier a échoué ou vous l&apos;avez annulée.</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3350"/>
        <source>The {0} file was created in the verac_admin directory.</source>
        <translation>Le fichier {0} a été créé dans le dossier verac_admin.</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1987"/>
        <source>Rename this profile</source>
        <translation>Renommer ce profil</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2587"/>
        <source>Create a new profile</source>
        <translation>Créer un nouveau profil</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1997"/>
        <source>Delete this profile</source>
        <translation>Supprimer ce profil</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2016"/>
        <source>&lt;p align=&quot;center&quot;&gt;The number of assessments posted in the bulletin is &lt;b&gt;limited to {0}&lt;/b&gt; for each subject.&lt;br/&gt;If you choose some more, only the first {0} evaluated assessments will be posted (for each student).&lt;br/&gt;The balance whose names are displayed in bold are your personal balance.&lt;/p&gt;</source>
        <translation>&lt;p align=&quot;center&quot;&gt;Le nombre de bilans affichés dans le bulletin est &lt;b&gt;limité à {0}&lt;/b&gt; pour chaque matière.&lt;br/&gt;Si vous en choisissez plus, seuls les {0} premiers bilans évalués seront affichés (pour chaque élève).&lt;br/&gt;Les bilans dont les noms sont affichés en gras sont vos bilans personnels.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2050"/>
        <source>Profiles</source>
        <translation>Profils</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2057"/>
        <source>Groups or students that this profile is assigned</source>
        <translation>Groupes ou élèves à qui ce profil est attribué</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2228"/>
        <source>Default profile for this subject</source>
        <translation>Profil par défaut pour cette matière</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2242"/>
        <source>STANDARD PROFILES FOR GROUPS:</source>
        <translation>PROFILS STANDARDS POUR DES GROUPES :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2263"/>
        <source>SPECIFIC PROFILES FOR STUDENTS:</source>
        <translation>PROFILS SPÉCIFIQUES POUR DES ÉLÈVES :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2399"/>
        <source>Rename a profile</source>
        <translation>Renommer un profil</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2591"/>
        <source>Profile name:</source>
        <translation>Nom du profil :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2597"/>
        <source>Profile type:</source>
        <translation>Type du profil :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2601"/>
        <source>standard profile for groups</source>
        <translation>profil standard pour des groupes</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="2604"/>
        <source>specific profile for students</source>
        <translation>profil spécifique pour des élèves</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="3600"/>
        <source>Base:</source>
        <translation>Base :</translation>
    </message>
    <message>
        <location filename="../libs/prof_itemsbilans.py" line="3350"/>
        <source>Advice</source>
        <translation>Conseil</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9925"/>
        <source>Advise</source>
        <translation>Avis</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="826"/>
        <source>Do you want to add it anyway?</source>
        <translation>Voulez-vous l&apos;ajouter tout de même ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="78"/>
        <source>THE VERAC VERSION IS TOO OLD.</source>
        <translation>VERSION DE VÉRAC TROP ANCIENNE.</translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="80"/>
        <source>VERAC version installed on this computer is too old</source>
        <translation>La version de VÉRAC qui est installée sur cet ordinateur est trop vieille</translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="82"/>
        <source>and may damage your database.</source>
        <translation>et pourrait endommager votre base de données.</translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="84"/>
        <source>Update VERAC before continuing.</source>
        <translation>Mettez le logiciel VÉRAC à jour avant de continuer.</translation>
    </message>
    <message>
        <location filename="../libs/utils_upgrades.py" line="86"/>
        <source>(&quot;Utils &gt; UpdateVerac&quot; or &quot;Utils &gt; TestUpdateVerac&quot; menu)</source>
        <translation>(menu &quot;Outils → Mettre VÉRAC à jour&quot; ou &quot;Outils → Tester votre version de VÉRAC&quot;)</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="1493"/>
        <source>Updating the recup_evals database.</source>
        <translation>Mise à jour de la base de données recup_evals.</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="174"/>
        <source>Other actions</source>
        <translation>Autres actions</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="177"/>
        <source>Show/Hide other actions available</source>
        <translation>Affiche/Masque les autres actions disponibles</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="212"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="223"/>
        <source>Change</source>
        <translation>Changer</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="231"/>
        <source>Configuration folder:</source>
        <translation>Dossier de configuration :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="282"/>
        <source>Files folder</source>
        <translation>Dossier des fichiers</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1062"/>
        <source>Insert New Item (before the selected item)</source>
        <translation>Insérer un nouvel élément (avant l&apos;élément sélectionné)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2700"/>
        <source>The first item can not be deleted.</source>
        <translation>Le premier item ne peut pas être supprimé.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6287"/>
        <source>The path must begin with a /</source>
        <translation>Le chemin doit commencer par un /</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5626"/>
        <source>Table to use:</source>
        <translation>Table à utiliser :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="651"/>
        <source>Uncheck the box below if test</source>
        <translation>Décochez la case ci-dessous en cas de test</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="653"/>
        <source>or whether the documents should not be made available to students</source>
        <translation>ou si les documents ne doivent pas être mis à disposition des utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="149"/>
        <source>Updates class groups from the download list.
Attention you can not add feedback to students removed from the group.</source>
        <translation>Met à jour les groupes classes à partir des listes téléchargées.
Attention, vous ne pourrez plus ajouter d&apos;évaluations aux élèves retirés du groupe.</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1810"/>
        <source>&lt;p&gt;Select the students you want to permanently delete.&lt;/p&gt;&lt;p&gt;All the traces (assessments, opinions, etc) &lt;br/&gt;of this students will be &lt;b&gt;definitively&lt;/b&gt; deleted.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Sélectionnez les élèves que vous voulez supprimer définitivement.&lt;/p&gt;&lt;p&gt;Toutes les traces (évaluations, appréciations, etc) &lt;br/&gt; de ces élèves seront &lt;b&gt;définitivement&lt;/b&gt; effacées.&lt;/p&gt; </translation>
    </message>
    <message>
        <location filename="../libs/prof_db.py" line="335"/>
        <source>Download Now</source>
        <translation>Télécharger maintenant</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5009"/>
        <source>Return to the simple management</source>
        <translation>Repasser à la gestion simple</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5009"/>
        <source>Return to the simple management of personal Version</source>
        <translation>Repasser à la gestion simple de la version perso</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2857"/>
        <source>Create the class file</source>
        <translation>Création du fichier de la classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="3074"/>
        <source>FINISHED</source>
        <translation>FINI</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="474"/>
        <source>annual-report</source>
        <translation>bilan-annuel</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="475"/>
        <source>ANNUAL REPORT</source>
        <translation>BILAN ANNUEL</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="486"/>
        <source>referential-report</source>
        <translation>referentiel</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="487"/>
        <source>REFERENTIAL REPORT</source>
        <translation>RELEVÉ DU RÉFÉRENTIEL</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2228"/>
        <source>The titles of shared competence</source>
        <translation>Les titres des compétences partagées</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2251"/>
        <source>The titles of personal competence</source>
        <translation>Les titres des compétences personnelles</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2008"/>
        <source>Before use the following database:</source>
        <translation>Avant d&apos;utiliser la base de données suivante :</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="102"/>
        <source>Referential</source>
        <translation>Référentiel</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="110"/>
        <source>Details</source>
        <translation>Détails</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="114"/>
        <source>Follows</source>
        <translation>Suivis</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="118"/>
        <source>Opinions (on groups)</source>
        <translation>Appréciations (sur les groupes)</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="122"/>
        <source>Papers</source>
        <translation>Documents</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="126"/>
        <source>Calculator</source>
        <translation>Calculatrice</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="73"/>
        <source>the site is enabled</source>
        <translation>le site est activé</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="77"/>
        <source>students have access to the site</source>
        <translation>les élèves ont accès au site</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="82"/>
        <source>students can view the details of shared competences</source>
        <translation>les élèves peuvent afficher les détails des compétences partagées</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="87"/>
        <source>students  see the &quot;Class&quot; column</source>
        <translation>les élèves voient la colonne &quot;Classe&quot;</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="131"/>
        <source>students have access to the actual period</source>
        <translation>les élèves ont accès à la période actuelle</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="136"/>
        <source>students see the opinions</source>
        <translation>les élèves voient les appréciations</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="141"/>
        <source>students see the notes (for classes with notes)</source>
        <translation>les élèves voient les notes (pour les classes à notes)</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="92"/>
        <source>teachers can validate the referential</source>
        <translation>les professeurs peuvent valider le référentiel</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5169"/>
        <source>The &lt;b&gt;{0}&lt;/b&gt; class&lt;br/&gt; (attributed to student &lt;b&gt;{1} {2}&lt;/b&gt;)&lt;br/&gt; does not exist!</source>
        <translation>La classe &lt;b&gt;{0}&lt;/b&gt;&lt;br/&gt; (attribuée à l&apos;élève &lt;b&gt;{1} {2}&lt;/b&gt;)&lt;br/&gt; n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5176"/>
        <source>Remember to assign a valid class or to create this class.</source>
        <translation>Pensez à lui attribuer une classe valide ou à créer cette classe.</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1058"/>
        <source>Here you set the number of decimal places for notes.</source>
        <translation>Réglez ici le nombre de décimales après la virgule pour les notes.</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1703"/>
        <source>The names of created groups begin by what you specify here and will be followed by the class name.&lt;br/&gt;&lt;br/&gt;If you leave this field blank, the group name will be that of the class.</source>
        <translation>Les noms des groupes créés commenceront par ce que vous indiquerez ici et seront suivis du nom de la classe.&lt;br/&gt;&lt;br/&gt;Si vous laissez ce champ vide, le nom du groupe sera celui de la classe.</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="1345"/>
        <source>Columns titles:</source>
        <translation>Titres des colonnes :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5901"/>
        <source>FTP Port:</source>
        <translation>Port FTP :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5904"/>
        <source>The default FTP port is 21. &lt;br/&gt;Do not change unless you are sure it is necessary.</source>
        <translation>Le port FTP par défaut est 21. &lt;br/&gt;Ne le modifiez que si vous êtes certain que c&apos;est nécessaire.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10921"/>
        <source>Edit this evaluation</source>
        <translation>Éditer cette évaluation</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="10929"/>
        <source>&lt;p&gt;You can use the following characters, &lt;br/&gt;but respect the case (upper or lower case):&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;b&gt;{0}&lt;/b&gt;.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Vous pouvez utiliser les caractères suivants, &lt;br/&gt;mais respectez la casse (majuscules ou minuscules) :&lt;/p&gt;&lt;p align=&quot;center&quot;&gt;&lt;b&gt;{0}&lt;/b&gt;.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="533"/>
        <source>VERAC will be displayed in a simplified interface.</source>
        <translation>VÉRAC va s&apos;afficher en interface simplifiée.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="535"/>
        <source>It presents the actions needed to get started.</source>
        <translation>Celle-ci ne présente que les actions indispensables pour débuter.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="537"/>
        <source>Remember to enable the complete interface once you are more comfortable with the software.</source>
        <translation>Pensez à activer l&apos;interface complète dès que vous serez plus à l&apos;aise avec le logiciel.</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="204"/>
        <source>Remove the user</source>
        <translation>Retirer l&apos;utilisateur</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="207"/>
        <source>Remove the selected user from the comboBox</source>
        <translation>Retire l&apos;utilisateur sélectionné de la liste déroulante</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4518"/>
        <source>Management of addresses</source>
        <translation>Gestion des adresses</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4518"/>
        <source>To manage manually the addresses of students</source>
        <translation>Pour gérer la liste des adresses des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5514"/>
        <source>Update addresses from SIECLE</source>
        <translation>Mettre à jour les adresses depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="1293"/>
        <source>The XML file provided does not seem to be the right one.</source>
        <translation>Le fichier XML fourni n&apos;a pas l&apos;air d&apos;être le bon.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="280"/>
        <source>Addresses</source>
        <translation>Adresses</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4945"/>
        <source>perso</source>
        <translation>perso</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4948"/>
        <source>other</source>
        <translation>autre</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4971"/>
        <source>Personal address of the student:</source>
        <translation>Adresse personnelle de l&apos;élève :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4980"/>
        <source>First address:</source>
        <translation>Première adresse :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4989"/>
        <source>Second address:</source>
        <translation>Seconde adresse :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4998"/>
        <source>Other address:</source>
        <translation>Autre adresse :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4946"/>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4947"/>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1518"/>
        <source>Would you post the referential DB?</source>
        <translation>Voulez-vous poster la base du référentiel ?</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="270"/>
        <source>Place your selection in the list on the right.</source>
        <translation>Placez votre sélection dans la liste située à droite.</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="273"/>
        <source>Place and order your selection in the list on the right.</source>
        <translation>Placez et ordonnez votre sélection dans la liste située à droite.</translation>
    </message>
    <message>
        <location filename="../libs/utils_2lists.py" line="276"/>
        <source>To select all, you can leave the empty right list.</source>
        <translation>Pour tout sélectionner, vous pouvez laisser la liste de droite vide.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1331"/>
        <source>School year:</source>
        <translation>Année scolaire :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1338"/>
        <source>Use the year to end of the school year.</source>
        <translation>Utilisez l&apos;année de fin de l&apos;année scolaire.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1342"/>
        <source>School year title:</source>
        <translation>Titre de l&apos;année scolaire :</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="662"/>
        <source>Use addresses</source>
        <translation>Utiliser les adresses</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="560"/>
        <source>Files names</source>
        <translation>Noms des fichiers</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="648"/>
        <source>Create only the class file</source>
        <translation>Créer uniquement le fichier de la classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="665"/>
        <source>Number lines for addresses:</source>
        <translation>Nombre de lignes pour les adresses :</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="149"/>
        <source>Tools</source>
        <translation>Outils</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5514"/>
        <source>Update addresses from ResponsablesAvecAdresses.xml file exported from SIECLE</source>
        <translation>Mettre à jour les adresses depuis le fichier ResponsablesAvecAdresses.xml exporté depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2861"/>
        <source>Choose a list of shared competences among the proposed models</source>
        <translation>Choisir une liste de compétences partagées parmi les modèles proposés</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2875"/>
        <source>School report</source>
        <translation>Bulletin</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="2904"/>
        <source>Follow</source>
        <translation>Suivi</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1214"/>
        <source>Do not import this</source>
        <translation>Ne pas l&apos;importer</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3033"/>
        <source>All Teachers</source>
        <translation>Tous les professeurs</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4621"/>
        <source>Export Lists of teachers to ods</source>
        <translation>Exporter la liste des profs en ods</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="4621"/>
        <source>Export Lists of teachers of the school in an ODF spreadsheet file (* .ods)</source>
        <translation>Exporter la liste des professeurs de l&apos;établissement en un fichier tableur ODF (*.ods)</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="642"/>
        <source>Show only colors</source>
        <translation>N&apos;afficher que les couleurs</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5316"/>
        <source>Hide empty columns</source>
        <translation>Masquer les colonnes vides</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5316"/>
        <source>Displays only columns with assessments</source>
        <translation>N&apos;affiche que les colonnes comportant des évaluations</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="1560"/>
        <source>Leo</source>
        <translation>Léo</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2351"/>
        <source>use a log file</source>
        <translation>utiliser un fichier log</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5963"/>
        <source>Default language of the website:</source>
        <translation>Langue par défaut du site web :</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5978"/>
        <source>FTP access (or SFTP)</source>
        <translation>Accès FTP (ou SFTP)</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6001"/>
        <source>Installation folders</source>
        <translation>Dossiers d&apos;installation</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6017"/>
        <source>HTTP access (or HTTPS)</source>
        <translation>Accès HTTP (ou HTTPS)</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="1048"/>
        <source>French</source>
        <translation>Français</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="1048"/>
        <source>English</source>
        <translation>Anglais</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="132"/>
        <source>Management of photos</source>
        <translation>Gestion des photos</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4527"/>
        <source>To manage the photos of students</source>
        <translation>Pour gérer les photos des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1813"/>
        <source>Send documents</source>
        <translation>Envoyer les documents</translation>
    </message>
    <message>
        <location filename="../libs/admin_docs.py" line="1813"/>
        <source>To send the documents and the database on the website</source>
        <translation>Pour envoyer les documents et la base de données sur le site web</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="60"/>
        <source>Sending photos</source>
        <translation>Envoi des photos</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="106"/>
        <source>Would you upload photos to your website now?</source>
        <translation>Voulez-vous envoyer les photos sur votre site web maintenant ?</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="142"/>
        <source>Trombinoscope</source>
        <translation>Trombinoscope</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="296"/>
        <source>You can view below photos &lt;br/&gt;and modify them one by one by drag and drop from your computer&apos;s file browser.&lt;br/&gt;A mass import procedure is available in the Tools tab.</source>
        <translation>Vous pouvez visualiser les photos ci-dessous &lt;br/&gt;et les modifier une à une par glisser-déposer depuis le navigateur de fichiers de votre ordinateur. &lt;br/&gt;Une procédure d&apos;importation en masse est disponible dans l&apos;onglet outils.</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="636"/>
        <source>Import photos</source>
        <translation>Importer des photos</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="850"/>
        <source>Import photos from a directory</source>
        <translation>Importer des photos depuis un dossier de votre ordinateur</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="654"/>
        <source>Send photos</source>
        <translation>Envoyer les photos</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="654"/>
        <source>To send the photos on the website</source>
        <translation>Envoyer les photos sur le site web de votre établissement</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="788"/>
        <source>Select the folder containing the photos</source>
        <translation>Sélectionnez le répertoire contenant les photos</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="855"/>
        <source>Allocated photos</source>
        <translation>Photos attribuées</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="856"/>
        <source>Unassigned photos</source>
        <translation>Photos non attribuées</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="857"/>
        <source>Balance sheet</source>
        <translation>Bilan</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="688"/>
        <source>Indicated below the file name format to import.</source>
        <translation>Indiquez ci-dessous le format des noms des fichiers à importer.</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="692"/>
        <source>Use the following benchmarks (see help for more explanation):</source>
        <translation>Utilisez les repères suivants (voir l&apos;aide pour plus d&apos;explications) :</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="695"/>
        <source>: the student&apos;s name</source>
        <translation> : le nom de l&apos;élève</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="697"/>
        <source>: her first name</source>
        <translation> : son prénom</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="699"/>
        <source>: her class</source>
        <translation> : sa classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="701"/>
        <source>: her identifier</source>
        <translation> : son identifiant</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="704"/>
        <source>[NAME]</source>
        <translation>[NOM]</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="705"/>
        <source>[FirstName]</source>
        <translation>[Prénom]</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="706"/>
        <source>[Class]</source>
        <translation>[Classe]</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="707"/>
        <source>[ID]</source>
        <translation>[ID]</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="777"/>
        <source>The proposed format contains errors.</source>
        <translation>Le format proposé contient des erreurs.</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="980"/>
        <source>Class Trombinoscope:</source>
        <translation>Trombinoscope de la classe :</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="429"/>
        <source>Trombinoscope of all students</source>
        <translation>Trombinoscope de tous les élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="304"/>
        <source>PDF</source>
        <translation>PDF</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="308"/>
        <source>Create a pdf file of this trombinoscope</source>
        <translation>Créer un fichier pdf de ce trombinoscope</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="645"/>
        <source>Clear photos</source>
        <translation>Nettoyer les photos</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="951"/>
        <source>Clear photos of students who left the school</source>
        <translation>Supprimer les photos des élèves ayant quitté l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="956"/>
        <source>Number of photos deleted:</source>
        <translation>Nombre de photos supprimées :</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5624"/>
        <source>Clear if unfavorable</source>
        <translation>Effacer si défavorable</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5624"/>
        <source>Clear evaluation if it is unfavorable to the results of the student on this item</source>
        <translation>Effacer l&apos;évaluation si elle elle est défavorable aux résultats de l&apos;élève sur cet item</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1736"/>
        <source>Search</source>
        <translation>Rechercher</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1739"/>
        <source>Automatically search the file</source>
        <translation>Lancer une recherche automatique du fichier</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2398"/>
        <source>Connection timeout test</source>
        <translation>Délai de test de connexion</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2404"/>
        <source>Time (in seconds) the connection test facility website.</source>
        <translation>Délai (en secondes) du test de connexion au site web de l&apos;établissement.</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5696"/>
        <source>phones</source>
        <translation>téléphones</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5703"/>
        <source>LEGAL REPRESENTATIVE</source>
        <translation>REPRÉSENTANT LÉGAL</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5704"/>
        <source>OTHER REPRESENTATIVE</source>
        <translation>AUTRE REPRÉSENTANT</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5705"/>
        <source>PERSONAL</source>
        <translation>PERSONNEL</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5706"/>
        <source>PORTABLE</source>
        <translation>PORTABLE</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5707"/>
        <source>PROFESSIONAL</source>
        <translation>PROFESSIONNEL</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5524"/>
        <source>Addresses, Phones and Mails SIECLE to ODS</source>
        <translation>Adresses, téléphones et mails de SIECLE vers ODS</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5524"/>
        <source>Export as ODF data (addresses + phone numbers + mails) in the file ResponsablesAvecAdresses.xml</source>
        <translation>Exporte en ODS les données (adresses + numéros de téléphone + mails) contenues dans le fichier ResponsablesAvecAdresses.xml</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5695"/>
        <source>addresses</source>
        <translation>adresses</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5701"/>
        <source>ADDRESS</source>
        <translation>ADRESSE</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5702"/>
        <source>OTHER ADDRESS</source>
        <translation>AUTRE ADRESSE</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4690"/>
        <source>Export bulletin assessments to ODS</source>
        <translation>Exporter les évaluations du bulletin en ODS</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4724"/>
        <source>Create an ODS file containing details of assessments of the bulletin (one tab per class).</source>
        <translation>Crée un fichier ODS contenant les détails des évaluations du bulletin (un onglet par classe).</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4620"/>
        <source>bulletins</source>
        <translation>bulletins</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="4695"/>
        <source>Creating a tab for each class</source>
        <translation>Création d&apos;un onglet pour chaque classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5697"/>
        <source>mails</source>
        <translation>mails</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5708"/>
        <source>MAIL ADDRESS</source>
        <translation>ADRESSE MAIL</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="3679"/>
        <source>The number of the school year will be incremented by the value indicated below.&lt;br/&gt;It must be the end of the new school year (eg 2017 for 2016-2017)&lt;br/&gt; and should not be changed during the school year.&lt;br/&gt;You can correct the value proposed below if it is not correct.&lt;br/&gt;&lt;br/&gt;By cons, you can change the title of the school year as you see fit&lt;br/&gt; in the general configuration of the facility.</source>
        <translation>Le numéro de l&apos;année scolaire sera incrémenté selon la valeur indiquée ci-dessous.&lt;br/&gt;Il doit correspondre à la fin de la nouvelle année scolaire (par exemple 2017 pour 2016-2017)&lt;br/&gt; et ne doit pas être modifié en cours d&apos;année scolaire.&lt;br/&gt;Vous pouvez corriger la valeur proposée ci-dessous si elle n&apos;est pas correcte.&lt;br/&gt;&lt;br/&gt;Par contre, vous pourrez modifier le titre de l&apos;année scolaire comme bon vous semble&lt;br/&gt; dans la configuration générale de l&apos;établissement.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1074"/>
        <source>The administrator of your institution has made&lt;br/&gt; a change of school year.&lt;br/&gt;&lt;br/&gt;You should do a &lt;b&gt;&quot;clean for another year&quot;&lt;/b&gt;&lt;br/&gt; (action available in the &lt;b&gt;&quot;File&quot;&lt;/b&gt; menu) before sending your file.&lt;br/&gt;&lt;br/&gt;If you think this is due to an error,&lt;br/&gt; you can also fix the school year included in your file&lt;br/&gt; (action available via the &lt;b&gt;&quot;Tools -&gt; Settings&quot;&lt;/b&gt; menu, &lt;b&gt;&quot;Other&quot;&lt;/b&gt; tab).</source>
        <translation>L&apos;administrateur de votre établissement a procédé&lt;br/&gt; à un changement d&apos;année scolaire.&lt;br/&gt;&lt;br/&gt;Vous devriez faire un &lt;b&gt;&quot;nettoyage pour une nouvelle année&quot;&lt;/b&gt;&lt;br/&gt; (action accessible dans le menu &lt;b&gt;&quot;Fichier&quot;&lt;/b&gt;) avant d&apos;envoyer votre fichier.&lt;br/&gt;&lt;br/&gt;Si vous pensez que cela est dû à une erreur,&lt;br/&gt; 
vous pouvez aussi corriger l&apos;année scolaire inscrite dans votre fichier&lt;br/&gt; (action accessible par le menu &lt;b&gt;&quot;Outils → Paramètres&quot;&lt;/b&gt;, onglet &lt;b&gt;&quot;Autres&quot;&lt;/b&gt;).</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="1108"/>
        <source>The school year included in your file is later than your institution.&lt;br/&gt; Your feedback will not be recovered until the establishment&lt;br/&gt; has not made the change of school year.&lt;br/&gt;&lt;br/&gt;If you think this is due to an error,&lt;br/&gt; you can correct the school year included in your file&lt;br/&gt; (action available via the &lt;b&gt;&quot;Tools -&gt; Settings&quot;&lt;/b&gt; menu, &lt;b&gt;&quot;Other&quot;&lt;/b&gt; tab).</source>
        <translation>L&apos;année scolaire inscrite dans votre fichier est postérieure à celle de votre établissement.&lt;br/&gt; Vos évaluations ne seront pas récupérées tant que l&apos;établissement&lt;br/&gt; n&apos;aura pas procédé au changement d&apos;année scolaire.&lt;br/&gt;&lt;br/&gt;Si vous pensez que cela est dû à une erreur,&lt;br/&gt; vous pouvez corriger l&apos;année scolaire inscrite dans votre fichier&lt;br/&gt; (action accessible par le menu &lt;b&gt;&quot;Outils → Paramètres&quot;&lt;/b&gt;, onglet &lt;b&gt;&quot;Autres&quot;&lt;/b&gt;).</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="5640"/>
        <source>Base model:</source>
        <translation>Modèle de base :</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="275"/>
        <source>Various</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="2387"/>
        <source>School year</source>
        <translation>Année scolaire</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="1362"/>
        <source>Absences and delays</source>
        <translation>Absences et retards</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="6556"/>
        <source>Message for userss</source>
        <translation>Message pour les utilisateurs</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="650"/>
        <source>Justified absences</source>
        <translation>Absences justifiées</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9448"/>
        <source>Number of half-days of justified absence</source>
        <translation>Nombre de demi-journées d&apos;absence justifiées</translation>
    </message>
    <message>
        <location filename="../libs/utils_export_xml.py" line="673"/>
        <source>Unjustified absences</source>
        <translation>Absences non justifiées</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9459"/>
        <source>Number of half-days of unjustified absence</source>
        <translation>Nombre de demi-journées d&apos;absence non justifiées</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9477"/>
        <source>Hours missed</source>
        <translation>Cours manqués</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9481"/>
        <source>Number of classroom hours missed</source>
        <translation>Nombre d&apos;heures de cours manquées</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="320"/>
        <source>Export LSU</source>
        <translation>Export LSU</translation>
    </message>
    <message>
        <location filename="../libs/utils_help.py" line="322"/>
        <source>To export results to LSU (Livret Scolaire Unique)</source>
        <translation>Pour exporter les résultats vers LSU (Livret Scolaire Unique)</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="745"/>
        <source>Create ODS spreadsheet for an assessment</source>
        <translation>Créer un tableau ODS pour une évaluation</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5376"/>
        <source>Create ODS spreadsheet with a list of items and the valuation levels, ready to be placed on your statements</source>
        <translation>Crée un tableau ODS avec une liste d&apos;items et les niveaux d&apos;évaluation, prêt à être placé sur vos énoncés</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3143"/>
        <source>XML File</source>
        <translation>Fichier XML</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3147"/>
        <source>xml files (*.xml)</source>
        <translation>fichiers xml (*.xml)</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="749"/>
        <source>Select below the items you want by moving them to the right list.</source>
        <translation>Sélectionnez ci-dessous les items que vous voulez en les déplaçant dans la liste de droite.</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="880"/>
        <source>Item</source>
        <translation>Item</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="816"/>
        <source>Very good control</source>
        <translation>Très bonne maîtrise</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="817"/>
        <source>Satisfactory control</source>
        <translation>Maîtrise satisfaisante</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="818"/>
        <source>Delicate control</source>
        <translation>Maîtrise fragile</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="819"/>
        <source>Insufficient control</source>
        <translation>Maîtrise insuffisante</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="820"/>
        <source>The student was marked missing</source>
        <translation>L&apos;élève a été marqué absent</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="932"/>
        <source>COLORS</source>
        <translation>COULEURS</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="990"/>
        <source>LEGENDS</source>
        <translation>LÉGENDES</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="121"/>
        <source>Notes calculations</source>
        <translation>Calculs Notes</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1011"/>
        <source>HOW NOTES ARE CALCULATED</source>
        <translation>COMMENT SONT CALCULÉES LES NOTES</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1017"/>
        <source>Calculation method:</source>
        <translation>Mode de calcul :</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1025"/>
        <source>Default</source>
        <translation>Par défaut</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1027"/>
        <source>As DNB</source>
        <translation>Comme pour le DNB</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="1029"/>
        <source>Custom</source>
        <translation>Personnalisé</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="278"/>
        <source>School Code:</source>
        <translation>Code UAI :</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="146"/>
        <source>To create a new separator</source>
        <translation>Pour créer un nouveau séparateur</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="152"/>
        <source>Delete the selected separator</source>
        <translation>Supprimer le séparateur sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="327"/>
        <source>Change the text of a separator:</source>
        <translation>Modifier le texte d&apos;un séparateur :</translation>
    </message>
    <message>
        <location filename="../libs/admin_structures.py" line="348"/>
        <source>NEW SEPARATOR</source>
        <translation>NOUVEAU SÉPARATEUR</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="4572"/>
        <source>Exporting the file failed or you have canceled.</source>
        <translation>L&apos;exportation du fichier a échoué ou vous l&apos;avez annulée.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="111"/>
        <source>Export LSU (Livret Scolaire Unique)</source>
        <translation>Export LSU (Livret Scolaire Unique)</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="127"/>
        <source>Checks</source>
        <translation>Vérifications</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="139"/>
        <source>File creation</source>
        <translation>Création du fichier</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="345"/>
        <source>S: core</source>
        <translation>S : tronc commun</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="346"/>
        <source>O: mandatory option</source>
        <translation>O : option obligatoire</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="347"/>
        <source>F: optional extra</source>
        <translation>F : option facultative</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="348"/>
        <source>L: Academic addition to the program</source>
        <translation>L : ajout académique au programme</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="349"/>
        <source>R: religious education</source>
        <translation>R : enseignement religieux</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="350"/>
        <source>X: specific measure</source>
        <translation>X : mesure spécifique</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="175"/>
        <source>If you change tab without clicking&lt;br/&gt;on the button &lt;b&gt;Apply&lt;/b&gt;,&lt;br/&gt;the modifications will not be recorded.</source>
        <translation>Si vous changez d&apos;onglet sans cliquer&lt;br/&gt;sur le bouton &lt;b&gt;Appliquer&lt;/b&gt;,&lt;br/&gt;les modifications ne seront pas enregistrées.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2899"/>
        <source>Create File</source>
        <translation>Créer le fichier</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2899"/>
        <source>To create the xml file to export LSU</source>
        <translation>Pour créer le fichier xml d&apos;export vers LSU</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="761"/>
        <source>Officials</source>
        <translation>Responsables</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1188"/>
        <source>teacher</source>
        <translation>prof</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="1189"/>
        <source>director</source>
        <translation>personnel de direction</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="663"/>
        <source>To create pdf files of trombinoscopes of all classes</source>
        <translation>Pour créer les fichiers pdf des trombinoscopes de toutes les classes</translation>
    </message>
    <message>
        <location filename="../libs/admin_photos.py" line="663"/>
        <source>Classes PDF</source>
        <translation>PDF des classes</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="475"/>
        <source>The &quot;personal.csv&quot; file could not be saved in the &lt;b&gt;{0}&lt;/b&gt; folder.</source>
        <translation>Le fichier &quot;personal.csv&quot; n&apos;a pas pu être enregistré dans le dossier &lt;b&gt;{0}&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="480"/>
        <source>This folder may be write protected.</source>
        <translation>Ce dossier est peut-être protégé en écriture.</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="483"/>
        <source>A copy of the file &quot;personal.csv&quot; was saved in the folder you selected.</source>
        <translation>Une copie du fichier &quot;personal.csv&quot; a été enregistrée dans le dossier que vous avez sélectionné.</translation>
    </message>
    <message>
        <location filename="../libs/utils_config.py" line="487"/>
        <source>So you can place it in the correct folder to resolve the problem.</source>
        <translation>Ainsi vous pourrez le placer dans le bon dossier pour résoudre le problème.</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5454"/>
        <source>Export students in subfolders</source>
        <translation>Exporter des élèves en sous-dossiers</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5454"/>
        <source>Create subfolders from a list of students</source>
        <translation>Pour créer des sous-dossiers à partir d&apos;une sélection d&apos;élèves du groupe actuel</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4625"/>
        <source>school year</source>
        <translation>année scolaire</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="284"/>
        <source>Back to School and End of Periods:</source>
        <translation>Rentrée scolaire et fin des périodes :</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="288"/>
        <source>Back to School</source>
        <translation>Rentrée scolaire</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1381"/>
        <source>8 components of the referential</source>
        <translation>Les 8 composantes du socle</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1384"/>
        <source>Double-click on an element allows you to edit.</source>
        <translation>Un double-clic sur un élément vous permet de le modifier.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2836"/>
        <source>You must select at least one period or end-of-cycle balances.</source>
        <translation>Vous devez sélectionner au moins une période ou les bilans de fin de cycle.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1814"/>
        <source>EPI management (Interdisciplinary Teaching Practices) of the establishment</source>
        <translation>Gestion des EPI (Enseignements Pratiques Interdisciplinaires) de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1827"/>
        <source>Thematic:</source>
        <translation>Thématique :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1205"/>
        <source>Subjects:</source>
        <translation>Matières :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1111"/>
        <source>Choose Teacher</source>
        <translation>Choisir un prof</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1200"/>
        <source>EPI reference of the school:</source>
        <translation>EPI de référence de l&apos;établissement :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1530"/>
        <source>no EPI for this thematic</source>
        <translation>pas d&apos;EPI pour cette thématique</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1535"/>
        <source>select EPI</source>
        <translation>sélectionnez un EPI</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1113"/>
        <source>Teachers:</source>
        <translation>Professeurs :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1207"/>
        <source>double-click to select a teacher</source>
        <translation>double-clic pour sélectionner un prof</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2245"/>
        <source>AP management (Personalized support) of the establishment</source>
        <translation>Gestion de l&apos;AP (Accompagnement personnalisé) de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1235"/>
        <source>AP reference of the school:</source>
        <translation>AP de référence de l&apos;établissement :</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1549"/>
        <source>no AP</source>
        <translation>pas d&apos;AP</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1554"/>
        <source>select AP</source>
        <translation>sélectionnez un AP</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3446"/>
        <source>Appreciation missing.</source>
        <translation>Appréciation manquante.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3182"/>
        <source>Missing Program Elements.</source>
        <translation>Éléments de programme manquants.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2908"/>
        <source>Check File</source>
        <translation>Vérifier un fichier</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2908"/>
        <source>To check if an export xml file to LSU is valid</source>
        <translation>Pour vérifier si un fichier xml d&apos;export vers LSU est valide</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="360"/>
        <source>Update subjects from SIECLE</source>
        <translation>Mettre à jour les matières depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="364"/>
        <source>Update subjects from Nomenclature.xml file exported from SIECLE</source>
        <translation>Mettre à jour les matières depuis le fichier Nomenclature.xml exporté depuis SIECLE</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="558"/>
        <source>Subjects in the file have not been assigned.</source>
        <translation>Des matières présentes dans le fichier n&apos;ont pas été attribuées.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="561"/>
        <source>You can see the list below and assign them by hand.</source>
        <translation>Vous pouvez en afficher la liste ci-dessous et les attribuer à la main.</translation>
    </message>
    <message>
        <location filename="../libs/utils_connect.py" line="248"/>
        <source>Open other teacher file</source>
        <translation>Ouvrir le fichier d&apos;un autre prof</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3017"/>
        <source>File is valid</source>
        <translation>Le fichier est valide</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3024"/>
        <source>File is invalid</source>
        <translation>Le fichier n&apos;est pas valide</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3026"/>
        <source>line</source>
        <translation>ligne</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="777"/>
        <source>Types of assessments</source>
        <translation>Types d&apos;évaluations</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="963"/>
        <source>positioning</source>
        <translation>positionnement</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="964"/>
        <source>notes</source>
        <translation>notes</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="2954"/>
        <source>Calculation of the components of the socle</source>
        <translation>Calcul des composantes du socle</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2857"/>
        <source>End-of-cycle balances</source>
        <translation>Bilans de fin de cycle</translation>
    </message>
    <message>
        <location filename="../libs/admin_web.py" line="106"/>
        <source>Components of the socle</source>
        <translation>Composantes du socle</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="792"/>
        <source>Types of classes and cycles</source>
        <translation>Types de classes et cycles</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1000"/>
        <source>cycle</source>
        <translation>cycle</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7522"/>
        <source>Number of balances</source>
        <translation>Nombre de bilans</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7523"/>
        <source>Percentage of balances</source>
        <translation>Pourcentage de bilans</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7526"/>
        <source>Number of items</source>
        <translation>Nombre d&apos;items</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="7527"/>
        <source>Percentage of items</source>
        <translation>Pourcentage d&apos;items</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="830"/>
        <source>LSU_PAR_LSU</source>
        <translation>Parcours éducatifs</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="831"/>
        <source>LSU_PAR_AVN</source>
        <translation>Parcours avenir</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="832"/>
        <source>LSU_PAR_CIT</source>
        <translation>Parcours citoyen</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="833"/>
        <source>LSU_PAR_ART</source>
        <translation>Parcours d&apos;éducation artistique et culturelle</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="834"/>
        <source>LSU_PAR_SAN</source>
        <translation>Parcours éducatif de santé</translation>
    </message>
    <message>
        <location filename="../libs/prof_groupes.py" line="1261"/>
        <source>Type of educational path:</source>
        <translation>Type de parcours éducatif :</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="836"/>
        <source>LSU_ACC_PAP</source>
        <translation>Plan d’accompagnement personnalisé</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="837"/>
        <source>LSU_ACC_PAI</source>
        <translation>Projet d’accueil individualisé</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="838"/>
        <source>LSU_ACC_PPRE</source>
        <translation>Programme personnalisé de réussite éducative</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="839"/>
        <source>LSU_ACC_PPS</source>
        <translation>Projet personnalisé de scolarisation</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="840"/>
        <source>LSU_ACC_ULIS</source>
        <translation>Unité localisée pour l’inclusion scolaire</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="841"/>
        <source>LSU_ACC_UPE2A</source>
        <translation>Unité pédagogique pour élèves allophones arrivants</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="842"/>
        <source>LSU_ACC_SEGPA</source>
        <translation>Section d’enseignement général adapté</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="846"/>
        <source>LSU_COMP_AUC</source>
        <translation>Aucun</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="847"/>
        <source>LSU_COMP_LCA</source>
        <translation>Langues et cultures de l&apos;Antiquité</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="848"/>
        <source>LSU_COMP_LCR</source>
        <translation>Langue et culture régionale</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="849"/>
        <source>LSU_COMP_PRO</source>
        <translation>Découverte professionnelle</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="850"/>
        <source>LSU_COMP_LSF</source>
        <translation>Langue des signes française</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="851"/>
        <source>LSU_COMP_LVE</source>
        <translation>Langue vivante étrangère</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8354"/>
        <source>Positioning</source>
        <translation>Positionnement</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8443"/>
        <source>validated.</source>
        <translation>validé.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8355"/>
        <source>can be validated</source>
        <translation>peut être validé</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5082"/>
        <source>Guiding</source>
        <translation>Accompagnement</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5082"/>
        <source>ShowGuidingStatusTip</source>
        <translation>Indiquer les modalités d&apos;accompagnement pour le groupe d&apos;élèves sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="9730"/>
        <source>You can give a detailed description of the guiding.</source>
        <translation>Vous pouvez donner une description détaillée de cet accompagnement.</translation>
    </message>
    <message>
        <location filename="../libs/prof.py" line="8439"/>
        <source>calculated by VERAC.</source>
        <translation>calculé par VÉRAC.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1397"/>
        <source>Educational Tours</source>
        <translation>Parcours éducatifs</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="1407"/>
        <source>Additional Teaching</source>
        <translation>Enseignements de complément</translation>
    </message>
    <message>
        <location filename="../libs/admin_compteur.py" line="340"/>
        <source>Week {0} (from {1} to {2})</source>
        <translation>Semaine {0} (du {1} au {2})</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="478"/>
        <source>Closing of message in {0}s.</source>
        <translation>Fermeture du message dans {0}s.</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="199"/>
        <source>Recovery level</source>
        <translation>Niveau des récupérations</translation>
    </message>
    <message>
        <location filename="../libs/verac_scheduler.pyw" line="192"/>
        <source>Complete calculations every time</source>
        <translation>Calculs complets à chaque fois</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="63"/>
        <source>Step</source>
        <translation>Étape</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="64"/>
        <source>class</source>
        <translation>classe</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="69"/>
        <source>School Reports</source>
        <translation>Bulletins périodiques</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="70"/>
        <source>Referential Reports</source>
        <translation>Relevés de référentiel</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="297"/>
        <source>Reports type, period and students</source>
        <translation>Type de relevés, période et élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="304"/>
        <source>Type of reports</source>
        <translation>Type de relevés</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="321"/>
        <source>Sort by origin</source>
        <translation>Trier par origine</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="331"/>
        <source>Template file</source>
        <translation>Fichier modèle</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="923"/>
        <source>Files creation</source>
        <translation>Création des fichiers</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="934"/>
        <source>Create Files</source>
        <translation>Créer les fichiers</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="934"/>
        <source>Launch files making</source>
        <translation>Lance la fabrication des fichiers</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="943"/>
        <source>Open the folder containing the created files</source>
        <translation>Ouvre le dossier contenant les fichiers créés</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="71"/>
        <source>Annual Reports</source>
        <translation>Bilans annuels</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="72"/>
        <source>DNB Reports (and Notanet file)</source>
        <translation>Fiches DNB (et fichier Notanet)</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="2950"/>
        <source>UNKNOWN</source>
        <translation>INCONNU</translation>
    </message>
    <message>
        <location filename="../libs/utils_functions.py" line="763"/>
        <source>DURATION:</source>
        <translation>DURÉE :</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3812"/>
        <source>Clear the referential</source>
        <translation>Nettoyage du référentiel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4675"/>
        <source>To erase records from previous years</source>
        <translation>Pour effacer des enregistrements des années précédentes</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3476"/>
        <source>Selection of cleaning type and students</source>
        <translation>Sélection du type de nettoyage et des élèves</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3531"/>
        <source>Check the classes to be processed</source>
        <translation>Cochez les classes à traiter</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3490"/>
        <source>Type of cleaning</source>
        <translation>Type de nettoyage</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3495"/>
        <source>socle only - previous years</source>
        <translation>socle seulement - années précédentes</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3497"/>
        <source>all referential - previous years</source>
        <translation>référentiel complet - années précédentes</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3499"/>
        <source>socle only - all years</source>
        <translation>socle seulement - toutes les années</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3501"/>
        <source>all referential - all years</source>
        <translation>référentiel complet - toutes les années</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3515"/>
        <source>Selection (classes or students)</source>
        <translation>Sélection (classes ou élèves)</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3664"/>
        <source>Launch the procedure</source>
        <translation>Lancer la procédure</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3664"/>
        <source>Launch the referential cleanup procedure</source>
        <translation>Lancer la procédure de nettoyage du référentiel</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3673"/>
        <source>Post the DB</source>
        <translation>Poster la base</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3673"/>
        <source>Post the referential DB to your website</source>
        <translation>Poster la base du référentiel sur le site web de l&apos;établissement</translation>
    </message>
    <message>
        <location filename="../libs/admin_calc_results.py" line="3653"/>
        <source>Cleaning and posting</source>
        <translation>Nettoyage et envoi</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="596"/>
        <source>SEX</source>
        <translation>SEXE</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="601"/>
        <source>BIRTH</source>
        <translation>NAISSANCE</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4294"/>
        <source>Shortcut to</source>
        <translation>Raccourci vers</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4283"/>
        <source>You must install pywin32 to do this action.</source>
        <translation>Vous devez installer pywin32 pour faire cette action.</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4553"/>
        <source>Display only if there is a problem</source>
        <translation>N&apos;afficher que s&apos;il y a un problème</translation>
    </message>
    <message>
        <location filename="../libs/utils_export.py" line="672"/>
        <source>students</source>
        <translation>élèves</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="3115"/>
        <source>trombinoscope</source>
        <translation>trombinoscope</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5445"/>
        <source>Trombinoscope of the current group</source>
        <translation>Trombinoscope du groupe actuel</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5445"/>
        <source>To create a PDF file with the selected group&apos;s trombinoscope</source>
        <translation>Pour créer un fichier PDF avec le trombinoscope du groupe sélectionné</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4307"/>
        <source>VERAC Scheduler</source>
        <translation>Planificateur de VÉRAC</translation>
    </message>
    <message>
        <location filename="../libs/utils_webengine.py" line="133"/>
        <source>Open this link?</source>
        <translation>Ouvrir ce lien ?</translation>
    </message>
    <message>
        <location filename="../libs/admin_create_report.py" line="645"/>
        <source>Delete lines not evaluated</source>
        <translation>Supprimer les lignes non évaluées</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="843"/>
        <source>LSU_ACC_CTR</source>
        <translation>Contrat de réussite</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="3184"/>
        <source>Participates in the Homework done scheme.</source>
        <translation>Participe au dispositif Devoirs faits.</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="844"/>
        <source>LSU_ACC_DF</source>
        <translation>Devoirs faits</translation>
    </message>
    <message>
        <location filename="../libs/utils_filesdirs.py" line="138"/>
        <source>The scheduler</source>
        <translation>Le planificateur</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="853"/>
        <source>LSU_COMP_CHK</source>
        <translation>Chant choral</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="852"/>
        <source>LSU_COMP_LCE</source>
        <translation>Langues et cultures européennes</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4448"/>
        <source>Locked classes</source>
        <translation>Classes verrouillées</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="4830"/>
        <source>To view and edit the list of locked classes</source>
        <translation>Pour afficher et modifier la liste des classes verrouillées</translation>
    </message>
    <message>
        <location filename="../libs/admin.py" line="4453"/>
        <source>Place the classes to be locked in the right list.</source>
        <translation>Placez les classes à verrouiller dans la liste de droite.</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2866"/>
        <source>Digital skills</source>
        <translation>Compétences numériques</translation>
    </message>
    <message>
        <location filename="../libs/main.py" line="5086"/>
        <source>ShowDigitalSkillsStatusTip</source>
        <translation>Pour évaluer les compétences numériques</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="855"/>
        <source>LSU_CN_INF</source>
        <translation>Information et données</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="856"/>
        <source>LSU_CN_INF_MEN</source>
        <translation>Mener une recherche et une veille d&apos;information</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="857"/>
        <source>LSU_CN_INF_GER</source>
        <translation>Gérer des données</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="858"/>
        <source>LSU_CN_INF_TRA</source>
        <translation>Traiter des données</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="859"/>
        <source>LSU_CN_COM</source>
        <translation>Communication et collaboration</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="860"/>
        <source>LSU_CN_COM_INT</source>
        <translation>Interagir</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="861"/>
        <source>LSU_CN_COM_PAR</source>
        <translation>Partager et publier</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="862"/>
        <source>LSU_CN_COM_COL</source>
        <translation>Collaborer</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="863"/>
        <source>LSU_CN_COM_SIN</source>
        <translation>S&apos;insérer dans le monde numérique</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="864"/>
        <source>LSU_CN_CRE</source>
        <translation>Création de contenus</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="865"/>
        <source>LSU_CN_CRE_TEX</source>
        <translation>Développer des documents textuels</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="866"/>
        <source>LSU_CN_CRE_MUL</source>
        <translation>Développer des documents multimédia</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="867"/>
        <source>LSU_CN_CRE_ADA</source>
        <translation>Adapter les documents à leur finalité</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="868"/>
        <source>LSU_CN_CRE_PRO</source>
        <translation>Programmer</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="869"/>
        <source>LSU_CN_PRO</source>
        <translation>Protection et sécurité</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="870"/>
        <source>LSU_CN_PRO_SEC</source>
        <translation>Sécuriser l&apos;environnement numérique</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="871"/>
        <source>LSU_CN_PRO_DON</source>
        <translation>Protéger les données personnelles et la vie privée</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="872"/>
        <source>LSU_CN_PRO_SAN</source>
        <translation>Protéger la santé, le bien-être et l&apos;environnement</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="873"/>
        <source>LSU_CN_ENV</source>
        <translation>Environnement numérique</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="874"/>
        <source>LSU_CN_ENV_RES</source>
        <translation>Résoudre des problèmes techniques</translation>
    </message>
    <message>
        <location filename="../libs/utils.py" line="875"/>
        <source>LSU_CN_ENV_EVO</source>
        <translation>Évoluer dans un environnement numérique</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2619"/>
        <source>Digital skills management</source>
        <translation>Gestion des compétences numériques</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2625"/>
        <source>Appreciation for the class:</source>
        <translation>Appréciation pour la classe :</translation>
    </message>
    <message>
        <location filename="../libs/admin_lsu.py" line="2870"/>
        <source>Insert the levels of mastery of digital skills</source>
        <translation>Insérer les niveaux de maîtrise des compétences numériques</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5870"/>
        <source>SFTP</source>
        <translation>SFTP</translation>
    </message>
    <message>
        <location filename="../libs/admin_edit.py" line="5867"/>
        <source>Use SFTP:</source>
        <translation>Utiliser SFTP :</translation>
    </message>
</context>
</TS>
