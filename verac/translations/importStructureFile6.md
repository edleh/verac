You can consult below the list of balances for the default profile (what will be on the bulletin for that subject).

**Color icons:**  
**blue:** personal balance; **magenta:** balance of the referential.

You can skip to the last step in checking the box at the bottom right.
