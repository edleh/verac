A file log is a file which is recorded in the file of configuration of **VÉRAC**.  
It is used to keep the trace of your actions, which is extremely useful in the event of bug.

You can nevertheless desactivate the writing of this file.
