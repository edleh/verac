Le **dossier des fichiers profs** est celui où seront enregistrés les bases des profs (profxx.sqlite).

Le **dossier de travail** est celui où seront enregistrés les documents des profs (exports par exemple) ; il peut être un dossier partagé du réseau.  
Si vous sélectionnez cette option, autant empêcher les utilisateurs de modifier le chemin de ce dossier (bouton rendu inactif dans la fenêtre de connexion).










