Vous pouvez consulter ci-dessous la liste des bilans du profil par défaut (ce qui sera sur les bulletins pour cette matière).

**Couleurs des icônes :**  
**bleu :** bilan personnel ; **magenta :** bilan du référentiel.

Vous pouvez passer directement à la dernière étape en cochant la case située en bas à droite.
