Sélectionnez les modèles de tableaux que vous voulez exporter en les déplaçant dans la liste située à droite.

Utilisez les touches Ctrl et Shift du clavier pour réaliser une sélection multiple.

Vous pouvez aussi sélectionner des tableaux qui ne sont pas liés à des modèles ; ils seront automatiquement transformés en modèles de tableaux dans le fichier de structure.
