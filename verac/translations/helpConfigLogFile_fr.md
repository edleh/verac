Un fichier log est un fichier qui est enregistré dans le dossier de configuration de **VÉRAC**.  
Il sert à garder la trace de vos actions, ce qui est fort utile en cas de bug.

Vous pouvez néanmoins désactiver l'écriture de ce fichier.
