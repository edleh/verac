Vous pouvez modifier des valeurs, mais aussi ajouter ou supprimer des lignes.

Vos modifications ne seront enregistrées que si vous cliquez sur le bouton Ok.
