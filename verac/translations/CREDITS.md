# The tools used to develop VÉRAC

#### Basic Tools
* [Python](https://www.python.org): programming language
* [Qt](https://www.qt.io): "toolkit" comprehensive (graphical user interface and a lot of things)
* [PyQt](https://riverbankcomputing.com): link between Python and Qt
* [SQLite](https://www.sqlite.org): databases

#### Other libraries used and other stuff
* [ODSlib](https://pypi.python.org/pypi/odslib): ODS manipulation
* [PyPDF2](https://github.com/mstamy2/PyPDF2): PDF manipulation
* [wkhtmltopdf](https://wkhtmltopdf.org): to create pdf files from html templates
* [Simple FTP Mirror](https://git.joonis.de/snippets/3): for updated website via FTP
* [pyspellchecker](https://github.com/barrust/pyspellchecker): for spell Checker
* [marked](https://github.com/markedjs/marked): to view the Markdown files

#### For the web interface
* [PHP](https://secure.php.net/): programming language
* [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript): programming language
* [Bootstrap](https://getbootstrap.com): framework CSS/JS
* [Chart.js](http://www.chartjs.org): pie charts and radar
* [Sortable](https://github.com/RubaXa/Sortable): reorderable lists by drag & drop (trombinoscopes)
* [Fixed-table](https://github.com/kevkan/fixed-table) : table with fixed header and sidebar

#### Third-party software used
* [LibreOffice](https://www.libreoffice.org): exports in ODF
* [Freeplane](https://www.freeplane.org): exports in mind maps
* [Blender](https://www.blender.org): for creating the logo
* [GIMP](https://www.gimp.org): image manipulation

#### Project website
* [TuxFamily](https://www.tuxfamily.org): free hosting for free people

#### Miscellaneous
* [GNU GPL 3](https://www.gnu.org/copyleft/gpl.html): GNU General Public License
