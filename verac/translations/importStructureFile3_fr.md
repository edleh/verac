Vous pouvez consulter ci-dessous la liste des items qui seront importés.

Si certains d'entre eux existent déjà dans votre structure actuelle, ils ne seront pas importés.

En cas de doute (par exemple si un item existant déjà a le même nom mais une description différente) il vous sera demandé quoi faire à la dernière étape.

Vous pouvez passer directement à la dernière étape en cochant la case située en bas à droite.
