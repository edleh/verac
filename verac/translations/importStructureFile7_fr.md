Vous pouvez consulter ci-dessous la liste des comptages qui seront importés.

Si certains d'entre eux existent déjà dans votre structure actuelle, ils ne seront pas importés.

En cas de doute il vous sera demandé quoi faire à la dernière étape.
