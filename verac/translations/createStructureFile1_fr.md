Sélectionnez les items que vous voulez exporter en les déplaçant dans la liste située à droite.

Utilisez les touches Ctrl et Shift du clavier pour réaliser une sélection multiple.

**Remarque : **à l'étape 4 (sélection des modèles de tableaux) les items liés aux modèles que vous sélectionnerez seront automatiquement ajoutés.
