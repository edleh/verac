Si **VÉRAC** est installé en réseau, le plus simple est de placer le dossier de configuration (**.Verac**) dans un dossier **lanConfig** situé à côté du dossier d'installation. Le dossier de configuration sera alors automatiquement détecté.  
Le bouton **"Créer le dossier de configuration"** vous permet de créer ce dossier facilement.

Sinon (**VÉRAC** installé sur chaque poste), vous pouvez définir un emplacement réseau pour le dossier de configuration, afin de le partager.  
Cochez alors la case et sélectionnez le chemin du dossier **.Verac**.










