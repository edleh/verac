La première étape sert à sélectionner ce qui sera effacé.

La dernière étape permet de lancer la procédure.

Cliquez sur le bouton **Aide** pour afficher l'aide détaillée dans votre navigateur.
