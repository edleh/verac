A profile is a list of assessments that will appear on the bulletin for a subject.  
In **VÉRAC**, you can create multiple profiles and assign different profiles to your groups (or to each of your students).

You can select below a **default profile** to be proposed when a group is created.  
Simply select the balances you want to put in this profile moving them to the list on the right.
