<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    CONNEXION À LA BASE CONFIGWEB
    ET LECTURE DES TABLES CONFIG ET STATE
****************************************************/

function errorMessageHeader() {
    $result = '';
    $result .= '<!DOCTYPE html>';
    $result .= '<html>';
    $result .= '<head>';
    $result .= '<meta charset="utf-8">';
    $result .= '<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">'."\n";
    $result .= '<link href="css/open-iconic-bootstrap.min.css" rel="stylesheet" type="text/css">'."\n";
    $result .= '</head>';
    $result .= '<body>';
    return $result;
    }

function configUnavailableMessage() {
    $result = '';
    $result .= message(
        'danger', 
        'La configuration du site est corrompue<br/>(base de données configweb.sqlite).', 
        "Prévenez l'administrateur du site afin qu'il y remédie."
        );
    $result .= '<br/><br/><br/>';
    $result .= message(
        'danger', 
        'The site configuration is corrupted<br/>(configweb.sqlite database).', 
        'Notify the site administrator to remedy it.'
        );
    return $result;
    }


try {
    $DB_CONFIGWEB = new PDO('sqlite:'.CHEMIN_CONFIG.'configweb.sqlite');
    $DB_CONFIGWEB -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

    $SQL = 'SELECT * FROM config';
    $STMT = $DB_CONFIGWEB -> prepare($SQL);
    if (!$STMT) {
        require_once('libs/fonctions_divers.php');
        $errorMessage = errorMessageHeader();
        $errorMessage .= configUnavailableMessage();
        $errorMessage .= '</body>';
        $errorMessage .= '</html>';
        exit($errorMessage);
        }
    $STMT -> execute();
    $configs = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($configs as $config) {
        $key_name = $config['key_name'];
        $value_int = $config['value_int'];
        $value_text = $config['value_text'];
        if (! defined($key_name)) {
            if ($value_int == '')
                define($key_name, $value_text);
            else
                define($key_name, $value_int);
            }
        }

    $SQL = 'SELECT * FROM state';
    $STMT = $DB_CONFIGWEB -> prepare($SQL);
    $STMT -> execute();
    $states = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($states as $state) {
        $key_name = $state['key_name'];
        $value_int = $state['value_int'];
        $value_text = $state['value_text'];
        if ($value_int == '')
            define($key_name, $value_text);
        else
            define($key_name, $value_int);
        }
    }
catch(PDOException $e) {
    $DB_CONFIGWEB -> rollBack();
    require_once('libs/fonctions_divers.php');
    $errorMessage = errorMessageHeader();
    $errorMessage .= configUnavailableMessage();
    $errorMessage .= '</body>';
    $errorMessage .= '</html>';
    exit($errorMessage);
    }

?>
