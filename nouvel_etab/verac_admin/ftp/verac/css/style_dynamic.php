<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    UN FICHIER CSS RÉCUPÉRANT LES LETTRES ET COULEURS
    DÉFINIES PAR L'ADMIN
****************************************************/

header("Content-type: text/css");

$DB_CONFIGWEB = new PDO('sqlite:../_private/configweb.sqlite');
$DB_CONFIGWEB -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

$SQL = 'SELECT * FROM config WHERE key_name IN ("COLOR_A", "COLOR_B", "COLOR_C", "COLOR_D","COLOR_X")';
$STMT = $DB_CONFIGWEB -> prepare($SQL);
$STMT -> execute();
$configs = ($STMT != '') ? $STMT -> fetchAll() : array();
foreach ($configs as $config) {
    $key_name = $config['key_name'];
    $value_int = $config['value_int'];
    $value_text = $config['value_text'];
    if ($value_int == '')
        define($key_name, $value_text);
    else
        define($key_name, $value_int);
    }

?>

.A {
    text-align: center;
    background-color: <?php echo COLOR_A ?>;
    }
.B {
    text-align: center;
    background-color:   <?php echo COLOR_B ?>;
    }
.C {
    text-align: center;
    background-color:   <?php echo COLOR_C ?>;
    }
.D {
    text-align: center;
    background-color:   <?php echo COLOR_D ?>;
    }
.X {
    text-align: center;
    background-color:   <?php echo COLOR_X ?>;
    }
td.A {
    text-align: center;
    font-weight: bold;
    background-color: <?php echo COLOR_A ?>;
    }
td.B {
    text-align: center;
    font-weight: bold;
    background-color:   <?php echo COLOR_B ?>;
    }
td.C {
    text-align: center;
    font-weight: bold;
    background-color:   <?php echo COLOR_C ?>;
    }
td.D {
    text-align: center;
    font-weight: bold;
    background-color:   <?php echo COLOR_D ?>;
    }
td.X {
    text-align: center;
    font-weight: bold;
    background-color:   <?php echo COLOR_X ?>;
    }


