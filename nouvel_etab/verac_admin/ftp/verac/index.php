<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    INITIALISATION
****************************************************/
/*
Pour être sûr que l'appel des pages passe par ici
on définit la variable globale VERAC.
Les autres pages commencent par tester son exitence
et quittent si VERAC n'existe pas.
*/
if (! defined('VERAC'))
    define('VERAC', 'index');

// réglage de l'heure :
//date_default_timezone_set('UTC');
date_default_timezone_set('Europe/Paris');

/*
Inclusion des fichiers contenant les constantes et les fonctions utiles.
La pluspart de ces fichiers sont dans le sous-dossier libs.
*/
require_once('libs/constantes.php');
//Fichier d'informations sur l'hébergement (requis avant la gestion de la session) :
$fichier_constantes = CHEMIN_CONFIG.'constantes.php';
if (is_file($fichier_constantes)) {
    if ($_SERVER['SERVER_NAME'] == '127.0.0.1')
        if (! defined('CHEMIN_SECRET_RELATIF'))
            define('CHEMIN_SECRET_RELATIF', '../secret');
    require_once($fichier_constantes);
    }
else {
    $errorMessage = '';
    $errorMessage .= '<!DOCTYPE html>';
    $errorMessage .= '<html>';
    $errorMessage .= '<head>';
    $errorMessage .= '<meta charset="utf-8">';
    $errorMessage .= '</head>';
    $errorMessage .= '<body>';
    $errorMessage .= '<h2 align="center">Installation corrompue.</h2>';
    $errorMessage .= '<p align="center">'."Prévenez l'administrateur du site afin qu'il y remédie.</p>";
    $errorMessage .= '<br/><br/><br/>';
    $errorMessage .= '<h2 align="center">Corrupted installation.</h2>';
    $errorMessage .= '<p align="center">Notify the site administrator to remedy it.</p>';
    $errorMessage .= '</body>';
    $errorMessage .= '</html>';
    echo $errorMessage;
    exit();
    }

/*
Mise à jour de la session 
(fonction libs/fonctions_session.php.updateSession)
*/
require_once('libs/fonctions_session.php');
updateSession();

// traductions (dans le sous-dossier translations) :
$translationFile = 'translations/'.$_SESSION['LANG'].'.php';
if (is_file($translationFile))
    require_once($translationFile);
else
    require_once('translations/default.php');
$_SESSION['TRANSLATIONS'] = new TRANSLATIONS;

// autres fichiers :
require_once('libs/fonctions_divers.php');
require_once('libs/fonctions_optimisation.php');



/****************************************************
    RÉCUPÉRATION-VÉRIFICATION DE LA PAGE À AFFICHER
****************************************************/
/*
On regarde s'il y a une demande de PAGE (GET),
sinon on récupère celle de la SESSION.
*/
if ($_SESSION['PAGE'] != 'login')
    $PAGE = (isset($_GET['page'])) ? $_GET['page'] : $_SESSION['PAGE'];
else
    $PAGE = 'login';
if ($PAGE == '')
    $PAGE = 'login';

// mise à jour de la variable ACTION si demandé :
$ACTION = (isset($_POST['action'])) ? $_POST['action'] : '';
if ($_SESSION['ACTION'] != $ACTION)
    $_SESSION['ACTION'] = $ACTION;

// connexion automatique depuis VÉRAC :
if ($ACTION == 'verac') {
    $login = ($_POST['login']) ? $_POST['login'] : '';
    $password = ($_POST['password']) ? $_POST['password'] : '';
    require_once(VERAC_CONNEXION_OTHER);
    if (openSession($login, $password) === 0) {
        $_SESSION['PAGE'] = 'infos';
        $_SESSION['COOKIE_LOGIN'] = $login;
        $PAGE = 'infos';
        }
    }

/*
cas particuliers de désactivation du site par l'admin :
    * site désactivé pour tous
    * site désactivé pour les élèves
    * période actuelle interdite aux élèves + période actuelle=1
*/
if (! SITE_ENABLED)
    $PAGE = 'travaux';
if ($_SESSION['USER_MODE'] == 'eleve') {
    if (! SITE_ENABLED_ELEVES)
        $PAGE = 'travaux';
    elseif ((! SHOW_ACTUAL_PERIODE) and ($_SESSION['ACTUAL_PERIOD'] == 1))
        $PAGE = 'travaux';
    }

/*
Test d'existence du fichier de la page (sous-dossier pages).
Sinon, on affichera la page d'erreur et on conserve le nom
de la page demandée dans la variable PAGE_ERROR
(permet d'afficher son nom).
*/
$phpFile = 'pages/'.$PAGE.'.php';
if (! is_file($phpFile)) {
    $PAGE_ERROR = $PAGE;
    $PAGE = 'error';
    }

/*
Arrivé ici, PAGE est connue et on met à jour la variable de SESSION
(sauf cas spéciaux indiqués par ACTION).
Mais d'abord on teste s'il faut recalculer des bilans.
*/
if (VERSION_NAME != 'demo')
    if ($_SESSION['PAGE'] == 'assess')
        if ($_SESSION['SELECTED_VUE'][0] == VUE_ITEM)
            if (count($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves']) > 0) {
                $mustRecalc = False;
                if ($PAGE != 'assess')
                    $mustRecalc = True;
                elseif ($ACTION == 'navbar')
                    $mustRecalc = True;
                if ($mustRecalc) {
                    require_once(VERAC_CONNEXION_PROF);
                    $CONNEXIONS = array();
                    for ($period = 0; $period < $_SESSION['ACTUAL_PERIOD'] + 1; $period++)
                        $CONNEXIONS[$period] = new CONNEXION_PROF($_SESSION['USER_ID'], $period);
                    $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
                    require_once('libs/fonctions_calcul.php');
                    $CONNEXION -> reCalcBilans();
                    }
                }
$specialActions = array('navbar', 'request', 'reload');
if ($_SESSION['PAGE'] != $PAGE) {
    if (! in_array($ACTION, $specialActions))
        $_SESSION['PAGE'] = $PAGE;
    }

// Pour la PAGE login, on (re)démarre une nouvelle session :
if ($PAGE == 'login' || $PAGE == 'travaux') {
    session_destroy();
    createSession('LOGIN');
    $_SESSION['TRANSLATIONS'] = new TRANSLATIONS;
    if (in_array($ACTION, $specialActions))
        exit('INACTIVE_TIME');
    }



/****************************************************
    ON PEUT ENFIN AFFICHER LA PAGE
****************************************************/
if (in_array($ACTION, $specialActions)) {
    /*
    L'ACTION reload permet de ne réafficher qu'une partie de la PAGE
    en fonction des actions de l'utilisateur (technique AJAX).
    Pas de demande "echo" dans ce cas.
    On retourne juste ce qui a été demandé et la mise à jour
    sera gérée par JavaScript.
    En général ce sera le contenu de la div #container
    qui sera remplacée.
    L'ACTION request permet de faire une demande à la PAGE.
    */
    // les connexions aux bases :
    if ($PAGE != 'travaux') {
        switch($_SESSION['USER_MODE']):
            case 'prof':
                require_once(VERAC_CONNEXION_PROF);
                $CONNEXIONS = array();
                for ($period = 0; $period < $_SESSION['ACTUAL_PERIOD'] + 1; $period++)
                    $CONNEXIONS[$period] = new CONNEXION_PROF($_SESSION['USER_ID'], $period);
                break;
            case 'eleve':
                require_once(VERAC_CONNEXION_ELEVE);
                $CONNEXIONS = array();
                for ($period = 0; $period < $_SESSION['ACTUAL_PERIOD'] + 1; $period++)
                    $CONNEXIONS[$period] = new CONNEXION_ELEVE($period);
                break;
            default:
                require_once(VERAC_CONNEXION_OTHER);
                $CONNEXION = new CONNEXION_OTHER;
                break;
        endswitch;
        }
    // affichage de la PAGE :
    $phpFile = 'pages/'.$PAGE.'.php';
    include($phpFile);
    }
else {
    /*
    On affiche tout (header, connexions, navbar, PAGE, footer, JavaScript, etc).
    */
    // cookie de connexion (doit être fait avant le header) :
    /*
    if ($_SESSION['COOKIE_LOGIN'] != '') {
        setCookie('verac_login', $_SESSION['COOKIE_LOGIN'], time() + 7*24*3600);
        $_SESSION['COOKIE_LOGIN'] = '';
        }
    */
    // header (on est en UTF-8, fichier css, etc) :
    include('pages/header.php');
    // les connexions aux bases et la barre de navigation :
    if ($PAGE != 'travaux') {
        switch($_SESSION['USER_MODE']):
            case 'prof':
                require_once(VERAC_CONNEXION_PROF);
                $CONNEXIONS = array();
                for ($period = 0; $period < $_SESSION['ACTUAL_PERIOD'] + 1; $period++)
                    $CONNEXIONS[$period] = new CONNEXION_PROF($_SESSION['USER_ID'], $period);
                include('./pages/navbar.php');
                break;
            case 'eleve':
                require_once(VERAC_CONNEXION_ELEVE);
                $CONNEXIONS = array();
                for ($period = 0; $period < $_SESSION['ACTUAL_PERIOD'] + 1; $period++)
                    $CONNEXIONS[$period] = new CONNEXION_ELEVE($period);
                include('./pages/navbar.php');
                break;
            default:
                require_once(VERAC_CONNEXION_OTHER);
                $CONNEXION = new CONNEXION_OTHER;
                break;
        endswitch;
        }
    // affichage de la page dans le div container :
    echo "\n\n".spaces(1).'<!-- Page content'."\n";
    echo spaces(1).'================================================== -->'."\n";
    //echo spaces(1).'<div class="container" id="container">'."\n";
    echo spaces(1).'<div class="container-fluid" id="container">'."\n";
    $phpFile = 'pages/'.$PAGE.'.php';
    include($phpFile);
    if ($PAGE == 'infos')
        include('pages/footer.php');
    echo "\n".spaces(1).'</div><!--container-->'."\n";
    // fichiers JavaScript (dans le sous-dossier js) :
    echo "\n\n".spaces(1).'<!-- Bootstrap core JavaScript'."\n";
    echo spaces(1).'================================================== -->'."\n";
    echo spaces(1).'<!-- Placed at the end of the document so the pages load faster -->'."\n";
    echo spaces(1)."<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->"."\n";
    echo spaces(1).'<script src="js/jquery.min.js"></script>'."\n";
    echo spaces(1).'<!-- Include all compiled plugins (below) -->'."\n";
    echo spaces(1).'<script src="js/bootstrap.min.js"></script>'."\n";
    echo spaces(1).'<script src="js/popper.min.js"></script>'."\n";
    echo spaces(1).'<!-- Include individual files as needed -->'."\n";
    // pour camembert et radar :
    $chartPages = array(
        'student_council', 
        'student_balances', 
        'student_referential', 
        'student_confidential');
    if (in_array($PAGE, $chartPages))
        echo spaces(1).'<script src="js/Chart.js"></script>'."\n";
    $jsFile = 'js/navbar.js';
    if ($_SERVER['SERVER_NAME'] != '127.0.0.1')
            $jsFile = fileInCache($jsFile);
    echo spaces(1).'<script src="'.$jsFile.'"></script>'."\n";
    // pages utilisant Sortable.js :
    $sortablePages = array(
        'class_trombinoscope');
    if (in_array($PAGE, $sortablePages))
        echo spaces(1).'<script src="js/Sortable.min.js"></script>'."\n";
    // le fichier JavaScript lié à la page s'il existe :
    $jsFile = 'js/'.$PAGE.'.js';
    if (file_exists($jsFile)) {
        if ($_SERVER['SERVER_NAME'] != '127.0.0.1')
            $jsFile = fileInCache($jsFile);
        echo spaces(1).'<script src="'.$jsFile.'"></script>'."\n";
        }
    // pour le débogage (seulement en local) :
    /*
    if ($_SERVER['SERVER_NAME'] == '127.0.0.1') {
        echo "\n\n".spaces(1).'<div id="debug" class="d-print-none">'."\n";
        include('pages/debug.php');
        echo "\n".spaces(1).'</div><!--debug-->'."\n";
        if ($_SESSION['TEMP_ALERT'] != '') {
            echo spaces(1).'<script type="text/javascript">alert("'.$_SESSION['TEMP_ALERT'].'");</script>'."\n";
            $_SESSION['TEMP_ALERT'] = '';
            }
        }
    */
    }


?>

</body>
</html>
