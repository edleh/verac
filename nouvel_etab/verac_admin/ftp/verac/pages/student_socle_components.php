<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if (($_SESSION['USER_MODE'] == 'eleve') and (! SHOW_SOCLE_COMPONENTS))
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if ($_SESSION['USER_MODE'] == 'prof') {
    if ($_SESSION['SELECTED_CLASS'][0] == -1)
        $result = "\n".message('info', 'Select a class.', '');
    elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
        $result = "\n".message('info', 'Select a student.', '');
        }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function studentComponents(
        $CONNEXION, $student, $SOCLE_COMPONENTS) {
    /*
    retourne les résultats d'un élève pour une liste de compétences partagées
    */
    $result = array();
    foreach ($SOCLE_COMPONENTS as $code => $row)
        $result[$code] = '';
    $version_db = getVersionDB($CONNEXION -> DB_RESULTATS);
    if ($version_db < 9)
        return $result;
    else {
        // on crée le texte de la liste des compétences :
        $competencesText = array();
        foreach ($SOCLE_COMPONENTS as $code => $row)
            $competencesText[]= '"'.$code.'"';
        $competencesText = implode(',', $competencesText);
        // récupération des résultats :
        $SQL = 'SELECT * FROM lsu ';
        $SQL .= 'WHERE lsuWhat="socle" ';
        $SQL .= 'AND id_eleve IN ('.$student.') ';
        $SQL .= 'AND lsu1 IN ('.$competencesText.') ';
        $SQL .= 'ORDER BY id_eleve DESC';
        $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
        // on les remet en forme :
        foreach ($tempResult as $row)
            $result[$row['lsu1']] = $row['lsu2'];
        return $result;
        }
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
if ($_SESSION['USER_MODE'] == 'prof')
    $selectedStudent = $_SESSION['SELECTED_STUDENT'][0];
else
    $selectedStudent = $_SESSION['USER_ID'];



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = '';
$fileDate = getDateResultsStudent($CONNEXION, $selectedStudent);
$nom_eleve = getStudentName($CONNEXION, $selectedStudent, $mode='');
$pageTitle .= spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.$nom_eleve.' : '.tr('COMPONENTS OF THE SOCLE IN ').$fileDate.'</h2>'."\n";
$pageTitle .= spaces(5).'<p>'.tr('Note: this statement takes account of previous years.').'</p>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";
$printTitle = tr('Components of the socle');
if ($_SESSION['USER_MODE'] == 'eleve')
    $printTitle .= ' - '.$_SESSION['USER_CLASS'][1];
else
    $printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.getStudentName($CONNEXION, $selectedStudent);
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';



/****************************************************
    CALCUL DES COMPOSANTES DU SOCLE
****************************************************/
$SOCLE_COMPONENTS = getSocleComponents(
    $CONNEXION, $_SESSION['SELECTED_CLASS'][0]);
$studentComponents = studentComponents(
    $CONNEXION, $selectedStudent, $SOCLE_COMPONENTS);

$table = '';
$table .= spaces(5).'<table border="1" class="bilan">'."\n";
$table .= spaces(6).'<col width=85%>'."\n";
$table .= spaces(6).'<col width=15%>'."\n";
$table .= spaces(6).'<tbody>'."\n";
foreach ($SOCLE_COMPONENTS as $code => $row) {
    $title = tr('LSU_'.$code);
    $value = $studentComponents[$code];
    $table .= spaces(7).'<tr>'."\n";
    $table .= spaces(8).'<td class="competence_label">'.$title.'</td>'."\n";
    $table .= spaces(8).'<td class="'.$value.'">'.valeur2affichage($value).'</td>'."\n";
    $table .= spaces(7).'</tr>'."\n";
    }
$table .= spaces(6).'</tbody>'."\n";
$table .= spaces(5).'</table>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
// titre en cas d'impression :
$print = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$print .= spaces(2).'<div class="d-none d-print-block">'."\n";
$print .= spaces(3).'<p class="text-center">'.$printTitle.'</p>'."\n";
$print .= spaces(2).'</div>'."\n";
$result .= "\n".$print;
// titre :
$result .= "\n".$pageTitle;
// pour centrer l'affichage :
$result .= spaces(2).'<div class="container theme-showcase">'."\n";
$result .= spaces(3).'<div class="row">'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(4).'<div class="col-sm-10">'."\n";
$result .= $table;
$result .= spaces(4).'</div>'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(3).'</div>'."\n";
$result .= spaces(2).'</div>'."\n";

// on vérifie si les élèves ont accès aux résultats de la période en cours :
if ($_SESSION['USER_MODE'] == 'eleve')
    if ((! SHOW_ACTUAL_PERIODE) and ($_SESSION['SELECTED_PERIOD'][0] >= $_SESSION['ACTUAL_PERIOD']))
        $result = "\n".$pageTitle.prohibitedPeriodMessage();

if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
