<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



require_once('../libs/constantes.php');
require_once('../libs/fonctions_divers.php');
require_once('../libs/fonctions_session.php');
$fichier_constantes = CHEMIN_CONFIG.'constantes.php';
if (is_file($fichier_constantes))
    require_once($fichier_constantes);
else
    echo "Il y a eu un problème";

header('Content-type: text/html; charset=UTF-8');

createSession();

$result = '';
$pageContent = '';

if (! empty($_POST)) {
    require_once('../'.VERAC_CONNEXION_OTHER);
    $CONNEXION = new CONNEXION_OTHER;
    // username and password sent from form 
    $login = ($_POST['login']) ? $_POST['login'] : '';
    $password = ($_POST['password']) ? $_POST['password'] : '';
    $groupName = ($_POST['groupName']) ? $_POST['groupName'] : '';
    $students = ($_POST['students']) ? $_POST['students'] : '';
    $class = ($_POST['class']) ? $_POST['class'] : 1;
    $tr = ($_POST['tr']) ? $_POST['tr'] : '';

    $photosDir = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_PROTECTED.'photos'.DIRECTORY_SEPARATOR;
    $id_students = string2array($students, $separator='|');

    if ($login and $password) {
        $id_prof = $CONNEXION -> verifyTeacherRemoteAccess($login, $password);
        if ($id_prof > -1) {

            $tableBegin = spaces(3).'<table width=1000 cellpadding=4 cellspacing=0 border="0">'."\n";
            $tableBegin .= spaces(4).'<col width=5>'."\n";
            $tableBegin .= spaces(4).'<col width=165>'."\n";
            $tableBegin .= spaces(4).'<col width=165>'."\n";
            $tableBegin .= spaces(4).'<col width=165>'."\n";
            $tableBegin .= spaces(4).'<col width=165>'."\n";
            $tableBegin .= spaces(4).'<col width=165>'."\n";
            $tableBegin .= spaces(4).'<col width=165>'."\n";
            $tableBegin .= spaces(4).'<col width=5>'."\n";

            $pageContent .= '<!DOCTYPE html>'."\n";
            $pageContent .= '<html>'."\n";
            $pageContent .= spaces(1).'<head>'."\n";
            $pageContent .= spaces(2).'<meta charset="utf-8">'."\n";
            $pageContent .= spaces(2).'<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all">'."\n";
            $pageContent .= spaces(2).'<link href="../css/trombinoscope.css" rel="stylesheet" type="text/css" media="all">'."\n";
            $pageContent .= spaces(1).'</head>'."\n";
            $pageContent .= spaces(1).'<body>'."\n";
            $pageContent .= spaces(2).'<div class="container" id="container">'."\n";
            $pageContent .= spaces(3).'<div class="container theme-showcase">'."\n";
            $pageContent .= spaces(4).'<div class="page-header">'."\n";
            $pageContent .= spaces(5).'<br /><h2>'.$groupName.htmlSpaces(20).'<small class="text-muted">'.count($id_students).' '.$tr.'</small></h2>'."\n";
            $pageContent .= spaces(4).'</div>'."\n";
            $pageContent .= spaces(3).'</div>'."\n";
            $pageContent .= spaces(3).'<hr />'."\n";
            $pageContent .= $tableBegin;
            $pageContent .= spaces(4).'<tr>'."\n";
            $pageContent .= spaces(5).'<td></td>'."\n";

            $row = 0;
            $column = 0;
            $students = $CONNEXION -> listStudentsFromIds(
                array2string($id_students), 
                $mode='TROMBI');

            foreach ($id_students as $eleve_id) {
                $student = $students[$eleve_id];
                $fileName = $photosDir.$eleve_id.'.jpeg';
                if (file_exists($fileName)) {
                    $tempFileName = $id_prof.'-'.$eleve_id.'.jpeg';
                    if (! is_file(CHEMIN_VERAC.'tmp'.DIRECTORY_SEPARATOR.$tempFileName))
                        copy(
                            $fileName, 
                            CHEMIN_VERAC.'tmp'.DIRECTORY_SEPARATOR.$tempFileName);
                    $fileName = $tempFileName;
                    }
                else {
                    $fileName = 'unknown-user.png';
                    if (! is_file(CHEMIN_VERAC.'tmp'.DIRECTORY_SEPARATOR.$fileName))
                        copy(
                            CHEMIN_VERAC.'images'.DIRECTORY_SEPARATOR.$fileName, 
                            CHEMIN_VERAC.'tmp'.DIRECTORY_SEPARATOR.$fileName);
                    }
                //$result .= '|'.$fileName;

                if ($column > 5) {
                    $column = 0;
                    $row += 1;
                    $pageContent .= spaces(5).'<td></td>'."\n";
                    $pageContent .= spaces(4).'</tr>'."\n";
                    if ($row > 4) {
                        $row = 0;
                        $pageContent .= spaces(3).'</table>'."\n";
                        $pageContent .= spaces(4).'<div class="page-break"></div><p><br /><br /><br /></p>'."\n";
                        $pageContent .= $tableBegin;
                        }
                    $pageContent .= spaces(4).'<tr>'."\n";
                    $pageContent .= spaces(5).'<td></td>'."\n";
                    }

                $pageContent .= spaces(5).'<td style="border:1px solid #e5e5e5">'."\n";
                $pageContent .= spaces(6).'<div class="text-center">'."\n";
                $pageContent .= spaces(7).'<img src="'.$fileName.'">'."\n";
                if ($class < 0)
                    $pageContent .= spaces(7).'<p>'.$student[2].'<br/>'.$student[3].'</p>'."\n";
                else
                    $pageContent .= spaces(7).'<p>'.$student[2].'</p>'."\n";
                $pageContent .= spaces(6).'</div>'."\n";
                $pageContent .= spaces(5).'</td>'."\n";
                $column += 1;
                }
            if ($column > 0) {
                for ($i=0; $i<7-$column; $i++)
                    $pageContent .= spaces(5).'<td></td>'."\n";
                $pageContent .= spaces(4).'</tr>'."\n";
                }

            $pageContent .= spaces(3).'</table>'."\n";
            $pageContent .= spaces(2).'</div>'."\n";
            $pageContent .= spaces(1).'</body>'."\n";
            $pageContent .= '</html>'."\n";

            $outFile = fopen(CHEMIN_VERAC.'tmp'.DIRECTORY_SEPARATOR.$id_prof.'-temp.html', 'w');
            fwrite($outFile, $pageContent);
            fclose($outFile);
            }
        }
    }


echo $result;

?>
