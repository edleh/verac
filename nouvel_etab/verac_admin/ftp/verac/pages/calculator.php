<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
// PAGE INTERDITE :
$result = '';
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if (($_SESSION['USER_MODE'] == 'eleve') and (! SHOW_CALCULATRICE))
    $result = "\n".prohibitedMessage();
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    MISE EN PLACE DU SCRIPT JS DE CALCUL
****************************************************/
$valeursNotes = spaces(2).'<script type="text/javascript">'."\n";
$valeursNotes .= spaces(3).'// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later'."\n";
$valeursNotes .= spaces(3).'var valeursNotes = ['.CALCULATRICE_A.', '.CALCULATRICE_B;
$valeursNotes .= ', '.CALCULATRICE_C.', '.CALCULATRICE_D.'];'."\n";
$valeursNotes .= spaces(3).'// @license-end'."\n";
$valeursNotes .= spaces(2).'</script>'."\n";
echo "\n".$valeursNotes;

?>

        <!-- PAGE HEADER -->
        <div class="container theme-showcase">
            <div class="page-header">
                <h2><?php echo tr('Calculate equivalence note') ?></h2><hr />
            </div>
        <!--</div>
        <div class="container theme-showcase">-->
            <div class="row">
                <!-- LES EXPLICATIONS -->
                <div class="col-sm-6">
                    <div class="jumbotron">
                        <h5><?php echo tr('Indicate the number of each color, then click on the <strong>Calculate</strong> button.') ?></h5>
                        <h5 class="text-center"><small><a href="https://verac.tuxfamily.org/site/help-tech-calculs" target="_blank">
                            <?php echo tr('Description of the algorithm') ?>
                        </a></small></h5>
                    </div>
                </div>
                <!-- LES CHAMPS DE SAISIE -->
                <div class="col-sm-3">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="background-color:<?php echo COLOR_A ?>; width:40px;"><strong><?php echo EVAL_A ?></strong></span>
                        </div>
                        <input type="number" id="nbA" value="0">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="background-color:<?php echo COLOR_B ?>; width:40px;"><strong><?php echo EVAL_B ?></strong></span>
                        </div>
                        <input type="number" id="nbB" value="0">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="background-color:<?php echo COLOR_C ?>; width:40px;"><strong><?php echo EVAL_C ?></strong></span>
                        </div>
                        <input type="number" id="nbC" value="0">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="background-color:<?php echo COLOR_D ?>; width:40px;"><strong><?php echo EVAL_D ?></strong></span>
                        </div>
                        <input type="number" id="nbD" value="0">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" style="background-color:<?php echo COLOR_X ?>; width:40px;"><strong><?php echo EVAL_X ?></strong></span>
                        </div>
                        <input type="number" id="nbX" value="0">
                    </div>
                </div>
                <!-- LE RÉSULTAT -->
                <div class="col-sm-3">
                    <br/><br/>
                    <button class="btn btn-primary btn-block" type="button" id="calcul">
                        <?php echo tr('Calcul') ?> &raquo;
                    </button>
                    <br/>
                    <div class="alert alert-success text-center" id="result"></div>
                </div>
            </div>
        </div>
