<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if (($_SESSION['USER_MODE'] == 'eleve') and (! SHOW_APPRECIATION_GROUPE))
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if (($_SESSION['USER_MODE'] == 'prof') and ($_SESSION['SELECTED_CLASS'][0] == -1))
    $result = "\n".message('info', 'Select a class.', '');
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function getGroups($CONNEXION, $students, $teachersNames) {
    /*
    retourne la liste des groupes
    */
    $groups = array();
    // on crée le texte de la liste des élèves :
    $id_students = array2string($students, 0);
    // on récupère dans la base résultats (on aura les id_prof pour l'instant)
    $SQL = 'SELECT DISTINCT Matiere, id_prof, groupeName ';
    $SQL .= 'FROM eleve_groupe ';
    $SQL .= 'WHERE id_eleve IN ('.$id_students.') ';
    $SQL .= 'ORDER BY Matiere, id_prof, id_groupe';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($tempResult as $row) {
        $matiere = $row[0];
        $id_prof = $row[1];
        $groupeName = $row[2];
        // on récupère le nom du prof :
        $profName = $teachersNames[$id_prof][1];
        if (array_key_exists($matiere, $groups))
            $groups[$matiere][] = array($profName, $groupeName);
        else
            $groups[$matiere] = array(array($profName, $groupeName));
        }
    return $groups;
    }

function getAppreciations($CONNEXION, $students, $teachersNames) {
    /*
    retourne un tableau des appréciations sur les groupes
    */
    $appreciations = array();
    // on crée le texte de la liste des élèves :
    $id_students = array2string($students, 0);
    // on récupère dans la base résultats (on aura les id_prof pour l'instant)
    $SQL = 'SELECT DISTINCT eleve_groupe.Matiere, ';
    $SQL .= 'eleve_groupe.id_prof, eleve_groupe.groupeName, ';
    $SQL .= 'appreciations.appreciation ';
    $SQL .= 'FROM eleve_groupe ';
    $SQL .= 'JOIN appreciations ON ';
    $SQL .= '(appreciations.id_prof=eleve_groupe.id_prof ';
    $SQL .= 'AND appreciations.id_eleve=-eleve_groupe.id_groupe-1) ';
    $SQL .= 'WHERE eleve_groupe.id_eleve IN ('.$id_students.') ';
    $SQL .= 'ORDER BY eleve_groupe.Matiere, eleve_groupe.id_prof, eleve_groupe.id_groupe';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($tempResult as $row) {
        $matiere = $row[0];
        $id_prof = $row[1];
        $groupeName = $row[2];
        $appreciation = $row[3];
        // on récupère le nom du prof :
        $profName = $teachersNames[$id_prof][1];
        if (array_key_exists($matiere, $appreciations))
            $appreciations[$matiere][] = array($profName, $groupeName, $appreciation);
        else
            $appreciations[$matiere] = array(array($profName, $groupeName, $appreciation));
        }
    return $appreciations;
    }

function getRows($n, $subject, $appreciations, $groups) {
    /*
    retourne les lignes des appréciations du tableau
    */
    $result = '';
    if (array_key_exists($subject['MatiereCode'], $groups)) {
        foreach ($groups[$subject['MatiereCode']] as $row) {
            $subjectTitle = $subject['MatiereLabel'];
            $subjectTitle .= ' ('.$row[0].')';
            $subjectTitle .= ' ['.$row[1].']';
            $appreciation = '';
            if (array_key_exists($subject['MatiereCode'], $appreciations)) {
                foreach ($appreciations[$subject['MatiereCode']] as $row2) {
                    if (($row2[0] == $row[0]) and ($row2[1] == $row[1]))
                        $appreciation = nl2br($row2[2]);
                    }
                }
            $result .= spaces($n).'<tr>'."\n";
            $result .= spaces($n + 1).'<td class="subject">'.$subjectTitle.'</td>'."\n";
            $result .= spaces($n).'</tr>'."\n";
            $result .= spaces($n).'<tr>'."\n";
            $result .= spaces($n + 1).'<td class="appreciation">'.$appreciation.'</td>'."\n";
            $result .= spaces($n).'</tr>'."\n";
            }
        }
    return $result;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
if ($_SESSION['USER_MODE'] == 'prof')
    $students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);
else
    $students = array(array($_SESSION['USER_ID'], $_SESSION['USER_NAME']));
// la liste des matières :
$subjects = subjects($CONNEXION);
// liste des noms des profs :
$teachersNames = listTeachersNames($CONNEXION);
// on récupère les groupes et les appréciations :
$groups = getGroups($CONNEXION, $students, $teachersNames);
if ($_SESSION['USER_MODE'] == 'prof')
    $appreciations = getAppreciations($CONNEXION, $students, $teachersNames);
elseif ($_SESSION['USER_MODE'] == 'eleve') {
    if ($_SESSION['SELECTED_PERIOD'][0] != $_SESSION['ACTUAL_PERIOD'])
        $appreciations = getAppreciations($CONNEXION, $students, $teachersNames);
    elseif (SHOW_APPRECIATION)
        $appreciations = getAppreciations($CONNEXION, $students, $teachersNames);
    }
else
    $appreciations = array();



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
if ($_SESSION['USER_MODE'] == 'prof') {
    $pageTitle .= spaces(5).'<h2>'.tr('Class Opinions: ').$_SESSION['SELECTED_CLASS'][1].'</h2>'."\n";
    $pageTitle .= spaces(5).'<p align="center">'.tr('At least one student in the class belongs to one of the listed groups.').'</p><hr />'."\n";
    }
else
    $pageTitle .= spaces(5).'<h2>'.tr('Class Opinions: ').$_SESSION['SELECTED_CLASS'][1].'</h2><hr />'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";

$printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
$printTitle .= spaces(3).'<p class="text-center">';
$printTitle .= tr('Opinions');
$printTitle .= ' - '.$_SESSION['SELECTED_PERIOD'][1];
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</p><hr />'."\n";
$printTitle .= spaces(2).'</div>'."\n";



/****************************************************
    MATIÈRES DU BULLETIN
****************************************************/
$bulletin = '';
$bulletin .= spaces(4).'<h3 align="center">'.tr('Sujects of Bulletin').'</h3>'."\n";
$bulletin .= spaces(4).'<table border="1" class="bilan">'."\n";
$bulletin .= spaces(5).'<tbody>'."\n";
foreach ($subjects['Bulletin'] as $subject)
    $bulletin .= getRows(6, $subject, $appreciations, $groups);
$bulletin .= spaces(5).'</tbody>'."\n";
$bulletin .= spaces(4).'</table>'."\n";



/****************************************************
    MATIÈRES SPÉCIALES
****************************************************/
$specials = '';
$specials .= spaces(4).'<h3 align="center">'.tr('Special subjects (Principal Teacher, School Life, ...)').'</h3>'."\n";
$specials .= spaces(4).'<table border="1" class="bilan">'."\n";
$specials .= spaces(5).'<tbody>'."\n";
foreach ($subjects['Speciales'] as $subject)
    $specials .= getRows(6, $subject, $appreciations, $groups);
$specials .= spaces(5).'</tbody>'."\n";
$specials .= spaces(4).'</table>'."\n";



/****************************************************
    AUTRES MATIÈRES
****************************************************/
$other = '';
$other .= spaces(4).'<h3 align="center">'.tr('Other subjects (Clubs, ...)').'</h3>'."\n";
$other .= spaces(4).'<table border="1" class="bilan">'."\n";
$other .= spaces(5).'<tbody>'."\n";
foreach ($subjects['Autre'] as $subject)
    $other .= getRows(6, $subject, $appreciations, $groups);
$other .= spaces(5).'</tbody>'."\n";
$other .= spaces(4).'</table>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle.$printTitle;
// pour centrer l'affichage :
$result .= spaces(2).'<div class="container theme-showcase">'."\n";
$result .= $bulletin.$specials.$other;
$result .= spaces(2).'</div><br />'."\n";

if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
