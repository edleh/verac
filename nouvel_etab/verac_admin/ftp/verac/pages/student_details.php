<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if ($_SESSION['USER_MODE'] == 'eleve') {
    if (! SHOW_DETAILS)
    $result = "\n".prohibitedMessage();
    }
// VÉRIFICATION DE LA SÉLECTION :
if ($_SESSION['USER_MODE'] == 'prof') {
    if ($_SESSION['SELECTED_CLASS'][0] == -1)
        $result = "\n".message('info', 'Select a class.', '');
    elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
        $result = "\n".message('info', 'Select a student.', '');
        }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    if ($what == 'changeDirectory') {
        $value = isset($_POST['value']) ? $_POST['value'] : '';
        $_SESSION['DETAILS_DIRECTORY'] = $value;
        }
    elseif ($what == 'changeSubject') {
        $value = isset($_POST['value']) ? $_POST['value'] : '';
        $_SESSION['DETAILS_SUBJECT'] = $value;
        }
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function listValidSubjects($CONNEXION, $id_eleve) {
    /*
    cherche les matières enseignées dans une classe
    */
    $EXCLUDE = array(
        'EPI_SAN', 'EPI_ART', 'EPI_EDD', 'EPI_ICC', 'EPI_LGA', 'EPI_LGE', 'EPI_PRO', 'EPI_STS', 
        'AP_LSU', 'PAR_LSU');
    $RESULT = array();
    // on vérifie les id_profs indiqués pour la matière et les élèves
    $SQL = 'SELECT * FROM eleve_prof ';
    $SQL .= 'WHERE id_eleve=:id_eleve';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> execute($OPT);
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($tempResult as $row)
        if (! in_array($row['Matiere'], $EXCLUDE)) {
            if (array_key_exists($row['Matiere'], $RESULT))
                $RESULT[$row['Matiere']][] = $row['id_prof'];
            else
                $RESULT[$row['Matiere']] = array($row['id_prof']);
            }
    return $RESULT;
    }

function getBilansBLT(
        $db_prof, $periode, $id_eleve, $matiere, $id_groupe) {
    $RESULT = array();
    // on cherche dans les tables profils et profil_who :
    $SQL = 'SELECT DISTINCT profils.*, profil_who.* ';
    $SQL .= 'FROM profils ';
    $SQL .= 'LEFT JOIN profil_who USING (id_profil) ';
    $SQL .= 'WHERE ';
    $SQL .= '   profils.Matiere="'.$matiere.'" ';
    $SQL .= 'AND ( ';
    $SQL .= '       (profils.profilType="STUDENTS" ';
    $SQL .= '       AND profil_who.id_who IN ('.$id_eleve.') ';
    $SQL .= '       AND profil_who.Periode IN (0, '.$periode.')) ';
    $SQL .= '   OR ';
    $SQL .= '       (profils.profilType="GROUPS" ';
    $SQL .= '       AND profil_who.id_who='.$id_groupe.' ';
    $SQL .= '       AND profil_who.Periode IN (0, '.$periode.')) ';
    $SQL .= '   OR ';
    $SQL .= '       (profils.id_profil<0) ';
    $SQL .= '   )';
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    $profils = array();
    $profils['DEFAULT'] = -999;
    foreach ($TEMP_RESULT as $profil) {
        $id_profil = $profil[0];
        $profilType = $profil[3];
        if ($id_profil < 0) // profil par défaut de la matière :
            $profils['DEFAULT'] = $id_profil;
        elseif ($profilType == 'STUDENTS') // profil attribué à l'élève :
            $profils['STUDENT'] = $id_profil;
        else // profil standard du groupe :
            $profils['GROUP'] = $id_profil;
        }
    if (array_key_exists('STUDENT', $profils))
        $id_profil = $profils['STUDENT'];
    elseif (array_key_exists('GROUP', $profils))
        $id_profil = $profils['GROUP'];
    else
        $id_profil = $profils['DEFAULT'];
    $SQL = 'SELECT profil_bilan_BLT.id_bilan, bilans.Name ';
    $SQL .= 'FROM profil_bilan_BLT ';
    $SQL .= 'JOIN bilans ON bilans.id_bilan=profil_bilan_BLT.id_bilan ';
    $SQL .= 'WHERE profil_bilan_BLT.id_profil IN ('.$id_profil.') ';
    $SQL .= 'ORDER BY profil_bilan_BLT.ordre';
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $bilan)
        $RESULT[$bilan['id_bilan']] = $bilan['Name'];
    return $RESULT;
    }

function listDetails(
        $db_prof, $periode, $id_eleve, $matiere, $sens, $confidentials=False) {
    /*
    cherche les détails des évaluations (items et bilans)
    */
    $RESULT = array();
    // on récupère le groupe de l'élève :
    $id_groupe = -1;
    $SQL = 'SELECT id_groupe ';
    $SQL .= 'FROM groupes ';
    $SQL .= 'JOIN groupe_eleve USING (id_groupe) ';
    $SQL .= 'WHERE groupes.Matiere=:matiere ';
    $SQL .= 'AND groupe_eleve.id_eleve=:id_eleve ';
    $SQL .= 'AND groupe_eleve.ordre>-1';
    $OPT = array(':matiere' => $matiere, ':id_eleve' => $id_eleve);
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> execute($OPT);
    $tempResult = ($STMT != '') ? $STMT -> fetch() : array('id_groupe' => -1);
    if (! is_array($tempResult))
        $tempResult = array('id_groupe' => -1);
    $id_groupe = $tempResult['id_groupe'];
    if ($id_groupe < 0)
        return $RESULT;
    // on récupère la liste des tableaux (publics uniquement) :
    $tableaux = array();
    $SQL = 'SELECT id_tableau FROM tableaux ';
    $SQL .= 'WHERE id_groupe=:id_groupe ';
    $SQL .= 'AND (Periode=:periode OR Periode=0) ';
    $SQL .= 'AND Public=1';
    $OPT = array(':periode' => $periode, ':id_groupe' => $id_groupe);
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> execute($OPT);
    while ($ROW = $STMT -> fetch())
        $tableaux[] = $ROW['id_tableau'];
    // on calcule le faux id_tableau (id_selection) pour lire les données des bilans :
    $id_selection = (10 + $periode) * 1000 + $id_groupe;
    $id_selectionAnnee = 10000 + $id_groupe;
    // on récupère les comptages liés à des items des élèves du groupe :
    $counts = countsFromGroup($db_prof, $id_groupe, $id_eleve, $periode);
    $count_items = array2string($counts['ITEMS']);
    // on récupère les items évalués :
    $items = array();
    $tempItems = array();
    $tableaux = array2string($tableaux);
    $SQL = 'SELECT evaluations.value, items.id_item, items.Name, ';
    $SQL .= 'items.Label, comments.comment ';
    $SQL .= 'FROM evaluations ';
    $SQL .= 'JOIN items USING (id_item) ';
    $SQL .= 'LEFT JOIN comments ON (comments.isItem=1 AND comments.id=items.id_item) ';
    $SQL .= 'WHERE evaluations.id_tableau IN ('.$tableaux.') ';
    $SQL .= 'AND evaluations.id_eleve=:id_eleve ';
    $SQL .= 'ORDER BY items.Name';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $tempItems = ($STMT != '') ? $STMT -> fetchAll(): array();
    // on rassemble les items évalués dans plusieurs tableaux :
    $last = -1;
    $actualItem = '';
    foreach ($tempItems as $item_ROW) {
        if ($item_ROW['id_item'] == $last) {
            $actualItem['value'] .= $item_ROW['value'];
            }
        else {
            if ($last > -1)
                $items[] = $actualItem;
            $actualItem = $item_ROW;
            $last = $item_ROW['id_item'];
            }
        }
    if ($last > -1)
        $items[] = $actualItem;

    //on ajoute les items évalués par comptage
    // (la liste countItemsAdded sert à ne récupérer la valeur qu'une fois) :
    $countItemsAdded = array();
    $SQL = 'SELECT DISTINCT items.*, bilans.*, comments.comment ';
    $SQL .= 'FROM items ';
    $SQL .= 'JOIN item_bilan USING (id_item) ';
    $SQL .= 'JOIN bilans USING (id_bilan) ';
    $SQL .= 'LEFT JOIN comments ON (comments.isItem=1 AND comments.id=items.id_item) ';
    $SQL .= 'WHERE items.id_item IN ('.$count_items.') ';
    $SQL .= 'AND bilans.id_competence>-2';
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $r) {
        $id_item = $r[0];
        $itemName = $r[1];
        $itemLabel = $r[2];
        $itemValue = '';
        if (array_key_exists($id_item, $counts['VALUES']))
            $itemValue = $counts['VALUES'][$id_item];
        $id_bilan = $r[3];
        $bilanName = $r[4];
        $itemComment = $r[7];
        if (! in_array($id_item, $countItemsAdded)) {
            $added = False;
            foreach ($items as $i => $actualItem)
                if ($actualItem['id_item'] == $id_item) {
                    $actualItem['value'] .= $itemValue;
                    $items[$i] = $actualItem;
                    $added = True;
                    }
            if ($added == False) {
                $items[] = array(
                    'value' => $itemValue,
                    'id_item' => $id_item,
                    'Name' => $itemName,
                    'Label' => $itemLabel,
                    'comment' => $itemComment);
                }
            $countItemsAdded[] = $id_item;
            }
        }

    // on récupère les bilans évalués :
    $bilans = array();
    $SQL = 'SELECT evaluations.value, bilans.id_bilan, bilans.Name, ';
    $SQL .= 'bilans.Label, bilans.id_competence, comments.comment ';
    $SQL .= 'FROM evaluations ';
    $SQL .= 'JOIN bilans USING (id_bilan) ';
    $SQL .= 'LEFT JOIN comments ON (comments.isItem=0 AND comments.id=bilans.id_bilan) ';
    $SQL .= 'WHERE evaluations.id_tableau IN (:id_selection, :id_selectionAnnee) ';
    $SQL .= 'AND evaluations.id_eleve=:id_eleve ';
    $SQL .= ($confidentials) ? '': 'AND bilans.id_bilan<'.DECALAGE_CFD.' ';
    $SQL .= 'AND bilans.id_competence>-2 ';
    $SQL .= 'ORDER BY bilans.id_competence DESC, bilans.Name ';
    $OPT = array(
        ':id_eleve' => $id_eleve, 
        ':id_selection' => $id_selection, 
        ':id_selectionAnnee' => $id_selectionAnnee);
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $bilans = ($STMT != '') ? $STMT -> fetchAll(): array();
    // on ajoute les bilans non-calculés (id_competence=-2) :
    $bilansNoCalc = array();
    $liste = array2string($items, 'id_item');
    $SQL = 'SELECT bilans.id_bilan, bilans.Name, bilans.Label, ';
    $SQL .= 'item_bilan.id_item ';
    $SQL .= 'FROM bilans ';
    $SQL .= 'JOIN item_bilan USING (id_bilan) ';
    $SQL .= 'JOIN items USING (id_item) ';
    $SQL .= 'WHERE bilans.id_competence=-2 ';
    $SQL .= 'AND item_bilan.id_item IN ('.$liste.') ';
    $SQL .= 'ORDER BY item_bilan.id_item, bilans.Name';
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $bilansNoCalc = ($STMT != '') ? $STMT -> fetchAll(): array();
    foreach ($bilansNoCalc as $ROW) {
        $bilans[] = array(
            'value' => '',
            'id_bilan' => $ROW['id_bilan'],
            'Name' => $ROW['Name'],
            'Label' => $ROW['Label'],
            'id_competence' => -2,
            'comment' => '');
        }

    if ($sens == 0) {
        $tab_temp = array();
        // préparation de la récupération des correspondances item/bilan
        $SQL = 'SELECT * FROM item_bilan ';
        $SQL .= 'WHERE id_item=:id_item';
        $OPT = array();
        $STMT = $db_prof -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        // on met en forme les données : on débute par les items (on garde le nom pour le tri)
        // on associe les items aux différents bilans auxquels ils sont reliés
        // on récupère aussi le coefficient du lien
        foreach ($items as $item_ROW) {
            $OPT['id_item'] = $item_ROW['id_item'];
            $STMT -> execute($OPT);
            if ($STMT != '') {
                // on récupère les liens item/bilans
                $liens = $STMT -> fetchAll();
                if ($liens) {
                    // l'item est relié
                    foreach ($liens as $lien) {
                        $id_bilan = $lien['id_bilan'];
                        $item_ROW['coeff'] = $lien['coeff'];
                        if (! isset($tab_temp[$id_bilan]))
                            $tab_temp[$id_bilan] = array();
                        $tab_temp[$id_bilan][$item_ROW['Name']] = $item_ROW;
                        }
                    }
                // l'item n'est pas relié
                else {
                    $item_ROW['coeff'] = 1;
                    $tab_temp['sans_lien'][$item_ROW['Name']] = $item_ROW;
                    }
                }
            else {
                $item_ROW['coeff'] = 1;
                $tab_temp['sans_lien'][$item_ROW['Name']] = $item_ROW;
                }
            }

        // pour chaque bilan, on associe les évaluations des items
        foreach ($bilans as $ROW) {
            if ($ROW['id_competence'] == -2)
                $ROW['value'] = '';
            $Name = $ROW['Name'];
            $NameForSort = doAscii($Name);
            if (! isset($RESULT[$ROW['id_competence']]))
                $RESULT[$ROW['id_competence']] = array();
            $RESULT[$ROW['id_competence']][$NameForSort] = array(
                'id_bilan' => $ROW['id_bilan'], 
                'bilan_name' => $Name, 
                'bilan_label' => $ROW['Label'], 
                'bilan_value' => $ROW['value'], 
                'bilan_comment' => $ROW['comment']);
            // on trie le détail par nom
            if ($tab_temp[$ROW['id_bilan']]) {
                uksort($tab_temp[$ROW['id_bilan']], 'strcasecmp');
                //ksort($tab_temp[$ROW['id_bilan']]);
                $RESULT[$ROW['id_competence']][$NameForSort]['detail'] = $tab_temp[$ROW['id_bilan']];
                }
            else
                $RESULT[$ROW['id_competence']][$NameForSort]['detail'] = array(array('Label' => '','value' => '','comment' => ''));
            }
        // on s'occupe des items sans liens avec des bilans
        if (isset($tab_temp['sans_lien'])) {
            if (! array_key_exists(-1, $RESULT))
                $RESULT[-1] = array();
            $RESULT[-1]['zzz'] = array(
            'id_bilan' => -1, 
            'bilan_name' => '', 
            'bilan_label' => tr('Unrelated to balance items'), 
            'bilan_value' => '', 
            'bilan_comment' => '', 
            'detail' => $tab_temp['sans_lien']);
            }
        // on trie les données
        foreach ($RESULT as $id_competence => $value)
            uksort($RESULT[$id_competence], 'strcasecmp');
        // on sépare les bilans persos du bulletin :
        if (! array_key_exists(-1, $RESULT))
            $RESULT[-1] = array();
        $RESULT[-3] = array();
        $bilansBLT = getBilansBLT($db_prof, $periode, $id_eleve, $matiere, $id_groupe);
        //debugToConsole($bilansBLT);
        foreach ($bilansBLT as $id_bilan => $name)
            foreach ($RESULT[-1] as $name2 => $value)
                if ($value['id_bilan'] == $id_bilan)
                    $RESULT[-3][$name] = $value;
        //debugToConsole($RESULT[-3]);
        ksort($RESULT);
        // on ajoute les bilans persos non évalués (id_competence=-2) à la fin des autres :
        if (array_key_exists(-2, $RESULT)) {
            if (! array_key_exists(-1, $RESULT))
                $RESULT[-1] = array();
            foreach ($RESULT[-2] as $name => $value)
                $RESULT[-1][$name] = $value;
            }
        }
    else {
        // récupération des bilans :
        $tab_temp = array();
        foreach ($bilans as $ROW) {
            if ($ROW['id_competence'] == -2) {
                $ROW['id_competence'] = -1;
                $ROW['value'] = '';
                }
            $tab_temp[$ROW['id_bilan']] = $ROW;
            }
        // préparation de la récupération des correspondances item/bilan
        $SQL = 'SELECT * FROM item_bilan ';
        $SQL .= 'WHERE id_item=:id_item ';
        $SQL .= 'ORDER BY id_bilan ASC';
        $OPT = array();
        $STMT = $db_prof -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        // on met en forme les données : on débute par les items (on garde le nom pour le tri)
        // on associe les items aux différents bilans auxquels ils sont reliés
        foreach ($items as $item_ROW) {
            $id_item = $item_ROW['id_item'];
            $RESULT[$id_item] = array($item_ROW, array());
            $OPT['id_item'] = $id_item;
            $STMT -> execute($OPT);
            if ($STMT != '') {
                // on récupère les liens item/bilans
                $liens = $STMT -> fetchAll();
                if ($liens) {
                    // l'item est relié
                    foreach ($liens as $lien) {
                        $id_bilan = $lien['id_bilan'];
                        if (array_key_exists($id_bilan, $tab_temp))
                            $RESULT[$id_item][1][] = $tab_temp[$id_bilan];
                        }
                    }
                }
            }
        }
    return $RESULT;
    }

function countsFromGroup($db_prof, $id_groupe, $id_eleve, $periode) {
    /*
    récupère les comptages liés à des items des élèves du groupe
    */
    $RESULT = array('ITEMS' => array(), 'VALUES' => array());
    // on continue :
    $SQL = 'SELECT DISTINCT counts_values.Periode, counts_values.id_eleve, ';
    $SQL .= 'counts.id_item, counts_values.perso ';
    $SQL .= 'FROM counts_values ';
    $SQL .= 'JOIN counts USING (id_count, id_groupe, Periode) ';
    $SQL .= 'WHERE counts.id_item>-1 ';
    $SQL .= 'AND counts_values.perso!="" ';
    $SQL .= 'AND counts_values.id_groupe='.$id_groupe.' ';
    $SQL .= 'AND counts_values.id_eleve='.$id_eleve.' ';
    $SQL .= 'AND counts_values.Periode IN (0, '.$periode.') ';
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $r) {
        $count_eleve = $r[1];
        $count_item = $r[2];
        $count_value = $r[3];
        if (! in_array($count_item, $RESULT['ITEMS']))
            $RESULT['ITEMS'][] = $count_item;
        if (! array_key_exists($count_item, $RESULT['VALUES']))
            $RESULT['VALUES'][$count_item] = $count_value;
        else
            $RESULT['VALUES'][$count_item] .= $count_value;
        }
    return $RESULT;
    }

function getItemClass_Details($db_prof, $value) {
    /*
    retourne le style (class ; défini dans le fichier css)
    à utiliser pour l'affichage d'un item (interroge la table moyennesItems).
    */
    // s'il n'y a qu'une lettre, pas besoin d'aller plus loin :
    if (strlen($value) < 2)
        return $value;
    $moyenne = $value;
    // on interroge la table moyennesItems :
    $SQL = 'SELECT moyenne FROM moyennesItems ';
    $SQL .= 'WHERE value=:value';
    $OPT = array(':value' => $value);
    $STMT = $db_prof -> prepare($SQL);
    $STMT -> execute($OPT);
    if ($STMT != '') {
        $RESULT = $STMT -> fetch();
        $moyenne = $RESULT['moyenne'];
        }
    // si la moyenne n'est pas trouvée dans la table, on lance le calcul :
    if ($moyenne == '')
        $moyenne = calcItemMoyenne_Details($value);
    return $moyenne;
    }

function calcItemMoyenne_Details($valueItem) {
    /*
    calcule la valeur retenue pour l'item en fonction des paramètres
    (nombre d'absence autorisées, nombre d'évaluations retenues, ...).
    On utilise les paramètres par défaut pour ne pas trop prendre de temps de calcul.
    (tant pis pour les réglages prof)
    */
    $result = '';
    $levelX = 50;
    $values = array();
    foreach (getArrayLetters() as $LETTRE)
        $values[] = substr_count($valueItem, $LETTRE);
    if (array_sum($values) < 1)
        return $result;
    $values2 = '';
    foreach ($values as $n)
        $values2 .= '|'.$n;
    $array_lettres = getArrayLetters();
    $last = count($values) - 1;
    $valuesPC = valuesInPercentages_Details($values);
    if ($valuesPC[$last] >= $levelX)
        $result = $array_lettres[$last];
    elseif (testA_Details($valuesPC) == 1)
        $result = $array_lettres[0];
    elseif (testD_Details($valuesPC) == 1)
        $result = $array_lettres[3];
    elseif (testB_Details($valuesPC) == 1)
        $result = $array_lettres[1];
    else
        $result = $array_lettres[2];
    return $result;
    }

function valuesInPercentages_Details($values) {
    /*
    renvoie le pourcentage de chaque valeur par rapport au total.
    */
    $last = count($values) - 1;
    $result = array();
    foreach ($values as $index => $elem)
        $result[$index] = 0;
    $total = array_sum($values);
    if ($total < 1)
        return $result;
    // calcul du % de X :
    $result[$last] = $values[$last] * 100 / $total;
    // total des autres :
    $total = ($values[0] + $values[1] + $values[2] + $values[3]);
    if ($total < 1)
        return $result;
    // calcul des % sans tenir compte des X :
    for ($i=0; $i<$last; $i++)
        $result[$i] = $values[$i] * 100 / $total;
    // on recentre les % en fonction des X :
    if ($result[$last] > 0) {
        $V2J = $result[0] * $result[$last] / 100;
        $result[0] -= $V2J;
        $result[1] += $V2J;
        $R2O = $result[3] * $result[$last] / 100;
        $result[3] -= $R2O;
        }
    // pour retomber sur 100 % :
    $result[2] = 100 - ($result[0] + $result[1] + $result[3]);
    return $result;
    }

function testA_Details($valuesPC) {
    /*
    on utilise les paramètres par défaut.
    */
    $levelA = array(50, 90, 10, 0);
    $result = 0;
    if (($valuesPC[0] >= $levelA[0])
        and (($valuesPC[0] + $valuesPC[1]) >= $levelA[1])
        and ($valuesPC[2] <= $levelA[2])
        and ($valuesPC[3] <= $levelA[3]))
            $result = 1;
    return $result;
    }

function testD_Details($valuesPC) {
    /*
    on utilise les paramètres par défaut.
    */
    $levelA = array(50, 90, 10, 0);
    $result = 0;
    if (($valuesPC[3] >= $levelA[0])
        and (($valuesPC[2] + $valuesPC[3]) >= $levelA[1])
        and ($valuesPC[1] <= $levelA[2])
        and ($valuesPC[0] <= $levelA[3]))
            $result = 1;
    return $result;
    }

function testB_Details($valuesPC) {
    /*
    on utilise les paramètres par défaut.
    */
    $levelB = array(50, 50, 30);
    $result = 0;
    if ((($valuesPC[0] + $valuesPC[1]) >= $levelB[0])
        and ($valuesPC[2] <= $levelB[1])
        and ($valuesPC[3] <= $levelB[2]))
            $result = 1;
    return $result;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
if ($_SESSION['USER_MODE'] == 'prof')
    $selectedStudent = $_SESSION['SELECTED_STUDENT'][0];
else
    $selectedStudent = $_SESSION['USER_ID'];
// la liste des matières :
$sortedSubjects = subjects($CONNEXION);
// on récupère les matières où un prof est signalé :
$validSubjects = listValidSubjects($CONNEXION, $selectedStudent);
// liste des noms des profs :
$teachersNames = listTeachersNames($CONNEXION);
// liste des noms des matières :
$subjectsLabels = listSubjectsLabels($CONNEXION);
// on recrée la liste des matières à prendre en compte :
$subjects = array();
$first = True;
foreach (array('Bulletin', 'Speciales', 'Autre') as $where) {
    if ($first)
        $first = False;
    else
        $subjects[] = array('SEPARATOR', -1, 'SEPARATOR');
    foreach ($sortedSubjects[$where] as $subject) {
        $matiere = $subject['MatiereCode'];
        // les élèves ne voient pas la matière confidentielle :
        if (($_SESSION['USER_MODE'] == 'prof') or ($matiere != 'CFD')) {
            if (array_key_exists($matiere, $validSubjects))
                foreach ($validSubjects[$matiere] as $id_prof) {
                    $teacherName = $teachersNames[$id_prof][0];
                    $label = $subjectsLabels[$matiere][0].' ('.$teacherName.')';
                    $subjects[] = array($matiere, $id_prof, $label);
                    }
            }
        }
    }

if (! isset($btnWidth1))
    $btnWidth1 = '250px';
if (! isset($btnWidth2))
    $btnWidth2 = '400px';
if (! isset($thePage))
    $thePage = $_SESSION['PAGE'];



/****************************************************
    SÉLECTEURS (MATIÈRE + SENS) 
    + TITRE POUR IMPRESSION
****************************************************/
$printTitle = tr('Details');
$printTitle .= ' - '.$_SESSION['SELECTED_PERIOD'][1];
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.$_SESSION['SELECTED_STUDENT'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</br>';

$selects = spaces(3).'<nav class="navbar navbar-expand-lg navbar-'.$_SESSION['NAVBAR_STYLE'].'">'."\n";
$selects .= spaces(4).'<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar2" aria-expanded="false" aria-label="Toggle navigation">'."\n";
$selects .= spaces(5).'<span class="navbar-toggler-icon"></span>'."\n";
$selects .= spaces(4).'</button>'."\n";
$selects .= spaces(3).'<div class="collapse navbar-collapse justify-content-md-center" id="navbar2">'."\n";

$select = '';
$select .= spaces(4).'<div id="select-subject" class="btn-group btn-group-sm text-center">'."\n";
$what = '';
$who = -1;
$title = tr('Subject');
foreach ($subjects as $subject) {
    if ($subject[2] == $_SESSION['DETAILS_SUBJECT']) {
        $what = $subject[0];
        $who = $subject[1];
        $title = $subject[2];
        }
    }
$printTitle .= $title;
$select .= spaces(5).'<a id="subject-title" class="nav-link btn-sm btn-light dropdown-toggle" data-toggle="dropdown" href="#" style="width:'.$btnWidth2.';">'.$title.'</a>'."\n";
$select .= spaces(5).'<ul class="dropdown-menu">'."\n";
foreach ($subjects as $subject) {
    if ($subject[1] < 0)
        $select .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    else
        $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="'.$subject[2].'" reload=true href="#">'.$subject[2].'</a></li>'."\n";
    }
$select .= spaces(5).'</ul>'."\n";
$select .= spaces(4).'</div>'."\n";
$selects .=  $select;

$selects .= spaces(4).'<div>'.htmlSpaces(4).'</div>'."\n";

$select = '';
$select .= spaces(4).'<div id="select-directory" class="btn-group btn-group-sm text-center">'."\n";
$sensLabel = array(tr('Balances'), tr('Items'));
$title = $sensLabel[$_SESSION['DETAILS_DIRECTORY']];
$printTitle .= ' - '.$title;
$select .= spaces(5).'<a id="directory-title" class="nav-link btn-sm btn-light dropdown-toggle" data-toggle="dropdown" href="#" style="width:'.$btnWidth1.';">'.$title.'</a>'."\n";
$select .= spaces(5).'<ul class="dropdown-menu">'."\n";
$select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="0" reload=true href="#">'.$sensLabel[0].'</a></li>'."\n";
$select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="1" reload=true href="#">'.$sensLabel[1].'</a></li>'."\n";
$select .= spaces(5).'</ul>'."\n";
$select .= spaces(4).'</div>'."\n";
$selects .=  $select;

$selects .= spaces(3).'</div>'."\n";
$selects .= spaces(3).'</nav>'."\n";



/****************************************************
    TITRE DU DOCUMENT
****************************************************/
$pageTitle = spaces(3).'<div class="alert alert-info text-center">'."\n";
// le texte affiché dépend du sens sélectionné :
if ($_SESSION['DETAILS_DIRECTORY'] == 0) {
    $pageTitle .= spaces(4).'<p>'.tr('Each table row corresponds to a rated balance in the selected subject.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('The items included in the calculation are displayed by clicking on a balance.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('The values ​​of the balance and items are shown in the Value column.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('For balances, the color displayed in the Value column depends on the settings chosen by the teacher.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('Tips to grow can be displayed if there is a link in the last column.')."\n";
    $pageTitle .= spaces(4).'</p>'."\n";
    }
else {
    $pageTitle .= spaces(4).'<p>'.tr('Each table row corresponds to a rated item in the selected subject.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('The balances for the calculation involving an item are displayed by clicking on this item.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('The values ​​of the item and balances are shown in the Value column.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('For balances, the color displayed in the Value column depends on the settings chosen by the teacher.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('Tips to grow can be displayed if there is a link in the last column.')."\n";
    $pageTitle .= spaces(4).'</p>'."\n";
    }
$pageTitle .= spaces(3).'</div>'."\n";



/****************************************************
    CALCUL DES ÉVALUATIONS
****************************************************/
$db_prof = $CONNEXION -> connectTeacherDB($who);

$evals = '';
if (($who < 0) or (! $db_prof)) {
    $evals .= "\n".message('info', 'Select a subject.', '');
    $pageTitle = '';
    }
elseif ($_SESSION['DETAILS_DIRECTORY'] == 0) {
    // SENS BILANS → ITEMS
    if ($_SESSION['USER_MODE'] == 'prof')
        $tempDetails = listDetails(
            $db_prof, $_SESSION['SELECTED_PERIOD'][0], 
            $selectedStudent, $subjectsLabels[$what][0], 
            0, True);
    else
        $tempDetails = listDetails(
            $db_prof, $_SESSION['SELECTED_PERIOD'][0], 
            $selectedStudent, $subjectsLabels[$what][0], 
            0, False);
    // on répartit les bilans entre les 5 parties :
    $details = array(
        'subjectBLT' => array(),
        'referential' => array(),
        'BLT' => array(),
        'subjectOther' => array(),
        'confidential' => array());
    foreach ($tempDetails as $id_competence => $data) {
        if ($id_competence == -3)
            $details['subjectBLT'] = $data;
        elseif ($id_competence < 0) {
            $keys = array_keys($data);
            foreach ($keys as $key)
                if (! array_key_exists($data[$key]['bilan_name'], $tempDetails[-3]))
                    $details['subjectOther'][$key] = $data[$key];
            }
        else {
            $key = array_keys($data);
            $key = $key[0];
            if ($id_competence < DECALAGE_BLT)
                $details['referential'][$key] = $data[$key];
            elseif ($id_competence < DECALAGE_CFD)
                $details['BLT'][$key] = $data[$key];
            else
                $details['confidential'][$key] = $data[$key];
            }
        }
    //debugToConsole($details['subjectBLT']);
    $tableTitle = tr('Balances and associated items (click on a balance to view items)');
    $detailsMessage = tr('Click to show/hide details.');
    $comment_message = tr('Click to show tips.');
    $colspan = 3;
    $id_toggle = 0;
    // les 5 parties et leurs labels :
    $parts = array(
        'subjectBLT' => tr('Disciplinary Competences of Bulletin'), 
        'BLT' => tr('Shared Competences of Bulletin'), 
        'referential' => tr('Referential Competences'),
        'subjectOther' => tr('Other Disciplinary Competences'));
    if ($_SESSION['USER_MODE'] == 'prof')
        $parts['confidential'] = tr('Confidential Competences');
    foreach ($parts as $part => $partTitle) {
        // début du tableau :
        $table = '';
        $table .= spaces(5).'<h3>'.htmlspecialchars($partTitle).'</h3>'."\n";
        $table .= spaces(5).'<table border="1" class="bilan">'."\n";
        $table .= spaces(6).'<thead class="bilan">'."\n";
        $table .= spaces(7).'<tr>'."\n";
        $table .= spaces(8).'<th class="title">'.$tableTitle.'</th>'."\n";
        $table .= spaces(8).'<th class="student">'.tr('Value').'</th>'."\n";
        $table .= spaces(8).'<th class="group">'.tr('Tips').'</th>'."\n";
        $table .= spaces(7).'</tr>'."\n";
        $table .= spaces(6).'</thead>'."\n";
        $table .= spaces(6).'<tbody>'."\n";
        // remplissage du tableau :
        foreach ($details[$part] as $key => $row) {
            $id_toggle += 1;
            $name = $row['bilan_name'];//$key;
            $label = $row['bilan_label'];
            if ($name == 'zzz')
                $name = '';
            $title = '['.$name.'] '.$label;
            $value = $row['bilan_value'];
            if ($value == '')
                $title .= htmlSpaces(10).'<em>('.tr('uncalculated balance').')</em>';
            $comment = $row['bilan_comment'];
            $table .= spaces(7).'<tr id="'.$id_toggle.'">'."\n";
            $table .= spaces(8).'<td class="detail_parent competence_label" title="'.$detailsMessage.'">'.$title.'</td>'."\n";
            $table .= spaces(8).'<td class="detail_parent '.$value.'" title="'.$detailsMessage.'">'.valeur2affichage($value).'</td>'."\n";
            if ($comment == '')
                $table .= spaces(8).'<td></td>'."\n";
            else {
                $table .= spaces(8).'<td><a class="comment-show" who="'.$name.'" what="'.$title.'" title="'.$comment_message.'">'.tr('show').'</a></td>'."\n";
                $table .= spaces(8).'<label class="sr-only" id="'.$name.'" >'.$comment.'</label>'."\n";
                }
            $table .= spaces(7).'</tr>'."\n";
            // les lignes des items liés (affichables par clic) :
            foreach ($row['detail'] as $detailKey => $detailRow) {
                $detailName = $detailKey;
                $detailLabel = $detailRow['Label'];
                $detailTitle = '['.$detailName.'] '.$detailLabel;
                $detailValue = $detailRow['value'];
                $detailComment = $detailRow['comment'];
                $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                $table .= spaces(8).'<td class="detail_label">'.$detailTitle.'</td>'."\n";
                $class = getItemClass_Details($db_prof, $detailValue);
                $table .= spaces(8).'<td class="'.$class.'">'.valeur2affichage($detailValue).'</td>'."\n";
                if ($detailComment == '')
                    $table .= spaces(8).'<td></td>'."\n";
                else {
                    $table .= spaces(8).'<td><a class="comment-show" who="'.$detailName.'" what="'.$detailTitle.'" title="'.$comment_message.'">'.tr('show').'</a></td>'."\n";
                    $table .= spaces(8).'<label class="sr-only" id="'.$detailName.'" >'.$detailComment.'</label>'."\n";
                    }
                $table .= spaces(7).'</tr>'."\n";
                }
            $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
            $table .= spaces(8).'<td class="interligne" colspan="'.$colspan.'"></td>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            }
        // fin du tableau :
        $table .= spaces(6).'</tbody>'."\n";
        $table .= spaces(5).'</table>'."\n";
        $evals .= $table;
        }
    }
else {
    // SENS ITEMS → BILANS
    if ($_SESSION['USER_MODE'] == 'prof')
        $tempDetails = listDetails(
            $db_prof, $_SESSION['SELECTED_PERIOD'][0], 
            $selectedStudent, $subjectsLabels[$what][0], 
            1, True);
    else
        $tempDetails = listDetails(
            $db_prof, $_SESSION['SELECTED_PERIOD'][0], 
            $selectedStudent, $subjectsLabels[$what][0], 
            1, False);
    $tableTitle = tr('Items and associated balances (click on item to view balances)');
    $detailsMessage = tr('Click to show/hide details.');
    $comment_message = tr('Click to show tips.');
    $colspan = 3;
    $id_toggle = 0;
    // début du tableau :
    $table = '';
    $table .= spaces(5).'<table border="1" class="bilan">'."\n";
    $table .= spaces(6).'<thead class="bilan">'."\n";
    $table .= spaces(7).'<tr>'."\n";
    $table .= spaces(8).'<th class="title">'.$tableTitle.'</th>'."\n";
    $table .= spaces(8).'<th class="student">'.tr('Value').'</th>'."\n";
    $table .= spaces(8).'<th class="group">'.tr('Tips').'</th>'."\n";
    $table .= spaces(7).'</tr>'."\n";
    $table .= spaces(6).'</thead>'."\n";
    $table .= spaces(6).'<tbody>'."\n";
    // remplissage du tableau :
    foreach ($tempDetails as $id_item => $data) {
        $id_toggle += 1;
        $name = $data[0]['Name'];
        $label = $data[0]['Label'];
        $title = '['.$name.'] '.$label;
        $value = $data[0]['value'];
        $comment = $data[0]['comment'];
        $table .= spaces(7).'<tr id="'.$id_toggle.'">'."\n";
        $table .= spaces(8).'<td class="detail_parent competence_label" title="'.$detailsMessage.'">'.$title.'</td>'."\n";
        $class = getItemClass_Details($db_prof, $value);
        $table .= spaces(8).'<td class="detail_parent '.$class.'" title="'.$detailsMessage.'">'.valeur2affichage($value).'</td>'."\n";
        if ($comment == '')
            $table .= spaces(8).'<td></td>'."\n";
        else {
            $table .= spaces(8).'<td><a class="comment-show" who="'.$name.'" what="'.$title.'" title="'.$comment_message.'">'.tr('show').'</a></td>'."\n";
            $table .= spaces(8).'<label class="sr-only" id="'.$name.'" >'.$comment.'</label>'."\n";
            }
        $table .= spaces(7).'</tr>'."\n";
        // les lignes des bilans liés (affichables par clic) :
        foreach ($data[1] as $detailKey => $detailRow) {
            $detailName = $detailRow['Name'];
            $detailLabel = $detailRow['Label'];
            $detailTitle = '['.$detailName.'] '.$detailLabel;
            $detailValue = $detailRow['value'];
            if ($detailValue == '')
                $detailTitle .= htmlSpaces(10).'('.tr('uncalculated balance').')';
            $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
            $table .= spaces(8).'<td class="detail_label">'.$detailTitle.'</td>'."\n";
            $table .= spaces(8).'<td class="'.$detailValue.'">'.valeur2affichage($detailValue).'</td>'."\n";
            $table .= spaces(8).'<td></td>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            }
        $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
        $table .= spaces(8).'<td class="interligne" colspan="'.$colspan.'"></td>'."\n";
        $table .= spaces(7).'</tr>'."\n";
        }
    // fin du tableau :
    $table .= spaces(6).'</tbody>'."\n";
    $table .= spaces(5).'</table>'."\n";
    $evals .= $table;
    }
// on n'a plus besoin de la base prof :
unset($db_prof);



/****************************************************
    FENÊTRE MODALE POUR AFFICHER LES CONSEILS
****************************************************/
$modal = "\n";
$modal .= spaces(2).'<!-- Modal -->'."\n";
$modal .= spaces(2).'<div class="my-modal" id="myModal">'."\n";
$modal .= spaces(3).'<div class="modal-dialog">'."\n";
$modal .= spaces(4).'<div class="modal-content">'."\n";
$modal .= spaces(5).'<div class="modal-header">'."\n";
$modal .= spaces(6).'<h4 class="modal-title">'.tr('Tips to grow').'</h4>'."\n";
$modal .= spaces(6).'<button type="button" id="myModalClose" class="close">&times;</button>'."\n";
$modal .= spaces(5).'</div>'."\n";
$modal .= spaces(5).'<div class="modal-body">'."\n";
$modal .= spaces(6).'<p id="myModalWhat"></p>'."\n";
$modal .= spaces(6).'<div id="myModalContent"></div>'."\n";
$modal .= spaces(5).'</div>'."\n";
$modal .= spaces(5).'<div class="modal-footer">'."\n";
$modal .= spaces(5).'</div>'."\n";
$modal .= spaces(4).'</div><!-- /.modal-content -->'."\n";
$modal .= spaces(3).'</div><!-- /.modal-dialog -->'."\n";
$modal .= spaces(2).'</div><!-- /.modal -->'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
// titre en cas d'impression :
$print = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$print .= spaces(2).'<div class="d-none d-print-block">'."\n";
$print .= spaces(3).'<p class="text-center">'.$printTitle.'</p>'."\n";
$print .= spaces(2).'</div>'."\n";
$result .= "\n".$print;
// sélecteurs :
$result .= spaces(2).'<div id="selects_top" class="d-print-none">'."\n";
$result .= $selects;
$result .= spaces(2).'</div>'."\n";
$result .= spaces(2).'<br /><br />'."\n";
// pour centrer l'affichage :
$result .= spaces(2).'<div class="container theme-showcase">'."\n";
$result .= $pageTitle;
$result .= spaces(3).'<div class="row">'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(4).'<div class="col-sm-10">'."\n";
$result .= $evals;
$result .= spaces(4).'</div>'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(3).'</div>'."\n";
$result .= spaces(2).'</div>'."\n";
// fenêtre modale des conseils :
$result .= $modal;


if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
