<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    PAS D'APPEL DIRECT DE CETTE PAGE
****************************************************/
if (! defined('VERAC'))
    exit;



/****************************************************
    LISTE DES PAGES ET DE LEURS SÉLECTEURS
****************************************************/
$selects = array();
if ($_SESSION['USER_MODE'] == 'prof') {
    $selects['class_synthese'] = array(tr('Synthesis'), 'period', 'class');
    $selects['class_opinions'] = array(tr('Opinions'), 'period', 'class');
    $selects['class_shared'] = array(tr('Shared competences'), 'period', 'class');
    $selects['class_socle_components'] = array(tr('Components of the socle'), 'class');
    $selects['class_follows'] = array(tr('Follows'), 'class');
    $selects['class_list'] = array(tr('List'), 'class');
    $selects['class_trombinoscope'] = array(tr('Trombinoscope'), 'class');
    $selects['class_statistics'] = array(tr('Statistics'), 'class');

    $selects['student_council'] = array(tr('Council'), 'period', 'class', 'student');
    $selects['student_balances'] = array(tr('Balances'), 'period', 'class', 'student');
    $selects['student_referential'] = 
        array(tr('Referential'), 'period', 'class', 'student');
    $selects['student_socle_components'] = 
        array(tr('Components of the socle'), 'period', 'class', 'student');
    $selects['student_confidential'] = 
        array(tr('Confidential'), 'period', 'class', 'student');
    $selects['student_details'] = 
        array(tr('Details'), 'period', 'class', 'student');
    $selects['student_follows'] = array(tr('Follows'), 'class', 'student');
    $selects['student_password'] = array(tr('Password'), 'class', 'student');
    $selects['student_papers'] = array(tr('Papers'), 'class', 'student');

    $selects['student_validations'] = 
        array(tr('Validations'), 'class', 'student');
    $selects['class_validations'] = array(tr('Validations'), 'class');
    $selects['student_validations_socle_components'] = 
        array(tr('Validations'), 'class', 'student');

    if ($_SESSION['CAN_EVALUATE'])
        $selects['assess'] = array(tr('Assess'), 'period', 'group', 'tableau', 'vue');

    $selects['infos'] = array(tr('News'));
    $selects['password'] = array(tr('Password'));
    $selects['papers'] = array(tr('Papers'));
    $selects['calculator'] = array(tr('Calculator'));
    }
else {
    $selects['student_balances'] = array(tr('Balances'), 'period');
    $selects['student_referential'] = array(tr('Referential'), 'period');
    $selects['student_socle_components'] = array(tr('Components of the socle'), 'period');
    $selects['student_details'] = array(tr('Details'), 'period');
    $selects['student_follows'] = array(tr('Follows'));

    $selects['class_opinions'] = array(tr('Opinions'), 'period');

    $selects['infos'] = array(tr('News'));
    $selects['password'] = array(tr('Password'));
    $selects['papers'] = array(tr('Papers'));
    $selects['calculator'] = array(tr('Calculator'));
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $_SESSION['WINDOW_WIDTH'] = isset($_POST['w']) ? $_POST['w'] : $_SESSION['WINDOW_WIDTH'];
    $_SESSION['WINDOW_HEIGHT'] = isset($_POST['h']) ? $_POST['h'] : $_SESSION['WINDOW_HEIGHT'];
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    if ($what == 'style') {
        $result = 'light';
        if ($_SESSION['NAVBAR_STYLE'] == 'light')
            $result = 'dark';
        else
            $result = 'light';
        $_SESSION['NAVBAR_STYLE'] = $result;
        setCookie('verac_navbar_style', $_SESSION['NAVBAR_STYLE'], time() + 7*24*3600, '', '', 1);
        }
    elseif ($what == 'lang') {
        $result = isset($_POST['lang']) ? $_POST['lang'] : '';
        $_SESSION['LANG'] = $result;
        setCookie('verac_lang', $result, time() + 7*24*3600, '', '', 1);
        }
    else {
        if ($_SESSION['PAGE'] == 'assess') {
            $period = isset($_POST['period']) ? $_POST['period'] : $_SESSION['ASSESS_PERIOD'][0];
            if ($period != $_SESSION['ASSESS_PERIOD'][0])
                $_SESSION['ASSESS_PERIOD'] = array($period, '');
            $group = isset($_POST['group']) ? $_POST['group'] : $_SESSION['SELECTED_GROUP'][0];
            if ($group != $_SESSION['SELECTED_GROUP'][0]) {
                $groupName = isset($_POST['groupName']) ? $_POST['groupName'] : '';
                $_SESSION['SELECTED_GROUP'] = array($group, $groupName);
                }
            $tableau = isset($_POST['tableau']) ? $_POST['tableau'] : $_SESSION['SELECTED_TABLEAU'][0];
            if ($tableau != $_SESSION['SELECTED_TABLEAU'][0]) {
                $tableauName = isset($_POST['tableauName']) ? $_POST['tableauName'] : '';
                $_SESSION['SELECTED_TABLEAU'] = array($tableau, $tableauName);
                }
            $vue = isset($_POST['vue']) ? $_POST['vue'] : $_SESSION['SELECTED_VUE'][0];
            if ($vue != $_SESSION['SELECTED_VUE'][0]) {
                $vueName = isset($_POST['vueName']) ? $_POST['vueName'] : '';
                $_SESSION['SELECTED_VUE'] = array($vue, $vueName);
                }
            }
        else {
            $period = isset($_POST['period']) ? $_POST['period'] : $_SESSION['SELECTED_PERIOD'][0];
            if ($period != $_SESSION['SELECTED_PERIOD'][0])
                $_SESSION['SELECTED_PERIOD'] = array($period, '');
            }
        $student = isset($_POST['student']) ? $_POST['student'] : $_SESSION['SELECTED_STUDENT'][0];
        if ($student != $_SESSION['SELECTED_STUDENT'][0]) {
            $studentName = isset($_POST['studentName']) ? $_POST['studentName'] : '';
            $_SESSION['SELECTED_STUDENT'] = array($student, $studentName);
            }
        $class = isset($_POST['class']) ? $_POST['class'] : $_SESSION['SELECTED_CLASS'][0];
        if ($class != $_SESSION['SELECTED_CLASS'][0]) {
            $className = isset($_POST['className']) ? $_POST['className'] : '';
            $_SESSION['SELECTED_CLASS'] = array($class, $className);
            $_SESSION['SELECTED_STUDENT'] = array(-1, '');
            }
        }
    exit($result);
    }



/****************************************************
    AUTRES TRUCS
****************************************************/
if ($ACTION == 'navbar')
    $thePage = isset($_POST['page']) ? $_POST['page'] : '';
else {
    if ($PAGE == 'error')
        $thePage = $PAGE_ERROR;
    else
        $thePage = $PAGE;
    }

// Largeur des listes déroulantes :
$btnWidth0 = '130px';
$btnWidth1 = '200px';
$btnWidth2 = '400px';

// pour effacer les fichiers temporaires (au cas où) :
foreach ($_SESSION['TEMP_FILES'] as $fileName) {
    if (is_file($fileName))
        unlink($fileName);
    }


/****************************************************
    FONCTIONS POUR REMPLIR LES LISTES DÉROULANTES
****************************************************/
function listPeriods($CONNEXION, $min=0, $max=-1, $assess=False) {
    /*
    retourne la liste des périodes (id, name).
    $min et $max permettent de ne pas donner toute la liste.
    $assess si un prof est en mode évaluation (on ajoute "toutes les périodes" et "bilan annuel").
    */
    $SQL = 'SELECT id, Periode FROM periodes ';
    $SQL .= 'WHERE id>='.$min.' ';
    if ($max > -1)
        $SQL .= 'AND id<='.$max.' ';
    $SQL .= 'ORDER BY id';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    $result = array();
    if ($assess)
        $result[] = array(-1, tr('All Periodes'));
    foreach ($tempResult as $period)
        $result[] = array($period[0], $period[1]);
    if ($assess)
        $result[] = array(999, tr('Annual Report'));
    return $result;
    }

function listClasses($CONNEXION) {
    /*
    retourne la liste des classes (id, name).
    */
    $result = array();
    $SQL = 'SELECT * FROM classes ORDER BY ordre, Classe';
    $STMT = $CONNEXION -> DB_COMMUN -> query($SQL);
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($tempResult as $class)
        if ($class[4] < 9999)
            $result[] = array($class[0], $class[1]);
    return $result;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];



/****************************************************
    LES SÉLECTEURS (PARTIE VARIABLE DU MENU)
****************************************************/
$menu1 = spaces(3).'<ul class="navbar-nav mr-auto" id="selects">'."\n";

// Affichage de la comboBox des périodes :
if (in_array('period', $selects[$thePage])) {
    if ($thePage == 'assess')
        $periods = listPeriods($CONNEXION, $min=0, $max=-1, $assess=True);
    else {
        if (($_SESSION['USER_MODE'] == 'eleve') and (! SHOW_ACTUAL_PERIODE))
            $maxPeriod = $_SESSION['ACTUAL_PERIOD'] - 1;
        else
            $maxPeriod = $_SESSION['ACTUAL_PERIOD'];
        $periods = listPeriods($CONNEXION, $min=1, $max=$maxPeriod, $assess=False);
        if ($_SESSION['SELECTED_PERIOD'][0] > $maxPeriod)
            $_SESSION['SELECTED_PERIOD'][0] = $maxPeriod;
        }
    $select = spaces(4).'<!-- PÉRIODE -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-period">'."\n";
    $title = tr('Period');
    if ($thePage == 'assess') {
        foreach ($periods as $period)
            if ($period[0] == $_SESSION['ASSESS_PERIOD'][0]) {
                $title = $period[1];
                $_SESSION['ASSESS_PERIOD'][1] = $title;
                }
        }
    else {
        foreach ($periods as $period)
            if ($period[0] == $_SESSION['SELECTED_PERIOD'][0]) {
                $title = $period[1];
                $_SESSION['SELECTED_PERIOD'][1] = $title;
                }
        }
    $select .= spaces(5).'<a id="period-title" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" href="#" style="max-width:'.$btnWidth0.';">'.$title.'</a>'."\n";
    $select .= spaces(5).'<ul class="dropdown-menu">'."\n";
    foreach ($periods as $period) {
        $reload = 'false';
        if (in_array('class', $selects[$thePage]))
            $reload = 'true';
        elseif (in_array('group', $selects[$thePage]))
            $reload = 'true';
        $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" reload='.$reload.' where="#period-title" what="'.$period[0].'" href="#">'.$period[1].'</a></li>'."\n";
        }
    $select .= spaces(5).'</ul>'."\n";
    $select .= spaces(4).'</li>'."\n";
    $menu1 .=  $select;
    }

// Affichage de la comboBox des classes :
if (in_array('class', $selects[$thePage])) {
    $classes = listClasses($CONNEXION);
    $select = spaces(4).'<!-- CLASSE -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-class">'."\n";
    $title = tr('Class');
    foreach ($classes as $classe)
        if ($classe[0] == $_SESSION['SELECTED_CLASS'][0])
            $title = $classe[1];
    $select .= spaces(8).'<a id="class-title" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" href="#" style="max-width:'.$btnWidth0.';">'.$title.'</a>'."\n";
    $select .= spaces(8).'<ul class="dropdown-menu">'."\n";
    foreach ($classes as $classe) {
        $reload = 'false';
        if (in_array('student', $selects[$thePage]))
            $reload = 'true';
        $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" reload='.$reload.' where="#class-title" what="'.$classe[0].'" href="#">'.$classe[1].'</a></li>'."\n";
        }
    $select .= spaces(5).'</ul>'."\n";
    $select .= spaces(4).'</li>'."\n";
    $menu1 .=  $select;
    }
// ou affichage de la comboBox des groupes :
elseif (in_array('group', $selects[$thePage])) {
    $groups = $CONNEXION -> listGroups();
    $select = spaces(4).'<!-- GROUPE -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-group">'."\n";
    $title = tr('Group');
    foreach ($groups as $group)
        if ($group[0] == $_SESSION['SELECTED_GROUP'][0])
            $title = $group[1];
    $select .= spaces(5).'<a id="group-title" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" href="#" style="max-width:'.$btnWidth1.';">'.$title.'</a>'."\n";
    $select .= spaces(5).'<ul class="dropdown-menu">'."\n";
    foreach ($groups as $group)
        $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" reload=true where="#group-title" what="'.$group[0].'" href="#">'.$group[1].'</a></li>'."\n";
    $select .= spaces(5).'</ul>'."\n";
    $select .= spaces(4).'</li>'."\n";
    $menu1 .=  $select;
    }

// Affichage de la comboBox des tableaux :
if (in_array('tableau', $selects[$thePage])) {
    $tableaux = $CONNEXION -> listTableaux($_SESSION['SELECTED_GROUP'][0], $_SESSION['ASSESS_PERIOD'][0]);
    $select = spaces(4).'<!-- TABLEAU -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-tableau">'."\n";
    $title = tr('Tableau');
    $found = False;
    foreach ($tableaux as $tableau)
        if ($tableau[0] == $_SESSION['SELECTED_TABLEAU'][0]) {
            $title = $tableau[1];
            $found = True;
            }
    if (count($tableaux) > 1)
        if ($_SESSION['SELECTED_TABLEAU'][0] == -2) {
            $title = tr('compilation');
            $found = True;
            }
    if (! $found)
        $_SESSION['SELECTED_TABLEAU'] = array(-1, '');
    $select .= spaces(5).'<a id="tableau-title" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" href="#" style="max-width:'.$btnWidth0.';">'.$title.'</a>'."\n";
    $select .= spaces(5).'<ul class="dropdown-menu">'."\n";
    foreach ($tableaux as $tableau)
        $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" reload=true where="#tableau-title" what="'.$tableau[0].'" href="#">'.$tableau[1].'</a></li>'."\n";
    // tableau de compilation de tous les tableaux publics :
    $addCompil = (count($tableaux) > 1);
    $addCompil = ($addCompil and ($_SESSION['SELECTED_GROUP'][0] > -1));
    $addCompil = ($addCompil and ($_SESSION['ASSESS_PERIOD'][0] > -1) and ($_SESSION['ASSESS_PERIOD'][0] < 999));
    if ($addCompil) {
        $select .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
        $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" reload=true where="#tableau-title" what="-2" href="#">'.tr('compilation').'</a></li>'."\n";
        }
    $select .= spaces(5).'</ul>'."\n";
    $select .= spaces(4).'</li>'."\n";
    $menu1 .=  $select;
    }
// Affichage de la comboBox des vues :
if (in_array('vue', $selects[$thePage])) {
    $vues = $CONNEXION -> listVues(
        $_SESSION['SELECTED_GROUP'][0], $_SESSION['ASSESS_PERIOD'][0], $_SESSION['SELECTED_TABLEAU'][0]);
    $select = spaces(4).'<!-- VUE -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-vue">'."\n";
    $title = tr('Vue');
    $found = False;
    foreach ($vues as $vue)
        if (($vue[2]) and ($vue[0] == $_SESSION['SELECTED_VUE'][0])) {
            $title = $vue[1];
            $found = True;
            }
    if (! $found)
        $_SESSION['SELECTED_VUE'] = array(-1, '');
    $select .= spaces(5).'<a id="vue-title" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" href="#" style="max-width:'.$btnWidth0.';">'.$title.'</a>'."\n";
    $select .= spaces(5).'<ul class="dropdown-menu">'."\n";
    //debugToConsole($vues);
    foreach ($vues as $vue)
        if ($vue[0] < 0)
            $select .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
        elseif ($vue[2])
            $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" reload=true where="#vue-title" what="'.$vue[0].'" href="#">'.$vue[1].'</a></li>'."\n";
        else
            $select .= spaces(6).'<a class="dropdown-header text-muted">'.$vue[1].'</a>'."\n";
    $select .= spaces(5).'</ul>'."\n";
    $select .= spaces(4).'</li>'."\n";
    $menu1 .=  $select;
    }

// Affichage de la comboBox des élèves et des boutons PRÉCÉDENT-SUIVANT :
if (in_array('student', $selects[$thePage])) {
    $select = '';

    // bouton précédent :
    $select .= spaces(4).'<!-- PRÉCÉDENT -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-previous">'."\n";
    $select .= spaces(5).'<a class="dropdown-item" page="'.$thePage.'" what="previous" title="'.tr('Previous').'" href="#"><span class="oi oi-caret-left"></span></a>'."\n";
    $select .= spaces(4).'</li>'."\n";
    // élèves :
    $students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);
    $select .= spaces(4).'<!-- ÉLÈVE -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-student">'."\n";
    $title = '';
    foreach ($students as $student)
        if ($student[0] == $_SESSION['SELECTED_STUDENT'][0])
            $title = $student[1];
    if ($title == '') {
        $_SESSION['SELECTED_STUDENT'] = array(-1, '');
        $title = tr('Student');
        }
    $selectStudent = spaces(5).'<a id="student-title" other="'.$_SESSION['SELECTED_STUDENT'][0].'" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" href="#" style="max-width:'.$btnWidth2.';">'.$title.'</a>'."\n";
    $selectStudent .= spaces(5).'<ul class="dropdown-menu">'."\n";
    $i = -1;
    foreach ($students as $student) {
        $i++;
        $selectStudent .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" where="#student-title" what="'.$student[0].'" href="#">'.$student[1].'</a></li>'."\n";
        }
    $selectStudent .= spaces(5).'</ul>'."\n";
    $select .= $selectStudent;
    $select .= spaces(4).'</li>'."\n";
    // bouton suivant :
    $select .= spaces(4).'<!-- SUIVANT -->'."\n";
    $select .= spaces(4).'<li class="nav-item dropdown" id="select-next">'."\n";
    $select .= spaces(5).'<a class="dropdown-item" page="'.$thePage.'" what="next" title="'.tr('Next').'" href="#"><span class="oi oi-caret-right"></span></a>'."\n";
    $select .= spaces(4).'</li>'."\n";
    $menu1 .=  $select;
    // pour pouvoir manipuler la liste des élèves avec JavaScript :
    $students4Js = spaces(4).'<script type="text/javascript">'."\n";
    $students4Js .= spaces(5).'// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later'."\n";
    $students4Js .= spaces(5).'var students = [';
    $i = -1;
    foreach ($students as $student) {
        $i++;
        if ($i > 0)
            $students4Js .= ', ';
        $students4Js .= '['.$student[0].', "'.$student[1].'"]';
        }
    $students4Js .= '];'."\n";
    $students4Js .= spaces(5).'// @license-end'."\n";
    $students4Js .= spaces(4).'</script>'."\n";
    $menu1 .=  $students4Js;
    }

$menu1 .= spaces(4).'<li id="select-custom-trombi"><!--trombinoscope perso-->'."\n";
// Affichage du bouton pour réaliser un trombinoscope personnalisé :
if ($thePage == 'class_trombinoscope')
    $menu1 .= spaces(5).'<a class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].'" page="'.$thePage.'" title="'.tr('Custom Trombinoscope').'" href="#"><img src="images/trombinoscope-custom.png" width="24px"></a>'."\n";
$menu1 .= spaces(4).'</li><!--trombinoscope perso-->'."\n";

$menu1 .= spaces(4).'<li id="select-compare-periods"><!--comparaison des périodes-->'."\n";
// Affichage du bouton pour comparer les périodes (camemberts et radars) :
if ($thePage == 'student_council')
    $menu1 .= spaces(5).'<a class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].'" page="'.$thePage.'" title="'.tr('Changes in results').'" href="#"><img src="images/compare-periods.png" width="24px"></a>'."\n";
$menu1 .= spaces(4).'</li><!--comparaison des périodes-->'."\n";

$menu1 .= spaces(4).'<li id="select-recalc-all"><!--recalculer tous les bilans-->'."\n";
// Affichage du bouton pour recalculer tous les bilans :
if ($thePage == 'assess')
    $menu1 .= spaces(5).'<a class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].'" page="'.$thePage.'" title="'.tr('Recalculate all balances&#10;(of the selected group)').'" href="#"><img src="images/xcalc.png" width="24px"></a>'."\n";
$menu1 .= spaces(4).'</li><!--recalculer tous les bilans-->'."\n";

$menu1 .= spaces(3).'</ul><!--selects-->'."\n";


if ($ACTION == 'navbar') {
    $result = $menu1;
    // le fichier JavaScript lié à la page s'il existe :
    $jsFile = 'js/navbar.js';
    if (file_exists($jsFile)) {
        $jsFile = fileInCache($jsFile);
        $result .= spaces(2).'<script src="'.$jsFile.'"></script>'."\n";
        }
    exit($result);
    }



/****************************************************
    DÉBUT ET TITRE
****************************************************/
$begin = spaces(1).'<!-- Fixed navbar'."\n";
$begin .= spaces(1).'================================================== -->'."\n";
$begin .= spaces(1).'<nav class="navbar navbar-expand-md navbar-'.$_SESSION['NAVBAR_STYLE'].' fixed-top bg-'.$_SESSION['NAVBAR_STYLE'].' d-print-none">'."\n";
//$begin .= spaces(2).'<a class="navbar-brand" href="https://verac.tuxfamily.org" target="_blank" title="'.tr('VÉRAC Web Site').'"><img src="images/logo.png" width="24px"></a>'."\n";
//$begin .= spaces(2).'<span aria-hidden="true">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>'."\n";
$begin .= spaces(2).'<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">'."\n";
$begin .= spaces(3).'<span class="navbar-toggler-icon"></span>'."\n";
$begin .= spaces(2).'</button>'."\n";

$begin .= spaces(2).'<!-- LE MENU -->'."\n";
$begin .= spaces(2).'<div id="navbarCollapse" class="navbar-collapse collapse">'."\n";



/****************************************************
    LE MENU (PARTIE FIXE)
****************************************************/
$menu0 = spaces(3).'<ul class="navbar-nav">'."\n";

if ($_SESSION['USER_MODE'] == 'prof') {
    $menu0 .= spaces(4).'<!-- CLASSES -->'."\n";
    $menu0 .= spaces(4).'<li class="nav-item dropdown">'."\n";
    $menu0 .= spaces(5).'<a href="#" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" title="'.tr('Classes').'"><img src="images/classes.png" width="24px"></a>'."\n";
    $menu0 .= spaces(5).'<div class="dropdown-menu">'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="class_synthese" href="./?page=class_synthese"><img src="images/classes.png" width="24px"> '.tr('Synthesis').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="class_opinions" href="./?page=class_opinions"><img src="images/appreciations.png" width="24px"> '.tr('Opinions').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="class_shared" href="./?page=class_shared"><img src="images/classes.png" width="24px"> '.tr('Shared competences').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=class_socle_components"><img src="images/socle-components.png" width="24px"> '.tr('Components of the socle').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="class_follows" href="./?page=class_follows"><img src="images/suivis.png" width="24px"> '.tr('Follows').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="class_list" href="./?page=class_list"><img src="images/classes.png" width="24px"> '.tr('List').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="class_trombinoscope" href="./?page=class_trombinoscope"><img src="images/trombinoscope.png" width="24px"> '.tr('Trombinoscope').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="class_statistics" href="./?page=class_statistics"><img src="images/statistics.png" width="24px"> '.tr('Statistics').'</a>'."\n";
    $menu0 .= spaces(5).'</div>'."\n";
    $menu0 .= spaces(4).'</li>'."\n";
    $menu0 .= spaces(4).'<!-- ÉLÈVES -->'."\n";
    $menu0 .= spaces(4).'<li class="nav-item dropdown">'."\n";
    $menu0 .= spaces(5).'<a href="#" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" title="'.tr('Students').'"><img src="images/eleve.png" width="24px"></a>'."\n";
    $menu0 .= spaces(5).'<div class="dropdown-menu">'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_council"><img src="images/conseil.png" width="24px"> '.tr('Council').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_balances"><img src="images/bilans.png" width="24px"> '.tr('Balances').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_referential"><img src="images/referentiel.png" width="24px"> '.tr('Referential').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_socle_components"><img src="images/socle-components.png" width="24px"> '.tr('Components of the socle').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_confidential"><img src="images/confidentiel.png" width="24px"> '.tr('Confidential').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_details"><img src="images/bilan-check.png" width="24px"> '.tr('Details').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_follows"><img src="images/suivis.png" width="24px"> '.tr('Follows').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_password"><img src="images/object-unlocked.png" width="24px"> '.tr('Password').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_papers"><img src="images/documents.png" width="24px"> '.tr('Papers').'</a>'."\n";
    $menu0 .= spaces(5).'</div>'."\n";
    $menu0 .= spaces(4).'</li>'."\n";
    if ($_SESSION['CAN_EVALUATE']) {
        $menu0 .= spaces(4).'<!-- ÉVALUER -->'."\n";
        $menu0 .= spaces(4).'<li class="nav-item dropdown">'."\n";
        $menu0 .= spaces(5).'<a class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].'" href="./?page=assess" title="'.tr('Assess').'"><img src="images/items.png" width="24px"></a>'."\n";
        $menu0 .= spaces(4).'</li>'."\n";
    }
    $menu0 .= spaces(4).'<!-- VALIDATIONS -->'."\n";
    $menu0 .= spaces(4).'<li class="nav-item dropdown">'."\n";
    $menu0 .= spaces(5).'<a href="#" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" title="'.tr('Validations of referential').'"><img src="images/validations.png" width="24px"></a>'."\n";
    $menu0 .= spaces(5).'<div class="dropdown-menu">'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_validations"><img src="images/validations-eleve.png" width="24px"> '.tr('Validations of a student').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=class_validations"><img src="images/validations-classe.png" width="24px"> '.tr('Synthesis of a class').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_validations_socle_components"><img src="images/validations-components.png" width="24px"> '.tr('Components of the socle').'</a>'."\n";
    $menu0 .= spaces(5).'</div>'."\n";
    $menu0 .= spaces(4).'</li>'."\n";
    $menu0 .= spaces(4).'<!-- OUTILS -->'."\n";
    $menu0 .= spaces(4).'<li class="nav-item dropdown">'."\n";
    $menu0 .= spaces(5).'<a href="#" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" title="'.tr('Tools').'"><img src="images/configure.png" width="24px"></a>'."\n";
    $menu0 .= spaces(5).'<div class="dropdown-menu">'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=infos"><img src="images/help-about.png" width="24px"> '.tr('News').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=password"><img src="images/object-unlocked.png" width="24px"> '.tr('Password').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=papers"><img src="images/documents.png" width="24px"> '.tr('Papers').'</a>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=calculator"><img src="images/xcalc.png" width="24px"> '.tr('Calculator').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="navbar_style" page="'.$thePage.'" href="#"><img src="images/navbar-style.png" width="24px"> '.tr('Navbar style').'</a>'."\n";
    $menu0 .= spaces(5).'</div>'."\n";
    $menu0 .= spaces(4).'</li>'."\n";
    $menu0 .= spaces(4).'<li class="nav-item dropdown"><img src="images/separator.png" width="16px"></li>'."\n";

    }
elseif ($_SESSION['USER_MODE'] == 'eleve') {
    $menu0 .= spaces(4).'<!-- RÉSULTATS -->'."\n";
    $menu0 .= spaces(4).'<li class="nav-item dropdown">'."\n";
    $menu0 .= spaces(5).'<a href="#" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown"><img src="images/bilans.png" width="24px"> '.tr('Results').'<strong class="caret"></strong></a>'."\n";
    $menu0 .= spaces(5).'<div class="dropdown-menu">'."\n";
    if (SHOW_BILANS)
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_balances"><img src="images/bilans.png" width="24px"> '.tr('Balances').'</a>'."\n";
    if (SHOW_REFERENTIEL)
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_referential"><img src="images/referentiel.png" width="24px"> '.tr('Referential').'</a>'."\n";
    if (SHOW_SOCLE_COMPONENTS)
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_socle_components"><img src="images/socle-components.png" width="24px"> '.tr('Components of the socle').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    if (SHOW_DETAILS)
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_details"><img src="images/bilan-check.png" width="24px"> '.tr('Details').'</a>'."\n";
    if ((SHOW_SUIVI) and ($_SESSION['USER_SUIVI']))
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=student_follows"><img src="images/suivis.png" width="24px"> '.tr('Follows').'</a>'."\n";
    if (SHOW_APPRECIATION_GROUPE) {
        $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=class_opinions"><img src="images/appreciations-groupes.png" width="24px"> '.tr('Opinions').'</a>'."\n";
        }
    $menu0 .= spaces(5).'</div>'."\n";
    $menu0 .= spaces(4).'</li>'."\n";
    $menu0 .= spaces(4).'<!-- OUTILS -->'."\n";
    $menu0 .= spaces(4).'<li class="nav-item dropdown">'."\n";
    $menu0 .= spaces(5).'<a href="#" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown"><img src="images/configure.png" width="24px"> '.tr('Tools').'<strong class="caret"></strong></a>'."\n";
    $menu0 .= spaces(5).'<div class="dropdown-menu">'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=infos"><img src="images/help-about.png" width="24px"> '.tr('News').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=password"><img src="images/object-unlocked.png" width="24px"> '.tr('Password').'</a>'."\n";
    if (SHOW_DOCUMENTS)
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=papers"><img src="images/documents.png" width="24px"> '.tr('Papers').'</a>'."\n";
    if (SHOW_CALCULATRICE)
        $menu0 .= spaces(6).'<a class="dropdown-item" href="./?page=calculator"><img src="images/xcalc.png" width="24px"> '.tr('Calculator').'</a>'."\n";
    $menu0 .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    $menu0 .= spaces(6).'<a class="dropdown-item" id="navbar_style" page="'.$thePage.'" href="#"><img src="images/navbar-style.png" width="24px"> '.tr('Navbar style').'</a>'."\n";
    $menu0 .= spaces(5).'</div>'."\n";
    $menu0 .= spaces(4).'</li>'."\n";
    }

$menu0 .= spaces(3).'</ul>'."\n";



/****************************************************
    LE MENU (FIN DE LA PARTIE FIXE)
****************************************************/
$menu9 = ''."\n";
$menu9 .= ''."\n";
$menu9 .= spaces(3).'<!-- PARTIE DE DROITE -->'."\n";
$menu9 .= spaces(3).'<ul class="navbar-nav navbar-right">'."\n";
$menu9 .= spaces(4).'<!-- I18N -->'."\n";
// locales :
$locales = array();
$locales[] = array('fr_FR', 'Français');
$locales[] = array('en_US', 'English');
//$locales[] = array('', '');
$menu9 .= spaces(4).'<li class="nav-item dropdown">'."\n";
$menu9 .= spaces(5).'<a href="#" class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].' dropdown-toggle" data-toggle="dropdown" title="'
    .tr('Change language').'"><strong>'.$_SESSION['LANG']
    .'</strong><strong class="caret"></strong></a>'."\n";
$menu9 .= spaces(5).'<ul id="lang" class="dropdown-menu">'."\n";
foreach ($locales as $locale)
    $menu9 .= spaces(6).'<li><a class="dropdown-item" lang="'.$locale[0].'" page="'.$thePage.'" href="#">'.$locale[0].' ('.$locale[1].')'.'</a></li>'."\n";
$menu9 .= spaces(5).'</ul>'."\n";
$menu9 .= spaces(4).'</li>'."\n";
$menu9 .= spaces(4).'<!-- AIDE ET DÉCONNEXION -->'."\n";
$menu9 .= spaces(4).'<li class="nav-item dropdown">'."\n";
$menu9 .= spaces(5).'<a class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].'" href="https://verac.tuxfamily.org/site/help-homepage.html#veracweb" target="_blank" title="'.tr('Help').'"><img src="images/help-about.png" width="24px"></a>'."\n";
$menu9 .= spaces(4).'</li>'."\n";
$menu9 .= spaces(4).'<li class="nav-item dropdown">'."\n";
$menu9 .= spaces(5).'<a class="nav-link btn-sm btn-'.$_SESSION['NAVBAR_STYLE'].'" href="./?page=login" title="'. tr('Disconnect').'"><img src="images/logout.png" width="24px"></a>'."\n";
$menu9 .= spaces(4).'</li>'."\n";
//$menu9 .= spaces(4).'<li class="nav-item dropdown"><img src="images/separator.png" width="16px"></li>'."\n";
$menu9 .= spaces(3).'</ul>'."\n";



/****************************************************
    FIN DE L'AFFICHAGE
****************************************************/
$end = spaces(2).'</div>'."\n";
$end .= ''."\n";
$end .= spaces(1).'</nav><!--navbar-->'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$menu1All = ''."\n";
$menu1All .= ''."\n";
$menu1All .= spaces(3).'<!-- ZONE DE SÉLECTION (REMPLIE SELON LA PAGE) -->'."\n";
$menu1All .= $menu1;
$result = $begin.$menu0.$menu1All.$menu9.$end;
echo $result;

?>

