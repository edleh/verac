<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    UTILISATION DE :
    http://www.tutorialchip.com/php-download-file-script
****************************************************/



/****************************************************
    VÉRIFICATIONS
****************************************************/
require_once('../libs/constantes.php');
require_once('../libs/fonctions_session.php');
$fichier_constantes = CHEMIN_CONFIG.'constantes.php';
require_once($fichier_constantes);

updateSession();

$path = '';
$thePage = $_SESSION['PAGE'];
if ($thePage == 'papers') {
    if ($_SESSION['USER_MODE'] == 'prof')
        $path = 'docsprofs'.DIRECTORY_SEPARATOR;
    elseif ($_SESSION['USER_MODE'] == 'eleve')
        $path = 'documents'.DIRECTORY_SEPARATOR;
    }
elseif ($thePage == 'student_papers')
    $path = 'documents'.DIRECTORY_SEPARATOR;
else
    exit('ERROR');
$downloadPath = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_PROTECTED.$path;
$fileName = isset($_REQUEST['f']) ? $_REQUEST['f'] : '';

require_once('../libs/class.chip_download.php');
$args = array(
    'download_path' => $downloadPath, 
    'file' => $fileName, 
    );
$download = new chip_download($args);
$download_hook = $download -> get_download_hook();
if( $download_hook['download'] == True ) {
    $download -> get_download();
    }
else
    exit('ERROR');



?>
