
    <div id="footer">
        <p class="text-center">
            <br/><br/>
            <a target="_blank" href="https://www.gnu.org/copyleft/gpl.html"><img src="images/logo-gplv3.png" alt="licence GNU GPL 3" title="licence GNU GPL 3" /></a>
            <a target="_blank" href="http://python.org"><img src="images/logo-python.png" alt="réalisé avec Python" title="réalisé avec Python" /></a>
            <a target="_blank" href="https://secure.php.net/"><img src="images/logo-php.png" alt="réalisé avec PHP" title="réalisé avec PHP" /></a>
            <a target="_blank" href="https://www.sqlite.org"><img src="images/logo-SQLite.png" alt="utilise des bases de données SQLite" title="utilise des bases de données SQLite" /></a>
            <a target="_blank" href="http://tuxfamily.org"><img src="images/logo-tf.png" alt="hébergé librement chez tuxfamily.org" title="hébergé librement chez tuxfamily.org" /></a>
        </p>
    </div>
