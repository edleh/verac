<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if (($_SESSION['USER_MODE'] == 'prof') and ($_SESSION['SELECTED_CLASS'][0] == -1))
    $result = "\n".message('info', 'Select a class.', '');
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function listValidSubjects($CONNEXION, $students) {
    /*
    cherche les matières enseignées dans une classe
    */
    $EXCLUDE = array(
        'EPI_SAN', 'EPI_ART', 'EPI_EDD', 'EPI_ICC', 'EPI_LGA', 'EPI_LGE', 'EPI_PRO', 'EPI_STS', 
        'AP_LSU', 'PAR_LSU');
    $RESULT = array();
    // on crée le texte de la liste des élèves :
    $id_students = array2string($students, 0);
    // on vérifie les id_profs indiqués pour la matière et les élèves
    $SQL = 'SELECT * FROM eleve_prof ';
    $SQL .= 'WHERE id_eleve IN ('.$id_students.')';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($tempResult as $row)
        if (! in_array($row['Matiere'], $EXCLUDE)) {
            if (array_key_exists($row['Matiere'], $RESULT))
                $RESULT[$row['Matiere']][] = $row['id_prof'];
            else
                $RESULT[$row['Matiere']] = array($row['id_prof']);
            }
    return $RESULT;
    }

function getTitles($CONNEXION, $n, $sousRubriques, $validSubjects) {
    /*
    retourne la ligne des titres du tableau
    */
    $result = '';
    $result .= spaces($n).'<thead class="bilan">'."\n";
    $result .= spaces($n + 1).'<tr>'."\n";
    $result .= spaces($n + 2).'<th class="student"><b>'.tr('STUDENT').'</b></th>'."\n";
    foreach ($sousRubriques as $sousRubrique)
        $result .= spaces($n + 2).'<th class="verticalTitle" title="'.$sousRubrique[1].'"><div class="vertical"><b>'.$sousRubrique[0].'</b></div></th>'."\n";
    foreach ($validSubjects as $matiere)
        $result .= spaces($n + 2).'<th class="verticalTitle" title="'.$matiere['MatiereLabel'].'"><div class="vertical"><b>'.$matiere['MatiereCode'].'</b></div></th>'."\n";
    $result .= spaces($n + 2).'<td class="separator"></td>'."\n";
    foreach (getArrayLetters() as $value)
        $result .= spaces($n + 2).'<th class="'.$value.'" width="40px"><b>'.valeur2affichage($value).'</b></th>'."\n";
    $result .= spaces($n + 1).'</tr>'."\n";
    $result .= spaces($n).'</thead>'."\n";
    return $result;
    }

function getStudentNumberForEachValue($CONNEXION, $id_eleve) {
    /*
    retourne le nombre de resultats par niveau pour un élève donné
    */
    $letters = getArrayLetters();
    $totaux = array();
    $SQL = 'SELECT value FROM syntheses ';
    $SQL .= 'WHERE id_eleve=:id_eleve ';
    $SQL .= 'AND id_prof=-1 ';
    $SQL .= 'AND name=:name';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    foreach ($letters as $letter) {
        $OPT[':name'] = 'Total-'.$letter;
        $STMT -> execute($OPT);
        $RESULT = $STMT -> fetch();
        $totaux[$letter] = ($RESULT) ? $RESULT[0] : 0;
        }
    return $totaux;
    }

function getStudentRow(
        $CONNEXION, $n, $student, $sousRubriques, $validSubjects, $subjectsLabels) {
    /*
    retourne la ligne des résultats d'un élève
    */
    $result = spaces($n).'<tr>'."\n";
    // le nom :
    if ($student[0] > 0)
        $result .= spaces($n + 1).'<td class="student">'.$student[1].'</td>'."\n";
    else
        $result .= spaces($n + 1).'<td class="student"><b>'.$student[1].'</b></td>'."\n";
    // les résultats :
    $data = studentResultsInSousRubriques(
        $CONNEXION, $student[0], $validSubjects, $sousRubriques);
    foreach ($data as $row) {
        $title = '';
        if (array_key_exists($row['name'], $subjectsLabels))
            $title = $subjectsLabels[$row['name']];
        $result .= spaces($n + 1).'<td class="'.$row['value'].'" title="'.$title.'">'.valeur2affichage($row['value']).'</td>'."\n";
        }
    // les pourcentages :
    $result .= spaces($n + 1).'<td class="separator"></td>'."\n";

    if ($student[0] > 0)
        $liste_totaux = getStudentNumberForEachValue($CONNEXION, $student[0]);
    else
        $liste_totaux = $_SESSION['TEMP_ARRAY'];
    // on récupère la valeur la plus haute :
    $max = 0;
    $key_max = '';
    foreach ($liste_totaux as $key => $value) {
        if ($student[0] > 0)
            $_SESSION['TEMP_ARRAY'][$key] += $value;
        if ($value > $max) {
            $key_max = $key;
            $max = $value;
            }
        }
    // on affiche
    foreach ($liste_totaux as $key => $value) {
        $somme = array_sum($liste_totaux);
        $class = ($key == $key_max) ? $key : '';
        $value = ($somme > 0) ? round(100 * ($value / $somme), 0) : 0;
        if ($value > 0)
            $result .= spaces($n + 1).'<td align="center" class="'.$class.'">'.$value.'%</td>'."\n";
        else
            $result .= spaces($n + 1).'<td align="center" class="'.$class.'"></td>'."\n";
        }
    $result .= spaces($n).'</tr>'."\n";
    return $result;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
$students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);

// TEMP_ARRAY servira à récupérer les % du groupe :
$_SESSION['TEMP_ARRAY'] = array();
foreach (getArrayLetters() as $key)
    $_SESSION['TEMP_ARRAY'][$key] = 0;

// on récupère la liste des sous-rubriques du bulletin :
$sousRubriques = sousRubriques($CONNEXION, 'bulletin');

// la liste des matières du bulletin :
$subjects = subjects($CONNEXION);
$subjectsLabels = array();
foreach (array('Bulletin', 'Speciales', 'Autre') as $where) {
    foreach ($subjects[$where] as $subject) {
        $subjectsLabels[$subject['MatiereCode']] = $subject['MatiereLabel'];
        }
    }
foreach ($sousRubriques as $sousRubrique)
    $subjectsLabels[$sousRubrique[0]] = $sousRubrique[1];

// limite du nombre de bilans persos sur le bulletin :
$limite = readConfig($CONNEXION, 'limiteBLTPerso');
$limite = $limite['INT'];
// on récupère les matières où un prof est signalé :
$validSubjects = array();
$tempValidSubjects = listValidSubjects($CONNEXION, $students);
foreach ($subjects['Bulletin'] as $subject) {
    if (array_key_exists($subject['MatiereCode'], $tempValidSubjects))
        $validSubjects[] = $subject;
    }
foreach ($subjects['Speciales'] as $subject) {
    if ($subject['MatiereCode'] == 'VS') {
        if (array_key_exists($subject['MatiereCode'], $tempValidSubjects))
            $validSubjects[] = $subject;
        }
    }

// on prépare l'interligne pour les cellules de bordure :
$colspan = 1 + count($sousRubriques) + count($validSubjects) + count(getArrayLetters()) + 2;

// on ajoute la classe à la liste d'élèves
$idClasseInResults = idClassInResults($CONNEXION, $_SESSION['SELECTED_CLASS'][1]);
$students[] = array($idClasseInResults, tr('CLASS'));



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.tr('Class Synthesis: ').$_SESSION['SELECTED_CLASS'][1].'</h2>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";

$printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
$printTitle .= spaces(3).'<p class="text-center">';
$printTitle .= tr('Synthesis');
$printTitle .= ' - '.$_SESSION['SELECTED_PERIOD'][1];
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</p>'."\n";
$printTitle .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE DU TABLEAU
****************************************************/
$table = '';
$table .= spaces(2).'<table border="1" class="bilan">'."\n";
$table .= getTitles($CONNEXION, 3, $sousRubriques, $validSubjects);
$table .= spaces(3).'<tbody>'."\n";
foreach ($students as $student) {
    if ($student[0] > 0)
        $table .= getStudentRow(
            $CONNEXION, 4, $student, $sousRubriques, $validSubjects, $subjectsLabels);
    else {
        $table .= spaces(4).'<tr><td class="interligne" colspan="'.$colspan.'"></td></tr>'."\n";
        $table .= getStudentRow(
            $CONNEXION, 4, $student, $sousRubriques, $validSubjects, $subjectsLabels);
        }
    }
$table .= spaces(3).'</tbody>'."\n";
$table .= spaces(2).'</table>'."\n";



/****************************************************
    REMARQUES
****************************************************/
$remarks = '';
$remarks .= spaces(2).'<br/>'."\n";
$remarks .= spaces(2).'<!-- REMARKS -->'."\n";
$remarks .= spaces(2).'<div class="alert alert-warning text-left">'."\n";
$remarks .= spaces(3).'<div class="row">'."\n";
$remarks .= spaces(4).'<div class="col-sm-2">'."\n";
$remarks .= spaces(5).'<small><b>'.tr('REMARKS:').'</b></small>'."\n";
$remarks .= spaces(4).'</div>'."\n";
$remarks .= spaces(4).'<div class="col-sm-10">'."\n";
$remarks .= spaces(3).'<small>'."\n";
$remarks .= spaces(4).'<ul> '."\n";
$remarks .= spaces(5).'<li>'
    .tr('percentages are calculated on all the balances present on the bulletin (and not on the columns of this table);').'</li> '."\n";
$remarks .= spaces(5).'<li>'
    .tr('percentages are rounded to the nearest integer (their sum is therefore not necessarily 100%).').'</li> '."\n";
$remarks .= spaces(4).'</ul>'."\n";
$remarks .= spaces(3).'</small>'."\n";
$remarks .= spaces(4).'</div>'."\n";
$remarks .= spaces(3).'</div>'."\n";
$remarks .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle.$printTitle.$table.$remarks;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
