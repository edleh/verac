<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
//if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
//    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if ($_SESSION['USER_MODE'] == 'prof') {
    if ($_SESSION['SELECTED_CLASS'][0] == -1)
        $result = "\n".message('info', 'Select a class.', '');
        }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    if ($what == 'changeDirectory') {
        $result = isset($_POST['value']) ? $_POST['value'] : '';
        $_SESSION['FOLLOWS_DIRECTORY'] = $result;
        }
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function studentFollowsEvals(
        $CONNEXION, $id_eleve, $id_PP, $order=' DESC') {
    /*
    */
    $filename = $CONNEXION -> CHEMIN_SUIVIS.'suivi_'.$id_PP.'.sqlite';
    if (! is_readable($filename))
        return array();
    $db_suivisPP = new PDO('sqlite:'.$filename);
    $db_suivisPP -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM suivi_evals ';
    $OPT = array();
    if ($id_eleve != -1) {
        $SQL .= 'WHERE id_eleve=:id_eleve ';
        $OPT = array(':id_eleve' => $id_eleve);
        }
    $SQL .= 'ORDER BY date'.$order.', horaire, id_prof, matiere, id_eleve';
    $STMT = $db_suivisPP -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    return $RESULT;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
// liste des élèves de la classe :
$students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);
$id_students = array2string($students, 0);
// on récupère la liste des compétences suivies pour ces élèves :
$follows = $CONNEXION -> getFollows($_SESSION['SELECTED_CLASS'][0], $id_students);
// liste des noms des profs :
$teachersNames = listTeachersNames($CONNEXION);
// et celle des horaires de l'établissement :
$horaires = $CONNEXION -> horaires();

if (! isset($btnWidth2))
    $btnWidth2 = '400px';
if (! isset($thePage))
    $thePage = $_SESSION['PAGE'];



/****************************************************
    SÉLECTEURS (SENS)
    + TITRE POUR IMPRESSION
****************************************************/
$printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
$printTitle .= spaces(3).'<p class="text-center">';
$printTitle .= tr('Follows');
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</br>';

$selects = spaces(3).'<nav class="navbar navbar-expand-lg navbar-'.$_SESSION['NAVBAR_STYLE'].'">'."\n";
$selects .= spaces(4).'<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar2" aria-expanded="false" aria-label="Toggle navigation">'."\n";
$selects .= spaces(5).'<span class="navbar-toggler-icon"></span>'."\n";
$selects .= spaces(4).'</button>'."\n";
$selects .= spaces(3).'<div class="collapse navbar-collapse justify-content-md-center" id="navbar2">'."\n";

$select = spaces(4).'<div id="select-directory" class="btn-group btn-group-sm text-center">'."\n";
$sensLabel = array(tr('reverse chronological directory'), tr('chronological directory'));
$title = $sensLabel[$_SESSION['FOLLOWS_DIRECTORY']];
$printTitle .= $title;
$select .= spaces(5).'<a id="directory-title" class="nav-link btn-sm btn-light dropdown-toggle" data-toggle="dropdown" href="#" style="width:'.$btnWidth2.';">'.$title.'</a>'."\n";
$select .= spaces(5).'<ul class="dropdown-menu">'."\n";
$select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="0" reload=true href="#">'.$sensLabel[0].'</a></li>'."\n";
$select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="1" reload=true href="#">'.$sensLabel[1].'</a></li>'."\n";
$select .= spaces(5).'</ul>'."\n";
$select .= spaces(4).'</div>'."\n";
$selects .=  $select;

$selects .= spaces(3).'</div>'."\n";
$selects .= spaces(3).'</nav>'."\n";

$printTitle .= '</p>'."\n";
$printTitle .= spaces(2).'</div>'."\n";



/****************************************************
    TITRE DU DOCUMENT
****************************************************/
if (count($follows) > 0) {
    $pageTitle = spaces(3).'<!-- PAGE HEADER -->'."\n";
    $pageTitle .= spaces(3).'<div class="d-print-none">'."\n";
    $pageTitle .= spaces(4).'<div class="page-header">'."\n";
    $pageTitle .= spaces(5).'<h2>'.tr('FOLLOWED ASSESSMENT: ').$_SESSION['SELECTED_CLASS'][1].'</h2>'."\n";
    $pageTitle .= spaces(4).'</div>'."\n";
    $pageTitle .= spaces(3).'</div>'."\n";
    }
else
    $pageTitle = "\n".message('danger', 'No student followed in this class.', '');



/****************************************************
    CONTENU DE LA PAGE
****************************************************/
$pageContent = '';

$evalsTitles = array(tr('Date'), tr('Schedule'), tr('Subject'), tr('STUDENT'));

$table = spaces(5).'<h3>'.tr('List of competences followed').'</h3>'."\n";
$table .= spaces(5).'<table border="1" class="bilan">'."\n";
$table .= spaces(6).'<tbody>'."\n";
foreach (array_keys($follows) as $id_PP)
    foreach ($follows[$id_PP]['ALL'] as $follow) {
        $table .= spaces(7).'<tr>'."\n";
        $table .= spaces(8).'<td class="competence_label"><b>'.$follow[1].'</b></td>'."\n";
        $table .= spaces(8).'<td class="competence_label">'.htmlspecialchars($follow[2]).'</td>'."\n";
        $table .= spaces(7).'</tr>'."\n";
        $evalsTitles[] = $follow[1];
        }
$table .= spaces(6).'</tbody>'."\n";
$table .= spaces(5).'</table>'."\n";
$evalsTitles[] = tr('Remark');
$pageContent .= $table;

$columnsCount = count($evalsTitles);
$evalsLines = array();

if ($_SESSION['FOLLOWS_DIRECTORY'] == 0)
    $order = ' DESC';
else
    $order = '';
foreach (array_keys($follows) as $id_PP) {
    // pour placer la valeur de l'évaluation dans la bonne colonne :
    $indexInLine = array();
    $index = 4;
    foreach ($follows[$id_PP]['ALL'] as $competence_suivie) {
        $indexInLine[$competence_suivie[0]] = $index;
        $index ++;
        }
    $followsEvals = studentFollowsEvals($CONNEXION, -1, $id_PP, $order);
    $line = array();
    $reference = '';
    foreach ($followsEvals as $evaluation) {
        $id_eleve = $evaluation['id_eleve'];
        if ($id_eleve == -1) {
            $studentName = '<b>'.tr('GROUP').'</b>';
            $who = 'ALL';
            }
        else {
            $studentName = getStudentName($CONNEXION, $id_eleve, $mode='');
            $who = $id_eleve;
            }
        $date = $evaluation['date'];
        $horaire = $evaluation['horaire'];
        $id_prof = $evaluation['id_prof'];
        $matiere = $evaluation['matiere'];
        $id_cpt = $evaluation['id_cpt'];
        $value = $evaluation['value'];
        $teacherName = $teachersNames[$id_prof][1];
        if ($date.$horaire.$id_prof.$matiere.$id_eleve != $reference) {
            // on démarre une nouvelle ligne
            if ($reference != '')
                $evalsLines[] = $line;
            $reference = $date.$horaire.$id_prof.$matiere.$id_eleve;
            // création de la ligne avec les valeurs par défaut :
            $line = array(array($date, ''), $horaires[$horaire]);
            $line[] = array($matiere, $teacherName);
            $line[] = array($studentName, $id_eleve);
            foreach ($follows[$id_PP]['ALL'] as $competence_suivie)
                $line[] = array('%%%', '');
            $line[] = array('', '');
            // on remplace ce qui doit l'être :
            foreach ($follows[$id_PP][$who] as $competence_suivie) {
                if ($competence_suivie[0] == $id_cpt) {
                    $title = $competence_suivie[1];
                    if ($competence_suivie[3] != '')
                        $title .= ' ('.$competence_suivie[3].')';
                    else
                        $title .= ' ('.$competence_suivie[2].')';
                    $line[$indexInLine[$id_cpt]] = array($value, $title);
                    }
                }
            if ($id_cpt == -1)
                $line[$columnsCount - 1] = array($value, '');
            }
        else {
            // on remplace ce qui doit l'être :
            foreach ($follows[$id_PP][$who] as $competence_suivie) {
                if ($competence_suivie[0] == $id_cpt) {
                    $title = $competence_suivie[1];
                    if ($competence_suivie[3] != '')
                        $title .= ' ('.$competence_suivie[3].')';
                    else
                        $title .= ' ('.$competence_suivie[2].')';
                    $line[$indexInLine[$id_cpt]] = array($value, $title);
                    }
                }
            if ($id_cpt == -1)
                $line[$columnsCount - 1] = array($value, '');
            }
        }
    if ($reference != '')
        $evalsLines[] = $line;
    }

$table = spaces(5).'<h3>'.tr('Assessments').'</h3>'."\n";
$table .= spaces(5).'<table border="1" class="bilan">'."\n";
$table .= spaces(6).'<thead class="bilan">'."\n";
$table .= spaces(7).'<tr>'."\n";
foreach ($evalsTitles as $title) {
    if ($title != tr('Remark'))
        $table .= spaces(8).'<th class="title">'.$title.'</th>'."\n";
    else
        $table .= spaces(8).'<th class="last">'.$title.'</th>'."\n";
    }
$table .= spaces(7).'</tr>'."\n";
$table .= spaces(6).'</thead>'."\n";
$table .= spaces(6).'<tbody>'."\n";
$actualDate = '';
foreach ($evalsLines as $line) {
    if ($line[0][0] != $actualDate) {
        $actualDate = $line[0][0];
        $table .= spaces(7).'<tr>'."\n";
        $table .= spaces(8).'<td class="interligne" colspan="'.$columnsCount.'"></td>'."\n";
        $table .= spaces(7).'</tr>'."\n";
        }
    else
        $line[0][0] = '';
    $table .= spaces(7).'<tr>'."\n";
    for ($i=0; $i<$columnsCount; $i++) {
        if ($i < 4)
            $table .= spaces(8).'<td align="center" title="'.$line[$i][1].'">'.$line[$i][0].'</td>'."\n";
        elseif ($i == $columnsCount - 1)
            $table .= spaces(8).'<td class="competence_label" title="'.$line[$i][1].'">'.$line[$i][0].'</td>'."\n";
        elseif ($line[$i][0] == '%%%')
            $table .= spaces(8).'<td bgcolor="#dddddd"></td>'."\n";
        else
            $table .= spaces(8).'<td class="'.$line[$i][0].'" title="'.$line[$i][1].'">'.valeur2affichage($line[$i][0]).'</td>'."\n";
        }
    $table .= spaces(7).'</tr>'."\n";
    }
$table .= spaces(6).'</tbody>'."\n";
$table .= spaces(5).'</table>'."\n";

$pageContent .= $table;

if (count($follows) < 1)
    $pageContent = '';



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
$result .= "\n".$printTitle;
// sélecteurs :
$result .= spaces(2).'<div id="selects_top" class="d-print-none">'."\n";
$result .= $selects;
$result .= spaces(2).'</div>'."\n";
$result .= spaces(2).'<br /><br />'."\n";
// pour centrer l'affichage :
$result .= spaces(2).'<div class="container theme-showcase">'."\n";
$result .= $pageTitle;
$result .= $pageContent;
$result .= spaces(2).'</div><br />'."\n";


if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
