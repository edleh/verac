<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
else {
    // VÉRIFICATION DE L'ÉTAT DE LA BASE PERSO :
    if (! $_SESSION['DB_PROF_EXISTS'])
        $result = "\n".message(
            'danger', 
            'No database.',
            'The database of your assessments is not available.',
            $message1 = 'Use VÉRAC software to create your database and send it to the website.');
    elseif (! $_SESSION['DB_PROF_OK'])
        $result = "\n".message(
            'danger', 
            'Your data is not writable.',
            'No changes you make will be taken into account.',
            $message1 = 'Notify your administrator to resolve the problem.');
    }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
require_once('libs/fonctions_calcul.php');

if ($ACTION == 'request') {
    $result = '';
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    $vue = isset($_POST['vue']) ? $_POST['vue'] : '';
    if ($what == 'changeAppreciation') {
        if (VERSION_NAME == 'demo')
            $result = True;
        elseif ($vue == 'suivis') {
            $id_prof = $_SESSION['USER_ID'];
            $matiere = isset($_POST['matiere']) ? $_POST['matiere'] : '';
            $id_pp = isset($_POST['id_pp']) ? $_POST['id_pp'] : '';
            $id_eleve = isset($_POST['id_eleve']) ? $_POST['id_eleve'] : '';
            if ($id_eleve < 0)
                $id_eleve = -1;
            $date = isset($_POST['date']) ? $_POST['date'] : '';
            $horaire = isset($_POST['horaire']) ? $_POST['horaire'] : '';
            $id_item = -1;
            $value = isset($_POST['value']) ? $_POST['value'] : '';
            // en cas d'ancienne version de PHP :
            if (get_magic_quotes_gpc())
                $value = stripslashes($value);
            $value = str_replace('|||', '&', $value);
            $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
            $data = array($id_prof, $matiere, $id_pp, $id_eleve, $date, $horaire, $id_item, $value);
            $result = $CONNEXION -> setFollows($data);
            }
        else {
            $_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime'] = True;
            $id_eleve = isset($_POST['id_eleve']) ? $_POST['id_eleve'] : '';
            $periode = isset($_POST['periode']) ? $_POST['periode'] : '';
            $value = isset($_POST['value']) ? $_POST['value'] : '';
            // en cas d'ancienne version de PHP :
            if (get_magic_quotes_gpc())
                $value = stripslashes($value);
            $value = str_replace('|||', '&', $value);
            $matiere = isset($_POST['matiere']) ? $_POST['matiere'] : '';
            $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
            $result = $CONNEXION -> setAppreciation($id_eleve, $periode, $value, $matiere);
            if ($result) {
                $result = $CONNEXION -> updateDatetime();
                if ($result)
                    $_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime'] = False;
                }
            }
        }
    elseif ($what == 'changeEval') {
        if (VERSION_NAME == 'demo') {
            $value = (isset($_POST['value'])) ? affichage2valeur($_POST['value']) : '';
            $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
            $result = getItemClass($CONNEXION, $value);
            }
        elseif ($vue == 'suivis') {
            $id_prof = $_SESSION['USER_ID'];
            $matiere = isset($_POST['matiere']) ? $_POST['matiere'] : '';
            $id_pp = isset($_POST['id_pp']) ? $_POST['id_pp'] : '';
            $id_eleve = isset($_POST['id_eleve']) ? $_POST['id_eleve'] : '';
            if ($id_eleve < 0)
                $id_eleve = -1;
            $date = isset($_POST['date']) ? $_POST['date'] : '';
            $horaire = isset($_POST['horaire']) ? $_POST['horaire'] : '';
            $id_item = isset($_POST['id_item']) ? $_POST['id_item'] : '';
            $value = (isset($_POST['value'])) ? affichage2valeur($_POST['value']) : '';
            $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
            $data = array($id_prof, $matiere, $id_pp, $id_eleve, $date, $horaire, $id_item, $value);
            $result = $CONNEXION -> setFollows($data);
            $result = getItemClass($CONNEXION, $value);
            }
        else {
            $_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime'] = True;
            $id_eleve = isset($_POST['id_eleve']) ? $_POST['id_eleve'] : '';
            $id_tableau = isset($_POST['id_tableau']) ? $_POST['id_tableau'] : '';
            $id_item = isset($_POST['id_item']) ? $_POST['id_item'] : '';
            $value = (isset($_POST['value'])) ? affichage2valeur($_POST['value']) : '';
            $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
            $result = $CONNEXION -> setItemValue($id_eleve, $id_tableau, $id_item, -1, $value);
            if ($result) {
                if (! in_array($id_eleve, $_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves']))
                    $_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'][] = $id_eleve;
                if (! in_array($id_item, $_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_items']))
                    $_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_items'][] = $id_item;
                $result = $CONNEXION -> updateDatetime();
                if ($result)
                    $_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime'] = False;
                }
            $result = getItemClass($CONNEXION, $value);
            }
        }
    elseif ($what == 'updateDatetime') {
        if (VERSION_NAME != 'demo') {
            if ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime']) {
                $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
                $result = $CONNEXION -> updateDatetime();
                if ($result)
                    $_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime'] = False;
                }
            }
        }
    elseif ($what == 'reCalcBilans') {
        if (VERSION_NAME != 'demo') {
            //debugToConsole('assess.php : reCalcBilans 2');
            $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
            $CONNEXION -> reCalcBilans($all=True);
            $result = $CONNEXION -> updateDatetime();
            if ($result)
                $_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime'] = False;
            }
        }
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function calcItems($CONNEXION, $id_tableau, $id_eleve, $id_item) {
    /*
    */
    $RESULT = array();
    $id_tableaux = $id_tableau;
    // cas d'une compilation de tableaux :
    if ($id_tableau == -2) {
        $tableaux = $CONNEXION -> listTableaux($_SESSION['SELECTED_GROUP'][0], $_SESSION['ASSESS_PERIOD'][0]);
        $id_tableaux = array2string($tableaux, 0);
        }
    $SQL = 'SELECT * FROM evaluations ';
    $SQL .= 'WHERE id_tableau IN ('.$id_tableaux.') ';
    $SQL .= 'AND id_eleve IN ('.$id_eleve.') ';
    if ($id_tableau != -2)
        $SQL .= 'AND id_item IN ('.$id_item.')';
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $evaluation) {
        $id_eleve = $evaluation['id_eleve'];
        $id_item = $evaluation['id_item'];
        $value = $evaluation['value'];
        if (! array_key_exists($id_eleve, $RESULT))
            $RESULT[$id_eleve] = array();
        if (! array_key_exists($id_item, $RESULT[$id_eleve]))
            $RESULT[$id_eleve][$id_item] = $value;
        else
            $RESULT[$id_eleve][$id_item] .= $value;
        }
    return $RESULT;
    }

function calcStats($CONNEXION, $id_tableau, $id_eleve, $id_item) {
    /*
    */
    $RESULT = array();

    //debugToConsole('id_tableau: '.$id_tableau);
    $id_tableaux = $id_tableau;
    // cas d'une compilation de tableaux :
    if ($id_tableau == -2) {
        $tableaux = $CONNEXION -> listTableaux($_SESSION['SELECTED_GROUP'][0], $_SESSION['ASSESS_PERIOD'][0]);
        $id_tableaux = array2string($tableaux, 0);
        }
    $SQL = 'SELECT * FROM evaluations ';
    $SQL .= 'WHERE id_tableau IN ('.$id_tableaux.') ';
    $SQL .= 'AND id_eleve IN ('.$id_eleve.') ';
    if ($id_tableau != -2)
        $SQL .= 'AND id_item IN ('.$id_item.')';
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    $TEMP_RESULT2 = array();
    foreach ($TEMP_RESULT as $evaluation) {
        $id_eleve = $evaluation['id_eleve'];
        $value = $evaluation['value'];
        if (! array_key_exists($id_eleve, $TEMP_RESULT2))
            $TEMP_RESULT2[$id_eleve] = $value;
        else
            $TEMP_RESULT2[$id_eleve] .= $value;
        }
    foreach ($TEMP_RESULT2 as $id_eleve => $value) {
        $RESULT[$id_eleve] = array();
        $total = strlen($value);
        // on sépare entre les 4 valeurs possibles :
        $values = value2List($value);
        foreach (array_reverse($values) as $value) {
            if ($value > 0)
                $RESULT[$id_eleve][] = array($value, '');
            else
                $RESULT[$id_eleve][] = array('', '');
            }
        // calcul des pourcentages :
        foreach (array_reverse($values) as $value) {
            if ($total > 0) {
                if (round(100.0 * $value / $total) > 0)
                    $RESULT[$id_eleve][] = array(round(100.0 * $value / $total), '');
                else
                    $RESULT[$id_eleve][] = array('', '');
                }
            else
                $RESULT[$id_eleve][] = array('', '');
            }
        // et on calcule la note :
        $note = '';
        if ($values[count($values) - 1] > 0) {
            // calcul du % de X :
            $pcX = $values[count($values) - 1] / $total;
            // on recentre les valeurs en fonction du % de X :
            $V2J = $values[0] * $pcX;
            $values[0] -= $V2J;
            $values[1] += $V2J;
            $R2O = $values[3] * $pcX;
            $values[3] -= $R2O;
            $values[2] += $R2O;
            // on recalcule le total sans tenir compte des X :
            $total = $values[0] + $values[1] + $values[2] + $values[3];
            }
        if ($total > 0) {
            $note = 0;
            for ($i=0; $i < count($values); $i++)
                $note += $values[$i] * $_SESSION['valeursEval'][$i] / 5;
            $note = $note / $total;
            $note = round($note, $_SESSION['precisionNotes']);
            }

        $RESULT[$id_eleve][] = array($note, '');
        if ($RESULT[$id_eleve][0][0] != '')
            $RESULT[$id_eleve][0][1] = LETTRE_X;
        if ($RESULT[$id_eleve][1][0] != '')
            $RESULT[$id_eleve][1][1] = LETTRE_D;
        if ($RESULT[$id_eleve][2][0] != '')
            $RESULT[$id_eleve][2][1] = LETTRE_C;
        if ($RESULT[$id_eleve][3][0] != '')
            $RESULT[$id_eleve][3][1] = LETTRE_B;
        if ($RESULT[$id_eleve][4][0] != '')
            $RESULT[$id_eleve][4][1] = LETTRE_A;
        if ($RESULT[$id_eleve][5][0] != '')
            $RESULT[$id_eleve][5][1] = LETTRE_X;
        if ($RESULT[$id_eleve][6][0] != '')
            $RESULT[$id_eleve][6][1] = LETTRE_D;
        if ($RESULT[$id_eleve][7][0] != '')
            $RESULT[$id_eleve][7][1] = LETTRE_C;
        if ($RESULT[$id_eleve][8][0] != '')
            $RESULT[$id_eleve][8][1] = LETTRE_B;
        if ($RESULT[$id_eleve][9][0] != '')
            $RESULT[$id_eleve][9][1] = LETTRE_A;
        }
    return $RESULT;
    }

function getItemsPeriods($CONNEXION, $id_groupe) {
    /*
    pour savoir le type d'organisation du prof (trimestre, année, mélange),
    on scrute un peu ses tableaux.
    retourne la proportion d'item en % pour chaque période.
    */
    $RESULT = array();
    $total =  0;
    $SQL = 'SELECT id FROM periodes ';
    $SQL .= 'ORDER BY id';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $p)
        $RESULT[$p['id']] = 0;
    // d'abord les tableaux qui utilisent des modèles :
    $SQL = 'SELECT tableaux.id_tableau, tableaux.Periode, ';
    $SQL .= 'tableau_item.id_item ';
    $SQL .= 'FROM tableaux ';
    $SQL .= 'JOIN tableau_item AS modele USING (id_tableau) ';
    $SQL .= 'JOIN tableau_item ON tableau_item.id_tableau=modele.id_item ';
    $SQL .= 'JOIN items ON items.id_item=tableau_item.id_item ';
    $SQL .= 'WHERE tableaux.Public=1 AND modele.id_item<0 ';
    $SQL .= 'AND tableaux.id_groupe='.$id_groupe;
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $itemPeriod) {
        $RESULT[$itemPeriod['Periode']] += 1;
        $total += 1;
        }
    // puis ceux qui n'utilisent pas de modèle :
    $SQL = 'SELECT tableaux.id_tableau, tableaux.Periode, ';
    $SQL .= 'tableau_item.id_item ';
    $SQL .= 'FROM tableaux ';
    $SQL .= 'JOIN tableau_item USING (id_tableau) ';
    $SQL .= 'WHERE tableaux.Public=1 AND tableau_item.id_item>-1 ';
    $SQL .= 'AND tableaux.id_groupe='.$id_groupe;
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $itemPeriod) {
        $RESULT[$itemPeriod['Periode']] += 1;
        $total += 1;
        }
    // arrivé là, $RESULT contient le nombre d'items pour chaque période
    // on le transforme en pourcentages :
    if ($total > 0)
        for ($p=0; $p < count($RESULT); $p++)
            $RESULT[$p] = round(100 * $RESULT[$p] / $total);
    return $RESULT;
    }

function getBilansValues(
        $CONNEXION, $id_groupe, $id_selection, $id_students, $id_bilans, 
        $annual=False, 
        $studentsProfils=array(), $bilansProfils=array(), $bulletin=array()) {
    /*
    calcul des valeurs à afficher.
    En cas de bilan annuel, on doit tenir compte des profils (car un bilan peut être évalué
    mais pas mis sur le bulletin chaque période)
    et on applique l'algorithme de calcul selon le type de prof.
    itemsPeriods donne la répartition des items selon les périodes
    et permet de choisir l'algorithme de calcul.
    */
    $RESULT = array();
    if ($annual)
        $itemsPeriods = getItemsPeriods($CONNEXION, $id_groupe);
    $SQL = 'SELECT * FROM evaluations ';
    $SQL .= 'WHERE id_tableau IN ('.$id_selection.') ';
    $SQL .= 'AND id_eleve IN ('.$id_students.') ';
    $SQL .= 'AND id_bilan IN ('.$id_bilans.') ';
    $SQL .= 'ORDER BY id_tableau';
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $evaluation) {
        $id_eleve = $evaluation['id_eleve'];
        $id_bilan = $evaluation['id_bilan'];
        $value = $evaluation['value'];
        if (! array_key_exists($id_eleve, $RESULT))
            $RESULT[$id_eleve] = array();
        if ($annual) {
            // on récupère la période :
            $id_tableau = $evaluation['id_tableau'];
            $p = $CONNEXION -> selection2pg($id_tableau);
            $p = $p[0];
            if (! array_key_exists($id_bilan, $RESULT[$id_eleve])) {
                $RESULT[$id_eleve][$id_bilan] = array();
                for ($i=0; $i < count($itemsPeriods) - 1; $i++)
                    $RESULT[$id_eleve][$id_bilan][$i] = '';
                }
            // on vérifie les profils pour les bilans persos :
            $mustDo = False;
            $studentProfil = $studentsProfils[$id_eleve][$p];
            if (in_array($id_bilan, $bulletin))
                $mustDo = True;
            elseif (array_key_exists($id_bilan, $bilansProfils)) {
                if (in_array($studentProfil, $bilansProfils[$id_bilan]))
                    $mustDo = True;
                }
            if ($mustDo)
                $RESULT[$id_eleve][$id_bilan][$p] = $value;
            }
        else
            $RESULT[$id_eleve][$id_bilan] = $value;
        }
    if ($annual) {
        foreach ($RESULT as $id_eleve => $RESULT_eleve)
            foreach ($RESULT_eleve as $id_bilan => $RESULT_eleve_bilan) {
                $value = '';
                if ($itemsPeriods[0] > 0) {
                    // on coefficiente en fonction de itemsPeriods :
                    for ($p=0; $p < count($itemsPeriods); $p++)
                        if (array_key_exists($p, $RESULT[$id_eleve][$id_bilan])) {
                            $v = $RESULT[$id_eleve][$id_bilan][$p];
                            if ($v != '')
                                for ($i=0; $i < $itemsPeriods[$p]; $i++)
                                    $value .= $v;
                            }
                    }
                else {
                    for ($p=1; $p < count($itemsPeriods); $p++)
                        if (array_key_exists($p, $RESULT[$id_eleve][$id_bilan])) {
                            $v = $RESULT[$id_eleve][$id_bilan][$p];
                            if ($v != '')
                                $value .= $v;
                            }
                    }
                $value = moyenneItem($value);
                $RESULT[$id_eleve][$id_bilan] = $value;
            }
        }
    return $RESULT;
    }

function getMoyennesAppreciations(
        $CONNEXION, $id_groupe, $period, $id_students) {
    /*
    */
    $RESULT = array();
    // on récupère la matière liée au groupe :
    $matiere = '';
    $SQL = 'SELECT * FROM groupes WHERE id_groupe='.$id_groupe;
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $group)
        $matiere = $group['Matiere'];
    // on ajoute l'id du groupe transformé (comme dans les tables) à la liste des élèves :
    $id_groupeInTables = - $id_groupe - 1;
    if ($id_students == '')
        $id_students = $id_groupeInTables;
    else
        $id_students .= ','.$id_groupeInTables;
    // récupération des notes moyennes :
    $SQL = 'SELECT * FROM notes_values ';
    $SQL .= 'WHERE id_groupe=:id_groupe ';
    $SQL .= 'AND Periode=:period ';
    $SQL .= 'AND id_note=-1';
    $OPT = array(':id_groupe' => $id_groupe, ':period' => $period);
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute($OPT);
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $moyenne)
        $RESULT[$moyenne['id_eleve']] = array(
            'moyenne' => $moyenne['value'], 'appreciation' => '');
    // récupération des appréciations :
    $SQL = 'SELECT * FROM appreciations ';
    $SQL .= 'WHERE id_eleve IN ('.$id_students.') ';
    $SQL .= 'AND Matiere="'.$matiere.'" ';
    $SQL .= 'AND Periode='.$period;
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $appreciation) {
        if (array_key_exists($appreciation['id_eleve'], $RESULT))
            $RESULT[$appreciation['id_eleve']]['appreciation'] = $appreciation['value'];
        else
            $RESULT[$appreciation['id_eleve']] = array(
                'moyenne' => '', 'appreciation' => $appreciation['value']);
        }
    return $RESULT;
    }

function getGroupSubject($CONNEXION, $id_groupe) {
    /*
    rtourne la matière liée au groupe
    */
    $RESULT = '';
    $SQL = 'SELECT * FROM groupes WHERE id_groupe='.$id_groupe;
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $group)
        $RESULT = $group['Matiere'];
    return $RESULT;
    }

function classesTypesNames($CONNEXION) {
    /*
    retourne la liste des classesTypes et les noms qui leur correspondent.
    Utilisé pour les indications au survol de la souris.
    */
    $RESULT = array();
    $SQL = 'SELECT * FROM classestypes';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $classType)
        $RESULT[$classType['id']] = $classType['name'];
    $RESULT[-2] = tr('nowhere');
    $RESULT[-1] = tr('everywhere');
    return $RESULT;
    }

function getStudentsClasseTypesAndNotes($CONNEXION, $id_eleve) {
    /*
    retourne la liste des classeType et s'il y a des notes
    pour une liste d'élèves.
    */
    $RESULT = array();
    $RESULT['GROUP'] = array(array(), False);
    // on récupère les classeType des classes dans la base commun :
    $CLASSES = array();
    $SQL = 'SELECT * FROM classes';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $class) {
        if ($class['notes'] == 1)
            $notes = True;
        else
            $notes = False;
        $CLASSES[$class['Classe']] = array($class['classeType'], $notes);
        }
    // puis les classes des élèves dans la base users :
    $SQL = 'SELECT * FROM eleves ';
    $SQL .= 'WHERE id IN ('.$id_eleve.')';
    $STMT = $CONNEXION -> DB_USERS -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $student) {
        $id_eleve = $student['id'];
        $className = $student['Classe'];
        $RESULT[$id_eleve] = $CLASSES[$className];
        if (! in_array($CLASSES[$className][0], $RESULT['GROUP'][0]))
            $RESULT['GROUP'][0][] = $CLASSES[$className][0];
        if ($CLASSES[$className][1])
            $RESULT['GROUP'][1] = True;
        }
    return $RESULT;
    }

function getStudentsAndGroupProfils(
        $CONNEXION, $students, $periods, $id_groupe, $matiere='') {
    /*
    retourne la liste des profils valables pour les élèves, 
    les périodes et le groupe indiqués.
    */
    $RESULT = array();
    $RESULT['STUDENTS'] = array();
    $RESULT['GROUP'] = array();
    $RESULT['DEFAULT'] = -999;
    $RESULT['ALL'] = array();
    // on récupère la matière liée au groupe :
    if ($matiere == '') {
        $SQL = 'SELECT * FROM groupes WHERE id_groupe='.$id_groupe;
        $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
        $STMT -> execute();
        $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($TEMP_RESULT as $group)
            $matiere = $group['Matiere'];
        }
    // on cherche dans les tables profils et profil_who :
    $SQL = 'SELECT DISTINCT profils.*, profil_who.* ';
    $SQL .= 'FROM profils ';
    $SQL .= 'LEFT JOIN profil_who USING (id_profil) ';
    $SQL .= 'WHERE ';
    $SQL .= '   profils.Matiere="'.$matiere.'" ';
    $SQL .= 'AND ( ';
    $SQL .= '       (profils.profilType="STUDENTS" ';
    $SQL .= '       AND profil_who.id_who IN ('.$students.') ';
    $SQL .= '       AND profil_who.Periode IN (0, '.$periods.')) ';
    $SQL .= '   OR ';
    $SQL .= '       (profils.profilType="GROUPS" ';
    $SQL .= '       AND profil_who.id_who='.$id_groupe.' ';
    $SQL .= '       AND profil_who.Periode IN (0, '.$periods.')) ';
    $SQL .= '   OR ';
    $SQL .= '       (profils.id_profil<0) ';
    $SQL .= '   )';
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $profil) {
        $id_profil = $profil[0];
        $profilType = $profil[3];
        if ($id_profil < 0) {
            // profil par défaut de la matière :
            $RESULT['DEFAULT'] = $id_profil;
            }
        elseif ($profilType == 'STUDENTS') {
            // profil attribué à un élève :
            $id_eleve = $profil[4];
            $period = $profil[6];
            if (! array_key_exists($id_eleve, $RESULT['STUDENTS']))
                $RESULT['STUDENTS'][$id_eleve] = array();
            if (! array_key_exists($period, $RESULT['STUDENTS'][$id_eleve]))
                $RESULT['STUDENTS'][$id_eleve][$period] = $id_profil;
            // on l'ajoute à la liste de tous les profils :
            if (! in_array($id_profil, $RESULT['ALL']))
                $RESULT['ALL'][] = $id_profil;
            }
        else {
            // profil standard du groupe pour cette période :
            $period = $profil[6];
            $RESULT['GROUP'][$period] = $id_profil;
            // on l'ajoute à la liste de tous les profils :
            if (! in_array($id_profil, $RESULT['ALL']))
                $RESULT['ALL'][] = $id_profil;
            }
        }
    // on transforme students et periods en tableaux :
    $students = string2array($students);
    $periods = string2array($periods);
    if (! in_array(0, $periods))
        $periods[] = 0;
    // on complète les profils des élèves.
    // Chaque élève aura un profil pour chaque période.
    // S'il n'en avait pas un d'attribué,
    // on lui met le profil standard du groupe
    // ou le profil par défaut (s'il n'y a pas de profil standard) :
    $useDefaultProfil = False;
    foreach ($students as $id_eleve) {
        if (! array_key_exists($id_eleve, $RESULT['STUDENTS']))
            $RESULT['STUDENTS'][$id_eleve] = array();
        foreach ($periods as $period)
            if (! array_key_exists($period, $RESULT['STUDENTS'][$id_eleve]))
                if (array_key_exists($period, $RESULT['GROUP'])) {
                    $id_profil = $RESULT['GROUP'][$period];
                    $RESULT['STUDENTS'][$id_eleve][$period] = $id_profil;
                    }
        if (count($RESULT['STUDENTS'][$id_eleve]) < 1) {
            $useDefaultProfil = True;
            $id_profil = $RESULT['DEFAULT'];
            foreach ($periods as $period)
                $RESULT['STUDENTS'][$id_eleve][$period] = $id_profil;
            }
        // si le profil par défaut est un vrai profil,
        // on l'ajoute à la liste de tous les profils :
        if ($useDefaultProfil == True)
            if ($id_profil != -999)
                if (! in_array($id_profil, $RESULT['ALL']))
                    $RESULT['ALL'][] = $id_profil;
        }
    return $RESULT;
    }

function getBilansProfils($CONNEXION) {
    /*
    retourne la liste des profils valables pour chaque bilan.
    */
    $RESULT = array();
    $SQL = 'SELECT * FROM profil_bilan_BLT';
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute();
    $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($TEMP_RESULT as $profilBilan) {
        if (! array_key_exists($profilBilan['id_bilan'], $RESULT))
            $RESULT[$profilBilan['id_bilan']] = array($profilBilan['id_profil']);
        elseif (! in_array($profilBilan['id_profil'], $RESULT[$profilBilan['id_bilan']]))
            $RESULT[$profilBilan['id_bilan']][] = $profilBilan['id_profil'];
        }
    return $RESULT;
    }

function addJavascripVariables4Editing() {
    // on prépare le regex pour l'évaluation des items et le message d'alerte
    $RESULT = '';
    $regex = 'var compare = /^[';
    $alertMessage = 'var alertMessage="'.tr('INCORRECT ENTRY!').' \n\n '.tr('Use the keys: ').' ';
    $horaireMessage = 'var horaireMessage="'.tr('Select horaire before evaluate!').'";';
    $letters = '';
    $specialsChars = array('\\', '.', '$', '[', ']', ')', ')', '{', '}', '^', '?', '*', '+', '-');
    foreach (getArrayEvals() as $index => $letter) {
        $letters .= 'var letter_'.$index.' = "'.$letter.'";';
        $alertMessage .= strtoupper($letter).', ';
        if (in_array($letter, $specialsChars))
            $regex .= '\\'.$letter;
        else
            $regex.= strtoupper($letter);
        }
    $regex.= ']{0,50}$/i;';
    $alertMessage = substr($alertMessage, 0, -2);
    $alertMessage .= '.";';

    $RESULT .= spaces(2).'<script type="text/javascript">'."\n";
    $RESULT .= spaces(3).'// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later'."\n";
    $RESULT .= spaces(3).$letters."\n";
    $RESULT .= spaces(3).$regex."\n";
    $RESULT .= spaces(3).$alertMessage."\n";
    $RESULT .= spaces(3).$horaireMessage."\n";
    $RESULT .= spaces(3).'// @license-end'."\n";
    $RESULT .= spaces(2).'</script>'."\n";
    return $RESULT;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];

// on prépare les paramètres de calcul du prof
defineCalculsParameters($CONNEXION);

# couleurs d'affichages des noms des bilans selon leur genre :
$colors = array(
    'BULLETIN' => '#00006a',
    'REFERENTIEL' => '#008a17',
    'CONFIDENTIEL' => '#ed56af',
    'PERSO_BLT' => '#5a0aed',
    'PERSO' => '#000000');

$nothing = '&nbsp;';
$studentNameWidth = 250;
$editMessage = tr('Click to edit.');



/****************************************************
    CONTENU DE LA PAGE
    ET TITRE EN CAS D'IMPRESSION
****************************************************/
$pageContent = '';
$printTitle = '';





























if ($_SESSION['SELECTED_VUE'][0] == VUE_ITEM) {
    $printTitle = $_SESSION['ASSESS_PERIOD'][1].' - '.$_SESSION['SELECTED_GROUP'][1];
    $printTitle .= ' - '.$_SESSION['SELECTED_TABLEAU'][1].' - '.$_SESSION['SELECTED_VUE'][1];
    if (VERSION_NAME == 'demo')
        $pageContent .= notInDemoMessage();
    // cas d'une compilation de tableaux :
    $_SESSION['ASSESS_TABLEAUX_FOR_ITEM'] = array();
    if ($_SESSION['SELECTED_TABLEAU'][0] == -2) {
        // liste des tableaux :
        $tableaux = $CONNEXION -> listTableaux($_SESSION['SELECTED_GROUP'][0], $_SESSION['ASSESS_PERIOD'][0]);
        // liste des élèves :
        $students = $CONNEXION -> listStudentsInGroup($_SESSION['SELECTED_GROUP'][0]);
        // récupération des items :
        $items = array();
        $id_items = array();
        foreach ($tableaux as $tableau) {
            // informations liées au tableau :
            $tableauInfos = $CONNEXION -> tableauInfos($tableau[0]);
            $id_template = $tableauInfos['id_template'];
            // récupération des items :
            $itemsInTableau = $CONNEXION -> tableauItems($tableau[0], $id_template);
            foreach ($itemsInTableau as $item) {
                if (! array_key_exists($item['id_item'], $_SESSION['ASSESS_TABLEAUX_FOR_ITEM']))
                    $_SESSION['ASSESS_TABLEAUX_FOR_ITEM'][$item['id_item']] = array();
                $_SESSION['ASSESS_TABLEAUX_FOR_ITEM'][$item['id_item']][] = $tableau[0];
                if (! in_array($item['id_item'], $id_items)) {
                    $id_items[] = $item['id_item'];
                    $items[] = array(
                        'id_tableau' => $tableau[0], 
                        'id_item' => $item['id_item'], 
                        'ordre' => $item['ordre'], 
                        'Name' => $item['Name'], 
                        'Label' => $item['Label']);
                    }
                }
            }
        }
    else {
        // informations liées au tableau :
        $tableauInfos = $CONNEXION -> tableauInfos($_SESSION['SELECTED_TABLEAU'][0]);
        $id_template = $tableauInfos['id_template'];
        // liste des élèves :
        $students = $CONNEXION -> listStudentsInGroup($tableauInfos['id_groupe']);
        // récupération des items :
        $items = $CONNEXION -> tableauItems($_SESSION['SELECTED_TABLEAU'][0], $id_template);
        }
    //debugToConsole($items);
    $id_students = array2string($students, 0);
    $itemsText = array2string($items, 'id_item');
    // on récupère les évaluations :
    $data = calcItems($CONNEXION, $_SESSION['SELECTED_TABLEAU'][0], $id_students, $itemsText);

    // on peut créer le tableau :
    $table = '';
    $table .= spaces(2).'<div id="fixedTable" class="fixedTable">'."\n";
    $table .= spaces(3).'<table>'."\n";
    $table .= spaces(4).'<thead>'."\n";
    $table .= spaces(5).'<tr>'."\n";
    $table .= spaces(6).'<th class="verticalTitle"></th>'."\n";
    foreach ($items as $item) {
        $toolTip = $item['Name'].' :&#10;'.$item['Label'];
        $table .= spaces(6).'<th class="bigWidth verticalTitle"'
            .' title="'.$toolTip.'">'
            .'<div class="verticalBig"><b>'.$item['Name'].'</b></div>'
            .'</th>'."\n";
        }
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</thead>'."\n";
    $table .= spaces(4).'<tbody>'."\n";
    $background = '';
    foreach ($students as $student) {
        $table .= spaces(5).'<tr>'."\n";
        // le nom :
        if ($background == 'gray')
            $background = 'white';
        else
            $background = 'gray';
        $table .= spaces(6).'<td class="'.$background.'">'.$student[1].'</td>'."\n";
        // les résultats :
        foreach ($items as $item) {
            $value = '';
            if (array_key_exists($student[0], $data))
                if (array_key_exists($item['id_item'], $data[$student[0]]))
                    $value = $data[$student[0]][$item['id_item']];
            $class = getItemClass($CONNEXION, $value);
            $value = valeur2affichage($value);
            if ($value == '')
                $value = $nothing;
            if ($_SESSION['SELECTED_TABLEAU'][0] == -2)
                $editData = 'vue="items" id_eleve="'.$student[0]
                    .'" id_item="'.$item['id_item']
                    .'" id_tableau="'.$item['id_tableau'].'"';
            else
                $editData = 'vue="items" id_eleve="'.$student[0]
                    .'" id_item="'.$item['id_item']
                    .'" id_tableau="'.$_SESSION['SELECTED_TABLEAU'][0].'"';
            $table .= spaces(6).'<td class="'.$class.' edit_eval" '.$editData
                .' title="'.$editMessage.'">'.$value.'</td>'."\n";
            }

        $table .= spaces(5).'</tr>'."\n";
        }
    $table .= spaces(4).'</tbody>'."\n";
    $table .= spaces(3).'</table>'."\n";
    $table .= spaces(2).'</div>'."\n";
    $pageContent .= $table;
    }





























elseif ($_SESSION['SELECTED_VUE'][0] == VUE_STATISTIQUES) {
    $printTitle = $_SESSION['ASSESS_PERIOD'][1].' - '.$_SESSION['SELECTED_GROUP'][1];
    $printTitle .= ' - '.$_SESSION['SELECTED_TABLEAU'][1].' - '.$_SESSION['SELECTED_VUE'][1];
    // informations liées au tableau :
    $tableauInfos = $CONNEXION -> tableauInfos($_SESSION['SELECTED_TABLEAU'][0]);
    $id_template = $tableauInfos['id_template'];
    // liste des élèves :
    if ($_SESSION['SELECTED_TABLEAU'][0] == -2)
        $students = $CONNEXION -> listStudentsInGroup($_SESSION['SELECTED_GROUP'][0]);
    else
        $students = $CONNEXION -> listStudentsInGroup($tableauInfos['id_groupe']);
    $id_students = array2string($students, 0);
    // récupération des items :
    $items = $CONNEXION -> tableauItems($_SESSION['SELECTED_TABLEAU'][0], $id_template);
    $itemsText = array2string($items, 'id_item');
    // on récupère les évaluations :
    $data = calcStats($CONNEXION, $_SESSION['SELECTED_TABLEAU'][0], $id_students, $itemsText);
    // les titres des colonnes :
    $columns = array();
    foreach (array_reverse(getArrayLetters()) as $letter) {
        $letter = valeur2affichage($letter);
        $columns[] = array($letter, tr('Number of items').' ('.$letter.')');
        }
    foreach (array_reverse(getArrayLetters()) as $letter) {
        $letter = valeur2affichage($letter);
        $columns[] = array($letter.' %', tr('Percentage of items').' ('.$letter.')');
        }
    $columns[] = array(tr('Notes'), tr('Note calculated from items'));

    // on peut créer le tableau :
    $table = '';
    $table .= spaces(2).'<div id="fixedTable" class="fixedTable">'."\n";
    $table .= spaces(3).'<table>'."\n";
    $table .= spaces(4).'<thead>'."\n";
    $table .= spaces(5).'<tr>'."\n";
    $table .= spaces(6).'<th class="horizontalTitle"></th>'."\n";
    foreach ($columns as $column)
        $table .= spaces(6).'<th class="mediumWidth" title="'.$column[1].'"><b>'.$column[0].'</b></th>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</thead>'."\n";
    $table .= spaces(4).'<tbody>'."\n";
    $background = '';
    foreach ($students as $student) {
        $table .= spaces(5).'<tr>'."\n";
        // le nom :
        if ($background == 'gray')
            $background = 'white';
        else
            $background = 'gray';
        $table .= spaces(6).'<td class="'.$background.'">'.$student[1].'</td>'."\n";
        // les résultats :
        $i = 0;
        foreach ($columns as $column) {
            $value = '';
            $class = '';
            if (array_key_exists($student[0], $data)) {
                $value = $data[$student[0]][$i][0];
                $class = getItemClass($CONNEXION, $data[$student[0]][$i][1]);
                }
            $value = valeur2affichage($value);
            $value = str_replace('.', ',', $value);
            if ($value == '')
                $value = $nothing;
            $table .= spaces(6).'<td class="'.$class.'">'.$value.'</td>'."\n";
            $i++;
            }
        $table .= spaces(5).'</tr>'."\n";
        }
    $table .= spaces(4).'</tbody>'."\n";
    $table .= spaces(3).'</table>'."\n";
    $table .= spaces(2).'</div>'."\n";
    $pageContent .= $table;
    }





























elseif ($_SESSION['SELECTED_VUE'][0] == VUE_BILAN) {
    $printTitle = $_SESSION['ASSESS_PERIOD'][1].' - '.$_SESSION['SELECTED_GROUP'][1];
    $printTitle .= ' - '.$_SESSION['SELECTED_VUE'][1];
    # on calcule le faux id_tableau (id_selection) pour lire les données :
    $pg2selection = $CONNEXION -> pg2selection(
        $_SESSION['ASSESS_PERIOD'][0], $_SESSION['SELECTED_GROUP'][0]);
    $periodes = array($_SESSION['ASSESS_PERIOD'][0], $pg2selection);
    // liste des élèves :
    $students = $CONNEXION -> listStudentsInGroup($_SESSION['SELECTED_GROUP'][0]);
    $id_students = array2string($students, 0);
    // récupération des profils des différents élèves :
    $studentsAndGroupProfils = getStudentsAndGroupProfils(
        $CONNEXION, $id_students, $periodes[0], $_SESSION['SELECTED_GROUP'][0]);
    // et de leurs classeTypes :
    $studentsClasseTypesNotes = getStudentsClasseTypesAndNotes($CONNEXION, $id_students);
    // récupération des bilans liés aux différents profils :
    $bilansProfils = getBilansProfils($CONNEXION);
    // récupération des bilans à afficher :
    $bilans = $CONNEXION -> getGroupBilans(
        $_SESSION['SELECTED_GROUP'][0], $periodes,
        $groupClassTypes=$studentsClasseTypesNotes['GROUP'][0],
        $profils=$studentsAndGroupProfils['ALL'],
        $bilansProfils=$bilansProfils);
    // récupération des bilans :
    $id_bilans = '';
    foreach ($bilans['ORDER'] as $bilan) {
        if ($id_bilans == '')
            $id_bilans = $bilans[$bilan]['id_bilan'];
        else
            $id_bilans .= ', '.$bilans[$bilan]['id_bilan'];
        }
    // on récupère les évaluations :
    $data = getBilansValues(
        $CONNEXION, $_SESSION['SELECTED_GROUP'][0], $periodes[1], $id_students, $id_bilans);
    $columnsCount = count($bilans);
    $classesTypesNames = classesTypesNames($CONNEXION);

    // on peut créer le tableau :
    $table = '';
    $table .= spaces(2).'<div id="fixedTable" class="fixedTable">'."\n";
    $table .= spaces(3).'<table>'."\n";
    $table .= spaces(4).'<thead>'."\n";
    $table .= spaces(5).'<tr>'."\n";
    $table .= spaces(6).'<th class="verticalTitle"></th>'."\n";
    $table .= spaces(6).'<th class="separator"></th>'."\n";
    $lastBilanType = '';
    foreach ($bilans as $name => $bilan)
        if ($name != 'ORDER') {
            if ($bilan['type'] != $lastBilanType) {
                // on ajoute la colonne séparateur :
                if ($lastBilanType != '') {
                    $table .= spaces(6).'<th class="separator"></th>'."\n";
                    $columnsCount += 1;
                    }
                $lastBilanType = $bilan['type'];
                }
            $bilanTitle = $bilan['Label'];
            foreach ($bilan['classeTypes'] as $classeType)
                $bilanTitle .= ' ('.$classesTypesNames[$classeType].')';
            $toolTip = $bilan['Name'].' :&#10;'.$bilanTitle;
            $table .= spaces(6).'<th class="smallWidth verticalTitle"'
                .' style="color:'.$colors[$bilan['type']].';" title="'.$toolTip.'">'
                .'<div class="verticalSmall"><b>'.$bilan['Name'].'</b></div>'
                .'</th>'."\n";
            }
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</thead>'."\n";
    $table .= spaces(4).'<tbody>'."\n";
    $background = '';
    foreach ($students as $student) {
        $table .= spaces(5).'<tr>'."\n";
        // le nom :
        if ($background == 'gray')
            $background = 'white';
        else
            $background = 'gray';
        $table .= spaces(6).'<td class="'.$background.'">'.$student[1].'</td>'."\n";
        $table .= spaces(6).'<td class="separator"></td>'."\n";
        // les résultats :
        $lastBilanType = '';
        foreach ($bilans as $name => $bilan)
            if ($name != 'ORDER') {
                $bilanTitle = '';
                if ($bilan['type'] != $lastBilanType) {
                    // on ajoute la colonne séparateur :
                    if ($lastBilanType != '')
                        $table .= spaces(6).'<td class="separator"></td>'."\n";
                    $lastBilanType = $bilan['type'];
                    }
                $bilanTitle = $bilan['Label'];
                foreach ($bilan['classeTypes'] as $classeType)
                    $bilanTitle .= ' ('.$classesTypesNames[$classeType].')';
                $value = '';
                if (array_key_exists($student[0], $data))
                    if (array_key_exists($bilan['id_bilan'], $data[$student[0]]))
                        $value = $data[$student[0]][$bilan['id_bilan']];
                $class = getItemClass($CONNEXION, $value);
                $value = valeur2affichage($value);
                if ($value == '')
                    $value = $nothing;
                $toolTip = $bilan['Name'].' :&#10;'.$bilanTitle;
                $table .= spaces(6).'<td class="'.$class.'" title="'.$toolTip.'">'.$value.'</td>'."\n";
                }
        $table .= spaces(5).'</tr>'."\n";
        }
    // on ajoute la ligne du groupe :
    $table .= spaces(5).'<tr>'."\n";
    for ($i=0; $i < $columnsCount + 1; $i++)
        $table .= spaces(6).'<td class="interligne"></td>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(5).'<tr>'."\n";
    $table .= spaces(6).'<td class="'.$background.'"><b>'.tr('GROUP').'</b></td>'."\n";
    $table .= spaces(6).'<td class="separator"></td>'."\n";
    $lastBilanType = '';
    foreach ($bilans as $name => $bilan)
        if ($name != 'ORDER') {
            if ($bilan['type'] != $lastBilanType) {
                // on ajoute la colonne séparateur :
                if ($lastBilanType != '')
                    $table .= spaces(6).'<td class="separator"></td>'."\n";
                $lastBilanType = $bilan['type'];
                }
            $value = $bilan['value'];
            $class = getItemClass($CONNEXION, $value);
            $value = valeur2affichage($value);
            if ($value == '')
                $value = $nothing;
            $table .= spaces(6).'<td class="'.$class.'">'.$value.'</td>'."\n";
            }
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</tbody>'."\n";
    $table .= spaces(3).'</table>'."\n";
    $table .= spaces(2).'</div>'."\n";
    $pageContent .= $table;
    }





























elseif ($_SESSION['SELECTED_VUE'][0] == VUE_BULLETIN) {
    $printTitle = $_SESSION['ASSESS_PERIOD'][1].' - '.$_SESSION['SELECTED_GROUP'][1];
    $printTitle .= ' - '.$_SESSION['SELECTED_VUE'][1];
    if (VERSION_NAME == 'demo')
        $pageContent .= notInDemoMessage();
    // on distingue la période "bilan annuel" des autres :
    $annual = ($_SESSION['ASSESS_PERIOD'][0] == 999) ? True : False;
    if ($annual) {
        $SQL = 'SELECT * FROM periodes ORDER BY id';
        $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
        $STMT -> execute();
        $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
        $periodes = array();
        foreach ($tempResult as $period) {
            $p = $period['id'];
            $s = $CONNEXION -> pg2selection($p, $_SESSION['SELECTED_GROUP'][0]);
            $periodes[] = array($p, $s);
            }
        $periodes = array(array2string($periodes, 0), array2string($periodes, 1));
        }
    else {
        $s = $CONNEXION -> pg2selection(
            $_SESSION['ASSESS_PERIOD'][0], $_SESSION['SELECTED_GROUP'][0]);
        $periodes = array($_SESSION['ASSESS_PERIOD'][0], $s);
        }
    // liste des élèves :
    $students = $CONNEXION -> listStudentsInGroup($_SESSION['SELECTED_GROUP'][0], $old=-2);
    $id_students = array2string($students, 0);
    // récupération des profils des différents élèves :
    $studentsAndGroupProfils = getStudentsAndGroupProfils(
        $CONNEXION, $id_students, $periodes[0], $_SESSION['SELECTED_GROUP'][0]);
    // et de leurs classeTypes :
    $studentsClasseTypesNotes = getStudentsClasseTypesAndNotes($CONNEXION, $id_students);
    // récupération des bilans liés aux différents profils :
    $bilansProfils = getBilansProfils($CONNEXION);
    // récupération des bilans à afficher :
    $bilans = $CONNEXION -> getGroupBilans(
        $_SESSION['SELECTED_GROUP'][0], $periodes,
        $groupClassTypes=$studentsClasseTypesNotes['GROUP'][0],
        $profils=$studentsAndGroupProfils['ALL'],
        $bilansProfils=$bilansProfils,
        $onlyBulletin=True);
    $id_bilans = '';
    foreach ($bilans['ORDER'] as $bilan) {
        if ($id_bilans == '')
            $id_bilans = $bilans[$bilan]['id_bilan'];
        else
            $id_bilans .= ', '.$bilans[$bilan]['id_bilan'];
        }
    // liste des bilans de la partie partagée du bulletin :
    $bulletin = array();
    if ($annual) {
        foreach ($bilans['ORDER'] as $bilan)
            if ($bilans[$bilan]['type'] == 'BULLETIN')
                $bulletin[] = $bilans[$bilan]['id_bilan'];
        }
    // on récupère les évaluations :
    $data = getBilansValues(
        $CONNEXION, $_SESSION['SELECTED_GROUP'][0], $periodes[1], $id_students, $id_bilans, 
        $annual,
        $studentsProfils=$studentsAndGroupProfils['STUDENTS'], 
        $bilansProfils=$bilansProfils, 
        $bulletin=$bulletin);
    $data2 = getMoyennesAppreciations(
        $CONNEXION, $_SESSION['SELECTED_GROUP'][0], $_SESSION['ASSESS_PERIOD'][0], $id_students);
    $subject = getGroupSubject($CONNEXION, $_SESSION['SELECTED_GROUP'][0]);
    $columnsCount = count($bilans) + 1;
    $lastBilanType = '';
    foreach ($bilans as $name => $bilan)
        if ($name != 'ORDER')
            if ($bilan['type'] != $lastBilanType) {
                // on ajoute la colonne séparateur :
                if ($lastBilanType != '')
                    $columnsCount += 1;
                $lastBilanType = $bilan['type'];
                }
    $notes = False;
    foreach ($studentsClasseTypesNotes as $studentClasseTypesNotes)
        if ($studentClasseTypesNotes[1])
            $notes = True;
    if ($notes)
        $columnsCount += 2;
    $classesTypesNames = classesTypesNames($CONNEXION);

    // on peut créer le tableau :
    $table = '';
    $table .= spaces(2).'<div id="fixedTable" class="fixedTable">'."\n";
    $table .= spaces(3).'<table>'."\n";
    $table .= spaces(4).'<thead>'."\n";
    $table .= spaces(5).'<tr>'."\n";
    $table .= spaces(6).'<th class="verticalTitle"></th>'."\n";
    $lastBilanType = '';
    foreach ($bilans as $name => $bilan)
        if ($name != 'ORDER') {
            if ($bilan['type'] != $lastBilanType) {
                // on ajoute la colonne séparateur :
                if ($lastBilanType != '')
                    $table .= spaces(6).'<th class="separator"></th>'."\n";
                $lastBilanType = $bilan['type'];
                }
            $bilanTitle = $bilan['Label'];
            foreach ($bilan['classeTypes'] as $classeType)
                $bilanTitle .= ' ('.$classesTypesNames[$classeType].')';
            $toolTip = $bilan['Name'].' :&#10;'.$bilanTitle;
            $table .= spaces(6).'<th class="smallWidth verticalTitle"'
                .' style="color:'.$colors[$bilan['type']].';" title="'.$toolTip.'">'
                .'<div class="verticalSmall"><b>'
                .$bilan['Name']
                .'</b></div>'
                .'</th>'."\n";
            }
    if ($notes) {
        $table .= spaces(6).'<th class="separator"></th>'."\n";
        $table .= spaces(6).'<th class="mediumWidth verticalTitle">'
            .'<div class="verticalMedium"><b>'.tr('Average').'</b></div></th>'."\n";
        }
    $table .= spaces(6).'<th class="separator"></th>'."\n";
    $table .= spaces(6).'<th class="appreciationTitle verticalTitle"><b><br ∕>'.tr('APPRECIATION').'</b></th>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</thead>'."\n";
    $table .= spaces(4).'<tbody>'."\n";
    $background = '';
    foreach ($students as $student) {
        $table .= spaces(5).'<tr class="appreciationColumn">'."\n";
        // le nom :
        if ($background == 'gray')
            $background = 'white';
        else
            $background = 'gray';
        $table .= spaces(6).'<td class="'.$background.' appreciationColumn">'.$student[1].'</td>'."\n";
        // les résultats :
        $lastBilanType = '';
        foreach ($bilans as $name => $bilan)
            if ($name != 'ORDER') {
                $bilanTitle = '';
                if ($bilan['type'] != $lastBilanType) {
                    // on ajoute la colonne séparateur :
                    if ($lastBilanType != '')
                        $table .= spaces(6).'<td class="separator"></td>'."\n";
                    $lastBilanType = $bilan['type'];
                    }
                $bilanTitle = $bilan['Label'];
                foreach ($bilan['classeTypes'] as $classeType)
                    $bilanTitle .= ' ('.$classesTypesNames[$classeType].')';
                $value = '';
                if (array_key_exists($student[0], $data))
                    if (array_key_exists($bilan['id_bilan'], $data[$student[0]]))
                        $value = $data[$student[0]][$bilan['id_bilan']];
                // on vérifie les profils :
                $mustDo = False;
                if ($bilan['type'] == 'PERSO_BLT') {
                    foreach ($studentsAndGroupProfils['STUDENTS'][$student[0]] as $studentProfil)
                        if (in_array($studentProfil, $bilansProfils[$bilan['id_bilan']]))
                            $mustDo = True;
                    }
                elseif (in_array(-1, $bilan['classeTypes']))
                    $mustDo = True;
                elseif (in_array($studentsClasseTypesNotes[$student[0]][0], $bilan['classeTypes']))
                    $mustDo = True;
                if ($mustDo != True)
                    $value = '';
                $class = getItemClass($CONNEXION, $value);
                $value = valeur2affichage($value);
                if ($value == '')
                    $value = $nothing;
                $toolTip = $bilan['Name'].' :&#10;'.$bilanTitle;
                $table .= spaces(6).'<td class="'.$class.'" title="'.$toolTip.'">'.$value.'</td>'."\n";
                }
        if ($notes) {
            $moyenne = '';
            if ($studentsClasseTypesNotes[$student[0]][1])
                if (array_key_exists($student[0], $data2))
                    $moyenne = $data2[$student[0]]['moyenne'];
            $moyenne = str_replace('.', ',', $moyenne);
            $table .= spaces(6).'<td class="separator"></td>'."\n";
            $table .= spaces(6).'<td>'.$moyenne.'</td>'."\n";
            }
        $appreciation = '';
        if (array_key_exists($student[0], $data2))
            $appreciation = nl2br($data2[$student[0]]['appreciation']);
        if ($appreciation == '')
            $appreciation = $nothing;
        $table .= spaces(6).'<td class="separator"></td>'."\n";
        $editData = 'vue="bulletin" id_eleve="'.$student[0].'" matiere="'.$subject
            .'" periode="'.$_SESSION['ASSESS_PERIOD'][0].'"';
        $table .= spaces(6).'<td class="appreciation edit_appreciation appreciationColumn" '.$editData
            .' title="'.$editMessage.'">'.$appreciation.'</td>'."\n";
        $table .= spaces(6).'</tr>'."\n";

        }
    // on ajoute la ligne du groupe :
    $table .= spaces(5).'<tr>'."\n";
    for ($i=0; $i < $columnsCount + 1; $i++)
        $table .= spaces(6).'<td class="interligne"></td>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(5).'<tr class="groupColumn">'."\n";
    $table .= spaces(6).'<td class="'.$background.' groupColumn"><b>'.tr('GROUP').'</b></td>'."\n";
    $lastBilanType = '';
    foreach ($bilans as $name => $bilan)
        if ($name != 'ORDER') {
            if ($bilan['type'] != $lastBilanType) {
                // on ajoute la colonne séparateur :
                if ($lastBilanType != '')
                    $table .= spaces(6).'<td class="separator"></td>'."\n";
                $lastBilanType = $bilan['type'];
                }
            $value = $bilan['value'];
            $class = getItemClass($CONNEXION, $value);
            $value = valeur2affichage($value);
            if ($value == '')
                $value = $nothing;
            $table .= spaces(6).'<td class="'.$class.'">'.$value.'</td>'."\n";
            }
    // moyenne et appréciation du groupe :
    $id_groupeInTables = - $_SESSION['SELECTED_GROUP'][0] - 1;
    if ($notes) {
        $table .= spaces(6).'<td class="separator"></td>'."\n";
        if (array_key_exists($id_groupeInTables, $data2)) {
            $moyenne = $data2[$id_groupeInTables]['moyenne'];
            $table .= spaces(6).'<td><b>'.$moyenne.'</b></td>'."\n";
            }
        else
            $table .= spaces(6).'<td></td>'."\n";
        }
    $appreciation = '';
    if (array_key_exists($id_groupeInTables, $data2))
        $appreciation = nl2br($data2[$id_groupeInTables]['appreciation']);
    if ($appreciation == '')
        $appreciation = $nothing;
    $table .= spaces(6).'<td class="separator"></td>'."\n";
    $editData = 'vue="bulletin" id_eleve="'.$id_groupeInTables.'" matiere="'.$subject
        .'" periode="'.$_SESSION['ASSESS_PERIOD'][0].'"';
    $table .= spaces(6).'<td class="appreciation edit_appreciation groupColumn" '.$editData
        .' title="'.$editMessage.'"><b>'.$appreciation.'</b></td>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</tbody>'."\n";
    $table .= spaces(3).'</table>'."\n";
    $table .= spaces(2).'</div>'."\n";
    $pageContent .= $table;
    }




























elseif ($_SESSION['SELECTED_VUE'][0] == VUE_APPRECIATIONS) {
    $printTitle = $_SESSION['ASSESS_PERIOD'][1].' - '.$_SESSION['SELECTED_GROUP'][1];
    $printTitle .= ' - '.$_SESSION['SELECTED_VUE'][1];
    if (VERSION_NAME == 'demo')
        $pageContent .= notInDemoMessage();
    // liste des élèves :
    $students = $CONNEXION -> listStudentsInGroup($_SESSION['SELECTED_GROUP'][0]);
    $id_students = array2string($students, 0);
    // on récupère les appréciations :
    $data2 = getMoyennesAppreciations(
        $CONNEXION, $_SESSION['SELECTED_GROUP'][0], $_SESSION['ASSESS_PERIOD'][0], $id_students);
    $subject = getGroupSubject($CONNEXION, $_SESSION['SELECTED_GROUP'][0]);

    // on peut créer le tableau :
    $table = '';
    $table .= spaces(2).'<div id="fixedTable" class="fixedTable">'."\n";
    $table .= spaces(3).'<table>'."\n";
    $table .= spaces(4).'<thead>'."\n";
    $table .= spaces(5).'<tr>'."\n";
    $table .= spaces(6).'<th class="verticalTitle"></th>'."\n";
    $table .= spaces(6).'<th class="separator"></th>'."\n";
    $table .= spaces(6).'<th class="appreciationTitle verticalTitle"><b><br ∕>'.tr('APPRECIATION').'</b></th>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</thead>'."\n";
    $table .= spaces(4).'<tbody>'."\n";
    $background = '';
    foreach ($students as $student) {
        $table .= spaces(5).'<tr class="appreciationColumn">'."\n";
        // le nom :
        if ($background == 'gray')
            $background = 'white';
        else
            $background = 'gray';
        $table .= spaces(6).'<td class="'.$background.' appreciationColumn">'.$student[1].'</td>'."\n";
        $appreciation = '';
        if (array_key_exists($student[0], $data2))
            $appreciation = nl2br($data2[$student[0]]['appreciation']);
        if ($appreciation == '')
            $appreciation = $nothing;
        $table .= spaces(6).'<td class="separator"></td>'."\n";
        $editData = 'vue="bulletin" id_eleve="'.$student[0].'" matiere="'.$subject
            .'" periode="'.$_SESSION['ASSESS_PERIOD'][0].'"';
        $table .= spaces(6).'<td class="appreciation edit_appreciation appreciationColumn" '.$editData
            .' title="'.$editMessage.'">'.$appreciation.'</td>'."\n";
        $table .= spaces(6).'</tr>'."\n";

        }
    // on ajoute la ligne du groupe :
    $table .= spaces(5).'<tr>'."\n";
    for ($i=0; $i < 3; $i++)
        $table .= spaces(6).'<td class="interligne"></td>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(5).'<tr class="groupColumn">'."\n";
    $table .= spaces(6).'<td class="'.$background.' groupColumn"><b>'.tr('GROUP').'</b></td>'."\n";
    $id_groupeInTables = - $_SESSION['SELECTED_GROUP'][0] - 1;
    $appreciation = '';
    if (array_key_exists($id_groupeInTables, $data2))
        $appreciation = nl2br($data2[$id_groupeInTables]['appreciation']);
    if ($appreciation == '')
        $appreciation = $nothing;
    $table .= spaces(6).'<td class="separator"></td>'."\n";
    $editData = 'vue="bulletin" id_eleve="'.$id_groupeInTables.'" matiere="'.$subject
        .'" periode="'.$_SESSION['ASSESS_PERIOD'][0].'"';
    $table .= spaces(6).'<td class="appreciation edit_appreciation groupColumn" '.$editData
        .' title="'.$editMessage.'" ><b>'.$appreciation.'</b></td>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</tbody>'."\n";
    $table .= spaces(3).'</table>'."\n";
    $table .= spaces(2).'</div>'."\n";
    $pageContent .= $table;
    }




























elseif ($_SESSION['SELECTED_VUE'][0] == VUE_SUIVIS) {
    $printTitle = $_SESSION['SELECTED_GROUP'][1].' - '.$_SESSION['SELECTED_VUE'][1];
    // on récupère la liste des horaires de l'établissement :
    $horaires = $CONNEXION -> horaires();
    $subject = getGroupSubject($CONNEXION, $_SESSION['SELECTED_GROUP'][0]);

    if (! isset($btnWidth0))
        $btnWidth0 = '130px';
    if (! isset($btnWidth1))
        $btnWidth1 = '250px';
    if (! isset($thePage))
        $thePage = $_SESSION['PAGE'];

    // sélecteurs (date et horaire) :
    $selects = spaces(3).'<nav class="navbar navbar-expand-lg navbar-'.$_SESSION['NAVBAR_STYLE'].'">'."\n";
    $selects .= spaces(4).'<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar2" aria-expanded="false" aria-label="Toggle navigation">'."\n";
    $selects .= spaces(5).'<span class="navbar-toggler-icon"></span>'."\n";
    $selects .= spaces(4).'</button>'."\n";
    $selects .= spaces(3).'<div class="collapse navbar-collapse justify-content-md-center" id="navbar2">'."\n";
    // la date :
    $title = date('Y-m-d');
    $comment = tr('format: yyyy-mm-dd');
    $printTitle .= '<br/>'.$title;
    $select = spaces(5).'<b>'.tr('Date:').$nothing.'</b>'."\n";
    $select .= spaces(4).'<div class="btn-group btn-group-sm">'."\n";
    $select .= spaces(5).'<input id="select-date" type="text" class="form-control"'
        .' style="width:'.$btnWidth0.';text-align:center;" value="'.$title
        .'" title="'.$comment.'"></input>'."\n";
    $select .= spaces(4).'</div>'."\n";
    $selects .=  $select;
    // un séparateur :
    $selects .= spaces(5).$nothing.$nothing.$nothing.$nothing.$nothing."\n";
    // l'horaire :
    $select = spaces(4).'<div id="select-horaire" class="btn-group btn-group-sm text-center">'."\n";
    $title = tr('Horaire');
    $printTitle .= ' - '.$title;
    $select .= spaces(5).'<a id="horaire-title" other=""'
        .' class="nav-link btn-sm btn-light dropdown-toggle"'
        .' data-toggle="dropdown" href="#" style="width:'.$btnWidth1.';">'
        .$title.'</a>'."\n";
    $select .= spaces(5).'<ul class="dropdown-menu">'."\n";
    foreach ($horaires as $idHoraire => $horaire) {
        $horaireText = $horaire[0].' ('.$horaire[1].')';
        $select .= spaces(6).'<li><a class="dropdown-item" what="'.$horaireText.'" other="'.$idHoraire.'" href="#">'
            .$horaireText.'</a></li>'."\n";
        }
    $select .= spaces(5).'</ul>'."\n";
    $select .= spaces(4).'</div>'."\n";
    $selects .=  $select;
    $selects .= spaces(3).'</div>'."\n";
    $selects .= spaces(3).'</nav>'."\n";

    if (VERSION_NAME == 'demo')
        $pageContent .= notInDemoMessage();
    // liste des élèves :
    $students = $CONNEXION -> listStudentsInGroup($_SESSION['SELECTED_GROUP'][0]);
    $id_students = array2string($students, 0);
    // on récupère la liste des compétences suivies pour ces élèves :
    $follows = $CONNEXION -> getFollows($_SESSION['SELECTED_CLASS'][0], $id_students);

    $nothingClass = getItemClass($CONNEXION, '');

    $evalsTitles = array();
    foreach (array_keys($follows) as $id_PP)
        foreach ($follows[$id_PP]['ALL'] as $follow) {
            $evalsTitles[] = array($follow[0], $follow[1], $follow[2]);
            }
    $columnsCount = count($evalsTitles) + 1;

    // on peut créer le tableau :
    $table = '';
    $table .= spaces(2).'<div class="fixedTable">'."\n";
    $table .= spaces(3).'<table>'."\n";
    $table .= spaces(4).'<thead>'."\n";
    $table .= spaces(5).'<tr>'."\n";
    $table .= spaces(6).'<th class="horizontalTitle"></th>'."\n";
    $table .= spaces(6).'<th class="separator"></th>'."\n";
    foreach ($evalsTitles as $evalTitle)
        $table .= spaces(6).'<th class="bigWidth" title="'.$evalTitle[2].'"><b>'.$evalTitle[1].'</b></th>'."\n";
    $table .= spaces(6).'<th class="separator"></th>'."\n";
    $table .= spaces(6).'<th class="appreciationTitle horizontalTitle"><b><br ∕>'.tr('Remark').'</b></th>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</thead>'."\n";
    // noms des élèves et résultats :
    $table .= spaces(4).'<tbody>'."\n";
    $background = '';
    foreach ($students as $student)
        foreach (array_keys($follows) as $id_PP)
            if (array_key_exists($student[0], $follows[$id_PP])) {
                $table .= spaces(5).'<tr>'."\n";
                // le nom :
                if ($background == 'gray')
                    $background = 'white';
                else
                    $background = 'gray';
                $table .= spaces(6).'<td class="'.$background.' appreciationColumn">'
                    .$student[1].'</td>'."\n";
                $table .= spaces(6).'<td class="separator"></td>'."\n";
                // les résultats :
                foreach ($evalsTitles as $evalTitle) {
                    $mustDo = False;
                    $title = '';
                    foreach ($follows[$id_PP][$student[0]] as $eval)
                        if ($eval[1] == $evalTitle[1]) {
                            $mustDo = True;
                            $id_item = $eval[0];
                            $title = $eval[2];
                            }
                    if ($mustDo) {
                        $editData = 'vue="suivis" id_eleve="'.$student[0].'" matiere="'.$subject
                            .'" id_item="'.$id_item.'" id_pp="'.$id_PP.'"';
                        $table .= spaces(6).'<td class="'.$background.' edit_eval" '.$editData
                            .' title="'.$title.'">'.$nothing.'</td>'."\n";
                        }
                    else
                        $table .= spaces(6).'<td class="interligne">'.$nothing.'</td>'."\n";
                    }
                $table .= spaces(6).'<td class="separator"></td>'."\n";
                $editData = 'vue="suivis" id_eleve="'.$student[0].'" matiere="'.$subject
                    .'" id_pp="'.$id_PP.'"';
                $table .= spaces(6).'<td class="'.$background.' appreciation edit_appreciation appreciationColumn" '.$editData
                    .' title="'.$editMessage.'">'.$nothing.'</td>'."\n";
                $table .= spaces(5).'</tr>'."\n";
                }
    // on ajoute la ligne du groupe :
    $id_groupeInTables = - $_SESSION['SELECTED_GROUP'][0] - 1;
    $table .= spaces(5).'<tr>'."\n";
    for ($i=0; $i < $columnsCount + 3; $i++)
        $table .= spaces(6).'<td class="interligne"></td>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(5).'<tr class="groupColumn">'."\n";
    $table .= spaces(6).'<td class="'.$background.' groupColumn"><b>'.tr('GROUP').'</b></td>'."\n";
    $table .= spaces(6).'<td class="separator"></td>'."\n";
    foreach ($evalsTitles as $evalTitle) {
        $editData = 'vue="suivis" id_eleve="'.$id_groupeInTables.'" matiere="'.$subject
            .'" id_item="'.$evalTitle[0].'" id_pp="'.$id_PP.'"';
        $table .= spaces(6).'<td class="'.$background.' edit_eval groupColumn" '.$editData
            .' title="'.$evalTitle[2].'">'.$nothing.'</td>'."\n";
        }
    $table .= spaces(6).'<td class="separator"></td>'."\n";
    $editData = 'vue="suivis" id_eleve="'.$id_groupeInTables.'" matiere="'.$subject
        .'" id_pp="'.$id_PP.'"';
    $table .= spaces(6).'<td class="'.$background.' appreciation edit_appreciation groupColumn" '.$editData
        .' title="'.$editMessage.'"><b>'.$nothing.'</b></td>'."\n";
    $table .= spaces(5).'</tr>'."\n";
    $table .= spaces(4).'</tbody>'."\n";
    $table .= spaces(3).'</table>'."\n";
    $table .= spaces(2).'</div>'."\n";
    $pageContent .= $table;
    if (count($follows) < 1)
        $pageContent = message(
            'danger', 
            'No student followed in this class.',
            '');
    }



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$print = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$print .= spaces(2).'<div class="d-none d-print-block">'."\n";
$print .= spaces(3).'<p class="text-center">'.$printTitle.'</p>'."\n";
$print .= spaces(2).'</div>'."\n";
$result .= "\n".$print;
if ($_SESSION['SELECTED_VUE'][0] == VUE_SUIVIS) {
    $result .= spaces(2).'<div id="selects_top" class="d-print-none">'."\n";
    $result .= $selects;
    $result .= spaces(2).'</div>'."\n";
    $result .= spaces(2).'<br /><br />'."\n";
    }
$result .= $pageContent;
// pour l'évaluation :
if (in_array($_SESSION['SELECTED_VUE'][0], array(VUE_ITEM, VUE_SUIVIS)))
    $result .= addJavascripVariables4Editing();

if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
