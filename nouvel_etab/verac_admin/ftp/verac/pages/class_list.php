<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if (($_SESSION['USER_MODE'] == 'prof') and ($_SESSION['SELECTED_CLASS'][0] == -1))
    $result = "\n".message('info', 'Select a class.', '');
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function verifyPasswords(
        $CONNEXION, $students, $loginsPasswords) {
    /*
    pour vérifier si les mots de passe ont été changés
    */

    $result = array();
    $table = 'eleves';
    $SQL = 'SELECT COUNT(*) AS TOTAL FROM '.$table.' ';
    $SQL .= 'WHERE id=:id ';
    $SQL .= 'AND Mdp IN (:mdp1, :mdp256)';
    $glyphicon = '<span class="oi oi-check" aria-hidden="true"></span>';
    foreach ($students as $student) {
        $eleve_id = $student[0];
        $result[$eleve_id] = '';
        $password = $loginsPasswords[$eleve_id][1];
        $mdp_enc = doEncode($password);
        $OPT = array(
            ':id' => $eleve_id, 
            ':mdp1' => $mdp_enc[0], ':mdp256' => $mdp_enc[1]);
        $STMT = $CONNEXION -> DB_USERS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        $RESULT = $STMT -> fetch();
        $changed = ($RESULT['TOTAL'] > 0) ? '' : $glyphicon;
        $result[$eleve_id] = $changed;
        }
    return $result;
    }

function doWidth($width) {
    return 'width="'.$width.'px"';
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
$students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);
$id_students = array2string($students, 0);
$studentsBirthdays = $CONNEXION -> getStudentBirthday($id_students);
$loginsPasswords = $CONNEXION -> getStudentLogin($id_students);
$verifyPasswords = verifyPasswords($CONNEXION, $students, $loginsPasswords);


/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.tr('Class List: ').$_SESSION['SELECTED_CLASS'][1].'</h2>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";

$printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
$printTitle .= spaces(3).'<p class="text-center">';
$printTitle .= tr('List');
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</p>'."\n";
$printTitle .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE DU TABLEAU DE LA LISTE
****************************************************/
$tbodyHeight = $_SESSION['WINDOW_HEIGHT'] - 250;

$table = '';
// la ligne des titres du tableau :
$table .= spaces(5).'<table class="table table-fixed table-striped table-bordered">'."\n";
$table .= spaces(6).'<thead>'."\n";
$table .= spaces(7).'<tr class="table-success">'."\n";
$table .= spaces(8).'<th '.doWidth(300).'>'.tr('STUDENT').'</th>'."\n";
$table .= spaces(8).'<th '.doWidth(150).' class="text-center">'.tr('Birthday').'</th>'."\n";
$table .= spaces(8).'<th '.doWidth(200).' class="text-center">'.tr('Login').'</th>'."\n";
$table .= spaces(8).'<th '.doWidth(150).' class="text-center">'.tr('Initial password').'</th>'."\n";
$table .= spaces(8).'<th '.doWidth(80).' class="text-center">'.tr('Changed').'</th>'."\n";
$table .= spaces(7).'</tr>'."\n";
$table .= spaces(6).'</thead>'."\n";
$table .= spaces(6).'<tbody height="'.$tbodyHeight.'">'."\n";
// pour chaque élève de la classe :
foreach ($students as $student) {
    $eleve_id = $student[0];
    $table .= spaces(7).'<tr>'."\n";
    $table .= spaces(8).'<td '.doWidth(300).'>'.$student[1].'</td>'."\n";
    $studentBirthday = $studentsBirthdays[$eleve_id];
    $table .= spaces(8).'<td '.doWidth(150).' class="text-center">'.$studentBirthday.'</td>'."\n";
    $loginPassword = $loginsPasswords[$eleve_id];
    $table .= spaces(8).'<td '.doWidth(200).' class="text-center">'.$loginPassword[0].'</td>'."\n";
    $table .= spaces(8).'<td '.doWidth(150).' class="text-center">'.$loginPassword[1].'</td>'."\n";
    $table .= spaces(8).'<td '.doWidth(80).' class="text-center">'.$verifyPasswords[$eleve_id].'</td>'."\n";
    $table .= spaces(7).'</tr>'."\n";
    }
$table .= spaces(6).'</tbody>'."\n";
$table .= spaces(5).'</table>'."\n";

// pour centrer le tableau :
$table_center = '';
$table_center .= spaces(2).'<div class="container theme-showcase">'."\n";
$table_center .= $table;
$table_center .= spaces(2).'</div><br />'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle.$printTitle.$table_center;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
