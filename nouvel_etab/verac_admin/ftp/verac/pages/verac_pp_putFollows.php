<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



require_once('../libs/constantes.php');
require_once('../libs/fonctions_divers.php');
require_once('../libs/fonctions_session.php');
$fichier_constantes = CHEMIN_CONFIG.'constantes.php';
if (is_file($fichier_constantes))
    require_once($fichier_constantes);
else
    echo "Il y a eu un problème";

if (move_uploaded_file($_FILES['fichier'] ['tmp_name'], 
    CHEMIN_VERAC."tmp/{$_FILES['fichier'] ['name']}")) {
    require_once('../'.VERAC_CONNEXION_OTHER);
    $CONNEXION = new CONNEXION_OTHER;

    // on récupère le contenu du fichier envoyé :
    $filetemp = CHEMIN_VERAC."tmp/{$_FILES['fichier'] ['name']}";
    $id_PP = substr(basename($filetemp, '.sqlite'), 6);
    $pp_eleves_suivis = $CONNEXION -> readFollowsDefines($filetemp);

    $filename = $CONNEXION -> CHEMIN_SUIVIS.'suivi_'.$id_PP.'.sqlite';
    if (! file_exists($filename))
        rename($filetemp, $filename);
    unlink($filetemp);

    // inscription des définitions dans les bases suivis.sqlite et suivi_pp.sqlite :
    $RESULT = $CONNEXION -> writeFollowsDefines($id_PP, $pp_eleves_suivis);

    echo "OK";
    }
else
    echo "PB";

?>
