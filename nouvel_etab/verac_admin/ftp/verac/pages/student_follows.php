<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if (($_SESSION['USER_MODE'] == 'eleve') and (! SHOW_SUIVI))
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if ($_SESSION['USER_MODE'] == 'prof') {
    if ($_SESSION['SELECTED_CLASS'][0] == -1)
        $result = "\n".message('info', 'Select a class.', '');
    elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
        $result = "\n".message('info', 'Select a student.', '');
        }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    if ($what == 'changeDirectory') {
        $value = isset($_POST['value']) ? $_POST['value'] : '';
        $_SESSION['FOLLOWS_DIRECTORY'] = $value;
        }
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function getFollows($CONNEXION, $id_eleve=-1) {
    /*
    */
    if ($CONNEXION -> no_db_suivis)
        return array();
    $SQL = 'SELECT * FROM suivi_pp_eleve_cpt ';
    $OPT = array();
    if ($id_eleve != -1) {
        $SQL .= 'WHERE id_eleve IN (:id_eleve) ';
        $OPT = array(':id_eleve' => $id_eleve);
        }
    $SQL .= 'ORDER BY id_pp, id_eleve';
    $STMT = $CONNEXION -> DB_SUIVIS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    return $RESULT;
    }

function followedCompetences($CONNEXION, $id_classe) {
    /*
    */
    $infos = getClassInfos($CONNEXION, '', $id_classe);
    $SQL = 'SELECT id, code, Competence FROM suivi ';
    $SQL .= 'WHERE Competence!="" ';
    $SQL .= 'AND (classeType=:classe_type OR classeType=-1) ';
    $SQL .= 'ORDER BY ordre';
    $OPT = array(':classe_type' => $infos['classeType']);
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $RESULT0 = ($STMT != '') ? $STMT -> fetchAll() : array();
    $RESULT = array();
    foreach ($RESULT0 as $followedCompetence) {
        $RESULT[$followedCompetence['id']] = array(
            $followedCompetence['code'], $followedCompetence['Competence']);
        }
    return $RESULT;
    }

function horaires($CONNEXION) {
    /*
    retourne la liste des horaires de l'établissement.
    La fonction est redéfinie ici pour que les élèves y aient accès.
    */
    $SQL = 'SELECT * FROM horaires ORDER BY id';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    $RESULT0 = ($STMT != '') ? $STMT -> fetchAll() : array();
    $RESULT = array();
    foreach ($RESULT0 as $horaire) {
        $id_horaire = $horaire[0];
        $name = $horaire[1];
        $label = $horaire[2];
        $RESULT[$id_horaire] = array($name, $label);
        }
    return $RESULT;
    }

function studentFollowsEvals(
        $CONNEXION, $id_eleve, $id_PP, $order=' DESC') {
    /*
    */
    $filename = $CONNEXION -> CHEMIN_SUIVIS.'suivi_'.$id_PP.'.sqlite';
    if (! is_readable($filename))
        return array();
    $db_suivisPP = new PDO('sqlite:'.$filename);
    $db_suivisPP -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
    $SQL = 'SELECT * FROM suivi_evals ';
    $OPT = array();
    if ($id_eleve != -1) {
        $SQL .= 'WHERE id_eleve=:id_eleve ';
        $OPT = array(':id_eleve' => $id_eleve);
        }
    $SQL .= 'ORDER BY date'.$order.', horaire, id_prof, matiere, id_eleve';
    $STMT = $db_suivisPP -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    return $RESULT;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
if ($_SESSION['USER_MODE'] == 'prof')
    $selectedStudent = $_SESSION['SELECTED_STUDENT'][0];
else
    $selectedStudent = $_SESSION['USER_ID'];
// liste des noms des profs :
$teachersNames = listTeachersNames($CONNEXION);

// on récupère la liste des compétences suivies pour cet élève (base suivis) :
$studentFollows = getFollows($CONNEXION, $selectedStudent);
// on récupère la liste complète des compétences suivies (base commun) :
$followedCompetences = followedCompetences($CONNEXION, $_SESSION['SELECTED_CLASS'][0]);
// et celle des horaires de l'établissement :
$horaires = horaires($CONNEXION);

// liste des compétences suivies pour cet élève :
$follows = array();
foreach ($studentFollows as $student) {
    $id_PP = $student['id_pp'];
    if (! array_key_exists($id_PP, $follows))
        $follows[$id_PP] = array();
    $id_cpt = $student['id_cpt'];
    $code = $followedCompetences[$id_cpt][0];
    $competence = $followedCompetences[$id_cpt][1];
    $label2 = $student['label2'];
    $follows[$id_PP][] = array($id_cpt, $code, $competence, $label2);
    }

if (! isset($btnWidth2))
    $btnWidth2 = '400px';
if (! isset($thePage))
    $thePage = $_SESSION['PAGE'];



/****************************************************
    SÉLECTEURS (SENS)
    + TITRE POUR IMPRESSION
****************************************************/
$printTitle = tr('Follows');
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.$_SESSION['SELECTED_STUDENT'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</br>';

$selects = spaces(3).'<nav class="navbar navbar-expand-lg navbar-'.$_SESSION['NAVBAR_STYLE'].'">'."\n";
$selects .= spaces(4).'<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar2" aria-expanded="false" aria-label="Toggle navigation">'."\n";
$selects .= spaces(5).'<span class="navbar-toggler-icon"></span>'."\n";
$selects .= spaces(4).'</button>'."\n";
$selects .= spaces(3).'<div class="collapse navbar-collapse justify-content-md-center" id="navbar2">'."\n";

$select = spaces(4).'<div id="select-directory" class="btn-group btn-group-sm text-center">'."\n";
$sensLabel = array(tr('reverse chronological directory'), tr('chronological directory'));
$title = $sensLabel[$_SESSION['FOLLOWS_DIRECTORY']];
$printTitle .= $title;
$select .= spaces(5).'<a id="directory-title" class="nav-link btn-sm btn-light dropdown-toggle" data-toggle="dropdown" href="#" style="width:'.$btnWidth2.';">'.$title.'</a>'."\n";
$select .= spaces(5).'<ul class="dropdown-menu">'."\n";
$select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="0" reload=true href="#">'.$sensLabel[0].'</a></li>'."\n";
$select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="1" reload=true href="#">'.$sensLabel[1].'</a></li>'."\n";
$select .= spaces(5).'</ul>'."\n";
$select .= spaces(4).'</div>'."\n";
$selects .=  $select;

$selects .= spaces(3).'</div>'."\n";
$selects .= spaces(3).'</nav>'."\n";



/****************************************************
    TITRE DU DOCUMENT
****************************************************/
if (count($follows) > 0) {
    $pageTitle = spaces(3).'<!-- PAGE HEADER -->'."\n";
    $pageTitle .= spaces(3).'<div class="d-print-none">'."\n";
    $pageTitle .= spaces(4).'<div class="page-header">'."\n";
    $pageTitle .= spaces(5).'<h2>'.tr('FOLLOWED ASSESSMENT').'</h2>'."\n";
    $pageTitle .= spaces(4).'</div>'."\n";
    $pageTitle .= spaces(3).'</div>'."\n";
    }
else
    $pageTitle = "\n".message('danger', 'This student is not followed.', '');



/****************************************************
    CONTENU DE LA PAGE
****************************************************/
$pageContent = '';

$evalsTitles = array(tr('Date'), tr('Schedule'), tr('Subject'));
$table = spaces(5).'<h3>'.tr('List of competences followed').'</h3>'."\n";
$table .= spaces(5).'<table border="1" class="bilan">'."\n";
$table .= spaces(6).'<tbody>'."\n";
foreach (array_keys($follows) as $id_PP)
    foreach ($follows[$id_PP] as $follow) {
        $table .= spaces(7).'<tr>'."\n";
        $table .= spaces(8).'<td class="competence_label"><b>'.$follow[1].'</b></td>'."\n";
        $table .= spaces(8).'<td class="competence_label">'.htmlspecialchars($follow[2]).'</td>'."\n";
        if ($follow[3] != '')
            $table .= spaces(8).'<td class="competence_label">'.htmlspecialchars($follow[3]).'</td>'."\n";
        else
            $table .= spaces(8).'<td class="competence_label"></td>'."\n";
        $table .= spaces(7).'</tr>'."\n";
        $evalsTitles[] = $follow[1];
        }
$table .= spaces(6).'</tbody>'."\n";
$table .= spaces(5).'</table>'."\n";
$evalsTitles[] = tr('Remark');
$pageContent .= $table;

$columnsCount = count($evalsTitles);
$evalsLines = array();
if ($_SESSION['FOLLOWS_DIRECTORY'] == 0)
    $order = ' DESC';
else
    $order = '';
foreach (array_keys($follows) as $id_PP) {
    $studentFollowsEvals = studentFollowsEvals($CONNEXION, $selectedStudent, $id_PP, $order);
    $line = array();
    $reference = '';
    foreach ($studentFollowsEvals as $evaluation) {
        $date = $evaluation['date'];
        $horaire = $evaluation['horaire'];
        $id_prof = $evaluation['id_prof'];
        $matiere = $evaluation['matiere'];
        $id_cpt = $evaluation['id_cpt'];
        $value = $evaluation['value'];
        $teacherName = $teachersNames[$id_prof][1];
        if ($date.$horaire.$id_prof.$matiere != $reference) {
            if ($reference != '')
                $evalsLines[] = $line;
            $reference = $date.$horaire.$id_prof.$matiere;
            $line = array(array($date, ''), $horaires[$horaire]);
            $line[] = array($matiere, $teacherName);
            foreach ($follows[$id_PP] as $competence_suivie) {
                $id_cpt2 = $competence_suivie[0];
                $title = $competence_suivie[2];
                if ($competence_suivie[3] != '')
                    $title .= ' ('.$competence_suivie[3].')';
                if ($id_cpt2 == $id_cpt)
                    $line[] = array($value, $title);
                else
                    $line[] = array('', '');
                }
            if ($id_cpt == -1)
                $line[] = array($value, '');
            else
                $line[] = array('', '');
            }
        else {
            for ($i=0; $i < count($follows[$id_PP]); $i++) {
                $id_cpt2 = $follows[$id_PP][$i][0];
                $title = $follows[$id_PP][$i][2];
                if ($follows[$id_PP][$i][3] != '')
                    $title .= ' ('.$follows[$id_PP][$i][3].')';
                if ($id_cpt2 == $id_cpt)
                    $line[3 + $i] = array($value, $title);
                }
            if ($id_cpt == -1)
                $line[$columnsCount - 1] = array($value, '');
            }
        }
    if ($reference != '')
        $evalsLines[] = $line;
    }

$table = spaces(5).'<h3>'.tr('Assessments').'</h3>'."\n";
$table .= spaces(5).'<table border="1" class="bilan">'."\n";
$table .= spaces(6).'<thead class="bilan">'."\n";
$table .= spaces(7).'<tr>'."\n";
foreach ($evalsTitles as $title) {
    if ($title != tr('Remark'))
        $table .= spaces(8).'<th class="title">'.$title.'</th>'."\n";
    else
        $table .= spaces(8).'<th class="last">'.$title.'</th>'."\n";
    }
$table .= spaces(7).'</tr>'."\n";
$table .= spaces(6).'</thead>'."\n";
$table .= spaces(6).'<tbody>'."\n";

$actualDate = '';
foreach ($evalsLines AS $line) {
    if ($line[0][0] != $actualDate) {
        $actualDate = $line[0][0];
        $table .= spaces(7).'<tr>'."\n";
        $table .= spaces(8).'<td class="interligne" colspan="'.$columnsCount.'"></td>'."\n";
        $table .= spaces(7).'</tr>'."\n";
        }
    else
        $line[0][0] = '';
    $table .= spaces(7).'<tr>'."\n";
    for ($i=0; $i<$columnsCount; $i++) {
        if ($i < 3)
            $table .= spaces(8).'<td align="center" title="'.$line[$i][1].'">'.$line[$i][0].'</td>'."\n";
        elseif ($i == $columnsCount - 1)
            $table .= spaces(8).'<td class="competence_label" title="'.$line[$i][1].'">'.$line[$i][0].'</td>'."\n";
        else
            $table .= spaces(8).'<td class="'.$line[$i][0].'" title="'.$line[$i][1].'">'.valeur2affichage($line[$i][0]).'</td>'."\n";
        }
    $table .= spaces(7).'</tr>'."\n";
    }

$table .= spaces(6).'</tbody>'."\n";
$table .= spaces(5).'</table>'."\n";

$pageContent .= $table;


if (count($follows) < 1)
    $pageContent = '';



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
// titre en cas d'impression :
$print = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$print .= spaces(2).'<div class="d-none d-print-block">'."\n";
$print .= spaces(3).'<p class="text-center">'.$printTitle.'</p>'."\n";
$print .= spaces(2).'</div>'."\n";
$result .= "\n".$print;
// sélecteurs :
$result .= spaces(2).'<div id="selects_top" class="d-print-none">'."\n";
$result .= $selects;
$result .= spaces(2).'</div>'."\n";
$result .= spaces(2).'<br /><br />'."\n";
// pour centrer l'affichage :
$result .= spaces(2).'<div class="container theme-showcase">'."\n";
$result .= $pageTitle;
$result .= spaces(3).'<div class="row">'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(4).'<div class="col-sm-10">'."\n";
$result .= $pageContent;
$result .= spaces(4).'</div>'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(3).'</div>'."\n";
$result .= spaces(2).'</div>'."\n";


if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
