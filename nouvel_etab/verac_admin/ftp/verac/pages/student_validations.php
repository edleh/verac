<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if ($_SESSION['USER_MODE'] == 'prof') {
    if ($_SESSION['SELECTED_CLASS'][0] == -1)
        $result = "\n".message('info', 'Select a class.', '');
    elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
        $result = "\n".message('info', 'Select a student.', '');
        }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }


/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    if ($what == 'changeSubject') {
        $value = isset($_POST['value']) ? $_POST['value'] : '';
        $_SESSION['VALIDATIONS_SUBJECT'] = $value;
        }
    elseif ($what == 'changeEval') {
        $id_eleve = isset($_POST['id_eleve']) ? $_POST['id_eleve'] : '';
        $date = isset($_POST['date']) ? $_POST['date'] : '';
        $bilan_name = isset($_POST['bilan_name']) ? $_POST['bilan_name'] : '';
        $id_prof = isset($_POST['id_prof']) ? $_POST['id_prof'] : '';
        $nom_prof = isset($_POST['nom_prof']) ? $_POST['nom_prof'] : '';
        $value = (isset($_POST['value'])) ? $_POST['value'] : '';
        $value = str_replace(getArrayEvals(), getArrayLetters(), $value);
        $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
        $db_referentialValidations = $CONNEXION -> connectReferentialValidationsDB();
        try {
            // on prépare la suppression :
            $SQL = 'DELETE FROM bilans_validations ';
            $SQL .= 'WHERE id_eleve='.$id_eleve.' AND bilan_name="'.$bilan_name.'"';
            $DEL_VALIDATION = $db_referentialValidations -> prepare($SQL);
            // on y va
            $db_referentialValidations -> beginTransaction();
            $DEL_VALIDATION -> execute();
            if ($value != '') {
                $SQL = 'INSERT INTO bilans_validations ';
                $SQL .= 'VALUES(:id_eleve, :date, :bilan_name, :id_prof, :nom_prof, :value, 1)';
                $STMT_VALIDATION = $db_referentialValidations -> prepare($SQL);
                $OPT = array(
                    ':id_eleve' => $id_eleve, 
                    ':date' => $date, 
                    ':bilan_name' => $bilan_name, 
                    ':id_prof' => $id_prof, 
                    ':nom_prof' => $nom_prof, 
                    ':value' => $value);
                $STMT_VALIDATION -> execute($OPT);
                }
            // On valide les modifications
            $db_referentialValidations -> commit();
            }
        catch(PDOException $e) {
            $db_referentialValidations -> rollBack();
            }
        $result = $value;
        }
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function studentValidations(
        $db_referentialPropositions, $db_referentialValidations, $id_eleve, $competences) {
    /*
    retourne les résultats d'un élève pour la validation du référentiel.
    */
    $validations = array(
        'PROPOSITIONS' => array(), 
        'DETAILS' => array(), 
        'VALIDATIONS' => array());

    // on crée le texte de la liste des compétences :
    foreach ($competences as $row) {
        $validations['PROPOSITIONS'][$row['code']] = '';
        $validations['DETAILS'][$row['code']] = array();
        $validations['VALIDATIONS'][$row['code']] = '';
        }

    // récupération des propositions :
    $SQL = 'SELECT * FROM bilans_propositions ';
    $SQL .= 'WHERE id_eleve IN ('.$id_eleve.') ';
    $STMT = $db_referentialPropositions -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    foreach ($tempResult as $row)
        $validations['PROPOSITIONS'][$row['bilan_name']] = $row['value'];

    // récupération des détails :
    $SQL = 'SELECT * FROM bilans_details ';
    $SQL .= 'WHERE id_eleve IN ('.$id_eleve.') ';
    $SQL .= 'ORDER BY bilan_name, annee_scolaire DESC , Matiere';
    $STMT = $db_referentialPropositions -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    foreach ($tempResult as $row)
        if (array_key_exists($row['bilan_name'], $validations['DETAILS'])) {
            $validations['DETAILS'][$row['bilan_name']][] = array(
                'annee' => $row['annee_scolaire'], 
                'matiere' => $row['Matiere'], 
                'prof' => $row['nom_prof'], 
                'value' => $row['value'], );
            }

    // récupération des validations :
    if ($db_referentialValidations != False) {
        $SQL = 'SELECT * FROM bilans_validations ';
        $SQL .= 'WHERE id_eleve IN ('.$id_eleve.') ';
        $STMT = $db_referentialValidations -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
        // on les remet en forme :
        foreach ($tempResult as $row) {
            $validations['VALIDATIONS'][$row['bilan_name']] = array(
                'date' => $row['date'], 
                'id_prof' => $row['id_prof'], 
                'nom_prof' => $row['nom_prof'], 
                'value' => $row['value'], );
            }
        }

    return $validations;
    }

function addJavascripVariables4Editing() {
    // on prépare le regex pour l'évaluation des items et le message d'alerte
    $RESULT = '';
    $regex = 'var compare = /^[';
    $alertMessage = 'var alertMessage="'.tr('INCORRECT ENTRY!').' \n\n '.tr('Use the keys: ').' ';
    $letters = '';
    $specialsChars = array('\\', '.', '$', '[', ']', ')', ')', '{', '}', '^', '?', '*', '+', '-');
    foreach (getArrayEvals() as $index => $letter) {
        $letters .= 'var letter_'.$index.' = "'.$letter.'";';
        $alertMessage .= strtoupper($letter).', ';
        if (in_array($letter, $specialsChars))
            $regex .= '\\'.$letter;
        else
            $regex.= strtoupper($letter);
        }
    $regex.= ']{0,1}$/i;';
    $alertMessage = substr($alertMessage, 0, -2);
    $alertMessage .= '.";';
    $RESULT .= spaces(2).'<script type="text/javascript">'."\n";
    $RESULT .= spaces(3).'// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later'."\n";
    $RESULT .= spaces(3).$letters."\n";
    $RESULT .= spaces(3).$regex."\n";
    $RESULT .= spaces(3).$alertMessage."\n";
    $RESULT .= spaces(3).'// @license-end'."\n";
    $RESULT .= spaces(2).'</script>'."\n";
    return $RESULT;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
$selectedStudent = $_SESSION['SELECTED_STUDENT'][0];

// la liste des matières :
$sortedSubjects = subjects($CONNEXION);
$subjectsLabels = listSubjectsLabels($CONNEXION);
// on recrée la liste des matières à prendre en compte :
$subjects = array();
$subjects[] = array('', tr('All subjects'));
foreach (array('Bulletin', 'Speciales', 'Autre') as $where) {
    $subjects[] = array('SEPARATOR', 'SEPARATOR');
    foreach ($sortedSubjects[$where] as $subject) {
        $matiere = $subject['MatiereCode'];
        $label = $subjectsLabels[$matiere][0];
        $subjects[] = array($matiere, $label);
        }
    }

if (! isset($btnWidth1))
    $btnWidth1 = '250px';
if (! isset($btnWidth2))
    $btnWidth2 = '400px';
if (! isset($thePage))
    $thePage = $_SESSION['PAGE'];

// variables utiles pour la validation en ligne :
if ($_SESSION['USER_ID'] < DECALAGE_PP)
    $PROF_ID = 0;
elseif ($_SESSION['USER_ID'] < DECALAGE_DIRECTEUR)
    $PROF_ID = DECALAGE_PP;
else
    $PROF_ID = DECALAGE_DIRECTEUR;
$BY_VERAC = tr('Calculated by VÉRAC.');
$BY_OTHER = tr('Validated by: ');
$editMessage = tr('Click to edit.');



/****************************************************
    SÉLECTEURS (MATIÈRE + EXPAND)
****************************************************/
$selects = spaces(3).'<nav class="navbar navbar-expand-lg navbar-'.$_SESSION['NAVBAR_STYLE'].'">'."\n";
$selects .= spaces(4).'<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar2" aria-controls="navbar2" aria-expanded="false" aria-label="Toggle navigation">'."\n";
$selects .= spaces(5).'<span class="navbar-toggler-icon"></span>'."\n";
$selects .= spaces(4).'</button>'."\n";
$selects .= spaces(3).'<div class="collapse navbar-collapse justify-content-md-center" id="navbar2">'."\n";

$select = '';
$select .= spaces(4).'<div id="select-subject" class="btn-group btn-group-sm text-center">'."\n";
$what = '';
$title = tr('Subject');
foreach ($subjects as $subject) {
    if ($subject[0] == $_SESSION['VALIDATIONS_SUBJECT']) {
        $what = $subject[0];
        $title = $subject[1];
        }
    }
$select .= spaces(5).'<a id="subject-title" class="nav-link btn-sm btn-light dropdown-toggle" data-toggle="dropdown" href="#" style="width:'.$btnWidth1.';">'.$title.'</a>'."\n";
$select .= spaces(5).'<ul class="dropdown-menu">'."\n";
foreach ($subjects as $subject) {
    if ($subject[0] == 'SEPARATOR')
        $select .= spaces(6).'<div class="dropdown-divider"></div>'."\n";
    else
        $select .= spaces(6).'<li><a class="dropdown-item" page="'.$thePage.'" what="'.$subject[0].'" reload=true href="#">'.$subject[1].'</a></li>'."\n";
    }
$select .= spaces(5).'</ul>'."\n";
$select .= spaces(4).'</div>'."\n";
$selects .=  $select;

$selects .= spaces(4).'<div>'.htmlSpaces(4).'</div>'."\n";

$select = '';
$select .= spaces(4).'<div class="btn-group btn-group-sm">'."\n";
$select .= spaces(5).'<button type="button" id="collapse" title="'.tr('Collapse all').'" class="btn btn-light"><span class="oi oi-minus" id="collapse"></span></button>'."\n";
$select .= spaces(5).'<button type="button" id="expand" title="'.tr('Expand all').'"  class="btn btn-light"><span class="oi oi-plus" id="expand"></span></button>'."\n";
$select .= spaces(4).'</div>'."\n";
$selects .=  $select;

$selects .= spaces(3).'</div>'."\n";
$selects .= spaces(3).'</nav>'."\n";



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = '';
$fileDate = getDateResultsStudent($CONNEXION, $selectedStudent);
$nom_eleve = getStudentName($CONNEXION, $selectedStudent, $mode='');
$pageTitle .= spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.$nom_eleve.' :<br/>'.tr('VALIDATIONS OF REFERENTIAL IN ').$fileDate.'</h2>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";
$printTitle = tr('Validations of referential');
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.getStudentName($CONNEXION, $selectedStudent);
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';



/****************************************************
    CALCUL DES VALIDATIONS DU RÉFÉRENTIEL
****************************************************/
$db_referentialPropositions = $CONNEXION -> connectReferentialPropositionsDB();
$db_referentialValidations = $CONNEXION -> connectReferentialValidationsDB();

$competencesTable = '';
if (! $db_referentialPropositions) {
    $competencesTable .= "\n".message(
        'danger', 
        'The validations DB is currently unavailable.');
    $pageTitle = '';
    }
else {
    $competences = sharedCompetences(
        $CONNEXION, 
        $_SESSION['SELECTED_CLASS'][0], 
        $what='referentiel');
    $validations = studentValidations(
        $db_referentialPropositions, 
        $db_referentialValidations, 
        $selectedStudent, 
        $competences);
    if ($db_referentialValidations != False)
        $editables = ((CAN_VALIDATE) or ($PROF_ID == DECALAGE_DIRECTEUR));
    else
        $editables = False;
    if (! $editables)
        $competencesTable .= "\n".message(
            'warning', 
            'Validation of the referential is disabled.');
    $detailsMessage = tr('Click to show/hide details.');
    $colspan = 2;
    $id_toggle = 0;
    $inTable = False;
    foreach ($competences as $ROW) {
        $table = '';
        $value = $validations['PROPOSITIONS'][$ROW['code']];
        $toolTip = '';
        $underline = array('', '');
        if ($value != '')
            $toolTip = $BY_VERAC;
        // on cherche s'il y a une validation
        // et on teste si la compétence est éditable :
        $validation = $validations['VALIDATIONS'][$ROW['code']];
        if ($validation != '') {
            $value = $validation['value'];
            $toolTip = $BY_OTHER.$validation['nom_prof'].' ('.$validation['date'].').';
            $editable = (($editables) and ($PROF_ID >= $validation['id_prof']));
            $underline = array('<u>', '</u>');
            }
        else
            $editable = $editables;

        if ($ROW['Competence'] != '')
            $name = htmlspecialchars($ROW['Competence']);
        elseif ($ROW['Titre1'] != '') {
            $name = htmlspecialchars($ROW['Titre1']);
            $level = 'h3';
            }
        elseif ($ROW['Titre2'] != '') {
            $name = htmlspecialchars($ROW['Titre2']);
            $level = 'h4';
            }
        elseif ($ROW['Titre3'] != '') {
            $name = htmlspecialchars($ROW['Titre3']);
            $level = 'b';
            }

        if ($ROW['Competence'] != '') {
            $id_toggle += 1;
            if (! $inTable) {
                $inTable = True;
                $table .= spaces(5).'<table border="1" class="bilan">'."\n";
                $table .= spaces(3).'<col width=90%>'."\n";
                $table .= spaces(3).'<col width=10%>'."\n";
                $table .= spaces(6).'<tbody>'."\n";
                }
            if ($value != '') {
                $toolTip .= "\n".$detailsMessage;
                $table .= spaces(7).'<tr class="detail_parent" title="'.$detailsMessage.'" id="'.$id_toggle.'">'."\n";
                $table .= spaces(8).'<td class="competence_label">'.$name.'</td>'."\n";
                $table .= spaces(8).'<td class="'.$value.'" title="'.$toolTip.'">'
                    .$underline[0].valeur2affichage($value).$underline[1].'</td>'."\n";
                $table .= spaces(7).'</tr>'."\n";
                // les lignes de détails (affichables par clic) :
                foreach ($validations['DETAILS'][$ROW['code']] as $detail) {
                    if ($_SESSION['VALIDATIONS_SUBJECT'] == '') {
                        $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                        $detailLabel = $detail['annee'].' - ';
                        if (array_key_exists($detail['matiere'], $subjectsLabels))
                            $detailLabel .= $subjectsLabels[$detail['matiere']][0];
                        else
                            $detailLabel .= $detail['matiere'];
                        $detailLabel .= ' ('.$detail['prof'].')';
                        $table .= spaces(8).'<td class="detail_label">'.$detailLabel.'</td>'."\n";
                        $table .= spaces(8).'<td class="'.$detail['value'].'">'
                            .valeur2affichage($detail['value']).'</td>'."\n";
                        $table .= spaces(7).'</tr>'."\n";
                        $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                        $table .= spaces(8).'<td class="interligne" colspan="'.$colspan.'"></td>'."\n";
                        $table .= spaces(7).'</tr>'."\n";
                        }
                    elseif ($_SESSION['VALIDATIONS_SUBJECT'] == $detail['matiere']) {
                        // on affiche les détails si une matière est sélectionnée :
                        $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden" style="display: table-row;">'."\n";
                        $detailLabel = $detail['annee'].' - ';
                        if (array_key_exists($detail['matiere'], $subjectsLabels))
                            $detailLabel .= $subjectsLabels[$detail['matiere']][0];
                        else
                            $detailLabel .= $detail['matiere'];
                        $detailLabel .= ' ('.$detail['prof'].')';
                        $table .= spaces(8).'<td class="detail_label">'.$detailLabel.'</td>'."\n";
                        $table .= spaces(8).'<td class="'.$detail['value'].'">'
                            .valeur2affichage($detail['value']).'</td>'."\n";
                        $table .= spaces(7).'</tr>'."\n";
                        $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden" style="display: table-row;">'."\n";
                        $table .= spaces(8).'<td class="interligne" colspan="'.$colspan.'"></td>'."\n";
                        $table .= spaces(7).'</tr>'."\n";
                        }
                    }
                }
            else {
                $table .= spaces(7).'<tr>'."\n";
                $table .= spaces(8).'<td class="competence_label">'
                    .$name.'</td>'."\n";
                $table .= spaces(8).'<td></td>'."\n";
                $table .= spaces(7).'</tr>'."\n";
                }
            }
        else {
            if ($editable) {
                if ($toolTip == '')
                    $toolTip = $editMessage;
                else
                    $toolTip .= "\n".$editMessage;
                $editData = 'id_eleve="'.$selectedStudent
                    .'" date="'.date('d/m/Y').'" bilan_name="'.$ROW['code']
                    .'" id_prof="'.$PROF_ID.'" nom_prof="'.$_SESSION['USER_NAME'].'"';
                }

            $affichage = valeur2affichage($value);
            if ($inTable) {
                $table .= spaces(6).'</tbody>'."\n";
                $table .= spaces(5).'</table>'."\n";
                $inTable = False;
                }

            if (($ROW['Titre1'] != '') or ($ROW['Titre2'] != '')) {
                $table .= spaces(5).'<br/>'."\n";
                $table .= spaces(5).'<table border="0" class="bilan2">'."\n";
                $table .= spaces(6).'<thead>'."\n";
                $table .= spaces(7).'<tr>'."\n";
                if ($editable)
                    $table .= spaces(8).'<td class="'.$value.' edit_eval" '.$editData
                        .' title="'.$toolTip.'">'.$underline[0].$affichage.$underline[1].'</td>'."\n";
                else
                    $table .= spaces(8).'<td class="'.$value.'" title="'.$toolTip.'">'
                        .$underline[0].$affichage.$underline[1].'</td>'."\n";
                $table .= spaces(8).'<th class="title"><'.$level.'>'.$name.'</'.$level.'></th>'."\n";
                $table .= spaces(7).'</tr>'."\n";
                $table .= spaces(6).'</thead>'."\n";
                $table .= spaces(5).'</table>'."\n";
                }
            elseif ($ROW['Titre3'] != '') {
                $inTable = True;
                $table .= spaces(5).'<table border="1" class="bilan">'."\n";
                $table .= spaces(6).'<thead class="bilan">'."\n";
                $table .= spaces(7).'<tr>'."\n";
                $table .= spaces(8).'<th class="title"><'.$level.'>'.$name.'</'.$level.'></th>'."\n";
                if ($editable)
                    $table .= spaces(8).'<td class="'.$value.' edit_eval" '.$editData
                        .' title="'.$toolTip.'">'.$underline[0].$affichage.$underline[1].'</td>'."\n";
                else
                    $table .= spaces(8).'<td class="'.$value.'" title="'.$toolTip.'">'
                        .$underline[0].$affichage.$underline[1].'</td>'."\n";
                $table .= spaces(7).'</tr>'."\n";
                $table .= spaces(6).'</thead>'."\n";
                $table .= spaces(6).'<tbody>'."\n";
                }
            }
        $competencesTable .= $table;
        }
    if ($inTable) {
        $competencesTable .= spaces(6).'</tbody>'."\n";
        $competencesTable .= spaces(5).'</table>'."\n";
        $inTable = False;
        }
    }



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
// titre en cas d'impression :
$print = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$print .= spaces(2).'<div class="d-none d-print-block">'."\n";
$print .= spaces(3).'<p class="text-center">'.$printTitle.'</p>'."\n";
$print .= spaces(2).'</div>'."\n";
$result .= "\n".$print;
// sélecteurs :
$result .= spaces(2).'<div id="selects_top" class="d-print-none">'."\n";
$result .= $selects;
$result .= spaces(2).'</div>'."\n";
$result .= spaces(2).'<br /><br />'."\n";
// titre :
$result .= "\n".$pageTitle;
// pour centrer l'affichage :
$result .= spaces(2).'<div class="container theme-showcase">'."\n";
$result .= spaces(3).'<div class="row">'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(4).'<div class="col-sm-10">'."\n";
$result .= $competencesTable;
$result .= spaces(4).'</div>'."\n";
$result .= spaces(4).'<div class="col-sm-1"></div>'."\n";
$result .= spaces(3).'</div>'."\n";
$result .= spaces(2).'</div>'."\n";
// pour l'évaluation :
$result .= addJavascripVariables4Editing();


if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
