<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if (($_SESSION['USER_MODE'] == 'prof') and ($_SESSION['SELECTED_CLASS'][0] == -1))
    $result = "\n".message('info', 'Select a class.', '');
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function studentsValidations(
        $db_referentialPropositions, $db_referentialValidations, $students, $competences) {
    /*
    retourne les résultats d'un élève pour la validation du référentiel.
    */
    $validations = array();
    // on crée le texte de la liste des élèves :
    $id_students = array2string($students, 0);
    // on crée le texte de la liste des compétences :
    foreach ($competences as $row) {
        $validations[$row['code']] = array();
        foreach ($students as $student)
            $validations[$row['code']][$student[0]] = array('', 0);
        }

    // récupération des propositions :
    $SQL = 'SELECT * FROM bilans_propositions ';
    $SQL .= 'WHERE id_eleve IN ('.$id_students.') ';
    $STMT = $db_referentialPropositions -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    foreach ($tempResult as $row)
        $validations[$row['bilan_name']][$row['id_eleve']] = array($row['value'], 0);

    // récupération des validations :
    if ($db_referentialValidations != False) {
        $SQL = 'SELECT * FROM bilans_validations ';
        $SQL .= 'WHERE id_eleve IN ('.$id_students.') ';
        $STMT = $db_referentialValidations -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
        // on les remet en forme :
        foreach ($tempResult as $row)
            $validations[$row['bilan_name']][$row['id_eleve']] = array($row['value'], 1);
        }

    return $validations;
    }

function getTitles($n, $competences) {
    /*
    retourne la ligne des titres du tableau
    */
    $result = '';
    $result .= spaces($n).'<thead class="bilan">'."\n";
    $result .= spaces($n + 1).'<tr>'."\n";
    $result .= spaces($n + 2).'<th class="student"><b>'.tr('STUDENT').'</b></th>'."\n";
    foreach ($competences as $ROW) {
        if ($ROW['Titre1'] != '') {
            $result .= spaces($n + 2).'<td class="separator"></td>'."\n";
            $result .= spaces($n + 2).'<th class="title1" title="'.$ROW['Titre1'].'"><b>'.$ROW['code'].'</b></th>'."\n";
            }
        elseif ($ROW['Titre2'] != '') {
            $result .= spaces($n + 2).'<th class="verticalTitle" title="'
                .$ROW['Titre2'].'"><div class="vertical">'.$ROW['code'].'</div></th>'."\n";
            }
        }
    $result .= spaces($n + 2).'<td class="separator"></td>'."\n";
    $result .= spaces($n + 1).'</tr>'."\n";
    $result .= spaces($n).'</thead>'."\n";
    return $result;
    }

function getStudentRow($n, $competences, $student, $validations) {
    /*
    retourne la ligne des résultats d'un élève
    */
    $result = spaces($n).'<tr>'."\n";
    // le nom :
    $result .= spaces($n + 1).'<td class="student">'.$student[1].'</td>'."\n";
    // les résultats :
    foreach ($competences as $ROW) {
        $underline = array('', '');
        if ($validations[$ROW['code']][$student[0]][1] == 1)
            $underline = array('<u>', '</u>');
        if ($ROW['Titre1'] != '') {
            $result .= spaces($n + 1).'<td class="separator"></td>'."\n";
            $value = $validations[$ROW['code']][$student[0]][0];
            $result .= spaces($n + 1).'<td class="'.$value.'" title="'.$ROW['Titre1'].'">'
                .$underline[0].valeur2affichage($value).$underline[1].'</td>'."\n";
            }
        elseif ($ROW['Titre2'] != '') {
            $value = $validations[$ROW['code']][$student[0]][0];
            $result .= spaces($n + 1).'<td class="'.$value.'" title="'.$ROW['Titre2'].'">'
                .$underline[0].valeur2affichage($value).$underline[1].'</td>'."\n";
            }
        }
    $result .= spaces($n + 1).'<td class="separator"></td>'."\n";
    $result .= spaces($n).'</tr>'."\n";
    return $result;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
$students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.tr('Synthesis of the class validations: ').$_SESSION['SELECTED_CLASS'][1].'</h2>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";

$printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
$printTitle .= spaces(3).'<p class="text-center">';
$printTitle .= tr('Validations');
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</p>'."\n";
$printTitle .= spaces(2).'</div>'."\n";



/****************************************************
    CALCUL DES VALIDATIONS DU RÉFÉRENTIEL
****************************************************/
$db_referentialPropositions = $CONNEXION -> connectReferentialPropositionsDB();
$db_referentialValidations = $CONNEXION -> connectReferentialValidationsDB();

$table = '';
if (! $db_referentialPropositions) {
    $table .= "\n".message(
        'danger', 
        'The validations DB is currently unavailable.');
    $pageTitle = '';
    }
else {
    $competences = sharedCompetences(
        $CONNEXION, 
        $_SESSION['SELECTED_CLASS'][0], 
        $what='referentiel');
    $validations = studentsValidations(
        $db_referentialPropositions, 
        $db_referentialValidations, 
        $students, 
        $competences);
    $table .= spaces(2).'<table border="1" class="bilan">'."\n";
    $table .= getTitles(3, $competences);
    $table .= spaces(3).'<tbody>'."\n";
    foreach ($students as $student)
        $table .= getStudentRow(4, $competences, $student, $validations);
    $table .= spaces(3).'</tbody>'."\n";
    $table .= spaces(2).'</table>'."\n";
    }



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle.$printTitle.$table;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
