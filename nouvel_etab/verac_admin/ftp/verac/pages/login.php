<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    PAS D'APPEL DIRECT DE CETTE PAGE
****************************************************/
if (! defined('VERAC'))
    exit;



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
$login = isset($_POST['login']) ? $_POST['login'] : '';
$password = isset($_POST['password']) ? $_POST['password'] :  '';
if ($ACTION == 'connect') {
    $message = '';
    switch(openSession($login, $password)):
        case 0:
            $_SESSION['PAGE'] = 'infos';
            $message = 'OK';
            $_SESSION['COOKIE_LOGIN'] = $login;
            break;
        case 3:
            $message = 'PASSWORD';
            break;
        case 5:
            $message = 'USER';
            break;
        case 8:
            $message = 'IP';
            break;
    endswitch;
    exit($message);
    }



/****************************************************
    LOGIN DE L''UTILISATEUR
****************************************************/
/*
if (isset($_COOKIE['verac_login']))
    $userName =  htmlentities($_COOKIE['verac_login'], ENT_QUOTES);
else
    $userName = '';
*/



/****************************************************
    CALCUL DE LA PAGE
****************************************************/
$result = '';

$result .= spaces(2).'<!-- TITRE ET LOGO -->'."\n";
$result .= spaces(2).'<div class="row">'."\n";
$result .= spaces(3).'<div class="col-lg-2">'."\n";
$result .= spaces(4).'<a href="https://verac.tuxfamily.org" target="_blank" title="'.tr('VÉRAC Web Site').'">'."\n";
$result .= spaces(5).'<img src="images/logo.png" class="img-fluid" alt="Responsive image">'."\n";
$result .= spaces(4).'</a>'."\n";
$result .= spaces(3).'</div>'."\n";
$result .= spaces(3).'<div class="col-lg-8">'."\n";
$result .= spaces(4).'<h1 class="text-center"><strong>VÉRAC</strong></h1>'."\n";
$result .= spaces(4).'<h4 class="text-center text-muted">'.appTitle().'</h4>'."\n";
$result .= spaces(4).'<p class="text-center">'."\n";
$result .= spaces(5).'<a href="https://verac.tuxfamily.org" target="_blank">'.tr('VÉRAC Web Site').'</a>'."\n";
$result .= spaces(4).'</p>'."\n";
$result .= spaces(3).'</div>'."\n";
$result .= spaces(3).'<div class="col-lg-2"></div>'."\n";
$result .= spaces(2).'</div>'."\n";
$result .= spaces(2).'<hr />'."\n";

$result .= spaces(2).'<!-- ÉTABLISSEMENT -->'."\n";
$result .= spaces(2).'<h3 class="text-center">'.VERSION_LABEL.'</h3>'."\n";

$result .= spaces(2).'<!-- CONNEXION -->'."\n";
$result .= spaces(2).'<div class="login">'."\n";
$result .= spaces(3).'<form class="form-login">'."\n";
$result .= spaces(4).'<div class="input-group">'."\n";
$result .= spaces(5).'<div class="input-group-prepend">'."\n";
$result .= spaces(6).'<span class="input-group-text"><span class="oi oi-person"></span></span>'."\n";
$result .= spaces(5).'</div>'."\n";
$result .= spaces(5).'<label class="sr-only" for="login"></label>'."\n";
//$result .= spaces(5).'<input type="text" class="form-control" id="login" value="'.$userName.'" placeholder="'.tr('Username').'">'."\n";
$result .= spaces(5).'<input type="text" class="form-control" id="login" placeholder="'.tr('Username').'">'."\n";
$result .= spaces(4).'</div>'."\n";
$result .= spaces(4).'<div class="input-group">'."\n";
$result .= spaces(5).'<div class="input-group-prepend">'."\n";
$result .= spaces(6).'<span class="input-group-text"><span class="oi oi-key"></span></span>'."\n";
$result .= spaces(5).'</div>'."\n";
$result .= spaces(5).'<label class="sr-only" for="password"></label>'."\n";
$result .= spaces(5).'<input type="password" class="form-control" id="password" placeholder="'.tr('Password').'">'."\n";
$result .= spaces(4).'</div>'."\n";
$result .= spaces(4).'<div class="input-group">'."\n";
$result .= spaces(5).'<label></label>'."\n";
$result .= spaces(4).'</div>'."\n";
$result .= spaces(4).'<button class="btn btn-primary btn-block" type="button" id="go" data-loading-text="'.tr('Connecting...').'">'."\n";
$result .= spaces(5).tr('Connect').' &raquo;'."\n";
$result .= spaces(4).'</button>'."\n";
$result .= spaces(3).'</form>'."\n";
$result .= spaces(2).'</div>'."\n";

$result .= spaces(2).'<!-- AIDE -->'."\n";
$result .= spaces(3).'<p class="text-center">'."\n";
$result .= spaces(4).'<small><a href="https://verac.tuxfamily.org/site/help-web-login" target="_blank">'."\n";
$result .= spaces(4).tr('If you fail to login, click here.')."\n";
$result .= spaces(4).'</a></small>'."\n";
$result .= spaces(3).'</p>'."\n";
$version = date2Timestamp(DATE_VERSION_INTERFACE);
$result .= spaces(2).'<p class="text-center"><small>'
        .tr('Installed Version: ').date('d/m/Y', $version).'</small></p>'."\n";

$result .= spaces(2).'<!-- POUR LES MESSAGES AFFICHÉS VIA JAVASCRIPT -->'."\n";
$result .= spaces(2).'<div class="message">'."\n";
//$result .= spaces(3).'<br/>'."\n";
$result .= spaces(3).'<div class="hide" id="alert_msg"></div>'."\n";
$result .= spaces(2).'</div>'."\n";
$result .= spaces(2).'<label class="sr-only" id="msgNoLogin">'
    .tr('Enter a username.').'</label>'."\n";
$result .= spaces(2).'<label class="sr-only" id="msgNoPassword">'
    .tr('Enter a password.').'</label>'."\n";
$result .= spaces(2).'<label class="sr-only" id="msgError">'
    .tr('The connection failed!').'</label>'."\n";
$result .= spaces(2).'<label class="sr-only" id="msgOK">'.tr('OK').'</label>'."\n";
$result .= spaces(2).'<label class="sr-only" id="msgBadPassword">'
    .tr('The password is incorrect.').'</label>'."\n";
$result .= spaces(2).'<label class="sr-only" id="msg5">'
    .tr('The user name is incorrect.').'</label>'."\n";
$result .= spaces(2).'<label class="sr-only" id="msg8">'."\n";
$result .= spaces(3).tr('You have exceeded the number of allowed errors.').'<br/>'."\n";
$result .= spaces(3).tr('Your IP address has been recorded and is temporarily blocked.').'<br/>'."\n";
$result .= spaces(3).tr('Reconnect in 15 minutes.')."\n";
$result .= spaces(2).'</label>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
echo $result;

?>

