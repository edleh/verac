<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--<link rel="shortcut icon" href="images/favicon.png">-->
    <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png">
    <link rel="icon" href="images/favicon/favicon.ico" sizes="any">
    <link rel="manifest" href="images/favicon/site.webmanifest">
    <link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg" color="#5bbad5">


    <title>VÉRAC</title>

    <?php
    echo '<!-- Bootstrap core CSS -->'."\n";
    echo '    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">'."\n";
    echo '    <link href="css/open-iconic-bootstrap.min.css" rel="stylesheet" type="text/css">'."\n";

    echo '    <!-- Custom styles for this template -->'."\n";
    echo '    <link href="css/style_dynamic.php" rel="stylesheet" type="text/css">'."\n";
    $cssFile = 'css/'.$PAGE.'.css';
    if (! file_exists($cssFile))
        $cssFile = 'css/other.css';
    $cssFile = fileInCache($cssFile);
    echo '    <link href="'.$cssFile.'" rel="stylesheet" type="text/css">'."\n";
    ?>

</head>
<body>
