<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    PAS D'APPEL DIRECT DE CETTE PAGE
****************************************************/
if (! defined('VERAC'))
    exit;



/****************************************************
    INFORMATIONS DE DÉBOGAGE
****************************************************/
//print_r($_SESSION['TEMP_ARRAY']);
$session = spaces(3).'<h2>$_SESSION :</h2>'."\n";
$session .= spaces(4).'<ul>'."\n";
foreach ($_SESSION as $cle => $valeur) {
    $what = gettype($valeur);
    //debugToConsole($what);
    if ($what == 'array') {
        $session .= spaces(5).'<li><strong>'.$cle.' : </strong><em>'.count($valeur)
            .'</em></li>'."\n";
        $session .= spaces(5).'<ul>'."\n";
        foreach ($valeur as $cle2 => $valeur2) {
            if (gettype($valeur2) == 'array')
                $session .= spaces(6).'<li><strong>'.$cle2.' : </strong><em>'.count($valeur2)
                    .'</em></li>'."\n";
            else
                $session .= spaces(6).'<li><strong>'.$cle2.' : </strong><em>'.$valeur2
                    .'</em></li>'."\n";
            }
        $session .= spaces(5).'</ul>'."\n";
        }
    elseif ($what != 'object')
        $session .= spaces(5).'<li><strong>'.$cle.' : </strong><em>'.$valeur.'</em></li>'."\n";
    }
$session .= spaces(4).'</ul>'."\n";

$get = spaces(3).'<h2>$_GET :</h2>'."\n";
$get .= spaces(4).'<ul>'."\n";
foreach ($_GET as $cle => $valeur)
    $get .= spaces(5).'<li><strong>'.$cle.' : </strong><em>'.$valeur.'</em></li>'."\n";
$get .= spaces(4).'</ul>'."\n";

$post = spaces(3).'<h2>$_POST :</h2>'."\n";
$post .= spaces(4).'<ul>'."\n";
foreach ($_POST as $cle => $valeur)
    $post .= spaces(5).'<li><strong>'.$cle.' : </strong><em>'.$valeur.'</em></li>'."\n";
$post .= spaces(4).'</ul>'."\n";

$post = spaces(3).'<h2>$_COOKIE :</h2>'."\n";
$post .= spaces(4).'<ul>'."\n";
foreach ($_COOKIE as $cle => $valeur)
    $post .= spaces(5).'<li><strong>'.$cle.' : </strong><em>'.$valeur.'</em></li>'."\n";
$post .= spaces(4).'</ul>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$session.$get.$post;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
