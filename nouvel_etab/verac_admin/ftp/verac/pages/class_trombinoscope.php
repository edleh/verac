<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    if ($what == 'custom') {
        $_SESSION['TEMP'] = 'custom';
        }
    elseif ($what == 'doCustom') {
        $_SESSION['TEMP'] = 'doCustom';
        $group_name = isset($_POST['group_name']) ? $_POST['group_name'] : '';
        $id_students = isset($_POST['who']) ? $_POST['who'] : '';
        $_SESSION['TEMP_ARRAY'] = array();
        $_SESSION['TEMP_ARRAY'][] = $group_name;
        $_SESSION['TEMP_ARRAY'][] = $id_students;
        }
    else {
        foreach ($_SESSION['TEMP_FILES'] as $fileName) {
            if (is_file($fileName))
                unlink($fileName);
            }
        }
    exit($result);
    }
// VÉRIFICATION DE LA SÉLECTION :
$result = '';
if (($_SESSION['SELECTED_CLASS'][0] == -1) and ($_SESSION['TEMP'] == ''))
    $result = "\n".message('info', 'Select a class.', 'You can also make a custom trombinoscope.');
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
if ($_SESSION['TEMP'] == 'custom') {
    // on va afficher le formulaire :
    $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
    $students = $CONNEXION -> listAllStudents();
    $height = 300;
    if ($_SESSION['WINDOW_HEIGHT'] > 300)
        $height = 3 * $_SESSION['WINDOW_HEIGHT'] / 5;
    }
elseif ($_SESSION['TEMP'] == 'doCustom') {
    // on va afficher le trombinoscope personnalisé :
    $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
    $group_name = $_SESSION['TEMP_ARRAY'][0];
    $id_students = $_SESSION['TEMP_ARRAY'][1];
    $students = $CONNEXION -> listStudentsFromIds($id_students, $mode='TROMBI');
    $id_students = string2array($id_students, $separator=',');
    $photosDir = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_PROTECTED.'photos'.DIRECTORY_SEPARATOR;
    // TEMP_FILES servira à récupérer les fichiers temporaires des photos :
    $_SESSION['TEMP_FILES'] = array();
    }
else {
    // on va afficher le trombinoscope de la classe :
    $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
    $students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0], $mode='TROMBI');
    $id_students = array2string($students, 0);
    $photosDir = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_PROTECTED.'photos'.DIRECTORY_SEPARATOR;
    // TEMP_FILES servira à récupérer les fichiers temporaires des photos :
    $_SESSION['TEMP_FILES'] = array();
    }



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
if ($_SESSION['TEMP'] == 'custom') {
    // on met juste des explications :
    $pageTitle = spaces(3).'<div class="alert alert-info text-center">'."\n";
    $pageTitle .= spaces(4).'<p>'.tr(
        'Enter a name for your group and select the students by putting them in the right list.')."\n";
    $pageTitle .= spaces(4).'<br/>'.tr('Click the button to finish.')."\n";
    $pageTitle .= spaces(4).'</p>'."\n";
    $pageTitle .= spaces(3).'</div>'."\n";
    $printTitle = '';
    }
elseif ($_SESSION['TEMP'] == 'doCustom') {
    $pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
    $pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
    $pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
    $pageTitle .= spaces(4).'<div class="page-header">'."\n";
    $pageTitle .= spaces(5).'<h2>'.tr('Group Trombinoscope: ').$group_name;
    $pageTitle .= htmlSpaces(12).'<small class="text-muted">'.count($id_students).' '.tr('students').'</small>';
    $pageTitle .= '</h2><hr />'."\n";
    $pageTitle .= spaces(4).'</div>'."\n";
    $pageTitle .= spaces(3).'</div>'."\n";
    $pageTitle .= spaces(2).'</div>'."\n";
    $printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
    $printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
    $printTitle .= spaces(4).'<div class="page-header">'."\n";
    $printTitle .= spaces(5).'<h2>'.tr('Group Trombinoscope: ').$group_name;
    $printTitle .= htmlSpaces(12).'<small class="text-muted">'.count($id_students).' '.tr('students').'</small>';
    $printTitle .= '</h2><hr />'."\n";
    $printTitle .= spaces(4).'</div>'."\n";
    $printTitle .= spaces(2).'</div>'."\n";
    }
else {
    $pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
    $pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
    $pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
    $pageTitle .= spaces(4).'<div class="page-header">'."\n";
    $pageTitle .= spaces(5).'<h2>'.tr('Class Trombinoscope: ').$_SESSION['SELECTED_CLASS'][1];
    $pageTitle .= htmlSpaces(12).'<small class="text-muted">'.count($students).' '.tr('students').'</small>';
    $pageTitle .= '</h2><hr />'."\n";
    $pageTitle .= spaces(4).'</div>'."\n";
    $pageTitle .= spaces(3).'</div>'."\n";
    $pageTitle .= spaces(2).'</div>'."\n";
    $printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
    $printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
    $printTitle .= spaces(4).'<div class="page-header">'."\n";
    $printTitle .= spaces(5).'<h2>'.tr('Class Trombinoscope: ').$_SESSION['SELECTED_CLASS'][1];
    $printTitle .= htmlSpaces(12).'<small class="text-muted">'.count($students).' '.tr('students').'</small>';
    $printTitle .= '</h2><hr />'."\n";
    $printTitle .= spaces(4).'</div>'."\n";
    $printTitle .= spaces(2).'</div>'."\n";
    }



/****************************************************
    AFFICHAGE DU TABLEAU TROMBINOSCOPE
****************************************************/
if ($_SESSION['TEMP'] == 'custom') {
    $pageContent = '';
    $pageContent .= spaces(2).'<div class="container theme-showcase">'."\n";
    // première partie :
    $pageContent .= spaces(3).'<div class="row">'."\n";
    // saisie du nom du groupe :
    $pageContent .= spaces(4).'<div class="col-sm-7">'."\n";
    $pageContent .= spaces(5).'<div class="input-group mb-3">'."\n";
    $pageContent .= spaces(6).'<div class="input-group-prepend">'."\n";
    $pageContent .= spaces(7).'<span class="input-group-text">'.tr('Group Name:').'</span>'."\n";
    $pageContent .= spaces(6).'</div>'."\n";
    $pageContent .= spaces(6).'<input type="text" class="form-control" id="group-name">'."\n";
    $pageContent .= spaces(5).'</div>'."\n";
    $pageContent .= spaces(4).'</div>'."\n";
    // bouton pour lancer la création :
    $pageContent .= spaces(4).'<div class="col-sm-1"></div>'."\n";
    $pageContent .= spaces(4).'<div class="col-sm-3">'."\n";
    $pageContent .= spaces(5).'<button class="btn btn-primary btn-block" type="button" id="do-trombi">'.tr('Create trombinoscope').' &raquo;</button>'."\n";
    $pageContent .= spaces(4).'</div>'."\n";
    $pageContent .= spaces(4).'<div class="col-sm-1"></div>'."\n";
    $pageContent .= spaces(3).'</div>'."\n";
    $pageContent .= spaces(3).'<div class="row"><br/></div>'."\n";
    // deuxième partie :
    $pageContent .= spaces(3).'<div class="row">'."\n";
    // liste de tous les élèves :
    $pageContent .= spaces(4).'<div class="card col-sm-6">'."\n";
    $pageContent .= spaces(5).'<div style="max-height:'.$height.'px; overflow-y:scroll" class="card-body">'."\n";
    $pageContent .= spaces(6).'<ul id="base" class="list-group list-group-sm">'."\n";
    foreach ($students as $student)
        $pageContent .= spaces(7).'<li class="list-group-item" who='.$student[0].'>'.$student[1].' --- '.$student[2].'</li>'."\n";
    $pageContent .= spaces(6).'</ul>'."\n";
    $pageContent .= spaces(5).'</div>'."\n";
    $pageContent .= spaces(4).'</div>'."\n";
    // sélection :
    $pageContent .= spaces(4).'<div class="col-sm-6">'."\n";
    $pageContent .= spaces(5).'<ul id="selection" class="list-group">'."\n";
    $pageContent .= spaces(6).'<li class="list-group-item"></li>'."\n";
    $pageContent .= spaces(5).'</ul>'."\n";
    $pageContent .= spaces(4).'</div>'."\n";
    $pageContent .= spaces(3).'</div>'."\n";
    $pageContent .= spaces(2).'</div>'."\n";
    }
elseif ($_SESSION['TEMP'] == 'doCustom') {
    $pageContent = '';
    $pageContent .= spaces(2).'<div class="container theme-showcase">'."\n";
    $pageContent .= spaces(3).'<div class="row">'."\n";
    // pour chaque élève de la classe :
    foreach ($id_students as $eleve_id) {
        $student = $students[$eleve_id];
        $fileName = $photosDir.$eleve_id.'.jpeg';
        if (file_exists($fileName)) {
            $tempFileName = 'tmp'.DIRECTORY_SEPARATOR.$_SESSION['USER_ID'].'-'.$eleve_id.'.jpeg';
            if (! is_file(CHEMIN_VERAC.$tempFileName))
                copy($fileName, CHEMIN_VERAC.$tempFileName);
            $fileName = $tempFileName;
            $_SESSION['TEMP_FILES'][] = $fileName;
            }
        else
            $fileName = 'images/unknown-user.png';
        $pageContent .= spaces(4).'<div style="page-break-inside:avoid" class="col-xs-4 col-sm-3 col-md-2 col-lg-2">'."\n";
        $pageContent .= spaces(5).'<div class="thumbnail text-center" title="'.$student[2].' '.$student[3].'">'."\n";
        $pageContent .= spaces(6).'<img src="'.$fileName.'">'."\n";
        $pageContent .= spaces(6).'<div class="caption">'.$student[1].'<br/>'.$student[3].'</div>'."\n";
        //$pageContent .= spaces(7).'<p class="text-center">'.$student[1].'<br/>'.$student[3].'</p>'."\n";
        $pageContent .= spaces(5).'</div>'."\n";
        $pageContent .= spaces(4).'</div>'."\n";
        }
    $pageContent .= spaces(3).'</div>'."\n";
    $pageContent .= spaces(2).'</div>'."\n";
    }
else {
    $pageContent = '';
    $pageContent .= spaces(2).'<div class="container theme-showcase">'."\n";
    $pageContent .= spaces(3).'<div class="row">'."\n";
    // pour chaque élève de la classe :
    foreach ($students as $student) {
        $eleve_id = $student[0];
        $fileName = $photosDir.$eleve_id.'.jpeg';
        if (file_exists($fileName)) {
            $tempFileName = 'tmp'.DIRECTORY_SEPARATOR.$_SESSION['USER_ID'].'-'.$eleve_id.'.jpeg';
            if (! is_file(CHEMIN_VERAC.$tempFileName))
                copy($fileName, CHEMIN_VERAC.$tempFileName);
            $fileName = $tempFileName;
            $_SESSION['TEMP_FILES'][] = $fileName;
            }
        else
            $fileName = 'images/unknown-user.png';
        $pageContent .= spaces(4).'<div style="page-break-inside:avoid;" class="col-xs-4 col-sm-3 col-md-2 col-lg-2">'."\n";
        $pageContent .= spaces(5).'<div class="thumbnail text-center" title="'.$student[2].'">'."\n";
        $pageContent .= spaces(6).'<img src="'.$fileName.'">'."\n";
        $pageContent .= spaces(6).'<div class="caption">'.$student[1].'</div>'."\n";
        //$pageContent .= spaces(7).'<p class="text-center">'.$student[1].'</p>'."\n";
        $pageContent .= spaces(5).'</div>'."\n";
        $pageContent .= spaces(4).'</div>'."\n";
        }
    $pageContent .= spaces(3).'</div>'."\n";
    $pageContent .= spaces(2).'</div>'."\n";
    }



/****************************************************
    AFFICHAGE
****************************************************/
if ($_SESSION['TEMP'] != '')
    $_SESSION['TEMP'] = '';

$result = "\n".$pageTitle.$printTitle.$pageContent;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
