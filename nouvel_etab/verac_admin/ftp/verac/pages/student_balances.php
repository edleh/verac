<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if ($_SESSION['USER_MODE'] == 'eleve') {
    if ((! SHOW_BILANS) or ($PAGE == 'student_council'))
    $result = "\n".prohibitedMessage();
    }
// VÉRIFICATION DE LA SÉLECTION :
if ($_SESSION['USER_MODE'] == 'prof') {
    if ($_SESSION['SELECTED_CLASS'][0] == -1)
        $result = "\n".message('info', 'Select a class.', '');
    elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
        $result = "\n".message('info', 'Select a student.', '');
        }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $what = isset($_POST['what']) ? $_POST['what'] : '';
    if ($what == 'changeAppreciationPP') {
        if (VERSION_NAME == 'demo')
            $result = True;
        else  {
            $idPP = isset($_POST['id_pp']) ? $_POST['id_pp'] : '';
            $appreciation = isset($_POST['appreciation']) ? $_POST['appreciation'] : '';
            // en cas d'ancienne version de PHP :
            if (get_magic_quotes_gpc())
                $appreciation = stripslashes($appreciation);
            $appreciation = str_replace('|||', '&', $appreciation);
            $matiereCode = isset($_POST['matiere_code']) ? $_POST['matiere_code'] : '';
            $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
            if ($_SESSION['USER_ID'] == $idPP) {
                // c'est le PP
                $db_tmp = $CONNEXION -> DB_PROF;
                }
            else {
                // c'est le directeur
                $filename = $CONNEXION -> CHEMIN_PROF.$idPP.'.sqlite';
                $db_tmp = new PDO('sqlite:'.$filename);
                $db_tmp -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                }
            try {
                // on prépare la suppression :
                $SQL = 'DELETE FROM appreciations ';
                $SQL .= 'WHERE id_eleve=:id_eleve AND Matiere=:matiere_nom AND Periode=:periode';
                $DEL_PROF = $db_tmp -> prepare($SQL);
                // et l'enregistrement
                $SQL = 'INSERT INTO appreciations ';
                $SQL .= 'VALUES(:id_eleve, :value, :matiere_nom, :periode)';
                $STMT_PROF = $db_tmp -> prepare($SQL);
                // on y va
                $db_tmp -> beginTransaction();
                $OPT_PROF = array(
                    ':id_eleve' => $_SESSION['SELECTED_STUDENT'][0], 
                    ':matiere_nom' => $matiereCode, 
                    ':periode' => $_SESSION['SELECTED_PERIOD'][0]);
                $DEL_PROF -> execute($OPT_PROF);
                if ($appreciation != '') {
                    $OPT_PROF[':value'] = $appreciation;
                    $STMT_PROF -> execute($OPT_PROF);
                    }
                // On enregistre aussi la date et l'heure
                $date = date("YmdHi");
                $key = 'datetime';
                // on prépare la suppression :
                $SQL = 'DELETE FROM config ';
                $SQL .= 'WHERE key_name=:key_name';
                $DEL_PROF = $db_tmp -> prepare($SQL);
                // et les enregistrements :
                $SQL = 'INSERT INTO config ';
                $SQL .= 'VALUES(:key_name, :value_int, :value_txt)';
                $STMT_PROF = $db_tmp -> prepare($SQL);
                // on y va
                $OPT_PROF = array(':key_name' => $key);
                $DEL_PROF -> execute($OPT_PROF);
                $OPT_PROF[':value_int'] = $date;
                $OPT_PROF[':value_txt'] = 'WEB';
                $STMT_PROF -> execute($OPT_PROF);
                // On valide les modifications
                $db_tmp -> commit();
                $result = True;
                }
            catch(PDOException $e) {
                $db_tmp -> rollBack();
                $result = 'ERROR IN changeAppreciationPP';
                }
            }
        }
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function studentResultsInSubject(
        $CONNEXION, $id_eleve, $matiere, $limite) {
    /*
    récupère les résultats d'un élève dans une matière
    */
    // mise en forme de la liste des noms des compétences persos :
    $liste = '';
    for ($i=1; $i<$limite+1; $i++) {
        $liste .= '"'.$matiere.$i.'", ';
        if ($i < 10)
            $liste .= '"'.$matiere.'-0'.$i.'", ';
        else
            $liste .= '"'.$matiere.'-'.$i.'", ';
        }
    $liste = substr($liste,0, -2);
    // on lance la requête :
    $SQL = 'SELECT label, value, groupeValue, id_prof FROM persos ';
    $SQL .= 'WHERE id_eleve=:id_eleve ';
    $SQL .= 'AND name IN('.$liste.') ';
    $SQL .= 'ORDER BY id_prof, name';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    return ($STMT != '') ? $STMT -> fetchAll(): array();
    }

function studentAppreciations($CONNEXION, $id_eleve) {
    /*
    retourne les appréciations d'un élève
    */
    $appreciations = array();
    $SQL = 'SELECT matiere, id_prof, appreciation FROM appreciations ';
    $SQL .= 'WHERE id_eleve=:id_eleve';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $RESULT = ($STMT != '') ? $STMT -> fetchAll(): array();
    foreach ($RESULT as $ROW) {
        if (! isset($appreciations[$ROW['matiere']]))
            $appreciations[$ROW['matiere']] = array($ROW['id_prof'] => $ROW['appreciation']);
        else
            $appreciations[$ROW['matiere']][$ROW['id_prof']] = $ROW['appreciation'];
        }
    return $appreciations;
    }

function studentNotes($CONNEXION, $id_eleve) {
    /*
    retourne les notes d'un élève
    */
    $notes = array();
    // on vérifie que la table notes existe :
    $SQL = 'PRAGMA table_info("notes")';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> execute();
    $notes_exists = ($STMT != '') ? $STMT -> fetchAll(): array();
    if (count($notes_exists) < 1)
        return $notes;
    // on lance la requête :
    $SQL = 'SELECT notes.matiere, notes.id_prof, value, moyenne, ecart, mini, maxi ';
    $SQL .= 'FROM notes ';
    $SQL .= 'JOIN notes_classes USING (id_classe, matiere, id_prof) ';
    $SQL .= 'WHERE id_eleve=:id_eleve';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $RESULT = ($STMT != '') ? $STMT -> fetchAll(): array();
    foreach ($RESULT as $ROW) {
        $note = array($ROW['value'], $ROW['moyenne'], $ROW['ecart'], $ROW['mini'], $ROW['maxi']);
        if (! isset($notes[$ROW['matiere']]))
            $notes[$ROW['matiere']] = array($ROW['id_prof'] => $note);
        else
            $notes[$ROW['matiere']][$ROW['id_prof']] = $note;
        }
    return $notes;
    }

function studentAbsences($CONNEXION, $id_eleve) {
    /*
    donne le nombre d'absences et retards d'un élève
    */
    $version_db = getVersionDB($CONNEXION -> DB_RESULTATS);
    $absences = array('abs0' => 0, 'abs1' => 0, 'abs2' => 0, 'abs3' => 0);
    // on vérifie que la table absences existe :
    $SQL = 'PRAGMA table_info("absences")';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> execute();
    $absences_exists = ($STMT != '') ? $STMT -> fetchAll(): array();
    if (count($absences_exists) < 1)
        return $absences;
    // on lance la requête :
    $SQL = 'SELECT * FROM absences ';
    $SQL .= 'WHERE id_eleve=:id_eleve';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> execute($OPT);
    if ($STMT != '') {
        $RESULT = $STMT -> fetchAll();
        if (count($RESULT) > 0) {
            $abs0 = intval($RESULT[0][1]);
            $abs1 = intval($RESULT[0][2]);
            $abs2 = intval($RESULT[0][3]);
            $abs3 = intval($RESULT[0][4]);
            $absences = array(
                'version_db' => $version_db, 
                'abs0' => $abs0, 
                'abs1' => $abs1, 
                'abs2' => $abs2, 
                'abs3' => $abs3);
            }
        }
    return $absences;
    }

function testEditable($CONNEXION, $PAGE, $id_PP) {
    // cas où on quitte direct :
    if ($PAGE != 'student_council')
        return False;
    if ($id_PP < DECALAGE_PP)
        return False;
    if ($_SESSION['SELECTED_PERIOD'][0] != $_SESSION['ACTUAL_PERIOD'])
        return False;
    if ($_SESSION['USER_ID'] == $id_PP) {
        // c'est le PP lui-même
        if ($_SESSION['DB_PROF_OK'])
            return True;
        }
    elseif ($_SESSION['USER_ID'] >= DECALAGE_DIRECTEUR) {
        // c'est le directeur
        $filename = $CONNEXION -> CHEMIN_PROF.$id_PP.'.sqlite';
        if (file_exists($filename))
            if (is_writable($filename))
                return True;
        }
    return False;
    }

function appreciationPP(
        $CONNEXION, $id_eleve, $matiere, $id_PP) {
    $SQL = 'SELECT value FROM appreciations ';
    $SQL .= 'WHERE id_eleve=:id_eleve ';
    $SQL .= 'AND Matiere=:matiere ';
    $SQL .= 'AND Periode=:periode';
    $OPT = array(':id_eleve' => $id_eleve, ':matiere' => $matiere, ':periode' => $_SESSION['SELECTED_PERIOD'][0]);
    // on distingue si c'est le PP ou le directeur :
    if ($id_PP == $_SESSION['USER_ID'])
        $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    else {
        $filename = $CONNEXION -> CHEMIN_PROF.$id_PP.'.sqlite';
        // on se connecte à la base du PP :
        if (is_readable($filename)) {
            $DB_PP = new PDO('sqlite:'.$filename);
            $DB_PP -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        else
            return '';
        $STMT = $DB_PP -> prepare($SQL);
        }
    $STMT -> execute($OPT);
    if ($STMT != '') {
        $RESULT = $STMT -> fetch();
        $appreciation = $RESULT['value'];
        }
    return $appreciation;
    }

function getStudentNumberForEachValue($CONNEXION, $id_eleve) {
    /*
    retourne le nombre de resultats par niveau pour un élève donné.
    La lecture de la table syntheses foire dans certains cas
    (Bug Maupassant).
    En attendant de déboguer je recalcule tout.
    */
    $letters = getArrayLetters();
    $totaux = array();
    foreach ($letters as $letter)
        $totaux[$letter] = 0;

    $idClasseInResults2 = idClassInResults(
        $CONNEXION, $_SESSION['SELECTED_CLASS'][1]);
    $sharedCompetences2 = sharedCompetences(
        $CONNEXION, $_SESSION['SELECTED_CLASS'][0], $what='bulletin');
    $results2 = studentResults(
        $CONNEXION, $id_eleve, $idClasseInResults2, $sharedCompetences2);
    foreach ($results2 as $ROW)
        if ($ROW['Competence'] != '')
            if ($ROW['value'] != '')
                $totaux[$ROW['value']]++;
    $limite2 = readConfig($CONNEXION, 'limiteBLTPerso');
    $limite2 = $limite2['INT'];
    $subjects2 = subjects($CONNEXION);
    foreach ($subjects2['Bulletin'] as $MATIERE_ROW)
        if ($MATIERE_ROW['OrdreBLT'] > 0) {
            $results = studentResultsInSubject(
                $CONNEXION, $id_eleve, $MATIERE_ROW['MatiereCode'], $limite2);
            // récupération des évaluations :
            foreach ($results as $ROW)
                if ($ROW['value'] != '')
                    $totaux[$ROW['value']]++;
            }
    $matiereCode = 'VS';
    $results = studentResultsInSubject(
        $CONNEXION, $id_eleve, $matiereCode, $limite2);
    foreach ($results as $ROW)
        if ($ROW['value'] != '')
            $totaux[$ROW['value']]++;

    return $totaux;
    }

function listPeriodsForModal($CONNEXION, $min=0, $max=-1) {
    /*
    retourne la liste des périodes (id, name).
    $min et $max permettent de ne pas donner toute la liste.
    */
    $SQL = 'SELECT id, Periode FROM periodes ';
    $SQL .= 'WHERE id>='.$min.' ';
    if ($max > -1)
        $SQL .= 'AND id<='.$max.' ';
    $SQL .= 'ORDER BY id';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    $result = array();
    foreach ($tempResult as $period)
        $result[] = array($period[0], $period[1]);
    return $result;
    }

function studentComponents(
        $CONNEXION, $student, $SOCLE_COMPONENTS) {
    /*
    retourne les résultats d'un élève pour une liste de compétences partagées
    */
    $result = array();
    foreach ($SOCLE_COMPONENTS as $code => $row)
        $result[$code] = '';
    $version_db = getVersionDB($CONNEXION -> DB_RESULTATS);
    if ($version_db < 9)
        return $result;
    else {
        // on crée le texte de la liste des compétences :
        $competencesText = array();
        foreach ($SOCLE_COMPONENTS as $code => $row)
            $competencesText[]= '"'.$code.'"';
        $competencesText = implode(',', $competencesText);
        // récupération des résultats :
        $SQL = 'SELECT * FROM lsu ';
        $SQL .= 'WHERE lsuWhat="socle" ';
        $SQL .= 'AND id_eleve IN ('.$student.') ';
        $SQL .= 'AND lsu1 IN ('.$competencesText.') ';
        $SQL .= 'ORDER BY id_eleve DESC';
        $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
        // on les remet en forme :
        foreach ($tempResult as $row)
            $result[$row['lsu1']] = $row['lsu2'];
        return $result;
        }
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
if ($_SESSION['USER_MODE'] == 'prof')
    $selectedStudent = $_SESSION['SELECTED_STUDENT'][0];
else
    $selectedStudent = $_SESSION['USER_ID'];
// la liste des matières :
$subjects = subjects($CONNEXION);
// limite du nombre de bilans persos sur le bulletin :
$limite = readConfig($CONNEXION, 'limiteBLTPerso');
$limite = $limite['INT'];
// appréciations et notes de l'élève :
if ($_SESSION['USER_MODE'] == 'prof')
    $appreciations = studentAppreciations($CONNEXION, $selectedStudent);
elseif ($_SESSION['USER_MODE'] == 'eleve') {
    if ($_SESSION['SELECTED_PERIOD'][0] != $_SESSION['ACTUAL_PERIOD'])
        $appreciations = studentAppreciations($CONNEXION, $selectedStudent);
    elseif (SHOW_APPRECIATION)
        $appreciations = studentAppreciations($CONNEXION, $selectedStudent);
    }
else
    $appreciations = array();
$notes = studentNotes($CONNEXION, $selectedStudent);
// liste des profs ayant cet élève (d'après la base resultats) :
$idsProfsStudent = idsProfsStudent($CONNEXION, $selectedStudent);
// liste des noms des profs :
$teachersNames = listTeachersNames($CONNEXION);
// liste des noms des matières :
$subjectsLabels = listSubjectsLabels($CONNEXION);
// affichage des détails par clic :
$studentsShowDetails = True;
if ($_SESSION['USER_MODE'] == 'eleve')
    if (! SHOW_CPT_DETAILS)
        $studentsShowDetails = False;
// affichage de la colonne "Classe" :
$studentsShowClassColumn  = True;
if ($_SESSION['USER_MODE'] == 'eleve')
    if (! SHOW_CLASS_COLUMN)
        $studentsShowClassColumn  = False;
// affichage des notes :
$studentsShowNotes  = True;
if ($_SESSION['USER_MODE'] == 'eleve')
    if ((! SHOW_NOTES) and ($_SESSION['SELECTED_PERIOD'][0] == $_SESSION['ACTUAL_PERIOD']))
        $studentsShowNotes  = False;
$photosDir = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_PROTECTED.'photos'.DIRECTORY_SEPARATOR;
// TEMP_FILES servira à récupérer les fichiers temporaires des photos :
$_SESSION['TEMP_FILES'] = array();
$bigHeight = 600;



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = '';
$printTitle = '';
$fileDate = getDateResultsStudent($CONNEXION, $selectedStudent);
$nom_periode = getPeriodName($CONNEXION, $_SESSION['SELECTED_PERIOD'][0]);
$nom_eleve = getStudentName($CONNEXION, $selectedStudent, $mode='');
if ($PAGE == 'student_council') {
    $studentBirthday = $CONNEXION -> getStudentBirthday($selectedStudent);
    $studentBirthday = $studentBirthday[$selectedStudent];
    $pageTitle .= spaces(4).'<!-- PAGE HEADER -->'."\n";
    $pageTitle .= spaces(4).'<b>'.$nom_eleve.'</b><br/>'.$studentBirthday.'<br/>'."\n";
    $photo = $photosDir.$selectedStudent.'.jpeg';
    if (file_exists($photo)) {
        $tempFileName = 'tmp'.DIRECTORY_SEPARATOR.$_SESSION['USER_ID'].'-'.$selectedStudent.'.jpeg';
        if (! is_file(CHEMIN_VERAC.$tempFileName))
            copy($photo, CHEMIN_VERAC.$tempFileName);
        $photo = $tempFileName;
        $_SESSION['TEMP_FILES'][] = $photo;
        }
    else
        $photo = 'images/unknown-user.png';
    if ($_SESSION['WINDOW_HEIGHT'] > $bigHeight)
        $pageTitle .= spaces(4).'<img src="'.$photo.'" width="100px"><br/>'."\n";
    else
        $pageTitle .= spaces(4).'<img src="'.$photo.'" width="70px"><br/>'."\n";
    $printTitle = tr('Council');
    }
else {
    $pageTitle .= spaces(2).'<!-- PAGE HEADER -->'."\n";
    $pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
    $pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
    $pageTitle .= spaces(4).'<div class="page-header">'."\n";
    $pageTitle .= spaces(5).'<h2>'.$nom_eleve.' : '.tr('STATEMENT OF BALANCE IN ').$fileDate.'</h2>'."\n";
    $pageTitle .= spaces(4).'</div>'."\n";
    $pageTitle .= spaces(3).'</div>'."\n";
    $pageTitle .= spaces(2).'</div>'."\n";
    $printTitle = tr('Balances');
    }
$printTitle .= ' - '.$nom_periode;
if ($_SESSION['USER_MODE'] == 'eleve')
    $printTitle .= ' - '.$_SESSION['USER_CLASS'][1];
else
    $printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.getStudentName($CONNEXION, $selectedStudent);
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';



/****************************************************
    PARTIE COMMUNE
****************************************************/
$commun = '';
$idClasseInResults = idClassInResults($CONNEXION, $_SESSION['SELECTED_CLASS'][1]);
$sharedCompetences = sharedCompetences($CONNEXION, $_SESSION['SELECTED_CLASS'][0], $what='bulletin');
$results = studentResults(
    $CONNEXION, $selectedStudent, $idClasseInResults, $sharedCompetences);
if ($studentsShowDetails) {
    // détails des évaluations :
    $details = studentDetails(
        $CONNEXION, $selectedStudent, $idClasseInResults, $sharedCompetences);
    $detailsMessage = tr('Click to show/hide details.');
    $colspan = 3;
    }
$id_toggle = 0;
$inTable = False;
foreach ($results as $ROW) {
    $table = '';
    if ($ROW['Competence'] != '') {
        $id_toggle += 1;
        if (! $inTable) {
            $inTable = True;
            $table .= spaces(5).'<table border="1" class="bilan">'."\n";
            $table .= spaces(6).'<thead class="bilan">'."\n";
            $table .= spaces(7).'<tr>'."\n";
            $table .= spaces(8).'<th class="title"></th>'."\n";
            $table .= spaces(8).'<th class="student">'.tr('Student').'</th>'."\n";
            $table .= spaces(8).'<th class="group">'.tr('Class').'</th>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            $table .= spaces(6).'</thead>'."\n";
            $table .= spaces(6).'<tbody>'."\n";
            }
        if ($ROW['value'] != '') {
            if ($studentsShowDetails)
                $table .= spaces(7).'<tr class="detail_parent" title="'.$detailsMessage.'" id="'.$id_toggle.'">'."\n";
            else
                $table .= spaces(7).'<tr>'."\n";
            $table .= spaces(8).'<td class="competence_label">'.htmlspecialchars($ROW['Competence']).'</td>'."\n";
            $table .= spaces(8).'<td class="'.$ROW['value'].'">'.valeur2affichage($ROW['value']).'</td>'."\n";
            if ($studentsShowClassColumn)
                $table .= spaces(8).'<td class="'.$ROW['groupeValue'].'">'.valeur2affichage($ROW['groupeValue']).'</td>'."\n";
            else
                $table .= '<td></td>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            if ($studentsShowDetails) {
                // les lignes de détails (affichables par clic) :
                foreach ($details[$ROW['code']] as $detail) {
                    $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                    $detailLabel = $subjectsLabels[$detail['matiere']][1];
                    $detailLabel .= ' ('.$teachersNames[$detail['id_prof']][0].')';
                    $table .= spaces(8).'<td class="detail_label">'.$detailLabel.'</td>'."\n";
                    $table .= spaces(8).'<td class="'.$detail['value'].'">'.valeur2affichage($detail['value']).'</td>'."\n";
                    $table .= spaces(8).'<td></td>'."\n";
                    $table .= spaces(7).'</tr>'."\n";
                    }
                $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                $table .= spaces(8).'<td class="interligne" colspan="'.$colspan.'"></td>'."\n";
                $table .= spaces(7).'</tr>'."\n";
                }
            }
        else {
            $table .= spaces(7).'<tr>'."\n";
            $table .= spaces(8).'<td class="competence_label">'.htmlspecialchars($ROW['Competence']).'</td>'."\n";
            $table .= spaces(8).'<td class="'.$ROW['value'].'">'.valeur2affichage($ROW['value']).'</td>'."\n";
            if ($studentsShowClassColumn)
                $table .= spaces(8).'<td class="'.$ROW['groupeValue'].'">'.valeur2affichage($ROW['groupeValue']).'</td>'."\n";
            else
                $table .= '<td></td>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            }
        }
    else {
        if ($inTable) {
            $table .= spaces(6).'</tbody>'."\n";
            $table .= spaces(5).'</table>'."\n";
            $inTable = False;
            }
        if ($ROW['Titre1'] != '')
            $table .= spaces(5).'<h3>'.htmlspecialchars($ROW['Titre1']).'</h3>'."\n";
        elseif ($ROW['Titre2'] != '')
            $table .= spaces(5).'<h4>'.htmlspecialchars($ROW['Titre2']).'</h4>'."\n";
        elseif ($ROW['Titre3'] != '') {
            $inTable = True;
            $table .= spaces(5).'<table border="1" class="bilan">'."\n";
            $table .= spaces(6).'<thead class="bilan">'."\n";
            $table .= spaces(7).'<tr>'."\n";
            $table .= spaces(8).'<th class="title"><b>'.htmlspecialchars($ROW['Titre3']).'</b></th>'."\n";
            $table .= spaces(8).'<th class="student">'.tr('Student').'</th>'."\n";
            $table .= spaces(8).'<th class="group">'.tr('Class').'</th>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            $table .= spaces(6).'</thead>'."\n";
            $table .= spaces(6).'<tbody>'."\n";
            }
        }
    $commun .= $table;
    }
if ($inTable) {
    $commun .= spaces(6).'</tbody>'."\n";
    $commun .= spaces(5).'</table>'."\n";
    $inTable = False;
    }



/****************************************************
    PARTIE DISCIPLINAIRE
****************************************************/
$discipline = '';
$lastTeacherName = '';
foreach ($subjects['Bulletin'] as $MATIERE_ROW)
    if ($MATIERE_ROW['OrdreBLT'] > 0) {
        $matiereCode = $MATIERE_ROW['MatiereCode'];
        $matiereProfs = array();
        $results = studentResultsInSubject($CONNEXION, $selectedStudent, $matiereCode, $limite);
        // récupération des évaluations :
        foreach ($results as $ROW) {
            $id_prof = $ROW['id_prof'];
            if (! array_key_exists($id_prof, $matiereProfs)) {
                $matiereProfs[$id_prof] = array();
                $matiereProfs[$id_prof]['resultats'] = '';
                $matiereProfs[$id_prof]['notes'] = '';
                $matiereProfs[$id_prof]['appreciations'] = '';
                }
            $label = $ROW['label'];
            $value = $ROW['value'];
            $groupeValue = $ROW['groupeValue'];
            $resultats = spaces(7).'<tr>'."\n";
            $resultats .= spaces(8).'<td class="competence_label">'.htmlspecialchars($label).'</td>'."\n";
            $resultats .= spaces(8).'<td class="'.$value.'">'.valeur2affichage($value).'</td>'."\n";
            if ($studentsShowClassColumn)
                $resultats .= spaces(8).'<td class="'.$groupeValue.'">'.valeur2affichage($groupeValue).'</td>'."\n";
            else
                $resultats .= spaces(8).'<td></td>'."\n";
            $resultats .= spaces(7).'</tr>'."\n";
            $matiereProfs[$id_prof]['resultats'] .= $resultats;
            }
        // récupération des notes :
        if (isset($notes[$matiereCode]))
            foreach ($notes[$matiereCode] as $id_prof => $ROW) {
                if (! array_key_exists($id_prof, $matiereProfs)) {
                    $matiereProfs[$id_prof] = array();
                    $matiereProfs[$id_prof]['resultats'] = '';
                    $matiereProfs[$id_prof]['notes'] = '';
                    $matiereProfs[$id_prof]['appreciations'] = '';
                    }
                $note = spaces(7).'<tr>'."\n";
                $note .= spaces(8).'<td align="right"><b>'.tr('Average rating:').' </b></td>'."\n";
                $note .= spaces(8).'<td align="center"><b>'.$ROW[0].'</b></td>'."\n";
                $note .= spaces(8).'<td align="center"><b>'.$ROW[1].'</b></td>'."\n";
                $note .= spaces(7).'</tr>'."\n";
                $notes_ligne = tr('standard deviation:').' '.$ROW[2];
                $notes_ligne .= htmlSpaces(10);
                $notes_ligne .= tr('minimum rating:').' '.$ROW[3];
                $notes_ligne .= htmlSpaces(10);
                $notes_ligne .= tr('maximum rating:').' '.$ROW[4];
                $note .= spaces(7).'<tr><td align="right" colspan="3" class="notes">'.$notes_ligne.'</td></tr>'."\n";
                if ($studentsShowNotes)
                    $matiereProfs[$id_prof]['notes'] .= $note;
                }
        // récupération des appréciations :
        if (count($appreciations) > 0)
            if (isset($appreciations[$matiereCode]))
                foreach ($appreciations[$matiereCode] as $id_prof => $ROW) {
                    if (! array_key_exists($id_prof, $matiereProfs)) {
                        $matiereProfs[$id_prof] = array();
                        $matiereProfs[$id_prof]['resultats'] = '';
                        $matiereProfs[$id_prof]['notes'] = '';
                        $matiereProfs[$id_prof]['appreciations'] = '';
                        }
                    $appreciation = spaces(7).'<tr><td colspan="3" class="appreciation">'.nl2br(htmlspecialchars($appreciations[$matiereCode][$id_prof])).'</td></tr>'."\n";
                    $matiereProfs[$id_prof]['appreciations'] .= $appreciation;
                    }
        foreach ($matiereProfs as $id_prof => $ROW) {
            $teacherName = $teachersNames[$id_prof][1];
            if ($lastTeacherName != '')
                if ($teacherName == $lastTeacherName)
                    $teacherName = '';
            if ($teacherName != '') {
                $lastTeacherName = $teacherName;
                $titre = spaces(8).'<th class="title">'.htmlspecialchars($MATIERE_ROW['MatiereLabel']).htmlSpaces(5).htmlspecialchars($teacherName).'</th>'."\n";
                }
            else
                $titre = spaces(8).'<th class="title">'.htmlspecialchars($MATIERE_ROW['MatiereLabel']).'</th>'."\n";
            $table = spaces(5).'<table border="1" class="bilan">'."\n";
            $table .= spaces(6).'<thead class="bilan">'."\n";
            $table .= spaces(7).'<tr>'."\n";
            $table .= $titre;
            $table .= spaces(8).'<th class="student">'.tr('Student').'</th>'."\n";
            $table .= spaces(8).'<th class="group">'.tr('Class').'</th>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            $table .= spaces(6).'</thead>'."\n";
            $table .= spaces(6).'<tbody>'."\n";
            $table .= $ROW['resultats'];
            $table .= $ROW['notes'];
            $table .= $ROW['appreciations'];
            $table .= spaces(6).'</tbody>'."\n";
            $table .= spaces(5).'</table>'."\n";
            $discipline .= $table;
            $discipline .= spaces(5).'<p></p>'."\n";;
            }
        }



/****************************************************
    VIE SCOLAIRE
****************************************************/
$vie_sco = '';
$matiereCode = 'VS';
$matiereLabel = 'Vie scolaire';
foreach ($subjects['Speciales'] as $MATIERE_ROW)
    if ($MATIERE_ROW['MatiereCode'] == 'VS')
        $matiereLabel = $MATIERE_ROW['MatiereLabel'];
$data = array('absences' => '', 'resultats' => '', 'notes' => '', 'appreciation' => '');
// absences :
$absencesData = studentAbsences($CONNEXION, $selectedStudent);
$absences = '';
if ($absencesData['abs0'] > 0) {
    $label = readConfig($CONNEXION, 'absences-01');
    $label = $label['TXT'];
    if ($label == '') {
        if ($absencesData['version_db'] < 8)
            $label = tr('Number of half-days absence');
        else
            $label = tr('Number of half-days of justified absence');
        }
    $absences .= $label.' : '.$absencesData['abs0'];
    }
if ($absencesData['abs1'] > 0) {
    $label = readConfig($CONNEXION, 'absences-11');
    $label = $label['TXT'];
    if ($label == '') {
        if ($absencesData['version_db'] < 8)
            $label = tr('Number of delays');
        else
            $label = tr('Number of half-days of unjustified absence');
        }
    if ($absences != '')
        $absences .= htmlSpaces(10);
    $absences .= $label.' : '.$absencesData['abs1'];
    }
$absences2 = '';
if ($absencesData['abs2'] > 0) {
    $label = readConfig($CONNEXION, 'absences-21');
    $label = $label['TXT'];
    if ($label == '')
        $label = tr('Number of delays');
    $absences2 .= $label.' : '.$absencesData['abs2'];
    }
if ($absencesData['abs3'] > 0) {
    $label = readConfig($CONNEXION, 'absences-31');
    $label = $label['TXT'];
    if ($label == '')
        $label = tr('Number of classroom hours missed');
    if ($absences2 != '')
        $absences2 .= htmlSpaces(10);
    $absences2 .= $label.' : '.$absencesData['abs3'];
    }
if ($absences != '') {
    if ($absences2 != '')
        $absences .= '<br/>'.$absences2;
    }
elseif ($absences2 != '')
    $absences .= $absences2;
if ($absences != '')
    $absences = spaces(7).'<tr><td align="right" colspan="3" class="competence_label">'.$absences.'</td></tr>'."\n";
$data['absences'] = $absences;
// résultats :
$results = studentResultsInSubject($CONNEXION, $selectedStudent, $matiereCode, $limite);
foreach ($results as $ROW) {
    $label = $ROW['label'];
    $value = $ROW['value'];
    $groupeValue = $ROW['groupeValue'];
    $resultats = spaces(7).'<tr>'."\n";
    $resultats .= spaces(8).'<td class="competence_label">'.htmlspecialchars($label).'</td>'."\n";
    $resultats .= spaces(8).'<td class="'.$value.'">'.valeur2affichage($value).'</td>'."\n";
    if ($studentsShowClassColumn)
        $resultats .= spaces(8).'<td class="'.$groupeValue.'">'.valeur2affichage($groupeValue).'</td>'."\n";
    else
        $resultats .= spaces(8).'<td></td>'."\n";
    $resultats .= spaces(7).'</tr>'."\n";
    $data['resultats'] .= $resultats;
    }
// récupération des notes :
if (isset($notes[$matiereCode]))
    foreach ($notes[$matiereCode] as $id_CPE => $ROW) {
        $note = spaces(7).'<tr>'."\n";
        $note .= spaces(8).'<td class="competence_label">'.tr('Rating of school life').'</td>'."\n";
        $note .= spaces(8).'<td align="center"><b>'.$ROW[0].'</b></td>'."\n";
        $note .= spaces(8).'<td align="center"><b>'.$ROW[1].'</b></td>'."\n";
        $note .= spaces(7).'</tr>'."\n";
        if ($studentsShowNotes)
            $data['notes'] .= $note;
        }
// appréciation :
if (isset($appreciations[$matiereCode])) {
    foreach ($appreciations[$matiereCode] as $id_CPE => $appreciation)
        $appreciation = (htmlspecialchars($appreciation));
    $data['appreciation'] = spaces(7).'<tr><td colspan="3" class="appreciation">'.nl2br($appreciation).'</td></tr>'."\n";
    }
// affichage :
$vie_sco .= spaces(5).'<h3>'.tr('SCHOOL LIFE').'</h3>'."\n";
$vie_sco .= spaces(5).'<table border="1" class="bilan">'."\n";
$vie_sco .= spaces(6).'<thead class="bilan">'."\n";
$vie_sco .= spaces(7).'<tr>'."\n";
$vie_sco .= spaces(8).'<th class="title">'.htmlspecialchars($matiereLabel).'</th>'."\n";
$vie_sco .= spaces(8).'<th class="student">'.tr('Student').'</th>'."\n";
$vie_sco .= spaces(8).'<th class="group">'.tr('Class').'</th>'."\n";
$vie_sco .= spaces(7).'</tr>'."\n";
$vie_sco .= spaces(6).'</thead>'."\n";
$vie_sco .= spaces(6).'<tbody>'."\n";
$vie_sco .= $data['resultats'];
$vie_sco .= $data['notes'];
$vie_sco .= $data['absences'];
$vie_sco .= $data['appreciation'];
$vie_sco .= spaces(6).'</tbody>'."\n";
$vie_sco .= spaces(5).'</table>'."\n";



/****************************************************
    COMPOSANTES DU SOCLE
****************************************************/
$socle = '';
if ($PAGE == 'student_council') {
    $SOCLE_COMPONENTS = getSocleComponents(
        $CONNEXION, $_SESSION['SELECTED_CLASS'][0]);
    $studentComponents = studentComponents(
        $CONNEXION, $selectedStudent, $SOCLE_COMPONENTS);

    $socle .= spaces(5).'<h3>'.tr('COMPONENTS OF THE SOCLE').'</h3>'."\n";
    $socle .= spaces(5).'<table border="1" class="bilan">'."\n";
    $socle .= spaces(6).'<col width=85%>'."\n";
    $socle .= spaces(6).'<col width=15%>'."\n";
    $socle .= spaces(6).'<tbody>'."\n";
    foreach ($SOCLE_COMPONENTS as $code => $row) {
        $title = tr('LSU_'.$code);
        $value = $studentComponents[$code];
        $socle .= spaces(7).'<tr>'."\n";
        $socle .= spaces(8).'<td class="competence_label">'.$title.'</td>'."\n";
        $socle .= spaces(8).'<td class="'.$value.'">'.valeur2affichage($value).'</td>'."\n";
        $socle .= spaces(7).'</tr>'."\n";
        }
    $socle .= spaces(6).'</tbody>'."\n";
    $socle .= spaces(5).'</table>'."\n";
    }



/****************************************************
    PROF PRINCIPAL
****************************************************/
$synthese = '';
$appreciation = '';
$data = array('appreciation' => '');
$editSynthese = '';
$matiereCode = 'PP';
$matiereLabel = 'Prof Principal';
foreach ($subjects['Speciales'] as $MATIERE_ROW)
    if ($MATIERE_ROW['MatiereCode'] == 'PP')
        $matiereLabel = $MATIERE_ROW['MatiereLabel'];
$id_PP = -1;
if (array_key_exists($matiereCode, $idsProfsStudent))
    $id_PP = $idsProfsStudent[$matiereCode][0];
//debugToConsole('student_balances.idsProfsStudent '.$id_PP);
// on cherche le PP si on ne le connait pas encore (pas d'appréciation)
if (($id_PP < 0) and ($_SESSION['USER_MODE'] !='eleve'))
    $id_PP = $CONNEXION -> find_PP($selectedStudent, $_SESSION['USER_ID'], $matiereCode);
//debugToConsole('student_balances.find_PP '.$id_PP);

if (array_key_exists($matiereCode, $appreciations)) {
    foreach ($appreciations[$matiereCode] as $id_PP2 => $appreciation)
        $appreciation = (htmlspecialchars($appreciation));
    }
// on ne peut modifier l'appréciation qu'en vue conseil et si c'est le PP ou le directeur :
$isEditable = testEditable($CONNEXION, $PAGE, $id_PP);
if ($isEditable) {
    // on lit l'appréciation directement dans le fichier du PP :
    $appreciationPP = appreciationPP($CONNEXION, $selectedStudent, $matiereCode, $id_PP);
    if ($appreciationPP != '')
        $appreciation = (htmlspecialchars($appreciationPP));
    // boutons (Éditer - Fermer Enregistrer) sous l'appréciation :
    $editSynthese .= spaces(5).'<!-- Buttons edit-close-save -->'."\n";
    $editSynthese .= spaces(5).'<div align="right">'."\n";
    $editSynthese .= spaces(6).'<button id="button-edit" type="button" class="btn btn-primary btn-sm">'."\n";
    $editSynthese .= spaces(7).tr('Edit synthesis')."\n";
    $editSynthese .= spaces(6).'</button>'."\n";
    $editSynthese .= spaces(6).'<button id="button-close" type="button" class="btn btn-light btn-sm collapse">'."\n";
    $editSynthese .= spaces(7).tr('Close')."\n";
    $editSynthese .= spaces(6).'</button>'."\n";
    $editSynthese .= spaces(6).'<button id="button-save" type="button" class="btn btn-primary btn-sm collapse" data-loading-text="'.tr('Saving...').'">'."\n";
    $editSynthese .= spaces(7).tr('Save changes')."\n";
    $editSynthese .= spaces(6).'</button>'."\n";
    $editSynthese .= spaces(5).'</div>'."\n";
    }
// affichage de l'appréciation :
if ($PAGE == 'student_balances')
    $synthese .= spaces(5).'<h3>'.tr('SYNTHESIS').'</h3>'."\n";
$synthese .= spaces(5).'<table border="1" class="bilan">'."\n";
$synthese .= spaces(6).'<thead class="bilan">'."\n";
$synthese .= spaces(7).'<tr>'."\n";
$synthese .= spaces(8).'<th class="title">'.htmlspecialchars($matiereLabel).'</th>'."\n";
$synthese .= spaces(7).'</tr>'."\n";
$synthese .= spaces(6).'</thead>'."\n";
$synthese .= spaces(6).'<tbody>'."\n";
$synthese .= spaces(7).'<tr>'."\n";
if ($isEditable) {
    $data = ' id="synthese"';
    $data .= ' id_pp="'.$id_PP.'"';
    $data .= ' matiere_code="'.$matiereCode.'"';
    }
else
    $data = '';
$synthese .= spaces(8).'<td class="appreciation"'.$data.'>'.nl2br($appreciation).'</td>'."\n";
$synthese .= spaces(7).'</tr>'."\n";
if ($isEditable) {
    $synthese .= spaces(7).'<tr id="synthese-edit" class="collapse">'."\n";
    $synthese .= spaces(8).'<td>'."\n";
    $synthese .= spaces(9).'<textarea class="form-control" id="appreciationPP" rows="5">'."\n";
    $synthese .= $appreciation.'</textarea>'."\n";
    $synthese .= spaces(8).'</td>'."\n";
    $synthese .= spaces(7).'</tr>'."\n";
    if (VERSION_NAME == 'demo')
        $pageTitle .= notInDemoMessage();
    }
$synthese .= spaces(6).'</tbody>'."\n";
$synthese .= spaces(5).'</table>'."\n";



/****************************************************
    DONNÉES POUR CAMEMBERT ET RADAR
****************************************************/
// récupération des données pour l'affichage du camembert :
$camembertData = getStudentNumberForEachValue($CONNEXION, $selectedStudent);
// récupération des données pour l'affichage du radar :
$sousRubriques = sousRubriques($CONNEXION, 'bulletin');
$allSubjects = array();
foreach (array('Bulletin', 'Speciales', 'Autre') as $where) {
    foreach ($subjects[$where] as $subject)
        $allSubjects[] = $subject;
    }
$radarData = studentResultsInSousRubriques(
    $CONNEXION, $selectedStudent, $allSubjects, $sousRubriques);
// en vue conseil, données des différentes périodes :
if ($PAGE == 'student_council') {
    $periodsForModal = listPeriodsForModal(
        $CONNEXION, $min=1, $max=$_SESSION['SELECTED_PERIOD'][0]);
    $camembertDatas = array();
    foreach ($periodsForModal as $period)
        $camembertDatas[$period[0]] = getStudentNumberForEachValue(
            $CONNEXIONS[$period[0]], $selectedStudent);
    $radarDatas = array();
    foreach ($periodsForModal as $period)
        $radarDatas[$period[0]] = studentResultsInSousRubriques(
            $CONNEXIONS[$period[0]], $selectedStudent, $allSubjects, $sousRubriques);
    }

$colors_args = '';
$rgb = hex2Rgb(COLOR_A);
$colors_args .= '&amp;COLOR_AR='.$rgb[0].'&amp;COLOR_AG='.$rgb[1].'&amp;COLOR_AB='.$rgb[2];
$rgb = hex2Rgb(COLOR_B);
$colors_args .= '&amp;COLOR_BR='.$rgb[0].'&amp;COLOR_BG='.$rgb[1].'&amp;COLOR_BB='.$rgb[2];
$rgb = hex2Rgb(COLOR_C);
$colors_args .= '&amp;COLOR_CR='.$rgb[0].'&amp;COLOR_CG='.$rgb[1].'&amp;COLOR_CB='.$rgb[2];
$rgb = hex2Rgb(COLOR_D);
$colors_args .= '&amp;COLOR_DR='.$rgb[0].'&amp;COLOR_DG='.$rgb[1].'&amp;COLOR_DB='.$rgb[2];
$rgb = hex2Rgb(COLOR_X);
$colors_args .= '&amp;COLOR_XR='.$rgb[0].'&amp;COLOR_XG='.$rgb[1].'&amp;COLOR_XB='.$rgb[2];

// les données pour javascript :
$n = 5;
$jsData = spaces($n).'<script type="text/javascript">'."\n";
$jsData .= spaces($n + 1).'// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later'."\n";
// données pour le camembert :
$jsData .= spaces($n + 1).'var camembertData = ['."\n";
foreach ($camembertData as $key => $value) {
    if ($key == 'vide')
        $label = tr('Unevaluated');
    else
        $label = tr('Number of ').valeur2affichage($key);
    if ($key == 'A')
        $color = COLOR_A;
    elseif ($key == 'B')
        $color = COLOR_B;
    elseif ($key == 'C')
        $color = COLOR_C;
    elseif ($key == 'D')
        $color = COLOR_D;
    elseif ($key == 'X')
        $color = COLOR_X;
    else
        $color = '#f3f3f3';
    $rgb = hex2Rgb($color);
    for ($j = 0; $j < 3; $j++) {
        $rgb[$j] -= 15;
        if ($rgb[$j] < 0)
            $rgb[$j] = 0;
        }
    $highlight = rgb2Hex($rgb);
    $jsData .= spaces($n + 2).'{value: '.$value.', color:"'.$color.'", highlight: "'.$highlight.'", label: "'.$label.'"},'."\n";
    }
$jsData .= spaces($n + 2).'];'."\n";
// pour la fenêtre modale :
if ($PAGE == 'student_council') {
    $jsData .= spaces($n + 1).'var selectedPeriod = '.$_SESSION['SELECTED_PERIOD'][0].';'."\n";
    $jsData .= spaces($n + 1).'var camembertDatas = {'."\n";
    foreach ($periodsForModal as $period) {
        // données pour le camembert :
        $jsData .= spaces($n + 2).$period[0].': ['."\n";
        foreach ($camembertDatas[$period[0]] as $key => $value) {
            if ($key == 'vide')
                $label = tr('Unevaluated');
            else
                $label = tr('Number of ').valeur2affichage($key);
            if ($key == 'A')
                $color = COLOR_A;
            elseif ($key == 'B')
                $color = COLOR_B;
            elseif ($key == 'C')
                $color = COLOR_C;
            elseif ($key == 'D')
                $color = COLOR_D;
            elseif ($key == 'X')
                $color = COLOR_X;
            else
                $color = '#f3f3f3';
            $rgb = hex2Rgb($color);
            for ($j = 0; $j < 3; $j++) {
                $rgb[$j] -= 15;
                if ($rgb[$j] < 0)
                    $rgb[$j] = 0;
                }
            $highlight = rgb2Hex($rgb);
            $jsData .= spaces($n + 3).'{value: '.$value.', color:"'.$color.'", highlight: "'.$highlight.'", label: "'.$label.'"},'."\n";
            }
        $jsData .= spaces($n + 3).'],'."\n";
        }
    $jsData .= spaces($n + 2).'};'."\n";
    }

// données pour le radar :
$jsRadarA = spaces($n + 4).'noToolTip: true,'."\n";
$jsRadarA .= spaces($n + 4).'strokeColor: "'.COLOR_A.'",'."\n";
$jsRadarA .= spaces($n + 4).'fillColor: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarA .= spaces($n + 4).'pointColor: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarA .= spaces($n + 4).'pointDot : false,'."\n";
$jsRadarA .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarA .= spaces($n + 4).'pointHighlightFill: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarA .= spaces($n + 4).'pointHighlightStroke: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarB = spaces($n + 4).'noToolTip: true,'."\n";
$jsRadarB .= spaces($n + 4).'strokeColor: "'.COLOR_B.'",'."\n";
$jsRadarB .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarC = spaces($n + 4).'noToolTip: true,'."\n";
$jsRadarC .= spaces($n + 4).'strokeColor: "'.COLOR_C.'",'."\n";
$jsRadarC .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarD = spaces($n + 4).'noToolTip: true,'."\n";
$jsRadarD .= spaces($n + 4).'strokeColor: "'.COLOR_D.'",'."\n";
$jsRadarD .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsRadarDatas = spaces($n + 4).'fillColor: "rgba(151,187,205,0.3)",'."\n";
$jsRadarDatas .= spaces($n + 4).'strokeColor: "rgba(151,187,205,1)",'."\n";
$jsRadarDatas .= spaces($n + 4).'pointColor: "rgba(151,187,205,1)",'."\n";

$jsData .= spaces($n + 1).'var radarData = {'."\n";
$radarCount = 0;
$jsData .= spaces($n + 2).'labels: [';
foreach ($radarData as $ROW)
    if (in_array($ROW['value'], array('A', 'B', 'C', 'D'))) {
        $jsData .= '"'.$ROW['name'].'",';
        $radarCount++;
        }
$jsData .= '],'."\n";
$jsData .= spaces($n + 2).'datasets: ['."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= $jsRadarA;
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '100,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= $jsRadarB;
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '75,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= $jsRadarC;
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '50,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= $jsRadarD;
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '25,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= $jsRadarDatas;
$jsData .= spaces($n + 4).'data: [';
foreach ($radarData as $ROW) {
    if ($ROW['value'] == 'A')
        $jsData .= '100,';
    elseif ($ROW['value'] == 'B')
        $jsData .= '75,';
    elseif ($ROW['value'] == 'C')
        $jsData .= '50,';
    elseif ($ROW['value'] == 'D')
        $jsData .= '25,';
    }
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'],'."\n";
$jsData .= spaces($n + 2).'};'."\n";
// pour la fenêtre modale :
if ($PAGE == 'student_council') {
    $jsData .= spaces($n + 1).'var radarDatas = {'."\n";
    foreach ($periodsForModal as $period) {
        // données pour le radar :
        $jsData .= spaces($n + 2).$period[0].': {'."\n";

        $radarCount = 0;
        $jsData .= spaces($n + 2).'labels: [';
        foreach ($radarDatas[$period[0]] as $ROW)
            if (in_array($ROW['value'], array('A', 'B', 'C', 'D'))) {
                $jsData .= '"'.$ROW['name'].'",';
                $radarCount++;
                }
        $jsData .= '],'."\n";
        $jsData .= spaces($n + 2).'datasets: ['."\n";
        $jsData .= spaces($n + 3).'{'."\n";
        $jsData .= $jsRadarA;
        $jsData .= spaces($n + 4).'data: [';
        for ($i = 0; $i < $radarCount; $i++)
            $jsData .= '100,';
        $jsData .= '],'."\n";
        $jsData .= spaces($n + 3).'},'."\n";
        $jsData .= spaces($n + 3).'{'."\n";
        $jsData .= $jsRadarB;
        $jsData .= spaces($n + 4).'data: [';
        for ($i = 0; $i < $radarCount; $i++)
            $jsData .= '75,';
        $jsData .= '],'."\n";
        $jsData .= spaces($n + 3).'},'."\n";
        $jsData .= spaces($n + 3).'{'."\n";
        $jsData .= $jsRadarC;
        $jsData .= spaces($n + 4).'data: [';
        for ($i = 0; $i < $radarCount; $i++)
            $jsData .= '50,';
        $jsData .= '],'."\n";
        $jsData .= spaces($n + 3).'},'."\n";
        $jsData .= spaces($n + 3).'{'."\n";
        $jsData .= $jsRadarD;
        $jsData .= spaces($n + 4).'data: [';
        for ($i = 0; $i < $radarCount; $i++)
            $jsData .= '25,';
        $jsData .= '],'."\n";
        $jsData .= spaces($n + 3).'},'."\n";
        $jsData .= spaces($n + 3).'{'."\n";
        $jsData .= $jsRadarDatas;
        $jsData .= spaces($n + 4).'data: [';
        foreach ($radarDatas[$period[0]] as $ROW) {
            if ($ROW['value'] == 'A')
                $jsData .= '100,';
            elseif ($ROW['value'] == 'B')
                $jsData .= '75,';
            elseif ($ROW['value'] == 'C')
                $jsData .= '50,';
            elseif ($ROW['value'] == 'D')
                $jsData .= '25,';
            }
        $jsData .= '],'."\n";
        $jsData .= spaces($n + 3).'},'."\n";
        $jsData .= spaces($n + 3).'],'."\n";
        $jsData .= spaces($n + 3).'},'."\n";
        }
    $jsData .= spaces($n + 2).'};'."\n";
    }

$jsData .= spaces($n + 1).'// @license-end'."\n";
$jsData .= spaces($n).'</script>'."\n";

/****************************************************
    CALCUL DU CAMEMBERT
****************************************************/
$camembert = '';
// image du camembert :
if ($PAGE == 'student_council') {
    if ($_SESSION['WINDOW_HEIGHT'] > $bigHeight)
        $width = 180;
    else
        $width = 120;
    }
else
    $width = 300;
$camembertImage = '<canvas id="camembertChart" width="'.$width.'" height="'.$width.'"/></canvas>'."\n";
// calcul de la légende :
$labels = '';
$values = '';
foreach ($camembertData as $key => $value) {
    if ($key == 'vide')
        $labels .= '<br/>'.tr('Unevaluated').' : ';
    else
        $labels .= '<br/>'.tr('Number of ').valeur2affichage($key).' : ';
    $values .= '<br/><b>'.$value.'</b>';
    }
// mise en forme :
$n = 5;
$camembert .= spaces($n).'<br/>'."\n";
$camembert .= spaces($n).'<table align="center" border="0" width="70%">'."\n";
// le titre :
$camembert .= spaces($n + 1).'<tr>'."\n";
$camembert .= spaces($n + 2).'<td align="left"><h4>'.tr('Summary (camembert):').'</h4></td>'."\n";
$camembert .= spaces($n + 2).'<td></td>'."\n";
$camembert .= spaces($n + 1).'</tr>'."\n";
$camembert .= spaces($n + 1).'<tr>'."\n";
// le camembert en lui-même :
$camembert .= spaces($n + 2).'<td align="center">'."\n";
$camembert .= spaces($n + 3).$camembertImage."\n";
$camembert .= spaces($n + 2).'</td>'."\n";
// la légende :
$camembert .= spaces($n + 2).'<td align="center">'."\n";
$camembert .= spaces($n + 3).'<table align="center" border="0" width="200">'."\n";
$camembert .= spaces($n + 4).'<tr>'."\n";
$camembert .= spaces($n + 5).'<td align="left">'.$labels.'</td>'."\n";
$camembert .= spaces($n + 5).'<td align="right">'.$values.'</td>'."\n";
$camembert .= spaces($n + 4).'</tr>'."\n";
$camembert .= spaces($n + 3).'</table>'."\n";
$camembert .= spaces($n + 2).'</td>'."\n";
$camembert .= spaces($n + 1).'</tr>'."\n";
$camembert .= spaces($n).'</table>'."\n";

/****************************************************
    CALCUL DU RADAR
****************************************************/
$radar = '';
$radarImage = '';
// image du radar :
if ($radarCount > 0) {
    if ($PAGE == 'student_council') {
        if ($_SESSION['WINDOW_HEIGHT'] > $bigHeight)
            $width = 220;
        else
            $width = 160;
        }
    else
        $width = 340;
    }
$radarImage = '<canvas id="radarChart" width="'.$width.'" height="'.$width.'"/></canvas>'."\n";
// on récupère la liste des sous-rubriques du bulletin :
foreach ($sousRubriques as $sousRubrique)
    $subjectsLabels[$sousRubrique[0]] = array($sousRubrique[0], $sousRubrique[1]);
// mise en forme :
$n = 5;
$radar .= spaces($n).'<br/>'."\n";
$radar .= spaces($n).'<table align="center" border="0" width="70%">'."\n";
// le titre :
$radar .= spaces($n + 1).'<tr>'."\n";
$radar .= spaces($n + 2).'<td align="left"><h4>'.tr('By subject (radar):').'</h4></td>'."\n";
$radar .= spaces($n + 2).'<td></td>'."\n";
$radar .= spaces($n + 1).'</tr>'."\n";
$radar .= spaces($n + 1).'<tr>'."\n";
// le radar en lui-même :
$radar .= spaces($n + 2).'<td align="center">'."\n";
$radar .= spaces($n + 3).$radarImage."\n";
$radar .= spaces($n + 2).'</td>'."\n";
// la légende :
$radar .= spaces($n + 2).'<td align="center">'."\n";
$radar .= spaces($n + 3).'<table align="center" border="0" width="200">'."\n";
foreach ($radarData as $ROW)
    if ($ROW['value']) {
        $title = '';
        if (array_key_exists($ROW['name'], $subjectsLabels)) {
            $title = $subjectsLabels[$ROW['name']][1];
            }
        $radar .= spaces($n + 4).'<tr>'."\n";
        $radar .= spaces($n + 5).'<td class="with_title" title="'.$title.'" align="left">'.$ROW['name'].' : </td>'."\n";
        $radar .= spaces($n + 5).'<td align="center"><b>'.valeur2affichage($ROW['value']).'</b></td>'."\n";
        $radar .= spaces($n + 4).'</tr>'."\n";
        }
$radar .= spaces($n + 3).'</table>'."\n";
$radar .= spaces($n + 2).'</td>'."\n";
$radar .= spaces($n + 1).'</tr>'."\n";
$radar .= spaces($n).'</table>'."\n";

/****************************************************
    FENÊTRE MODALE POUR LES COMPARAISONS
****************************************************/
if ($PAGE == 'student_council') {
    $compareModal = "\n";
    $compareModal .= spaces(2).'<!-- Modal -->'."\n";
    $compareModal .= spaces(2).'<div class="my-modal" id="compareModal">'."\n";
    if (count($periodsForModal) < 3)
        $compareModal .= spaces(3).'<div class="modal-dialog modal-lg">'."\n";
    else
        $compareModal .= spaces(3).'<div class="modal-dialog modal-xl">'."\n";
    $compareModal .= spaces(4).'<div class="modal-content">'."\n";
    $compareModal .= spaces(5).'<div class="modal-header">'."\n";
    $compareModal .= spaces(6).'<h4 class="modal-title">'.tr('Changes in results').htmlSpaces(10).$nom_eleve.'</h4>'."\n";
    $compareModal .= spaces(6).'<button type="button" id="myModalClose" class="close">&times;</button>'."\n";
    $compareModal .= spaces(5).'</div>'."\n";
    $compareModal .= spaces(5).'<div class="modal-body" id="compareModalContent">'."\n";
    $compareModal .= spaces(6).'<table align="center"  border="1" width="95%">'."\n";
    $compareModal .= spaces(7).'<tr>'."\n";
    foreach ($periodsForModal as $period)
        $compareModal .= spaces(8).'<td align="center"><b>'.$period[1].'</b></td>'."\n";
    $compareModal .= spaces(7).'</tr>'."\n";
    $compareModal .= spaces(7).'<tr>'."\n";
    $width = $_SESSION['WINDOW_WIDTH'] / 8;
    foreach ($periodsForModal as $period) {
        $compareModal .= spaces(8).'<td align="center">'."\n";
        $compareModal .= spaces(9).'<canvas id="camembertChart'.$period[0].'" width="'.$width.'" height="'.$width.'"/></canvas>'."\n";
        $compareModal .= spaces(8).'</td>'."\n";
        }
    $compareModal .= spaces(7).'</tr>'."\n";
    $compareModal .= spaces(7).'<tr>'."\n";
    $width = $_SESSION['WINDOW_WIDTH'] / 6;
    foreach ($periodsForModal as $period) {
        $compareModal .= spaces(8).'<td align="center">'."\n";
        $compareModal .= spaces(9).'<canvas id="radarChart'.$period[0].'" width="'.$width.'" height="'.$width.'"/></canvas>'."\n";
        $compareModal .= spaces(8).'</td>'."\n";
        }
    $compareModal .= spaces(7).'</tr>'."\n";
    $compareModal .= spaces(6).'</table>'."\n";
    $compareModal .= spaces(5).'</div>'."\n";
    $compareModal .= spaces(4).'</div><!-- /.modal-content -->'."\n";
    $compareModal .= spaces(3).'</div><!-- /.modal-dialog -->'."\n";
    $compareModal .= spaces(2).'</div><!-- /.modal -->'."\n";
    }



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
// titre en cas d'impression :
$print = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$print .= spaces(2).'<div class="d-none d-print-block">'."\n";
$print .= spaces(3).'<p class="text-center">'.$printTitle.'</p>'."\n";
$print .= spaces(2).'</div>'."\n";
$result .= "\n".$print;
if ($PAGE == 'student_council') {
    // la partie de gauche :
    $result .= spaces(2).'<div id="conseil_left">'."\n";
    $result .= spaces(3).'<p align="center">'."\n";
    $result .= $pageTitle.$jsData.spaces(4).$camembertImage.spaces(4).'<br/>'.$radarImage."\n";
    $result .= spaces(3).'</p>'."\n";
    $result .= spaces(2).'</div>'."\n";
    // la synthèse du PP :
    $result .= spaces(4).'<div id="conseil_synthese">'."\n";
    $result .= $synthese.$editSynthese;
    $result .= spaces(4).'</div>'."\n";
    // le contenu :
    $result .= spaces(4).'<div id="conseil_affcontent">'."\n";
    $result .= $commun.$discipline.$vie_sco.$socle;
    $result .= spaces(4).'</div>'."\n";
    // fenêtre modale de comparaison :
    $result .= $compareModal;
    }
elseif ($PAGE == 'student_balances') {
    // titre :
    $result .= "\n".$pageTitle;
    $result .= spaces(2).'<div class="container theme-showcase">'."\n";
    $result .= $commun.$discipline.$vie_sco.$synthese.$jsData.$camembert.$radar;
    $result .= spaces(2).'</div>'."\n";
    }

// on vérifie si les élèves ont accès aux résultats de la période en cours :
if ($_SESSION['USER_MODE'] == 'eleve')
    if ((! SHOW_ACTUAL_PERIODE) and ($_SESSION['SELECTED_PERIOD'][0] >= $_SESSION['ACTUAL_PERIOD']))
        $result = "\n".$pageTitle.prohibitedPeriodMessage();

if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
