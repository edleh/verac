<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if ($PAGE == 'student_password') {
    if ($_SESSION['USER_MODE'] == 'eleve')
        $result = "\n".prohibitedMessage();
    // VÉRIFICATION DE LA SÉLECTION :
    if ($_SESSION['USER_MODE'] == 'prof') {
        if ($_SESSION['SELECTED_CLASS'][0] == -1)
            $result = "\n".message('info', 'Select a class.', '');
        elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
            $result = "\n".message('info', 'Select a student.', '');
            }
    }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $result = '';
    $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
    if ($PAGE == 'password') {
        // on vérifie la saisie du mot de passe actuel puis on le change :
        $password0 = isset($_POST['password0']) ? $_POST['password0'] : '';
        $password1 = isset($_POST['password1']) ? $_POST['password1'] : '';
        $user_id = $_SESSION['USER_ID'];
        $user_mode = $_SESSION['USER_MODE'];
        if (verifyPassword($CONNEXION, $password0, $user_id, $user_mode)) {
            if (VERSION_NAME == 'demo')
                $result = 'OK';
            elseif (changePassword($CONNEXION, $password1, $user_id, $user_mode))
                $result = 'OK';
            else
                $result = '';
            }
        else
            $result = 'PASSWORD';
        }
    elseif ($PAGE == 'student_password') {
        // on change le mot de passe :
        $password1 = isset($_POST['password1']) ? $_POST['password1'] : '';
        if (VERSION_NAME == 'demo')
            $result = 'OK';
        elseif (changePassword($CONNEXION, $password1, $_SESSION['SELECTED_STUDENT'][0], 'eleve'))
            $result = 'OK';
        else
            $result = '';
        }
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function verifyPassword(
        $CONNEXION, $password, $user_id, $user_mode) {
    /*
    pour vérifier le mot de passe saisi
    */
    $table = ($user_mode == 'eleve') ? 'eleves' : 'profs';
    $mdp_enc = doEncode($password);
    $SQL = 'SELECT COUNT(*) AS TOTAL FROM '.$table.' ';
    $SQL .= 'WHERE id=:id ';
    $SQL .= 'AND Mdp IN (:mdp1, :mdp256)';
    $OPT = array(
        ':id' => $user_id, 
        ':mdp1' => $mdp_enc[0], ':mdp256' => $mdp_enc[1]);
    $STMT = $CONNEXION -> DB_USERS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $RESULT = $STMT -> fetch();
    return ($RESULT['TOTAL'] > 0) ? True : False;
    }

function changePassword(
        $CONNEXION, $password, $user_id, $user_mode) {
    /*
    on écrit le nouveau mot de passe dans la table eleves ou profs
    */
    $result = False;
    $table = ($user_mode == 'eleve') ? 'eleves' : 'profs';
    $mdp_enc = doEncode($password, $algo='sha256');
    $SQL = 'UPDATE '.$table.' ';
    $SQL .= 'SET Mdp=:mdp ';
    $SQL .= 'WHERE id=:id';
    $OPT = array(':id' => $user_id, ':mdp' => $mdp_enc);
    $STMT = $CONNEXION -> DB_USERS -> prepare($SQL);
    try {
        $CONNEXION -> DB_USERS -> beginTransaction();
        $STMT -> execute($OPT);
        $CONNEXION -> DB_USERS -> commit();
        $result = True;
        }
    catch (PDOException $e) {
        $CONNEXION -> DB_USERS -> rollBack();
        }
    return $result;
    }



/****************************************************
    TITRE DU DOCUMENT
****************************************************/
$pageTitle = '';
$pageTitle .= spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(3).'<div class="page-header">'."\n";
if ($PAGE == 'password')
    $pageTitle .= spaces(4).'<h2>'.tr('CHANGE PASSWORD').'</h2><hr />'."\n";
elseif ($PAGE == 'student_password') {
    $CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
    $nom_eleve = getStudentName($CONNEXION, $_SESSION['SELECTED_STUDENT'][0]);
    $loginsPasswords = $CONNEXION -> getStudentLogin($_SESSION['SELECTED_STUDENT'][0]);
    $loginPassword = $loginsPasswords[$_SESSION['SELECTED_STUDENT'][0]];
    if (verifyPassword($CONNEXION, $loginPassword[1], $_SESSION['SELECTED_STUDENT'][0], 'eleve'))
        $changed = '<b>'.tr('The initial password has not been changed!').'</b>';
    else
        $changed = tr('The password has been changed.');
    $pageTitle .= spaces(4).'<h2>'.tr('CHANGE THE PASSWORD OF A STUDENT').'</h2>'."\n";
    $pageTitle .= spaces(4).'<h4><b>'.$nom_eleve.'</b></h4>'."\n";
    $pageTitle .= spaces(4).'<h4><b>'.tr('Login: ').'</b>'.$loginPassword[0].'</h4>'."\n";
    $pageTitle .= spaces(4).'<h4><b>'.tr('Initial password: ').'</b>'.$loginPassword[1].'</h4>'."\n";
    $pageTitle .= spaces(4).'<p>'.$changed.'</p><hr />'."\n";
    }
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";
if (VERSION_NAME == 'demo')
    $pageTitle .= notInDemoMessage();



/****************************************************
    CONTENU DE LA PAGE
****************************************************/
$pageContent = '';

// pour centrer l'affichage :
$pageContent .= spaces(2).'<div class="container theme-showcase" style="max-width: 450px;">'."\n";

if ($PAGE == 'password') {
    $pageContent .= spaces(3).'<div class="input-group">'."\n";
    $pageContent .= spaces(4).'<div class="input-group-prepend">'."\n";
    $pageContent .= spaces(5).'<span class="input-group-text" style="width: 200px;">'.tr('Current password:').'</span>'."\n";
    $pageContent .= spaces(4).'</div>'."\n";
    $pageContent .= spaces(4).'<label class="sr-only" for="password0"></label>'."\n";
    $pageContent .= spaces(4).'<input type="password" class="form-control" id="password0">'."\n";
    $pageContent .= spaces(3).'</div>'."\n";
    $pageContent .= spaces(3).'<div class="input-group">'."\n";
    $pageContent .= spaces(4).'<label></label>'."\n";
    $pageContent .= spaces(3).'</div>'."\n";
    }

$pageContent .= spaces(3).'<div class="input-group">'."\n";
$pageContent .= spaces(4).'<div class="input-group-prepend">'."\n";
$pageContent .= spaces(5).'<span class="input-group-text" style="width: 200px;">'.tr('New password:').'</span>'."\n";
$pageContent .= spaces(4).'</div>'."\n";
$pageContent .= spaces(4).'<label class="sr-only" for="password1"></label>'."\n";
$pageContent .= spaces(4).'<input type="password" class="form-control" id="password1">'."\n";
$pageContent .= spaces(3).'</div>'."\n";

$pageContent .= spaces(3).'<div class="input-group">'."\n";
$pageContent .= spaces(4).'<div class="input-group-prepend">'."\n";
$pageContent .= spaces(5).'<span class="input-group-text" style="width: 200px;">'.tr('Confirmation:').'</span>'."\n";
$pageContent .= spaces(4).'</div>'."\n";
$pageContent .= spaces(4).'<label class="sr-only" for="password2"></label>'."\n";
$pageContent .= spaces(4).'<input type="password" class="form-control" id="password2">'."\n";
$pageContent .= spaces(3).'</div>'."\n";

$pageContent .= spaces(3).'<div class="input-group">'."\n";
$pageContent .= spaces(4).'<label></label>'."\n";
$pageContent .= spaces(3).'</div>'."\n";

$pageContent .= spaces(3).'<button class="btn btn-primary btn-block" type="button" id="go" data-loading-text="'.tr('Change in progress ...').'">'."\n";
$pageContent .= spaces(4).tr('Change').' &raquo;'."\n";
$pageContent .= spaces(3).'</button>'."\n";

$pageContent .= spaces(3).'<!-- POUR LES MESSAGES AFFICHÉS VIA JAVASCRIPT -->'."\n";
$pageContent .= spaces(3).'<div class="message">'."\n";
$pageContent .= spaces(4).'<br/>'."\n";
$pageContent .= spaces(4).'<div class="hide" id="alert_msg"></div>'."\n";
$pageContent .= spaces(3).'</div>'."\n";
$pageContent .= spaces(3).'<label class="sr-only" id="msgNoCurrentPassword">'
    .tr('Enter the current password.').'</label>'."\n";
$pageContent .= spaces(3).'<label class="sr-only" id="msgNoNewPassword">'
    .tr('Enter a new password.').'</label>'."\n";
$pageContent .= spaces(3).'<label class="sr-only" id="msgNoConfirmation">'
    .tr('Confirm new password.').'</label>'."\n";
$pageContent .= spaces(3).'<label class="sr-only" id="msgNotMatch">'
    .tr('The new password does not match the confirmation.').'</label>'."\n";
$pageContent .= spaces(3).'<label class="sr-only" id="msgError">'
    .tr('There was a problem. The change could not be performed.').'</label>'."\n";
$pageContent .= spaces(3).'<label class="sr-only" id="msgOK">'.tr('OK').'</label>'."\n";
$pageContent .= spaces(3).'<label class="sr-only" id="msgBadPassword">'
    .tr('Error in the current password.').'</label>'."\n";

$pageContent .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle;
$result .= $pageContent;



if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
