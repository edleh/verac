<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



require_once('../libs/constantes.php');
require_once('../libs/fonctions_divers.php');
require_once('../libs/fonctions_session.php');
$fichier_constantes = CHEMIN_CONFIG.'constantes.php';
if (is_file($fichier_constantes))
    require_once($fichier_constantes);
else
    echo "Il y a eu un problème";

header('Content-type: text/html; charset=UTF-8');

createSession();

$result = '|-1|';
if (! empty($_POST)) {
    require_once('../'.VERAC_CONNEXION_OTHER);
    $CONNEXION = new CONNEXION_OTHER;
    // username and password sent from form 
    $login = ($_POST['login']) ? $_POST['login'] : '';
    $password = ($_POST['password']) ? $_POST['password'] : '';
    $filename = ($_POST['fileName']) ? $_POST['fileName'] : '';
    $fromDir = ($_POST['fromDir']) ? $_POST['fromDir'] : '';
    // PB DELAUNAY :
    $ext = ($_POST['ext']) ? $_POST['ext'] : '';

    if ($login and $password and $filename) {
        $id_prof = $CONNEXION -> verifyTeacherRemoteAccess($login, $password);
        $fichier = '';
        $fichierTmp = '';
        if ($id_prof > -1) {
            $fichier = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.'/verac'.$fromDir.$filename;
            // PB DELAUNAY :
            $fichierTmp = CHEMIN_VERAC.'tmp'.DIRECTORY_SEPARATOR.$filename.$ext;
            if (! is_file($fichierTmp))
                copy($fichier, $fichierTmp);
            }
        // activer en cas de besoin de debogage :
        //$result = '|'.$fichier.'|'.$fichierTmp.'|'.$ext.'|';
        $result = '||'.$fichierTmp.'|'.$ext.'|';
        }
    }
echo $result;

?>
