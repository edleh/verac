<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    PAS D'APPEL DIRECT DE CETTE PAGE
****************************************************/
if (! defined('VERAC'))
    exit;

?>

        <!-- TITRE ET LOGO -->
        <div class="row">
            <div class="col-lg-2">
                <a href="https://verac.tuxfamily.org" target="_blank" title="<?php echo tr('VÉRAC Web Site') ?>">
                <img src="images/logo.png" class="img-fluid" alt="Responsive image">
                </a>
            </div>
            <div class="col-lg-8">
                <h1 class="text-center"><strong>VÉRAC</strong></h1>
                <h4 class="text-center text-muted">
                    <?php echo appTitle() ?>
                </h4>
                <p class="text-center">
                    <a href="https://verac.tuxfamily.org" target="_blank">
                        <?php echo tr('VÉRAC Web Site') ?>
                    </a>
                </p>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <hr />
        <!-- MESSAGE ERREUR 404-->
        <h3 class="text-center"><?php echo tr('Page Not Found!') ?></h3>

        <div class="row">
            <div class="col-sm-3">
            </div>
            <div class="col-sm-6">
                <div class="well">
                    <p class="text-center"><?php echo tr('The page you requested can not be displayed.') ?></p>
                    <p class="text-center"><?php echo '('.$PAGE_ERROR.')' ?></p>
                </div>
            </div>
        </div>
