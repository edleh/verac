<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if ($PAGE == 'papers') {
    if (($_SESSION['USER_MODE'] == 'eleve') and (! SHOW_DOCUMENTS))
        $result = "\n".prohibitedMessage();
    }
elseif ($PAGE == 'student_papers') {
    if ($_SESSION['USER_MODE'] == 'eleve')
        $result = "\n".prohibitedMessage();
    // VÉRIFICATION DE LA SÉLECTION :
    if ($_SESSION['USER_MODE'] == 'prof') {
        if ($_SESSION['SELECTED_CLASS'][0] == -1)
            $result = "\n".message('info', 'Select a class.', '');
        elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
            $result = "\n".message('info', 'Select a student.', '');
            }
    }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function getDocuments($CONNEXION, $user_mode, $user_id=-1) {
    /*
    */
    if ($user_mode == 'eleve') {
        $table = 'eleves';
        $WHERE = 'WHERE id_eleve=-1 OR id_eleve=:id_eleve ';
        $OPT = array(':id_eleve' => $user_id);
        }
    else {
        $table = 'profs';
        $WHERE = ' ';
        $OPT = array();
        }
    $SQL = 'SELECT * FROM '.$table.' ';
    $SQL .= $WHERE;
    $SQL .= 'ORDER BY type, labelDocument';
    $STMT = $CONNEXION -> DB_DOCUMENT -> prepare($SQL);
    $STMT -> execute($OPT);
    return ($STMT!='') ? $STMT -> fetchAll(): array();
    }

function getExtensionImage($fileName) {
    /*
    
    */
    $extension = strtolower(substr($fileName, strrpos($fileName, '.') + 1));
    $extensionImage = './images/extension-'.$extension.'.png';
    if (! is_file($extensionImage))
        $extensionImage = './images/extension-unknown.png';
    return array($extensionImage, $extension);
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];

$documentsPath = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_PROTECTED;
$documentsDir = '';
if ($PAGE == 'papers') {
    if ($_SESSION['USER_MODE'] == 'prof')
        $documentsDir = 'docsprofs';
    elseif ($_SESSION['USER_MODE'] == 'eleve')
        $documentsDir = 'documents';
    $documentsPath .= $documentsDir.DIRECTORY_SEPARATOR;
    $documents = getDocuments($CONNEXION, $_SESSION['USER_MODE'], $_SESSION['USER_ID']);
    }
elseif ($PAGE == 'student_papers') {
    $documentsDir = 'documents';
    $documentsPath .= $documentsDir.DIRECTORY_SEPARATOR;
    $documents = getDocuments($CONNEXION, 'eleve', $_SESSION['SELECTED_STUDENT'][0]);
    }

// on crée les lignes du tableau
$documentsLines = array(0 => '', 1 => '', 2 => '');
$first = True;
$downloadTitle = tr('Click to download the document.');
$noFileTitle = tr('File moved, renamed, or nonexistent');
foreach ($documents as $document) {
    $fileName = $document['nomFichier'];
    $label = $document['labelDocument'];
    if ($label == '')
        $label = $fileName;
    $type = $document['type'];
    $extension = getExtensionImage($fileName);
    if (file_exists($documentsPath.$fileName)) {
        $linkBegin = '<a href="pages/papers_download.php?f='.$fileName.'" target="_blank">';
        $linkEnd = '</a>';
        $docTitle = $downloadTitle;
        }
    else {
        $linkBegin = '';
        $linkEnd = '';
        $docTitle = $noFileTitle;
        }
    $sortie = spaces(7).'<tr>'."\n";
        $sortie .= spaces(8).'<td width="90%" align="left" title="'.$docTitle.'">'.$linkBegin.$label.$linkEnd.'</td>'."\n";
        $sortie .= spaces(8).'<td align="center">'."\n";
        $sortie .= spaces(9).'<img src="'.$extension[0].'" width="24px" title="'.$extension[1]
            .'" alt="extension"/>'."\n";
        $sortie .= spaces(8).'</td>'."\n";
    $sortie .= spaces(7).'</tr>'."\n";
    $documentsLines[$type] .= $sortie;
    }



/****************************************************
    TITRE DU DOCUMENT
****************************************************/
$pageTitle = '';
$pageTitle .= spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(3).'<div class="page-header">'."\n";
$pageTitle .= spaces(4).'<h2>'.tr('Documents available').'</h2><hr />'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";



/****************************************************
    CONTENU DE LA PAGE
****************************************************/
if (($PAGE == 'papers') and ($_SESSION['USER_MODE'] == 'prof'))
    $disposition = '<div class="col-sm-4">';
else
    $disposition = '<div class="col-sm-6">';
$pageContent = '';
$pageContent .= spaces(2).'<div class="row">'."\n";

// résultats :
$pageContent .= spaces(3).$disposition."\n";
$panel = '';
$panel .= spaces(2).'<div class="card">'."\n";
$panel .= spaces(3).'<div class="card-header"><b>'.tr('Results').'</b></div>'."\n";
$panel .= spaces(3).'<table class="table">'."\n";
$panel .= spaces(4).'<tbody>'."\n";
$panel .= $documentsLines[1];
$panel .= spaces(4).'</tbody>'."\n";
$panel .= spaces(3).'</table>'."\n";
$panel .= spaces(2).'</div>'."\n";
$pageContent .= $panel;
$pageContent .= spaces(3).'</div>'."\n";

// documents confidentiels :
if (($PAGE == 'papers') and ($_SESSION['USER_MODE'] == 'prof')) {
    $pageContent .= spaces(3).$disposition."\n";
    $panel = '';
    $panel .= spaces(2).'<div class="card">'."\n";
    $panel .= spaces(3).'<div class="card-header"><b>'.tr('Confidential documents').'</b></div>'."\n";
    $panel .= spaces(3).'<table class="table">'."\n";
    $panel .= spaces(4).'<tbody>'."\n";
    $panel .= $documentsLines[2];
    $panel .= spaces(4).'</tbody>'."\n";
    $panel .= spaces(3).'</table>'."\n";
    $panel .= spaces(2).'</div>'."\n";
    $pageContent .= $panel;
    $pageContent .= spaces(3).'</div>'."\n";
    }

// documents généraux :
$pageContent .= spaces(3).$disposition."\n";
$panel = '';
$panel .= spaces(2).'<div class="card">'."\n";
$panel .= spaces(3).'<div class="card-header"><b>'.tr('General documents').'</b></div>'."\n";
$panel .= spaces(3).'<table class="table">'."\n";
$panel .= spaces(4).'<tbody>'."\n";
$panel .= $documentsLines[0];
$panel .= spaces(4).'</tbody>'."\n";
$panel .= spaces(3).'</table>'."\n";
$panel .= spaces(2).'</div>'."\n";
$pageContent .= $panel;
$pageContent .= spaces(3).'</div>'."\n";

$pageContent .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle.$pageContent;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
