<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]])) {
    $result = "\n".resultatsUnavailableMessage();
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    POUR MISE À JOUR PAR AJAX
****************************************************/
if ($ACTION == 'request') {
    $_SESSION['WINDOW_WIDTH'] = isset($_POST['w']) ? $_POST['w'] : $_SESSION['WINDOW_WIDTH'];
    $_SESSION['WINDOW_HEIGHT'] = isset($_POST['h']) ? $_POST['h'] : $_SESSION['WINDOW_HEIGHT'];
    $result = '';
    exit($result);
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function getDateResults($CONNEXION) {
    $SQL = 'SELECT value_text AS TXT FROM config ';
    $SQL .= 'WHERE key_name=:key';
    $OPT = array(':key' => "dateRecup");
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    if ($STMT != '') {
        $RESULT = $STMT -> fetch();
        $RESULT = $RESULT['TXT'];
        }
    else
        $RESULT = '';
    return $RESULT;
    }

function verifyUpdate() {
    if (! isset($_SESSION['DATE_VERSION_INTERFACE']))
        $_SESSION['DATE_VERSION_INTERFACE'] = date2Timestamp(DATE_VERSION_INTERFACE);
    if (! isset($_SESSION['DATE_VERSION_SERVEUR'])) {
        // on récupère la date de la version en ligne
        // on récupère le contenu en ligne
        $contenu_date = getContents(DATE_VERSION_SERVEUR);
        if ($contenu_date) {
            $dates = explode('|', $contenu_date);
            $date = str_replace(chr(0), '', $dates[2]);
            $_SESSION['DATE_VERSION_SERVEUR'] = date2Timestamp($date);
            }
        else
            $_SESSION['DATE_VERSION_SERVEUR'] = 0;
        }
    }

function getContents($url) {
    /*
    Pour récupérer le contenu d'un fichier distant en contournant 
    allow_url_fopen et safe_mode
    http://qualitypoint.blogspot.fr/2009/05/use-curl-in-case-allowurlfopen-is-off.html
    by Rajamanickam Antonimuthu
    modification du param CURLOPT_FOLLOWLOCATION pour éviter les erreurs avec safe_mode
    */
    if (in_array('curl', get_loaded_extensions())) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION ,0); // ON NE SUIT PAS LES REDIRECTIONS
        curl_setopt($ch, CURLOPT_HEADER,0);  // DO NOT RETURN HTTP HEADERS
        curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT  ,10); // TIME OUT après 10 secondes
        $Rec_Data = curl_exec($ch);
        }
    else {
        $content = file_get_contents($url);
        if ($content === False)
            $Rec_Data = False;
        else
            $Rec_Data = $content;
        }
    return $Rec_Data;
    }

function yesNo($what) {
    if ($what == True)
        $result = '<td align="center"><span class="oi oi-check"></span></td>';
    else
        $result = '<td align="center"><span class="oi oi-x"></span></td>';
    return $result;
    }

function infosOS() {
    /*
    
    */
    $user_agent = getenv("HTTP_USER_AGENT");
    if (strpos($user_agent, "Win") !== False)
        $os = "Windows";
    elseif ((strpos($user_agent, "Mac") !== False) or (strpos($user_agent, "PPC") !== False))
        $os = "Mac";
    elseif (strpos($user_agent, "Linux") !== False)
        $os = "Linux";
    elseif (strpos($user_agent, "FreeBSD") !== False)
        $os = "FreeBSD";
    elseif (strpos($user_agent, "SunOS") !== False)
        $os = "SunOS";
    elseif (strpos($user_agent, "IRIX") !== False)
        $os = "IRIX";
    elseif (strpos($user_agent, "BeOS") !== False)
        $os = "BeOS";
    elseif (strpos($user_agent, "OS/2") !== False)
        $os = "OS/2";
    elseif (strpos($user_agent, "AIX") !== False)
        $os = "AIX";
    else
        $os = "Other";
    return array($os, $_SERVER['HTTP_USER_AGENT']);
    }

function clearTempDirectory() {
    /*
    pour effacer les fichiers qui seraient restés dans le 
    dosssier tmp à la session précédente.
    Par exemple après avoir quitté depuis la page conseil, 
    la photo peut être restée dans tmp.
    */
    $fileList = array();
    $begin = 'tmp'.DIRECTORY_SEPARATOR.$_SESSION['USER_ID'].'-';
    $objects = new RecursiveIteratorIterator(
        new RecursiveDirectoryIterator('tmp'.DIRECTORY_SEPARATOR));
    foreach($objects as $fileName => $object){
        $pos = strpos($fileName, $begin);
        if ($pos !== false)
            $fileList[] = $fileName;
        }
    foreach ($fileList as $fileName)
        unlink($fileName);
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];



/****************************************************
    INFORMATIONS CONCERNANT L'UTILISATEUR
****************************************************/
$welcome = spaces(2).'<div class="container theme-showcase">'."\n";
$welcome .= spaces(3).'<div class="page-header">'."\n";
$welcome .= spaces(4).'<h1>'.tr("Welcome to the VÉRAC's web interface.").'</h1>'."\n";
if ($_SESSION['USER_MODE'] == 'prof') {
    clearTempDirectory();
    $you = $_SESSION['USER_NAME'];
    $fileDate = getDateResults($CONNEXION);
    }
elseif ($_SESSION['USER_MODE'] == 'eleve') {
    $you = $_SESSION['USER_NAME'];
    $fileDate = getDateResultsStudent($CONNEXION, $_SESSION['USER_ID']);
    }
$welcome .= spaces(4).'<h4>'.$you.'<br/>'.tr('Last data recovery:').' '.$fileDate.'</h4>'."\n";
$welcome .= spaces(3).'</div>'."\n";
$welcome .= spaces(2).'</div>'."\n";



/****************************************************
    SI LE MOT DE PASSE N'A AS ÉTÉ CHANGÉ
****************************************************/
$password = '';
if ($_SESSION['USER_MUST_CHANGE_PASSWORD']) {
    $password .= spaces(2).'<div class="row">'."\n";
    $password .= spaces(3).'<div class="col-sm-2"></div>'."\n";
    $password .= spaces(3).'<div class="col-sm-8">'."\n";
    $msg0 = tr('For better security, consider changing your password!');
    $msg1 = tr('(menu: Tools → Password)');
    $password .= spaces(4).'<div class="alert alert-danger text-center">'."\n";
    $password .= spaces(5).'<strong>'.$msg0.'</strong><br/>'.$msg1."\n";
    $password .= spaces(4).'</div>'."\n";
    $password .= spaces(3).'</div>'."\n";
    $password .= spaces(3).'<div class="col-sm-2"></div>'."\n";
    $password .= spaces(2).'</div>'."\n";
    }



/****************************************************
    MESSAGE ÉVENTUEL DE L'ADMINISTRATEUR
****************************************************/
$messageToUsers = '';
if (! MESSAGE_TO_USERS)
    $message = '';
elseif (MESSAGE_TO_USERS == 'MESSAGE_TO_USERS')
    $message = '';
else
    $message = MESSAGE_TO_USERS;
if ($message != '') {
    $messageToUsers .= spaces(2).'<div class="row">'."\n";
    $messageToUsers .= spaces(3).'<div class="col-sm-1"></div>'."\n";
    $messageToUsers .= spaces(3).'<div class="col-sm-10">'."\n";
    $messageToUsers .= spaces(4).'<div class="alert alert-info">'."\n";
    $messageToUsers .= spaces(5).nl2br($message)."\n";
    $messageToUsers .= spaces(4).'</div>'."\n";
    $messageToUsers .= spaces(3).'</div>'."\n";
    $messageToUsers .= spaces(3).'<div class="col-sm-1"></div>'."\n";
    $messageToUsers .= spaces(2).'</div>'."\n";
    }



/****************************************************
    CONFIGURATION DU SITE
****************************************************/
$disposition = '<div class="col-sm-5">';
$siteConfig = '';
$siteConfig .= spaces(2).'<div class="row">'."\n";
$siteConfig .= spaces(3).'<div class="col-sm-1"></div>'."\n";

// général :
$siteConfig .= spaces(3).$disposition."\n";
$panel = '';
$panel .= spaces(2).'<div class="card">'."\n";
$panel .= spaces(3).'<div class="card-header text-white bg-danger">'.tr('General Restrictions').'</div>'."\n";
$panel .= spaces(3).'<table class="table">'."\n";
$panel .= spaces(4).'<tbody>'."\n";
if ($_SESSION['USER_MODE'] == 'prof') {
    $panel .= spaces(6).'<tr>'.yesNo(SITE_ENABLED_ELEVES)
        .'<td>'.tr('students have access to the site').'</td>'.'</tr>'."\n";
    }
$panel .= spaces(6).'<tr>'.yesNo(SHOW_CPT_DETAILS)
    .'<td>'.tr('students can view the details of shared competences').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_CLASS_COLUMN)
    .'<td>'.tr('students see the "Class" column').'</td>'.'</tr>'."\n";
if ($_SESSION['USER_MODE'] == 'prof') {
    $panel .= spaces(6).'<tr>'.yesNo(CAN_VALIDATE)
        .'<td>'.tr('teachers can validate the referential').'</td>'.'</tr>'."\n";
    }
$panel .= spaces(4).'</tbody>'."\n";
$panel .= spaces(3).'</table>'."\n";
$panel .= spaces(2).'</div>'."\n";

// période actuelle :
$panel .= spaces(2).'<div class="card">'."\n";
$panel .= spaces(3).'<div class="card-header text-white bg-danger">'
    .tr('Restrictions relating only to the current period').'</div>'."\n";
$panel .= spaces(3).'<table class="table">'."\n";
$panel .= spaces(4).'<tbody>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_ACTUAL_PERIODE)
    .'<td>'.tr('students have access to the actual period').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_APPRECIATION)
    .'<td>'.tr('students see the opinions').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_NOTES)
    .'<td>'.tr('students see the notes (for classes with notes)').'</td>'.'</tr>'."\n";
$panel .= spaces(4).'</tbody>'."\n";
$panel .= spaces(3).'</table>'."\n";
$panel .= spaces(2).'</div>'."\n";
$siteConfig .= $panel;
$siteConfig .= spaces(3).'</div>'."\n";

// pages restreintes :
$siteConfig .= spaces(3).$disposition."\n";
$panel = '';
$panel .= spaces(2).'<div class="card">'."\n";
$panel .= spaces(3).'<div class="card-header text-white bg-info">'
    .tr('Pages authorized for students (all periods)').'</div>'."\n";
$panel .= spaces(3).'<table class="table">'."\n";
$panel .= spaces(4).'<tbody>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_BILANS)
    .'<td>'.tr('Balances').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_REFERENTIEL)
    .'<td>'.tr('Referential').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_SOCLE_COMPONENTS)
    .'<td>'.tr('Components of the socle').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_DETAILS)
    .'<td>'.tr('Details').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_SUIVI)
    .'<td>'.tr('Follows').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_APPRECIATION_GROUPE)
    .'<td>'.tr('Opinions (of the groups)').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_DOCUMENTS)
    .'<td>'.tr('Papers').'</td>'.'</tr>'."\n";
$panel .= spaces(6).'<tr>'.yesNo(SHOW_CALCULATRICE)
    .'<td>'.tr('Calculator').'</td>'.'</tr>'."\n";
$panel .= spaces(4).'</tbody>'."\n";
$panel .= spaces(3).'</table>'."\n";
$panel .= spaces(2).'</div>'."\n";
$siteConfig .= $panel;
$siteConfig .= spaces(3).'</div>'."\n";

$siteConfig .= spaces(2).'</div>'."\n";



/****************************************************
    VERSION DE L'INTERFACE WEB (PROF SEULEMENT)
****************************************************/
$siteVersion = '';
if ($_SESSION['USER_MODE'] == 'prof') {
    verifyUpdate();
    if ($_SESSION['DATE_VERSION_SERVEUR'] > $_SESSION['DATE_VERSION_INTERFACE'])
        $span = '<span class="badge badge-secondary">'
            .tr('The web interface must be updated!').'</span>';
    elseif ($_SESSION['DATE_VERSION_SERVEUR'] == 0)
        $span = '<span class="badge badge-secondary">'
            .tr('Not available').'</span>';
    else
        $span = '<span class="badge">'
            .tr('The web interface is updated').'</span>';
    if ($_SESSION['DATE_VERSION_SERVEUR'] == 0)
        $dateVersionServeurMessage = '';
    else
        $dateVersionServeurMessage = date('d/m/Y', $_SESSION['DATE_VERSION_SERVEUR']);
    $siteVersion .= spaces(2).'<div class="row">'."\n";
    $siteVersion .= spaces(3).'<div class="col-sm-3"></div>'."\n";
    $siteVersion .= spaces(3).'<div class="col-sm-6">'."\n";
    $panel = '';
    $panel .= spaces(2).'<div class="card">'."\n";
    $panel .= spaces(3).'<div class="card-header text-white bg-secondary">'.tr('WebSite Version').'</div>'."\n";
    $panel .= spaces(3).'<table class="table">'."\n";
    $panel .= spaces(4).'<tbody>'."\n";
    $panel .= spaces(6).'<tr>'
        .'<td><b>'.tr('State:').'</b></td>'
        .'<td align="right">'.$span.'</td></tr>'."\n";
    $panel .= spaces(6).'<tr>'
        .'<td>'.tr('Installed Version: ').'</td>'
        .'<td align="right">'.date('d/m/Y', $_SESSION['DATE_VERSION_INTERFACE']).'</td></tr>'."\n";
    $panel .= spaces(6).'<tr>'
        .'<td>'.tr('Current Version: ').'</td>'
        .'<td align="right">'.$dateVersionServeurMessage.'</td></tr>'."\n";
    $panel .= spaces(4).'</tbody>'."\n";
    $panel .= spaces(3).'</table>'."\n";
    $panel .= spaces(2).'</div>'."\n";
    $siteVersion .= $panel;
    $siteVersion .= spaces(3).'</div>'."\n";
    $siteVersion .= spaces(2).'</div>'."\n";
    }



/****************************************************
    INFORMATIONS SUR L'ORDINATEUR
****************************************************/
$infos = '';
if ($_SESSION['USER_MODE'] == 'prof') {
    $infos .= spaces(2).'<div class="container theme-showcase">'."\n";
    $infos .= spaces(3).'<div class="row">'."\n";
    $infos .= spaces(4).'<div class="col-sm-2">'."\n";
    $infos .= spaces(4).'</div>'."\n";
    $infos .= spaces(4).'<div class="col-sm-8">'."\n";
    $infos .= spaces(5).'<div class="list-group">'."\n";
    $infos .= spaces(6).'<a class="list-group-item active">'
        .tr('Other information').'</a>'."\n";
    $infosOS = infosOS();
    $infos .= spaces(6).'<p class="list-group-item"><b>'
        .tr('Operating System: ').'</b>'.$infosOS[0].'</p>'."\n";
    $infos .= spaces(6).'<p class="list-group-item"><b>'
        .tr('Browser: ').'</b>'.$infosOS[1].'</p>'."\n";
    $infos .= spaces(6).'<p class="list-group-item"><b>'
        .tr('PHP version: ').'</b>'.phpversion().'</p>'."\n";
    $infos .= spaces(5).'</div>'."\n";
    $infos .= spaces(4).'</div>'."\n";
    $infos .= spaces(3).'</div>'."\n";
    $infos .= spaces(2).'</div>'."\n";
    }


/****************************************************
    AFFICHAGE
****************************************************/
$result = $welcome.$password.$messageToUsers.$siteConfig.$siteVersion;//.$infos;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
