<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if (($_SESSION['USER_MODE'] == 'prof') and ($_SESSION['SELECTED_CLASS'][0] == -1))
    $result = "\n".message('info', 'Select a class.', '');
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function getStatistics($CONNEXION, $id_eleve) {
    /*
    récupère les statistiques de visite du site
    pour les élèves passés en paramètre.
    On ne calcule pas encore les totaux
    car des semaines non affichées peuvent 
    encore être dans la base.
    */
    $statistics = array();
    // pour afficher les totaux du groupe :
    $statistics['GROUP'] = array('TOTAL' => 0);
    // pour repérer les semaines à afficher :
    $SQL = 'SELECT * FROM users ';
    $SQL .= 'WHERE id_eleve IN('.$id_eleve.') ';
    $SQL .= 'ORDER BY id_eleve, week';
    $STMT = $CONNEXION -> DB_COMPTEUR -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $RESULT = ($STMT != '') ? $STMT -> fetchAll(): array();
    foreach ($RESULT as $ROW) {
        $id_eleve = $ROW['id_eleve'];
        $week = $ROW['week'];
        $value = $ROW['total'];
        if (! array_key_exists($id_eleve, $statistics))
            $statistics[$id_eleve] = array('TOTAL' => 0);
        if (! array_key_exists($week, $statistics[$id_eleve]))
            $statistics[$id_eleve][$week] = $value;
        else
            $statistics[$id_eleve][$week] += $value;
        if (! array_key_exists($week, $statistics['GROUP']))
            $statistics['GROUP'][$week] = $value;
        else
            $statistics['GROUP'][$week] += $value;
        }
    return $statistics;
    }

function getWeeks() {
    /*
    donne la liste ordonnée des numéros de semaine.
    On tient compte des changements d'année.
    */
    $weeks = array();
    $week = date('W');
    $lastlundi = mktime(
        0, 0, 0, 
        date('m'), 
        date('d') + 1 - date('w'), 
        date('Y'));
    $nbWeeks = 13;
    for ($i=0; $i<$nbWeeks; $i++) {
        $numWeek = $week - $nbWeeks + $i + 1;
        if ($numWeek < 1)
            $numWeek += 52;
        $lundi = mktime(
            0, 0, 0, 
            date('m', $lastlundi), 
            date('d', $lastlundi) + 7 * (1 + $i - $nbWeeks), 
            date('Y', $lastlundi));
        $dimanche = mktime(
            0, 0, 0, 
            date('m', $lundi), 
            date('d', $lundi) + 6, 
            date('Y', $lundi));
        $toolTip = tr('Week').' '.$numWeek.' :'
            .'&#10;&emsp;'.tr('from').'&emsp;'.date('d-m-Y', $lundi)
            .'&#10;&emsp;'.tr('to').'&emsp;'.date('d-m-Y', $dimanche);
        $weeks[] = array($numWeek, $toolTip);
        }
    return $weeks;
    }

function doWidth($width) {
    return 'width="'.$width.'px"';
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
$students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);
$id_students = array2string($students, 0);
$statistics = getStatistics($CONNEXION, $id_eleve=$id_students);
$weeks = getWeeks();



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.tr('Statistics of website visits:').' '
    .$_SESSION['SELECTED_CLASS'][1].'</h2>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";

$printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
$printTitle .= spaces(3).'<p class="text-center">';
$printTitle .= tr('Statistics');
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</p>'."\n";
$printTitle .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE DU TABLEAU DES STATSISTIQUES
****************************************************/
$tbodyHeight = $_SESSION['WINDOW_HEIGHT'] - 200;

$table = '';
// la ligne des titres du tableau :
$table .= spaces(5).'<table class="table table-fixed table-striped table-bordered">'."\n";
$table .= spaces(6).'<thead>'."\n";
$table .= spaces(7).'<tr class="table-success">'."\n";
$table .= spaces(8).'<th '.doWidth(200).'>'.tr('STUDENT').'</th>'."\n";
foreach ($weeks as $week)
    $table .= spaces(8).'<th '.doWidth(50).' class="title" title="'.$week[1].'">'.$week[0].'</th>'."\n";
$table .= spaces(8).'<th '.doWidth(100).' class="title">'.tr('Total').'</th>'."\n";
$table .= spaces(7).'</tr>'."\n";
$table .= spaces(6).'</thead>'."\n";
$table .= spaces(6).'<tbody height="'.$tbodyHeight.'">'."\n";
// pour chaque élève de la classe :
foreach ($students as $student) {
    $eleve_id = $student[0];
    $table .= spaces(7).'<tr>'."\n";
    $table .= spaces(8).'<td '.doWidth(200).'>'.$student[1].'</td>'."\n";
    foreach ($weeks as $week) {
        $value = '';
        if (array_key_exists($eleve_id, $statistics))
            if (array_key_exists($week[0], $statistics[$eleve_id])) {
                $value = $statistics[$eleve_id][$week[0]];
                $statistics[$eleve_id]['TOTAL'] += $value;
                }
        $table .= spaces(8).'<td '.doWidth(50).' align="center" title="'.$week[1].'">'.$value.'</td>'."\n";
        }
    $value = 0;
    if (array_key_exists($eleve_id, $statistics))
        $value = $statistics[$eleve_id]['TOTAL'];
    $table .= spaces(8).'<td '.doWidth(100).' align="center"><b>'.$value.'</b></td>'."\n";
    $table .= spaces(7).'</tr>'."\n";
    }
// on ajoute la ligne du groupe :
$table .= spaces(7).'<tr class="table-success">'."\n";
$table .= spaces(8).'<td '.doWidth(200).'><b>'.tr('CLASS').'</b></td>'."\n";
foreach ($weeks as $week) {
    $value = '';
    if (array_key_exists($week[0], $statistics['GROUP'])) {
        $value = $statistics['GROUP'][$week[0]];
        $statistics['GROUP']['TOTAL'] += $value;
        }
    $table .= spaces(8).'<td '.doWidth(50).' align="center" title="'.$week[1].'"><b>'.$value.'</b></td>'."\n";
    }
$table .= spaces(8).'<td '.doWidth(100).' align="center"><b>'.$statistics['GROUP']['TOTAL'].'</b></td>'."\n";
$table .= spaces(7).'</tr>'."\n";
$table .= spaces(6).'</tbody>'."\n";
$table .= spaces(5).'</table>'."\n";

// pour centrer le tableau :
$table_center = spaces(2).'<div class="container theme-showcase">'."\n";
$table_center .= $table;
$table_center .= spaces(2).'</div><br />'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle.$printTitle.$table_center;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
