<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] != 'prof')
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if (($_SESSION['USER_MODE'] == 'prof') and ($_SESSION['SELECTED_CLASS'][0] == -1))
    $result = "\n".message('info', 'Select a class.', '');
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function sharedResults($CONNEXION, $students, $sharedCompetences) {
    /*
    ajoute les résultats des élèves à la liste de compétences partagées
    */
    // on crée le texte de la liste des compétences :
    $competencesText = array();
    foreach ($sharedCompetences as $row) {
        if ($row != 'SEPARATOR') {
            if ($row['Competence'] != '')
                $competencesText[]= '"'.$row['code'].'"';
            }
        }
    $competencesText = implode(',', $competencesText);
    $id_students = array2string($students, 0);
    // récupération des résultats :
    $SQL = 'SELECT * FROM bilans ';
    $SQL .= 'WHERE id_eleve IN ('.$id_students.') ';
    $SQL .= 'AND name IN ('.$competencesText.') ';
    $SQL .= 'ORDER BY id_eleve DESC';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    $results = array();
    foreach ($tempResult as $row) {
        if ($row['id_eleve'] < 0)
            $results[$row['name']]['groupeValue'] = $row['value'];
        else
            $results[$row['name']][$row['id_eleve']] = $row['value'];
        }
    // et on les insère dans la liste des compétences :
    for($i=0; $i < count($sharedCompetences); $i++) {
        if ($sharedCompetences[$i] != 'SEPARATOR') {
            if ($sharedCompetences[$i]['Competence'] != '') {
                $code = $sharedCompetences[$i]['code'];
                $sharedCompetences[$i]['groupeValue'] = '';
                if (array_key_exists($code, $results)) {
                    foreach ($results[$code] as $key => $value)
                        $sharedCompetences[$i][$key] = $value;
                    }
                }
            }
        }
    return $sharedCompetences;
    }

function sharedDetails(
        $CONNEXION, $students, $sharedCompetences) {
    /*
    retourne les détails des résultats
    pour une liste de compétences partagées
    */
    $results = array();
    // on crée le texte de la liste des compétences :
    $competencesText = array();
    foreach ($sharedCompetences as $row) {
        if ($row != 'SEPARATOR') {
            if ($row['Competence'] != '')
                $competencesText[]= '"'.$row['code'].'"';
            }
        }
    $competencesText = implode(',', $competencesText);
    $id_students = array2string($students, 0);
    // récupération des résultats :
    $SQL = 'SELECT * FROM bilans_details ';
    $SQL .= 'WHERE id_eleve IN ('.$id_students.') ';
    $SQL .= 'AND name IN ('.$competencesText.') ';
    $SQL .= 'ORDER BY matiere';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    $letters = getArrayLetters();
    $results['GROUP'] = array();
    foreach ($letters as $letter)
        $results['GROUP'][$letter] = 0;
    foreach ($tempResult as $row) {
        if (! array_key_exists($row['id_eleve'], $results)) {
            $results[$row['id_eleve']] = array();
            foreach ($letters as $letter)
                $results[$row['id_eleve']][$letter] = 0;
            }
        $results[$row['id_eleve']][$row['value']] += 1;
        $results['GROUP'][$row['value']] += 1;
        }
    return $results;
    }

function getTitles($CONNEXION, $n, $sharedCompetences) {
    /*
    retourne la ligne des titres du tableau
    */
    $result = '';
    $result .= spaces($n).'<thead class="bilan">'."\n";
    $result .= spaces($n + 1).'<tr>'."\n";
    $result .= spaces($n + 2).'<th class="student"><b>'.tr('STUDENT').'</b></th>'."\n";
    foreach ($sharedCompetences as $column) {
        if ($column == 'SEPARATOR')
            $result .= spaces($n + 2).'<td class="separator"></td>'."\n";
        else
            $result .= spaces($n + 2).'<th class="verticalTitle" title="'.$column['Competence'].'"><div class="vertical"><b>'.$column['code'].'</b></div></th>'."\n";
        }
    foreach (getArrayLetters() as $value)
        $result .= spaces($n + 2).'<th class="'.$value.'" width="40px"><b>'.valeur2affichage($value).'</b></th>'."\n";
    $result .= spaces($n + 1).'</tr>'."\n";
    $result .= spaces($n).'</thead>'."\n";
    return $result;
    }

function getStudentRow(
        $CONNEXION, $n, $student, $sharedCompetences, $sharedDetails) {
    /*
    retourne la ligne des résultats d'un élève
    */
    $result = spaces($n).'<tr>'."\n";
    // le nom :
    if ($student[0] > 0)
        $result .= spaces($n + 1).'<td class="student">'.$student[1].'</td>'."\n";
    else
        $result .= spaces($n + 1).'<td class="student"><b>'.$student[1].'</b></td>'."\n";
    // les résultats :

    foreach ($sharedCompetences as $column) {
        if ($column == 'SEPARATOR')
            $result .= spaces($n + 1).'<td class="separator"></td>'."\n";
        else {
            $row = array('value' => 'A');
            if ($student[0] > 0) {
                if (array_key_exists($student[0], $column))
                    $value = $column[$student[0]];
                else
                    $value = '';
                }
            else
                $value = $column['groupeValue'];
            $title = $column['Competence'];
            $result .= spaces($n + 1).'<td class="'.$value.'" title="'.$title.'">'.valeur2affichage($value).'</td>'."\n";
            }
        }
    // les pourcentages :
    if ($student[0] > 0)
        $liste_totaux = $sharedDetails[$student[0]];
    else
        $liste_totaux = $sharedDetails['GROUP'];
    // on récupère la valeur la plus haute :
    $max = 0;
    $key_max = '';
    foreach ($liste_totaux as $key => $value) {
        if ($value > $max) {
            $key_max = $key;
            $max = $value;
            }
        }
    // on affiche
    foreach ($liste_totaux as $key => $value) {
        $somme = array_sum($liste_totaux);
        $class = ($key == $key_max) ? $key : '';
        $value = ($somme > 0) ? round(100 * ($value / $somme), 0) : 0;
        if ($value > 0)
            $result .= spaces($n + 1).'<td align="center" class="'.$class.'">'.$value.'%</td>'."\n";
        else
            $result .= spaces($n + 1).'<td align="center" class="'.$class.'"></td>'."\n";
        }
    $result .= spaces($n).'</tr>'."\n";
    return $result;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
$students = $CONNEXION -> listStudents($_SESSION['SELECTED_CLASS'][0]);

$sharedCompetences = array();
$tempResult = sharedCompetences(
    $CONNEXION, 
    $_SESSION['SELECTED_CLASS'][0], 
    $what='bulletin');
// on les remet en forme :
$separator = True;
foreach ($tempResult as $row) {
    if ($row['Competence'] != '') {
        $sharedCompetences[] = $row;
        $separator = True;
        }
    elseif ($separator) {
        $sharedCompetences[] = 'SEPARATOR';
        $separator = False;
        }
    }

// on prépare l'interligne pour les cellules de bordure :
$colspan = 1 + count($sharedCompetences) + count(getArrayLetters()) + 2;

// on ajoute la classe à la liste d'élèves
$idClasseInResults = idClassInResults($CONNEXION, $_SESSION['SELECTED_CLASS'][1]);
$students[] = array($idClasseInResults, tr('CLASS'));

// récupération des évaluations :
$sharedCompetences = sharedResults(
    $CONNEXION, $students, $sharedCompetences);
$sharedDetails = sharedDetails(
    $CONNEXION, $students, $sharedCompetences);



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.tr('Shared competences: ').$_SESSION['SELECTED_CLASS'][1].'</h2>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";

$printTitle = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$printTitle .= spaces(2).'<div class="d-none d-print-block">'."\n";
$printTitle .= spaces(3).'<p class="text-center">';
$printTitle .= tr('Shared competences');
$printTitle .= ' - '.$_SESSION['SELECTED_PERIOD'][1];
$printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';
$printTitle .= '</p>'."\n";
$printTitle .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE DU TABLEAU
****************************************************/
$table = '';
$table .= spaces(2).'<div class="table-responsive">'."\n";
$table .= spaces(2).'<table border="1" class="bilan">'."\n";
$table .= getTitles($CONNEXION, 3, $sharedCompetences);
$table .= spaces(3).'<tbody>'."\n";
foreach ($students as $student) {
    if ($student[0] > 0)
        $table .= getStudentRow(
            $CONNEXION, 4, $student, $sharedCompetences, $sharedDetails);
    else {
        $table .= spaces(4).'<tr><td class="interligne" colspan="'.$colspan.'"></td></tr>'."\n";
        $table .= getStudentRow(
            $CONNEXION, 4, $student, $sharedCompetences, $sharedDetails);
        }
    }
$table .= spaces(3).'</tbody>'."\n";
$table .= spaces(2).'</table>'."\n";
$table .= spaces(2).'</div>'."\n";



/****************************************************
    REMARQUES
****************************************************/
$remarks = '';
$remarks .= spaces(2).'<br/>'."\n";
$remarks .= spaces(2).'<!-- REMARKS -->'."\n";
$remarks .= spaces(2).'<div class="alert alert-warning text-left">'."\n";
$remarks .= spaces(3).'<div class="row">'."\n";
$remarks .= spaces(4).'<div class="col-sm-2">'."\n";
$remarks .= spaces(5).'<small><b>'.tr('REMARKS:').'</b></small>'."\n";
$remarks .= spaces(4).'</div>'."\n";
$remarks .= spaces(4).'<div class="col-sm-10">'."\n";
$remarks .= spaces(3).'<small>'."\n";
$remarks .= spaces(4).'<ul> '."\n";
$remarks .= spaces(5).'<li>'
    .tr('percentages are calculated on all the balances present on the bulletin (and not on the columns of this table);').'</li> '."\n";
$remarks .= spaces(5).'<li>'
    .tr('percentages are rounded to the nearest integer (their sum is therefore not necessarily 100%).').'</li> '."\n";
$remarks .= spaces(4).'</ul>'."\n";
$remarks .= spaces(3).'</small>'."\n";
$remarks .= spaces(4).'</div>'."\n";
$remarks .= spaces(3).'</div>'."\n";
$remarks .= spaces(2).'</div>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = "\n".$pageTitle.$printTitle.$table.$remarks;
if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
