<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    VÉRIFICATIONS
****************************************************/
// PAS D'APPEL DIRECT DE CETTE PAGE :
if (! defined('VERAC'))
    exit;
$result = '';
// ÉTAT DE LA BASE RÉSULTATS (EN CAS D'ENVOI) :
if (! testResultatsDBState($CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]]))
    $result = "\n".resultatsUnavailableMessage();
// PAGE INTERDITE :
if ($_SESSION['USER_MODE'] == 'public')
    $result = "\n".prohibitedMessage();
if (($_SESSION['USER_MODE'] == 'eleve') and (! SHOW_REFERENTIEL))
    $result = "\n".prohibitedMessage();
// VÉRIFICATION DE LA SÉLECTION :
if ($_SESSION['USER_MODE'] == 'prof') {
    if ($_SESSION['SELECTED_CLASS'][0] == -1)
        $result = "\n".message('info', 'Select a class.', '');
    elseif ($_SESSION['SELECTED_STUDENT'][0] == -1)
        $result = "\n".message('info', 'Select a student.', '');
        }
if ($result != '') {
    if ($ACTION == 'reload')
        exit($result);
    else {
        echo $result;
        return;
        }
    }



/****************************************************
    FONCTIONS UTILES
****************************************************/
function studentValidations(
        $db_referentialPropositions, $db_referentialValidations, $id_eleve, $competences) {
    /*
    retourne les résultats d'un élève pour la validation du référentiel.
    */
    $validations = array(
        'VALUES' => array(), 
        'DETAILS' => array());

    // on crée le texte de la liste des compétences :
    foreach ($competences as $row) {
        $validations['VALUES'][$row['code']] = '';
        $validations['DETAILS'][$row['code']] = array();
        }

    // récupération des propositions :
    $SQL = 'SELECT * FROM bilans_propositions ';
    $SQL .= 'WHERE id_eleve IN ('.$id_eleve.') ';
    $STMT = $db_referentialPropositions -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    foreach ($tempResult as $row)
        $validations['VALUES'][$row['bilan_name']] = $row['value'];

    // récupération des détails :
    $SQL = 'SELECT * FROM bilans_details ';
    $SQL .= 'WHERE id_eleve IN ('.$id_eleve.') ';
    $SQL .= 'ORDER BY bilan_name, annee_scolaire DESC , Matiere';
    $STMT = $db_referentialPropositions -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    foreach ($tempResult as $row)
        if (array_key_exists($row['bilan_name'], $validations['DETAILS'])) {
            $validations['DETAILS'][$row['bilan_name']][] = array(
                'annee' => $row['annee_scolaire'], 
                'matiere' => $row['Matiere'], 
                'prof' => $row['nom_prof'], 
                'value' => $row['value'], );
            }

    // récupération des validations :
    if ($db_referentialValidations != False) {
        $SQL = 'SELECT * FROM bilans_validations ';
        $SQL .= 'WHERE id_eleve IN ('.$id_eleve.') ';
        $STMT = $db_referentialValidations -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
        // on les remet en forme :
        foreach ($tempResult as $row) {
            $validations['VALUES'][$row['bilan_name']] = $row['value'];
            }
        }

    return $validations;
    }



/****************************************************
    CONNEXION ET AUTRES PARAMÈTRES
****************************************************/
$CONNEXION = $CONNEXIONS[$_SESSION['SELECTED_PERIOD'][0]];
if ($_SESSION['USER_MODE'] == 'prof')
    $selectedStudent = $_SESSION['SELECTED_STUDENT'][0];
else
    $selectedStudent = $_SESSION['USER_ID'];

// initialisation de la liste pour l'affichage du camembert :
$camembertData = array();
foreach (getArrayLetters() as $key)
    $camembertData[$key] = 0;
$camembertData['vide'] = 0;

// liste des noms des profs :
$teachersNames = listTeachersNames($CONNEXION);
// liste des noms des matières :
$subjectsLabels = listSubjectsLabels($CONNEXION);

// affichage des détails par clic :
if ($_SESSION['USER_MODE'] == 'prof')
    $studentsShowDetails = True;
elseif (SHOW_CPT_DETAILS)
    $studentsShowDetails = True;
else
    $studentsShowDetails = False;



/****************************************************
    TITRE DU DOCUMENT + TITRE POUR IMPRESSION
****************************************************/
$pageTitle = '';
$fileDate = getDateResultsStudent($CONNEXION, $selectedStudent);
$nom_eleve = getStudentName($CONNEXION, $selectedStudent, $mode='');
$pageTitle .= spaces(2).'<!-- PAGE HEADER -->'."\n";
$pageTitle .= spaces(2).'<div class="d-print-none">'."\n";
$pageTitle .= spaces(3).'<div class="container theme-showcase">'."\n";
$pageTitle .= spaces(4).'<div class="page-header">'."\n";
$pageTitle .= spaces(5).'<h2>'.$nom_eleve.' : '.tr('STATEMENT OF REFERENTIAL IN ').$fileDate.'</h2>'."\n";
//$pageTitle .= spaces(5).'<p>'.tr('Note: this statement reflects only the current school year.').'</p>'."\n";
$pageTitle .= spaces(5).'<p>'.tr('Note: this statement takes account of previous years.').'</p>'."\n";
$pageTitle .= spaces(4).'</div>'."\n";
$pageTitle .= spaces(3).'</div>'."\n";
$pageTitle .= spaces(2).'</div>'."\n";
$printTitle = tr('Referential');
$printTitle .= ' - '.getPeriodName($CONNEXION, $_SESSION['SELECTED_PERIOD'][0]);
if ($_SESSION['USER_MODE'] == 'eleve')
    $printTitle .= ' - '.$_SESSION['USER_CLASS'][1];
else
    $printTitle .= ' - '.$_SESSION['SELECTED_CLASS'][1];
$printTitle .= ' - '.getStudentName($CONNEXION, $selectedStudent);
$printTitle .= ' - '.' ('.date('o-m-d H:i').')';



/****************************************************
    CALCUL DU RÉFÉRENTIEL
****************************************************/
$competencesTable = '';
$idClasseInResults = idClassInResults($CONNEXION, $_SESSION['SELECTED_CLASS'][1]);
$competences = sharedCompetences(
    $CONNEXION, 
    $_SESSION['SELECTED_CLASS'][0], 
    $what='referentiel');
$results = studentResults(
    $CONNEXION, 
    $selectedStudent, 
    $idClasseInResults, 
    $competences);

$db_referentialPropositions = $CONNEXION -> connectReferentialPropositionsDB();
$db_referentialValidations = $CONNEXION -> connectReferentialValidationsDB();
$validations = studentValidations(
    $db_referentialPropositions, 
    $db_referentialValidations, 
    $selectedStudent, 
    $competences);

if ($studentsShowDetails) {
    $detailsMessage = tr('Click to show/hide details.');
    $colspan = 2;
    }
$id_toggle = 0;
$inTable = False;
foreach ($results as $ROW) {
    if (array_key_exists($ROW['code'], $validations['VALUES']))
        $ROW['value'] = $validations['VALUES'][$ROW['code']];
    $table = '';
    if ($ROW['Competence'] != '') {
        $id_toggle += 1;
        if (! $inTable) {
            $inTable = True;
            $table .= spaces(5).'<table border="1" class="bilan">'."\n";
            $table .= spaces(3).'<col width=85%>'."\n";
            $table .= spaces(3).'<col width=15%>'."\n";
            $table .= spaces(6).'<thead class="bilan">'."\n";
            //$table .= spaces(7).'<tr>'."\n";
            //$table .= spaces(8).'<th class="title"></th>'."\n";
            //$table .= spaces(8).'<th class="student">'.tr('Student').'</th>'."\n";
            //$table .= spaces(8).'<th class="group">'.tr('Class').'</th>'."\n";
            //$table .= spaces(7).'</tr>'."\n";
            $table .= spaces(6).'</thead>'."\n";
            $table .= spaces(6).'<tbody>'."\n";
            }
        if ($ROW['value'] != '') {
            if ($studentsShowDetails)
                $table .= spaces(7).'<tr class="detail_parent" title="'.$detailsMessage.'" id="'.$id_toggle.'">'."\n";
            else
                $table .= spaces(7).'<tr>'."\n";
            $table .= spaces(8).'<td class="competence_label">'.htmlspecialchars($ROW['Competence']).'</td>'."\n";
            $table .= spaces(8).'<td class="'.$ROW['value'].'">'.valeur2affichage($ROW['value']).'</td>'."\n";
            //$table .= spaces(8).'<td class="'.$ROW['groupeValue'].'">'.valeur2affichage($ROW['groupeValue']).'</td>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            $camembertData[$ROW['value']]++;
            if ($studentsShowDetails) {
                // les lignes de détails (affichables par clic) :
                foreach ($validations['DETAILS'][$ROW['code']] as $detail) {
                    $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                    $detailLabel = $detail['annee'].' - ';
                    if (array_key_exists($detail['matiere'], $subjectsLabels))
                        $detailLabel .= $subjectsLabels[$detail['matiere']][0];
                    else
                        $detailLabel .= $detail['matiere'];
                    $detailLabel .= ' ('.$detail['prof'].')';
                    $table .= spaces(8).'<td class="detail_label">'.$detailLabel.'</td>'."\n";
                    $table .= spaces(8).'<td class="'.$detail['value'].'">'
                        .valeur2affichage($detail['value']).'</td>'."\n";
                    $table .= spaces(7).'</tr>'."\n";
                    $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                    $table .= spaces(8).'<td class="interligne" colspan="'.$colspan.'"></td>'."\n";
                    $table .= spaces(7).'</tr>'."\n";
                    }
                $table .= spaces(7).'<tr class="'.$id_toggle.' detail_child hidden">'."\n";
                $table .= spaces(8).'<td class="interligne" colspan="'.$colspan.'"></td>'."\n";
                $table .= spaces(7).'</tr>'."\n";
                }
            }
        else {
            $table .= spaces(7).'<tr>'."\n";
            $table .= spaces(8).'<td class="competence_label">'.htmlspecialchars($ROW['Competence']).'</td>'."\n";
            $table .= spaces(8).'<td class="'.$ROW['value'].'">'.valeur2affichage($ROW['value']).'</td>'."\n";
            //$table .= spaces(8).'<td class="'.$ROW['groupeValue'].'">'.valeur2affichage($ROW['groupeValue']).'</td>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            $camembertData['vide']++;
            }
        }
    else {
        if ($inTable) {
            $table .= spaces(6).'</tbody>'."\n";
            $table .= spaces(5).'</table>'."\n";
            $inTable = False;
            }
        if ($ROW['Titre1'] != '')
            $table .= spaces(5).'<h3>'.htmlspecialchars($ROW['Titre1']).'</h3>'."\n";
        elseif ($ROW['Titre2'] != '')
            $table .= spaces(5).'<h4>'.htmlspecialchars($ROW['Titre2']).'</h4>'."\n";
        elseif ($ROW['Titre3'] != '') {
            $inTable = True;
            $table .= spaces(5).'<table border="1" class="bilan">'."\n";
            $table .= spaces(6).'<thead class="bilan">'."\n";
            $table .= spaces(7).'<tr>'."\n";
            $table .= spaces(8).'<th class="title"><b>'.htmlspecialchars($ROW['Titre3']).'</b></th>'."\n";
            $table .= spaces(8).'<th class="student">'.tr('Student').'</th>'."\n";
            //$table .= spaces(8).'<th class="group">'.tr('Class').'</th>'."\n";
            $table .= spaces(7).'</tr>'."\n";
            $table .= spaces(6).'</thead>'."\n";
            $table .= spaces(6).'<tbody>'."\n";
            }
        }
    $competencesTable .= $table;
    }
if ($inTable) {
    $competencesTable .= spaces(6).'</tbody>'."\n";
    $competencesTable .= spaces(5).'</table>'."\n";
    $inTable = False;
    }



/****************************************************
    COULEURS ET DONNÉES POUR CAMEMBERT ET RADAR
****************************************************/
$colors_args = '';
$rgb = hex2Rgb(COLOR_A);
$colors_args .= '&amp;COLOR_AR='.$rgb[0].'&amp;COLOR_AG='.$rgb[1].'&amp;COLOR_AB='.$rgb[2];
$rgb = hex2Rgb(COLOR_B);
$colors_args .= '&amp;COLOR_BR='.$rgb[0].'&amp;COLOR_BG='.$rgb[1].'&amp;COLOR_BB='.$rgb[2];
$rgb = hex2Rgb(COLOR_C);
$colors_args .= '&amp;COLOR_CR='.$rgb[0].'&amp;COLOR_CG='.$rgb[1].'&amp;COLOR_CB='.$rgb[2];
$rgb = hex2Rgb(COLOR_D);
$colors_args .= '&amp;COLOR_DR='.$rgb[0].'&amp;COLOR_DG='.$rgb[1].'&amp;COLOR_DB='.$rgb[2];
$rgb = hex2Rgb(COLOR_X);
$colors_args .= '&amp;COLOR_XR='.$rgb[0].'&amp;COLOR_XG='.$rgb[1].'&amp;COLOR_XB='.$rgb[2];

// on récupère la liste des sous-rubriques du référentiel :
$sousRubriques = sousRubriques($CONNEXION, 'referentiel');
// et les résultats des élèves (sous-rubriques seulement) :
$radarData = studentResultsInSousRubriques(
    $CONNEXION, $selectedStudent, array(), $sousRubriques);

// les données pour javascript :
$n = 5;
$jsData = spaces($n).'<script type="text/javascript">'."\n";
$jsData .= spaces($n + 1).'// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later'."\n";
// données pour le camembert :
$jsData .= spaces($n + 1).'var camembertData = ['."\n";
foreach ($camembertData as $key => $value) {
    if ($key == 'vide')
        $label = tr('Unevaluated');
    else
        $label = tr('Number of ').valeur2affichage($key);
    if ($key == 'A')
        $color = COLOR_A;
    elseif ($key == 'B')
        $color = COLOR_B;
    elseif ($key == 'C')
        $color = COLOR_C;
    elseif ($key == 'D')
        $color = COLOR_D;
    elseif ($key == 'X')
        $color = COLOR_X;
    else
        $color = '#f3f3f3';
    $rgb = hex2Rgb($color);
    for ($j = 0; $j < 3; $j++) {
        $rgb[$j] -= 15;
        if ($rgb[$j] < 0)
            $rgb[$j] = 0;
        }
    $highlight = rgb2Hex($rgb);
    $jsData .= spaces($n + 2).'{value: '.$value.', color:"'.$color.'", highlight: "'.$highlight.'", label: "'.$label.'"},'."\n";
    }
$jsData .= spaces($n + 2).'];'."\n";
// données pour le radar :
$jsData .= spaces($n + 1).'var radarData = {'."\n";
$radarCount = 0;
$jsData .= spaces($n + 2).'labels: [';
foreach ($radarData as $ROW)
    if (in_array($ROW['value'], array('A', 'B', 'C', 'D'))) {
        $jsData .= '"'.$ROW['name'].'",';
        $radarCount++;
        }
$jsData .= '],'."\n";
$jsData .= spaces($n + 2).'datasets: ['."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= spaces($n + 4).'noToolTip: true,'."\n";
$jsData .= spaces($n + 4).'strokeColor: "'.COLOR_A.'",'."\n";
$jsData .= spaces($n + 4).'fillColor: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'pointColor: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'pointDot : false,'."\n";
$jsData .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'pointHighlightFill: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'pointHighlightStroke: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '100,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= spaces($n + 4).'noToolTip: true,'."\n";
$jsData .= spaces($n + 4).'strokeColor: "'.COLOR_B.'",'."\n";
$jsData .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '75,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= spaces($n + 4).'noToolTip: true,'."\n";
$jsData .= spaces($n + 4).'strokeColor: "'.COLOR_C.'",'."\n";
$jsData .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '50,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= spaces($n + 4).'noToolTip: true,'."\n";
$jsData .= spaces($n + 4).'strokeColor: "'.COLOR_D.'",'."\n";
$jsData .= spaces($n + 4).'pointStrokeColor: "rgba(0, 0, 0, 0)",'."\n";
$jsData .= spaces($n + 4).'data: [';
for ($i = 0; $i < $radarCount; $i++)
    $jsData .= '25,';
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'{'."\n";
$jsData .= spaces($n + 4).'fillColor: "rgba(151,187,205,0.3)",'."\n";
$jsData .= spaces($n + 4).'strokeColor: "rgba(151,187,205,1)",'."\n";
$jsData .= spaces($n + 4).'pointColor: "rgba(151,187,205,1)",'."\n";
$jsData .= spaces($n + 4).'data: [';
foreach ($radarData as $ROW) {
    if ($ROW['value'] == 'A')
        $jsData .= '100,';
    elseif ($ROW['value'] == 'B')
        $jsData .= '75,';
    elseif ($ROW['value'] == 'C')
        $jsData .= '50,';
    elseif ($ROW['value'] == 'D')
        $jsData .= '25,';
    }
$jsData .= '],'."\n";
$jsData .= spaces($n + 3).'},'."\n";
$jsData .= spaces($n + 3).'],'."\n";
$jsData .= spaces($n + 2).'};'."\n";
$jsData .= spaces($n + 1).'// @license-end'."\n";
$jsData .= spaces($n).'</script>'."\n";



/****************************************************
    CALCUL DU CAMEMBERT
****************************************************/
$camembert = '';
// calcul de la légende :
$labels = '';
$values = '';
foreach ($camembertData as $key => $value) {
    if ($key == 'vide')
        $labels .= '<br/>'.tr('Unevaluated').' : ';
    else
        $labels .= '<br/>'.tr('Number of ').valeur2affichage($key).' : ';
    $values .= '<br/><b>'.$value.'</b>';
    }
// mise en forme :
$n = 5;
$camembert .= spaces($n).'<br/>'."\n";
$camembert .= spaces($n).'<table align="center" border="0" width="70%">'."\n";
// le titre :
$camembert .= spaces($n + 1).'<tr>'."\n";
$camembert .= spaces($n + 2).'<td align="left"><h4>'.tr('Summary (camembert):').'</h4></td>'."\n";
$camembert .= spaces($n + 1).'</tr>'."\n";
$camembert .= spaces($n + 1).'<tr>'."\n";
// le camembert en lui-même :
$camembert .= spaces($n + 2).'<td align="center">'."\n";
$camembert .= spaces($n + 3).'<canvas id="camembertChart" width="300" height="300"/></canvas>'."\n";
$camembert .= spaces($n + 2).'</td>'."\n";
// la légende :
$camembert .= spaces($n + 2).'<td align="center">'."\n";
$camembert .= spaces($n + 3).'<table align="center" border="0" width="200">'."\n";
$camembert .= spaces($n + 4).'<tr>'."\n";
$camembert .= spaces($n + 5).'<td align="left">'.$labels.'</td>'."\n";
$camembert .= spaces($n + 5).'<td align="right">'.$values.'</td>'."\n";
$camembert .= spaces($n + 4).'</tr>'."\n";
$camembert .= spaces($n + 3).'</table>'."\n";
$camembert .= spaces($n + 2).'</td>'."\n";
$camembert .= spaces($n + 1).'</tr>'."\n";
$camembert .= spaces($n).'</table>'."\n";



/****************************************************
    CALCUL DU RADAR
****************************************************/
$radar = '';
$radarImage = '';
// image du radar :
if ($radarCount > 0)
    $radarImage = '<canvas id="radarChart" width="340" height="340"/></canvas>'."\n";
// on récupère la liste des sous-rubriques du bulletin :
foreach ($sousRubriques as $sousRubrique)
    $subjectsLabels[$sousRubrique[0]] = array($sousRubrique[0], $sousRubrique[1]);
// mise en forme :
$n = 5;
$radar .= spaces($n).'<br/>'."\n";
$radar .= spaces($n).'<table align="center" border="0" width="70%">'."\n";
// le titre :
$radar .= spaces($n + 1).'<tr>'."\n";
$radar .= spaces($n + 2).'<td align="left"><h4>'.tr('By subject (radar):').'</h4></td>'."\n";
$radar .= spaces($n + 2).'<td></td>'."\n";
$radar .= spaces($n + 1).'</tr>'."\n";
$radar .= spaces($n + 1).'<tr>'."\n";
// le radar en lui-même :
$radar .= spaces($n + 2).'<td align="center">'."\n";
$radar .= spaces($n + 3).$radarImage."\n";
$radar .= spaces($n + 2).'</td>'."\n";
// la légende :
$radar .= spaces($n + 2).'<td align="center">'."\n";
$radar .= spaces($n + 3).'<table align="center" border="0" width="200">'."\n";
foreach ($radarData as $ROW)
    if ($ROW['value']) {
        $title = '';
        if (array_key_exists($ROW['name'], $subjectsLabels)) {
            $title = $subjectsLabels[$ROW['name']][1];
            }
        $radar .= spaces($n + 4).'<tr>'."\n";
        $radar .= spaces($n + 5).'<td class="with_title" title="'.$title.'" align="left">'.$ROW['name'].' : </td>'."\n";
        $radar .= spaces($n + 5).'<td align="center"><b>'.valeur2affichage($ROW['value']).'</b></td>'."\n";
        $radar .= spaces($n + 4).'</tr>'."\n";
        }
$radar .= spaces($n + 3).'</table>'."\n";
$radar .= spaces($n + 2).'</td>'."\n";
$radar .= spaces($n + 1).'</tr>'."\n";
$radar .= spaces($n).'</table>'."\n";



/****************************************************
    AFFICHAGE
****************************************************/
$result = '';
// titre en cas d'impression :
$print = spaces(2).'<!-- PRINTABLE HEADER -->'."\n";
$print .= spaces(2).'<div class="d-none d-print-block">'."\n";
$print .= spaces(3).'<p class="text-center">'.$printTitle.'</p>'."\n";
$print .= spaces(2).'</div>'."\n";
$result .= "\n".$print;
// titre :
$result .= "\n".$pageTitle;
// pour centrer l'affichage :
$result .= spaces(2).'<div class="container theme-showcase">'."\n";
$result .= $competencesTable.$jsData.$camembert.$radar;
$result .= spaces(2).'</div>'."\n";

// on vérifie si les élèves ont accès aux résultats de la période en cours :
if ($_SESSION['USER_MODE'] == 'eleve')
    if ((! SHOW_ACTUAL_PERIODE) and ($_SESSION['SELECTED_PERIOD'][0] >= $_SESSION['ACTUAL_PERIOD']))
        $result = "\n".$pageTitle.prohibitedPeriodMessage();

if ($ACTION == 'reload')
    exit($result);
else
    echo $result;

?>
