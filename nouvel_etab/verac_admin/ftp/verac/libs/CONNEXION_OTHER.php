<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    OBJET ET MÉTHODES POUR LA CONNEXION
    ET LES APPELS DIRECTS DEPUIS VÉRAC
****************************************************/

class CONNEXION_OTHER {

    private $DB_USERS;
    private $DB_COMPTEUR;
    private $DB_SUIVIS;

    private $no_write_users;
    private $no_write_compteur;
    public $no_db_suivis;


    public function __construct() {
        /*
        initialisation.
        On se connecte aux bases de données.
        */
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_USERS;
        if (is_readable($filename)) {
            $this -> DB_USERS = new PDO('sqlite:'.$filename);
            $this -> DB_USERS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        if (! is_writable($filename))
            $this -> no_write_users = True;
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_COMPTEUR;
            if (is_readable($filename)) {
            $this -> DB_COMPTEUR = new PDO('sqlite:'.$filename);
            $this -> DB_COMPTEUR -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        if (! is_writable($filename))
            $this -> no_write_compteur = True;
        $this -> CHEMIN_SUIVIS = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_SUIVIS;
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_SUIVIS;
        if (is_readable($filename)) {
            $this -> DB_SUIVIS = new PDO('sqlite:'.$filename);
            $this -> DB_SUIVIS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            $this -> no_db_suivis = False;
            }
        else
            $this -> no_db_suivis = True;
    }



    /****************************************************
        FONCTIONS UTILISÉES LORS DE LA CONNEXION
    ****************************************************/

    public function verifyLogin($login, $user_mode) {
        /*
        vérifie si le login proposé existe dans la base users.
        */
        $login_enc = doEncode($login);
        $SQL = 'SELECT * FROM '.$user_mode.' ';
        $SQL .= 'WHERE Login IN (:login1, :login256)';
        $OPT = array(':login1' => $login_enc[0], ':login256' => $login_enc[1]);
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        return ($STMT != '') ? $STMT -> fetch() : NULL;
        }

    public function verifyLoginPassword(
            $login, $password, $user_mode) {
        /*
        vérifie les login et mot de passe proposés lors de la connexion.
        */
        $login_enc = doEncode($login);
        $password_enc = doEncode($password);

        $SQL = 'SELECT * FROM '.$user_mode.'s ';
        $SQL .= 'WHERE Login IN (:login1, :login256) ';
        $SQL .= 'AND Mdp IN (:mdp1, :mdp256)';
        $OPT = array(
            ':login1' => $login_enc[0], ':login256' => $login_enc[1], 
            ':mdp1' => $password_enc[0], ':mdp256' => $password_enc[1]);
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        return ($STMT != '') ? $STMT -> fetch() : NULL;
        }

    public function saveConnexion($id_user, $user) {
        /*
        enregistrement dans la base compteur
        */
        if ($this -> no_write_compteur)
            return;
        ($user == 'eleve') ? $user = 'id_eleve' : $user = 'id_prof';
        $timestamp = time();
        $year = date('Y', $timestamp);
        $month = date('m', $timestamp);
        $week = date('W', $timestamp);
        $total = 0;
        $SQL = 'SELECT total FROM users ';
        $SQL .= 'WHERE year=:year AND month=:month ';
        $SQL .= 'AND week=:week AND '.$user.'=:id_user';
        $OPT = array(':year' => $year, ':month' => $month, ':week' => $week, ':id_user' => $id_user);
        $STMT = $this -> DB_COMPTEUR -> prepare($SQL);
        $STMT -> execute($OPT);
        $RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($RESULT as $row)
            $total = intval($row['total']);

        if ($total == 0) {
            // L'id ne se trouve pas dans la table, on va l'ajouter.
            $SQL = 'INSERT INTO users ('.$user.', week, month, year, total) ';
            $SQL .= 'VALUES(:id_user, :week, :month, :year, 1);';
            }
        else {
            // L'id se trouve déjà dans la table, on met juste à jour le total.
            $total += 1;
            $SQL = 'UPDATE users SET total=:total ';
            $SQL .= 'WHERE year=:year AND month=:month AND week=:week AND '.$user.'=:id_user;';
            $OPT[':total'] = $total;
            }
        $STMT = $this -> DB_COMPTEUR -> prepare($SQL);
        try {
            $this -> DB_COMPTEUR -> beginTransaction();
            $STMT -> execute($OPT);
            $this -> DB_COMPTEUR -> commit();
            }
        catch(PDOException $e) {
            $this -> DB_COMPTEUR -> rollBack();
            }
        }

    public function isEleveSuivi($id_eleve) {
        /*
        indique si l'élève fait l'objet d'une fiche de suivi
        */
        if ($this -> no_db_suivis)
            return False;
        $SQL = 'SELECT * FROM suivi_pp_eleve_cpt ';
        $SQL .= 'WHERE id_eleve=:id_eleve';
        $OPT = array(':id_eleve' => $id_eleve);
        $STMT = $this -> DB_SUIVIS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        return ($STMT -> fetch()>0) ? True : False;
        }

    public function saveIpLogin($ip, $login) {
        /*
        enregistre ip et login dans la base compteur en cas d'erreur
        */
        if ($this -> no_db_suivis)
            return False;
        $timestamp = time();
        $date = strval(date('dmY', $timestamp));
        $SQL = 'INSERT INTO error ';
        $SQL .= 'VALUES(:ip, :login, :time, :date);';
        $OPT = array(':ip' => $ip, ':login' => $login, ':time' => $timestamp, ':date' => $date);
        $STMT = $this -> DB_COMPTEUR -> prepare($SQL);
        try {
            $this -> DB_COMPTEUR -> beginTransaction();
            $STMT -> execute($OPT);
            $this -> DB_COMPTEUR -> commit();
            }
        catch (PDOException $e) {
            $this -> DB_COMPTEUR -> rollBack();
            }
        }

    public function verifyIpLogin($ip, $login) {
        /*
        on ralentit les tentatives de recherche de mdp par force brute
        */
        if ($this -> no_write_compteur)
            return True;
        $retour = True;
        $timestamp = time();
        // on cherche les enregistrements de moins de 15 min
        $timestamp_15min = $timestamp - (60 * 15);
        $SQL = 'SELECT COUNT(*) AS total_error ';
        $SQL .= 'FROM error ';
        $SQL .= 'WHERE login=:login ';
        $SQL .= 'AND ip=:ip ';
        $SQL .= 'AND timestamp>:time_15';
        $OPT = array(':login' => $login, ':ip' => $ip, ':time_15' => $timestamp_15min);
        $STMT = $this -> DB_COMPTEUR -> prepare($SQL);
        $STMT -> execute($OPT);
        $RESULT = $STMT -> fetch();
        $total_error = intval($RESULT['total_error']);
        if ($total_error > 9)
            $retour = False;
        return $retour;
        }



    /****************************************************
        FONCTIONS UTILISÉES LORS
        DES APPELS DIRECTS DEPUIS VÉRAC
    ****************************************************/

    public function verifyTeacherRemoteAccess(
            $login, $password) {
        /*
        vérifier un accès distant au site par un prof
            * @param string $login    le login de l'utilisateur
            * @param string $password    le mdp à vérifier
            * @return int           l'id de l'utilisateur
        */
        $RESULT = -2;
        $password_enc = doEncode($password);
        $login_enc = doEncode($login);
        $SQL = 'SELECT id, Mdp FROM profs ';
        $SQL .= 'WHERE Login IN (:login1, :login256)';
        $OPT = array(':login1' => $login_enc[0], ':login256' => $login_enc[1]);
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> execute($OPT);
        if ($STMT != '') {
            $TEMP_RESULT = $STMT -> fetch();
            if ($TEMP_RESULT['Mdp'] == $password_enc[0])
                $RESULT = $TEMP_RESULT['id'];
            elseif ($TEMP_RESULT['Mdp'] == $password_enc[1])
                $RESULT = $TEMP_RESULT['id'];
            else
                $RESULT = -1;
            }
        return $RESULT;
        }

    public function readConfigProf(
            $key, $id_prof, $default_int=0, $default_text='') {
        /*
        lire et retourner une clé de la table prof.config
            * @param string     $key
            * @param int        $id_prof
            * @return array()
        */
        $CHEMIN_BASE_PROF = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .DOSSIER_UP.PREFIXE_DB_PROF;
        $filename = $CHEMIN_BASE_PROF.$id_prof.'.sqlite';
        if (! is_readable($filename))
            return False;
        if (filesize($filename) < 10)
            return False;
        $db = new PDO('sqlite:'.$filename);
        $db -> setAttribute( PDO::ATTR_ERRMODE, DEBUG_PDO );
        $SQL = 'SELECT value_int AS INT, value_text AS TXT FROM config';
        $SQL.= ' WHERE key_name=:key';
        $OPT = array(':key' => $key);
        $STMT = $db -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        if ($STMT != '')
            $result = $STMT -> fetch();
        if (! isset($result['INT']))
            $result = array('INT' => $default_int, 'TXT' => $default_text);
        return $result;
        }

    public function changePassword($password, $user_id, $user_mode) {
        /*
        on écrit le nouveau mot de passe dans la table eleves ou profs
        */
        $result = False;
        if ($this -> no_write_users)
            return $result;
        $table = ($user_mode == 'eleve') ? 'eleves' : 'profs';
        $password_enc = doEncode($password, $algo='sha256');
        $SQL = 'UPDATE '.$table.' ';
        $SQL .= 'SET Mdp=:mdp ';
        $SQL .= 'WHERE id=:id';
        $OPT = array(':id' => $user_id, ':mdp' => $password_enc);
        $STMT = $this -> DB_USERS -> prepare($SQL);
        try {
            $this -> DB_USERS -> beginTransaction();
            $STMT -> execute($OPT);
            $this -> DB_USERS -> commit();
            $result = True;
            }
        catch (PDOException $e) {
            $this -> DB_USERS -> rollBack();
            }
        return $result;
        }

    public function updateUpload($filename) {
        /*
        mise à jour du nombre d'uploads dans la base compteur.
        */
        if ($this -> no_write_compteur)
            return;
        $timestamp = time();
        $year = date('Y', $timestamp);
        $month = date('m', $timestamp);
        $week = date('W', $timestamp);

        // on récupère l'id depuis le nom du fichier
        $id = str_replace('.sqlite', '', $filename);
        $id = str_replace(PREFIXE_DB_PROF, '', $id);

        $total = '';
        $SQL = 'SELECT total AS TOTAL FROM upload ';
        $SQL .= 'WHERE year=:year AND month=:month ';
        $SQL .= 'AND week=:week AND id_prof=:id_user';
        $OPT = array(':year' => $year, ':month' => $month, ':week' => $week, ':id_user' => $id);
        $STMT = $this -> DB_COMPTEUR -> prepare($SQL);
        $STMT -> execute($OPT);
        if ($STMT != "") {
            $RESULT = $STMT -> fetch();
            $total = intval($RESULT[0]['TOTAL']);
            }

        if ($total == '') {
            // L'id ne se trouve pas dans la table, on va l'ajouter.
            $SQL = 'INSERT INTO upload (id_prof, week, month, year, total) ';
            $SQL .= 'VALUES(:id_user, :week, :month, :year, 1);';
            }
        else {
            // L'id se trouve déjà dans la table, on met juste à jour le total.
            $total += 1;
            $SQL = 'UPDATE upload SET total=:total ';
            $SQL .= 'WHERE year=:year AND month=:month AND week=:week AND id_prof=:id_user;';
            $OPT[':total'] = $total;
            }
        $STMT = $this -> DB_COMPTEUR -> prepare($SQL);
        try {
            $this -> DB_COMPTEUR -> beginTransaction();
            $STMT -> execute($OPT);
            $this -> DB_COMPTEUR -> commit();
            }
        catch(PDOException $e) {
            $this -> DB_COMPTEUR -> rollBack();
            }
    }

    public function cleanCompteurDB($nb_semaines=13) {
        /*
        on nettoie la base compteur des enregistrements trop anciens
            * @param int $nb_semaines     le nombre de semaines à conserver
        */
        if ($this -> no_write_compteur)
            return;
        $timestamp = time();
        $year = date('Y', $timestamp);
        $month = date('m', $timestamp);
        $week = date('W', $timestamp);
        // cas des premiers jours de l'année :
        if (($month == 1) && ($week == 52))
            $week = 1;

        // On stocke dans une variable le timestamp qu'il était il y a x semaines :
        // 90jours * 24heures * 60minutes * 60secondes
        $timestamp_before = $timestamp - ($nb_semaines * 7 * 24 * 60 * 60);

        $sql = 'DELETE FROM error WHERE timestamp<'.$timestamp_before.';';
        $this -> DB_COMPTEUR -> query($sql);
        // ou encore x semaines :
        if ($week < $nb_semaines) {
            // début de l'année civile ; on en profite pour nettoyer :
            $week_before = (53 - $nb_semaines) + $week;
            $sql_end = ' WHERE week>'.$week.' AND week<'.$week_before.';';
            }
        else {
            // on en profite aussi pour nettoyer :
            $week_before = $week - $nb_semaines;
            $sql_end = ' WHERE week>'.$week.' OR week<'.$week_before.' OR year<'.$year.';';
            }
        foreach (array('users', 'upload') as $base)
            $this -> DB_COMPTEUR -> query('DELETE FROM '.$base.$sql_end);
        }

    public function readFollowsEvals($filename) {
        /*
        lit le contenu du fichier d'évaluation des élèves suivis
        envoyé par un prof.
            * @param string $filename    le nom du fichier à récupérer (envoyé par un prof)
            * @return array              la liste de PP et des évaluations
        */
        $RESULT = array(array(), array());
        if ($this -> no_db_suivis)
            return $RESULT;
        if (! is_readable($filename))
            return $RESULT;

        $db_suivisxx = new PDO('sqlite:'.$filename);
        $db_suivisxx -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

        $SQL = 'SELECT * FROM suivi_evals';
        $OPT = array();
        $STMT = $db_suivisxx -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        $suivi_evals = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($suivi_evals as $suivi_eval) {
            $ligne = array();
            $ligne[] = $suivi_eval['id_prof'];
            $ligne[] = $suivi_eval['matiere'];
            $id_PP = $suivi_eval['id_pp'];
            $ligne[] = $id_PP;
            $ligne[] = $suivi_eval['id_eleve'];
            $ligne[] = $suivi_eval['date'];
            $ligne[] = $suivi_eval['horaire'];
            $ligne[] = $suivi_eval['id_cpt'];
            $ligne[] = $suivi_eval['value'];
            $RESULT[1][] = $ligne;
            if (! in_array($id_PP, $RESULT[0]))
                $RESULT[0][] = $id_PP;
            }
        return $RESULT;
        }

    public function writeFollowsEvals($followsEvals) {
        /*
        inscription des évaluation des élèves suivis
        dans la base suivi_pp.sqlite
            * @param array $followsEvals    
            * @return bool             
        */
        $RESULT = '|';

        // on prépare la suppression :
        $DEL_SQL = 'DELETE FROM suivi_evals';
        $DEL_SQL .= ' WHERE id_prof=:id_prof AND matiere=:matiere AND id_pp=:id_pp';
        $DEL_SQL .= ' AND id_eleve=:id_eleve AND date=:date AND horaire=:horaire';
        $DEL_SQL .= ' AND id_cpt=:id_cpt';
        // et les enregistrements :
        $STMT_SQL = 'INSERT INTO suivi_evals';
        $STMT_SQL .= ' VALUES(:id_prof, :matiere, :id_pp, :id_eleve,';
        $STMT_SQL .= ' :date, :horaire, :id_cpt, :value)';

        // inscription dans les bases suivi_pp.sqlite :
        foreach ($followsEvals[0] as $id_PP) {
            $filename = $this -> CHEMIN_SUIVIS.'suivi_'.$id_PP.'.sqlite';
            if (! is_readable($filename))
                $RESULT .= $id_PP.':-1|';
            else {
                $db_suivisPP = new PDO('sqlite:'.$filename);
                $db_suivisPP -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                try {
                    // on y va
                    $db_suivisPP -> beginTransaction();
                    foreach ($followsEvals[1] as $suivi_eval)
                        if ($suivi_eval[2] == $id_PP) {
                            $OPT_SUIVI = array();
                            $OPT_SUIVI[':id_prof'] = $suivi_eval[0];
                            $OPT_SUIVI[':matiere'] = $suivi_eval[1];
                            $OPT_SUIVI[':id_pp'] = $suivi_eval[2];
                            $OPT_SUIVI[':id_eleve'] = $suivi_eval[3];
                            $OPT_SUIVI[':date'] = $suivi_eval[4];
                            $OPT_SUIVI[':horaire'] = $suivi_eval[5];
                            $OPT_SUIVI[':id_cpt'] = $suivi_eval[6];
                            $DEL_SUIVI = $db_suivisPP -> prepare($DEL_SQL);
                            $DEL_SUIVI -> execute($OPT_SUIVI);

                            $value = $suivi_eval[7];
                            // la valeur XXX permet au prof d'effacer un enregistrement
                            if ($value != "XXX") {
                                $OPT_SUIVI[':value'] = $value;
                                $STMT_SUIVI = $db_suivisPP -> prepare($STMT_SQL);
                                $STMT_SUIVI -> execute($OPT_SUIVI);
                                }
                            }
                    $db_suivisPP -> commit();
                    $RESULT .= $id_PP.':1|';
                    }
                catch(PDOException $e) {
                    $RESULT .= $id_PP.':-2|';
                    $db_suivisPP -> rollBack();
                    }
                }
            }
        return $RESULT;
        }

    public function readFollowsDefines($filename) {
        /*
        lit le fichier de définition des élèves suivis
        envoyé par un PP.
            * @param string $filename    le nom du fichier à récupérer (envoyé par un PP)
            * @return array              la liste des élèves suivis et des compétences suivies
        */
        $RESULT = array();
        if ($this -> no_db_suivis)
            return $RESULT;
        if (! is_readable($filename))
            return $RESULT;

        $db_suivisPP = new PDO('sqlite:'.$filename);
        $db_suivisPP -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

        $SQL = 'SELECT * FROM suivi_pp_eleve_cpt';
        $OPT = array();
        $STMT = $db_suivisPP -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        $suivi_defines = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($suivi_defines as $suivi_define) {
            $ligne = array();
            $ligne[] = $suivi_define['id_pp'];
            $ligne[] = $suivi_define['id_eleve'];
            $ligne[] = $suivi_define['id_cpt'];
            $ligne[] = $suivi_define['label2'];
            $RESULT[] = $ligne;
            }
        return $RESULT;
        }

    public function writeFollowsDefines($id_PP, $pp_eleves_suivis) {
        /*
            * @param int $id_PP    
            * @param array $pp_eleves_suivis    
        */
        $RESULT = '|';
        if ($this -> no_db_suivis)
            return $RESULT.$id_PP.':-1|';

        // inscription dans les bases suivis.sqlite et suivi_PP.sqlite :
        $filename = $this -> CHEMIN_SUIVIS.'suivi_'.$id_PP.'.sqlite';
        if (! is_readable($filename))
            return $RESULT.$id_PP.':-2|';

        $db_suivisPP = new PDO('sqlite:'.$filename);
        $db_suivisPP -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

        // on prépare la suppression :
        $DEL_SQL = 'DELETE FROM suivi_pp_eleve_cpt';
        $DEL_SQL .= ' WHERE id_pp=:id_pp';
        // et les enregistrements :
        $STMT_SQL = 'INSERT INTO suivi_pp_eleve_cpt';
        $STMT_SQL .= ' VALUES(:id_pp, :id_eleve, :id_cpt, :label2)';

        try {
            // on y va
            $db_suivisPP -> beginTransaction();
            $this -> DB_SUIVIS -> beginTransaction();

            $OPT_SUIVI = array();
            $OPT_SUIVI[':id_pp'] = $id_PP;
            $DEL_SUIVI = $db_suivisPP -> prepare($DEL_SQL);
            $DEL_SUIVI -> execute($OPT_SUIVI);
            $DEL_SUIVI = $this -> DB_SUIVIS -> prepare($DEL_SQL);
            $DEL_SUIVI -> execute($OPT_SUIVI);

            foreach ($pp_eleves_suivis as $pp_eleve_suivi) {
                $OPT_SUIVI[':id_eleve'] = $pp_eleve_suivi[1];
                $OPT_SUIVI[':id_cpt'] = $pp_eleve_suivi[2];
                $OPT_SUIVI[':label2'] = $pp_eleve_suivi[3];
                $STMT_SUIVI = $db_suivisPP -> prepare($STMT_SQL);
                $STMT_SUIVI -> execute($OPT_SUIVI);
                $STMT_SUIVI = $this -> DB_SUIVIS -> prepare($STMT_SQL);
                $STMT_SUIVI -> execute($OPT_SUIVI);
                }
            $db_suivisPP -> commit();
            $this -> DB_SUIVIS -> commit();
            $RESULT .= $id_PP.':1|';
            }
        catch(PDOException $e) {
            $RESULT .= $id_PP.':-3|';
            $db_suivisPP -> rollBack();
            $this -> DB_SUIVIS -> rollBack();
            }
        return $RESULT;
        }

    public function listStudentsFromIds($id_students, $mode='ALL') {
        /*
        retourne la liste des élèves d'après leurs id
        */
        $SQL = 'SELECT id, NOM, Prenom, Classe FROM eleves ';
        $SQL .= 'WHERE id IN ('.$id_students.') ';
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> execute();
        if ($STMT != '') {
            $result = array();
            $tempResult = array();
            while ($ROW = $STMT -> fetch()) {
                $tmp = array();
                $tmp[0] = $ROW[0];
                $nom = formatName($ROW, $mode=$mode);
                $tmp[1] = $nom;
                if ($mode == 'TROMBI')
                    $tmp[2] = formatName($ROW);
                $tmp[] = $ROW[3];
                $tempResult[doAscii($nom)] = $tmp;
                }
            //uksort($tempResult, 'strcasecmp');
            foreach ($tempResult as $key => $val)
                $result[$val[0]] = $val;
            return $result;
            }
        return array();
        }




    }
?>
