<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    OBJET ET MÉTHODES POUR L'ÉLÈVE
****************************************************/

class CONNEXION_ELEVE {

    public $DB_USERS;
    public $DB_COMMUN;
    public $DB_RESULTATS;
    public $DB_DOCUMENT;
    public $DB_SUIVIS;

    private $CHEMIN_PROF;
    public $CHEMIN_SUIVIS;
    public $no_db_suivis;


    public function __construct($period='') {
        /*
        initialisation.
        On se connecte aux bases de données.
        */
        $this -> CHEMIN_PROF = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .DOSSIER_UP.PREFIXE_DB_PROF;
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_USERS;
        if (is_readable($filename)) {
            $this -> DB_USERS = new PDO('sqlite:'.$filename);
            $this -> DB_USERS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        if (($period == '') or (intval($period) >= intval($_SESSION['ACTUAL_PERIOD']))) {
            $suffixe_base_commun = '.sqlite';
            $suffixe_base_resultat = '.sqlite';
            }
        else {
            $suffixe_base_commun = '-'.$period.'.sqlite';
            $suffixe_base_resultat = '-'.$period.'.sqlite';
            }
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .VERAC_DB_COMMUN.$suffixe_base_commun;
        if (is_readable($filename)) {
            $this -> DB_COMMUN = new PDO('sqlite:'.$filename);
            $this -> DB_COMMUN -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .VERAC_DB_RESULTATS.$suffixe_base_resultat;
        if (is_readable($filename)) {
            $this -> DB_RESULTATS = new PDO('sqlite:'.$filename);
            $this -> DB_RESULTATS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_DOCUMENT;
        if (is_readable($filename)) {
            $this -> DB_DOCUMENT = new PDO('sqlite:'.$filename);
            $this -> DB_DOCUMENT -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        $this -> CHEMIN_SUIVIS = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .DOSSIER_SUIVIS;
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_SUIVIS;
        if (is_readable($filename)) {
            $this -> DB_SUIVIS = new PDO('sqlite:'.$filename);
            $this -> DB_SUIVIS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            $this -> no_db_suivis = False;
            }
        else
            $this -> no_db_suivis = True;
        }



    public function connectTeacherDB($id_prof) {
        /*
        connexion à une base prof.
        Utilisé dans la page student_details
        */
        if ($id_prof < 0)
            return False;
        $filename = $this -> CHEMIN_PROF.$id_prof.'.sqlite';
        if (! is_readable($filename))
            return False;
        if (filesize($filename) < 10)
            return False;
        $db = new PDO('sqlite:'.$filename);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        return $db;
        }

    public function connectReferentialPropositionsDB() {
        /*
        connexion à la base referential_propositions.sqlite.
        Utilisé dans les pages student_validations et class_validations
        */
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF
            .DIRECTORY_SEPARATOR.VERAC_DB_REFERENTIAL_PROPOSITIONS;
        if (! file_exists($filename)) {
            // si pas encore mise à jour :
            $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF
                .DIRECTORY_SEPARATOR.DOSSIER_PROTECTED.'validations.sqlite';
            }
        if (! file_exists($filename))
            return False;
        if (! is_readable($filename))
            return False;
        if (filesize($filename) < 10)
            return False;
        $db = new PDO('sqlite:'.$filename);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        return $db;
        }

    public function connectReferentialValidationsDB() {
        /*
        connexion à la base referential_validations.sqlite.
        Utilisé dans les pages student_validations et class_validations
        */
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF
            .DIRECTORY_SEPARATOR.VERAC_DB_REFERENTIAL_VALIDATIONS;
        if (! file_exists($filename))
            return False;
        if (! is_readable($filename))
            return False;
        if (filesize($filename) < 10)
            return False;
        $db = new PDO('sqlite:'.$filename);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        return $db;
        }



    }
?>
