<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    FONCTIONS DE SESSION
****************************************************/

/*
LISTE DES VARIABLES :
-------------------------
    toujours là :
-------------------------
        INITIAL_TIME (int) : horaire de la connexion
        LAST_TIME (int) : horaire de la dernière action
        INACTIVE_TIME (int) : calcul du temps d'inactivité (pour déconnexion automatique)
        NAVBAR_STYLE (string) : aspect de la navbar (default ou inverse ; mis en cookie)
        USER_MODE (string) : public, eleve ou prof
        ACTUAL_PERIOD (int) : période actuelle (définie par l'administrateur)
        PAGE (string) : nom de la page demandée
        ACTION (string) : nom de l'action demandée
        WHY (string) : raison de la création de session (LOGIN, FIRST_TIME, INACTIVE_TIME, )
        TEMP_ARRAY (array) : tableau temporaire utile pour certaines pages
        TEMP_FILES (array) : tableau des fichiers temporaires utile pour certaines pages
        TEMP (string) : texte temporaire utile pour certaines pages
        TEMP_FILE (string) : nom du fichier (papers.php)
        WINDOW_WIDTH (int) : largeur de la fenêtre
        WINDOW_HEIGHT (int) : hauteur de la fenêtre
        DETAILS_DIRECTORY (int) : sens d'affichage des détails
                                    0 : Bilans → Items
                                    1 : Items → Bilans
        DETAILS_SUBJECT (string) : matière sélectionnée dans la page détails
        VALIDATIONS_SUBJECT (string) : matière sélectionnée dans la page validations
        FOLLOWS_DIRECTORY (int) : sens d'affichage des évaluations suivies
                                    0 : Chronologique inverse
                                    1 : Chronologique
        COOKIE_LOGIN (string) : login à mettre en cookie (si modifié)
-------------------------
    élève :
-------------------------
        USER_ID (int) : id de l'élève
        USER_NAME (string) : nom de l'élève
        USER_CLASS (array(id, name)) : classe de l'élève
        USER_MUST_CHANGE_PASSWORD (boolean) : indique si le mot de passe doit être changé
        USER_SUIVI (boolean) : indique si l'élève est suivi
        SELECTED_PERIOD (array(id, name)) : la période sélectionnée
-------------------------
    prof :
-------------------------
        USER_ID (int) : id du prof
        USER_NAME (string) : nom du prof
        USER_CLASS (array(id, name)) :  (-2, '') par défaut
                                        (id, name) de la classe pour un Prof Principal
                                        (-1, '') pour directeur
        USER_MUST_CHANGE_PASSWORD (boolean) : indique si le mot de passe doit être changé
        MATIERE (string) : matière du prof
        SELECTED_PERIOD (array(id, name)) : la période sélectionnée
        SELECTED_CLASS (array(id, name)) : la classe sélectionnée
        SELECTED_GROUP (array(id, name)) : le groupe sélectionné (en mode évaluation)
        SELECTED_TABLEAU (array(id, name)) : le tableau sélectionné (en mode évaluation)
        SELECTED_VUE (array(id, name)) : la vue sélectionnée (en mode évaluation)
        SELECTED_STUDENT (array(id, name)) : l'élève sélectionné
        DB_PROF_EXISTS (boolean) : le fichier profxx.sqlite existe
        DB_PROF_OK (boolean) : le fichier profxx.sqlite est utilisable (on peut y écrire)
        ASSESS_PERIOD (array(id, name)) : la période sélectionnée
        ASSESS_WHAT_IS_MODIFIED (array) : (en mode évaluation)
        ASSESS_TABLEAUX_FOR_ITEM (array) : (en mode évaluation, pour les compilations)
        PROTECTED_PERIODS (array) : (en mode évaluation)
        RECALCUL_BILAN (boolean) :  (en mode évaluation)
*/



function getActualPeriod() {
    /*
    pour récupérer la période en cours (en testant l'existence des fichiers resultats)
    */
    $period = -1;
    $test = True;
    while ($test) {
        $period++;
        $suffixe = ($period == 0) ? '.sqlite' : '-'.$period.'.sqlite';
        $nom_fichier = CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_RESULTATS.$suffixe;
        $test = (file_exists($nom_fichier)) ? True : False;
        }
    return $period;
    }

function createSession($why='') {
    /*
    Création d'une nouvelle session.
    La variable $why indique la raison de l'appel à la fonction
    (LOGIN, FIRST_TIME, INACTIVE_TIME, ).
    */
    if ($why == 'FIRST_TIME')
        session_destroy();
    session_name('verac_web');
    if (PHP_VERSION_ID < 70300)
        session_start();
    else
        session_start(['cookie_samesite' => ' Lax']);
    //session_start();
    $_SESSION = array();
    $_SESSION['INITIAL_TIME'] = 0;
    $_SESSION['LAST_TIME'] = time();
    $_SESSION['INACTIVE_TIME'] = 0;
    // locales :
    if (isset($_COOKIE['verac_lang']))
        $_SESSION['LANG'] = htmlentities($_COOKIE['verac_lang'], ENT_QUOTES);
    else
        $_SESSION['LANG'] = LANG;
    if ($_SESSION['LANG'] == 'fr')
        $_SESSION['LANG'] = 'fr_FR';
    if (isset($_COOKIE['verac_navbar_style'])) {
        $_SESSION['NAVBAR_STYLE'] =  htmlentities($_COOKIE['verac_navbar_style'], ENT_QUOTES);
        if ($_SESSION['NAVBAR_STYLE'] != 'dark')
            $_SESSION['NAVBAR_STYLE'] = 'light';
        }
    else
        $_SESSION['NAVBAR_STYLE'] = 'light';
    // Données associées à l'utilisateur.
    $_SESSION['USER_MODE'] = 'public';
    // gestion des connexions
    $_SESSION['ACTUAL_PERIOD'] = getActualPeriod();
    $_SESSION['PAGE'] = 'login';
    $_SESSION['ACTION'] = '';
    $_SESSION['WHY'] = $why;
    $_SESSION['TEMP_ARRAY'] = array();
    $_SESSION['TEMP_FILES'] = array();
    $_SESSION['TEMP'] = '';
    $_SESSION['TEMP_FILE'] = '';
    $_SESSION['TEMP_ALERT'] = '';
    $_SESSION['WINDOW_WIDTH'] = 0;
    $_SESSION['WINDOW_HEIGHT'] = 0;
    $_SESSION['DETAILS_DIRECTORY'] = 0;
    $_SESSION['DETAILS_SUBJECT'] = '';
    $_SESSION['VALIDATIONS_SUBJECT'] = '';
    $_SESSION['FOLLOWS_DIRECTORY'] = 0;
    $_SESSION['COOKIE_LOGIN'] = '';
    }

function updateSession() {
    /*
    Mise à jour de l'état de la session.
    Fonction appelée systématiquement depuis index.php.
    */
    session_name('verac_web');
    if (PHP_VERSION_ID < 70300)
        session_start();
    else
        session_start(['cookie_samesite' => ' Lax']);
    //session_start();
    if (! $_SESSION)
        createSession('FIRST_TIME');
    $_SESSION['INACTIVE_TIME'] = (time() - $_SESSION['LAST_TIME']);
    if ($_SESSION['INACTIVE_TIME'] > TPS_MAX_INACTIVITE)
        createSession('INACTIVE_TIME');
    else
        $_SESSION['LAST_TIME'] = time();
    }

function openSession($login, $password) {
    /*
    Fonction appelée depuis la page login.php.
    ÉTATS DE SORTIE :
        9 : valeur initiale
        8 : trop de tests avec erreur (on endort 15 min)
        5 : login incorrect
        3 : password incorrect
        0 : OK (connexion réussie)
    */
    $result = 9;
    $CONNEXION = new CONNEXION_OTHER;
    // test du nombre de tentatives de connexion :
    if (! $CONNEXION -> verifyIpLogin($_SERVER['REMOTE_ADDR'], $login))
        return 8;
    // test du login (on teste d'abord la base eleves puis profs) :
    $record = $CONNEXION -> verifyLogin($login, 'eleves');
    $user_mode = 'eleve';
    if (! $record) {
        $record = $CONNEXION -> verifyLogin($login, 'profs');
        $user_mode = 'prof';
        }
    if (! $record)
        return 5;
    // test du password :
    $record = $CONNEXION -> verifyLoginPassword($login, $password, $user_mode);
    if ($record) {
        // connexion réussie :
        $_SESSION['USER_MODE'] = $user_mode;
        $_SESSION['USER_ID'] = intval($record['id']);
        $_SESSION['USER_NAME'] = $record['NOM'].' '.$record['Prenom'];
        $_SESSION['USER_CLASS'] = array(-2, '');
        $_SESSION['USER_MUST_CHANGE_PASSWORD'] = True;
        if ($_SESSION['USER_MODE'] == 'eleve') {
            $classeName = $record['Classe'];
            $_SESSION['USER_SUIVI'] = $CONNEXION -> isEleveSuivi($_SESSION['USER_ID']);
            $_SESSION['SELECTED_PERIOD'] = array($_SESSION['ACTUAL_PERIOD'], '');
            // on se connecte en tant qu'élève pour récupérer l'id de la classe :
            require_once(VERAC_CONNEXION_ELEVE);
            $CONNEXION2 = new CONNEXION_ELEVE($_SESSION['ACTUAL_PERIOD']);
            $infos = getClassInfos($CONNEXION2, $classeName, -1);
            $_SESSION['USER_CLASS'] = array($infos['id'], $classeName);
            // pour compatibilité :
            $_SESSION['SELECTED_STUDENT'] = array(
                $_SESSION['USER_ID'], $_SESSION['USER_NAME']);
            $_SESSION['SELECTED_CLASS'] = array($infos['id'], $classeName);
            }
        elseif ($_SESSION['USER_MODE'] == 'prof') {
            $_SESSION['MATIERE'] = $record['Matiere'];
            $_SESSION['CAN_EVALUATE'] = True;
            if (empty($_SESSION['MATIERE']))
                $_SESSION['CAN_EVALUATE'] = False;
            $_SESSION['SELECTED_PERIOD'] = array($_SESSION['ACTUAL_PERIOD'], '');
            $_SESSION['SELECTED_CLASS'] = array(-1, '');
            $_SESSION['SELECTED_GROUP'] = array(-1, '');
            $_SESSION['SELECTED_TABLEAU'] = array(-1, '');
            $_SESSION['SELECTED_VUE'] = array(-1, '');
            $_SESSION['SELECTED_STUDENT'] = array(-1, '');
            $_SESSION['ASSESS_PERIOD'] = array($_SESSION['ACTUAL_PERIOD'], '');
            $_SESSION['ASSESS_WHAT_IS_MODIFIED'] = 
                array('mustSaveDateTime' => False, 'id_eleves' => array(), 'id_items' => array());
            $_SESSION['ASSESS_TABLEAUX_FOR_ITEM'] = array();
            $_SESSION['PROTECTED_PERIODS'] = array();
            $_SESSION['RECALCUL_BILAN'] = False;
            // on se connecte en tant que prof pour récupérer d'autres trucs :
            require_once(VERAC_CONNEXION_PROF);
            $CONNEXION2 = new CONNEXION_PROF($_SESSION['USER_ID'], $_SESSION['ACTUAL_PERIOD']);
            $_SESSION['DB_PROF_EXISTS'] = $CONNEXION2 -> DB_PROF_EXISTS;
            $_SESSION['DB_PROF_OK'] = $CONNEXION2 -> DB_PROF_OK;
            if ($_SESSION['USER_ID'] >= DECALAGE_DIRECTEUR) {
                $_SESSION['CAN_EVALUATE'] = True;
                $_SESSION['USER_CLASS'][0] = -1;
                }
            elseif ($_SESSION['USER_ID'] >= DECALAGE_PP) {
                $className = $CONNEXION2 -> classNamePP($_SESSION['MATIERE']);
                $infos = getClassInfos($CONNEXION2, $className, -1);
                $_SESSION['USER_CLASS'] = array($infos['id'], $className);
                if ($infos['id'] > -1)
                    $_SESSION['SELECTED_CLASS'] = array($infos['id'], $className);
                }
            }
        $password_enc = doEncode($password);
        if ($password_enc[0] == $record['initialMdp'])
            $_SESSION['USER_MUST_CHANGE_PASSWORD'] = True;
        elseif ($password_enc[1] == $record['initialMdp'])
            $_SESSION['USER_MUST_CHANGE_PASSWORD'] = True;
        else
            $_SESSION['USER_MUST_CHANGE_PASSWORD'] = False;
        $_SESSION['INITIAL_TIME'] = time();
        $CONNEXION -> saveConnexion($_SESSION['USER_ID'], $_SESSION['USER_MODE']);
        return 0;
        }
    else {
        // il y a eu erreur, on enregistre l'ip et le login :
        $CONNEXION -> saveIpLogin($_SERVER['REMOTE_ADDR'], $login);
        return 3;
        }
    // au cas où :
    return $result;
    }



?>
