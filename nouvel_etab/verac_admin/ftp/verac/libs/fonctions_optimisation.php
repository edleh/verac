<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



function fileInCache($fileName) {
    /*
    Retourne ou crée si besoin la version du fichier située dans le cache.
    @param string $fileName           le nom du fichier
    @return string                    le chemin vers le fichier en cache
    */
    // création du dossier cache si besoin :
    $cacheDir = CHEMIN_VERAC.'cache';
    if (!file_exists($cacheDir))
        mkdir($cacheDir);
    // si le fichier cache existe, on le retourne :
    $infos = pathinfo($fileName);
    $baseName = $infos['basename'];
    $cacheFileName = 'cache/'.$baseName;
    $absoluteCacheFileName = CHEMIN_VERAC.$cacheFileName;
    if (file_exists($absoluteCacheFileName))
        return $cacheFileName;
    // si le fichier initial n'existe pas :
    $absoluteFileName = CHEMIN_VERAC.$fileName;
    if (! is_file($absoluteFileName) || ! file_exists($absoluteFileName))
        return $fileName;
    // on crée le fichier cache :
    $content = file_get_contents($absoluteFileName);
    $newContent = minify_js($content);
    $file = fopen($absoluteCacheFileName, "w");
    fwrite($file, $newContent);
    fclose($file);
    if (! file_exists($absoluteCacheFileName))
        return $fileName;
    return $cacheFileName;
    }

function minify_js($input) {
    /*
    http://code.seebz.net/p/minify-js
    */
    $output = '';

    $inQuotes = array();
    $noSpacesAround = '{}()[]<>|&!?:;,+-*/="\'';

    $input = preg_replace("`(\r\n|\r)`", "\n", $input);
    $inputs = str_split($input);
    $inputs_count = count($inputs);
    $prevChr = null;
    for ($i = 0; $i < $inputs_count; $i++) {
        $chr = $inputs[$i];
        $nextChr = $i+1 < $inputs_count ? $inputs[$i+1] : null;

        switch($chr) {
            case '/':
                if (!count($inQuotes) && $nextChr == '*' && $inputs[$i+2] != '@') {
                    $i = 1 + strpos($input, '*/', $i);
                    continue 2;
                    }
                elseif (!count($inQuotes) && $nextChr == '/') {
                    $i = strpos($input, "\n", $i);
                    continue 2;
                    }
                elseif (!count($inQuotes)) {
                    // C'est peut-être le début d'une RegExp
                    $eolPos = strpos($input, "\n", $i);
                    if ($eolPos === False)
                        $eolPos = $inputs_count;
                    $eol = substr($input, $i, $eolPos-$i);
                    if (!preg_match('`^(/.+(?<=\\\/)/(?!/)[gim]*)[^gim]`U', $eol, $m))
                        preg_match('`^(/.+(?<!/)/(?!/)[gim]*)[^gim]`U', $eol, $m);
                    if (isset($m[1])) {
                        // C'est bien une RegExp, on la retourne telle quelle
                        $output .= $m[1];
                        $i += strlen($m[1])-1;
                        continue 2;
                        }
                    }
                break;

            case "'":
            case '"':
                if ($prevChr != '\\' || ($prevChr == '\\' && $inputs[$i-2] == '\\')) {
                    if (end($inQuotes) == $chr)
                        array_pop($inQuotes);
                    elseif (!count($inQuotes))
                        $inQuotes[] = $chr;
                    }
                break;

            case ' ':
            case "\t":
            case "\n":
                if (!count($inQuotes)) {
                    if (strstr("{$noSpacesAround} \t\n", $nextChr)
                        || strstr("{$noSpacesAround} \t\n", $prevChr))
                        continue 2;
                    $chr = ' ';
                    }
                break;

            default:
                break;
            }

        $output .= $chr;
        $prevChr = $chr;
        }
    $output = trim($output);
    $output = str_replace(';}', '}', $output);
    $licence = 
        '/*!'."\n"
        .' * This file is a part of VÉRAC project (https://verac.tuxfamily.org).'."\n"
        .' * @source: https://gitlab.com/edleh/verac'."\n"
        .' * @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later'."\n"
        .' * @license-end'."\n"
        .' */';
    $output = $licence."\n".$output;
    return $output;
    }


?>
