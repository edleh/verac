<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    FONCTIONS GENERALES
****************************************************/

function doEncode($texte, $algo='') {
    /*
    encode un texte en Sha1 ou Sha256 
    (pour mots de passe et logins)
    */
    if ($algo == 'sha256')
        return hash('sha256', $texte);
    elseif ($algo == 'sha1')
        return sha1($texte);
    else
        return [sha1($texte), hash('sha256', $texte)];
    }

function doAscii($text) {
    /*
    fonction pour retourner une version ascii d'un texte (pour tri alphabétique correct)
    */
    $result = $text;
    $result = str_replace('Á', 'A', $result);
    $result = str_replace('À', 'A', $result);
    $result = str_replace('Â', 'A', $result);
    $result = str_replace('Ã', 'A', $result);
    $result = str_replace('Ä', 'A', $result);
    $result = str_replace('Å', 'A', $result);
    $result = str_replace('á', 'A', $result);
    $result = str_replace('à', 'A', $result);
    $result = str_replace('â', 'A', $result);
    $result = str_replace('ã', 'A', $result);
    $result = str_replace('ä', 'A', $result);
    $result = str_replace('å', 'A', $result);
    $result = str_replace('Æ', 'AE', $result);
    $result = str_replace('æ', 'AE', $result);
    $result = str_replace('Ç', 'C', $result);
    $result = str_replace('ç', 'C', $result);
    $result = str_replace('É', 'E', $result);
    $result = str_replace('È', 'E', $result);
    $result = str_replace('Ê', 'E', $result);
    $result = str_replace('Ë', 'E', $result);
    $result = str_replace('é', 'E', $result);
    $result = str_replace('è', 'E', $result);
    $result = str_replace('ê', 'E', $result);
    $result = str_replace('ë', 'E', $result);
    $result = str_replace('Í', 'I', $result);
    $result = str_replace('Ì', 'I', $result);
    $result = str_replace('Î', 'I', $result);
    $result = str_replace('Ï', 'I', $result);
    $result = str_replace('í', 'I', $result);
    $result = str_replace('ì', 'I', $result);
    $result = str_replace('î', 'I', $result);
    $result = str_replace('ï', 'I', $result);
    $result = str_replace('Ñ', 'N', $result);
    $result = str_replace('ñ', 'N', $result);
    $result = str_replace('Ó', 'O', $result);
    $result = str_replace('Ò', 'O', $result);
    $result = str_replace('Ô', 'O', $result);
    $result = str_replace('Õ', 'O', $result);
    $result = str_replace('Ö', 'O', $result);
    $result = str_replace('ó', 'O', $result);
    $result = str_replace('ò', 'O', $result);
    $result = str_replace('ô', 'O', $result);
    $result = str_replace('õ', 'O', $result);
    $result = str_replace('ö', 'O', $result);
    $result = str_replace('Œ', 'OE', $result);
    $result = str_replace('œ', 'OE', $result);
    $result = str_replace('Ú', 'U', $result);
    $result = str_replace('Ù', 'U', $result);
    $result = str_replace('Û', 'U', $result);
    $result = str_replace('Ü', 'U', $result);
    $result = str_replace('ú', 'U', $result);
    $result = str_replace('ù', 'U', $result);
    $result = str_replace('û', 'U', $result);
    $result = str_replace('ü', 'U', $result);
    $result = str_replace('Ý', 'Y', $result);
    $result = str_replace('Ÿ', 'Y', $result);
    $result = str_replace('ý', 'Y', $result);
    $result = str_replace('ÿ', 'Y', $result);
    return $result;
    }

function formatName($data, $mode='ALL') {
    /*
    Mise en forme d'un nom pour affichage (avec prénom complet ou pas)
    */
    if ($mode == 'ALL')
        return $data['NOM'].' '.$data['Prenom'];
    elseif ($mode == 'TROMBI') {
        $l = 11;
        $n = $data['NOM'];
        if (strlen($n) > $l)
            $n = mb_substr($data['NOM'], 0, $l, 'UTF-8').'.';
        $p = $data['Prenom'];
        if (strlen($p) > $l)
            $p = mb_substr($data['Prenom'], 0, $l, 'UTF-8').'.';
        return $n.'<br/>'.$p;
        }
    else
        return $data['NOM'].' '.mb_substr($data['Prenom'], 0, 1, 'UTF-8').'.';
    }

function array2string($array, $key=-1, $separator=', ') {
    /*
    crée une liste (texte) à partir d'un tableau.
    La liste est un texte contenant les éléments du tableau,
    séparés par une virgule (par défaut).
    Utilisé surtout pour les requêtes SQL avec des "IN (...)".
    */
    if ($key == -1) {
        $RESULT = implode($separator, $array);
        }
    else {
        $RESULT = array();
        foreach ($array as $row)
            $RESULT[]= $row[$key];
        $RESULT = implode($separator, $RESULT);
        }
    return $RESULT;
    }

function string2array($string, $separator=', ') {
    /*
    crée un tableau à partir d'une liste (texte).
    La liste est un texte contenant les éléments du tableau,
    séparés par une virgule (par défaut).
    */
    $RESULT = explode($separator, $string);
    return $RESULT;
    }

function getArrayLetters() {
    /*
    Pour récupérer un tableau des lettres utilisées dans les bases
    */
    return array(LETTRE_A, LETTRE_B, LETTRE_C, LETTRE_D, LETTRE_X);
    }

function getArrayEvals() {
    /*
    Pour récupérer un tableau des lettres utilisées dans l'établissement
    */
    return array(EVAL_A, EVAL_B, EVAL_C, EVAL_D, EVAL_X);
    }

function valeur2affichage($valeur) {
    /*
    traduit une chaine enregistrée pour l'afficher (A → V, ...)
    */
    $result = strtoupper($valeur);
    return str_replace(getArrayLetters(), getArrayEvals(), $result);
    }

function backupFile($filename, $tempDir, $destDir) {
    /*
    pour sauvegarder les anciennes versions des fichiers profs lors des uploads
        * @param string     $filename       le nom du fichier
        * @param string     $tempDir     le chemin du répertoire tmp
        * @param string     $destDir   le chemin du répertoire up/files
    */
    $backupDir2 = $destDir."../files_backup_2/";
    $backupDir1 = $destDir."../files_backup_1/";
    if (file_exists($backupDir1.$filename))
        rename($backupDir1.$filename, $backupDir2.$filename);
    if (file_exists($destDir.$filename))
        rename($destDir.$filename, $backupDir1.$filename);
    rename($tempDir.$filename, $destDir.$filename);
    }

function hex2Rgb($color) {
    /*
    
    */
    $red = hexdec(substr($color, 1, 2));
    $green = hexdec(substr($color, 3, 2));
    $blue = hexdec(substr($color, 5, 2));
    return array($red, $green, $blue);
    }

function rgb2Hex($color) {
    /*
    
    */
    $hex_RGB = '';
    foreach ($color as $value) {
        $hex_value = dechex($value);
        if (strlen($hex_value) < 2)
            $hex_value = "0".$hex_value;
        $hex_RGB .= $hex_value;
        }
    return "#".$hex_RGB;
    }

function spaces($n) {
    /*
    
    */
    $result = '';
    for ($i=0; $i<$n; $i++)
        $result = $result.'    ';
    return $result;
    }

function htmlSpaces($n) {
    /*
    
    */
    $result = '';
    for ($i=0; $i<$n; $i++)
        $result = $result.'&nbsp;';
    return $result;
    }

function message(
        $type, $strongMessage, $message0='', $message1='', $message2='') {
    /*
    affiche un message d'erreur.
    $type : 'success', 'info', 'warning', 'danger'
    $strongMessage ou $message0 est obligatoire.
    $message1, etc sont des lignes supplémentaires facultatives.
    */
    $result = '';
    $result .= spaces(2).'<div class="row">'."\n";
    $result .= spaces(3).'<div class="col-sm-2">'."\n";
    $result .= spaces(3).'</div>'."\n";
    $result .= spaces(3).'<div class="col-sm-8">'."\n";
    $result .= spaces(4).'<div class="alert alert-'.$type.' text-center">'."\n";
    if ($strongMessage != '') {
        $result .= spaces(5).'<strong>'.tr($strongMessage).'</strong>'."\n";
        if ($message0 != '')
            $result .= spaces(5).'<br/>'.tr($message0)."\n";
        }
    else
        $result .= spaces(5).tr($message0)."\n";
    if ($message1 != '')
        $result .= spaces(5).'<br/>'.tr($message1)."\n";
    if ($message2 != '')
        $result .= spaces(5).'<br/>'.tr($message2)."\n";
    $result .= spaces(4).'</div>'."\n";
    $result .= spaces(3).'</div>'."\n";
    $result .= spaces(2).'</div>'."\n";
    return $result;
    }

function resultatsUnavailableMessage() {
    return message(
        'danger', 
        'The results DB is currently unavailable.', 
        'Please wait a few minutes and try again.');
    }

function prohibitedMessage() {
    return message(
        'danger', 
        'Access to this page is prohibited.', '');
    }

function prohibitedPeriodMessage() {
    return message(
        'danger', 
        'The results are not yet available.', 
        'But you can see the detail of the assessments by subject.');
    }

function notInDemoMessage() {
    return message(
        'danger', 
        'DEMO VERSION:', 
        'changes will not be saved.');
    }

function appTitle() {
    $result = '<strong>V</strong>ers une ';
    $result .= '<strong>É</strong>valuation ';
    $result .= '<strong>R</strong>éussie ';
    $result .= '<strong>A</strong>vec les ';
    $result .= '<strong>C</strong>ompétences.';
    return $result;
    }

function debugToConsole($data) {
    error_log(print_r($data, True));
    }

function verticalText($text) {
    $result = '';
    for ($i=0, $n=strlen($text); $i<$n; $i++)
        $result .= $text[$i].'<br/>';
    return $result;
    }

function tr($texte) {
    $result = $texte;
    if (isset($_SESSION['TRANSLATIONS']))
        $result = $_SESSION['TRANSLATIONS'] -> tr($texte);
    return $result;
    }

function date2Timestamp($date) {
    /*
    fonction pour convertir une date en format YYYYMMDDHHMM en timestamp
    param string      $date
    return int
    */
    $len = strlen($date);
    $timestamp = 0;
    if (($len == 8) or ($len == 12)) {
        $second = 0;
        $year = intval(substr($date, 0, 4));
        $month = intval(substr($date, 4, 2));
        $day = intval(substr($date, 6, 2));
        if ($len > 8) {
            $hour = intval(substr($date, 8, 2));
            $minute = intval(substr($date, 10, 2));
            }
        else {
            $hour = 0;
            $minute = 0;
            }
        $timestamp = mktime($hour, $minute, $second, $month, $day, $year);
        }
    return $timestamp;
    }



/****************************************************
    FONCTIONS INTERROGEANT LES BASES DE DONNÉES
****************************************************/

function testResultatsDBState($CONNEXION) {
    /*
    
    */
    if (! isset($CONNEXION -> DB_RESULTATS))
        return False;
    try {
        $SQL = 'SELECT * FROM config';
        $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
        if (! $STMT)
            return False;
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        if (! $STMT)
            return False;
        return True;
        }
    catch(PDOException $e) {
        return False;
        }
    }

function readConfig($CONNEXION, $key, $add_commas=False) {
    /*
    
    */
    $SQL = 'SELECT value_int AS INT, value_text AS TXT FROM config ';
    if ($add_commas)
        $SQL .= 'WHERE key_name=":key"';
    else
        $SQL .= 'WHERE key_name=:key';
    $OPT = array(':key' => $key);
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    return ($STMT != '') ? $STMT -> fetch() : array('INT' => 0, 'TXT' => '');
    }

function getStudentName($CONNEXION, $id_eleve, $mode='ALL') {
    /*
    retourne le nom d'un élève
    @param int        $id_eleve
    @return string    le nom
    */
    $result = '';
    $SQL = 'SELECT * FROM eleves ';
    $SQL .= 'WHERE id=:id_eleve';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_USERS -> prepare($SQL);
    $STMT -> execute($OPT);
    if ($STMT!='') {
        $RESULT = $STMT -> fetch();
        $result = formatName($RESULT, $mode);
        }
    return $result;
    }

function listTeachersNames($CONNEXION) {
    /*
    retourne la liste des profs (id => [NOM COMPLET, NOM COURT])
    */
    $result = array();
    $SQL = 'SELECT id, NOM, Prenom FROM profs';
    $STMT = $CONNEXION -> DB_USERS -> prepare($SQL);
    $STMT -> execute();
    if ($STMT != '') {
        while ($ROW = $STMT -> fetch()) {
            $name = array('NOM' => $ROW['NOM'], 'Prenom' => $ROW['Prenom']);
            $result[$ROW['id']] = array(formatName($name), formatName($name, $mode=''));
            }
        }
    return $result;
    }

function listSubjectsLabels($CONNEXION) {
    /*
    retourne la liste des profs (id => [NOM COMPLET, NOM COURT])
    */
    $result = array();
    $SQL = 'SELECT * FROM matieres';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    if ($STMT != '') {
        while ($ROW = $STMT -> fetch())
            $result[$ROW['MatiereCode']] = array($ROW['Matiere'], $ROW['MatiereLabel']);
        }
    return $result;
    }

function getClassInfos($CONNEXION, $className='', $id_classe=-1) {
    /*
    retourne les infos de la classe d'après son nom ou son id
    */
    if ($id_classe > -1) {
        $SQL = 'SELECT * FROM classes ';
        $SQL .= 'WHERE id=:id_classe';
        $OPT = array(':id_classe' => $id_classe);
        }
    else {
        $SQL = 'SELECT * FROM classes ';
        $SQL .= 'WHERE Classe=:classe';
        $OPT = array(':classe' => $className);
        }
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    return ($STMT != '') ?  $STMT -> fetch() : array();
    }

function idClassInResults($CONNEXION, $className) {
    /*
    retourne l'id d'une classe dans la base résultats
    */
    $SQL = 'SELECT id_eleve FROM eleves ';
    $SQL .= 'WHERE id_eleve<0 AND Classe="'.$className.'"';
    $RESULT = $CONNEXION -> DB_RESULTATS -> query($SQL, PDO::FETCH_ASSOC);
    if ($RESULT != '') {
        $RESULT = $RESULT -> fetch();
        return intval($RESULT['id_eleve']);
        }
    else
        return 0;
    }

function sousRubriques($CONNEXION, $what='bulletin') {
    /*
    retourne la liste des sous_rubriques du bulletin, referentiel etc
    */
    $RESULT = array();
    $SQL = 'SELECT DISTINCT sous_rubriques.code, ';
    $SQL .= $what.'.Titre1, '.$what.'.Titre2, '.$what.'.Titre3, '.$what.'.Competence ';
    $SQL .= 'FROM sous_rubriques ';
    $SQL .= 'JOIN '.$what.' ON '.$what.'.code=sous_rubriques.code ';
    $SQL .= 'WHERE sous_rubriques.'.$what.'!="" ';
    $SQL .= 'ORDER BY '.$what.'.ordre';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    if ($STMT) {
        $STMT -> setFetchMode(PDO::FETCH_NUM);
        $STMT -> execute();
        if ($STMT != '') {
            while($tmp = $STMT -> fetch()) {
                if ($tmp[1] != '')
                    $label = $tmp[1];
                elseif ($tmp[2] != '')
                    $label = $tmp[2];
                elseif ($tmp[3] != '')
                    $label = $tmp[3];
                else
                    $label = $tmp[4];
                $RESULT[] = array($tmp[0], $label);
                }
            }
        }
    return $RESULT;
    }

function getPeriodName($CONNEXION, $id_period) {
    /*
    retourne le nom d'une période
    */
    $SQL = 'SELECT Periode FROM periodes ';
    $SQL .= 'WHERE id=:id';
    $OPT = array(':id' => $id_period);
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    if ($STMT != '') {
        $RESULT = $STMT -> fetch();
        $RESULT = $RESULT['Periode'];
        }
    else
        $RESULT = '';
    return $RESULT;
    }

function sharedCompetences($CONNEXION, $id_classe, $what='bulletin') {
    /*
    retourne la liste des compétences partagées d'une table
    */
    $infos = getClassInfos($CONNEXION, '', $id_classe);
    $SQL = 'SELECT code, Titre1, Titre2, Titre3, Competence FROM '.$what.' ';
    $SQL .= 'WHERE classeType=:classe_type OR classeType=-1 ';
    $SQL .= 'ORDER BY ordre';
    $OPT = array(':classe_type' => $infos['classeType']);
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    return ($STMT != '') ? $STMT -> fetchAll() : array();
    }

function studentResultsInSousRubriques(
        $CONNEXION, $id_eleve, $matieres, $sousRubriques) {
    /*
    retourne le nombre de resultats par niveau pour un élève donné
    */
    $RESULT = array();
    $OPT = array(':id_eleve' => $id_eleve);
    // on commence par les sous-rubriques :
    $SQL = 'SELECT name, value FROM syntheses ';
    $SQL .= 'WHERE id_eleve=:id_eleve ';
    $SQL .= 'AND id_prof<='.DECALAGE_TRANS.' ';
    $SQL .= 'AND name=:name';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    foreach ($sousRubriques as $sousRubrique) {
        $name = $sousRubrique[0];
        $OPT[':name'] = $name;
        $STMT -> execute($OPT);
        $VAL = $STMT -> fetch();
        $RESULT[] = array("name" => $name, "value" => (($VAL) ? $VAL['value']:''));
        }
    // puis les matières :
    $SQL = 'SELECT * FROM syntheses ';
    $SQL .= 'WHERE id_eleve=:id_eleve ';
    $SQL .= 'AND name=:name';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    foreach ($matieres as $matiere) {
        $OPT[':name'] = $matiere['MatiereCode'];
        $STMT -> execute($OPT);
        $VALUES = $STMT -> fetchAll();
        $VAL = '';
        foreach ($VALUES as $VALUE)
            $VAL .= $VALUE['value'];
        $VAL = moyenneBilan2($VAL);
        $RESULT[] = array('name' => $matiere['MatiereCode'], 'value' => (($VAL) ? $VAL:''));
        }
    return $RESULT;
    }

function moyenneBilan2($value) {
    /*
    au cas où il y ait 2 profs pour une matière, 
    on calcule la moyenne de leurs bilans.
    Utilisé pour le radar et la vue synthèse
    */
    if (strlen($value) < 2)
        return $value;

    $newValue = '';
    $array_lettres = getArrayLetters();
    $values = array();
    foreach ($array_lettres as $LETTRE)
        $values[] = substr_count($value, $LETTRE);

    $somme = 0;
    $total = 0;
    for ($i=0; $i < count($values) - 1; $i++)
        $total += $values[$i];
    if ($total > 0) {
        // on utilise les valeurs par défaut :
        $valeursEval = array(100, 67, 33, 0, 0);
        $levelItem = array(83, 50, 17, 0, 0);
        foreach ($values as $index => $value2)
            $somme += $value2 * $valeursEval[$index];
        $result = $somme / $total;
        for ($i=count($values) - 2; $i > - 1; $i--) {
            if ($result >= $levelItem[$i])
                $newValue = $array_lettres[$i];
            }
        }
    elseif ($values[count($values) - 1] > 0)
        $newValue = $array_lettres[count($array_lettres) - 1];

    //debugToConsole('value: '.$value.'| total: '.$total.'| newValue: '.$newValue);
    return $newValue;
    }

function idsProfsStudent($CONNEXION, $id_eleve) {
    /*
    retourne les ids des profs triés par matière d'après la base résultats
    */
    $RESULT = array();
    $SQL = 'SELECT * FROM eleve_prof ';
    $SQL .= 'WHERE id_eleve=:id_eleve ';
    $SQL .= 'ORDER BY Matiere, id_prof';
    $OPT = array(':id_eleve' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $VALUES = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($VALUES as $VALUE) {
        $matiere = $VALUE['Matiere'];
        if (array_key_exists($matiere, $RESULT))
            $RESULT[$matiere][] = $VALUE['id_prof'];
        else
            $RESULT[$matiere] = array($VALUE['id_prof']);
        }
    return $RESULT;
    }

function getDateResultsStudent($CONNEXION, $id_eleve) {
    /*
    retourne la date des résultats d'un élève (resultats.eleves.DateFichier)
    */
    $SQL = 'SELECT DateFichier FROM eleves ';
    $SQL .= 'WHERE id_eleve=:id';
    $OPT = array(':id' => $id_eleve);
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    if ($STMT != '') {
        $RESULT = $STMT -> fetch();
        $RESULT = $RESULT['DateFichier'];
        }
    else
        $RESULT = '';
    return $RESULT;
    }

function subjects($CONNEXION) {
    /*
    retourne la liste des matières.
    On la sépare en 3 parties (Bulletin, Speciales, Autre).
    */
    $SQL = 'SELECT Matiere, MatiereCode, MatiereLabel, OrdreBLT FROM matieres ';
    $SQL .= 'ORDER BY OrdreBLT';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
    $sortedSubjects = array('Bulletin' => array(), 'Speciales' => array(), 'Autre' => array());
    foreach ($RESULT as $subject) {
        if ($subject['OrdreBLT'] > 0)
            $sortedSubjects['Bulletin'][] = $subject;
        elseif ($subject['OrdreBLT'] < 0)
            $sortedSubjects['Speciales'][] = $subject;
        else
            $sortedSubjects['Autre'][] = $subject;
        }
    return $sortedSubjects;
    }

function studentResults(
        $CONNEXION, $id_eleve, $id_classe, $competences) {
    /*
    retourne les résultats d'un élève pour une liste de compétences partagées
    */
    // on crée le texte de la liste des compétences :
    $competencesText = array();
    foreach ($competences as $row) {
        if ($row['Competence'] != '')
            $competencesText[]= '"'.$row['code'].'"';
        }
    $competencesText = implode(',', $competencesText);
    // récupération des résultats :
    $SQL = 'SELECT * FROM bilans ';
    $SQL .= 'WHERE id_eleve IN ('.$id_eleve.','.$id_classe.') ';
    $SQL .= 'AND name IN ('.$competencesText.') ';
    $SQL .= 'ORDER BY id_eleve DESC';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    // on les remet en forme :
    $results = array();
    foreach ($tempResult as $row) {
        if ($row['id_eleve'] == $id_eleve)
            $results[$row['name']]['value'] = $row['value'];
        else
            $results[$row['name']]['groupeValue'] = $row['value'];
        }
    // et on les insère dans la liste des compétences :
    for($i=0; $i < count($competences); $i++) {
        if ($competences[$i]['Competence'] != '') {
            $code = $competences[$i]['code'];
            $competences[$i]['value'] = '';
            $competences[$i]['groupeValue'] = '';
            if (array_key_exists($code, $results)) {
                if (array_key_exists('value', $results[$code]))
                    $competences[$i]['value'] = $results[$code]['value'];
                if (array_key_exists('groupeValue', $results[$code]))
                    $competences[$i]['groupeValue'] = $results[$code]['groupeValue'];
                }
            }
        }
    return $competences;
    }

function studentDetails(
        $CONNEXION, $id_eleve, $id_classe, $competences) {
    /*
    retourne les détails des résultats d'un élève
    pour une liste de compétences partagées
    */
    // on crée le texte de la liste des compétences :
    $competencesText = array();
    foreach ($competences as $row) {
        if ($row['Competence'] != '')
            $competencesText[]= '"'.$row['code'].'"';
        }
    $competencesText = implode(',', $competencesText);
    // récupération des résultats :
    $SQL = 'SELECT * FROM bilans_details ';
    $SQL .= 'WHERE id_eleve='.$id_eleve.' ';
    $SQL .= 'AND name IN ('.$competencesText.') ';
    $SQL .= 'ORDER BY matiere';
    $STMT = $CONNEXION -> DB_RESULTATS -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    $results = array();
    // on les remet en forme :
    foreach ($tempResult as $row) {
        $results[$row['name']][] = array(
            'matiere' => $row['matiere'],
            'id_prof' => $row['id_prof'],
            'value' => $row['value']);
        }
    return $results;
    }

function getSocleComponents($CONNEXION, $id_classe) {
    /*
    retourne la liste des 8 composantes du socle 
    avec les bilans du référentiel correspondants
    */
    $SOCLE_COMPONENTS = array(
        'CPD_FRA' => 'S2D1O1', 
        'CPD_SCI' => 'S2D1O3', 
        'REP_MND' => 'S2D5', 
        'CPD_ETR' => 'S2D1O2', 
        'SYS_NAT' => 'S2D4', 
        'CPD_ART' => 'S2D1O4', 
        'FRM_CIT' => 'S2D3', 
        'MET_APP' => 'S2D2');
    $infos = getClassInfos($CONNEXION, '', $id_classe);
    $SQL = 'SELECT code, Titre1, Titre2, Titre3, Competence FROM referentiel ';
    $SQL .= 'WHERE classeType=:classe_type OR classeType=-1 ';
    $SQL .= 'ORDER BY ordre';
    $OPT = array(':classe_type' => $infos['classeType']);
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute($OPT);
    $temp = ($STMT != '') ? $STMT -> fetchAll() : array();
    $result = array();
    foreach ($SOCLE_COMPONENTS as $code => $title)
        $result[$code] = array($title, tr('LSU_'.$code));
    return $result;
    }

function getVersionDB($DB) {
    /*
    retourne la version d'une base
    */
    $SQL = 'SELECT value_int AS VERSION FROM config WHERE key_name="versionDB"';
    $STMT = $DB -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $RESULT = $STMT -> fetch();
    $result = $RESULT['VERSION'];
    return $result;
    }

function tableInDB($table, $DB) {
    /*
    indique si une table existe dans une base
    */
    $result = False;
    $SQL = 'SELECT name FROM sqlite_master WHERE type="table"';
    $STMT = $DB -> prepare($SQL);
    $STMT -> setFetchMode(PDO::FETCH_ASSOC);
    $STMT -> execute();
    $RESULT = $STMT -> fetch();
    if (in_array($table, $RESULT))
        $result = True;
    else
        $result = False;
    return $result;
    }



?>
