<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/***********************************************
    UTILITAIRES DE CALCULS GÉNÉRAUX
***********************************************/

function defineCalculsParameters($CONNEXION) {
    /*
    lancée une seule fois (au premier affichage de la page assess), 
    cette fonction récupère les paramètres du prof pour les calculs des bilans.
    (d'après utils.py)
    */
    if (isset($_SESSION['MODECALCUL']))
        return;
    // valeurs initiales :
    $_SESSION['MODECALCUL'] = 0;
    $_SESSION['MODECALCULNOTES'] = 0;
    $_SESSION['NBMAXEVAL'] = 3;
    $_SESSION['COEFLAST'] = 0;
    $_SESSION['levelValidItemPC'] = 50;
    $_SESSION['levelValidItemNB'] = 3;
    $_SESSION['levelValidItemOP'] = 0;
    $_SESSION['valeursEval'] = array(100, 67, 33, 0, 0);
    $_SESSION['levelItem'] = array(83, 50, 17, 0, 0);
    $_SESSION['levelA'] = array(50, 90, 10, 0);
    $_SESSION['levelB'] = array(50, 50, 30);
    $_SESSION['levelX'] = 50;
    $_SESSION['levelX_NB'] = 1;
    $_SESSION['precisionNotes'] = 1;
    // 2 dictionnaires pour accélérer les calculs :
    $_SESSION['calculBilanDic'] = array();
    $_SESSION['moyenneBilanDic'] = array();
    // mise à jour des valeurs si définies dans la base du prof :
    $keyNames = array(
        'MODECALCUL', 'MODECALCULNOTES', 'NBMAXEVAL', 'COEFLAST', 
        'levelValidItemPC', 'levelValidItemNB', 'levelValidItemOP', 
        'levelX', 'levelX_NB');
    foreach ($keyNames as $keyName) {
        // on récupère dans la base prof
        $value = $CONNEXION -> readConfigProf($keyName, $default_int=$_SESSION[$keyName]);
        $_SESSION[$keyName] = intval($value['INT']);
        }
    // idem avec les listes de constantes :
    $keyNames = array('levelA', 'levelB');
    foreach ($keyNames as $keyName) {
        $new_array = array();
        for ($i=0; $i<4; $i++) {
            if (($i != 3) or ($keyName == 'levelA')) {
                $key = $keyName.'_'.$i;
                $value = $CONNEXION -> readConfigProf($key, $default_int=$_SESSION[$keyName][$i]);
                $new_array[] = intval($value['INT']);
                }
            }
        $_SESSION[$keyName] = $new_array;
        }
    // les coefficients pour calculer les notes :
    if ($_SESSION['MODECALCULNOTES'] == 1)
        $_SESSION['valeursEval'] = array(100, 80, 50, 20, 0);
    elseif ($_SESSION['MODECALCULNOTES'] == 2) {
        $new_array = array();
        for ($i=0; $i<4; $i++) {
            $key = 'valeursEval_'.$i;
            $value = $CONNEXION -> readConfigProf($key, $default_int=$_SESSION['valeursEval'][$i]);
            $new_array[] = intval($value['INT']);
            }
        $new_array[] = 0;
        $_SESSION['valeursEval'] = $new_array;
        }
    // on récupère aussi la liste des périodes protégées :
    $protectedPeriods = $CONNEXION -> readConfigProf('ProtectedPeriods');
    $protectedPeriods = $protectedPeriods['TXT'];
    $protectedPeriods = string2array($protectedPeriods, $separator='|');
    $_SESSION['PROTECTED_PERIODS'] = array();
    $SQL = 'SELECT id FROM periodes ';
    $SQL .= 'WHERE id>0 ';
    $SQL .= 'ORDER BY id';
    $STMT = $CONNEXION -> DB_COMMUN -> prepare($SQL);
    $STMT -> execute();
    $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
    foreach ($tempResult as $period) {
        if (in_array($period[0], $protectedPeriods))
            $_SESSION['PROTECTED_PERIODS'][$period[0]] = 1;
        else
            $_SESSION['PROTECTED_PERIODS'][$period[0]] = 0;
        }
    $_SESSION['PROTECTED_PERIODS'][0] = 0;
    //debugToConsole($_SESSION['PROTECTED_PERIODS']);
    }

function affichage2valeur($affichage) {
    /*
    traduit une chaine affichée pour l'enregistrer dans la base
        * @param string $affichage        la valeur affichée
        * @return string                la valeur interne (A, B, C,...)
    */
    // on enlève les blancs :
    $valeur = trim($affichage);
    // on passe en majuscule :
    $valeur = strtoupper($valeur);
    // on "convertit" les lettres connues :
    $valeur = str_replace(getArrayEvals(), getArrayLetters(), $valeur);
    // on supprime les lettres indésirables :
    $pattern = '#[';
    foreach (getArrayLetters() as $lettre)
        $pattern .= $lettre;
    $pattern .= ']#i';
    preg_match_all($pattern, $valeur, $array, PREG_PATTERN_ORDER);
    $result = implode('', $array[0]);
    return $result;
    }

function value2List($text) {
    /*
    renvoie le nombre de chaque charactères pour une chaîne donnée
    */
    $listValues = array();
    foreach (getArrayLetters() as $LETTRE)
        $listValues[] = substr_count($text, $LETTRE);
    return $listValues;
    }

function valuesInPercentages($values) {
    /*
    renvoie le pourcentage de chaque valeur par rapport au total.
    */
    $last = count($values) - 1;
    $result = array();
    foreach ($values as $index => $elem)
        $result[$index] = 0;
    $total = array_sum($values);
    if ($total < 1)
        return $result;
    // calcul du % de X :
    $result[$last] = $values[$last] * 100 / $total;
    // total des autres :
    $total = ($values[0] + $values[1] + $values[2] + $values[3]);
    if ($total < 1)
        return $result;
    // calcul des % sans tenir compte des X :
    for ($i=0; $i<$last; $i++)
        $result[$i] = $values[$i] * 100 / $total;
    // on recentre les % en fonction des X :
    if ($result[$last] > 0) {
        $V2J = $result[0] * $result[$last] / 100;
        $result[0] -= $V2J;
        $result[1] += $V2J;
        $R2O = $result[3] * $result[$last] / 100;
        $result[3] -= $R2O;
        }
    // pour retomber sur 100 % :
    $result[2] = 100 - ($result[0] + $result[1] + $result[3]);
    return $result;
    }

function testA($valuesPC) {
    /*
    test des conditions pour attribuer "A".
    Les valeurs sont données en % (valuesPC)
    */
    $result = 0;
    if (($valuesPC[0] >= $_SESSION['levelA'][0])
        and (($valuesPC[0] + $valuesPC[1]) >= $_SESSION['levelA'][1])
        and ($valuesPC[2] <= $_SESSION['levelA'][2])
        and ($valuesPC[3] <= $_SESSION['levelA'][3]))
            $result = 1;
    return $result;
    }

function testD($valuesPC) {
    $result = 0;
    if (($valuesPC[3] >= $_SESSION['levelA'][0])
        and (($valuesPC[2] + $valuesPC[3]) >= $_SESSION['levelA'][1])
        and ($valuesPC[1] <= $_SESSION['levelA'][2])
        and ($valuesPC[0] <= $_SESSION['levelA'][3]))
            $result = 1;
    return $result;
    }

function testB($valuesPC) {
    $result = 0;
    if ((($valuesPC[0] + $valuesPC[1]) >= $_SESSION['levelB'][0])
        and ($valuesPC[2] <= $_SESSION['levelB'][1])
        and ($valuesPC[3] <= $_SESSION['levelB'][2]))
            $result = 1;
    return $result;
    }



/***********************************************
   CALCULS DES ITEMS
***********************************************/

function getItemClass($CONNEXION, $value) {
    /*
    retourne le style (class ; défini dans le fichier css)
    à utiliser pour l'affichage d'un item (interroge la table moyennesItems).
    */
    // s'il n'y a qu'une lettre, pas besoin d'aller plus loin :
    if (strlen($value) < 2)
        return $value;
    $moyenne = $value;
    // on interroge la table moyennes :
    $SQL = 'SELECT moyenne FROM moyennesItems ';
    $SQL .= 'WHERE value=:value';
    $OPT = array(':value' => $value);
    $STMT = $CONNEXION -> DB_PROF -> prepare($SQL);
    $STMT -> execute($OPT);
    if ($STMT != '') {
        $RESULT = $STMT -> fetch();
        $moyenne = $RESULT['moyenne'];
        }
    // si la moyenne n'est pas trouvée dans la table, on lance le calcul :
    if ($moyenne == '') {
        $moyenne = moyenneItem($value);
        $CONNEXION -> setMoyenne($value, $moyenne);
        }
    return $moyenne;
    }

function moyenneItem($text) {
    /*
    renvoie la "moyenne" de la chaîne de caractères 
    en fonctions des valeurs définies pour chaque lettre
    et des seuils d'acquisition définis (utile pour choisir la couleur de la cellule).
    S'il n'y a rien, on renvoie une chaîne vide.
    */
    $result = '';
    $values = calculItemValue($text, -1);
    $result = calculBilan($values);
    return $result;
    }

function calculItemValue($valueItem, $coeff=1) {
    /*
    calcule la valeur retenue pour l'item en fonction des paramètres
    (nombre d'absence autorisées, nombre d'évaluations retenues, ...).
    */
    $value = '';
    $values = value2List($valueItem);
    $array_lettres = getArrayLetters();

    // itemMayBeStored indique si l'item peut être pris en compte
    // en fonction des seuils d'évaluations minimaux :
    $itemMayBeStored = False;
    $lastValue = $values[count($values) - 1];
    if ($lastValue == 0)
        $itemMayBeStored = True;
    else {
        // on vérifie les pourcentages d'évaluations par rapport aux non évalués
        $pc = False;
        if ((100 * $lastValue / array_sum($values)) < $_SESSION['levelValidItemPC'])
            $pc = True;
        // on vérifie si le nbre mini d'évaluation est respecté
        $nb = False;
        $values2 = $values;
        $values2[count($values2)-1] = 0;
        if (array_sum($values2) >= $_SESSION['levelValidItemNB'])
            $nb = True;
        if ($_SESSION['levelValidItemOP'] == 0) {
            if ($pc or $nb)
                $itemMayBeStored = True;
            }
        else {
            if ($pc and $nb)
                $itemMayBeStored = True;
            }
        }

    // s'il y a trop d'absences par rapport aux évaluations, on renvoie "X" :
    if ($itemMayBeStored == False)
        $value = 'X';
    elseif (($_SESSION['MODECALCUL'] == 0) or ($_SESSION['NBMAXEVAL'] == 0))
        $value = $valueItem;
    elseif (array_sum($values) <= $_SESSION['NBMAXEVAL'])
        $value = $valueItem;
    elseif ($_SESSION['MODECALCUL'] == 1) {
        // si on choisit l'option BESTVALUES
        $nbchar = 0;
        foreach ($values as $index => $value2) {
            if ($nbchar >= $_SESSION['NBMAXEVAL'])
                break;
            for ($i=0; $i<$value2; $i++) {
                if ($nbchar >= $_SESSION['NBMAXEVAL'])
                    break;
                $value .= $array_lettres[$index];
                $nbchar ++;
                }
            }
        }
    elseif ($_SESSION['MODECALCUL'] == 2) {
        // si on choisit l'option LASTVALUES
        $nbchar = 0;
        $isXInItem = False;
        for($i=strlen($valueItem)-1; $i>=0; $i--) {
            if($nbchar>=$_SESSION['NBMAXEVAL'])
                break;
            $char=$valueItem[$i];
            if ($char!=$array_lettres[count($array_lettres)-1]) {
                $value = $char.$value;
                $nbchar++;
                }
            else
                $isXInItem = True;
            }
        if (($isXInItem) and ($value == ''))
            $value = $array_lettres[count($array_lettres)-1];
        }
    else
        $value = $valueItem;
    // coefficiente plus fortement les 4 dernières évaluations (option COEFLAST):
    if ($_SESSION['COEFLAST'] == 1) {
        $debut = 0;
        $newvaleur = '';
        $coef = 1;
        if (strlen($value) > 5) {
            $debut = strlen($value) - 5;
            $newvaleur = substr($value, 0, $debut);
            }
        else
            $coef = 6 - strlen($value);
        for($i=$debut; $i<strlen($value); $i++) {
            $char = $value[$i];
            for($j=0; $j<$coef; $j++)
                $newvaleur .= $char;
            $coef++;
            }
        $value = $newvaleur;
        }
    $values = value2List($value);
    // pondère le poids de chaque item pour le calcul du bilan
    if (($coeff > 0) and (strlen($value) > 0)) {
        $l = strlen($value);
        $rapport = round(100 / $l);
        foreach ($values as $index => $val)
            $values[$index] = $val * $rapport * $coeff;
        }
    return $values;
    }

function calculBilan($values) {
    /*
    on utilise un dictionnaire $_SESSION['calculBilanDic']
    pour ne pas recalculer systématiquement
    (permet d'accélérer un peu l'affichage)
    REMARQUE : par rapport à la version Python, le dictionnaire utilise $values
    transformé en chaîne de caractères ($values2)
    car php ne veut pas d'array comme clé.

    refaire :
    * calculer d'abord sans tenir compte des X
    * puis relativiser en fonction de la proportion de X
    */
    $result = '';
    if (array_sum($values) < 1)
        return $result;
    $values2 = '';
    foreach ($values as $n)
        $values2 .= '|'.$n;
    if (array_key_exists($values2, $_SESSION['calculBilanDic']))
        return $_SESSION['calculBilanDic'][$values2];
    $array_lettres = getArrayLetters();
    $last = count($values) - 1;
    $valuesPC = valuesInPercentages($values);
    if ($valuesPC[$last] >= $_SESSION['levelX'])
        $result = $array_lettres[$last];
    elseif (testA($valuesPC) == 1)
        $result = $array_lettres[0];
    elseif (testD($valuesPC) == 1)
        $result = $array_lettres[3];
    elseif (testB($valuesPC) == 1)
        $result = $array_lettres[1];
    else
        $result = $array_lettres[2];
    $_SESSION['calculBilanDic'][$values2] = $result;
    return $result;
    }

function moyenneBilan($value) {
    /*
    on utilise un dictionnaire pour ne pas recalculer systématiquement
    (permet d'accélérer un peu l'affichage)
        $_SESSION['moyenneBilanDic']
    */
    $newValue = '';
    // on teste si la valeur est déjà dans le dictionnaire :
    $values = value2List($value);

    $valuesText = '|';
    foreach ($values as $index => $value2)
        $valuesText .= $value2.'|';
    if (array_key_exists($valuesText, $_SESSION['moyenneBilanDic']))
        return $_SESSION['moyenneBilanDic'][$valuesText];

    $array_lettres = getArrayLetters();
    $somme = 0;
    $listeValeurBegin = $values;
    $listeValeurBegin[count($listeValeurBegin) - 1] = 0;
    $total = array_sum($listeValeurBegin);
    if ($total > 0) {
        // on utilise les valeurs par défaut :
        $valeursEval = array(100, 67, 33, 0, 0);
        foreach ($values as $index => $value2)
            $somme += $value2 * $valeursEval[$index];
        $result = $somme / $total;
        $test = False;
        foreach ($array_lettres as $index => $lettre) {
            if ($result >= $_SESSION['levelItem'][$index]) {
                $newValue = $array_lettres[$index];
                $test = True;
                }
            if ($test)
                break;
            }
        }
    elseif ($values[count($values) - 1] > 0)
        $newValue = $array_lettres[count($array_lettres) - 1];
    else
        $newValue = '';
    $_SESSION['moyenneBilanDic'][$valuesText] = $newValue;
    return $newValue;
    }


?>
