<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



// des variables indiquant les statuts
define('DECALAGE_PP', 10000);
define('DECALAGE_DIRECTEUR', 20000);
// variable indiquant les parties partagées dans la synthèse
define('DECALAGE_TRANS', -10);
// variable indiquant les compétences du bulletin ou confidentielles
define('DECALAGE_BLT', 10000);
define('DECALAGE_CFD', 20000);

// des variables indiquant les lettres utilisées dans la bdd
define('LETTRE_A', 'A');
define('LETTRE_B', 'B');
define('LETTRE_C', 'C');
define('LETTRE_D', 'D');
define('LETTRE_X', 'X');

// des variables indiquant les vues utilisées lors des évaluation par le prof
define('VUE_ITEM', 0);
define('VUE_STATISTIQUES', 1);
define('VUE_BILAN', 2);
define('VUE_BULLETIN', 3);
define('VUE_APPRECIATIONS', 4);
define('VUE_SUIVIS', 5);

// Quelques chemins, avec le séparateur final
define('CHEMIN_VERAC', realpath(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR);
define('DOSSIER_CONNEXIONS', 'libs'.DIRECTORY_SEPARATOR);
define('DOSSIER_UP', 'verac'.DIRECTORY_SEPARATOR.'up'.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR);
define('DOSSIER_PUBLIC', 'verac'.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR);
define('DOSSIER_PROTECTED', 'verac'.DIRECTORY_SEPARATOR.'protected'.DIRECTORY_SEPARATOR);
define('DOSSIER_SUIVIS', DOSSIER_PROTECTED.'suivis'.DIRECTORY_SEPARATOR);
define('CHEMIN_CONFIG', CHEMIN_VERAC.'_private'.DIRECTORY_SEPARATOR);


// Paramètres des bdd sqlite pour Verac.
define('VERAC_DB_USERS', DOSSIER_PUBLIC.'users.sqlite');
define('VERAC_DB_COMMUN', DOSSIER_PUBLIC.'commun');
define('VERAC_DB_COMPTEUR' , DOSSIER_PUBLIC.'compteur.sqlite');
define('VERAC_DB_RESULTATS', DOSSIER_PROTECTED.'resultats');
define('VERAC_DB_REFERENTIAL_PROPOSITIONS', DOSSIER_PROTECTED.'referential_propositions.sqlite');
define('VERAC_DB_REFERENTIAL_VALIDATIONS', DOSSIER_PROTECTED.'referential_validations.sqlite');
define('VERAC_DB_DOCUMENT', DOSSIER_PROTECTED.'documents.sqlite');
define('VERAC_DB_SUIVIS', DOSSIER_SUIVIS.'suivis.sqlite');
define('VERAC_CONNEXION_OTHER', DOSSIER_CONNEXIONS.'CONNEXION_OTHER.php');
define('VERAC_CONNEXION_ELEVE', DOSSIER_CONNEXIONS.'CONNEXION_ELEVE.php');
define('VERAC_CONNEXION_PROF', DOSSIER_CONNEXIONS.'CONNEXION_PROF.php');




// URL du projet Verac
define('DATE_VERSION_SERVEUR', 'http://download.tuxfamily.org/verac/downloads/dates_versions.html');

$dateFile = CHEMIN_CONFIG.'dates_versions.html';
if (is_file($dateFile)) {
    $contenu_date = file_get_contents($dateFile);
    $dates = explode('|', $contenu_date);
    $date = str_replace(chr(0), '', $dates[2]);
    define('DATE_VERSION_INTERFACE', $date);
    }
else
    define('DATE_VERSION_INTERFACE', '201308182359');



?>
