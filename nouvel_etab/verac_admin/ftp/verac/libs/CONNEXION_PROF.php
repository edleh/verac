<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



/****************************************************
    OBJET ET MÉTHODES POUR LE PROF
****************************************************/

class CONNEXION_PROF {

    public $DB_USERS;
    public $DB_COMMUN;
    public $DB_RESULTATS;
    public $DB_PROF;
    public $DB_DOCUMENT;
    public $DB_SUIVIS;
    public $DB_COMPTEUR;

    public $CHEMIN_PROF;
    public $PERIOD;
    public $DB_PROF_EXISTS = False;
    public $DB_PROF_OK = False;
    public $no_db_suivis;


    public function __construct($id_prof=-1, $period='') {
        /*
        initialisation.
        On se connecte aux bases de données.
        */
        $this -> CHEMIN_PROF = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .DOSSIER_UP.PREFIXE_DB_PROF;
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_USERS;
        if (is_readable($filename)) {
            $this -> DB_USERS = new PDO('sqlite:'.$filename);
            $this -> DB_USERS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        if (($period == '') or (intval($period) == intval($_SESSION['ACTUAL_PERIOD']))) {
            $suffixe_base_commun = '.sqlite';
            $suffixe_base_resultat = '.sqlite';
            $this -> PERIOD = $_SESSION['ACTUAL_PERIOD'];
            }
        else {
            // si le prof évalue à une période non calculée :
            if ((intval($period) > intval($_SESSION['ACTUAL_PERIOD']))) {
                $suffixe_base_commun = '.sqlite';
                $suffixe_base_resultat = '.sqlite';
                }
            else {
                $suffixe_base_commun = '-'.$period.'.sqlite';
                $suffixe_base_resultat = '-'.$period.'.sqlite';
                }
            $this -> PERIOD = $period;
            }
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .VERAC_DB_COMMUN.$suffixe_base_commun;
        if (is_readable($filename)) {
            $this -> DB_COMMUN = new PDO('sqlite:'.$filename);
            $this -> DB_COMMUN -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR
            .VERAC_DB_RESULTATS.$suffixe_base_resultat;
        if (is_readable($filename)) {
            $this -> DB_RESULTATS = new PDO('sqlite:'.$filename);
            $this -> DB_RESULTATS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            //$this -> DB_RESULTATS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

        $filename = $this -> CHEMIN_PROF.$id_prof.'.sqlite';
        if (file_exists($filename)) {
            $this -> DB_PROF_EXISTS = True;
            if (is_readable($filename)) {
                $this -> DB_PROF = new PDO('sqlite:'.$filename);
                $this -> DB_PROF -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                if (is_writable($filename))
                    $this -> DB_PROF_OK = True;
                }
            }

        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_DOCUMENT;
        if (is_readable($filename)) {
            $this -> DB_DOCUMENT = new PDO('sqlite:'.$filename);
            $this -> DB_DOCUMENT -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }

        $this -> CHEMIN_SUIVIS = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.DOSSIER_SUIVIS;
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_SUIVIS;
        if (is_readable($filename)) {
            $this -> DB_SUIVIS = new PDO('sqlite:'.$filename);
            $this -> DB_SUIVIS -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            $this -> no_db_suivis = False;
            }
        else
            $this -> no_db_suivis = True;

        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF.DIRECTORY_SEPARATOR.VERAC_DB_COMPTEUR;
        if (is_readable($filename)) {
            $this -> DB_COMPTEUR = new PDO('sqlite:'.$filename);
            $this -> DB_COMPTEUR -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            }
    }



    /****************************************************
        FONCTIONS GÉNÉRALES
    ****************************************************/

    public function readConfigProf(
            $key, $default_int=0, $default_text='') {
        /*
        lit et retourner une clé de la table prof.config
        */
        if (! $this -> DB_PROF_EXISTS)
            return False;
        $SQL = 'SELECT value_int AS INT, value_text AS TXT FROM config ';
        $SQL .= 'WHERE key_name=:key';
        $OPT = array(':key' => $key);
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        if ($STMT != '')
            $result = $STMT -> fetch();
        if (! isset($result['INT']))
            $result = array('INT' => $default_int, 'TXT' => $default_text);
        return $result;
        }

    public function connectTeacherDB($id_prof) {
        /*
        connexion à une base prof.
        Utilisé dans la page student_details
        */
        if ($id_prof < 0)
            return False;
        $filename = $this -> CHEMIN_PROF.$id_prof.'.sqlite';
        if (! is_readable($filename))
            return False;
        if (filesize($filename) < 10)
            return False;
        $db = new PDO('sqlite:'.$filename);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        return $db;
        }

    public function connectReferentialPropositionsDB() {
        /*
        connexion à la base referential_propositions.sqlite.
        Utilisé dans les pages student_validations et class_validations
        */
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF
            .DIRECTORY_SEPARATOR.VERAC_DB_REFERENTIAL_PROPOSITIONS;
        if (! file_exists($filename)) {
            // si pas encore mise à jour :
            $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF
                .DIRECTORY_SEPARATOR.DOSSIER_PROTECTED.'validations.sqlite';
            }
        if (! file_exists($filename))
            return False;
        if (! is_readable($filename))
            return False;
        if (filesize($filename) < 10)
            return False;
        $db = new PDO('sqlite:'.$filename);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        return $db;
        }

    public function connectReferentialValidationsDB() {
        /*
        connexion à la base referential_validations.sqlite.
        Utilisé dans les pages student_validations et class_validations
        */
        $filename = CHEMIN_VERAC.CHEMIN_SECRET_RELATIF
            .DIRECTORY_SEPARATOR.VERAC_DB_REFERENTIAL_VALIDATIONS;
        if (! file_exists($filename))
            return False;
        if (! is_readable($filename))
            return False;
        if (filesize($filename) < 10)
            return False;
        $db = new PDO('sqlite:'.$filename);
        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
        return $db;
        }

    public function classNamePP($matiere) {
        /*
        retourne le nom de la classe dans laquelle officie un PP
        */
        if (! $this -> DB_PROF_OK)
            return '';
        $SQL = 'SELECT groupes.*, groupe_eleve.id_eleve FROM groupes ';
        $SQL .= 'JOIN groupe_eleve USING (id_groupe) ';
        $SQL .= 'WHERE groupes.Matiere=:matiere ';
        $SQL .= 'ORDER BY isClasse DESC, id_groupe';
        $OPT = array(':matiere' => $matiere);
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute($OPT);
        $className = '';
        if ($STMT != '') {
            $RESULT = $STMT -> fetch();
            if ($RESULT['isClasse'] > 0)
                $className = $RESULT['nom_classe'];
            else {
                //on va chercher dans users
                $id_eleve = $RESULT['id_eleve'];
                $className = $this -> studentClassName($id_eleve);
                }
            }
        return $className;
        }



    /****************************************************
        RENSEIGNEMENTS CONCERNANT LES ÉLÈVES
    ****************************************************/

    public function find_PP($id_eleve, $id_prof, $matiere='PP') {
        /*
        retourne l'id du prof principal d'un élève.
        */
        $id_pp = -1;
        // on récupère la liste des PP ayant cet élève :
        $liste_PP = array();
        $SQL = 'SELECT id FROM profs ';
        $SQL .= 'WHERE id>='.DECALAGE_PP;
        $RESULT = $this -> DB_USERS -> query($SQL);
        if ($RESULT != '') {
            while ($ROW = $RESULT -> fetch()) {
                $id_test = $ROW['id'];
                // on vérifie si c'est le PP en cherchant dans sa base s'il y a
                // un groupe de la matière PP avec cet élève :
                $filename = $this -> CHEMIN_PROF.$id_test.'.sqlite';
                $isPP = False;
                if (file_exists($filename))
                    if (is_readable($filename)) {
                        $db = new PDO('sqlite:'.$filename);
                        $db -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
                        $count = 0;
                        $SQL = 'SELECT COUNT(*) FROM groupes ';
                        $SQL .= 'JOIN groupe_eleve USING (id_groupe) ';
                        $SQL .= 'WHERE groupes.Matiere=:nom_matiere ';
                        $SQL .= 'AND groupe_eleve.id_eleve=:id_eleve ';
                        $SQL .= 'AND groupe_eleve.ordre>0';
                        $OPT = array(':nom_matiere' => $matiere, ':id_eleve' => $id_eleve);
                        $STMT = $db -> prepare($SQL);
                        $STMT -> execute($OPT);
                        $count = $STMT -> fetch();
                        if ($count['COUNT(*)'] > 0)
                            $isPP = True;
                        }
                if ($isPP)
                    $liste_PP[] = $id_test;
                }
            }
        // $liste_PP est maintenant remplie.
        if (count($liste_PP) > 0) {
            // il y en a plus d'un, on teste l'user_id pour voir s'il est dans le groupe
            if (in_array($id_prof, $liste_PP))
                $id_pp = $id_prof;
            else
                $id_pp = $liste_PP[0];
            }
        return $id_pp;
        }

    public function getStudentLogin($id_eleve) {
        /*
        retourne un tableau contenant le login et le mot de passe initial 
        d'un ou plusieurs élèves.
        (on vérifie d'abord la version de la base).
        */
        $logins = array();
        $SQL = 'SELECT id_eleve, Login, initialMdp FROM eleves ';
        $SQL .= 'WHERE id_eleve IN('.$id_eleve.')';
        $STMT = $this -> DB_RESULTATS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $RESULT = ($STMT != '') ? $STMT -> fetchAll(): array();
        foreach ($RESULT as $ROW)
            $logins[$ROW['id_eleve']] = array($ROW['Login'], $ROW['initialMdp']);
        return $logins;
        }

    public function getStudentBirthday($id_eleve) {
        /*
        retourne la date de naissance d'un ou plusieurs élèves
        */
        $birthdays = array();
        $SQL = 'SELECT * FROM eleves ';
        $SQL .= 'WHERE id_eleve IN('.$id_eleve.')';
        $STMT = $this -> DB_RESULTATS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $RESULT = ($STMT != '') ? $STMT -> fetchAll(): array();
        foreach ($RESULT as $ROW)
            $birthdays[$ROW['id_eleve']] = $ROW['Date_Naissance'];
        return $birthdays;
        }

    public function studentClassName($id_eleve) {
        /*
        retourne le nom de la classe d'un élève
        */
        $className = '';
        $SQL = 'SELECT * FROM eleves ';
        $SQL .= 'WHERE id=:id_eleve';
        $OPT = array(':id_eleve' => $id_eleve);
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> execute($OPT);
        if ($STMT!='') {
            $RESULT = $STMT -> fetch();
            $className = $RESULT['Classe'];
            }
        return $className;
        }

    public function listStudents($id_classe, $mode='ALL') {
        /*
        retourne la liste des élèves d'une classe
        */
        $SQL = 'SELECT Classe FROM classes ';
        $SQL .= 'WHERE id=:classe_id';
        $OPT = array(':classe_id' => $id_classe);
        $STMT = $this -> DB_COMMUN -> prepare($SQL);
        $STMT -> execute($OPT);
        if ($STMT != '') {
            $classe_nom = $STMT -> fetch();
            $classe_nom = $classe_nom['Classe'];
            }
        else
            return array();
        $SQL = 'SELECT id, NOM, Prenom FROM eleves ';
        $SQL .= 'WHERE Classe=:classe ';
        $SQL .= 'ORDER BY NOM, Prenom';
        $OPT = array(':classe' => $classe_nom);
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> execute($OPT);
        if ($STMT != '') {
            $result = array();
            $tempResult = array();
            while ($ROW = $STMT -> fetch()) {
                $tmp = array();
                $tmp[0] = $ROW[0];
                $nom = formatName($ROW, $mode=$mode);
                $tmp[1] = $nom;
                if ($mode == 'TROMBI')
                    $tmp[2] = formatName($ROW);
                $tempResult[doAscii($nom)] = $tmp;
                }
            uksort($tempResult, 'strcasecmp');
            foreach ($tempResult as $key => $val)
                $result[] = $val;
            return $result;
            }
        return array();
        }

    public function listAllStudents($mode='ALL') {
        /*
        retourne la liste de tous les élèves
        */
        $SQL = 'SELECT id, NOM, Prenom, Classe FROM eleves ';
        $SQL .= 'ORDER BY Classe, NOM, Prenom';
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> execute();
        if ($STMT != '') {
            $result = array();
            $tempResult = array();
            while ($ROW = $STMT -> fetch()) {
                $tmp = array();
                $tmp[0] = $ROW[0];
                $nom = formatName($ROW, $mode=$mode);
                $tmp[1] = $nom;
                if ($mode == 'TROMBI')
                    $tmp[2] = formatName($ROW);
                $tmp[] = $ROW[3];
                $tempResult[doAscii($nom)] = $tmp;
                }
            //uksort($tempResult, 'strcasecmp');
            foreach ($tempResult as $key => $val)
                $result[] = $val;
            return $result;
            }
        return array();
        }

    public function listStudentsFromIds($id_students, $mode='ALL') {
        /*
        retourne la liste des élèves d'après leurs id
        */
        $SQL = 'SELECT id, NOM, Prenom, Classe FROM eleves ';
        $SQL .= 'WHERE id IN ('.$id_students.') ';
        //$SQL .= 'ORDER BY NOM, Prenom';
        $STMT = $this -> DB_USERS -> prepare($SQL);
        $STMT -> execute();
        if ($STMT != '') {
            $result = array();
            $tempResult = array();
            while ($ROW = $STMT -> fetch()) {
                $tmp = array();
                $tmp[0] = $ROW[0];
                $nom = formatName($ROW, $mode=$mode);
                $tmp[1] = $nom;
                if ($mode == 'TROMBI')
                    $tmp[2] = formatName($ROW);
                $tmp[] = $ROW[3];
                $tempResult[doAscii($nom)] = $tmp;
                }
            //uksort($tempResult, 'strcasecmp');
            foreach ($tempResult as $key => $val)
                $result[$val[0]] = $val;
            return $result;
            }
        return array();
        }

    public function listStudentsInGroup($id_group, $old=0) {
        /*
        retourne la liste des élèves d'un groupe.
        On récupère d'abord les id_eleve dans la base DB_PROF,
        puis les noms des élèves dans la base DB_USERS.
        $old = 0 : on récupère les élèves du groupe (par défaut)
        $old = -1 : on récupère aussi les élèves suprimés manuellement du groupe
        $old = -2 : on récupère aussi les élèves suprimés de la base users
        */
        $result = array();
        $SQL = 'SELECT id_eleve FROM groupe_eleve ';
        $SQL .= 'WHERE id_groupe=:id_group AND ordre>0 ';
        $SQL .= 'ORDER BY ordre';
        $OPT = array(':id_group' => $id_group);
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute($OPT);
        $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
        $SQL = 'SELECT NOM, Prenom, Classe from eleves ';
        $SQL .= 'WHERE id=:id';
        $STMT = $this -> DB_USERS -> prepare($SQL);
        foreach ($tempResult as $ROW) {
            $OPT = array(':id' => $ROW['id_eleve']);
            $STMT -> execute($OPT);
            if ($STMT != '') {
                $data = $STMT -> fetch();
                $nom = formatName($data).' '.$data['Classe'];
                $result[] = array($ROW['id_eleve'], $nom);
                }
            }
        if ($old < 0) {
            $SQL = 'SELECT id_eleve FROM groupe_eleve ';
            $SQL .= 'WHERE id_groupe=:id_group ';
            $SQL .= 'AND ordre<=0 AND ordre>:min ';
            $SQL .= 'ORDER BY ordre DESC';
            $OPT = array(':id_group' => $id_group, ':min' => $old);
            $STMT = $this -> DB_PROF -> prepare($SQL);
            $STMT -> execute($OPT);
            $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
            $SQL = 'SELECT NOM, Prenom, Classe from eleves ';
            $SQL .= 'WHERE id=:id';
            $STMT = $this -> DB_USERS -> prepare($SQL);
            foreach ($tempResult as $ROW) {
                $OPT = array(':id' => $ROW['id_eleve']);
                $STMT -> execute($OPT);
                if ($STMT != '') {
                    $data = $STMT -> fetch();
                    $nom = '('.formatName($data).' '.$data['Classe'].')';
                    $result[] = array($ROW['id_eleve'], $nom);
                    }
                }
            }
        return $result;
        }



    /****************************************************
        GROUPES, TABLEAUX, VUES DISPONIBLES, ETC
    ****************************************************/

    public function listGroups() {
        /*
        retourne la liste des groupes du prof
        */
        $RESULT = array();
        $RESULT[] = array(-1, tr('All Groups'));
        if (! $this -> DB_PROF_EXISTS)
            return $RESULT;
        $SQL = 'SELECT id_groupe, Name, Matiere FROM groupes ';
        $SQL .= 'ORDER BY ordre, Matiere, UPPER(Name)';
        $STMT = $this -> DB_PROF -> query($SQL);
        if ($STMT != '') {
            while ($ROW = $STMT -> fetch())
                $RESULT[] = array($ROW[0], $ROW[1].' ('.$ROW[2].')');
            }
        return $RESULT;
        }

    public function listTableaux($id_groupe, $id_periode) {
        /*
        retourne la liste des tableaux publics pour un groupe de la base prof
        */
        $RESULT = array();
        if (! $this -> DB_PROF_EXISTS)
            return $RESULT;
        $OPT = array();
        $SQL = 'SELECT id_tableau, Name FROM tableaux ';
        $SQL .= 'WHERE Public=1 ';
        if ($id_groupe >= 0) {
            $SQL .= 'AND id_groupe=:id_groupe ';
            $OPT[':id_groupe'] = $id_groupe;
            }
        if ($id_periode >= 0) {
            $SQL .= 'AND Periode=:periode ';
            $OPT[':periode'] = $id_periode;
            }
        $SQL .= 'ORDER BY ordre, Name';

        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute($OPT);
        if ($STMT != '')
            while ($ROW = $STMT -> fetch())
                $RESULT[] = array($ROW[0], $ROW[1]);
        return $RESULT;
        }

    public function listVues(
            $id_groupe, $id_periode, $id_tableau) {
        /*
        retourne la liste des vues et de leur disponibilité
        en fonction de la sélection (groupe + période + tableau).
        */
        $RESULT = array();
        if (! $this -> DB_PROF_EXISTS)
            return $RESULT;
        if ($id_tableau > -1) {
            $RESULT[] = array(VUE_ITEM, tr('Items'), True);
            $RESULT[] = array(VUE_STATISTIQUES, tr('Statistics'), True);
            }
        elseif ($id_tableau == -2) {
            // cas d'une compilation de tableaux
            $RESULT[] = array(VUE_ITEM, tr('Items'), True);
            $RESULT[] = array(VUE_STATISTIQUES, tr('Statistics'), True);
            }
        else {
            $RESULT[] = array(VUE_ITEM, tr('Items'), False);
            $RESULT[] = array(VUE_STATISTIQUES, tr('Statistics'), False);
            }
        $RESULT[] = array(-1, 'SEPARATOR', False);
        if (($id_groupe > -1) and ($id_periode > -1)) {
            if ($id_periode < 999)
                $RESULT[] = array(VUE_BILAN, tr('Balances'), True);
            else
                $RESULT[] = array(VUE_BILAN, tr('Balances'), False);
            $RESULT[] = array(VUE_BULLETIN, tr('Bulletin'), True);
            $RESULT[] = array(VUE_APPRECIATIONS, tr('Opinions'), True);
            }
        else {
            $RESULT[] = array(VUE_BILAN, tr('Balances'), False);
            $RESULT[] = array(VUE_BULLETIN, tr('Bulletin'), False);
            $RESULT[] = array(VUE_APPRECIATIONS, tr('Opinions'), False);
            }
        $RESULT[] = array(-1, 'SEPARATOR', False);
        if ($id_groupe > -1)
            $RESULT[] = array(VUE_SUIVIS, tr('Followed students'), True);
        else
            $RESULT[] = array(VUE_SUIVIS, tr('Followed students'), False);
        return $RESULT;
        }

    public function tableauItems($id_tableau, $id_template=0) {
        /*
        retourne la liste des items d'un tableau
        */
        if ($id_template == 0) {
            // on cherche si un modèle existe
            $SQL = 'SELECT * FROM tableau_item ';
            $SQL .= 'WHERE id_tableau=:id_tableau ';
            $SQL .= 'AND id_item<0';
            $OPT = array(':id_tableau' => $id_tableau);
            $STMT = $this -> DB_PROF -> prepare($SQL);
            $STMT -> setFetchMode(PDO::FETCH_ASSOC);
            $STMT -> execute($OPT);
            if ($STMT != '') {
                $RESULT = $STMT -> fetch();
                $id_template = $RESULT['id_item'];
                }
            }
        // on récupère la liste
        $SQL = 'SELECT tableau_item.*, items.* FROM tableau_item ';
        $SQL .= 'JOIN items USING (id_item) ';
        $SQL .= 'WHERE id_tableau=:id_tableau ';
        $SQL .= 'ORDER BY ordre';
        $OPT[':id_tableau'] = ($id_template < 0) ? $id_template : $id_tableau;
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        return ($STMT != '') ? $STMT -> fetchAll(): array();
        }

    public function tableauInfos($id_tableau) {
        /*
        retourne les infos d'un tableau
        */
        $SQL = 'SELECT DISTINCT tableaux.*, templates.* FROM tableaux ';
        $SQL .= 'JOIN tableau_item USING (id_tableau) ';
        $SQL .= 'LEFT JOIN templates ON tableau_item.id_item=templates.id_template ';
        $SQL .= 'WHERE id_tableau=:id_tableau';
        $OPT = array(':id_tableau' => $id_tableau);
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute($OPT);
        return ($STMT != '') ? $STMT -> fetch() : array();
        }

    public function getGroupBilans($id_groupe, $periodes, 
                                    $groupClassTypes=array(), $profils=array(), 
                                    $bilansProfils=array(),$onlyBulletin=False) {
        /*
        retourne la liste des bilans pour un groupe et une période donnée.
        On récupère d'abord les tableaux, puis les bilans évalués.
        Ensuite on trie les bilans (par type puis noms).
        */
        $RESULT = array('ORDER' => array());

        // on récupère les classeType des compétences partagées :
        $what = array('confidentiel' => 'CONFIDENTIEL', 'referentiel' => 'REFERENTIEL', 'bulletin' => 'BULLETIN');
        $competences = array(
            'CONFIDENTIEL' => array('ORDER' => array()), 
            'REFERENTIEL' => array('ORDER' => array()), 
            'BULLETIN' => array('ORDER' => array()));
        foreach ($what as $table => $name) {
            $SQL = 'SELECT code, classeType FROM '.$table.' ORDER BY code';
            $STMT = $this -> DB_COMMUN -> prepare($SQL);
            $STMT -> execute();
            if ($STMT != '')
                while ($ROW = $STMT -> fetch()) {
                    $competences[$name]['ORDER'][] = $ROW['code'];
                    if (! array_key_exists($ROW['code'], $competences[$name]))
                        $competences[$name][$ROW['code']] = array();
                    $competences[$name][$ROW['code']][] = $ROW['classeType'];
                    }
            }

        // on regroupe les bilans par type :
        $sortedBilans = array(
            'ORDER' => array('CONFIDENTIEL', 'REFERENTIEL', 'BULLETIN', 'PERSO_BLT', 'PERSO'),
            'CONFIDENTIEL' => array(), 'REFERENTIEL' => array(), 'BULLETIN' => array(),
            'PERSO_BLT' => array(), 'PERSO' => array());
        $dicoBilans = array();

        // on récupère uniquement les bilans pour lesquels des évaluations sont disponibles :
        // LEFT JOIN pour récupérer les bilans même sans synthèse de groupe
        $SQL = 'SELECT DISTINCT bilans.*, groupe_bilan.value FROM bilans ';
        $SQL .= 'JOIN evaluations USING (id_bilan) ';
        $SQL .= 'LEFT JOIN groupe_bilan ON ';
        $SQL .= '(groupe_bilan.id_bilan=bilans.id_bilan ';
        $SQL .= 'AND groupe_bilan.id_groupe='.$id_groupe.' ';
        $SQL .= 'AND groupe_bilan.Periode IN ('.$periodes[0].')) ';
        $SQL .= 'WHERE evaluations.id_tableau IN ('.$periodes[1].') ';
        $SQL .= 'AND bilans.id_competence>-2 ';
        $SQL .= 'ORDER BY bilans.Name';
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute();
        $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($TEMP_RESULT as $bilan) {
            if ($bilan['id_competence'] >= DECALAGE_CFD)
                $bilanType = 'CONFIDENTIEL';
            elseif ($bilan['id_competence'] >= DECALAGE_BLT)
                $bilanType = 'BULLETIN';
            elseif ($bilan['id_competence'] > -1) {
                $bilanType = 'REFERENTIEL';
                // un bilan du référentiel peut être mis sur la partie perso du bulletin :
                if (array_key_exists($bilan['id_bilan'], $bilansProfils))
                    foreach ($bilansProfils[$bilan['id_bilan']] as $profil)
                        if (in_array($profil, $profils)) {
                            $bilan['id_competence'] = -1;
                            $classeTypes = array(0 => -1);
                            $bilanType = 'PERSO_BLT';
                            }
                }
            else {
                $bilanType = 'PERSO';
                $classeTypes = array(0 => -1);
                if (array_key_exists($bilan['id_bilan'], $bilansProfils))
                    foreach ($bilansProfils[$bilan['id_bilan']] as $profil)
                        if (in_array($profil, $profils))
                            $bilanType = 'PERSO_BLT';
                }
            // si le classeType d'une compétence partagée ne correspond pas,
            // on passe le bilan dans PERSO :
            if ($bilan['id_competence'] > -1) {
                $classeTypes = $competences[$bilanType][$bilan['Name']];
                $mustDo = False;
                foreach ($classeTypes as $classeType) {
                    if (($classeType == -1) or (in_array($classeType, $groupClassTypes)))
                        $mustDo = True;
                    }
                if (! $mustDo)
                    $bilanType = 'PERSO';
                }
            $mustDo = True;
            if ($onlyBulletin)
                if ($bilanType != 'BULLETIN')
                    if ($bilanType != 'PERSO_BLT')
                    $mustDo = False;
            if ($mustDo) {
                $bilan2 = array(
                    'Name' => $bilan['Name'], 
                    'id_bilan' => $bilan['id_bilan'], 
                    'Label' => $bilan['Label'], 
                    'id_competence' => $bilan['id_competence'], 
                    'type' => $bilanType, 
                    'classeTypes' => $classeTypes, 
                    'value' => $bilan['value']);
                $sortedBilans[$bilanType][] = $bilan['Name'];
                $dicoBilans[$bilan['Name']] = $bilan2;
                }
            }

        // on réordonne les bilans communs :
        foreach (array('CONFIDENTIEL', 'REFERENTIEL', 'BULLETIN') as $bilansList)
            foreach ($competences[$bilansList]['ORDER'] as $bilanName)
                if (in_array($bilanName, $sortedBilans[$bilansList])) {
                    $RESULT[$bilanName] = $dicoBilans[$bilanName];
                    if (! in_array($bilanName, $RESULT['ORDER']))
                        $RESULT['ORDER'][] = $bilanName;
                    }
        // on réordonne les bilans persos du bulletin :
        $persosText = '';
        foreach ($sortedBilans['PERSO_BLT'] as $bilanName) {
            if ($persosText == '')
                $persosText = '"'.$bilanName.'"';
            else
                $persosText .= ', "'.$bilanName.'"';
            }
        $sortedBilans['PERSO_BLT'] = array();
        $SQL = 'SELECT DISTINCT bilans.Name ';
        $SQL .= 'FROM bilans ';
        $SQL .= 'JOIN profil_bilan_BLT USING (id_bilan) ';
        $SQL .= 'JOIN profils USING (id_profil) ';
        $SQL .= 'WHERE bilans.Name IN ('.$persosText.') ';
        $SQL .= 'AND bilans.id_competence>-2 ';
        $SQL .= 'ORDER BY profils.id_profil, profil_bilan_BLT.ordre';
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute();
        $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($TEMP_RESULT as $bilan)
            $sortedBilans['PERSO_BLT'][] = $bilan['Name'];
        // on inscrit les bilans persos dans les résultats :
        foreach (array('PERSO_BLT', 'PERSO') as $bilansList)
            foreach ($sortedBilans[$bilansList] as $bilanName) {
                $RESULT[$bilanName] = $dicoBilans[$bilanName];
                if (! in_array($bilanName, $RESULT['ORDER']))
                    $RESULT['ORDER'][] = $bilanName;
                }

        return $RESULT;
        }

    public function pg2selection($periode, $id_groupe) {
        // on calcule le faux id_tableau (id_selection) :
        return (10 + $periode) * 1000 + $id_groupe;
        }

    public function selection2pg($id_selection) {
        // on calcule le faux id_tableau (id_selection) :
        $id_groupe = $id_selection % 1000;
        $periode = ($id_selection - $id_groupe) / 1000 - 10;
        return array($periode, $id_groupe);
        }

    public function diversFromSelection(
            $id_tableau=-1, $id_groupe=-1, $periode=-1) {
        /*
        Retourne divers renseignements utiles sur un tableau ou une sélection (groupe + période).
        data = (id_groupe, groupeName, matiere, periode, 
                tableauName, public, periodesGroupe, periodesToUse, 
                id_template, templateName, 
                tableauLabel, templateLabel)
        periodesGroupe est la liste des périodes pour lesquelles le groupe possède un tableau.
        periodesToUse donne la période à lire et la liste des périodes à calculer :
            periodesToUse = (période à lire, [périodes à calculer])
        */
        $public = -1;
        $groupeName = '';
        $matiere = '';
        $tableauName = '';
        $tableauLabel = '';
        $periodesGroupe = array();
        $periodesToUse = array();
        # si tout est à -1:
        if (($id_tableau == -1) and ($id_groupe == -1) and ($periode == -1))
            $id_tableau = $_SESSION['SELECTED_TABLEAU'][0];
        if ($id_tableau != -1) {
            $SQL = 'SELECT DISTINCT tableaux.id_groupe, groupes.Name, groupes.Matiere, ';
            $SQL .= 'tableaux.Periode, tableaux.Name, tableaux.Public, tableaux.Label, ';
            $SQL .= 'tableaux2.Periode ';
            $SQL .= 'FROM tableaux ';
            $SQL .= 'JOIN groupes USING (id_groupe) ';
            $SQL .= 'JOIN tableaux AS tableaux2 USING (id_groupe) ';
            $SQL .= 'WHERE tableaux.id_tableau='.$id_tableau;
            $STMT = $this -> DB_PROF -> prepare($SQL);
            $STMT -> execute();
            $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
            // on passe par foreach car on doit récupérer toutes les périodes :
            $first = True;
            foreach ($TEMP_RESULT as $r) {
                if ($first) {
                    $first = False;
                    $id_groupe = $r[0];
                    $groupeName = $r[1];
                    $matiere = $r[2];
                    $periode = $r[3];
                    $tableauName = $r[4];
                    $public = $r[5];
                    $tableauLabel = $r[6];
                    }
                $periodesGroupe[] = $r[7];
                }
            }
        else {
            if ($id_groupe == -1)
                $id_groupe = $_SESSION['SELECTED_GROUP'][0];
            $matiere = '';
            if ($periode == -1)
                $periode = $_SESSION['ASSESS_PERIOD'][0];
            $tableaux = $this -> tableauxFromGroupe($id_groupe, $periode);
            if (count($tableaux) > 0) {
                $SQL = 'SELECT DISTINCT groupes.Name, groupes.Matiere, tableaux.Periode ';
                $SQL .= 'FROM groupes ';
                $SQL .= 'JOIN tableaux USING (id_groupe) ';
                $SQL .= 'WHERE groupes.id_groupe='.$id_groupe;
                $STMT = $this -> DB_PROF -> prepare($SQL);
                $STMT -> execute();
                $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
                // on passe par foreach car on doit récupérer toutes les périodes :
                $first = True;
                foreach ($TEMP_RESULT as $r) {
                    if ($first) {
                        $first = False;
                        $groupeName = $r[0];
                        $matiere = $r[1];
                        }
                    $periodesGroupe[] = $r[2];
                    }
                }
            else {
                $SQL = 'SELECT Name, Matiere FROM groupes ';
                $SQL .= 'WHERE id_groupe='.$id_groupe;
                $STMT = $this -> DB_PROF -> prepare($SQL);
                $STMT -> execute();
                $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
                foreach ($TEMP_RESULT as $r) {
                    $groupeName = $r[0];
                    $matiere = $r[1];
                    }
                }
            }
        // ICI COUNTS
        // en cas de comptages sans tableaux (PP par exemple) :
        $counts = $this -> countsFromGroup($id_groupe);
        if (array_key_exists($periode, $counts))
            if (count($counts[$periode]['ITEMS']) > 0)
                $periodesGroupe[] = $periode;
        // recherche des périodes à lire et à calculer :
        if ($periode > 0) {
            if (in_array($periode, $periodesGroupe))
                $periodesToUse = array($periode, array($periode));
            else
                $periodesToUse = array(0, array(0));
            }
        elseif ($periode == 0)
            $periodesToUse = array(0, $periodesGroupe);
        // recherche d'un modèle lié au tableau :
        $id_template = 0;
        $templateName = tr('no template');
        $templateLabel = '';
        if ($id_tableau > -1) {
            $SQL = 'SELECT * FROM tableau_item ';
            $SQL .= 'WHERE id_tableau='.$id_tableau.' ';
            $SQL .= 'AND id_item<0';
            $STMT = $this -> DB_PROF -> prepare($SQL);
            $STMT -> execute();
            $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
            foreach ($TEMP_RESULT as $r)
                $id_template = $r[1];
            }
        if ($id_template < 0) {
            $SQL = 'SELECT * FROM templates ';
            $SQL .= 'WHERE id_template='.$id_template;

            $STMT = $this -> DB_PROF -> prepare($SQL);
            $STMT -> execute();
            $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
            foreach ($TEMP_RESULT as $r) {
                $templateName = $r[1];
                $templateLabel = $r[3];
                }
            }
        // on a tout récupéré :
        $data = array($id_groupe, $groupeName, $matiere, $periode, 
                $tableauName, $public, $periodesGroupe, $periodesToUse, 
                $id_template, $templateName, 
                $tableauLabel, $templateLabel);
        return $data;
        }

    public function tableauxFromGroupe(
            $id_groupe, $periode=-1, $publicOnly=True) {
        /*
        */
        $RESULT = array();
        $SQL = 'SELECT id_tableau FROM tableaux WHERE id_groupe='.$id_groupe.' ';
        if ($periode != -1)
            $SQL .= 'AND (Periode='.$periode.' OR Periode=0) ';
        if ($publicOnly)
            $SQL .= 'AND Public=1';
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute();
        $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($TEMP_RESULT as $r)
            $RESULT[] = $r['id_tableau'];
        return $RESULT;
        }

    public function countsFromGroup(
            $id_groupe, $id_eleve=-1, $period=-1) {
        /*
        récupère les comptages liés à des items des élèves du groupe
        Si id_eleve et period sont donnés en arguments, 
        on ne retourne les résultats que d'un élève pour une période (plus année)
        pour la vue "Élève".
        */
        $RESULT = array();
        if ($id_eleve < 0) {
            // on cherche tous les élèves et pour toutes les périodes :
            $SQL = 'SELECT id FROM periodes ';
            $SQL .= 'ORDER BY id';
            $STMT = $this -> DB_COMMUN -> prepare($SQL);
            $STMT -> execute();
            $tempResult = ($STMT != '') ? $STMT -> fetchAll() : array();
            $periods = array();
            foreach ($tempResult as $period) {
                $RESULT[$period[0]] = array('ITEMS' => array(), 'VALUES' => array());
                }
            $SQL = 'SELECT DISTINCT counts_values.Periode, counts_values.id_eleve, ';
            $SQL .= 'counts.id_item, counts_values.perso ';
            $SQL .= 'FROM counts_values ';
            $SQL .= 'JOIN counts USING (id_count, id_groupe, Periode) ';
            $SQL .= 'WHERE counts.id_item>-1 ';
            $SQL .= 'AND counts_values.perso!="" ';
            $SQL .= 'AND counts_values.id_groupe='.$id_groupe;
            $STMT = $this -> DB_PROF -> prepare($SQL);
            $STMT -> execute();
            $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
            foreach ($TEMP_RESULT as $r) {
                $count_period = $r[0];
                $count_eleve = $r[1];
                $count_item = $r[2];
                $count_value = $r[3];
                if (! in_array($count_item, $RESULT[$count_period]['ITEMS']))
                    $RESULT[$count_period]['ITEMS'][] = $count_item;
                if (! array_key_exists($count_eleve.'|'.$count_item, $RESULT[$count_period]['VALUES']))
                    $RESULT[$count_period]['VALUES'][$count_eleve.'|'.$count_item] = $count_value;
                else
                    $RESULT[$count_period]['VALUES'][$count_eleve.'|'.$count_item] .= $count_value;
                }
            }
        else {
            // un élève et une période sont sélectionnés :
            $RESULT = array('ITEMS' => array(), 'VALUES' => array());
            $SQL = 'SELECT DISTINCT counts_values.Periode, counts_values.id_eleve, ';
            $SQL .= 'counts.id_item, counts_values.perso ';
            $SQL .= 'FROM counts_values ';
            $SQL .= 'JOIN counts USING (id_count, id_groupe, Periode) ';
            $SQL .= 'WHERE counts.id_item>-1 ';
            $SQL .= 'AND counts_values.perso!="" ';
            $SQL .= 'AND counts_values.id_groupe='.$id_groupe.' ';
            $SQL .= 'AND counts_values.id_eleve='.$id_eleve.' ';
            $SQL .= 'AND counts_values.Periode IN (0, '.$periode.') ';
            $STMT = $this -> DB_PROF -> prepare($SQL);
            $STMT -> execute();
            $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
            foreach ($TEMP_RESULT as $r) {
                $count_eleve = $r[1];
                $count_item = $r[2];
                $count_value = $r[3];
                if (! in_array($count_item, $RESULT['ITEMS']))
                    $RESULT['ITEMS'][] = $count_item;
                if (! array_key_exists($count_item, $RESULT['VALUES']))
                    $RESULT['VALUES'][$count_item] = $count_value;
                else
                    $RESULT['VALUES'][$count_item] .= $count_value;
                }
            }
        return $RESULT;
        }


    /****************************************************
        ÉLÈVES SUIVIS (AFFICHAGE ET ÉVALUATION)
    ****************************************************/

    public function getFollows($id_classe, $id_eleve=-1) {
        /*
        */
        if ($this -> no_db_suivis)
            return array();
        // on récupère d'abord la liste de toutes les compétences suivies 
        // dans la base commun :
        $CPT = array();
        $infos = getClassInfos($this, '', $id_classe);
        $SQL = 'SELECT id, code, Competence FROM suivi ';
        $SQL .= 'WHERE Competence!="" ';
        $SQL .= 'AND (classeType=:classe_type OR classeType=-1) ';
        $SQL .= 'ORDER BY ordre';
        $OPT = array(':classe_type' => $infos['classeType']);
        $STMT = $this -> DB_COMMUN -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute($OPT);
        $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($TEMP_RESULT as $follow)
            $CPT[$follow['id']] = array($follow['code'], $follow['Competence']);
        // puis celles suivies pour les élèves concernés :
        $RESULT = array();
        $SQL = 'SELECT * FROM suivi_pp_eleve_cpt ';
        if ($id_eleve != -1)
            $SQL .= 'WHERE id_eleve IN ('.$id_eleve.') ';
        $SQL .= 'ORDER BY id_pp, id_eleve';
        $STMT = $this -> DB_SUIVIS -> prepare($SQL);
        $STMT -> setFetchMode(PDO::FETCH_ASSOC);
        $STMT -> execute();
        $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        $TEMP_ALL = array();
        foreach ($TEMP_RESULT as $follow) {
            $id_PP = $follow['id_pp'];
            $id_eleve = $follow['id_eleve'];
            $id_cpt = $follow['id_cpt'];
            if (! array_key_exists($id_PP, $RESULT)) {
                $RESULT[$id_PP] = array('ALL' => array());
                $TEMP_ALL[$id_PP] = array();
                }
            if (! array_key_exists($id_eleve, $RESULT[$id_PP]))
                $RESULT[$id_PP][$id_eleve] = array();
            $RESULT[$id_PP][$id_eleve][] = array(
                $id_cpt, 
                $CPT[$id_cpt][0], 
                $CPT[$id_cpt][1], 
                $follow['label2']);
            if (! in_array($id_cpt, $TEMP_ALL[$id_PP]))
                $TEMP_ALL[$id_PP][] = $id_cpt;
            }
        foreach (array_keys($RESULT) as $id_PP)
            foreach (array_keys($CPT) as $id_cpt)
                if (in_array($id_cpt, $TEMP_ALL[$id_PP]))
                    $RESULT[$id_PP]['ALL'][] = array($id_cpt, $CPT[$id_cpt][0], $CPT[$id_cpt][1], '');
        return $RESULT;
        }

    public function horaires() {
        /*
        retourne la liste des horaires de l'établissement.
        */
        $SQL = 'SELECT * FROM horaires ORDER BY id';
        $STMT = $this -> DB_COMMUN -> prepare($SQL);
        $STMT -> execute();
        $RESULT0 = ($STMT != '') ? $STMT -> fetchAll() : array();
        $RESULT = array();
        foreach ($RESULT0 as $horaire) {
            $id_horaire = $horaire[0];
            $name = $horaire[1];
            $label = $horaire[2];
            $RESULT[$id_horaire] = array($name, $label);
            }
        return $RESULT;
        }

    public function setFollows($data) {
        /*
        enregistrement d'une évaluation ou d'une appréciation dans la base suivis du PP.
            * @param array $data
                ($id_prof, $matiere, $id_pp, $id_eleve, $date, $horaire, $id_item, $value)
        */
        $RESULT = '|';
        // on prépare la suppression : :
        $DEL_SQL = 'DELETE FROM suivi_evals ';
        $DEL_SQL .= 'WHERE id_prof=:id_prof AND matiere=:matiere AND id_pp=:id_pp ';
        $DEL_SQL .= 'AND id_eleve=:id_eleve AND date=:date AND horaire=:horaire ';
        $DEL_SQL .= 'AND id_cpt=:id_cpt';
        // et les enregistrements :
        $STMT_SQL = 'INSERT INTO suivi_evals ';
        $STMT_SQL .= 'VALUES(:id_prof, :matiere, :id_pp, :id_eleve, ';
        $STMT_SQL .= ':date, :horaire, :id_cpt, :value)';
        // inscription dans la base suivi_pp.sqlite :
        $id_pp = $data[2];
        $filename = $this -> CHEMIN_SUIVIS.'suivi_'.$id_pp.'.sqlite';
        if (! is_writable($filename))
            $RESULT .= $id_pp.':-1|';
        else {
            $db_suivisPP = new PDO('sqlite:'.$filename);
            $db_suivisPP -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
            try {
                // on y va
                $db_suivisPP -> beginTransaction();
                $OPT_SUIVI = array();
                $OPT_SUIVI[':id_prof'] = $data[0];
                $OPT_SUIVI[':matiere'] = $data[1];
                $OPT_SUIVI[':id_pp'] = $data[2];
                $OPT_SUIVI[':id_eleve'] = $data[3];
                $OPT_SUIVI[':date'] = $data[4];
                $OPT_SUIVI[':horaire'] = $data[5];
                $OPT_SUIVI[':id_cpt'] = $data[6];
                $DEL_SUIVI = $db_suivisPP -> prepare($DEL_SQL);
                $DEL_SUIVI -> execute($OPT_SUIVI);
                $value = $data[7];
                // la valeur XXX permet au prof d'effacer un enregistrement :
                if ($value != 'XXX') {
                    $OPT_SUIVI[':value'] = $value;
                    $STMT_SUIVI = $db_suivisPP -> prepare($STMT_SQL);
                    $STMT_SUIVI -> execute($OPT_SUIVI);
                    }
                $db_suivisPP -> commit();
                $RESULT = True;
                }
            catch(PDOException $e) {
                $RESULT .= $id_pp.':-2|';
                $db_suivisPP -> rollBack();
                }
            }
        return $RESULT;
        }



    /****************************************************
        ÉCRITURE DANS LA BASE PROF
    ****************************************************/

    public function updateDatetime() {
        /*
        mise à jour de la date de la base prof
        */
        $date = date("YmdHi");
        $key = 'datetime';
        try {
            // on prépare la suppression :
            $SQL = 'DELETE FROM config ';
            $SQL .= 'WHERE key_name=:key_name';
            $DEL_PROF = $this -> DB_PROF -> prepare($SQL);
            // et les enregistrements :
            $SQL = 'INSERT INTO config ';
            $SQL .= 'VALUES(:key_name, :value_int, :value_txt)';
            $STMT_PROF = $this -> DB_PROF -> prepare($SQL);
            // on y va
            $this -> DB_PROF -> beginTransaction();
            $OPT_PROF = array(':key_name' => $key);
            $DEL_PROF -> execute($OPT_PROF);
            $OPT_PROF[':value_int'] = $date;
            $OPT_PROF[':value_txt'] = 'WEB';
            $STMT_PROF -> execute($OPT_PROF);
            // On valide les modifications
            $this -> DB_PROF -> commit();
            return True;
            }
        catch(PDOException $e) {
            $this -> DB_PROF -> rollBack();
            }
        return False;
        }

    public function setAppreciation(
            $id_eleve, $periode, $value, $matiere) {
        /*
        enregistrement d'une appréciation dans la base prof
            * @param int        $id_eleve
            * @param int        $periode
            * @param string     $value
            * @param string     $matiere    la matière à saisir (nom)
        */
        if (! $this -> DB_PROF_OK)
            return False;
        try {
            // on prépare la suppression :
            $SQL = 'DELETE FROM appreciations ';
            $SQL .= 'WHERE id_eleve=:id_eleve AND Matiere=:matiere AND Periode=:periode';
            $DEL_PROF = $this -> DB_PROF -> prepare($SQL);
            // et les enregistrements :
            $SQL = 'INSERT INTO appreciations ';
            $SQL .= 'VALUES(:id_eleve, :value, :matiere, :periode)';
            $STMT_PROF = $this -> DB_PROF -> prepare($SQL);
            // on y va
            $this -> DB_PROF -> beginTransaction();
            $OPT_PROF = array(
                ':id_eleve' => $id_eleve, ':matiere' => $matiere, ':periode' => $periode);
            $DEL_PROF -> execute($OPT_PROF);
            if ($value != '') {
                $OPT_PROF[':value'] = $value;
                $STMT_PROF -> execute($OPT_PROF);
                }
            $this -> DB_PROF -> commit();
            return True;
            }
        catch(PDOException $e) {
            $this -> DB_PROF -> rollBack();
            }
        return False;
        }

    public function setItemValue(
            $id_eleve, $id_tableau, $id_item, $id_bilan, $value) {
        /*
        enregistrement d'une évaluation dans la base prof
            * @param int        $id_eleve
            * @param int        $id_tableau
            * @param int        $id_item
            * @param string     $value
        */
        if (! $this -> DB_PROF_OK)
            return False;
        try {
            // cas d'une compilation de tableaux : on efface dans les autres tableaux
            if (count($_SESSION['ASSESS_TABLEAUX_FOR_ITEM']) > 0)
                $id_tableaux = array2string($_SESSION['ASSESS_TABLEAUX_FOR_ITEM'][$id_item]);
            else
                $id_tableaux = $id_tableau;
            // on prépare la suppression :
            $SQL = 'DELETE FROM evaluations ';
            $SQL .= 'WHERE id_eleve=:id_eleve AND id_tableau IN ('.$id_tableaux.') ';
            $SQL .= 'AND id_item=:id_item AND id_bilan=:id_bilan';
            $DEL_PROF = $this -> DB_PROF -> prepare($SQL);
            // et les enregistrements :
            $SQL = 'INSERT INTO evaluations ';
            $SQL .= 'VALUES(:id_tableau, :id_eleve, :id_item, :id_bilan, :value)';
            $STMT_PROF = $this -> DB_PROF -> prepare($SQL);
            // on y va
            $this -> DB_PROF -> beginTransaction();
            $OPT_PROF = array(
                ':id_eleve' => $id_eleve, 
                ':id_item' => $id_item, ':id_bilan' => $id_bilan);
            $DEL_PROF -> execute($OPT_PROF);
            if ($value != '') {
                $OPT_PROF[':id_tableau'] = $id_tableau;
                $OPT_PROF[':value'] = $value;
                $STMT_PROF -> execute($OPT_PROF);
                }
            $this -> DB_PROF -> commit();
            return True;
            }
        catch(PDOException $e) {
            $this -> DB_PROF -> rollBack();
            }
        return False;
        }

    public function setMoyenne($value, $moyenne) {
        /*
        inscription d'une moyenne d'item dans la table moyennesItems.
        Cette fonction n'est appelée que si la moyenne n'est pas trouvée dans la table.
        */
        if (! $this -> DB_PROF_OK)
            return False;
        try {
            // on prépare l'enregistrement :
            $SQL = 'INSERT INTO moyennesItems ';
            $SQL .= 'VALUES(:value, :moyenne)';
            $STMT_PROF = $this -> DB_PROF -> prepare($SQL);
            // on y va
            $this -> DB_PROF -> beginTransaction();
            $OPT_PROF = array(':value' => $value, ':moyenne' => $moyenne);
            $STMT_PROF -> execute($OPT_PROF);
            $this -> DB_PROF -> commit();
            return True;
            }
        catch(PDOException $e) {
            $this -> DB_PROF -> rollBack();
            }
        return False;
        }

    public function reCalcBilans($all=False) {
        /*
        On recalcule les valeurs et on les inscrit dans la table evaluations.
        On tient compte des comptages liés à des bilans.
        S'il y a des notes calculées (calcMode = 1 ou 2), on relance le calcul.
        Si $all est à True, on recalcule tout.
        */
        // récupération de la sélection :
        $data = $this -> diversFromSelection(
            $_SESSION['SELECTED_TABLEAU'][0], 
            $_SESSION['SELECTED_GROUP'][0], 
            $_SESSION['ASSESS_PERIOD'][0]);
        $id_groupe = $_SESSION['SELECTED_GROUP'][0];
        $periode = $data[3];
        $periodesToUse = $data[7];
        //debugToConsole('reCalcBilans: '.$id_groupe.'| periode: '.$periode);
        // liste des élèves :
        $students = $this -> listStudentsInGroup($_SESSION['SELECTED_GROUP'][0]);
        $id_students = array2string($students, 0);
        // pour ne lancer les écritures qu'à la fin :
        $SQL_lines = array(
            'deleteEvaluations' => array(), 'insertEvaluations' => array(), 
            'deleteGroupeBilan' => array(), 'insertGroupeBilan' => array());

        // on récupère les comptages liés à des items des élèves du groupe :
        $counts = $this -> countsFromGroup($id_groupe);

        foreach ($periodesToUse[1] as $index => $periodeToCalc)
            if ($_SESSION['PROTECTED_PERIODS'][$periodeToCalc] != 1) {
                // on récupère tous les tableaux du groupe pour la période considérée
                $tableaux = $this -> tableauxFromGroupe($id_groupe, $periodeToCalc);
                // et leurs ids (tableaux ou templates) :
                $tableauxOrTemplates = array();
                foreach ($tableaux as $index => $id_tableau) {
                    $id_template = $this -> diversFromSelection($id_tableau=$id_tableau);
                    $id_template = $id_template[8];
                    if ($id_template < 0)
                        $tableauxOrTemplates[] = $id_template;
                    else
                        $tableauxOrTemplates[] = $id_tableau;
                    }
                $id_tableauxOrTemplates = array2string($tableauxOrTemplates);
                $id_tableaux = array2string($tableaux);

                // dicoBilans pour récupérer les bilans à calculer
                // pour chaque bilan, on récupère bilanName, bilanLabel, id_competence
                // mais aussi la liste des items liés (dicoBilans[id_bilan][3])
                // et un boolean mustRecalc pour savoir s'il faut recalculer ce bilan
                // (on ne le sait qu'après avoir listé tous les items liés)
                $dicoBilans = array();

                // on cherche tous les bilans :
                $SQL = 'SELECT DISTINCT items.*, bilans.*, item_bilan.coeff ';
                $SQL .= 'FROM items ';
                $SQL .= 'JOIN item_bilan USING (id_item) ';
                $SQL .= 'JOIN tableau_item USING (id_item) ';
                $SQL .= 'JOIN bilans USING (id_bilan) ';
                $SQL .= 'WHERE item_bilan.id_item>-1 ';
                $SQL .= 'AND bilans.id_competence>-2 ';
                $SQL .= 'AND tableau_item.id_tableau IN ('.$id_tableauxOrTemplates.')';
                $STMT = $this -> DB_PROF -> prepare($SQL);
                $STMT -> execute();
                $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
                foreach ($TEMP_RESULT as $r) {
                    $id_item = $r[0];
                    $itemName = $r[1];
                    $id_bilan = $r[3];
                    $bilanName = $r[4];
                    $bilanLabel = $r[5];
                    $id_competence = $r[6];
                    $coeff = $r[7];
                    $mustRecalc = False;
// ICI
                    if (in_array($id_item, $_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_items']))
                        $mustRecalc = True;
                    elseif ($all)
                        $mustRecalc = True;
                    // on place les données dans le dictionnaire :
                    if (! array_key_exists($id_bilan, $dicoBilans)) {
                        $dicoBilans[$id_bilan] = array(
                            $bilanName, $bilanLabel, $id_competence, 
                            array(), 
                            $mustRecalc);
                        }
                    $dicoBilans[$id_bilan][3][] = array($id_item, $itemName, $coeff);
                    if ($mustRecalc)
                        $dicoBilans[$id_bilan][4] = True;
                    }
                // on ajoute les bilans des items évalués par comptage :
                $count_items = array2string($counts[$periodeToCalc]['ITEMS']);
                $SQL = 'SELECT DISTINCT items.*, bilans.*, item_bilan.coeff ';
                $SQL .= 'FROM items ';
                $SQL .= 'JOIN item_bilan USING (id_item) ';
                $SQL .= 'JOIN bilans USING (id_bilan) ';
                $SQL .= 'WHERE items.id_item IN ('.$count_items.') ';
                $SQL .= 'AND bilans.id_competence>-2';
                $STMT = $this -> DB_PROF -> prepare($SQL);
                $STMT -> execute();
                $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
                foreach ($TEMP_RESULT as $r) {
                    $id_item = $r[0];
                    $itemName = $r[1];
                    $id_bilan = $r[3];
                    $bilanName = $r[4];
                    $bilanLabel = $r[5];
                    $id_competence = $r[6];
                    $coeff = $r[7];
// ICI COUNTS
                    $mustRecalc = True;//False;
                    /*
                    if (in_array($id_item, $_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_items']))
                        $mustRecalc = True;
                    */
                    // on place les données dans le dictionnaire :
                    if (! array_key_exists($id_bilan, $dicoBilans)) {
                        $dicoBilans[$id_bilan] = array(
                            $bilanName, $bilanLabel, $id_competence, 
                            array(), 
                            $mustRecalc);
                        }
                    $dicoBilans[$id_bilan][3][] = array($id_item, $itemName, $coeff);
                    if ($mustRecalc)
                        $dicoBilans[$id_bilan][4] = True;
                    }

                // récupération des valeurs dans un dictionnaire :
                $dicoValues = array();
                $SQL = 'SELECT * FROM evaluations ';
                $SQL .= 'WHERE id_tableau IN ('.$id_tableaux.') ';
                $SQL .= 'AND id_bilan<0';
                $STMT = $this -> DB_PROF -> prepare($SQL);
                $STMT -> execute();
                $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
                foreach ($TEMP_RESULT as $r) {
                    $id_eleve = $r[1];
                    $id_item = $r[2];
                    $value = $r[4];
                    if (! array_key_exists($id_eleve.'|'.$id_item, $dicoValues))
                        $dicoValues[$id_eleve.'|'.$id_item] = $value;
                    else
                        $dicoValues[$id_eleve.'|'.$id_item] .= $value;
                    }
                // on ajoute les évaluations faites par comptage :
                foreach ($counts[$periodeToCalc]['VALUES'] as $what => $count_value) {
                    if (! array_key_exists($what, $dicoValues))
                        $dicoValues[$what] = $count_value;
                    else
                        $dicoValues[$what] .= $count_value;
                    }

                // on calcule le faux id_tableau (id_selection) pour lire les données :
                $id_selection = $this -> pg2selection($periodeToCalc, $id_groupe);

                // on vide la table evaluations des bilans de cette sélection :
// ICI
                if ($all) {
                    foreach ($students as $student) {
                        $id_eleve = $student[0];
                        foreach ($dicoBilans as $id_bilan => $b)
                            if ($dicoBilans[$id_bilan][4]) {
                                // DELETE FROM evaluations ...
                                $SQL_lines['deleteEvaluations'][] = array(
                                    $id_selection, $id_eleve, $id_bilan);
                                }
                        }
                    }
                else {
                    foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve)
                        foreach ($dicoBilans as $id_bilan => $b)
                            if ($dicoBilans[$id_bilan][4]) {
                                // DELETE FROM evaluations ...
                                $SQL_lines['deleteEvaluations'][] = array(
                                    $id_selection, $id_eleve, $id_bilan);
                                }
                    }
                // pour chaque élève :
                foreach ($students as $student) {
                    $id_eleve = $student[0];
// ICI
                    $mustRecalc = False;
                    if (in_array($id_eleve, $_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves']))
                        $mustRecalc = True;
                    elseif ($all)
                        $mustRecalc = True;
                    if ($mustRecalc)
                        foreach ($dicoBilans as $id_bilan => $b)
                            if ($dicoBilans[$id_bilan][4]) {
                                // C'EST LÀ QU'IL FAUT CALCULER LE BILAN :
                                $value = '';
                                $values = value2List('');
                                foreach ($dicoBilans[$id_bilan][3] as $index => $item) {
                                    $id_item = $item[0];
                                    $coeff = $item[2];
                                    $value2 = '';
                                    if (array_key_exists($id_eleve.'|'.$id_item, $dicoValues)) {
                                        $value2 = $dicoValues[$id_eleve.'|'.$id_item];
                                        $value2 = moyenneItem($value2);
                                        }
                                    $listvalueItem = calculItemValue(
                                        $value2, $coeff);
                                    for ($i=0; $i<5; $i++)
                                        $values[$i] += $listvalueItem[$i];
                                    }
                                $value = calculBilan($values);
                                // on remplit la table evaluations :
                                if ($value != '') {
                                    // INSERT INTO evaluations ...
                                    $SQL_lines['insertEvaluations'][] = array(
                                        $id_selection, $id_eleve, -1, $id_bilan, $value);
                                    }
                                }
                    }

                // un deuxième dictionnaire pour les bilans du groupe
                // on passe tous les bilans en revue, mais on ne gardera
                // que ceux qui sont à recalculer (dicoBilans[id_bilan][4] à True) :
                $bilansGroupeDic = array();
                $SQL = 'SELECT id_bilan, value FROM evaluations ';
                $SQL .= 'WHERE id_eleve IN ('.$id_students.') ';
                $SQL .= 'AND id_tableau='.$id_selection.' AND id_bilan!=-1';
                $STMT = $this -> DB_PROF -> prepare($SQL);
                $STMT -> execute();
                $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
                foreach ($TEMP_RESULT as $r) {
                    $id_bilan = $r[0];
                    $value = $r[1];
                    if ($dicoBilans[$id_bilan][4]) {
                        if (array_key_exists($id_bilan, $bilansGroupeDic))
                            $bilansGroupeDic[$id_bilan] .= $value;
                        else
                            $bilansGroupeDic[$id_bilan] = $value;
                        }
                    }
                foreach ($bilansGroupeDic as $id_bilan => $b) {
                    // DELETE FROM groupe_bilan ...
                    $SQL_lines['deleteGroupeBilan'][] = array(
                        $id_bilan, $id_groupe, $periodeToCalc);
                    $value = $bilansGroupeDic[$id_bilan];
                    $value = moyenneBilan($bilansGroupeDic[$id_bilan]);
                    if ($value != '') {
                        // INSERT INTO groupe_bilan ...
                        $SQL_lines['insertGroupeBilan'][] = array(
                            $id_bilan, $id_groupe, $periodeToCalc, $value);
                        }
                    }

                }

        # mise à jour des notes calculées d'après les items ou les bilans :
        $SQL = 'SELECT * FROM notes ';
        $SQL .= 'WHERE id_groupe='.$id_groupe.' AND calcMode>=0';
        $STMT = $this -> DB_PROF -> prepare($SQL);
        $STMT -> execute();
        $TEMP_RESULT = ($STMT != '') ? $STMT -> fetchAll() : array();
        foreach ($TEMP_RESULT as $r) {
            $notePeriode = $r[1];
            if ($_SESSION['PROTECTED_PERIODS'][$notePeriode] != 1) {
                $id_note = $r[2];
                $name = $r[3];
                $base = $r[4];
                $coeff = $r[5];
                $calcMode = $r[6];
                $data = array($id_groupe, $notePeriode, $id_note, $name, $base, $coeff, $calcMode);
                // À FAIRE :
                $this -> recalculeNote($data);
                }
            }

        // inscriptions des lignes de la liste $SQL_lines dans la base prof :
        try {
            $this -> DB_PROF -> beginTransaction();
            $SQL = 'DELETE FROM evaluations';
            $SQL.= ' WHERE id_tableau=:id_tableau AND id_eleve=:id_eleve AND id_bilan=:id_bilan';
            $STMT_0 = $this -> DB_PROF -> prepare($SQL);
            $SQL = 'INSERT INTO evaluations ';
            $SQL .= 'VALUES(:id_tableau, :id_eleve, :id_item, :id_bilan, :value)';
            $STMT_1 = $this -> DB_PROF -> prepare($SQL);
            $SQL = 'DELETE FROM groupe_bilan';
            $SQL.= ' WHERE id_bilan=:id_bilan AND id_groupe=:id_groupe AND Periode=:periode';
            $STMT_2 = $this -> DB_PROF -> prepare($SQL);
            $SQL = 'INSERT INTO groupe_bilan';
            $SQL.= ' VALUES(:id_bilan, :id_groupe, :periode, :value)';
            $STMT_3 = $this -> DB_PROF -> prepare($SQL);
            foreach ($SQL_lines['deleteEvaluations'] as $index => $data) {
                $OPT = array(
                    ':id_tableau' => $data[0], ':id_eleve' => $data[1], ':id_bilan' => $data[2]);
                $STMT_0 -> execute($OPT);
                }
            foreach ($SQL_lines['insertEvaluations'] as $index => $data) {
                $OPT = array(
                    ':id_tableau' => $data[0], ':id_eleve' => $data[1], ':id_item' => $data[2],
                    ':id_bilan' => $data[3], ':value' => $data[4]);
                $STMT_1 -> execute($OPT);
                }
            foreach ($SQL_lines['deleteGroupeBilan'] as $index => $data) {
                $OPT = array(
                    ':id_bilan' => $data[0], ':id_groupe' => $data[1], ':periode' => $data[2]);
                $STMT_2 -> execute($OPT);
                }
            foreach ($SQL_lines['insertGroupeBilan'] as $index => $data) {
                $OPT = array(
                    ':id_bilan' => $data[0], ':id_groupe' => $data[1], ':periode' => $data[2],
                    ':value' => $data[3]);
                $STMT_3 -> execute($OPT);
                }
            $this -> DB_PROF -> commit();
            }
        catch(PDOException $e) {
            $this -> DB_PROF -> rollBack();
            }

        // on vide la liste ASSESS_WHAT_IS_MODIFIED :
        $mustSaveDateTime = $_SESSION['ASSESS_WHAT_IS_MODIFIED']['mustSaveDateTime'];
        $_SESSION['ASSESS_WHAT_IS_MODIFIED'] = 
            array('mustSaveDateTime' => $mustSaveDateTime, 'id_eleves' => array(), 'id_items' => array());
        }

    public function recalculeNote($data) {
        /*
        $data = array($id_groupe, $notePeriode, $id_note, $name, $base, $coeff, $calcMode);

        data :          id_groupe, Periode, id_note,    Name, base, coeff, calcMode
        dans la base :  id_groupe, Periode, id_note,    id_eleve, value

        on utilise les id_eleve contenus dans la liste whatIsModified['id_eleves'].
        */

        if ($data[6] < 0) {
            $this -> recalculeMoyennes($data);
            return;
            }
        // pour ne lancer les écritures qu'à la fin :
        //$SQL_linesNotes = array(array(), array(), array(), array());

/*
        # on efface les anciennes valeurs :
        commandLine_my = utils_db.qdf_prof_noteValue2.format(data[0], data[1], data[2])
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # une liste pour les nouvelles lignes à inscrire dans la table notes_values :
        listNoteValues = []
        # calcul des notes :
        if data[6] == 0:
            # calcul d'après les items :
            id_groupe = data[0]
            tableauxInCompil = []
            commandLine_my = (
                'SELECT id_tableau FROM tableaux'
                ' WHERE id_groupe={0} AND Public=1'
                ' AND (Periode={1} OR Periode=0)')
            commandLine_my = commandLine_my.format(id_groupe, data[1])
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                tableauxInCompil.append(int(query_my.value(0)))
            base = data[4]
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            listesForIn = ['', '']
            valuesDic = {}
            for id_tableau in tableauxInCompil:
                if listesForIn[0] == '':
                    listesForIn[0] = '{0}'.format(id_tableau)
                else:
                    listesForIn[0] = '{0}, {1}'.format(listesForIn[0], id_tableau)
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {
                if listesForIn[1] == '':
                    listesForIn[1] = '{0}'.format(id_eleve)
                else:
                    listesForIn[1] = '{0}, {1}'.format(listesForIn[1], id_eleve)
            commandLine_my = ('SELECT * FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_bilan=-1').format(listesForIn[0], listesForIn[1])
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(1))
                value = query_my.value(4)
                if id_eleve in valuesDic:
                    valuesDic[id_eleve] += value
                else:
                    valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {
                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        elif data[6] == 1:
            # calcul d'après les bilans :
            id_groupe = data[0]
            periodeToUse = diversFromSelection(main, id_groupe=id_groupe, periode=data[1])[7][0]
            base = data[4]
            id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            forIn = ''
            valuesDic = {}
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {
                if forIn == '':
                    forIn = '{0}'.format(id_eleve)
                else:
                    forIn = '{0}, {1}'.format(forIn, id_eleve)
            commandLine_my = (
                'SELECT id_eleve, value FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_item=-1').format(id_selection, forIn)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                value = query_my.value(1)
                if id_eleve in valuesDic:
                    valuesDic[id_eleve] += value
                else:
                    valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {

                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        elif data[6] == 2:
            # calcul d'après les bilans persos :
            id_groupe = data[0]
            periodeToUse = diversFromSelection(main, id_groupe=id_groupe, periode=data[1])[7][0]
            base = data[4]
            id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            forIn = ''
            valuesDic = {}
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {

                if forIn == '':
                    forIn = '{0}'.format(id_eleve)
                else:
                    forIn = '{0}, {1}'.format(forIn, id_eleve)
            commandLine_my = (
                'SELECT evaluations.id_eleve, evaluations.value '
                'FROM evaluations '
                'JOIN bilans ON bilans.id_bilan=evaluations.id_bilan '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND bilans.id_competence=-1').format(id_selection, forIn)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                value = query_my.value(1)
                if id_eleve in valuesDic:
                    valuesDic[id_eleve] += value
                else:
                    valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {

                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        elif data[6] == 3:
            # calcul d'après les bilans persos sélectionnés pour le bulletin :
            # on doit aussi gérer les profils.
            id_groupe = data[0]
            periodeToUse = diversFromSelection(main, id_groupe=id_groupe, periode=data[1])[7][0]
            base = data[4]
            id_selection = utils_functions.pg2selection(periodeToUse, id_groupe)
            # récupération des profils et des bilans sélectionnés pour chacun :
            commandLine_my = (
                'SELECT DISTINCT bilans.*, profils.id_profil '
                'FROM bilans '
                'JOIN item_bilan ON item_bilan.id_bilan=bilans.id_bilan '
                'JOIN groupes ON groupes.id_groupe={0} '
                'JOIN profils ON (profils.id_groupe=groupes.id_groupe AND (profils.Periode={1} OR profils.Periode=0)) '
                'JOIN profil_bilan_BLT ON profil_bilan_BLT.id_profil=profils.id_profil '
                'WHERE profil_bilan_BLT.id_bilan=bilans.id_bilan '
                'AND item_bilan.id_item>-1 '
                'ORDER BY profils.id_profil, profil_bilan_BLT.ordre').format(
                    id_groupe, periodeToUse)
            profilBilans = {}
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_bilan = int(query_my.value(0))
                id_profil = int(query_my.value(4))
                # on inscrit dans le dictionnaire :
                try:
                    profilBilans[id_profil].append(id_bilan)
                except:
                    profilBilans[id_profil] = [id_bilan]
            # récupération des bilans à utiliser pour chaque élève :
            elevesBilans = {}
            matiere = diversFromSelection(main, id_groupe=id_groupe, periode=data[1])[2]
            studentsAndGroupProfils = getStudentsAndGroupProfils(
                main, id_students, periodeToUse, id_groupe, matiere=matiere)
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {
                elevesBilans[id_eleve] = []
                # il faut récupérer la liste des profils valables de l'élève :
                eleveProfilsDic = studentsAndGroupProfils['STUDENTS'][id_eleve]
                for profilPeriode in eleveProfilsDic:
                    id_profil = eleveProfilsDic[profilPeriode]
                    if id_profil in profilBilans:
                        for id_bilan in profilBilans[id_profil]:
                            if not(id_bilan in elevesBilans[id_eleve]):
                                elevesBilans[id_eleve].append(id_bilan)
            # utilisation d'un dico et de IN pour accélérer l'affichage :
            forIn = ''
            valuesDic = {}
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {

                if forIn == '':
                    forIn = '{0}'.format(id_eleve)
                else:
                    forIn = '{0}, {1}'.format(forIn, id_eleve)
            commandLine_my = (
                'SELECT evaluations.id_eleve, evaluations.id_bilan, evaluations.value '
                'FROM evaluations '
                'WHERE id_tableau IN ({0}) '
                'AND id_eleve IN ({1}) '
                'AND id_item=-1').format(id_selection, forIn)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                id_eleve = int(query_my.value(0))
                id_bilan = int(query_my.value(1))
                # on ne prend en compte que si le bilan est sélectionné :
                if id_bilan in elevesBilans[id_eleve]:
                    value = query_my.value(2)
                    if id_eleve in valuesDic:
                        valuesDic[id_eleve] += value
                    else:
                        valuesDic[id_eleve] = value
            for id_eleve in valuesDic:
                value = valuesDic[id_eleve]
                note = utils_calculs.calculNote(main, value=value)
                valuesDic[id_eleve] = note
            for id_eleve in ids_eleves:
            foreach ($_SESSION['ASSESS_WHAT_IS_MODIFIED']['id_eleves'] as $index => $id_eleve) {

                try:
                    value = valuesDic[id_eleve]
                    value = base * value / 20
                    listNoteValues.append((data[0], data[1], data[2], id_eleve, value))
                except:
                    continue
        # inscription des nouveaux enregistrements :
        commandLine_my = utils_db.insertInto('notes_values')
        query_my = utils_db.queryExecute(
            {commandLine_my: listNoteValues}, query=query_my)
        # calcul et inscription de la moyenne :
        $this -> recalculeMoyennes($data, id_eleve=-2, column=-1);
*/
        }

    public function recalculeMoyennes($data) {
        /*
        Calcule la moyenne de la note (données passées via data),
        ainsi que la moyenne de l'élève (via id_eleve),
        la moyenne des moyennes (pour la période sélectionnée)
        et enfin les moyennes du bilan annuel.

        Si id_eleve = -1, on doit recalculer pour tous les élèves du groupe.
        Si id_eleve = -2, on recalcule pour les id_eleve contenus dans la liste 
            whatIsModified['id_eleves'].
        Sinon, on ne calcule que pour l'élève correspondant.

        data :          id_groupe, Periode, id_note,    Name, base, coeff, calcMode
        dans la base :  id_groupe, Periode, id_note,    id_eleve, value
                        (avec id_eleve = - id_groupe - 1)

        row, column : indiquent la case sélectionnée
            cela permet de mettre à jour l'affichage
            (row = -1 si aucune case sélectionnée, c'est à dire si id_eleve = -1)
            (column = -1 si on ne veut pas mettre à jour l'affichage)

        */

/*
        # cas de mise à jour de l'affichage :
        mustUpdateView = (column >= 0) and (id_eleve > -2)
        # récupération de la liste des id_eleve :
        ids_eleves = []
        if id_eleve == -1:
            elevesInGroup = listStudentsInGroup(main, id_groupe=data[0])
            for eleve in elevesInGroup:
                ids_eleves.append(eleve[0])
            row = 0
        elif id_eleve == -2:
            for idEleve in main.whatIsModified['id_eleves']:
                ids_eleves.append(idEleve)
        else:
            ids_eleves.append(id_eleve)

        # Calcul de la moyenne de la note indiquée :
        somme, effectif, moyenne = 0.0, 0, ''
        commandLine_my = utils_db.qs_prof_noteValue2.format(data[0], data[1], data[2])
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            value = query_my.value(0)
            if value != 'X':
                somme += float(value)
                effectif += 1
        if effectif > 0:
            moyenne = round(somme / effectif, utils.precisionNotes)
        # inscription de la moyenne dans la table notes_values :
        idForGroupe = - data[0] - 1
        commandLine_my = utils_db.qdf_prof_noteValue.format(data[0], data[1], data[2], idForGroupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        if moyenne != '':
            commandLine_my = utils_db.insertInto('notes_values')
            listArgs = (data[0], data[1], data[2], idForGroupe, moyenne)
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
        # et on modifie l'affichage :
        if mustUpdateView:
            rowGroupe = main.model.rowCount() - 1
            item = main.view.model().index(rowGroupe, column)
            main.model.rows[rowGroupe][column]['value'] = moyenne
            main.view.update(item)

        # Pour chaque élève :
        lines = []
        for id_eleve in ids_eleves:
            # calcul de la moyenne de l'élève pour la période sélectionnée :
            somme, effectif, moyenne = 0.0, 0, ''
            commandLine_my = utils_db.qs_prof_noteValues.format(data[0], data[1], id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            while query_my.next():
                base = float(query_my.value(1))
                coeff = float(query_my.value(2))
                value = query_my.value(0)
                if value != 'X':
                    value = float(query_my.value(0))
                    value = 20 *  value / base
                    somme += coeff * value
                    effectif += coeff
            if effectif > 0:
                moyenne = round(somme / effectif, utils.precisionNotes)
            # inscription de la moyenne dans la table notes_values :
            commandLine_my = utils_db.qdf_prof_noteValue.format(data[0], data[1], -1, id_eleve)
            query_my = utils_db.queryExecute(commandLine_my, query=query_my)
            if moyenne != '':
                listArgs = [data[0], data[1], -1, id_eleve, moyenne]
                lines.append(listArgs)
            # et on modifie l'affichage :
            if mustUpdateView:
                columnMoyenne = main.model.columnCount() - 1
                item = main.view.model().index(row, columnMoyenne)
                main.model.rows[row][columnMoyenne]['value'] = moyenne
                main.view.update(item)
                row += 1
        # inscription de la moyenne dans la table notes_values :
        commandLine_my = utils_db.insertInto('notes_values')
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)

        # Calcul de la moyenne des moyennes :
        somme, effectif, moyenne = 0.0, 0, ''
        commandLine_my = utils_db.qs_prof_noteValue2.format(data[0], data[1], -1)
        query_my = utils_db.queryExecute(commandLine_my, db=main.db_my)
        while query_my.next():
            value = query_my.value(0)
            if value != 'X':
                somme += float(value)
                effectif += 1
        if effectif > 0:
            moyenne = round(somme / effectif, utils.precisionNotes)
        # inscription de la moyenne dans la table notes_values :
        idForGroupe = - data[0] - 1
        commandLine_my = utils_db.qdf_prof_noteValue.format(data[0], data[1], -1, idForGroupe)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        if moyenne != '':
            commandLine_my = utils_db.insertInto('notes_values')
            listArgs = (data[0], data[1], -1, idForGroupe, moyenne)
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
        # et on modifie l'affichage :
        if mustUpdateView:
            rowGroupe = main.model.rowCount() - 1
            columnMoyenne = main.model.columnCount() - 1
            item = main.view.model().index(rowGroupe, columnMoyenne)
            main.model.rows[rowGroupe][columnMoyenne]['value'] = moyenne
            main.view.update(item)

        # Calcul des bilans annuels :
        elevesNotes = {}
        commandLine_my = (
            'SELECT * FROM notes_values '
            'WHERE id_groupe={0} '
            'AND Periode<999 '
            'AND id_note=-1 '
            'AND id_eleve>-1').format(data[0])
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        while query_my.next():
            id_eleve = int(query_my.value(3))
            moyenne = query_my.value(4)
            if moyenne != 'X':
                moyenne = float(query_my.value(4))
            if id_eleve in elevesNotes:
                elevesNotes[id_eleve].append(moyenne)
            else:
                elevesNotes[id_eleve] = [moyenne]
        # on efface les anciennes valeurs :
        commandLine_my = utils_db.qdf_prof_noteValue2.format(data[0], 999, -1)
        query_my = utils_db.queryExecute(commandLine_my, query=query_my)
        # une liste pour calculer la moyenne du groupe :
        moyennesNotes = []
        lines = []
        for id_eleve in elevesNotes:
            # Calcul de la moyenne annuelle de l'élève :
            somme, moyenne = 0.0, ''
            effectif = len(elevesNotes[id_eleve])
            for value in  elevesNotes[id_eleve]:
                somme += value
            if effectif > 0:
                moyenne = round(somme / effectif, utils.precisionNotes)
            # inscription de la moyenne dans la table notes_values :
            if moyenne != '':
                listArgs = [data[0], 999, -1, id_eleve, moyenne]
                lines.append(listArgs)
            # et ajout à la liste moyennesNotes du groupe :
            moyennesNotes.append(moyenne)
        # inscription de la moyenne dans la table notes_values :
        commandLine_my = utils_db.insertInto('notes_values')
        query_my = utils_db.queryExecute({commandLine_my: lines}, query=query_my)
        # Calcul de la moyenne du groupe :
        somme, moyenne = 0.0, ''
        effectif = len(moyennesNotes)
        for value in  moyennesNotes:
            somme += value
        if effectif > 0:
            moyenne = round(somme / effectif, utils.precisionNotes)
        # inscription de la moyenne dans la table notes_values :
        if moyenne != '':
            commandLine_my = utils_db.insertInto('notes_values')
            listArgs = (data[0], 999, -1, idForGroupe, moyenne)
            query_my = utils_db.queryExecute({commandLine_my: listArgs}, query=query_my)
*/
        }



    }
?>
