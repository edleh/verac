/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var calcul = document.getElementById('calcul');

calcul.onclick = function(event) {
    /*
    clic sur le bouton calculer
    */

    var result = document.getElementById('result');
    while (result.firstChild)
        result.removeChild(result.firstChild);

    var values = [
        parseInt(document.getElementById('nbA').value), 
        parseInt(document.getElementById('nbB').value), 
        parseInt(document.getElementById('nbC').value), 
        parseInt(document.getElementById('nbD').value), 
        parseInt(document.getElementById('nbX').value)];

    var total = parseInt(values[0] + values[1] + values[2] + values[3] + values[4]);
    if (values[4] > 0) {
        // calcul du % de X :
        var pcX = values[4] / total;
        // on recentre les valeurs en fonction du % de X :
        var V2J = values[0] * pcX;
        values[0] -= V2J;
        values[1] += V2J;
        var R2O = values[3] * pcX;
        values[3] -= R2O;
        values[2] += R2O;
        // on recalcule le total sans tenir compte des X :
        total = parseInt(values[0] + values[1] + values[2] + values[3]);
        }
    // on peut calculer la note :
    if (total > 0) {
        var note = 0;
        for (i=0; i<4; i++)
            note += valeursNotes[i] * values[i];
        note = note / total;
        note = Math.round(note * 10) / 10;
        result.innerHTML = '<strong>' + note + ' / 20</strong>';
        }
    };

