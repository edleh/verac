/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

var alertMsg = document.getElementById('alert_msg');

function go() {
    /*
    demande de changement de mot de passe.
    */
    alertMsg.innerHTML = '';
    while (alertMsg.classList.length > 0)
        alertMsg.classList.remove(alertMsg.classList.item(0));
    alertMsg.classList.add('alert', 'text-center');
    setTimeout(function() {
        var password1 = document.getElementById('password1').value;
        var password2 = document.getElementById('password2').value;
        if (password1 == '') {
            var textContent = document.getElementById('msgNoNewPassword').textContent;
            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
            alertMsg.classList.add('alert-danger');
            }
        else if (password2 == '') {
            var textContent = document.getElementById('msgNoConfirmation').textContent;
            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
            alertMsg.classList.add('alert-danger');
            }
        else if (password1 != password2) {
            var textContent = document.getElementById('msgNotMatch').textContent;
            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
            alertMsg.classList.add('alert-danger');
            }
        else {
            var httpRequest = new XMLHttpRequest();
            httpRequest.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {
                        if (this.responseText.indexOf('OK') >= 0) {
                            var textContent = document.getElementById('msgOK').textContent;
                            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                            alertMsg.classList.add('alert-success');
                            window.location = '?page=student_password';
                            }
                        else {
                            var textContent = document.getElementById('msgError').textContent;
                            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                            alertMsg.classList.add('alert-danger');
                            }
                        }
                    else {
                        var textContent = document.getElementById('msgError').textContent;
                        alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                        alertMsg.classList.add('alert-danger');
                        }
                    }
                };
            httpRequest.open('POST', '?page=student_password', true);
            httpRequest.setRequestHeader(
                'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            httpRequest.send('action=request&password1=' + password1);
            }
        }, 1000);
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    var target = event.target;
    var id = target.getAttribute('id');
    if (id == 'go')
        go();
    };

container.onkeypress = function(event) {
    /*
    touche entrée
    */
    if (event.key == 'Enter' || event.which == 13 || event.keyCode == 13) {
        go();
        return false;
        }
    return true;
    };
