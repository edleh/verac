/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    /*
    pour plier-déplier les détails des compétences partagées.
    */
    var target = event.target;
    if (target.parentNode.classList.contains('detail_parent')) {
        var index = target.parentNode.getAttribute('id');
        var elems = document.querySelectorAll('tr.detail_child');
        //console.log(target.parentNode.getAttribute('id') + ' - ' + elems.length);
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].classList.contains(index))
                elems[i].classList.toggle('hidden');
            }
        }
    };
