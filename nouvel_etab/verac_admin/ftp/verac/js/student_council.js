/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

function saveChanges(newValue) {
    // enregistrement de la nouvelle appréciation :
    var synthese = document.getElementById('synthese');
    var id_pp = synthese.getAttribute('id_pp');
    var newValue2 = newValue.split('&').join('|||');
    var matiere_code = synthese.getAttribute('matiere_code');
    setTimeout(function() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    if (this.responseText == 'INACTIVE_TIME')
                        window.location = '?page=login';
                    else {

                        var httpRequest2 = new XMLHttpRequest();
                        httpRequest2.onreadystatechange = function() {
                            if (this.readyState == 4) {
                                if (this.status == 200) {
                                    container.innerHTML = this.responseText;
                                    updateChart();
                                    }
                                else 
                                    window.location = '?page=student_council';
                                }
                            };
                        httpRequest2.open('POST', '?page=student_council', true);
                        httpRequest2.setRequestHeader(
                            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        httpRequest2.send('action=reload');
                        }
                    }
                else 
                    window.location = '?page=student_council';
                }
            };
        httpRequest.open('POST', '?page=student_balances', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send(
            'action=request&what=changeAppreciationPP' 
            + '&id_pp=' + id_pp
            + '&appreciation=' + newValue2 
            + '&matiere_code=' + matiere_code);
        }, 1000);
    };

function nl2br(str, is_xhtml) {
    /*
    pour garder les sauts de lignes (http://phpjs.org/functions/nl2br)
    */
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    };

function syntheseToggle() {
    // pour gérer les appréciations PP :
    toggle_visibility('synthese', '');
    toggle_visibility('synthese-edit', 'table-row');
    toggle_visibility('button-edit', '');
    toggle_visibility('button-close', 'inline-block');
    toggle_visibility('button-save', 'inline-block');
    };

function toggle_visibility(id, display) {
    var e = document.getElementById(id);
    if (e.style.display == display)
        e.style.display = 'none';
    else
        e.style.display = display;
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    /*
    pour plier-déplier les détails des compétences partagées.
    Mais aussi permettre au PP d'éditer sa synthèse.
    */
    var target = event.target;
    if (target.parentNode.classList.contains('detail_parent')) {
        var index = target.parentNode.getAttribute('id');
        var elems = document.querySelectorAll('tr.detail_child');
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].classList.contains(index))
                elems[i].classList.toggle('hidden');
            }
        }
    else {
        var id = target.getAttribute('id');
        if (id == 'button-edit') {
            // pour éditer l'appréciation PP :
            syntheseToggle();
            }
        else if (id == 'button-save') {
            // pour enregistrer l'appréciation PP :
            var newValue = document.getElementById('appreciationPP').value;
            //console.log('save: ' + newValue);
            saveChanges(newValue);
            }
        else if (id == 'button-close') {
            // pour fermer sans enregistrer :
            var newValue = document.getElementById('synthese').textContent;
            //console.log('close: ' + newValue);
            document.getElementById('appreciationPP').value = newValue;
            syntheseToggle();
            }
        }
    };

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    var compareModal = document.getElementById('compareModal');
    if (event.target == compareModal)
        compareModal.style.display = 'none';
    };
