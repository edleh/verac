/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
    adapté de http://gregweber.info/projects/uitableedit
***************************************************************/

var EDITING = false;

function nl2br(str, is_xhtml) {
    /*
    pour garder les sauts de lignes (http://phpjs.org/functions/nl2br)
    */
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br/>' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    };

function editEval(target) {
    /*
    on demande l'édition d'une évaluation.
    On teste d'abord si on est déja en mode édition, auquel cas on valide.
    En vue suivis, on vérifie si un horaire est bien sélectionné.
    Sinon on passe bien en mode édition.
    */
    if (EDITING) {
        if (document.getElementById('editor_appreciation')) {
            var who = document.getElementById('editor_appreciation').parentNode;
            if (who.getAttribute('id_eleve') != undefined) {
                validateAppreciation('editEval');
                return;
                }
            }
        if (document.getElementById('editor_eval')) {
        var who = document.getElementById('editor_eval').parentNode;
            if (who.getAttribute('id_eleve') != undefined) {
                if (who.getAttribute('id_item') != target.getAttribute('id_item'))
                    validateEval('other');
                else if (who.getAttribute('id_eleve') != target.getAttribute('id_eleve'))
                    validateEval('other');
                }
            return;
            }
        }
    // en vue suivis, un horaire doit être sélectionné :
    var vue = target.getAttribute('vue');
    if (vue == 'suivis') {
        var horaire = document.getElementById('horaire-title').getAttribute('other');
        if (horaire == '') {
            alert(horaireMessage);
            return;
            }
        }
    // on passe donc en mode édition :
    EDITING = true;
    var oldText = target.textContent;
    target.setAttribute('old', oldText);
    editor_eval = '<input id="editor_eval" type="text" class="form-control"'
        + ' value="' + oldText + '"></input>';
    target.innerHTML = editor_eval;
    // gestion des touches entrée et échapp :
    target.onkeydown = function(event) {
        if (event.key == 'Enter' || event.which == 13 || event.keyCode == 13)
            validateEval('checkKeyPress');
        else if (event.key == 'Escape' || event.which == 27 || event.keyCode == 27)
            abortEval();
        };
    // focus bug (seen in FireFox) fixed by small delay
    function focus_text() {
        document.getElementById('editor_eval').focus();
        };
    setTimeout(focus_text, 50);
    };

function validateEval(what) {
    /*
    on sort du mode édition.
    Si l'évaluation a été modifiée, on enregistre.
    */
    var who = document.getElementById('editor_eval').parentNode;
    var newText = document.getElementById('editor_eval').value;
    if (newText == undefined)
        return;
    // suppression des éventuels caractères invisibles de début et fin :
    newText = newText.replace(/^\s+/g,'').replace(/\s+$/g,'');
    newText = newText.toUpperCase();
    oldText = who.getAttribute('old').replace(/^\s+/g,'').replace(/\s+$/g,'');
    if (compare.test(newText)) {
        if (newText != oldText) {
            var vue = who.getAttribute('vue');
            var id_eleve = who.getAttribute('id_eleve');
            var id_item = who.getAttribute('id_item');
            var id_tableau = who.getAttribute('id_tableau');
            var matiere = who.getAttribute('matiere');
            var periode = who.getAttribute('periode');
            var id_pp = who.getAttribute('id_pp');
            var date = '';
            if (document.getElementById('select-date'))
                date = document.getElementById('select-date').value;
            var horaire = '';
            if (document.getElementById('horaire-title'))
                horaire = document.getElementById('horaire-title').getAttribute('other');
            setTimeout(function() {
                var httpRequest = new XMLHttpRequest();
                httpRequest.onreadystatechange = function() {
                    if (this.readyState == 4) {
                        if (this.status == 200) {
                            if (this.responseText == 'INACTIVE_TIME')
                                window.location = '?page=login';
                            else if (this.responseText != false) {
                                while (who.classList.length > 0)
                                    who.classList.remove(who.classList.item(0));
                                who.classList.add(this.responseText, 'edit_eval');
                                }
                            }
                        else 
                            window.location = '?page=assess';
                        }
                    };
                httpRequest.open('POST', '?page=assess', true);
                httpRequest.setRequestHeader(
                    'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                httpRequest.send('action=request&what=changeEval' 
                    + '&vue=' + vue  + '&value=' + newText 
                    + '&id_eleve=' + id_eleve 
                    + '&id_item=' + id_item + '&id_tableau=' + id_tableau 
                    + '&matiere=' + matiere + '&periode=' + periode 
                    + '&id_pp=' + id_pp + '&date=' + date + '&horaire=' + horaire);
                }, 1000);
            }
        who.innerHTML = '';
        if (newText == '')
            newText = '&nbsp;';
        who.innerHTML = newText;
        }
    else {
        alert(alertMessage);
        who.innerHTML = who.getAttribute('old');
        }
    who.removeAttribute('old');
    EDITING = false;
    };

function abortEval() {
    /*
    abandon de modification. On remet la valeur initiale.
    */
    var who = document.getElementById('editor_eval').parentNode;
    var oldText = who.getAttribute('old');
    if (oldText == undefined)
        return;
    who.textContent = oldText;
    who.removeAttribute('old');
    EDITING = false;
    };

function editAppreciation(target) {
    /*
    on demande l'édition d'une appréciation.
    On teste d'abord si on est déja en mode édition, auquel cas on valide.
    En vue suivis, on vérifie si un horaire est bien sélectionné.
    Sinon on passe bien en mode édition.
    */
    if (EDITING) {
        if (document.getElementById('editor_eval')) {
            var who = document.getElementById('editor_eval').parentNode;
            if (who.getAttribute('id_eleve') != undefined) {
                validateEval('document');
                return;
                }
            }
        if (document.getElementById('editor_appreciation')) {
            var who = document.getElementById('editor_appreciation').parentNode;
            if (who.getAttribute('id_eleve') != undefined)
                if (who.getAttribute('id_eleve') != target.getAttribute('id_eleve'))
                    validateAppreciation('other');
            return;
            }
        }
    // en vue suivis, un horaire doit être sélectionné :
    var vue = target.getAttribute('vue');
    if (vue == 'suivis') {
        var horaire = document.getElementById('horaire-title').getAttribute('other');
        if (horaire == '') {
            alert(horaireMessage);
            return;
            }
        }
    // on passe donc en mode édition :
    EDITING = true;
    var oldText = target.textContent;
    target.setAttribute('old', oldText);
    editor_appreciation = '<textarea id="editor_appreciation" class="form-control">'
        + oldText + '</textarea>';
    target.innerHTML = editor_appreciation;
    // gestion des touches entrée et échapp :
    target.onkeydown = function(event) {
        // touche échapp pour abandonner :
        if (event.key == 'Escape' || event.which == 27 || event.keyCode == 27)
            abortAppreciation();
        };
    // focus bug (seen in FireFox) fixed by small delay
    function focus_text() {
        document.getElementById('editor_appreciation').focus();
        };
    setTimeout(focus_text, 50);
    };

function validateAppreciation(what) {
    /*
    on sort du mode édition.
    Si l'appréciation a été modifiée, on enregistre.
    */
    //alert(what);
    var who = document.getElementById('editor_appreciation').parentNode;
    var newText = document.getElementById('editor_appreciation').value;
    if (newText == undefined)
        return;
    // suppression des éventuels caractères invisibles de début et fin :
    newText = newText.replace(/^\s+/g,'').replace(/\s+$/g,'');
    oldText = who.getAttribute('old').replace(/^\s+/g,'').replace(/\s+$/g,'');
    if (newText != oldText) {
        var vue = who.getAttribute('vue');
        // remplacement des "&" qui pourraient être dans l'appréciation :
        var newText2 = newText.split('&').join('|||');
        var id_eleve = who.getAttribute('id_eleve');
        var matiere = who.getAttribute('matiere');
        var periode = who.getAttribute('periode');
        var id_pp = who.getAttribute('id_pp');
        var date = '';
        if (document.getElementById('select-date'))
            date = document.getElementById('select-date').value;
        var horaire = '';
        if (document.getElementById('horaire-title'))
            horaire = document.getElementById('horaire-title').getAttribute('other');
        setTimeout(function() {
            var httpRequest = new XMLHttpRequest();
            httpRequest.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {
                        if (this.responseText == 'INACTIVE_TIME')
                            window.location = '?page=login';
                        }
                    else 
                        window.location = '?page=assess';
                    }
                };
            httpRequest.open('POST', '?page=assess', true);
            httpRequest.setRequestHeader(
                'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            httpRequest.send('action=request&what=changeAppreciation' 
                + '&vue=' + vue  + '&value=' + newText2 
                + '&id_eleve=' + id_eleve 
                + '&matiere=' + matiere + '&periode=' + periode 
                + '&id_pp=' + id_pp + '&date=' + date + '&horaire=' + horaire);
            }, 1000);
        }
    who.innerHTML = nl2br(newText);
    who.removeAttribute('old');
    EDITING = false;
    };

function abortAppreciation() {
    /*
    abandon de modification. On remet la valeur initiale.
    */
    var who = document.getElementById('editor_appreciation').parentNode;
    var oldText = who.getAttribute('old');
    if (oldText == undefined)
        return;
    who.innerHTML = nl2br(oldText);
    who.removeAttribute('old');
    EDITING = false;
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    /*
    clic.
    On gère la sélection de l'horaire.
    On enregistre si on était en mode édition (évaluation ou appréciation).
    Sinon on passe en mode édition.
    */
    //console.log('container.onclick');
    var target = event.target;
    var parentId = target.parentNode.parentNode.parentNode.getAttribute('id');

    if (parentId == 'select-horaire') {
        // changement d'horaire
        var what = target.getAttribute('what');
        var other = target.getAttribute('other');
        var horaireTitle = document.getElementById('horaire-title');
        horaireTitle.innerHTML = what;
        if (other != '')
            horaireTitle.setAttribute('other', other);
        }
    else if (target.classList.contains('edit_eval'))
        editEval(target);
    else if (target.classList.contains('edit_appreciation'))
        editAppreciation(target);
    else if (EDITING) {
        if (document.getElementById('editor_eval')) {
            var who = document.getElementById('editor_eval').parentNode;
            if (who.getAttribute('id_eleve') != undefined) {
                validateEval('document');
                return;
                }
            }
        if (document.getElementById('editor_appreciation')) {
            var who = document.getElementById('editor_appreciation').parentNode;
            if (who.getAttribute('id_eleve') != undefined)
                if (who.getAttribute('id_eleve') != target.parentNode.getAttribute('id_eleve'))
                    validateAppreciation('container.click');
            }
        }
    };





var fixedTable = document.getElementById('fixedTable');
if (fixedTable)
    fixTable(fixedTable);

function fixTable(container) {
    // https://github.com/kevkan/fixed-table

    // Store references to table elements
    var thead = container.querySelector('thead');
    var tbody = container.querySelector('tbody');
    var h = window.innerHeight - 150;
    //console.log('resize: ' + h);
    fixedTable.style.height = h + 'px';

    // Style container
    container.style.overflow = 'auto';
    container.style.position = 'relative';

    // Add inline styles to fix the header row and leftmost column
    function relayout() {
        var ths = [].slice.call(thead.querySelectorAll('th'));
        var tbodyTrs = [].slice.call(tbody.querySelectorAll('tr'));

        /**
        * Remove inline styles so we resort to the default table layout algorithm
        * For thead, th, and td elements, don't remove the 'transform' styles applied
        * by the scroll event listener
        */
        tbody.setAttribute('style', '');
        thead.style.width = '';
        thead.style.position = '';
        thead.style.top = '';
        thead.style.left = '';
        thead.style.zIndex = '';
        ths.forEach(function(th) {
            th.style.display = '';
            th.style.width = '';
            th.style.position = '';
            th.style.top = '';
            th.style.left = '';
            });
        tbodyTrs.forEach(function(tr) {
            tr.setAttribute('style', '');
            });
        [].slice.call(tbody.querySelectorAll('td')).forEach(function(td) {
            td.style.width = '';
            td.style.position = '';
            td.style.left = '';
            });

        /**
        * Store width and height of each th
        * getBoundingClientRect()'s dimensions include paddings and borders
        */
        var thStyles = ths.map(function(th) {
            var rect = th.getBoundingClientRect();
            var style = document.defaultView.getComputedStyle(th, '');
            return {
                boundingWidth: rect.width,
                boundingHeight: rect.height,
                width: parseInt(style.width, 10),
                paddingLeft: parseInt(style.paddingLeft, 10)
                };
            });

        // Set widths of thead and tbody
        var totalWidth = thStyles.reduce(function(sum, cur) {
            return sum + cur.boundingWidth;
            }, 0);
        tbody.style.display = 'block';
        tbody.style.width = totalWidth + 'px';
        thead.style.width = totalWidth - thStyles[0].boundingWidth + 'px';

        // Position thead
        thead.style.position = 'absolute';
        thead.style.top = '0';
        thead.style.left = thStyles[0].boundingWidth + 'px';
        thead.style.zIndex = 10;

        // Set widths of the th elements in thead. For the fixed th, set its position
        ths.forEach(function(th, i) {
            th.style.width = thStyles[i].width + 'px';
            if (i === 0) {
                th.style.position = 'absolute';
                th.style.top = '0';
                th.style.left = -thStyles[0].boundingWidth + 'px';
                }
            });

        // Set margin-top for tbody - the fixed header is displayed in this margin
        tbody.style.marginTop = thStyles[0].boundingHeight + 'px';

        // Set widths of the td elements in tbody. For the fixed td, set its position
        tbodyTrs.forEach(function(tr, i) {
            tr.style.display = 'block';
            tr.style.paddingLeft = thStyles[0].boundingWidth + 'px';
            [].slice.call(tr.querySelectorAll('td')).forEach(function(td, j) {
                td.style.width = thStyles[j].width + 'px';
                if (j === 0) {
                    td.style.position = 'absolute';
                    td.style.left = '0';
                    }
                });
            });
    }

    // Initialize table styles
    relayout();

    // Update table cell dimensions on resize
    window.addEventListener('resize', resizeThrottler, false);
    var resizeTimeout;
    function resizeThrottler() {
        var h = window.innerHeight - 150;
        //console.log('resize: ' + h);
        fixedTable.style.height = h + 'px';

        if (!resizeTimeout) {
            resizeTimeout = setTimeout(function() {
                resizeTimeout = null;
                relayout();
                }, 500);
            }
        }

    // Fix thead and first column on scroll
    container.addEventListener('scroll', function() {
        thead.style.transform = 'translate3d(0,' + this.scrollTop + 'px,0)';
        var hTransform = 'translate3d(' + this.scrollLeft + 'px,0,0)';
        thead.querySelector('th').style.transform = hTransform;
        [].slice.call(tbody.querySelectorAll('tr > td:first-child')).forEach(function(td, i) {
            td.style.transform = hTransform;
            });
        });

    /**
    * Return an object that exposes the relayout function so that we can
    * update the table when the number of columns or the content inside columns changes
    */
    return {
        relayout: relayout
        };
    };




var reCalcBilans = document.getElementById('select-recalc-all');

reCalcBilans.onclick = function(event) {
    // on recalcule les bilans de la sélection :
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                if (this.responseText == 'INACTIVE_TIME')
                    window.location = '?page=login';
                else {
                    var httpRequest2 = new XMLHttpRequest();
                    httpRequest2.onreadystatechange = function() {
                        if (this.readyState == 4) {
                            if (this.status == 200) {
                                container.innerHTML = this.responseText;
                                }
                            else 
                                window.location = '?page=assess';
                            }
                        };
                    httpRequest2.open('POST', '?page=assess', true);
                    httpRequest2.setRequestHeader(
                        'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                    httpRequest2.send('action=reload');
                    }
                }
            else
                window.location = '?page=assess';
            }
        };
    httpRequest.open('POST', '?page=assess', true);
    httpRequest.setRequestHeader(
        'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    httpRequest.send('action=request&what=reCalcBilans');
    };




document.addEventListener('onunload', function() {
    /*
    On tente d'enregistrer datetime une dernière fois 
    lors de la fermeture.
    */
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (this.readyState == 4) {
            if (this.status == 200) {
                if (this.responseText == 'INACTIVE_TIME')
                    window.location = '?page=login';
                }
            else
                window.location = '?page=assess';
            }
        };
    httpRequest.open('POST', '?page=assess', true);
    httpRequest.setRequestHeader(
        'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    httpRequest.send('action=request&what=updateDatetime');
    return;
    });
