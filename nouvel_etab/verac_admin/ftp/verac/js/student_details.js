/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

function changeSubject(newValue) {
    // changement de matière :
    loading();
    setTimeout(function() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    if (this.responseText == 'INACTIVE_TIME')
                        window.location = '?page=login';
                    else {
                        var httpRequest2 = new XMLHttpRequest();
                        httpRequest2.onreadystatechange = function() {
                            if (this.readyState == 4) {
                                if (this.status == 200) {
                                    container.innerHTML = this.responseText;
                                    }
                                else 
                                    window.location = '?page=student_details';
                                }
                            };
                        httpRequest2.open('POST', '?page=student_details', true);
                        httpRequest2.setRequestHeader(
                            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        httpRequest2.send('action=reload');
                        }
                    }
                else 
                    window.location = '?page=student_details';
                }
            };
        httpRequest.open('POST', '?page=student_details', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send('action=request&what=changeSubject' + '&value=' + newValue);
        }, 1000);
    };

function changeDirectory(newValue) {
    // changement de sens :
    loading();
    setTimeout(function() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    if (this.responseText == 'INACTIVE_TIME')
                        window.location = '?page=login';
                    else {
                        var httpRequest2 = new XMLHttpRequest();
                        httpRequest2.onreadystatechange = function() {
                            if (this.readyState == 4) {
                                if (this.status == 200) {
                                    container.innerHTML = this.responseText;
                                    }
                                else 
                                    window.location = '?page=student_details';
                                }
                            };
                        httpRequest2.open('POST', '?page=student_details', true);
                        httpRequest2.setRequestHeader(
                            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        httpRequest2.send('action=reload');
                        }
                    }
                else 
                    window.location = '?page=student_details';
                }
            };
        httpRequest.open('POST', '?page=student_details', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send('action=request&what=changeDirectory' + '&value=' + newValue);
        }, 1000);
    };

function matches(el, selector) {
    return (
        el.matches || el.matchesSelector || el.msMatchesSelector 
        || el.mozMatchesSelector || el.webkitMatchesSelector 
        || el.oMatchesSelector).call(el, selector);
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    /*
    pour plier-déplier les détails des compétences.
    Mais aussi gérer les sélecteurs et l'affichage des conseils.
    */
    var target = event.target;
    if (target.classList.contains('detail_parent')) {
        var index = target.parentNode.getAttribute('id');
        var elems = document.querySelectorAll('tr.detail_child');
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].classList.contains(index))
                elems[i].classList.toggle('hidden');
            }
        }
    else if (matches(target, 'a')) {
        var parentId = target.parentNode.parentNode.parentNode.getAttribute('id');
        if (parentId == 'select-subject') {
            // changement de matière :
            changeSubject(target.getAttribute('what'));
            }
        else if (parentId == 'select-directory') {
            // changement de sens :
            changeDirectory(target.getAttribute('what'));
            }
        else if (target.classList.contains('comment-show')) {
            // pour afficher les conseils dans la fenêtre modale :
            var who = target.getAttribute('who');
            var what = target.getAttribute('what');
            var tip = document.getElementById(who).innerHTML;
            document.getElementById('myModalWhat').innerHTML = what;
            document.getElementById('myModalContent').innerHTML = tip;
            var myModal = document.getElementById('myModal');
            myModal.style.display = 'block';
            // When the user clicks on x, close the modal
            var myModalClose = document.getElementById('myModalClose');
            myModalClose.onclick = function() {
                var myModal = document.getElementById('myModal');
                myModal.style.display = 'none';
                };
            }
        }
    };

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    var myModal = document.getElementById('myModal');
    if (event.target == myModal)
        myModal.style.display = 'none';
    };
