/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

var alertMsg = document.getElementById('alert_msg');

function doGo() {
    // test de connexion :
    alertMsg.innerHTML = '';
    while (alertMsg.classList.length > 0)
        alertMsg.classList.remove(alertMsg.classList.item(0));
    alertMsg.classList.add('alert', 'text-center');
    setTimeout(function() {
        var login = document.getElementById('login').value;
        var password = document.getElementById('password').value;
        if (login == '') {
            var textContent = document.getElementById('msgNoLogin').textContent;
            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
            alertMsg.classList.add('alert-danger');
            }
        else if (password == '') {
            var textContent = document.getElementById('msgNoPassword').textContent;
            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
            alertMsg.classList.add('alert-danger');
            }
        else {
            var httpRequest = new XMLHttpRequest();
            httpRequest.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.status == 200) {
                        if (this.responseText.indexOf('OK') >= 0) {
                            var textContent = document.getElementById('msgOK').textContent;
                            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                            alertMsg.classList.add('alert-success');
                            window.location = '?page=infos';
                            }
                        else if (this.responseText.indexOf('PASSWORD') >= 0) {
                            var textContent = document.getElementById('msgBadPassword').textContent;
                            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                            alertMsg.classList.add('alert-danger');
                            }
                        else if (this.responseText.indexOf('USER') >= 0) {
                            var textContent = document.getElementById('msg5').textContent;
                            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                            alertMsg.classList.add('alert-danger');
                            }
                        else {
                            var textContent = document.getElementById('msg8').textContent;
                            alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                            alertMsg.classList.add('alert-danger');
                            }
                        }
                    else {
                        var textContent = document.getElementById('msgError').textContent;
                        alertMsg.innerHTML = '<strong>' + textContent + '</strong>';
                        alertMsg.classList.add('alert-danger');
                        }
                    }
                };
            httpRequest.open('POST', '?page=login', true);
            httpRequest.setRequestHeader(
                'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            httpRequest.send('action=connect&login=' + login + '&password=' + password);
            }
        }, 1000);
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');
var go = document.getElementById('go');

go.onclick = function(event) {
    doGo();
    };

container.onkeypress = function(event) {
    /*
    touche entrée
    */
    if (event.key == 'Enter' || event.which == 13 || event.keyCode == 13) {
        doGo();
        return false;
        }
    return true;
    };
