/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

function showPage(page) {
    // mise à jour de la page :
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('container').innerHTML = this.responseText;
            var base = document.getElementById('base');
            if (base) {
                Sortable.create(
                    base, 
                    {group: 'omega', sort: false, scroll: true,});
                var selection = document.getElementById('selection');
                Sortable.create(
                    selection, 
                    {group: 'omega', sort: true,});
                }
            }
        };
    httpRequest.open('POST', '?page=' + page, true);
    httpRequest.setRequestHeader(
        'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    httpRequest.send('action=reload');
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    var target = event.target;
    var targetId = target.getAttribute('id');
    if (targetId == 'do-trombi') {
        var group_name = document.getElementById('group-name').value;
        var id_students = '';
        [].forEach.call(document.querySelectorAll('#selection li'), function(el) {
            var text = el.textContent;
            if (text != '') {
                var studentId = el.getAttribute('who');
                if (id_students == '')
                    id_students = studentId;
                else
                    id_students += ',' + studentId;
                }
            });
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200)
                showPage('class_trombinoscope');
            };
        httpRequest.open('POST', '?page=class_trombinoscope', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send(
            'action=request&what=doCustom&group_name=' + group_name 
            + '&who=' + id_students);
        }
    };

document.addEventListener('DOMContentLoaded', function() {
    setTimeout(function() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.open('POST', '?page=class_trombinoscope', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send('action=request&what=clearTempDirectory');
        }, 1000);
    });
