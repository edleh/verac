/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

function changeDirectory(newValue) {
    // changement de sens :
    loading();
    setTimeout(function() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    if (this.responseText == 'INACTIVE_TIME')
                        window.location = '?page=login';
                    else {

                        var httpRequest2 = new XMLHttpRequest();
                        httpRequest2.onreadystatechange = function() {
                            if (this.readyState == 4) {
                                if (this.status == 200) {
                                    container.innerHTML = this.responseText;
                                    }
                                else 
                                    window.location = '?page=class_follows';
                                }
                            };
                        httpRequest2.open('POST', '?page=class_follows', true);
                        httpRequest2.setRequestHeader(
                            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        httpRequest2.send('action=reload');
                        }
                    }
                else 
                    window.location = '?page=class_follows';
                }
            };
        httpRequest.open('POST', '?page=class_follows', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send('action=request&what=changeDirectory' + '&value=' + newValue);
        }, 1000);
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    /*
    changement de sens via le sélecteur.
    */
    var target = event.target;
    var parentId = target.parentNode.parentNode.parentNode.getAttribute('id');
    if (parentId == 'select-directory')
        changeDirectory(target.getAttribute('what'));
    };
