/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

var INACTIVE_TIME = false;
//var DEBUG = true;
var DEBUG = false;

function loading() {
    /*
    on affiche ajax-loader.gif.
    */
    var image = '<p align="center"><br/><br/><img src="images/ajax-loader.gif"></p>';
    container.innerHTML = image;
    };

function navbar(page, reload, request, where, text, other) {
    /*
    mise à jour d'une liste déroulante.
    En cas d'erreur, on rechargera toute la page.
    * page : la page à mettre à jour
    * reload : s'il faut mettre à jour la partie variable de la navbar
    * request : la requête à envoyer à navbar
    * where : 
    * text : 
    * other : 
    */
    //console.log(page + '|' + reload + '|' + request + '|' + where + '|' + text + '|' + other);
    INACTIVE_TIME = false;
    other = (other ? other : '');
    loading();

    // envoi de la requête à la page navbar :
    var w = window.innerWidth;
    var h = window.innerHeight;
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            if (this.responseText == 'INACTIVE_TIME') {
                INACTIVE_TIME = true;
                window.location = '?page=login';
                }
            else {
                reloadNavbar(where, text, other);
                showPage(page);
                //showDebug();
                if (reload)
                    window.location = '?page=' + page;
                }
            }
        };
    httpRequest.open('POST', '?page=navbar', true);
    httpRequest.setRequestHeader(
        'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    httpRequest.send('action=request&' + request + '&w=' + w + '&h=' + h);
    };

function reloadNavbar(where, text, other) {
    // mise à jour du menu qui a été modifié (where, text, other) :
    where = where.replace('#', '');
    //console.log(where + '|' + other);
    document.getElementById(where).innerHTML = text;
    if (other != '')
        document.getElementById(where).setAttribute('other', other);
    };

function showPage(page) {
    // mise à jour de la page :
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            container.innerHTML = this.responseText;
            updateChart();
            }
        };
    httpRequest.open('POST', '?page=' + page, true);
    httpRequest.setRequestHeader(
        'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    httpRequest.send('action=reload');
    };

function updateChart(modal) {
    /*
    mise à jour de l'affichage du camembert et du radar.
    Dans la vue conseil de classe, on peut aussi afficher
    la fenêtre modale de comparaison des périodes.
    */
    modal = (modal ? modal : false);
    if (modal) {
        // on demande la fenêtre modale :
        var w = window.innerWidth;
        var h = window.innerHeight;
        var modalWidth = w - 80;
        if (w > 950)
            modalWidth = 820;
        else if (w > 768)
            modalWidth = 520;
        var size = modalWidth / selectedPeriod;
        if (h / 3 < size)
            size = h / 3;
        //console.log(selectedPeriod + '|' + modalWidth + '|' + size);
        var camembertSize = size - 40;
        var radarSize = size;
        for (i=1; i<selectedPeriod + 1; i++) {
            document.getElementById('camembertChart' + i).innerHTML =
                '<canvas id="camembertChart' + i 
                + '" width="' + camembertSize 
                + '" height="' + camembertSize + '"></canvas>';
            var camembertCtx = document.getElementById('camembertChart' + i).getContext("2d");
            new Chart(camembertCtx).Pie(camembertDatas[i]);
            document.getElementById('radarChart' + i).innerHTML =
                '<canvas id="radarChart' + i 
                + '" width="' + radarSize 
                + '" height="' + radarSize + '"></canvas>';
            var radarCtx = document.getElementById('radarChart' + i).getContext("2d");
            new Chart(radarCtx).Radar2(radarDatas[i], {pointDotRadius : 3});
            };
        }
    else {
        // affichage classique dans la page :
        var camembert = document.getElementById('camembertChart');
        if (camembert) {
            var camembertWidth = camembert.getAttribute('width');
            if (camembertWidth != undefined) {
                camembert.innerHTML =
                    '<canvas id="camembertChart" width="' + camembertWidth 
                    + '" height="' + camembertWidth + '"></canvas>';
                var camembertCtx = camembert.getContext("2d");
                new Chart(camembertCtx).Pie(camembertData);
                }
            var radar = document.getElementById('radarChart');
            var radarWidth = radar.getAttribute('width');
            if (radarWidth != undefined) {
                radar.innerHTML =
                    '<canvas id="radarChart" width="' + radarWidth 
                    + '" height="' + radarWidth + '"></canvas>';
                var radarCtx = radar.getContext("2d");
                new Chart(radarCtx).Radar2(radarData, {pointDotRadius : 3});
                }
            }
        }
    };

function showDebug() {
    // mise à jour de l'affichage debug :
    if (DEBUG) {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById('debug').innerHTML = this.responseText;
                }
            };
        httpRequest.open('POST', '?page=debug', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send('action=reload');
        }
    else
        document.getElementById('debug').innerHTML = '';
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

document.addEventListener('DOMContentLoaded', function() {
    updateChart();
    });

var navbarStyle = document.getElementById('navbar_style');
if (navbarStyle)
    navbarStyle.onclick = function(event) {
        // changement de style de la navbar :
        loading();
        var page = this.getAttribute('page');
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200)
                window.location = '?page=' + page;
            };
        httpRequest.open('POST', '?page=navbar', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send('action=request&what=style');
        };

var lang = document.getElementById('lang');
if (lang)
    [].forEach.call(lang.querySelectorAll('a'), function(el) {
        el.addEventListener('click', function() {
            // changement de langue :
            loading();
            var page = this.getAttribute('page');
            var httpRequest = new XMLHttpRequest();
            httpRequest.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200)
                    window.location = '?page=' + page;
                };
            httpRequest.open('POST', '?page=navbar', true);
            httpRequest.setRequestHeader(
                'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
            httpRequest.send('action=request&what=lang&lang=' + this.getAttribute('lang'));
            })
        });

[].forEach.call(document.querySelectorAll('#select-period li a'), function(el) {
    el.addEventListener('click', function() {
        // changement de période :
        var request = 'period=' + this.getAttribute('what');
        navbar(
            this.getAttribute('page'), 
            this.getAttribute('reload'), 
            request, 
            this.getAttribute('where'), 
            this.textContent);
        })
    });

[].forEach.call(document.querySelectorAll('#select-class li a'), function(el) {
    el.addEventListener('click', function() {
        // changement de classe :
        var request = 'class=' + this.getAttribute('what') + '&className=' + this.textContent;
        navbar(
            this.getAttribute('page'), 
            this.getAttribute('reload'), 
            request, 
            this.getAttribute('where'), 
            this.textContent);
        })
    });

[].forEach.call(document.querySelectorAll('#select-group li a'), function(el) {
    el.addEventListener('click', function() {
        // changement de groupe (partie évaluer) :
        var request = 'group=' + this.getAttribute('what') + '&groupName=' + this.textContent;
        navbar(
            this.getAttribute('page'), 
            this.getAttribute('reload'), 
            request, 
            this.getAttribute('where'), 
            this.textContent);
        })
    });

[].forEach.call(document.querySelectorAll('#select-tableau li a'), function(el) {
    el.addEventListener('click', function() {
        // changement de tableau (partie évaluer) :
        var request = 'tableau=' + this.getAttribute('what') + '&tableauName=' + this.textContent;
        navbar(
            this.getAttribute('page'), 
            this.getAttribute('reload'), 
            request, 
            this.getAttribute('where'), 
            this.textContent);
        })
    });

[].forEach.call(document.querySelectorAll('#select-vue li a'), function(el) {
    el.addEventListener('click', function() {
        // changement de vue (partie évaluer) :
        var request = 'vue=' + this.getAttribute('what') + '&vueName=' + this.textContent;
        navbar(
            this.getAttribute('page'), 
            this.getAttribute('reload'), 
            request, 
            this.getAttribute('where'), 
            this.textContent);
        })
    });

[].forEach.call(document.querySelectorAll('#select-student li a'), function(el) {
    el.addEventListener('click', function() {
        // changement d'élève (par la liste déroulante) :
        var request = 'student=' + this.getAttribute('what') + '&studentName=' + this.textContent;
        navbar(
            this.getAttribute('page'), 
            true, 
            request, 
            this.getAttribute('where'), 
            this.textContent, 
            this.getAttribute('what'));
        })
    });

[].forEach.call(document.querySelectorAll('#select-previous a'), function(el) {
    el.addEventListener('click', function() {
        // changement d'élève (par le bouton précédent ou suivant) :
        var nbStudents = students.length;
        if (nbStudents > 0) {
            loading();
            var what = this.getAttribute('what');
            var actualStudentId = document.getElementById('student-title').getAttribute('other');
            // on cherche le précédent et le suivant :
            var actualStudent = -1;
            for (i=0; i<nbStudents; i++)
                if (students[i][0] == actualStudentId)
                    actualStudent = i;
            var previousStudent = actualStudent - 1;
            if (previousStudent < 0)
                previousStudent = nbStudents - 1;
            var nextStudent = actualStudent + 1;
            if (nextStudent > nbStudents - 1)
                nextStudent = 0;
            // donc celui à sélectionner :
            if (what == 'previous')
                var student = students[previousStudent];
            else
                var student = students[nextStudent];
            // enfin on met à jour la page :
            var request = 'student=' + student[0] + '&studentName=' + student[1];
            navbar(
                this.getAttribute('page'), 
                true, 
                request, 
                'student-title', 
                student[1], 
                student[0]);
            }
        })
    });

[].forEach.call(document.querySelectorAll('#select-next a'), function(el) {
    el.addEventListener('click', function() {
        // changement d'élève (par le bouton précédent ou suivant) :
        var nbStudents = students.length;
        if (nbStudents > 0) {
            loading();
            var what = this.getAttribute('what');
            var actualStudentId = document.getElementById('student-title').getAttribute('other');
            // on cherche le précédent et le suivant :
            var actualStudent = -1;
            for (i=0; i<nbStudents; i++)
                if (students[i][0] == actualStudentId)
                    actualStudent = i;
            var previousStudent = actualStudent - 1;
            if (previousStudent < 0)
                previousStudent = nbStudents - 1;
            var nextStudent = actualStudent + 1;
            if (nextStudent > nbStudents - 1)
                nextStudent = 0;
            // donc celui à sélectionner :
            if (what == 'previous')
                var student = students[previousStudent];
            else
                var student = students[nextStudent];
            // enfin on met à jour la page :
            var request = 'student=' + student[0] + '&studentName=' + student[1];
            navbar(
                this.getAttribute('page'), 
                true, 
                request, 
                'student-title', 
                student[1], 
                student[0]);
            }
        })
    });

[].forEach.call(document.querySelectorAll('#select-custom-trombi a'), function(el) {
    el.addEventListener('click', function() {
        // enfin on met à jour la page :
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200)
                showPage('class_trombinoscope');
            };
        httpRequest.open('POST', '?page=class_trombinoscope', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send('action=request&what=custom');
        })
    });

[].forEach.call(document.querySelectorAll('#select-compare-periods a'), function(el) {
    el.addEventListener('click', function() {
        // pour afficher la comparaison dans la fenêtre modale :
        var compareModal = document.getElementById('compareModal');
        compareModal.style.display = 'block';
        // When the user clicks on x, close the modal
        var myModalClose = document.getElementById('myModalClose');
        myModalClose.onclick = function() {
            var compareModal = document.getElementById('compareModal');
            compareModal.style.display = 'none';
            };
        updateChart(true);
        })
    });
