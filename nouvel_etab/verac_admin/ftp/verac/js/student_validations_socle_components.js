/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/





/***************************************************************
                        FONCTIONS
***************************************************************/

var EDITING = false;

function changeSubject(newValue) {
    // changement de matière :
    loading();
    setTimeout(function() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onreadystatechange = function() {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    if (this.responseText == 'INACTIVE_TIME')
                        window.location = '?page=login';
                    else {

                        var httpRequest2 = new XMLHttpRequest();
                        httpRequest2.onreadystatechange = function() {
                            if (this.readyState == 4) {
                                if (this.status == 200) {
                                    container.innerHTML = this.responseText;
                                    }
                                else 
                                    window.location = '?page=student_validations_socle_components';
                                }
                            };
                        httpRequest2.open(
                            'POST', '?page=student_validations_socle_components', true);
                        httpRequest2.setRequestHeader(
                            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                        httpRequest2.send('action=reload');
                        }
                    }
                else 
                    window.location = '?page=student_validations_socle_components';
                }
            };
        httpRequest.open(
            'POST', '?page=student_validations_socle_components', true);
        httpRequest.setRequestHeader(
            'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        httpRequest.send(
            'action=request&what=changeSubject' + '&value=' + newValue);
        }, 1000);
    };

function editEval(target) {
    /*
    on demande l'édition d'une évaluation.
    On teste d'abord si on est déja en mode édition, auquel cas on valide.
    Sinon on passe bien en mode édition.
    */
    if (EDITING) {
        var who = document.getElementById('editor_eval').parentNode;
        if (who.getAttribute('id_eleve') != undefined) {
            if (who.getAttribute('bilan_name') != target.getAttribute('bilan_name'))
                validateEval('other');
            }
        return;
        }
    // on passe donc en mode édition :
    EDITING = true;
    var oldText = target.textContent;
    target.setAttribute('old', oldText);
    editor_eval = '<input id="editor_eval" type="text" class="form-control"'
        + ' value="' + oldText + '"></input>';
    target.innerHTML = editor_eval;
    // gestion des touches entrée et échapp :
    target.onkeydown = function(event) {
        if (event.key == 'Enter' || event.which == 13 || event.keyCode == 13)
            validateEval('checkKeyPress');
        else if (event.key == 'Escape' || event.which == 27 || event.keyCode == 27)
            abortEval();
        };
    // focus bug (seen in FireFox) fixed by small delay
    function focus_text() {
        document.getElementById('editor_eval').focus();
        };
    setTimeout(focus_text, 50);
    };

function validateEval(what) {
    /*
    on sort du mode édition.
    Si l'évaluation a été modifiée, on enregistre.
    */
    var who = document.getElementById('editor_eval').parentNode;
    var newText = document.getElementById('editor_eval').value;
    if (newText == undefined)
        return;
    // suppression des éventuels caractères invisibles de début et fin :
    newText = newText.replace(/^\s+/g,'').replace(/\s+$/g,'');
    newText = newText.toUpperCase();
    if (newText.length > 1)
        newText = newText.charAt(0);
    oldText = who.getAttribute('old').replace(/^\s+/g,'').replace(/\s+$/g,'');
    if (compare.test(newText)) {
        if (newText != oldText) {
            var id_eleve = who.getAttribute('id_eleve');
            var date = who.getAttribute('date');
            var bilan_name = who.getAttribute('bilan_name');
            var id_prof = who.getAttribute('id_prof');
            var nom_prof = who.getAttribute('nom_prof');
            setTimeout(function() {
                var httpRequest = new XMLHttpRequest();
                httpRequest.onreadystatechange = function() {
                    if (this.readyState == 4) {
                        if (this.status == 200) {
                            if (this.responseText == 'INACTIVE_TIME')
                                window.location = '?page=login';
                            else if (this.responseText != false) {
                                while (who.classList.length > 0)
                                    who.classList.remove(who.classList.item(0));
                                who.classList.add(this.responseText, 'edit_eval');
                                }
                            else {
                                while (who.classList.length > 0)
                                    who.classList.remove(who.classList.item(0));
                                who.classList.add('edit_eval');
                                }
                            }
                        else
                            window.location = '?page=student_validations_socle_components';
                        }
                    };
                httpRequest.open(
                    'POST', '?page=student_validations_socle_components', true);
                httpRequest.setRequestHeader(
                    'Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                httpRequest.send('action=request&what=changeEval' 
                    + '&id_eleve=' + id_eleve 
                    + '&date=' + date + '&bilan_name=' + bilan_name 
                    + '&id_prof=' + id_prof + '&nom_prof=' + nom_prof 
                    + '&value=' + newText);
                }, 1000);
            }
        who.innerHTML = '';
        if (newText == '')
            newText = '&nbsp;';
        who.innerHTML = newText;
        }
    else {
        alert(alertMessage);
        who.innerHTML = who.getAttribute('old');
        }
    who.removeAttribute('old');
    EDITING = false;
    };

function abortEval() {
    /*
    abandon de modification. On remet la valeur initiale.
    */
    var who = document.getElementById('editor_eval').parentNode;
    var oldText = who.getAttribute('old');
    if (oldText == undefined)
        return;
    who.textContent = oldText;
    who.removeAttribute('old');
    EDITING = false;
    };

function matches(el, selector) {
    return (
        el.matches || el.matchesSelector || el.msMatchesSelector 
        || el.mozMatchesSelector || el.webkitMatchesSelector 
        || el.oMatchesSelector).call(el, selector);
    };





/***************************************************************
                        ÉVÉNEMENTS
***************************************************************/

var container = document.getElementById('container');

container.onclick = function(event) {
    /*
    pour plier-déplier les détails des compétences partagées.
    Mais aussi gérer les sélecteurs.
    */
    var target = event.target;
    var targetId = target.getAttribute('id');
    if (target.classList.contains('competence_label')) {
        // plier-déplier les détails des compétences partagées :
        var elems = document.querySelectorAll('tr.detail_child');
        for (var i = 0; i < elems.length; i++) {
            if (elems[i].classList.contains(targetId))
                elems[i].classList.toggle('hidden');
            }
        }
    else if (target.classList.contains('edit_eval'))
        editEval(target);
    else if (EDITING) {
        var who = document.getElementById('editor_eval').parentNode;
        if (who.getAttribute('id_eleve') != undefined) {
            validateEval('document');
            return;
            }
        }
    else if (targetId == 'collapse') {
        // tout replier :
        var elems = document.querySelectorAll('tr.detail_child');
        for (var i = 0; i < elems.length; i++)
            if (! elems[i].classList.contains('hidden'))
                elems[i].classList.add('hidden');
        }
    else if (targetId == 'expand') {
        // tout déplier :
        var elems = document.querySelectorAll('tr.detail_child');
        for (var i = 0; i < elems.length; i++)
            if (elems[i].classList.contains('hidden'))
                elems[i].classList.remove('hidden');
        }
    else if (matches(target, 'a')) {
        var parentId = target.parentNode.parentNode.parentNode.getAttribute('id');
        if (parentId == 'select-subject') {
            // changement de matière :
            changeSubject(target.getAttribute('what'));
            }
        }
    };
