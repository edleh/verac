<?php
/**
#-----------------------------------------------------------------
# This file is a part of VÉRAC project.
# Name:         VÉRAC - Vers une Évaluation Réussie Avec les Compétences
# Copyright:    (C) VÉRAC authors
# License:      GNU General Public License version 3
# Website:      https://verac.tuxfamily.org
# Email:        verac at tuxfamily.org
#-----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#-----------------------------------------------------------------
**/



class TRANSLATIONS {

    public function tr($texte) {
        $tr = array();

        // login.php
        $tr['VÉRAC Web Site'] = 'Site du projet VÉRAC';
        $tr['Username'] = "Nom d'utilisateur";
        $tr['Password'] = 'Mot de passe';
        $tr['Connect'] = 'Se connecter';
        $tr['Connecting...'] = 'Connexion...';
        $tr['Enter a username.'] = "Saisissez un nom d'utilisateur.";
        $tr['Enter a password.'] = 'Saisissez un mot de passe.';
        $tr['OK'] = 'OK';
        $tr['The connection failed!'] = 'La connexion a échoué !';
        $tr['The user name is incorrect.'] = "Le nom d'utilisateur est incorrect.";
        $tr['The password is incorrect.'] = 'Le mot de passe est incorrect.';
        $tr['You have exceeded the number of allowed errors.'] = 
            "Vous avez dépassé le nombre d'erreurs autorisées.";
        $tr['Your IP address has been recorded and is temporarily blocked.'] = 
            'Votre adresse IP a été enregistrée et est bloquée temporairement.';
        $tr['Reconnect in 15 minutes.'] = 'Reconnectez-vous dans 15 minutes.';
        $tr['If you fail to login, click here.'] = 
            "Si vous ne parvenez pas à vous connecter, cliquez ici.";

        // travaux.php
        $tr['Site temporarily unavailable'] = 'Site temporairement indisponible';
        $tr['The site is currently down for maintenance.'] = 
            'Le site est actuellement en maintenance.';
        $tr['Thank you to login later.'] = 'Merci de vous reconnecter ultérieurement.';
        $tr['This section is under construction.'] = 'Cette partie est en cours de construction.';
        $tr['This part is not yet done.'] = "Cette partie n'est pas encore faite.";

        // error.php
        $tr['Page Not Found!'] = 'Page introuvable !';
        $tr['The page you requested can not be displayed.'] = 
            'La page que vous avez demandée ne peut pas être affichée.';
        $tr['The results DB is currently unavailable.'] = 
            'La base des résultats est actuellement indisponible.';
        $tr['Please wait a few minutes and try again.'] = 
            'Veuillez patienter quelques minutes avant de réessayer.';

        // navbar.php
        $tr['Disconnect'] = 'Se déconnecter';
        $tr['Help'] = 'Aide';
        $tr['Classes'] = 'Classes';
        $tr['Synthesis'] = 'Synthèse';
        $tr['Opinions'] = 'Appréciations';
        $tr['Shared competences'] = 'Compétences partagées';
        $tr['Follows'] = 'Suivis';
        $tr['List'] = 'Liste';
        $tr['Trombinoscope'] = 'Trombinoscope';
        $tr['Custom Trombinoscope'] = 'Trombinoscope personnalisé';
        $tr['Students'] = 'Élèves';
        $tr['Council'] = 'Conseil';
        $tr['Balances'] = 'Bilans';
        $tr['Referential'] = 'Référentiel';
        $tr['Confidential'] = 'Confidentiel';
        $tr['Validations'] = 'Validations';
        $tr['Details'] = 'Détails';
        $tr['Papers'] = 'Documents';
        $tr['Assess'] = 'Évaluer';
        $tr['Tools'] = 'Outils';
        $tr['News'] = 'Infos';
        $tr['Calculator'] = 'Calculatrice';
        $tr['Results'] = 'Résultats';
        $tr['Period'] = 'Période';
        $tr['Class'] = 'Classe';
        $tr['Group'] = 'Groupe';
        $tr['Tableau'] = 'Tableau';
        $tr['Vue'] = 'Vue';
        $tr['Student'] = 'Élève';
        $tr['Previous'] = 'Précédent';
        $tr['Next'] = 'Suivant';
        $tr['Navbar style'] = 'Aspect du menu';
        $tr['Printable Version'] = 'Version imprimable';
        $tr['Statistics'] = 'Statistiques';
        $tr['Statistics of website visits:'] = 'Statistiques des visites du site web :';
        $tr['Change language'] = 'Changer de langue';

        // messages divers lors des demandes de pages
        $tr['Access to this page is prohibited.'] = "L'accès à cette page vous est interdit.";
        $tr['Select a period.'] = 'Sélectionnez une période.';
        $tr['Select a class.'] = 'Sélectionnez une classe.';
        $tr['Select a student.'] = 'Sélectionnez un élève.';
        $tr['Select a subject.'] = 'Sélectionnez une matière.';
        $tr['DEMO VERSION:'] = 'VERSION DÉMO :';
        $tr['changes will not be saved.'] = 'les changements ne seront pas enregistrés.';

        // class_synthese.php, class_shared.php
        $tr['Class Synthesis: '] = 'Synthèse de la classe : ';
        $tr['Shared competences: '] = 'Compétences partagées : ';
        $tr['CLASS'] = 'CLASSE';
        $tr['REMARKS:'] = 'REMARQUES :';
        $tr['percentages are calculated on all the balances present on the bulletin '
            .'(and not on the columns of this table);'] = 
            "les pourcentages sont calculés sur l'ensemble des bilans présents sur les bulletins "
            .'(et non sur les colonnes de ce tableau) ;';
        $tr['percentages are rounded to the nearest integer '
            .'(their sum is therefore not necessarily 100%).'] = 
            "les pourcentages sont arrondis à l'entier le plus proche "
            .'(leur somme ne fait donc pas forcément 100%).';

        // class_opinions.php
        $tr['Class Opinions: '] = 'Appréciations sur la classe : ';
        $tr['At least one student in the class belongs to one of the listed groups.'] = 
            "Au moins un élève de la classe appartient à l'un des groupes listés.";
        $tr['Sujects of Bulletin'] = 'Matières du bulletin';
        $tr['Special subjects (Principal Teacher, School Life, ...)'] = 
            'Matières spéciales (Prof Principal, Vie Scolaire, ...)';
        $tr['Other subjects (Clubs, ...)'] = 'Autres matières (Clubs, ...)';

        // class_list.php
        $tr['Class List: '] = 'Liste des élèves de la classe : ';
        $tr['Birthday'] = 'Date de naissance';
        $tr['Login'] = 'Login';
        $tr['Initial password'] = 'Mot de passe initial';
        $tr['Changed'] = 'Changé';

        // class_trombinoscope.php
        $tr['Class Trombinoscope: '] = 'Trombinoscope de la classe : ';
        $tr['Group Trombinoscope: '] = 'Trombinoscope du groupe : ';
        $tr['Group Name:'] = 'Nom du groupe :';
        $tr['You can also make a custom trombinoscope.'] = 
            'Vous pouvez aussi réaliser un trombinoscope personnalisé.';
        $tr['Create trombinoscope'] = 'Créer le trombinoscope';
        $tr['Enter a name for your group and select the students '
            .'by putting them in the right list.'] = 
            'Donnez un nom à votre groupe, puis sélectionnez les élèves '
            .'en les mettant dans la liste de droite.';
        $tr['Click the button to finish.'] = 'Cliquez sur le bouton pour terminer.';
        $tr['students'] = 'élèves';

        // class_statistics.php
        $tr['Week'] = 'Semaine';
        $tr['from'] = 'du';
        $tr['to'] = 'au';

        // student_balances.php et student_council.php
        $tr['STATEMENT OF BALANCE IN '] = 'RELEVÉ DES BILANS AU ';
        $tr['The results are not yet available.'] = 
            'Les résultats ne sont pas encore accessibles.';
        $tr['But you can see the detail of the assessments by subject.'] = 
            'Mais vous pouvez consulter le détail des évaluations par matière.';
        $tr['Average rating:'] = 'Note moyenne :';
        $tr['standard deviation:'] = 'écart-type :';
        $tr['minimum rating:'] = 'note minimale :';
        $tr['maximum rating:'] = 'note maximale :';
        $tr['Number of half-days absence'] = 
            "Nombre de demi-journées d'absence";
        $tr['Number of half-days of justified absence'] = 
            "Nombre de demi-journées d'absence justifiées";
        $tr['Number of half-days of unjustified absence'] = 
            "Nombre de demi-journées d'absence non justifiées";
        $tr['Number of delays'] = 'Nombre de retards';
        $tr['Number of classroom hours missed'] = 
            "Nombre d'heures de cours manquées";
        $tr['Rating of school life'] = 'Note de vie scolaire';
        $tr['SCHOOL LIFE'] = 'VIE SCOLAIRE';
        $tr['Edit synthesis'] = 'Éditer ou créer la synthèse';
        $tr['Close'] = 'Fermer';
        $tr['Save changes'] = 'Enregistrer les modifications';
        $tr['Saving...'] = 'Enregistrement...';
        $tr['SYNTHESIS'] = 'SYNTHÈSE';
        $tr['Summary (camembert):'] = 'Résumé (camembert) :';
        $tr['Number of '] = 'Nombre de ';
        $tr['By subject (radar):'] = 'Par matière (radar) :';
        $tr['Click to show/hide details.'] = 'Cliquez pour afficher/masquer les détails.';
        $tr['Changes in results'] = 'Évolution des résultats';
        $tr['COMPONENTS OF THE SOCLE'] = 'COMPOSANTES DU SOCLE';

        // student_referential.php etc
        $tr['STATEMENT OF REFERENTIAL IN '] = 'RELEVÉ DU RÉFÉRENTIEL AU ';
        $tr['Note: this statement reflects only the current school year.'] = 
            "Remarque : ce relevé ne tient compte que de l'année scolaire en cours.";
        $tr['STATEMENT OF CONFIDENTIAL IN '] = 'ÉVALUATIONS CONFIDENTIELLES AU ';
        $tr['Unevaluated'] = 'Non évalués';
        $tr['By rubric (radar):'] = 'Par rubrique (radar) :';

        // student_validations.php, class_validations.php
        $tr['VALIDATIONS OF REFERENTIAL IN '] = 'VALIDATIONS DU RÉFÉRENTIEL AU ';
        $tr['Validations of referential'] = 'Validations du référentiel';
        $tr['Validations of a student'] = "Validations d'un élève";
        $tr['Synthesis of a class'] = "Synthèse d'une classe";
        $tr['Synthesis of the class validations: '] = 'Synthèse des validations de la classe : ';
        $tr['The validations DB is currently unavailable.'] = 
            'La base des validations est actuellement indisponible.';
        $tr['Validation of the referential is disabled.'] = 
            'La validation du référentiel est désactivée.';
        $tr['All subjects'] = 'Toutes les matières';
        $tr['Collapse all'] = 'Replier tout';
        $tr['Expand all'] = 'Déplier tout';
        $tr['Calculated by VÉRAC.'] = 'Calculé par VÉRAC.';
        $tr['Validated by: '] = 'Validé par : ';

        // LSU : class_lsu_components.php, student_socle_components.php
        $tr['Components of the socle'] = 'Composantes du socle';
        $tr['Components of the socle:'] = 'Composantes du socle :';
        $tr['LEGEND:'] = 'LÉGENDE :';
        $tr['Components of the socle for the class: '] = 'Composantes du socle pour la classe : ';

        $tr['LSU_CPD_FRA'] = "Comprendre, s'exprimer en utilisant la langue française à l'oral et à l'écrit";
        $tr['LSU_CPD_ETR'] = "Comprendre, s'exprimer en utilisant une langue étrangère et, le cas échéant, une langue régionale";
        $tr['LSU_CPD_SCI'] = "Comprendre, s'exprimer en utilisant les langages mathématiques, scientifiques et informatiques";
        $tr['LSU_CPD_ART'] = "Comprendre, s'exprimer en utilisant les langages des arts et du corps";
        $tr['LSU_MET_APP'] = "Les méthodes et outils pour apprendre";
        $tr['LSU_FRM_CIT'] = "La formation de la personne et du citoyen";
        $tr['LSU_SYS_NAT'] = "Les systèmes naturels et les systèmes techniques";
        $tr['LSU_REP_MND'] = "Les représentations du monde et l'activité humaine";
        $tr['Note: this statement takes account of previous years.'] = 
            "Remarque : ce relevé tient compte des années précédentes.";
        $tr['COMPONENTS OF THE SOCLE IN '] = 'COMPOSANTES DU SOCLE AU ';

        $tr['COMPLEMENTARY TEACHING'] = 'ENSEIGNEMENT DE COMPLÉMENT';
        $tr['Note: the positioning can only be J or V.'] = 'Remarque : le positionnement ne peut être que J ou V.';
        $tr['LSU_ENSCOMP_LCA'] = "Langues et cultures de l'Antiquité";
        $tr['LSU_ENSCOMP_CHK'] = 'Chant choral';

        // student_details.php
        $tr['Subject'] = 'Matière';
        $tr['Balances'] = 'Bilans';
        $tr['Items'] = 'Items';
        $tr['Each table row corresponds to a rated balance in the selected subject.'] = 
            "Chaque ligne d'un tableau correspond à un bilan évalué dans la matière sélectionnée.";
        $tr['The items included in the calculation are displayed by clicking on a balance.'] = 
            'Les items intervenant dans le calcul sont affichés en cliquant sur un bilan.';
        $tr['The values ​​of the balance and items are shown in the Value column.'] = 
            'Les valeurs du bilan et des items sont indiqués dans la colonne Valeur.';
        $tr['For balances, the color displayed in the Value column '
            .'depends on the settings chosen by the teacher.'] = 
            'Pour les bilans, la couleur affichée dans la colonne Valeur '
            .'dépend des réglages choisis par le professeur.';
        $tr['Tips to grow can be displayed if there is a link in the last column.'] = 
            "Des conseils pour progresser peuvent être affichés "
            ."s'il y a un lien dans la dernière colonne.";
        $tr['Each table row corresponds to a rated item in the selected subject.'] = 
            'Chaque ligne du tableau correspond à un item évalué dans la matière sélectionnée.';
        $tr['The balances for the calculation involving an item '
            .'are displayed by clicking on this item.'] = 
            'Les bilans pour le calcul desquels intervient un item '
            .'sont affichés en cliquant sur cet item.';
        $tr['The values ​​of the item and balances are shown in the Value column.'] = 
            "Les valeurs de l'item et des bilans sont indiqués dans la colonne Valeur.";
        $tr['Disciplinary Competences of Bulletin'] = 'Compétences disciplinaires du bulletin';
        $tr['Other Disciplinary Competences'] = 'Autres compétences disciplinaires';
        $tr['Shared Competences of Bulletin'] = 'Compétences partagées du bulletin';
        $tr['Referential Competences'] = 'Compétences du référentiel';
        $tr['Confidential Competences'] = 'Compétences confidentielles';
        $tr['Balances and associated items (click on a balance to view items)'] = 
            'Bilans et items associés (cliquez sur un bilan pour afficher les items)';
        $tr['Value'] = 'Valeur';
        $tr['Tips'] = 'Conseils';
        $tr['Items and associated balances (click on item to view balances)'] = 
            'Items et bilans associés (cliquez sur un item pour afficher les bilans)';
        $tr['Click to show tips.'] = 'Cliquez pour afficher les conseils.';
        $tr['Tips to grow'] = 'Conseils pour progresser';
        $tr['show'] = 'afficher';
        $tr['Unrelated to balance items'] = 'Items non reliés à un bilan';
        $tr['uncalculated balance'] = 'bilan non calculé';

        // student_follows.php et class_follows.php
        $tr['FOLLOWED ASSESSMENT'] = 'ÉVALUATIONS SUIVIES';
        $tr['reverse chronological directory'] = 'sens chronologique inverse';
        $tr['chronological directory'] = 'sens chronologique';
        $tr['This student is not followed.'] = "Cet élève n'est pas suivi.";
        $tr['List of competences followed'] = 'Liste des compétences suivies';
        $tr['Assessments'] = 'Évaluations';
        $tr['Date'] = 'Date';
        $tr['Schedule'] = 'Horaire';
        $tr['Subject'] = 'Matière';
        $tr['Remark'] = 'Remarque';
        $tr['FOLLOWED ASSESSMENT: '] = 'ÉVALUATIONS SUIVIES : ';
        $tr['No student followed in this class.'] = "Pas d'élève suivi dans cette classe.";

        // papers.php et student_papers.php
        $tr['Documents available'] = 'Documents disponibles';
        $tr['General documents'] = 'Documents généraux';
        $tr['Results'] = 'Résultats';
        $tr['Confidential documents'] = 'Documents confidentiels';
        $tr['File moved, renamed, or nonexistent'] = 'Fichier déplacé, renommé ou inexistant';
        $tr['error'] = 'erreur';
        $tr['Click to download the document.'] = 'Cliquez pour télécharger le document.';
        $tr['download'] = 'télécharger';

        // password.php et student_password.php
        $tr['CHANGE PASSWORD'] = 'CHANGER DE MOT DE PASSE';
        $tr['CHANGE THE PASSWORD OF A STUDENT'] = "CHANGER LE MOT DE PASSE D'UN ÉLÈVE";
        $tr['Login: '] = 'Login : ';
        $tr['Initial password: '] = 'Mot de passe initial : ';
        $tr['Current password:'] = 'Mot de passe actuel :';
        $tr['New password:'] = 'Nouveau mot de passe :';
        $tr['Confirmation:'] = 'Confirmation :';
        $tr['Change'] = 'Modifier';
        $tr['Change in progress ...'] = 'Modification en cours...';
        $tr['Enter the current password.'] = 'Saisissez le mot de passe actuel.';
        $tr['Enter a new password.'] = 'Saisissez un nouveau mot de passe.';
        $tr['Confirm new password.'] = 'Confirmez le nouveau mot de passe.';
        $tr['Error in the current password.'] = 'Erreur dans le mot de passe actuel.';
        $tr['The new password does not match the confirmation.'] = 
            'Le nouveau mot de passe ne correspond pas à la confirmation.';
        $tr['There was a problem. The change could not be performed.'] = 
            "Il y a eu un problème. Le changement n'a pas pu être effectué.";
        $tr['The initial password has not been changed!'] = 
            "Le mot de passe initial n'a pas été changé !";
        $tr['The password has been changed.'] = 'Le mot de passe a été changé.';

        // infos.php
        $tr["Welcome to the VÉRAC's web interface."] = "Bienvenue dans l'interface web de VÉRAC.";
        $tr['Last data recovery:'] = 'Dernière récupération :';
        $tr['For better security, consider changing your password!'] = 
            'Pour une meilleure sécurité, pensez à changer de mot de passe !';
        $tr['(menu: Tools → Password)'] = '(menu : Outils → Mot de passe)';
        $tr['Site Configuration'] = 'Configuration du site';

        $tr['General Restrictions'] = 'Restrictions générales';
        $tr['students have access to the site'] = 'les élèves ont accès au site';
        $tr['students can view the details of shared competences'] = 
            'les élèves peuvent afficher les détails des compétences partagées';
        $tr['students see the "Class" column'] = 
            'les élèves voient la colonne "Classe"';
        $tr['teachers can validate the referential'] = 
            'les professeurs peuvent valider le référentiel';

        $tr['Pages authorized for students (all periods)'] = 
            'Pages autorisées aux élèves (toutes périodes)';
        $tr['Balances'] = 'Bilans';
        $tr['Referential'] = 'Référentiel';
        $tr['Details'] = 'Détails';
        $tr['Follows'] = 'Suivis';
        $tr['Opinions (of the groups)'] = 'Appréciations (sur les groupes)';
        $tr['Papers'] = 'Documents';
        $tr['Calculator'] = 'Calculatrice';

        $tr['Restrictions relating only to the current period'] = 
            'Restrictions ne concernant que la période actuelle';
        $tr['students have access to the actual period'] = 
            'les élèves ont accès à la période actuelle';
        $tr['students see the opinions'] = 'les élèves voient les appréciations';
        $tr['students see the notes (for classes with notes)'] = 
            'les élèves voient les notes (pour les classes à notes)';

        $tr['WebSite Version'] = "Version de l'interface web";
        $tr['State:'] = 'État :';
        $tr['Installed Version: '] = 'Version installée : ';
        $tr['Current Version: '] = 'Version actuelle : ';
        $tr['The web interface is updated'] = "L'interface web est à jour";
        $tr['The web interface must be updated!'] = "L'interface web doit être mise à jour !";
        $tr['Not available'] = 'Non disponible';
        $tr['Other information'] = 'Autres renseignements';
        $tr['Operating System: '] = "Système d'exploitation : ";
        $tr['Browser: '] = 'Navigateur : ';
        $tr['PHP version: '] = 'Version de PHP : ';

        // calculator.php
        $tr['Calculate equivalence note'] = 'Calculer une équivalence en note';
        $tr['Indicate the number of each color, then click on the <strong>Calculate</strong> button.'] = 
            'Indiquez le nombre de chaque couleur, puis cliquez sur le bouton <strong>Calculer</strong>.';
        $tr['Calcul'] = 'Calculer';
        $tr['Description of the algorithm'] = "Description de l'algorithme de calcul";

        // assess.php
        $tr['No database.'] = 'Pas de base de données.';
        $tr['The database of your assessments is not available.'] = 
            "La base de données de vos évaluations n'est pas disponible.";
        $tr['Use VÉRAC software to create your database and send it to the website.'] = 
            'Utilisez le logiciel VÉRAC pour créer votre base, puis envoyez-la sur le site web.';
        $tr['Your data is not writable.'] = 'Vos données ne sont pas accessibles en écriture.';
        $tr['No changes you make will be taken into account.'] = 
            'Aucun des changements que vous effectuerez ne sera pris en compte.';
        $tr['Notify your administrator to resolve the problem.'] = 
            'Prévenez votre administrateur pour résoudre le problème.';
        $tr['All Periodes'] = 'Toutes les périodes';
        $tr['Annual Report'] = 'Bilan annuel';
        $tr['All Groups'] = 'Tous les groupes';
        $tr['Number of items'] = "Nombre d'items";
        $tr['Percentage of items'] = "Pourcentage d'items";
        $tr['Notes'] = 'Notes';
        $tr['Note calculated from items'] = 'Note calculée à partir des items';
        $tr['STUDENT'] = 'ÉLÈVE';
        $tr['GROUP'] = 'GROUPE';
        $tr['APPRECIATION'] = 'APPRÉCIATION';
        $tr['nowhere'] = 'nulle part';
        $tr['everywhere'] = 'partout';
        $tr['Average'] = 'Moyenne';
        $tr['Date:'] = 'Date :';
        $tr['format: yyyy-mm-dd'] = 'format : aaaa-mm-jj';
        $tr['Click to edit.'] = 'Cliquez pour modifier.';
        $tr['Edit opinion'] = 'Éditer ou créer une appréciation';
        $tr['Edit remark'] = 'Écrire une remarque';
        $tr['INCORRECT ENTRY!'] = 'MAUVAISE SAISIE !';
        $tr['Use the keys: '] = 'Utilisez les touches : ';
        $tr['Select horaire before evaluate!'] = "Sélectionnez un horaire avant d'évaluer !";
        $tr['compilation'] = 'compilation';
        $tr['Recalculate all balances&#10;(of the selected group)'] = 
            'Recalculer tous les bilans&#10;(du groupe sélectionné)';

        $tr['Items'] = 'Items';
        //$tr['Statistics'] = 'Statistiques';
        $tr['Balances'] = 'Bilans';
        $tr['Bulletin'] = 'Bulletin';
        //$tr['Opinions'] = 'Appréciations';
        $tr['Followed students'] = 'Élèves suivis';


        $result = $texte;
        if (array_key_exists($texte, $tr))
            $result = $tr[$texte];
        return $result;
        }

    }
?>
